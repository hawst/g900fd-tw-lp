.class public LMD;
.super Ljava/lang/Object;
.source "DragDetector.java"


# instance fields
.field private final a:F

.field private final a:I

.field private a:Z

.field private b:F

.field private b:I

.field private b:Z

.field private c:F

.field private c:I

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-boolean v0, p0, LMD;->a:Z

    .line 25
    iput-boolean v0, p0, LMD;->b:Z

    .line 26
    iput-boolean v0, p0, LMD;->c:Z

    .line 32
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v0

    iput v0, p0, LMD;->a:I

    .line 33
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, LMD;->a:F

    .line 34
    return-void
.end method

.method private a(FF)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 122
    iget v0, p0, LMD;->b:I

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    cmpl-float v0, p2, v2

    if-gtz v0, :cond_1

    :cond_0
    iget v0, p0, LMD;->c:I

    iget v1, p0, LMD;->b:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_2

    cmpg-float v0, p2, v2

    if-gez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 84
    iput-boolean v0, p0, LMD;->a:Z

    .line 85
    iput-boolean v0, p0, LMD;->b:Z

    .line 86
    iput-boolean v0, p0, LMD;->c:Z

    .line 87
    iput v1, p0, LMD;->b:F

    .line 88
    iput v1, p0, LMD;->c:F

    .line 89
    return-void
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 37
    iput p1, p0, LMD;->c:I

    .line 38
    div-int/lit8 v0, p1, 0xa

    const/high16 v1, 0x41800000    # 16.0f

    iget v2, p0, LMD;->a:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, LMD;->b:I

    .line 39
    return-void
.end method

.method public a(Landroid/view/MotionEvent;Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 42
    iget-boolean v0, p0, LMD;->b:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LMD;->c:Z

    if-nez v0, :cond_0

    iget v0, p0, LMD;->c:I

    if-nez v0, :cond_1

    .line 73
    :cond_0
    :goto_0
    return-void

    .line 46
    :cond_1
    iget-boolean v0, p0, LMD;->a:Z

    if-nez v0, :cond_2

    .line 47
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, LMD;->b:F

    .line 48
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, LMD;->c:F

    .line 49
    iput-boolean v8, p0, LMD;->a:Z

    goto :goto_0

    .line 53
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 54
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 56
    iget v2, p0, LMD;->b:F

    sub-float v6, v0, v2

    .line 57
    iget v2, p0, LMD;->c:F

    sub-float v7, v1, v2

    .line 60
    invoke-direct {p0, v0, v6}, LMD;->a(FF)Z

    move-result v2

    if-nez v2, :cond_3

    float-to-int v2, v6

    float-to-int v3, v0

    float-to-int v4, v1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p2

    .line 61
    invoke-virtual/range {v0 .. v5}, LMD;->a(Landroid/view/View;IIIZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 62
    iput-boolean v8, p0, LMD;->c:Z

    goto :goto_0

    .line 66
    :cond_3
    iget v0, p0, LMD;->a:I

    int-to-float v0, v0

    cmpl-float v0, v7, v0

    if-lez v0, :cond_4

    .line 67
    iput-boolean v8, p0, LMD;->c:Z

    goto :goto_0

    .line 70
    :cond_4
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, LMD;->a:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 71
    iput-boolean v8, p0, LMD;->b:Z

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, LMD;->b:Z

    return v0
.end method

.method protected a(Landroid/view/View;IIIZ)Z
    .locals 10

    .prologue
    const/4 v5, 0x1

    .line 102
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    move-object v6, p1

    .line 103
    check-cast v6, Landroid/view/ViewGroup;

    .line 104
    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v0

    add-int v8, p3, v0

    .line 105
    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v0

    add-int v9, p4, v0

    .line 106
    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 108
    add-int/lit8 v0, v0, -0x1

    move v7, v0

    :goto_0
    if-ltz v7, :cond_2

    .line 109
    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 110
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v0

    if-lt v8, v0, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v0

    if-ge v8, v0, :cond_1

    .line 111
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    if-lt v9, v0, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v0

    if-ge v9, v0, :cond_1

    .line 112
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v0

    sub-int v3, v8, v0

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    sub-int v4, v9, v0

    move-object v0, p0

    move v2, p2

    invoke-virtual/range {v0 .. v5}, LMD;->a(Landroid/view/View;IIIZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    :cond_0
    :goto_1
    return v5

    .line 108
    :cond_1
    add-int/lit8 v0, v7, -0x1

    move v7, v0

    goto :goto_0

    .line 118
    :cond_2
    if-eqz p5, :cond_3

    neg-int v0, p2

    invoke-virtual {p1, v0}, Landroid/view/View;->canScrollHorizontally(I)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, LMD;->c:Z

    return v0
.end method
