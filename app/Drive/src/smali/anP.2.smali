.class public final LanP;
.super LanO;
.source "ChainedImageDecodeFetcher.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LanO",
        "<",
        "Lcom/google/android/apps/docs/utils/FetchSpec;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LanS;


# direct methods
.method private constructor <init>(Laoo;LanS;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoo",
            "<",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "LanS;",
            "Ljava/util/List",
            "<",
            "LalD",
            "<*>;>;",
            "Ljava/util/List",
            "<",
            "LakH",
            "<",
            "Ljava/lang/Long;",
            "*>;>;)V"
        }
    .end annotation

    .prologue
    .line 89
    invoke-direct {p0, p1, p3, p4}, LanO;-><init>(Laoo;Ljava/util/List;Ljava/util/List;)V

    .line 91
    iput-object p2, p0, LanP;->a:LanS;

    .line 92
    return-void
.end method

.method synthetic constructor <init>(Laoo;LanS;Ljava/util/List;Ljava/util/List;LanQ;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3, p4}, LanP;-><init>(Laoo;LanS;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/docs/utils/FetchSpec;)LbsU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            ")",
            "LbsU",
            "<",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, LanP;->a:LanS;

    invoke-virtual {v0, p1}, LanS;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 140
    invoke-super {p0}, LanO;->a()V

    .line 141
    iget-object v0, p0, LanP;->a:LanS;

    invoke-virtual {v0}, LanS;->a()V

    .line 142
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 105
    invoke-super {p0, p1, p2}, LanO;->a(J)V

    .line 106
    iget-object v0, p0, LanP;->a:LanS;

    invoke-virtual {v0, p1, p2}, LanS;->a(J)V

    .line 107
    return-void
.end method

.method public b(Lcom/google/android/apps/docs/utils/FetchSpec;)LbsU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            ")",
            "LbsU",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, LanP;->a:LanS;

    invoke-virtual {v0, p1}, LanS;->a(Lcom/google/android/apps/docs/utils/FetchSpec;)LbsU;

    move-result-object v0

    return-object v0
.end method
