.class LadX;
.super Ljava/lang/Thread;
.source "PipeExposer.java"


# instance fields
.field final synthetic a:LadW;

.field final synthetic a:Ljava/io/OutputStream;


# direct methods
.method constructor <init>(LadW;Ljava/lang/String;Ljava/io/OutputStream;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, LadX;->a:LadW;

    iput-object p3, p0, LadX;->a:Ljava/io/OutputStream;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 61
    :try_start_0
    iget-object v0, p0, LadX;->a:LadW;

    iget-object v0, v0, LadW;->a:LadV;

    invoke-static {v0}, LadV;->a(LadV;)LadJ;

    move-result-object v0

    iget-object v1, p0, LadX;->a:LadW;

    invoke-static {v1}, LadW;->a(LadW;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LadX;->a:Ljava/io/OutputStream;

    invoke-interface {v0, v1, v2}, LadJ;->a(Ljava/lang/String;Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    :try_start_1
    iget-object v0, p0, LadX;->a:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 71
    :goto_0
    return-void

    .line 67
    :catch_0
    move-exception v0

    .line 68
    const-string v1, "PipeExposer"

    const-string v2, "Close pipe failed."

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0

    .line 62
    :catch_1
    move-exception v0

    .line 63
    :try_start_2
    const-string v1, "PipeExposer"

    const-string v2, "Obtain content failed."

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 66
    :try_start_3
    iget-object v0, p0, LadX;->a:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 67
    :catch_2
    move-exception v0

    .line 68
    const-string v1, "PipeExposer"

    const-string v2, "Close pipe failed."

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0

    .line 65
    :catchall_0
    move-exception v0

    .line 66
    :try_start_4
    iget-object v1, p0, LadX;->a:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 69
    :goto_1
    throw v0

    .line 67
    :catch_3
    move-exception v1

    .line 68
    const-string v2, "PipeExposer"

    const-string v3, "Close pipe failed."

    invoke-static {v2, v1, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_1
.end method
