.class public final enum LaEZ;
.super Ljava/lang/Enum;
.source "LocalFileEntryTable.java"

# interfaces
.implements Lbjv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaEZ;",
        ">;",
        "Lbjv",
        "<",
        "LaFr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaEZ;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final synthetic a:[LaEZ;

.field public static final enum b:LaEZ;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum c:LaEZ;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum d:LaEZ;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum e:LaEZ;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum f:LaEZ;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum g:LaEZ;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field private final a:LaFr;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/16 v8, 0x67

    const/16 v7, 0x66

    .line 40
    new-instance v0, LaEZ;

    const-string v1, "TITLE"

    .line 41
    invoke-static {}, LaEY;->b()LaEY;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "title"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 42
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    .line 43
    invoke-virtual {v2, v8}, LaFt;->b(I)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, LaEZ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEZ;->a:LaEZ;

    .line 45
    new-instance v0, LaEZ;

    const-string v1, "FILE_URI"

    .line 46
    invoke-static {}, LaEY;->b()LaEY;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "fileUri"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 47
    invoke-virtual {v2, v7, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    .line 48
    invoke-virtual {v2, v8}, LaFt;->b(I)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, LaEZ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEZ;->b:LaEZ;

    .line 50
    new-instance v0, LaEZ;

    const-string v1, "THUMBNAIL_URI"

    .line 51
    invoke-static {}, LaEY;->b()LaEY;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "thumbnailUri"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 52
    invoke-virtual {v2, v7, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    .line 53
    invoke-virtual {v2, v8}, LaFt;->b(I)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, LaEZ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEZ;->c:LaEZ;

    .line 55
    new-instance v0, LaEZ;

    const-string v1, "DETAIL_PANEL_THUMBNAIL_URI"

    const/4 v2, 0x3

    .line 56
    invoke-static {}, LaEY;->b()LaEY;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "detailPanelThumbnailUri"

    sget-object v6, LaFI;->c:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 57
    invoke-virtual {v4}, LaFG;->b()LaFG;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    .line 58
    invoke-virtual {v3, v8}, LaFt;->b(I)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEZ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEZ;->d:LaEZ;

    .line 61
    new-instance v0, LaEZ;

    const-string v1, "LAST_MODIFIED_TIME"

    const/4 v2, 0x4

    .line 62
    invoke-static {}, LaEY;->b()LaEY;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "lastModifiedTime"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 63
    invoke-virtual {v3, v7, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    .line 64
    invoke-virtual {v3, v8}, LaFt;->b(I)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEZ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEZ;->e:LaEZ;

    .line 66
    new-instance v0, LaEZ;

    const-string v1, "LAST_OPENED_TIME"

    const/4 v2, 0x5

    .line 67
    invoke-static {}, LaEY;->b()LaEY;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "lastOpenedTime"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 68
    invoke-virtual {v4}, LaFG;->b()LaFG;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    .line 69
    invoke-virtual {v3, v8}, LaFt;->b(I)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEZ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEZ;->f:LaEZ;

    .line 71
    new-instance v0, LaEZ;

    const-string v1, "MIME_TYPE"

    const/4 v2, 0x6

    .line 72
    invoke-static {}, LaEY;->b()LaEY;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "mimeType"

    sget-object v6, LaFI;->c:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 73
    invoke-virtual {v3, v7, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    .line 74
    invoke-virtual {v3, v8}, LaFt;->b(I)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEZ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEZ;->g:LaEZ;

    .line 39
    const/4 v0, 0x7

    new-array v0, v0, [LaEZ;

    sget-object v1, LaEZ;->a:LaEZ;

    aput-object v1, v0, v9

    sget-object v1, LaEZ;->b:LaEZ;

    aput-object v1, v0, v10

    sget-object v1, LaEZ;->c:LaEZ;

    aput-object v1, v0, v11

    const/4 v1, 0x3

    sget-object v2, LaEZ;->d:LaEZ;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LaEZ;->e:LaEZ;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LaEZ;->f:LaEZ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LaEZ;->g:LaEZ;

    aput-object v2, v0, v1

    sput-object v0, LaEZ;->a:[LaEZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaFt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 80
    invoke-virtual {p3}, LaFt;->a()LaFr;

    move-result-object v0

    iput-object v0, p0, LaEZ;->a:LaFr;

    .line 81
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaEZ;
    .locals 1

    .prologue
    .line 39
    const-class v0, LaEZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaEZ;

    return-object v0
.end method

.method public static values()[LaEZ;
    .locals 1

    .prologue
    .line 39
    sget-object v0, LaEZ;->a:[LaEZ;

    invoke-virtual {v0}, [LaEZ;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaEZ;

    return-object v0
.end method


# virtual methods
.method public a()LaFr;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, LaEZ;->a:LaFr;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, LaEZ;->a()LaFr;

    move-result-object v0

    return-object v0
.end method
