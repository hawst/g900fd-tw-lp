.class public LGP;
.super Ljava/lang/Object;
.source "WebViewOpenActivity.java"

# interfaces
.implements LGH;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, LGP;->a:Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()LGI;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, LGP;->a:Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;)LGG;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, LGP;->a:Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->finish()V

    .line 153
    return-void
.end method

.method public a(LDL;)V
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, LGP;->a:Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;LDL;)LDL;

    .line 182
    iget-object v0, p0, LGP;->a:Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->showDialog(I)V

    .line 183
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, LGP;->a:Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->startActivity(Landroid/content/Intent;)V

    .line 148
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 162
    iget-object v0, p0, LGP;->a:Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LQr;

    const-string v1, "enableWebViewAccessibilityWorkaround"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, LGP;->a:Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;

    .line 164
    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LUs;->a(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    .line 165
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 166
    new-instance v1, LGU;

    invoke-direct {v1, v0}, LGU;-><init>(Landroid/view/accessibility/AccessibilityManager;)V

    .line 168
    iget-object v0, p0, LGP;->a:Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;)Landroid/webkit/WebView;

    move-result-object v0

    const-string v2, "accessibility"

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    :cond_0
    iget-object v0, p0, LGP;->a:Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 172
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, LGP;->a:Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;Ljava/lang/String;)V

    .line 177
    return-void
.end method
