.class public LaVl;
.super LaMV;

# interfaces
.implements LaNG;
.implements LaNk;
.implements LaNv;
.implements LaNx;
.implements LaOX;
.implements LaOl;
.implements LaOo;
.implements LaPK;
.implements LaVk;


# instance fields
.field private final a:LaMt;

.field private final a:LaMy;

.field private final a:LaNO;

.field private final a:LaVp;

.field private final a:Landroid/content/ComponentCallbacks;

.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/aq;Ljava/lang/String;LaNO;Lcom/google/android/gms/internal/fa;)V
    .locals 1

    invoke-direct {p0}, LaMV;-><init>()V

    new-instance v0, LaVm;

    invoke-direct {v0, p0}, LaVm;-><init>(LaVl;)V

    iput-object v0, p0, LaVl;->a:Landroid/content/ComponentCallbacks;

    new-instance v0, LaVp;

    invoke-direct {v0, p1, p2, p3, p5}, LaVp;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/aq;Ljava/lang/String;Lcom/google/android/gms/internal/fa;)V

    iput-object v0, p0, LaVl;->a:LaVp;

    iput-object p4, p0, LaVl;->a:LaNO;

    new-instance v0, LaMt;

    invoke-direct {v0, p0}, LaMt;-><init>(LaVl;)V

    iput-object v0, p0, LaVl;->a:LaMt;

    new-instance v0, LaMy;

    invoke-direct {v0}, LaMy;-><init>()V

    iput-object v0, p0, LaVl;->a:LaMy;

    invoke-static {p1}, LaPS;->a(Landroid/content/Context;)V

    invoke-direct {p0}, LaVl;->p()V

    return-void
.end method

.method private a(Lcom/google/android/gms/internal/an;)LaPm;
    .locals 11

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, LaVl;->a:LaVp;

    iget-object v3, v3, LaVp;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    :try_start_0
    iget-object v3, p0, LaVl;->a:LaVp;

    iget-object v3, v3, LaVp;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iget-object v4, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    :goto_0
    iget-object v3, p0, LaVl;->a:LaVp;

    iget-object v3, v3, LaVp;->a:Lcom/google/android/gms/internal/aq;

    iget-boolean v3, v3, Lcom/google/android/gms/internal/aq;->a:Z

    if-nez v3, :cond_1

    iget-object v3, p0, LaVl;->a:LaVp;

    iget-object v3, v3, LaVp;->a:LaVn;

    invoke-virtual {v3}, LaVn;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v0, 0x2

    new-array v0, v0, [I

    iget-object v3, p0, LaVl;->a:LaVp;

    iget-object v3, v3, LaVp;->a:LaVn;

    invoke-virtual {v3, v0}, LaVn;->getLocationOnScreen([I)V

    aget v3, v0, v2

    aget v4, v0, v1

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget-object v7, p0, LaVl;->a:LaVp;

    iget-object v7, v7, LaVp;->a:LaVn;

    invoke-virtual {v7}, LaVn;->getWidth()I

    move-result v7

    iget-object v8, p0, LaVl;->a:LaVp;

    iget-object v8, v8, LaVp;->a:LaVn;

    invoke-virtual {v8}, LaVn;->getHeight()I

    move-result v8

    iget-object v9, p0, LaVl;->a:LaVp;

    iget-object v9, v9, LaVp;->a:LaVn;

    invoke-virtual {v9}, LaVn;->isShown()Z

    move-result v9

    if-eqz v9, :cond_0

    add-int v9, v3, v7

    if-lez v9, :cond_0

    add-int v9, v4, v8

    if-lez v9, :cond_0

    iget v9, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-gt v3, v9, :cond_0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-gt v4, v0, :cond_0

    move v0, v1

    :goto_1
    new-instance v1, Landroid/os/Bundle;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(I)V

    const-string v2, "x"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "y"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "width"

    invoke-virtual {v1, v2, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "height"

    invoke-virtual {v1, v2, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "visible"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_2
    invoke-static {}, LaPI;->a()Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, LaVl;->a:LaVp;

    new-instance v2, LaPG;

    iget-object v3, p0, LaVl;->a:LaVp;

    iget-object v3, v3, LaVp;->a:Ljava/lang/String;

    invoke-direct {v2, v7, v3}, LaPG;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, v0, LaVp;->a:LaPG;

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPG;

    invoke-virtual {v0, p1}, LaPG;->a(Lcom/google/android/gms/internal/an;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Landroid/content/Context;

    invoke-static {v0, p0, v7}, LaPI;->a(Landroid/content/Context;LaPK;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v10

    new-instance v0, LaPm;

    iget-object v2, p0, LaVl;->a:LaVp;

    iget-object v3, v2, LaVp;->a:Lcom/google/android/gms/internal/aq;

    iget-object v2, p0, LaVl;->a:LaVp;

    iget-object v4, v2, LaVp;->a:Ljava/lang/String;

    sget-object v8, LaPI;->a:Ljava/lang/String;

    iget-object v2, p0, LaVl;->a:LaVp;

    iget-object v9, v2, LaVp;->a:Lcom/google/android/gms/internal/fa;

    move-object v2, p1

    invoke-direct/range {v0 .. v10}, LaPm;-><init>(Landroid/os/Bundle;Lcom/google/android/gms/internal/an;Lcom/google/android/gms/internal/aq;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Landroid/content/pm/PackageInfo;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/fa;Landroid/os/Bundle;)V

    return-object v0

    :catch_0
    move-exception v3

    move-object v6, v0

    goto/16 :goto_0

    :cond_0
    move v0, v2

    goto :goto_1

    :cond_1
    move-object v1, v0

    goto :goto_2
.end method

.method static synthetic a(LaVl;)LaVp;
    .locals 1

    iget-object v0, p0, LaVl;->a:LaVp;

    return-object v0
.end method

.method private a(I)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Failed to load ad: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaQc;->e(Ljava/lang/String;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaMR;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaMR;

    invoke-interface {v0, p1}, LaMR;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Could not call AdListener.onAdFailedToLoad()."

    invoke-static {v1, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    const/4 v1, -0x2

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:LaVn;

    invoke-virtual {v1, p1, v0}, LaVn;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private a(LaPF;)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-boolean v0, p1, LaPF;->a:Z

    if-eqz v0, :cond_5

    :try_start_0
    iget-object v0, p1, LaPF;->a:LaNR;

    invoke-interface {v0}, LaNR;->a()LaLm;

    move-result-object v0

    invoke-static {v0}, LaLp;->a(LaLm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v3, p0, LaVl;->a:LaVp;

    iget-object v3, v3, LaVp;->a:LaVn;

    invoke-virtual {v3}, LaVn;->getNextView()Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v4, p0, LaVl;->a:LaVp;

    iget-object v4, v4, LaVp;->a:LaVn;

    invoke-virtual {v4, v3}, LaVn;->removeView(Landroid/view/View;)V

    :cond_0
    :try_start_1
    invoke-direct {p0, v0}, LaVl;->a(Landroid/view/View;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_0
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaVn;

    invoke-virtual {v0}, LaVn;->getChildCount()I

    move-result v0

    if-le v0, v2, :cond_2

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaVn;

    invoke-virtual {v0}, LaVn;->showNext()V

    :cond_2
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    if-eqz v0, :cond_4

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaVn;

    invoke-virtual {v0}, LaVn;->getNextView()Landroid/view/View;

    move-result-object v0

    instance-of v3, v0, Lcom/google/android/gms/internal/fc;

    if-eqz v3, :cond_6

    check-cast v0, Lcom/google/android/gms/internal/fc;

    iget-object v3, p0, LaVl;->a:LaVp;

    iget-object v3, v3, LaVp;->a:Landroid/content/Context;

    iget-object v4, p0, LaVl;->a:LaVp;

    iget-object v4, v4, LaVp;->a:Lcom/google/android/gms/internal/aq;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/internal/fc;->a(Landroid/content/Context;Lcom/google/android/gms/internal/aq;)V

    :cond_3
    :goto_1
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:LaNR;

    if-eqz v0, :cond_4

    :try_start_2
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:LaNR;

    invoke-interface {v0}, LaNR;->b()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_4
    :goto_2
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaVn;

    invoke-virtual {v0, v1}, LaVn;->setVisibility(I)V

    move v0, v2

    :goto_3
    return v0

    :catch_0
    move-exception v0

    const-string v2, "Could not get View from mediation adapter."

    invoke-static {v2, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    goto :goto_3

    :catch_1
    move-exception v0

    const-string v2, "Could not add mediation view to view hierarchy."

    invoke-static {v2, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    goto :goto_3

    :cond_5
    iget-object v0, p1, LaPF;->a:Lcom/google/android/gms/internal/aq;

    if-eqz v0, :cond_1

    iget-object v0, p1, LaPF;->a:Lcom/google/android/gms/internal/fc;

    iget-object v3, p1, LaPF;->a:Lcom/google/android/gms/internal/aq;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/internal/fc;->a(Lcom/google/android/gms/internal/aq;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaVn;

    invoke-virtual {v0}, LaVn;->removeAllViews()V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaVn;

    iget-object v3, p1, LaPF;->a:Lcom/google/android/gms/internal/aq;

    iget v3, v3, Lcom/google/android/gms/internal/aq;->e:I

    invoke-virtual {v0, v3}, LaVn;->setMinimumWidth(I)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaVn;

    iget-object v3, p1, LaPF;->a:Lcom/google/android/gms/internal/aq;

    iget v3, v3, Lcom/google/android/gms/internal/aq;->c:I

    invoke-virtual {v0, v3}, LaVn;->setMinimumHeight(I)V

    iget-object v0, p1, LaPF;->a:Lcom/google/android/gms/internal/fc;

    invoke-direct {p0, v0}, LaVl;->a(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_6
    if-eqz v0, :cond_3

    iget-object v3, p0, LaVl;->a:LaVp;

    iget-object v3, v3, LaVp;->a:LaVn;

    invoke-virtual {v3, v0}, LaVn;->removeView(Landroid/view/View;)V

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v0, "Could not destroy previous mediation adapter."

    invoke-static {v0}, LaQc;->e(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private b(Z)V
    .locals 6

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    if-nez v0, :cond_1

    const-string v0, "Ad state was null when trying to ping impression URLs."

    invoke-static {v0}, LaQc;->e(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "Pinging Impression URLs."

    invoke-static {v0}, LaQc;->a(Ljava/lang/String;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPG;

    invoke-virtual {v0}, LaPG;->a()V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->b:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Landroid/content/Context;

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:Lcom/google/android/gms/internal/fa;

    iget-object v1, v1, Lcom/google/android/gms/internal/fa;->a:Ljava/lang/String;

    iget-object v2, p0, LaVl;->a:LaVp;

    iget-object v2, v2, LaVp;->a:LaPF;

    iget-object v2, v2, LaPF;->b:Ljava/util/List;

    invoke-static {v0, v1, v2}, LaPS;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    :cond_2
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:LaNF;

    if-eqz v0, :cond_3

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:LaNF;

    iget-object v0, v0, LaNF;->c:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Landroid/content/Context;

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:Lcom/google/android/gms/internal/fa;

    iget-object v1, v1, Lcom/google/android/gms/internal/fa;->a:Ljava/lang/String;

    iget-object v2, p0, LaVl;->a:LaVp;

    iget-object v2, v2, LaVp;->a:LaPF;

    iget-object v3, p0, LaVl;->a:LaVp;

    iget-object v3, v3, LaVp;->a:Ljava/lang/String;

    iget-object v4, p0, LaVl;->a:LaVp;

    iget-object v4, v4, LaVp;->a:LaPF;

    iget-object v4, v4, LaPF;->a:LaNF;

    iget-object v5, v4, LaNF;->c:Ljava/util/List;

    move v4, p1

    invoke-static/range {v0 .. v5}, LaNM;->a(Landroid/content/Context;Ljava/lang/String;LaPF;Ljava/lang/String;ZLjava/util/List;)V

    :cond_3
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:LaNE;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:LaNE;

    iget-object v0, v0, LaNE;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Landroid/content/Context;

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:Lcom/google/android/gms/internal/fa;

    iget-object v1, v1, Lcom/google/android/gms/internal/fa;->a:Ljava/lang/String;

    iget-object v2, p0, LaVl;->a:LaVp;

    iget-object v2, v2, LaVp;->a:LaPF;

    iget-object v3, p0, LaVl;->a:LaVp;

    iget-object v3, v3, LaVp;->a:Ljava/lang/String;

    iget-object v4, p0, LaVl;->a:LaVp;

    iget-object v4, v4, LaVp;->a:LaPF;

    iget-object v4, v4, LaPF;->a:LaNE;

    iget-object v5, v4, LaNE;->b:Ljava/util/List;

    move v4, p1

    invoke-static/range {v0 .. v5}, LaNM;->a(Landroid/content/Context;Ljava/lang/String;LaPF;Ljava/lang/String;ZLjava/util/List;)V

    goto/16 :goto_0
.end method

.method private b()Z
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, LaVl;->a:LaVp;

    iget-object v2, v2, LaVp;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, LaVl;->a:LaVp;

    iget-object v3, v3, LaVp;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "android.permission.INTERNET"

    invoke-static {v2, v3, v4}, LaPS;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Lcom/google/android/gms/internal/aq;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/aq;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaVn;

    iget-object v2, p0, LaVl;->a:LaVp;

    iget-object v2, v2, LaVp;->a:Lcom/google/android/gms/internal/aq;

    const-string v3, "Missing internet permission in AndroidManifest.xml."

    const-string v4, "Missing internet permission in AndroidManifest.xml. You must have the following declaration: <uses-permission android:name=\"android.permission.INTERNET\" />"

    invoke-static {v0, v2, v3, v4}, LaQb;->a(Landroid/view/ViewGroup;Lcom/google/android/gms/internal/aq;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move v0, v1

    :cond_1
    iget-object v2, p0, LaVl;->a:LaVp;

    iget-object v2, v2, LaVp;->a:Landroid/content/Context;

    invoke-static {v2}, LaPS;->a(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Lcom/google/android/gms/internal/aq;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/aq;->a:Z

    if-nez v0, :cond_2

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaVn;

    iget-object v2, p0, LaVl;->a:LaVp;

    iget-object v2, v2, LaVp;->a:Lcom/google/android/gms/internal/aq;

    const-string v3, "Missing AdActivity with android:configChanges in AndroidManifest.xml."

    const-string v4, "Missing AdActivity with android:configChanges in AndroidManifest.xml. You must have the following declaration within the <application> element: <activity android:name=\"com.google.android.gms.ads.AdActivity\" android:configChanges=\"keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize|smallestScreenSize\" />"

    invoke-static {v0, v2, v3, v4}, LaQb;->a(Landroid/view/ViewGroup;Lcom/google/android/gms/internal/aq;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move v0, v1

    :cond_3
    if-nez v0, :cond_4

    iget-object v2, p0, LaVl;->a:LaVp;

    iget-object v2, v2, LaVp;->a:Lcom/google/android/gms/internal/aq;

    iget-boolean v2, v2, Lcom/google/android/gms/internal/aq;->a:Z

    if-nez v2, :cond_4

    iget-object v2, p0, LaVl;->a:LaVp;

    iget-object v2, v2, LaVp;->a:LaVn;

    invoke-virtual {v2, v1}, LaVn;->setVisibility(I)V

    :cond_4
    return v0
.end method

.method private p()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    iget-object v0, p0, LaVl;->a:LaVp;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Landroid/content/Context;

    iget-object v1, p0, LaVl;->a:Landroid/content/ComponentCallbacks;

    invoke-virtual {v0, v1}, Landroid/content/Context;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    :cond_0
    return-void
.end method

.method private q()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    iget-object v0, p0, LaVl;->a:LaVp;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Landroid/content/Context;

    iget-object v1, p0, LaVl;->a:Landroid/content/ComponentCallbacks;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    :cond_0
    return-void
.end method

.method private r()V
    .locals 2

    const-string v0, "Ad closing."

    invoke-static {v0}, LaQc;->c(Ljava/lang/String;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaMR;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaMR;

    invoke-interface {v0}, LaMR;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Could not call AdListener.onAdClosed()."

    invoke-static {v1, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private s()V
    .locals 2

    const-string v0, "Ad leaving application."

    invoke-static {v0}, LaQc;->c(Ljava/lang/String;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaMR;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaMR;

    invoke-interface {v0}, LaMR;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Could not call AdListener.onAdLeftApplication()."

    invoke-static {v1, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private t()V
    .locals 2

    const-string v0, "Ad opening."

    invoke-static {v0}, LaQc;->c(Ljava/lang/String;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaMR;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaMR;

    invoke-interface {v0}, LaMR;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Could not call AdListener.onAdOpened()."

    invoke-static {v1, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private u()V
    .locals 2

    const-string v0, "Ad finished loading."

    invoke-static {v0}, LaQc;->c(Ljava/lang/String;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaMR;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaMR;

    invoke-interface {v0}, LaMR;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Could not call AdListener.onAdLoaded()."

    invoke-static {v1, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private v()V
    .locals 6

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    if-nez v0, :cond_1

    const-string v0, "Ad state was null when trying to ping click URLs."

    invoke-static {v0}, LaQc;->e(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "Pinging click URLs."

    invoke-static {v0}, LaQc;->a(Ljava/lang/String;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPG;

    invoke-virtual {v0}, LaPG;->b()V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Landroid/content/Context;

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:Lcom/google/android/gms/internal/fa;

    iget-object v1, v1, Lcom/google/android/gms/internal/fa;->a:Ljava/lang/String;

    iget-object v2, p0, LaVl;->a:LaVp;

    iget-object v2, v2, LaVp;->a:LaPF;

    iget-object v2, v2, LaPF;->a:Ljava/util/List;

    invoke-static {v0, v1, v2}, LaPS;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    :cond_2
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:LaNF;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:LaNF;

    iget-object v0, v0, LaNF;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Landroid/content/Context;

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:Lcom/google/android/gms/internal/fa;

    iget-object v1, v1, Lcom/google/android/gms/internal/fa;->a:Ljava/lang/String;

    iget-object v2, p0, LaVl;->a:LaVp;

    iget-object v2, v2, LaVp;->a:LaPF;

    iget-object v3, p0, LaVl;->a:LaVp;

    iget-object v3, v3, LaVp;->a:Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, LaVl;->a:LaVp;

    iget-object v5, v5, LaVp;->a:LaPF;

    iget-object v5, v5, LaPF;->a:LaNF;

    iget-object v5, v5, LaNF;->b:Ljava/util/List;

    invoke-static/range {v0 .. v5}, LaNM;->a(Landroid/content/Context;Ljava/lang/String;LaPF;Ljava/lang/String;ZLjava/util/List;)V

    goto :goto_0
.end method

.method private w()V
    .locals 2

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->destroy()V

    iget-object v0, p0, LaVl;->a:LaVp;

    const/4 v1, 0x0

    iput-object v1, v0, LaVp;->a:LaPF;

    :cond_0
    return-void
.end method


# virtual methods
.method public a()LaLm;
    .locals 1

    const-string v0, "getAdFrame must be called on the main UI thread."

    invoke-static {v0}, LaSc;->a(Ljava/lang/String;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaVn;

    invoke-static {v0}, LaLp;->a(Ljava/lang/Object;)LaLm;

    move-result-object v0

    return-object v0
.end method

.method public a()Lcom/google/android/gms/internal/aq;
    .locals 1

    const-string v0, "getAdSize must be called on the main UI thread."

    invoke-static {v0}, LaSc;->a(Ljava/lang/String;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Lcom/google/android/gms/internal/aq;

    return-object v0
.end method

.method public a()V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "destroy must be called on the main UI thread."

    invoke-static {v0}, LaSc;->a(Ljava/lang/String;)V

    invoke-direct {p0}, LaVl;->q()V

    iget-object v0, p0, LaVl;->a:LaVp;

    iput-object v1, v0, LaVp;->a:LaMR;

    iget-object v0, p0, LaVl;->a:LaVp;

    iput-object v1, v0, LaVp;->a:LaNa;

    iget-object v0, p0, LaVl;->a:LaMt;

    invoke-virtual {v0}, LaMt;->a()V

    iget-object v0, p0, LaVl;->a:LaMy;

    invoke-virtual {v0}, LaMy;->a()V

    invoke-virtual {p0}, LaVl;->e()V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaVn;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaVn;

    invoke-virtual {v0}, LaVn;->removeAllViews()V

    :cond_0
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    if-eqz v0, :cond_1

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:Lcom/google/android/gms/internal/fc;

    if-eqz v0, :cond_1

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->destroy()V

    :cond_1
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    if-eqz v0, :cond_2

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:LaNR;

    if-eqz v0, :cond_2

    :try_start_0
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:LaNR;

    invoke-interface {v0}, LaNR;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "Could not destroy mediation adapter."

    invoke-static {v0}, LaQc;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(LaMR;)V
    .locals 1

    const-string v0, "setAdListener must be called on the main UI thread."

    invoke-static {v0}, LaSc;->a(Ljava/lang/String;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iput-object p1, v0, LaVp;->a:LaMR;

    return-void
.end method

.method public a(LaNa;)V
    .locals 1

    const-string v0, "setAppEventListener must be called on the main UI thread."

    invoke-static {v0}, LaSc;->a(Ljava/lang/String;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iput-object p1, v0, LaVp;->a:LaNa;

    return-void
.end method

.method public a(LaOH;)V
    .locals 1

    const-string v0, "setInAppPurchaseListener must be called on the main UI thread."

    invoke-static {v0}, LaSc;->a(Ljava/lang/String;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iput-object p1, v0, LaVp;->a:LaOH;

    return-void
.end method

.method public a(LaOP;Ljava/lang/String;)V
    .locals 4

    const-string v0, "setPlayStorePurchaseParams must be called on the main UI thread."

    invoke-static {v0}, LaSc;->a(Ljava/lang/String;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    new-instance v1, LaOC;

    invoke-direct {v1, p2}, LaOC;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, LaVp;->a:LaOC;

    iget-object v0, p0, LaVl;->a:LaVp;

    iput-object p1, v0, LaVp;->a:LaOP;

    invoke-static {}, LaPI;->a()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    new-instance v0, LaOt;

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:Landroid/content/Context;

    iget-object v2, p0, LaVl;->a:LaVp;

    iget-object v2, v2, LaVp;->a:LaOP;

    iget-object v3, p0, LaVl;->a:LaVp;

    iget-object v3, v3, LaVp;->a:LaOC;

    invoke-direct {v0, v1, v2, v3}, LaOt;-><init>(Landroid/content/Context;LaOP;LaOC;)V

    invoke-virtual {v0}, LaOt;->d()V

    :cond_0
    return-void
.end method

.method public a(LaPF;)V
    .locals 8

    const-wide/16 v2, 0x0

    const/4 v7, 0x0

    const/4 v5, 0x3

    const/4 v6, -0x2

    const/4 v4, 0x0

    iget-object v0, p0, LaVl;->a:LaVp;

    iput-object v7, v0, LaVp;->a:LaPN;

    iget v0, p1, LaPF;->a:I

    if-eq v0, v6, :cond_0

    iget v0, p1, LaPF;->a:I

    if-eq v0, v5, :cond_0

    iget-object v0, p0, LaVl;->a:LaVp;

    invoke-virtual {v0}, LaVp;->a()Ljava/util/HashSet;

    move-result-object v0

    invoke-static {v0}, LaPI;->a(Ljava/util/HashSet;)V

    :cond_0
    iget v0, p1, LaPF;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p1, LaPF;->a:Lcom/google/android/gms/internal/an;

    iget-object v0, v0, Lcom/google/android/gms/internal/an;->a:Landroid/os/Bundle;

    if-eqz v0, :cond_4

    iget-object v0, p1, LaPF;->a:Lcom/google/android/gms/internal/an;

    iget-object v0, v0, Lcom/google/android/gms/internal/an;->a:Landroid/os/Bundle;

    const-string v1, "_noRefresh"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :goto_1
    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:Lcom/google/android/gms/internal/aq;

    iget-boolean v1, v1, Lcom/google/android/gms/internal/aq;->a:Z

    if-eqz v1, :cond_5

    iget-object v0, p1, LaPF;->a:Lcom/google/android/gms/internal/fc;

    invoke-static {v0}, LaPS;->a(Landroid/webkit/WebView;)V

    :cond_2
    :goto_2
    iget v0, p1, LaPF;->a:I

    if-ne v0, v5, :cond_3

    iget-object v0, p1, LaPF;->a:LaNF;

    if-eqz v0, :cond_3

    iget-object v0, p1, LaPF;->a:LaNF;

    iget-object v0, v0, LaNF;->d:Ljava/util/List;

    if-eqz v0, :cond_3

    const-string v0, "Pinging no fill URLs."

    invoke-static {v0}, LaQc;->a(Ljava/lang/String;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Landroid/content/Context;

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:Lcom/google/android/gms/internal/fa;

    iget-object v1, v1, Lcom/google/android/gms/internal/fa;->a:Ljava/lang/String;

    iget-object v2, p0, LaVl;->a:LaVp;

    iget-object v3, v2, LaVp;->a:Ljava/lang/String;

    iget-object v2, p1, LaPF;->a:LaNF;

    iget-object v5, v2, LaNF;->d:Ljava/util/List;

    move-object v2, p1

    invoke-static/range {v0 .. v5}, LaNM;->a(Landroid/content/Context;Ljava/lang/String;LaPF;Ljava/lang/String;ZLjava/util/List;)V

    :cond_3
    iget v0, p1, LaPF;->a:I

    if-eq v0, v6, :cond_8

    iget v0, p1, LaPF;->a:I

    invoke-direct {p0, v0}, LaVl;->a(I)V

    goto :goto_0

    :cond_4
    move v0, v4

    goto :goto_1

    :cond_5
    if-nez v0, :cond_2

    iget-wide v0, p1, LaPF;->a:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_6

    iget-object v0, p0, LaVl;->a:LaMt;

    iget-object v1, p1, LaPF;->a:Lcom/google/android/gms/internal/an;

    iget-wide v2, p1, LaPF;->a:J

    invoke-virtual {v0, v1, v2, v3}, LaMt;->a(Lcom/google/android/gms/internal/an;J)V

    goto :goto_2

    :cond_6
    iget-object v0, p1, LaPF;->a:LaNF;

    if-eqz v0, :cond_7

    iget-object v0, p1, LaPF;->a:LaNF;

    iget-wide v0, v0, LaNF;->b:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_7

    iget-object v0, p0, LaVl;->a:LaMt;

    iget-object v1, p1, LaPF;->a:Lcom/google/android/gms/internal/an;

    iget-object v2, p1, LaPF;->a:LaNF;

    iget-wide v2, v2, LaNF;->b:J

    invoke-virtual {v0, v1, v2, v3}, LaMt;->a(Lcom/google/android/gms/internal/an;J)V

    goto :goto_2

    :cond_7
    iget-boolean v0, p1, LaPF;->a:Z

    if-nez v0, :cond_2

    iget v0, p1, LaPF;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    iget-object v0, p0, LaVl;->a:LaMt;

    iget-object v1, p1, LaPF;->a:Lcom/google/android/gms/internal/an;

    invoke-virtual {v0, v1}, LaMt;->a(Lcom/google/android/gms/internal/an;)V

    goto :goto_2

    :cond_8
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Lcom/google/android/gms/internal/aq;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/aq;->a:Z

    if-nez v0, :cond_a

    invoke-direct {p0, p1}, LaVl;->a(LaPF;)Z

    move-result v0

    if-nez v0, :cond_9

    invoke-direct {p0, v4}, LaVl;->a(I)V

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaVn;

    if-eqz v0, :cond_a

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaVn;

    invoke-static {v0}, LaVn;->a(LaVn;)LaPV;

    move-result-object v0

    iget-object v1, p1, LaPF;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, LaPV;->a(Ljava/lang/String;)V

    :cond_a
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    if-eqz v0, :cond_b

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:LaNH;

    if-eqz v0, :cond_b

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:LaNH;

    invoke-virtual {v0, v7}, LaNH;->a(LaNG;)V

    :cond_b
    iget-object v0, p1, LaPF;->a:LaNH;

    if-eqz v0, :cond_c

    iget-object v0, p1, LaPF;->a:LaNH;

    invoke-virtual {v0, p0}, LaNH;->a(LaNG;)V

    :cond_c
    iget-object v0, p0, LaVl;->a:LaMy;

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:LaPF;

    invoke-virtual {v0, v1}, LaMy;->a(LaPF;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iput-object p1, v0, LaVp;->a:LaPF;

    iget-object v0, p1, LaPF;->a:Lcom/google/android/gms/internal/aq;

    if-eqz v0, :cond_d

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v1, p1, LaPF;->a:Lcom/google/android/gms/internal/aq;

    iput-object v1, v0, LaVp;->a:Lcom/google/android/gms/internal/aq;

    :cond_d
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPG;

    iget-wide v2, p1, LaPF;->d:J

    invoke-virtual {v0, v2, v3}, LaPG;->a(J)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPG;

    iget-wide v2, p1, LaPF;->e:J

    invoke-virtual {v0, v2, v3}, LaPG;->b(J)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPG;

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:Lcom/google/android/gms/internal/aq;

    iget-boolean v1, v1, Lcom/google/android/gms/internal/aq;->a:Z

    invoke-virtual {v0, v1}, LaPG;->a(Z)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPG;

    iget-boolean v1, p1, LaPF;->a:Z

    invoke-virtual {v0, v1}, LaPG;->b(Z)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Lcom/google/android/gms/internal/aq;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/aq;->a:Z

    if-nez v0, :cond_e

    invoke-direct {p0, v4}, LaVl;->b(Z)V

    :cond_e
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPL;

    if-nez v0, :cond_f

    iget-object v0, p0, LaVl;->a:LaVp;

    new-instance v1, LaPL;

    iget-object v2, p0, LaVl;->a:LaVp;

    iget-object v2, v2, LaVp;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, LaPL;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, LaVp;->a:LaPL;

    :cond_f
    iget-object v0, p1, LaPF;->a:LaNF;

    if-eqz v0, :cond_12

    iget-object v0, p1, LaPF;->a:LaNF;

    iget v0, v0, LaNF;->a:I

    iget-object v1, p1, LaPF;->a:LaNF;

    iget v4, v1, LaNF;->b:I

    :goto_3
    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:LaPL;

    invoke-virtual {v1, v0, v4}, LaPL;->a(II)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Lcom/google/android/gms/internal/aq;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/aq;->a:Z

    if-nez v0, :cond_11

    iget-object v0, p1, LaPF;->a:Lcom/google/android/gms/internal/fc;

    if-eqz v0, :cond_11

    iget-object v0, p1, LaPF;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->a()LaQu;

    move-result-object v0

    invoke-virtual {v0}, LaQu;->a()Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p1, LaPF;->a:Lorg/json/JSONObject;

    if-eqz v0, :cond_11

    :cond_10
    iget-object v0, p0, LaVl;->a:LaMy;

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:Lcom/google/android/gms/internal/aq;

    iget-object v2, p0, LaVl;->a:LaVp;

    iget-object v2, v2, LaVp;->a:LaPF;

    invoke-virtual {v0, v1, v2}, LaMy;->a(Lcom/google/android/gms/internal/aq;LaPF;)LaMz;

    move-result-object v0

    iget-object v1, p1, LaPF;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/fc;->a()LaQu;

    move-result-object v1

    invoke-virtual {v1}, LaQu;->a()Z

    move-result v1

    if-eqz v1, :cond_11

    if-eqz v0, :cond_11

    new-instance v1, LaMs;

    iget-object v2, p1, LaPF;->a:Lcom/google/android/gms/internal/fc;

    invoke-direct {v1, v2}, LaMs;-><init>(Lcom/google/android/gms/internal/fc;)V

    invoke-virtual {v0, v1}, LaMz;->a(LaMw;)V

    :cond_11
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->a()V

    invoke-direct {p0}, LaVl;->u()V

    goto/16 :goto_0

    :cond_12
    move v0, v4

    goto :goto_3
.end method

.method public a(Lcom/google/android/gms/internal/an;)V
    .locals 2

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaVn;

    invoke-virtual {v0}, LaVn;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, LaPS;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LaVl;->a:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, LaVl;->a(Lcom/google/android/gms/internal/an;)Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "Ad is not visible. Not refreshing ad."

    invoke-static {v0}, LaQc;->c(Ljava/lang/String;)V

    iget-object v0, p0, LaVl;->a:LaMt;

    invoke-virtual {v0, p1}, LaMt;->a(Lcom/google/android/gms/internal/an;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/internal/aq;)V
    .locals 2

    const-string v0, "setAdSize must be called on the main UI thread."

    invoke-static {v0}, LaSc;->a(Ljava/lang/String;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iput-object p1, v0, LaVp;->a:Lcom/google/android/gms/internal/aq;

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/fc;->a(Lcom/google/android/gms/internal/aq;)V

    :cond_0
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaVn;

    invoke-virtual {v0}, LaVn;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaVn;

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:LaVn;

    invoke-virtual {v1}, LaVn;->getNextView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, LaVn;->removeView(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaVn;

    iget v1, p1, Lcom/google/android/gms/internal/aq;->e:I

    invoke-virtual {v0, v1}, LaVn;->setMinimumWidth(I)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaVn;

    iget v1, p1, Lcom/google/android/gms/internal/aq;->c:I

    invoke-virtual {v0, v1}, LaVn;->setMinimumHeight(I)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaVn;

    invoke-virtual {v0}, LaVn;->requestLayout()V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaNa;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaNa;

    invoke-interface {v0, p1, p2}, LaNa;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Could not call the AppEventListener."

    invoke-static {v1, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    new-instance v0, LaOv;

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:Landroid/content/Context;

    iget-object v2, p0, LaVl;->a:LaVp;

    iget-object v2, v2, LaVp;->a:Lcom/google/android/gms/internal/fa;

    iget-object v2, v2, Lcom/google/android/gms/internal/fa;->a:Ljava/lang/String;

    invoke-direct {v0, p1, p2, v1, v2}, LaOv;-><init>(Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Context;Ljava/lang/String;)V

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:LaOH;

    if-nez v1, :cond_4

    const-string v1, "InAppPurchaseListener is not set. Try to launch default purchase flow."

    invoke-static {v1}, LaQc;->e(Ljava/lang/String;)V

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:Landroid/content/Context;

    invoke-static {v1}, LaCJ;->a(Landroid/content/Context;)I

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "Google Play Service unavailable, cannot launch default purchase flow."

    invoke-static {v0}, LaQc;->e(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:LaOP;

    if-nez v1, :cond_2

    const-string v0, "PlayStorePurchaseListener is not set."

    invoke-static {v0}, LaQc;->e(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:LaOC;

    if-nez v1, :cond_3

    const-string v0, "PlayStorePurchaseVerifier is not initialized."

    invoke-static {v0}, LaQc;->e(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    :try_start_0
    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:LaOP;

    invoke-interface {v1, p1}, LaOP;->a(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    :goto_1
    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:Landroid/content/Context;

    iget-object v2, p0, LaVl;->a:LaVp;

    iget-object v2, v2, LaVp;->a:Lcom/google/android/gms/internal/fa;

    iget-boolean v2, v2, Lcom/google/android/gms/internal/fa;->a:Z

    new-instance v3, Lcom/google/android/gms/internal/cv;

    iget-object v4, p0, LaVl;->a:LaVp;

    iget-object v4, v4, LaVp;->a:LaOP;

    iget-object v5, p0, LaVl;->a:LaVp;

    iget-object v5, v5, LaVp;->a:LaOC;

    iget-object v6, p0, LaVl;->a:LaVp;

    iget-object v6, v6, LaVp;->a:Landroid/content/Context;

    invoke-direct {v3, v0, v4, v5, v6}, Lcom/google/android/gms/internal/cv;-><init>(LaOE;LaOP;LaOC;Landroid/content/Context;)V

    invoke-static {v1, v2, v3}, LaOw;->a(Landroid/content/Context;ZLcom/google/android/gms/internal/cv;)V

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v1, "Could not start In-App purchase."

    invoke-static {v1}, LaQc;->e(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    :try_start_1
    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:LaOH;

    invoke-interface {v1, v0}, LaOH;->a(LaOE;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v0, "Could not start In-App purchase."

    invoke-static {v0}, LaQc;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/util/HashSet;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "LaPG;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, LaVl;->a:LaVp;

    invoke-virtual {v0, p1}, LaVp;->a(Ljava/util/HashSet;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, LaVl;->a:LaVp;

    iput-boolean p1, v0, LaVp;->a:Z

    return-void
.end method

.method public a()Z
    .locals 1

    const-string v0, "isLoaded must be called on the main UI thread."

    invoke-static {v0}, LaSc;->a(Ljava/lang/String;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPN;

    if-nez v0, :cond_0

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/internal/an;)Z
    .locals 12

    const/4 v10, 0x1

    const/4 v2, 0x0

    const-string v0, "loadAd must be called on the main UI thread."

    invoke-static {v0}, LaSc;->a(Ljava/lang/String;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPN;

    if-eqz v0, :cond_1

    const-string v0, "An ad request is already in progress. Aborting."

    invoke-static {v0}, LaQc;->e(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Lcom/google/android/gms/internal/aq;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/aq;->a:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    if-eqz v0, :cond_2

    const-string v0, "An interstitial is already loading. Aborting."

    invoke-static {v0}, LaQc;->e(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, LaVl;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Starting ad request."

    invoke-static {v0}, LaQc;->c(Ljava/lang/String;)V

    iget-boolean v0, p1, Lcom/google/android/gms/internal/an;->a:Z

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Use AdRequest.Builder.addTestDevice(\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:Landroid/content/Context;

    invoke-static {v1}, LaQb;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\") to get test ads on this device."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaQc;->c(Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, LaVl;->a:LaMt;

    invoke-virtual {v0}, LaMt;->a()V

    iget-object v0, p0, LaVl;->a:LaVp;

    iput-boolean v2, v0, LaVp;->a:Z

    invoke-direct {p0, p1}, LaVl;->a(Lcom/google/android/gms/internal/an;)LaPm;

    move-result-object v11

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Lcom/google/android/gms/internal/aq;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/aq;->a:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Landroid/content/Context;

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:Lcom/google/android/gms/internal/aq;

    iget-object v3, p0, LaVl;->a:LaVp;

    iget-object v4, v3, LaVp;->a:LaSP;

    iget-object v3, p0, LaVl;->a:LaVp;

    iget-object v5, v3, LaVp;->a:Lcom/google/android/gms/internal/fa;

    move v3, v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/internal/fc;->a(Landroid/content/Context;Lcom/google/android/gms/internal/aq;ZZLaSP;Lcom/google/android/gms/internal/fa;)Lcom/google/android/gms/internal/fc;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/gms/internal/fc;->a()LaQu;

    move-result-object v0

    const/4 v2, 0x0

    move-object v1, p0

    move-object v3, p0

    move-object v4, p0

    move v5, v10

    move-object v6, p0

    move-object v7, p0

    invoke-virtual/range {v0 .. v7}, LaQu;->a(LaVk;LaOl;LaNk;LaOo;ZLaNv;LaNx;)V

    move-object v3, v8

    :goto_1
    iget-object v6, p0, LaVl;->a:LaVp;

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Landroid/content/Context;

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v2, v1, LaVp;->a:LaSP;

    iget-object v4, p0, LaVl;->a:LaNO;

    move-object v1, v11

    move-object v5, p0

    invoke-static/range {v0 .. v5}, LaOW;->a(Landroid/content/Context;LaPm;LaSP;Lcom/google/android/gms/internal/fc;LaNO;LaOX;)LaPN;

    move-result-object v0

    iput-object v0, v6, LaVp;->a:LaPN;

    move v2, v10

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaVn;

    invoke-virtual {v0}, LaVn;->getNextView()Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/gms/internal/fc;

    if-eqz v1, :cond_6

    check-cast v0, Lcom/google/android/gms/internal/fc;

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:Landroid/content/Context;

    iget-object v3, p0, LaVl;->a:LaVp;

    iget-object v3, v3, LaVp;->a:Lcom/google/android/gms/internal/aq;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/internal/fc;->a(Landroid/content/Context;Lcom/google/android/gms/internal/aq;)V

    :cond_5
    :goto_2
    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->a()LaQu;

    move-result-object v3

    move-object v4, p0

    move-object v5, p0

    move-object v6, p0

    move-object v7, p0

    move v8, v2

    move-object v9, p0

    invoke-virtual/range {v3 .. v9}, LaQu;->a(LaVk;LaOl;LaNk;LaOo;ZLaNv;)V

    move-object v3, v0

    goto :goto_1

    :cond_6
    if-eqz v0, :cond_7

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:LaVn;

    invoke-virtual {v1, v0}, LaVn;->removeView(Landroid/view/View;)V

    :cond_7
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Landroid/content/Context;

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:Lcom/google/android/gms/internal/aq;

    iget-object v3, p0, LaVl;->a:LaVp;

    iget-object v4, v3, LaVp;->a:LaSP;

    iget-object v3, p0, LaVl;->a:LaVp;

    iget-object v5, v3, LaVp;->a:Lcom/google/android/gms/internal/fa;

    move v3, v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/internal/fc;->a(Landroid/content/Context;Lcom/google/android/gms/internal/aq;ZZLaSP;Lcom/google/android/gms/internal/fa;)Lcom/google/android/gms/internal/fc;

    move-result-object v0

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:Lcom/google/android/gms/internal/aq;

    iget-object v1, v1, Lcom/google/android/gms/internal/aq;->a:[Lcom/google/android/gms/internal/aq;

    if-nez v1, :cond_5

    invoke-direct {p0, v0}, LaVl;->a(Landroid/view/View;)V

    goto :goto_2
.end method

.method public b()V
    .locals 1

    const-string v0, "pause must be called on the main UI thread."

    invoke-static {v0}, LaSc;->a(Ljava/lang/String;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:Lcom/google/android/gms/internal/fc;

    invoke-static {v0}, LaPS;->a(Landroid/webkit/WebView;)V

    :cond_0
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    if-eqz v0, :cond_1

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:LaNR;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:LaNR;

    invoke-interface {v0}, LaNR;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    iget-object v0, p0, LaVl;->a:LaMy;

    invoke-virtual {v0}, LaMy;->b()V

    iget-object v0, p0, LaVl;->a:LaMt;

    invoke-virtual {v0}, LaMt;->b()V

    return-void

    :catch_0
    move-exception v0

    const-string v0, "Could not pause mediation adapter."

    invoke-static {v0}, LaQc;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public c()V
    .locals 1

    const-string v0, "resume must be called on the main UI thread."

    invoke-static {v0}, LaSc;->a(Ljava/lang/String;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:Lcom/google/android/gms/internal/fc;

    invoke-static {v0}, LaPS;->b(Landroid/webkit/WebView;)V

    :cond_0
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    if-eqz v0, :cond_1

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:LaNR;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:LaNR;

    invoke-interface {v0}, LaNR;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    iget-object v0, p0, LaVl;->a:LaMt;

    invoke-virtual {v0}, LaMt;->c()V

    iget-object v0, p0, LaVl;->a:LaMy;

    invoke-virtual {v0}, LaMy;->c()V

    return-void

    :catch_0
    move-exception v0

    const-string v0, "Could not resume mediation adapter."

    invoke-static {v0}, LaQc;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public d()V
    .locals 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "showInterstitial must be called on the main UI thread."

    invoke-static {v0}, LaSc;->a(Ljava/lang/String;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Lcom/google/android/gms/internal/aq;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/aq;->a:Z

    if-nez v0, :cond_0

    const-string v0, "Cannot call showInterstitial on a banner ad."

    invoke-static {v0}, LaQc;->e(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    if-nez v0, :cond_1

    const-string v0, "The interstitial has not loaded."

    invoke-static {v0}, LaQc;->e(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "The interstitial is already showing."

    invoke-static {v0}, LaQc;->e(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/fc;->a(Z)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->a()LaQu;

    move-result-object v0

    invoke-virtual {v0}, LaQu;->a()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:Lorg/json/JSONObject;

    if-eqz v0, :cond_4

    :cond_3
    iget-object v0, p0, LaVl;->a:LaMy;

    iget-object v3, p0, LaVl;->a:LaVp;

    iget-object v3, v3, LaVp;->a:Lcom/google/android/gms/internal/aq;

    iget-object v4, p0, LaVl;->a:LaVp;

    iget-object v4, v4, LaVp;->a:LaPF;

    invoke-virtual {v0, v3, v4}, LaMy;->a(Lcom/google/android/gms/internal/aq;LaPF;)LaMz;

    move-result-object v0

    iget-object v3, p0, LaVl;->a:LaVp;

    iget-object v3, v3, LaVp;->a:LaPF;

    iget-object v3, v3, LaPF;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v3}, Lcom/google/android/gms/internal/fc;->a()LaQu;

    move-result-object v3

    invoke-virtual {v3}, LaQu;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    if-eqz v0, :cond_4

    new-instance v3, LaMs;

    iget-object v4, p0, LaVl;->a:LaVp;

    iget-object v4, v4, LaVp;->a:LaPF;

    iget-object v4, v4, LaPF;->a:Lcom/google/android/gms/internal/fc;

    invoke-direct {v3, v4}, LaMs;-><init>(Lcom/google/android/gms/internal/fc;)V

    invoke-virtual {v0, v3}, LaMz;->a(LaMw;)V

    :cond_4
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-boolean v0, v0, LaPF;->a:Z

    if-eqz v0, :cond_5

    :try_start_0
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:LaNR;

    invoke-interface {v0}, LaNR;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Could not show interstitial."

    invoke-static {v1, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, LaVl;->w()V

    goto/16 :goto_0

    :cond_5
    new-instance v8, Lcom/google/android/gms/internal/aa;

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-boolean v0, v0, LaVp;->a:Z

    invoke-direct {v8, v0, v2}, Lcom/google/android/gms/internal/aa;-><init>(ZZ)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_6

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    iget v0, v3, Landroid/graphics/Rect;->bottom:I

    if-eqz v0, :cond_6

    iget v0, v4, Landroid/graphics/Rect;->bottom:I

    if-eqz v0, :cond_6

    new-instance v8, Lcom/google/android/gms/internal/aa;

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-boolean v5, v0, LaVp;->a:Z

    iget v0, v3, Landroid/graphics/Rect;->top:I

    iget v3, v4, Landroid/graphics/Rect;->top:I

    if-ne v0, v3, :cond_7

    move v0, v1

    :goto_1
    invoke-direct {v8, v5, v0}, Lcom/google/android/gms/internal/aa;-><init>(ZZ)V

    :cond_6
    new-instance v0, Lcom/google/android/gms/internal/cm;

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:LaPF;

    iget-object v4, v1, LaPF;->a:Lcom/google/android/gms/internal/fc;

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:LaPF;

    iget v5, v1, LaPF;->b:I

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v6, v1, LaVp;->a:Lcom/google/android/gms/internal/fa;

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:LaPF;

    iget-object v7, v1, LaPF;->c:Ljava/lang/String;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/internal/cm;-><init>(LaVk;LaOl;LaOo;Lcom/google/android/gms/internal/fc;ILcom/google/android/gms/internal/fa;Ljava/lang/String;Lcom/google/android/gms/internal/aa;)V

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:Landroid/content/Context;

    invoke-static {v1, v0}, LaOf;->a(Landroid/content/Context;Lcom/google/android/gms/internal/cm;)V

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto :goto_1
.end method

.method public e()V
    .locals 2

    const-string v0, "stopLoading must be called on the main UI thread."

    invoke-static {v0}, LaSc;->a(Ljava/lang/String;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->stopLoading()V

    iget-object v0, p0, LaVl;->a:LaVp;

    const/4 v1, 0x0

    iput-object v1, v0, LaVp;->a:LaPF;

    :cond_0
    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPN;

    if-eqz v0, :cond_1

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPN;

    invoke-virtual {v0}, LaPN;->e()V

    :cond_1
    return-void
.end method

.method public f()V
    .locals 3

    const-string v0, "recordManualImpression must be called on the main UI thread."

    invoke-static {v0}, LaSc;->a(Ljava/lang/String;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    if-nez v0, :cond_1

    const-string v0, "Ad state was null when trying to ping manual tracking URLs."

    invoke-static {v0}, LaQc;->e(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "Pinging manual tracking URLs."

    invoke-static {v0}, LaQc;->a(Ljava/lang/String;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    iget-object v0, v0, LaPF;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Landroid/content/Context;

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:Lcom/google/android/gms/internal/fa;

    iget-object v1, v1, Lcom/google/android/gms/internal/fa;->a:Ljava/lang/String;

    iget-object v2, p0, LaVl;->a:LaVp;

    iget-object v2, v2, LaVp;->a:LaPF;

    iget-object v2, v2, LaPF;->c:Ljava/util/List;

    invoke-static {v0, v1, v2}, LaPS;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0
.end method

.method public g()V
    .locals 0

    invoke-virtual {p0}, LaVl;->o()V

    return-void
.end method

.method public h()V
    .locals 0

    invoke-virtual {p0}, LaVl;->l()V

    return-void
.end method

.method public i()V
    .locals 0

    invoke-virtual {p0}, LaVl;->n()V

    return-void
.end method

.method public j()V
    .locals 0

    invoke-virtual {p0}, LaVl;->m()V

    return-void
.end method

.method public k()V
    .locals 2

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPF;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Mediation adapter "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:LaPF;

    iget-object v1, v1, LaPF;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " refreshed, but mediation adapters should never refresh."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaQc;->e(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LaVl;->b(Z)V

    invoke-direct {p0}, LaVl;->u()V

    return-void
.end method

.method public l()V
    .locals 2

    iget-object v0, p0, LaVl;->a:LaMy;

    iget-object v1, p0, LaVl;->a:LaVp;

    iget-object v1, v1, LaVp;->a:LaPF;

    invoke-virtual {v0, v1}, LaMy;->a(LaPF;)V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Lcom/google/android/gms/internal/aq;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/aq;->a:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, LaVl;->w()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LaVl;->a:Z

    invoke-direct {p0}, LaVl;->r()V

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:LaPG;

    invoke-virtual {v0}, LaPG;->c()V

    return-void
.end method

.method public m()V
    .locals 1

    iget-object v0, p0, LaVl;->a:LaVp;

    iget-object v0, v0, LaVp;->a:Lcom/google/android/gms/internal/aq;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/aq;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, LaVl;->b(Z)V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LaVl;->a:Z

    invoke-direct {p0}, LaVl;->t()V

    return-void
.end method

.method public n()V
    .locals 0

    invoke-direct {p0}, LaVl;->s()V

    return-void
.end method

.method public o()V
    .locals 0

    invoke-direct {p0}, LaVl;->v()V

    return-void
.end method
