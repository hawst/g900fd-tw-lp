.class public LrV;
.super Lbuo;
.source "ContextScopedProviderPackageModule.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lbuo;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 0

    .prologue
    .line 57
    return-void
.end method

.method public get1(Lbxw;LaiU;LaiW;)Laja;
    .locals 1
    .param p1    # Lbxw;
        .annotation runtime LQM;
        .end annotation
    .end param
    .annotation runtime LQM;
    .end annotation

    .annotation runtime LbuF;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbxw",
            "<",
            "LsF;",
            ">;",
            "LaiU;",
            "LaiW;",
            ")",
            "Laja",
            "<",
            "LsF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18
    new-instance v0, Laja;

    invoke-direct {v0, p1, p2, p3}, Laja;-><init>(Lbxw;LaiU;LaiW;)V

    return-object v0
.end method

.method public get2(Lbxw;LaiU;LaiW;)Laja;
    .locals 1
    .param p1    # Lbxw;
        .annotation runtime Lbwm;
            value = "scanIntentProvider"
        .end annotation
    .end param
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbwm;
        value = "scanIntentProvider"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbxw",
            "<",
            "Lub;",
            ">;",
            "LaiU;",
            "LaiW;",
            ")",
            "Laja",
            "<",
            "Lub;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    new-instance v0, Laja;

    invoke-direct {v0, p1, p2, p3}, Laja;-><init>(Lbxw;LaiU;LaiW;)V

    return-object v0
.end method

.method public get3(Lbxw;LaiU;LaiW;)Laja;
    .locals 1
    .param p1    # Lbxw;
        .annotation runtime LQJ;
        .end annotation
    .end param
    .annotation runtime LQJ;
    .end annotation

    .annotation runtime LbuF;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbxw",
            "<",
            "LsI;",
            ">;",
            "LaiU;",
            "LaiW;",
            ")",
            "Laja",
            "<",
            "LsI;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    new-instance v0, Laja;

    invoke-direct {v0, p1, p2, p3}, Laja;-><init>(Lbxw;LaiU;LaiW;)V

    return-object v0
.end method

.method public getLazy1(Laja;)Lajw;
    .locals 1
    .param p1    # Laja;
        .annotation runtime LQM;
        .end annotation
    .end param
    .annotation runtime LQM;
    .end annotation

    .annotation runtime LbuF;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "LsF;",
            ">;)",
            "Lajw",
            "<",
            "LsF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    new-instance v0, Lajw;

    invoke-direct {v0, p1}, Lajw;-><init>(Laja;)V

    return-object v0
.end method

.method public getLazy2(Laja;)Lajw;
    .locals 1
    .param p1    # Laja;
        .annotation runtime Lbwm;
            value = "scanIntentProvider"
        .end annotation
    .end param
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbwm;
        value = "scanIntentProvider"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "Lub;",
            ">;)",
            "Lajw",
            "<",
            "Lub;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    new-instance v0, Lajw;

    invoke-direct {v0, p1}, Lajw;-><init>(Laja;)V

    return-object v0
.end method

.method public getLazy3(Laja;)Lajw;
    .locals 1
    .param p1    # Laja;
        .annotation runtime LQJ;
        .end annotation
    .end param
    .annotation runtime LQJ;
    .end annotation

    .annotation runtime LbuF;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "LsI;",
            ">;)",
            "Lajw",
            "<",
            "LsI;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    new-instance v0, Lajw;

    invoke-direct {v0, p1}, Lajw;-><init>(Laja;)V

    return-object v0
.end method
