.class LbnD;
.super Ljava/lang/Object;
.source "LinkedHashMultimap.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TV;>;"
    }
.end annotation


# instance fields
.field a:I

.field a:LbnB;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbnB",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final synthetic a:LbnC;

.field a:LbnE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbnE",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LbnC;)V
    .locals 1

    .prologue
    .line 360
    iput-object p1, p0, LbnD;->a:LbnC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 361
    iget-object v0, p0, LbnD;->a:LbnC;

    invoke-static {v0}, LbnC;->a(LbnC;)LbnE;

    move-result-object v0

    iput-object v0, p0, LbnD;->a:LbnE;

    .line 363
    iget-object v0, p0, LbnD;->a:LbnC;

    invoke-static {v0}, LbnC;->a(LbnC;)I

    move-result v0

    iput v0, p0, LbnD;->a:I

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 366
    iget-object v0, p0, LbnD;->a:LbnC;

    invoke-static {v0}, LbnC;->a(LbnC;)I

    move-result v0

    iget v1, p0, LbnD;->a:I

    if-eq v0, v1, :cond_0

    .line 367
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 369
    :cond_0
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .prologue
    .line 372
    invoke-direct {p0}, LbnD;->a()V

    .line 373
    iget-object v0, p0, LbnD;->a:LbnE;

    iget-object v1, p0, LbnD;->a:LbnC;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 377
    invoke-virtual {p0}, LbnD;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 378
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 380
    :cond_0
    iget-object v0, p0, LbnD;->a:LbnE;

    check-cast v0, LbnB;

    .line 381
    invoke-virtual {v0}, LbnB;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 382
    iput-object v0, p0, LbnD;->a:LbnB;

    .line 383
    invoke-virtual {v0}, LbnB;->b()LbnE;

    move-result-object v0

    iput-object v0, p0, LbnD;->a:LbnE;

    .line 384
    return-object v1
.end method

.method public remove()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 388
    invoke-direct {p0}, LbnD;->a()V

    .line 389
    iget-object v0, p0, LbnD;->a:LbnB;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbnr;->a(Z)V

    .line 390
    iget-object v0, p0, LbnD;->a:LbnB;

    invoke-virtual {v0}, LbnB;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 391
    if-nez v0, :cond_2

    .line 392
    :goto_1
    invoke-static {v1}, Lbms;->a(I)I

    move-result v0

    iget-object v1, p0, LbnD;->a:LbnC;

    iget-object v1, v1, LbnC;->a:[LbnB;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    and-int v3, v0, v1

    .line 394
    iget-object v0, p0, LbnD;->a:LbnC;

    iget-object v0, v0, LbnC;->a:[LbnB;

    aget-object v0, v0, v3

    move-object v1, v2

    :goto_2
    if-eqz v0, :cond_0

    .line 395
    iget-object v4, p0, LbnD;->a:LbnB;

    if-ne v0, v4, :cond_4

    .line 396
    if-nez v1, :cond_3

    .line 398
    iget-object v1, p0, LbnD;->a:LbnC;

    iget-object v1, v1, LbnC;->a:[LbnB;

    iget-object v0, v0, LbnB;->a:LbnB;

    aput-object v0, v1, v3

    .line 402
    :goto_3
    iget-object v0, p0, LbnD;->a:LbnB;

    invoke-static {v0}, LbnA;->a(LbnE;)V

    .line 403
    iget-object v0, p0, LbnD;->a:LbnB;

    invoke-static {v0}, LbnA;->a(LbnB;)V

    .line 404
    iget-object v0, p0, LbnD;->a:LbnC;

    invoke-static {v0}, LbnC;->b(LbnC;)I

    .line 405
    iget-object v0, p0, LbnD;->a:LbnC;

    invoke-static {v0}, LbnC;->c(LbnC;)I

    move-result v0

    iput v0, p0, LbnD;->a:I

    .line 409
    :cond_0
    iput-object v2, p0, LbnD;->a:LbnB;

    .line 410
    return-void

    :cond_1
    move v0, v1

    .line 389
    goto :goto_0

    .line 391
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    .line 400
    :cond_3
    iget-object v0, v0, LbnB;->a:LbnB;

    iput-object v0, v1, LbnB;->a:LbnB;

    goto :goto_3

    .line 394
    :cond_4
    iget-object v1, v0, LbnB;->a:LbnB;

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_2
.end method
