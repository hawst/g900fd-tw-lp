.class public LamO;
.super Ljava/lang/Object;
.source "StringPartitionTree.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field final a:LamR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LamR",
            "<TT;>;"
        }
    .end annotation
.end field

.field a:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method private constructor <init>(LamR;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LamR",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, LamO;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 164
    iput-object p1, p0, LamO;->a:LamR;

    .line 165
    return-void
.end method

.method static a(I)I
    .locals 1

    .prologue
    .line 522
    mul-int/lit8 v0, p0, 0x2

    rsub-int/lit8 v0, v0, 0x1

    return v0
.end method

.method static a(II)I
    .locals 1

    .prologue
    .line 534
    mul-int/lit8 v0, p0, 0x2

    add-int/2addr v0, p1

    return v0
.end method

.method static synthetic a(ILamQ;LamQ;)I
    .locals 1

    .prologue
    .line 44
    invoke-static {p0, p1, p2}, LamO;->b(ILamQ;LamQ;)I

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 334
    if-nez p0, :cond_0

    .line 336
    const/4 v0, 0x1

    .line 340
    :goto_0
    return v0

    .line 337
    :cond_0
    invoke-virtual {p0, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 338
    const/4 v0, 0x0

    goto :goto_0

    .line 340
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)LamO;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<",
            "LamQ",
            "<TU;>;>;)",
            "LamO",
            "<TU;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 152
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    invoke-static {}, LamO;->a()[Ljava/lang/String;

    move-result-object v1

    .line 155
    const/4 v0, 0x0

    .line 156
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 157
    const/4 v0, 0x2

    new-array v5, v0, [Z

    .line 158
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v3, v0, -0x1

    move-object v0, p0

    move v4, v2

    invoke-static/range {v0 .. v5}, LamO;->a(Ljava/util/List;[Ljava/lang/String;III[Z)LamR;

    move-result-object v0

    .line 160
    :cond_0
    new-instance v1, LamO;

    invoke-direct {v1, v0}, LamO;-><init>(LamR;)V

    return-object v1
.end method

.method private static a(Ljava/util/List;[Ljava/lang/String;III[Z)LamR;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<",
            "LamQ",
            "<TU;>;>;[",
            "Ljava/lang/String;",
            "III[Z)",
            "LamR",
            "<TU;>;"
        }
    .end annotation

    .prologue
    .line 360
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    if-gt p2, p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 362
    array-length v0, p5

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LbiT;->a(Z)V

    .line 364
    invoke-static {}, LpD;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 365
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 361
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 362
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 378
    :cond_2
    add-int/lit8 v0, p4, 0x1

    rem-int/lit8 v4, v0, 0x2

    .line 383
    aget-boolean v0, p5, p4

    if-eqz v0, :cond_3

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p5

    .line 384
    invoke-static/range {v0 .. v5}, LamO;->a(Ljava/util/List;[Ljava/lang/String;III[Z)LamR;

    move-result-object v0

    .line 451
    :goto_2
    return-object v0

    .line 389
    :cond_3
    sub-int v0, p3, p2

    const/4 v1, 0x3

    if-ge v0, v1, :cond_4

    .line 392
    new-instance v0, LamR;

    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    add-int/lit8 v4, p3, 0x1

    invoke-interface {p0, p2, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LamR;-><init>(ILjava/lang/String;Ljava/util/List;Ljava/util/List;LamP;)V

    goto :goto_2

    .line 398
    :cond_4
    add-int/lit8 v0, p3, 0x1

    .line 399
    invoke-interface {p0, p2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    new-instance v1, LamP;

    invoke-direct {v1, p4}, LamP;-><init>(I)V

    .line 398
    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 409
    add-int v0, p2, p3

    ushr-int/lit8 v3, v0, 0x1

    .line 410
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LamQ;

    .line 415
    invoke-interface {p0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LamQ;

    invoke-interface {p0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LamQ;

    invoke-static {p4, v0, v1}, LamO;->b(ILamQ;LamQ;)I

    move-result v0

    if-nez v0, :cond_8

    .line 419
    invoke-virtual {p5}, [Z->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Z

    .line 420
    const/4 v0, 0x1

    aput-boolean v0, v5, p4

    .line 424
    const/4 v1, 0x0

    .line 425
    array-length v3, v5

    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_3
    if-ge v1, v3, :cond_6

    aget-boolean v0, v5, v1

    .line 426
    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_4
    add-int/2addr v2, v0

    .line 425
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 426
    :cond_5
    const/4 v0, 0x0

    goto :goto_4

    .line 428
    :cond_6
    const/4 v0, 0x2

    if-ne v2, v0, :cond_7

    .line 432
    new-instance v0, LamR;

    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    add-int/lit8 v4, p3, 0x1

    invoke-interface {p0, p2, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LamR;-><init>(ILjava/lang/String;Ljava/util/List;Ljava/util/List;LamP;)V

    goto :goto_2

    :cond_7
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    .line 434
    invoke-static/range {v0 .. v5}, LamO;->a(Ljava/util/List;[Ljava/lang/String;III[Z)LamR;

    move-result-object v0

    goto :goto_2

    .line 441
    :cond_8
    const/4 v0, 0x2

    new-array v7, v0, [[Ljava/lang/String;

    .line 442
    const/4 v0, 0x0

    move v1, v0

    :goto_5
    const/4 v0, 0x2

    if-ge v1, v0, :cond_9

    .line 443
    invoke-virtual {p1}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aput-object v0, v7, v1

    .line 444
    aget-object v0, v7, v1

    mul-int/lit8 v2, p4, 0x2

    add-int/2addr v2, v1

    iget-object v5, v6, LamQ;->a:[Ljava/lang/String;

    aget-object v5, v5, p4

    aput-object v5, v0, v2

    .line 442
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 447
    :cond_9
    const/4 v0, 0x0

    aget-object v1, v7, v0

    move-object v0, p0

    move v2, p2

    move-object v5, p5

    .line 448
    invoke-static/range {v0 .. v5}, LamO;->a(Ljava/util/List;[Ljava/lang/String;III[Z)LamR;

    move-result-object v8

    const/4 v0, 0x1

    aget-object v1, v7, v0

    add-int/lit8 v2, v3, 0x1

    move-object v0, p0

    move v3, p3

    move-object v5, p5

    .line 449
    invoke-static/range {v0 .. v5}, LamO;->a(Ljava/util/List;[Ljava/lang/String;III[Z)LamR;

    move-result-object v0

    .line 447
    invoke-static {v8, v0}, LbmF;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmF;

    move-result-object v3

    .line 451
    new-instance v0, LamR;

    iget-object v1, v6, LamQ;->a:[Ljava/lang/String;

    aget-object v2, v1, p4

    const/4 v4, 0x0

    const/4 v5, 0x0

    move v1, p4

    invoke-direct/range {v0 .. v5}, LamR;-><init>(ILjava/lang/String;Ljava/util/List;Ljava/util/List;LamP;)V

    goto/16 :goto_2
.end method

.method private a()V
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, LamO;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 239
    return-void
.end method

.method static synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 44
    invoke-static {p0}, LamO;->b(Ljava/lang/Object;)V

    return-void
.end method

.method private a(Ljava/util/Set;LamQ;[Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<TT;>;",
            "LamQ",
            "<TT;>;[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 249
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    iget-object v0, p2, LamQ;->a:[Ljava/lang/String;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    iget-object v0, p2, LamQ;->a:[Ljava/lang/String;

    array-length v0, v0

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 253
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    array-length v0, p3

    if-ne v0, v3, :cond_1

    :goto_1
    invoke-static {v1}, LbiT;->a(Z)V

    .line 258
    :goto_2
    if-ge v2, v3, :cond_3

    .line 259
    iget-object v0, p2, LamQ;->a:[Ljava/lang/String;

    aget-object v0, v0, v2

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    aget-object v0, p3, v2

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    iget-object v0, p2, LamQ;->a:[Ljava/lang/String;

    aget-object v0, v0, v2

    aget-object v1, p3, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 266
    :goto_3
    return-void

    :cond_0
    move v0, v2

    .line 252
    goto :goto_0

    :cond_1
    move v1, v2

    .line 254
    goto :goto_1

    .line 258
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 265
    :cond_3
    iget-object v0, p2, LamQ;->a:Ljava/lang/Object;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3
.end method

.method private a(Ljava/util/Set;[Ljava/lang/String;LamR;[Ljava/lang/String;I)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<TT;>;[",
            "Ljava/lang/String;",
            "LamR",
            "<TT;>;[",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 194
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    if-ge v0, p5, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 195
    array-length v0, p2

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, LbiT;->a(Z)V

    .line 196
    array-length v0, p4

    if-ne v0, v7, :cond_3

    :goto_2
    invoke-static {v1}, LbiT;->a(Z)V

    .line 197
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    invoke-direct {p0}, LamO;->b()V

    .line 202
    invoke-virtual {p3}, LamR;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 203
    iget-object v0, p3, LamR;->b:LbmF;

    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LamQ;

    .line 204
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v2

    if-lt v2, p5, :cond_4

    .line 235
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 194
    goto :goto_0

    :cond_2
    move v0, v2

    .line 195
    goto :goto_1

    :cond_3
    move v1, v2

    .line 196
    goto :goto_2

    .line 208
    :cond_4
    invoke-direct {p0, p1, v0, p4}, LamO;->a(Ljava/util/Set;LamQ;[Ljava/lang/String;)V

    goto :goto_3

    :cond_5
    move v6, v2

    .line 214
    :goto_4
    if-ge v6, v7, :cond_0

    .line 218
    invoke-virtual {p2}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 224
    iget v0, p3, LamR;->a:I

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v6

    iget-object v1, p3, LamR;->a:Ljava/lang/String;

    aput-object v1, v2, v0

    .line 226
    invoke-direct {p0, p4, v2}, LamO;->a([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 227
    iget-object v0, p3, LamR;->a:LbmF;

    invoke-virtual {v0, v6}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LamR;

    move-object v0, p0

    move-object v1, p1

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, LamO;->a(Ljava/util/Set;[Ljava/lang/String;LamR;[Ljava/lang/String;I)V

    .line 229
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    if-ge v0, p5, :cond_0

    .line 214
    :cond_6
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_4
.end method

.method private a([Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 277
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    array-length v0, p1

    if-ne v0, v6, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 279
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    array-length v0, p2

    const/4 v3, 0x4

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LbiT;->a(Z)V

    move v4, v2

    .line 284
    :goto_2
    if-ge v4, v6, :cond_5

    move v3, v2

    .line 289
    :goto_3
    if-ge v3, v6, :cond_4

    .line 290
    invoke-static {v4, v3}, LamO;->a(II)I

    move-result v0

    aget-object v0, p2, v0

    .line 291
    aget-object v5, p1, v4

    .line 292
    invoke-static {v5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    invoke-static {v0, v5}, LamO;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 306
    invoke-static {v3}, LamO;->a(I)I

    move-result v5

    .line 307
    mul-int/2addr v0, v5

    if-gtz v0, :cond_2

    move v0, v1

    .line 308
    :goto_4
    if-nez v0, :cond_3

    .line 314
    :goto_5
    return v2

    :cond_0
    move v0, v2

    .line 278
    goto :goto_0

    :cond_1
    move v0, v2

    .line 280
    goto :goto_1

    :cond_2
    move v0, v2

    .line 307
    goto :goto_4

    .line 289
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 284
    :cond_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    :cond_5
    move v2, v1

    .line 314
    goto :goto_5
.end method

.method static a()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 480
    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    .line 481
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x2

    if-ge v0, v2, :cond_0

    .line 482
    mul-int/lit8 v2, v0, 0x2

    add-int/lit8 v2, v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    .line 483
    mul-int/lit8 v2, v0, 0x2

    add-int/lit8 v2, v2, 0x1

    const/4 v3, 0x0

    aput-object v3, v1, v2

    .line 481
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 485
    :cond_0
    return-object v1
.end method

.method private static b(ILamQ;LamQ;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U:",
            "Ljava/lang/Object;",
            ">(I",
            "LamQ",
            "<TU;>;",
            "LamQ",
            "<TU;>;)I"
        }
    .end annotation

    .prologue
    .line 458
    iget-object v0, p1, LamQ;->a:[Ljava/lang/String;

    aget-object v0, v0, p0

    iget-object v1, p2, LamQ;->a:[Ljava/lang/String;

    aget-object v1, v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, LamO;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 243
    return-void
.end method

.method private static b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 468
    if-eqz p0, :cond_0

    .line 469
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 471
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/util/Set;[Ljava/lang/String;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<TT;>;[",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 179
    invoke-direct {p0}, LamO;->a()V

    .line 181
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    if-ge v0, p3, :cond_0

    iget-object v0, p0, LamO;->a:LamR;

    if-nez v0, :cond_1

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    invoke-static {}, LamO;->a()[Ljava/lang/String;

    move-result-object v2

    .line 186
    iget-object v3, p0, LamO;->a:LamR;

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, LamO;->a(Ljava/util/Set;[Ljava/lang/String;LamR;[Ljava/lang/String;I)V

    goto :goto_0
.end method
