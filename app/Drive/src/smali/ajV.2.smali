.class public LajV;
.super Landroid/widget/BaseAdapter;
.source "Accounts.java"


# instance fields
.field private final a:I

.field private final a:LabF;

.field private final a:LabI;

.field private final a:Landroid/content/Context;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Z

.field private final b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;LabI;LabF;ZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LabI;",
            "LabF;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    .line 159
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 160
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LajV;->a:Landroid/content/Context;

    .line 161
    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 162
    iput p2, p0, LajV;->a:I

    .line 163
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, LajV;->a:Ljava/util/List;

    .line 164
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LabI;

    iput-object v0, p0, LajV;->a:LabI;

    .line 165
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LabF;

    iput-object v0, p0, LajV;->a:LabF;

    .line 166
    iput-boolean p6, p0, LajV;->a:Z

    .line 167
    iput-boolean p7, p0, LajV;->b:Z

    .line 168
    return-void

    .line 161
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, LajV;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    .line 178
    iget-object v0, p0, LajV;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, LajV;->a:LabI;

    iget-object v3, p0, LajV;->a:LabF;

    iget v4, p0, LajV;->a:I

    iget-object v5, p0, LajV;->a:Landroid/content/Context;

    iget-boolean v6, p0, LajV;->b:Z

    move-object v1, p2

    invoke-static/range {v0 .. v6}, LajS;->a(Ljava/lang/String;Landroid/view/View;LabI;LabF;ILandroid/content/Context;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, LajV;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 194
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    .line 172
    iget-object v0, p0, LajV;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, LajV;->a:LabI;

    iget-object v3, p0, LajV;->a:LabF;

    iget v4, p0, LajV;->a:I

    iget-object v5, p0, LajV;->a:Landroid/content/Context;

    iget-boolean v6, p0, LajV;->a:Z

    move-object v1, p2

    invoke-static/range {v0 .. v6}, LajS;->a(Ljava/lang/String;Landroid/view/View;LabI;LabF;ILandroid/content/Context;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
