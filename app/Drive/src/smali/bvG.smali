.class public final LbvG;
.super Ljava/lang/Object;
.source "SourceProvider.java"


# static fields
.field public static final a:LbvG;

.field public static final a:Ljava/lang/Object;


# instance fields
.field private final a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LbvG;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    const-string v0, "[unknown source]"

    sput-object v0, LbvG;->a:Ljava/lang/Object;

    .line 38
    new-instance v0, LbvG;

    const-class v1, LbvG;

    .line 39
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v1

    invoke-direct {v0, v1}, LbvG;-><init>(Ljava/lang/Iterable;)V

    sput-object v0, LbvG;->a:LbvG;

    .line 38
    return-void
.end method

.method private constructor <init>(LbvG;Ljava/lang/Iterable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbvG;",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, LbvG;->b:LbvG;

    .line 48
    invoke-static {}, LbmY;->a()Lbna;

    move-result-object v1

    .line 49
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 50
    if-eqz p1, :cond_1

    invoke-direct {p1, v0}, LbvG;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 51
    :cond_1
    invoke-virtual {v1, v0}, Lbna;->a(Ljava/lang/Object;)Lbna;

    goto :goto_0

    .line 54
    :cond_2
    invoke-virtual {v1}, Lbna;->a()LbmY;

    move-result-object v0

    iput-object v0, p0, LbvG;->a:LbmY;

    .line 55
    return-void
.end method

.method private constructor <init>(Ljava/lang/Iterable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, LbvG;-><init>(LbvG;Ljava/lang/Iterable;)V

    .line 43
    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, LbvG;->b:LbvG;

    if-eqz v0, :cond_0

    iget-object v0, p0, LbvG;->b:LbvG;

    invoke-direct {v0, p1}, LbvG;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, LbvG;->a:LbmY;

    .line 65
    invoke-virtual {v0, p1}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
