.class LaCo;
.super LaCu;


# instance fields
.field final synthetic a:J

.field final synthetic a:LaCV;

.field final synthetic a:LaCh;

.field final synthetic a:LaCn;

.field final synthetic a:Lorg/json/JSONObject;

.field final synthetic a:Z


# direct methods
.method constructor <init>(LaCn;LaCV;LaCh;ZJLorg/json/JSONObject;)V
    .locals 1

    iput-object p1, p0, LaCo;->a:LaCn;

    iput-object p2, p0, LaCo;->a:LaCV;

    iput-object p3, p0, LaCo;->a:LaCh;

    iput-boolean p4, p0, LaCo;->a:Z

    iput-wide p5, p0, LaCo;->a:J

    iput-object p7, p0, LaCo;->a:Lorg/json/JSONObject;

    invoke-direct {p0}, LaCu;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic a(LaCR;)V
    .locals 0

    check-cast p1, LaQO;

    invoke-virtual {p0, p1}, LaCo;->a(LaQO;)V

    return-void
.end method

.method protected a(LaQO;)V
    .locals 8

    iget-object v0, p0, LaCo;->a:LaCn;

    invoke-static {v0}, LaCn;->a(LaCn;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    :try_start_0
    iget-object v0, p0, LaCo;->a:LaCn;

    invoke-static {v0}, LaCn;->a(LaCn;)LaCs;

    move-result-object v0

    iget-object v1, p0, LaCo;->a:LaCV;

    invoke-virtual {v0, v1}, LaCs;->a(LaCV;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, LaCo;->a:LaCn;

    invoke-static {v0}, LaCn;->a(LaCn;)LaRf;

    move-result-object v0

    iget-object v1, p0, LaCo;->a:LaRh;

    iget-object v2, p0, LaCo;->a:LaCh;

    iget-boolean v3, p0, LaCo;->a:Z

    iget-wide v4, p0, LaCo;->a:J

    iget-object v6, p0, LaCo;->a:Lorg/json/JSONObject;

    invoke-virtual/range {v0 .. v6}, LaRf;->a(LaRh;LaCh;ZJLorg/json/JSONObject;)J
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v0, p0, LaCo;->a:LaCn;

    invoke-static {v0}, LaCn;->a(LaCn;)LaCs;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LaCs;->a(LaCV;)V

    :goto_0
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void

    :catch_0
    move-exception v0

    :try_start_3
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-virtual {p0, v0}, LaCo;->a(Lcom/google/android/gms/common/api/Status;)LaCr;

    move-result-object v0

    invoke-virtual {p0, v0}, LaCo;->a(LaDc;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    iget-object v0, p0, LaCo;->a:LaCn;

    invoke-static {v0}, LaCn;->a(LaCn;)LaCs;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LaCs;->a(LaCV;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_5
    iget-object v1, p0, LaCo;->a:LaCn;

    invoke-static {v1}, LaCn;->a(LaCn;)LaCs;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LaCs;->a(LaCV;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method
