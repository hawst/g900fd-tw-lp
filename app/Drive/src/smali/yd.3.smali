.class public Lyd;
.super Ljava/lang/Object;
.source "CrossAppPromoIntentCreator.java"


# instance fields
.field private final a:Landroid/content/Context;

.field private final a:LqF;


# direct methods
.method constructor <init>(Landroid/content/Context;LqF;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lajg;
        .end annotation
    .end param

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p2, p0, Lyd;->a:LqF;

    .line 31
    iput-object p1, p0, Lyd;->a:Landroid/content/Context;

    .line 32
    return-void
.end method


# virtual methods
.method a(LasP;Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 37
    if-nez p1, :cond_1

    .line 71
    :cond_0
    :goto_0
    return-object v0

    .line 41
    :cond_1
    new-instance v1, LasM;

    invoke-direct {v1}, LasM;-><init>()V

    sget-object v2, LasO;->c:LasO;

    invoke-virtual {v1, v2}, LasM;->a(LasO;)LasM;

    move-result-object v1

    invoke-interface {p1}, LasP;->a()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, LasM;->a(Landroid/net/Uri;)LasM;

    move-result-object v1

    .line 42
    invoke-interface {p1}, LasP;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LasM;->a(Ljava/lang/String;)LasM;

    move-result-object v1

    invoke-virtual {v1}, LasM;->a()LasJ;

    move-result-object v1

    .line 46
    invoke-virtual {v1}, LasJ;->a()I

    move-result v2

    if-eqz v2, :cond_0

    .line 50
    iget-object v0, p0, Lyd;->a:Landroid/content/Context;

    invoke-static {v0, p2, p4, p3}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v2

    .line 55
    iget-object v0, p0, Lyd;->a:Landroid/content/Context;

    sget v3, Lxi;->cross_app_promo_install_button_text:I

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 56
    iget-object v0, p0, Lyd;->a:Landroid/content/Context;

    sget v4, Lxi;->cross_app_promo_dismiss_button_text:I

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 58
    iget-object v0, p0, Lyd;->a:LqF;

    .line 59
    invoke-virtual {v0, p2}, LqF;->a(Ljava/lang/String;)LqH;

    move-result-object v4

    .line 60
    iget-object v0, p0, Lyd;->a:LqF;

    .line 61
    invoke-virtual {v0, p2}, LqF;->b(Ljava/lang/String;)LqH;

    move-result-object v6

    .line 63
    iget-object v0, p0, Lyd;->a:Landroid/content/Context;

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->a(Landroid/content/Context;LasJ;Landroid/content/Intent;Ljava/lang/String;LqH;Ljava/lang/String;LqH;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method
