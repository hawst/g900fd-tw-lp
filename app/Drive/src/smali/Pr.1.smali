.class LPr;
.super Ljava/lang/Object;
.source "EntryActionHelperImpl.java"

# interfaces
.implements LbsJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbsJ",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LH;

.field final synthetic a:LPq;


# direct methods
.method constructor <init>(LPq;LH;)V
    .locals 0

    .prologue
    .line 297
    iput-object p1, p0, LPr;->a:LPq;

    iput-object p2, p0, LPr;->a:LH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 309
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, LPr;->a:LH;

    sget v1, Lxi;->share_card_enabled_link_sharing_toast:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 311
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 313
    iget-object v0, p0, LPr;->a:LPq;

    invoke-static {v0}, LPq;->a(LPq;)Lacj;

    move-result-object v0

    invoke-virtual {v0}, Lacj;->c()V

    .line 314
    iget-object v0, p0, LPr;->a:LPq;

    invoke-static {v0}, LPq;->a(LPq;)LvU;

    move-result-object v0

    invoke-interface {v0}, LvU;->a()V

    .line 316
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 297
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, LPr;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 302
    iget-object v0, p0, LPr;->a:LH;

    sget v1, Lxi;->sharing_message_unable_to_change:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 303
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 304
    iget-object v0, p0, LPr;->a:LPq;

    invoke-virtual {v0, v2}, LPq;->a(Z)V

    .line 305
    return-void
.end method
