.class public final LaML;
.super LaLr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaLr",
        "<",
        "LaMX;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LaML;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaML;

    invoke-direct {v0}, LaML;-><init>()V

    sput-object v0, LaML;->a:LaML;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const-string v0, "com.google.android.gms.ads.AdManagerCreatorImpl"

    invoke-direct {p0, v0}, LaLr;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/internal/aq;Ljava/lang/String;LaNN;)LaMU;
    .locals 6

    const v1, 0x4da6e8

    invoke-static {p0}, LaCJ;->a(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LaML;->a:LaML;

    invoke-direct {v0, p0, p1, p2, p3}, LaML;->b(Landroid/content/Context;Lcom/google/android/gms/internal/aq;Ljava/lang/String;LaNN;)LaMU;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "Using AdManager from the client jar."

    invoke-static {v0}, LaQc;->a(Ljava/lang/String;)V

    new-instance v5, Lcom/google/android/gms/internal/fa;

    const/4 v0, 0x1

    invoke-direct {v5, v1, v1, v0}, Lcom/google/android/gms/internal/fa;-><init>(IIZ)V

    new-instance v0, LaVl;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, LaVl;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/aq;Ljava/lang/String;LaNO;Lcom/google/android/gms/internal/fa;)V

    :cond_1
    return-object v0
.end method

.method private b(Landroid/content/Context;Lcom/google/android/gms/internal/aq;Ljava/lang/String;LaNN;)LaMU;
    .locals 7

    const/4 v6, 0x0

    :try_start_0
    invoke-static {p1}, LaLp;->a(Ljava/lang/Object;)LaLm;

    move-result-object v1

    invoke-virtual {p0, p1}, LaML;->a(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaMX;

    const v5, 0x4da6e8

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-interface/range {v0 .. v5}, LaMX;->a(LaLm;Lcom/google/android/gms/internal/aq;Ljava/lang/String;LaNO;I)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, LaMV;->a(Landroid/os/IBinder;)LaMU;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LaLs; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "Could not create remote AdManager."

    invoke-static {v1, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v6

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "Could not create remote AdManager."

    invoke-static {v1, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v6

    goto :goto_0
.end method


# virtual methods
.method protected a(Landroid/os/IBinder;)LaMX;
    .locals 1

    invoke-static {p1}, LaMY;->a(Landroid/os/IBinder;)LaMX;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic a(Landroid/os/IBinder;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, LaML;->a(Landroid/os/IBinder;)LaMX;

    move-result-object v0

    return-object v0
.end method
