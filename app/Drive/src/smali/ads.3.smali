.class public Lads;
.super Ljava/lang/Object;
.source "DocumentFileManagerImpl.java"

# interfaces
.implements LbsJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbsJ",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ladw;

.field final synthetic a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

.field private final a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private final b:Ladw;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Lcom/google/android/gms/drive/database/data/EntrySpec;Ladw;Ladw;)V
    .locals 1

    .prologue
    .line 906
    iput-object p1, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 907
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lads;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 908
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladw;

    iput-object v0, p0, Lads;->b:Ladw;

    .line 909
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladw;

    iput-object v0, p0, Lads;->a:Ladw;

    .line 910
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 924
    iget-object v1, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    monitor-enter v1

    .line 926
    :try_start_0
    iget-object v0, p0, Lads;->a:Ladw;

    invoke-virtual {v0}, Ladw;->a()LaGp;

    move-result-object v0

    .line 927
    invoke-virtual {v0}, LaGp;->d()Ljava/lang/Long;

    move-result-object v2

    .line 928
    iget-object v3, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    .line 929
    invoke-static {v3}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)Lalo;

    move-result-object v3

    iget-object v4, p0, Lads;->b:Ladw;

    invoke-virtual {v4}, Ladw;->a()LadY;

    move-result-object v4

    invoke-interface {v3, v4}, Lalo;->a(LadY;)J

    move-result-wide v4

    .line 934
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 935
    iget-object v2, p0, Lads;->b:Ladw;

    invoke-virtual {v2}, Ladw;->a()LaGp;

    move-result-object v2

    .line 938
    iget-object v3, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v3}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v3

    invoke-interface {v3}, LaGg;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 941
    :try_start_1
    iget-object v3, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v3}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v3

    iget-object v6, p0, Lads;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v3, v6}, LaGg;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;

    move-result-object v3

    .line 942
    if-nez v3, :cond_0

    .line 943
    const-string v0, "DocumentFileManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Document for EntrySpec no longer exists: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lads;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 971
    :try_start_2
    iget-object v0, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v0

    invoke-interface {v0}, LaGg;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 984
    :try_start_3
    iget-object v0, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    iget-object v2, p0, Lads;->a:Ladw;

    invoke-static {v0, v2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Ladw;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 986
    :try_start_4
    iget-object v0, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    iget-object v2, p0, Lads;->b:Ladw;

    invoke-static {v0, v2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Ladw;)V

    monitor-exit v1

    .line 990
    :goto_0
    return-void

    .line 986
    :catchall_0
    move-exception v0

    iget-object v2, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    iget-object v3, p0, Lads;->b:Ladw;

    invoke-static {v2, v3}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Ladw;)V

    throw v0

    .line 989
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 946
    :cond_0
    :try_start_5
    invoke-virtual {v2}, LaGp;->a()V

    .line 947
    invoke-virtual {v2, v4, v5}, LaGp;->d(J)V

    .line 949
    invoke-virtual {v0}, LaGp;->a()Ljava/util/Date;

    move-result-object v4

    .line 948
    invoke-virtual {v2, v4}, LaGp;->a(Ljava/util/Date;)V

    .line 950
    invoke-virtual {v2, v0}, LaGp;->a(LaGp;)V

    .line 951
    iget-object v4, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v4}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LadE;

    move-result-object v4

    invoke-interface {v4, v2}, LadE;->a(LaGp;)V

    .line 958
    invoke-virtual {v3}, LaGb;->a()LaGc;

    move-result-object v3

    .line 960
    invoke-virtual {v2}, LaGp;->c()J

    move-result-wide v4

    sget-object v2, LacY;->a:LacY;

    invoke-virtual {v3, v4, v5, v2}, LaGc;->a(JLacY;)LaGc;

    move-result-object v2

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    .line 961
    invoke-virtual {v2, v4}, LaGc;->a(Ljava/util/Date;)V

    .line 962
    invoke-virtual {v3}, LaGc;->b()LaGb;

    move-result-object v2

    .line 964
    iget-object v3, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v3, v2, v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;LaGo;LaGp;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 965
    const-string v3, "DocumentFileManager"

    const-string v4, "Scheduling an implicit upload of %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v3, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 966
    iget-object v3, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v3}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LahT;

    move-result-object v3

    invoke-interface {v3, v2, v0}, LahT;->a(LaGo;LaGp;)V

    .line 969
    :cond_1
    iget-object v0, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v0

    invoke-interface {v0}, LaGg;->c()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 971
    :try_start_6
    iget-object v0, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v0

    invoke-interface {v0}, LaGg;->b()V

    .line 974
    if-eqz v2, :cond_2

    .line 975
    iget-object v0, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v0

    invoke-virtual {v2}, LaGb;->a()LaFM;

    move-result-object v2

    invoke-interface {v0, v2}, LaGg;->b(LaFM;)V

    .line 978
    :cond_2
    iget-object v0, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LtK;

    move-result-object v0

    sget-object v2, Lry;->m:Lry;

    invoke-interface {v0, v2}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 979
    iget-object v0, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LMs;

    move-result-object v0

    invoke-virtual {v0}, LMs;->a()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 984
    :cond_3
    :try_start_7
    iget-object v0, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    iget-object v2, p0, Lads;->a:Ladw;

    invoke-static {v0, v2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Ladw;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 986
    :try_start_8
    iget-object v0, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    iget-object v2, p0, Lads;->b:Ladw;

    invoke-static {v0, v2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Ladw;)V

    .line 989
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_0

    .line 971
    :catchall_2
    move-exception v0

    :try_start_9
    iget-object v2, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v2

    invoke-interface {v2}, LaGg;->b()V

    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 983
    :catchall_3
    move-exception v0

    .line 984
    :try_start_a
    iget-object v2, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    iget-object v3, p0, Lads;->a:Ladw;

    invoke-static {v2, v3}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Ladw;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    .line 986
    :try_start_b
    iget-object v2, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    iget-object v3, p0, Lads;->b:Ladw;

    invoke-static {v2, v3}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Ladw;)V

    throw v0

    :catchall_4
    move-exception v0

    iget-object v2, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    iget-object v3, p0, Lads;->b:Ladw;

    invoke-static {v2, v3}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Ladw;)V

    throw v0

    :catchall_5
    move-exception v0

    iget-object v2, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    iget-object v3, p0, Lads;->b:Ladw;

    invoke-static {v2, v3}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Ladw;)V

    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 914
    const-string v0, "DocumentFileManager"

    const-string v1, "Update paging error:"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, p1, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 916
    :try_start_0
    iget-object v0, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    iget-object v1, p0, Lads;->a:Ladw;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Ladw;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 918
    iget-object v0, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    iget-object v1, p0, Lads;->b:Ladw;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Ladw;)V

    .line 920
    return-void

    .line 918
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lads;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    iget-object v2, p0, Lads;->b:Ladw;

    invoke-static {v1, v2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Ladw;)V

    throw v0
.end method
