.class public LXn;
.super Ljava/lang/Object;
.source "PassThroughPunchWebViewTouchListener.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private final a:LSC;

.field private final a:LXp;

.field private final a:LYO;

.field private final a:LaqO;

.field protected final a:LqK;


# direct methods
.method protected constructor <init>(Landroid/content/Context;LXz;LYO;LqK;)V
    .locals 2

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, LXp;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LXp;-><init>(LXn;LXo;)V

    iput-object v0, p0, LXn;->a:LXp;

    .line 80
    new-instance v0, LSC;

    iget-object v1, p0, LXn;->a:LXp;

    invoke-direct {v0, p1, v1}, LSC;-><init>(Landroid/content/Context;LSD;)V

    iput-object v0, p0, LXn;->a:LSC;

    .line 82
    iput-object p3, p0, LXn;->a:LYO;

    .line 83
    iput-object p4, p0, LXn;->a:LqK;

    .line 85
    new-instance v0, LaqO;

    new-instance v1, LXo;

    invoke-direct {v1, p0, p3, p2}, LXo;-><init>(LXn;LYO;LXz;)V

    invoke-direct {v0, v1}, LaqO;-><init>(LaqP;)V

    iput-object v0, p0, LXn;->a:LaqO;

    .line 111
    return-void
.end method

.method static synthetic a(LXn;)LXp;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, LXn;->a:LXp;

    return-object v0
.end method

.method static synthetic a(LXn;)LYO;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, LXn;->a:LYO;

    return-object v0
.end method


# virtual methods
.method protected a(I)V
    .locals 0

    .prologue
    .line 128
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, LXn;->a:LSC;

    invoke-virtual {v0, p2}, LSC;->a(Landroid/view/MotionEvent;)Z

    .line 117
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 118
    and-int/lit16 v0, v0, 0xff

    .line 120
    iget-object v0, p0, LXn;->a:LSC;

    invoke-virtual {v0}, LSC;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LXn;->a:LXp;

    iget-boolean v0, v0, LXp;->a:Z

    if-nez v0, :cond_0

    .line 121
    iget-object v0, p0, LXn;->a:LaqO;

    invoke-virtual {v0, p2}, LaqO;->a(Landroid/view/MotionEvent;)V

    .line 124
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
