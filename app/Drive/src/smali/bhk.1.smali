.class public final enum Lbhk;
.super Ljava/lang/Enum;
.source "ScanSession.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lbhk;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lbhk;

.field private static final synthetic a:[Lbhk;

.field public static final enum b:Lbhk;

.field public static final enum c:Lbhk;

.field public static final enum d:Lbhk;

.field public static final enum e:Lbhk;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 39
    new-instance v0, Lbhk;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, Lbhk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbhk;->a:Lbhk;

    .line 40
    new-instance v0, Lbhk;

    const-string v1, "STORAGE_UNAVAILABLE"

    invoke-direct {v0, v1, v3}, Lbhk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbhk;->b:Lbhk;

    .line 41
    new-instance v0, Lbhk;

    const-string v1, "STORAGE_FILE_NOT_FOUND"

    invoke-direct {v0, v1, v4}, Lbhk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbhk;->c:Lbhk;

    .line 42
    new-instance v0, Lbhk;

    const-string v1, "STORAGE_WRITE_ERROR"

    invoke-direct {v0, v1, v5}, Lbhk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbhk;->d:Lbhk;

    .line 43
    new-instance v0, Lbhk;

    const-string v1, "RECTIFY_FAILED"

    invoke-direct {v0, v1, v6}, Lbhk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbhk;->e:Lbhk;

    .line 38
    const/4 v0, 0x5

    new-array v0, v0, [Lbhk;

    sget-object v1, Lbhk;->a:Lbhk;

    aput-object v1, v0, v2

    sget-object v1, Lbhk;->b:Lbhk;

    aput-object v1, v0, v3

    sget-object v1, Lbhk;->c:Lbhk;

    aput-object v1, v0, v4

    sget-object v1, Lbhk;->d:Lbhk;

    aput-object v1, v0, v5

    sget-object v1, Lbhk;->e:Lbhk;

    aput-object v1, v0, v6

    sput-object v0, Lbhk;->a:[Lbhk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbhk;
    .locals 1

    .prologue
    .line 38
    const-class v0, Lbhk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbhk;

    return-object v0
.end method

.method public static values()[Lbhk;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lbhk;->a:[Lbhk;

    invoke-virtual {v0}, [Lbhk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbhk;

    return-object v0
.end method
