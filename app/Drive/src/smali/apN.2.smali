.class public LapN;
.super LSB;
.source "GestureImageView.java"


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/view/GestureImageView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/docs/view/GestureImageView;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-direct {p0}, LSB;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/docs/view/GestureImageView;LapL;)V
    .locals 0

    .prologue
    .line 150
    invoke-direct {p0, p1}, LapN;-><init>(Lcom/google/android/apps/docs/view/GestureImageView;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    iget-object v0, v0, Lcom/google/android/apps/docs/view/GestureImageView;->a:LaqL;

    invoke-interface {v0}, LaqL;->f()V

    .line 208
    invoke-super {p0, p1}, LSB;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3

    .prologue
    .line 154
    iget-object v0, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    iget-object v1, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v1}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/graphics/Matrix;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;Landroid/graphics/Matrix;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 155
    iget-object v0, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v0}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/graphics/Matrix;

    move-result-object v0

    neg-float v1, p3

    neg-float v2, p4

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 156
    iget-object v0, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    iget-object v1, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v1}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/graphics/Matrix;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;Landroid/graphics/Matrix;)V

    .line 157
    iget-object v0, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v0}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)V

    .line 158
    iget-object v0, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/GestureImageView;->invalidate()V

    .line 160
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public b(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    .line 180
    iget-object v0, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v0}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v0}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/graphics/Matrix;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Landroid/graphics/Matrix;)F

    move-result v0

    .line 183
    const/high16 v1, 0x3f800000    # 1.0f

    iget-object v2, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v2}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)F

    move-result v2

    div-float v2, v0, v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v2, 0x3dcccccd    # 0.1f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 184
    iget-object v0, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v0}, Lcom/google/android/apps/docs/view/GestureImageView;->b(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/graphics/Matrix;

    move-result-object v4

    .line 198
    :goto_0
    iget-object v0, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v0}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)V

    .line 199
    iget-object v8, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    new-instance v0, LapM;

    iget-object v1, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    const/high16 v2, 0x43160000    # 150.0f

    iget-object v3, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    .line 200
    invoke-static {v3}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/graphics/Matrix;

    move-result-object v3

    new-instance v5, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v5}, Landroid/view/animation/LinearInterpolator;-><init>()V

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, LapM;-><init>(Lcom/google/android/apps/docs/view/GestureImageView;FLandroid/graphics/Matrix;Landroid/graphics/Matrix;Landroid/view/animation/Interpolator;ZLapL;)V

    .line 199
    invoke-static {v8, v0}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;LapM;)LapM;

    .line 202
    :cond_0
    invoke-super {p0, p1}, LSB;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 186
    :cond_1
    iget-object v1, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    iget-object v2, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v2}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;F)F

    move-result v1

    .line 187
    new-instance v4, Landroid/graphics/Matrix;

    iget-object v2, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v2}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/graphics/Matrix;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 188
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 189
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 190
    div-float v0, v1, v0

    .line 192
    invoke-static {v2, v3, v0}, Lcom/google/android/apps/docs/view/GestureImageView;->a(FFF)Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 194
    iget-object v0, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/GestureImageView;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    sub-float/2addr v0, v2

    iget-object v1, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/view/GestureImageView;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    sub-float/2addr v1, v3

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 195
    iget-object v0, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v0, v4}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;Landroid/graphics/Matrix;)V

    goto :goto_0
.end method

.method public b(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 9

    .prologue
    const v5, 0x3e4ccccd    # 0.2f

    .line 166
    iget-object v0, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    iget-object v1, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v1}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/graphics/Matrix;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;Landroid/graphics/Matrix;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 167
    iget-object v0, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v0}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)V

    .line 168
    const-wide/16 v2, 0x2bc

    .line 169
    new-instance v4, Landroid/graphics/Matrix;

    iget-object v0, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v0}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/graphics/Matrix;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 170
    mul-float v0, p3, v5

    mul-float v1, p4, v5

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 172
    iget-object v8, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    new-instance v0, LapM;

    iget-object v1, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    long-to-float v2, v2

    iget-object v3, p0, LapN;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    .line 173
    invoke-static {v3}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/graphics/Matrix;

    move-result-object v3

    new-instance v5, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-direct {v5, v6}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, LapM;-><init>(Lcom/google/android/apps/docs/view/GestureImageView;FLandroid/graphics/Matrix;Landroid/graphics/Matrix;Landroid/view/animation/Interpolator;ZLapL;)V

    .line 172
    invoke-static {v8, v0}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;LapM;)LapM;

    .line 175
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, LSB;->b(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    return v0
.end method
