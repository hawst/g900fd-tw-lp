.class public final enum Luj;
.super Ljava/lang/Enum;
.source "MainProxyLogic.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Luj;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Luj;

.field private static final synthetic a:[Luj;

.field public static final enum b:Luj;

.field public static final enum c:Luj;

.field public static final enum d:Luj;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 55
    new-instance v0, Luj;

    const-string v1, "SHOW_WELCOME"

    invoke-direct {v0, v1, v2}, Luj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Luj;->a:Luj;

    .line 56
    new-instance v0, Luj;

    const-string v1, "OPEN_DOC_LIST"

    invoke-direct {v0, v1, v3}, Luj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Luj;->b:Luj;

    .line 57
    new-instance v0, Luj;

    const-string v1, "OPEN_ENTRY"

    invoke-direct {v0, v1, v4}, Luj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Luj;->c:Luj;

    .line 61
    new-instance v0, Luj;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v5}, Luj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Luj;->d:Luj;

    .line 54
    const/4 v0, 0x4

    new-array v0, v0, [Luj;

    sget-object v1, Luj;->a:Luj;

    aput-object v1, v0, v2

    sget-object v1, Luj;->b:Luj;

    aput-object v1, v0, v3

    sget-object v1, Luj;->c:Luj;

    aput-object v1, v0, v4

    sget-object v1, Luj;->d:Luj;

    aput-object v1, v0, v5

    sput-object v0, Luj;->a:[Luj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Luj;
    .locals 1

    .prologue
    .line 54
    const-class v0, Luj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Luj;

    return-object v0
.end method

.method public static values()[Luj;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Luj;->a:[Luj;

    invoke-virtual {v0}, [Luj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Luj;

    return-object v0
.end method
