.class public abstract LaGd;
.super Ljava/lang/Object;
.source "DatabaseEntry.java"

# interfaces
.implements LaGu;


# instance fields
.field protected final a:LaGe;


# direct methods
.method protected constructor <init>(LaGe;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, LaGd;->a:LaGe;

    .line 19
    return-void
.end method


# virtual methods
.method public a()LaFM;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->a()LaFM;

    move-result-object v0

    return-object v0
.end method

.method public abstract a()LaGe;
.end method

.method public a()LaGn;
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0}, LaGd;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaGn;->a(Ljava/lang/String;)LaGn;

    move-result-object v0

    return-object v0
.end method

.method public a()LaGv;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->a()LaGv;

    move-result-object v0

    return-object v0
.end method

.method public a()LaGw;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->a()LaGw;

    move-result-object v0

    return-object v0
.end method

.method public a()LaGx;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->a()LaGx;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a()Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, LaGd;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/drive/database/data/ResourceSpec;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/Date;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->a()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->a()Z

    move-result v0

    return v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 22
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method public b()Ljava/util/Date;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->b()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->b()Z

    move-result v0

    return v0
.end method

.method public c()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->d()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/util/Date;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->c()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->c()Z

    move-result v0

    return v0
.end method

.method public d()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->e()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/util/Date;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->d()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->e()Z

    move-result v0

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->f()Z

    move-result v0

    return v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->g()Z

    move-result v0

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->h()Z

    move-result v0

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->j()Z

    move-result v0

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->l()Z

    move-result v0

    return v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, LaGd;->a:LaGe;

    invoke-virtual {v0}, LaGe;->m()Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 234
    const-string v0, "Entry %s of %s with resource id: %s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, LaGd;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, LaGd;->a()LaFM;

    move-result-object v3

    .line 235
    invoke-virtual {v3}, LaFM;->a()LaFO;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, LaGd;->i()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 234
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
