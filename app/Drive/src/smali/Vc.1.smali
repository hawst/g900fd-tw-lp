.class public final enum LVc;
.super Ljava/lang/Enum;
.source "UrlType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LVc;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LVc;

.field private static final synthetic a:[LVc;

.field public static final enum b:LVc;

.field public static final enum c:LVc;

.field public static final enum d:LVc;

.field public static final enum e:LVc;

.field public static final enum f:LVc;

.field public static final enum g:LVc;

.field public static final enum h:LVc;

.field public static final enum i:LVc;

.field public static final enum j:LVc;

.field public static final enum k:LVc;

.field public static final enum l:LVc;


# instance fields
.field private a:LaGv;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 12
    new-instance v0, LVc;

    const-string v1, "DOCUMENT"

    sget-object v2, LaGv;->b:LaGv;

    invoke-direct {v0, v1, v4, v2}, LVc;-><init>(Ljava/lang/String;ILaGv;)V

    sput-object v0, LVc;->a:LVc;

    .line 14
    new-instance v0, LVc;

    const-string v1, "DRAWING"

    sget-object v2, LaGv;->c:LaGv;

    invoke-direct {v0, v1, v5, v2}, LVc;-><init>(Ljava/lang/String;ILaGv;)V

    sput-object v0, LVc;->b:LVc;

    .line 16
    new-instance v0, LVc;

    const-string v1, "FILE"

    sget-object v2, LaGv;->d:LaGv;

    invoke-direct {v0, v1, v6, v2}, LVc;-><init>(Ljava/lang/String;ILaGv;)V

    sput-object v0, LVc;->c:LVc;

    .line 18
    new-instance v0, LVc;

    const-string v1, "FORM"

    sget-object v2, LaGv;->e:LaGv;

    invoke-direct {v0, v1, v7, v2}, LVc;-><init>(Ljava/lang/String;ILaGv;)V

    sput-object v0, LVc;->d:LVc;

    .line 20
    new-instance v0, LVc;

    const-string v1, "PRESENTATION"

    sget-object v2, LaGv;->g:LaGv;

    invoke-direct {v0, v1, v8, v2}, LVc;-><init>(Ljava/lang/String;ILaGv;)V

    sput-object v0, LVc;->e:LVc;

    .line 22
    new-instance v0, LVc;

    const-string v1, "SPREADSHEET"

    const/4 v2, 0x5

    sget-object v3, LaGv;->i:LaGv;

    invoke-direct {v0, v1, v2, v3}, LVc;-><init>(Ljava/lang/String;ILaGv;)V

    sput-object v0, LVc;->f:LVc;

    .line 25
    new-instance v0, LVc;

    const-string v1, "COLLECTION"

    const/4 v2, 0x6

    sget-object v3, LaGv;->a:LaGv;

    invoke-direct {v0, v1, v2, v3}, LVc;-><init>(Ljava/lang/String;ILaGv;)V

    sput-object v0, LVc;->g:LVc;

    .line 27
    new-instance v0, LVc;

    const-string v1, "HOME"

    const/4 v2, 0x7

    sget-object v3, LaGv;->a:LaGv;

    invoke-direct {v0, v1, v2, v3}, LVc;-><init>(Ljava/lang/String;ILaGv;)V

    sput-object v0, LVc;->h:LVc;

    .line 30
    new-instance v0, LVc;

    const-string v1, "LEAF"

    const/16 v2, 0x8

    sget-object v3, LaGv;->k:LaGv;

    invoke-direct {v0, v1, v2, v3}, LVc;-><init>(Ljava/lang/String;ILaGv;)V

    sput-object v0, LVc;->i:LVc;

    .line 32
    new-instance v0, LVc;

    const-string v1, "OPEN"

    const/16 v2, 0x9

    sget-object v3, LaGv;->k:LaGv;

    invoke-direct {v0, v1, v2, v3}, LVc;-><init>(Ljava/lang/String;ILaGv;)V

    sput-object v0, LVc;->j:LVc;

    .line 34
    new-instance v0, LVc;

    const-string v1, "VIEWER"

    const/16 v2, 0xa

    sget-object v3, LaGv;->k:LaGv;

    invoke-direct {v0, v1, v2, v3}, LVc;-><init>(Ljava/lang/String;ILaGv;)V

    sput-object v0, LVc;->k:LVc;

    .line 36
    new-instance v0, LVc;

    const-string v1, "UNDETERMINED"

    const/16 v2, 0xb

    sget-object v3, LaGv;->k:LaGv;

    invoke-direct {v0, v1, v2, v3}, LVc;-><init>(Ljava/lang/String;ILaGv;)V

    sput-object v0, LVc;->l:LVc;

    .line 10
    const/16 v0, 0xc

    new-array v0, v0, [LVc;

    sget-object v1, LVc;->a:LVc;

    aput-object v1, v0, v4

    sget-object v1, LVc;->b:LVc;

    aput-object v1, v0, v5

    sget-object v1, LVc;->c:LVc;

    aput-object v1, v0, v6

    sget-object v1, LVc;->d:LVc;

    aput-object v1, v0, v7

    sget-object v1, LVc;->e:LVc;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LVc;->f:LVc;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LVc;->g:LVc;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LVc;->h:LVc;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LVc;->i:LVc;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LVc;->j:LVc;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LVc;->k:LVc;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LVc;->l:LVc;

    aput-object v2, v0, v1

    sput-object v0, LVc;->a:[LVc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaGv;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGv;",
            ")V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput-object p3, p0, LVc;->a:LaGv;

    .line 42
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LVc;
    .locals 1

    .prologue
    .line 10
    const-class v0, LVc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LVc;

    return-object v0
.end method

.method public static values()[LVc;
    .locals 1

    .prologue
    .line 10
    sget-object v0, LVc;->a:[LVc;

    invoke-virtual {v0}, [LVc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LVc;

    return-object v0
.end method


# virtual methods
.method public a()LaGv;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, LVc;->a:LaGv;

    return-object v0
.end method
