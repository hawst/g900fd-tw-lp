.class public final LGu;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field public A:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laja",
            "<",
            "LFR;",
            ">;>;"
        }
    .end annotation
.end field

.field public B:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;",
            "Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;",
            ">;>;"
        }
    .end annotation
.end field

.field public C:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;",
            ">;"
        }
    .end annotation
.end field

.field public D:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;",
            "Lcom/google/android/apps/docs/doclist/documentopener/PdfExportDocumentOpener;",
            ">;>;"
        }
    .end annotation
.end field

.field public E:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaHL;",
            ">;"
        }
    .end annotation
.end field

.field public F:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laja",
            "<",
            "LFR;",
            ">;>;"
        }
    .end annotation
.end field

.field public G:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;",
            ">;"
        }
    .end annotation
.end field

.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LFX;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LFR;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LGA;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LGF;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LFL;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/PdfExportDocumentOpener;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/ForcePreventOpener;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/BlobFileExportDocumentOpener;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LGh;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LGO;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laja",
            "<",
            "LGt;",
            ">;>;"
        }
    .end annotation
.end field

.field public m:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LGE;",
            ">;"
        }
    .end annotation
.end field

.field public o:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laja",
            "<",
            "LGF;",
            ">;>;"
        }
    .end annotation
.end field

.field public p:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/PunchPreviewDocumentOpener;",
            ">;"
        }
    .end annotation
.end field

.field public q:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;",
            ">;"
        }
    .end annotation
.end field

.field public r:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LGv;",
            ">;"
        }
    .end annotation
.end field

.field public s:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LGt;",
            ">;"
        }
    .end annotation
.end field

.field public t:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LGi;",
            ">;"
        }
    .end annotation
.end field

.field public u:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LGC;",
            ">;"
        }
    .end annotation
.end field

.field public v:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LGd;",
            ">;"
        }
    .end annotation
.end field

.field public w:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LGa;",
            ">;"
        }
    .end annotation
.end field

.field public x:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lajw",
            "<",
            "LFR;",
            ">;>;"
        }
    .end annotation
.end field

.field public y:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LFR;",
            ">;"
        }
    .end annotation
.end field

.field public z:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lajw",
            "<",
            "LFR;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 69
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 70
    iput-object p1, p0, LGu;->a:LbrA;

    .line 71
    const-class v0, LFX;

    invoke-static {v0, v3}, LGu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->a:Lbsk;

    .line 74
    const-class v0, LFR;

    new-instance v1, Lbwl;

    const-string v2, "DefaultRemote"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    .line 75
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 74
    invoke-static {v0, v3}, LGu;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->b:Lbsk;

    .line 77
    const-class v0, LGA;

    invoke-static {v0, v3}, LGu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->c:Lbsk;

    .line 80
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    invoke-static {v0, v3}, LGu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->d:Lbsk;

    .line 83
    const-class v0, LGF;

    invoke-static {v0, v3}, LGu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->e:Lbsk;

    .line 86
    const-class v0, LFL;

    invoke-static {v0, v3}, LGu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->f:Lbsk;

    .line 89
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/PdfExportDocumentOpener;

    invoke-static {v0, v3}, LGu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->g:Lbsk;

    .line 92
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/ForcePreventOpener;

    invoke-static {v0, v3}, LGu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->h:Lbsk;

    .line 95
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/BlobFileExportDocumentOpener;

    invoke-static {v0, v3}, LGu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->i:Lbsk;

    .line 98
    const-class v0, LGh;

    invoke-static {v0, v3}, LGu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->j:Lbsk;

    .line 101
    const-class v0, LGO;

    invoke-static {v0, v3}, LGu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->k:Lbsk;

    .line 104
    const-class v0, Laja;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LGt;

    aput-object v2, v1, v4

    .line 105
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    invoke-static {v0}, Lbuv;->a(LbuP;)Lbuv;

    move-result-object v0

    .line 104
    invoke-static {v0, v3}, LGu;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->l:Lbsk;

    .line 107
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;

    invoke-static {v0, v3}, LGu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->m:Lbsk;

    .line 110
    const-class v0, LGE;

    invoke-static {v0, v3}, LGu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->n:Lbsk;

    .line 113
    const-class v0, Laja;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LGF;

    aput-object v2, v1, v4

    .line 114
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    invoke-static {v0}, Lbuv;->a(LbuP;)Lbuv;

    move-result-object v0

    .line 113
    invoke-static {v0, v3}, LGu;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->o:Lbsk;

    .line 116
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/PunchPreviewDocumentOpener;

    invoke-static {v0, v3}, LGu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->p:Lbsk;

    .line 119
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;

    invoke-static {v0, v3}, LGu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->q:Lbsk;

    .line 122
    const-class v0, LGv;

    invoke-static {v0, v3}, LGu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->r:Lbsk;

    .line 125
    const-class v0, LGt;

    invoke-static {v0, v3}, LGu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->s:Lbsk;

    .line 128
    const-class v0, LGi;

    invoke-static {v0, v3}, LGu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->t:Lbsk;

    .line 131
    const-class v0, LGC;

    invoke-static {v0, v3}, LGu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->u:Lbsk;

    .line 134
    const-class v0, LGd;

    invoke-static {v0, v3}, LGu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->v:Lbsk;

    .line 137
    const-class v0, LGa;

    invoke-static {v0, v3}, LGu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->w:Lbsk;

    .line 140
    const-class v0, Lajw;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LFR;

    aput-object v2, v1, v4

    .line 141
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "DefaultLocal"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 140
    invoke-static {v0, v3}, LGu;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->x:Lbsk;

    .line 143
    const-class v0, LFR;

    new-instance v1, Lbwl;

    const-string v2, "DefaultLocal"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    .line 144
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 143
    invoke-static {v0, v3}, LGu;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->y:Lbsk;

    .line 146
    const-class v0, Lajw;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LFR;

    aput-object v2, v1, v4

    .line 147
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "DefaultRemote"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 146
    invoke-static {v0, v3}, LGu;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->z:Lbsk;

    .line 149
    const-class v0, Laja;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LFR;

    aput-object v2, v1, v4

    .line 150
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "DefaultRemote"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 149
    invoke-static {v0, v3}, LGu;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->A:Lbsk;

    .line 152
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;

    new-array v1, v6, [Ljava/lang/reflect/Type;

    const-class v2, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;

    aput-object v2, v1, v4

    const-class v2, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;

    aput-object v2, v1, v5

    .line 153
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    invoke-static {v0}, Lbuv;->a(LbuP;)Lbuv;

    move-result-object v0

    .line 152
    invoke-static {v0, v3}, LGu;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->B:Lbsk;

    .line 155
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;

    invoke-static {v0, v3}, LGu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->C:Lbsk;

    .line 158
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;

    new-array v1, v6, [Ljava/lang/reflect/Type;

    const-class v2, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;

    aput-object v2, v1, v4

    const-class v2, Lcom/google/android/apps/docs/doclist/documentopener/PdfExportDocumentOpener;

    aput-object v2, v1, v5

    .line 159
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    invoke-static {v0}, Lbuv;->a(LbuP;)Lbuv;

    move-result-object v0

    .line 158
    invoke-static {v0, v3}, LGu;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->D:Lbsk;

    .line 161
    const-class v0, LaHL;

    invoke-static {v0, v3}, LGu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->E:Lbsk;

    .line 164
    const-class v0, Laja;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LFR;

    aput-object v2, v1, v4

    .line 165
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "DefaultLocal"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 164
    invoke-static {v0, v3}, LGu;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->F:Lbsk;

    .line 167
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

    invoke-static {v0, v3}, LGu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LGu;->G:Lbsk;

    .line 170
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 461
    sparse-switch p1, :sswitch_data_0

    .line 901
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 463
    :sswitch_0
    new-instance v3, LGA;

    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->M:Lbsk;

    .line 466
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->M:Lbsk;

    .line 464
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LGu;

    iget-object v1, v1, LGu;->l:Lbsk;

    .line 472
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LGu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LGu;

    iget-object v2, v2, LGu;->l:Lbsk;

    .line 470
    invoke-static {v1, v2}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laja;

    iget-object v2, p0, LGu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->J:Lbsk;

    .line 478
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LGu;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lajo;

    iget-object v4, v4, Lajo;->J:Lbsk;

    .line 476
    invoke-static {v2, v4}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Laja;

    invoke-direct {v3, v0, v1, v2}, LGA;-><init>(Laja;Laja;Laja;)V

    move-object v0, v3

    .line 899
    :goto_0
    return-object v0

    .line 485
    :sswitch_1
    new-instance v0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->d:Lbsk;

    .line 488
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LGu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LpG;

    iget-object v2, v2, LpG;->d:Lbsk;

    .line 486
    invoke-static {v1, v2}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lrm;

    iget-object v2, p0, LGu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Laic;

    iget-object v2, v2, Laic;->e:Lbsk;

    .line 494
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LGu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Laic;

    iget-object v3, v3, Laic;->e:Lbsk;

    .line 492
    invoke-static {v2, v3}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LagG;

    iget-object v3, p0, LGu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaGH;

    iget-object v3, v3, LaGH;->l:Lbsk;

    .line 500
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LGu;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaGH;

    iget-object v4, v4, LaGH;->l:Lbsk;

    .line 498
    invoke-static {v3, v4}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaGM;

    iget-object v4, p0, LGu;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LqD;

    iget-object v4, v4, LqD;->c:Lbsk;

    .line 506
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LGu;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LqD;

    iget-object v5, v5, LqD;->c:Lbsk;

    .line 504
    invoke-static {v4, v5}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LqK;

    iget-object v5, p0, LGu;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LGu;

    iget-object v5, v5, LGu;->y:Lbsk;

    .line 512
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LGu;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LGu;

    iget-object v6, v6, LGu;->y:Lbsk;

    .line 510
    invoke-static {v5, v6}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LFR;

    iget-object v6, p0, LGu;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LadM;

    iget-object v6, v6, LadM;->l:Lbsk;

    .line 518
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LGu;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LadM;

    iget-object v7, v7, LadM;->l:Lbsk;

    .line 516
    invoke-static {v6, v7}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ladb;

    iget-object v7, p0, LGu;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LGu;

    iget-object v7, v7, LGu;->w:Lbsk;

    .line 524
    invoke-virtual {v7}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v7

    iget-object v8, p0, LGu;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LGu;

    iget-object v8, v8, LGu;->w:Lbsk;

    .line 522
    invoke-static {v7, v8}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LGa;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;-><init>(Lrm;LagG;LaGM;LqK;LFR;Ladb;LGa;)V

    goto/16 :goto_0

    .line 531
    :sswitch_2
    new-instance v1, LGF;

    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 534
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LGu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->a:Lbsk;

    .line 532
    invoke-static {v0, v2}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LGF;-><init>(Landroid/content/Context;)V

    move-object v0, v1

    .line 539
    goto/16 :goto_0

    .line 541
    :sswitch_3
    new-instance v1, LFL;

    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 544
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LGu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->a:Lbsk;

    .line 542
    invoke-static {v0, v2}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LFL;-><init>(Landroid/content/Context;)V

    move-object v0, v1

    .line 549
    goto/16 :goto_0

    .line 551
    :sswitch_4
    new-instance v2, Lcom/google/android/apps/docs/doclist/documentopener/PdfExportDocumentOpener;

    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LHc;

    iget-object v0, v0, LHc;->a:Lbsk;

    .line 554
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LHc;

    iget-object v1, v1, LHc;->a:Lbsk;

    .line 552
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHm;

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LGu;

    iget-object v1, v1, LGu;->v:Lbsk;

    .line 560
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LGu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LGu;

    iget-object v3, v3, LGu;->v:Lbsk;

    .line 558
    invoke-static {v1, v3}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LGd;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/docs/doclist/documentopener/PdfExportDocumentOpener;-><init>(LHm;LGd;)V

    move-object v0, v2

    .line 565
    goto/16 :goto_0

    .line 567
    :sswitch_5
    new-instance v0, Lcom/google/android/apps/docs/doclist/documentopener/ForcePreventOpener;

    invoke-direct {v0}, Lcom/google/android/apps/docs/doclist/documentopener/ForcePreventOpener;-><init>()V

    goto/16 :goto_0

    .line 571
    :sswitch_6
    new-instance v2, Lcom/google/android/apps/docs/doclist/documentopener/BlobFileExportDocumentOpener;

    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LHc;

    iget-object v0, v0, LHc;->a:Lbsk;

    .line 574
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LHc;

    iget-object v1, v1, LHc;->a:Lbsk;

    .line 572
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHm;

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LGu;

    iget-object v1, v1, LGu;->v:Lbsk;

    .line 580
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LGu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LGu;

    iget-object v3, v3, LGu;->v:Lbsk;

    .line 578
    invoke-static {v1, v3}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LGd;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/docs/doclist/documentopener/BlobFileExportDocumentOpener;-><init>(LHm;LGd;)V

    move-object v0, v2

    .line 585
    goto/16 :goto_0

    .line 587
    :sswitch_7
    new-instance v0, LGh;

    invoke-direct {v0}, LGh;-><init>()V

    .line 589
    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LGu;

    .line 590
    invoke-virtual {v1, v0}, LGu;->a(LGh;)V

    goto/16 :goto_0

    .line 593
    :sswitch_8
    new-instance v1, LGO;

    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 596
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LGu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->a:Lbsk;

    .line 594
    invoke-static {v0, v2}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LGO;-><init>(Landroid/content/Context;)V

    move-object v0, v1

    .line 601
    goto/16 :goto_0

    .line 603
    :sswitch_9
    new-instance v2, Laja;

    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LGu;

    iget-object v0, v0, LGu;->s:Lbsk;

    .line 604
    invoke-static {v0}, LGu;->a(LbuE;)LbuE;

    move-result-object v3

    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->a:Lbsk;

    .line 611
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->a:Lbsk;

    .line 609
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiU;

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->c:Lbsk;

    .line 617
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v4, p0, LGu;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lajo;

    iget-object v4, v4, Lajo;->c:Lbsk;

    .line 615
    invoke-static {v1, v4}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaiW;

    invoke-direct {v2, v3, v0, v1}, Laja;-><init>(Lbxw;LaiU;LaiW;)V

    move-object v0, v2

    .line 622
    goto/16 :goto_0

    .line 624
    :sswitch_a
    new-instance v3, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;

    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->t:Lbsk;

    .line 627
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->t:Lbsk;

    .line 625
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 633
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LGu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 631
    invoke-static {v1, v2}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LQr;

    iget-object v2, p0, LGu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaHX;

    iget-object v2, v2, LaHX;->m:Lbsk;

    .line 639
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LGu;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaHX;

    iget-object v4, v4, LaHX;->m:Lbsk;

    .line 637
    invoke-static {v2, v4}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LaIm;

    invoke-direct {v3, v0, v1, v2}, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;-><init>(Laja;LQr;LaIm;)V

    move-object v0, v3

    .line 644
    goto/16 :goto_0

    .line 646
    :sswitch_b
    new-instance v3, LGE;

    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LGu;

    iget-object v0, v0, LGu;->l:Lbsk;

    .line 649
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LGu;

    iget-object v1, v1, LGu;->l:Lbsk;

    .line 647
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->U:Lbsk;

    .line 655
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LGu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->U:Lbsk;

    .line 653
    invoke-static {v1, v2}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laja;

    iget-object v2, p0, LGu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LGu;

    iget-object v2, v2, LGu;->o:Lbsk;

    .line 661
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LGu;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LGu;

    iget-object v4, v4, LGu;->o:Lbsk;

    .line 659
    invoke-static {v2, v4}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Laja;

    invoke-direct {v3, v0, v1, v2}, LGE;-><init>(Laja;Laja;Laja;)V

    move-object v0, v3

    .line 666
    goto/16 :goto_0

    .line 668
    :sswitch_c
    new-instance v2, Laja;

    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LGu;

    iget-object v0, v0, LGu;->e:Lbsk;

    .line 669
    invoke-static {v0}, LGu;->a(LbuE;)LbuE;

    move-result-object v3

    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->a:Lbsk;

    .line 676
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->a:Lbsk;

    .line 674
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiU;

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->c:Lbsk;

    .line 682
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v4, p0, LGu;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lajo;

    iget-object v4, v4, Lajo;->c:Lbsk;

    .line 680
    invoke-static {v1, v4}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaiW;

    invoke-direct {v2, v3, v0, v1}, Laja;-><init>(Lbxw;LaiU;LaiW;)V

    move-object v0, v2

    .line 687
    goto/16 :goto_0

    .line 689
    :sswitch_d
    new-instance v0, Lcom/google/android/apps/docs/doclist/documentopener/PunchPreviewDocumentOpener;

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->a:Lbsk;

    .line 692
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LGu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->a:Lbsk;

    .line 690
    invoke-static {v1, v2}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, LGu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 698
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LGu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LQH;

    iget-object v3, v3, LQH;->d:Lbsk;

    .line 696
    invoke-static {v2, v3}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LQr;

    iget-object v3, p0, LGu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LpG;

    iget-object v3, v3, LpG;->m:Lbsk;

    .line 704
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LGu;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LpG;

    iget-object v4, v4, LpG;->m:Lbsk;

    .line 702
    invoke-static {v3, v4}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LtK;

    iget-object v4, p0, LGu;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LalC;

    iget-object v4, v4, LalC;->e:Lbsk;

    .line 710
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LGu;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LalC;

    iget-object v5, v5, LalC;->e:Lbsk;

    .line 708
    invoke-static {v4, v5}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lala;

    iget-object v5, p0, LGu;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Lyh;

    iget-object v5, v5, Lyh;->k:Lbsk;

    .line 716
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LGu;->a:LbrA;

    iget-object v6, v6, LbrA;->a:Lyh;

    iget-object v6, v6, Lyh;->k:Lbsk;

    .line 714
    invoke-static {v5, v6}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lye;

    iget-object v6, p0, LGu;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LGu;

    iget-object v6, v6, LGu;->h:Lbsk;

    .line 722
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LGu;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LGu;

    iget-object v7, v7, LGu;->h:Lbsk;

    .line 720
    invoke-static {v6, v7}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/docs/doclist/documentopener/ForcePreventOpener;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/docs/doclist/documentopener/PunchPreviewDocumentOpener;-><init>(Landroid/content/Context;LQr;LtK;Lala;Lye;Lcom/google/android/apps/docs/doclist/documentopener/ForcePreventOpener;)V

    goto/16 :goto_0

    .line 729
    :sswitch_e
    new-instance v4, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;

    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 732
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->a:Lbsk;

    .line 730
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LadM;

    iget-object v1, v1, LadM;->l:Lbsk;

    .line 738
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LGu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LadM;

    iget-object v2, v2, LadM;->l:Lbsk;

    .line 736
    invoke-static {v1, v2}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ladb;

    iget-object v2, p0, LGu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LqD;

    iget-object v2, v2, LqD;->c:Lbsk;

    .line 744
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LGu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LqD;

    iget-object v3, v3, LqD;->c:Lbsk;

    .line 742
    invoke-static {v2, v3}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LqK;

    iget-object v3, p0, LGu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LGu;

    iget-object v3, v3, LGu;->G:Lbsk;

    .line 750
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, LGu;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LGu;

    iget-object v5, v5, LGu;->G:Lbsk;

    .line 748
    invoke-static {v3, v5}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;-><init>(Landroid/content/Context;Ladb;LqK;Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;)V

    move-object v0, v4

    .line 755
    goto/16 :goto_0

    .line 757
    :sswitch_f
    new-instance v2, LGv;

    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 760
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->a:Lbsk;

    .line 758
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->e:Lbsk;

    .line 766
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LGu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->e:Lbsk;

    .line 764
    invoke-static {v1, v3}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lala;

    invoke-direct {v2, v0, v1}, LGv;-><init>(Landroid/content/Context;Lala;)V

    move-object v0, v2

    .line 771
    goto/16 :goto_0

    .line 773
    :sswitch_10
    new-instance v4, LGt;

    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 776
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->a:Lbsk;

    .line 774
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 782
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LGu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LpG;

    iget-object v2, v2, LpG;->m:Lbsk;

    .line 780
    invoke-static {v1, v2}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LtK;

    iget-object v2, p0, LGu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lyh;

    iget-object v2, v2, Lyh;->k:Lbsk;

    .line 788
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LGu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lyh;

    iget-object v3, v3, Lyh;->k:Lbsk;

    .line 786
    invoke-static {v2, v3}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lye;

    iget-object v3, p0, LGu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LGu;

    iget-object v3, v3, LGu;->h:Lbsk;

    .line 794
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, LGu;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LGu;

    iget-object v5, v5, LGu;->h:Lbsk;

    .line 792
    invoke-static {v3, v5}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/docs/doclist/documentopener/ForcePreventOpener;

    invoke-direct {v4, v0, v1, v2, v3}, LGt;-><init>(Landroid/content/Context;LtK;Lye;Lcom/google/android/apps/docs/doclist/documentopener/ForcePreventOpener;)V

    move-object v0, v4

    .line 799
    goto/16 :goto_0

    .line 801
    :sswitch_11
    new-instance v2, LGi;

    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->G:Lbsk;

    .line 804
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->G:Lbsk;

    .line 802
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->aq:Lbsk;

    .line 810
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LGu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lajo;

    iget-object v3, v3, Lajo;->aq:Lbsk;

    .line 808
    invoke-static {v1, v3}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laja;

    invoke-direct {v2, v0, v1}, LGi;-><init>(Laja;Laja;)V

    move-object v0, v2

    .line 815
    goto/16 :goto_0

    .line 817
    :sswitch_12
    new-instance v4, LGC;

    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 820
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->a:Lbsk;

    .line 818
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lyh;

    iget-object v1, v1, Lyh;->k:Lbsk;

    .line 826
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LGu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lyh;

    iget-object v2, v2, Lyh;->k:Lbsk;

    .line 824
    invoke-static {v1, v2}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lye;

    iget-object v2, p0, LGu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LtQ;

    iget-object v2, v2, LtQ;->w:Lbsk;

    .line 832
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LGu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LtQ;

    iget-object v3, v3, LtQ;->w:Lbsk;

    .line 830
    invoke-static {v2, v3}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LsP;

    iget-object v3, p0, LGu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaHX;

    iget-object v3, v3, LaHX;->m:Lbsk;

    .line 838
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, LGu;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LaHX;

    iget-object v5, v5, LaHX;->m:Lbsk;

    .line 836
    invoke-static {v3, v5}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaIm;

    invoke-direct {v4, v0, v1, v2, v3}, LGC;-><init>(Landroid/content/Context;Lye;LsP;LaIm;)V

    move-object v0, v4

    .line 843
    goto/16 :goto_0

    .line 845
    :sswitch_13
    new-instance v0, LGd;

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->d:Lbsk;

    .line 848
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LGu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LpG;

    iget-object v2, v2, LpG;->d:Lbsk;

    .line 846
    invoke-static {v1, v2}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lrm;

    iget-object v2, p0, LGu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Laic;

    iget-object v2, v2, Laic;->e:Lbsk;

    .line 854
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LGu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Laic;

    iget-object v3, v3, Laic;->e:Lbsk;

    .line 852
    invoke-static {v2, v3}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LagG;

    iget-object v3, p0, LGu;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaGH;

    iget-object v3, v3, LaGH;->l:Lbsk;

    .line 860
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LGu;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaGH;

    iget-object v4, v4, LaGH;->l:Lbsk;

    .line 858
    invoke-static {v3, v4}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaGM;

    iget-object v4, p0, LGu;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LqD;

    iget-object v4, v4, LqD;->c:Lbsk;

    .line 866
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LGu;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LqD;

    iget-object v5, v5, LqD;->c:Lbsk;

    .line 864
    invoke-static {v4, v5}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LqK;

    iget-object v5, p0, LGu;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LGu;

    iget-object v5, v5, LGu;->y:Lbsk;

    .line 872
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LGu;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LGu;

    iget-object v6, v6, LGu;->y:Lbsk;

    .line 870
    invoke-static {v5, v6}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LFR;

    iget-object v6, p0, LGu;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LadM;

    iget-object v6, v6, LadM;->l:Lbsk;

    .line 878
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LGu;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LadM;

    iget-object v7, v7, LadM;->l:Lbsk;

    .line 876
    invoke-static {v6, v7}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ladb;

    iget-object v7, p0, LGu;->a:LbrA;

    iget-object v7, v7, LbrA;->a:Lajo;

    iget-object v7, v7, Lajo;->m:Lbsk;

    .line 884
    invoke-virtual {v7}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v7

    iget-object v8, p0, LGu;->a:LbrA;

    iget-object v8, v8, LbrA;->a:Lajo;

    iget-object v8, v8, Lajo;->m:Lbsk;

    .line 882
    invoke-static {v7, v8}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Laja;

    invoke-direct/range {v0 .. v7}, LGd;-><init>(Lrm;LagG;LaGM;LqK;LFR;Ladb;Laja;)V

    goto/16 :goto_0

    .line 891
    :sswitch_14
    new-instance v1, LGa;

    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laek;

    iget-object v0, v0, Laek;->h:Lbsk;

    .line 894
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LGu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Laek;

    iget-object v2, v2, Laek;->h:Lbsk;

    .line 892
    invoke-static {v0, v2}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laer;

    invoke-direct {v1, v0}, LGa;-><init>(Laer;)V

    move-object v0, v1

    .line 899
    goto/16 :goto_0

    .line 461
    nop

    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_14
        0xa7 -> :sswitch_e
        0xac -> :sswitch_12
        0xae -> :sswitch_1
        0xb0 -> :sswitch_13
        0xb1 -> :sswitch_4
        0xb2 -> :sswitch_0
        0xb4 -> :sswitch_9
        0xb7 -> :sswitch_7
        0xbb -> :sswitch_2
        0xbc -> :sswitch_3
        0xbe -> :sswitch_5
        0xbf -> :sswitch_f
        0xc0 -> :sswitch_6
        0xc1 -> :sswitch_8
        0xc3 -> :sswitch_11
        0xc4 -> :sswitch_b
        0xc5 -> :sswitch_10
        0xc6 -> :sswitch_a
        0xcb -> :sswitch_c
        0xcc -> :sswitch_d
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 934
    sparse-switch p2, :sswitch_data_0

    .line 1054
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 936
    :sswitch_0
    check-cast p1, LFN;

    .line 938
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LGu;

    iget-object v0, v0, LGu;->F:Lbsk;

    .line 941
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    .line 938
    invoke-virtual {p1, v0}, LFN;->getLazy2(Laja;)Lajw;

    move-result-object v0

    .line 1047
    :goto_0
    return-object v0

    .line 945
    :sswitch_1
    check-cast p1, LFW;

    .line 947
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LGu;

    iget-object v0, v0, LGu;->q:Lbsk;

    .line 950
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;

    .line 947
    invoke-virtual {p1, v0}, LFW;->provideDocumentOpener(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;)LFR;

    move-result-object v0

    goto :goto_0

    .line 954
    :sswitch_2
    check-cast p1, LFN;

    .line 956
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LGu;

    iget-object v0, v0, LGu;->A:Lbsk;

    .line 959
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    .line 956
    invoke-virtual {p1, v0}, LFN;->getLazy1(Laja;)Lajw;

    move-result-object v0

    goto :goto_0

    .line 963
    :sswitch_3
    check-cast p1, LFN;

    .line 965
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LGu;

    iget-object v2, v0, LGu;->b:Lbsk;

    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->a:Lbsk;

    .line 972
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiU;

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->c:Lbsk;

    .line 976
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaiW;

    .line 965
    invoke-virtual {p1, v2, v0, v1}, LFN;->get1(Lbxw;LaiU;LaiW;)Laja;

    move-result-object v0

    goto :goto_0

    .line 980
    :sswitch_4
    check-cast p1, LGg;

    .line 982
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LGu;

    iget-object v0, v0, LGu;->q:Lbsk;

    .line 985
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LGu;

    iget-object v1, v1, LGu;->u:Lbsk;

    .line 989
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LGC;

    .line 982
    invoke-virtual {p1, v0, v1}, LGg;->provideLocalGDocsAsPdfOpener(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;LGC;)Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;

    move-result-object v0

    goto :goto_0

    .line 993
    :sswitch_5
    check-cast p1, LFW;

    .line 995
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LGu;

    iget-object v0, v0, LGu;->d:Lbsk;

    .line 998
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    .line 995
    invoke-virtual {p1, v0}, LFW;->provideDownloadFileDocumentOpener(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;

    move-result-object v0

    goto :goto_0

    .line 1002
    :sswitch_6
    check-cast p1, LGg;

    .line 1004
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LGu;

    iget-object v0, v0, LGu;->v:Lbsk;

    .line 1007
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LGd;

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LGu;

    iget-object v1, v1, LGu;->g:Lbsk;

    .line 1011
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/docs/doclist/documentopener/PdfExportDocumentOpener;

    iget-object v2, p0, LGu;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LGu;

    iget-object v2, v2, LGu;->u:Lbsk;

    .line 1015
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LGC;

    .line 1004
    invoke-virtual {p1, v0, v1, v2}, LGg;->provideRemoteGDocsAsPdfOpener(LGd;Lcom/google/android/apps/docs/doclist/documentopener/PdfExportDocumentOpener;LGC;)Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;

    move-result-object v0

    goto/16 :goto_0

    .line 1019
    :sswitch_7
    check-cast p1, LFW;

    .line 1021
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaHQ;

    iget-object v0, v0, LaHQ;->a:Lbsk;

    .line 1024
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaHO;

    .line 1021
    invoke-virtual {p1, v0}, LFW;->provideDriveAppSetProvider(LaHO;)LaHL;

    move-result-object v0

    goto/16 :goto_0

    .line 1028
    :sswitch_8
    check-cast p1, LFN;

    .line 1030
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LGu;

    iget-object v2, v0, LGu;->y:Lbsk;

    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->a:Lbsk;

    .line 1037
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiU;

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->c:Lbsk;

    .line 1041
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaiW;

    .line 1030
    invoke-virtual {p1, v2, v0, v1}, LFN;->get2(Lbxw;LaiU;LaiW;)Laja;

    move-result-object v0

    goto/16 :goto_0

    .line 1045
    :sswitch_9
    check-cast p1, LFW;

    .line 1047
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LGu;

    iget-object v0, v0, LGu;->m:Lbsk;

    .line 1050
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;

    .line 1047
    invoke-virtual {p1, v0}, LFW;->provideFileOpenerIntentCreator(Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;)Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

    move-result-object v0

    goto/16 :goto_0

    .line 934
    :sswitch_data_0
    .sparse-switch
        0xa4 -> :sswitch_0
        0xa5 -> :sswitch_8
        0xa6 -> :sswitch_1
        0xa8 -> :sswitch_2
        0xa9 -> :sswitch_3
        0xab -> :sswitch_4
        0xad -> :sswitch_5
        0xaf -> :sswitch_6
        0xc8 -> :sswitch_7
        0xcd -> :sswitch_9
    .end sparse-switch
.end method

.method public a()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 345
    const-class v0, LGh;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x19

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 348
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x1a

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 351
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x1b

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Lbse;->a(LbuP;LbuB;)V

    .line 354
    const-class v0, LFX;

    iget-object v1, p0, LGu;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 355
    const-class v0, LFR;

    new-instance v1, Lbwl;

    const-string v2, "DefaultRemote"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LGu;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 356
    const-class v0, LGA;

    iget-object v1, p0, LGu;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 357
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    iget-object v1, p0, LGu;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 358
    const-class v0, LGF;

    iget-object v1, p0, LGu;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 359
    const-class v0, LFL;

    iget-object v1, p0, LGu;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 360
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/PdfExportDocumentOpener;

    iget-object v1, p0, LGu;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 361
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/ForcePreventOpener;

    iget-object v1, p0, LGu;->h:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 362
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/BlobFileExportDocumentOpener;

    iget-object v1, p0, LGu;->i:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 363
    const-class v0, LGh;

    iget-object v1, p0, LGu;->j:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 364
    const-class v0, LGO;

    iget-object v1, p0, LGu;->k:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 365
    const-class v0, Laja;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LGt;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    invoke-static {v0}, Lbuv;->a(LbuP;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LGu;->l:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 366
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;

    iget-object v1, p0, LGu;->m:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 367
    const-class v0, LGE;

    iget-object v1, p0, LGu;->n:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 368
    const-class v0, Laja;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LGF;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    invoke-static {v0}, Lbuv;->a(LbuP;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LGu;->o:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 369
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/PunchPreviewDocumentOpener;

    iget-object v1, p0, LGu;->p:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 370
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;

    iget-object v1, p0, LGu;->q:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 371
    const-class v0, LGv;

    iget-object v1, p0, LGu;->r:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 372
    const-class v0, LGt;

    iget-object v1, p0, LGu;->s:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 373
    const-class v0, LGi;

    iget-object v1, p0, LGu;->t:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 374
    const-class v0, LGC;

    iget-object v1, p0, LGu;->u:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 375
    const-class v0, LGd;

    iget-object v1, p0, LGu;->v:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 376
    const-class v0, LGa;

    iget-object v1, p0, LGu;->w:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 377
    const-class v0, Lajw;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LFR;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "DefaultLocal"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LGu;->x:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 378
    const-class v0, LFR;

    new-instance v1, Lbwl;

    const-string v2, "DefaultLocal"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LGu;->y:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 379
    const-class v0, Lajw;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LFR;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "DefaultRemote"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LGu;->z:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 380
    const-class v0, Laja;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LFR;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "DefaultRemote"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LGu;->A:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 381
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;

    aput-object v2, v1, v3

    const-class v2, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    invoke-static {v0}, Lbuv;->a(LbuP;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LGu;->B:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 382
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;

    iget-object v1, p0, LGu;->C:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 383
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;

    aput-object v2, v1, v3

    const-class v2, Lcom/google/android/apps/docs/doclist/documentopener/PdfExportDocumentOpener;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    invoke-static {v0}, Lbuv;->a(LbuP;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LGu;->D:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 384
    const-class v0, LaHL;

    iget-object v1, p0, LGu;->E:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 385
    const-class v0, Laja;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LFR;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "DefaultLocal"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LGu;->F:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Lbuv;Lbsk;)V

    .line 386
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

    iget-object v1, p0, LGu;->G:Lbsk;

    invoke-virtual {p0, v0, v1}, Lbse;->a(Ljava/lang/Class;Lbsk;)V

    .line 387
    iget-object v0, p0, LGu;->a:Lbsk;

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LGu;

    iget-object v1, v1, LGu;->j:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 389
    iget-object v0, p0, LGu;->b:Lbsk;

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LGu;

    iget-object v1, v1, LGu;->r:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 391
    iget-object v0, p0, LGu;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xb2

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 393
    iget-object v0, p0, LGu;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xae

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 395
    iget-object v0, p0, LGu;->e:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xbb

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 397
    iget-object v0, p0, LGu;->f:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xbc

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 399
    iget-object v0, p0, LGu;->g:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xb1

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 401
    iget-object v0, p0, LGu;->h:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xbe

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 403
    iget-object v0, p0, LGu;->i:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xc0

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 405
    iget-object v0, p0, LGu;->j:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xb7

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 407
    iget-object v0, p0, LGu;->k:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xc1

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 409
    iget-object v0, p0, LGu;->l:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xb4

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 411
    iget-object v0, p0, LGu;->m:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xc6

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 413
    iget-object v0, p0, LGu;->n:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xc4

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 415
    iget-object v0, p0, LGu;->o:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xcb

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 417
    iget-object v0, p0, LGu;->p:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xcc

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 419
    iget-object v0, p0, LGu;->q:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xa7

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 421
    iget-object v0, p0, LGu;->r:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xbf

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 423
    iget-object v0, p0, LGu;->s:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xc5

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 425
    iget-object v0, p0, LGu;->t:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xc3

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 427
    iget-object v0, p0, LGu;->u:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xac

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 429
    iget-object v0, p0, LGu;->v:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xb0

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 431
    iget-object v0, p0, LGu;->w:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x65

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 433
    iget-object v0, p0, LGu;->x:Lbsk;

    const-class v1, LFN;

    const/16 v2, 0xa4

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 435
    iget-object v0, p0, LGu;->y:Lbsk;

    const-class v1, LFW;

    const/16 v2, 0xa6

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 437
    iget-object v0, p0, LGu;->z:Lbsk;

    const-class v1, LFN;

    const/16 v2, 0xa8

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 439
    iget-object v0, p0, LGu;->A:Lbsk;

    const-class v1, LFN;

    const/16 v2, 0xa9

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 441
    iget-object v0, p0, LGu;->B:Lbsk;

    const-class v1, LGg;

    const/16 v2, 0xab

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 443
    iget-object v0, p0, LGu;->C:Lbsk;

    const-class v1, LFW;

    const/16 v2, 0xad

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 445
    iget-object v0, p0, LGu;->D:Lbsk;

    const-class v1, LGg;

    const/16 v2, 0xaf

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 447
    iget-object v0, p0, LGu;->E:Lbsk;

    const-class v1, LFW;

    const/16 v2, 0xc8

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 449
    iget-object v0, p0, LGu;->F:Lbsk;

    const-class v1, LFN;

    const/16 v2, 0xa5

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 451
    iget-object v0, p0, LGu;->G:Lbsk;

    const-class v1, LFW;

    const/16 v2, 0xcd

    invoke-virtual {p0, v1, v2}, Lbse;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 453
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 908
    packed-switch p1, :pswitch_data_0

    .line 928
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 910
    :pswitch_0
    check-cast p2, LGh;

    .line 912
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LGu;

    .line 913
    invoke-virtual {v0, p2}, LGu;->a(LGh;)V

    .line 930
    :goto_0
    return-void

    .line 916
    :pswitch_1
    check-cast p2, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;

    .line 918
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LGu;

    .line 919
    invoke-virtual {v0, p2}, LGu;->a(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;)V

    goto :goto_0

    .line 922
    :pswitch_2
    check-cast p2, Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;

    .line 924
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LGu;

    .line 925
    invoke-virtual {v0, p2}, LGu;->a(Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;)V

    goto :goto_0

    .line 908
    :pswitch_data_0
    .packed-switch 0x19
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(LGh;)V
    .locals 2

    .prologue
    .line 176
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->L:Lbsk;

    .line 179
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->L:Lbsk;

    .line 177
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKR;

    iput-object v0, p1, LGh;->a:LaKR;

    .line 183
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LGu;

    iget-object v0, v0, LGu;->b:Lbsk;

    .line 186
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LGu;

    iget-object v1, v1, LGu;->b:Lbsk;

    .line 184
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LFR;

    iput-object v0, p1, LGh;->a:LFR;

    .line 190
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LadM;

    iget-object v0, v0, LadM;->l:Lbsk;

    .line 193
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LadM;

    iget-object v1, v1, LadM;->l:Lbsk;

    .line 191
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladb;

    iput-object v0, p1, LGh;->a:Ladb;

    .line 197
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LHc;

    iget-object v0, v0, LHc;->b:Lbsk;

    .line 200
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LHc;

    iget-object v1, v1, LHc;->b:Lbsk;

    .line 198
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;

    iput-object v0, p1, LGh;->a:Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;

    .line 204
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LGu;

    iget-object v0, v0, LGu;->y:Lbsk;

    .line 207
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LGu;

    iget-object v1, v1, LGu;->y:Lbsk;

    .line 205
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LFR;

    iput-object v0, p1, LGh;->b:LFR;

    .line 211
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LGu;

    iget-object v0, v0, LGu;->k:Lbsk;

    .line 214
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LGu;

    iget-object v1, v1, LGu;->k:Lbsk;

    .line 212
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LGO;

    iput-object v0, p1, LGh;->a:LGO;

    .line 218
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LGu;

    iget-object v0, v0, LGu;->f:Lbsk;

    .line 221
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LGu;

    iget-object v1, v1, LGu;->f:Lbsk;

    .line 219
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LFL;

    iput-object v0, p1, LGh;->a:LFL;

    .line 225
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LGu;

    iget-object v0, v0, LGu;->t:Lbsk;

    .line 228
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LGu;

    iget-object v1, v1, LGu;->t:Lbsk;

    .line 226
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LGi;

    iput-object v0, p1, LGh;->a:LGi;

    .line 232
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->y:Lbsk;

    .line 235
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->y:Lbsk;

    .line 233
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lald;

    iput-object v0, p1, LGh;->a:Lald;

    .line 239
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LGu;

    iget-object v0, v0, LGu;->n:Lbsk;

    .line 242
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LGu;

    iget-object v1, v1, LGu;->n:Lbsk;

    .line 240
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LGE;

    iput-object v0, p1, LGh;->a:LGE;

    .line 246
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->b:Lbsk;

    .line 249
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->b:Lbsk;

    .line 247
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p1, LGh;->a:Landroid/content/Context;

    .line 253
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 256
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 254
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p1, LGh;->a:LtK;

    .line 260
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LGu;

    iget-object v0, v0, LGu;->c:Lbsk;

    .line 263
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LGu;

    iget-object v1, v1, LGu;->c:Lbsk;

    .line 261
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LGA;

    iput-object v0, p1, LGh;->a:LGA;

    .line 267
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;)V
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LqD;

    iget-object v0, v0, LqD;->c:Lbsk;

    .line 336
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LqD;

    iget-object v1, v1, LqD;->c:Lbsk;

    .line 334
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqK;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;->a:LqK;

    .line 340
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;)V
    .locals 2

    .prologue
    .line 271
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 272
    invoke-virtual {v0, p1}, LtQ;->a(Lrm;)V

    .line 273
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->y:Lbsk;

    .line 276
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->y:Lbsk;

    .line 274
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lald;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lald;

    .line 280
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LUB;

    iget-object v0, v0, LUB;->f:Lbsk;

    .line 283
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LUB;

    iget-object v1, v1, LUB;->f:Lbsk;

    .line 281
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUT;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LUT;

    .line 287
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 290
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 288
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LaGM;

    .line 294
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->l:Lbsk;

    .line 297
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->l:Lbsk;

    .line 295
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Ljava/lang/Class;

    .line 301
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->i:Lbsk;

    .line 304
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->i:Lbsk;

    .line 302
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTO;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LTO;

    .line 308
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->k:Lbsk;

    .line 311
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->k:Lbsk;

    .line 309
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTd;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LTd;

    .line 315
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 318
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 316
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LQr;

    .line 322
    iget-object v0, p0, LGu;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 325
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LGu;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 323
    invoke-static {v0, v1}, LGu;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p1, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LtK;

    .line 329
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 457
    return-void
.end method
