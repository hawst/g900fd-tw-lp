.class public LbhZ;
.super Lcom/google/bionics/scanner/unveil/util/Picture;
.source "PictureFactory.java"


# instance fields
.field private final a:Lcom/google/bionics/scanner/unveil/util/Picture;

.field private final a:Lcom/google/bionics/scanner/unveil/util/PictureTracker;


# direct methods
.method public constructor <init>(Lcom/google/bionics/scanner/unveil/util/Picture;)V
    .locals 2

    .prologue
    .line 207
    invoke-virtual {p1}, Lcom/google/bionics/scanner/unveil/util/Picture;->getOrientation()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/bionics/scanner/unveil/util/Picture;->getSource()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/bionics/scanner/unveil/util/Picture;-><init>(II)V

    .line 204
    invoke-static {}, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->getInstance()Lcom/google/bionics/scanner/unveil/util/PictureTracker;

    move-result-object v0

    iput-object v0, p0, LbhZ;->a:Lcom/google/bionics/scanner/unveil/util/PictureTracker;

    .line 208
    iput-object p1, p0, LbhZ;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    .line 209
    iget-object v0, p0, LbhZ;->a:Lcom/google/bionics/scanner/unveil/util/PictureTracker;

    invoke-virtual {v0, p0, p1}, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->track(Lcom/google/bionics/scanner/unveil/util/Picture;Lcom/google/bionics/scanner/unveil/util/Picture;)V

    .line 210
    return-void
.end method


# virtual methods
.method public getByteSize()I
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, LbhZ;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/util/Picture;->getByteSize()I

    move-result v0

    return v0
.end method

.method public getCropArea()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, LbhZ;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/util/Picture;->getCropArea()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getCroppedPicture()Lcom/google/bionics/scanner/unveil/util/Picture;
    .locals 3

    .prologue
    .line 257
    iget-object v0, p0, LbhZ;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/util/Picture;->getCroppedPicture()Lcom/google/bionics/scanner/unveil/util/Picture;

    move-result-object v1

    .line 258
    iget-object v0, p0, LbhZ;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    if-ne v1, v0, :cond_0

    .line 263
    :goto_0
    return-object p0

    .line 261
    :cond_0
    new-instance v0, LbhZ;

    invoke-direct {v0, v1}, LbhZ;-><init>(Lcom/google/bionics/scanner/unveil/util/Picture;)V

    .line 262
    iget-object v2, p0, LbhZ;->a:Lcom/google/bionics/scanner/unveil/util/PictureTracker;

    invoke-virtual {v2, v0, v1}, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->track(Lcom/google/bionics/scanner/unveil/util/Picture;Lcom/google/bionics/scanner/unveil/util/Picture;)V

    move-object p0, v0

    .line 263
    goto :goto_0
.end method

.method public getDrawable()Landroid/graphics/drawable/BitmapDrawable;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, LbhZ;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/util/Picture;->getDrawable()Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 213
    iget-object v0, p0, LbhZ;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/util/Picture;->getId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getJpegData()[B
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, LbhZ;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/util/Picture;->getJpegData()[B

    move-result-object v0

    return-object v0
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, LbhZ;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/util/Picture;->getOrientation()I

    move-result v0

    return v0
.end method

.method public getSize()Lcom/google/bionics/scanner/unveil/util/Size;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, LbhZ;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/util/Picture;->getSize()Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v0

    return-object v0
.end method

.method public getYuvData()[B
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, LbhZ;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/util/Picture;->getYuvData()[B

    move-result-object v0

    return-object v0
.end method

.method public peekBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, LbhZ;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/util/Picture;->peekBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public peekBitmap(Landroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, LbhZ;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    invoke-virtual {v0, p1}, Lcom/google/bionics/scanner/unveil/util/Picture;->peekBitmap(Landroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public recycle()V
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, LbhZ;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/util/Picture;->recycle()V

    .line 272
    return-void
.end method

.method public setCropArea(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, LbhZ;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    invoke-virtual {v0, p1}, Lcom/google/bionics/scanner/unveil/util/Picture;->setCropArea(Landroid/graphics/Rect;)V

    .line 218
    return-void
.end method

.method public setOrientation(I)V
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, LbhZ;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    invoke-virtual {v0, p1}, Lcom/google/bionics/scanner/unveil/util/Picture;->setOrientation(I)V

    .line 226
    return-void
.end method
