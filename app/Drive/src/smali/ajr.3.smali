.class public Lajr;
.super LH;
.source "GuiceFragmentActivity.java"

# interfaces
.implements Laju;


# instance fields
.field private a:LaiN;

.field protected a:LaiU;

.field private a:Lbuu;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, LH;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lajr;->a:LaiN;

    return-void
.end method


# virtual methods
.method public a()Lbuu;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lajr;->a:Lbuu;

    invoke-static {v0}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbuu;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1}, Lajr;->b(Landroid/os/Bundle;)V

    .line 30
    iget-object v0, p0, Lajr;->a:Lbuu;

    invoke-interface {v0, p0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 31
    invoke-virtual {p0}, Lajr;->h()V

    .line 32
    return-void
.end method

.method protected b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 35
    invoke-static {p0}, LaiZ;->b(Landroid/content/Context;)LaiY;

    move-result-object v0

    iput-object v0, p0, Lajr;->a:Lbuu;

    .line 36
    iget-object v0, p0, Lajr;->a:Lbuu;

    const-class v1, LaiG;

    invoke-interface {v0, v1}, Lbuu;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiG;

    .line 37
    invoke-interface {v0, p0}, LaiG;->a(Landroid/app/Activity;)LaiN;

    move-result-object v0

    iput-object v0, p0, Lajr;->a:LaiN;

    .line 38
    iget-object v0, p0, Lajr;->a:LaiN;

    invoke-interface {v0, p1}, LaiN;->a(Landroid/os/Bundle;)V

    .line 39
    iget-object v0, p0, Lajr;->a:Lbuu;

    const-class v1, LaiU;

    invoke-interface {v0, v1}, Lbuu;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiU;

    iput-object v0, p0, Lajr;->a:LaiU;

    .line 40
    return-void
.end method

.method protected h()V
    .locals 0

    .prologue
    .line 57
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lajr;->a(Landroid/os/Bundle;)V

    .line 25
    invoke-super {p0, p1}, LH;->onCreate(Landroid/os/Bundle;)V

    .line 26
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 69
    invoke-super {p0}, LH;->onDestroy()V

    .line 70
    iget-object v0, p0, Lajr;->a:LaiN;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lajr;->a:LaiN;

    invoke-interface {v0}, LaiN;->b()V

    .line 76
    :cond_0
    return-void
.end method

.method public onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 44
    invoke-super {p0, p1}, LH;->onPostCreate(Landroid/os/Bundle;)V

    .line 45
    iget-object v0, p0, Lajr;->a:LaiN;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lajr;->a:LaiN;

    invoke-interface {v0}, LaiN;->a()V

    .line 48
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0, p1}, LH;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 62
    iget-object v0, p0, Lajr;->a:LaiN;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lajr;->a:LaiN;

    invoke-interface {v0, p1}, LaiN;->b(Landroid/os/Bundle;)V

    .line 65
    :cond_0
    return-void
.end method
