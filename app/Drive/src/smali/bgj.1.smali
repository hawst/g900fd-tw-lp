.class public Lbgj;
.super Landroid/view/OrientationEventListener;
.source "BaseActivity.java"


# instance fields
.field final synthetic a:Lbgg;

.field private final a:[Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;


# direct methods
.method public varargs constructor <init>(Lbgg;Landroid/content/Context;[Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lbgj;->a:Lbgg;

    .line 71
    invoke-direct {p0, p2}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    .line 72
    iput-object p3, p0, Lbgj;->a:[Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    .line 73
    return-void
.end method


# virtual methods
.method public onOrientationChanged(I)V
    .locals 5

    .prologue
    .line 79
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 91
    :cond_0
    return-void

    .line 80
    :cond_1
    iget-object v0, p0, Lbgj;->a:Lbgg;

    iget-object v1, p0, Lbgj;->a:Lbgg;

    invoke-static {v1}, Lbgg;->a(Lbgg;)I

    move-result v1

    invoke-static {p1, v1}, LbgB;->a(II)I

    move-result v1

    invoke-static {v0, v1}, Lbgg;->a(Lbgg;I)I

    .line 84
    iget-object v0, p0, Lbgj;->a:Lbgg;

    invoke-static {v0}, Lbgg;->a(Lbgg;)I

    move-result v0

    iget-object v1, p0, Lbgj;->a:Lbgg;

    invoke-static {v1}, LbgB;->a(Landroid/app/Activity;)I

    move-result v1

    add-int/2addr v1, v0

    .line 85
    iget-object v0, p0, Lbgj;->a:Lbgg;

    invoke-static {v0}, Lbgg;->b(Lbgg;)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 86
    iget-object v0, p0, Lbgj;->a:Lbgg;

    invoke-static {v0, v1}, Lbgg;->b(Lbgg;I)I

    .line 87
    iget-object v2, p0, Lbgj;->a:[Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 88
    invoke-virtual {v4, v1}, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->setOrientation(I)V

    .line 87
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
