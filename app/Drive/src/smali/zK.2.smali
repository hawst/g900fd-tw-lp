.class public abstract LzK;
.super Ljava/lang/Object;
.source "AbstractDocListViewDelegate.java"

# interfaces
.implements LBJ;


# instance fields
.field private final a:LKs;

.field protected final a:LaGM;

.field protected final a:Landroid/widget/ListView;

.field protected final a:LarF;

.field protected final a:Lcom/google/android/apps/docs/view/DocListView;

.field protected final a:Lcom/google/android/apps/docs/view/StickyHeaderView;

.field protected final a:LtK;

.field protected final a:Z

.field protected b:Z


# direct methods
.method protected constructor <init>(LKs;LaGM;LtK;Lcom/google/android/apps/docs/view/DocListView;Landroid/widget/ListView;Lcom/google/android/apps/docs/view/StickyHeaderView;LarF;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, LzK;->a:LKs;

    .line 63
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p0, LzK;->a:LtK;

    .line 64
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p0, LzK;->a:LaGM;

    .line 65
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/DocListView;

    iput-object v0, p0, LzK;->a:Lcom/google/android/apps/docs/view/DocListView;

    .line 66
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, LzK;->a:Landroid/widget/ListView;

    .line 67
    invoke-static {p6}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/StickyHeaderView;

    iput-object v0, p0, LzK;->a:Lcom/google/android/apps/docs/view/StickyHeaderView;

    .line 68
    invoke-static {p7}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LarF;

    iput-object v0, p0, LzK;->a:LarF;

    .line 69
    invoke-interface {p7}, LarF;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lry;->t:Lry;

    .line 70
    invoke-interface {p3, v0}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lry;->aH:Lry;

    .line 71
    invoke-interface {p3, v0}, LtK;->a(LtJ;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LzK;->a:Z

    .line 72
    return-void

    .line 71
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 157
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 158
    invoke-virtual {p0}, LzK;->a()LzZ;

    move-result-object v1

    new-instance v2, LzL;

    invoke-direct {v2, p0, v0}, LzL;-><init>(LzK;Ljava/util/List;)V

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LzZ;->a(LCs;I)V

    .line 165
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 166
    if-lez v1, :cond_1

    .line 167
    const/16 v2, 0x14

    const/16 v3, 0xc8

    div-int v1, v3, v1

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-long v2, v1

    .line 168
    iget-object v1, p0, LzK;->a:LarF;

    invoke-interface {v1, v2, v3}, LarF;->a(J)LarE;

    move-result-object v1

    .line 169
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 170
    invoke-interface {v1, v0}, LarE;->a(Landroid/view/View;)V

    goto :goto_0

    .line 172
    :cond_0
    invoke-interface {v1}, LarE;->a()V

    .line 174
    :cond_1
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, LzK;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    return v0
.end method

.method protected abstract a()LzZ;
.end method

.method public a()V
    .locals 1

    .prologue
    .line 150
    iget-boolean v0, p0, LzK;->b:Z

    if-eqz v0, :cond_0

    .line 151
    invoke-direct {p0}, LzK;->f()V

    .line 152
    const/4 v0, 0x0

    iput-boolean v0, p0, LzK;->b:Z

    .line 154
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, LzK;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setSelection(I)V

    .line 109
    return-void
.end method

.method public a(IZ)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, LzK;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p1, p2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 99
    return-void
.end method

.method public a(LQX;)V
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, LzK;->a:Z

    if-eqz v0, :cond_0

    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, LzK;->b:Z

    .line 79
    :cond_0
    return-void
.end method

.method protected a(LQX;LCq;)V
    .locals 8

    .prologue
    .line 125
    iget-object v0, p0, LzK;->a:LtK;

    sget-object v1, Lry;->ay:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    invoke-virtual {p0, p1}, LzK;->a(LQX;)Z

    move-result v7

    .line 127
    new-instance v0, LBG;

    iget-object v1, p0, LzK;->a:Lcom/google/android/apps/docs/view/DocListView;

    .line 129
    invoke-virtual {v1}, Lcom/google/android/apps/docs/view/DocListView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LzK;->a:LKs;

    .line 131
    invoke-virtual {p1}, LQX;->a()LIK;

    move-result-object v3

    .line 133
    invoke-virtual {p1}, LQX;->a()LzO;

    move-result-object v5

    iget-object v6, p0, LzK;->a:Lcom/google/android/apps/docs/view/DocListView;

    move-object v4, p2

    invoke-direct/range {v0 .. v7}, LBG;-><init>(Landroid/content/Context;LKs;LIK;LCq;LzO;LapF;Z)V

    .line 136
    iget-object v1, p0, LzK;->a:Lcom/google/android/apps/docs/view/DocListView;

    iget-object v2, p0, LzK;->a:Lcom/google/android/apps/docs/view/StickyHeaderView;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/docs/view/DocListView;->setViewModeListener(LapH;)V

    .line 137
    iget-object v1, p0, LzK;->a:Lcom/google/android/apps/docs/view/StickyHeaderView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/view/StickyHeaderView;->setAdapter(LaqN;)V

    .line 138
    iget-object v0, p0, LzK;->a:Lcom/google/android/apps/docs/view/DocListView;

    iget-object v1, p0, LzK;->a:Lcom/google/android/apps/docs/view/StickyHeaderView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/DocListView;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 142
    :goto_0
    return-void

    .line 140
    :cond_0
    iget-object v0, p0, LzK;->a:Lcom/google/android/apps/docs/view/StickyHeaderView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/StickyHeaderView;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(LaFX;)V
    .locals 1

    .prologue
    .line 113
    invoke-virtual {p0}, LzK;->a()LzZ;

    move-result-object v0

    .line 114
    invoke-interface {v0, p1}, LCq;->a(LaFX;)V

    .line 115
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 119
    invoke-virtual {p0}, LzK;->a()LzZ;

    move-result-object v0

    .line 120
    invoke-interface {v0}, LCq;->a()Z

    move-result v0

    return v0
.end method

.method protected a(LQX;)Z
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x0

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, LzK;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 190
    invoke-virtual {p0}, LzK;->a()LzZ;

    move-result-object v0

    .line 191
    if-eqz v0, :cond_0

    .line 192
    invoke-interface {v0}, LCq;->b()V

    .line 194
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 198
    invoke-virtual {p0}, LzK;->a()LzZ;

    move-result-object v0

    .line 199
    invoke-interface {v0}, LCq;->a()V

    .line 200
    return-void
.end method
