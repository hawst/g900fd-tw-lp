.class public LTn;
.super Ljava/lang/Object;
.source "HttpModule.java"

# interfaces
.implements LbuC;


# instance fields
.field private final a:LTc;

.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, LTn;->a:Landroid/content/Context;

    .line 35
    new-instance v0, LTo;

    invoke-direct {v0, p0}, LTo;-><init>(LTn;)V

    iput-object v0, p0, LTn;->a:LTc;

    .line 41
    return-void
.end method

.method static synthetic a(LTn;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, LTn;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/inject/Binder;)V
    .locals 2

    .prologue
    .line 82
    const-class v0, LTO;

    const-class v1, LQJ;

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Lbuv;)LbvX;

    .line 84
    return-void
.end method

.method provideAccountAuthenticator(LTb;)LSZ;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 52
    return-object p1
.end method

.method provideApplicationContextProvider()LTc;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, LTn;->a:LTc;

    return-object v0
.end method

.method provideAuthTokenManager(LTe;)LTd;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 46
    return-object p1
.end method

.method provideAuthenticatedHttpIssuer(LTj;)LTh;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 64
    return-object p1
.end method

.method provideHttpIssuer(LTw;)LTF;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 58
    return-object p1
.end method

.method provideUserAgentStringGenerator(LTQ;)LTO;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 71
    invoke-virtual {p1}, LTQ;->a()LTO;

    move-result-object v0

    return-object v0
.end method
