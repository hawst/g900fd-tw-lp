.class public LqB;
.super Ljava/lang/Object;
.source "ApplicationStartupTrackerImpl.java"

# interfaces
.implements LqA;


# instance fields
.field private final a:J

.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, LqB;->a:Z

    .line 17
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LqB;->a:J

    .line 18
    return-void
.end method


# virtual methods
.method public a(LqK;)V
    .locals 6

    .prologue
    .line 22
    iget-boolean v0, p0, LqB;->a:Z

    if-nez v0, :cond_0

    .line 23
    const/4 v0, 0x1

    iput-boolean v0, p0, LqB;->a:Z

    .line 24
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    invoke-virtual {p1}, LqK;->a()V

    .line 27
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, LqB;->a:J

    sub-long/2addr v0, v2

    .line 28
    const-string v2, "timeSpan"

    const-string v3, "applicationStartupDuration"

    const/4 v4, 0x0

    .line 29
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 28
    invoke-virtual {p1, v2, v3, v4, v5}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 30
    const-string v2, "timeSpan"

    const-string v3, "applicationStartupDuration"

    const/4 v4, 0x0

    .line 31
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 30
    invoke-virtual {p1, v2, v3, v4, v0}, LqK;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    invoke-virtual {p1}, LqK;->b()V

    .line 36
    :cond_0
    return-void

    .line 33
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, LqK;->b()V

    throw v0
.end method
