.class public final Laek;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laej;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laei;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LbwT;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LbwT;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laeu;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laeo;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LbwT;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laer;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lbxr;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 45
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 46
    iput-object p1, p0, Laek;->a:LbrA;

    .line 47
    const-class v0, Laej;

    invoke-static {v0, v3}, Laek;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laek;->a:Lbsk;

    .line 50
    const-class v0, Laei;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, Laek;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laek;->b:Lbsk;

    .line 53
    const-class v0, LbwT;

    new-instance v1, Lbwl;

    const-string v2, "AclFeed"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    .line 54
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 53
    invoke-static {v0, v3}, Laek;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laek;->c:Lbsk;

    .line 56
    const-class v0, LbwT;

    new-instance v1, Lbwl;

    const-string v2, "DocFeed"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    .line 57
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 56
    invoke-static {v0, v3}, Laek;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laek;->d:Lbsk;

    .line 59
    const-class v0, Laeu;

    new-instance v1, Lbwl;

    const-string v2, "DocFeed"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    .line 60
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 59
    invoke-static {v0, v3}, Laek;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laek;->e:Lbsk;

    .line 62
    const-class v0, Laeo;

    invoke-static {v0, v3}, Laek;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laek;->f:Lbsk;

    .line 65
    const-class v0, LbwT;

    new-instance v1, Lbwl;

    const-string v2, "AccountMetadata"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    .line 66
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 65
    invoke-static {v0, v3}, Laek;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laek;->g:Lbsk;

    .line 68
    const-class v0, Laer;

    invoke-static {v0, v3}, Laek;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laek;->h:Lbsk;

    .line 71
    const-class v0, Lbxr;

    invoke-static {v0, v3}, Laek;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laek;->i:Lbsk;

    .line 74
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 130
    packed-switch p1, :pswitch_data_0

    .line 148
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 132
    :pswitch_0
    new-instance v1, Laej;

    iget-object v0, p0, Laek;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 135
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Laek;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LpG;

    iget-object v2, v2, LpG;->m:Lbsk;

    .line 133
    invoke-static {v0, v2}, Laek;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    invoke-direct {v1, v0}, Laej;-><init>(LtK;)V

    move-object v0, v1

    .line 146
    :goto_0
    return-object v0

    .line 142
    :pswitch_1
    new-instance v0, Laei;

    invoke-direct {v0}, Laei;-><init>()V

    .line 144
    iget-object v1, p0, Laek;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Laek;

    .line 145
    invoke-virtual {v1, v0}, Laek;->a(Laei;)V

    goto :goto_0

    .line 130
    :pswitch_data_0
    .packed-switch 0x51c
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 169
    sparse-switch p2, :sswitch_data_0

    .line 234
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 171
    :sswitch_0
    check-cast p1, Laeh;

    .line 173
    iget-object v0, p0, Laek;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaeQ;

    iget-object v0, v0, LaeQ;->a:Lbsk;

    .line 176
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaeJ;

    .line 173
    invoke-virtual {p1, v0}, Laeh;->provideAclFeedGDataParserFactory(LaeJ;)LbwT;

    move-result-object v0

    .line 227
    :goto_0
    return-object v0

    .line 180
    :sswitch_1
    check-cast p1, Laeh;

    .line 182
    iget-object v0, p0, Laek;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaeQ;

    iget-object v0, v0, LaeQ;->b:Lbsk;

    .line 185
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaeN;

    .line 182
    invoke-virtual {p1, v0}, Laeh;->provideDocFeedGDataParserFactory(LaeN;)LbwT;

    move-result-object v0

    goto :goto_0

    .line 189
    :sswitch_2
    check-cast p1, Laeh;

    .line 191
    iget-object v0, p0, Laek;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaeA;

    iget-object v0, v0, LaeA;->a:Lbsk;

    .line 194
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laes;

    .line 191
    invoke-virtual {p1, v0}, Laeh;->provideGDataAuthenticatedEntryClient(Laes;)Laeu;

    move-result-object v0

    goto :goto_0

    .line 198
    :sswitch_3
    check-cast p1, Laeh;

    .line 200
    iget-object v0, p0, Laek;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laek;

    iget-object v0, v0, Laek;->b:Lbsk;

    .line 203
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laei;

    .line 200
    invoke-virtual {p1, v0}, Laeh;->provideChangedMetadataSyncerFactory(Laei;)Laeo;

    move-result-object v0

    goto :goto_0

    .line 207
    :sswitch_4
    check-cast p1, Laeh;

    .line 209
    iget-object v0, p0, Laek;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaeQ;

    iget-object v0, v0, LaeQ;->c:Lbsk;

    .line 212
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaeH;

    .line 209
    invoke-virtual {p1, v0}, Laeh;->provideMetadataFeedGDataParserFactory(LaeH;)LbwT;

    move-result-object v0

    goto :goto_0

    .line 216
    :sswitch_5
    check-cast p1, Laeh;

    .line 218
    iget-object v0, p0, Laek;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaeA;

    iget-object v0, v0, LaeA;->a:Lbsk;

    .line 221
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laes;

    .line 218
    invoke-virtual {p1, v0}, Laeh;->provideDocsListFeedClient(Laes;)Laer;

    move-result-object v0

    goto :goto_0

    .line 225
    :sswitch_6
    check-cast p1, Laeh;

    .line 227
    iget-object v0, p0, Laek;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laek;

    iget-object v0, v0, Laek;->a:Lbsk;

    .line 230
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laej;

    .line 227
    invoke-virtual {p1, v0}, Laeh;->provideXmlParserFactory(Laej;)Lbxr;

    move-result-object v0

    goto :goto_0

    .line 169
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_6
        0x88 -> :sswitch_5
        0x172 -> :sswitch_1
        0x176 -> :sswitch_0
        0x177 -> :sswitch_4
        0x1b2 -> :sswitch_2
        0x2c8 -> :sswitch_3
    .end sparse-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 92
    const-class v0, Laei;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x7c

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Laek;->a(LbuP;LbuB;)V

    .line 95
    const-class v0, Laej;

    iget-object v1, p0, Laek;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, Laek;->a(Ljava/lang/Class;Lbsk;)V

    .line 96
    const-class v0, Laei;

    iget-object v1, p0, Laek;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, Laek;->a(Ljava/lang/Class;Lbsk;)V

    .line 97
    const-class v0, LbwT;

    new-instance v1, Lbwl;

    const-string v2, "AclFeed"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, Laek;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, Laek;->a(Lbuv;Lbsk;)V

    .line 98
    const-class v0, LbwT;

    new-instance v1, Lbwl;

    const-string v2, "DocFeed"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, Laek;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, Laek;->a(Lbuv;Lbsk;)V

    .line 99
    const-class v0, Laeu;

    new-instance v1, Lbwl;

    const-string v2, "DocFeed"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, Laek;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, Laek;->a(Lbuv;Lbsk;)V

    .line 100
    const-class v0, Laeo;

    iget-object v1, p0, Laek;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, Laek;->a(Ljava/lang/Class;Lbsk;)V

    .line 101
    const-class v0, LbwT;

    new-instance v1, Lbwl;

    const-string v2, "AccountMetadata"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, Laek;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, Laek;->a(Lbuv;Lbsk;)V

    .line 102
    const-class v0, Laer;

    iget-object v1, p0, Laek;->h:Lbsk;

    invoke-virtual {p0, v0, v1}, Laek;->a(Ljava/lang/Class;Lbsk;)V

    .line 103
    const-class v0, Lbxr;

    iget-object v1, p0, Laek;->i:Lbsk;

    invoke-virtual {p0, v0, v1}, Laek;->a(Ljava/lang/Class;Lbsk;)V

    .line 104
    iget-object v0, p0, Laek;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x51d

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 106
    iget-object v0, p0, Laek;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x51c

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 108
    iget-object v0, p0, Laek;->c:Lbsk;

    const-class v1, Laeh;

    const/16 v2, 0x176

    invoke-virtual {p0, v1, v2}, Laek;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 110
    iget-object v0, p0, Laek;->d:Lbsk;

    const-class v1, Laeh;

    const/16 v2, 0x172

    invoke-virtual {p0, v1, v2}, Laek;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 112
    iget-object v0, p0, Laek;->e:Lbsk;

    const-class v1, Laeh;

    const/16 v2, 0x1b2

    invoke-virtual {p0, v1, v2}, Laek;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 114
    iget-object v0, p0, Laek;->f:Lbsk;

    const-class v1, Laeh;

    const/16 v2, 0x2c8

    invoke-virtual {p0, v1, v2}, Laek;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 116
    iget-object v0, p0, Laek;->g:Lbsk;

    const-class v1, Laeh;

    const/16 v2, 0x177

    invoke-virtual {p0, v1, v2}, Laek;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 118
    iget-object v0, p0, Laek;->h:Lbsk;

    const-class v1, Laeh;

    const/16 v2, 0x88

    invoke-virtual {p0, v1, v2}, Laek;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 120
    iget-object v0, p0, Laek;->i:Lbsk;

    const-class v1, Laeh;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Laek;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 122
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 155
    packed-switch p1, :pswitch_data_0

    .line 163
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157
    :pswitch_0
    check-cast p2, Laei;

    .line 159
    iget-object v0, p0, Laek;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laek;

    .line 160
    invoke-virtual {v0, p2}, Laek;->a(Laei;)V

    .line 165
    return-void

    .line 155
    :pswitch_data_0
    .packed-switch 0x7c
        :pswitch_0
    .end packed-switch
.end method

.method public a(Laei;)V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Laek;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->q:Lbsk;

    .line 83
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Laek;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->q:Lbsk;

    .line 81
    invoke-static {v0, v1}, Laek;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, Laei;->a:Laja;

    .line 87
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 126
    return-void
.end method
