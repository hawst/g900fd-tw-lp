.class final Laho;
.super Ljava/lang/Object;
.source "SynchronizeToDBProcessor.java"

# interfaces
.implements Lagy;


# instance fields
.field private a:J

.field private final a:LaFM;

.field private final a:LaGM;

.field private final a:Lagl;

.field private final a:Landroid/content/SyncResult;

.field private final a:Ljava/lang/Boolean;


# direct methods
.method constructor <init>(LaFM;Landroid/content/SyncResult;LaGM;Lagl;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Laho;->a:LaFM;

    .line 37
    iput-object p2, p0, Laho;->a:Landroid/content/SyncResult;

    .line 38
    iput-object p3, p0, Laho;->a:LaGM;

    .line 39
    iput-object p5, p0, Laho;->a:Ljava/lang/Boolean;

    .line 40
    iput-object p4, p0, Laho;->a:Lagl;

    .line 41
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Laho;->a:LaGM;

    iget-object v1, p0, Laho;->a:LaFM;

    invoke-virtual {v1}, LaFM;->a()LaFO;

    move-result-object v1

    invoke-interface {v0, v1}, LaGM;->a(LaFO;)LaFQ;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, LaFQ;->a()J

    move-result-wide v0

    iput-wide v0, p0, Laho;->a:J

    .line 75
    return-void
.end method

.method public a(LaeD;)V
    .locals 0

    .prologue
    .line 45
    return-void
.end method

.method public a(LaeD;LaJT;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x1

    .line 55
    invoke-interface {p2}, LaJT;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    iget-object v0, p0, Laho;->a:Lagl;

    iget-object v1, p0, Laho;->a:LaFM;

    iget-object v2, p0, Laho;->a:Ljava/lang/Boolean;

    invoke-interface {v0, v1, p2, v2}, Lagl;->b(LaFM;LaJT;Ljava/lang/Boolean;)V

    .line 58
    iget-object v0, p0, Laho;->a:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numEntries:J

    add-long/2addr v2, v6

    iput-wide v2, v0, Landroid/content/SyncStats;->numEntries:J

    .line 59
    iget-object v0, p0, Laho;->a:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numDeletes:J

    add-long/2addr v2, v6

    iput-wide v2, v0, Landroid/content/SyncStats;->numDeletes:J

    .line 66
    :goto_0
    const-string v0, "CompleteFeedProcessor"

    const/4 v1, 0x2

    invoke-static {v0, v1}, LalV;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    const-string v0, "CompleteFeedProcessor"

    const-string v1, "Entry: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LalV;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 69
    :cond_0
    return-void

    .line 61
    :cond_1
    iget-object v0, p0, Laho;->a:Lagl;

    iget-object v1, p0, Laho;->a:LaFM;

    iget-object v3, p0, Laho;->a:Ljava/lang/Boolean;

    iget-wide v4, p0, Laho;->a:J

    move-object v2, p2

    invoke-interface/range {v0 .. v5}, Lagl;->a(LaFM;LaJT;Ljava/lang/Boolean;J)V

    .line 62
    iget-object v0, p0, Laho;->a:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numInserts:J

    add-long/2addr v2, v6

    iput-wide v2, v0, Landroid/content/SyncStats;->numInserts:J

    .line 63
    iget-object v0, p0, Laho;->a:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numEntries:J

    add-long/2addr v2, v6

    iput-wide v2, v0, Landroid/content/SyncStats;->numEntries:J

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 79
    return-void
.end method

.method public b(LaeD;)V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Laho;->a:LaGM;

    iget-object v1, p0, Laho;->a:LaFM;

    invoke-interface {v0, v1}, LaGM;->b(LaFM;)V

    .line 50
    return-void
.end method
