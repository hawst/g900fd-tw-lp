.class final LaiH;
.super Laiv;
.source "ActivitySessionScopeImpl.java"

# interfaces
.implements LaiG;


# instance fields
.field private final a:LaiU;

.field private final a:Lajj;

.field private final a:Lajy;

.field private final a:Landroid/content/Context;

.field private final a:LbuE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbuE",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;LaiU;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation runtime Lajg;
        .end annotation
    .end param

    .prologue
    .line 100
    invoke-direct {p0}, Laiv;-><init>()V

    .line 89
    new-instance v0, LaiI;

    invoke-direct {v0, p0}, LaiI;-><init>(LaiH;)V

    iput-object v0, p0, LaiH;->a:LbuE;

    .line 101
    iput-object p1, p0, LaiH;->a:Landroid/content/Context;

    .line 102
    invoke-static {p2}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiU;

    iput-object v0, p0, LaiH;->a:LaiU;

    .line 103
    new-instance v0, Lajy;

    iget-object v1, p0, LaiH;->a:LbuE;

    invoke-direct {v0, v1}, Lajy;-><init>(LbuE;)V

    iput-object v0, p0, LaiH;->a:Lajy;

    .line 104
    new-instance v0, Lajj;

    iget-object v1, p0, LaiH;->a:LbuE;

    invoke-direct {v0, v1}, Lajj;-><init>(LbuE;)V

    iput-object v0, p0, LaiH;->a:Lajj;

    .line 105
    return-void
.end method

.method private a(Landroid/app/Activity;)LaiM;
    .locals 2

    .prologue
    .line 132
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    instance-of v0, p1, LH;

    if-eqz v0, :cond_1

    .line 134
    :cond_0
    iget-object v0, p0, LaiH;->a:Lajj;

    .line 136
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LaiH;->a:Lajy;

    goto :goto_0
.end method

.method static synthetic a(LaiH;)LaiU;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LaiH;->a:LaiU;

    return-object v0
.end method

.method static synthetic a(LaiH;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LaiH;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/app/Activity;)LaiN;
    .locals 1

    .prologue
    .line 124
    invoke-direct {p0, p1}, LaiH;->a(Landroid/app/Activity;)LaiM;

    move-result-object v0

    invoke-interface {v0, p1}, LaiM;->a(Landroid/app/Activity;)LaiN;

    move-result-object v0

    return-object v0
.end method

.method protected a()LajA;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, LaiH;->a:LaiU;

    invoke-interface {v0}, LaiU;->a()Landroid/content/Context;

    move-result-object v0

    .line 115
    instance-of v0, v0, LaiJ;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, LaiH;->a:LaiU;

    invoke-interface {v0}, LaiU;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiW;

    invoke-virtual {v0}, LaiW;->a()LajA;

    move-result-object v0

    .line 119
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LaiH;->a:LbuE;

    invoke-interface {v0}, LbuE;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-direct {p0, v0}, LaiH;->a(Landroid/app/Activity;)LaiM;

    move-result-object v0

    invoke-interface {v0}, LaiM;->a()LajA;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lbuv;LbuE;)LbuE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbuv",
            "<TT;>;",
            "LbuE",
            "<TT;>;)",
            "LbuE",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 109
    new-instance v0, LaiK;

    invoke-direct {v0, p0, p2}, LaiK;-><init>(LaiH;LbuE;)V

    invoke-super {p0, p1, v0}, Laiv;->a(Lbuv;LbuE;)LbuE;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/Class;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, LaiH;->a:LaiU;

    invoke-interface {v0}, LaiU;->a()Landroid/content/Context;

    move-result-object v0

    .line 143
    instance-of v1, v0, LaiJ;

    if-eqz v1, :cond_0

    .line 144
    check-cast v0, LaiJ;

    iget-object v0, v0, LaiJ;->a:Ljava/lang/Class;

    .line 148
    :goto_0
    return-object v0

    .line 146
    :cond_0
    invoke-static {v0}, Lajt;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v1

    .line 147
    instance-of v2, v1, Landroid/app/Activity;

    if-eqz v2, :cond_1

    move-object v0, v1

    .line 148
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 150
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Active Context "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not an Activity nor an ActivitySessionContext"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
