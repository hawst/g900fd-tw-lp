.class abstract Lpd;
.super LoX;
.source "BaseIcsClassLoaderExtender.java"


# instance fields
.field private final a:Lpc;

.field private final a:Lps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lps",
            "<",
            "Ljava/lang/ClassLoader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LoZ;LoY;Lpw;Lps;Lpc;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LoZ;",
            "LoY;",
            "Lpw;",
            "Lps",
            "<",
            "Ljava/lang/ClassLoader;",
            ">;",
            "Lpc;",
            ")V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, LoX;-><init>(LoZ;LoY;Lpw;)V

    .line 26
    iput-object p4, p0, Lpd;->a:Lps;

    .line 27
    iput-object p5, p0, Lpd;->a:Lpc;

    .line 28
    return-void
.end method


# virtual methods
.method protected abstract a(Lps;Ljava/util/ArrayList;Ljava/io/File;)[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lps",
            "<*>;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;",
            "Ljava/io/File;",
            ")[",
            "Ljava/lang/Object;"
        }
    .end annotation
.end method

.method protected b(Ljava/util/List;Ljava/io/File;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Ljava/io/File;",
            ">;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    .prologue
    .line 42
    :try_start_0
    new-instance v0, Lps;

    iget-object v1, p0, Lpd;->a:Lps;

    const-string v2, "pathList"

    invoke-virtual {v1, v2}, Lps;->a(Ljava/lang/String;)Lpp;

    move-result-object v1

    invoke-virtual {v1}, Lpp;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Lps;-><init>(Ljava/lang/Object;)V

    .line 43
    const-string v1, "dexElements"

    invoke-virtual {v0, v1}, Lps;->a(Ljava/lang/String;)Lpp;

    move-result-object v1

    .line 44
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0, v2, p2}, Lpd;->a(Lps;Ljava/util/ArrayList;Ljava/io/File;)[Ljava/lang/Object;

    move-result-object v0

    .line 46
    iget-object v2, p0, Lpd;->a:Lpc;

    invoke-virtual {v2, v1, v0}, Lpc;->a(Lpp;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lpr; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    return-void

    .line 47
    :catch_0
    move-exception v0

    .line 48
    new-instance v1, Lpm;

    invoke-direct {v1, v0}, Lpm;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
