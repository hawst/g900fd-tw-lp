.class final enum LWq;
.super Ljava/lang/Enum;
.source "CommonPreferencesInstaller.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LWq;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LWq;

.field private static final synthetic a:[LWq;

.field public static final enum b:LWq;

.field public static final enum c:LWq;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 67
    new-instance v0, LWq;

    const-string v1, "ALWAYS"

    invoke-direct {v0, v1, v2}, LWq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LWq;->a:LWq;

    new-instance v0, LWq;

    const-string v1, "ENABLED"

    invoke-direct {v0, v1, v3}, LWq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LWq;->b:LWq;

    new-instance v0, LWq;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v4}, LWq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LWq;->c:LWq;

    const/4 v0, 0x3

    new-array v0, v0, [LWq;

    sget-object v1, LWq;->a:LWq;

    aput-object v1, v0, v2

    sget-object v1, LWq;->b:LWq;

    aput-object v1, v0, v3

    sget-object v1, LWq;->c:LWq;

    aput-object v1, v0, v4

    sput-object v0, LWq;->a:[LWq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LWq;
    .locals 1

    .prologue
    .line 67
    const-class v0, LWq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LWq;

    return-object v0
.end method

.method public static values()[LWq;
    .locals 1

    .prologue
    .line 67
    sget-object v0, LWq;->a:[LWq;

    invoke-virtual {v0}, [LWq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LWq;

    return-object v0
.end method
