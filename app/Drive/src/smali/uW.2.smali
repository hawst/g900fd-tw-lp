.class public LuW;
.super Ljava/lang/Object;
.source "PickEntryActivity.java"


# instance fields
.field private final a:Landroid/content/Intent;

.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/Context;LaFO;)V
    .locals 3

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LuW;->a:Ljava/util/ArrayList;

    .line 54
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LuW;->a:Landroid/content/Intent;

    .line 55
    iget-object v0, p0, LuW;->a:Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/app/PickEntryActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 56
    const-string v0, "Account name not specified"

    invoke-static {p2, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    iget-object v0, p0, LuW;->a:Landroid/content/Intent;

    const-string v1, "accountName"

    invoke-virtual {p2}, LaFO;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 58
    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;LaFO;LuV;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, LuW;-><init>(Landroid/content/Context;LaFO;)V

    return-void
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 148
    iget-object v0, p0, LuW;->a:Landroid/content/Intent;

    const-string v1, "disabledAncestors"

    iget-object v2, p0, LuW;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 150
    iget-object v0, p0, LuW;->a:Landroid/content/Intent;

    return-object v0
.end method

.method public a()LuW;
    .locals 3

    .prologue
    .line 83
    iget-object v0, p0, LuW;->a:Landroid/content/Intent;

    const-string v1, "showNewFolder"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 84
    return-object p0
.end method

.method public a(I)LuW;
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, LuW;->a:Landroid/content/Intent;

    const-string v1, "selectButtonText"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 99
    return-object p0
.end method

.method public a(Lbmv;)LuW;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbmv",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;)",
            "LuW;"
        }
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, LuW;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 117
    return-object p0
.end method

.method public a(Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;)LuW;
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, LuW;->a:Landroid/content/Intent;

    const-string v1, "documentTypeFilter"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 79
    return-object p0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LuW;
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, LuW;->a:Landroid/content/Intent;

    const-string v1, "entrySpec.v2"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 71
    return-object p0
.end method

.method public a(Ljava/lang/String;)LuW;
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, LuW;->a:Landroid/content/Intent;

    const-string v1, "dialogTitle"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 143
    return-object p0
.end method

.method public a([Ljava/lang/String;)LuW;
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, LuW;->a:Landroid/content/Intent;

    const-string v1, "mimeTypes"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 135
    return-object p0
.end method

.method public b()LuW;
    .locals 3

    .prologue
    .line 88
    iget-object v0, p0, LuW;->a:Landroid/content/Intent;

    const-string v1, "showTopCollections"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 89
    return-object p0
.end method

.method public b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LuW;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, LuW;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    return-object p0
.end method

.method public c()LuW;
    .locals 3

    .prologue
    .line 105
    iget-object v0, p0, LuW;->a:Landroid/content/Intent;

    const-string v1, "disablePreselectedEntry"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 106
    return-object p0
.end method
