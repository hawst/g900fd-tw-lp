.class public LaaH;
.super Ljava/lang/Object;
.source "ExtraTextDataSource.java"

# interfaces
.implements LaaC;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/CharSequence;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaaH;->b:Ljava/lang/String;

    .line 34
    if-nez p1, :cond_0

    .line 35
    iget-object v0, p0, LaaH;->b:Ljava/lang/String;

    const/16 v1, 0xf

    invoke-static {v0, p3, v1}, LaaH;->a(Ljava/lang/String;Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaaH;->a:Ljava/lang/String;

    .line 39
    :goto_0
    return-void

    .line 37
    :cond_0
    iput-object p1, p0, LaaH;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method static a(Ljava/lang/String;Landroid/content/Context;I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 74
    sget-object v0, Lbih;->j:Lbih;

    sget-object v1, Lbih;->n:Lbih;

    invoke-virtual {v0, v1}, Lbih;->a(Lbih;)Lbih;

    move-result-object v1

    .line 75
    invoke-virtual {v1}, Lbih;->a()Lbih;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbih;->a(Ljava/lang/CharSequence;)I

    move-result v0

    .line 76
    if-gez v0, :cond_0

    .line 77
    sget v0, Lxi;->upload_untitled_file_title:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 90
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 79
    :cond_0
    add-int v2, p2, v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 80
    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 81
    const/16 v3, 0x20

    invoke-virtual {v1, v0, v3}, Lbih;->b(Ljava/lang/CharSequence;C)Ljava/lang/String;

    move-result-object v0

    .line 82
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 83
    invoke-virtual {v1, v0}, Lbih;->b(Ljava/lang/CharSequence;)I

    move-result v2

    .line 84
    if-ltz v2, :cond_1

    .line 85
    const/4 v3, 0x0

    invoke-virtual {v0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 88
    :cond_1
    invoke-virtual {v1, v0}, Lbih;->b(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(LNb;)LMZ;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, LaaH;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, LNb;->b(Ljava/lang/String;)LNb;

    .line 67
    invoke-virtual {p1}, LNb;->a()LMZ;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, LaaH;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 57
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    const-string v0, "text/plain"

    return-object v0
.end method
