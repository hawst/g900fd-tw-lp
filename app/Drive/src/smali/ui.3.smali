.class public final enum Lui;
.super Ljava/lang/Enum;
.source "MainProxyLogic.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lui;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lui;

.field private static final synthetic a:[Lui;

.field public static final enum b:Lui;

.field public static final enum c:Lui;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 36
    new-instance v0, Lui;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lui;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lui;->a:Lui;

    new-instance v0, Lui;

    const-string v1, "CREATE_NEW"

    invoke-direct {v0, v1, v3}, Lui;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lui;->b:Lui;

    new-instance v0, Lui;

    const-string v1, "WARM_WELCOME"

    invoke-direct {v0, v1, v4}, Lui;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lui;->c:Lui;

    .line 35
    const/4 v0, 0x3

    new-array v0, v0, [Lui;

    sget-object v1, Lui;->a:Lui;

    aput-object v1, v0, v2

    sget-object v1, Lui;->b:Lui;

    aput-object v1, v0, v3

    sget-object v1, Lui;->c:Lui;

    aput-object v1, v0, v4

    sput-object v0, Lui;->a:[Lui;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Landroid/content/Intent;)Lui;
    .locals 1

    .prologue
    .line 39
    const-string v0, "dialogToShow"

    .line 40
    invoke-virtual {p0, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lui;

    .line 41
    if-nez v0, :cond_0

    .line 42
    sget-object v0, Lui;->a:Lui;

    .line 44
    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lui;
    .locals 1

    .prologue
    .line 35
    const-class v0, Lui;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lui;

    return-object v0
.end method

.method public static values()[Lui;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lui;->a:[Lui;

    invoke-virtual {v0}, [Lui;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lui;

    return-object v0
.end method
