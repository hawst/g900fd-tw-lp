.class public LQA;
.super Ljava/lang/Object;
.source "ClientFlagJsonParserImpl.java"

# interfaces
.implements LQy;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/io/InputStream;LQr;Z)V
    .locals 4

    .prologue
    .line 34
    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 37
    :try_start_0
    new-instance v0, Lbtm;

    invoke-direct {v0}, Lbtm;-><init>()V

    invoke-virtual {v0, v1}, Lbtm;->a(Ljava/io/Reader;)Lbth;

    move-result-object v0

    check-cast v0, Lbtk;
    :try_end_0
    .catch Lbtl; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 42
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStreamReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 51
    :goto_0
    monitor-enter p2

    .line 52
    if-eqz p3, :cond_0

    .line 53
    :try_start_2
    invoke-interface {p2}, LQr;->b()V

    .line 55
    :cond_0
    invoke-virtual {v0}, Lbtk;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 56
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbth;

    .line 57
    invoke-virtual {v1}, Lbth;->d()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 58
    check-cast v1, Lbtn;

    .line 59
    invoke-virtual {v1}, Lbtn;->f()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 60
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1}, Lbtn;->a()Z

    move-result v1

    invoke-interface {p2, v0, v1}, LQr;->a(Ljava/lang/String;Z)V

    goto :goto_1

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit p2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 38
    :catch_0
    move-exception v0

    .line 39
    :try_start_3
    new-instance v2, LQz;

    const-string v3, "Error parsing client flags file: "

    invoke-direct {v2, v3, v0}, LQz;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 41
    :catchall_1
    move-exception v0

    .line 42
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStreamReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 45
    :goto_2
    throw v0

    .line 61
    :cond_2
    :try_start_5
    invoke-virtual {v1}, Lbtn;->g()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 62
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1}, Lbtn;->b()I

    move-result v1

    invoke-interface {p2, v0, v1}, LQr;->a(Ljava/lang/String;I)V

    goto :goto_1

    .line 63
    :cond_3
    invoke-virtual {v1}, Lbtn;->h()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 64
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1}, Lbtn;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 68
    :cond_4
    monitor-exit p2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 69
    return-void

    .line 43
    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;LQr;Z)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 74
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    .line 76
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 77
    const-string v0, "ClientFlagJsonParserImpl"

    const-string v1, "Local flag override path: %s"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v5

    invoke-static {v0, v1, v2}, LalV;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 79
    const/4 v2, 0x0

    .line 81
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    :try_start_1
    invoke-virtual {p0, v1, p2, p3}, LQA;->a(Ljava/io/InputStream;LQr;Z)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 88
    if-eqz v1, :cond_0

    .line 90
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 83
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 86
    :goto_1
    :try_start_3
    const-string v2, "ClientFlagJsonParserImpl"

    const-string v3, "Local flag file not available: e=%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, LalV;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 88
    if-eqz v1, :cond_0

    .line 90
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 91
    :catch_1
    move-exception v0

    goto :goto_0

    .line 88
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_1

    .line 90
    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 93
    :cond_1
    :goto_3
    throw v0

    .line 91
    :catch_2
    move-exception v0

    goto :goto_0

    :catch_3
    move-exception v1

    goto :goto_3

    .line 88
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 83
    :catch_4
    move-exception v0

    goto :goto_1
.end method
