.class LaeY;
.super Ljava/lang/Object;
.source "DatabaseSyncMoreController.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:LaeX;

.field final synthetic a:Z

.field final synthetic b:Z


# direct methods
.method constructor <init>(LaeX;ZZ)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, LaeY;->a:LaeX;

    iput-boolean p2, p0, LaeY;->a:Z

    iput-boolean p3, p0, LaeY;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 54
    iget-object v0, p0, LaeY;->a:LaeX;

    invoke-static {v0}, LaeX;->a(LaeX;)I

    move-result v0

    iget-object v1, p0, LaeY;->a:LaeX;

    iget-object v1, v1, LaeX;->a:LaeV;

    invoke-static {v1}, LaeV;->b(LaeV;)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 63
    :goto_0
    return-void

    .line 57
    :cond_0
    iget-boolean v0, p0, LaeY;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LaeY;->a:LaeX;

    invoke-static {v0}, LaeX;->a(LaeX;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58
    iget-object v0, p0, LaeY;->a:LaeX;

    invoke-static {v0}, LaeX;->a(LaeX;)Landroid/content/Context;

    move-result-object v0

    sget v1, Lxi;->sync_more_error:I

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 62
    :goto_1
    iget-object v0, p0, LaeY;->a:LaeX;

    iget-object v0, v0, LaeX;->a:LaeV;

    iget-object v1, p0, LaeY;->a:LaeX;

    invoke-static {v1}, LaeX;->a(LaeX;)LaFM;

    move-result-object v1

    iget-object v2, p0, LaeY;->a:LaeX;

    invoke-static {v2}, LaeX;->a(LaeX;)Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    move-result-object v2

    iget-object v3, p0, LaeY;->a:LaeX;

    invoke-static {v3}, LaeX;->a(LaeX;)Lafy;

    move-result-object v3

    invoke-static {v0, v1, v2, v3, v4}, LaeV;->a(LaeV;LaFM;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;Lafy;Z)V

    goto :goto_0

    .line 60
    :cond_1
    iget-object v0, p0, LaeY;->a:LaeX;

    iget-object v0, v0, LaeX;->a:LaeV;

    iget-boolean v1, p0, LaeY;->b:Z

    invoke-static {v0, v1}, LaeV;->a(LaeV;Z)Z

    goto :goto_1
.end method
