.class LSO;
.super Ljava/lang/Object;
.source "ActiveSearch.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field final synthetic a:LSL;

.field final synthetic a:Landroid/widget/AutoCompleteTextView;


# direct methods
.method constructor <init>(LSL;Landroid/widget/AutoCompleteTextView;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, LSO;->a:LSL;

    iput-object p2, p0, LSO;->a:Landroid/widget/AutoCompleteTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 5

    .prologue
    .line 138
    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    .line 139
    iget-object v0, p0, LSO;->a:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 140
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 141
    iget-object v1, p0, LSO;->a:LSL;

    invoke-static {v1}, LSL;->b(LSL;)Landroid/app/Activity;

    move-result-object v1

    .line 142
    new-instance v2, Landroid/provider/SearchRecentSuggestions;

    .line 143
    invoke-static {v1}, Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {v2, v1, v3, v4}, Landroid/provider/SearchRecentSuggestions;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    .line 145
    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/provider/SearchRecentSuggestions;->saveRecentQuery(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, LSO;->a:LSL;

    invoke-static {v0}, LSL;->a(LSL;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->clearFocus()V

    .line 149
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
