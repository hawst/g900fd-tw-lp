.class public LasC;
.super Ljava/lang/Object;
.source "RedeemVoucherController.java"

# interfaces
.implements LasY;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/welcome/RedeemVoucherController;

.field private a:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/welcome/RedeemVoucherController;)V
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, LasC;->a:Lcom/google/android/apps/docs/welcome/RedeemVoucherController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 5

    .prologue
    .line 280
    const-string v0, "RedeemVoucherController"

    const-string v1, "Redeem voucher progress: %d / %d."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 281
    return-void
.end method

.method public a(LaFO;)V
    .locals 1

    .prologue
    .line 252
    new-instance v0, LasD;

    invoke-direct {v0, p0, p1}, LasD;-><init>(LasC;LaFO;)V

    invoke-virtual {p0, v0}, LasC;->a(Ljava/lang/Runnable;)V

    .line 257
    return-void
.end method

.method a(Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 284
    iget-object v0, p0, LasC;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 285
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Setting outcome a second time"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 286
    const-string v1, "RedeemVoucherController"

    const-string v2, "RedeemNotifier overwriting outcome with a new value. "

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 288
    :cond_0
    iput-object p1, p0, LasC;->a:Ljava/lang/Runnable;

    .line 289
    return-void
.end method

.method public a(ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 261
    new-instance v0, LasE;

    invoke-direct {v0, p0, p1, p2}, LasE;-><init>(LasC;ZLjava/lang/String;)V

    invoke-virtual {p0, v0}, LasC;->a(Ljava/lang/Runnable;)V

    .line 266
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 270
    iget-object v0, p0, LasC;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, LasC;->a:Lcom/google/android/apps/docs/welcome/RedeemVoucherController;

    iget-object v1, p0, LasC;->a:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a(Lcom/google/android/apps/docs/welcome/RedeemVoucherController;Ljava/lang/Runnable;)V

    .line 275
    :goto_0
    return-void

    .line 273
    :cond_0
    const-string v0, "RedeemVoucherController"

    const-string v1, "RedeemNotifier runs (a noop) without an outcome set."

    invoke-static {v0, v1}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
