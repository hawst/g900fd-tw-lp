.class public LMI;
.super Ljava/lang/Object;
.source "SwipableDocListLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic a:Landroid/view/View;

.field final synthetic a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

.field a:Z

.field final synthetic b:I

.field final synthetic c:I

.field final synthetic d:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;IIIILandroid/view/View;)V
    .locals 1

    .prologue
    .line 290
    iput-object p1, p0, LMI;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    iput p2, p0, LMI;->a:I

    iput p3, p0, LMI;->b:I

    iput p4, p0, LMI;->c:I

    iput p5, p0, LMI;->d:I

    iput-object p6, p0, LMI;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 291
    const/4 v0, 0x1

    iput-boolean v0, p0, LMI;->a:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 295
    iget-object v0, p0, LMI;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, LMI;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->post(Ljava/lang/Runnable;)Z

    .line 320
    :goto_0
    return-void

    .line 302
    :cond_0
    iget-boolean v0, p0, LMI;->a:Z

    if-eqz v0, :cond_1

    .line 303
    iget-object v0, p0, LMI;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)Landroid/widget/Scroller;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 304
    iget-object v0, p0, LMI;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)Landroid/widget/Scroller;

    move-result-object v0

    iget v1, p0, LMI;->a:I

    iget v3, p0, LMI;->b:I

    iget-object v4, p0, LMI;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v4}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)I

    move-result v6

    move v4, v2

    move v5, v2

    move v7, v2

    move v8, v2

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 305
    iget-object v0, p0, LMI;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)Landroid/widget/Scroller;

    move-result-object v0

    iget v1, p0, LMI;->c:I

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->setFinalX(I)V

    .line 306
    iput-boolean v2, p0, LMI;->a:Z

    .line 307
    iget-object v0, p0, LMI;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)LML;

    move-result-object v0

    sget-object v1, LML;->b:LML;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, LMI;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)Landroid/view/View;

    move-result-object v0

    .line 308
    :goto_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 311
    :cond_1
    iget-object v0, p0, LMI;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)Landroid/widget/Scroller;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 312
    const-string v0, "SwipableDocListLayout"

    const-string v1, "Fling is done, now at %s"

    new-array v3, v9, [Ljava/lang/Object;

    iget v4, p0, LMI;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0, v1, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 313
    iget-object v0, p0, LMI;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    iget v1, p0, LMI;->d:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->setCurrentPageInternal(I)V

    .line 319
    :goto_2
    iget-object v0, p0, LMI;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v0}, Lec;->a(Landroid/view/View;)V

    goto :goto_0

    .line 307
    :cond_2
    iget-object v0, p0, LMI;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->b(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 315
    :cond_3
    iget-object v0, p0, LMI;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)Landroid/widget/Scroller;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    .line 316
    iget-object v0, p0, LMI;->a:Landroid/view/View;

    iget-object v1, p0, LMI;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v1}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)Landroid/widget/Scroller;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrX()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 317
    iget-object v0, p0, LMI;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->post(Ljava/lang/Runnable;)Z

    goto :goto_2
.end method
