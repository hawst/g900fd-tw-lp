.class public final LhY;
.super Ljava/lang/Object;
.source "ActionBarActivityDelegateBase.java"

# interfaces
.implements Ljr;


# instance fields
.field final synthetic a:Landroid/support/v7/app/ActionBarActivityDelegateBase;


# direct methods
.method private constructor <init>(Landroid/support/v7/app/ActionBarActivityDelegateBase;)V
    .locals 0

    .prologue
    .line 1400
    iput-object p1, p0, LhY;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/support/v7/app/ActionBarActivityDelegateBase;LhR;)V
    .locals 0

    .prologue
    .line 1400
    invoke-direct {p0, p1}, LhY;-><init>(Landroid/support/v7/app/ActionBarActivityDelegateBase;)V

    return-void
.end method


# virtual methods
.method public a(Ljb;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 1403
    invoke-virtual {p1}, Ljb;->a()Ljb;

    move-result-object v2

    .line 1404
    if-eq v2, p1, :cond_2

    move v0, v1

    .line 1405
    :goto_0
    iget-object v3, p0, LhY;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    if-eqz v0, :cond_0

    move-object p1, v2

    :cond_0
    invoke-static {v3, p1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase;Landroid/view/Menu;)Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;

    move-result-object v3

    .line 1406
    if-eqz v3, :cond_1

    .line 1407
    if-eqz v0, :cond_3

    .line 1408
    iget-object v0, p0, LhY;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget v4, v3, Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;->a:I

    invoke-static {v0, v4, v3, v2}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase;ILandroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Landroid/view/Menu;)V

    .line 1409
    iget-object v0, p0, LhY;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    invoke-static {v0, v3, v1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase;Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Z)V

    .line 1417
    :cond_1
    :goto_1
    return-void

    .line 1404
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1413
    :cond_3
    iget-object v0, p0, LhY;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:LhN;

    invoke-virtual {v0}, LhN;->closeOptionsMenu()V

    .line 1414
    iget-object v0, p0, LhY;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    invoke-static {v0, v3, p2}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a(Landroid/support/v7/app/ActionBarActivityDelegateBase;Landroid/support/v7/app/ActionBarActivityDelegateBase$PanelFeatureState;Z)V

    goto :goto_1
.end method

.method public a(Ljb;)Z
    .locals 2

    .prologue
    .line 1421
    if-nez p1, :cond_0

    iget-object v0, p0, LhY;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-boolean v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Z

    if-eqz v0, :cond_0

    .line 1422
    iget-object v0, p0, LhY;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a()LiC;

    move-result-object v0

    .line 1423
    if-eqz v0, :cond_0

    iget-object v1, p0, LhY;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    invoke-virtual {v1}, Landroid/support/v7/app/ActionBarActivityDelegateBase;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1424
    const/16 v1, 0x8

    invoke-interface {v0, v1, p1}, LiC;->b(ILandroid/view/Menu;)Z

    .line 1427
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
