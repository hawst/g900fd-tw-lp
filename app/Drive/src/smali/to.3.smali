.class Lto;
.super Landroid/os/AsyncTask;
.source "DocumentPreviewActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LaGu;

.field final synthetic a:Ltn;


# direct methods
.method constructor <init>(Ltn;LaGu;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lto;->a:Ltn;

    iput-object p2, p0, Lto;->a:LaGu;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 180
    :try_start_0
    iget-object v0, p0, Lto;->a:LaGu;

    if-nez v0, :cond_0

    .line 181
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 187
    :goto_0
    return-object v0

    .line 183
    :cond_0
    iget-object v0, p0, Lto;->a:Ltn;

    iget-object v0, v0, Ltn;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LaGM;

    iget-object v2, p0, Lto;->a:LaGu;

    invoke-interface {v2}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-interface {v0, v2}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGu;

    move-result-object v0

    .line 184
    if-eqz v0, :cond_1

    invoke-interface {v0}, LaGu;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch LaGT; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 186
    :catch_0
    move-exception v0

    .line 187
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 193
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lto;->a:Ltn;

    iget-object v0, v0, Ltn;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lto;->a:Ltn;

    iget-object v0, v0, Ltn;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;)V

    .line 196
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 176
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lto;->a([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 176
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lto;->a(Ljava/lang/Boolean;)V

    return-void
.end method
