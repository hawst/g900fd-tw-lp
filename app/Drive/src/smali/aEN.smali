.class LaEN;
.super Ljava/lang/Object;
.source "EntryChangeDatabaseHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:LaGu;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LaEO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LaGu;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGu;",
            "Ljava/util/List",
            "<",
            "LaEO;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    iput-object v0, p0, LaEN;->a:LaGu;

    .line 143
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, LaEN;->a:Ljava/util/List;

    .line 144
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 148
    iget-object v0, p0, LaEN;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaEO;

    .line 149
    invoke-virtual {v0}, LaEO;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaEC;

    .line 150
    if-eqz v0, :cond_0

    .line 151
    iget-object v2, p0, LaEN;->a:LaGu;

    invoke-interface {v0, v2}, LaEC;->a(LaGu;)V

    goto :goto_0

    .line 154
    :cond_1
    return-void
.end method
