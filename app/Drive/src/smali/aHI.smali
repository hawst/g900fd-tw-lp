.class public LaHI;
.super Ljava/lang/Object;
.source "WapiServerRequestIssuer.java"

# interfaces
.implements LaHE;


# instance fields
.field private final a:Laer;


# direct methods
.method public constructor <init>(Laer;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, LaHI;->a:Laer;

    .line 32
    return-void
.end method

.method private a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 92
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "root"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, LaHI;->a:Laer;

    invoke-interface {v0, p1}, Laer;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Ljava/lang/String;

    move-result-object v0

    .line 94
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "contents"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 98
    :goto_0
    return-object v0

    .line 96
    :cond_0
    iget-object v0, p0, LaHI;->a:Laer;

    invoke-interface {v0, p1}, Laer;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 151
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 152
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 153
    const-string v1, "remove-behavior"

    const-string v2, "drive_v1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 154
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 155
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(LaHy;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 159
    instance-of v0, p2, LbwO;

    if-eqz v0, :cond_0

    .line 160
    const/4 v0, 0x1

    invoke-interface {p1, v0, p2}, LaHy;->a(ILjava/lang/Throwable;)V

    .line 168
    :goto_0
    const-string v0, "WapiServerRequestIssuer"

    const-string v1, "applyOnServer failed"

    invoke-static {v0, p2, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 169
    return-void

    .line 161
    :cond_0
    instance-of v0, p2, Lbxk;

    if-eqz v0, :cond_1

    .line 162
    const/4 v0, 0x2

    invoke-interface {p1, v0, p2}, LaHy;->a(ILjava/lang/Throwable;)V

    goto :goto_0

    .line 163
    :cond_1
    instance-of v0, p2, Ljava/io/IOException;

    if-eqz v0, :cond_2

    .line 164
    const/4 v0, 0x3

    invoke-interface {p1, v0, p2}, LaHy;->a(ILjava/lang/Throwable;)V

    goto :goto_0

    .line 166
    :cond_2
    const/4 v0, 0x4

    invoke-interface {p1, v0, p2}, LaHy;->a(ILjava/lang/Throwable;)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;ZLaHy;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 48
    iget-object v2, p1, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    .line 49
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a()Ljava/lang/String;

    move-result-object v3

    .line 52
    :try_start_0
    invoke-direct {p0, p2}, LaHI;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Ljava/lang/String;

    move-result-object v4

    .line 53
    if-nez v4, :cond_0

    .line 54
    const-string v1, "WapiServerRequestIssuer"

    const-string v2, "contentSource is null for: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 82
    :goto_0
    return v0

    .line 58
    :cond_0
    if-eqz p4, :cond_1

    .line 59
    iget-object v3, p0, LaHI;->a:Laer;

    .line 60
    invoke-interface {v3, p1}, Laer;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaeF;

    move-result-object v3

    .line 61
    new-instance v5, LaeC;

    invoke-virtual {v3}, Lbxb;->x()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v3}, LaeC;-><init>(Ljava/lang/String;)V

    .line 62
    iget-object v3, p0, LaHI;->a:Laer;

    invoke-interface {v3, v4, v2, v5}, Laer;->a(Ljava/lang/String;LaFO;Lbxb;)Lbxb;

    :goto_1
    move v0, v1

    .line 82
    goto :goto_0

    .line 64
    :cond_1
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 65
    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    .line 66
    invoke-virtual {v4, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 67
    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 68
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 70
    iget-object v4, p0, LaHI;->a:Laer;

    const-string v5, "*"

    invoke-interface {v4, v3, v2, v5}, Laer;->a(Ljava/lang/String;LaFO;Ljava/lang/String;)V
    :try_end_0
    .catch Lbxk; {:try_start_0 .. :try_end_0} :catch_0
    .catch LbwO; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_1

    .line 72
    :catch_0
    move-exception v1

    .line 73
    invoke-direct {p0, p5, v1}, LaHI;->a(LaHy;Ljava/lang/Exception;)V

    goto :goto_0

    .line 75
    :catch_1
    move-exception v1

    .line 76
    invoke-direct {p0, p5, v1}, LaHI;->a(LaHy;Ljava/lang/Exception;)V

    goto :goto_0

    .line 78
    :catch_2
    move-exception v1

    .line 79
    invoke-direct {p0, p5, v1}, LaHI;->a(LaHy;Ljava/lang/Exception;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;LaHy;Z)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 175
    :try_start_0
    iget-object v1, p0, LaHI;->a:Laer;

    invoke-interface {v1, p1}, Laer;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaeF;

    move-result-object v1

    .line 177
    const-string v2, "https://docs.google.com/feeds/default/private/full/"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 178
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1}, LaJT;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v1}, LaJT;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 179
    if-eqz p3, :cond_0

    .line 180
    const-string v1, "delete"

    const-string v3, "true"

    invoke-virtual {v2, v1, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 182
    :cond_0
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 185
    iget-object v2, p0, LaHI;->a:Laer;

    iget-object v3, p1, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    const-string v4, "*"

    invoke-interface {v2, v1, v3, v4}, Laer;->a(Ljava/lang/String;LaFO;Ljava/lang/String;)V
    :try_end_0
    .catch Lbxk; {:try_start_0 .. :try_end_0} :catch_0
    .catch LbwO; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 186
    const/4 v0, 0x1

    .line 195
    :goto_0
    return v0

    .line 187
    :catch_0
    move-exception v1

    .line 188
    invoke-direct {p0, p2, v1}, LaHI;->a(LaHy;Ljava/lang/Exception;)V

    goto :goto_0

    .line 190
    :catch_1
    move-exception v1

    .line 191
    invoke-direct {p0, p2, v1}, LaHI;->a(LaHy;Ljava/lang/Exception;)V

    goto :goto_0

    .line 193
    :catch_2
    move-exception v1

    .line 194
    invoke-direct {p0, p2, v1}, LaHI;->a(LaHy;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/gms/drive/database/data/ResourceSpec;LaHy;)Z
    .locals 6

    .prologue
    .line 37
    const/4 v3, 0x0

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LaHI;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;ZLaHy;)Z

    move-result v0

    return v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;LaHy;)Z
    .locals 6

    .prologue
    .line 43
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LaHI;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;ZLaHy;)Z

    move-result v0

    return v0
.end method

.method public b(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;LaHy;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 104
    iget-object v2, p1, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    .line 105
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a()Ljava/lang/String;

    move-result-object v1

    .line 109
    if-eqz p2, :cond_2

    .line 111
    :try_start_0
    iget-object v3, p0, LaHI;->a:Laer;

    .line 112
    invoke-interface {v3, p2}, Laer;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Ljava/lang/String;

    move-result-object v3

    .line 113
    if-nez v3, :cond_0

    .line 114
    const/4 v1, 0x0

    invoke-direct {p0, p4, v1}, LaHI;->a(LaHy;Ljava/lang/Exception;)V

    .line 143
    :goto_0
    return v0

    .line 117
    :cond_0
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 118
    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    .line 119
    invoke-virtual {v3, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 120
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 121
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 131
    :cond_1
    :goto_1
    invoke-direct {p0, v1}, LaHI;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 132
    iget-object v3, p0, LaHI;->a:Laer;

    invoke-interface {v3, v1, v2, p3}, Laer;->a(Ljava/lang/String;LaFO;Ljava/lang/String;)V

    .line 143
    const/4 v0, 0x1

    goto :goto_0

    .line 123
    :cond_2
    iget-object v1, p0, LaHI;->a:Laer;

    invoke-interface {v1, p1}, Laer;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaeF;

    move-result-object v3

    .line 125
    invoke-interface {v3}, LaJT;->o()Ljava/lang/String;

    move-result-object v1

    .line 126
    if-nez v1, :cond_1

    .line 127
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "https://docs.google.com/feeds/default/private/full/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 128
    invoke-interface {v3}, LaJT;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v3}, LaJT;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lbxk; {:try_start_0 .. :try_end_0} :catch_0
    .catch LbwO; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v1

    goto :goto_1

    .line 133
    :catch_0
    move-exception v1

    .line 134
    invoke-direct {p0, p4, v1}, LaHI;->a(LaHy;Ljava/lang/Exception;)V

    goto :goto_0

    .line 136
    :catch_1
    move-exception v1

    .line 137
    invoke-direct {p0, p4, v1}, LaHI;->a(LaHy;Ljava/lang/Exception;)V

    goto :goto_0

    .line 139
    :catch_2
    move-exception v1

    .line 140
    invoke-direct {p0, p4, v1}, LaHI;->a(LaHy;Ljava/lang/Exception;)V

    goto :goto_0
.end method
