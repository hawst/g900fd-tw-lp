.class public abstract LbvT;
.super Ljava/lang/Object;
.source "Multibinder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LbvU;)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0}, LbvT;-><init>()V

    return-void
.end method

.method static a(LbuP;)LbuP;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbuP",
            "<TT;>;)",
            "LbuP",
            "<",
            "Ljava/util/Set",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 187
    invoke-virtual {p0}, LbuP;->a()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, LbwL;->b(Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    .line 188
    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ljava/util/Map;Lbup;Ljava/lang/Object;Lbup;)Lbur;
    .locals 1

    .prologue
    .line 116
    invoke-static {p0, p1, p2, p3}, LbvT;->b(Ljava/util/Map;Lbup;Ljava/lang/Object;Lbup;)Lbur;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/inject/Binder;LbuP;)LbvT;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/inject/Binder;",
            "LbuP",
            "<TT;>;)",
            "LbvT",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 124
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, LbvV;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, LbvT;

    aput-object v2, v0, v1

    invoke-interface {p0, v0}, Lcom/google/inject/Binder;->a([Ljava/lang/Class;)Lcom/google/inject/Binder;

    move-result-object v1

    .line 125
    new-instance v0, LbvV;

    .line 126
    invoke-static {p1}, LbvT;->a(LbuP;)LbuP;

    move-result-object v2

    invoke-static {v2}, Lbuv;->a(LbuP;)Lbuv;

    move-result-object v3

    invoke-static {p1}, LbvT;->b(LbuP;)LbuP;

    move-result-object v2

    invoke-static {v2}, Lbuv;->a(LbuP;)Lbuv;

    move-result-object v4

    const/4 v5, 0x0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, LbvV;-><init>(Lcom/google/inject/Binder;LbuP;Lbuv;Lbuv;LbvU;)V

    .line 127
    invoke-interface {v1, v0}, Lcom/google/inject/Binder;->a(LbuC;)V

    .line 128
    return-object v0
.end method

.method public static a(Lcom/google/inject/Binder;Ljava/lang/Class;)LbvT;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/inject/Binder;",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LbvT",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 136
    invoke-static {p1}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    invoke-static {p0, v0}, LbvT;->a(Lcom/google/inject/Binder;LbuP;)LbvT;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 539
    if-eqz p0, :cond_0

    .line 540
    return-object p0

    .line 543
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 544
    new-instance v1, Lbur;

    new-instance v2, Lbwy;

    .line 545
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lbwy;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 544
    invoke-static {v2}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v0

    invoke-direct {v1, v0}, Lbur;-><init>(Ljava/lang/Iterable;)V

    throw v1
.end method

.method static varargs a(ZLjava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 502
    if-eqz p0, :cond_0

    .line 503
    return-void

    .line 506
    :cond_0
    new-instance v0, Lbur;

    new-instance v1, Lbwy;

    invoke-static {p1, p2}, Lbvd;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lbwy;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v1

    invoke-direct {v0, v1}, Lbur;-><init>(Ljava/lang/Iterable;)V

    throw v0
.end method

.method static b(LbuP;)LbuP;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbuP",
            "<TT;>;)",
            "LbuP",
            "<",
            "Ljava/util/Collection",
            "<",
            "LbuE",
            "<TT;>;>;>;"
        }
    .end annotation

    .prologue
    .line 194
    invoke-virtual {p0}, LbuP;->a()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, LbwL;->c(Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    .line 195
    const-class v1, Ljava/util/Collection;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/reflect/Type;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    .line 196
    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/util/Map;Lbup;Ljava/lang/Object;Lbup;)Lbur;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map",
            "<TT;",
            "Lbup",
            "<TT;>;>;",
            "Lbup",
            "<TT;>;TT;",
            "Lbup",
            "<TT;>;)",
            "Lbur;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 514
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {p2}, LbiV;->a(Ljava/lang/Object;)LbiU;

    move-result-object v1

    invoke-static {v0, v1}, Lbnm;->a(Ljava/lang/Iterable;LbiU;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lbnm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v1

    .line 515
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 516
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 517
    invoke-static {v0, v2}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 519
    new-instance v0, Lbur;

    new-instance v1, Lbwy;

    const-string v2, "Set injection failed due to duplicated element \"%s\"\n    Bound at %s\n    Bound at %s"

    new-array v3, v8, [Ljava/lang/Object;

    aput-object p2, v3, v5

    .line 523
    invoke-interface {p3}, Lbup;->b()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v6

    .line 524
    invoke-interface {p1}, Lbup;->b()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v7

    .line 519
    invoke-static {v2, v3}, Lbvd;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lbwy;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v1

    invoke-direct {v0, v1}, Lbur;-><init>(Ljava/lang/Iterable;)V

    .line 527
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lbur;

    new-instance v2, Lbwy;

    const-string v3, "Set injection failed due to multiple elements comparing equal:\n    \"%s\"\n        bound at %s\n    \"%s\"\n        bound at %s"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    .line 532
    invoke-interface {p3}, Lbup;->b()Ljava/lang/Object;

    move-result-object v1

    aput-object v1, v4, v6

    aput-object p2, v4, v7

    .line 534
    invoke-interface {p1}, Lbup;->b()Ljava/lang/Object;

    move-result-object v1

    aput-object v1, v4, v8

    .line 527
    invoke-static {v3, v4}, Lbvd;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lbwy;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v1

    invoke-direct {v0, v1}, Lbur;-><init>(Ljava/lang/Iterable;)V

    goto :goto_0
.end method


# virtual methods
.method public abstract a()LbuT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbuT",
            "<TT;>;"
        }
    .end annotation
.end method
