.class LblG;
.super LblB;
.source "AbstractMapBasedMultimap.java"

# interfaces
.implements Ljava/util/SortedSet;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbls",
        "<TK;TV;>.blB;",
        "Ljava/util/SortedSet",
        "<TV;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lbls;


# direct methods
.method constructor <init>(Lbls;Ljava/lang/Object;Ljava/util/SortedSet;LblB;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/util/SortedSet",
            "<TV;>;",
            "Lbls",
            "<TK;TV;>.blB;)V"
        }
    .end annotation

    .prologue
    .line 628
    iput-object p1, p0, LblG;->a:Lbls;

    .line 629
    invoke-direct {p0, p1, p2, p3, p4}, LblB;-><init>(Lbls;Ljava/lang/Object;Ljava/util/Collection;LblB;)V

    .line 630
    return-void
.end method


# virtual methods
.method a()Ljava/util/SortedSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/SortedSet",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 633
    invoke-virtual {p0}, LblG;->a()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-TV;>;"
        }
    .end annotation

    .prologue
    .line 637
    invoke-virtual {p0}, LblG;->a()Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedSet;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public first()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 641
    invoke-virtual {p0}, LblG;->a()V

    .line 642
    invoke-virtual {p0}, LblG;->a()Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)",
            "Ljava/util/SortedSet",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 651
    invoke-virtual {p0}, LblG;->a()V

    .line 652
    new-instance v0, LblG;

    iget-object v1, p0, LblG;->a:Lbls;

    invoke-virtual {p0}, LblG;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0}, LblG;->a()Ljava/util/SortedSet;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/SortedSet;->headSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v3

    invoke-virtual {p0}, LblG;->a()LblB;

    move-result-object v4

    if-nez v4, :cond_0

    :goto_0
    invoke-direct {v0, v1, v2, v3, p0}, LblG;-><init>(Lbls;Ljava/lang/Object;Ljava/util/SortedSet;LblB;)V

    return-object v0

    :cond_0
    invoke-virtual {p0}, LblG;->a()LblB;

    move-result-object p0

    goto :goto_0
.end method

.method public last()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 646
    invoke-virtual {p0}, LblG;->a()V

    .line 647
    invoke-virtual {p0}, LblG;->a()Ljava/util/SortedSet;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedSet;->last()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;TV;)",
            "Ljava/util/SortedSet",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 657
    invoke-virtual {p0}, LblG;->a()V

    .line 658
    new-instance v0, LblG;

    iget-object v1, p0, LblG;->a:Lbls;

    invoke-virtual {p0}, LblG;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0}, LblG;->a()Ljava/util/SortedSet;

    move-result-object v3

    invoke-interface {v3, p1, p2}, Ljava/util/SortedSet;->subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v3

    invoke-virtual {p0}, LblG;->a()LblB;

    move-result-object v4

    if-nez v4, :cond_0

    :goto_0
    invoke-direct {v0, v1, v2, v3, p0}, LblG;-><init>(Lbls;Ljava/lang/Object;Ljava/util/SortedSet;LblB;)V

    return-object v0

    :cond_0
    invoke-virtual {p0}, LblG;->a()LblB;

    move-result-object p0

    goto :goto_0
.end method

.method public tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)",
            "Ljava/util/SortedSet",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 663
    invoke-virtual {p0}, LblG;->a()V

    .line 664
    new-instance v0, LblG;

    iget-object v1, p0, LblG;->a:Lbls;

    invoke-virtual {p0}, LblG;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0}, LblG;->a()Ljava/util/SortedSet;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/SortedSet;->tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v3

    invoke-virtual {p0}, LblG;->a()LblB;

    move-result-object v4

    if-nez v4, :cond_0

    :goto_0
    invoke-direct {v0, v1, v2, v3, p0}, LblG;-><init>(Lbls;Ljava/lang/Object;Ljava/util/SortedSet;LblB;)V

    return-object v0

    :cond_0
    invoke-virtual {p0}, LblG;->a()LblB;

    move-result-object p0

    goto :goto_0
.end method
