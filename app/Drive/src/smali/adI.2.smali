.class public final enum LadI;
.super Ljava/lang/Enum;
.source "FileProvider.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LadI;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LadI;

.field private static final synthetic a:[LadI;

.field public static final enum b:LadI;

.field public static final enum c:LadI;


# instance fields
.field private final a:C

.field private final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 109
    new-instance v0, LadI;

    const-string v1, "READ"

    const/16 v2, 0x72

    const/high16 v3, 0x10000000

    invoke-direct {v0, v1, v4, v2, v3}, LadI;-><init>(Ljava/lang/String;ICI)V

    sput-object v0, LadI;->a:LadI;

    .line 110
    new-instance v0, LadI;

    const-string v1, "WRITE"

    const/16 v2, 0x77

    const/high16 v3, 0x20000000

    invoke-direct {v0, v1, v5, v2, v3}, LadI;-><init>(Ljava/lang/String;ICI)V

    sput-object v0, LadI;->b:LadI;

    .line 111
    new-instance v0, LadI;

    const-string v1, "TRUNCATE"

    const/16 v2, 0x74

    const/high16 v3, 0x4000000

    invoke-direct {v0, v1, v6, v2, v3}, LadI;-><init>(Ljava/lang/String;ICI)V

    sput-object v0, LadI;->c:LadI;

    .line 108
    const/4 v0, 0x3

    new-array v0, v0, [LadI;

    sget-object v1, LadI;->a:LadI;

    aput-object v1, v0, v4

    sget-object v1, LadI;->b:LadI;

    aput-object v1, v0, v5

    sget-object v1, LadI;->c:LadI;

    aput-object v1, v0, v6

    sput-object v0, LadI;->a:[LadI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ICI)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(CI)V"
        }
    .end annotation

    .prologue
    .line 116
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 117
    iput-char p3, p0, LadI;->a:C

    .line 118
    iput p4, p0, LadI;->a:I

    .line 119
    return-void
.end method

.method public static a(LbmY;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "LadI;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 136
    const/4 v0, 0x0

    .line 137
    invoke-virtual {p0}, LbmY;->a()Lbqv;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LadI;

    .line 138
    iget v0, v0, LadI;->a:I

    or-int/2addr v0, v1

    move v1, v0

    .line 139
    goto :goto_0

    .line 141
    :cond_0
    return v1
.end method

.method public static a(Ljava/lang/String;)LbmY;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LbmY",
            "<",
            "LadI;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    invoke-static {}, LbmY;->a()Lbna;

    move-result-object v1

    .line 123
    invoke-static {}, LadI;->values()[LadI;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 124
    iget-char v5, v4, LadI;->a:C

    invoke-static {v5}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 125
    invoke-virtual {v1, v4}, Lbna;->a(Ljava/lang/Object;)Lbna;

    .line 123
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 128
    :cond_1
    invoke-virtual {v1}, Lbna;->a()LbmY;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LadI;
    .locals 1

    .prologue
    .line 108
    const-class v0, LadI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LadI;

    return-object v0
.end method

.method public static values()[LadI;
    .locals 1

    .prologue
    .line 108
    sget-object v0, LadI;->a:[LadI;

    invoke-virtual {v0}, [LadI;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LadI;

    return-object v0
.end method
