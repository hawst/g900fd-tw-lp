.class public final LaeZ;
.super Lafe;
.source "DocumentFeedAndFolderFeedFilter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lafe",
        "<",
        "Lafi;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LaeZ;

.field public static final b:LaeZ;

.field public static final c:LaeZ;

.field public static final d:LaeZ;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 44
    new-instance v0, LaeZ;

    sget-object v1, Lafi;->a:Lafi;

    sget-object v2, Lafi;->a:Lafi;

    invoke-direct {v0, v1, v2}, LaeZ;-><init>(Lafi;Lafi;)V

    sput-object v0, LaeZ;->a:LaeZ;

    .line 50
    new-instance v0, LaeZ;

    sget-object v1, Lafi;->a:Lafi;

    sget-object v2, Lafi;->b:Lafi;

    invoke-direct {v0, v1, v2}, LaeZ;-><init>(Lafi;Lafi;)V

    sput-object v0, LaeZ;->b:LaeZ;

    .line 56
    new-instance v0, LaeZ;

    sget-object v1, Lafi;->b:Lafi;

    sget-object v2, Lafi;->a:Lafi;

    invoke-direct {v0, v1, v2}, LaeZ;-><init>(Lafi;Lafi;)V

    sput-object v0, LaeZ;->c:LaeZ;

    .line 62
    new-instance v0, LaeZ;

    sget-object v1, Lafi;->b:Lafi;

    sget-object v2, Lafi;->b:Lafi;

    invoke-direct {v0, v1, v2}, LaeZ;-><init>(Lafi;Lafi;)V

    sput-object v0, LaeZ;->d:LaeZ;

    return-void
.end method

.method constructor <init>(Lafi;Lafi;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lafe;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 32
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    return-void
.end method

.method public static a()LaeZ;
    .locals 2

    .prologue
    .line 150
    const/4 v0, 0x0

    invoke-static {v0}, Lafi;->a(Z)Lafi;

    move-result-object v0

    .line 152
    new-instance v1, LaeZ;

    invoke-direct {v1, v0, v0}, LaeZ;-><init>(Lafi;Lafi;)V

    return-object v1
.end method

.method public static a(LaFV;)LaeZ;
    .locals 2

    .prologue
    .line 105
    invoke-interface {p0}, LaFV;->a()Z

    move-result v0

    .line 106
    invoke-interface {p0}, LaFV;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v1

    .line 107
    if-nez v1, :cond_0

    .line 109
    sget-object v0, LaeZ;->d:LaeZ;

    .line 111
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v1, v0}, LaeZ;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Z)LaeZ;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LaGv;)LaeZ;
    .locals 2

    .prologue
    .line 77
    sget-object v0, LaGv;->a:LaGv;

    sget-object v1, LaGv;->k:LaGv;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 78
    invoke-virtual {p0}, LaGv;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaeZ;->a(Ljava/lang/String;)LaeZ;

    move-result-object v0

    return-object v0

    .line 77
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LbmY;)LaeZ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "Lafu;",
            ">;)",
            "LaeZ;"
        }
    .end annotation

    .prologue
    .line 121
    new-instance v0, LaeZ;

    invoke-static {p0}, Lafi;->a(LbmY;)Lafi;

    move-result-object v1

    .line 122
    invoke-static {p0}, Lafi;->a(LbmY;)Lafi;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LaeZ;-><init>(Lafi;Lafi;)V

    return-object v0
.end method

.method public static a(LbmY;LbmY;)LaeZ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "LaGv;",
            ">;",
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LaeZ;"
        }
    .end annotation

    .prologue
    .line 189
    invoke-static {p0, p1}, Lafi;->a(Ljava/util/Set;Ljava/util/Set;)Lafi;

    move-result-object v0

    .line 190
    new-instance v1, LaeZ;

    sget-object v2, Lafi;->b:Lafi;

    invoke-direct {v1, v0, v2}, LaeZ;-><init>(Lafi;Lafi;)V

    return-object v1
.end method

.method public static a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaeZ;
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x1

    invoke-static {p0, v0}, LaeZ;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Z)LaeZ;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Z)LaeZ;
    .locals 2

    .prologue
    .line 140
    const/4 v0, 0x0

    .line 142
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a()Ljava/lang/String;

    move-result-object v1

    .line 141
    invoke-static {v1, p1, v0}, Lafi;->a(Ljava/lang/String;ZZ)Lafi;

    move-result-object v0

    .line 143
    new-instance v1, LaeZ;

    invoke-direct {v1, v0, v0}, LaeZ;-><init>(Lafi;Lafi;)V

    return-object v1
.end method

.method public static a(Ljava/lang/String;)LaeZ;
    .locals 3

    .prologue
    .line 69
    new-instance v0, LaeZ;

    .line 70
    invoke-static {p0}, Lafi;->a(Ljava/lang/String;)Lafi;

    move-result-object v1

    sget-object v2, Lafi;->b:Lafi;

    invoke-direct {v0, v1, v2}, LaeZ;-><init>(Lafi;Lafi;)V

    return-object v0
.end method

.method public static b(LbmY;)LaeZ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "LaGv;",
            ">;)",
            "LaeZ;"
        }
    .end annotation

    .prologue
    .line 167
    invoke-static {p0}, Lafi;->a(Ljava/util/Set;)Lafi;

    move-result-object v0

    .line 168
    new-instance v1, LaeZ;

    sget-object v2, Lafi;->b:Lafi;

    invoke-direct {v1, v0, v2}, LaeZ;-><init>(Lafi;Lafi;)V

    return-object v1
.end method

.method public static b(Ljava/lang/String;)LaeZ;
    .locals 2

    .prologue
    .line 87
    invoke-static {p0}, Lafi;->a(Ljava/lang/String;)Lafi;

    move-result-object v0

    .line 88
    new-instance v1, LaeZ;

    invoke-direct {v1, v0, v0}, LaeZ;-><init>(Lafi;Lafi;)V

    return-object v1
.end method

.method public static c(LbmY;)LaeZ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LaeZ;"
        }
    .end annotation

    .prologue
    .line 178
    sget-object v0, LaGv;->a:LaGv;

    invoke-virtual {v0}, LaGv;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 180
    invoke-static {p0}, Lafi;->b(Ljava/util/Set;)Lafi;

    move-result-object v0

    .line 181
    new-instance v1, LaeZ;

    sget-object v2, Lafi;->b:Lafi;

    invoke-direct {v1, v0, v2}, LaeZ;-><init>(Lafi;Lafi;)V

    return-object v1

    .line 178
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)LaeZ;
    .locals 3

    .prologue
    .line 159
    invoke-static {p0}, Lafi;->b(Ljava/lang/String;)Lafi;

    move-result-object v0

    .line 160
    new-instance v1, LaeZ;

    sget-object v2, Lafi;->b:Lafi;

    invoke-direct {v1, v0, v2}, LaeZ;-><init>(Lafi;Lafi;)V

    return-object v1
.end method


# virtual methods
.method public a(LaeZ;)LaeZ;
    .locals 4

    .prologue
    .line 37
    new-instance v2, LaeZ;

    iget-object v0, p0, LaeZ;->a:Ljava/lang/Object;

    check-cast v0, Lafi;

    iget-object v1, p1, LaeZ;->a:Ljava/lang/Object;

    check-cast v1, Lafi;

    invoke-virtual {v0, v1}, Lafi;->a(Lafi;)Lafi;

    move-result-object v3

    iget-object v0, p0, LaeZ;->b:Ljava/lang/Object;

    check-cast v0, Lafi;

    iget-object v1, p1, LaeZ;->b:Ljava/lang/Object;

    check-cast v1, Lafi;

    .line 38
    invoke-virtual {v0, v1}, Lafi;->a(Lafi;)Lafi;

    move-result-object v0

    invoke-direct {v2, v3, v0}, LaeZ;-><init>(Lafi;Lafi;)V

    return-object v2
.end method
