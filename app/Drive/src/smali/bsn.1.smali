.class public Lbsn;
.super Ljava/lang/Object;
.source "Keys.java"


# direct methods
.method public static a(Lbuv;)Lbuv;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbuv",
            "<*>;)",
            "Lbuv",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 28
    invoke-virtual {p0}, Lbuv;->a()LbuP;

    move-result-object v0

    invoke-virtual {v0}, LbuP;->a()Ljava/lang/Class;

    move-result-object v0

    .line 29
    const-class v1, LbuE;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 30
    const/4 v0, 0x0

    .line 55
    :goto_0
    return-object v0

    .line 33
    :cond_0
    invoke-virtual {p0}, Lbuv;->a()LbuP;

    move-result-object v0

    invoke-virtual {v0}, LbuP;->a()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 34
    instance-of v0, v1, Ljava/lang/reflect/ParameterizedType;

    const-string v4, "Provider must be a parameterized type: %s"

    new-array v5, v2, [Ljava/lang/Object;

    aput-object v1, v5, v3

    invoke-static {v0, v4, v5}, Lbso;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 37
    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v0

    .line 38
    array-length v0, v0

    if-ne v0, v2, :cond_1

    move v0, v2

    :goto_1
    const-string v2, "Provider must have exactly one type argument."

    new-array v4, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v4}, Lbso;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 41
    check-cast v1, Ljava/lang/reflect/ParameterizedType;

    invoke-interface {v1}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v0

    aget-object v0, v0, v3

    .line 42
    invoke-virtual {p0}, Lbuv;->a()Ljava/lang/annotation/Annotation;

    move-result-object v1

    .line 44
    if-eqz v1, :cond_2

    .line 45
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/reflect/Type;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v3

    .line 38
    goto :goto_1

    .line 47
    :cond_2
    invoke-virtual {p0}, Lbuv;->a()Ljava/lang/Class;

    move-result-object v1

    .line 48
    if-eqz v1, :cond_3

    .line 49
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/reflect/Type;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    goto :goto_0

    .line 51
    :cond_3
    invoke-static {v0}, Lbuv;->a(Ljava/lang/reflect/Type;)Lbuv;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lbuv;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbuv",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 62
    invoke-virtual {p0}, Lbuv;->a()Ljava/lang/Class;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbuv;->a()LbuP;

    move-result-object v0

    invoke-virtual {v0}, LbuP;->a()Ljava/lang/reflect/Type;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/Class;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
