.class public LKT;
.super Landroid/widget/BaseAdapter;
.source "MenuFunctionViewManagerBase.java"

# interfaces
.implements Landroid/widget/SpinnerAdapter;


# instance fields
.field private final a:Landroid/widget/ListPopupWindow;

.field private final a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "LKP;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/widget/ListPopupWindow;LbmF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ListPopupWindow;",
            "LbmF",
            "<",
            "LKP;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 28
    iput-object p1, p0, LKT;->a:Landroid/widget/ListPopupWindow;

    .line 29
    iput-object p2, p0, LKT;->a:LbmF;

    .line 30
    return-void
.end method


# virtual methods
.method public a(I)LKP;
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, LKT;->a:LbmF;

    iget-object v1, p0, LKT;->a:LbmF;

    invoke-virtual {v1}, LbmF;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    sub-int/2addr v1, p1

    invoke-virtual {v0, v1}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKP;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, LKT;->a:LbmF;

    invoke-virtual {v0}, LbmF;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0, p1}, LKT;->a(I)LKP;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 45
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 0

    .prologue
    .line 50
    return p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 60
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    if-eqz p2, :cond_0

    .line 65
    :goto_0
    return-object p2

    .line 64
    :cond_0
    invoke-virtual {p0, p1}, LKT;->a(I)LKP;

    move-result-object v0

    .line 65
    iget-object v1, p0, LKT;->a:Landroid/widget/ListPopupWindow;

    invoke-interface {v0, p3, v1}, LKP;->a(Landroid/view/ViewGroup;Landroid/widget/ListPopupWindow;)Landroid/view/View;

    move-result-object p2

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, LKT;->getCount()I

    move-result v0

    return v0
.end method
