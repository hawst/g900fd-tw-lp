.class public final LvH;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 37
    iput-object p1, p0, LvH;->a:LbrA;

    .line 38
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 162
    .line 164
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 191
    .line 193
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 148
    const-class v0, LvC;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xb4

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LvH;->a(LbuP;LbuB;)V

    .line 151
    const-class v0, Lcom/google/android/apps/docs/app/detailpanel/DetailActivityDelegate;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0xb5

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LvH;->a(LbuP;LbuB;)V

    .line 154
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 171
    packed-switch p1, :pswitch_data_0

    .line 185
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 173
    :pswitch_0
    check-cast p2, LvC;

    .line 175
    iget-object v0, p0, LvH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LvH;

    .line 176
    invoke-virtual {v0, p2}, LvH;->a(LvC;)V

    .line 187
    :goto_0
    return-void

    .line 179
    :pswitch_1
    check-cast p2, Lcom/google/android/apps/docs/app/detailpanel/DetailActivityDelegate;

    .line 181
    iget-object v0, p0, LvH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LvH;

    .line 182
    invoke-virtual {v0, p2}, LvH;->a(Lcom/google/android/apps/docs/app/detailpanel/DetailActivityDelegate;)V

    goto :goto_0

    .line 171
    :pswitch_data_0
    .packed-switch 0xb4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/docs/app/detailpanel/DetailActivityDelegate;)V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, LvH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LvH;

    .line 142
    invoke-virtual {v0, p1}, LvH;->a(LvC;)V

    .line 143
    return-void
.end method

.method public a(LvC;)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, LvH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 45
    invoke-virtual {v0, p1}, LtQ;->a(Lrd;)V

    .line 46
    iget-object v0, p0, LvH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LPt;

    iget-object v0, v0, LPt;->d:Lbsk;

    .line 49
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LvH;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LPt;

    iget-object v1, v1, LPt;->d:Lbsk;

    .line 47
    invoke-static {v0, v1}, LvH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPp;

    iput-object v0, p1, LvC;->a:LPp;

    .line 53
    iget-object v0, p0, LvH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lwc;

    iget-object v0, v0, Lwc;->j:Lbsk;

    .line 56
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LvH;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lwc;

    iget-object v1, v1, Lwc;->j:Lbsk;

    .line 54
    invoke-static {v0, v1}, LvH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvU;

    iput-object v0, p1, LvC;->a:LvU;

    .line 60
    iget-object v0, p0, LvH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LCw;

    iget-object v0, v0, LCw;->M:Lbsk;

    .line 63
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LvH;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->M:Lbsk;

    .line 61
    invoke-static {v0, v1}, LvH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBZ;

    iput-object v0, p1, LvC;->a:LBZ;

    .line 67
    iget-object v0, p0, LvH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LadM;

    iget-object v0, v0, LadM;->h:Lbsk;

    .line 70
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LvH;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LadM;

    iget-object v1, v1, LadM;->h:Lbsk;

    .line 68
    invoke-static {v0, v1}, LvH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladf;

    iput-object v0, p1, LvC;->a:Ladf;

    .line 74
    iget-object v0, p0, LvH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->f:Lbsk;

    .line 77
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LvH;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->f:Lbsk;

    .line 75
    invoke-static {v0, v1}, LvH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGR;

    iput-object v0, p1, LvC;->a:LaGR;

    .line 81
    iget-object v0, p0, LvH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lwc;

    iget-object v0, v0, Lwc;->h:Lbsk;

    .line 84
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LvH;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lwc;

    iget-object v1, v1, Lwc;->h:Lbsk;

    .line 82
    invoke-static {v0, v1}, LvH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwa;

    iput-object v0, p1, LvC;->a:Lwa;

    .line 88
    iget-object v0, p0, LvH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 91
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LvH;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 89
    invoke-static {v0, v1}, LvH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p1, LvC;->a:LtK;

    .line 95
    iget-object v0, p0, LvH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LVq;

    iget-object v0, v0, LVq;->d:Lbsk;

    .line 98
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LvH;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LVq;

    iget-object v1, v1, LVq;->d:Lbsk;

    .line 96
    invoke-static {v0, v1}, LvH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LVm;

    iput-object v0, p1, LvC;->a:LVm;

    .line 102
    iget-object v0, p0, LvH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->g:Lbsk;

    .line 105
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LvH;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LabP;

    iget-object v1, v1, LabP;->g:Lbsk;

    .line 103
    invoke-static {v0, v1}, LvH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacF;

    iput-object v0, p1, LvC;->a:LacF;

    .line 109
    iget-object v0, p0, LvH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->e:Lbsk;

    .line 112
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LvH;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LabP;

    iget-object v1, v1, LabP;->e:Lbsk;

    .line 110
    invoke-static {v0, v1}, LvH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacj;

    iput-object v0, p1, LvC;->a:Lacj;

    .line 116
    iget-object v0, p0, LvH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 119
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LvH;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 117
    invoke-static {v0, v1}, LvH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p1, LvC;->a:LaGM;

    .line 123
    iget-object v0, p0, LvH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    iget-object v0, v0, LtQ;->w:Lbsk;

    .line 126
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LvH;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LtQ;

    iget-object v1, v1, LtQ;->w:Lbsk;

    .line 124
    invoke-static {v0, v1}, LvH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LsP;

    iput-object v0, p1, LvC;->a:LsP;

    .line 130
    iget-object v0, p0, LvH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaHX;

    iget-object v0, v0, LaHX;->m:Lbsk;

    .line 133
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LvH;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaHX;

    iget-object v1, v1, LaHX;->m:Lbsk;

    .line 131
    invoke-static {v0, v1}, LvH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaIm;

    iput-object v0, p1, LvC;->a:LaIm;

    .line 137
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 158
    return-void
.end method
