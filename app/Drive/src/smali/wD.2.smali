.class public LwD;
.super Ljava/lang/Object;
.source "FileCacheStore.java"

# interfaces
.implements LwB;


# instance fields
.field private final a:Lalo;

.field private final a:Ljava/io/File;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LwC;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lalo;Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LwD;->a:Ljava/util/List;

    .line 63
    iput-object p1, p0, LwD;->a:Lalo;

    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "apps_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 65
    const/4 v1, 0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, LwD;->a:Ljava/io/File;

    .line 66
    return-void
.end method

.method static synthetic a(LwD;)Lalo;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, LwD;->a:Lalo;

    return-object v0
.end method

.method static synthetic a(LwD;)Ljava/io/File;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, LwD;->a:Ljava/io/File;

    return-object v0
.end method


# virtual methods
.method public a()LbmF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmF",
            "<",
            "LwC;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, LwD;->a:Ljava/util/List;

    invoke-static {v0}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)LwC;
    .locals 2

    .prologue
    .line 70
    new-instance v0, LwF;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, LwF;-><init>(LwD;Ljava/lang/String;LwE;)V

    .line 71
    iget-object v1, p0, LwD;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, LwD;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LwC;

    .line 78
    invoke-interface {v0}, LwC;->a()V

    goto :goto_0

    .line 80
    :cond_0
    iget-object v0, p0, LwD;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 81
    iget-object v0, p0, LwD;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 82
    return-void
.end method
