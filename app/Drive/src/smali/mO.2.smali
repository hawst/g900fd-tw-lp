.class LmO;
.super Landroid/support/v7/internal/widget/ListViewCompat;
.source "ListPopupWindow.java"


# instance fields
.field private a:LeZ;

.field private a:LgM;

.field private a:Z

.field private b:Z

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 1535
    const/4 v0, 0x0

    sget v1, Liq;->dropDownListViewStyle:I

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v7/internal/widget/ListViewCompat;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1536
    iput-boolean p2, p0, LmO;->b:Z

    .line 1537
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LmO;->setCacheColorHint(I)V

    .line 1538
    return-void
.end method

.method private a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 1607
    invoke-virtual {p0, p2}, LmO;->getItemIdAtPosition(I)J

    move-result-wide v0

    .line 1608
    invoke-virtual {p0, p1, p2, v0, v1}, LmO;->performItemClick(Landroid/view/View;IJ)Z

    .line 1609
    return-void
.end method

.method private a(Landroid/view/View;IFF)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1624
    iput-boolean v0, p0, LmO;->c:Z

    .line 1628
    invoke-virtual {p0, v0}, LmO;->setPressed(Z)V

    .line 1629
    invoke-virtual {p0}, LmO;->layoutChildren()V

    .line 1632
    invoke-virtual {p0, p2}, LmO;->setSelection(I)V

    .line 1633
    invoke-virtual {p0, p2, p1, p3, p4}, LmO;->a(ILandroid/view/View;FF)V

    .line 1638
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LmO;->a(Z)V

    .line 1642
    invoke-virtual {p0}, LmO;->refreshDrawableState()V

    .line 1643
    return-void
.end method

.method static synthetic a(LmO;Z)Z
    .locals 0

    .prologue
    .line 1483
    iput-boolean p1, p0, LmO;->a:Z

    return p1
.end method

.method private b()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1612
    iput-boolean v0, p0, LmO;->c:Z

    .line 1613
    invoke-virtual {p0, v0}, LmO;->setPressed(Z)V

    .line 1615
    invoke-virtual {p0}, LmO;->drawableStateChanged()V

    .line 1617
    iget-object v0, p0, LmO;->a:LeZ;

    if-eqz v0, :cond_0

    .line 1618
    iget-object v0, p0, LmO;->a:LeZ;

    invoke-virtual {v0}, LeZ;->a()V

    .line 1619
    const/4 v0, 0x0

    iput-object v0, p0, LmO;->a:LeZ;

    .line 1621
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/view/MotionEvent;I)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1547
    .line 1550
    invoke-static {p1}, LdH;->a(Landroid/view/MotionEvent;)I

    move-result v3

    .line 1551
    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    move v0, v1

    move v3, v2

    .line 1584
    :goto_1
    if-eqz v3, :cond_1

    if-eqz v0, :cond_2

    .line 1585
    :cond_1
    invoke-direct {p0}, LmO;->b()V

    .line 1589
    :cond_2
    if-eqz v3, :cond_7

    .line 1590
    iget-object v0, p0, LmO;->a:LgM;

    if-nez v0, :cond_3

    .line 1591
    new-instance v0, LgM;

    invoke-direct {v0, p0}, LgM;-><init>(Landroid/widget/ListView;)V

    iput-object v0, p0, LmO;->a:LgM;

    .line 1593
    :cond_3
    iget-object v0, p0, LmO;->a:LgM;

    invoke-virtual {v0, v2}, LgM;->a(Z)Lgg;

    .line 1594
    iget-object v0, p0, LmO;->a:LgM;

    invoke-virtual {v0, p0, p1}, LgM;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 1599
    :cond_4
    :goto_2
    return v3

    :pswitch_0
    move v0, v1

    move v3, v1

    .line 1554
    goto :goto_1

    :pswitch_1
    move v0, v1

    .line 1559
    :goto_3
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v4

    .line 1560
    if-gez v4, :cond_5

    move v0, v1

    move v3, v1

    .line 1562
    goto :goto_1

    .line 1565
    :cond_5
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    float-to-int v5, v5

    .line 1566
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    float-to-int v4, v4

    .line 1567
    invoke-virtual {p0, v5, v4}, LmO;->pointToPosition(II)I

    move-result v6

    .line 1568
    const/4 v7, -0x1

    if-ne v6, v7, :cond_6

    move v3, v0

    move v0, v2

    .line 1570
    goto :goto_1

    .line 1573
    :cond_6
    invoke-virtual {p0}, LmO;->getFirstVisiblePosition()I

    move-result v0

    sub-int v0, v6, v0

    invoke-virtual {p0, v0}, LmO;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1574
    int-to-float v5, v5

    int-to-float v4, v4

    invoke-direct {p0, v0, v6, v5, v4}, LmO;->a(Landroid/view/View;IFF)V

    .line 1577
    if-ne v3, v2, :cond_0

    .line 1578
    invoke-direct {p0, v0, v6}, LmO;->a(Landroid/view/View;I)V

    goto :goto_0

    .line 1595
    :cond_7
    iget-object v0, p0, LmO;->a:LgM;

    if-eqz v0, :cond_4

    .line 1596
    iget-object v0, p0, LmO;->a:LgM;

    invoke-virtual {v0, v1}, LgM;->a(Z)Lgg;

    goto :goto_2

    :pswitch_2
    move v0, v2

    goto :goto_3

    .line 1551
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method protected b()Z
    .locals 1

    .prologue
    .line 1647
    iget-boolean v0, p0, LmO;->c:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/support/v7/internal/widget/ListViewCompat;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFocus()Z
    .locals 1

    .prologue
    .line 1683
    iget-boolean v0, p0, LmO;->b:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/support/v7/internal/widget/ListViewCompat;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWindowFocus()Z
    .locals 1

    .prologue
    .line 1663
    iget-boolean v0, p0, LmO;->b:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/support/v7/internal/widget/ListViewCompat;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFocused()Z
    .locals 1

    .prologue
    .line 1673
    iget-boolean v0, p0, LmO;->b:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/support/v7/internal/widget/ListViewCompat;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInTouchMode()Z
    .locals 1

    .prologue
    .line 1653
    iget-boolean v0, p0, LmO;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LmO;->a:Z

    if-nez v0, :cond_1

    :cond_0
    invoke-super {p0}, Landroid/support/v7/internal/widget/ListViewCompat;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
