.class public LTs;
.super Ljava/lang/Object;
.source "RedirectHelper.java"


# direct methods
.method public static a(LTh;LaFO;Ljava/net/URI;IZ)Lorg/apache/http/HttpResponse;
    .locals 2

    .prologue
    .line 61
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, p2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 62
    if-eqz p4, :cond_0

    .line 63
    invoke-interface {p0, v0}, LTh;->a(Lorg/apache/http/HttpRequest;)V

    .line 66
    :cond_0
    const/4 v1, 0x0

    invoke-static {p0, p1, v0, p3, v1}, LTs;->a(LTh;LaFO;Lorg/apache/http/client/methods/HttpRequestBase;ILjava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public static a(LTh;LaFO;Lorg/apache/http/client/methods/HttpRequestBase;ILjava/lang/String;)Lorg/apache/http/HttpResponse;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 86
    invoke-virtual {p2}, Lorg/apache/http/client/methods/HttpRequestBase;->getURI()Ljava/net/URI;

    move-result-object v1

    .line 87
    invoke-virtual {v1}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    .line 89
    new-instance v0, Ljava/net/URI;

    const-string v3, "https"

    .line 90
    invoke-virtual {v1}, Ljava/net/URI;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Ljava/net/URI;->getFragment()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v4, v1}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    invoke-virtual {p2, v0}, Lorg/apache/http/client/methods/HttpRequestBase;->setURI(Ljava/net/URI;)V

    .line 93
    :goto_0
    invoke-virtual {v0}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 94
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Request URI host should not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v1, v2

    move-object v2, v0

    .line 97
    :goto_1
    if-gt v1, p3, :cond_3

    .line 98
    if-nez p4, :cond_2

    invoke-interface {p0, p1, p2}, LTh;->a(LaFO;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 101
    :goto_2
    :try_start_0
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    .line 102
    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v4

    const/16 v5, 0x12e

    if-eq v4, v5, :cond_1

    .line 103
    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    const/16 v4, 0x12d

    if-ne v3, v4, :cond_4

    .line 104
    :cond_1
    new-instance v3, Ljava/net/URI;

    const-string v4, "Location"

    invoke-interface {v0, v4}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v0

    const/4 v4, 0x0

    aget-object v0, v0, v4

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 105
    invoke-virtual {v2, v3}, Ljava/net/URI;->resolve(Ljava/net/URI;)Ljava/net/URI;

    move-result-object v2

    .line 106
    invoke-virtual {p2, v2}, Lorg/apache/http/client/methods/HttpRequestBase;->setURI(Ljava/net/URI;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    invoke-interface {p0}, LTh;->a()V

    .line 115
    invoke-interface {p0}, LTh;->b()V

    .line 97
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 99
    :cond_2
    invoke-interface {p0, p1, p2, p4}, LTh;->a(LaFO;Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    goto :goto_2

    .line 113
    :catchall_0
    move-exception v0

    .line 114
    invoke-interface {p0}, LTh;->a()V

    .line 115
    invoke-interface {p0}, LTh;->b()V

    throw v0

    .line 120
    :cond_3
    new-instance v0, LTt;

    const-string v1, "Excessive redirects."

    invoke-direct {v0, v1}, LTt;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_4
    return-object v0

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method
