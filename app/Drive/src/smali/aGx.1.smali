.class public final enum LaGx;
.super Ljava/lang/Enum;
.source "Entry.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaGx;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaGx;

.field private static final synthetic a:[LaGx;

.field public static final enum b:LaGx;

.field public static final enum c:LaGx;


# instance fields
.field private final a:J


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 111
    new-instance v0, LaGx;

    const-string v1, "UNKNOWN"

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v4, v2, v3}, LaGx;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, LaGx;->a:LaGx;

    .line 112
    new-instance v0, LaGx;

    const-string v1, "HAS_THUMBNAIL"

    const-wide/16 v2, 0x1

    invoke-direct {v0, v1, v5, v2, v3}, LaGx;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, LaGx;->b:LaGx;

    .line 113
    new-instance v0, LaGx;

    const-string v1, "NO_THUMBNAIL"

    const-wide/16 v2, 0x2

    invoke-direct {v0, v1, v6, v2, v3}, LaGx;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, LaGx;->c:LaGx;

    .line 110
    const/4 v0, 0x3

    new-array v0, v0, [LaGx;

    sget-object v1, LaGx;->a:LaGx;

    aput-object v1, v0, v4

    sget-object v1, LaGx;->b:LaGx;

    aput-object v1, v0, v5

    sget-object v1, LaGx;->c:LaGx;

    aput-object v1, v0, v6

    sput-object v0, LaGx;->a:[LaGx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IJ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)V"
        }
    .end annotation

    .prologue
    .line 118
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 119
    iput-wide p3, p0, LaGx;->a:J

    .line 120
    return-void
.end method

.method public static a(J)LaGx;
    .locals 6

    .prologue
    .line 123
    invoke-static {}, LaGx;->values()[LaGx;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 124
    iget-wide v4, v0, LaGx;->a:J

    cmp-long v4, v4, p0

    if-nez v4, :cond_0

    .line 129
    :goto_1
    return-object v0

    .line 123
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 129
    :cond_1
    sget-object v0, LaGx;->a:LaGx;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LaGx;
    .locals 1

    .prologue
    .line 110
    const-class v0, LaGx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaGx;

    return-object v0
.end method

.method public static values()[LaGx;
    .locals 1

    .prologue
    .line 110
    sget-object v0, LaGx;->a:[LaGx;

    invoke-virtual {v0}, [LaGx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaGx;

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 133
    iget-wide v0, p0, LaGx;->a:J

    return-wide v0
.end method
