.class LazX;
.super Ljava/util/TimerTask;


# instance fields
.field final synthetic a:LazS;


# direct methods
.method private constructor <init>(LazS;)V
    .locals 0

    iput-object p1, p0, LazX;->a:LazS;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LazS;LazT;)V
    .locals 0

    invoke-direct {p0, p1}, LazX;-><init>(LazS;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, LazX;->a:LazS;

    invoke-static {v0}, LazS;->a(LazS;)LazW;

    move-result-object v0

    sget-object v1, LazW;->b:LazW;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LazX;->a:LazS;

    invoke-static {v0}, LazS;->a(LazS;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LazX;->a:LazS;

    invoke-static {v0}, LazS;->a(LazS;)J

    move-result-wide v0

    iget-object v2, p0, LazX;->a:LazS;

    invoke-static {v2}, LazS;->b(LazS;)J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-object v2, p0, LazX;->a:LazS;

    invoke-static {v2}, LazS;->a(LazS;)LazE;

    move-result-object v2

    invoke-interface {v2}, LazE;->a()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const-string v0, "Disconnecting due to inactivity"

    invoke-static {v0}, LaAj;->c(Ljava/lang/String;)V

    iget-object v0, p0, LazX;->a:LazS;

    invoke-static {v0}, LazS;->d(LazS;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LazX;->a:LazS;

    invoke-static {v0}, LazS;->a(LazS;)Ljava/util/Timer;

    move-result-object v0

    new-instance v1, LazX;

    iget-object v2, p0, LazX;->a:LazS;

    invoke-direct {v1, v2}, LazX;-><init>(LazS;)V

    iget-object v2, p0, LazX;->a:LazS;

    invoke-static {v2}, LazS;->b(LazS;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0
.end method
