.class public Lals;
.super Ljava/lang/Object;
.source "FragmentTransactionSafeExecutor.java"

# interfaces
.implements Ljava/util/concurrent/Executor;


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field private final a:Landroid/os/Handler;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lrm;

.field private final a:Lrq;


# direct methods
.method constructor <init>(Lrm;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Lalt;

    invoke-direct {v0, p0}, Lalt;-><init>(Lals;)V

    iput-object v0, p0, Lals;->a:Lrq;

    .line 54
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lals;->a:Ljava/util/List;

    .line 56
    invoke-static {}, Lanj;->a()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lals;->a:Landroid/os/Handler;

    .line 60
    iput-object p1, p0, Lals;->a:Lrm;

    .line 61
    iget-object v0, p0, Lals;->a:Lrq;

    invoke-virtual {p1, v0}, Lrm;->a(Lrq;)V

    .line 62
    return-void
.end method

.method static synthetic a(Lals;)Ljava/util/List;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lals;->a:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lals;)Lrm;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lals;->a:Lrm;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 83
    invoke-static {}, Lanj;->a()V

    .line 85
    iget-object v0, p0, Lals;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 86
    iget-object v2, p0, Lals;->a:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 88
    :cond_0
    iget-object v0, p0, Lals;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 89
    return-void
.end method

.method public execute(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 73
    new-instance v0, Lalu;

    invoke-direct {v0, p0, p1}, Lalu;-><init>(Lals;Ljava/lang/Runnable;)V

    .line 74
    iget-object v1, p0, Lals;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 75
    return-void
.end method
