.class public abstract LbmS;
.super LblM;
.source "ImmutableMultimap.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LblM",
        "<TK;TV;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field final transient a:I

.field final transient a:LbmL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmL",
            "<TK;+",
            "Lbmv",
            "<TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LbmL;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmL",
            "<TK;+",
            "Lbmv",
            "<TV;>;>;I)V"
        }
    .end annotation

    .prologue
    .line 314
    invoke-direct {p0}, LblM;-><init>()V

    .line 315
    iput-object p1, p0, LbmS;->a:LbmL;

    .line 316
    iput p2, p0, LbmS;->a:I

    .line 317
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 432
    iget v0, p0, LbmS;->a:I

    return v0
.end method

.method public a()LbmL;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmL",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 457
    iget-object v0, p0, LbmS;->a:LbmL;

    return-object v0
.end method

.method public a()LbmY;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmY",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 445
    iget-object v0, p0, LbmS;->a:LbmL;

    invoke-virtual {v0}, LbmL;->c()LbmY;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(Ljava/lang/Object;)Lbmv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Lbmv",
            "<TV;>;"
        }
    .end annotation
.end method

.method a()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;",
            "Ljava/util/Collection",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 462
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should never be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public a()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 352
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public bridge synthetic a()Z
    .locals 1

    .prologue
    .line 64
    invoke-super {p0}, LblM;->a()Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Iterable;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/lang/Iterable",
            "<+TV;>;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 394
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)Z"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 382
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0, p1}, LbmS;->a(Ljava/lang/Object;)Lbmv;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b()Ljava/util/Map;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, LbmS;->a()LbmL;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b()Ljava/util/Set;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, LbmS;->a()LbmY;

    move-result-object v0

    return-object v0
.end method

.method b()Z
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, LbmS;->a:LbmL;

    invoke-virtual {v0}, LbmL;->a()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 64
    invoke-super {p0, p1}, LblM;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic hashCode()I
    .locals 1

    .prologue
    .line 64
    invoke-super {p0}, LblM;->hashCode()I

    move-result v0

    return v0
.end method

.method public bridge synthetic toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    invoke-super {p0}, LblM;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
