.class public LtM;
.super Ljava/lang/Object;
.source "FeatureCheckerImpl.java"

# interfaces
.implements LQs;
.implements LtK;


# annotations
.annotation runtime Lbxz;
.end annotation


# instance fields
.field private final a:LQr;

.field a:LbiP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiP",
            "<",
            "LtN;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LpW;

.field private final a:Lrx;


# direct methods
.method public constructor <init>(Lrx;LQr;LpW;LbiP;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx;",
            "LQr;",
            "LpW;",
            "LbiP",
            "<",
            "LtN;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LtM;->a:Ljava/util/Set;

    .line 38
    iput-object p1, p0, LtM;->a:Lrx;

    .line 39
    iput-object p2, p0, LtM;->a:LQr;

    .line 40
    iput-object p3, p0, LtM;->a:LpW;

    .line 41
    iput-object p4, p0, LtM;->a:LbiP;

    .line 42
    invoke-interface {p2, p0}, LQr;->a(LQs;)V

    .line 43
    invoke-virtual {p0}, LtM;->a()V

    .line 44
    return-void
.end method

.method private a(LtJ;)LtJ;
    .locals 1

    .prologue
    .line 92
    .line 93
    iget-object v0, p0, LtM;->a:LbiP;

    invoke-virtual {v0}, LbiP;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LtM;->a:LbiP;

    .line 94
    invoke-virtual {v0}, LbiP;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtN;

    invoke-interface {v0, p1}, LtN;->a(LtJ;)LtJ;

    move-result-object v0

    .line 96
    :goto_0
    if-eqz v0, :cond_0

    move-object p1, v0

    .line 99
    :cond_0
    return-object p1

    .line 94
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/util/Set;Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 51
    const-string v0, ","

    invoke-virtual {p2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 52
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 53
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 51
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 57
    :cond_0
    :try_start_0
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 58
    :catch_0
    move-exception v5

    .line 60
    const-string v5, "FeatureCheckerImpl"

    const-string v6, "Can\'t disable feature, not found: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v4, v7, v1

    invoke-static {v5, v6, v7}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1

    .line 63
    :cond_1
    return-void
.end method

.method private a(Lrx;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 83
    iget-object v1, p0, LtM;->a:Ljava/util/Set;

    monitor-enter v1

    .line 84
    :try_start_0
    iget-object v0, p0, LtM;->a:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    const/4 v0, 0x0

    monitor-exit v1

    .line 88
    :goto_0
    return v0

    .line 87
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    iget-object v0, p0, LtM;->a:Lrx;

    invoke-virtual {v0, p1}, Lrx;->a(Lrx;)Z

    move-result v0

    goto :goto_0

    .line 87
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 67
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v0

    .line 70
    iget-object v1, p0, LtM;->a:LQr;

    const-string v2, "disableFeatures"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LtM;->a(Ljava/util/Set;Ljava/lang/String;)V

    .line 71
    iget-object v1, p0, LtM;->a:LQr;

    const-string v2, "disableFeaturesList"

    const-string v3, ""

    .line 72
    invoke-interface {v1, v2, v3}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 71
    invoke-direct {p0, v0, v1}, LtM;->a(Ljava/util/Set;Ljava/lang/String;)V

    .line 74
    iget-object v1, p0, LtM;->a:Ljava/util/Set;

    monitor-enter v1

    .line 75
    :try_start_0
    iget-object v2, p0, LtM;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 76
    iget-object v2, p0, LtM;->a:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 77
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    const-string v1, "FeatureCheckerImpl"

    const-string v2, "Configured disabled features: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 79
    return-void

    .line 77
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Lrg;LaFO;)Z
    .locals 2

    .prologue
    .line 112
    instance-of v0, p1, Lrh;

    if-eqz v0, :cond_0

    .line 113
    check-cast p1, Lrh;

    .line 114
    invoke-virtual {p1}, Lrh;->a()LtJ;

    move-result-object v0

    .line 115
    invoke-direct {p0, v0}, LtM;->a(LtJ;)LtJ;

    move-result-object v0

    invoke-static {v0}, Lrh;->a(LtJ;)Lrh;

    move-result-object p1

    .line 118
    :cond_0
    invoke-interface {p1}, Lrg;->a()Lrx;

    move-result-object v0

    invoke-interface {p1}, Lrg;->a()Ljava/lang/String;

    move-result-object v1

    .line 117
    invoke-direct {p0, v0, v1}, LtM;->a(Lrx;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LtM;->a:LQr;

    iget-object v1, p0, LtM;->a:LpW;

    .line 119
    invoke-interface {p1, p0, v0, v1, p2}, Lrg;->a(LtK;LQr;LpW;LaFO;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 120
    :goto_0
    return v0

    .line 119
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LtJ;)Z
    .locals 3

    .prologue
    .line 104
    invoke-direct {p0, p1}, LtM;->a(LtJ;)LtJ;

    move-result-object v0

    .line 106
    invoke-interface {v0}, LtJ;->a()Lrx;

    move-result-object v1

    invoke-interface {v0}, LtJ;->name()Ljava/lang/String;

    move-result-object v2

    .line 105
    invoke-direct {p0, v1, v2}, LtM;->a(Lrx;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LtM;->a:LQr;

    .line 106
    invoke-interface {v0, p0, v1}, LtJ;->a(LtK;LQr;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 107
    :goto_0
    return v0

    .line 106
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
