.class public abstract LatE;
.super Ljava/lang/Object;
.source "Action.java"


# instance fields
.field private final a:I

.field private final a:Ljava/lang/String;

.field private final b:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {}, LatF;->a()I

    move-result v0

    iput v0, p0, LatE;->b:I

    .line 29
    iput-object p1, p0, LatE;->a:Ljava/lang/String;

    .line 30
    iput p2, p0, LatE;->a:I

    .line 31
    return-void
.end method

.method static a(Landroid/content/Intent;LaFO;)V
    .locals 3

    .prologue
    .line 82
    const-string v0, "null accountId for %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {p1, v0, v1}, LbiT;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    const-string v0, "accountName"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    const-string v0, "accountName"

    invoke-virtual {p1}, LaFO;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    :cond_0
    const-string v0, "referrer"

    const-string v1, "/widget"

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    sget-object v0, LaEG;->b:LaEG;

    .line 90
    invoke-virtual {v0}, LaEG;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {p1}, LaFO;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 89
    invoke-virtual {p0, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 91
    const/high16 v0, 0x10000000

    invoke-virtual {p0, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 92
    const/high16 v0, 0x24000000

    invoke-virtual {p0, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 93
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, LatE;->b:I

    return v0
.end method

.method final a(Landroid/content/Context;LaFO;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 46
    invoke-static {p1, p0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    invoke-static {p2, p0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    invoke-virtual {p0, p1, p2}, LatE;->a(Landroid/content/Context;LaFO;)Landroid/content/Intent;

    move-result-object v0

    .line 50
    invoke-static {v0, p2}, LatE;->a(Landroid/content/Intent;LaFO;)V

    .line 53
    invoke-virtual {p0}, LatE;->a()I

    move-result v1

    const/high16 v2, 0x8000000

    .line 52
    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(Landroid/content/Context;LaFO;)Landroid/content/Intent;
.end method

.method final a(Landroid/widget/RemoteViews;Landroid/content/Context;LaFO;)V
    .locals 2

    .prologue
    .line 57
    invoke-virtual {p0, p2, p3}, LatE;->a(Landroid/content/Context;LaFO;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 58
    invoke-virtual {p0, p2}, LatE;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59
    iget v1, p0, LatE;->a:I

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 63
    :goto_0
    return-void

    .line 61
    :cond_0
    iget v0, p0, LatE;->a:I

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0
.end method

.method public a(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x1

    return v0
.end method
