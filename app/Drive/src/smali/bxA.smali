.class public LbxA;
.super Landroid/graphics/drawable/Drawable;
.source "BaseGifDrawable.java"

# interfaces
.implements Landroid/graphics/drawable/Animatable;
.implements Landroid/os/Handler$Callback;
.implements Ljava/lang/Runnable;


# static fields
.field private static a:Landroid/graphics/Paint;

.field private static a:LbxB;

.field private static final a:[B

.field private static b:Landroid/graphics/Paint;

.field private static b:Landroid/os/Handler;


# instance fields
.field private a:F

.field protected a:I

.field private a:J

.field private final a:Landroid/graphics/Bitmap$Config;

.field protected a:Landroid/graphics/Bitmap;

.field private final a:Landroid/os/Handler;

.field protected final a:LbxC;

.field protected a:Z

.field protected a:[I

.field private a:[S

.field protected b:I

.field protected b:Z

.field private final b:[B

.field private b:[I

.field protected c:I

.field private c:Z

.field private c:[B

.field private c:[I

.field private d:I

.field private volatile d:Z

.field private d:[B

.field private d:[I

.field private e:I

.field private volatile e:Z

.field private e:[B

.field private f:I

.field private volatile f:Z

.field private f:[B

.field private g:I

.field private g:Z

.field private h:I

.field private h:Z

.field private i:I

.field private i:Z

.field private j:I

.field private j:Z

.field private k:I

.field private k:Z

.field private l:I

.field private l:Z

.field private m:I

.field private m:Z

.field private n:I

.field private n:Z

.field private o:I

.field private o:Z

.field private p:I

.field private q:I

.field private r:I

.field private s:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-string v0, "NETSCAPE2.0"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, LbxA;->a:[B

    return-void
.end method

.method public constructor <init>(LbxC;Landroid/graphics/Bitmap$Config;)V
    .locals 5

    .prologue
    const/16 v1, 0x1000

    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 126
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 72
    iput-boolean v3, p0, LbxA;->f:Z

    .line 90
    const/16 v0, 0x100

    new-array v0, v0, [B

    iput-object v0, p0, LbxA;->c:[B

    .line 91
    iput v4, p0, LbxA;->n:I

    .line 96
    new-array v0, v1, [S

    iput-object v0, p0, LbxA;->a:[S

    .line 97
    new-array v0, v1, [B

    iput-object v0, p0, LbxA;->d:[B

    .line 98
    const/16 v0, 0x1001

    new-array v0, v0, [B

    iput-object v0, p0, LbxA;->e:[B

    .line 112
    iput-boolean v3, p0, LbxA;->l:Z

    .line 113
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, LbxA;->a:Landroid/os/Handler;

    .line 120
    iput v2, p0, LbxA;->r:I

    .line 121
    iput v2, p0, LbxA;->s:I

    .line 124
    iput-boolean v3, p0, LbxA;->o:Z

    .line 127
    iput-object p2, p0, LbxA;->a:Landroid/graphics/Bitmap$Config;

    .line 130
    sget-object v0, LbxA;->a:LbxB;

    if-nez v0, :cond_0

    .line 131
    new-instance v0, LbxB;

    invoke-direct {v0}, LbxB;-><init>()V

    sput-object v0, LbxA;->a:LbxB;

    .line 132
    sget-object v0, LbxA;->a:LbxB;

    invoke-virtual {v0}, LbxB;->start()V

    .line 133
    new-instance v0, Landroid/os/Handler;

    sget-object v1, LbxA;->a:LbxB;

    invoke-virtual {v1}, LbxB;->getLooper()Landroid/os/Looper;

    move-result-object v1

    sget-object v2, LbxA;->a:LbxB;

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    sput-object v0, LbxA;->b:Landroid/os/Handler;

    .line 136
    :cond_0
    sget-object v0, LbxA;->a:Landroid/graphics/Paint;

    if-nez v0, :cond_1

    .line 137
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, LbxA;->a:Landroid/graphics/Paint;

    .line 138
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, LbxA;->b:Landroid/graphics/Paint;

    .line 139
    sget-object v0, LbxA;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 142
    :cond_1
    iput-object p1, p0, LbxA;->a:LbxC;

    .line 143
    invoke-virtual {p1}, LbxC;->a()[B

    move-result-object v0

    iput-object v0, p0, LbxA;->b:[B

    .line 144
    iget-object v0, p0, LbxA;->a:LbxC;

    iget v0, v0, LbxC;->a:I

    iput v0, p0, LbxA;->d:I

    .line 145
    invoke-virtual {p1}, LbxC;->a()I

    move-result v0

    iput v0, p0, LbxA;->a:I

    iput v0, p0, LbxA;->m:I

    iput v0, p0, LbxA;->k:I

    .line 146
    invoke-virtual {p1}, LbxC;->b()I

    move-result v0

    iput v0, p0, LbxA;->b:I

    iput v0, p0, LbxA;->l:I

    .line 147
    iget-object v0, p0, LbxA;->a:LbxC;

    iget v0, v0, LbxC;->c:I

    iput v0, p0, LbxA;->g:I

    .line 148
    iget-object v0, p0, LbxA;->a:LbxC;

    iget-boolean v0, v0, LbxC;->b:Z

    iput-boolean v0, p0, LbxA;->d:Z

    .line 150
    iget-boolean v0, p0, LbxA;->d:Z

    if-nez v0, :cond_2

    .line 152
    :try_start_0
    iget v0, p0, LbxA;->a:I

    iget v1, p0, LbxA;->b:I

    iget-object v2, p0, LbxA;->a:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, LbxA;->a:Landroid/graphics/Bitmap;

    .line 153
    iget-object v0, p0, LbxA;->a:Landroid/graphics/Bitmap;

    if-nez v0, :cond_3

    .line 154
    new-instance v0, Ljava/lang/OutOfMemoryError;

    const-string v1, "Cannot allocate bitmap"

    invoke-direct {v0, v1}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    :catch_0
    move-exception v0

    .line 167
    iput-boolean v3, p0, LbxA;->d:Z

    .line 170
    :cond_2
    :goto_0
    return-void

    .line 157
    :cond_3
    :try_start_1
    iget v0, p0, LbxA;->a:I

    iget v1, p0, LbxA;->b:I

    mul-int/2addr v0, v1

    .line 158
    new-array v1, v0, [I

    iput-object v1, p0, LbxA;->a:[I

    .line 159
    new-array v0, v0, [B

    iput-object v0, p0, LbxA;->f:[B

    .line 161
    iget v0, p0, LbxA;->b:I

    iput v0, p0, LbxA;->e:I

    .line 162
    iget v0, p0, LbxA;->b:I

    iput v0, p0, LbxA;->f:I

    .line 165
    sget-object v0, LbxA;->b:Landroid/os/Handler;

    sget-object v1, LbxA;->b:Landroid/os/Handler;

    const/16 v2, 0xa

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private a()I
    .locals 5

    .prologue
    .line 829
    iget-object v0, p0, LbxA;->b:[B

    iget v1, p0, LbxA;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LbxA;->d:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    .line 830
    if-lez v0, :cond_0

    .line 831
    iget-object v1, p0, LbxA;->b:[B

    iget v2, p0, LbxA;->d:I

    iget-object v3, p0, LbxA;->c:[B

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 832
    iget v1, p0, LbxA;->d:I

    add-int/2addr v1, v0

    iput v1, p0, LbxA;->d:I

    .line 834
    :cond_0
    return v0
.end method

.method static synthetic a(LbxA;)I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, LbxA;->p:I

    return v0
.end method

.method static synthetic a(LbxA;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, LbxA;->a:Landroid/os/Handler;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 366
    iget-object v0, p0, LbxA;->a:LbxC;

    iget v0, v0, LbxC;->a:I

    iput v0, p0, LbxA;->d:I

    .line 367
    iput-boolean v1, p0, LbxA;->j:Z

    .line 368
    iput v1, p0, LbxA;->p:I

    .line 369
    iput v1, p0, LbxA;->n:I

    .line 370
    return-void
.end method

.method static synthetic a(LbxA;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, LbxA;->b()V

    return-void
.end method

.method private a([II)V
    .locals 6

    .prologue
    .line 390
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    .line 391
    iget-object v1, p0, LbxA;->b:[B

    iget v2, p0, LbxA;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LbxA;->d:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    .line 392
    iget-object v2, p0, LbxA;->b:[B

    iget v3, p0, LbxA;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LbxA;->d:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    .line 393
    iget-object v3, p0, LbxA;->b:[B

    iget v4, p0, LbxA;->d:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, LbxA;->d:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    .line 394
    const/high16 v4, -0x1000000

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v1, v4

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    or-int/2addr v1, v3

    aput v1, p1, v0

    .line 390
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 396
    :cond_0
    return-void
.end method

.method static synthetic a(LbxA;)Z
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, LbxA;->m:Z

    return v0
.end method

.method static synthetic a(LbxA;Z)Z
    .locals 0

    .prologue
    .line 23
    iput-boolean p1, p0, LbxA;->n:Z

    return p1
.end method

.method private b()I
    .locals 4

    .prologue
    .line 842
    iget-object v0, p0, LbxA;->b:[B

    iget v1, p0, LbxA;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LbxA;->d:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    .line 843
    iget-object v1, p0, LbxA;->b:[B

    iget v2, p0, LbxA;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, LbxA;->d:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    .line 844
    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    return v0
.end method

.method static synthetic b(LbxA;)I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, LbxA;->r:I

    return v0
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 406
    iget-object v0, p0, LbxA;->b:[B

    iget v3, p0, LbxA;->d:I

    aget-byte v0, v0, v3

    and-int/lit16 v0, v0, 0xff

    const/16 v3, 0x3b

    if-ne v0, v3, :cond_0

    .line 407
    iput-boolean v2, p0, LbxA;->n:Z

    .line 462
    :goto_0
    return-void

    .line 410
    :cond_0
    invoke-direct {p0}, LbxA;->c()V

    .line 412
    iput v1, p0, LbxA;->n:I

    .line 413
    iput-boolean v1, p0, LbxA;->i:Z

    .line 415
    iput-boolean v1, p0, LbxA;->n:Z

    .line 416
    const/16 v0, 0x64

    iput v0, p0, LbxA;->q:I

    .line 417
    const/4 v0, 0x0

    iput-object v0, p0, LbxA;->b:[I

    .line 420
    :goto_1
    :sswitch_0
    iget-object v0, p0, LbxA;->b:[B

    iget v3, p0, LbxA;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LbxA;->d:I

    aget-byte v0, v0, v3

    and-int/lit16 v0, v0, 0xff

    .line 421
    sparse-switch v0, :sswitch_data_0

    goto :goto_1

    .line 425
    :sswitch_1
    iget-object v0, p0, LbxA;->b:[B

    iget v3, p0, LbxA;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LbxA;->d:I

    aget-byte v0, v0, v3

    and-int/lit16 v0, v0, 0xff

    .line 426
    sparse-switch v0, :sswitch_data_1

    .line 452
    invoke-direct {p0}, LbxA;->j()V

    goto :goto_1

    .line 428
    :sswitch_2
    invoke-direct {p0}, LbxA;->d()V

    goto :goto_1

    .line 431
    :sswitch_3
    invoke-direct {p0}, LbxA;->a()I

    move v0, v1

    .line 433
    :goto_2
    sget-object v3, LbxA;->a:[B

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 434
    iget-object v3, p0, LbxA;->c:[B

    aget-byte v3, v3, v0

    sget-object v4, LbxA;->a:[B

    aget-byte v4, v4, v0

    if-eq v3, v4, :cond_1

    move v0, v1

    .line 439
    :goto_3
    if-eqz v0, :cond_2

    .line 440
    invoke-direct {p0}, LbxA;->e()V

    goto :goto_1

    .line 433
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 442
    :cond_2
    invoke-direct {p0}, LbxA;->j()V

    goto :goto_1

    .line 446
    :sswitch_4
    invoke-direct {p0}, LbxA;->j()V

    goto :goto_1

    .line 449
    :sswitch_5
    invoke-direct {p0}, LbxA;->j()V

    goto :goto_1

    .line 457
    :sswitch_6
    invoke-direct {p0}, LbxA;->f()V

    goto :goto_0

    .line 461
    :sswitch_7
    iput-boolean v2, p0, LbxA;->n:Z

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_3

    .line 421
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x21 -> :sswitch_1
        0x2c -> :sswitch_6
        0x3b -> :sswitch_7
    .end sparse-switch

    .line 426
    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_5
        0xf9 -> :sswitch_2
        0xfe -> :sswitch_4
        0xff -> :sswitch_3
    .end sparse-switch
.end method

.method static synthetic b(LbxA;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, LbxA;->a()V

    return-void
.end method

.method static synthetic b(LbxA;)Z
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, LbxA;->n:Z

    return v0
.end method

.method static synthetic b(LbxA;Z)Z
    .locals 0

    .prologue
    .line 23
    iput-boolean p1, p0, LbxA;->d:Z

    return p1
.end method

.method static synthetic c(LbxA;)I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, LbxA;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LbxA;->s:I

    return v0
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 474
    iget-boolean v0, p0, LbxA;->o:Z

    if-eqz v0, :cond_1

    .line 475
    iput-boolean v1, p0, LbxA;->o:Z

    .line 508
    :cond_0
    :goto_0
    return-void

    .line 478
    :cond_1
    iget v0, p0, LbxA;->n:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 481
    :pswitch_0
    iput-boolean v1, p0, LbxA;->j:Z

    goto :goto_0

    .line 485
    :pswitch_1
    iget-boolean v0, p0, LbxA;->j:Z

    if-eqz v0, :cond_0

    .line 486
    iget-object v0, p0, LbxA;->d:[I

    iget-object v2, p0, LbxA;->a:[I

    iget-object v3, p0, LbxA;->d:[I

    array-length v3, v3

    invoke-static {v0, v1, v2, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 491
    :pswitch_2
    iput-boolean v1, p0, LbxA;->j:Z

    .line 495
    iget-boolean v0, p0, LbxA;->i:Z

    if-nez v0, :cond_3

    .line 496
    iget v0, p0, LbxA;->g:I

    .line 498
    :goto_1
    iget v2, p0, LbxA;->l:I

    if-ge v1, v2, :cond_0

    .line 499
    iget v2, p0, LbxA;->j:I

    add-int/2addr v2, v1

    iget v3, p0, LbxA;->a:I

    mul-int/2addr v2, v3

    iget v3, p0, LbxA;->i:I

    add-int/2addr v2, v3

    .line 500
    iget v3, p0, LbxA;->k:I

    add-int/2addr v3, v2

    .line 501
    :goto_2
    if-ge v2, v3, :cond_2

    .line 502
    iget-object v4, p0, LbxA;->a:[I

    aput v0, v4, v2

    .line 501
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 498
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1

    .line 478
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic c(LbxA;)Z
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, LbxA;->d:Z

    return v0
.end method

.method static synthetic c(LbxA;Z)Z
    .locals 0

    .prologue
    .line 23
    iput-boolean p1, p0, LbxA;->e:Z

    return p1
.end method

.method static synthetic d(LbxA;)I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, LbxA;->q:I

    return v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 514
    iget v0, p0, LbxA;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LbxA;->d:I

    .line 516
    iget-object v0, p0, LbxA;->b:[B

    iget v1, p0, LbxA;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LbxA;->d:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    .line 518
    and-int/lit8 v1, v0, 0x1c

    shr-int/lit8 v1, v1, 0x2

    iput v1, p0, LbxA;->n:I

    .line 519
    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LbxA;->i:Z

    .line 520
    invoke-direct {p0}, LbxA;->b()I

    move-result v0

    mul-int/lit8 v0, v0, 0xa

    iput v0, p0, LbxA;->q:I

    .line 526
    iget v0, p0, LbxA;->q:I

    const/16 v1, 0xa

    if-gt v0, v1, :cond_0

    .line 527
    const/16 v0, 0x64

    iput v0, p0, LbxA;->q:I

    .line 530
    :cond_0
    iget-object v0, p0, LbxA;->b:[B

    iget v1, p0, LbxA;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LbxA;->d:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    iput v0, p0, LbxA;->o:I

    .line 532
    iget v0, p0, LbxA;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LbxA;->d:I

    .line 533
    return-void

    .line 519
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(LbxA;)Z
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, LbxA;->e:Z

    return v0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 541
    :cond_0
    invoke-direct {p0}, LbxA;->a()I

    move-result v0

    .line 542
    if-lez v0, :cond_1

    iget-boolean v0, p0, LbxA;->d:Z

    if-eqz v0, :cond_0

    .line 543
    :cond_1
    return-void
.end method

.method private f()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 549
    invoke-direct {p0}, LbxA;->b()I

    move-result v0

    iput v0, p0, LbxA;->i:I

    .line 550
    invoke-direct {p0}, LbxA;->b()I

    move-result v0

    iput v0, p0, LbxA;->j:I

    .line 552
    invoke-direct {p0}, LbxA;->b()I

    move-result v0

    .line 553
    invoke-direct {p0}, LbxA;->b()I

    move-result v3

    .line 556
    iget v4, p0, LbxA;->a:I

    iget v5, p0, LbxA;->i:I

    sub-int/2addr v4, v5

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    iput v4, p0, LbxA;->k:I

    .line 557
    iget v4, p0, LbxA;->b:I

    iget v5, p0, LbxA;->j:I

    sub-int/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    iput v4, p0, LbxA;->l:I

    .line 560
    iput v0, p0, LbxA;->m:I

    .line 563
    mul-int/2addr v0, v3

    .line 564
    iget-object v3, p0, LbxA;->f:[B

    array-length v3, v3

    if-le v0, v3, :cond_0

    .line 565
    new-array v0, v0, [B

    iput-object v0, p0, LbxA;->f:[B

    .line 568
    :cond_0
    iget-object v0, p0, LbxA;->b:[B

    iget v3, p0, LbxA;->d:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LbxA;->d:I

    aget-byte v0, v0, v3

    and-int/lit16 v3, v0, 0xff

    .line 572
    and-int/lit8 v0, v3, 0x40

    if-eqz v0, :cond_6

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LbxA;->h:Z

    .line 573
    and-int/lit16 v0, v3, 0x80

    if-eqz v0, :cond_7

    move v0, v1

    :goto_1
    iput-boolean v0, p0, LbxA;->g:Z

    .line 574
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    and-int/lit8 v0, v3, 0x7

    add-int/lit8 v0, v0, 0x1

    int-to-double v6, v0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-int v0, v4

    iput v0, p0, LbxA;->h:I

    .line 576
    iget-boolean v0, p0, LbxA;->g:Z

    if-eqz v0, :cond_8

    .line 577
    iget-object v0, p0, LbxA;->b:[I

    if-nez v0, :cond_1

    .line 578
    const/16 v0, 0x100

    new-array v0, v0, [I

    iput-object v0, p0, LbxA;->b:[I

    .line 580
    :cond_1
    iget-object v0, p0, LbxA;->b:[I

    iget v3, p0, LbxA;->h:I

    invoke-direct {p0, v0, v3}, LbxA;->a([II)V

    .line 581
    iget-object v0, p0, LbxA;->b:[I

    iput-object v0, p0, LbxA;->c:[I

    .line 589
    :cond_2
    :goto_2
    iget-boolean v0, p0, LbxA;->i:Z

    if-eqz v0, :cond_3

    .line 590
    iget-object v0, p0, LbxA;->c:[I

    iget v3, p0, LbxA;->o:I

    aget v0, v0, v3

    .line 591
    iget-object v3, p0, LbxA;->c:[I

    iget v4, p0, LbxA;->o:I

    aput v2, v3, v4

    move v2, v0

    .line 594
    :cond_3
    iget-object v0, p0, LbxA;->c:[I

    if-nez v0, :cond_4

    .line 595
    iput-boolean v1, p0, LbxA;->d:Z

    .line 598
    :cond_4
    iget-boolean v0, p0, LbxA;->d:Z

    if-eqz v0, :cond_9

    .line 621
    :cond_5
    :goto_3
    return-void

    :cond_6
    move v0, v2

    .line 572
    goto :goto_0

    :cond_7
    move v0, v2

    .line 573
    goto :goto_1

    .line 583
    :cond_8
    iget-object v0, p0, LbxA;->a:LbxC;

    iget-object v0, v0, LbxC;->a:[I

    iput-object v0, p0, LbxA;->c:[I

    .line 584
    iget-object v0, p0, LbxA;->a:LbxC;

    iget v0, v0, LbxC;->d:I

    iget v3, p0, LbxA;->o:I

    if-ne v0, v3, :cond_2

    .line 585
    iput v2, p0, LbxA;->g:I

    goto :goto_2

    .line 602
    :cond_9
    invoke-direct {p0}, LbxA;->h()V

    .line 604
    invoke-direct {p0}, LbxA;->j()V

    .line 606
    iget-boolean v0, p0, LbxA;->d:Z

    if-nez v0, :cond_5

    .line 610
    iget v0, p0, LbxA;->n:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_a

    .line 611
    invoke-direct {p0}, LbxA;->g()V

    .line 614
    :cond_a
    invoke-direct {p0}, LbxA;->i()V

    .line 616
    iget-boolean v0, p0, LbxA;->i:Z

    if-eqz v0, :cond_b

    .line 617
    iget-object v0, p0, LbxA;->c:[I

    iget v1, p0, LbxA;->o:I

    aput v2, v0, v1

    .line 620
    :cond_b
    iget v0, p0, LbxA;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LbxA;->p:I

    goto :goto_3
.end method

.method private g()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 628
    iget-boolean v0, p0, LbxA;->j:Z

    if-eqz v0, :cond_1

    .line 645
    :cond_0
    :goto_0
    return-void

    .line 632
    :cond_1
    iget-object v0, p0, LbxA;->d:[I

    if-nez v0, :cond_2

    .line 633
    const/4 v0, 0x0

    iput-object v0, p0, LbxA;->d:[I

    .line 635
    :try_start_0
    iget-object v0, p0, LbxA;->a:[I

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, LbxA;->d:[I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 641
    :cond_2
    :goto_1
    iget-object v0, p0, LbxA;->d:[I

    if-eqz v0, :cond_0

    .line 642
    iget-object v0, p0, LbxA;->a:[I

    iget-object v1, p0, LbxA;->d:[I

    iget-object v2, p0, LbxA;->a:[I

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 643
    const/4 v0, 0x1

    iput-boolean v0, p0, LbxA;->j:Z

    goto :goto_0

    .line 636
    :catch_0
    move-exception v0

    .line 637
    const-string v1, "GifDrawable"

    const-string v2, "GifDrawable.backupFrame threw an OOME"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private h()V
    .locals 20

    .prologue
    .line 651
    move-object/from16 v0, p0

    iget v2, v0, LbxA;->k:I

    move-object/from16 v0, p0

    iget v3, v0, LbxA;->l:I

    mul-int v13, v2, v3

    .line 654
    move-object/from16 v0, p0

    iget-object v2, v0, LbxA;->b:[B

    move-object/from16 v0, p0

    iget v3, v0, LbxA;->d:I

    add-int/lit8 v4, v3, 0x1

    move-object/from16 v0, p0

    iput v4, v0, LbxA;->d:I

    aget-byte v2, v2, v3

    and-int/lit16 v14, v2, 0xff

    .line 655
    const/4 v2, 0x1

    shl-int v15, v2, v14

    .line 656
    add-int/lit8 v16, v15, 0x1

    .line 657
    add-int/lit8 v10, v15, 0x2

    .line 658
    const/4 v9, -0x1

    .line 659
    add-int/lit8 v8, v14, 0x1

    .line 660
    const/4 v2, 0x1

    shl-int/2addr v2, v8

    add-int/lit8 v7, v2, -0x1

    .line 661
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v15, :cond_0

    .line 662
    move-object/from16 v0, p0

    iget-object v3, v0, LbxA;->a:[S

    const/4 v4, 0x0

    aput-short v4, v3, v2

    .line 663
    move-object/from16 v0, p0

    iget-object v3, v0, LbxA;->d:[B

    int-to-byte v4, v2

    aput-byte v4, v3, v2

    .line 661
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 667
    :cond_0
    const/4 v6, 0x0

    .line 668
    const/4 v5, 0x0

    .line 669
    const/4 v4, 0x0

    .line 670
    const/4 v3, 0x0

    .line 671
    const/4 v2, 0x0

    .line 672
    :cond_1
    if-ge v2, v13, :cond_2

    .line 673
    move-object/from16 v0, p0

    iget-object v11, v0, LbxA;->b:[B

    move-object/from16 v0, p0

    iget v12, v0, LbxA;->d:I

    add-int/lit8 v17, v12, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, LbxA;->d:I

    aget-byte v11, v11, v12

    and-int/lit16 v11, v11, 0xff

    .line 674
    if-nez v11, :cond_3

    .line 762
    :cond_2
    :goto_1
    if-ge v2, v13, :cond_6

    .line 763
    move-object/from16 v0, p0

    iget-object v4, v0, LbxA;->f:[B

    add-int/lit8 v3, v2, 0x1

    const/4 v5, 0x0

    aput-byte v5, v4, v2

    move v2, v3

    goto :goto_1

    .line 678
    :cond_3
    move-object/from16 v0, p0

    iget v12, v0, LbxA;->d:I

    add-int v17, v12, v11

    .line 679
    :cond_4
    move-object/from16 v0, p0

    iget v11, v0, LbxA;->d:I

    move/from16 v0, v17

    if-ge v11, v0, :cond_1

    .line 680
    move-object/from16 v0, p0

    iget-object v11, v0, LbxA;->b:[B

    move-object/from16 v0, p0

    iget v12, v0, LbxA;->d:I

    add-int/lit8 v18, v12, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, LbxA;->d:I

    aget-byte v11, v11, v12

    and-int/lit16 v11, v11, 0xff

    shl-int/2addr v11, v5

    add-int/2addr v6, v11

    .line 681
    add-int/lit8 v5, v5, 0x8

    .line 683
    :goto_2
    if-lt v5, v8, :cond_4

    .line 685
    and-int v11, v6, v7

    .line 686
    shr-int v12, v6, v8

    .line 687
    sub-int v6, v5, v8

    .line 690
    if-ne v11, v15, :cond_5

    .line 692
    add-int/lit8 v8, v14, 0x1

    .line 693
    const/4 v5, 0x1

    shl-int/2addr v5, v8

    add-int/lit8 v7, v5, -0x1

    .line 694
    add-int/lit8 v10, v15, 0x2

    .line 695
    const/4 v9, -0x1

    move v5, v6

    move v6, v12

    .line 696
    goto :goto_2

    .line 700
    :cond_5
    move/from16 v0, v16

    if-ne v11, v0, :cond_7

    .line 701
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, LbxA;->d:I

    .line 765
    :cond_6
    :goto_3
    return-void

    .line 705
    :cond_7
    const/4 v5, -0x1

    if-ne v9, v5, :cond_8

    .line 706
    move-object/from16 v0, p0

    iget-object v5, v0, LbxA;->f:[B

    add-int/lit8 v4, v2, 0x1

    move-object/from16 v0, p0

    iget-object v9, v0, LbxA;->d:[B

    aget-byte v9, v9, v11

    aput-byte v9, v5, v2

    move v2, v4

    move v5, v6

    move v9, v11

    move v6, v12

    move v4, v11

    .line 709
    goto :goto_2

    .line 713
    :cond_8
    if-lt v11, v10, :cond_10

    .line 714
    move-object/from16 v0, p0

    iget-object v0, v0, LbxA;->e:[B

    move-object/from16 v18, v0

    add-int/lit8 v5, v3, 0x1

    int-to-byte v4, v4

    aput-byte v4, v18, v3

    .line 716
    const/16 v3, 0x1001

    if-ne v5, v3, :cond_f

    .line 717
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, LbxA;->d:Z

    goto :goto_3

    :cond_9
    move v4, v5

    .line 722
    :goto_4
    if-lt v3, v15, :cond_c

    .line 723
    const/16 v5, 0x1001

    if-ge v3, v5, :cond_a

    move-object/from16 v0, p0

    iget-object v5, v0, LbxA;->a:[S

    aget-short v5, v5, v3

    if-ne v3, v5, :cond_b

    .line 724
    :cond_a
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, LbxA;->d:Z

    goto :goto_3

    .line 728
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, LbxA;->e:[B

    move-object/from16 v18, v0

    add-int/lit8 v5, v4, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, LbxA;->d:[B

    move-object/from16 v19, v0

    aget-byte v19, v19, v3

    aput-byte v19, v18, v4

    .line 729
    move-object/from16 v0, p0

    iget-object v4, v0, LbxA;->a:[S

    aget-short v3, v4, v3

    .line 731
    const/16 v4, 0x1001

    if-ne v5, v4, :cond_9

    .line 732
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, LbxA;->d:Z

    goto :goto_3

    .line 737
    :cond_c
    move-object/from16 v0, p0

    iget-object v5, v0, LbxA;->d:[B

    aget-byte v5, v5, v3

    .line 738
    move-object/from16 v0, p0

    iget-object v0, v0, LbxA;->e:[B

    move-object/from16 v18, v0

    add-int/lit8 v3, v4, 0x1

    int-to-byte v0, v5

    move/from16 v19, v0

    aput-byte v19, v18, v4

    .line 741
    const/16 v4, 0x1000

    if-ge v10, v4, :cond_d

    .line 742
    move-object/from16 v0, p0

    iget-object v4, v0, LbxA;->a:[S

    int-to-short v9, v9

    aput-short v9, v4, v10

    .line 743
    move-object/from16 v0, p0

    iget-object v4, v0, LbxA;->d:[B

    int-to-byte v9, v5

    aput-byte v9, v4, v10

    .line 744
    add-int/lit8 v10, v10, 0x1

    .line 746
    and-int v4, v10, v7

    if-nez v4, :cond_d

    const/16 v4, 0x1000

    if-ge v10, v4, :cond_d

    .line 747
    add-int/lit8 v8, v8, 0x1

    .line 748
    add-int/2addr v7, v10

    :cond_d
    move v4, v3

    .line 756
    :goto_5
    move-object/from16 v0, p0

    iget-object v9, v0, LbxA;->f:[B

    add-int/lit8 v3, v2, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, LbxA;->e:[B

    move-object/from16 v18, v0

    add-int/lit8 v4, v4, -0x1

    aget-byte v18, v18, v4

    aput-byte v18, v9, v2

    .line 757
    if-gtz v4, :cond_e

    move v2, v3

    move v9, v11

    move v3, v4

    move v4, v5

    move v5, v6

    move v6, v12

    .line 758
    goto/16 :goto_2

    :cond_e
    move v2, v3

    goto :goto_5

    :cond_f
    move v3, v9

    move v4, v5

    goto/16 :goto_4

    :cond_10
    move v4, v3

    move v3, v11

    goto/16 :goto_4
.end method

.method private i()V
    .locals 13

    .prologue
    const/4 v5, 0x4

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 773
    .line 774
    const/16 v1, 0x8

    move v2, v1

    move v3, v4

    move v1, v0

    .line 776
    :goto_0
    iget v7, p0, LbxA;->l:I

    if-ge v0, v7, :cond_3

    .line 778
    iget-boolean v7, p0, LbxA;->h:Z

    if-eqz v7, :cond_4

    .line 779
    iget v7, p0, LbxA;->l:I

    if-lt v1, v7, :cond_0

    .line 780
    add-int/lit8 v3, v3, 0x1

    .line 781
    packed-switch v3, :pswitch_data_0

    .line 798
    :cond_0
    :goto_1
    add-int v7, v1, v2

    move v12, v1

    move v1, v7

    move v7, v12

    .line 800
    :goto_2
    iget v8, p0, LbxA;->j:I

    add-int/2addr v7, v8

    .line 801
    iget v8, p0, LbxA;->b:I

    if-ge v7, v8, :cond_2

    .line 802
    iget v8, p0, LbxA;->a:I

    mul-int/2addr v7, v8

    .line 803
    iget v8, p0, LbxA;->i:I

    add-int/2addr v8, v7

    .line 804
    iget v7, p0, LbxA;->k:I

    add-int v10, v8, v7

    .line 809
    iget v7, p0, LbxA;->m:I

    mul-int/2addr v7, v0

    move v9, v8

    .line 810
    :goto_3
    if-ge v9, v10, :cond_2

    .line 812
    iget-object v11, p0, LbxA;->f:[B

    add-int/lit8 v8, v7, 0x1

    aget-byte v7, v11, v7

    and-int/lit16 v7, v7, 0xff

    .line 813
    iget-object v11, p0, LbxA;->c:[I

    aget v7, v11, v7

    .line 814
    if-eqz v7, :cond_1

    .line 815
    iget-object v11, p0, LbxA;->a:[I

    aput v7, v11, v9

    .line 817
    :cond_1
    add-int/lit8 v7, v9, 0x1

    move v9, v7

    move v7, v8

    .line 818
    goto :goto_3

    :pswitch_0
    move v1, v5

    .line 784
    goto :goto_1

    :pswitch_1
    move v1, v6

    move v2, v5

    .line 788
    goto :goto_1

    :pswitch_2
    move v1, v4

    move v2, v6

    .line 792
    goto :goto_1

    .line 776
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 821
    :cond_3
    return-void

    :cond_4
    move v7, v0

    goto :goto_2

    .line 781
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private j()V
    .locals 3

    .prologue
    .line 854
    :cond_0
    iget-object v0, p0, LbxA;->b:[B

    iget v1, p0, LbxA;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LbxA;->d:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    .line 855
    iget v1, p0, LbxA;->d:I

    add-int/2addr v1, v0

    iput v1, p0, LbxA;->d:I

    .line 856
    if-gtz v0, :cond_0

    .line 857
    return-void
.end method


# virtual methods
.method protected a(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 882
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 256
    iget-boolean v0, p0, LbxA;->d:Z

    if-nez v0, :cond_0

    iget v0, p0, LbxA;->e:I

    if-eqz v0, :cond_0

    iget v0, p0, LbxA;->f:I

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LbxA;->m:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LbxA;->b:Z

    if-nez v0, :cond_1

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 260
    :cond_1
    iget-boolean v0, p0, LbxA;->c:Z

    if-eqz v0, :cond_2

    .line 261
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 262
    iget v0, p0, LbxA;->a:F

    iget v1, p0, LbxA;->a:F

    invoke-virtual {p1, v0, v1, v2, v2}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 263
    iget-object v0, p0, LbxA;->a:Landroid/graphics/Bitmap;

    sget-object v1, LbxA;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 264
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 269
    :goto_1
    iget-boolean v0, p0, LbxA;->k:Z

    if-eqz v0, :cond_3

    .line 270
    iget-boolean v0, p0, LbxA;->a:Z

    if-nez v0, :cond_0

    .line 273
    iget-wide v0, p0, LbxA;->a:J

    iget v2, p0, LbxA;->c:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 275
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x5

    add-long/2addr v2, v4

    .line 273
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LbxA;->a:J

    .line 276
    iget-wide v0, p0, LbxA;->a:J

    invoke-virtual {p0, p0, v0, v1}, LbxA;->scheduleSelf(Ljava/lang/Runnable;J)V

    goto :goto_0

    .line 266
    :cond_2
    iget-object v0, p0, LbxA;->a:Landroid/graphics/Bitmap;

    sget-object v1, LbxA;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 278
    :cond_3
    iget-boolean v0, p0, LbxA;->e:Z

    if-nez v0, :cond_4

    .line 279
    invoke-virtual {p0}, LbxA;->start()V

    goto :goto_0

    .line 281
    :cond_4
    invoke-virtual {p0, p0}, LbxA;->unscheduleSelf(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 292
    iget v0, p0, LbxA;->b:I

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 287
    iget v0, p0, LbxA;->a:I

    return v0
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 297
    const/4 v0, 0x0

    return v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 861
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_1

    .line 862
    iget v0, p1, Landroid/os/Message;->arg1:I

    iput v0, p0, LbxA;->c:I

    .line 863
    iget-object v0, p0, LbxA;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 864
    iget-object v0, p0, LbxA;->a:Landroid/graphics/Bitmap;

    iget-object v1, p0, LbxA;->a:[I

    iget v3, p0, LbxA;->a:I

    iget v6, p0, LbxA;->a:I

    iget v7, p0, LbxA;->b:I

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 866
    iget-object v0, p0, LbxA;->a:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, LbxA;->a(Landroid/graphics/Bitmap;)V

    .line 867
    iput-boolean v8, p0, LbxA;->b:Z

    .line 868
    iput-boolean v2, p0, LbxA;->a:Z

    .line 869
    invoke-virtual {p0}, LbxA;->invalidateSelf()V

    :cond_0
    move v2, v8

    .line 874
    :cond_1
    return v2
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 310
    iget-boolean v0, p0, LbxA;->k:Z

    return v0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    .line 226
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 227
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    iput v0, p0, LbxA;->e:I

    .line 228
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    iput v0, p0, LbxA;->f:I

    .line 229
    iget v0, p0, LbxA;->e:I

    iget v1, p0, LbxA;->a:I

    if-eq v0, v1, :cond_2

    iget v0, p0, LbxA;->f:I

    iget v1, p0, LbxA;->b:I

    if-eq v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LbxA;->c:Z

    .line 230
    iget-boolean v0, p0, LbxA;->c:Z

    if-eqz v0, :cond_0

    .line 231
    iget v0, p0, LbxA;->e:I

    int-to-float v0, v0

    iget v1, p0, LbxA;->a:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, LbxA;->f:I

    int-to-float v1, v1

    iget v2, p0, LbxA;->b:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LbxA;->a:F

    .line 235
    :cond_0
    iget-boolean v0, p0, LbxA;->d:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, LbxA;->m:Z

    if-nez v0, :cond_1

    .line 237
    sget-object v0, LbxA;->b:Landroid/os/Handler;

    sget-object v1, LbxA;->b:Landroid/os/Handler;

    const/16 v2, 0xc

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 239
    :cond_1
    return-void

    .line 229
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public run()V
    .locals 3

    .prologue
    .line 351
    iget-boolean v0, p0, LbxA;->m:Z

    if-eqz v0, :cond_1

    .line 359
    :cond_0
    :goto_0
    return-void

    .line 356
    :cond_1
    iget-boolean v0, p0, LbxA;->e:Z

    if-nez v0, :cond_0

    .line 357
    sget-object v0, LbxA;->b:Landroid/os/Handler;

    sget-object v1, LbxA;->b:Landroid/os/Handler;

    const/16 v2, 0xa

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public scheduleSelf(Ljava/lang/Runnable;J)V
    .locals 2

    .prologue
    .line 334
    iget-boolean v0, p0, LbxA;->l:Z

    if-eqz v0, :cond_0

    .line 335
    invoke-super {p0, p1, p2, p3}, Landroid/graphics/drawable/Drawable;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 336
    const/4 v0, 0x1

    iput-boolean v0, p0, LbxA;->a:Z

    .line 338
    :cond_0
    return-void
.end method

.method public setAlpha(I)V
    .locals 0

    .prologue
    .line 302
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 306
    return-void
.end method

.method public setVisible(ZZ)Z
    .locals 1

    .prologue
    .line 243
    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    move-result v0

    .line 244
    if-eqz p1, :cond_2

    .line 245
    if-nez v0, :cond_0

    if-eqz p2, :cond_1

    .line 246
    :cond_0
    invoke-virtual {p0}, LbxA;->start()V

    .line 251
    :cond_1
    :goto_0
    return v0

    .line 249
    :cond_2
    invoke-virtual {p0}, LbxA;->stop()V

    goto :goto_0
.end method

.method public start()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 315
    invoke-virtual {p0}, LbxA;->isRunning()Z

    move-result v0

    if-nez v0, :cond_1

    .line 316
    iput-boolean v1, p0, LbxA;->k:Z

    .line 317
    iget-boolean v0, p0, LbxA;->f:Z

    if-nez v0, :cond_0

    .line 318
    iput-boolean v1, p0, LbxA;->e:Z

    .line 320
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LbxA;->a:J

    .line 321
    invoke-virtual {p0}, LbxA;->run()V

    .line 323
    :cond_1
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 327
    invoke-virtual {p0}, LbxA;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    invoke-virtual {p0, p0}, LbxA;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 330
    :cond_0
    return-void
.end method

.method public unscheduleSelf(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 342
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 343
    const/4 v0, 0x0

    iput-boolean v0, p0, LbxA;->k:Z

    .line 344
    return-void
.end method
