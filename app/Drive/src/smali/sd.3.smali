.class public Lsd;
.super Ljava/lang/Object;
.source "DefaultTitleBarListener.java"

# interfaces
.implements LarB;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final a:LqK;

.field private final a:LsI;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lsd;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsd;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;LsI;LqK;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lsd;->a:Landroid/app/Activity;

    .line 30
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqK;

    iput-object v0, p0, Lsd;->a:LqK;

    .line 31
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LsI;

    iput-object v0, p0, Lsd;->a:LsI;

    .line 32
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 46
    iget-object v0, p0, Lsd;->a:LqK;

    const-string v1, "search"

    const-string v2, "searchModeEntered"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lsd;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->onSearchRequested()Z

    .line 49
    return-void
.end method

.method public a(LaFO;)V
    .locals 2

    .prologue
    .line 36
    if-nez p1, :cond_0

    .line 37
    sget-object v0, Lsd;->a:Ljava/lang/String;

    const-string v1, "null accountId"

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    :goto_0
    return-void

    .line 39
    :cond_0
    iget-object v0, p0, Lsd;->a:LsI;

    invoke-interface {v0, p1}, LsI;->a(LaFO;)Landroid/content/Intent;

    move-result-object v0

    .line 40
    iget-object v1, p0, Lsd;->a:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method public b(LaFO;)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lsd;->a:Landroid/app/Activity;

    instance-of v0, v0, Lcom/google/android/apps/docs/app/HomeScreenActivity;

    if-eqz v0, :cond_0

    .line 68
    :goto_0
    return-void

    .line 67
    :cond_0
    iget-object v0, p0, Lsd;->a:Landroid/app/Activity;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/app/DocListActivity;->a(Landroid/app/Activity;LaFO;)V

    goto :goto_0
.end method

.method public c(LaFO;)V
    .locals 0

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lsd;->b(LaFO;)V

    .line 77
    return-void
.end method
