.class public abstract Lrd;
.super Lrm;
.source "AccountListeningActivity.java"

# interfaces
.implements Lakc;


# instance fields
.field private a:LTa;

.field private a:LajZ;

.field a:Lajw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajw",
            "<",
            "LSZ;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lrm;-><init>()V

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lrd;->i:Z

    .line 41
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 68
    iget-boolean v0, p0, Lrd;->i:Z

    if-nez v0, :cond_0

    .line 92
    :goto_0
    return-void

    .line 71
    :cond_0
    new-instance v0, LajZ;

    new-instance v1, Lakd;

    invoke-direct {v1}, Lakd;-><init>()V

    invoke-direct {v0, p0, v1}, LajZ;-><init>(Lakc;Lake;)V

    iput-object v0, p0, Lrd;->a:LajZ;

    .line 72
    new-instance v0, Lre;

    invoke-direct {v0, p0}, Lre;-><init>(Lrd;)V

    iput-object v0, p0, Lrd;->a:LTa;

    goto :goto_0
.end method


# virtual methods
.method public onDestroy()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lrd;->a:LajZ;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lrd;->a:LajZ;

    invoke-virtual {v0}, LajZ;->a()V

    .line 64
    :cond_0
    invoke-super {p0}, Lrm;->onDestroy()V

    .line 65
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lrd;->a:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSZ;

    iget-object v1, p0, Lrd;->a:LTa;

    invoke-interface {v0, v1}, LSZ;->b(LTa;)V

    .line 55
    iget-object v0, p0, Lrd;->a:LajZ;

    invoke-virtual {v0, p0}, LajZ;->a(Landroid/content/Context;)V

    .line 56
    invoke-super {p0}, Lrm;->onPause()V

    .line 57
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-super {p0}, Lrm;->onResume()V

    .line 46
    invoke-direct {p0}, Lrd;->f()V

    .line 47
    iget-object v2, p0, Lrd;->a:LajZ;

    iget-boolean v0, p0, Lrd;->i:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, p0, v0}, LajZ;->a(Landroid/content/Context;Z)V

    .line 48
    iput-boolean v1, p0, Lrd;->i:Z

    .line 49
    iget-object v0, p0, Lrd;->a:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSZ;

    iget-object v1, p0, Lrd;->a:LTa;

    invoke-interface {v0, v1}, LSZ;->a(LTa;)V

    .line 50
    return-void

    :cond_0
    move v0, v1

    .line 47
    goto :goto_0
.end method
