.class public Ladd;
.super Ljava/lang/Object;
.source "DocumentContentSpec.java"


# instance fields
.field private final a:J

.field private final a:[B

.field private final b:J


# direct methods
.method private constructor <init>(JJ[B)V
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    cmp-long v0, p1, v4

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 41
    cmp-long v0, p3, v4

    if-ltz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LbiT;->a(Z)V

    .line 42
    if-eqz p5, :cond_2

    array-length v0, p5

    const/16 v3, 0x10

    if-ne v0, v3, :cond_2

    :goto_2
    invoke-static {v1}, LbiT;->a(Z)V

    .line 44
    iput-wide p1, p0, Ladd;->a:J

    .line 45
    iput-wide p3, p0, Ladd;->b:J

    .line 46
    invoke-virtual {p5}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Ladd;->a:[B

    .line 47
    return-void

    :cond_0
    move v0, v2

    .line 40
    goto :goto_0

    :cond_1
    move v0, v2

    .line 41
    goto :goto_1

    :cond_2
    move v1, v2

    .line 42
    goto :goto_2
.end method

.method public static a(JJ)Ladd;
    .locals 8

    .prologue
    .line 111
    const/16 v0, 0x10

    new-array v6, v0, [B

    .line 112
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v0, v6}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 113
    new-instance v1, Ladd;

    move-wide v2, p0

    move-wide v4, p2

    invoke-direct/range {v1 .. v6}, Ladd;-><init>(JJ[B)V

    return-object v1
.end method

.method public static a([B)Ladd;
    .locals 7

    .prologue
    .line 98
    array-length v0, p0

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    .line 99
    new-instance v0, Lade;

    invoke-direct {v0}, Lade;-><init>()V

    throw v0

    .line 102
    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 103
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v2

    .line 104
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v4

    .line 105
    const/16 v1, 0x10

    new-array v6, v1, [B

    .line 106
    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 107
    new-instance v1, Ladd;

    invoke-direct/range {v1 .. v6}, Ladd;-><init>(JJ[B)V

    return-object v1
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 57
    iget-wide v0, p0, Ladd;->a:J

    return-wide v0
.end method

.method public a(J)Z
    .locals 3

    .prologue
    .line 53
    iget-wide v0, p0, Ladd;->b:J

    cmp-long v0, v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()[B
    .locals 4

    .prologue
    .line 87
    const/4 v0, 0x3

    new-array v0, v0, [[B

    const/4 v1, 0x0

    iget-wide v2, p0, Ladd;->a:J

    .line 88
    invoke-static {v2, v3}, LbsA;->a(J)[B

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Ladd;->b:J

    invoke-static {v2, v3}, LbsA;->a(J)[B

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Ladd;->a:[B

    aput-object v2, v0, v1

    .line 87
    invoke-static {v0}, Lbsx;->a([[B)[B

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 62
    instance-of v1, p1, Ladd;

    if-nez v1, :cond_1

    .line 68
    :cond_0
    :goto_0
    return v0

    .line 66
    :cond_1
    check-cast p1, Ladd;

    .line 67
    iget-wide v2, p0, Ladd;->a:J

    iget-wide v4, p1, Ladd;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-wide v2, p0, Ladd;->b:J

    iget-wide v4, p1, Ladd;->b:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-object v1, p0, Ladd;->a:[B

    iget-object v2, p1, Ladd;->a:[B

    .line 68
    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 73
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Ladd;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Ladd;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Ladd;->a:[B

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 78
    new-instance v0, Ljava/util/Date;

    iget-wide v2, p0, Ladd;->b:J

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 79
    const-string v1, "DocumentContentSpec[%s, %s, %s]"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Ladd;->a:J

    .line 80
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    const/4 v0, 0x2

    iget-object v3, p0, Ladd;->a:[B

    invoke-static {v3}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 79
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
