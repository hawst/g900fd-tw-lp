.class public final LaIG;
.super LaIC;
.source "RootGuid.java"


# instance fields
.field private final a:LaGM;

.field private final a:LbuE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbuE",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(JLaGM;Laja;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LaGM;",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, LaIC;-><init>(J)V

    .line 35
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p0, LaIG;->a:LaGM;

    .line 36
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuE;

    iput-object v0, p0, LaIG;->a:LbuE;

    .line 37
    return-void
.end method


# virtual methods
.method public a()LaGo;
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Z[Ljava/lang/String;LIK;Landroid/net/Uri;)LaHY;
    .locals 13

    .prologue
    .line 50
    new-instance v0, LaHV;

    invoke-direct {v0, p2}, LaHV;-><init>([Ljava/lang/String;)V

    .line 51
    new-instance v10, LaHY;

    invoke-direct {v10, p2}, LaHY;-><init>([Ljava/lang/String;)V

    .line 53
    invoke-static {}, LaIJ;->values()[LaIJ;

    move-result-object v11

    array-length v12, v11

    const/4 v1, 0x0

    move v9, v1

    :goto_0
    if-ge v9, v12, :cond_0

    aget-object v8, v11, v9

    .line 55
    invoke-virtual {p0}, LaIG;->a()J

    move-result-wide v2

    invoke-virtual {v8}, LaIJ;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v1}, LaIF;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LaIG;->a:LbuE;

    .line 56
    invoke-interface {v2}, LbuE;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-virtual {v8, v2}, LaIJ;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, LaGv;->a:LaGv;

    const-string v4, "vnd.android.document/directory"

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 61
    invoke-virtual {v8}, LaIJ;->a()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 62
    invoke-virtual {v8}, LaIJ;->a()Z

    move-result v8

    invoke-static {v8}, LaHW;->a(Z)LaHW;

    move-result-object v8

    .line 54
    invoke-virtual/range {v0 .. v8}, LaHV;->a(Ljava/lang/String;Ljava/lang/String;LaGv;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;LaHW;)[Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v10, v1}, LaHY;->addRow([Ljava/lang/Object;)V

    .line 53
    add-int/lit8 v1, v9, 0x1

    move v9, v1

    goto :goto_0

    .line 65
    :cond_0
    return-object v10
.end method

.method public a(Z[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 76
    iget-object v0, p0, LaIG;->a:LaGM;

    invoke-virtual {p0}, LaIG;->a()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, LaGM;->a(J)LaFM;

    move-result-object v0

    .line 77
    if-nez v0, :cond_0

    .line 80
    :goto_0
    return-object v5

    .line 82
    :cond_0
    invoke-static {p0}, LaIF;->a(LaIC;)Ljava/lang/String;

    move-result-object v1

    .line 83
    invoke-virtual {v0}, LaFM;->a()LaFO;

    move-result-object v0

    invoke-virtual {v0}, LaFO;->b()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LaGv;->a:LaGv;

    const-string v4, "vnd.android.document/directory"

    sget-object v8, LaHW;->b:LaHW;

    move-object v0, p2

    move-object v6, v5

    move-object v7, v5

    .line 80
    invoke-static/range {v0 .. v8}, LaHV;->a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LaGv;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;LaHW;)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    const-string v0, "0"

    return-object v0
.end method
