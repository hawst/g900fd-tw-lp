.class public final enum Lqt;
.super Ljava/lang/Enum;
.source "AclType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lqt;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lqt;

.field private static final synthetic a:[Lqt;

.field public static final enum b:Lqt;

.field public static final enum c:Lqt;

.field public static final enum d:Lqt;

.field public static final enum e:Lqt;

.field public static final enum f:Lqt;

.field public static final enum g:Lqt;


# instance fields
.field private final a:I

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lqq;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lqv;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v2, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 118
    new-instance v0, Lqt;

    const-string v1, "OWNER"

    sget-object v3, Lqv;->a:Lqv;

    invoke-direct {v0, v1, v7, v3}, Lqt;-><init>(Ljava/lang/String;ILqv;)V

    sput-object v0, Lqt;->a:Lqt;

    .line 119
    new-instance v0, Lqt;

    const-string v1, "WRITER"

    sget-object v3, Lqv;->b:Lqv;

    invoke-direct {v0, v1, v8, v3}, Lqt;-><init>(Ljava/lang/String;ILqv;)V

    sput-object v0, Lqt;->b:Lqt;

    .line 120
    new-instance v0, Lqt;

    const-string v1, "COMMENTER"

    sget v3, Lxi;->sharing_role_commenter:I

    sget-object v4, Lqv;->c:Lqv;

    sget-object v5, Lqq;->a:Lqq;

    new-array v6, v7, [Lqq;

    invoke-direct/range {v0 .. v6}, Lqt;-><init>(Ljava/lang/String;IILqv;Lqq;[Lqq;)V

    sput-object v0, Lqt;->c:Lqt;

    .line 121
    new-instance v0, Lqt;

    const-string v1, "READER"

    sget-object v3, Lqv;->c:Lqv;

    invoke-direct {v0, v1, v9, v3}, Lqt;-><init>(Ljava/lang/String;ILqv;)V

    sput-object v0, Lqt;->d:Lqt;

    .line 122
    new-instance v0, Lqt;

    const-string v1, "NONE"

    sget-object v3, Lqv;->d:Lqv;

    invoke-direct {v0, v1, v10, v3}, Lqt;-><init>(Ljava/lang/String;ILqv;)V

    sput-object v0, Lqt;->e:Lqt;

    .line 123
    new-instance v0, Lqt;

    const-string v1, "NOACCESS"

    const/4 v3, 0x5

    sget-object v4, Lqv;->e:Lqv;

    invoke-direct {v0, v1, v3, v4}, Lqt;-><init>(Ljava/lang/String;ILqv;)V

    sput-object v0, Lqt;->f:Lqt;

    .line 124
    new-instance v0, Lqt;

    const-string v1, "UNKNOWN"

    const/4 v3, 0x6

    sget-object v4, Lqv;->f:Lqv;

    invoke-direct {v0, v1, v3, v4}, Lqt;-><init>(Ljava/lang/String;ILqv;)V

    sput-object v0, Lqt;->g:Lqt;

    .line 117
    const/4 v0, 0x7

    new-array v0, v0, [Lqt;

    sget-object v1, Lqt;->a:Lqt;

    aput-object v1, v0, v7

    sget-object v1, Lqt;->b:Lqt;

    aput-object v1, v0, v8

    sget-object v1, Lqt;->c:Lqt;

    aput-object v1, v0, v2

    sget-object v1, Lqt;->d:Lqt;

    aput-object v1, v0, v9

    sget-object v1, Lqt;->e:Lqt;

    aput-object v1, v0, v10

    const/4 v1, 0x5

    sget-object v2, Lqt;->f:Lqt;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lqt;->g:Lqt;

    aput-object v2, v0, v1

    sput-object v0, Lqt;->a:[Lqt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILqv;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lqv;",
            "Ljava/util/Set",
            "<",
            "Lqq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 130
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 131
    iput p3, p0, Lqt;->a:I

    .line 132
    iput-object p4, p0, Lqt;->a:Lqv;

    .line 133
    invoke-static {p5}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lqt;->a:Ljava/util/Set;

    .line 134
    return-void
.end method

.method private varargs constructor <init>(Ljava/lang/String;IILqv;Lqq;[Lqq;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lqv;",
            "Lqq;",
            "[",
            "Lqq;",
            ")V"
        }
    .end annotation

    .prologue
    .line 141
    invoke-static {p5, p6}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;[Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lqt;-><init>(Ljava/lang/String;IILqv;Ljava/util/Set;)V

    .line 142
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILqv;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lqv;",
            ")V"
        }
    .end annotation

    .prologue
    .line 137
    invoke-static {p3}, Lqv;->a(Lqv;)I

    move-result v3

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lqt;-><init>(Ljava/lang/String;IILqv;Ljava/util/Set;)V

    .line 138
    return-void
.end method

.method public static a(Lqv;Ljava/util/Set;)Lqt;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lqv;",
            "Ljava/util/Set",
            "<",
            "Lqq;",
            ">;)",
            "Lqt;"
        }
    .end annotation

    .prologue
    .line 167
    invoke-static {}, Lqt;->values()[Lqt;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 168
    iget-object v4, v0, Lqt;->a:Lqv;

    invoke-virtual {v4, p0}, Lqv;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v0, Lqt;->a:Ljava/util/Set;

    .line 169
    invoke-interface {v4, p1}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 176
    :goto_1
    return-object v0

    .line 167
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 173
    :cond_1
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 174
    sget-object v0, Lqt;->g:Lqt;

    goto :goto_1

    .line 176
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {p0, v0}, Lqt;->a(Lqv;Ljava/util/Set;)Lqt;

    move-result-object v0

    goto :goto_1
.end method

.method public static varargs a(Lqv;[Lqq;)Lqt;
    .locals 1

    .prologue
    .line 161
    const-class v0, Lqq;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 162
    invoke-static {v0, p1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 163
    invoke-static {p0, v0}, Lqt;->a(Lqv;Ljava/util/Set;)Lqt;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lqt;
    .locals 1

    .prologue
    .line 117
    const-class v0, Lqt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lqt;

    return-object v0
.end method

.method public static values()[Lqt;
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lqt;->a:[Lqt;

    invoke-virtual {v0}, [Lqt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lqt;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lqt;->a:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lqq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 153
    iget-object v0, p0, Lqt;->a:Ljava/util/Set;

    return-object v0
.end method

.method public a()Lqv;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lqt;->a:Lqv;

    return-object v0
.end method
