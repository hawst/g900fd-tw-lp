.class public final LakU;
.super Ljava/lang/Object;
.source "DiskCacheDir.java"


# instance fields
.field private final a:LaGM;

.field private final a:LakY;

.field private final a:Ljava/io/File;


# direct methods
.method private constructor <init>(LaGM;Ljava/io/File;LakY;)V
    .locals 3

    .prologue
    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_1

    .line 146
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to get application cache directory: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 148
    :cond_1
    iput-object p2, p0, LakU;->a:Ljava/io/File;

    .line 149
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LakY;

    iput-object v0, p0, LakU;->a:LakY;

    .line 150
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p0, LakU;->a:LaGM;

    .line 151
    invoke-direct {p0}, LakU;->a()V

    .line 152
    return-void
.end method

.method synthetic constructor <init>(LaGM;Ljava/io/File;LakY;LakV;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, LakU;-><init>(LaGM;Ljava/io/File;LakY;)V

    return-void
.end method

.method private a(Ljava/io/File;)LbmF;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "LbmF",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 228
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v1

    .line 229
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 230
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 231
    invoke-direct {p0, v4}, LakU;->a(Ljava/io/File;)LbmF;

    move-result-object v4

    .line 232
    invoke-virtual {v1, v4}, LbmH;->a(Ljava/lang/Iterable;)LbmH;

    .line 229
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 234
    :cond_0
    invoke-virtual {v1, v4}, LbmH;->a(Ljava/lang/Object;)LbmH;

    goto :goto_1

    .line 238
    :cond_1
    invoke-virtual {v1}, LbmH;->a()LbmF;

    move-result-object v0

    return-object v0
.end method

.method private static a(LaGM;Ljava/io/File;LaFO;)Ljava/io/File;
    .locals 2

    .prologue
    .line 213
    invoke-static {p0, p2}, LakU;->b(LaGM;LaFO;)Ljava/lang/String;

    move-result-object v0

    .line 214
    invoke-static {p1, v0}, LakU;->b(Ljava/io/File;Ljava/lang/String;)V

    .line 215
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method static synthetic a(Landroid/content/Context;)Ljava/io/File;
    .locals 1

    .prologue
    .line 27
    invoke-static {p0}, LakU;->b(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LaGM;LaFO;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    invoke-static {p0, p1}, LakU;->b(LaGM;LaFO;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 174
    iget-object v0, p0, LakU;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 175
    iget-object v0, p0, LakU;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 176
    iget-object v0, p0, LakU;->a:Ljava/io/File;

    const-string v1, "diskCache"

    invoke-static {v0, v1}, LakU;->b(Ljava/io/File;Ljava/lang/String;)V

    .line 177
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LakU;->a:Ljava/io/File;

    const-string v2, "diskCache"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 178
    iget-object v1, p0, LakU;->a:LakY;

    iget-object v1, v1, LakY;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LakU;->b(Ljava/io/File;Ljava/lang/String;)V

    .line 179
    return-void
.end method

.method static synthetic a(Ljava/io/File;)V
    .locals 0

    .prologue
    .line 27
    invoke-static {p0}, LakU;->b(Ljava/io/File;)V

    return-void
.end method

.method static synthetic a(Ljava/io/File;LbmY;)V
    .locals 0

    .prologue
    .line 27
    invoke-static {p0, p1}, LakU;->b(Ljava/io/File;LbmY;)V

    return-void
.end method

.method static synthetic a(Ljava/io/File;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 27
    invoke-static {p0, p1}, LakU;->b(Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method private static b(Landroid/content/Context;)Ljava/io/File;
    .locals 1

    .prologue
    .line 242
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private static b(LaGM;LaFO;)Ljava/lang/String;
    .locals 8

    .prologue
    .line 219
    invoke-interface {p0, p1}, LaGM;->a(LaFO;)LaFM;

    move-result-object v0

    .line 220
    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v2, "accountCache_%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, LaFM;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/io/File;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 259
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 260
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 261
    invoke-static {v4}, LakU;->b(Ljava/io/File;)V

    .line 259
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 263
    :cond_1
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    move-result v5

    if-nez v5, :cond_0

    .line 264
    const-string v5, "DiskCacheDir"

    const-string v6, "Failed to delete cache file: %s."

    new-array v7, v8, [Ljava/lang/Object;

    aput-object v4, v7, v1

    invoke-static {v5, v6, v7}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1

    .line 268
    :cond_2
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_3

    .line 269
    const-string v0, "DiskCacheDir"

    const-string v2, "Failed to delete cache dir: %s."

    new-array v3, v8, [Ljava/lang/Object;

    aput-object p0, v3, v1

    invoke-static {v0, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 271
    :cond_3
    return-void
.end method

.method private static b(Ljava/io/File;LbmY;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 247
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 248
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 249
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    .line 251
    invoke-virtual {p1, v4}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 252
    invoke-static {v3}, LakU;->b(Ljava/io/File;)V

    .line 247
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 256
    :cond_1
    return-void
.end method

.method private static b(Ljava/io/File;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 182
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 185
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 186
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_0

    .line 187
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to delete a file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 191
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 192
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 193
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 194
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to create disk cache directory: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 198
    :cond_1
    return-void
.end method


# virtual methods
.method public a()LbmF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmF",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 224
    invoke-virtual {p0}, LakU;->a()Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, LakU;->a(Ljava/io/File;)LbmF;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/io/File;
    .locals 3

    .prologue
    .line 158
    iget-object v0, p0, LakU;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 159
    iget-object v0, p0, LakU;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 160
    invoke-direct {p0}, LakU;->a()V

    .line 161
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LakU;->a:Ljava/io/File;

    const-string v2, "diskCache"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 162
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, LakU;->a:LakY;

    iget-object v2, v2, LakY;->a:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method public a(LaFO;Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, LakU;->a:LaGM;

    invoke-virtual {p0}, LakU;->a()Ljava/io/File;

    move-result-object v1

    invoke-static {v0, v1, p1}, LakU;->a(LaGM;Ljava/io/File;LaFO;)Ljava/io/File;

    move-result-object v0

    .line 209
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method a(LaFO;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, LakU;->a:LaGM;

    invoke-virtual {p0}, LakU;->a()Ljava/io/File;

    move-result-object v1

    invoke-static {v0, v1, p1}, LakU;->a(LaGM;Ljava/io/File;LaFO;)Ljava/io/File;

    move-result-object v0

    .line 202
    invoke-static {v0, p2}, LakU;->b(Ljava/io/File;Ljava/lang/String;)V

    .line 203
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 204
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method
