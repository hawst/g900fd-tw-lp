.class LmU;
.super Ljava/lang/Object;
.source "ListPopupWindow.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:LmK;


# direct methods
.method private constructor <init>(LmK;)V
    .locals 0

    .prologue
    .line 1736
    iput-object p1, p0, LmU;->a:LmK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LmK;LmL;)V
    .locals 0

    .prologue
    .line 1736
    invoke-direct {p0, p1}, LmU;-><init>(LmK;)V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 1740
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    .prologue
    .line 1743
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    iget-object v0, p0, LmU;->a:LmK;

    invoke-virtual {v0}, LmK;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LmU;->a:LmK;

    invoke-static {v0}, LmK;->a(LmK;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1745
    iget-object v0, p0, LmU;->a:LmK;

    invoke-static {v0}, LmK;->a(LmK;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, LmU;->a:LmK;

    invoke-static {v1}, LmK;->a(LmK;)LmW;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1746
    iget-object v0, p0, LmU;->a:LmK;

    invoke-static {v0}, LmK;->a(LmK;)LmW;

    move-result-object v0

    invoke-virtual {v0}, LmW;->run()V

    .line 1748
    :cond_0
    return-void
.end method
