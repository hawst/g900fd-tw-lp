.class public abstract enum LAi;
.super Ljava/lang/Enum;
.source "CreateBarButton.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LAi;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LAi;

.field private static final synthetic a:[LAi;

.field public static final enum b:LAi;

.field public static final enum c:LAi;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 28
    new-instance v0, LAj;

    const-string v1, "CREATE_NEW"

    invoke-direct {v0, v1, v2}, LAj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LAi;->a:LAi;

    .line 50
    new-instance v0, LAk;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v3}, LAk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LAi;->b:LAi;

    .line 67
    new-instance v0, LAl;

    const-string v1, "UPLOAD"

    invoke-direct {v0, v1, v4}, LAl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LAi;->c:LAi;

    .line 27
    const/4 v0, 0x3

    new-array v0, v0, [LAi;

    sget-object v1, LAi;->a:LAi;

    aput-object v1, v0, v2

    sget-object v1, LAi;->b:LAi;

    aput-object v1, v0, v3

    sget-object v1, LAi;->c:LAi;

    aput-object v1, v0, v4

    sput-object v0, LAi;->a:[LAi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILAj;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, LAi;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LAi;
    .locals 1

    .prologue
    .line 27
    const-class v0, LAi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LAi;

    return-object v0
.end method

.method public static values()[LAi;
    .locals 1

    .prologue
    .line 27
    sget-object v0, LAi;->a:[LAi;

    invoke-virtual {v0}, [LAi;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LAi;

    return-object v0
.end method


# virtual methods
.method public abstract a()I
.end method

.method public abstract a(Landroid/app/Activity;LAm;LaFO;LaGu;)V
.end method
