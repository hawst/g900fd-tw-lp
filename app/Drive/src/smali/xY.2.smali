.class LxY;
.super Ljava/lang/Object;
.source "CrossAppPromoFetchingManagerImpl.java"

# interfaces
.implements LxX;


# instance fields
.field private final a:LQr;

.field private final a:Ljava/util/concurrent/Executor;

.field private final a:LxQ;

.field private final a:LxV;

.field private final a:Lyb;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;LQr;LxV;LxQ;Lyb;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, LxY;->a:Ljava/util/concurrent/Executor;

    .line 40
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p0, LxY;->a:LQr;

    .line 41
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LxV;

    iput-object v0, p0, LxY;->a:LxV;

    .line 42
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LxQ;

    iput-object v0, p0, LxY;->a:LxQ;

    .line 43
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyb;

    iput-object v0, p0, LxY;->a:Lyb;

    .line 45
    new-instance v0, LxZ;

    invoke-direct {v0, p0}, LxZ;-><init>(LxY;)V

    invoke-interface {p2, v0}, LQr;->a(LQs;)V

    .line 51
    return-void
.end method

.method private a(Ljava/util/Set;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LxO;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LxO;

    .line 59
    invoke-virtual {v0}, LxO;->b()Ljava/lang/String;

    move-result-object v2

    .line 60
    invoke-virtual {v0}, LxO;->a()Ljava/lang/String;

    move-result-object v3

    .line 63
    iget-object v4, p0, LxY;->a:Lyb;

    invoke-virtual {v0}, LxO;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Lyb;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, LxY;->a:Lyb;

    invoke-interface {v0, v2, v3}, Lyb;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 66
    :cond_0
    iget-object v0, p0, LxY;->a:Lyb;

    invoke-interface {v0, v2}, Lyb;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 69
    :cond_1
    return-void
.end method

.method static synthetic a(LxY;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, LxY;->b()V

    return-void
.end method

.method private a(Lath;)Z
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p1}, Lath;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lath;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 86
    iget-object v0, p0, LxY;->a:LQr;

    const-string v1, "crossAppPromoCatalogUrl"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 88
    invoke-static {v0}, Lbju;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 89
    const-string v1, "CrossAppPromoFetchingManager"

    const-string v2, "The requested catalog url, %s, was null or empty"

    new-array v3, v8, [Ljava/lang/Object;

    aput-object v0, v3, v7

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 126
    :goto_0
    return-void

    .line 93
    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 94
    iget-object v1, p0, LxY;->a:LxQ;

    .line 95
    invoke-interface {v1, v0}, LxQ;->a(Landroid/net/Uri;)LxS;

    move-result-object v1

    .line 97
    invoke-virtual {v1}, LxS;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LxP;

    .line 98
    invoke-virtual {v0}, LxP;->a()Ljava/lang/String;

    move-result-object v3

    .line 99
    invoke-virtual {v0}, LxP;->a()Lath;

    move-result-object v4

    .line 100
    invoke-virtual {v4}, Lath;->toString()Ljava/lang/String;

    move-result-object v5

    .line 101
    invoke-virtual {v0}, LxP;->b()Ljava/lang/String;

    move-result-object v0

    .line 103
    invoke-direct {p0, v4}, LxY;->a(Lath;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 104
    const-string v0, "CrossAppPromoFetchingManager"

    const-string v3, "Received fetch request for invalid URL: %s"

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v4, v5, v7

    invoke-static {v0, v3, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1

    .line 108
    :cond_2
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 111
    iget-object v6, p0, LxY;->a:Lyb;

    invoke-interface {v6, v3}, Lyb;->a(Ljava/lang/String;)V

    .line 114
    :cond_3
    iget-object v6, p0, LxY;->a:LxV;

    .line 115
    invoke-interface {v6, v3, v4, v0}, LxV;->a(Ljava/lang/String;Lath;Ljava/lang/String;)Z

    move-result v0

    .line 117
    if-eqz v0, :cond_1

    .line 120
    iget-object v0, p0, LxY;->a:Lyb;

    invoke-interface {v0, v3, v5}, Lyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 124
    :cond_4
    invoke-virtual {v1}, LxS;->b()Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, v0}, LxY;->a(Ljava/util/Set;)V

    .line 125
    invoke-virtual {v1}, LxS;->c()Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, v0}, LxY;->b(Ljava/util/Set;)V

    goto :goto_0
.end method

.method private b(Ljava/util/Set;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LxO;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 72
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LxO;

    .line 73
    invoke-virtual {v0}, LxO;->b()Ljava/lang/String;

    move-result-object v2

    .line 74
    invoke-virtual {v0}, LxO;->a()Ljava/lang/String;

    move-result-object v3

    .line 77
    iget-object v4, p0, LxY;->a:Lyb;

    invoke-virtual {v0}, LxO;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Lyb;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, LxY;->a:Lyb;

    invoke-interface {v0, v2, v3}, Lyb;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 80
    :cond_0
    iget-object v0, p0, LxY;->a:Lyb;

    invoke-interface {v0, v2}, Lyb;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 83
    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, LxY;->a:Ljava/util/concurrent/Executor;

    new-instance v1, Lya;

    invoke-direct {v1, p0}, Lya;-><init>(LxY;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 136
    return-void
.end method
