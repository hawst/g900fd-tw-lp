.class public LbeN;
.super Lbfz;
.source "GenericJson.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private jsonFactory:LbeO;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lbfz;-><init>()V

    return-void
.end method


# virtual methods
.method public clone()LbeN;
    .locals 1

    .prologue
    .line 90
    invoke-super {p0}, Lbfz;->clone()Lbfz;

    move-result-object v0

    check-cast v0, LbeN;

    return-object v0
.end method

.method public bridge synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    return-object v0
.end method

.method public final getFactory()LbeO;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, LbeN;->jsonFactory:LbeO;

    return-object v0
.end method

.method public set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 95
    invoke-super {p0, p1, p2}, Lbfz;->set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;

    move-result-object v0

    check-cast v0, LbeN;

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    return-object v0
.end method

.method public final setFactory(LbeO;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, LbeN;->jsonFactory:LbeO;

    .line 61
    return-void
.end method

.method public toPrettyString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, LbeN;->jsonFactory:LbeO;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, LbeN;->jsonFactory:LbeO;

    invoke-virtual {v0, p0}, LbeO;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 85
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lbfz;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, LbeN;->jsonFactory:LbeO;

    if-eqz v0, :cond_0

    .line 67
    :try_start_0
    iget-object v0, p0, LbeN;->jsonFactory:LbeO;

    invoke-virtual {v0, p0}, LbeO;->a(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 72
    :goto_0
    return-object v0

    .line 68
    :catch_0
    move-exception v0

    .line 69
    invoke-static {v0}, LbfU;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 72
    :cond_0
    invoke-super {p0}, Lbfz;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
