.class final LlQ;
.super Ljava/lang/Object;
.source "RegisteredMediaRouteProvider.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field private a:I

.field private final a:Landroid/os/Messenger;

.field private final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lln;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic a:LlO;

.field private final a:LlV;

.field private b:I

.field private final b:Landroid/os/Messenger;

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(LlO;Landroid/os/Messenger;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 394
    iput-object p1, p0, LlQ;->a:LlO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 386
    iput v0, p0, LlQ;->a:I

    .line 387
    iput v0, p0, LlQ;->b:I

    .line 391
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LlQ;->a:Landroid/util/SparseArray;

    .line 395
    iput-object p2, p0, LlQ;->a:Landroid/os/Messenger;

    .line 396
    new-instance v0, LlV;

    invoke-direct {v0, p0}, LlV;-><init>(LlQ;)V

    iput-object v0, p0, LlQ;->a:LlV;

    .line 397
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, LlQ;->a:LlV;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, LlQ;->b:Landroid/os/Messenger;

    .line 398
    return-void
.end method

.method static synthetic a(LlQ;)V
    .locals 0

    .prologue
    .line 381
    invoke-direct {p0}, LlQ;->b()V

    return-void
.end method

.method private a(IIILjava/lang/Object;Landroid/os/Bundle;)Z
    .locals 3

    .prologue
    .line 566
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 567
    iput p1, v0, Landroid/os/Message;->what:I

    .line 568
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 569
    iput p3, v0, Landroid/os/Message;->arg2:I

    .line 570
    iput-object p4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 571
    invoke-virtual {v0, p5}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 572
    iget-object v1, p0, LlQ;->b:Landroid/os/Messenger;

    iput-object v1, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 574
    :try_start_0
    iget-object v1, p0, LlQ;->a:Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 575
    const/4 v0, 0x1

    .line 583
    :goto_0
    return v0

    .line 578
    :catch_0
    move-exception v0

    .line 579
    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    .line 580
    const-string v1, "MediaRouteProviderProxy"

    const-string v2, "Could not send message to service."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 583
    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 576
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 431
    .line 432
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LlQ;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 433
    iget-object v0, p0, LlQ;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lln;

    invoke-virtual {v0, v2, v2}, Lln;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 432
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 435
    :cond_0
    iget-object v0, p0, LlQ;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 436
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)I
    .locals 6

    .prologue
    .line 510
    iget v3, p0, LlQ;->b:I

    add-int/lit8 v0, v3, 0x1

    iput v0, p0, LlQ;->b:I

    .line 511
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 512
    const-string v0, "routeId"

    invoke-virtual {v5, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    const/4 v1, 0x3

    iget v2, p0, LlQ;->a:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, LlQ;->a:I

    const/4 v4, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, LlQ;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 515
    return v3
.end method

.method public a()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 418
    const/4 v1, 0x2

    move-object v0, p0

    move v3, v2

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, LlQ;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 419
    iget-object v0, p0, LlQ;->a:LlV;

    invoke-virtual {v0}, LlV;->a()V

    .line 420
    iget-object v0, p0, LlQ;->a:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {v0, p0, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 422
    iget-object v0, p0, LlQ;->a:LlO;

    invoke-static {v0}, LlO;->a(LlO;)LlU;

    move-result-object v0

    new-instance v1, LlR;

    invoke-direct {v1, p0}, LlR;-><init>(LlQ;)V

    invoke-virtual {v0, v1}, LlU;->post(Ljava/lang/Runnable;)Z

    .line 428
    return-void
.end method

.method public a(I)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 519
    const/4 v1, 0x4

    iget v2, p0, LlQ;->a:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, LlQ;->a:I

    move-object v0, p0

    move v3, p1

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, LlQ;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 521
    return-void
.end method

.method public a(II)V
    .locals 6

    .prologue
    .line 534
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 535
    const-string v0, "volume"

    invoke-virtual {v5, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 536
    const/4 v1, 0x7

    iget v2, p0, LlQ;->a:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, LlQ;->a:I

    const/4 v4, 0x0

    move-object v0, p0

    move v3, p1

    invoke-direct/range {v0 .. v5}, LlQ;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 538
    return-void
.end method

.method public a(LkQ;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 561
    const/16 v1, 0xa

    iget v2, p0, LlQ;->a:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, LlQ;->a:I

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, LkQ;->a()Landroid/os/Bundle;

    move-result-object v4

    :goto_0
    move-object v0, p0

    invoke-direct/range {v0 .. v5}, LlQ;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 563
    return-void

    :cond_0
    move-object v4, v5

    .line 561
    goto :goto_0
.end method

.method public a()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 401
    iget v0, p0, LlQ;->a:I

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, LlQ;->a:I

    iput v0, p0, LlQ;->d:I

    .line 402
    iget v2, p0, LlQ;->d:I

    move-object v0, p0

    move v3, v1

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, LlQ;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_0

    move v1, v6

    .line 414
    :goto_0
    return v1

    .line 409
    :cond_0
    :try_start_0
    iget-object v0, p0, LlQ;->a:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, p0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 411
    :catch_0
    move-exception v0

    .line 412
    invoke-virtual {p0}, LlQ;->binderDied()V

    move v1, v6

    .line 414
    goto :goto_0
.end method

.method public a(I)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 439
    iget v0, p0, LlQ;->d:I

    if-ne p1, v0, :cond_0

    .line 440
    const/4 v0, 0x0

    iput v0, p0, LlQ;->d:I

    .line 441
    iget-object v0, p0, LlQ;->a:LlO;

    const-string v1, "Registation failed"

    invoke-static {v0, p0, v1}, LlO;->a(LlO;LlQ;Ljava/lang/String;)V

    .line 443
    :cond_0
    iget-object v0, p0, LlQ;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lln;

    .line 444
    if-eqz v0, :cond_1

    .line 445
    iget-object v1, p0, LlQ;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 446
    invoke-virtual {v0, v2, v2}, Lln;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 448
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public a(IILandroid/os/Bundle;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 457
    iget v2, p0, LlQ;->c:I

    if-nez v2, :cond_0

    iget v2, p0, LlQ;->d:I

    if-ne p1, v2, :cond_0

    if-lt p2, v0, :cond_0

    .line 460
    iput v1, p0, LlQ;->d:I

    .line 461
    iput p2, p0, LlQ;->c:I

    .line 462
    iget-object v1, p0, LlQ;->a:LlO;

    invoke-static {p3}, LkX;->a(Landroid/os/Bundle;)LkX;

    move-result-object v2

    invoke-static {v1, p0, v2}, LlO;->a(LlO;LlQ;LkX;)V

    .line 464
    iget-object v1, p0, LlQ;->a:LlO;

    invoke-static {v1, p0}, LlO;->a(LlO;LlQ;)V

    .line 467
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public a(ILandroid/content/Intent;Lln;)Z
    .locals 6

    .prologue
    .line 549
    iget v2, p0, LlQ;->a:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, LlQ;->a:I

    .line 550
    const/16 v1, 0x9

    const/4 v5, 0x0

    move-object v0, p0

    move v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, LlQ;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 552
    if-eqz p3, :cond_0

    .line 553
    iget-object v0, p0, LlQ;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v2, p3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 555
    :cond_0
    const/4 v0, 0x1

    .line 557
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(ILandroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 480
    iget-object v0, p0, LlQ;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lln;

    .line 481
    if-eqz v0, :cond_0

    .line 482
    iget-object v1, p0, LlQ;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 483
    invoke-virtual {v0, p2}, Lln;->a(Landroid/os/Bundle;)V

    .line 484
    const/4 v0, 0x1

    .line 486
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(ILjava/lang/String;Landroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 490
    iget-object v0, p0, LlQ;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lln;

    .line 491
    if-eqz v0, :cond_0

    .line 492
    iget-object v1, p0, LlQ;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 493
    invoke-virtual {v0, p2, p3}, Lln;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 494
    const/4 v0, 0x1

    .line 496
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 471
    iget v0, p0, LlQ;->c:I

    if-eqz v0, :cond_0

    .line 472
    iget-object v0, p0, LlQ;->a:LlO;

    invoke-static {p1}, LkX;->a(Landroid/os/Bundle;)LkX;

    move-result-object v1

    invoke-static {v0, p0, v1}, LlO;->a(LlO;LlQ;LkX;)V

    .line 474
    const/4 v0, 0x1

    .line 476
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 524
    const/4 v1, 0x5

    iget v2, p0, LlQ;->a:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, LlQ;->a:I

    move-object v0, p0

    move v3, p1

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, LlQ;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 526
    return-void
.end method

.method public b(II)V
    .locals 6

    .prologue
    .line 541
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 542
    const-string v0, "volume"

    invoke-virtual {v5, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 543
    const/16 v1, 0x8

    iget v2, p0, LlQ;->a:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, LlQ;->a:I

    const/4 v4, 0x0

    move-object v0, p0

    move v3, p1

    invoke-direct/range {v0 .. v5}, LlQ;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 545
    return-void
.end method

.method public b(I)Z
    .locals 1

    .prologue
    .line 452
    const/4 v0, 0x1

    return v0
.end method

.method public binderDied()V
    .locals 2

    .prologue
    .line 501
    iget-object v0, p0, LlQ;->a:LlO;

    invoke-static {v0}, LlO;->a(LlO;)LlU;

    move-result-object v0

    new-instance v1, LlS;

    invoke-direct {v1, p0}, LlS;-><init>(LlQ;)V

    invoke-virtual {v0, v1}, LlU;->post(Ljava/lang/Runnable;)Z

    .line 507
    return-void
.end method

.method public c(I)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 529
    const/4 v1, 0x6

    iget v2, p0, LlQ;->a:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, LlQ;->a:I

    move-object v0, p0

    move v3, p1

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, LlQ;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 531
    return-void
.end method
