.class public Lzn;
.super LyR;
.source "PinCard.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field private final a:LVg;

.field private a:LaGu;

.field private final a:Landroid/view/View;

.field private final a:Landroid/widget/Switch;


# direct methods
.method public constructor <init>(Landroid/content/Context;LVg;)V
    .locals 3

    .prologue
    .line 34
    invoke-direct {p0}, LyR;-><init>()V

    .line 35
    const-string v0, "layout_inflater"

    .line 36
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 37
    sget v1, Lxe;->detail_card_pin:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lzn;->a:Landroid/view/View;

    .line 38
    iget-object v0, p0, Lzn;->a:Landroid/view/View;

    new-instance v1, Lzo;

    invoke-direct {v1, p0}, Lzo;-><init>(Lzn;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    iget-object v0, p0, Lzn;->a:Landroid/view/View;

    sget v1, Lxc;->pin:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lzn;->a:Landroid/widget/Switch;

    .line 46
    iget-object v0, p0, Lzn;->a:Landroid/widget/Switch;

    new-instance v1, Lzp;

    invoke-direct {v1, p0}, Lzp;-><init>(Lzn;)V

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 52
    iput-object p2, p0, Lzn;->a:LVg;

    .line 53
    return-void
.end method

.method static synthetic a(Lzn;)Landroid/widget/Switch;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lzn;->a:Landroid/widget/Switch;

    return-object v0
.end method

.method static synthetic a(Lzn;Z)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lzn;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lzn;->a:LaGu;

    invoke-interface {v0}, LaGu;->f()Z

    move-result v0

    if-ne v0, p1, :cond_0

    .line 66
    :goto_0
    return-void

    .line 59
    :cond_0
    iget-object v0, p0, Lzn;->a:LaGu;

    invoke-interface {v0}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 61
    iget-object v1, p0, Lzn;->a:LVg;

    iget-object v2, p0, Lzn;->a:LaGu;

    .line 62
    invoke-interface {v2}, LaGu;->a()LaFM;

    move-result-object v2

    invoke-virtual {v2}, LaFM;->a()LaFO;

    move-result-object v2

    .line 61
    invoke-virtual {v1, v2}, LVg;->a(LaFO;)LVf;

    move-result-object v1

    .line 64
    if-eqz p1, :cond_1

    invoke-virtual {v1, v0}, LVf;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LVf;

    move-result-object v0

    .line 65
    :goto_1
    iget-object v1, p0, Lzn;->a:LVg;

    invoke-virtual {v0}, LVf;->a()LVd;

    move-result-object v0

    invoke-virtual {v1, v0}, LVg;->a(LVd;)V

    goto :goto_0

    .line 64
    :cond_1
    invoke-virtual {v1, v0}, LVf;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LVf;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method protected a()Landroid/view/View;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lzn;->a:Landroid/view/View;

    return-object v0
.end method

.method public a(LaGu;)V
    .locals 5

    .prologue
    .line 80
    iput-object p1, p0, Lzn;->a:LaGu;

    .line 81
    iget-object v0, p0, Lzn;->a:Landroid/widget/Switch;

    invoke-interface {p1}, LaGu;->f()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 83
    iget-object v1, p0, Lzn;->a:Landroid/view/View;

    const-string v2, "%s, %s"

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v4, p0, Lzn;->a:Landroid/widget/Switch;

    .line 84
    invoke-virtual {v4}, Landroid/widget/Switch;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x1

    iget-object v0, p0, Lzn;->a:Landroid/widget/Switch;

    .line 85
    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lzn;->a:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->getTextOn()Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    aput-object v0, v3, v4

    .line 83
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 86
    invoke-virtual {p0}, Lzn;->notifyDataSetChanged()V

    .line 87
    return-void

    .line 85
    :cond_0
    iget-object v0, p0, Lzn;->a:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->getTextOff()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method protected a()Z
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lzn;->a:LaGu;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lzn;->a:LaGu;

    invoke-interface {v0}, LaGu;->g()Z

    move-result v0

    goto :goto_0
.end method
