.class LayO;
.super Lazm;


# static fields
.field private static a:LayO;

.field private static final a:Ljava/lang/Object;


# instance fields
.field private a:I

.field private a:Landroid/content/Context;

.field private a:Landroid/os/Handler;

.field private volatile a:LazA;

.field private a:LazR;

.field private a:Lazv;

.field private a:Lazw;

.field private a:Ljava/lang/String;

.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LayO;->a:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Lazm;-><init>()V

    const/16 v0, 0x708

    iput v0, p0, LayO;->a:I

    iput-boolean v1, p0, LayO;->a:Z

    iput-boolean v1, p0, LayO;->c:Z

    iput-boolean v1, p0, LayO;->d:Z

    new-instance v0, LayP;

    invoke-direct {v0, p0}, LayP;-><init>(LayO;)V

    iput-object v0, p0, LayO;->a:Lazw;

    const/4 v0, 0x0

    iput-boolean v0, p0, LayO;->e:Z

    return-void
.end method

.method static synthetic a(LayO;)I
    .locals 1

    iget v0, p0, LayO;->a:I

    return v0
.end method

.method static synthetic a(LayO;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, LayO;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static a()LayO;
    .locals 1

    sget-object v0, LayO;->a:LayO;

    if-nez v0, :cond_0

    new-instance v0, LayO;

    invoke-direct {v0}, LayO;-><init>()V

    sput-object v0, LayO;->a:LayO;

    :cond_0
    sget-object v0, LayO;->a:LayO;

    return-object v0
.end method

.method static synthetic a()Ljava/lang/Object;
    .locals 1

    sget-object v0, LayO;->a:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(LayO;)Z
    .locals 1

    iget-boolean v0, p0, LayO;->c:Z

    return v0
.end method

.method static synthetic b(LayO;)Z
    .locals 1

    iget-boolean v0, p0, LayO;->e:Z

    return v0
.end method

.method private d()V
    .locals 2

    new-instance v0, LazR;

    invoke-direct {v0, p0}, LazR;-><init>(Lazm;)V

    iput-object v0, p0, LayO;->a:LazR;

    iget-object v0, p0, LayO;->a:LazR;

    iget-object v1, p0, LayO;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, LazR;->a(Landroid/content/Context;)V

    return-void
.end method

.method private e()V
    .locals 4

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, LayO;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, LayQ;

    invoke-direct {v2, p0}, LayQ;-><init>(LayO;)V

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, LayO;->a:Landroid/os/Handler;

    iget v0, p0, LayO;->a:I

    if-lez v0, :cond_0

    iget-object v0, p0, LayO;->a:Landroid/os/Handler;

    iget-object v1, p0, LayO;->a:Landroid/os/Handler;

    const/4 v2, 0x1

    sget-object v3, LayO;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget v2, p0, LayO;->a:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_0
    return-void
.end method


# virtual methods
.method declared-synchronized a()Lazv;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LayO;->a:Lazv;

    if-nez v0, :cond_1

    iget-object v0, p0, LayO;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cant get a store unless we have a context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    new-instance v0, Lazh;

    iget-object v1, p0, LayO;->a:Lazw;

    iget-object v2, p0, LayO;->a:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lazh;-><init>(Lazw;Landroid/content/Context;)V

    iput-object v0, p0, LayO;->a:Lazv;

    iget-object v0, p0, LayO;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, LayO;->a:Lazv;

    invoke-interface {v0}, Lazv;->a()LazO;

    move-result-object v0

    iget-object v1, p0, LayO;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LazO;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, LayO;->a:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, LayO;->a:Landroid/os/Handler;

    if-nez v0, :cond_2

    invoke-direct {p0}, LayO;->e()V

    :cond_2
    iget-object v0, p0, LayO;->a:LazR;

    if-nez v0, :cond_3

    iget-boolean v0, p0, LayO;->d:Z

    if-eqz v0, :cond_3

    invoke-direct {p0}, LayO;->d()V

    :cond_3
    iget-object v0, p0, LayO;->a:Lazv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method declared-synchronized a()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LayO;->a:LazA;

    if-nez v0, :cond_0

    const-string v0, "Dispatch call queued. Dispatch will run once initialization is complete."

    invoke-static {v0}, LaAj;->c(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, LayO;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-static {}, LaAb;->a()LaAb;

    move-result-object v0

    sget-object v1, LaAc;->S:LaAc;

    invoke-virtual {v0, v1}, LaAb;->a(LaAc;)V

    iget-object v0, p0, LayO;->a:LazA;

    invoke-interface {v0}, LazA;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(I)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LayO;->a:Landroid/os/Handler;

    if-nez v0, :cond_1

    const-string v0, "Dispatch period set with null handler. Dispatch will run once initialization is complete."

    invoke-static {v0}, LaAj;->c(Ljava/lang/String;)V

    iput p1, p0, LayO;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-static {}, LaAb;->a()LaAb;

    move-result-object v0

    sget-object v1, LaAc;->T:LaAc;

    invoke-virtual {v0, v1}, LaAb;->a(LaAc;)V

    iget-boolean v0, p0, LayO;->e:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, LayO;->c:Z

    if-eqz v0, :cond_2

    iget v0, p0, LayO;->a:I

    if-lez v0, :cond_2

    iget-object v0, p0, LayO;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    sget-object v2, LayO;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    :cond_2
    iput p1, p0, LayO;->a:I

    if-lez p1, :cond_0

    iget-boolean v0, p0, LayO;->e:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LayO;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LayO;->a:Landroid/os/Handler;

    iget-object v1, p0, LayO;->a:Landroid/os/Handler;

    const/4 v2, 0x1

    sget-object v3, LayO;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    mul-int/lit16 v2, p1, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(Landroid/content/Context;LazA;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LayO;->a:Landroid/content/Context;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LayO;->a:Landroid/content/Context;

    iget-object v0, p0, LayO;->a:LazA;

    if-nez v0, :cond_0

    iput-object p2, p0, LayO;->a:LazA;

    iget-boolean v0, p0, LayO;->a:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, LayO;->a()V

    const/4 v0, 0x0

    iput-boolean v0, p0, LayO;->a:Z

    :cond_2
    iget-boolean v0, p0, LayO;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LayO;->b()V

    const/4 v0, 0x0

    iput-boolean v0, p0, LayO;->b:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LayO;->e:Z

    invoke-virtual {p0, v0, p1}, LayO;->a(ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(ZZ)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LayO;->e:Z

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, LayO;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, p2, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    if-nez p1, :cond_1

    if-nez p2, :cond_2

    :cond_1
    :try_start_1
    iget v0, p0, LayO;->a:I

    if-lez v0, :cond_2

    iget-object v0, p0, LayO;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    sget-object v2, LayO;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    :cond_2
    if-nez p1, :cond_3

    if-eqz p2, :cond_3

    iget v0, p0, LayO;->a:I

    if-lez v0, :cond_3

    iget-object v0, p0, LayO;->a:Landroid/os/Handler;

    iget-object v1, p0, LayO;->a:Landroid/os/Handler;

    const/4 v2, 0x1

    sget-object v3, LayO;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget v2, p0, LayO;->a:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PowerSaveMode "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez p1, :cond_4

    if-nez p2, :cond_5

    :cond_4
    const-string v0, "initiated."

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaAj;->c(Ljava/lang/String;)V

    iput-boolean p1, p0, LayO;->e:Z

    iput-boolean p2, p0, LayO;->c:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_5
    :try_start_2
    const-string v0, "terminated."
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method b()V
    .locals 2

    iget-object v0, p0, LayO;->a:LazA;

    if-nez v0, :cond_0

    const-string v0, "setForceLocalDispatch() queued. It will be called once initialization is complete."

    invoke-static {v0}, LaAj;->c(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, LayO;->b:Z

    :goto_0
    return-void

    :cond_0
    invoke-static {}, LaAb;->a()LaAb;

    move-result-object v0

    sget-object v1, LaAc;->af:LaAc;

    invoke-virtual {v0, v1}, LaAb;->a(LaAc;)V

    iget-object v0, p0, LayO;->a:LazA;

    invoke-interface {v0}, LazA;->c()V

    goto :goto_0
.end method

.method declared-synchronized c()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LayO;->e:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LayO;->c:Z

    if-eqz v0, :cond_0

    iget v0, p0, LayO;->a:I

    if-lez v0, :cond_0

    iget-object v0, p0, LayO;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    sget-object v2, LayO;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    iget-object v0, p0, LayO;->a:Landroid/os/Handler;

    iget-object v1, p0, LayO;->a:Landroid/os/Handler;

    const/4 v2, 0x1

    sget-object v3, LayO;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
