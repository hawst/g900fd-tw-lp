.class public LajN;
.super Ljava/lang/Object;
.source "AccountCapability.java"


# instance fields
.field private final a:LaJQ;

.field private final a:Ljava/util/Date;


# direct methods
.method public constructor <init>(LaJQ;Ljava/util/Date;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, LajN;->a:LaJQ;

    .line 42
    iput-object p2, p0, LajN;->a:Ljava/util/Date;

    .line 43
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, LajN;->a:LaJQ;

    invoke-interface {v0}, LaJQ;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(LaGv;)J
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, LajN;->a:LaJQ;

    invoke-interface {v0, p1}, LaJQ;->a(LaGv;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a()LaHK;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, LajN;->a:LaJQ;

    invoke-interface {v0}, LaJQ;->a()LaHK;

    move-result-object v0

    invoke-static {v0}, LaHR;->a(LaHK;)LaHR;

    move-result-object v0

    return-object v0
.end method

.method public a()LaJS;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, LajN;->a:LaJQ;

    invoke-interface {v0}, LaJQ;->a()LaJS;

    move-result-object v0

    return-object v0
.end method

.method public a(LaGu;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGu;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lqt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, LajN;->a:LaJQ;

    invoke-interface {p1}, LaGu;->j()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LaJQ;->b(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, LajN;->a:LaJQ;

    invoke-interface {v0, p1}, LaJQ;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 46
    sget-object v0, LaFP;->a:Ljava/util/Date;

    iget-object v1, p0, LajN;->a:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, LajN;->a:LaJQ;

    invoke-interface {v0, p1}, LaJQ;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, LajN;->a:LaJQ;

    invoke-interface {v0}, LaJQ;->b()J

    move-result-wide v0

    return-wide v0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, LajN;->a:LaJQ;

    invoke-interface {v0}, LaJQ;->a()Ljava/util/Set;

    move-result-object v0

    const-string v1, "upload_any"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
