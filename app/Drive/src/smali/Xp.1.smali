.class LXp;
.super Ljava/lang/Object;
.source "PassThroughPunchWebViewTouchListener.java"

# interfaces
.implements LSD;


# instance fields
.field final synthetic a:LXn;

.field a:Z


# direct methods
.method private constructor <init>(LXn;)V
    .locals 1

    .prologue
    .line 24
    iput-object p1, p0, LXp;->a:LXn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, LXp;->a:Z

    return-void
.end method

.method synthetic constructor <init>(LXn;LXo;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, LXp;-><init>(LXn;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, LXp;->a:Z

    .line 29
    return-void
.end method

.method public a(LSC;)V
    .locals 2

    .prologue
    .line 39
    const-string v0, "BasePunchWebViewTouchHandler"

    const-string v1, "onScaleEnd()"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    iget-object v0, p0, LXp;->a:LXn;

    invoke-static {v0}, LXn;->a(LXn;)LXp;

    move-result-object v0

    invoke-virtual {v0}, LXp;->a()V

    .line 41
    return-void
.end method

.method public a(LSC;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 45
    iget-boolean v0, p0, LXp;->a:Z

    if-eqz v0, :cond_1

    .line 65
    :cond_0
    :goto_0
    return v4

    .line 49
    :cond_1
    invoke-virtual {p1}, LSC;->c()F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3ff199999999999aL    # 1.1

    cmpl-double v0, v0, v2

    if-lez v0, :cond_2

    .line 51
    iget-object v0, p0, LXp;->a:LXn;

    iget-object v0, v0, LXn;->a:LqK;

    const-string v1, "punch"

    const-string v2, "webViewPunchFullscreenPinchOpen"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, LXp;->a:LXn;

    invoke-static {v0}, LXn;->a(LXn;)LYO;

    move-result-object v0

    sget-object v1, LYR;->b:LYR;

    invoke-interface {v0, v1}, LYO;->a(LYR;)V

    .line 55
    iput-boolean v4, p0, LXp;->a:Z

    goto :goto_0

    .line 56
    :cond_2
    invoke-virtual {p1}, LSC;->c()F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3feccccccccccccdL    # 0.9

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 58
    iget-object v0, p0, LXp;->a:LXn;

    iget-object v0, v0, LXn;->a:LqK;

    const-string v1, "punch"

    const-string v2, "webViewPunchFullscreenPinchClose"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, LXp;->a:LXn;

    invoke-static {v0}, LXn;->a(LXn;)LYO;

    move-result-object v0

    sget-object v1, LYR;->a:LYR;

    invoke-interface {v0, v1}, LYO;->a(LYR;)V

    .line 62
    iput-boolean v4, p0, LXp;->a:Z

    goto :goto_0
.end method

.method public b(LSC;)Z
    .locals 2

    .prologue
    .line 33
    const-string v0, "BasePunchWebViewTouchHandler"

    const-string v1, "onScaleBegin()"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    const/4 v0, 0x1

    return v0
.end method
