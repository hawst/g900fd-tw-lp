.class public final enum LaFd;
.super Ljava/lang/Enum;
.source "PartialFeedTable.java"

# interfaces
.implements Lbjv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaFd;",
        ">;",
        "Lbjv",
        "<",
        "LaFr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaFd;

.field private static final synthetic a:[LaFd;

.field public static final enum b:LaFd;

.field public static final enum c:LaFd;

.field public static final enum d:LaFd;

.field public static final enum e:LaFd;

.field public static final enum f:LaFd;


# instance fields
.field private final a:LaFr;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/16 v8, 0x14

    .line 46
    new-instance v0, LaFd;

    const-string v1, "ACCOUNT_ID"

    invoke-static {}, LaFc;->b()LaFc;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "accountId"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 47
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v3}, LaFG;->a()LaFG;

    move-result-object v3

    .line 48
    invoke-static {}, LaEe;->a()LaEe;

    move-result-object v4

    invoke-virtual {v3, v4}, LaFG;->a(LaFy;)LaFG;

    move-result-object v3

    .line 47
    invoke-virtual {v2, v8, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, LaFd;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFd;->a:LaFd;

    .line 50
    new-instance v0, LaFd;

    const-string v1, "INITIAL_URI"

    invoke-static {}, LaFc;->b()LaFc;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "initialUri"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 51
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v3}, LaFG;->a()LaFG;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, LaFd;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFd;->b:LaFd;

    .line 53
    new-instance v0, LaFd;

    const-string v1, "NEXT_URI"

    invoke-static {}, LaFc;->b()LaFc;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "nextUri"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 54
    invoke-virtual {v2, v8, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, LaFd;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFd;->c:LaFd;

    .line 56
    new-instance v0, LaFd;

    const-string v1, "CLIP_TIME"

    invoke-static {}, LaFc;->b()LaFc;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    const/16 v3, 0x23

    new-instance v4, LaFG;

    const-string v5, "clipTime"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    const-wide v6, 0x7fffffffffffffffL

    .line 57
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v12, v2}, LaFd;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFd;->d:LaFd;

    .line 59
    new-instance v0, LaFd;

    const-string v1, "CACHED_SEARCH_ID"

    const/4 v2, 0x4

    invoke-static {}, LaFc;->b()LaFc;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "cachedSearchId"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 60
    invoke-virtual {v4}, LaFG;->a()LaFG;

    move-result-object v4

    .line 61
    invoke-static {}, LaEs;->a()LaEs;

    move-result-object v5

    .line 60
    invoke-virtual {v4, v5}, LaFG;->a(LaFy;)LaFG;

    move-result-object v4

    invoke-virtual {v3, v8, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaFd;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFd;->e:LaFd;

    .line 63
    new-instance v0, LaFd;

    const-string v1, "NUM_PAGES_RETRIEVED"

    const/4 v2, 0x5

    invoke-static {}, LaFc;->b()LaFc;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x3b

    new-instance v5, LaFG;

    const-string v6, "numPagesRetrieved"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 64
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v5

    invoke-virtual {v5}, LaFG;->b()LaFG;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaFd;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFd;->f:LaFd;

    .line 45
    const/4 v0, 0x6

    new-array v0, v0, [LaFd;

    sget-object v1, LaFd;->a:LaFd;

    aput-object v1, v0, v9

    sget-object v1, LaFd;->b:LaFd;

    aput-object v1, v0, v10

    sget-object v1, LaFd;->c:LaFd;

    aput-object v1, v0, v11

    sget-object v1, LaFd;->d:LaFd;

    aput-object v1, v0, v12

    const/4 v1, 0x4

    sget-object v2, LaFd;->e:LaFd;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LaFd;->f:LaFd;

    aput-object v2, v0, v1

    sput-object v0, LaFd;->a:[LaFd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaFt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 69
    invoke-virtual {p3}, LaFt;->a()LaFr;

    move-result-object v0

    iput-object v0, p0, LaFd;->a:LaFr;

    .line 70
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaFd;
    .locals 1

    .prologue
    .line 45
    const-class v0, LaFd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaFd;

    return-object v0
.end method

.method public static values()[LaFd;
    .locals 1

    .prologue
    .line 45
    sget-object v0, LaFd;->a:[LaFd;

    invoke-virtual {v0}, [LaFd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaFd;

    return-object v0
.end method


# virtual methods
.method public a()LaFr;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, LaFd;->a:LaFr;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, LaFd;->a()LaFr;

    move-result-object v0

    return-object v0
.end method
