.class public Lauu;
.super Ljava/lang/Exception;
.source "PrintDialogActivity.java"


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Ljava/lang/String;J)V
    .locals 7

    .prologue
    .line 102
    sget v0, Laum;->error_print_too_large:I

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    const-wide/16 v4, 0x400

    div-long v4, p3, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 103
    return-void
.end method
