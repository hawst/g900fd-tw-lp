.class public LaeI;
.super Lbxl;
.source "AclGDataParser.java"


# instance fields
.field private final a:LtK;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lorg/xmlpull/v1/XmlPullParser;LtK;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Lbxl;-><init>(Ljava/io/InputStream;Lorg/xmlpull/v1/XmlPullParser;)V

    .line 47
    iput-object p3, p0, LaeI;->a:LtK;

    .line 48
    return-void
.end method


# virtual methods
.method protected a()LaeB;
    .locals 1

    .prologue
    .line 52
    new-instance v0, LaeB;

    invoke-direct {v0}, LaeB;-><init>()V

    return-object v0
.end method

.method public a(Lbxb;)LaeB;
    .locals 1

    .prologue
    .line 88
    invoke-super {p0, p1}, Lbxl;->a(Lbxb;)Lbxb;

    move-result-object v0

    check-cast v0, LaeB;

    return-object v0
.end method

.method protected bridge synthetic a()Lbxb;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, LaeI;->a()LaeB;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lbxb;)Lbxb;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0, p1}, LaeI;->a(Lbxb;)LaeB;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lbxb;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 57
    instance-of v0, p1, LaeB;

    const-string v1, "The entry is NOT AclEntry!"

    invoke-static {v0, v1}, LbiT;->a(ZLjava/lang/Object;)V

    .line 58
    invoke-virtual {p0}, LaeI;->a()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    .line 60
    check-cast p1, LaeB;

    .line 61
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 62
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v2

    .line 63
    const-string v3, "http://schemas.google.com/acl/2007"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 64
    const-string v2, "role"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 65
    const-string v1, "value"

    invoke-interface {v0, v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 66
    const-string v1, "AclGDataParser"

    const-string v2, "role = %s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 67
    invoke-virtual {p1, v0}, LaeB;->b(Ljava/lang/String;)V

    .line 84
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    const-string v2, "scope"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 69
    sget-object v1, Lbxq;->l:Ljava/lang/String;

    invoke-interface {v0, v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 70
    const-string v2, "value"

    invoke-interface {v0, v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 71
    invoke-virtual {p1, v1}, LaeB;->c(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p1, v0}, LaeB;->a(Ljava/lang/String;)V

    .line 73
    const-string v2, "AclGDataParser"

    const-string v3, "scope = %s for %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    aput-object v0, v4, v6

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 74
    :cond_2
    const-string v2, "additionalRole"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LaeI;->a:LtK;

    sget-object v2, Lry;->d:Lry;

    .line 75
    invoke-interface {v1, v2}, LtK;->a(LtJ;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 76
    const-string v1, "value"

    invoke-interface {v0, v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 77
    const-string v1, "AclGDataParser"

    const-string v2, "additional role = %s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 78
    invoke-static {v0}, Lqq;->a(Ljava/lang/String;)Lqq;

    move-result-object v0

    .line 79
    sget-object v1, Lqq;->b:Lqq;

    if-eq v0, v1, :cond_0

    .line 80
    invoke-virtual {p1, v0}, LaeB;->a(Lqq;)V

    goto :goto_0
.end method
