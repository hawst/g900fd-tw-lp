.class Lajl;
.super Lajk;
.source "FragmentActivitySession.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lajk",
        "<",
        "LaiF;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic b:Lajj;


# direct methods
.method constructor <init>(Lajj;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lajl;->b:Lajj;

    .line 173
    invoke-direct {p0, p1, p2}, Lajk;-><init>(Lajj;Landroid/app/Activity;)V

    .line 174
    return-void
.end method


# virtual methods
.method protected bridge synthetic a()LaiD;
    .locals 1

    .prologue
    .line 171
    invoke-virtual {p0}, Lajl;->a()LaiF;

    move-result-object v0

    return-object v0
.end method

.method protected a()LaiF;
    .locals 2

    .prologue
    .line 178
    new-instance v0, LaiF;

    invoke-direct {v0}, LaiF;-><init>()V

    .line 179
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LaiF;->setRetainInstance(Z)V

    .line 180
    return-object v0
.end method

.method protected c()V
    .locals 3

    .prologue
    .line 185
    iget-object v0, p0, Lajl;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 187
    const-string v1, "ActivitySessionFragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 188
    if-eqz v1, :cond_0

    .line 189
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 192
    :cond_0
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    iget-object v0, p0, Lajl;->a:LaiD;

    check-cast v0, Landroid/app/Fragment;

    const-string v2, "ActivitySessionFragment"

    invoke-virtual {v1, v0, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 193
    return-void
.end method
