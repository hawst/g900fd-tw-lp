.class final LUO;
.super Ljava/lang/Object;
.source "RitzUriUtils.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LTF;

.field final synthetic a:Landroid/net/Uri;

.field final synthetic b:Landroid/net/Uri;


# direct methods
.method constructor <init>(LTF;Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, LUO;->a:LTF;

    iput-object p2, p0, LUO;->a:Landroid/net/Uri;

    iput-object p3, p0, LUO;->b:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Landroid/net/Uri;
    .locals 6

    .prologue
    .line 67
    :try_start_0
    iget-object v0, p0, LUO;->a:LTF;

    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    iget-object v2, p0, LUO;->a:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, LTF;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 68
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    const/16 v2, 0xc8

    if-ne v1, v2, :cond_0

    .line 69
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    iget-object v1, p0, LUO;->a:LTF;

    invoke-static {v0, v1}, LTp;->a(Lorg/apache/http/HttpEntity;LTF;)Ljava/lang/String;

    move-result-object v1

    .line 70
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 71
    invoke-static {}, LUN;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Got current URL: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    iget-object v1, p0, LUO;->a:LTF;

    invoke-interface {v1}, LTF;->b()V

    .line 79
    :goto_0
    return-object v0

    .line 77
    :cond_0
    iget-object v0, p0, LUO;->a:LTF;

    invoke-interface {v0}, LTF;->b()V

    .line 79
    :goto_1
    iget-object v0, p0, LUO;->b:Landroid/net/Uri;

    goto :goto_0

    .line 74
    :catch_0
    move-exception v0

    .line 75
    :try_start_1
    invoke-static {}, LUN;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error checking current URL"

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    iget-object v0, p0, LUO;->a:LTF;

    invoke-interface {v0}, LTF;->b()V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, LUO;->a:LTF;

    invoke-interface {v1}, LTF;->b()V

    throw v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, LUO;->a()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
