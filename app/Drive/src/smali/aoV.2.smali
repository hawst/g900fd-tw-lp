.class public final LaoV;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lapf;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lapi;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lapl;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaoY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 41
    iput-object p1, p0, LaoV;->a:LbrA;

    .line 42
    const-class v0, Lapf;

    invoke-static {v0, v1}, LaoV;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaoV;->a:Lbsk;

    .line 45
    const-class v0, Lapi;

    invoke-static {v0, v1}, LaoV;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaoV;->b:Lbsk;

    .line 48
    const-class v0, Lapl;

    invoke-static {v0, v1}, LaoV;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaoV;->c:Lbsk;

    .line 51
    const-class v0, LaoY;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LaoV;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaoV;->d:Lbsk;

    .line 54
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 81
    sparse-switch p1, :sswitch_data_0

    .line 195
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :sswitch_0
    new-instance v0, Lapf;

    iget-object v1, p0, LaoV;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 86
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LaoV;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 84
    invoke-static {v1, v2}, LaoV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LQr;

    iget-object v2, p0, LaoV;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaoA;

    iget-object v2, v2, LaoA;->n:Lbsk;

    .line 92
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LaoV;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaoA;

    iget-object v3, v3, LaoA;->n:Lbsk;

    .line 90
    invoke-static {v2, v3}, LaoV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LakR;

    iget-object v3, p0, LaoV;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaoA;

    iget-object v3, v3, LaoA;->e:Lbsk;

    .line 98
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LaoV;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaoA;

    iget-object v4, v4, LaoA;->e:Lbsk;

    .line 96
    invoke-static {v3, v4}, LaoV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaoJ;

    iget-object v4, p0, LaoV;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaoA;

    iget-object v4, v4, LaoA;->a:Lbsk;

    .line 104
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LaoV;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LaoA;

    iget-object v5, v5, LaoA;->a:Lbsk;

    .line 102
    invoke-static {v4, v5}, LaoV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Laom;

    iget-object v5, p0, LaoV;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LaoV;

    iget-object v5, v5, LaoV;->c:Lbsk;

    .line 110
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LaoV;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LaoV;

    iget-object v6, v6, LaoV;->c:Lbsk;

    .line 108
    invoke-static {v5, v6}, LaoV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lapl;

    iget-object v6, p0, LaoV;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LaoV;

    iget-object v6, v6, LaoV;->d:Lbsk;

    .line 116
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LaoV;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LaoV;

    iget-object v7, v7, LaoV;->d:Lbsk;

    .line 114
    invoke-static {v6, v7}, LaoV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LaoY;

    iget-object v7, p0, LaoV;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LaoA;

    iget-object v7, v7, LaoA;->g:Lbsk;

    .line 122
    invoke-virtual {v7}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v7

    iget-object v8, p0, LaoV;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LaoA;

    iget-object v8, v8, LaoA;->g:Lbsk;

    .line 120
    invoke-static {v7, v8}, LaoV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LaoG;

    iget-object v8, p0, LaoV;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LaoA;

    iget-object v8, v8, LaoA;->o:Lbsk;

    .line 128
    invoke-virtual {v8}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v8

    iget-object v9, p0, LaoV;->a:LbrA;

    iget-object v9, v9, LbrA;->a:LaoA;

    iget-object v9, v9, LaoA;->o:Lbsk;

    .line 126
    invoke-static {v8, v9}, LaoV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LZS;

    iget-object v9, p0, LaoV;->a:LbrA;

    iget-object v9, v9, LbrA;->a:LaoA;

    iget-object v9, v9, LaoA;->p:Lbsk;

    .line 134
    invoke-virtual {v9}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v9

    iget-object v10, p0, LaoV;->a:LbrA;

    iget-object v10, v10, LbrA;->a:LaoA;

    iget-object v10, v10, LaoA;->p:Lbsk;

    .line 132
    invoke-static {v9, v10}, LaoV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LZS;

    iget-object v10, p0, LaoV;->a:LbrA;

    iget-object v10, v10, LbrA;->a:LpG;

    iget-object v10, v10, LpG;->m:Lbsk;

    .line 140
    invoke-virtual {v10}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v10

    iget-object v11, p0, LaoV;->a:LbrA;

    iget-object v11, v11, LbrA;->a:LpG;

    iget-object v11, v11, LpG;->m:Lbsk;

    .line 138
    invoke-static {v10, v11}, LaoV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LtK;

    invoke-direct/range {v0 .. v10}, Lapf;-><init>(LQr;LakR;LaoJ;Laom;Lapl;LaoY;LaoG;LZS;LZS;LtK;)V

    .line 193
    :goto_0
    return-object v0

    .line 147
    :sswitch_1
    new-instance v2, Lapi;

    iget-object v0, p0, LaoV;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaoV;

    iget-object v0, v0, LaoV;->a:Lbsk;

    .line 150
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaoV;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaoV;

    iget-object v1, v1, LaoV;->a:Lbsk;

    .line 148
    invoke-static {v0, v1}, LaoV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapf;

    iget-object v1, p0, LaoV;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 156
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LaoV;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LQH;

    iget-object v3, v3, LQH;->d:Lbsk;

    .line 154
    invoke-static {v1, v3}, LaoV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LQr;

    invoke-direct {v2, v0, v1}, Lapi;-><init>(Lapf;LQr;)V

    move-object v0, v2

    .line 161
    goto :goto_0

    .line 163
    :sswitch_2
    new-instance v2, Lapl;

    iget-object v0, p0, LaoV;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->C:Lbsk;

    .line 166
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaoV;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->C:Lbsk;

    .line 164
    invoke-static {v0, v1}, LaoV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LamW;

    iget-object v1, p0, LaoV;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LqD;

    iget-object v1, v1, LqD;->c:Lbsk;

    .line 172
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LaoV;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LqD;

    iget-object v3, v3, LqD;->c:Lbsk;

    .line 170
    invoke-static {v1, v3}, LaoV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LqK;

    invoke-direct {v2, v0, v1}, Lapl;-><init>(LamW;LqK;)V

    move-object v0, v2

    .line 177
    goto :goto_0

    .line 179
    :sswitch_3
    new-instance v2, LaoY;

    iget-object v0, p0, LaoV;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->b:Lbsk;

    .line 182
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaoV;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->b:Lbsk;

    .line 180
    invoke-static {v0, v1}, LaoV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, LaoV;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 188
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LaoV;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LQH;

    iget-object v3, v3, LQH;->d:Lbsk;

    .line 186
    invoke-static {v1, v3}, LaoV;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LQr;

    invoke-direct {v2, v0, v1}, LaoY;-><init>(Landroid/content/Context;LQr;)V

    move-object v0, v2

    .line 193
    goto/16 :goto_0

    .line 81
    :sswitch_data_0
    .sparse-switch
        0x195 -> :sswitch_1
        0x215 -> :sswitch_2
        0x2a9 -> :sswitch_3
        0x54a -> :sswitch_0
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 210
    .line 212
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 61
    const-class v0, Lapf;

    iget-object v1, p0, LaoV;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LaoV;->a(Ljava/lang/Class;Lbsk;)V

    .line 62
    const-class v0, Lapi;

    iget-object v1, p0, LaoV;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LaoV;->a(Ljava/lang/Class;Lbsk;)V

    .line 63
    const-class v0, Lapl;

    iget-object v1, p0, LaoV;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LaoV;->a(Ljava/lang/Class;Lbsk;)V

    .line 64
    const-class v0, LaoY;

    iget-object v1, p0, LaoV;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LaoV;->a(Ljava/lang/Class;Lbsk;)V

    .line 65
    iget-object v0, p0, LaoV;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x54a

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 67
    iget-object v0, p0, LaoV;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x195

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 69
    iget-object v0, p0, LaoV;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x215

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 71
    iget-object v0, p0, LaoV;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2a9

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 73
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 202
    .line 204
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method
