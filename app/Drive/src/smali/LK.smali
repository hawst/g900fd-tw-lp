.class LLK;
.super LLQ;
.source "SelectionFunctionDoRename.java"


# instance fields
.field private final a:LPp;


# direct methods
.method constructor <init>(LPp;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, LLQ;-><init>()V

    .line 22
    iput-object p1, p0, LLK;->a:LPp;

    .line 23
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Runnable;LaFM;LbmF;LbmF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "LaFM;",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-static {p3}, Lbnm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    .line 36
    iget-object v1, p0, LLK;->a:LPp;

    invoke-interface {v1, v0}, LPp;->a(LaGu;)V

    .line 37
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 38
    return-void
.end method

.method public a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-super {p0, p1, p2}, LLQ;->a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    invoke-virtual {p1, v1}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    invoke-interface {v0}, LaGu;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
