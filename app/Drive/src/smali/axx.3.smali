.class public Laxx;
.super Landroid/view/View;
.source "TileView.java"


# static fields
.field private static final a:Landroid/graphics/Matrix;

.field private static final a:Landroid/graphics/Point;


# instance fields
.field private a:Landroid/graphics/Bitmap;

.field public final a:Lawr;

.field private final a:Lcom/google/android/apps/viewer/client/Dimensions;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    sput-object v0, Laxx;->a:Landroid/graphics/Point;

    .line 23
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    sput-object v0, Laxx;->a:Landroid/graphics/Matrix;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lawr;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 32
    invoke-virtual {p2}, Lawr;->b()Lcom/google/android/apps/viewer/client/Dimensions;

    move-result-object v0

    iput-object v0, p0, Laxx;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    .line 33
    iput-object p2, p0, Laxx;->a:Lawr;

    .line 34
    return-void
.end method


# virtual methods
.method public a()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Laxx;->a:Lawr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laxx;->a:Lawr;

    invoke-virtual {v0}, Lawr;->a()Landroid/graphics/Point;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Laxx;->a:Landroid/graphics/Point;

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Laxx;->a:Lawr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laxx;->a:Lawr;

    invoke-virtual {v0}, Lawr;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "TileView - empty"

    goto :goto_0
.end method

.method a()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Laxx;->a:Landroid/graphics/Bitmap;

    .line 49
    return-void
.end method

.method public a(Lawr;Landroid/graphics/Bitmap;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 37
    iget-object v0, p0, Laxx;->a:Lawr;

    if-ne p1, v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Got wrong tileId %s : %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Laxx;->a:Lawr;

    aput-object v5, v4, v2

    aput-object p1, v4, v1

    .line 38
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 37
    invoke-static {v0, v1}, LauV;->b(ZLjava/lang/String;)V

    .line 39
    iget-object v0, p0, Laxx;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 40
    invoke-virtual {p0}, Laxx;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Used tile receiving new bitmap "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    :cond_0
    iput-object p2, p0, Laxx;->a:Landroid/graphics/Bitmap;

    .line 43
    invoke-virtual {p0}, Laxx;->requestLayout()V

    .line 44
    invoke-virtual {p0}, Laxx;->invalidate()V

    .line 45
    return-void

    :cond_1
    move v0, v2

    .line 37
    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Laxx;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOpaque()Z
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x1

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/16 v8, 0x14

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 66
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 67
    iget-object v0, p0, Laxx;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 68
    iget-object v0, p0, Laxx;->a:Landroid/graphics/Bitmap;

    sget-object v2, Laxx;->a:Landroid/graphics/Matrix;

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 75
    :goto_0
    sget-boolean v0, LavX;->f:Z

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Laxx;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v6, v0, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    .line 77
    iget-object v0, p0, Laxx;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v7, v0, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    .line 78
    new-instance v0, Landroid/graphics/Rect;

    iget-object v2, p0, Laxx;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v2, v2, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    iget-object v3, p0, Laxx;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v3, v3, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    invoke-direct {v0, v5, v5, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 79
    invoke-virtual {v0, v8, v8}, Landroid/graphics/Rect;->inset(II)V

    .line 80
    invoke-virtual {p0}, Laxx;->a()Ljava/lang/String;

    move-result-object v2

    div-int/lit8 v3, v6, 0x2

    int-to-float v3, v3

    div-int/lit8 v4, v7, 0x2

    add-int/lit8 v4, v4, -0xa

    int-to-float v4, v4

    sget-object v5, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 81
    sget-object v2, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 82
    int-to-float v3, v6

    int-to-float v4, v7

    sget-object v5, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 83
    int-to-float v2, v7

    int-to-float v3, v6

    sget-object v5, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 85
    :cond_0
    return-void

    .line 70
    :cond_1
    invoke-virtual {p0}, Laxx;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;

    .line 71
    new-instance v2, Landroid/graphics/Rect;

    iget-object v3, p0, Laxx;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v3, v3, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    iget-object v4, p0, Laxx;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v4, v4, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 72
    iget-object v3, p0, Laxx;->a:Lawr;

    invoke-virtual {v3}, Lawr;->a()Landroid/graphics/Point;

    move-result-object v3

    invoke-virtual {v0, p1, v3, v2}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(Landroid/graphics/Canvas;Landroid/graphics/Point;Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 0

    .prologue
    .line 61
    invoke-virtual {p0, p1, p2}, Laxx;->setMeasuredDimension(II)V

    .line 62
    return-void
.end method
