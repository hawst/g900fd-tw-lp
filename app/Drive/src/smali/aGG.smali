.class public LaGG;
.super Ljava/lang/Object;
.source "GDataConverterImpl.java"

# interfaces
.implements LaGF;


# static fields
.field private static final a:LaHg;

.field private static final a:Ljava/util/Locale;

.field private static final b:LaHg;


# instance fields
.field private final a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    sput-object v0, LaGG;->a:Ljava/util/Locale;

    .line 29
    const-string v0, "yyyy-MM-dd\'T\'HH:mm:ss.SSS"

    invoke-static {v0}, LaGG;->a(Ljava/lang/String;)LaHg;

    move-result-object v0

    sput-object v0, LaGG;->a:LaHg;

    .line 30
    const-string v0, "yyyy-MM-dd\'T\'HH:mm:ss.SSSz"

    .line 31
    invoke-static {v0}, LaGG;->a(Ljava/lang/String;)LaHg;

    move-result-object v0

    sput-object v0, LaGG;->b:LaHg;

    .line 30
    return-void
.end method

.method public constructor <init>(LtK;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    sget-object v0, Lry;->ab:Lry;

    invoke-interface {p1, v0}, LtK;->a(LtJ;)Z

    move-result v0

    iput-boolean v0, p0, LaGG;->a:Z

    .line 37
    return-void
.end method

.method private static a(Ljava/lang/String;)LaHg;
    .locals 2

    .prologue
    .line 40
    new-instance v0, LaHg;

    sget-object v1, LaGG;->a:Ljava/util/Locale;

    invoke-direct {v0, p0, v1}, LaHg;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 41
    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, LaHg;->a(Ljava/util/TimeZone;)V

    .line 42
    return-object v0
.end method

.method private a(LaJT;LaGe;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 54
    .line 55
    invoke-virtual {p2}, LaGe;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, LaGe;->k()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, LaJT;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    .line 54
    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 58
    invoke-virtual {p2}, LaGe;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 59
    iget-boolean v0, p0, LaGG;->a:Z

    if-eqz v0, :cond_2

    .line 61
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The target entry must not be local only."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v1

    .line 55
    goto :goto_0

    .line 65
    :cond_2
    const-string v0, "GDataConverterImpl"

    const-string v3, "Copying GData into a doc entry which is local only. The document should have already been marked as non-local-only."

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 67
    invoke-interface {p1}, LaJT;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LaGe;->h(Ljava/lang/String;)LaGe;

    .line 71
    :cond_3
    invoke-interface {p1}, LaJT;->g()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 72
    sget-object v0, LaGv;->a:LaGv;

    invoke-virtual {v0}, LaGv;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, LaJT;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 73
    sget-object v0, LaGw;->b:LaGw;

    invoke-virtual {p2, v0}, LaGe;->a(LaGw;)V

    .line 84
    :goto_1
    invoke-interface {p1}, LaJT;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LaGG;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 85
    invoke-virtual {p2, v0}, LaGe;->b(Ljava/util/Date;)LaGe;

    .line 87
    invoke-interface {p1}, LaJT;->p()Ljava/lang/String;

    move-result-object v3

    .line 89
    if-nez v3, :cond_d

    :goto_2
    invoke-virtual {p2, v0}, LaGe;->a(Ljava/util/Date;)LaGe;

    .line 94
    invoke-interface {p1}, LaJT;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LaGG;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    .line 95
    invoke-virtual {p2}, LaGe;->c()Ljava/util/Date;

    move-result-object v0

    .line 96
    if-nez v0, :cond_e

    move-object v0, v3

    .line 104
    :cond_4
    :goto_3
    invoke-virtual {p2, v0}, LaGe;->c(Ljava/util/Date;)LaGe;

    .line 106
    invoke-interface {p1}, LaJT;->d()Ljava/lang/String;

    move-result-object v0

    .line 107
    if-nez v0, :cond_f

    const/4 v0, 0x0

    :goto_4
    invoke-virtual {p2, v0}, LaGe;->a(Ljava/lang/Long;)V

    .line 110
    invoke-interface {p1}, LaJT;->e()Ljava/lang/String;

    move-result-object v0

    .line 111
    invoke-virtual {p2}, LaGe;->d()Ljava/lang/Long;

    move-result-object v3

    .line 112
    if-eqz v0, :cond_6

    .line 115
    invoke-virtual {p0, v0}, LaGG;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    .line 116
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-ltz v0, :cond_6

    .line 117
    :cond_5
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p2, v0}, LaGe;->a(Ljava/lang/Long;)LaGe;

    .line 121
    :cond_6
    invoke-interface {p1}, LaJT;->g()Ljava/lang/String;

    move-result-object v0

    .line 122
    if-nez v0, :cond_7

    .line 123
    const-string v0, ""

    .line 125
    :cond_7
    invoke-virtual {p2, v0}, LaGe;->c(Ljava/lang/String;)LaGe;

    .line 126
    invoke-interface {p1}, LaJT;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LaGe;->c(Ljava/lang/String;)V

    .line 127
    invoke-interface {p1}, LaJT;->q()Ljava/lang/String;

    move-result-object v0

    .line 128
    if-nez v0, :cond_8

    .line 129
    const-string v0, ""

    .line 131
    :cond_8
    invoke-virtual {p2, v0}, LaGe;->b(Ljava/lang/String;)LaGe;

    .line 132
    invoke-interface {p1}, LaJT;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, LaGe;->g(Ljava/lang/String;)LaGe;

    .line 133
    invoke-virtual {p2, v0}, LaGe;->d(Ljava/lang/String;)LaGe;

    .line 134
    invoke-interface {p1}, LaJT;->a()Z

    move-result v0

    invoke-virtual {p2, v0}, LaGe;->a(Z)LaGe;

    .line 135
    invoke-interface {p1}, LaJT;->b()Z

    move-result v0

    invoke-virtual {p2, v0}, LaGe;->a(Z)V

    .line 136
    invoke-interface {p1}, LaJT;->c()Z

    move-result v0

    invoke-virtual {p2, v0}, LaGe;->b(Z)LaGe;

    .line 137
    invoke-interface {p1}, LaJT;->f()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 138
    invoke-virtual {p2}, LaGe;->a()V

    .line 145
    :cond_9
    :goto_5
    invoke-interface {p1}, LaJT;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LaGe;->a(Ljava/lang/String;)LaGe;

    .line 146
    invoke-interface {p1}, LaJT;->o()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_12

    move v0, v2

    :goto_6
    invoke-virtual {p2, v0}, LaGe;->c(Z)V

    .line 147
    invoke-interface {p1}, LaJT;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_13

    :goto_7
    invoke-virtual {p2, v2}, LaGe;->d(Z)V

    .line 148
    return-void

    .line 75
    :cond_a
    const-string v0, "GDataConverterImpl"

    const-string v3, "A folder has been marked as plus media root but it is not a folder %s"

    new-array v4, v2, [Ljava/lang/Object;

    .line 76
    invoke-virtual {p2}, LaGe;->k()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    .line 75
    invoke-static {v0, v3, v4}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_1

    .line 78
    :cond_b
    invoke-interface {p1}, LaJT;->h()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 79
    sget-object v0, LaGw;->c:LaGw;

    invoke-virtual {p2, v0}, LaGe;->a(LaGw;)V

    goto/16 :goto_1

    .line 81
    :cond_c
    sget-object v0, LaGw;->a:LaGw;

    invoke-virtual {p2, v0}, LaGe;->a(LaGw;)V

    goto/16 :goto_1

    .line 89
    :cond_d
    invoke-virtual {p0, v3}, LaGG;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    goto/16 :goto_2

    .line 99
    :cond_e
    if-eqz v3, :cond_4

    .line 102
    invoke-virtual {v0, v3}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v4

    if-nez v4, :cond_4

    move-object v0, v3

    goto/16 :goto_3

    .line 108
    :cond_f
    invoke-virtual {p0, v0}, LaGG;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_4

    .line 139
    :cond_10
    invoke-interface {p1}, LaJT;->i()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 140
    invoke-virtual {p2}, LaGe;->c()V

    goto :goto_5

    .line 141
    :cond_11
    invoke-virtual {p2}, LaGe;->f()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 142
    invoke-virtual {p2}, LaGe;->d()V

    goto :goto_5

    :cond_12
    move v0, v1

    .line 146
    goto :goto_6

    :cond_13
    move v2, v1

    .line 147
    goto :goto_7
.end method

.method public static b(Ljava/util/Date;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ss.SSS"

    invoke-static {v1}, LaGG;->a(Ljava/lang/String;)LaHg;

    move-result-object v1

    invoke-virtual {v1, p0}, LaHg;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Z"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/util/Date;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 181
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LaGG;->a:LaHg;

    invoke-virtual {v1, p1}, LaHg;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "z"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/util/Date;
    .locals 1

    .prologue
    .line 171
    if-nez p1, :cond_0

    .line 172
    const/4 v0, 0x0

    .line 176
    :goto_0
    return-object v0

    .line 174
    :cond_0
    const-string v0, "z"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Z"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    sget-object v0, LaGG;->a:LaHg;

    .line 176
    :goto_1
    invoke-virtual {v0, p1}, LaHg;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    goto :goto_0

    .line 174
    :cond_2
    sget-object v0, LaGG;->b:LaHg;

    goto :goto_1
.end method

.method public a(LaJT;LaGa;)V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0, p1, p2}, LaGG;->a(LaJT;LaGe;)V

    .line 154
    return-void
.end method

.method public a(LaJT;LaGc;)V
    .locals 2

    .prologue
    .line 159
    invoke-direct {p0, p1, p2}, LaGG;->a(LaJT;LaGe;)V

    .line 160
    invoke-interface {p1}, LaJT;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LaGc;->a(Ljava/lang/String;)V

    .line 161
    invoke-interface {p1}, LaJT;->a()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p2, v0}, LaGc;->a(Ljava/lang/Long;)LaGc;

    .line 162
    invoke-interface {p1}, LaJT;->b()Ljava/lang/Long;

    move-result-object v0

    .line 163
    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    invoke-virtual {p2, v0, v1}, LaGc;->b(J)V

    .line 164
    invoke-interface {p1}, LaJT;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LaGc;->a(Ljava/lang/String;)LaGc;

    .line 165
    invoke-interface {p1}, LaJT;->s()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LaGc;->e(Ljava/lang/String;)LaGe;

    .line 166
    invoke-interface {p1}, LaJT;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LaGc;->f(Ljava/lang/String;)LaGe;

    .line 167
    return-void

    .line 163
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method
