.class public LaZJ;
.super Ljava/lang/Object;
.source "OwnersImageManager.java"


# static fields
.field private static a:I


# instance fields
.field private a:F

.field public final a:LaCV;

.field private a:LaZK;

.field public final a:Landroid/content/Context;

.field private final a:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LaZK;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private b:F

.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, -0x1

    sput v0, LaZJ;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LaCV;Z)V
    .locals 2

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, LaZJ;->a:Ljava/util/concurrent/ConcurrentHashMap;

    .line 65
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LaZJ;->a:Ljava/util/LinkedList;

    .line 75
    iput-object p1, p0, LaZJ;->a:Landroid/content/Context;

    .line 76
    iput-object p2, p0, LaZJ;->a:LaCV;

    .line 77
    iput-boolean p3, p0, LaZJ;->b:Z

    .line 78
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 79
    sget v1, LaZY;->cover_photo_ratio_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, LaZJ;->b:F

    .line 80
    sget v1, LaZY;->cover_photo_ratio_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LaZJ;->a:F

    .line 81
    return-void
.end method

.method static synthetic a(LaZJ;)F
    .locals 1

    .prologue
    .line 35
    iget v0, p0, LaZJ;->a:F

    return v0
.end method

.method public static a(Landroid/graphics/Bitmap;IF)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    const/high16 v1, 0x3f000000    # 0.5f

    .line 257
    int-to-float v0, p1

    mul-float/2addr v0, p2

    float-to-int v0, v0

    invoke-static {p0, p1, v0, v1, v1}, LaZJ;->a(Landroid/graphics/Bitmap;IIFF)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/graphics/Bitmap;IIFF)Landroid/graphics/Bitmap;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 293
    cmpg-float v0, p3, v1

    if-ltz v0, :cond_0

    cmpl-float v0, p3, v2

    if-gtz v0, :cond_0

    cmpg-float v0, p4, v1

    if-ltz v0, :cond_0

    cmpl-float v0, p4, v2

    if-lez v0, :cond_1

    .line 295
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "horizontalCenterPercent and verticalCenterPercent must be between 0.0f and 1.0f, inclusive."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 299
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 300
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 303
    if-ne p1, v0, :cond_2

    if-ne p2, v2, :cond_2

    .line 328
    :goto_0
    return-object p0

    .line 307
    :cond_2
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 308
    int-to-float v1, p1

    int-to-float v3, v0

    div-float/2addr v1, v3

    int-to-float v3, p2

    int-to-float v4, v2

    div-float/2addr v3, v4

    invoke-static {v1, v3}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 311
    invoke-virtual {v5, v1, v1}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 316
    int-to-float v3, p1

    div-float/2addr v3, v1

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 317
    int-to-float v4, p2

    div-float v1, v4, v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 318
    int-to-float v1, v0

    mul-float/2addr v1, p3

    div-int/lit8 v6, v3, 0x2

    int-to-float v6, v6

    sub-float/2addr v1, v6

    float-to-int v1, v1

    .line 319
    int-to-float v6, v2

    mul-float/2addr v6, p4

    div-int/lit8 v7, v4, 0x2

    int-to-float v7, v7

    sub-float/2addr v6, v7

    float-to-int v6, v6

    .line 322
    sub-int/2addr v0, v3

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v8}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 323
    sub-int v0, v2, v4

    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v8}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 325
    const/4 v6, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p0

    goto :goto_0
.end method

.method static synthetic a(LaZJ;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, LaZJ;->a:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, LaZJ;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    iget-object v0, p0, LaZJ;->a:LaZK;

    if-nez v0, :cond_0

    .line 153
    iget-object v0, p0, LaZJ;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaZK;

    iput-object v0, p0, LaZJ;->a:LaZK;

    .line 154
    iget-object v0, p0, LaZJ;->a:LaZK;

    invoke-virtual {v0}, LaZK;->a()V

    goto :goto_0
.end method

.method static synthetic a(LaZJ;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, LaZJ;->a(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private a(Landroid/widget/ImageView;LaZK;)V
    .locals 2

    .prologue
    .line 112
    invoke-virtual {p0, p1}, LaZJ;->a(Landroid/widget/ImageView;)V

    .line 114
    iget-object v0, p0, LaZJ;->a:LaCV;

    invoke-interface {v0}, LaCV;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 115
    const-string v0, "AvatarManager"

    const-string v1, "Client not connected."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    :goto_0
    return-void

    .line 119
    :cond_0
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 120
    iget-object v0, p0, LaZJ;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 122
    invoke-direct {p0}, LaZJ;->a()V

    goto :goto_0
.end method

.method private a(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 332
    if-eqz p2, :cond_1

    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 333
    new-instance v0, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    .line 334
    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aput-object v3, v1, v2

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, LaZJ;->a:Landroid/content/Context;

    .line 335
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v2, v1, v4

    invoke-direct {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 337
    sget v1, LaZJ;->a:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 338
    iget-object v1, p0, LaZJ;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x10e0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, LaZJ;->a:I

    .line 341
    :cond_0
    invoke-virtual {v0, v4}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 342
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 343
    sget v1, LaZJ;->a:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 347
    :goto_0
    return-void

    .line 345
    :cond_1
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method static synthetic a(LaZJ;)Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, LaZJ;->b:Z

    return v0
.end method

.method static synthetic b(LaZJ;)F
    .locals 1

    .prologue
    .line 35
    iget v0, p0, LaZJ;->b:F

    return v0
.end method


# virtual methods
.method public a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, LaZJ;->a:Landroid/content/Context;

    return-object v0
.end method

.method public a(LaZK;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, LaZJ;->a:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v1, p1, LaZK;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v1, p1, LaZK;->a:Landroid/widget/ImageView;

    iget-object v0, p0, LaZJ;->a:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v2, p1, LaZK;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 101
    iget-object v0, p1, LaZK;->a:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, LaZJ;->a(Landroid/widget/ImageView;)V

    .line 105
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-object v0, p1, LaZK;->a:Landroid/widget/ImageView;

    invoke-direct {p0, v0, p1}, LaZJ;->a(Landroid/widget/ImageView;LaZK;)V

    goto :goto_0
.end method

.method public a(Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 129
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 130
    const/4 v0, 0x0

    move v1, v0

    .line 132
    :goto_0
    iget-object v0, p0, LaZJ;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 133
    iget-object v0, p0, LaZJ;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaZK;

    iget-object v0, v0, LaZK;->a:Landroid/widget/ImageView;

    if-ne v0, p1, :cond_0

    .line 134
    iget-object v0, p0, LaZJ;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 136
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 140
    :cond_1
    iget-object v0, p0, LaZJ;->a:LaZK;

    if-eqz v0, :cond_2

    iget-object v0, p0, LaZJ;->a:LaZK;

    iget-object v0, v0, LaZK;->a:Landroid/widget/ImageView;

    if-ne v0, p1, :cond_2

    .line 141
    iget-object v0, p0, LaZJ;->a:LaZK;

    const/4 v1, 0x1

    iput-boolean v1, v0, LaZK;->a:Z

    .line 143
    :cond_2
    return-void
.end method

.method public a(Lcom/google/android/gms/common/api/Status;Landroid/os/ParcelFileDescriptor;LaZK;Landroid/graphics/Bitmap;I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 159
    .line 161
    :try_start_0
    iget-object v0, p0, LaZJ;->a:LaZK;

    if-eq v0, p3, :cond_2

    .line 162
    const-string v0, "AvatarManager"

    const-string v1, "Got a different request than we\'re waiting for!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    iget-boolean v0, p0, LaZJ;->a:Z

    if-nez v0, :cond_0

    .line 186
    invoke-direct {p0}, LaZJ;->a()V

    .line 188
    :cond_0
    if-eqz p2, :cond_1

    .line 190
    :try_start_1
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 195
    :cond_1
    :goto_0
    return-void

    .line 166
    :cond_2
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, LaZJ;->a:LaZK;

    .line 167
    iget-boolean v0, p0, LaZJ;->a:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_4

    .line 185
    iget-boolean v0, p0, LaZJ;->a:Z

    if-nez v0, :cond_3

    .line 186
    invoke-direct {p0}, LaZJ;->a()V

    .line 188
    :cond_3
    if-eqz p2, :cond_1

    .line 190
    :try_start_3
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 191
    :catch_0
    move-exception v0

    goto :goto_0

    .line 170
    :cond_4
    :try_start_4
    iget-object v0, p3, LaZK;->a:Landroid/widget/ImageView;

    .line 171
    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p3, :cond_5

    iget-boolean v0, p3, LaZK;->a:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v0, :cond_7

    .line 185
    :cond_5
    iget-boolean v0, p0, LaZJ;->a:Z

    if-nez v0, :cond_6

    .line 186
    invoke-direct {p0}, LaZJ;->a()V

    .line 188
    :cond_6
    if-eqz p2, :cond_1

    .line 190
    :try_start_5
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_0

    .line 191
    :catch_1
    move-exception v0

    goto :goto_0

    .line 176
    :cond_7
    :try_start_6
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    if-nez p2, :cond_9

    .line 177
    :cond_8
    const-string v0, "AvatarManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Avatar loaded: status="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  pfd="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    :cond_9
    if-eqz p2, :cond_a

    .line 181
    new-instance v0, LaZL;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p2

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, LaZL;-><init>(LaZJ;LaZK;Landroid/os/ParcelFileDescriptor;Landroid/graphics/Bitmap;I)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, LaZL;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-object p2, v6

    .line 185
    :cond_a
    iget-boolean v0, p0, LaZJ;->a:Z

    if-nez v0, :cond_b

    .line 186
    invoke-direct {p0}, LaZJ;->a()V

    .line 188
    :cond_b
    if-eqz p2, :cond_1

    .line 190
    :try_start_7
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_0

    .line 191
    :catch_2
    move-exception v0

    goto :goto_0

    .line 185
    :catchall_0
    move-exception v0

    iget-boolean v1, p0, LaZJ;->a:Z

    if-nez v1, :cond_c

    .line 186
    invoke-direct {p0}, LaZJ;->a()V

    .line 188
    :cond_c
    if-eqz p2, :cond_d

    .line 190
    :try_start_8
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 192
    :cond_d
    :goto_1
    throw v0

    .line 191
    :catch_3
    move-exception v0

    goto/16 :goto_0

    :catch_4
    move-exception v1

    goto :goto_1
.end method
