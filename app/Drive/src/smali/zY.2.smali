.class LzY;
.super LzM;
.source "CompoundAdapterToMainAdapterVisibleRange.java"


# instance fields
.field private final a:LzZ;


# direct methods
.method constructor <init>(LzN;LzZ;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0, p1}, LzM;-><init>(LzN;)V

    .line 18
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LzZ;

    iput-object v0, p0, LzY;->a:LzZ;

    .line 19
    return-void
.end method


# virtual methods
.method protected a(LalS;)LalS;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 23
    iget-object v0, p0, LzY;->a:LzZ;

    invoke-interface {v0}, LzZ;->a()I

    move-result v0

    .line 24
    iget-object v1, p0, LzY;->a:LzZ;

    invoke-interface {v1}, LzZ;->b()I

    move-result v1

    .line 26
    if-nez v0, :cond_0

    if-nez v1, :cond_0

    .line 38
    :goto_0
    return-object p1

    .line 29
    :cond_0
    invoke-virtual {p1}, LalS;->a()I

    move-result v2

    sub-int v2, v0, v2

    invoke-virtual {p1}, LalS;->c()I

    move-result v3

    invoke-static {v2, v4, v3}, LalX;->a(III)I

    move-result v2

    .line 30
    iget-object v3, p0, LzY;->a:LzZ;

    invoke-interface {v3}, LzZ;->getCount()I

    move-result v3

    sub-int v1, v3, v1

    .line 32
    invoke-virtual {p1}, LalS;->b()I

    move-result v3

    sub-int v1, v3, v1

    invoke-virtual {p1}, LalS;->c()I

    move-result v3

    invoke-static {v1, v4, v3}, LalX;->a(III)I

    move-result v1

    .line 34
    invoke-virtual {p1}, LalS;->c()I

    move-result v3

    sub-int v2, v3, v2

    sub-int v1, v2, v1

    .line 35
    invoke-virtual {p1}, LalS;->a()I

    move-result v2

    sub-int v0, v2, v0

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 37
    invoke-static {v0, v1}, LalS;->b(II)LalS;

    move-result-object p1

    goto :goto_0
.end method
