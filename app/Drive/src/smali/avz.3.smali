.class public Lavz;
.super Ljava/lang/Object;
.source "FilmScrollView.java"

# interfaces
.implements LavA;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LavA;"
    }
.end annotation


# instance fields
.field private a:F

.field private final a:I

.field private final a:Landroid/widget/OverScroller;

.field final synthetic a:Lcom/google/android/apps/viewer/film/FilmScrollView;

.field private a:Z

.field private b:F

.field private b:I

.field private b:Z

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/viewer/film/FilmScrollView;)V
    .locals 2

    .prologue
    .line 522
    iput-object p1, p0, Lavz;->a:Lcom/google/android/apps/viewer/film/FilmScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 503
    const/16 v0, 0x3e8

    iput v0, p0, Lavz;->a:I

    .line 505
    const/4 v0, 0x0

    iput v0, p0, Lavz;->a:F

    .line 523
    new-instance v0, Landroid/widget/OverScroller;

    invoke-virtual {p1}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lavz;->a:Landroid/widget/OverScroller;

    .line 524
    iget-object v0, p0, Lavz;->a:Landroid/widget/OverScroller;

    const v1, 0x3c75c28f    # 0.015f

    invoke-virtual {v0, v1}, Landroid/widget/OverScroller;->setFriction(F)V

    .line 525
    return-void
.end method

.method private a(I)I
    .locals 2

    .prologue
    .line 688
    int-to-float v0, p1

    iget v1, p0, Lavz;->b:F

    div-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private a(F)V
    .locals 3

    .prologue
    .line 537
    iput p1, p0, Lavz;->a:F

    .line 538
    iget-object v0, p0, Lavz;->a:Lcom/google/android/apps/viewer/film/FilmScrollView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->a(Lcom/google/android/apps/viewer/film/FilmScrollView;)Lcom/google/android/apps/viewer/film/FilmView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/film/FilmView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    .line 539
    iget v1, p0, Lavz;->a:F

    mul-float/2addr v1, v0

    iget-object v2, p0, Lavz;->a:Lcom/google/android/apps/viewer/film/FilmScrollView;

    invoke-virtual {v2}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget-object v2, p0, Lavz;->a:Lcom/google/android/apps/viewer/film/FilmScrollView;

    invoke-virtual {v2}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v0, v2

    div-float v0, v1, v0

    iput v0, p0, Lavz;->b:F

    .line 540
    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 528
    iput-boolean p1, p0, Lavz;->b:Z

    .line 529
    const/4 v0, 0x1

    iput-boolean v0, p0, Lavz;->c:Z

    .line 530
    iput v1, p0, Lavz;->b:I

    .line 531
    iget-object v0, p0, Lavz;->a:Lcom/google/android/apps/viewer/film/FilmScrollView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->a(Lcom/google/android/apps/viewer/film/FilmScrollView;)F

    move-result v0

    invoke-direct {p0, v0}, Lavz;->a(F)V

    .line 532
    iput-boolean v1, p0, Lavz;->a:Z

    .line 533
    return-void
.end method

.method private b(I)I
    .locals 2

    .prologue
    .line 693
    int-to-float v0, p1

    iget v1, p0, Lavz;->b:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private b()V
    .locals 7

    .prologue
    const/16 v6, 0x3e8

    .line 609
    iget v0, p0, Lavz;->a:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 610
    const-string v0, "FilmScrollView"

    const-string v1, "ComputeNextZoom with currZoom == 0"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    :cond_0
    iget-boolean v0, p0, Lavz;->c:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lavz;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lavz;->a:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->getStartX()I

    move-result v0

    iget-object v1, p0, Lavz;->a:Landroid/widget/OverScroller;

    invoke-virtual {v1}, Landroid/widget/OverScroller;->getFinalX()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-ge v0, v6, :cond_2

    .line 636
    :cond_1
    :goto_0
    return-void

    .line 616
    :cond_2
    iget-object v0, p0, Lavz;->a:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->getStartX()I

    move-result v0

    iget-object v1, p0, Lavz;->a:Landroid/widget/OverScroller;

    invoke-virtual {v1}, Landroid/widget/OverScroller;->getCurrX()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 617
    iget-object v0, p0, Lavz;->a:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->getFinalX()I

    move-result v0

    iget-object v1, p0, Lavz;->a:Landroid/widget/OverScroller;

    invoke-virtual {v1}, Landroid/widget/OverScroller;->getCurrX()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 618
    const v0, 0x3f333333    # 0.7f

    .line 619
    const/high16 v4, 0x3f800000    # 1.0f

    .line 620
    sub-float v1, v4, v0

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float v5, v1, v5

    .line 622
    if-ge v2, v3, :cond_4

    const/4 v1, 0x1

    .line 625
    :goto_1
    if-eqz v1, :cond_5

    .line 626
    if-ge v2, v6, :cond_3

    int-to-float v0, v2

    mul-float/2addr v0, v5

    sub-float v0, v4, v0

    iget v1, p0, Lavz;->a:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 635
    :cond_3
    :goto_2
    invoke-direct {p0, v0}, Lavz;->a(F)V

    goto :goto_0

    .line 622
    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    .line 628
    :cond_5
    iget-boolean v1, p0, Lavz;->a:Z

    if-nez v1, :cond_3

    if-gt v3, v6, :cond_3

    .line 631
    int-to-float v0, v3

    mul-float/2addr v0, v5

    sub-float v0, v4, v0

    iget v1, p0, Lavz;->a:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto :goto_2
.end method


# virtual methods
.method public a()F
    .locals 2

    .prologue
    .line 562
    iget v0, p0, Lavz;->a:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 563
    const-string v0, "FilmScrollView"

    const-string v1, "GetNextZoom with currZoom == 0"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 565
    :cond_0
    iget v0, p0, Lavz;->a:F

    return v0
.end method

.method public a()I
    .locals 1

    .prologue
    .line 549
    iget-boolean v0, p0, Lavz;->c:Z

    if-eqz v0, :cond_0

    .line 550
    iget-object v0, p0, Lavz;->a:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->getCurrX()I

    move-result v0

    invoke-direct {p0, v0}, Lavz;->b(I)I

    move-result v0

    .line 552
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lavz;->b:I

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 682
    invoke-virtual {p0}, Lavz;->a()I

    move-result v0

    iput v0, p0, Lavz;->b:I

    .line 683
    const/4 v0, 0x0

    iput-boolean v0, p0, Lavz;->c:Z

    .line 684
    return-void
.end method

.method public a(IIII)V
    .locals 5

    .prologue
    .line 640
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lavz;->a(Z)V

    .line 641
    iget-object v0, p0, Lavz;->a:Landroid/widget/OverScroller;

    invoke-direct {p0, p1}, Lavz;->a(I)I

    move-result v1

    invoke-direct {p0, p2}, Lavz;->a(I)I

    move-result v2

    invoke-direct {p0, p3}, Lavz;->a(I)I

    move-result v3

    invoke-direct {p0, p4}, Lavz;->a(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/OverScroller;->startScroll(IIII)V

    .line 642
    return-void
.end method

.method public a(IIIIIIIIII)V
    .locals 12

    .prologue
    .line 656
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lavz;->a(Z)V

    .line 657
    iget-object v1, p0, Lavz;->a:Lcom/google/android/apps/viewer/film/FilmScrollView;

    invoke-static {v1}, Lcom/google/android/apps/viewer/film/FilmScrollView;->b(Lcom/google/android/apps/viewer/film/FilmScrollView;)V

    .line 658
    sget-boolean v1, LavX;->e:Z

    if-eqz v1, :cond_0

    .line 659
    const-string v1, "FilmScrollView"

    const-string v2, "DBG Fling (EXT) @%s, v:%s z:%s] in (%s, %s)"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 660
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Lavz;->a:Lcom/google/android/apps/viewer/film/FilmScrollView;

    invoke-static {v5}, Lcom/google/android/apps/viewer/film/FilmScrollView;->a(Lcom/google/android/apps/viewer/film/FilmScrollView;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 659
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 662
    :cond_0
    iget-object v1, p0, Lavz;->a:Landroid/widget/OverScroller;

    invoke-direct {p0, p1}, Lavz;->a(I)I

    move-result v2

    invoke-direct {p0, p2}, Lavz;->a(I)I

    move-result v3

    .line 663
    invoke-direct {p0, p3}, Lavz;->a(I)I

    move-result v4

    div-int/lit8 v4, v4, 0x1

    .line 664
    move/from16 v0, p4

    invoke-direct {p0, v0}, Lavz;->a(I)I

    move-result v5

    div-int/lit8 v5, v5, 0x1

    .line 665
    move/from16 v0, p5

    invoke-direct {p0, v0}, Lavz;->a(I)I

    move-result v6

    move/from16 v0, p6

    invoke-direct {p0, v0}, Lavz;->a(I)I

    move-result v7

    .line 666
    move/from16 v0, p7

    invoke-direct {p0, v0}, Lavz;->a(I)I

    move-result v8

    move/from16 v0, p8

    invoke-direct {p0, v0}, Lavz;->a(I)I

    move-result v9

    .line 667
    move/from16 v0, p9

    invoke-direct {p0, v0}, Lavz;->a(I)I

    move-result v10

    move/from16 v0, p10

    invoke-direct {p0, v0}, Lavz;->a(I)I

    move-result v11

    .line 662
    invoke-virtual/range {v1 .. v11}, Landroid/widget/OverScroller;->fling(IIIIIIIIII)V

    .line 669
    sget-boolean v1, LavX;->e:Z

    if-eqz v1, :cond_1

    .line 670
    const-string v1, "FilmScrollView"

    const-string v2, "DBG Fling (INT) @%s, v:%s z:%s] in (%s, %s): [%s, %s]"

    const/4 v3, 0x7

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 671
    invoke-direct {p0, p1}, Lavz;->a(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-direct {p0, p3}, Lavz;->a(I)I

    move-result v5

    div-int/lit8 v5, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Lavz;->a:Lcom/google/android/apps/viewer/film/FilmScrollView;

    invoke-static {v5}, Lcom/google/android/apps/viewer/film/FilmScrollView;->a(Lcom/google/android/apps/viewer/film/FilmScrollView;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    .line 672
    move/from16 v0, p5

    invoke-direct {p0, v0}, Lavz;->a(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    move/from16 v0, p6

    invoke-direct {p0, v0}, Lavz;->a(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    iget-object v5, p0, Lavz;->a:Landroid/widget/OverScroller;

    invoke-virtual {v5}, Landroid/widget/OverScroller;->getStartX()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x6

    iget-object v5, p0, Lavz;->a:Landroid/widget/OverScroller;

    invoke-virtual {v5}, Landroid/widget/OverScroller;->getFinalX()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 670
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 674
    :cond_1
    iget-object v1, p0, Lavz;->a:Landroid/widget/OverScroller;

    invoke-virtual {v1}, Landroid/widget/OverScroller;->getFinalX()I

    move-result v1

    move/from16 v0, p6

    invoke-direct {p0, v0}, Lavz;->a(I)I

    move-result v2

    if-lt v1, v2, :cond_2

    .line 676
    const/4 v1, 0x1

    iput-boolean v1, p0, Lavz;->a:Z

    .line 678
    :cond_2
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 544
    iget-boolean v0, p0, Lavz;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lavz;->a:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(IIIIII)Z
    .locals 1

    .prologue
    .line 648
    iget-object v0, p0, Lavz;->a:Lcom/google/android/apps/viewer/film/FilmScrollView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->b(Lcom/google/android/apps/viewer/film/FilmScrollView;)V

    .line 649
    iget-object v0, p0, Lavz;->a:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->abortAnimation()V

    .line 650
    const/4 v0, 0x1

    return v0
.end method

.method public b()F
    .locals 1

    .prologue
    .line 570
    iget-object v0, p0, Lavz;->a:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->getCurrVelocity()F

    move-result v0

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 558
    iget-object v0, p0, Lavz;->a:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->getCurrY()I

    move-result v0

    invoke-direct {p0, v0}, Lavz;->b(I)I

    move-result v0

    return v0
.end method

.method public b()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 595
    iget-object v2, p0, Lavz;->a:Landroid/widget/OverScroller;

    invoke-virtual {v2}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 596
    iget-object v2, p0, Lavz;->a:Lcom/google/android/apps/viewer/film/FilmScrollView;

    invoke-static {v2}, Lcom/google/android/apps/viewer/film/FilmScrollView;->b(Lcom/google/android/apps/viewer/film/FilmScrollView;)V

    .line 597
    invoke-direct {p0}, Lavz;->b()V

    .line 598
    sget-boolean v2, LavX;->e:Z

    if-eqz v2, :cond_0

    .line 599
    const-string v2, "FilmScrollView"

    const-string v3, "Scroller [S:%s, C:%s, F:%s]; Ext:[S:%s, C:%s, F:%s] @z %s"

    const/4 v4, 0x7

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lavz;->a:Landroid/widget/OverScroller;

    .line 600
    invoke-virtual {v5}, Landroid/widget/OverScroller;->getStartX()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    iget-object v1, p0, Lavz;->a:Landroid/widget/OverScroller;

    invoke-virtual {v1}, Landroid/widget/OverScroller;->getCurrX()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v1, 0x2

    iget-object v5, p0, Lavz;->a:Landroid/widget/OverScroller;

    invoke-virtual {v5}, Landroid/widget/OverScroller;->getFinalX()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x3

    .line 601
    invoke-virtual {p0}, Lavz;->c()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x4

    invoke-virtual {p0}, Lavz;->a()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x5

    invoke-virtual {p0}, Lavz;->d()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x6

    iget v5, p0, Lavz;->a:F

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v1

    .line 599
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 605
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 575
    iget-object v0, p0, Lavz;->a:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->getStartX()I

    move-result v0

    invoke-direct {p0, v0}, Lavz;->b(I)I

    move-result v0

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 585
    iget-object v0, p0, Lavz;->a:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->getFinalX()I

    move-result v0

    invoke-direct {p0, v0}, Lavz;->b(I)I

    move-result v0

    return v0
.end method
