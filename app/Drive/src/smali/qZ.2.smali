.class public LqZ;
.super Ljava/lang/Object;
.source "AccountInfoBanner.java"


# instance fields
.field private final a:LQr;

.field private final a:LSF;

.field private final a:Landroid/content/Context;

.field private final a:Landroid/os/Handler;

.field private final a:Landroid/view/View;

.field private final a:Ljava/lang/Runnable;

.field private final a:LtK;

.field private final b:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(LaFO;LSF;LtK;LabF;LQr;Landroid/content/Context;Landroid/view/View;)V
    .locals 7

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    invoke-static {}, Lanj;->a()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, LqZ;->a:Landroid/os/Handler;

    .line 72
    iput-object p2, p0, LqZ;->a:LSF;

    .line 73
    iput-object p3, p0, LqZ;->a:LtK;

    .line 74
    iput-object p5, p0, LqZ;->a:LQr;

    .line 75
    iput-object p6, p0, LqZ;->a:Landroid/content/Context;

    .line 77
    new-instance v0, Lra;

    invoke-direct {v0, p0}, Lra;-><init>(LqZ;)V

    iput-object v0, p0, LqZ;->b:Ljava/lang/Runnable;

    .line 84
    new-instance v0, Lrb;

    invoke-direct {v0, p0}, Lrb;-><init>(LqZ;)V

    iput-object v0, p0, LqZ;->a:Ljava/lang/Runnable;

    .line 96
    new-instance v2, LabI;

    invoke-direct {v2, p6}, LabI;-><init>(Landroid/content/Context;)V

    .line 97
    invoke-virtual {p1}, LaFO;->b()Ljava/lang/String;

    move-result-object v0

    sget v4, Lxe;->account_info_banner:I

    const/4 v6, 0x1

    move-object v1, p7

    move-object v3, p4

    move-object v5, p6

    invoke-static/range {v0 .. v6}, LajS;->a(Ljava/lang/String;Landroid/view/View;LabI;LabF;ILandroid/content/Context;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LqZ;->a:Landroid/view/View;

    .line 99
    return-void
.end method

.method static synthetic a(LqZ;)LQr;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, LqZ;->a:LQr;

    return-object v0
.end method

.method static synthetic a(LqZ;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, LqZ;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(LqZ;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, LqZ;->b:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic a(LqZ;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, LqZ;->d()V

    return-void
.end method

.method static synthetic b(LqZ;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, LqZ;->c()V

    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, LqZ;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 106
    iget-object v0, p0, LqZ;->a:Landroid/content/Context;

    sget v1, LwV;->fade_in:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 107
    iget-object v1, p0, LqZ;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 108
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, LqZ;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 115
    iget-object v0, p0, LqZ;->a:Landroid/content/Context;

    sget v1, LwV;->fade_out:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 116
    iget-object v1, p0, LqZ;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 117
    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 123
    iget-object v0, p0, LqZ;->a:LtK;

    sget-object v1, Lry;->y:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LqZ;->a:LSF;

    .line 124
    invoke-interface {v0}, LSF;->a()[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    iget-object v0, p0, LqZ;->a:LQr;

    const-string v1, "displayAccountInfoBannerDelay"

    const/16 v2, 0x2bc

    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    .line 130
    iget-object v1, p0, LqZ;->a:Landroid/os/Handler;

    iget-object v2, p0, LqZ;->a:Ljava/lang/Runnable;

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, LqZ;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, LqZ;->a:Landroid/os/Handler;

    iget-object v1, p0, LqZ;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 140
    :cond_0
    iget-object v0, p0, LqZ;->b:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 141
    iget-object v0, p0, LqZ;->a:Landroid/os/Handler;

    iget-object v1, p0, LqZ;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 143
    :cond_1
    return-void
.end method
