.class public LOG;
.super Ljava/lang/Object;
.source "PhotosHighlightsLauncher.java"

# interfaces
.implements Lasb;


# instance fields
.field private final a:LSu;

.field private final a:Lasa;

.field private final a:Lati;

.field private final a:Lrg;

.field private final a:LtK;


# direct methods
.method public constructor <init>(LbiP;Lasa;LtK;Lati;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbiP",
            "<",
            "LSu;",
            ">;",
            "Lasa;",
            "LtK;",
            "Lati;",
            ")V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    sget-object v0, LOE;->b:Lrg;

    iput-object v0, p0, LOG;->a:Lrg;

    .line 53
    invoke-virtual {p1}, LbiP;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LbiP;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSu;

    :goto_0
    iput-object v0, p0, LOG;->a:LSu;

    .line 54
    iput-object p2, p0, LOG;->a:Lasa;

    .line 55
    iput-object p3, p0, LOG;->a:LtK;

    .line 56
    iput-object p4, p0, LOG;->a:Lati;

    .line 57
    return-void

    .line 53
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 101
    iget-object v0, p0, LOG;->a:Lati;

    iget-object v1, p0, LOG;->a:Lrg;

    invoke-interface {v0, v1}, Lati;->a(Lrg;)LasP;

    move-result-object v0

    .line 102
    new-instance v1, LasM;

    invoke-direct {v1}, LasM;-><init>()V

    sget-object v2, LasO;->c:LasO;

    invoke-virtual {v1, v2}, LasM;->a(LasO;)LasM;

    move-result-object v1

    .line 103
    invoke-interface {v0}, LasP;->a()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, LasM;->a(Landroid/net/Uri;)LasM;

    move-result-object v1

    .line 104
    invoke-interface {v0}, LasP;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LasM;->a(Ljava/lang/String;)LasM;

    move-result-object v0

    invoke-virtual {v0}, LasM;->a()LasJ;

    move-result-object v0

    .line 106
    invoke-static {p1, v0}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->a(Landroid/content/Context;LasJ;)Landroid/content/Intent;

    move-result-object v0

    .line 107
    sget v1, LOV;->welcome_photo_backup_suggest_positive_button_label:I

    .line 108
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 109
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->b(Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 110
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 111
    return-void
.end method

.method private a(ILaFO;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 83
    iget-object v0, p0, LOG;->a:LSu;

    if-eqz v0, :cond_0

    iget-object v0, p0, LOG;->a:Lrg;

    if-eqz v0, :cond_0

    iget-object v0, p0, LOG;->a:LtK;

    iget-object v2, p0, LOG;->a:Lrg;

    .line 84
    invoke-interface {v0, v2, p2}, LtK;->a(Lrg;LaFO;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 97
    :goto_0
    return v0

    .line 88
    :cond_1
    add-int/lit8 v0, p1, 0x1

    .line 89
    :goto_1
    iget-object v2, p0, LOG;->a:Lasa;

    invoke-virtual {v2}, Lasa;->a()I

    move-result v2

    if-gt v0, v2, :cond_3

    .line 90
    iget-object v2, p0, LOG;->a:Lasa;

    .line 91
    invoke-virtual {v2, v0}, Lasa;->a(I)Ljava/util/Set;

    move-result-object v2

    .line 92
    iget-object v3, p0, LOG;->a:Lrg;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 93
    const/4 v0, 0x1

    goto :goto_0

    .line 89
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 97
    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;ILaFO;)Z
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0, p2, p3}, LOG;->a(ILaFO;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    invoke-direct {p0, p1}, LOG;->a(Landroid/content/Context;)V

    .line 76
    const/4 v0, 0x1

    .line 78
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
