.class public final enum LalZ;
.super Ljava/lang/Enum;
.source "MediaStoreUtilities.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LalZ;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LalZ;

.field private static final synthetic a:[LalZ;

.field public static final enum b:LalZ;

.field public static final enum c:LalZ;

.field public static final enum d:LalZ;


# instance fields
.field private final a:Lama;

.field private final a:Landroid/net/Uri;

.field private final a:Ljava/lang/String;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 63
    new-instance v0, LalZ;

    const-string v1, "INTERNAL_PHOTOS"

    sget-object v3, Lama;->a:Lama;

    sget-object v4, Lamb;->a:Lamb;

    sget-object v5, Landroid/provider/MediaStore$Images$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct/range {v0 .. v5}, LalZ;-><init>(Ljava/lang/String;ILama;Lamb;Landroid/net/Uri;)V

    sput-object v0, LalZ;->a:LalZ;

    .line 64
    new-instance v3, LalZ;

    const-string v4, "EXTERNAL_PHOTOS"

    sget-object v6, Lama;->a:Lama;

    sget-object v7, Lamb;->b:Lamb;

    sget-object v8, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move v5, v9

    invoke-direct/range {v3 .. v8}, LalZ;-><init>(Ljava/lang/String;ILama;Lamb;Landroid/net/Uri;)V

    sput-object v3, LalZ;->b:LalZ;

    .line 65
    new-instance v3, LalZ;

    const-string v4, "INTERNAL_VIDEOS"

    sget-object v6, Lama;->b:Lama;

    sget-object v7, Lamb;->a:Lamb;

    sget-object v8, Landroid/provider/MediaStore$Video$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    move v5, v10

    invoke-direct/range {v3 .. v8}, LalZ;-><init>(Ljava/lang/String;ILama;Lamb;Landroid/net/Uri;)V

    sput-object v3, LalZ;->c:LalZ;

    .line 66
    new-instance v3, LalZ;

    const-string v4, "EXTERNAL_VIDEOS"

    sget-object v6, Lama;->b:Lama;

    sget-object v7, Lamb;->b:Lamb;

    sget-object v8, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move v5, v11

    invoke-direct/range {v3 .. v8}, LalZ;-><init>(Ljava/lang/String;ILama;Lamb;Landroid/net/Uri;)V

    sput-object v3, LalZ;->d:LalZ;

    .line 62
    const/4 v0, 0x4

    new-array v0, v0, [LalZ;

    sget-object v1, LalZ;->a:LalZ;

    aput-object v1, v0, v2

    sget-object v1, LalZ;->b:LalZ;

    aput-object v1, v0, v9

    sget-object v1, LalZ;->c:LalZ;

    aput-object v1, v0, v10

    sget-object v1, LalZ;->d:LalZ;

    aput-object v1, v0, v11

    sput-object v0, LalZ;->a:[LalZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILama;Lamb;Landroid/net/Uri;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lama;",
            "Lamb;",
            "Landroid/net/Uri;",
            ")V"
        }
    .end annotation

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 74
    iput-object p3, p0, LalZ;->a:Lama;

    .line 75
    iput-object p5, p0, LalZ;->a:Landroid/net/Uri;

    .line 76
    invoke-virtual {p5}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LalZ;->a:Ljava/util/List;

    .line 77
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v1, "%s-%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p3}, Lama;->a(Lama;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 78
    invoke-static {p4}, Lamb;->a(Lamb;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 77
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LalZ;->a:Ljava/lang/String;

    .line 79
    return-void
.end method

.method static synthetic a(LalZ;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, LalZ;->a:Landroid/net/Uri;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LalZ;
    .locals 1

    .prologue
    .line 62
    const-class v0, LalZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LalZ;

    return-object v0
.end method

.method public static values()[LalZ;
    .locals 1

    .prologue
    .line 62
    sget-object v0, LalZ;->a:[LalZ;

    invoke-virtual {v0}, [LalZ;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LalZ;

    return-object v0
.end method


# virtual methods
.method a(Landroid/net/Uri;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 94
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    .line 95
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    iget-object v3, p0, LalZ;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 98
    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, LalZ;->a:Ljava/util/List;

    iget-object v3, p0, LalZ;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v1, v0, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
