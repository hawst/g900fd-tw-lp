.class public Lach;
.super Ljava/lang/Object;
.source "ShareLinkCreator.java"


# annotations
.annotation runtime Lbxz;
.end annotation


# instance fields
.field private final a:LQr;

.field private final a:LtK;


# direct methods
.method public constructor <init>(LtK;LQr;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lach;->a:LtK;

    .line 29
    iput-object p2, p0, Lach;->a:LQr;

    .line 30
    return-void
.end method


# virtual methods
.method public a(LaGu;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 36
    iget-object v1, p0, Lach;->a:LtK;

    sget-object v2, Lry;->aw:Lry;

    invoke-interface {v1, v2}, LtK;->a(LtJ;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 37
    instance-of v1, p1, LaGo;

    if-eqz v1, :cond_1

    .line 38
    check-cast p1, LaGo;

    invoke-interface {p1}, LaGo;->a()Ljava/lang/String;

    move-result-object v0

    .line 58
    :cond_0
    :goto_0
    return-object v0

    .line 39
    :cond_1
    instance-of v1, p1, LaFV;

    if-eqz v1, :cond_0

    .line 40
    invoke-interface {p1}, LaGu;->i()Ljava/lang/String;

    move-result-object v1

    .line 41
    if-eqz v1, :cond_0

    .line 45
    iget-object v0, p0, Lach;->a:LQr;

    const-string v2, "folderSendLinkUrlPattern"

    const-string v3, "https://drive.google.com/folder/d/%1$s/edit"

    invoke-interface {v0, v2, v3}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 54
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
