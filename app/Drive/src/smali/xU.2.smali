.class LxU;
.super Ljava/lang/Object;
.source "CrossAppPromoCatalogInfoParser.java"


# instance fields
.field private final a:I


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1    # I
        .annotation runtime LQK;
        .end annotation
    .end param

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput p1, p0, LxU;->a:I

    .line 41
    return-void
.end method

.method private a(Lbtk;)LbmY;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbtk;",
            ")",
            "LbmY",
            "<",
            "LxP;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    const-string v0, "promos"

    invoke-virtual {p1, v0}, Lbtk;->a(Ljava/lang/String;)Lbtk;

    move-result-object v0

    .line 78
    invoke-static {}, LbmY;->a()Lbna;

    move-result-object v2

    .line 80
    invoke-virtual {v0}, Lbtk;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 81
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 82
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbth;

    invoke-virtual {v0}, Lbth;->a()Lbtk;

    move-result-object v0

    .line 83
    const-string v4, "url"

    .line 84
    invoke-virtual {v0, v4}, Lbtk;->a(Ljava/lang/String;)Lbth;

    move-result-object v4

    invoke-virtual {v4}, Lbth;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 85
    invoke-static {v4}, Lath;->a(Ljava/lang/String;)Lath;

    move-result-object v4

    .line 86
    const-string v5, "version"

    invoke-virtual {v0, v5}, Lbtk;->a(Ljava/lang/String;)Lbth;

    move-result-object v0

    invoke-virtual {v0}, Lbth;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 87
    invoke-static {v1, v0, v4}, LxP;->a(Ljava/lang/String;Ljava/lang/String;Lath;)LxP;

    move-result-object v0

    invoke-virtual {v2, v0}, Lbna;->a(Ljava/lang/Object;)Lbna;

    goto :goto_0

    .line 90
    :cond_0
    invoke-virtual {v2}, Lbna;->a()LbmY;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;I)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 54
    :try_start_0
    const-string v2, "-"

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 55
    array-length v3, v2

    packed-switch v3, :pswitch_data_0

    .line 64
    new-instance v2, LxT;

    const-string v3, "Failed to parse number in this range %s. Please specify a version range offormal \"fromVersion-\" / \"fromVersion-toVersion\""

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, LxT;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    :catch_0
    move-exception v2

    .line 69
    new-instance v3, LxT;

    const-string v4, "Failed to parse number in this range %s. Please specify a version range offormal \"fromVersion-\" / \"fromVersion-toVersion\""

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p0, v0, v1

    invoke-static {v4, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0, v2}, LxT;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 57
    :pswitch_0
    :try_start_1
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-lt p1, v2, :cond_1

    .line 62
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 57
    goto :goto_0

    .line 60
    :pswitch_1
    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    if-lt p1, v3, :cond_2

    const/4 v3, 0x1

    aget-object v2, v2, v3

    .line 61
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v2

    if-le p1, v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 55
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b(Lbtk;)LbmY;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbtk;",
            ")",
            "LbmY",
            "<",
            "LxO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 95
    const-string v0, "mime-types-mapping"

    .line 96
    invoke-virtual {p1, v0}, Lbtk;->a(Ljava/lang/String;)Lbtg;

    move-result-object v0

    .line 97
    invoke-static {}, LbmY;->a()Lbna;

    move-result-object v1

    .line 99
    invoke-virtual {v0}, Lbtg;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbth;

    .line 100
    invoke-virtual {v0}, Lbth;->a()Lbtk;

    move-result-object v3

    .line 101
    const-string v0, "mime-types"

    invoke-virtual {v3, v0}, Lbtk;->a(Ljava/lang/String;)Lbtg;

    move-result-object v0

    .line 103
    invoke-virtual {v0}, Lbtg;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbth;

    .line 104
    invoke-virtual {v0}, Lbth;->a()Ljava/lang/String;

    move-result-object v0

    .line 105
    invoke-static {v0, v3}, LxO;->a(Ljava/lang/String;Lbtk;)LxO;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbna;->a(Ljava/lang/Object;)Lbna;

    goto :goto_0

    .line 109
    :cond_1
    invoke-virtual {v1}, Lbna;->a()LbmY;

    move-result-object v0

    return-object v0
.end method

.method private c(Lbtk;)LbmY;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbtk;",
            ")",
            "LbmY",
            "<",
            "LxO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    const-string v0, "features-mapping"

    invoke-virtual {p1, v0}, Lbtk;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 117
    invoke-static {}, LbmY;->a()LbmY;

    move-result-object v0

    .line 131
    :goto_0
    return-object v0

    .line 120
    :cond_0
    const-string v0, "features-mapping"

    .line 121
    invoke-virtual {p1, v0}, Lbtk;->a(Ljava/lang/String;)Lbtg;

    move-result-object v0

    .line 122
    invoke-static {}, LbmY;->a()Lbna;

    move-result-object v1

    .line 124
    invoke-virtual {v0}, Lbtg;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbth;

    .line 125
    invoke-virtual {v0}, Lbth;->a()Lbtk;

    move-result-object v0

    .line 126
    const-string v3, "featureName"

    invoke-virtual {v0, v3}, Lbtk;->a(Ljava/lang/String;)Lbth;

    move-result-object v3

    invoke-virtual {v3}, Lbth;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 128
    invoke-static {v3, v0}, LxO;->a(Ljava/lang/String;Lbtk;)LxO;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbna;->a(Ljava/lang/Object;)Lbna;

    goto :goto_1

    .line 131
    :cond_1
    invoke-virtual {v1}, Lbna;->a()LbmY;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method a(Lbtk;)LxS;
    .locals 4

    .prologue
    .line 152
    if-nez p1, :cond_0

    .line 153
    new-instance v0, LxT;

    const-string v1, "The catalog JsonObject is null"

    invoke-direct {v0, v1}, LxT;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lbtk;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 158
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 160
    iget v3, p0, LxU;->a:I

    invoke-static {v1, v3}, LxU;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 164
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbth;

    invoke-virtual {v0}, Lbth;->a()Lbtk;

    move-result-object v0

    .line 166
    invoke-direct {p0, v0}, LxU;->a(Lbtk;)LbmY;

    move-result-object v1

    .line 168
    invoke-direct {p0, v0}, LxU;->b(Lbtk;)LbmY;

    move-result-object v2

    .line 169
    invoke-direct {p0, v0}, LxU;->c(Lbtk;)LbmY;

    move-result-object v0

    .line 172
    invoke-static {v1, v2, v0}, LxS;->a(LbmY;LbmY;LbmY;)LxS;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 180
    :goto_0
    return-object v0

    .line 175
    :catch_0
    move-exception v0

    .line 176
    new-instance v1, LxT;

    const-string v2, "Ensure that the JSON structure matches the expected structure."

    invoke-direct {v1, v2, v0}, LxT;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 180
    :cond_2
    sget-object v0, LxS;->a:LxS;

    goto :goto_0
.end method
