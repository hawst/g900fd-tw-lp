.class LKj;
.super Ljava/lang/Object;
.source "ActionCardFunctionManager.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LKg;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, LKj;-><init>()V

    return-void
.end method

.method private a(Landroid/view/View;I)Z
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 125
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 126
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    move v2, v3

    .line 128
    :goto_0
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 129
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    if-ne v0, v4, :cond_2

    .line 130
    add-int/2addr v2, p2

    .line 132
    if-ltz v2, :cond_0

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-lt v2, v4, :cond_1

    .line 140
    :cond_0
    :goto_1
    return v3

    .line 135
    :cond_1
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 136
    invoke-direct {p0, v0, v1}, LKj;->a(Landroid/view/ViewGroup;Landroid/view/ViewGroup;)Z

    move-result v3

    goto :goto_1

    .line 128
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private a(Landroid/view/ViewGroup;I)Z
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_0
    if-ltz v0, :cond_1

    .line 88
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 89
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    move-result v1

    .line 91
    if-eqz v1, :cond_0

    .line 92
    const/4 v0, 0x1

    .line 95
    :goto_1
    return v0

    .line 87
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 95
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Landroid/view/ViewGroup;Landroid/view/ViewGroup;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 104
    if-nez p2, :cond_1

    .line 115
    :cond_0
    :goto_0
    return v1

    .line 108
    :cond_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getFocusedChild()Landroid/view/View;

    move-result-object v2

    move v0, v1

    .line 109
    :goto_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 110
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-ne v3, v2, :cond_2

    .line 112
    invoke-direct {p0, p2, v0}, LKj;->a(Landroid/view/ViewGroup;I)Z

    move-result v1

    goto :goto_0

    .line 109
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 148
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v1, :cond_1

    .line 161
    :cond_0
    :goto_0
    return v0

    .line 152
    :cond_1
    const/16 v2, 0x13

    if-ne p2, v2, :cond_2

    .line 153
    const/4 v2, -0x1

    invoke-direct {p0, p1, v2}, LKj;->a(Landroid/view/View;I)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 154
    goto :goto_0

    .line 156
    :cond_2
    const/16 v2, 0x14

    if-ne p2, v2, :cond_0

    .line 157
    invoke-direct {p0, p1, v1}, LKj;->a(Landroid/view/View;I)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 158
    goto :goto_0
.end method
