.class public LEU;
.super Landroid/app/AlertDialog$Builder;
.source "DriveDialogBuilder.java"


# instance fields
.field private a:Landroid/content/DialogInterface$OnShowListener;

.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 38
    instance-of v0, p1, Landroid/view/ContextThemeWrapper;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 40
    :goto_0
    invoke-static {p1}, LakQ;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, Lamt;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const/4 v1, 0x0

    .line 38
    :goto_1
    invoke-direct {p0, v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 41
    return-void

    .line 39
    :cond_1
    invoke-static {p1}, LEL;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    goto :goto_0

    .line 40
    :cond_2
    sget v1, Lxj;->CakemixTheme_Dialog:I

    goto :goto_1
.end method

.method static synthetic a(LEU;)Landroid/content/DialogInterface$OnShowListener;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, LEU;->a:Landroid/content/DialogInterface$OnShowListener;

    return-object v0
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 270
    invoke-virtual {p0}, LEU;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 271
    invoke-static {v0}, Lamt;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, LakQ;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(LEU;)Z
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, LEU;->a()Z

    move-result v0

    return v0
.end method

.method static synthetic b(LEU;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, LEU;->a:Z

    return v0
.end method


# virtual methods
.method public a(IILandroid/content/DialogInterface$OnClickListener;)LEU;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, LEU;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    .line 55
    invoke-virtual {p0, v0, p2, p3}, LEU;->a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)LEU;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/DialogInterface$OnShowListener;)LEU;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, LEU;->a:Landroid/content/DialogInterface$OnShowListener;

    .line 48
    return-object p0
.end method

.method public a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)LEU;
    .locals 3

    .prologue
    .line 61
    invoke-direct {p0}, LEU;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    new-instance v0, Landroid/widget/ArrayAdapter;

    .line 63
    invoke-virtual {p0}, LEU;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lxe;->list_item_single_choice:I

    invoke-direct {v0, v1, v2, p1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 64
    invoke-virtual {p0, v0, p2, p3}, LEU;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 68
    :goto_0
    return-object p0

    .line 66
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 75
    iput-boolean p1, p0, LEU;->a:Z

    .line 76
    return-void
.end method

.method public create()Landroid/app/AlertDialog;
    .locals 2

    .prologue
    .line 80
    invoke-super {p0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 83
    new-instance v1, LEV;

    invoke-direct {v1, p0, v0}, LEV;-><init>(LEU;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 266
    return-object v0
.end method

.method public synthetic setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0, p1, p2, p3}, LEU;->a(IILandroid/content/DialogInterface$OnClickListener;)LEU;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0, p1, p2, p3}, LEU;->a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)LEU;

    move-result-object v0

    return-object v0
.end method
