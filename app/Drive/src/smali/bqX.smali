.class final LbqX;
.super LbqG;
.source "MessageDigestHashFunction.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final a:I

.field private final a:Ljava/lang/String;

.field private final a:Ljava/security/MessageDigest;

.field private final a:Z


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, LbqG;-><init>()V

    .line 38
    invoke-static {p1}, LbqX;->a(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    iput-object v0, p0, LbqX;->a:Ljava/security/MessageDigest;

    .line 39
    iget-object v0, p0, LbqX;->a:Ljava/security/MessageDigest;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->getDigestLength()I

    move-result v0

    iput v0, p0, LbqX;->a:I

    .line 40
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LbqX;->a:Ljava/lang/String;

    .line 41
    invoke-direct {p0}, LbqX;->a()Z

    move-result v0

    iput-boolean v0, p0, LbqX;->a:Z

    .line 42
    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/security/MessageDigest;
    .locals 2

    .prologue
    .line 73
    :try_start_0
    invoke-static {p0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 74
    :catch_0
    move-exception v0

    .line 75
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 56
    :try_start_0
    iget-object v0, p0, LbqX;->a:Ljava/security/MessageDigest;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    const/4 v0, 0x1

    .line 59
    :goto_0
    return v0

    .line 58
    :catch_0
    move-exception v0

    .line 59
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()LbqR;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 80
    iget-boolean v0, p0, LbqX;->a:Z

    if-eqz v0, :cond_0

    .line 82
    :try_start_0
    new-instance v1, LbqZ;

    iget-object v0, p0, LbqX;->a:Ljava/security/MessageDigest;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/MessageDigest;

    iget v2, p0, LbqX;->a:I

    const/4 v3, 0x0

    invoke-direct {v1, v0, v2, v3}, LbqZ;-><init>(Ljava/security/MessageDigest;ILbqY;)V
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 87
    :goto_0
    return-object v0

    .line 83
    :catch_0
    move-exception v0

    .line 87
    :cond_0
    new-instance v0, LbqZ;

    iget-object v1, p0, LbqX;->a:Ljava/security/MessageDigest;

    invoke-virtual {v1}, Ljava/security/MessageDigest;->getAlgorithm()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LbqX;->a(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    iget v2, p0, LbqX;->a:I

    invoke-direct {v0, v1, v2, v4}, LbqZ;-><init>(Ljava/security/MessageDigest;ILbqY;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, LbqX;->a:Ljava/lang/String;

    return-object v0
.end method
