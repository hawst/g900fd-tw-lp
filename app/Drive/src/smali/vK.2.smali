.class public final LvK;
.super Ljava/lang/Object;
.source "ChildrenOfCollectionCriterion.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;
    .locals 2

    .prologue
    .line 75
    new-instance v0, Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;-><init>(Landroid/os/Parcel;LvK;)V

    return-object v0
.end method

.method public a(I)[Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;
    .locals 1

    .prologue
    .line 80
    new-array v0, p1, [Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0, p1}, LvK;->a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0, p1}, LvK;->a(I)[Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;

    move-result-object v0

    return-object v0
.end method
