.class public final LaZZ;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f040000

.field public static final abc_action_bar_up_container:I = 0x7f040001

.field public static final abc_action_bar_view_list_nav_layout:I = 0x7f040002

.field public static final abc_action_menu_item_layout:I = 0x7f040003

.field public static final abc_action_menu_layout:I = 0x7f040004

.field public static final abc_action_mode_bar:I = 0x7f040005

.field public static final abc_action_mode_close_item_material:I = 0x7f040006

.field public static final abc_activity_chooser_view:I = 0x7f040007

.field public static final abc_activity_chooser_view_include:I = 0x7f040008

.field public static final abc_activity_chooser_view_list_item:I = 0x7f040009

.field public static final abc_expanded_menu_layout:I = 0x7f04000a

.field public static final abc_list_menu_item_checkbox:I = 0x7f04000b

.field public static final abc_list_menu_item_icon:I = 0x7f04000c

.field public static final abc_list_menu_item_layout:I = 0x7f04000d

.field public static final abc_list_menu_item_radio:I = 0x7f04000e

.field public static final abc_popup_menu_item_layout:I = 0x7f04000f

.field public static final abc_screen_content_include:I = 0x7f040010

.field public static final abc_screen_simple:I = 0x7f040011

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f040012

.field public static final abc_screen_toolbar:I = 0x7f040013

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f040014

.field public static final abc_search_view:I = 0x7f040015

.field public static final abc_simple_dropdown_hint:I = 0x7f040016

.field public static final account_details_with_avatar:I = 0x7f040017

.field public static final account_dialog:I = 0x7f040018

.field public static final account_info_banner:I = 0x7f040019

.field public static final account_item_view:I = 0x7f04001a

.field public static final account_spinner:I = 0x7f04001b

.field public static final account_switcher:I = 0x7f04001c

.field public static final accounts_activity:I = 0x7f04001d

.field public static final accounts_list_row:I = 0x7f04001e

.field public static final action_bar_search_back_button:I = 0x7f04001f

.field public static final action_card:I = 0x7f040020

.field public static final action_card_button_placeholder:I = 0x7f040021

.field public static final action_card_button_template:I = 0x7f040022

.field public static final action_card_item_template:I = 0x7f040023

.field public static final action_card_overflow_menu_button:I = 0x7f040024

.field public static final action_card_row_template:I = 0x7f040025

.field public static final add_account:I = 0x7f040026

.field public static final add_collaborator:I = 0x7f040027

.field public static final add_collaborator_list_item:I = 0x7f040028

.field public static final add_collaborator_sharing_option:I = 0x7f0400e7

.field public static final app_widget:I = 0x7f040029

.field public static final app_widget_broken:I = 0x7f04002a

.field public static final audio_player:I = 0x7f04002b

.field public static final avatars:I = 0x7f04002c

.field public static final central_slide_view:I = 0x7f04002d

.field public static final create_bar:I = 0x7f04002e

.field public static final create_document_host_activity:I = 0x7f04002f

.field public static final create_entry_dialog_list:I = 0x7f040030

.field public static final create_entry_dialog_row:I = 0x7f040031

.field public static final custom_notification:I = 0x7f040032

.field public static final default_account_info_banner:I = 0x7f040033

.field public static final detail_card_activity_bottom:I = 0x7f040034

.field public static final detail_card_activity_row_expand:I = 0x7f040035

.field public static final detail_card_activity_row_extra:I = 0x7f040036

.field public static final detail_card_activity_row_info:I = 0x7f040037

.field public static final detail_card_activity_row_target:I = 0x7f040038

.field public static final detail_card_activity_top:I = 0x7f040039

.field public static final detail_card_divider_row:I = 0x7f04003a

.field public static final detail_card_information:I = 0x7f04003b

.field public static final detail_card_pin:I = 0x7f04003c

.field public static final detail_card_preview:I = 0x7f04003d

.field public static final detail_card_share_link_status:I = 0x7f04003e

.field public static final detail_card_sharing:I = 0x7f04003f

.field public static final detail_card_sharing_header:I = 0x7f040040

.field public static final detail_card_sharing_row:I = 0x7f040041

.field public static final detail_drawer:I = 0x7f040042

.field public static final detail_fragment:I = 0x7f040043

.field public static final detail_fragment_pin_button:I = 0x7f040044

.field public static final detail_fragment_sharing_add_entry:I = 0x7f040045

.field public static final detail_fragment_sharing_entry:I = 0x7f040046

.field public static final detail_list_activity:I = 0x7f040047

.field public static final detail_list_header:I = 0x7f040048

.field public static final detail_list_title_bar:I = 0x7f040049

.field public static final detail_listview:I = 0x7f04004a

.field public static final dialog_password:I = 0x7f04004b

.field public static final dialog_print:I = 0x7f04004c

.field public static final doc_entry_folder_select_layout:I = 0x7f04004d

.field public static final doc_entry_group_title:I = 0x7f0400e5

.field public static final doc_entry_group_title_onecolumn:I = 0x7f04004e

.field public static final doc_entry_group_title_sticky:I = 0x7f0400e6

.field public static final doc_entry_group_title_sticky_onecolumn:I = 0x7f04004f

.field public static final doc_entry_group_title_sticky_twocolumn:I = 0x7f040050

.field public static final doc_entry_group_title_twocolumn:I = 0x7f040051

.field public static final doc_entry_row:I = 0x7f0400e4

.field public static final doc_entry_row_details_button:I = 0x7f040052

.field public static final doc_entry_row_onecolumn:I = 0x7f040053

.field public static final doc_entry_row_twocolumn:I = 0x7f040054

.field public static final doc_entry_select_layout:I = 0x7f040055

.field public static final doc_grid_empty_title:I = 0x7f040056

.field public static final doc_grid_entry_folder_select_layout:I = 0x7f040057

.field public static final doc_grid_entry_select_layout:I = 0x7f040058

.field public static final doc_grid_folder:I = 0x7f040059

.field public static final doc_grid_item:I = 0x7f04005a

.field public static final doc_grid_item_details_button:I = 0x7f04005b

.field public static final doc_grid_item_incoming_photos:I = 0x7f04005c

.field public static final doc_grid_item_sync_layout:I = 0x7f04005d

.field public static final doc_grid_ripple_overlay_layout:I = 0x7f04005e

.field public static final doc_grid_row:I = 0x7f04005f

.field public static final doc_grid_title:I = 0x7f0400e8

.field public static final doc_grid_title_sticky:I = 0x7f0400e9

.field public static final doc_list_container:I = 0x7f040060

.field public static final doc_list_empty_recent_view:I = 0x7f040061

.field public static final doc_list_empty_view:I = 0x7f040062

.field public static final doc_list_syncing:I = 0x7f040063

.field public static final doc_list_view:I = 0x7f040064

.field public static final doc_list_view_sync_more_spinner:I = 0x7f040065

.field public static final document_opener_activity_error:I = 0x7f040066

.field public static final drag_knob:I = 0x7f040067

.field public static final drive_search_bar:I = 0x7f040068

.field public static final drive_welcome_help_card:I = 0x7f040069

.field public static final drop_to_this_folder_action_bar_overlay:I = 0x7f04006a

.field public static final ds_actionbar_compat:I = 0x7f04006b

.field public static final ds_actionbar_title:I = 0x7f04006c

.field public static final ds_camera_controls:I = 0x7f04006d

.field public static final ds_camera_preview:I = 0x7f04006e

.field public static final ds_capture_activity:I = 0x7f04006f

.field public static final ds_editor_activity:I = 0x7f040070

.field public static final ds_editor_controls:I = 0x7f040071

.field public static final ds_editor_panel:I = 0x7f040072

.field public static final ds_rename_dialog:I = 0x7f040073

.field public static final ds_rename_dialog_buttons:I = 0x7f040074

.field public static final ds_rename_dialog_cancel_button:I = 0x7f040075

.field public static final ds_rename_dialog_ok_button:I = 0x7f040076

.field public static final edit_title_dialog_content:I = 0x7f040077

.field public static final file_icon:I = 0x7f040078

.field public static final file_picker:I = 0x7f040079

.field public static final file_viewer_image:I = 0x7f04007a

.field public static final file_viewer_pdf:I = 0x7f04007b

.field public static final file_viewer_text:I = 0x7f04007c

.field public static final film_strip:I = 0x7f04007d

.field public static final find_in_file:I = 0x7f04007e

.field public static final gif_preview_page:I = 0x7f04007f

.field public static final gview:I = 0x7f040080

.field public static final help_card_buttons:I = 0x7f040081

.field public static final help_card_common:I = 0x7f040082

.field public static final help_card_common_horizontal:I = 0x7f040083

.field public static final home_screen_activity:I = 0x7f040084

.field public static final home_screen_activity_list:I = 0x7f040085

.field public static final ic_sync_progress:I = 0x7f040086

.field public static final image_preview_page:I = 0x7f040087

.field public static final incoming_card:I = 0x7f040088

.field public static final incoming_view_tabs:I = 0x7f040089

.field public static final internal_release_dialog:I = 0x7f04008a

.field public static final linear_layout_list_view_test_fragment_activity:I = 0x7f04008b

.field public static final list_item_single_choice:I = 0x7f04008c

.field public static final list_view:I = 0x7f04008d

.field public static final loading_indicator:I = 0x7f04008e

.field public static final manage_accounts:I = 0x7f04008f

.field public static final mr_media_route_chooser_dialog:I = 0x7f040090

.field public static final mr_media_route_controller_dialog:I = 0x7f040091

.field public static final mr_media_route_list_item:I = 0x7f040092

.field public static final mtl_video_controller:I = 0x7f040093

.field public static final navigation_breadcrumb:I = 0x7f040094

.field public static final navigation_breadcrumb_item:I = 0x7f040095

.field public static final navigation_list_footerview:I = 0x7f040096

.field public static final navigation_list_footerview_drive:I = 0x7f040097

.field public static final navigation_list_footerview_editors:I = 0x7f040098

.field public static final navigation_list_item:I = 0x7f040099

.field public static final navigation_menu_item:I = 0x7f04009a

.field public static final navigation_sliding_panel:I = 0x7f04009b

.field public static final new_document_button:I = 0x7f04009c

.field public static final opener_option:I = 0x7f04009d

.field public static final operation_dialog:I = 0x7f04009e

.field public static final operation_dialog_buttons:I = 0x7f04009f

.field public static final operation_dialog_cancel_button:I = 0x7f0400a0

.field public static final operation_dialog_ok_button:I = 0x7f0400a1

.field public static final page_1:I = 0x7f0400a2

.field public static final page_1_images:I = 0x7f0400a3

.field public static final page_2:I = 0x7f0400a4

.field public static final page_2_images:I = 0x7f0400a5

.field public static final page_3:I = 0x7f0400a6

.field public static final page_3_images:I = 0x7f0400a7

.field public static final page_indicator:I = 0x7f0400a8

.field public static final photo_backup_suggest_help_card:I = 0x7f0400a9

.field public static final pick_entry_dialog_header:I = 0x7f0400aa

.field public static final preview_activity:I = 0x7f0400ab

.field public static final preview_offline_open_in_another_app:I = 0x7f0400ac

.field public static final preview_pager_fragment:I = 0x7f0400ad

.field public static final print_dialog:I = 0x7f0400ae

.field public static final progress_bar_dialog:I = 0x7f0400af

.field public static final progress_spinner_dialog:I = 0x7f0400b0

.field public static final punch_grid_view_slide_picker:I = 0x7f0400b1

.field public static final punch_list_view_slide_picker:I = 0x7f0400b2

.field public static final punch_presentation_content:I = 0x7f0400b3

.field public static final punch_slide_view:I = 0x7f0400b4

.field public static final punch_thumbnail_wrapper:I = 0x7f0400b5

.field public static final punch_web_view:I = 0x7f0400b6

.field public static final punch_web_view_fragment:I = 0x7f0400b7

.field public static final punch_web_view_presentation_mode:I = 0x7f0400b8

.field public static final punch_web_view_presentation_mode_tablet:I = 0x7f0400b9

.field public static final punch_web_view_tablet:I = 0x7f0400ba

.field public static final punch_web_view_whole:I = 0x7f0400bb

.field public static final punch_web_view_whole_presentation_mode:I = 0x7f0400bc

.field public static final replies_bubble:I = 0x7f0400bd

.field public static final scan_help_card:I = 0x7f0400be

.field public static final search:I = 0x7f0400bf

.field public static final search_suggestion_item:I = 0x7f0400c0

.field public static final selected_account:I = 0x7f0400c1

.field public static final selected_account_short:I = 0x7f0400c2

.field public static final selected_entry_card:I = 0x7f0400c3

.field public static final selection_drop_zone_overlay:I = 0x7f0400c4

.field public static final selection_floating_handle:I = 0x7f0400c5

.field public static final selection_floating_handle_pop_menu:I = 0x7f0400c6

.field public static final selection_floating_overlay:I = 0x7f0400c7

.field public static final selection_menu_icon_template:I = 0x7f0400c8

.field public static final selection_menu_item_template:I = 0x7f0400c9

.field public static final sharing_entry:I = 0x7f0400ca

.field public static final sharing_entry_group:I = 0x7f0400cb

.field public static final sharing_list:I = 0x7f0400cc

.field public static final sidebar_action_list_item:I = 0x7f0400cd

.field public static final sidebar_action_top_margin:I = 0x7f0400ce

.field public static final sign_in:I = 0x7f0400cf

.field public static final speaker_note_content:I = 0x7f0400d0

.field public static final speaker_note_presence:I = 0x7f0400d1

.field public static final splash:I = 0x7f0400d2

.field public static final support_simple_spinner_dropdown_item:I = 0x7f0400d3

.field public static final swipable_doc_list:I = 0x7f0400d4

.field public static final tablet_doclist_with_nav_drawer:I = 0x7f0400d5

.field public static final test_dummy:I = 0x7f0400d6

.field public static final test_fragment_activity:I = 0x7f0400d7

.field public static final thumbnail_view:I = 0x7f0400d8

.field public static final undo_banner:I = 0x7f0400d9

.field public static final upload_shared_item_activity:I = 0x7f0400da

.field public static final upload_shared_item_header:I = 0x7f0400db

.field public static final video_controller:I = 0x7f0400dc

.field public static final video_player:I = 0x7f0400dd

.field public static final warm_welcome_activity:I = 0x7f0400de

.field public static final warm_welcome_fake_page:I = 0x7f0400df

.field public static final warm_welcome_fragment:I = 0x7f0400e0

.field public static final warm_welcome_splash:I = 0x7f0400e1

.field public static final web_view_open:I = 0x7f0400e2

.field public static final welcome:I = 0x7f0400e3


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
