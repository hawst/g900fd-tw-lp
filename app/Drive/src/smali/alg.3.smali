.class public Lalg;
.super Ljava/lang/Object;
.source "ExecutorUtils.java"


# direct methods
.method public static a()LbsW;
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x1

    invoke-static {v0}, Lalg;->a(I)LbsW;

    move-result-object v0

    return-object v0
.end method

.method public static a(I)LbsW;
    .locals 2

    .prologue
    .line 42
    const-wide/32 v0, 0xea60

    invoke-static {p0, v0, v1}, Lalg;->a(IJ)LbsW;

    move-result-object v0

    return-object v0
.end method

.method public static a(IJ)LbsW;
    .locals 1

    .prologue
    .line 59
    const-string v0, "SafeThreadPool"

    invoke-static {p0, p1, p2, v0}, Lalg;->a(IJLjava/lang/String;)LbsW;

    move-result-object v0

    return-object v0
.end method

.method public static a(IJLjava/lang/String;)LbsW;
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 74
    new-instance v8, Lalh;

    invoke-direct {v8, p3}, Lalh;-><init>(Ljava/lang/String;)V

    .line 85
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    move v2, p0

    move v3, p0

    move-wide v4, p1

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    .line 88
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-lt v0, v2, :cond_0

    .line 89
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ThreadPoolExecutor;->allowCoreThreadTimeOut(Z)V

    .line 91
    :cond_0
    invoke-static {v1}, LbsY;->a(Ljava/util/concurrent/ExecutorService;)LbsW;

    move-result-object v0

    return-object v0
.end method

.method public static a(ILjava/lang/String;)LbsW;
    .locals 2

    .prologue
    .line 50
    const-wide/32 v0, 0xea60

    invoke-static {p0, v0, v1, p1}, Lalg;->a(IJLjava/lang/String;)LbsW;

    move-result-object v0

    return-object v0
.end method
