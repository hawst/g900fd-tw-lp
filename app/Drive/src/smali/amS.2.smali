.class public LamS;
.super Ljava/lang/Object;
.source "SyncUtilities.java"


# direct methods
.method public static a()V
    .locals 3

    .prologue
    .line 28
    invoke-static {}, LpD;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    new-instance v0, LQp;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " interrupted"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LQp;-><init>(Ljava/lang/String;)V

    throw v0

    .line 31
    :cond_0
    return-void
.end method

.method public static a(Landroid/widget/TextView;Landroid/widget/ProgressBar;LahR;Ljava/lang/String;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/16 v8, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 39
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 41
    sget-object v0, LamT;->a:[I

    invoke-virtual {p2}, LahR;->a()Lagg;

    move-result-object v3

    invoke-virtual {v3}, Lagg;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 65
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 39
    goto :goto_0

    .line 44
    :pswitch_0
    invoke-virtual {p1, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_1

    .line 47
    :pswitch_1
    invoke-virtual {p1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 49
    invoke-virtual {p2}, LahR;->a()J

    move-result-wide v4

    invoke-static {v10, v11, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 50
    invoke-virtual {p2}, LahR;->b()J

    move-result-wide v6

    .line 51
    cmp-long v0, v6, v10

    if-lez v0, :cond_1

    .line 52
    long-to-double v8, v4

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    mul-double/2addr v8, v10

    long-to-double v6, v6

    div-double v6, v8, v6

    double-to-int v0, v6

    .line 53
    invoke-virtual {p1, v2}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 54
    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 55
    new-array v0, v1, [Ljava/lang/Object;

    .line 56
    invoke-static {v4, v5}, Lalr;->a(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p3, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 55
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    invoke-virtual {p0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 59
    :cond_1
    invoke-virtual {p1, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 60
    invoke-virtual {p0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 41
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
