.class final LbfB;
.super Ljava/util/AbstractSet;
.source "GenericData.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractSet",
        "<",
        "Ljava/util/Map$Entry",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lbfw;

.field final synthetic a:Lbfz;


# direct methods
.method constructor <init>(Lbfz;)V
    .locals 2

    .prologue
    .line 212
    iput-object p1, p0, LbfB;->a:Lbfz;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    .line 213
    new-instance v0, Lbft;

    iget-object v1, p1, Lbfz;->classInfo:Lbfq;

    invoke-virtual {v1}, Lbfq;->a()Z

    move-result v1

    invoke-direct {v0, p1, v1}, Lbft;-><init>(Ljava/lang/Object;Z)V

    invoke-virtual {v0}, Lbft;->a()Lbfw;

    move-result-object v0

    iput-object v0, p0, LbfB;->a:Lbfw;

    .line 214
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, LbfB;->a:Lbfz;

    iget-object v0, v0, Lbfz;->unknownFields:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 229
    iget-object v0, p0, LbfB;->a:Lbfw;

    invoke-virtual {v0}, Lbfw;->clear()V

    .line 230
    return-void
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 218
    new-instance v0, LbfA;

    iget-object v1, p0, LbfB;->a:Lbfz;

    iget-object v2, p0, LbfB;->a:Lbfw;

    invoke-direct {v0, v1, v2}, LbfA;-><init>(Lbfz;Lbfw;)V

    return-object v0
.end method

.method public size()I
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, LbfB;->a:Lbfz;

    iget-object v0, v0, Lbfz;->unknownFields:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iget-object v1, p0, LbfB;->a:Lbfw;

    invoke-virtual {v1}, Lbfw;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
