.class public final enum LZD;
.super Ljava/lang/Enum;
.source "PunchWebViewSupportLevel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LZD;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LZD;

.field private static final synthetic a:[LZD;

.field public static final enum b:LZD;

.field public static final enum c:LZD;

.field public static final enum d:LZD;


# instance fields
.field private final a:LYK;

.field private final a:Z

.field private final b:LYK;

.field private final b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 20
    new-instance v0, LZD;

    const-string v1, "NO_SVG"

    sget-object v3, LYK;->b:LYK;

    sget-object v5, LYK;->b:LYK;

    move v4, v2

    move v6, v2

    invoke-direct/range {v0 .. v6}, LZD;-><init>(Ljava/lang/String;ILYK;ZLYK;Z)V

    sput-object v0, LZD;->a:LZD;

    .line 23
    new-instance v3, LZD;

    const-string v4, "HARDWARE_UNUSABLE"

    sget-object v6, LYK;->a:LYK;

    sget-object v8, LYK;->b:LYK;

    move v5, v10

    move v7, v2

    move v9, v2

    invoke-direct/range {v3 .. v9}, LZD;-><init>(Ljava/lang/String;ILYK;ZLYK;Z)V

    sput-object v3, LZD;->b:LZD;

    .line 27
    new-instance v3, LZD;

    const-string v4, "BUGGY_SUPPORT"

    sget-object v6, LYK;->a:LYK;

    sget-object v8, LYK;->b:LYK;

    move v5, v11

    move v7, v2

    move v9, v10

    invoke-direct/range {v3 .. v9}, LZD;-><init>(Ljava/lang/String;ILYK;ZLYK;Z)V

    sput-object v3, LZD;->c:LZD;

    .line 30
    new-instance v3, LZD;

    const-string v4, "FULL_SUPPORT"

    sget-object v6, LYK;->a:LYK;

    sget-object v8, LYK;->b:LYK;

    move v5, v12

    move v7, v10

    move v9, v10

    invoke-direct/range {v3 .. v9}, LZD;-><init>(Ljava/lang/String;ILYK;ZLYK;Z)V

    sput-object v3, LZD;->d:LZD;

    .line 15
    const/4 v0, 0x4

    new-array v0, v0, [LZD;

    sget-object v1, LZD;->a:LZD;

    aput-object v1, v0, v2

    sget-object v1, LZD;->b:LZD;

    aput-object v1, v0, v10

    sget-object v1, LZD;->c:LZD;

    aput-object v1, v0, v11

    sget-object v1, LZD;->d:LZD;

    aput-object v1, v0, v12

    sput-object v0, LZD;->a:[LZD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILYK;ZLYK;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LYK;",
            "Z",
            "LYK;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 48
    iput-object p3, p0, LZD;->a:LYK;

    .line 49
    iput-boolean p4, p0, LZD;->a:Z

    .line 50
    iput-object p5, p0, LZD;->b:LYK;

    .line 51
    iput-boolean p6, p0, LZD;->b:Z

    .line 52
    return-void
.end method

.method public static a(LQr;)LZD;
    .locals 3

    .prologue
    .line 58
    const-string v0, "punchWebViewBuggySupportMinVersion"

    const/16 v1, 0xb

    invoke-interface {p0, v0, v1}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    .line 61
    const-string v1, "punchWebViewFullSupportMinVersion"

    const/16 v2, 0x10

    invoke-interface {p0, v1, v2}, LQr;->a(Ljava/lang/String;I)I

    move-result v1

    .line 65
    invoke-static {}, LakQ;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 66
    sget-object v0, LZD;->a:LZD;

    .line 72
    :goto_0
    return-object v0

    .line 67
    :cond_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v2, v0, :cond_1

    .line 68
    sget-object v0, LZD;->b:LZD;

    goto :goto_0

    .line 69
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v1, :cond_2

    .line 70
    sget-object v0, LZD;->c:LZD;

    goto :goto_0

    .line 72
    :cond_2
    sget-object v0, LZD;->d:LZD;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LZD;
    .locals 1

    .prologue
    .line 15
    const-class v0, LZD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LZD;

    return-object v0
.end method

.method public static values()[LZD;
    .locals 1

    .prologue
    .line 15
    sget-object v0, LZD;->a:[LZD;

    invoke-virtual {v0}, [LZD;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LZD;

    return-object v0
.end method


# virtual methods
.method public a()LYK;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, LZD;->a:LYK;

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, LZD;->a:Z

    return v0
.end method

.method public b()LYK;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, LZD;->b:LYK;

    return-object v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, LZD;->b:Z

    return v0
.end method
