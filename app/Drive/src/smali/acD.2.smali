.class public LacD;
.super Ljava/lang/Object;
.source "SharingListElement.java"

# interfaces
.implements LacK;


# instance fields
.field private final a:LabD;

.field private a:Laci;


# direct methods
.method public constructor <init>(LabD;Laci;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, LacD;->a:LabD;

    .line 26
    iput-object p2, p0, LacD;->a:Laci;

    .line 27
    return-void
.end method


# virtual methods
.method public a()LabD;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, LacD;->a:LabD;

    return-object v0
.end method

.method public a()Laci;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, LacD;->a:Laci;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, LacD;->a:LabD;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LacD;->a:LabD;

    invoke-interface {v0}, LabD;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a()Lqo;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, LacD;->a:Laci;

    invoke-virtual {v0}, Laci;->a()Lqo;

    move-result-object v0

    return-object v0
.end method

.method public a(Laci;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, LacD;->a:Laci;

    .line 39
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 52
    instance-of v1, p1, LacD;

    if-nez v1, :cond_1

    .line 57
    :cond_0
    :goto_0
    return v0

    .line 55
    :cond_1
    check-cast p1, LacD;

    .line 56
    iget-object v1, p0, LacD;->a:LabD;

    iget-object v2, p1, LacD;->a:LabD;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LacD;->a:Laci;

    iget-object v2, p1, LacD;->a:Laci;

    .line 57
    invoke-virtual {v1, v2}, Laci;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 61
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LacD;->a:LabD;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LacD;->a:Laci;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
