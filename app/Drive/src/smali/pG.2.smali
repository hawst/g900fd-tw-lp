.class public final LpG;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LpC;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lpy;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LpB;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lrm;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LZS;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LZP;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LUI;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lrx;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaFO;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LsF;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;>;"
        }
    .end annotation
.end field

.field public m:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LtK;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Ltt;",
            ">;"
        }
    .end annotation
.end field

.field public o:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lamx;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LqA;",
            ">;"
        }
    .end annotation
.end field

.field public q:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LqW;",
            ">;"
        }
    .end annotation
.end field

.field public r:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Luc;",
            ">;"
        }
    .end annotation
.end field

.field public s:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;>;"
        }
    .end annotation
.end field

.field public t:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LFf;",
            ">;"
        }
    .end annotation
.end field

.field public u:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LWL;",
            ">;"
        }
    .end annotation
.end field

.field public v:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LtB;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 58
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 59
    iput-object p1, p0, LpG;->a:LbrA;

    .line 60
    const-class v0, LpC;

    invoke-static {v0, v3}, LpG;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LpG;->a:Lbsk;

    .line 63
    const-class v0, Lpy;

    invoke-static {v0, v3}, LpG;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LpG;->b:Lbsk;

    .line 66
    const-class v0, LpB;

    invoke-static {v0, v3}, LpG;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LpG;->c:Lbsk;

    .line 69
    const-class v0, Lrm;

    invoke-static {v0, v3}, LpG;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LpG;->d:Lbsk;

    .line 72
    const-class v0, LZS;

    new-instance v1, Lbwl;

    const-string v2, "wapiFeedProcessor"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    .line 73
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 72
    invoke-static {v0, v3}, LpG;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LpG;->e:Lbsk;

    .line 75
    const-class v0, Ljava/lang/Integer;

    sget-object v1, LQH;->g:Ljava/lang/Class;

    .line 76
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 75
    invoke-static {v0, v3}, LpG;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LpG;->f:Lbsk;

    .line 78
    const-class v0, LZP;

    new-instance v1, Lbwl;

    const-string v2, "wapiFeedProcessor"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    .line 79
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 78
    invoke-static {v0, v3}, LpG;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LpG;->g:Lbsk;

    .line 81
    const-class v0, LUI;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LpG;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LpG;->h:Lbsk;

    .line 84
    const-class v0, Lrx;

    invoke-static {v0, v3}, LpG;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LpG;->i:Lbsk;

    .line 87
    const-class v0, LaFO;

    invoke-static {v0, v3}, LpG;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LpG;->j:Lbsk;

    .line 90
    const-class v0, LsF;

    invoke-static {v0, v3}, LpG;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LpG;->k:Lbsk;

    .line 93
    const-class v0, Ljava/lang/Class;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, Landroid/app/Activity;

    .line 94
    invoke-static {v2}, LbwL;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/WildcardType;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "DocListActivity"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    const-class v1, Lbxz;

    .line 93
    invoke-static {v0, v1}, LpG;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LpG;->l:Lbsk;

    .line 96
    const-class v0, LtK;

    invoke-static {v0, v3}, LpG;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LpG;->m:Lbsk;

    .line 99
    const-class v0, Ltt;

    invoke-static {v0, v3}, LpG;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LpG;->n:Lbsk;

    .line 102
    const-class v0, Lamx;

    invoke-static {v0, v3}, LpG;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LpG;->o:Lbsk;

    .line 105
    const-class v0, LqA;

    invoke-static {v0, v3}, LpG;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LpG;->p:Lbsk;

    .line 108
    const-class v0, LqW;

    invoke-static {v0, v3}, LpG;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LpG;->q:Lbsk;

    .line 111
    const-class v0, Luc;

    invoke-static {v0, v3}, LpG;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LpG;->r:Lbsk;

    .line 114
    const-class v0, Ljava/lang/Class;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, Landroid/app/Activity;

    .line 115
    invoke-static {v2}, LbwL;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/WildcardType;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "StartingActivityOnLaunch"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    const-class v1, Lbxz;

    .line 114
    invoke-static {v0, v1}, LpG;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LpG;->s:Lbsk;

    .line 117
    const-class v0, LFf;

    invoke-static {v0, v3}, LpG;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LpG;->t:Lbsk;

    .line 120
    const-class v0, LWL;

    invoke-static {v0, v3}, LpG;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LpG;->u:Lbsk;

    .line 123
    const-class v0, LtB;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LpG;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LpG;->v:Lbsk;

    .line 126
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 340
    packed-switch p1, :pswitch_data_0

    .line 374
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 342
    :pswitch_1
    new-instance v1, LpC;

    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 345
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LpG;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->a:Lbsk;

    .line 343
    invoke-static {v0, v2}, LpG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LpC;-><init>(Landroid/content/Context;)V

    .line 350
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    .line 351
    invoke-virtual {v0, v1}, LpG;->a(LpC;)V

    move-object v0, v1

    .line 372
    :goto_0
    return-object v0

    .line 354
    :pswitch_2
    new-instance v1, Lpy;

    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 357
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LpG;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 355
    invoke-static {v0, v2}, LpG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    invoke-direct {v1, v0}, Lpy;-><init>(LQr;)V

    move-object v0, v1

    .line 362
    goto :goto_0

    .line 364
    :pswitch_3
    new-instance v1, LpB;

    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 367
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LpG;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->a:Lbsk;

    .line 365
    invoke-static {v0, v2}, LpG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LpB;-><init>(Landroid/content/Context;)V

    move-object v0, v1

    .line 372
    goto :goto_0

    .line 340
    :pswitch_data_0
    .packed-switch 0x279
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 407
    sparse-switch p2, :sswitch_data_0

    .line 560
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 409
    :sswitch_0
    check-cast p1, Lpz;

    .line 411
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 414
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 411
    invoke-virtual {p1, v0}, Lpz;->provideBaseActivity(Landroid/content/Context;)Lrm;

    move-result-object v0

    .line 553
    :goto_0
    return-object v0

    .line 418
    :sswitch_1
    check-cast p1, Lpz;

    .line 420
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->g:Lbsk;

    .line 423
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZP;

    .line 420
    invoke-virtual {p1, v0}, Lpz;->provideWaitingRateLimiter(LZP;)LZS;

    move-result-object v0

    goto :goto_0

    .line 427
    :sswitch_2
    check-cast p1, Lpz;

    .line 429
    invoke-virtual {p1}, Lpz;->provideAppVersionCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 432
    :sswitch_3
    check-cast p1, Lpz;

    .line 434
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LZR;

    iget-object v0, v0, LZR;->a:Lbsk;

    .line 437
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZP;

    .line 434
    invoke-virtual {p1, v0}, Lpz;->provideDelayWaitingRateLimiter(LZP;)LZP;

    move-result-object v0

    goto :goto_0

    .line 441
    :sswitch_4
    check-cast p1, Lpx;

    .line 443
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->b:Lbsk;

    .line 446
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpy;

    .line 443
    invoke-virtual {p1, v0}, Lpx;->provideOpenUrlAliasConfiguration(Lpy;)LUI;

    move-result-object v0

    goto :goto_0

    .line 450
    :sswitch_5
    check-cast p1, Lpz;

    .line 452
    invoke-virtual {p1}, Lpz;->provideClientMode()Lrx;

    move-result-object v0

    goto :goto_0

    .line 455
    :sswitch_6
    check-cast p1, Lpz;

    .line 457
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    iget-object v0, v0, LtQ;->l:Lbsk;

    .line 460
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqY;

    .line 457
    invoke-virtual {p1, v0}, Lpz;->provideAccountId(LqY;)LaFO;

    move-result-object v0

    goto :goto_0

    .line 464
    :sswitch_7
    check-cast p1, Lpz;

    .line 466
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    iget-object v0, v0, LtQ;->q:Lbsk;

    .line 469
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LsG;

    .line 466
    invoke-virtual {p1, v0}, Lpz;->provideDocsCentricTaskCompat(LsG;)LsF;

    move-result-object v0

    goto :goto_0

    .line 473
    :sswitch_8
    check-cast p1, Lpz;

    .line 475
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->c:Lbsk;

    .line 478
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LpB;

    .line 475
    invoke-virtual {p1, v0}, Lpz;->provideDefaultHomeActivity(LpB;)Ljava/lang/Class;

    move-result-object v0

    goto/16 :goto_0

    .line 482
    :sswitch_9
    check-cast p1, Lpz;

    .line 484
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    iget-object v0, v0, LtQ;->z:Lbsk;

    .line 487
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtM;

    .line 484
    invoke-virtual {p1, v0}, Lpz;->provideFeatureChecker(LtM;)LtK;

    move-result-object v0

    goto/16 :goto_0

    .line 491
    :sswitch_a
    check-cast p1, Lpz;

    .line 493
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    iget-object v0, v0, LtQ;->e:Lbsk;

    .line 496
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lts;

    .line 493
    invoke-virtual {p1, v0}, Lpz;->provideDriveEnabledChecker(Lts;)Ltt;

    move-result-object v0

    goto/16 :goto_0

    .line 500
    :sswitch_b
    check-cast p1, Lpz;

    .line 502
    invoke-virtual {p1}, Lpz;->provideRateLimitedExecutorFactory()Lamx;

    move-result-object v0

    goto/16 :goto_0

    .line 505
    :sswitch_c
    check-cast p1, Lpz;

    .line 507
    invoke-virtual {p1}, Lpz;->provideApplicationStartupTracker()LqA;

    move-result-object v0

    goto/16 :goto_0

    .line 510
    :sswitch_d
    check-cast p1, Lpz;

    .line 512
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    iget-object v0, v0, LtQ;->v:Lbsk;

    .line 515
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqX;

    .line 512
    invoke-virtual {p1, v0}, Lpz;->provideAccountDeletionChecker(LqX;)LqW;

    move-result-object v0

    goto/16 :goto_0

    .line 519
    :sswitch_e
    check-cast p1, Lpz;

    .line 521
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    iget-object v0, v0, LtQ;->p:Lbsk;

    .line 524
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lud;

    .line 521
    invoke-virtual {p1, v0}, Lpz;->provideIntentStarter(Lud;)Luc;

    move-result-object v0

    goto/16 :goto_0

    .line 528
    :sswitch_f
    check-cast p1, Lpz;

    .line 530
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->a:Lbsk;

    .line 533
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LpC;

    .line 530
    invoke-virtual {p1, v0}, Lpz;->provideStartingActivity(LpC;)Ljava/lang/Class;

    move-result-object v0

    goto/16 :goto_0

    .line 537
    :sswitch_10
    check-cast p1, Lpz;

    .line 539
    invoke-virtual {p1}, Lpz;->provideInternalReleaseDisplayer()LFf;

    move-result-object v0

    goto/16 :goto_0

    .line 542
    :sswitch_11
    check-cast p1, Lpz;

    .line 544
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LWA;

    iget-object v0, v0, LWA;->a:Lbsk;

    .line 547
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LWM;

    .line 544
    invoke-virtual {p1, v0}, Lpz;->providePrinter(LWM;)LWL;

    move-result-object v0

    goto/16 :goto_0

    .line 551
    :sswitch_12
    check-cast p1, Lpz;

    .line 553
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    iget-object v0, v0, LtQ;->t:Lbsk;

    .line 556
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtC;

    .line 553
    invoke-virtual {p1, v0}, Lpz;->provideEditorsOfflineSwitch(LtC;)LtB;

    move-result-object v0

    goto/16 :goto_0

    .line 407
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_9
        0x21 -> :sswitch_6
        0x49 -> :sswitch_5
        0x71 -> :sswitch_4
        0x7d -> :sswitch_2
        0xb8 -> :sswitch_0
        0xd0 -> :sswitch_8
        0x103 -> :sswitch_12
        0x105 -> :sswitch_11
        0x12c -> :sswitch_b
        0x24e -> :sswitch_1
        0x262 -> :sswitch_3
        0x27b -> :sswitch_7
        0x280 -> :sswitch_a
        0x282 -> :sswitch_c
        0x283 -> :sswitch_d
        0x285 -> :sswitch_e
        0x287 -> :sswitch_f
        0x288 -> :sswitch_10
    .end sparse-switch
.end method

.method public a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 257
    const-class v0, Lcom/google/android/apps/docs/DocsApplication;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x59

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LpG;->a(LbuP;LbuB;)V

    .line 260
    const-class v0, LpC;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x5a

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LpG;->a(LbuP;LbuB;)V

    .line 263
    const-class v0, LpH;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x46

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LpG;->a(LbuP;LbuB;)V

    .line 266
    const-class v0, LpC;

    iget-object v1, p0, LpG;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LpG;->a(Ljava/lang/Class;Lbsk;)V

    .line 267
    const-class v0, Lpy;

    iget-object v1, p0, LpG;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LpG;->a(Ljava/lang/Class;Lbsk;)V

    .line 268
    const-class v0, LpB;

    iget-object v1, p0, LpG;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LpG;->a(Ljava/lang/Class;Lbsk;)V

    .line 269
    const-class v0, Lrm;

    iget-object v1, p0, LpG;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LpG;->a(Ljava/lang/Class;Lbsk;)V

    .line 270
    const-class v0, LZS;

    new-instance v1, Lbwl;

    const-string v2, "wapiFeedProcessor"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LpG;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LpG;->a(Lbuv;Lbsk;)V

    .line 271
    const-class v0, Ljava/lang/Integer;

    sget-object v1, LQH;->g:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LpG;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, LpG;->a(Lbuv;Lbsk;)V

    .line 272
    const-class v0, LZP;

    new-instance v1, Lbwl;

    const-string v2, "wapiFeedProcessor"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LpG;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, LpG;->a(Lbuv;Lbsk;)V

    .line 273
    const-class v0, LUI;

    iget-object v1, p0, LpG;->h:Lbsk;

    invoke-virtual {p0, v0, v1}, LpG;->a(Ljava/lang/Class;Lbsk;)V

    .line 274
    const-class v0, Lrx;

    iget-object v1, p0, LpG;->i:Lbsk;

    invoke-virtual {p0, v0, v1}, LpG;->a(Ljava/lang/Class;Lbsk;)V

    .line 275
    const-class v0, LaFO;

    iget-object v1, p0, LpG;->j:Lbsk;

    invoke-virtual {p0, v0, v1}, LpG;->a(Ljava/lang/Class;Lbsk;)V

    .line 276
    const-class v0, LsF;

    iget-object v1, p0, LpG;->k:Lbsk;

    invoke-virtual {p0, v0, v1}, LpG;->a(Ljava/lang/Class;Lbsk;)V

    .line 277
    const-class v0, Ljava/lang/Class;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Landroid/app/Activity;

    invoke-static {v2}, LbwL;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/WildcardType;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "DocListActivity"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LpG;->l:Lbsk;

    invoke-virtual {p0, v0, v1}, LpG;->a(Lbuv;Lbsk;)V

    .line 278
    const-class v0, LtK;

    iget-object v1, p0, LpG;->m:Lbsk;

    invoke-virtual {p0, v0, v1}, LpG;->a(Ljava/lang/Class;Lbsk;)V

    .line 279
    const-class v0, Ltt;

    iget-object v1, p0, LpG;->n:Lbsk;

    invoke-virtual {p0, v0, v1}, LpG;->a(Ljava/lang/Class;Lbsk;)V

    .line 280
    const-class v0, Lamx;

    iget-object v1, p0, LpG;->o:Lbsk;

    invoke-virtual {p0, v0, v1}, LpG;->a(Ljava/lang/Class;Lbsk;)V

    .line 281
    const-class v0, LqA;

    iget-object v1, p0, LpG;->p:Lbsk;

    invoke-virtual {p0, v0, v1}, LpG;->a(Ljava/lang/Class;Lbsk;)V

    .line 282
    const-class v0, LqW;

    iget-object v1, p0, LpG;->q:Lbsk;

    invoke-virtual {p0, v0, v1}, LpG;->a(Ljava/lang/Class;Lbsk;)V

    .line 283
    const-class v0, Luc;

    iget-object v1, p0, LpG;->r:Lbsk;

    invoke-virtual {p0, v0, v1}, LpG;->a(Ljava/lang/Class;Lbsk;)V

    .line 284
    const-class v0, Ljava/lang/Class;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Landroid/app/Activity;

    invoke-static {v2}, LbwL;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/WildcardType;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "StartingActivityOnLaunch"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LpG;->s:Lbsk;

    invoke-virtual {p0, v0, v1}, LpG;->a(Lbuv;Lbsk;)V

    .line 285
    const-class v0, LFf;

    iget-object v1, p0, LpG;->t:Lbsk;

    invoke-virtual {p0, v0, v1}, LpG;->a(Ljava/lang/Class;Lbsk;)V

    .line 286
    const-class v0, LWL;

    iget-object v1, p0, LpG;->u:Lbsk;

    invoke-virtual {p0, v0, v1}, LpG;->a(Ljava/lang/Class;Lbsk;)V

    .line 287
    const-class v0, LtB;

    iget-object v1, p0, LpG;->v:Lbsk;

    invoke-virtual {p0, v0, v1}, LpG;->a(Ljava/lang/Class;Lbsk;)V

    .line 288
    iget-object v0, p0, LpG;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x27d

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 290
    iget-object v0, p0, LpG;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x279

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 292
    iget-object v0, p0, LpG;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x27e

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 294
    iget-object v0, p0, LpG;->d:Lbsk;

    const-class v1, Lpz;

    const/16 v2, 0xb8

    invoke-virtual {p0, v1, v2}, LpG;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 296
    iget-object v0, p0, LpG;->e:Lbsk;

    const-class v1, Lpz;

    const/16 v2, 0x24e

    invoke-virtual {p0, v1, v2}, LpG;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 298
    iget-object v0, p0, LpG;->f:Lbsk;

    const-class v1, Lpz;

    const/16 v2, 0x7d

    invoke-virtual {p0, v1, v2}, LpG;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 300
    iget-object v0, p0, LpG;->g:Lbsk;

    const-class v1, Lpz;

    const/16 v2, 0x262

    invoke-virtual {p0, v1, v2}, LpG;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 302
    iget-object v0, p0, LpG;->h:Lbsk;

    const-class v1, Lpx;

    const/16 v2, 0x71

    invoke-virtual {p0, v1, v2}, LpG;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 304
    iget-object v0, p0, LpG;->i:Lbsk;

    const-class v1, Lpz;

    const/16 v2, 0x49

    invoke-virtual {p0, v1, v2}, LpG;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 306
    iget-object v0, p0, LpG;->j:Lbsk;

    const-class v1, Lpz;

    const/16 v2, 0x21

    invoke-virtual {p0, v1, v2}, LpG;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 308
    iget-object v0, p0, LpG;->k:Lbsk;

    const-class v1, Lpz;

    const/16 v2, 0x27b

    invoke-virtual {p0, v1, v2}, LpG;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 310
    iget-object v0, p0, LpG;->l:Lbsk;

    const-class v1, Lpz;

    const/16 v2, 0xd0

    invoke-virtual {p0, v1, v2}, LpG;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 312
    iget-object v0, p0, LpG;->m:Lbsk;

    const-class v1, Lpz;

    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, LpG;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 314
    iget-object v0, p0, LpG;->n:Lbsk;

    const-class v1, Lpz;

    const/16 v2, 0x280

    invoke-virtual {p0, v1, v2}, LpG;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 316
    iget-object v0, p0, LpG;->o:Lbsk;

    const-class v1, Lpz;

    const/16 v2, 0x12c

    invoke-virtual {p0, v1, v2}, LpG;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 318
    iget-object v0, p0, LpG;->p:Lbsk;

    const-class v1, Lpz;

    const/16 v2, 0x282

    invoke-virtual {p0, v1, v2}, LpG;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 320
    iget-object v0, p0, LpG;->q:Lbsk;

    const-class v1, Lpz;

    const/16 v2, 0x283

    invoke-virtual {p0, v1, v2}, LpG;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 322
    iget-object v0, p0, LpG;->r:Lbsk;

    const-class v1, Lpz;

    const/16 v2, 0x285

    invoke-virtual {p0, v1, v2}, LpG;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 324
    iget-object v0, p0, LpG;->s:Lbsk;

    const-class v1, Lpz;

    const/16 v2, 0x287

    invoke-virtual {p0, v1, v2}, LpG;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 326
    iget-object v0, p0, LpG;->t:Lbsk;

    const-class v1, Lpz;

    const/16 v2, 0x288

    invoke-virtual {p0, v1, v2}, LpG;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 328
    iget-object v0, p0, LpG;->u:Lbsk;

    const-class v1, Lpz;

    const/16 v2, 0x105

    invoke-virtual {p0, v1, v2}, LpG;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 330
    iget-object v0, p0, LpG;->v:Lbsk;

    const-class v1, Lpz;

    const/16 v2, 0x103

    invoke-virtual {p0, v1, v2}, LpG;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 332
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 381
    sparse-switch p1, :sswitch_data_0

    .line 401
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 383
    :sswitch_0
    check-cast p2, Lcom/google/android/apps/docs/DocsApplication;

    .line 385
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    .line 386
    invoke-virtual {v0, p2}, LpG;->a(Lcom/google/android/apps/docs/DocsApplication;)V

    .line 403
    :goto_0
    return-void

    .line 389
    :sswitch_1
    check-cast p2, LpC;

    .line 391
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    .line 392
    invoke-virtual {v0, p2}, LpG;->a(LpC;)V

    goto :goto_0

    .line 395
    :sswitch_2
    check-cast p2, LpH;

    .line 397
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    .line 398
    invoke-virtual {v0, p2}, LpG;->a(LpH;)V

    goto :goto_0

    .line 381
    :sswitch_data_0
    .sparse-switch
        0x46 -> :sswitch_2
        0x59 -> :sswitch_0
        0x5a -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Lcom/google/android/apps/docs/DocsApplication;)V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LahE;

    iget-object v0, v0, LahE;->g:Lbsk;

    .line 135
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LpG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LahE;

    iget-object v1, v1, LahE;->g:Lbsk;

    .line 133
    invoke-static {v0, v1}, LpG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahw;

    iput-object v0, p1, Lcom/google/android/apps/docs/DocsApplication;->a:Lahw;

    .line 139
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 142
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LpG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 140
    invoke-static {v0, v1}, LpG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p1, Lcom/google/android/apps/docs/DocsApplication;->a:LQr;

    .line 146
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LagN;

    iget-object v0, v0, LagN;->L:Lbsk;

    .line 149
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LpG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LagN;

    iget-object v1, v1, LagN;->L:Lbsk;

    .line 147
    invoke-static {v0, v1}, LpG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lagd;

    iput-object v0, p1, Lcom/google/android/apps/docs/DocsApplication;->a:Lagd;

    .line 153
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LSK;

    iget-object v0, v0, LSK;->b:Lbsk;

    .line 156
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LpG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LSK;

    iget-object v1, v1, LSK;->b:Lbsk;

    .line 154
    invoke-static {v0, v1}, LpG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSF;

    iput-object v0, p1, Lcom/google/android/apps/docs/DocsApplication;->a:LSF;

    .line 160
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->f:Lbsk;

    .line 163
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LpG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->f:Lbsk;

    .line 161
    invoke-static {v0, v1}, LpG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQy;

    iput-object v0, p1, Lcom/google/android/apps/docs/DocsApplication;->a:LQy;

    .line 167
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LagN;

    iget-object v0, v0, LagN;->c:Lbsk;

    .line 170
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LpG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LagN;

    iget-object v1, v1, LagN;->c:Lbsk;

    .line 168
    invoke-static {v0, v1}, LpG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LafX;

    iput-object v0, p1, Lcom/google/android/apps/docs/DocsApplication;->a:LafX;

    .line 174
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaHj;

    iget-object v0, v0, LaHj;->d:Lbsk;

    .line 177
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LpG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaHj;

    iget-object v1, v1, LaHj;->d:Lbsk;

    .line 175
    invoke-static {v0, v1}, LpG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaHv;

    iput-object v0, p1, Lcom/google/android/apps/docs/DocsApplication;->a:LaHv;

    .line 181
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laae;

    iget-object v0, v0, Laae;->c:Lbsk;

    .line 184
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LpG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Laae;

    iget-object v1, v1, Laae;->c:Lbsk;

    .line 182
    invoke-static {v0, v1}, LpG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laac;

    iput-object v0, p1, Lcom/google/android/apps/docs/DocsApplication;->a:Laac;

    .line 188
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->h:Lbsk;

    .line 191
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LpG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->h:Lbsk;

    .line 189
    invoke-static {v0, v1}, LpG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGU;

    iput-object v0, p1, Lcom/google/android/apps/docs/DocsApplication;->a:LaGU;

    .line 195
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LadM;

    iget-object v0, v0, LadM;->g:Lbsk;

    .line 198
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LpG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LadM;

    iget-object v1, v1, LadM;->g:Lbsk;

    .line 196
    invoke-static {v0, v1}, LpG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacV;

    iput-object v0, p1, Lcom/google/android/apps/docs/DocsApplication;->a:LacV;

    .line 202
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->e:Lbsk;

    .line 205
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LpG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->e:Lbsk;

    .line 203
    invoke-static {v0, v1}, LpG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQt;

    iput-object v0, p1, Lcom/google/android/apps/docs/DocsApplication;->a:LQt;

    .line 209
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->B:Lbsk;

    .line 212
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LpG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->B:Lbsk;

    .line 210
    invoke-static {v0, v1}, LpG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamn;

    iput-object v0, p1, Lcom/google/android/apps/docs/DocsApplication;->a:Lamn;

    .line 216
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LahE;

    iget-object v0, v0, LahE;->j:Lbsk;

    .line 219
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LpG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LahE;

    iget-object v1, v1, LahE;->j:Lbsk;

    .line 217
    invoke-static {v0, v1}, LpG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahy;

    iput-object v0, p1, Lcom/google/android/apps/docs/DocsApplication;->a:Lahy;

    .line 223
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LUB;

    iget-object v0, v0, LUB;->c:Lbsk;

    .line 226
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LpG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LUB;

    iget-object v1, v1, LUB;->c:Lbsk;

    .line 224
    invoke-static {v0, v1}, LpG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUJ;

    iput-object v0, p1, Lcom/google/android/apps/docs/DocsApplication;->a:LUJ;

    .line 230
    return-void
.end method

.method public a(LpC;)V
    .locals 2

    .prologue
    .line 234
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LbiH;

    iget-object v0, v0, LbiH;->S:Lbsk;

    .line 237
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LpG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LbiH;

    iget-object v1, v1, LbiH;->S:Lbsk;

    .line 235
    invoke-static {v0, v1}, LpG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbiP;

    iput-object v0, p1, LpC;->a:LbiP;

    .line 241
    return-void
.end method

.method public a(LpH;)V
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, LpG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LqD;

    iget-object v0, v0, LqD;->c:Lbsk;

    .line 248
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LpG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LqD;

    iget-object v1, v1, LqD;->c:Lbsk;

    .line 246
    invoke-static {v0, v1}, LpG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqK;

    iput-object v0, p1, LpH;->a:LqK;

    .line 252
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 336
    return-void
.end method
