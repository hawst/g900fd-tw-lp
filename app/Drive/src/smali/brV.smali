.class abstract enum LbrV;
.super Ljava/lang/Enum;
.source "GellyInjectorBuilderBase.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LbrV;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LbrV;

.field private static final synthetic a:[LbrV;

.field public static final enum b:LbrV;

.field public static final enum c:LbrV;

.field public static final enum d:LbrV;

.field public static final enum e:LbrV;

.field public static final enum f:LbrV;

.field public static final enum g:LbrV;


# instance fields
.field private final a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "LbrV;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 61
    new-instance v0, LbrW;

    const-string v1, "LOAD_USER_ELEMENTS"

    new-array v2, v5, [LbrV;

    invoke-direct {v0, v1, v5, v2}, LbrW;-><init>(Ljava/lang/String;I[LbrV;)V

    sput-object v0, LbrV;->a:LbrV;

    .line 74
    new-instance v0, LbrX;

    const-string v1, "INITIALIZE_INJECTOR_STORE"

    new-array v2, v5, [LbrV;

    invoke-direct {v0, v1, v6, v2}, LbrX;-><init>(Ljava/lang/String;I[LbrV;)V

    sput-object v0, LbrV;->b:LbrV;

    .line 89
    new-instance v0, LbrY;

    const-string v1, "SET_INSTANCE_BINDING_INSTANCES"

    new-array v2, v7, [LbrV;

    sget-object v3, LbrV;->b:LbrV;

    aput-object v3, v2, v5

    sget-object v3, LbrV;->a:LbrV;

    aput-object v3, v2, v6

    invoke-direct {v0, v1, v7, v2}, LbrY;-><init>(Ljava/lang/String;I[LbrV;)V

    sput-object v0, LbrV;->c:LbrV;

    .line 121
    new-instance v0, LbrZ;

    const-string v1, "APPLY_SCOPE"

    new-array v2, v7, [LbrV;

    sget-object v3, LbrV;->a:LbrV;

    aput-object v3, v2, v5

    sget-object v3, LbrV;->b:LbrV;

    aput-object v3, v2, v6

    invoke-direct {v0, v1, v8, v2}, LbrZ;-><init>(Ljava/lang/String;I[LbrV;)V

    sput-object v0, LbrV;->d:LbrV;

    .line 132
    new-instance v0, Lbsa;

    const-string v1, "CREATE_INJECTOR"

    new-array v2, v8, [LbrV;

    sget-object v3, LbrV;->a:LbrV;

    aput-object v3, v2, v5

    sget-object v3, LbrV;->b:LbrV;

    aput-object v3, v2, v6

    sget-object v3, LbrV;->d:LbrV;

    aput-object v3, v2, v7

    invoke-direct {v0, v1, v9, v2}, Lbsa;-><init>(Ljava/lang/String;I[LbrV;)V

    sput-object v0, LbrV;->e:LbrV;

    .line 144
    new-instance v0, Lbsb;

    const-string v1, "PROCESS_LOOKUP_REQUESTS"

    const/4 v2, 0x5

    new-array v3, v6, [LbrV;

    sget-object v4, LbrV;->e:LbrV;

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lbsb;-><init>(Ljava/lang/String;I[LbrV;)V

    sput-object v0, LbrV;->f:LbrV;

    .line 154
    new-instance v0, Lbsc;

    const-string v1, "POST_INITIALIZATION_INJECTIONS"

    const/4 v2, 0x6

    new-array v3, v7, [LbrV;

    sget-object v4, LbrV;->e:LbrV;

    aput-object v4, v3, v5

    sget-object v4, LbrV;->f:LbrV;

    aput-object v4, v3, v6

    invoke-direct {v0, v1, v2, v3}, Lbsc;-><init>(Ljava/lang/String;I[LbrV;)V

    sput-object v0, LbrV;->g:LbrV;

    .line 57
    const/4 v0, 0x7

    new-array v0, v0, [LbrV;

    sget-object v1, LbrV;->a:LbrV;

    aput-object v1, v0, v5

    sget-object v1, LbrV;->b:LbrV;

    aput-object v1, v0, v6

    sget-object v1, LbrV;->c:LbrV;

    aput-object v1, v0, v7

    sget-object v1, LbrV;->d:LbrV;

    aput-object v1, v0, v8

    sget-object v1, LbrV;->e:LbrV;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LbrV;->f:LbrV;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LbrV;->g:LbrV;

    aput-object v2, v0, v1

    sput-object v0, LbrV;->a:[LbrV;

    return-void
.end method

.method private varargs constructor <init>(Ljava/lang/String;I[LbrV;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "LbrV;",
            ")V"
        }
    .end annotation

    .prologue
    .line 167
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 168
    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LbrV;->a:Ljava/util/Collection;

    .line 169
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;I[LbrV;LbrT;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3}, LbrV;-><init>(Ljava/lang/String;I[LbrV;)V

    return-void
.end method

.method private final a(Ljava/util/Collection;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LbrV;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 178
    iget-object v0, p0, LbrV;->a:Ljava/util/Collection;

    invoke-interface {p1, v0}, Ljava/util/Collection;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    const-string v1, "Required: %s Completed: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LbrV;->a:Ljava/util/Collection;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lbso;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 180
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LbrV;
    .locals 1

    .prologue
    .line 57
    const-class v0, LbrV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LbrV;

    return-object v0
.end method

.method public static values()[LbrV;
    .locals 1

    .prologue
    .line 57
    sget-object v0, LbrV;->a:[LbrV;

    invoke-virtual {v0}, [LbrV;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LbrV;

    return-object v0
.end method


# virtual methods
.method final a(LbrS;)V
    .locals 1

    .prologue
    .line 172
    invoke-static {p1}, LbrS;->a(LbrS;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-direct {p0, v0}, LbrV;->a(Ljava/util/Collection;)V

    .line 173
    invoke-virtual {p0, p1}, LbrV;->b(LbrS;)V

    .line 174
    invoke-static {p1}, LbrS;->a(LbrS;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 175
    return-void
.end method

.method abstract b(LbrS;)V
.end method
