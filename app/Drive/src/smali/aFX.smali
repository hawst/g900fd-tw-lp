.class public LaFX;
.super Ljava/lang/Object;
.source "CursorsContainer.java"


# instance fields
.field private final a:LbmL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmL",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "LaFW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LbmL;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmL",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "LaFW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbmL;

    iput-object v0, p0, LaFX;->a:LbmL;

    .line 27
    return-void
.end method

.method public static a(Ljava/lang/Class;LaFW;)LaFX;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LaFW;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)",
            "LaFX;"
        }
    .end annotation

    .prologue
    .line 22
    new-instance v0, LaFX;

    invoke-static {p0, p1}, LbmL;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmL;

    move-result-object v1

    invoke-direct {v0, v1}, LaFX;-><init>(LbmL;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Class;)LaFW;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LaFW;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, LaFX;->a:LbmL;

    invoke-virtual {v0, p1}, LbmL;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 34
    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFW;

    return-object v0
.end method

.method public a(LaFX;)LaFX;
    .locals 2

    .prologue
    .line 63
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    invoke-static {}, LbmL;->a()LbmM;

    move-result-object v0

    iget-object v1, p0, LaFX;->a:LbmL;

    .line 67
    invoke-virtual {v0, v1}, LbmM;->a(Ljava/util/Map;)LbmM;

    move-result-object v0

    iget-object v1, p1, LaFX;->a:LbmL;

    .line 68
    invoke-virtual {v0, v1}, LbmM;->a(Ljava/util/Map;)LbmM;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, LbmM;->a()LbmL;

    move-result-object v0

    .line 70
    new-instance v1, LaFX;

    invoke-direct {v1, v0}, LaFX;-><init>(LbmL;)V

    return-object v1
.end method

.method public a()V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, LaFX;->a:LbmL;

    invoke-virtual {v0}, LbmL;->a()Lbmv;

    move-result-object v0

    invoke-virtual {v0}, Lbmv;->a()Lbqv;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFW;

    .line 42
    invoke-interface {v0}, LaFW;->a()V

    goto :goto_0

    .line 44
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 74
    const-class v0, LaGA;

    invoke-virtual {p0, v0}, LaFX;->a(Ljava/lang/Class;)LaFW;

    move-result-object v0

    check-cast v0, LaGA;

    .line 75
    invoke-interface {v0}, LaGA;->m()Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/Class;LaFW;)LaFX;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LaFW;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)",
            "LaFX;"
        }
    .end annotation

    .prologue
    .line 51
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, LaFX;->a:LbmL;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 52
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    new-instance v1, LaFX;

    invoke-static {v0}, LbmL;->a(Ljava/util/Map;)LbmL;

    move-result-object v0

    invoke-direct {v1, v0}, LaFX;-><init>(LbmL;)V

    return-object v1
.end method
