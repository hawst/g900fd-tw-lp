.class Laor;
.super Ljava/lang/Object;
.source "FetchingModule.java"

# interfaces
.implements Lbxw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbxw",
        "<",
        "LakR;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LQr;

.field private final a:LakX;


# direct methods
.method public constructor <init>(LakX;LQr;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Laor;->a:LakX;

    .line 39
    iput-object p2, p0, Laor;->a:LQr;

    .line 40
    return-void
.end method


# virtual methods
.method public a()LakR;
    .locals 4

    .prologue
    .line 44
    iget-object v0, p0, Laor;->a:LQr;

    const-string v1, "fetchingDiskCacheMaxSizeRatio"

    const-wide v2, 0x3fc99999a0000000L    # 0.20000000298023224

    invoke-interface {v0, v1, v2, v3}, LQr;->a(Ljava/lang/String;D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 48
    iget-object v1, p0, Laor;->a:LQr;

    const-string v2, "fetchingMaxNumberOfCachedFiles"

    const/16 v3, 0x7d0

    invoke-interface {v1, v2, v3}, LQr;->a(Ljava/lang/String;I)I

    move-result v1

    .line 51
    iget-object v2, p0, Laor;->a:LakX;

    sget-object v3, LakY;->a:LakY;

    invoke-virtual {v2, v3}, LakX;->a(LakY;)LakU;

    move-result-object v2

    .line 52
    invoke-static {v2, v1, v0}, LakR;->a(LakU;IF)LakR;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Laor;->a()LakR;

    move-result-object v0

    return-object v0
.end method
