.class final Lafv;
.super Ljava/lang/Object;
.source "SyncMoreAlgorithm.java"

# interfaces
.implements Laiq;


# instance fields
.field private final a:I

.field private final a:LaFM;

.field private final a:LaGF;

.field private final a:LaGW;

.field private a:LagV;

.field private final a:Lagy;

.field private a:Lahp;


# direct methods
.method public constructor <init>(LaFM;Lagy;LaGW;ILaGF;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object v0, p0, Lafv;->a:LagV;

    .line 39
    iput-object v0, p0, Lafv;->a:Lahp;

    .line 53
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    if-ltz p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 56
    iput-object p1, p0, Lafv;->a:LaFM;

    .line 57
    iput-object p2, p0, Lafv;->a:Lagy;

    .line 58
    iput p4, p0, Lafv;->a:I

    .line 59
    iput-object p3, p0, Lafv;->a:LaGW;

    .line 60
    iput-object p5, p0, Lafv;->a:LaGF;

    .line 61
    return-void

    .line 55
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lafv;->a:LagV;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lafv;->a:LagV;

    invoke-virtual {v0}, LagV;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lagx;Landroid/content/SyncResult;)V
    .locals 6

    .prologue
    .line 65
    iget-object v0, p0, Lafv;->a:LaGW;

    invoke-virtual {v0}, LaGW;->b()Ljava/lang/String;

    move-result-object v0

    .line 66
    if-eqz v0, :cond_0

    .line 68
    iget-object v1, p0, Lafv;->a:LaGW;

    invoke-virtual {v1}, LaGW;->a()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 69
    new-instance v1, Lahq;

    iget-object v4, p0, Lafv;->a:Lagy;

    iget-object v5, p0, Lafv;->a:LaGF;

    invoke-direct {v1, v4, v2, v3, v5}, Lahq;-><init>(Lagy;JLaGF;)V

    iput-object v1, p0, Lafv;->a:Lahp;

    .line 71
    new-instance v1, LagV;

    iget-object v2, p0, Lafv;->a:Lahp;

    invoke-direct {v1, v2}, LagV;-><init>(Lagy;)V

    iput-object v1, p0, Lafv;->a:LagV;

    .line 72
    iget-object v1, p0, Lafv;->a:LaFM;

    invoke-virtual {v1}, LaFM;->a()LaFO;

    move-result-object v1

    .line 73
    iget-object v2, p0, Lafv;->a:LagV;

    iget v3, p0, Lafv;->a:I

    invoke-interface {p1, v0, v1, v2, v3}, Lagx;->a(Ljava/lang/String;LaFO;Lagy;I)V

    .line 75
    :cond_0
    return-void
.end method

.method public a(Landroid/content/SyncResult;Z)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 79
    if-eqz p2, :cond_0

    .line 80
    iget-object v1, p0, Lafv;->a:LagV;

    if-eqz v1, :cond_2

    .line 81
    invoke-virtual {p0}, Lafv;->a()Ljava/lang/String;

    move-result-object v1

    .line 82
    iget-object v2, p0, Lafv;->a:Lahp;

    invoke-interface {v2}, Lahp;->a()Ljava/util/Date;

    move-result-object v2

    .line 83
    if-nez v2, :cond_1

    .line 84
    :goto_0
    iget-object v2, p0, Lafv;->a:LaGW;

    invoke-virtual {v2, v1, v0}, LaGW;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 85
    iget-object v0, p0, Lafv;->a:LaGW;

    invoke-virtual {v0}, LaGW;->e()V

    .line 93
    :cond_0
    :goto_1
    return-void

    .line 83
    :cond_1
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 89
    :cond_2
    iget-object v1, p0, Lafv;->a:Lagy;

    invoke-interface {v1}, Lagy;->a()V

    .line 90
    iget-object v1, p0, Lafv;->a:Lagy;

    invoke-interface {v1, v0}, Lagy;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 104
    const-string v0, "SyncMoreAlgorithm[delegate=%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lafv;->a:Lagy;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
