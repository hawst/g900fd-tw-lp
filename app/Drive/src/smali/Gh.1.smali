.class LGh;
.super Ljava/lang/Object;
.source "DriveDocumentOpenerProvider.java"

# interfaces
.implements LFX;


# instance fields
.field a:LFL;

.field a:LFR;
    .annotation runtime Lbxv;
        a = "DefaultRemote"
    .end annotation
.end field

.field a:LGA;

.field a:LGE;

.field a:LGO;

.field a:LGi;

.field a:LaKR;

.field a:Ladb;

.field a:Lald;

.field a:Landroid/content/Context;
    .annotation runtime Lajg;
    .end annotation
.end field

.field a:Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;

.field a:LtK;

.field b:LFR;
    .annotation runtime Lbxv;
        a = "DefaultLocal"
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LaGo;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Z)LFR;
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 81
    const-string v0, "audio/mpeg"

    .line 83
    invoke-interface {p1}, LaGo;->a()LaGv;

    move-result-object v0

    .line 87
    iget-object v1, p0, LGh;->a:LtK;

    sget-object v4, Lry;->av:Lry;

    invoke-interface {v1, v4}, LtK;->a(LtJ;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 88
    invoke-interface {p1}, LaGo;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {p1}, LUN;->a(LaGo;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 89
    sget-object v0, LaGv;->i:LaGv;

    .line 93
    :cond_0
    sget-object v1, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    if-ne p2, v1, :cond_c

    .line 94
    iget-object v1, p0, LGh;->a:LtK;

    sget-object v4, Lry;->aB:Lry;

    .line 95
    invoke-interface {v1, v4}, LtK;->a(LtJ;)Z

    move-result v6

    .line 96
    iget-object v1, p0, LGh;->a:LtK;

    sget-object v4, Lry;->az:Lry;

    .line 97
    invoke-interface {v1, v4}, LtK;->a(LtJ;)Z

    move-result v7

    .line 103
    invoke-interface {p1}, LaGo;->f()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    move v1, v2

    .line 104
    :goto_0
    if-eqz v1, :cond_3

    iget-object v1, p0, LGh;->a:Ladb;

    sget-object v4, LacY;->a:LacY;

    .line 105
    invoke-interface {v1, p1, v4}, Ladb;->a(LaGo;LacY;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    .line 107
    :goto_1
    invoke-interface {p1}, LaGo;->i()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-interface {p1}, LaGo;->a()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    move v4, v2

    .line 109
    :goto_2
    invoke-interface {p1}, LaGo;->a()LaGn;

    move-result-object v8

    .line 110
    invoke-interface {p1}, LaGo;->f()Ljava/lang/String;

    move-result-object v9

    .line 112
    if-nez v1, :cond_5

    if-nez v4, :cond_5

    .line 113
    const-string v0, "DriveDocumentOpenerProvider"

    const-string v1, "Cannot open %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v5

    :cond_1
    :goto_3
    move-object v5, v0

    .line 157
    :goto_4
    return-object v5

    :cond_2
    move v1, v3

    .line 103
    goto :goto_0

    :cond_3
    move v1, v3

    .line 105
    goto :goto_1

    :cond_4
    move v4, v3

    .line 107
    goto :goto_2

    .line 114
    :cond_5
    const-string v2, "audio/mpeg"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    if-eqz v7, :cond_6

    .line 117
    iget-object v0, p0, LGh;->a:LFL;

    goto :goto_3

    .line 118
    :cond_6
    sget-object v2, LaGn;->c:LaGn;

    invoke-virtual {v2, v8}, LaGn;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    if-eqz v6, :cond_7

    if-nez v1, :cond_7

    .line 121
    iget-object v0, p0, LGh;->a:LGO;

    goto :goto_3

    .line 122
    :cond_7
    if-eqz v4, :cond_a

    if-eqz v1, :cond_8

    if-nez p3, :cond_a

    .line 123
    :cond_8
    iget-object v1, p0, LGh;->a:LGE;

    invoke-virtual {v1, v0}, LGE;->a(Ljava/lang/Object;)LFR;

    move-result-object v0

    .line 124
    if-nez v0, :cond_9

    iget-object v1, p0, LGh;->a:Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;

    invoke-interface {v1, p1, p2}, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;->a(LaGo;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 125
    iget-object v0, p0, LGh;->a:Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;

    .line 127
    :cond_9
    if-nez v0, :cond_1

    .line 128
    const-string v0, "DriveDocumentOpenerProvider"

    const-string v1, "Using defaultRemoteDocumentOpener"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    iget-object v0, p0, LGh;->a:Landroid/content/Context;

    sget v1, Lxi;->open_document_error_item_cannot_be_exported:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 130
    iget-object v1, p0, LGh;->a:Lald;

    invoke-interface {v1, v0, v5}, Lald;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 131
    iget-object v0, p0, LGh;->a:LFR;

    goto :goto_3

    .line 133
    :cond_a
    if-eqz v1, :cond_b

    .line 134
    iget-object v1, p0, LGh;->a:LGA;

    invoke-virtual {v1, v0}, LGA;->a(Ljava/lang/Object;)LFR;

    move-result-object v0

    .line 135
    if-nez v0, :cond_1

    .line 136
    const-string v0, "DriveDocumentOpenerProvider"

    const-string v1, "Using defaultLocalDocumentOpener"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    iget-object v0, p0, LGh;->b:LFR;

    goto :goto_3

    .line 139
    :cond_b
    iget-object v0, p0, LGh;->a:LtK;

    sget-object v1, Lry;->ab:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 140
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Cannot happen"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 143
    :cond_c
    invoke-interface {p1}, LaGo;->a()LaGv;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a(LaGv;)LacY;

    move-result-object v1

    .line 144
    iget-object v4, p0, LGh;->a:Ladb;

    .line 145
    invoke-interface {v4, p1, v1}, Ladb;->a(LaGo;LacY;)Z

    move-result v1

    .line 146
    invoke-interface {p1}, LaGo;->i()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_e

    iget-object v4, p0, LGh;->a:LaKR;

    .line 147
    invoke-interface {v4}, LaKR;->a()Z

    move-result v4

    if-nez v4, :cond_d

    if-nez v1, :cond_e

    .line 148
    :cond_d
    iget-object v1, p0, LGh;->a:LGi;

    invoke-virtual {v1, v0}, LGi;->a(Ljava/lang/Object;)LFR;

    move-result-object v0

    goto/16 :goto_3

    .line 149
    :cond_e
    if-eqz v1, :cond_f

    .line 150
    const-string v0, "DriveDocumentOpenerProvider"

    const-string v1, "Using defaultLocalDocumentOpener"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    iget-object v0, p0, LGh;->b:LFR;

    goto/16 :goto_3

    .line 153
    :cond_f
    const-string v0, "DriveDocumentOpenerProvider"

    const-string v1, "Cannot open %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_4

    :cond_10
    move-object v0, v5

    goto/16 :goto_3
.end method
