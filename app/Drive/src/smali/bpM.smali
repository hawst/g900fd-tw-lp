.class final LbpM;
.super Lbng;
.source "RegularImmutableSortedMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lbng",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final transient a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final transient a:LbpQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbpQ",
            "<TK;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LbpQ;LbmF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbpQ",
            "<TK;>;",
            "LbmF",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Lbng;-><init>()V

    .line 36
    iput-object p1, p0, LbpM;->a:LbpQ;

    .line 37
    iput-object p2, p0, LbpM;->a:LbmF;

    .line 38
    return-void
.end method

.method static synthetic a(LbpM;)LbmF;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, LbpM;->a:LbmF;

    return-object v0
.end method

.method private a(II)Lbng;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lbng",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 99
    if-nez p1, :cond_0

    invoke-virtual {p0}, LbpM;->size()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 104
    :goto_0
    return-object p0

    .line 101
    :cond_0
    if-ne p1, p2, :cond_1

    .line 102
    invoke-virtual {p0}, LbpM;->comparator()Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v0}, LbpM;->a(Ljava/util/Comparator;)Lbng;

    move-result-object p0

    goto :goto_0

    .line 104
    :cond_1
    iget-object v0, p0, LbpM;->a:LbpQ;

    invoke-virtual {v0, p1, p2}, LbpQ;->a(II)Lbnk;

    move-result-object v0

    iget-object v1, p0, LbpM;->a:LbmF;

    invoke-virtual {v1, p1, p2}, LbmF;->a(II)LbmF;

    move-result-object v1

    invoke-static {v0, v1}, LbpM;->a(Lbnk;LbmF;)Lbng;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public a()Lbmv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbmv",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, LbpM;->a:LbmF;

    return-object v0
.end method

.method public a(Ljava/lang/Object;Z)Lbng;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;Z)",
            "Lbng",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 110
    const/4 v0, 0x0

    iget-object v1, p0, LbpM;->a:LbpQ;

    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, LbpQ;->a(Ljava/lang/Object;Z)I

    move-result v1

    invoke-direct {p0, v0, v1}, LbpM;->a(II)Lbng;

    move-result-object v0

    return-object v0
.end method

.method public a()Lbnk;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbnk",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, LbpM;->a:LbpQ;

    return-object v0
.end method

.method b()LbmY;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmY",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 49
    new-instance v0, LbpO;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LbpO;-><init>(LbpM;LbpN;)V

    return-object v0
.end method

.method public b(Ljava/lang/Object;Z)Lbng;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;Z)",
            "Lbng",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, LbpM;->a:LbpQ;

    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, LbpQ;->b(Ljava/lang/Object;Z)I

    move-result v0

    invoke-virtual {p0}, LbpM;->size()I

    move-result v1

    invoke-direct {p0, v0, v1}, LbpM;->a(II)Lbng;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c()LbmY;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, LbpM;->a()Lbnk;

    move-result-object v0

    return-object v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, LbpM;->a:LbpQ;

    invoke-virtual {v0, p1}, LbpQ;->a(Ljava/lang/Object;)I

    move-result v0

    .line 95
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LbpM;->a:LbmF;

    invoke-virtual {v1, v0}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, LbpM;->a()Lbnk;

    move-result-object v0

    return-object v0
.end method

.method public synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, LbpM;->a()Lbmv;

    move-result-object v0

    return-object v0
.end method
