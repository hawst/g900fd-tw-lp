.class public abstract LbdO;
.super LbdI;
.source "AbstractGoogleJsonClient.java"


# direct methods
.method public constructor <init>(Lbeq;LbeO;Ljava/lang/String;Ljava/lang/String;Lbek;Z)V
    .locals 6

    .prologue
    .line 74
    new-instance v1, LbeR;

    invoke-direct {v1, p2}, LbeR;-><init>(LbeO;)V

    if-eqz p6, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "data"

    aput-object v3, v0, v2

    const/4 v2, 0x1

    const-string v3, "error"

    aput-object v3, v0, v2

    .line 76
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 75
    :goto_0
    invoke-virtual {v1, v0}, LbeR;->a(Ljava/util/Collection;)LbeR;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, LbeR;->a()LbeQ;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v5, p5

    .line 74
    invoke-direct/range {v0 .. v5}, LbdI;-><init>(Lbeq;Ljava/lang/String;Ljava/lang/String;LbfL;Lbek;)V

    .line 78
    return-void

    .line 76
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic build()LbdH;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, LbdO;->build()LbdN;

    move-result-object v0

    return-object v0
.end method

.method public abstract build()LbdN;
.end method

.method public final getJsonFactory()LbeO;
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0}, LbdO;->getObjectParser()LbeQ;

    move-result-object v0

    invoke-virtual {v0}, LbeQ;->a()LbeO;

    move-result-object v0

    return-object v0
.end method

.method public final getObjectParser()LbeQ;
    .locals 1

    .prologue
    .line 82
    invoke-super {p0}, LbdI;->getObjectParser()LbfL;

    move-result-object v0

    check-cast v0, LbeQ;

    return-object v0
.end method

.method public bridge synthetic getObjectParser()LbfL;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, LbdO;->getObjectParser()LbeQ;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic setApplicationName(Ljava/lang/String;)LbdI;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0, p1}, LbdO;->setApplicationName(Ljava/lang/String;)LbdO;

    move-result-object v0

    return-object v0
.end method

.method public setApplicationName(Ljava/lang/String;)LbdO;
    .locals 1

    .prologue
    .line 116
    invoke-super {p0, p1}, LbdI;->setApplicationName(Ljava/lang/String;)LbdI;

    move-result-object v0

    check-cast v0, LbdO;

    return-object v0
.end method

.method public bridge synthetic setGoogleClientRequestInitializer(LbdM;)LbdI;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0, p1}, LbdO;->setGoogleClientRequestInitializer(LbdM;)LbdO;

    move-result-object v0

    return-object v0
.end method

.method public setGoogleClientRequestInitializer(LbdM;)LbdO;
    .locals 1

    .prologue
    .line 106
    invoke-super {p0, p1}, LbdI;->setGoogleClientRequestInitializer(LbdM;)LbdI;

    move-result-object v0

    check-cast v0, LbdO;

    return-object v0
.end method

.method public bridge synthetic setHttpRequestInitializer(Lbek;)LbdI;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0, p1}, LbdO;->setHttpRequestInitializer(Lbek;)LbdO;

    move-result-object v0

    return-object v0
.end method

.method public setHttpRequestInitializer(Lbek;)LbdO;
    .locals 1

    .prologue
    .line 111
    invoke-super {p0, p1}, LbdI;->setHttpRequestInitializer(Lbek;)LbdI;

    move-result-object v0

    check-cast v0, LbdO;

    return-object v0
.end method

.method public bridge synthetic setRootUrl(Ljava/lang/String;)LbdI;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0, p1}, LbdO;->setRootUrl(Ljava/lang/String;)LbdO;

    move-result-object v0

    return-object v0
.end method

.method public setRootUrl(Ljava/lang/String;)LbdO;
    .locals 1

    .prologue
    .line 95
    invoke-super {p0, p1}, LbdI;->setRootUrl(Ljava/lang/String;)LbdI;

    move-result-object v0

    check-cast v0, LbdO;

    return-object v0
.end method

.method public bridge synthetic setServicePath(Ljava/lang/String;)LbdI;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0, p1}, LbdO;->setServicePath(Ljava/lang/String;)LbdO;

    move-result-object v0

    return-object v0
.end method

.method public setServicePath(Ljava/lang/String;)LbdO;
    .locals 1

    .prologue
    .line 100
    invoke-super {p0, p1}, LbdI;->setServicePath(Ljava/lang/String;)LbdI;

    move-result-object v0

    check-cast v0, LbdO;

    return-object v0
.end method

.method public bridge synthetic setSuppressAllChecks(Z)LbdI;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0, p1}, LbdO;->setSuppressAllChecks(Z)LbdO;

    move-result-object v0

    return-object v0
.end method

.method public setSuppressAllChecks(Z)LbdO;
    .locals 1

    .prologue
    .line 131
    invoke-super {p0, p1}, LbdI;->setSuppressAllChecks(Z)LbdI;

    move-result-object v0

    check-cast v0, LbdO;

    return-object v0
.end method

.method public bridge synthetic setSuppressPatternChecks(Z)LbdI;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0, p1}, LbdO;->setSuppressPatternChecks(Z)LbdO;

    move-result-object v0

    return-object v0
.end method

.method public setSuppressPatternChecks(Z)LbdO;
    .locals 1

    .prologue
    .line 121
    invoke-super {p0, p1}, LbdI;->setSuppressPatternChecks(Z)LbdI;

    move-result-object v0

    check-cast v0, LbdO;

    return-object v0
.end method

.method public bridge synthetic setSuppressRequiredParameterChecks(Z)LbdI;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0, p1}, LbdO;->setSuppressRequiredParameterChecks(Z)LbdO;

    move-result-object v0

    return-object v0
.end method

.method public setSuppressRequiredParameterChecks(Z)LbdO;
    .locals 1

    .prologue
    .line 126
    invoke-super {p0, p1}, LbdI;->setSuppressRequiredParameterChecks(Z)LbdI;

    move-result-object v0

    check-cast v0, LbdO;

    return-object v0
.end method
