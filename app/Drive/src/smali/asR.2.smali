.class public LasR;
.super Ljava/lang/Object;
.source "StoryShops.java"


# instance fields
.field private final a:LTl;

.field private final a:Laeb;

.field private final a:Lalo;

.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Laja;Lalo;LTl;Laeb;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Lalo;",
            "LTl;",
            "Laeb;",
            ")V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, LasR;->a:Lbxw;

    .line 44
    iput-object p2, p0, LasR;->a:Lalo;

    .line 45
    iput-object p3, p0, LasR;->a:LTl;

    .line 46
    iput-object p4, p0, LasR;->a:Laeb;

    .line 47
    return-void
.end method

.method static synthetic a(LasR;)LTl;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, LasR;->a:LTl;

    return-object v0
.end method

.method static synthetic a(LasR;)Laeb;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, LasR;->a:Laeb;

    return-object v0
.end method

.method static synthetic a(LasR;)Lalo;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, LasR;->a:Lalo;

    return-object v0
.end method

.method static synthetic a(LasR;)Lbxw;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, LasR;->a:Lbxw;

    return-object v0
.end method


# virtual methods
.method public a(Ladj;Ljava/lang/String;)LasP;
    .locals 1

    .prologue
    .line 66
    new-instance v0, LasT;

    invoke-direct {v0, p0, p1, p2}, LasT;-><init>(LasR;Ladj;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;)LasP;
    .locals 1

    .prologue
    .line 61
    new-instance v0, LasW;

    invoke-direct {v0, p0, p1}, LasW;-><init>(LasR;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)LasP;
    .locals 1

    .prologue
    .line 51
    new-instance v0, LasS;

    invoke-direct {v0, p0, p1, p2}, LasS;-><init>(LasR;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public a()LasV;
    .locals 1

    .prologue
    .line 71
    new-instance v0, LasV;

    invoke-direct {v0, p0}, LasV;-><init>(LasR;)V

    return-object v0
.end method

.method protected a(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 75
    if-nez p1, :cond_0

    .line 76
    const-string v0, ""

    .line 88
    :goto_0
    return-object v0

    .line 80
    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, LasR;->a:Lalo;

    invoke-interface {v1, p1}, Lalo;->a(Ljava/io/InputStream;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    :try_start_1
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 86
    :catch_0
    move-exception v1

    goto :goto_0

    .line 81
    :catch_1
    move-exception v0

    .line 82
    :try_start_2
    const-string v0, ""
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 85
    :try_start_3
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 86
    :catch_2
    move-exception v1

    goto :goto_0

    .line 84
    :catchall_0
    move-exception v0

    .line 85
    :try_start_4
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 88
    :goto_1
    throw v0

    .line 86
    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)LasP;
    .locals 1

    .prologue
    .line 56
    new-instance v0, LasU;

    invoke-direct {v0, p0, p1, p2}, LasU;-><init>(LasR;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
