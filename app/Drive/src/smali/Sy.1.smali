.class LSy;
.super Landroid/os/Handler;
.source "GestureDetector.java"


# instance fields
.field final synthetic a:LSx;


# direct methods
.method constructor <init>(LSx;)V
    .locals 0

    .prologue
    .line 256
    iput-object p1, p0, LSy;->a:LSx;

    .line 257
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 258
    return-void
.end method

.method constructor <init>(LSx;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 260
    iput-object p1, p0, LSy;->a:LSx;

    .line 261
    invoke-virtual {p2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 262
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 266
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 283
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 268
    :pswitch_0
    iget-object v0, p0, LSy;->a:LSx;

    invoke-static {v0}, LSx;->a(LSx;)LSA;

    move-result-object v0

    iget-object v1, p0, LSy;->a:LSx;

    invoke-static {v1}, LSx;->a(LSx;)Landroid/view/MotionEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LSA;->a(Landroid/view/MotionEvent;)V

    .line 285
    :cond_0
    :goto_0
    return-void

    .line 272
    :pswitch_1
    iget-object v0, p0, LSy;->a:LSx;

    invoke-static {v0}, LSx;->a(LSx;)V

    goto :goto_0

    .line 277
    :pswitch_2
    iget-object v0, p0, LSy;->a:LSx;

    invoke-static {v0}, LSx;->a(LSx;)LSz;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LSy;->a:LSx;

    invoke-static {v0}, LSx;->a(LSx;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 278
    iget-object v0, p0, LSy;->a:LSx;

    invoke-static {v0}, LSx;->a(LSx;)LSz;

    move-result-object v0

    iget-object v1, p0, LSy;->a:LSx;

    invoke-static {v1}, LSx;->a(LSx;)Landroid/view/MotionEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LSz;->a(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 266
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
