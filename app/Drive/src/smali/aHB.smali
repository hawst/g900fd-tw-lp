.class public LaHB;
.super LaHp;
.source "OrganizeOperation.java"


# instance fields
.field private final a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;LbmY;LbmY;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGg;",
            "Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;",
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;",
            ">;",
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 73
    const-string v1, "moveOperation"

    invoke-direct {p0, p1, p2, v1}, LaHp;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Ljava/lang/String;)V

    .line 74
    iget-object v1, p2, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;->a:LaFO;

    .line 75
    invoke-direct {p0, v1, p3}, LaHB;->a(LaFO;LbmY;)V

    .line 76
    invoke-direct {p0, v1, p4}, LaHB;->a(LaFO;LbmY;)V

    .line 77
    invoke-virtual {p3}, LbmY;->size()I

    move-result v1

    if-eq v1, v0, :cond_0

    invoke-virtual {p4}, LbmY;->size()I

    move-result v1

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 78
    invoke-static {p3, p4}, LbpU;->b(Ljava/util/Set;Ljava/util/Set;)Lbqb;

    move-result-object v0

    invoke-virtual {v0}, Lbqb;->isEmpty()Z

    move-result v0

    invoke-static {v0}, LbiT;->a(Z)V

    .line 79
    iput-object p4, p0, LaHB;->a:LbmY;

    .line 80
    iput-object p3, p0, LaHB;->b:LbmY;

    .line 81
    return-void

    .line 77
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Lorg/json/JSONObject;)LaHB;
    .locals 7

    .prologue
    .line 228
    iget-object v1, p1, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;->a:LaFO;

    .line 229
    const-string v3, "fromCollectionEntrySqlIds"

    const-string v4, "fromCollectionResourceId"

    const-string v5, "fromCollectionEntrySqlId"

    move-object v0, p0

    move-object v2, p2

    invoke-static/range {v0 .. v5}, LaHB;->a(LaGg;LaFO;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LbmY;

    move-result-object v6

    .line 236
    const-string v3, "toCollectionEntrySqlIds"

    const-string v4, "toCollectionResourceId"

    const-string v5, "toCollectionEntrySqlId"

    move-object v0, p0

    move-object v2, p2

    invoke-static/range {v0 .. v5}, LaHB;->a(LaGg;LaFO;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LbmY;

    move-result-object v0

    .line 243
    new-instance v1, LaHB;

    invoke-direct {v1, p0, p1, v6, v0}, LaHB;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;LbmY;LbmY;)V

    return-object v1
.end method

.method private static a(LaGg;LaFO;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LbmY;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGg;",
            "LaFO;",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;",
            ">;"
        }
    .end annotation

    .prologue
    .line 253
    invoke-static {}, LbmY;->a()Lbna;

    move-result-object v1

    .line 255
    invoke-virtual {p2, p3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    invoke-virtual {p2, p3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 257
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 258
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v4

    invoke-static {p1, v4, v5}, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;->a(LaFO;J)Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v3

    .line 259
    invoke-virtual {v1, v3}, Lbna;->a(Ljava/lang/Object;)Lbna;

    .line 257
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 263
    :cond_0
    invoke-static {p0, p1, p2, p4, p5}, LaHB;->a(LaGg;LaFO;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v0

    .line 264
    if-eqz v0, :cond_1

    .line 265
    invoke-virtual {v1, v0}, Lbna;->a(Ljava/lang/Object;)Lbna;

    .line 269
    :cond_1
    invoke-virtual {v1}, Lbna;->a()LbmY;

    move-result-object v0

    return-object v0
.end method

.method private static a(LaGg;LaFO;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;
    .locals 2

    .prologue
    .line 276
    invoke-static {p2, p3}, LaHB;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 277
    if-eqz v0, :cond_0

    .line 278
    invoke-static {p1, v0}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a(LaFO;Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    invoke-interface {p0, v0}, LaGg;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v0

    .line 282
    :goto_0
    return-object v0

    .line 279
    :cond_0
    invoke-virtual {p2, p4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 280
    invoke-virtual {p2, p4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;->a(LaFO;J)Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v0

    goto :goto_0

    .line 282
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 219
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 222
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LbmY;)Lorg/json/JSONArray;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;",
            ">;)",
            "Lorg/json/JSONArray;"
        }
    .end annotation

    .prologue
    .line 207
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 208
    invoke-virtual {p1}, LbmY;->a()Lbqv;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    .line 209
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;->a()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lorg/json/JSONArray;->put(J)Lorg/json/JSONArray;

    goto :goto_0

    .line 211
    :cond_0
    return-object v1
.end method

.method private a(LaFO;LbmY;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFO;",
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 85
    invoke-virtual {p2}, LbmY;->a()Lbqv;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 86
    iget-object v0, v0, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-virtual {p1, v0}, LaFO;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LbiT;->a(Z)V

    goto :goto_0

    .line 88
    :cond_0
    return-void
.end method

.method private a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/gms/drive/database/data/ResourceSpec;LaHD;)Z
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, LaHB;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/gms/drive/database/data/ResourceSpec;LaHD;Z)Z

    move-result v0

    return v0
.end method

.method private a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/gms/drive/database/data/ResourceSpec;LaHD;Z)Z
    .locals 3

    .prologue
    .line 183
    invoke-virtual {p3}, LaHD;->a()LaHE;

    move-result-object v0

    .line 185
    if-eqz p4, :cond_0

    .line 186
    sget-object v1, LUo;->a:LaHy;

    invoke-interface {v0, p1, p2, v1}, LaHE;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/gms/drive/database/data/ResourceSpec;LaHy;)Z

    move-result v0

    .line 192
    :goto_0
    return v0

    .line 189
    :cond_0
    const-string v1, "*"

    sget-object v2, LUo;->a:LaHy;

    invoke-interface {v0, p1, p2, v1, v2}, LaHE;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;LaHy;)Z

    move-result v0

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/gms/drive/database/data/ResourceSpec;LaHD;)Z
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LaHB;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/gms/drive/database/data/ResourceSpec;LaHD;Z)Z

    move-result v0

    return v0
.end method

.method private c()Ljava/lang/String;
    .locals 4

    .prologue
    .line 96
    const-string v0, "organize: remove from: %s, add to: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LaHB;->b:LbmY;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LaHB;->a:LbmY;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(LaGe;)LaHo;
    .locals 10

    .prologue
    .line 102
    iget-object v0, p0, LaHB;->a:LbmY;

    invoke-virtual {v0}, LbmY;->a()Lbqv;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 103
    iget-object v2, p0, LaHB;->a:LaGg;

    invoke-interface {v2, v0}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFZ;

    move-result-object v0

    .line 104
    if-eqz v0, :cond_0

    .line 105
    iget-object v2, p0, LaHB;->a:LaGg;

    .line 106
    invoke-virtual {p1}, LaGe;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v3

    .line 105
    invoke-interface {v2, v3, v0}, LaGg;->a(Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;LaFZ;)LaGB;

    move-result-object v0

    .line 107
    invoke-virtual {v0}, LaGB;->e()V

    goto :goto_0

    .line 110
    :cond_1
    iget-object v0, p0, LaHB;->b:LbmY;

    invoke-virtual {v0}, LbmY;->a()Lbqv;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 111
    iget-object v1, p0, LaHB;->a:LaGg;

    invoke-virtual {p0}, LaHB;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-interface {v1, v3}, LaGg;->a(LaFO;)LaFM;

    move-result-object v3

    .line 112
    iget-object v1, p0, LaHB;->a:LaGg;

    .line 113
    invoke-virtual {p1}, LaGe;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v4

    .line 112
    invoke-interface {v1, v4}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Ljava/util/Map;

    move-result-object v4

    .line 114
    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 115
    iget-object v6, p0, LaHB;->a:LaGg;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-interface {v6, v3, v8, v9}, LaGg;->a(LaFM;J)LaFV;

    move-result-object v6

    .line 116
    invoke-interface {v6}, LaFV;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 117
    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGB;

    invoke-virtual {v1}, LaGB;->f()V

    goto :goto_1

    .line 121
    :cond_4
    new-instance v0, LaHB;

    iget-object v1, p0, LaHB;->a:LaGg;

    .line 122
    invoke-virtual {p1}, LaGe;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v2

    iget-object v3, p0, LaHB;->a:LbmY;

    iget-object v4, p0, LaHB;->b:LbmY;

    invoke-direct {v0, v1, v2, v3, v4}, LaHB;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;LbmY;LbmY;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, LaHp;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, LaHB;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 197
    invoke-super {p0}, LaHp;->a()Lorg/json/JSONObject;

    move-result-object v0

    .line 198
    const-string v1, "operationName"

    const-string v2, "moveOperation"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 199
    iget-object v1, p0, LaHB;->b:LbmY;

    invoke-direct {p0, v1}, LaHB;->a(LbmY;)Lorg/json/JSONArray;

    move-result-object v1

    .line 200
    const-string v2, "fromCollectionEntrySqlIds"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 201
    iget-object v1, p0, LaHB;->a:LbmY;

    invoke-direct {p0, v1}, LaHB;->a(LbmY;)Lorg/json/JSONArray;

    move-result-object v1

    .line 202
    const-string v2, "toCollectionEntrySqlIds"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 203
    return-object v0
.end method

.method public a()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 127
    invoke-super {p0}, LaHp;->a()Z

    move-result v0

    .line 131
    iget-object v1, p0, LaHB;->b:LbmY;

    invoke-virtual {v1}, LbmY;->a()Lbqv;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 132
    iget-object v5, p0, LaHB;->a:LaGg;

    invoke-interface {v5, v0}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v2

    :goto_1
    and-int/2addr v0, v1

    move v1, v0

    .line 133
    goto :goto_0

    :cond_0
    move v0, v3

    .line 132
    goto :goto_1

    .line 134
    :cond_1
    iget-object v0, p0, LaHB;->a:LbmY;

    invoke-virtual {v0}, LbmY;->a()Lbqv;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 135
    iget-object v5, p0, LaHB;->a:LaGg;

    invoke-interface {v5, v0}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v2

    :goto_3
    and-int/2addr v1, v0

    .line 136
    goto :goto_2

    :cond_2
    move v0, v3

    .line 135
    goto :goto_3

    .line 137
    :cond_3
    return v1
.end method

.method public a(LaHy;LaHD;Lcom/google/android/gms/drive/database/data/ResourceSpec;)Z
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 149
    .line 152
    iget-object v0, p0, LaHB;->a:LbmY;

    invoke-virtual {v0}, LbmY;->a()Lbqv;

    move-result-object v3

    move v1, v2

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 153
    iget-object v1, p0, LaHB;->a:LaGg;

    invoke-interface {v1, v0}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v1

    .line 154
    invoke-direct {p0, p3, v1, p2}, LaHB;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/gms/drive/database/data/ResourceSpec;LaHD;)Z

    move-result v1

    .line 155
    if-nez v1, :cond_0

    .line 156
    const-string v1, "moveOperation"

    const-string v3, "Error adding entry %s to %s"

    new-array v5, v10, [Ljava/lang/Object;

    invoke-virtual {p0}, LaHB;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v6

    aput-object v6, v5, v4

    aput-object v0, v5, v2

    invoke-static {v1, v3, v5}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v4

    .line 168
    :cond_1
    :goto_0
    return v0

    .line 160
    :cond_2
    iget-object v0, p0, LaHB;->b:LbmY;

    invoke-virtual {v0}, LbmY;->a()Lbqv;

    move-result-object v5

    move v0, v2

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 161
    iget-object v3, p0, LaHB;->a:LaGg;

    invoke-interface {v3, v0}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v3

    .line 162
    invoke-direct {p0, p3, v3, p2}, LaHB;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/gms/drive/database/data/ResourceSpec;LaHD;)Z

    move-result v3

    .line 163
    if-nez v3, :cond_3

    .line 164
    const-string v6, "moveOperation"

    const-string v7, "Error removing entry %s from %s"

    new-array v8, v10, [Ljava/lang/Object;

    invoke-virtual {p0}, LaHB;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v9

    aput-object v9, v8, v4

    aput-object v0, v8, v2

    invoke-static {v6, v7, v8}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_3
    move v0, v3

    .line 166
    goto :goto_1

    .line 168
    :cond_4
    iget-object v2, p0, LaHB;->a:LbmY;

    invoke-virtual {v2}, LbmY;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 288
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LaHB;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, LaHB;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 289
    return-object v0
.end method
