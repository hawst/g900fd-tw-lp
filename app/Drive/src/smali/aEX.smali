.class public final enum LaEX;
.super Ljava/lang/Enum;
.source "JobsetTable.java"

# interfaces
.implements Lbjv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaEX;",
        ">;",
        "Lbjv",
        "<",
        "LaFr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaEX;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final synthetic a:[LaEX;

.field public static final enum b:LaEX;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum c:LaEX;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum d:LaEX;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field private final a:LaFr;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/16 v9, 0x3f

    const/16 v8, 0x36

    const/4 v7, 0x0

    .line 44
    new-instance v0, LaEX;

    const-string v1, "JOBSET_NAME"

    .line 45
    invoke-static {}, LaEW;->b()LaEW;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "jobsetName"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 46
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/String;

    invoke-virtual {v3, v4}, LaFG;->a([Ljava/lang/String;)LaFG;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    .line 47
    invoke-virtual {v2, v9}, LaFt;->b(I)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, LaEX;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEX;->a:LaEX;

    .line 52
    new-instance v0, LaEX;

    const-string v1, "APP_CACHE_ID"

    .line 53
    invoke-static {}, LaEW;->b()LaEW;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "appCacheId"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 55
    invoke-static {}, LaEj;->a()LaEj;

    move-result-object v4

    invoke-virtual {v3, v4}, LaFG;->a(LaFy;)LaFG;

    move-result-object v3

    .line 54
    invoke-virtual {v2, v8, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    .line 56
    invoke-virtual {v2, v9}, LaFt;->b(I)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, LaEX;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEX;->b:LaEX;

    .line 61
    new-instance v0, LaEX;

    const-string v1, "REFRESH_TIME"

    .line 62
    invoke-static {}, LaEW;->b()LaEW;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "refreshTime"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 63
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    .line 64
    invoke-virtual {v2, v9}, LaFt;->b(I)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, LaEX;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEX;->c:LaEX;

    .line 70
    new-instance v0, LaEX;

    const-string v1, "IS_CACHE_OBSOLETE"

    const/4 v2, 0x3

    .line 71
    invoke-static {}, LaEW;->b()LaEW;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "isCacheObsolete"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 72
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v4

    invoke-virtual {v4}, LaFG;->b()LaFG;

    move-result-object v4

    invoke-virtual {v3, v8, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    .line 73
    invoke-virtual {v3, v9}, LaFt;->b(I)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEX;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEX;->d:LaEX;

    .line 40
    const/4 v0, 0x4

    new-array v0, v0, [LaEX;

    sget-object v1, LaEX;->a:LaEX;

    aput-object v1, v0, v7

    sget-object v1, LaEX;->b:LaEX;

    aput-object v1, v0, v10

    sget-object v1, LaEX;->c:LaEX;

    aput-object v1, v0, v11

    const/4 v1, 0x3

    sget-object v2, LaEX;->d:LaEX;

    aput-object v2, v0, v1

    sput-object v0, LaEX;->a:[LaEX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaFt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 79
    invoke-virtual {p3}, LaFt;->a()LaFr;

    move-result-object v0

    iput-object v0, p0, LaEX;->a:LaFr;

    .line 80
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaEX;
    .locals 1

    .prologue
    .line 40
    const-class v0, LaEX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaEX;

    return-object v0
.end method

.method public static values()[LaEX;
    .locals 1

    .prologue
    .line 40
    sget-object v0, LaEX;->a:[LaEX;

    invoke-virtual {v0}, [LaEX;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaEX;

    return-object v0
.end method


# virtual methods
.method public a()LaFr;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, LaEX;->a:LaFr;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, LaEX;->a()LaFr;

    move-result-object v0

    return-object v0
.end method
