.class public LRK;
.super Lhi;
.source "NavigationListAdapter.java"


# static fields
.field private static final b:I

.field private static final b:[Ljava/lang/String;

.field private static final c:[I


# instance fields
.field private final a:LaFO;

.field private final a:LaGM;

.field private final a:Lahh;

.field private final a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "LRN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31
    sget v0, Lxe;->navigation_list_item:I

    sput v0, LRK;->b:I

    .line 32
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "LABEL"

    aput-object v1, v0, v2

    sput-object v0, LRK;->b:[Ljava/lang/String;

    .line 33
    new-array v0, v3, [I

    sget v1, Lxc;->name:I

    aput v1, v0, v2

    sput-object v0, LRK;->c:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;LaFO;LaGM;Lahh;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "LRN;",
            ">;",
            "LaFO;",
            "LaGM;",
            "Lahh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 54
    sget v2, LRK;->b:I

    invoke-static {p2}, LRK;->a(Ljava/util/List;)Landroid/database/Cursor;

    move-result-object v3

    sget-object v4, LRK;->b:[Ljava/lang/String;

    sget-object v5, LRK;->c:[I

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lhi;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    .line 56
    invoke-static {p2}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    iput-object v0, p0, LRK;->a:LbmF;

    .line 57
    iput-object p4, p0, LRK;->a:LaGM;

    .line 58
    iput-object p3, p0, LRK;->a:LaFO;

    .line 59
    iput-object p5, p0, LRK;->a:Lahh;

    .line 60
    return-void
.end method

.method static synthetic a(LRK;)LaFO;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, LRK;->a:LaFO;

    return-object v0
.end method

.method static synthetic a(LRK;)LaGM;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, LRK;->a:LaGM;

    return-object v0
.end method

.method static synthetic a(LRK;)Lahh;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, LRK;->a:Lahh;

    return-object v0
.end method

.method private static a(Ljava/util/List;)Landroid/database/Cursor;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LRN;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 137
    new-instance v3, Landroid/database/MatrixCursor;

    new-array v0, v8, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "LABEL"

    aput-object v1, v0, v6

    const-string v1, "FILTER_NAME"

    aput-object v1, v0, v7

    invoke-direct {v3, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    move v1, v2

    .line 139
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 140
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LRN;

    .line 141
    new-array v4, v8, [Ljava/lang/Object;

    add-int/lit8 v5, v1, 0x1

    .line 142
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v0}, LRN;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    .line 143
    invoke-virtual {v0}, LRN;->a()LCl;

    move-result-object v0

    invoke-interface {v0}, LCl;->name()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    .line 141
    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 139
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 146
    :cond_0
    return-object v3
.end method

.method static synthetic a(LRK;Landroid/content/Context;LRN;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, LRK;->a(Landroid/content/Context;LRN;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Context;LRN;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 123
    invoke-virtual {p2}, LRN;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lxi;->description_icon_stale_items:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 125
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 65
    invoke-super {p0, p1, p2, p3}, Lhi;->a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 67
    const-string v0, "_id"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 68
    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 69
    add-int/lit8 v0, v0, -0x1

    .line 70
    iget-object v1, p0, LRK;->a:LbmF;

    invoke-virtual {v1, v0}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LRN;

    .line 72
    sget v0, Lxc;->group_icon:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 73
    invoke-virtual {v2}, LRN;->a()I

    move-result v1

    .line 74
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 76
    sget v0, Lxc;->navigation_warning_image:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 77
    invoke-virtual {v2}, LRN;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 79
    sget v0, Lxc;->name:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 87
    new-instance v0, LRL;

    move-object v1, p0

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LRL;-><init>(LRK;LRN;Landroid/widget/ImageView;Landroid/content/Context;Landroid/view/View;)V

    new-array v1, v6, [Ljava/lang/Void;

    .line 115
    invoke-virtual {v0, v1}, LRL;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 116
    return-void
.end method
