.class Lpy;
.super Ljava/lang/Object;
.source "AppPackageListModule.java"

# interfaces
.implements LUI;


# instance fields
.field private final a:LQr;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LQr;)V
    .locals 5

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-string v0, "com.google.android.apps.docs.openurl.DriveOpenUrlActivityAlias"

    const-string v1, "com.google.android.apps.docs.openurl.KixOpenUrlActivityAlias"

    const-string v2, "com.google.android.apps.docs.openurl.PunchOpenUrlActivityAlias"

    const-string v3, "com.google.android.apps.docs.openurl.DrawingsOpenUrlActivityAlias"

    const-string v4, "com.google.android.apps.docs.openurl.TrixOpenUrlActivityAlias"

    invoke-static {v0, v1, v2, v3, v4}, LbmF;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmF;

    move-result-object v0

    iput-object v0, p0, Lpy;->a:Ljava/util/List;

    .line 57
    sget-object v0, LuM;->b:LuM;

    .line 58
    invoke-virtual {v0}, LuM;->a()Ljava/lang/String;

    move-result-object v0

    sget-object v1, LuM;->c:LuM;

    .line 59
    invoke-virtual {v1}, LuM;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LuM;->d:LuM;

    .line 60
    invoke-virtual {v2}, LuM;->a()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LuM;->e:LuM;

    .line 61
    invoke-virtual {v3}, LuM;->a()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LuM;->a:LuM;

    .line 62
    invoke-virtual {v4}, LuM;->a()Ljava/lang/String;

    move-result-object v4

    .line 57
    invoke-static {v0, v1, v2, v3, v4}, LbmF;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmF;

    move-result-object v0

    iput-object v0, p0, Lpy;->b:Ljava/util/List;

    .line 69
    iput-object p1, p0, Lpy;->a:LQr;

    .line 70
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lpy;->a:LQr;

    const-string v1, "openUrlActivityAliases"

    const/4 v2, 0x0

    .line 76
    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 78
    if-eqz v0, :cond_0

    const-string v1, ","

    .line 79
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LbnG;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 82
    :goto_0
    return-object v0

    .line 79
    :cond_0
    iget-object v0, p0, Lpy;->a:Ljava/util/List;

    goto :goto_0
.end method

.method public b()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lpy;->a:LQr;

    const-string v1, "openUrlPackagePriorityList"

    const/4 v2, 0x0

    .line 89
    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 91
    if-eqz v0, :cond_0

    const-string v1, ","

    .line 92
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LbnG;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 95
    :goto_0
    return-object v0

    .line 92
    :cond_0
    iget-object v0, p0, Lpy;->b:Ljava/util/List;

    goto :goto_0
.end method
