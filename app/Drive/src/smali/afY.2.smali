.class final enum LafY;
.super Ljava/lang/Enum;
.source "ContentSyncOverallStatusNotifier.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LafY;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LafY;

.field private static final synthetic a:[LafY;

.field public static final enum b:LafY;


# instance fields
.field final a:I

.field final a:LCn;

.field final a:LahS;

.field final a:Ljava/lang/String;

.field final b:I

.field final c:I

.field final d:I

.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    .line 66
    new-instance v0, LafY;

    const-string v1, "DOWNLOAD"

    const/4 v2, 0x0

    const/4 v3, 0x5

    const/4 v4, 0x2

    sget v5, Lxb;->ic_offline_notification:I

    sget v6, Lxg;->pin_notification_sync_progress:I

    sget v7, Lxg;->pin_notification_waiting_title:I

    sget-object v8, LahS;->a:LahS;

    sget-object v9, LCn;->i:LCn;

    const-string v10, "com.google.android.apps.docs.receivers.UploadActionsReceiver.FORCE_RESUME_DOWNLOADS"

    invoke-direct/range {v0 .. v10}, LafY;-><init>(Ljava/lang/String;IIIIIILahS;LCn;Ljava/lang/String;)V

    sput-object v0, LafY;->a:LafY;

    .line 74
    new-instance v0, LafY;

    const-string v1, "UPLOAD"

    const/4 v2, 0x1

    const/4 v3, 0x6

    const/16 v4, 0x9

    sget v5, Lxb;->ic_upload_notification:I

    sget v6, Lxg;->upload_notification_sync_progress:I

    sget v7, Lxg;->upload_notification_waiting_title:I

    sget-object v8, LahS;->b:LahS;

    sget-object v9, LCn;->h:LCn;

    const-string v10, "com.google.android.apps.docs.receivers.UploadActionsReceiver.FORCE_RESUME_UPLOADS"

    invoke-direct/range {v0 .. v10}, LafY;-><init>(Ljava/lang/String;IIIIIILahS;LCn;Ljava/lang/String;)V

    sput-object v0, LafY;->b:LafY;

    .line 65
    const/4 v0, 0x2

    new-array v0, v0, [LafY;

    const/4 v1, 0x0

    sget-object v2, LafY;->a:LafY;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LafY;->b:LafY;

    aput-object v2, v0, v1

    sput-object v0, LafY;->a:[LafY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIIILahS;LCn;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIII",
            "LahS;",
            "LCn;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 95
    iput p3, p0, LafY;->a:I

    .line 96
    iput p4, p0, LafY;->b:I

    .line 97
    iput p5, p0, LafY;->c:I

    .line 98
    iput p6, p0, LafY;->d:I

    .line 99
    iput p7, p0, LafY;->e:I

    .line 100
    iput-object p8, p0, LafY;->a:LahS;

    .line 101
    iput-object p9, p0, LafY;->a:LCn;

    .line 102
    iput-object p10, p0, LafY;->a:Ljava/lang/String;

    .line 103
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LafY;
    .locals 1

    .prologue
    .line 65
    const-class v0, LafY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LafY;

    return-object v0
.end method

.method public static values()[LafY;
    .locals 1

    .prologue
    .line 65
    sget-object v0, LafY;->a:[LafY;

    invoke-virtual {v0}, [LafY;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LafY;

    return-object v0
.end method
