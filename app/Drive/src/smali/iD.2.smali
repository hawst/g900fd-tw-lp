.class public LiD;
.super LhJ;
.source "WindowDecorActionBar.java"

# interfaces
.implements LjJ;


# static fields
.field static final synthetic b:Z

.field private static final c:Z


# instance fields
.field private a:I

.field private a:LH;

.field private a:Landroid/content/Context;

.field private a:Landroid/support/v7/internal/widget/ActionBarContainer;

.field private a:Landroid/support/v7/internal/widget/ActionBarContextView;

.field private a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

.field private a:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

.field private a:Landroid/view/View;

.field final a:Lfn;

.field final a:Lfp;

.field a:LiH;

.field private a:LiR;

.field private a:Ljava/util/ArrayList;

.field private a:Lkf;

.field a:Lmu;

.field a:Lmv;

.field a:Z

.field private b:I

.field private b:Landroid/content/Context;

.field private b:Landroid/support/v7/internal/widget/ActionBarContainer;

.field final b:Lfn;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LhL;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 75
    const-class v0, LiD;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, LiD;->b:Z

    .line 82
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v0, v3, :cond_1

    :goto_1
    sput-boolean v1, LiD;->c:Z

    return-void

    :cond_0
    move v0, v2

    .line 75
    goto :goto_0

    :cond_1
    move v1, v2

    .line 82
    goto :goto_1
.end method

.method public constructor <init>(LhN;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 172
    invoke-direct {p0}, LhJ;-><init>()V

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LiD;->a:Ljava/util/ArrayList;

    .line 100
    const/4 v0, -0x1

    iput v0, p0, LiD;->a:I

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LiD;->b:Ljava/util/ArrayList;

    .line 120
    const/4 v0, 0x0

    iput v0, p0, LiD;->c:I

    .line 122
    iput-boolean v1, p0, LiD;->g:Z

    .line 127
    iput-boolean v1, p0, LiD;->k:Z

    .line 135
    new-instance v0, LiE;

    invoke-direct {v0, p0}, LiE;-><init>(LiD;)V

    iput-object v0, p0, LiD;->a:Lfn;

    .line 155
    new-instance v0, LiF;

    invoke-direct {v0, p0}, LiF;-><init>(LiD;)V

    iput-object v0, p0, LiD;->b:Lfn;

    .line 163
    new-instance v0, LiG;

    invoke-direct {v0, p0}, LiG;-><init>(LiD;)V

    iput-object v0, p0, LiD;->a:Lfp;

    .line 173
    iput-object p1, p0, LiD;->a:LH;

    .line 174
    invoke-virtual {p1}, LhN;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 175
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 176
    invoke-direct {p0, v0}, LiD;->a(Landroid/view/View;)V

    .line 177
    if-nez p2, :cond_0

    .line 178
    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LiD;->a:Landroid/view/View;

    .line 180
    :cond_0
    return-void
.end method

.method static synthetic a(LiD;)I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, LiD;->b:I

    return v0
.end method

.method static synthetic a(LiD;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, LiD;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(LiD;)Landroid/support/v7/internal/widget/ActionBarContainer;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    return-object v0
.end method

.method static synthetic a(LiD;)Landroid/support/v7/internal/widget/ActionBarContextView;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    return-object v0
.end method

.method static synthetic a(LiD;)Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    return-object v0
.end method

.method static synthetic a(LiD;)Landroid/view/View;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, LiD;->a:Landroid/view/View;

    return-object v0
.end method

.method static synthetic a(LiD;LiR;)LiR;
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, LiD;->a:LiR;

    return-object p1
.end method

.method private a(Landroid/view/View;)Lkf;
    .locals 3

    .prologue
    .line 243
    instance-of v0, p1, Lkf;

    if-eqz v0, :cond_0

    .line 244
    check-cast p1, Lkf;

    .line 246
    :goto_0
    return-object p1

    .line 245
    :cond_0
    instance-of v0, p1, Landroid/support/v7/widget/Toolbar;

    if-eqz v0, :cond_1

    .line 246
    check-cast p1, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->a()Lkf;

    move-result-object p1

    goto :goto_0

    .line 248
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t make a decor toolbar out of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic a(LiD;)Lkf;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, LiD;->a:Lkf;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 197
    sget v0, Liv;->decor_content_parent:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iput-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    .line 198
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->setActionBarVisibilityCallback(LjJ;)V

    .line 201
    :cond_0
    sget v0, Liv;->action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, LiD;->a(Landroid/view/View;)Lkf;

    move-result-object v0

    iput-object v0, p0, LiD;->a:Lkf;

    .line 202
    sget v0, Liv;->action_context_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContextView;

    iput-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    .line 204
    sget v0, Liv;->action_bar_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContainer;

    iput-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    .line 207
    sget v0, Liv;->split_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContainer;

    iput-object v0, p0, LiD;->b:Landroid/support/v7/internal/widget/ActionBarContainer;

    .line 209
    iget-object v0, p0, LiD;->a:Lkf;

    if-eqz v0, :cond_1

    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-nez v0, :cond_2

    .line 210
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " can only be used "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "with a compatible window decor layout"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 214
    :cond_2
    iget-object v0, p0, LiD;->a:Lkf;

    invoke-interface {v0}, Lkf;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LiD;->a:Landroid/content/Context;

    .line 215
    iget-object v0, p0, LiD;->a:Lkf;

    invoke-interface {v0}, Lkf;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_0
    iput v0, p0, LiD;->b:I

    .line 219
    iget-object v0, p0, LiD;->a:Lkf;

    invoke-interface {v0}, Lkf;->a()I

    move-result v0

    .line 220
    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_8

    move v0, v1

    .line 221
    :goto_1
    if-eqz v0, :cond_3

    .line 222
    iput-boolean v1, p0, LiD;->d:Z

    .line 225
    :cond_3
    iget-object v3, p0, LiD;->a:Landroid/content/Context;

    invoke-static {v3}, LiK;->a(Landroid/content/Context;)LiK;

    move-result-object v3

    .line 226
    invoke-virtual {v3}, LiK;->c()Z

    move-result v4

    if-nez v4, :cond_4

    if-eqz v0, :cond_9

    :cond_4
    move v0, v1

    :goto_2
    invoke-virtual {p0, v0}, LiD;->c(Z)V

    .line 227
    invoke-virtual {v3}, LiK;->b()Z

    move-result v0

    invoke-direct {p0, v0}, LiD;->l(Z)V

    .line 229
    iget-object v0, p0, LiD;->a:Landroid/content/Context;

    const/4 v3, 0x0

    sget-object v4, LiA;->ActionBar:[I

    sget v5, Liq;->actionBarStyle:I

    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 232
    sget v3, LiA;->ActionBar_hideOnContentScroll:I

    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 233
    invoke-virtual {p0, v1}, LiD;->d(Z)V

    .line 235
    :cond_5
    sget v1, LiA;->ActionBar_elevation:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 236
    if-eqz v1, :cond_6

    .line 237
    int-to-float v1, v1

    invoke-virtual {p0, v1}, LiD;->a(F)V

    .line 239
    :cond_6
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 240
    return-void

    :cond_7
    move v0, v2

    .line 215
    goto :goto_0

    :cond_8
    move v0, v2

    .line 220
    goto :goto_1

    :cond_9
    move v0, v2

    .line 226
    goto :goto_2
.end method

.method static synthetic a(LiD;)Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, LiD;->g:Z

    return v0
.end method

.method static synthetic a(ZZZ)Z
    .locals 1

    .prologue
    .line 75
    invoke-static {p0, p1, p2}, LiD;->b(ZZZ)Z

    move-result v0

    return v0
.end method

.method static synthetic b(LiD;)Landroid/support/v7/internal/widget/ActionBarContainer;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, LiD;->b:Landroid/support/v7/internal/widget/ActionBarContainer;

    return-object v0
.end method

.method static synthetic b(LiD;)Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, LiD;->h:Z

    return v0
.end method

.method private static b(ZZZ)Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 738
    if-eqz p2, :cond_1

    .line 743
    :cond_0
    :goto_0
    return v0

    .line 740
    :cond_1
    if-nez p0, :cond_2

    if-eqz p1, :cond_0

    .line 741
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(LiD;)Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, LiD;->i:Z

    return v0
.end method

.method private g()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 666
    iget-boolean v0, p0, LiD;->j:Z

    if-nez v0, :cond_1

    .line 667
    iput-boolean v1, p0, LiD;->j:Z

    .line 668
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    .line 669
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->setShowingForActionMode(Z)V

    .line 671
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LiD;->m(Z)V

    .line 673
    :cond_1
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 691
    iget-boolean v0, p0, LiD;->j:Z

    if-eqz v0, :cond_1

    .line 692
    iput-boolean v1, p0, LiD;->j:Z

    .line 693
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    .line 694
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->setShowingForActionMode(Z)V

    .line 696
    :cond_0
    invoke-direct {p0, v1}, LiD;->m(Z)V

    .line 698
    :cond_1
    return-void
.end method

.method private l(Z)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 271
    iput-boolean p1, p0, LiD;->f:Z

    .line 273
    iget-boolean v0, p0, LiD;->f:Z

    if-nez v0, :cond_1

    .line 274
    iget-object v0, p0, LiD;->a:Lkf;

    invoke-interface {v0, v3}, Lkf;->a(Landroid/support/v7/internal/widget/ScrollingTabContainerView;)V

    .line 275
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    iget-object v3, p0, LiD;->a:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ActionBarContainer;->setTabContainer(Landroid/support/v7/internal/widget/ScrollingTabContainerView;)V

    .line 280
    :goto_0
    invoke-virtual {p0}, LiD;->c()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v1

    .line 281
    :goto_1
    iget-object v3, p0, LiD;->a:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    if-eqz v3, :cond_0

    .line 282
    if-eqz v0, :cond_3

    .line 283
    iget-object v3, p0, LiD;->a:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    invoke-virtual {v3, v2}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->setVisibility(I)V

    .line 284
    iget-object v3, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v3, :cond_0

    .line 285
    iget-object v3, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {v3}, Lec;->b(Landroid/view/View;)V

    .line 291
    :cond_0
    :goto_2
    iget-object v4, p0, LiD;->a:Lkf;

    iget-boolean v3, p0, LiD;->f:Z

    if-nez v3, :cond_4

    if-eqz v0, :cond_4

    move v3, v1

    :goto_3
    invoke-interface {v4, v3}, Lkf;->a(Z)V

    .line 292
    iget-object v3, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-boolean v4, p0, LiD;->f:Z

    if-nez v4, :cond_5

    if-eqz v0, :cond_5

    :goto_4
    invoke-virtual {v3, v1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->setHasNonEmbeddedTabs(Z)V

    .line 293
    return-void

    .line 277
    :cond_1
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ActionBarContainer;->setTabContainer(Landroid/support/v7/internal/widget/ScrollingTabContainerView;)V

    .line 278
    iget-object v0, p0, LiD;->a:Lkf;

    iget-object v3, p0, LiD;->a:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    invoke-interface {v0, v3}, Lkf;->a(Landroid/support/v7/internal/widget/ScrollingTabContainerView;)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 280
    goto :goto_1

    .line 288
    :cond_3
    iget-object v3, p0, LiD;->a:Landroid/support/v7/internal/widget/ScrollingTabContainerView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/support/v7/internal/widget/ScrollingTabContainerView;->setVisibility(I)V

    goto :goto_2

    :cond_4
    move v3, v2

    .line 291
    goto :goto_3

    :cond_5
    move v1, v2

    .line 292
    goto :goto_4
.end method

.method private m(Z)V
    .locals 3

    .prologue
    .line 749
    iget-boolean v0, p0, LiD;->h:Z

    iget-boolean v1, p0, LiD;->i:Z

    iget-boolean v2, p0, LiD;->j:Z

    invoke-static {v0, v1, v2}, LiD;->b(ZZZ)Z

    move-result v0

    .line 752
    if-eqz v0, :cond_1

    .line 753
    iget-boolean v0, p0, LiD;->k:Z

    if-nez v0, :cond_0

    .line 754
    const/4 v0, 0x1

    iput-boolean v0, p0, LiD;->k:Z

    .line 755
    invoke-virtual {p0, p1}, LiD;->i(Z)V

    .line 763
    :cond_0
    :goto_0
    return-void

    .line 758
    :cond_1
    iget-boolean v0, p0, LiD;->k:Z

    if-eqz v0, :cond_0

    .line 759
    const/4 v0, 0x0

    iput-boolean v0, p0, LiD;->k:Z

    .line 760
    invoke-virtual {p0, p1}, LiD;->j(Z)V

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, LiD;->a:Lkf;

    invoke-interface {v0}, Lkf;->a()I

    move-result v0

    return v0
.end method

.method public a()Landroid/content/Context;
    .locals 4

    .prologue
    .line 882
    iget-object v0, p0, LiD;->b:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 883
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 884
    iget-object v1, p0, LiD;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    .line 885
    sget v2, Liq;->actionBarWidgetTheme:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 886
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    .line 888
    if-eqz v0, :cond_1

    .line 889
    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, LiD;->a:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, LiD;->b:Landroid/content/Context;

    .line 894
    :cond_0
    :goto_0
    iget-object v0, p0, LiD;->b:Landroid/content/Context;

    return-object v0

    .line 891
    :cond_1
    iget-object v0, p0, LiD;->a:Landroid/content/Context;

    iput-object v0, p0, LiD;->b:Landroid/content/Context;

    goto :goto_0
.end method

.method public a(Lmv;)Lmu;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 503
    iget-object v0, p0, LiD;->a:LiH;

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, LiD;->a:LiH;

    invoke-virtual {v0}, LiH;->a()V

    .line 507
    :cond_0
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->setHideOnContentScrollEnabled(Z)V

    .line 508
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->b()V

    .line 509
    new-instance v0, LiH;

    invoke-direct {v0, p0, p1}, LiH;-><init>(LiD;Lmv;)V

    .line 510
    invoke-virtual {v0}, LiH;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 511
    invoke-virtual {v0}, LiH;->b()V

    .line 512
    iget-object v1, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(Lmu;)V

    .line 513
    invoke-virtual {p0, v3}, LiD;->k(Z)V

    .line 514
    iget-object v1, p0, LiD;->b:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v1, :cond_1

    iget v1, p0, LiD;->b:I

    if-ne v1, v3, :cond_1

    .line 516
    iget-object v1, p0, LiD;->b:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarContainer;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    .line 517
    iget-object v1, p0, LiD;->b:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    .line 518
    iget-object v1, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v1, :cond_1

    .line 519
    iget-object v1, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {v1}, Lec;->b(Landroid/view/View;)V

    .line 523
    :cond_1
    iget-object v1, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ActionBarContextView;->sendAccessibilityEvent(I)V

    .line 524
    iput-object v0, p0, LiD;->a:LiH;

    .line 527
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 659
    iget-boolean v0, p0, LiD;->h:Z

    if-eqz v0, :cond_0

    .line 660
    iput-boolean v1, p0, LiD;->h:Z

    .line 661
    invoke-direct {p0, v1}, LiD;->m(Z)V

    .line 663
    :cond_0
    return-void
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, p1}, Lec;->f(Landroid/view/View;F)V

    .line 256
    iget-object v0, p0, LiD;->b:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, LiD;->b:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, p1}, Lec;->f(Landroid/view/View;F)V

    .line 259
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 1314
    iget-object v0, p0, LiD;->a:Lkf;

    invoke-interface {v0, p1}, Lkf;->a(I)V

    .line 1315
    return-void
.end method

.method public a(II)V
    .locals 4

    .prologue
    .line 461
    iget-object v0, p0, LiD;->a:Lkf;

    invoke-interface {v0}, Lkf;->a()I

    move-result v0

    .line 462
    and-int/lit8 v1, p2, 0x4

    if-eqz v1, :cond_0

    .line 463
    const/4 v1, 0x1

    iput-boolean v1, p0, LiD;->d:Z

    .line 465
    :cond_0
    iget-object v1, p0, LiD;->a:Lkf;

    and-int v2, p1, p2

    xor-int/lit8 v3, p2, -0x1

    and-int/2addr v0, v3

    or-int/2addr v0, v2

    invoke-interface {v1, v0}, Lkf;->c(I)V

    .line 466
    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, LiD;->a:Landroid/content/Context;

    invoke-static {v0}, LiK;->a(Landroid/content/Context;)LiK;

    move-result-object v0

    invoke-virtual {v0}, LiK;->b()Z

    move-result v0

    invoke-direct {p0, v0}, LiD;->l(Z)V

    .line 268
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 904
    iget-object v0, p0, LiD;->a:Lkf;

    invoke-interface {v0, p1}, Lkf;->b(Landroid/graphics/drawable/Drawable;)V

    .line 905
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, LiD;->a:Lkf;

    invoke-interface {v0, p1}, Lkf;->b(Ljava/lang/CharSequence;)V

    .line 442
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 383
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, LiD;->a(II)V

    .line 384
    return-void

    .line 383
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 650
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v0

    return v0
.end method

.method b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 320
    iget-object v0, p0, LiD;->a:Lmv;

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, LiD;->a:Lmv;

    iget-object v1, p0, LiD;->a:Lmu;

    invoke-interface {v0, v1}, Lmv;->a(Lmu;)V

    .line 322
    iput-object v2, p0, LiD;->a:Lmu;

    .line 323
    iput-object v2, p0, LiD;->a:Lmv;

    .line 325
    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 919
    iget-object v0, p0, LiD;->a:Lkf;

    invoke-interface {v0, p1}, Lkf;->e(I)V

    .line 920
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, LiD;->a:Lkf;

    invoke-interface {v0, p1}, Lkf;->a(Ljava/lang/CharSequence;)V

    .line 447
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 388
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, LiD;->a(II)V

    .line 389
    return-void

    .line 388
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 936
    iget-object v0, p0, LiD;->a:Lkf;

    if-eqz v0, :cond_0

    iget-object v0, p0, LiD;->a:Lkf;

    invoke-interface {v0}, Lkf;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 937
    iget-object v0, p0, LiD;->a:Lkf;

    invoke-interface {v0}, Lkf;->a()V

    .line 938
    const/4 v0, 0x1

    .line 940
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 495
    iget-object v0, p0, LiD;->a:Lkf;

    invoke-interface {v0}, Lkf;->b()I

    move-result v0

    return v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 676
    iget-boolean v0, p0, LiD;->i:Z

    if-eqz v0, :cond_0

    .line 677
    const/4 v0, 0x0

    iput-boolean v0, p0, LiD;->i:Z

    .line 678
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LiD;->m(Z)V

    .line 680
    :cond_0
    return-void
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 328
    iput p1, p0, LiD;->c:I

    .line 329
    return-void
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, LiD;->a:Lkf;

    invoke-interface {v0, p1}, Lkf;->b(Z)V

    .line 399
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 701
    iget-boolean v0, p0, LiD;->i:Z

    if-nez v0, :cond_0

    .line 702
    iput-boolean v1, p0, LiD;->i:Z

    .line 703
    invoke-direct {p0, v1}, LiD;->m(Z)V

    .line 705
    :cond_0
    return-void
.end method

.method public d(Z)V
    .locals 2

    .prologue
    .line 709
    if-eqz p1, :cond_0

    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 710
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 713
    :cond_0
    iput-boolean p1, p0, LiD;->a:Z

    .line 714
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->setHideOnContentScrollEnabled(Z)V

    .line 715
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 924
    iget-object v0, p0, LiD;->a:LiR;

    if-eqz v0, :cond_0

    .line 925
    iget-object v0, p0, LiD;->a:LiR;

    invoke-virtual {v0}, LiR;->b()V

    .line 926
    const/4 v0, 0x0

    iput-object v0, p0, LiD;->a:LiR;

    .line 928
    :cond_0
    return-void
.end method

.method public e(Z)V
    .locals 1

    .prologue
    .line 1341
    iget-boolean v0, p0, LiD;->d:Z

    if-nez v0, :cond_0

    .line 1342
    invoke-virtual {p0, p1}, LiD;->a(Z)V

    .line 1344
    :cond_0
    return-void
.end method

.method public f()V
    .locals 0

    .prologue
    .line 932
    return-void
.end method

.method public f(Z)V
    .locals 1

    .prologue
    .line 339
    iput-boolean p1, p0, LiD;->l:Z

    .line 340
    if-nez p1, :cond_0

    iget-object v0, p0, LiD;->a:LiR;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, LiD;->a:LiR;

    invoke-virtual {v0}, LiR;->b()V

    .line 343
    :cond_0
    return-void
.end method

.method public g(Z)V
    .locals 3

    .prologue
    .line 354
    iget-boolean v0, p0, LiD;->e:Z

    if-ne p1, v0, :cond_1

    .line 363
    :cond_0
    return-void

    .line 357
    :cond_1
    iput-boolean p1, p0, LiD;->e:Z

    .line 359
    iget-object v0, p0, LiD;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 360
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 361
    iget-object v0, p0, LiD;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LhL;

    invoke-interface {v0, p1}, LhL;->a(Z)V

    .line 360
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public h(Z)V
    .locals 0

    .prologue
    .line 654
    iput-boolean p1, p0, LiD;->g:Z

    .line 655
    return-void
.end method

.method public i(Z)V
    .locals 7

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 766
    iget-object v0, p0, LiD;->a:LiR;

    if-eqz v0, :cond_0

    .line 767
    iget-object v0, p0, LiD;->a:LiR;

    invoke-virtual {v0}, LiR;->b()V

    .line 769
    :cond_0
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v5}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    .line 771
    iget v0, p0, LiD;->c:I

    if-nez v0, :cond_6

    sget-boolean v0, LiD;->c:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, LiD;->l:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_6

    .line 774
    :cond_1
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, Lec;->b(Landroid/view/View;F)V

    .line 775
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    .line 776
    if-eqz p1, :cond_2

    .line 777
    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    .line 778
    iget-object v2, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v2, v1}, Landroid/support/v7/internal/widget/ActionBarContainer;->getLocationInWindow([I)V

    .line 779
    aget v1, v1, v6

    int-to-float v1, v1

    sub-float/2addr v0, v1

    .line 781
    :cond_2
    iget-object v1, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v1, v0}, Lec;->b(Landroid/view/View;F)V

    .line 782
    new-instance v1, LiR;

    invoke-direct {v1}, LiR;-><init>()V

    .line 783
    iget-object v2, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v2}, Lec;->a(Landroid/view/View;)LeZ;

    move-result-object v2

    invoke-virtual {v2, v4}, LeZ;->c(F)LeZ;

    move-result-object v2

    .line 784
    iget-object v3, p0, LiD;->a:Lfp;

    invoke-virtual {v2, v3}, LeZ;->a(Lfp;)LeZ;

    .line 785
    invoke-virtual {v1, v2}, LiR;->a(LeZ;)LiR;

    .line 786
    iget-boolean v2, p0, LiD;->g:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, LiD;->a:Landroid/view/View;

    if-eqz v2, :cond_3

    .line 787
    iget-object v2, p0, LiD;->a:Landroid/view/View;

    invoke-static {v2, v0}, Lec;->b(Landroid/view/View;F)V

    .line 788
    iget-object v0, p0, LiD;->a:Landroid/view/View;

    invoke-static {v0}, Lec;->a(Landroid/view/View;)LeZ;

    move-result-object v0

    invoke-virtual {v0, v4}, LeZ;->c(F)LeZ;

    move-result-object v0

    invoke-virtual {v1, v0}, LiR;->a(LeZ;)LiR;

    .line 790
    :cond_3
    iget-object v0, p0, LiD;->b:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_4

    iget v0, p0, LiD;->b:I

    if-ne v0, v6, :cond_4

    .line 791
    iget-object v0, p0, LiD;->b:Landroid/support/v7/internal/widget/ActionBarContainer;

    iget-object v2, p0, LiD;->b:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-static {v0, v2}, Lec;->b(Landroid/view/View;F)V

    .line 792
    iget-object v0, p0, LiD;->b:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v5}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    .line 793
    iget-object v0, p0, LiD;->b:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0}, Lec;->a(Landroid/view/View;)LeZ;

    move-result-object v0

    invoke-virtual {v0, v4}, LeZ;->c(F)LeZ;

    move-result-object v0

    invoke-virtual {v1, v0}, LiR;->a(LeZ;)LiR;

    .line 795
    :cond_4
    iget-object v0, p0, LiD;->a:Landroid/content/Context;

    const v2, 0x10a0006

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    invoke-virtual {v1, v0}, LiR;->a(Landroid/view/animation/Interpolator;)LiR;

    .line 797
    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v2, v3}, LiR;->a(J)LiR;

    .line 805
    iget-object v0, p0, LiD;->b:Lfn;

    invoke-virtual {v1, v0}, LiR;->a(Lfn;)LiR;

    .line 806
    iput-object v1, p0, LiD;->a:LiR;

    .line 807
    invoke-virtual {v1}, LiR;->a()V

    .line 821
    :goto_0
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_5

    .line 822
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {v0}, Lec;->b(Landroid/view/View;)V

    .line 824
    :cond_5
    return-void

    .line 809
    :cond_6
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v1}, Lec;->c(Landroid/view/View;F)V

    .line 810
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, Lec;->b(Landroid/view/View;F)V

    .line 811
    iget-boolean v0, p0, LiD;->g:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, LiD;->a:Landroid/view/View;

    if-eqz v0, :cond_7

    .line 812
    iget-object v0, p0, LiD;->a:Landroid/view/View;

    invoke-static {v0, v4}, Lec;->b(Landroid/view/View;F)V

    .line 814
    :cond_7
    iget-object v0, p0, LiD;->b:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_8

    iget v0, p0, LiD;->b:I

    if-ne v0, v6, :cond_8

    .line 815
    iget-object v0, p0, LiD;->b:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v1}, Lec;->c(Landroid/view/View;F)V

    .line 816
    iget-object v0, p0, LiD;->b:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, Lec;->b(Landroid/view/View;F)V

    .line 817
    iget-object v0, p0, LiD;->b:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v5}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    .line 819
    :cond_8
    iget-object v0, p0, LiD;->b:Lfn;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lfn;->b(Landroid/view/View;)V

    goto :goto_0

    .line 777
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public j(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    .line 827
    iget-object v0, p0, LiD;->a:LiR;

    if-eqz v0, :cond_0

    .line 828
    iget-object v0, p0, LiD;->a:LiR;

    invoke-virtual {v0}, LiR;->b()V

    .line 831
    :cond_0
    iget v0, p0, LiD;->c:I

    if-nez v0, :cond_5

    sget-boolean v0, LiD;->c:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, LiD;->l:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_5

    .line 833
    :cond_1
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, Lec;->c(Landroid/view/View;F)V

    .line 834
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v5}, Landroid/support/v7/internal/widget/ActionBarContainer;->setTransitioning(Z)V

    .line 835
    new-instance v1, LiR;

    invoke-direct {v1}, LiR;-><init>()V

    .line 836
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    .line 837
    if-eqz p1, :cond_2

    .line 838
    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    .line 839
    iget-object v3, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v3, v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->getLocationInWindow([I)V

    .line 840
    aget v2, v2, v5

    int-to-float v2, v2

    sub-float/2addr v0, v2

    .line 842
    :cond_2
    iget-object v2, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v2}, Lec;->a(Landroid/view/View;)LeZ;

    move-result-object v2

    invoke-virtual {v2, v0}, LeZ;->c(F)LeZ;

    move-result-object v2

    .line 843
    iget-object v3, p0, LiD;->a:Lfp;

    invoke-virtual {v2, v3}, LeZ;->a(Lfp;)LeZ;

    .line 844
    invoke-virtual {v1, v2}, LiR;->a(LeZ;)LiR;

    .line 845
    iget-boolean v2, p0, LiD;->g:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, LiD;->a:Landroid/view/View;

    if-eqz v2, :cond_3

    .line 846
    iget-object v2, p0, LiD;->a:Landroid/view/View;

    invoke-static {v2}, Lec;->a(Landroid/view/View;)LeZ;

    move-result-object v2

    invoke-virtual {v2, v0}, LeZ;->c(F)LeZ;

    move-result-object v0

    invoke-virtual {v1, v0}, LiR;->a(LeZ;)LiR;

    .line 848
    :cond_3
    iget-object v0, p0, LiD;->b:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_4

    iget-object v0, p0, LiD;->b:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    .line 849
    iget-object v0, p0, LiD;->b:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, Lec;->c(Landroid/view/View;F)V

    .line 850
    iget-object v0, p0, LiD;->b:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0}, Lec;->a(Landroid/view/View;)LeZ;

    move-result-object v0

    iget-object v2, p0, LiD;->b:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v2}, LeZ;->c(F)LeZ;

    move-result-object v0

    invoke-virtual {v1, v0}, LiR;->a(LeZ;)LiR;

    .line 852
    :cond_4
    iget-object v0, p0, LiD;->a:Landroid/content/Context;

    const v2, 0x10a0005

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    invoke-virtual {v1, v0}, LiR;->a(Landroid/view/animation/Interpolator;)LiR;

    .line 854
    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v2, v3}, LiR;->a(J)LiR;

    .line 855
    iget-object v0, p0, LiD;->a:Lfn;

    invoke-virtual {v1, v0}, LiR;->a(Lfn;)LiR;

    .line 856
    iput-object v1, p0, LiD;->a:LiR;

    .line 857
    invoke-virtual {v1}, LiR;->a()V

    .line 861
    :goto_0
    return-void

    .line 859
    :cond_5
    iget-object v0, p0, LiD;->a:Lfn;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lfn;->b(Landroid/view/View;)V

    goto :goto_0

    .line 838
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public k(Z)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 870
    if-eqz p1, :cond_0

    .line 871
    invoke-direct {p0}, LiD;->g()V

    .line 876
    :goto_0
    iget-object v3, p0, LiD;->a:Lkf;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_1
    invoke-interface {v3, v0}, Lkf;->d(I)V

    .line 877
    iget-object v0, p0, LiD;->a:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz p1, :cond_2

    :goto_2
    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(I)V

    .line 879
    return-void

    .line 873
    :cond_0
    invoke-direct {p0}, LiD;->h()V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 876
    goto :goto_1

    :cond_2
    move v2, v1

    .line 877
    goto :goto_2
.end method
