.class public LaaS;
.super Landroid/os/AsyncTask;
.source "UploadSharedItemActivityDelegate.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "LaaC;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Intent;

.field final synthetic a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

.field final synthetic a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Landroid/content/Intent;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 388
    iput-object p1, p0, LaaS;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iput-object p2, p0, LaaS;->a:Landroid/content/Intent;

    iput-object p3, p0, LaaS;->a:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "LaaC;",
            ">;"
        }
    .end annotation

    .prologue
    .line 391
    iget-object v0, p0, LaaS;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iget-object v1, p0, LaaS;->a:Landroid/content/Intent;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LaaC;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 396
    iget-object v0, p0, LaaS;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Ljava/util/List;)Ljava/util/List;

    .line 398
    iget-object v0, p0, LaaS;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 399
    iget-object v0, p0, LaaS;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No files requested to be uploaded: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LaaS;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Ljava/lang/String;)V

    .line 425
    :goto_0
    return-void

    .line 403
    :cond_0
    iget-object v0, p0, LaaS;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iget-object v1, p0, LaaS;->a:Landroid/content/Intent;

    const-string v2, "convertDocument"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Z)Z

    .line 404
    iget-object v0, p0, LaaS;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iget-object v1, p0, LaaS;->a:Landroid/content/Intent;

    const-string v2, "deleteAfterUpload"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Z)Z

    .line 405
    iget-object v0, p0, LaaS;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iget-object v1, p0, LaaS;->a:Landroid/content/Intent;

    const-string v2, "showConversionOption"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->c(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Z)Z

    .line 406
    iget-object v0, p0, LaaS;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iget-object v1, p0, LaaS;->a:Landroid/content/Intent;

    const-string v2, "forceFileCopy"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->d(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Z)Z

    .line 408
    iget-object v0, p0, LaaS;->a:Landroid/content/Intent;

    const-string v1, "collectionEntrySpec"

    .line 409
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 410
    iget-object v1, p0, LaaS;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iget-object v2, p0, LaaS;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)LaFO;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 411
    iget-object v0, p0, LaaS;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iget-object v1, p0, LaaS;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Z)V

    goto :goto_0

    .line 412
    :cond_1
    iget-object v0, p0, LaaS;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 413
    new-instance v0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;-><init>()V

    .line 414
    iget-object v1, p0, LaaS;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a()LM;

    move-result-object v1

    const-string v2, "UploadSharedItemDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;->a(LM;Ljava/lang/String;)V

    goto :goto_0

    .line 420
    :cond_2
    iget-object v0, p0, LaaS;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    .line 421
    invoke-virtual {v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxi;->upload_notification_failure_no_retry_title:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 422
    iget-object v1, p0, LaaS;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 423
    const-string v0, "UploadSharedItemActivityDelegate"

    const-string v1, "Fragment transactions unsafe, did not display upload dialog"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 388
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, LaaS;->a([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 388
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, LaaS;->a(Ljava/util/List;)V

    return-void
.end method
