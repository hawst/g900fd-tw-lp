.class public LBe;
.super Ljava/lang/Object;
.source "DocListEntryCommonViewHolder.java"


# instance fields
.field protected a:I

.field protected a:LaGA;

.field protected final a:Landroid/content/Context;

.field protected final a:Landroid/view/View;

.field protected a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field protected final a:Lcom/google/android/apps/docs/view/FixedSizeTextView;

.field protected final b:Landroid/view/View;

.field protected final b:Lcom/google/android/apps/docs/view/FixedSizeTextView;

.field protected final c:Landroid/view/View;

.field protected final c:Lcom/google/android/apps/docs/view/FixedSizeTextView;

.field protected final d:Landroid/view/View;

.field protected final e:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, LBe;->e:Landroid/view/View;

    .line 37
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LBe;->a:Landroid/content/Context;

    .line 38
    sget v0, Lxc;->title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/FixedSizeTextView;

    iput-object v0, p0, LBe;->a:Lcom/google/android/apps/docs/view/FixedSizeTextView;

    .line 39
    sget v0, Lxc;->group_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/FixedSizeTextView;

    iput-object v0, p0, LBe;->b:Lcom/google/android/apps/docs/view/FixedSizeTextView;

    .line 40
    sget v0, Lxc;->group_order:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/FixedSizeTextView;

    iput-object v0, p0, LBe;->c:Lcom/google/android/apps/docs/view/FixedSizeTextView;

    .line 41
    sget v0, Lxc;->more_actions_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LBe;->a:Landroid/view/View;

    .line 43
    sget v0, Lxc;->doc_entry_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LBe;->c:Landroid/view/View;

    .line 44
    sget v0, Lxc;->doc_entry_root:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LBe;->d:Landroid/view/View;

    .line 45
    iget-object v0, p0, LBe;->a:Landroid/view/View;

    iget-object v1, p0, LBe;->d:Landroid/view/View;

    invoke-static {v0, v1}, LbmF;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmF;

    move-result-object v0

    iput-object v0, p0, LBe;->a:LbmF;

    .line 47
    sget v0, Lxc;->details_triangle:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LBe;->b:Landroid/view/View;

    .line 48
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, LBe;->a:I

    return v0
.end method

.method public a()LaGu;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-static {}, LamV;->a()V

    .line 62
    iget-object v1, p0, LBe;->a:LaGA;

    if-eqz v1, :cond_0

    iget-object v1, p0, LBe;->a:LaGA;

    invoke-interface {v1}, LaGA;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 68
    :cond_0
    :goto_0
    return-object v0

    .line 65
    :cond_1
    iget-object v1, p0, LBe;->a:LaGA;

    iget v2, p0, LBe;->a:I

    invoke-interface {v1, v2}, LaGA;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    iget-object v0, p0, LBe;->a:LaGA;

    invoke-interface {v0}, LaGA;->a()LaGu;

    move-result-object v0

    goto :goto_0
.end method

.method public a(LaGA;I)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, LBe;->a:LaGA;

    .line 56
    iput p2, p0, LBe;->a:I

    .line 57
    return-void
.end method
