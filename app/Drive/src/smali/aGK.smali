.class public LaGK;
.super LaGm;
.source "Manifest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGm",
        "<",
        "LaFa;",
        "LaEz;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LaGs;

.field private a:Ljava/lang/Long;

.field private final a:Ljava/lang/String;

.field private final a:Ljava/util/Date;

.field private a:Z

.field private final b:Ljava/lang/Long;

.field private final b:Ljava/lang/String;

.field private final b:Z

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LaEz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LaGs;Ljava/lang/Long;Ljava/util/Date;ZLjava/lang/Long;Z)V
    .locals 2

    .prologue
    .line 60
    invoke-static {}, LaFa;->a()LaFa;

    move-result-object v0

    sget-object v1, LaEG;->i:LaEG;

    invoke-virtual {v1}, LaEG;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, LaGm;-><init>(LaFm;LaFy;Landroid/net/Uri;)V

    .line 61
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LaGK;->a:Ljava/lang/String;

    .line 62
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LaGK;->b:Ljava/lang/String;

    .line 63
    iput-object p4, p0, LaGK;->c:Ljava/lang/String;

    .line 64
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGs;

    iput-object v0, p0, LaGK;->a:LaGs;

    .line 65
    invoke-static {p6}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iput-object v0, p0, LaGK;->a:Ljava/lang/Long;

    .line 66
    invoke-static {p7}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    iput-object v0, p0, LaGK;->a:Ljava/util/Date;

    .line 67
    iput-boolean p8, p0, LaGK;->a:Z

    .line 68
    iput-object p9, p0, LaGK;->b:Ljava/lang/Long;

    .line 69
    iput-boolean p10, p0, LaGK;->b:Z

    .line 70
    return-void
.end method

.method public static a(LaEz;Landroid/database/Cursor;)LaGK;
    .locals 11

    .prologue
    .line 150
    sget-object v0, LaFb;->a:LaFb;

    invoke-virtual {v0}, LaFb;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    .line 151
    sget-object v0, LaFb;->b:LaFb;

    invoke-virtual {v0}, LaFb;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    .line 152
    sget-object v0, LaFb;->c:LaFb;

    invoke-virtual {v0}, LaFb;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    .line 153
    new-instance v5, LaGs;

    sget-object v0, LaFb;->i:LaFb;

    invoke-virtual {v0}, LaFb;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, LaGs;-><init>(Ljava/lang/String;)V

    .line 154
    sget-object v0, LaFb;->d:LaFb;

    invoke-virtual {v0}, LaFb;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v6

    .line 155
    new-instance v7, Ljava/util/Date;

    sget-object v0, LaFb;->e:LaFb;

    invoke-virtual {v0}, LaFb;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {v7, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 156
    sget-object v0, LaFb;->f:LaFb;

    invoke-virtual {v0}, LaFb;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Z

    move-result v8

    .line 157
    sget-object v0, LaFb;->g:LaFb;

    invoke-virtual {v0}, LaFb;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v9

    .line 158
    sget-object v0, LaFb;->h:LaFb;

    invoke-virtual {v0}, LaFb;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Z

    move-result v10

    .line 159
    new-instance v0, LaGK;

    move-object v1, p0

    invoke-direct/range {v0 .. v10}, LaGK;-><init>(LaEz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LaGs;Ljava/lang/Long;Ljava/util/Date;ZLjava/lang/Long;Z)V

    .line 169
    invoke-static {}, LaFa;->a()LaFa;

    move-result-object v1

    invoke-virtual {v1}, LaFa;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, LaFr;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LaGK;->c(J)V

    .line 170
    return-object v0
.end method


# virtual methods
.method public a()LaGs;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, LaGK;->a:LaGs;

    return-object v0
.end method

.method public a()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, LaGK;->a:Ljava/lang/Long;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, LaGK;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 109
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LaGK;->a:Ljava/lang/Long;

    .line 110
    return-void
.end method

.method protected a(Landroid/content/ContentValues;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 118
    sget-object v0, LaFb;->a:LaFb;

    invoke-virtual {v0}, LaFb;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LaGK;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    sget-object v0, LaFb;->b:LaFb;

    invoke-virtual {v0}, LaFb;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LaGK;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    sget-object v0, LaFb;->c:LaFb;

    invoke-virtual {v0}, LaFb;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LaGK;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    sget-object v0, LaFb;->i:LaFb;

    invoke-virtual {v0}, LaFb;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LaGK;->a:LaGs;

    invoke-virtual {v3}, LaGs;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    sget-object v0, LaFb;->d:LaFb;

    invoke-virtual {v0}, LaFb;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LaGK;->a:Ljava/lang/Long;

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 123
    sget-object v0, LaFb;->e:LaFb;

    invoke-virtual {v0}, LaFb;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LaGK;->a:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 124
    sget-object v0, LaFb;->f:LaFb;

    invoke-virtual {v0}, LaFb;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v3

    iget-boolean v0, p0, LaGK;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 125
    sget-object v0, LaFb;->g:LaFb;

    invoke-virtual {v0}, LaFb;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LaGK;->b:Ljava/lang/Long;

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 126
    sget-object v0, LaFb;->h:LaFb;

    invoke-virtual {v0}, LaFb;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-boolean v3, p0, LaGK;->b:Z

    if-eqz v3, :cond_1

    :goto_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 127
    return-void

    :cond_0
    move v0, v2

    .line 124
    goto :goto_0

    :cond_1
    move v1, v2

    .line 126
    goto :goto_1
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 101
    iput-boolean p1, p0, LaGK;->a:Z

    .line 102
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, LaGK;->a:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 131
    const-string v0, "Manifest[appName=%s, jobsetName=%s, eTag=%s, locale=%s, appCacheSqlId=%s, refreshTime=%s, isCacheObsolete=%s, accountId=%s, isFastTrack=%s, sqlId=%s]"

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LaGK;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LaGK;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, LaGK;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, LaGK;->a:LaGs;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, LaGK;->a:Ljava/lang/Long;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, LaGK;->a:Ljava/util/Date;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-boolean v3, p0, LaGK;->a:Z

    .line 140
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-object v3, p0, LaGK;->b:Ljava/lang/Long;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    iget-boolean v3, p0, LaGK;->b:Z

    .line 142
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x9

    .line 143
    invoke-virtual {p0}, LaGK;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 131
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
