.class LvX;
.super LaGN;
.source "EntryInformationImpl.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGN",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

.field private final a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field final synthetic a:LvW;


# direct methods
.method public constructor <init>(LvW;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1

    .prologue
    .line 61
    iput-object p1, p0, LvX;->a:LvW;

    invoke-direct {p0}, LaGN;-><init>()V

    .line 62
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    iput-object v0, p0, LvX;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    .line 63
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, LvX;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 64
    return-void
.end method


# virtual methods
.method public a(LaGM;)Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 68
    iget-object v0, p0, LvX;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    invoke-interface {v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()LvN;

    move-result-object v0

    iget-object v1, p0, LvX;->a:LvW;

    .line 69
    invoke-static {v1}, LvW;->a(LvW;)LvL;

    move-result-object v1

    iget-object v2, p0, LvX;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v1, v2}, LvL;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v1

    invoke-virtual {v0, v1}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    move-result-object v0

    .line 70
    invoke-virtual {v0}, LvN;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    .line 71
    new-array v1, v3, [Ljava/lang/String;

    .line 72
    const/4 v2, 0x0

    invoke-interface {p1, v0, v2, v1}, LaGM;->a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;LIK;[Ljava/lang/String;)LaGA;

    move-result-object v1

    .line 73
    if-nez v1, :cond_0

    .line 74
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 80
    :goto_0
    return-object v0

    .line 77
    :cond_0
    :try_start_0
    invoke-interface {v1}, LaGA;->l()Z

    move-result v0

    .line 78
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 80
    invoke-interface {v1}, LaGA;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, LaGA;->a()V

    throw v0
.end method

.method public bridge synthetic a(LaGM;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0, p1}, LvX;->a(LaGM;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 86
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, LvX;->a:LvW;

    invoke-virtual {v0}, LvW;->a()V

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, LvX;->a:LvW;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LvW;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 57
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, LvX;->a(Ljava/lang/Boolean;)V

    return-void
.end method
