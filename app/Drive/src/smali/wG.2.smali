.class public final LwG;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lwy;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LwJ;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LwH;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lww;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 41
    iput-object p1, p0, LwG;->a:LbrA;

    .line 42
    const-class v0, Lwy;

    invoke-static {v0, v1}, LwG;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LwG;->a:Lbsk;

    .line 45
    const-class v0, LwJ;

    invoke-static {v0, v1}, LwG;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LwG;->b:Lbsk;

    .line 48
    const-class v0, LwH;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LwG;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LwG;->c:Lbsk;

    .line 51
    const-class v0, Lww;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LwG;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LwG;->d:Lbsk;

    .line 54
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 81
    packed-switch p1, :pswitch_data_0

    .line 133
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :pswitch_0
    new-instance v3, Lwy;

    iget-object v0, p0, LwG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->h:Lbsk;

    .line 86
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LwG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->h:Lbsk;

    .line 84
    invoke-static {v0, v1}, LwG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTh;

    iget-object v1, p0, LwG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->K:Lbsk;

    .line 92
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LwG;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LalC;

    iget-object v2, v2, LalC;->K:Lbsk;

    .line 90
    invoke-static {v1, v2}, LwG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lalo;

    iget-object v2, p0, LwG;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->t:Lbsk;

    .line 98
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LwG;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lajo;

    iget-object v4, v4, Lajo;->t:Lbsk;

    .line 96
    invoke-static {v2, v4}, LwG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Laja;

    invoke-direct {v3, v0, v1, v2}, Lwy;-><init>(LTh;Lalo;Laja;)V

    move-object v0, v3

    .line 131
    :goto_0
    return-object v0

    .line 105
    :pswitch_1
    new-instance v4, LwJ;

    iget-object v0, p0, LwG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->j:Lbsk;

    .line 108
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LwG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->j:Lbsk;

    .line 106
    invoke-static {v0, v1}, LwG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGg;

    iget-object v1, p0, LwG;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LwG;

    iget-object v1, v1, LwG;->d:Lbsk;

    .line 114
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LwG;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LwG;

    iget-object v2, v2, LwG;->d:Lbsk;

    .line 112
    invoke-static {v1, v2}, LwG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lww;

    iget-object v2, p0, LwG;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LadM;

    iget-object v2, v2, LadM;->m:Lbsk;

    .line 120
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LwG;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LadM;

    iget-object v3, v3, LadM;->m:Lbsk;

    .line 118
    invoke-static {v2, v3}, LwG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ladi;

    iget-object v3, p0, LwG;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->L:Lbsk;

    .line 126
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, LwG;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LalC;

    iget-object v5, v5, LalC;->L:Lbsk;

    .line 124
    invoke-static {v3, v5}, LwG;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaKR;

    invoke-direct {v4, v0, v1, v2, v3}, LwJ;-><init>(LaGg;Lww;Ladi;LaKR;)V

    move-object v0, v4

    .line 131
    goto :goto_0

    .line 81
    :pswitch_data_0
    .packed-switch 0x5a
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 148
    sparse-switch p2, :sswitch_data_0

    .line 168
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 150
    :sswitch_0
    check-cast p1, LwA;

    .line 152
    iget-object v0, p0, LwG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LwG;

    iget-object v0, v0, LwG;->b:Lbsk;

    .line 155
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LwJ;

    .line 152
    invoke-virtual {p1, v0}, LwA;->provideManifestManager(LwJ;)LwH;

    move-result-object v0

    .line 161
    :goto_0
    return-object v0

    .line 159
    :sswitch_1
    check-cast p1, Lwz;

    .line 161
    iget-object v0, p0, LwG;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LwG;

    iget-object v0, v0, LwG;->a:Lbsk;

    .line 164
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwy;

    .line 161
    invoke-virtual {p1, v0}, Lwz;->provideAppCacheFetcher(Lwy;)Lww;

    move-result-object v0

    goto :goto_0

    .line 148
    :sswitch_data_0
    .sparse-switch
        0x59 -> :sswitch_0
        0x5d -> :sswitch_1
    .end sparse-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 61
    const-class v0, Lwy;

    iget-object v1, p0, LwG;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LwG;->a(Ljava/lang/Class;Lbsk;)V

    .line 62
    const-class v0, LwJ;

    iget-object v1, p0, LwG;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LwG;->a(Ljava/lang/Class;Lbsk;)V

    .line 63
    const-class v0, LwH;

    iget-object v1, p0, LwG;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LwG;->a(Ljava/lang/Class;Lbsk;)V

    .line 64
    const-class v0, Lww;

    iget-object v1, p0, LwG;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LwG;->a(Ljava/lang/Class;Lbsk;)V

    .line 65
    iget-object v0, p0, LwG;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x5b

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 67
    iget-object v0, p0, LwG;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x5a

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 69
    iget-object v0, p0, LwG;->c:Lbsk;

    const-class v1, LwA;

    const/16 v2, 0x59

    invoke-virtual {p0, v1, v2}, LwG;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 71
    iget-object v0, p0, LwG;->d:Lbsk;

    const-class v1, Lwz;

    const/16 v2, 0x5d

    invoke-virtual {p0, v1, v2}, LwG;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 73
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 140
    .line 142
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method
