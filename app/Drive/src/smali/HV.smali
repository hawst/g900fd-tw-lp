.class public LHV;
.super LIq;
.source "DateGrouper.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LIq",
        "<",
        "LIP;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LHX;

.field private final a:LIQ;


# direct methods
.method constructor <init>(LHX;LIg;J)V
    .locals 3

    .prologue
    .line 98
    invoke-virtual {p1}, LHX;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, LIq;-><init>(Ljava/lang/String;LIg;)V

    .line 99
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 100
    invoke-virtual {v0, p3, p4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 101
    new-instance v1, LIQ;

    invoke-direct {v1, v0}, LIQ;-><init>(Ljava/util/Calendar;)V

    iput-object v1, p0, LHV;->a:LIQ;

    .line 102
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHX;

    iput-object v0, p0, LHV;->a:LHX;

    .line 103
    return-void
.end method


# virtual methods
.method protected a(LCv;)LIP;
    .locals 4

    .prologue
    .line 131
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    invoke-virtual {p0, p1}, LHV;->a(LCv;)Ljava/lang/Long;

    move-result-object v0

    .line 133
    if-nez v0, :cond_0

    .line 134
    sget-object v0, LIP;->g:LIP;

    .line 136
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LHV;->a:LIQ;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LIQ;->a(J)LIP;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic a(LCv;)LIy;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0, p1}, LHV;->a(LCv;)LIP;

    move-result-object v0

    return-object v0
.end method

.method public a()LaFr;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, LHV;->a:LHX;

    invoke-virtual {v0}, LHX;->a()LaFr;

    move-result-object v0

    return-object v0
.end method

.method public a(LCv;)Ljava/lang/Long;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, LHV;->a:LHX;

    invoke-virtual {v0, p1}, LHX;->a(LCv;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected a(LCv;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 141
    invoke-virtual {p0, p1}, LHV;->a(LCv;)Ljava/lang/Long;

    move-result-object v0

    .line 143
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    neg-long v0, v0

    .line 144
    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0

    .line 143
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method protected a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, LHV;->a:LHX;

    invoke-virtual {v0}, LHX;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a()Z
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, LHV;->a:LHX;

    sget-object v1, LHX;->f:LHX;

    invoke-virtual {v0, v1}, LHX;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    const/4 v0, 0x0

    .line 125
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
