.class public final LbiV;
.super Ljava/lang/Object;
.source "Predicates.java"


# static fields
.field private static final a:LbiI;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 330
    const-string v0, ","

    invoke-static {v0}, LbiI;->a(Ljava/lang/String;)LbiI;

    move-result-object v0

    sput-object v0, LbiV;->a:LbiI;

    return-void
.end method

.method static synthetic a()LbiI;
    .locals 1

    .prologue
    .line 48
    sget-object v0, LbiV;->a:LbiI;

    return-object v0
.end method

.method public static a()LbiU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "LbiU",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 76
    sget-object v0, Lbjb;->c:Lbjb;

    invoke-virtual {v0}, Lbjb;->a()LbiU;

    move-result-object v0

    return-object v0
.end method

.method public static a(LbiU;)LbiU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbiU",
            "<TT;>;)",
            "LbiU",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 93
    new-instance v0, Lbja;

    invoke-direct {v0, p0}, Lbja;-><init>(LbiU;)V

    return-object v0
.end method

.method public static a(LbiU;LbiU;)LbiU;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbiU",
            "<-TT;>;",
            "LbiU",
            "<-TT;>;)",
            "LbiU",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 129
    new-instance v2, LbiX;

    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbiU;

    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LbiU;

    invoke-static {v0, v1}, LbiV;->a(LbiU;LbiU;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {v2, v0, v1}, LbiX;-><init>(Ljava/util/List;LbiW;)V

    return-object v2
.end method

.method public static a(Ljava/lang/Object;)LbiU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "LbiU",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 173
    if-nez p0, :cond_0

    invoke-static {}, LbiV;->a()LbiU;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LbiZ;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LbiZ;-><init>(Ljava/lang/Object;LbiW;)V

    goto :goto_0
.end method

.method public static a(Ljava/util/Collection;)LbiU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<+TT;>;)",
            "LbiU",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 223
    new-instance v0, LbiY;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LbiY;-><init>(Ljava/util/Collection;LbiW;)V

    return-object v0
.end method

.method private static a(LbiU;LbiU;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbiU",
            "<-TT;>;",
            "LbiU",
            "<-TT;>;)",
            "Ljava/util/List",
            "<",
            "LbiU",
            "<-TT;>;>;"
        }
    .end annotation

    .prologue
    .line 647
    const/4 v0, 0x2

    new-array v0, v0, [LbiU;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
