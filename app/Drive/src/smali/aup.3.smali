.class Laup;
.super Lauq;
.source "CloudPrintDialog.java"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 227
    invoke-direct {p0, p1, p5}, Lauq;-><init>(Landroid/app/Activity;Landroid/accounts/Account;)V

    .line 228
    iput-object p2, p0, Laup;->a:Ljava/lang/String;

    .line 229
    iput-object p3, p0, Laup;->c:Ljava/lang/String;

    .line 230
    iput-object p4, p0, Laup;->b:Ljava/lang/String;

    .line 231
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 240
    const-string v0, "javascript:printDialog.setPrintDocument(printDialog.createPrintDocument(\'%s\', \'%s\', \'%s\', \'%s\'))"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    aput-object p3, v1, v2

    const/4 v2, 0x3

    const-string v3, "base64"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 235
    iget-object v0, p0, Laup;->a:Ljava/lang/String;

    iget-object v1, p0, Laup;->c:Ljava/lang/String;

    iget-object v2, p0, Laup;->b:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Laup;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 236
    invoke-super {p0, p1, p2}, Lauq;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 237
    return-void
.end method
