.class public Lwh;
.super Ljava/lang/Object;
.source "ListViewStateImpl.java"

# interfaces
.implements Lwg;


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput v0, p0, Lwh;->a:I

    .line 26
    iput v0, p0, Lwh;->b:I

    .line 21
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lwh;->a:I

    return v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 35
    iput p1, p0, Lwh;->a:I

    .line 36
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 50
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 51
    const-string v0, "keyListViewPosition"

    invoke-virtual {p0}, Lwh;->a()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 52
    const-string v0, "keyListViewCheckItemPosition"

    invoke-virtual {p0}, Lwh;->b()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 53
    return-void

    .line 50
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lwh;->b:I

    return v0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 45
    iput p1, p0, Lwh;->b:I

    .line 46
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 57
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 58
    const-string v0, "keyListViewPosition"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lwh;->a(I)V

    .line 59
    const-string v0, "keyListViewCheckItemPosition"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lwh;->b(I)V

    .line 60
    return-void

    .line 57
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
