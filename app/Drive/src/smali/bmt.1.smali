.class abstract Lbmt;
.super LbmF;
.source "ImmutableAsList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LbmF",
        "<TE;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, LbmF;-><init>()V

    .line 62
    return-void
.end method


# virtual methods
.method abstract a()Lbmv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbmv",
            "<TE;>;"
        }
    .end annotation
.end method

.method a()Z
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lbmt;->a()Lbmv;

    move-result-object v0

    invoke-virtual {v0}, Lbmv;->a()Z

    move-result v0

    return v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lbmt;->a()Lbmv;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbmv;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0}, Lbmt;->a()Lbmv;

    move-result-object v0

    invoke-virtual {v0}, Lbmv;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lbmt;->a()Lbmv;

    move-result-object v0

    invoke-virtual {v0}, Lbmv;->size()I

    move-result v0

    return v0
.end method
