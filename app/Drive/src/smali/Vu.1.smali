.class public LVu;
.super Ljava/lang/Object;
.source "PinOperation.java"

# interfaces
.implements LVt;


# instance fields
.field private final a:LTT;

.field private final a:LUi;

.field private final a:LaGM;

.field private final a:LaHl;

.field private final a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private final a:LqK;

.field private final a:LtK;

.field private final a:Z

.field private b:Z


# direct methods
.method constructor <init>(LqK;LUi;LtK;LaGM;LTT;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V
    .locals 2

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, LaHl;

    const-string v1, "PinOperation"

    invoke-direct {v0, v1}, LaHl;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LVu;->a:LaHl;

    .line 86
    invoke-static {p6}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, LVu;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 87
    iput-boolean p7, p0, LVu;->a:Z

    .line 88
    iput-object p1, p0, LVu;->a:LqK;

    .line 89
    iput-object p2, p0, LVu;->a:LUi;

    .line 90
    iput-object p3, p0, LVu;->a:LtK;

    .line 91
    iput-object p4, p0, LVu;->a:LaGM;

    .line 92
    iput-object p5, p0, LVu;->a:LTT;

    .line 93
    return-void
.end method

.method private a(LaGo;)V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, LVu;->a:LTT;

    invoke-interface {v0, p1}, LTT;->b(LaGu;)Z

    .line 130
    return-void
.end method

.method private a(LaGo;Z)V
    .locals 4

    .prologue
    .line 113
    iget-object v0, p0, LVu;->a:LUi;

    invoke-interface {p1}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    iget-object v2, p0, LVu;->a:LaHl;

    invoke-interface {v0, v1, p2, v2}, LUi;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;ZLaHy;)V

    .line 115
    iget-object v0, p0, LVu;->a:LtK;

    sget-object v1, Lry;->m:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    invoke-direct {p0, p1}, LVu;->a(LaGo;)V

    .line 119
    :cond_0
    iget-object v1, p0, LVu;->a:LqK;

    const-string v2, "pinning"

    if-eqz p2, :cond_1

    const-string v0, "pin"

    .line 121
    :goto_0
    invoke-static {p1}, Lala;->a(LaGu;)Ljava/lang/String;

    move-result-object v3

    .line 119
    invoke-virtual {v1, v2, v0, v3}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    return-void

    .line 119
    :cond_1
    const-string v0, "unpin"

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 98
    :try_start_0
    iget-object v0, p0, LVu;->a:LaGM;

    iget-object v1, p0, LVu;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v1}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v0

    .line 99
    if-eqz v0, :cond_1

    .line 100
    invoke-interface {v0}, LaGo;->f()Z

    move-result v1

    iput-boolean v1, p0, LVu;->b:Z

    .line 101
    iget-boolean v1, p0, LVu;->b:Z

    iget-boolean v2, p0, LVu;->a:Z

    if-eq v1, v2, :cond_0

    .line 102
    iget-boolean v1, p0, LVu;->a:Z

    invoke-direct {p0, v0, v1}, LVu;->a(LaGo;Z)V

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    const-string v0, "PinOperation"

    const-string v1, "Failed to find document: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LVu;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catch LaGT; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 107
    :catch_0
    move-exception v0

    .line 108
    const-string v0, "PinOperation"

    const-string v1, "Failed to get document: %s"

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, LVu;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public b()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 134
    iget-boolean v0, p0, LVu;->b:Z

    iget-boolean v3, p0, LVu;->a:Z

    if-eq v0, v3, :cond_0

    .line 136
    :try_start_0
    iget-object v0, p0, LVu;->a:LaGM;

    iget-object v3, p0, LVu;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v3}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v3

    .line 137
    if-eqz v3, :cond_2

    .line 138
    invoke-interface {v3}, LaGo;->f()Z

    move-result v0

    .line 139
    iget-boolean v4, p0, LVu;->a:Z

    if-ne v0, v4, :cond_0

    .line 140
    iget-boolean v0, p0, LVu;->a:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-direct {p0, v3, v0}, LVu;->a(LaGo;Z)V

    .line 149
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 140
    goto :goto_0

    .line 143
    :cond_2
    const-string v0, "PinOperation"

    const-string v3, "Failed to find document: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LVu;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    aput-object v6, v4, v5

    invoke-static {v0, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catch LaGT; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 145
    :catch_0
    move-exception v0

    .line 146
    const-string v0, "PinOperation"

    const-string v3, "Failed to get document: %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, LVu;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1
.end method
