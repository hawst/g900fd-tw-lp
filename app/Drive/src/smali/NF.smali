.class public final LNF;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LOf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 38
    iput-object p1, p0, LNF;->a:LbrA;

    .line 39
    const-class v0, LOf;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LNF;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LNF;->a:Lbsk;

    .line 42
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 176
    packed-switch p1, :pswitch_data_0

    .line 194
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 178
    :pswitch_0
    new-instance v2, LOf;

    iget-object v0, p0, LNF;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->h:Lbsk;

    .line 181
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LNF;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->h:Lbsk;

    .line 179
    invoke-static {v0, v1}, LNF;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTh;

    iget-object v1, p0, LNF;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->K:Lbsk;

    .line 187
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LNF;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->K:Lbsk;

    .line 185
    invoke-static {v1, v3}, LNF;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lalo;

    invoke-direct {v2, v0, v1}, LOf;-><init>(LTh;Lalo;)V

    .line 192
    return-object v2

    .line 176
    :pswitch_data_0
    .packed-switch 0x63
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 221
    .line 223
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 159
    const-class v0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x12

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LNF;->a(LbuP;LbuB;)V

    .line 162
    const-class v0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x13

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LNF;->a(LbuP;LbuB;)V

    .line 165
    const-class v0, LOf;

    iget-object v1, p0, LNF;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LNF;->a(Ljava/lang/Class;Lbsk;)V

    .line 166
    iget-object v0, p0, LNF;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x63

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 168
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 201
    packed-switch p1, :pswitch_data_0

    .line 215
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 203
    :pswitch_0
    check-cast p2, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;

    .line 205
    iget-object v0, p0, LNF;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LNF;

    .line 206
    invoke-virtual {v0, p2}, LNF;->a(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;)V

    .line 217
    :goto_0
    return-void

    .line 209
    :pswitch_1
    check-cast p2, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    .line 211
    iget-object v0, p0, LNF;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LNF;

    .line 212
    invoke-virtual {v0, p2}, LNF;->a(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;)V

    goto :goto_0

    .line 201
    :pswitch_data_0
    .packed-switch 0x12
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;)V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, LNF;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 104
    invoke-virtual {v0, p1}, LtQ;->a(Lrm;)V

    .line 105
    iget-object v0, p0, LNF;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->c:Lbsk;

    .line 108
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LNF;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->c:Lbsk;

    .line 106
    invoke-static {v0, v1}, LNF;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTj;

    iput-object v0, p1, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:LTj;

    .line 112
    iget-object v0, p0, LNF;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LPt;

    iget-object v0, v0, LPt;->d:Lbsk;

    .line 115
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LNF;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LPt;

    iget-object v1, v1, LPt;->d:Lbsk;

    .line 113
    invoke-static {v0, v1}, LNF;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPp;

    iput-object v0, p1, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:LPp;

    .line 119
    iget-object v0, p0, LNF;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 122
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LNF;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 120
    invoke-static {v0, v1}, LNF;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p1, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:LaGM;

    .line 126
    iget-object v0, p0, LNF;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LGu;

    iget-object v0, v0, LGu;->w:Lbsk;

    .line 129
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LNF;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LGu;

    iget-object v1, v1, LGu;->w:Lbsk;

    .line 127
    invoke-static {v0, v1}, LNF;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LGa;

    iput-object v0, p1, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:LGa;

    .line 133
    iget-object v0, p0, LNF;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 136
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LNF;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 134
    invoke-static {v0, v1}, LNF;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p1, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:LtK;

    .line 140
    iget-object v0, p0, LNF;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->F:Lbsk;

    .line 143
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LNF;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->F:Lbsk;

    .line 141
    invoke-static {v0, v1}, LNF;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:Laja;

    .line 147
    iget-object v0, p0, LNF;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->f:Lbsk;

    .line 150
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LNF;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->f:Lbsk;

    .line 148
    invoke-static {v0, v1}, LNF;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGR;

    iput-object v0, p1, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:LaGR;

    .line 154
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;)V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, LNF;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 49
    invoke-virtual {v0, p1}, LtQ;->a(Lrm;)V

    .line 50
    iget-object v0, p0, LNF;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LOg;

    iget-object v0, v0, LOg;->a:Lbsk;

    .line 53
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LNF;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LOg;

    iget-object v1, v1, LOg;->a:Lbsk;

    .line 51
    invoke-static {v0, v1}, LNF;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LOn;

    iput-object v0, p1, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:LOn;

    .line 57
    iget-object v0, p0, LNF;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->F:Lbsk;

    .line 60
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LNF;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->F:Lbsk;

    .line 58
    invoke-static {v0, v1}, LNF;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Laja;

    .line 64
    iget-object v0, p0, LNF;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 67
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LNF;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 65
    invoke-static {v0, v1}, LNF;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p1, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:LtK;

    .line 71
    iget-object v0, p0, LNF;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LagN;

    iget-object v0, v0, LagN;->y:Lbsk;

    .line 74
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LNF;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LagN;

    iget-object v1, v1, LagN;->y:Lbsk;

    .line 72
    invoke-static {v0, v1}, LNF;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahr;

    iput-object v0, p1, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Lahr;

    .line 78
    iget-object v0, p0, LNF;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LPt;

    iget-object v0, v0, LPt;->d:Lbsk;

    .line 81
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LNF;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LPt;

    iget-object v1, v1, LPt;->d:Lbsk;

    .line 79
    invoke-static {v0, v1}, LNF;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPp;

    iput-object v0, p1, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:LPp;

    .line 85
    iget-object v0, p0, LNF;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 88
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LNF;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 86
    invoke-static {v0, v1}, LNF;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p1, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:LaGM;

    .line 92
    iget-object v0, p0, LNF;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LNF;

    iget-object v0, v0, LNF;->a:Lbsk;

    .line 95
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LNF;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LNF;

    iget-object v1, v1, LNF;->a:Lbsk;

    .line 93
    invoke-static {v0, v1}, LNF;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LOf;

    iput-object v0, p1, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:LOf;

    .line 99
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 172
    return-void
.end method
