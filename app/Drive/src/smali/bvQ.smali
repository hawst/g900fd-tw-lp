.class LbvQ;
.super Lbwr;
.source "Indexer.java"

# interfaces
.implements Lbwp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbwr",
        "<",
        "Ljava/lang/Object;",
        "LbvS;",
        ">;",
        "Lbwp",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Object;


# instance fields
.field final a:Lbuu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 152
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LbvQ;->a:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Lbuu;)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Lbwr;-><init>()V

    .line 97
    iput-object p1, p0, LbvQ;->a:Lbuu;

    .line 98
    return-void
.end method

.method private b(Lbup;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbup",
            "<*>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 105
    invoke-interface {p1, p0}, Lbup;->a(Lbwp;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(LbwA;)LbvS;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbwA",
            "<+",
            "Ljava/lang/Object;",
            ">;)",
            "LbvS;"
        }
    .end annotation

    .prologue
    .line 139
    new-instance v0, LbvS;

    sget-object v1, LbvR;->b:LbvR;

    invoke-direct {p0, p1}, LbvQ;->b(Lbup;)Ljava/lang/Object;

    move-result-object v2

    .line 140
    invoke-interface {p1}, LbwA;->a()Lbxw;

    move-result-object v3

    invoke-direct {v0, p1, v1, v2, v3}, LbvS;-><init>(Lbup;LbvR;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method public a(LbuH;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 163
    return-object p1
.end method

.method public bridge synthetic a(LbwA;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0, p1}, LbvQ;->a(LbwA;)LbvS;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 167
    return-object p1
.end method
