.class LbmB;
.super LbmY;
.source "ImmutableEnumMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LbmY",
        "<TK;>;"
    }
.end annotation


# instance fields
.field final synthetic a:LbmA;


# direct methods
.method constructor <init>(LbmA;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, LbmB;->a:LbmA;

    invoke-direct {p0}, LbmY;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lbqv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbqv",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, LbmB;->a:LbmA;

    invoke-static {v0}, LbmA;->a(LbmA;)Ljava/util/EnumMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lbnr;->a(Ljava/util/Iterator;)Lbqv;

    move-result-object v0

    return-object v0
.end method

.method a()Z
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x1

    return v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, LbmB;->a:LbmA;

    invoke-static {v0}, LbmA;->a(LbmA;)Ljava/util/EnumMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, LbmB;->a()Lbqv;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, LbmB;->a:LbmA;

    invoke-virtual {v0}, LbmA;->size()I

    move-result v0

    return v0
.end method
