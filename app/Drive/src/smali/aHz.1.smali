.class public LaHz;
.super Ljava/lang/Object;
.source "OperationUtils.java"


# instance fields
.field private final a:LaGg;

.field private final a:LaHr;

.field private final a:LaKR;

.field private final a:Lald;

.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LtK;


# direct methods
.method constructor <init>(LtK;LaGg;LaHr;Lald;Laja;LaKR;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LtK;",
            "LaGg;",
            "LaHr;",
            "Lald;",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LaKR;",
            ")V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, LaHz;->a:LtK;

    .line 56
    iput-object p2, p0, LaHz;->a:LaGg;

    .line 57
    iput-object p3, p0, LaHz;->a:LaHr;

    .line 58
    iput-object p4, p0, LaHz;->a:Lald;

    .line 59
    iput-object p5, p0, LaHz;->a:Lbxw;

    .line 60
    iput-object p6, p0, LaHz;->a:LaKR;

    .line 61
    return-void
.end method

.method public static a(LaFM;LaGg;Lorg/json/JSONObject;)LaHo;
    .locals 4

    .prologue
    .line 137
    const-string v0, "operationName"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 139
    invoke-interface {p1}, LaGg;->a()V

    .line 141
    :try_start_0
    const-string v0, "resourceId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    const-string v0, "resourceId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 143
    invoke-interface {p1, p0, v0}, LaGg;->a(LaFM;Ljava/lang/String;)LaGd;

    move-result-object v0

    .line 149
    :goto_0
    invoke-interface {p1}, LaGg;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    invoke-interface {p1}, LaGg;->b()V

    .line 153
    if-nez v0, :cond_1

    .line 154
    new-instance v0, LaGC;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No entry for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LaGC;-><init>(Ljava/lang/String;)V

    throw v0

    .line 145
    :cond_0
    :try_start_1
    const-string v0, "entrySqlId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 146
    invoke-virtual {p0}, LaFM;->a()LaFO;

    move-result-object v0

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;->a(LaFO;J)Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v0

    .line 147
    invoke-interface {p1, v0}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 151
    :catchall_0
    move-exception v0

    invoke-interface {p1}, LaGg;->b()V

    throw v0

    .line 156
    :cond_1
    invoke-virtual {v0}, LaGd;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v0

    .line 158
    const-string v2, "starred"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 159
    invoke-static {p1, v0, p2}, LaHF;->a(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Lorg/json/JSONObject;)LaHF;

    move-result-object v0

    .line 175
    :goto_1
    return-object v0

    .line 160
    :cond_2
    const-string v2, "trash"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 161
    invoke-static {p1, v0, p2}, LaHG;->a(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Lorg/json/JSONObject;)LaHG;

    move-result-object v0

    goto :goto_1

    .line 162
    :cond_3
    const-string v2, "untrash"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 163
    invoke-static {p1, v0, p2}, LaHH;->a(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Lorg/json/JSONObject;)LaHH;

    move-result-object v0

    goto :goto_1

    .line 164
    :cond_4
    const-string v2, "rename"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 165
    invoke-static {p1, v0, p2}, LaHn;->a(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Lorg/json/JSONObject;)LaHn;

    move-result-object v0

    goto :goto_1

    .line 166
    :cond_5
    const-string v2, "viewed"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 167
    invoke-static {p1, v0, p2}, LaHk;->a(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Lorg/json/JSONObject;)LaHk;

    move-result-object v0

    goto :goto_1

    .line 168
    :cond_6
    const-string v2, "moveOperation"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 169
    invoke-static {p1, v0, p2}, LaHB;->a(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Lorg/json/JSONObject;)LaHB;

    move-result-object v0

    goto :goto_1

    .line 170
    :cond_7
    const-string v2, "opMayFail"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 171
    invoke-static {p1, v0, p2}, LaHm;->a(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Lorg/json/JSONObject;)LaHm;

    move-result-object v0

    goto :goto_1

    .line 172
    :cond_8
    const-string v2, "unsubscribe"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 173
    invoke-static {p1, v0, p2}, LaHC;->a(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Lorg/json/JSONObject;)LaHC;

    move-result-object v0

    goto :goto_1

    .line 175
    :cond_9
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a(LaHr;LaHo;LaFM;LaHy;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 116
    invoke-interface {p1, p2}, LaHr;->a(LaHo;)V

    .line 118
    invoke-interface {p1}, LaHr;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    const-string v0, "OperationUtils"

    const-string v2, "Skipping change %s as local saving failed."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-interface {p2}, LaHo;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v0, v2, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 120
    const/4 v0, 0x2

    .line 131
    :goto_0
    return v0

    .line 123
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p4, v1, v0}, LaHy;->a(ILjava/lang/Throwable;)V

    .line 125
    iget-object v0, p0, LaHz;->a:LaKR;

    invoke-interface {v0}, LaKR;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, p0, LaHz;->a:Lbxw;

    .line 127
    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v2, p0, LaHz;->a:Lald;

    invoke-static {v0, v2}, LUo;->a(Landroid/content/Context;Lald;)LaHy;

    move-result-object v0

    .line 129
    invoke-interface {p1, p3, v0}, LaHr;->a(LaFM;LaHy;)V

    :cond_1
    move v0, v1

    .line 131
    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 70
    iget-object v0, p0, LaHz;->a:LaGg;

    invoke-interface {v0, p1}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;

    move-result-object v3

    .line 71
    if-eqz v3, :cond_1

    .line 72
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 73
    invoke-virtual {v3}, LaGd;->a()LaFM;

    move-result-object v4

    .line 74
    iget-object v5, p0, LaHz;->a:LaGg;

    invoke-interface {v5, v4}, LaGg;->a(LaFM;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v4

    .line 75
    invoke-virtual {p1, v4}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 76
    if-nez v4, :cond_0

    iget-object v4, p0, LaHz;->a:LtK;

    sget-object v5, Lry;->ar:Lry;

    invoke-interface {v4, v5}, LtK;->a(LtJ;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 77
    iget-object v4, p0, LaHz;->a:LaGg;

    invoke-static {v4, v3, v0}, LaHk;->a(LaGg;LaGd;Ljava/util/Date;)LaHk;

    move-result-object v4

    .line 78
    iget-object v0, p0, LaHz;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 79
    iget-object v5, p0, LaHz;->a:Lald;

    .line 80
    invoke-static {v0, v5}, LUo;->a(Landroid/content/Context;Lald;)LaHy;

    move-result-object v0

    .line 81
    iget-object v5, p0, LaHz;->a:LaHr;

    invoke-virtual {v3}, LaGd;->a()LaFM;

    move-result-object v3

    invoke-virtual {p0, v5, v4, v3, v0}, LaHz;->a(LaHr;LaHo;LaFM;LaHy;)I

    move-result v0

    .line 83
    if-eqz v0, :cond_1

    .line 84
    const-string v3, "OperationUtils"

    const-string v4, "Could not mark Entry as viewed; result=%s"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v1

    invoke-static {v3, v4, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v1

    .line 103
    :goto_0
    return v0

    .line 90
    :cond_0
    iget-object v1, p0, LaHz;->a:LaGg;

    invoke-interface {v1}, LaGg;->a()V

    .line 93
    :try_start_0
    iget-object v1, p0, LaHz;->a:LaGg;

    invoke-interface {v1, p1}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;

    move-result-object v1

    .line 94
    invoke-virtual {v1}, LaGd;->a()LaGe;

    move-result-object v1

    .line 95
    invoke-virtual {v1, v0}, LaGe;->c(Ljava/util/Date;)LaGe;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, LaGe;->e()V

    .line 97
    iget-object v0, p0, LaHz;->a:LaGg;

    invoke-interface {v0}, LaGg;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    iget-object v0, p0, LaHz;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V

    :cond_1
    move v0, v2

    .line 103
    goto :goto_0

    .line 99
    :catchall_0
    move-exception v0

    iget-object v1, p0, LaHz;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    throw v0
.end method
