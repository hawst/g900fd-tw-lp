.class public LafI;
.super LafC;
.source "BinaryFileSyncable.java"


# instance fields
.field private final a:LMU;

.field private final a:LaGF;

.field private final a:LaGg;

.field private final a:LagG;

.field private final a:LagZ;

.field private final a:Lago;


# direct methods
.method constructor <init>(LahJ;LagH;LaGg;LagG;Lago;LMU;LagZ;LaGF;)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, LafC;-><init>(LahJ;LagH;)V

    .line 80
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGg;

    iput-object v0, p0, LafI;->a:LaGg;

    .line 81
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LagG;

    iput-object v0, p0, LafI;->a:LagG;

    .line 82
    iput-object p5, p0, LafI;->a:Lago;

    .line 83
    iput-object p6, p0, LafI;->a:LMU;

    .line 84
    invoke-static {p7}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LagZ;

    iput-object v0, p0, LafI;->a:LagZ;

    .line 85
    invoke-static {p8}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGF;

    iput-object v0, p0, LafI;->a:LaGF;

    .line 86
    return-void
.end method

.method private a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaJT;)V
    .locals 3

    .prologue
    .line 221
    iget-object v0, p0, LafI;->a:LaGg;

    .line 222
    invoke-interface {v0, p1}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    .line 223
    if-eqz v0, :cond_0

    .line 224
    invoke-interface {p2}, LaJT;->a()Ljava/util/List;

    move-result-object v1

    .line 225
    new-instance v2, LafJ;

    invoke-direct {v2, p0, v0}, LafJ;-><init>(LafI;Lcom/google/android/gms/drive/database/data/ResourceSpec;)V

    .line 226
    invoke-static {v1, v2}, Lbnm;->b(Ljava/lang/Iterable;LbiU;)Z

    move-result v1

    .line 234
    if-nez v1, :cond_0

    .line 235
    iget-object v1, p0, LafI;->a:LaGg;

    invoke-interface {v1, v0}, LaGg;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaFZ;

    move-result-object v0

    .line 236
    if-eqz v0, :cond_0

    .line 237
    invoke-interface {v0}, LaFV;->i()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, LaFV;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v1, v0}, LaJT;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    :cond_0
    return-void
.end method

.method private d()Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 107
    invoke-virtual {p0}, LafI;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    .line 108
    iget-object v0, p0, LafI;->a:LaGg;

    invoke-interface {v0, v2}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;

    move-result-object v0

    .line 109
    if-nez v0, :cond_1

    .line 110
    invoke-virtual {p0}, LafI;->a()V

    .line 212
    :cond_0
    :goto_0
    return v6

    .line 115
    :cond_1
    const/4 v1, 0x0

    .line 117
    :try_start_0
    invoke-virtual {p0}, LafI;->a()LahJ;

    move-result-object v0

    .line 118
    invoke-interface {v0}, LahJ;->a()LMZ;

    move-result-object v1

    .line 119
    if-nez v1, :cond_2

    .line 120
    invoke-virtual {p0}, LafI;->a()V
    :try_end_0
    .catch LNh; {:try_start_0 .. :try_end_0} :catch_0
    .catch LMT; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    if-eqz v1, :cond_0

    .line 137
    invoke-virtual {v1}, LMZ;->close()V

    goto :goto_0

    .line 123
    :cond_2
    :try_start_1
    invoke-virtual {p0}, LafI;->b()V

    .line 127
    new-instance v0, LafM;

    iget-object v3, p0, LafI;->a:LagH;

    invoke-direct {v0, v3}, LafM;-><init>(LagH;)V

    .line 128
    iget-object v3, p0, LafI;->a:LMU;

    invoke-interface {v3, v1, v0}, LMU;->a(LMZ;LagH;)LaJT;
    :try_end_1
    .catch LNh; {:try_start_1 .. :try_end_1} :catch_0
    .catch LMT; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 136
    if-eqz v1, :cond_3

    .line 137
    invoke-virtual {v1}, LMZ;->close()V

    .line 141
    :cond_3
    iget-object v0, p0, LafI;->a:LaGg;

    invoke-interface {v0}, LaGg;->a()V

    .line 146
    :try_start_2
    iget-object v0, p0, LafI;->a:LaGg;

    invoke-interface {v0, v2}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;

    move-result-object v0

    .line 147
    if-nez v0, :cond_5

    .line 148
    invoke-virtual {p0}, LafI;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 158
    iget-object v0, p0, LafI;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V

    goto :goto_0

    .line 130
    :catch_0
    move-exception v0

    .line 131
    :try_start_3
    invoke-virtual {p0, v0}, LafI;->a(Ljava/lang/Exception;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 136
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    .line 137
    invoke-virtual {v1}, LMZ;->close()V

    :cond_4
    throw v0

    .line 132
    :catch_1
    move-exception v0

    .line 133
    :try_start_4
    invoke-virtual {p0}, LafI;->a()LahJ;

    move-result-object v2

    invoke-interface {v2}, LahJ;->q()V

    .line 134
    invoke-virtual {p0, v0}, LafI;->a(Ljava/lang/Exception;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 152
    :cond_5
    :try_start_5
    invoke-virtual {v0}, LaGb;->b()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 153
    invoke-virtual {v0}, LaGb;->a()LaGc;

    move-result-object v0

    .line 154
    invoke-interface {v3}, LaJT;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LaGc;->h(Ljava/lang/String;)LaGe;

    move-result-object v0

    invoke-virtual {v0}, LaGe;->b()LaGd;

    move-result-object v0

    check-cast v0, LaGb;

    .line 156
    :cond_6
    iget-object v4, p0, LafI;->a:LaGg;

    invoke-interface {v4}, LaGg;->c()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 158
    iget-object v4, p0, LafI;->a:LaGg;

    invoke-interface {v4}, LaGg;->b()V

    .line 161
    invoke-virtual {v1}, LMZ;->b()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    .line 162
    if-eqz v1, :cond_7

    .line 163
    invoke-direct {p0, v1, v3}, LafI;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaJT;)V

    .line 171
    :cond_7
    iget-object v1, p0, LafI;->a:LagZ;

    invoke-interface {v1, v0}, LagZ;->e(LaGo;)V

    .line 172
    iget-object v0, p0, LafI;->a:LaGg;

    invoke-interface {v0, v2}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;

    move-result-object v0

    .line 173
    if-nez v0, :cond_8

    .line 174
    const-string v0, "BinaryFileSyncable"

    const-string v1, "A document was not found in the database after uploading, entrySpec: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v6

    invoke-static {v0, v1, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_0

    .line 158
    :catchall_1
    move-exception v0

    iget-object v1, p0, LafI;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    throw v0

    .line 184
    :cond_8
    :try_start_6
    invoke-interface {v3}, LaJT;->i()Ljava/lang/String;

    move-result-object v2

    .line 185
    invoke-interface {v3}, LaJT;->n()Ljava/lang/String;

    move-result-object v1

    .line 186
    iget-object v3, p0, LafI;->a:LaGF;

    invoke-interface {v3, v1}, LaGF;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    .line 187
    sget-object v1, LacY;->a:LacY;

    invoke-virtual {v0, v1}, LaGb;->a(LacY;)J

    move-result-wide v0

    .line 189
    iget-object v4, p0, LafI;->a:LaGg;

    invoke-interface {v4}, LaGg;->a()V
    :try_end_6
    .catch Ljava/text/ParseException; {:try_start_6 .. :try_end_6} :catch_2

    .line 191
    :goto_1
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-ltz v4, :cond_b

    .line 192
    :try_start_7
    iget-object v4, p0, LafI;->a:LaGg;

    invoke-interface {v4, v0, v1}, LaGg;->a(J)LaGp;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-result-object v0

    .line 193
    if-nez v0, :cond_9

    .line 205
    :try_start_8
    iget-object v0, p0, LafI;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V
    :try_end_8
    .catch Ljava/text/ParseException; {:try_start_8 .. :try_end_8} :catch_2

    goto/16 :goto_0

    .line 207
    :catch_2
    move-exception v0

    .line 211
    :goto_2
    invoke-virtual {p0}, LafI;->c()V

    goto/16 :goto_0

    .line 196
    :cond_9
    :try_start_9
    invoke-virtual {v0, v2}, LaGp;->a(Ljava/lang/String;)V

    .line 197
    invoke-virtual {v0, v3}, LaGp;->a(Ljava/util/Date;)V

    .line 198
    invoke-virtual {v0}, LaGp;->e()V

    .line 200
    invoke-virtual {v0}, LaGp;->a()Ljava/lang/Long;

    move-result-object v0

    .line 201
    if-eqz v0, :cond_a

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_1

    :cond_a
    const-wide/16 v0, -0x1

    goto :goto_1

    .line 203
    :cond_b
    iget-object v0, p0, LafI;->a:LaGg;

    invoke-interface {v0}, LaGg;->c()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 205
    :try_start_a
    iget-object v0, p0, LafI;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V

    goto :goto_2

    :catchall_2
    move-exception v0

    iget-object v1, p0, LafI;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    throw v0
    :try_end_a
    .catch Ljava/text/ParseException; {:try_start_a .. :try_end_a} :catch_2
.end method

.method private e()Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 244
    invoke-virtual {p0}, LafI;->b()V

    .line 245
    invoke-virtual {p0}, LafI;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 246
    iget-object v1, p0, LafI;->a:LaGg;

    invoke-interface {v1, v0}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;

    move-result-object v0

    .line 247
    if-nez v0, :cond_0

    .line 294
    :goto_0
    return v7

    .line 250
    :cond_0
    invoke-interface {v0}, LaGo;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v1

    .line 252
    invoke-static {v1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    :try_start_0
    iget-object v2, p0, LafI;->a:Lago;

    sget-object v3, LacY;->a:LacY;

    invoke-virtual {v2, v0, v3}, Lago;->a(LaGo;LacY;)Lagq;
    :try_end_0
    .catch LbwO; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lbxk; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2
    .catch LTr; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_4
    .catch LTt; {:try_start_0 .. :try_end_0} :catch_5

    move-result-object v3

    .line 271
    if-nez v3, :cond_1

    .line 272
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Null content URL"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 257
    :catch_0
    move-exception v0

    .line 258
    new-instance v1, LafD;

    invoke-direct {v1, v0}, LafD;-><init>(Ljava/lang/Exception;)V

    throw v1

    .line 259
    :catch_1
    move-exception v0

    .line 260
    new-instance v1, LafD;

    invoke-direct {v1, v0}, LafD;-><init>(Ljava/lang/Exception;)V

    throw v1

    .line 261
    :catch_2
    move-exception v0

    .line 262
    new-instance v1, LafD;

    invoke-direct {v1, v0}, LafD;-><init>(Ljava/lang/Exception;)V

    throw v1

    .line 263
    :catch_3
    move-exception v0

    .line 264
    new-instance v1, LafD;

    invoke-direct {v1, v0}, LafD;-><init>(Ljava/lang/Exception;)V

    throw v1

    .line 265
    :catch_4
    move-exception v0

    .line 266
    new-instance v1, LafD;

    invoke-direct {v1, v0}, LafD;-><init>(Ljava/lang/Exception;)V

    throw v1

    .line 267
    :catch_5
    move-exception v0

    .line 268
    new-instance v1, LafD;

    invoke-direct {v1, v0}, LafD;-><init>(Ljava/lang/Exception;)V

    throw v1

    .line 275
    :cond_1
    new-instance v6, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v6, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 277
    new-instance v5, LafK;

    iget-object v0, p0, LafI;->a:LagH;

    invoke-direct {v5, p0, v0, v6}, LafK;-><init>(LafI;LagH;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    .line 284
    iget-object v0, p0, LafI;->a:LagG;

    iget-object v2, v3, Lagq;->a:Landroid/net/Uri;

    .line 286
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v3, Lagq;->a:Ljava/lang/String;

    sget-object v4, LacY;->a:LacY;

    .line 284
    invoke-interface/range {v0 .. v5}, LagG;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;Ljava/lang/String;LacY;LagH;)V

    .line 290
    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 291
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Content sync failed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 293
    :cond_2
    iget-object v0, p0, LafI;->a:LagH;

    invoke-interface {v0}, LagH;->b()V

    goto :goto_0
.end method


# virtual methods
.method public c()Z
    .locals 4

    .prologue
    .line 90
    invoke-virtual {p0}, LafI;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 92
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 94
    invoke-virtual {p0}, LafI;->a()LahJ;

    move-result-object v0

    invoke-interface {v0}, LahJ;->a()LahS;

    move-result-object v0

    .line 96
    sget-object v1, LafL;->a:[I

    invoke-virtual {v0}, LahS;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 102
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalide task type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 92
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 98
    :pswitch_0
    invoke-direct {p0}, LafI;->e()Z

    move-result v0

    .line 100
    :goto_1
    return v0

    :pswitch_1
    invoke-direct {p0}, LafI;->d()Z

    move-result v0

    goto :goto_1

    .line 96
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 299
    const-string v0, "BinaryFileSyncable[%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, LafI;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
