.class public LWE;
.super Ljava/lang/Object;
.source "KitKatPrinter.java"

# interfaces
.implements LWL;


# static fields
.field private static final a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LaGM;

.field private final a:LaKR;

.field private final a:Lago;

.field private final a:Landroid/content/Context;

.field private final a:LsP;

.field private final a:LtK;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 28
    const-string v0, "image/jpeg"

    const-string v1, "image/png"

    const-string v2, "image/gif"

    const-string v3, "image/bmp"

    const-string v4, "image/webp"

    .line 29
    invoke-static {v0, v1, v2, v3, v4}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmY;

    move-result-object v0

    sput-object v0, LWE;->a:LbmY;

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LtK;LsP;LaGM;LaKR;Lago;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, LWE;->a:Landroid/content/Context;

    .line 43
    iput-object p2, p0, LWE;->a:LtK;

    .line 44
    iput-object p3, p0, LWE;->a:LsP;

    .line 45
    iput-object p4, p0, LWE;->a:LaGM;

    .line 46
    iput-object p5, p0, LWE;->a:LaKR;

    .line 47
    iput-object p6, p0, LWE;->a:Lago;

    .line 48
    return-void
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 76
    sget-object v0, LWE;->a:LbmY;

    invoke-virtual {v0, p0}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 80
    invoke-static {p0}, LWE;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "application/pdf"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(LaGu;)V
    .locals 3

    .prologue
    .line 85
    invoke-virtual {p0, p1}, LWE;->a(LaGu;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 96
    :goto_0
    return-void

    .line 90
    :cond_0
    :try_start_0
    iget-object v0, p0, LWE;->a:Landroid/content/Context;

    iget-object v1, p0, LWE;->a:LsP;

    sget-object v2, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->f:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    .line 91
    invoke-virtual {v1, p1, v2}, LsP;->a(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)Landroid/content/Intent;

    move-result-object v1

    .line 90
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 92
    :catch_0
    move-exception v0

    .line 93
    const-string v1, "KitKatPrinter"

    const-string v2, "Failed to print"

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public a(LaGu;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 52
    iget-object v2, p0, LWE;->a:LtK;

    sget-object v3, Lry;->aj:Lry;

    invoke-interface {v2, v3}, LtK;->a(LtJ;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 72
    :cond_0
    :goto_0
    return v1

    .line 55
    :cond_1
    sget-object v2, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->f:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    invoke-interface {p1}, LaGu;->a()LaGv;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a(LaGv;)LacY;

    move-result-object v2

    .line 56
    iget-object v3, p0, LWE;->a:Lago;

    invoke-virtual {v3, p1, v2}, Lago;->a(LaGu;LacY;)Ljava/lang/String;

    move-result-object v3

    .line 58
    if-eqz v3, :cond_0

    invoke-interface {p1}, LaGu;->e()Z

    move-result v4

    if-nez v4, :cond_0

    .line 62
    invoke-static {v3}, LWE;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 66
    invoke-interface {p1}, LaGu;->b()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, LWE;->a:LaKR;

    invoke-interface {v3}, LaKR;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    move v1, v0

    .line 67
    goto :goto_0

    .line 70
    :cond_3
    instance-of v3, p1, LaGo;

    if-eqz v3, :cond_4

    iget-object v3, p0, LWE;->a:LaGM;

    check-cast p1, LaGo;

    .line 71
    invoke-interface {v3, p1, v2}, LaGM;->b(LaGo;LacY;)Z

    move-result v2

    if-eqz v2, :cond_4

    :goto_1
    move v1, v0

    .line 72
    goto :goto_0

    :cond_4
    move v0, v1

    .line 71
    goto :goto_1
.end method
