.class LLC;
.super Ljava/lang/Object;
.source "SelectionFunctionDoMoveToCurrentFolder.java"

# interfaces
.implements Lapu;


# instance fields
.field final synthetic a:LLB;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LLB;LbmF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LaGu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    iput-object p1, p0, LLC;->a:LLB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, LLD;

    invoke-direct {v0, p0, p1}, LLD;-><init>(LLC;LLB;)V

    invoke-static {p2, v0}, LbnG;->a(Ljava/util/List;LbiG;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LLC;->a:Ljava/util/List;

    .line 49
    return-void
.end method


# virtual methods
.method public a(LQX;)V
    .locals 4

    .prologue
    .line 53
    invoke-virtual {p1}, LQX;->a()LaFX;

    move-result-object v0

    const-class v1, LaGA;

    invoke-virtual {v0, v1}, LaFX;->a(Ljava/lang/Class;)LaFW;

    move-result-object v0

    check-cast v0, LaGA;

    .line 54
    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, LaGA;->a()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 55
    invoke-interface {v0, v1}, LaGA;->a(I)Z

    move-result v2

    invoke-static {v2}, LbiT;->b(Z)V

    .line 56
    invoke-interface {v0}, LaGA;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    .line 57
    iget-object v3, p0, LLC;->a:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 58
    iget-object v0, p0, LLC;->a:LLB;

    invoke-static {v0}, LLB;->a(LLB;)Laja;

    move-result-object v0

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/DocListView;->setSelectionWithCursorPosition(I)V

    .line 62
    :cond_0
    return-void

    .line 54
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
