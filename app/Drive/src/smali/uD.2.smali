.class public abstract enum LuD;
.super Ljava/lang/Enum;
.source "MoveEntryActivityLegacy.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LuD;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LuD;

.field private static final synthetic a:[LuD;

.field public static final enum b:LuD;

.field public static final enum c:LuD;

.field public static final enum d:LuD;

.field public static final enum e:LuD;

.field public static final enum f:LuD;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 49
    new-instance v0, LuE;

    const-string v1, "VALIDATE_SELECTED_ENTRY"

    invoke-direct {v0, v1, v3}, LuE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LuD;->a:LuD;

    .line 55
    new-instance v0, LuF;

    const-string v1, "LAUNCH_PICK_ENTRY_DIALOG"

    invoke-direct {v0, v1, v4}, LuF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LuD;->b:LuD;

    .line 61
    new-instance v0, LuG;

    const-string v1, "SELECTING_TARGET"

    invoke-direct {v0, v1, v5}, LuG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LuD;->c:LuD;

    .line 68
    new-instance v0, LuH;

    const-string v1, "WARNING_DIALOG"

    invoke-direct {v0, v1, v6}, LuH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LuD;->d:LuD;

    .line 74
    new-instance v0, LuI;

    const-string v1, "PERFORM_MOVE"

    invoke-direct {v0, v1, v7}, LuI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LuD;->e:LuD;

    .line 80
    new-instance v0, LuJ;

    const-string v1, "FINISH"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LuJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LuD;->f:LuD;

    .line 48
    const/4 v0, 0x6

    new-array v0, v0, [LuD;

    sget-object v1, LuD;->a:LuD;

    aput-object v1, v0, v3

    sget-object v1, LuD;->b:LuD;

    aput-object v1, v0, v4

    sget-object v1, LuD;->c:LuD;

    aput-object v1, v0, v5

    sget-object v1, LuD;->d:LuD;

    aput-object v1, v0, v6

    sget-object v1, LuD;->e:LuD;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LuD;->f:LuD;

    aput-object v2, v0, v1

    sput-object v0, LuD;->a:[LuD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILuy;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, LuD;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LuD;
    .locals 1

    .prologue
    .line 48
    const-class v0, LuD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LuD;

    return-object v0
.end method

.method public static values()[LuD;
    .locals 1

    .prologue
    .line 48
    sget-object v0, LuD;->a:[LuD;

    invoke-virtual {v0}, [LuD;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LuD;

    return-object v0
.end method


# virtual methods
.method abstract a(Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;)LuD;
.end method
