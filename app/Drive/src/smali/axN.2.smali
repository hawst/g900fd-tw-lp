.class LaxN;
.super Laxy;
.source "PdfPageLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laxy",
        "<",
        "Lcom/google/android/apps/viewer/pdflib/MatchRects;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LaxI;

.field private final a:Ljava/lang/String;


# direct methods
.method constructor <init>(LaxI;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 345
    iput-object p1, p0, LaxN;->a:LaxI;

    .line 346
    invoke-static {p1}, LaxI;->a(LaxI;)Laxz;

    move-result-object v0

    sget-object v1, LaxR;->f:LaxR;

    invoke-direct {p0, v0, v1}, Laxy;-><init>(Laxz;LaxR;)V

    .line 347
    iput-object p2, p0, LaxN;->a:Ljava/lang/String;

    .line 348
    return-void
.end method

.method static synthetic a(LaxN;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, LaxN;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected a(LavE;)Lcom/google/android/apps/viewer/pdflib/MatchRects;
    .locals 2

    .prologue
    .line 352
    iget-object v0, p0, LaxN;->a:LaxI;

    invoke-static {v0}, LaxI;->a(LaxI;)I

    move-result v0

    iget-object v1, p0, LaxN;->a:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, LavE;->a(ILjava/lang/String;)Lcom/google/android/apps/viewer/pdflib/MatchRects;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(LavE;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 342
    invoke-virtual {p0, p1}, LaxN;->a(LavE;)Lcom/google/android/apps/viewer/pdflib/MatchRects;

    move-result-object v0

    return-object v0
.end method

.method protected a(LaxH;Lcom/google/android/apps/viewer/pdflib/MatchRects;)V
    .locals 2

    .prologue
    .line 356
    iget-object v0, p0, LaxN;->a:Ljava/lang/String;

    iget-object v1, p0, LaxN;->a:LaxI;

    invoke-static {v1}, LaxI;->a(LaxI;)I

    move-result v1

    invoke-interface {p1, v0, v1, p2}, LaxH;->a(Ljava/lang/String;ILcom/google/android/apps/viewer/pdflib/MatchRects;)V

    .line 357
    iget-object v0, p0, LaxN;->a:LaxI;

    const/4 v1, 0x0

    iput-object v1, v0, LaxI;->a:LaxN;

    .line 358
    return-void
.end method

.method protected bridge synthetic a(LaxH;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 342
    check-cast p2, Lcom/google/android/apps/viewer/pdflib/MatchRects;

    invoke-virtual {p0, p1, p2}, LaxN;->a(LaxH;Lcom/google/android/apps/viewer/pdflib/MatchRects;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 361
    const-string v0, "SearchPageTextTask(page=%d, query=\"%s\")"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LaxN;->a:LaxI;

    invoke-static {v3}, LaxI;->a(LaxI;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LaxN;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
