.class public interface abstract LaWA;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a(Lcom/google/android/gms/maps/model/StreetViewPanoramaOrientation;)LaLm;
.end method

.method public abstract a()Lcom/google/android/gms/maps/model/StreetViewPanoramaCamera;
.end method

.method public abstract a()Lcom/google/android/gms/maps/model/StreetViewPanoramaLocation;
.end method

.method public abstract a(LaLm;)Lcom/google/android/gms/maps/model/StreetViewPanoramaOrientation;
.end method

.method public abstract a(LaXD;)V
.end method

.method public abstract a(LaXG;)V
.end method

.method public abstract a(LaXJ;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/LatLng;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/LatLng;I)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/StreetViewPanoramaCamera;J)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract a()Z
.end method

.method public abstract b(Z)V
.end method

.method public abstract b()Z
.end method

.method public abstract c(Z)V
.end method

.method public abstract c()Z
.end method

.method public abstract d(Z)V
.end method

.method public abstract d()Z
.end method
