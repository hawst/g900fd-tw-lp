.class public Lado;
.super Ljava/lang/Object;
.source "DocumentFileManagerImpl.java"

# interfaces
.implements LbiG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbiG",
        "<",
        "Ladw;",
        "Ladj;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LaGp;

.field final synthetic a:LacY;

.field final synthetic a:Ladw;

.field final synthetic a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

.field final synthetic a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field final synthetic b:LaGp;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Ladw;Lcom/google/android/gms/drive/database/data/EntrySpec;LacY;LaGp;LaGp;)V
    .locals 0

    .prologue
    .line 1105
    iput-object p1, p0, Lado;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    iput-object p2, p0, Lado;->a:Ladw;

    iput-object p3, p0, Lado;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object p4, p0, Lado;->a:LacY;

    iput-object p5, p0, Lado;->a:LaGp;

    iput-object p6, p0, Lado;->b:LaGp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ladw;)Ladj;
    .locals 8

    .prologue
    .line 1108
    iget-object v1, p0, Lado;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    monitor-enter v1

    .line 1109
    :try_start_0
    iget-object v0, p0, Lado;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    .line 1110
    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)Lalo;

    move-result-object v0

    iget-object v2, p0, Lado;->a:Ladw;

    invoke-virtual {v2}, Ladw;->a()Ljava/io/File;

    move-result-object v2

    invoke-interface {v0, v2}, Lalo;->a(Ljava/io/File;)J

    move-result-wide v2

    .line 1112
    iget-object v0, p0, Lado;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    iget-object v4, p0, Lado;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v5, p0, Lado;->a:LacY;

    iget-object v6, p0, Lado;->a:Ladw;

    invoke-static {v0, v4, v5, v6}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Lcom/google/android/gms/drive/database/data/EntrySpec;LacY;Ladw;)Ladj;

    move-result-object v4

    .line 1115
    iget-object v0, p0, Lado;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v0

    invoke-interface {v0}, LaGg;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1118
    :try_start_1
    iget-object v0, p0, Lado;->a:LaGp;

    invoke-virtual {v0}, LaGp;->a()V

    .line 1119
    iget-object v0, p0, Lado;->a:LaGp;

    invoke-virtual {v0, v2, v3}, LaGp;->d(J)V

    .line 1123
    iget-object v0, p0, Lado;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v0

    iget-object v2, p0, Lado;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v2}, LaGg;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;

    move-result-object v0

    .line 1125
    if-eqz v0, :cond_2

    .line 1126
    iget-object v2, p0, Lado;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v2

    iget-object v3, p0, Lado;->a:LacY;

    .line 1127
    invoke-virtual {v0, v3}, LaGb;->a(LacY;)J

    move-result-wide v6

    .line 1126
    invoke-interface {v2, v6, v7}, LaGg;->a(J)LaGp;

    move-result-object v2

    .line 1131
    if-eqz v2, :cond_0

    .line 1132
    invoke-virtual {v2}, LaGp;->c()J

    move-result-wide v2

    iget-object v5, p0, Lado;->b:LaGp;

    invoke-virtual {v5}, LaGp;->c()J

    move-result-wide v6

    cmp-long v2, v2, v6

    if-nez v2, :cond_0

    .line 1133
    invoke-virtual {v0}, LaGb;->a()LaGc;

    move-result-object v0

    iget-object v2, p0, Lado;->a:LaGp;

    .line 1134
    invoke-virtual {v2}, LaGp;->c()J

    move-result-wide v2

    iget-object v5, p0, Lado;->a:LacY;

    invoke-virtual {v0, v2, v3, v5}, LaGc;->a(JLacY;)LaGc;

    move-result-object v0

    .line 1135
    invoke-virtual {v0}, LaGc;->b()LaGb;

    move-result-object v0

    .line 1136
    iget-object v2, p0, Lado;->a:LaGp;

    iget-object v3, p0, Lado;->b:LaGp;

    invoke-virtual {v2, v3}, LaGp;->a(LaGp;)V

    .line 1138
    :cond_0
    iget-object v2, p0, Lado;->b:LaGp;

    invoke-virtual {v2}, LaGp;->a()V

    .line 1139
    invoke-virtual {v0}, LaGb;->c()Ljava/util/Date;

    move-result-object v0

    .line 1140
    if-eqz v0, :cond_1

    .line 1141
    iget-object v2, p0, Lado;->b:LaGp;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, LaGp;->b(J)V

    .line 1143
    :cond_1
    iget-object v0, p0, Lado;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LadE;

    move-result-object v0

    iget-object v2, p0, Lado;->b:LaGp;

    invoke-interface {v0, v2}, LadE;->a(LaGp;)V

    .line 1145
    :cond_2
    iget-object v0, p0, Lado;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LadE;

    move-result-object v0

    iget-object v2, p0, Lado;->a:LaGp;

    invoke-interface {v0, v2}, LadE;->a(LaGp;)V

    .line 1146
    iget-object v0, p0, Lado;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v0

    invoke-interface {v0}, LaGg;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1148
    :try_start_2
    iget-object v0, p0, Lado;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v0

    invoke-interface {v0}, LaGg;->b()V

    .line 1150
    monitor-exit v1

    return-object v4

    .line 1148
    :catchall_0
    move-exception v0

    iget-object v2, p0, Lado;->a:Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;

    invoke-static {v2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;

    move-result-object v2

    invoke-interface {v2}, LaGg;->b()V

    throw v0

    .line 1151
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1105
    check-cast p1, Ladw;

    invoke-virtual {p0, p1}, Lado;->a(Ladw;)Ladj;

    move-result-object v0

    return-object v0
.end method
