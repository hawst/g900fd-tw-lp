.class LLO;
.super Ljava/lang/Object;
.source "SelectionFunctionDoUnPin.java"

# interfaces
.implements LLw;


# instance fields
.field private final a:LVg;

.field private final a:LtK;


# direct methods
.method public constructor <init>(LtK;LVg;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, LLO;->a:LtK;

    .line 27
    iput-object p2, p0, LLO;->a:LVg;

    .line 28
    return-void
.end method


# virtual methods
.method public a(LaFM;LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFM;",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ")V"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, LLO;->a:LVg;

    .line 50
    invoke-virtual {p1}, LaFM;->a()LaFO;

    move-result-object v1

    invoke-virtual {v0, v1}, LVg;->a(LaFO;)LVf;

    move-result-object v1

    .line 51
    invoke-virtual {p2}, LbmF;->a()Lbqv;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    .line 52
    invoke-interface {v0}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-virtual {v1, v0}, LVf;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LVf;

    goto :goto_0

    .line 54
    :cond_0
    iget-object v0, p0, LLO;->a:LVg;

    invoke-virtual {v1}, LVf;->a()LVd;

    move-result-object v1

    invoke-virtual {v0, v1}, LVg;->a(LVd;)V

    .line 55
    return-void
.end method

.method public a(Ljava/lang/Runnable;LaFM;LbmF;LbmF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "LaFM;",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 61
    return-void
.end method

.method public a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 33
    iget-object v0, p0, LLO;->a:LtK;

    sget-object v2, Lry;->af:Lry;

    invoke-interface {v0, v2}, LtK;->a(LtJ;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 43
    :cond_0
    :goto_0
    return v1

    .line 38
    :cond_1
    invoke-virtual {p1}, LbmF;->a()Lbqv;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    .line 39
    invoke-interface {v0}, LaGu;->g()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, LaGu;->e()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, LaGu;->f()Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    .line 43
    :cond_3
    invoke-virtual {p1}, LbmF;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method
