.class LUC;
.super LVb;
.source "GroupIndexPatternMatcher.java"


# instance fields
.field private final a:I


# direct methods
.method constructor <init>(LVc;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 34
    invoke-static {p2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, LUC;-><init>(LVc;Ljava/util/regex/Pattern;I)V

    .line 35
    return-void
.end method

.method constructor <init>(LVc;Ljava/util/regex/Pattern;I)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, LVb;-><init>(LVc;Ljava/util/regex/Pattern;)V

    .line 23
    iput p3, p0, LUC;->a:I

    .line 24
    return-void
.end method


# virtual methods
.method protected a(Ljava/util/regex/Matcher;Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget v0, p0, LUC;->a:I

    invoke-virtual {p1, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/util/regex/Matcher;Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 39
    invoke-super {p0, p1, p2}, LVb;->a(Ljava/util/regex/Matcher;Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v0

    iget v1, p0, LUC;->a:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 49
    const-string v0, "groupIndex=[%d] %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, LUC;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-super {p0}, LVb;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
