.class final Lafr;
.super Lafi;
.source "SingleFeedFilter.java"


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic a:Z


# direct methods
.method constructor <init>(Ljava/lang/String;ZLjava/lang/String;Z)V
    .locals 0

    .prologue
    .line 281
    iput-object p3, p0, Lafr;->a:Ljava/lang/String;

    iput-boolean p4, p0, Lafr;->a:Z

    invoke-direct {p0, p1, p2}, Lafi;-><init>(Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method protected b(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x1

    .line 285
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    .line 286
    const-string v0, "-"

    invoke-interface {v2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 287
    if-ne v0, v7, :cond_0

    .line 288
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 290
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    .line 291
    invoke-interface {v2, v8, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    .line 292
    invoke-static {v3, v4}, Lafi;->a(Landroid/net/Uri$Builder;Ljava/util/List;)V

    .line 293
    iget-object v1, p0, Lafr;->a:Ljava/lang/String;

    .line 294
    iget-boolean v5, p0, Lafr;->a:Z

    if-eqz v5, :cond_1

    .line 295
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "folder:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 298
    :cond_1
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    aput-object v1, v5, v8

    const/4 v1, 0x1

    const-string v6, "contents"

    aput-object v6, v5, v1

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 300
    invoke-static {v4, v1}, Ljava/util/Collections;->indexOfSubList(Ljava/util/List;Ljava/util/List;)I

    move-result v4

    if-ne v4, v7, :cond_2

    .line 301
    invoke-static {v3, v1}, Lafi;->a(Landroid/net/Uri$Builder;Ljava/util/List;)V

    .line 303
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v2, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 304
    invoke-static {v3, v0}, Lafi;->a(Landroid/net/Uri$Builder;Ljava/util/List;)V

    .line 305
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
