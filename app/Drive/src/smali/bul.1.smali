.class public final enum Lbul;
.super Ljava/lang/Enum;
.source "JsonToken.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lbul;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lbul;

.field private static final synthetic a:[Lbul;

.field public static final enum b:Lbul;

.field public static final enum c:Lbul;

.field public static final enum d:Lbul;

.field public static final enum e:Lbul;

.field public static final enum f:Lbul;

.field public static final enum g:Lbul;

.field public static final enum h:Lbul;

.field public static final enum i:Lbul;

.field public static final enum j:Lbul;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 31
    new-instance v0, Lbul;

    const-string v1, "BEGIN_ARRAY"

    invoke-direct {v0, v1, v3}, Lbul;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbul;->a:Lbul;

    .line 37
    new-instance v0, Lbul;

    const-string v1, "END_ARRAY"

    invoke-direct {v0, v1, v4}, Lbul;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbul;->b:Lbul;

    .line 43
    new-instance v0, Lbul;

    const-string v1, "BEGIN_OBJECT"

    invoke-direct {v0, v1, v5}, Lbul;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbul;->c:Lbul;

    .line 49
    new-instance v0, Lbul;

    const-string v1, "END_OBJECT"

    invoke-direct {v0, v1, v6}, Lbul;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbul;->d:Lbul;

    .line 56
    new-instance v0, Lbul;

    const-string v1, "NAME"

    invoke-direct {v0, v1, v7}, Lbul;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbul;->e:Lbul;

    .line 61
    new-instance v0, Lbul;

    const-string v1, "STRING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lbul;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbul;->f:Lbul;

    .line 67
    new-instance v0, Lbul;

    const-string v1, "NUMBER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lbul;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbul;->g:Lbul;

    .line 72
    new-instance v0, Lbul;

    const-string v1, "BOOLEAN"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lbul;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbul;->h:Lbul;

    .line 77
    new-instance v0, Lbul;

    const-string v1, "NULL"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lbul;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbul;->i:Lbul;

    .line 84
    new-instance v0, Lbul;

    const-string v1, "END_DOCUMENT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lbul;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbul;->j:Lbul;

    .line 25
    const/16 v0, 0xa

    new-array v0, v0, [Lbul;

    sget-object v1, Lbul;->a:Lbul;

    aput-object v1, v0, v3

    sget-object v1, Lbul;->b:Lbul;

    aput-object v1, v0, v4

    sget-object v1, Lbul;->c:Lbul;

    aput-object v1, v0, v5

    sget-object v1, Lbul;->d:Lbul;

    aput-object v1, v0, v6

    sget-object v1, Lbul;->e:Lbul;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lbul;->f:Lbul;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lbul;->g:Lbul;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lbul;->h:Lbul;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lbul;->i:Lbul;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lbul;->j:Lbul;

    aput-object v2, v0, v1

    sput-object v0, Lbul;->a:[Lbul;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbul;
    .locals 1

    .prologue
    .line 25
    const-class v0, Lbul;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbul;

    return-object v0
.end method

.method public static values()[Lbul;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lbul;->a:[Lbul;

    invoke-virtual {v0}, [Lbul;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbul;

    return-object v0
.end method
