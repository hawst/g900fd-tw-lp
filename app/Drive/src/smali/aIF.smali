.class public LaIF;
.super Ljava/lang/Object;
.source "GuidFactory.java"


# instance fields
.field private final a:LaGM;

.field private final a:LaIy;

.field private final a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LvL;


# direct methods
.method public constructor <init>(LaGM;LaIy;Laja;LvL;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGM;",
            "LaIy;",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LvL;",
            ")V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p0, LaIF;->a:LaGM;

    .line 36
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaIy;

    iput-object v0, p0, LaIF;->a:LaIy;

    .line 37
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p0, LaIF;->a:Laja;

    .line 38
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvL;

    iput-object v0, p0, LaIF;->a:LvL;

    .line 39
    return-void
.end method

.method private a(JLjava/lang/String;)LaIC;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 60
    .line 61
    invoke-static {}, LaIJ;->values()[LaIJ;

    move-result-object v4

    array-length v5, v4

    const/4 v0, 0x0

    move v3, v0

    move-object v1, v2

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v0, v4, v3

    .line 62
    invoke-virtual {v0}, LaIJ;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 61
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move-object v1, v0

    goto :goto_0

    .line 66
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {p0, p1, p2, v1}, LaIF;->a(JLaIJ;)LaII;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_1
    move-object v0, v2

    goto :goto_2

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public static a(JLcom/google/android/gms/drive/database/data/EntrySpec;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 130
    invoke-static {p2}, LaIA;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Ljava/lang/String;

    move-result-object v0

    .line 131
    invoke-static {p0, p1, v0}, LaIF;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(JLjava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 135
    const-string v0, "%s%s;%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "acc="

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LaIC;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 126
    invoke-virtual {p0}, LaIC;->a()J

    move-result-wide v0

    invoke-virtual {p0}, LaIC;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LaIF;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(JLjava/lang/String;)LaIA;
    .locals 9

    .prologue
    .line 47
    new-instance v1, LaIA;

    iget-object v5, p0, LaIF;->a:LaGM;

    iget-object v6, p0, LaIF;->a:LvL;

    iget-object v7, p0, LaIF;->a:LaIy;

    move-wide v2, p1

    move-object v4, p3

    invoke-direct/range {v1 .. v7}, LaIA;-><init>(JLjava/lang/String;LaGM;LvL;LaIy;)V

    return-object v1
.end method

.method public final a(Ljava/lang/String;)LaIC;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 91
    const-string v1, ";"

    const/4 v2, -0x1

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v1

    .line 92
    array-length v2, v1

    const/4 v3, 0x2

    if-eq v2, v3, :cond_1

    .line 115
    :cond_0
    :goto_0
    return-object v0

    .line 95
    :cond_1
    aget-object v2, v1, v5

    const-string v3, "acc="

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 98
    aget-object v2, v1, v5

    const-string v3, "acc="

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 100
    aget-object v1, v1, v6

    .line 101
    const-string v4, "0"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 102
    invoke-virtual {p0, v2, v3}, LaIF;->a(J)LaIG;

    move-result-object v0

    goto :goto_0

    .line 105
    :cond_2
    invoke-static {v1}, LaIA;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 106
    invoke-virtual {p0, v2, v3, v1}, LaIF;->a(JLjava/lang/String;)LaIA;

    move-result-object v0

    goto :goto_0

    .line 109
    :cond_3
    const-string v4, "view="

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 110
    const-string v0, "view="

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 111
    invoke-direct {p0, v2, v3, v0}, LaIF;->a(JLjava/lang/String;)LaIC;

    move-result-object v0

    goto :goto_0

    .line 114
    :cond_4
    const-string v2, "GuidFactory"

    const-string v3, "Unsupported DOC_ID format: %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public a(J)LaIG;
    .locals 3

    .prologue
    .line 75
    new-instance v0, LaIG;

    iget-object v1, p0, LaIF;->a:LaGM;

    iget-object v2, p0, LaIF;->a:Laja;

    invoke-direct {v0, p1, p2, v1, v2}, LaIG;-><init>(JLaGM;Laja;)V

    return-object v0
.end method

.method public a(JLjava/lang/String;)LaIH;
    .locals 7

    .prologue
    .line 122
    new-instance v1, LaIH;

    iget-object v5, p0, LaIF;->a:LaGM;

    iget-object v6, p0, LaIF;->a:LvL;

    move-wide v2, p1

    move-object v4, p3

    invoke-direct/range {v1 .. v6}, LaIH;-><init>(JLjava/lang/String;LaGM;LvL;)V

    return-object v1
.end method

.method public a(JLaIJ;)LaII;
    .locals 9

    .prologue
    .line 56
    new-instance v1, LaII;

    iget-object v5, p0, LaIF;->a:LaGM;

    iget-object v6, p0, LaIF;->a:LvL;

    iget-object v7, p0, LaIF;->a:LaIy;

    iget-object v8, p0, LaIF;->a:Laja;

    move-wide v2, p1

    move-object v4, p3

    invoke-direct/range {v1 .. v8}, LaII;-><init>(JLaIJ;LaGM;LvL;LaIy;Laja;)V

    return-object v1
.end method
