.class public abstract LqS;
.super Lrm;
.source "AbstractPickAccountAndEntryActivity.java"

# interfaces
.implements LFq;


# instance fields
.field private a:LaFO;

.field a:LaGR;

.field public a:Lcom/google/android/gms/drive/database/data/EntrySpec;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lrm;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;
.end method

.method protected a(LaFO;)V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, LqS;->a:LaGR;

    new-instance v1, LqT;

    invoke-direct {v1, p0, p1}, LqT;-><init>(LqS;LaFO;)V

    invoke-virtual {v0, v1}, LaGR;->b(LaGN;)V

    .line 91
    return-void
.end method

.method public a(Landroid/accounts/Account;)V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    iput-object v0, p0, LqS;->a:LaFO;

    .line 118
    iget-object v0, p0, LqS;->a:LaFO;

    invoke-virtual {p0, v0}, LqS;->a(LaFO;)V

    .line 119
    return-void
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
.end method

.method public a(LuW;)V
    .locals 0

    .prologue
    .line 98
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 101
    new-instance v0, LqU;

    invoke-direct {v0, p0}, LqU;-><init>(LqS;)V

    invoke-virtual {p0, v0}, LqS;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 108
    return-void
.end method

.method public f()V
    .locals 0

    .prologue
    .line 112
    invoke-virtual {p0}, LqS;->e()V

    .line 113
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 123
    if-nez p1, :cond_0

    .line 124
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    .line 125
    const-string v0, "entrySpec.v2"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, LqS;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 126
    iget-object v0, p0, LqS;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-eqz v0, :cond_1

    .line 127
    iget-object v0, p0, LqS;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {p0, v0}, LqS;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 135
    :cond_0
    :goto_0
    return-void

    .line 129
    :cond_1
    invoke-virtual {p0}, LqS;->e()V

    goto :goto_0

    .line 132
    :cond_2
    invoke-virtual {p0}, LqS;->e()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 36
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 37
    if-eqz p1, :cond_0

    .line 38
    const-string v0, "accountName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    iput-object v0, p0, LqS;->a:LaFO;

    .line 39
    const-string v0, "entrySpec.v2"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, LqS;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 43
    :cond_0
    iget-object v0, p0, LqS;->a:LaFO;

    if-nez v0, :cond_1

    .line 44
    invoke-virtual {p0}, LqS;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    iput-object v0, p0, LqS;->a:LaFO;

    .line 47
    :cond_1
    iget-object v0, p0, LqS;->a:LaFO;

    if-nez v0, :cond_3

    .line 48
    invoke-virtual {p0}, LqS;->a()LM;

    move-result-object v1

    .line 49
    const-string v0, "PickAccountDialogFragment"

    .line 50
    invoke-virtual {v1, v0}, LM;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;

    .line 51
    if-nez v0, :cond_2

    .line 52
    invoke-static {v1}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a(LM;)Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;

    .line 59
    :cond_2
    :goto_0
    return-void

    .line 54
    :cond_3
    iget-object v0, p0, LqS;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-nez v0, :cond_4

    .line 55
    iget-object v0, p0, LqS;->a:LaFO;

    invoke-virtual {p0, v0}, LqS;->a(LaFO;)V

    goto :goto_0

    .line 57
    :cond_4
    iget-object v0, p0, LqS;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {p0, v0}, LqS;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 63
    invoke-super {p0, p1}, Lrm;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 64
    const-string v0, "accountName"

    iget-object v1, p0, LqS;->a:LaFO;

    invoke-static {v1}, LaFO;->a(LaFO;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v0, "entrySpec.v2"

    iget-object v1, p0, LqS;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 66
    return-void
.end method
