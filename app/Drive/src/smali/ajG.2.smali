.class LajG;
.super Ljava/lang/Object;
.source "WrappedConstantBindingBuilder.java"

# interfaces
.implements LbuR;
.implements LbuS;


# instance fields
.field private final a:LbuR;

.field private a:LbuS;

.field private a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/lang/annotation/Annotation;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lbuv",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LbuR;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbuR;",
            "Ljava/util/Set",
            "<",
            "Lbuv",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuR;

    iput-object v0, p0, LajG;->a:LbuR;

    .line 27
    invoke-static {p2}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, LajG;->a:Ljava/util/Set;

    .line 28
    return-void
.end method

.method private a(Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, LajG;->a:Ljava/lang/annotation/Annotation;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, LajG;->a:Ljava/lang/annotation/Annotation;

    invoke-static {p1, v0}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 120
    :goto_0
    iget-object v1, p0, LajG;->a:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 121
    return-void

    .line 114
    :cond_0
    iget-object v0, p0, LajG;->a:Ljava/lang/Class;

    if-eqz v0, :cond_1

    .line 115
    iget-object v0, p0, LajG;->a:Ljava/lang/Class;

    invoke-static {p1, v0}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    goto :goto_0

    .line 117
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Annotation for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not specified"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a(Ljava/lang/Class;)LbuS;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LbuS;"
        }
    .end annotation

    .prologue
    .line 32
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, LajG;->a:Ljava/lang/Class;

    .line 33
    iget-object v0, p0, LajG;->a:LbuR;

    invoke-interface {v0, p1}, LbuR;->a(Ljava/lang/Class;)LbuS;

    move-result-object v0

    iput-object v0, p0, LajG;->a:LbuS;

    .line 34
    return-object p0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 46
    const-class v0, Ljava/lang/String;

    invoke-direct {p0, v0, p1}, LajG;->a(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 47
    iget-object v0, p0, LajG;->a:LbuS;

    invoke-interface {v0, p1}, LbuS;->a(Ljava/lang/String;)V

    .line 48
    return-void
.end method
