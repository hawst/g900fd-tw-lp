.class public LBP;
.super Ljava/lang/Object;
.source "DocListViewGridArrangementModeDelegate.java"


# instance fields
.field a:LBM;
    .annotation runtime LQP;
    .end annotation
.end field

.field private final a:LBa;

.field private final a:LHG;

.field private final a:LHM;

.field private final a:LHz;

.field private final a:LKs;

.field private final a:LaGM;

.field private final a:LarF;

.field private final a:LtK;

.field private final a:LzZ;


# direct methods
.method public constructor <init>(LKs;LzZ;LHz;LaGM;LtK;LHG;LHM;LBa;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p0, LBP;->a:LtK;

    .line 68
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p0, LBP;->a:LaGM;

    .line 69
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHz;

    iput-object v0, p0, LBP;->a:LHz;

    .line 70
    iput-object p1, p0, LBP;->a:LKs;

    .line 71
    iput-object p8, p0, LBP;->a:LBa;

    .line 73
    new-instance v0, LBQ;

    invoke-direct {v0, p0}, LBQ;-><init>(LBP;)V

    iput-object v0, p0, LBP;->a:LarF;

    .line 81
    iput-object p2, p0, LBP;->a:LzZ;

    .line 82
    invoke-static {p6}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHG;

    iput-object v0, p0, LBP;->a:LHG;

    .line 83
    iput-object p7, p0, LBP;->a:LHM;

    .line 84
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/docs/view/DocListView;Landroid/support/v4/app/Fragment;Landroid/widget/ListView;Lcom/google/android/apps/docs/view/StickyHeaderView;I)LBK;
    .locals 18

    .prologue
    .line 92
    move-object/from16 v0, p0

    iget-object v2, v0, LBP;->a:LHM;

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v2, v0, v1}, LHM;->a(Landroid/support/v4/app/Fragment;LCt;)LHL;

    move-result-object v2

    .line 94
    new-instance v15, LBO;

    move-object/from16 v0, p0

    iget-object v3, v0, LBP;->a:LHG;

    invoke-direct {v15, v3, v2}, LBO;-><init>(LHG;LHL;)V

    .line 97
    new-instance v2, LBK;

    move-object/from16 v0, p0

    iget-object v3, v0, LBP;->a:LHz;

    move-object/from16 v0, p0

    iget-object v4, v0, LBP;->a:LKs;

    move-object/from16 v0, p0

    iget-object v5, v0, LBP;->a:LaGM;

    move-object/from16 v0, p0

    iget-object v6, v0, LBP;->a:LtK;

    move-object/from16 v0, p0

    iget-object v12, v0, LBP;->a:LarF;

    move-object/from16 v0, p0

    iget-object v13, v0, LBP;->a:LzZ;

    move-object/from16 v0, p0

    iget-object v14, v0, LBP;->a:LBM;

    move-object/from16 v0, p0

    iget-object v0, v0, LBP;->a:LBa;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move/from16 v11, p5

    invoke-direct/range {v2 .. v17}, LBK;-><init>(LHz;LKs;LaGM;LtK;Lcom/google/android/apps/docs/view/DocListView;Landroid/support/v4/app/Fragment;Landroid/widget/ListView;Lcom/google/android/apps/docs/view/StickyHeaderView;ILarF;LzZ;LBM;LBM;LBa;LBL;)V

    return-object v2
.end method
