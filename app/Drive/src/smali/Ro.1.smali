.class public final LRo;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LRa;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LRh;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LSc;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LQV;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LRP;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LalR;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 43
    iput-object p1, p0, LRo;->a:LbrA;

    .line 44
    const-class v0, LRa;

    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LRo;->a:Lbsk;

    .line 47
    const-class v0, LRh;

    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LRo;->b:Lbsk;

    .line 50
    const-class v0, LSc;

    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LRo;->c:Lbsk;

    .line 53
    const-class v0, LQV;

    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LRo;->d:Lbsk;

    .line 56
    const-class v0, LRP;

    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LRo;->e:Lbsk;

    .line 59
    const-class v0, LalR;

    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LRo;->f:Lbsk;

    .line 62
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 527
    sparse-switch p1, :sswitch_data_0

    .line 663
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 529
    :sswitch_0
    new-instance v0, LRa;

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->a:Lbsk;

    .line 532
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LRo;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->a:Lbsk;

    .line 530
    invoke-static {v1, v2}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, LRo;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LRo;

    iget-object v2, v2, LRo;->d:Lbsk;

    .line 538
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LRo;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LRo;

    iget-object v3, v3, LRo;->d:Lbsk;

    .line 536
    invoke-static {v2, v3}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LQV;

    iget-object v3, p0, LRo;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaGH;

    iget-object v3, v3, LaGH;->l:Lbsk;

    .line 544
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LRo;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaGH;

    iget-object v4, v4, LaGH;->l:Lbsk;

    .line 542
    invoke-static {v3, v4}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaGM;

    iget-object v4, p0, LRo;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LCw;

    iget-object v4, v4, LCw;->N:Lbsk;

    .line 550
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LRo;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LCw;

    iget-object v5, v5, LCw;->N:Lbsk;

    .line 548
    invoke-static {v4, v5}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LCo;

    iget-object v5, p0, LRo;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LIv;

    iget-object v5, v5, LIv;->i:Lbsk;

    .line 556
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LRo;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LIv;

    iget-object v6, v6, LIv;->i:Lbsk;

    .line 554
    invoke-static {v5, v6}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LIh;

    iget-object v6, p0, LRo;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LCw;

    iget-object v6, v6, LCw;->ag:Lbsk;

    .line 562
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LRo;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LCw;

    iget-object v7, v7, LCw;->ag:Lbsk;

    .line 560
    invoke-static {v6, v7}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LAX;

    invoke-direct/range {v0 .. v6}, LRa;-><init>(Landroid/content/Context;LQV;LaGM;LCo;LIh;LAX;)V

    .line 661
    :goto_0
    return-object v0

    .line 569
    :sswitch_1
    new-instance v4, LRh;

    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 572
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->a:Lbsk;

    .line 570
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->aa:Lbsk;

    .line 578
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LRo;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LCw;

    iget-object v2, v2, LCw;->aa:Lbsk;

    .line 576
    invoke-static {v1, v2}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LsC;

    iget-object v2, p0, LRo;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lyh;

    iget-object v2, v2, Lyh;->k:Lbsk;

    .line 584
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LRo;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lyh;

    iget-object v3, v3, Lyh;->k:Lbsk;

    .line 582
    invoke-static {v2, v3}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lye;

    iget-object v3, p0, LRo;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LJQ;

    iget-object v3, v3, LJQ;->d:Lbsk;

    .line 590
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, LRo;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LJQ;

    iget-object v5, v5, LJQ;->d:Lbsk;

    .line 588
    invoke-static {v3, v5}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LJK;

    invoke-direct {v4, v0, v1, v2, v3}, LRh;-><init>(Landroid/content/Context;LsC;Lye;LJK;)V

    move-object v0, v4

    .line 595
    goto :goto_0

    .line 597
    :sswitch_2
    new-instance v0, LSc;

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 600
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LRo;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 598
    invoke-static {v1, v2}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LQr;

    iget-object v2, p0, LRo;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->a:Lbsk;

    .line 606
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LRo;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lc;

    iget-object v3, v3, Lc;->a:Lbsk;

    .line 604
    invoke-static {v2, v3}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    iget-object v3, p0, LRo;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LpG;

    iget-object v3, v3, LpG;->j:Lbsk;

    .line 612
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LRo;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LpG;

    iget-object v4, v4, LpG;->j:Lbsk;

    .line 610
    invoke-static {v3, v4}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaFO;

    iget-object v4, p0, LRo;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LCw;

    iget-object v4, v4, LCw;->aa:Lbsk;

    .line 618
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LRo;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LCw;

    iget-object v5, v5, LCw;->aa:Lbsk;

    .line 616
    invoke-static {v4, v5}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LsC;

    iget-object v5, p0, LRo;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LalC;

    iget-object v5, v5, LalC;->O:Lbsk;

    .line 624
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LRo;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LalC;

    iget-object v6, v6, LalC;->O:Lbsk;

    .line 622
    invoke-static {v5, v6}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LajO;

    invoke-direct/range {v0 .. v5}, LSc;-><init>(LQr;Landroid/content/Context;LaFO;LsC;LajO;)V

    goto/16 :goto_0

    .line 631
    :sswitch_3
    new-instance v3, LQV;

    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lqa;

    iget-object v0, v0, Lqa;->b:Lbsk;

    .line 634
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lqa;

    iget-object v1, v1, Lqa;->b:Lbsk;

    .line 632
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LpW;

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->N:Lbsk;

    .line 640
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LRo;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LCw;

    iget-object v2, v2, LCw;->N:Lbsk;

    .line 638
    invoke-static {v1, v2}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LCo;

    iget-object v2, p0, LRo;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LCw;

    iget-object v2, v2, LCw;->H:Lbsk;

    .line 646
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LRo;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LCw;

    iget-object v4, v4, LCw;->H:Lbsk;

    .line 644
    invoke-static {v2, v4}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LzQ;

    invoke-direct {v3, v0, v1, v2}, LQV;-><init>(LpW;LCo;LzQ;)V

    move-object v0, v3

    .line 651
    goto/16 :goto_0

    .line 653
    :sswitch_4
    new-instance v1, LRP;

    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->t:Lbsk;

    .line 656
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LRo;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->t:Lbsk;

    .line 654
    invoke-static {v0, v2}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    invoke-direct {v1, v0}, LRP;-><init>(Laja;)V

    move-object v0, v1

    .line 661
    goto/16 :goto_0

    .line 527
    :sswitch_data_0
    .sparse-switch
        0x2aa -> :sswitch_0
        0x2ab -> :sswitch_3
        0x2ad -> :sswitch_2
        0x2ae -> :sswitch_1
        0x2b6 -> :sswitch_4
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 720
    packed-switch p2, :pswitch_data_0

    .line 731
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 722
    :pswitch_0
    check-cast p1, LRl;

    .line 724
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 727
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 724
    invoke-virtual {p1, v0}, LRl;->provideLightOutHandler(Landroid/content/Context;)LalR;

    move-result-object v0

    return-object v0

    .line 720
    nop

    :pswitch_data_0
    .packed-switch 0x2b7
        :pswitch_0
    .end packed-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 480
    const-class v0, Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x5e

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LRo;->a(LbuP;LbuB;)V

    .line 483
    const-class v0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x5f

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LRo;->a(LbuP;LbuB;)V

    .line 486
    const-class v0, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x60

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LRo;->a(LbuP;LbuB;)V

    .line 489
    const-class v0, Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x61

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LRo;->a(LbuP;LbuB;)V

    .line 492
    const-class v0, Lcom/google/android/apps/docs/fragment/ContactSharingDialogFragmentImpl;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x62

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LRo;->a(LbuP;LbuB;)V

    .line 495
    const-class v0, Lcom/google/android/apps/docs/fragment/ErrorFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x63

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LRo;->a(LbuP;LbuB;)V

    .line 498
    const-class v0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x64

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LRo;->a(LbuP;LbuB;)V

    .line 501
    const-class v0, LRa;

    iget-object v1, p0, LRo;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LRo;->a(Ljava/lang/Class;Lbsk;)V

    .line 502
    const-class v0, LRh;

    iget-object v1, p0, LRo;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LRo;->a(Ljava/lang/Class;Lbsk;)V

    .line 503
    const-class v0, LSc;

    iget-object v1, p0, LRo;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LRo;->a(Ljava/lang/Class;Lbsk;)V

    .line 504
    const-class v0, LQV;

    iget-object v1, p0, LRo;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LRo;->a(Ljava/lang/Class;Lbsk;)V

    .line 505
    const-class v0, LRP;

    iget-object v1, p0, LRo;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LRo;->a(Ljava/lang/Class;Lbsk;)V

    .line 506
    const-class v0, LalR;

    iget-object v1, p0, LRo;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, LRo;->a(Ljava/lang/Class;Lbsk;)V

    .line 507
    iget-object v0, p0, LRo;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2aa

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 509
    iget-object v0, p0, LRo;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2ae

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 511
    iget-object v0, p0, LRo;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2ad

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 513
    iget-object v0, p0, LRo;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2ab

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 515
    iget-object v0, p0, LRo;->e:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2b6

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 517
    iget-object v0, p0, LRo;->f:Lbsk;

    const-class v1, LRl;

    const/16 v2, 0x2b7

    invoke-virtual {p0, v1, v2}, LRo;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 519
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 670
    packed-switch p1, :pswitch_data_0

    .line 714
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 672
    :pswitch_0
    check-cast p2, Lcom/google/android/apps/docs/fragment/NavigationFragment;

    .line 674
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LRo;

    .line 675
    invoke-virtual {v0, p2}, LRo;->a(Lcom/google/android/apps/docs/fragment/NavigationFragment;)V

    .line 716
    :goto_0
    return-void

    .line 678
    :pswitch_1
    check-cast p2, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;

    .line 680
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LRo;

    .line 681
    invoke-virtual {v0, p2}, LRo;->a(Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;)V

    goto :goto_0

    .line 684
    :pswitch_2
    check-cast p2, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;

    .line 686
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LRo;

    .line 687
    invoke-virtual {v0, p2}, LRo;->a(Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;)V

    goto :goto_0

    .line 690
    :pswitch_3
    check-cast p2, Lcom/google/android/apps/docs/fragment/DocListFragment;

    .line 692
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LRo;

    .line 693
    invoke-virtual {v0, p2}, LRo;->a(Lcom/google/android/apps/docs/fragment/DocListFragment;)V

    goto :goto_0

    .line 696
    :pswitch_4
    check-cast p2, Lcom/google/android/apps/docs/fragment/ContactSharingDialogFragmentImpl;

    .line 698
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LRo;

    .line 699
    invoke-virtual {v0, p2}, LRo;->a(Lcom/google/android/apps/docs/fragment/ContactSharingDialogFragmentImpl;)V

    goto :goto_0

    .line 702
    :pswitch_5
    check-cast p2, Lcom/google/android/apps/docs/fragment/ErrorFragment;

    .line 704
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LRo;

    .line 705
    invoke-virtual {v0, p2}, LRo;->a(Lcom/google/android/apps/docs/fragment/ErrorFragment;)V

    goto :goto_0

    .line 708
    :pswitch_6
    check-cast p2, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    .line 710
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LRo;

    .line 711
    invoke-virtual {v0, p2}, LRo;->a(Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;)V

    goto :goto_0

    .line 670
    :pswitch_data_0
    .packed-switch 0x5e
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/docs/fragment/ContactSharingDialogFragmentImpl;)V
    .locals 2

    .prologue
    .line 444
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    .line 445
    invoke-virtual {v0, p1}, LabP;->a(Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;)V

    .line 446
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->x:Lbsk;

    .line 449
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->x:Lbsk;

    .line 447
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/ContactSharingDialogFragmentImpl;->a:Laja;

    .line 453
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/fragment/DocListFragment;)V
    .locals 2

    .prologue
    .line 356
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LbiH;

    iget-object v0, v0, LbiH;->be:Lbsk;

    .line 359
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LbiH;

    iget-object v1, v1, LbiH;->be:Lbsk;

    .line 357
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbiP;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LbiP;

    .line 363
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LahE;

    iget-object v0, v0, LahE;->j:Lbsk;

    .line 366
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LahE;

    iget-object v1, v1, LahE;->j:Lbsk;

    .line 364
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahy;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lahy;

    .line 370
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 373
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 371
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LaGM;

    .line 377
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LCw;

    iget-object v0, v0, LCw;->af:Lbsk;

    .line 380
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->af:Lbsk;

    .line 378
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laqw;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Laqw;

    .line 384
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LME;

    iget-object v0, v0, LME;->a:Lbsk;

    .line 387
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LME;

    iget-object v1, v1, LME;->a:Lbsk;

    .line 385
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LMF;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LMF;

    .line 391
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LPt;

    iget-object v0, v0, LPt;->c:Lbsk;

    .line 394
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LPt;

    iget-object v1, v1, LPt;->c:Lbsk;

    .line 392
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPu;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LPu;

    .line 398
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LJT;

    iget-object v0, v0, LJT;->c:Lbsk;

    .line 401
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LJT;

    iget-object v1, v1, LJT;->c:Lbsk;

    .line 399
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJR;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LJR;

    .line 405
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LCw;

    iget-object v0, v0, LCw;->y:Lbsk;

    .line 408
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->y:Lbsk;

    .line 406
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCU;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LCU;

    .line 412
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LCw;

    iget-object v0, v0, LCw;->t:Lbsk;

    .line 415
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->t:Lbsk;

    .line 413
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCM;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LCM;

    .line 419
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LapK;

    iget-object v0, v0, LapK;->f:Lbsk;

    .line 422
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LapK;

    iget-object v1, v1, LapK;->f:Lbsk;

    .line 420
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapn;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lapn;

    .line 426
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->l:Lbsk;

    .line 429
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->l:Lbsk;

    .line 427
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamt;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lamt;

    .line 433
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 436
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 434
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LtK;

    .line 440
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/fragment/ErrorFragment;)V
    .locals 2

    .prologue
    .line 457
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->y:Lbsk;

    .line 460
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->y:Lbsk;

    .line 458
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lald;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/ErrorFragment;->a:Lald;

    .line 464
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;)V
    .locals 2

    .prologue
    .line 468
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LRo;

    iget-object v0, v0, LRo;->f:Lbsk;

    .line 471
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LRo;

    iget-object v1, v1, LRo;->f:Lbsk;

    .line 469
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LalR;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->a:LalR;

    .line 475
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;)V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->g:Lbsk;

    .line 185
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LabP;

    iget-object v1, v1, LabP;->g:Lbsk;

    .line 183
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacF;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LacF;

    .line 189
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LCw;

    iget-object v0, v0, LCw;->Z:Lbsk;

    .line 192
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->Z:Lbsk;

    .line 190
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQU;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LQU;

    .line 196
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->O:Lbsk;

    .line 199
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->O:Lbsk;

    .line 197
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LajO;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LajO;

    .line 203
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->y:Lbsk;

    .line 206
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->y:Lbsk;

    .line 204
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lald;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->b:Lald;

    .line 210
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->L:Lbsk;

    .line 213
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->L:Lbsk;

    .line 211
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Laja;

    .line 217
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LCw;

    iget-object v0, v0, LCw;->aa:Lbsk;

    .line 220
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->aa:Lbsk;

    .line 218
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LsC;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LsC;

    .line 224
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LUh;

    iget-object v0, v0, LUh;->f:Lbsk;

    .line 227
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LUh;

    iget-object v1, v1, LUh;->f:Lbsk;

    .line 225
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUi;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LUi;

    .line 231
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->y:Lbsk;

    .line 234
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->y:Lbsk;

    .line 232
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lald;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lald;

    .line 238
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->n:Lbsk;

    .line 241
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LabP;

    iget-object v1, v1, LabP;->n:Lbsk;

    .line 239
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacs;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lacs;

    .line 245
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LCw;

    iget-object v0, v0, LCw;->M:Lbsk;

    .line 248
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->M:Lbsk;

    .line 246
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBZ;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LBZ;

    .line 252
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lwc;

    iget-object v0, v0, Lwc;->j:Lbsk;

    .line 255
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lwc;

    iget-object v1, v1, Lwc;->j:Lbsk;

    .line 253
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvU;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LvU;

    .line 259
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 262
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 260
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LtK;

    .line 266
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LqD;

    iget-object v0, v0, LqD;->c:Lbsk;

    .line 269
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LqD;

    iget-object v1, v1, LqD;->c:Lbsk;

    .line 267
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqK;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LqK;

    .line 273
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 276
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 274
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LaGM;

    .line 280
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->N:Lbsk;

    .line 283
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->N:Lbsk;

    .line 281
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LamL;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LamL;

    .line 287
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LagN;

    iget-object v0, v0, LagN;->J:Lbsk;

    .line 290
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LagN;

    iget-object v1, v1, LagN;->J:Lbsk;

    .line 288
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lagl;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lagl;

    .line 294
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LCw;

    iget-object v0, v0, LCw;->t:Lbsk;

    .line 297
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->t:Lbsk;

    .line 295
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCM;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LCM;

    .line 301
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LCw;

    iget-object v0, v0, LCw;->ah:Lbsk;

    .line 304
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LRA;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LRA;

    .line 306
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->e:Lbsk;

    .line 309
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LabP;

    iget-object v1, v1, LabP;->e:Lbsk;

    .line 307
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacj;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lacj;

    .line 313
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 316
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 314
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LQr;

    .line 320
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->L:Lbsk;

    .line 323
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->L:Lbsk;

    .line 321
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKR;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LaKR;

    .line 327
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/fragment/NavigationFragment;)V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lyh;

    iget-object v0, v0, Lyh;->k:Lbsk;

    .line 71
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lyh;

    iget-object v1, v1, Lyh;->k:Lbsk;

    .line 69
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lye;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Lye;

    .line 75
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->f:Lbsk;

    .line 78
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->f:Lbsk;

    .line 76
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGR;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LaGR;

    .line 82
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LCw;

    iget-object v0, v0, LCw;->ao:Lbsk;

    .line 85
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->ao:Lbsk;

    .line 83
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LRJ;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LRJ;

    .line 89
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 92
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 90
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LtK;

    .line 96
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lwc;

    iget-object v0, v0, Lwc;->g:Lbsk;

    .line 99
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lwc;

    iget-object v1, v1, Lwc;->g:Lbsk;

    .line 97
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwm;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Lwm;

    .line 103
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LRo;

    iget-object v0, v0, LRo;->c:Lbsk;

    .line 106
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LRo;

    iget-object v1, v1, LRo;->c:Lbsk;

    .line 104
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSc;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LSc;

    .line 110
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LCw;

    iget-object v0, v0, LCw;->aa:Lbsk;

    .line 113
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->aa:Lbsk;

    .line 111
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LsC;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LsC;

    .line 117
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->O:Lbsk;

    .line 120
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->O:Lbsk;

    .line 118
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LajO;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LajO;

    .line 124
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LRo;

    iget-object v0, v0, LRo;->b:Lbsk;

    .line 127
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LRo;

    iget-object v1, v1, LRo;->b:Lbsk;

    .line 125
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LRh;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LRh;

    .line 131
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 134
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 132
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LQr;

    .line 138
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LagN;

    iget-object v0, v0, LagN;->F:Lbsk;

    .line 141
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LagN;

    iget-object v1, v1, LagN;->F:Lbsk;

    .line 139
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahh;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Lahh;

    .line 145
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 148
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 146
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LaGM;

    .line 152
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->j:Lbsk;

    .line 155
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->j:Lbsk;

    .line 153
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFO;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LaFO;

    .line 159
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LabP;

    iget-object v0, v0, LabP;->r:Lbsk;

    .line 162
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LabP;

    iget-object v1, v1, LabP;->r:Lbsk;

    .line 160
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LabF;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LabF;

    .line 166
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lqn;

    iget-object v0, v0, Lqn;->a:Lbsk;

    .line 169
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqd;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Lqd;

    .line 171
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->B:Lbsk;

    .line 174
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->B:Lbsk;

    .line 172
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamn;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Lamn;

    .line 178
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;)V
    .locals 2

    .prologue
    .line 331
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laak;

    iget-object v0, v0, Laak;->d:Lbsk;

    .line 334
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Laak;

    iget-object v1, v1, Laak;->d:Lbsk;

    .line 332
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laas;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a:Laas;

    .line 338
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 341
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 339
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a:LaGM;

    .line 345
    iget-object v0, p0, LRo;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->S:Lbsk;

    .line 348
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LRo;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->S:Lbsk;

    .line 346
    invoke-static {v0, v1}, LRo;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKM;

    iput-object v0, p1, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a:LaKM;

    .line 352
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 523
    return-void
.end method
