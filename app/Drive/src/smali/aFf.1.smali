.class public final enum LaFf;
.super Ljava/lang/Enum;
.source "PendingOperationTable.java"

# interfaces
.implements Lbjv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaFf;",
        ">;",
        "Lbjv",
        "<",
        "LaFr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaFf;

.field private static final synthetic a:[LaFf;

.field public static final enum b:LaFf;

.field public static final enum c:LaFf;

.field public static final enum d:LaFf;


# instance fields
.field private final a:LaFr;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/16 v6, 0x15

    .line 41
    new-instance v0, LaFf;

    const-string v1, "ACCOUNT_ID"

    invoke-static {}, LaFe;->b()LaFe;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "accountId"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 42
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    .line 43
    invoke-static {}, LaEe;->a()LaEe;

    move-result-object v4

    invoke-virtual {v3, v4}, LaFG;->a(LaFy;)LaFG;

    move-result-object v3

    .line 44
    invoke-virtual {v3}, LaFG;->a()LaFG;

    move-result-object v3

    .line 42
    invoke-virtual {v2, v6, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, LaFf;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFf;->a:LaFf;

    .line 46
    new-instance v0, LaFf;

    const-string v1, "PAYLOAD"

    invoke-static {}, LaFe;->b()LaFe;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "payload"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 47
    invoke-virtual {v3}, LaFG;->a()LaFG;

    move-result-object v3

    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, LaFf;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFf;->b:LaFf;

    .line 49
    new-instance v0, LaFf;

    const-string v1, "TIMESTAMP"

    invoke-static {}, LaFe;->b()LaFe;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "timestamp"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 50
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, LaFf;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFf;->c:LaFf;

    .line 52
    new-instance v0, LaFf;

    const-string v1, "RETRY_COUNT"

    invoke-static {}, LaFe;->b()LaFe;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "retryCount"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 53
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, LaFf;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFf;->d:LaFf;

    .line 40
    const/4 v0, 0x4

    new-array v0, v0, [LaFf;

    sget-object v1, LaFf;->a:LaFf;

    aput-object v1, v0, v7

    sget-object v1, LaFf;->b:LaFf;

    aput-object v1, v0, v8

    sget-object v1, LaFf;->c:LaFf;

    aput-object v1, v0, v9

    sget-object v1, LaFf;->d:LaFf;

    aput-object v1, v0, v10

    sput-object v0, LaFf;->a:[LaFf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaFt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 60
    invoke-virtual {p3}, LaFt;->a()LaFr;

    move-result-object v0

    iput-object v0, p0, LaFf;->a:LaFr;

    .line 61
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaFf;
    .locals 1

    .prologue
    .line 40
    const-class v0, LaFf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaFf;

    return-object v0
.end method

.method public static values()[LaFf;
    .locals 1

    .prologue
    .line 40
    sget-object v0, LaFf;->a:[LaFf;

    invoke-virtual {v0}, [LaFf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaFf;

    return-object v0
.end method


# virtual methods
.method public a()LaFr;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, LaFf;->a:LaFr;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, LaFf;->a()LaFr;

    move-result-object v0

    return-object v0
.end method
