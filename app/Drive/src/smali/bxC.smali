.class public LbxC;
.super Ljava/lang/Object;
.source "BaseGifImage.java"


# static fields
.field private static final b:[B


# instance fields
.field a:I

.field a:Z

.field private final a:[B

.field a:[I

.field b:I

.field b:Z

.field c:I

.field d:I

.field private e:I

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/16 v0, 0x300

    new-array v0, v0, [B

    sput-object v0, LbxC;->b:[B

    return-void
.end method

.method public constructor <init>([B)V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/16 v0, 0x100

    new-array v0, v0, [I

    iput-object v0, p0, LbxC;->a:[I

    .line 26
    iput-object p1, p0, LbxC;->a:[B

    .line 28
    new-instance v0, LbxE;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, LbxE;-><init>(LbxC;[BLbxD;)V

    .line 30
    :try_start_0
    invoke-direct {p0, v0}, LbxC;->a(Ljava/io/InputStream;)V

    .line 31
    invoke-virtual {v0}, LbxE;->a()I

    move-result v1

    iput v1, p0, LbxC;->a:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    :goto_0
    :try_start_1
    invoke-virtual {v0}, LbxE;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 41
    :goto_1
    return-void

    .line 32
    :catch_0
    move-exception v1

    .line 33
    const/4 v1, 0x1

    iput-boolean v1, p0, LbxC;->b:Z

    goto :goto_0

    .line 38
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private a(Ljava/io/InputStream;)I
    .locals 2

    .prologue
    .line 134
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    return v0
.end method

.method private a(Ljava/io/InputStream;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 66
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v2

    const/16 v3, 0x47

    if-ne v2, v3, :cond_2

    move v2, v1

    .line 67
    :goto_0
    if-eqz v2, :cond_3

    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v2

    const/16 v3, 0x49

    if-ne v2, v3, :cond_3

    move v2, v1

    .line 68
    :goto_1
    if-eqz v2, :cond_0

    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v2

    const/16 v3, 0x46

    if-ne v2, v3, :cond_0

    move v0, v1

    .line 69
    :cond_0
    if-nez v0, :cond_4

    .line 70
    iput-boolean v1, p0, LbxC;->b:Z

    .line 83
    :cond_1
    :goto_2
    return-void

    :cond_2
    move v2, v0

    .line 66
    goto :goto_0

    :cond_3
    move v2, v0

    .line 67
    goto :goto_1

    .line 75
    :cond_4
    const-wide/16 v0, 0x3

    invoke-virtual {p1, v0, v1}, Ljava/io/InputStream;->skip(J)J

    .line 77
    invoke-direct {p0, p1}, LbxC;->b(Ljava/io/InputStream;)V

    .line 79
    iget-boolean v0, p0, LbxC;->a:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LbxC;->b:Z

    if-nez v0, :cond_1

    .line 80
    iget-object v0, p0, LbxC;->a:[I

    iget v1, p0, LbxC;->b:I

    invoke-static {p1, v0, v1}, LbxC;->a(Ljava/io/InputStream;[II)Z

    .line 81
    iget-object v0, p0, LbxC;->a:[I

    iget v1, p0, LbxC;->d:I

    aget v0, v0, v1

    iput v0, p0, LbxC;->c:I

    goto :goto_2
.end method

.method static a(Ljava/io/InputStream;[II)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 109
    sget-object v3, LbxC;->b:[B

    monitor-enter v3

    .line 110
    mul-int/lit8 v1, p2, 0x3

    .line 111
    :try_start_0
    sget-object v2, LbxC;->b:[B

    const/4 v4, 0x0

    invoke-virtual {p0, v2, v4, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 112
    if-ge v2, v1, :cond_0

    .line 113
    monitor-exit v3

    .line 126
    :goto_0
    return v0

    :cond_0
    move v1, v0

    .line 117
    :goto_1
    if-ge v1, p2, :cond_1

    .line 118
    sget-object v2, LbxC;->b:[B

    add-int/lit8 v4, v0, 0x1

    aget-byte v0, v2, v0

    and-int/lit16 v5, v0, 0xff

    .line 119
    sget-object v0, LbxC;->b:[B

    add-int/lit8 v2, v4, 0x1

    aget-byte v0, v0, v4

    and-int/lit16 v4, v0, 0xff

    .line 120
    sget-object v6, LbxC;->b:[B

    add-int/lit8 v0, v2, 0x1

    aget-byte v2, v6, v2

    and-int/lit16 v6, v2, 0xff

    .line 121
    add-int/lit8 v2, v1, 0x1

    const/high16 v7, -0x1000000

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v5, v7

    shl-int/lit8 v4, v4, 0x8

    or-int/2addr v4, v5

    or-int/2addr v4, v6

    aput v4, p1, v1

    move v1, v2

    .line 122
    goto :goto_1

    .line 124
    :cond_1
    monitor-exit v3

    .line 126
    const/4 v0, 0x1

    goto :goto_0

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private b(Ljava/io/InputStream;)V
    .locals 2

    .prologue
    .line 90
    invoke-direct {p0, p1}, LbxC;->a(Ljava/io/InputStream;)I

    move-result v0

    iput v0, p0, LbxC;->e:I

    .line 91
    invoke-direct {p0, p1}, LbxC;->a(Ljava/io/InputStream;)I

    move-result v0

    iput v0, p0, LbxC;->f:I

    .line 93
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 94
    and-int/lit16 v0, v1, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LbxC;->a:Z

    .line 97
    const/4 v0, 0x2

    and-int/lit8 v1, v1, 0x7

    shl-int/2addr v0, v1

    iput v0, p0, LbxC;->b:I

    .line 98
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    iput v0, p0, LbxC;->d:I

    .line 99
    const-wide/16 v0, 0x1

    invoke-virtual {p1, v0, v1}, Ljava/io/InputStream;->skip(J)J

    .line 100
    return-void

    .line 94
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, LbxC;->e:I

    return v0
.end method

.method public a()[B
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, LbxC;->a:[B

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, LbxC;->f:I

    return v0
.end method
