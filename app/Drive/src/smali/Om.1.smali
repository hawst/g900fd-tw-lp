.class LOm;
.super Ljava/lang/Object;
.source "StreamingMediaProxy.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:LOk;

.field private final a:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(LOk;)V
    .locals 1

    .prologue
    .line 153
    iput-object p1, p0, LOm;->a:LOk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, LOm;->a:Ljava/util/concurrent/ExecutorService;

    .line 155
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 160
    :goto_0
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 161
    iget-object v0, p0, LOm;->a:LOk;

    invoke-static {v0}, LOk;->a(LOk;)Ljava/net/ServerSocket;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v0

    .line 162
    iget-object v1, p0, LOm;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v2, LOo;

    iget-object v3, p0, LOm;->a:LOk;

    invoke-direct {v2, v3, v0}, LOo;-><init>(LOk;Ljava/net/Socket;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 164
    :catch_0
    move-exception v0

    .line 165
    :try_start_1
    invoke-virtual {v0}, Ljava/net/SocketException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Socket closed"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 166
    const-string v0, "StreamingMediaProxy"

    const-string v1, "Server socket closed"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 173
    :goto_1
    iget-object v0, p0, LOm;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 175
    :goto_2
    return-void

    .line 173
    :cond_0
    iget-object v0, p0, LOm;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    goto :goto_2

    .line 168
    :cond_1
    :try_start_2
    const-string v1, "StreamingMediaProxy"

    const-string v2, "SocketException when accepting a new connection"

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 173
    :catchall_0
    move-exception v0

    iget-object v1, p0, LOm;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    throw v0

    .line 170
    :catch_1
    move-exception v0

    .line 171
    :try_start_3
    const-string v1, "StreamingMediaProxy"

    const-string v2, "IOException when accepting a new connection"

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 173
    iget-object v0, p0, LOm;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    goto :goto_2
.end method
