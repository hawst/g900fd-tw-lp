.class public final enum LaGy;
.super Ljava/lang/Enum;
.source "Entry.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaGy;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaGy;

.field private static final synthetic a:[LaGy;

.field public static final enum b:LaGy;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum c:LaGy;

.field public static final enum d:LaGy;

.field public static final enum e:LaGy;


# instance fields
.field private final a:J


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 75
    new-instance v0, LaGy;

    const-string v1, "UNTRASHED"

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v4, v2, v3}, LaGy;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, LaGy;->a:LaGy;

    .line 80
    new-instance v0, LaGy;

    const-string v1, "LEGACY_TRASHED"

    const-wide/16 v2, 0x1

    invoke-direct {v0, v1, v5, v2, v3}, LaGy;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, LaGy;->b:LaGy;

    .line 85
    new-instance v0, LaGy;

    const-string v1, "IMPLICITLY_TRASHED"

    const-wide/16 v2, 0x2

    invoke-direct {v0, v1, v6, v2, v3}, LaGy;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, LaGy;->c:LaGy;

    .line 89
    new-instance v0, LaGy;

    const-string v1, "EXPLICITLY_TRASHED"

    const-wide/16 v2, 0x3

    invoke-direct {v0, v1, v7, v2, v3}, LaGy;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, LaGy;->d:LaGy;

    .line 93
    new-instance v0, LaGy;

    const-string v1, "UNSUBSCRIBED"

    const-wide/16 v2, 0x4

    invoke-direct {v0, v1, v8, v2, v3}, LaGy;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, LaGy;->e:LaGy;

    .line 71
    const/4 v0, 0x5

    new-array v0, v0, [LaGy;

    sget-object v1, LaGy;->a:LaGy;

    aput-object v1, v0, v4

    sget-object v1, LaGy;->b:LaGy;

    aput-object v1, v0, v5

    sget-object v1, LaGy;->c:LaGy;

    aput-object v1, v0, v6

    sget-object v1, LaGy;->d:LaGy;

    aput-object v1, v0, v7

    sget-object v1, LaGy;->e:LaGy;

    aput-object v1, v0, v8

    sput-object v0, LaGy;->a:[LaGy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IJ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)V"
        }
    .end annotation

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 99
    iput-wide p3, p0, LaGy;->a:J

    .line 100
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaGy;
    .locals 1

    .prologue
    .line 71
    const-class v0, LaGy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaGy;

    return-object v0
.end method

.method public static values()[LaGy;
    .locals 1

    .prologue
    .line 71
    sget-object v0, LaGy;->a:[LaGy;

    invoke-virtual {v0}, [LaGy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaGy;

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 103
    iget-wide v0, p0, LaGy;->a:J

    return-wide v0
.end method
