.class public final enum LID;
.super Ljava/lang/Enum;
.source "QuotaRange.java"

# interfaces
.implements LIy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LID;",
        ">;",
        "LIy;"
    }
.end annotation


# static fields
.field public static final enum a:LID;

.field private static final synthetic a:[LID;

.field public static final enum b:LID;

.field public static final enum c:LID;

.field public static final enum d:LID;

.field public static final enum e:LID;

.field public static final enum f:LID;


# instance fields
.field private final a:LIr;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 14
    new-instance v0, LID;

    const-string v1, "LARGEST"

    sget v2, Lxi;->quota_range_largest:I

    invoke-direct {v0, v1, v4, v2}, LID;-><init>(Ljava/lang/String;II)V

    sput-object v0, LID;->a:LID;

    .line 15
    new-instance v0, LID;

    const-string v1, "LARGE"

    sget v2, Lxi;->quota_range_large:I

    invoke-direct {v0, v1, v5, v2}, LID;-><init>(Ljava/lang/String;II)V

    sput-object v0, LID;->b:LID;

    .line 16
    new-instance v0, LID;

    const-string v1, "LARGER"

    sget v2, Lxi;->quota_range_larger:I

    invoke-direct {v0, v1, v6, v2}, LID;-><init>(Ljava/lang/String;II)V

    sput-object v0, LID;->c:LID;

    .line 17
    new-instance v0, LID;

    const-string v1, "MEDIUM"

    sget v2, Lxi;->quota_range_medium:I

    invoke-direct {v0, v1, v7, v2}, LID;-><init>(Ljava/lang/String;II)V

    sput-object v0, LID;->d:LID;

    .line 18
    new-instance v0, LID;

    const-string v1, "SMALL"

    sget v2, Lxi;->quota_range_small:I

    invoke-direct {v0, v1, v8, v2}, LID;-><init>(Ljava/lang/String;II)V

    sput-object v0, LID;->e:LID;

    .line 19
    new-instance v0, LID;

    const-string v1, "ZERO_QUOTA"

    const/4 v2, 0x5

    sget v3, Lxi;->quota_range_undefined:I

    invoke-direct {v0, v1, v2, v3}, LID;-><init>(Ljava/lang/String;II)V

    sput-object v0, LID;->f:LID;

    .line 13
    const/4 v0, 0x6

    new-array v0, v0, [LID;

    sget-object v1, LID;->a:LID;

    aput-object v1, v0, v4

    sget-object v1, LID;->b:LID;

    aput-object v1, v0, v5

    sget-object v1, LID;->c:LID;

    aput-object v1, v0, v6

    sget-object v1, LID;->d:LID;

    aput-object v1, v0, v7

    sget-object v1, LID;->e:LID;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LID;->f:LID;

    aput-object v2, v0, v1

    sput-object v0, LID;->a:[LID;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 24
    new-instance v0, LIr;

    invoke-direct {v0, p3}, LIr;-><init>(I)V

    iput-object v0, p0, LID;->a:LIr;

    .line 25
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LID;
    .locals 1

    .prologue
    .line 13
    const-class v0, LID;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LID;

    return-object v0
.end method

.method public static values()[LID;
    .locals 1

    .prologue
    .line 13
    sget-object v0, LID;->a:[LID;

    invoke-virtual {v0}, [LID;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LID;

    return-object v0
.end method


# virtual methods
.method public a(ZZ)LIB;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LID;->a:LIr;

    invoke-virtual {v0, p1, p2}, LIr;->a(ZZ)LIB;

    move-result-object v0

    return-object v0
.end method
