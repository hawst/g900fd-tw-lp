.class public abstract Lbse;
.super Ljava/lang/Object;
.source "GellyInjectorStoreBase.java"


# instance fields
.field private final a:LbrS;


# direct methods
.method public constructor <init>(LbrS;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lbse;->a:LbrS;

    .line 51
    return-void
.end method

.method protected static a(Lbuv;Ljava/lang/Class;)Lbsk;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbuv",
            "<TT;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "Lbsk",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 81
    new-instance v0, Lbsk;

    invoke-direct {v0, p0, p1}, Lbsk;-><init>(Lbuv;Ljava/lang/Class;)V

    return-object v0
.end method

.method protected static a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "Lbsk",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 86
    new-instance v0, Lbsk;

    invoke-direct {v0, p0, p1}, Lbsk;-><init>(Ljava/lang/Class;Ljava/lang/Class;)V

    return-object v0
.end method

.method protected static final a(LbuE;)LbuE;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbuE",
            "<TT;>;)",
            "LbuE",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 76
    const-string v0, "provider=null"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lbso;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuE;

    return-object v0
.end method

.method protected static a(Lbxw;)LbuE;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbxw",
            "<+",
            "Lbxw",
            "<+TT;>;>;)",
            "LbuE",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 91
    new-instance v0, Lbsg;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lbsg;-><init>(Lbxw;Lbsf;)V

    return-object v0
.end method

.method protected static final a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lbxw",
            "<+TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 67
    const-string v0, "provider=%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Lbso;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected static a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 95
    if-nez p0, :cond_0

    if-nez p1, :cond_0

    .line 96
    const/4 v0, 0x1

    .line 100
    :goto_0
    return v0

    .line 97
    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_2

    .line 98
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 100
    :cond_2
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method protected a(Ljava/lang/Class;I)Lbxw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<*>;I)",
            "Lbxw",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 105
    new-instance v0, Lbsj;

    invoke-direct {v0, p0, p1, p2}, Lbsj;-><init>(Lbse;Ljava/lang/Class;I)V

    return-object v0
.end method

.method protected abstract a(I)Ljava/lang/Object;
.end method

.method a(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lbse;->a:LbrS;

    invoke-virtual {v0, p1}, LbrS;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected abstract a(Ljava/lang/Object;I)Ljava/lang/Object;
.end method

.method protected abstract a()V
.end method

.method protected abstract a(ILjava/lang/Object;)V
.end method

.method protected final a(LbuP;LbuB;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbuP",
            "<TT;>;",
            "LbuB",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lbse;->a:LbrS;

    invoke-virtual {v0, p1, p2}, LbrS;->a(LbuP;LbuB;)V

    .line 56
    return-void
.end method

.method protected final a(Lbuv;Lbsk;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbuv",
            "<TT;>;",
            "Lbsk",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lbse;->a:LbrS;

    invoke-virtual {v0, p1, p2}, LbrS;->a(Lbuv;Lbsk;)V

    .line 60
    return-void
.end method

.method protected final a(Ljava/lang/Class;Lbsk;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lbsk",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lbse;->a:LbrS;

    invoke-virtual {v0, p1, p2}, LbrS;->a(Ljava/lang/Class;Lbsk;)V

    .line 64
    return-void
.end method

.method protected abstract b()V
.end method
