.class public Laim;
.super Ljava/lang/Object;
.source "ChangeLogSyncAlgorithm.java"

# interfaces
.implements Laiq;


# instance fields
.field private final a:LaFM;

.field private final a:LaGM;

.field private final a:LagE;

.field private final a:Lagl;


# direct methods
.method public constructor <init>(LaFM;Lagl;LagE;LaGM;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Laim;->a:LaFM;

    .line 35
    iput-object p3, p0, Laim;->a:LagE;

    .line 36
    iput-object p2, p0, Laim;->a:Lagl;

    .line 37
    iput-object p4, p0, Laim;->a:LaGM;

    .line 38
    return-void
.end method


# virtual methods
.method public a(Lagx;Landroid/content/SyncResult;)V
    .locals 6

    .prologue
    .line 43
    iget-object v0, p0, Laim;->a:LagE;

    iget-object v1, p0, Laim;->a:LaFM;

    iget-object v2, p0, Laim;->a:LagE;

    iget-object v3, p0, Laim;->a:Lagl;

    iget-object v4, p0, Laim;->a:LaFM;

    const/4 v5, 0x1

    .line 45
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 44
    invoke-interface {v2, v3, p2, v4, v5}, LagE;->a(Lagl;Landroid/content/SyncResult;LaFM;Ljava/lang/Boolean;)Lagy;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LagE;->a(LaFM;Lagy;)Lagy;

    move-result-object v0

    .line 47
    iget-object v1, p0, Laim;->a:LaGM;

    iget-object v2, p0, Laim;->a:LaFM;

    invoke-virtual {v2}, LaFM;->a()LaFO;

    move-result-object v2

    invoke-interface {v1, v2}, LaGM;->a(LaFO;)LaFQ;

    move-result-object v1

    .line 48
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "https://docs.google.com/feeds/default/private/changes?showroot=true&start-index="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, LaFQ;->a()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 49
    iget-object v2, p0, Laim;->a:LaFM;

    invoke-virtual {v2}, LaFM;->a()LaFO;

    move-result-object v2

    const v3, 0x7fffffff

    invoke-interface {p1, v1, v2, v0, v3}, Lagx;->a(Ljava/lang/String;LaFO;Lagy;I)V

    .line 51
    return-void
.end method

.method public a(Landroid/content/SyncResult;Z)V
    .locals 0

    .prologue
    .line 56
    return-void
.end method
