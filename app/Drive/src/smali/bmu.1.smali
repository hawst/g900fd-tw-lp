.class public abstract Lbmu;
.super LbmL;
.source "ImmutableBiMap.java"

# interfaces
.implements LblT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LbmL",
        "<TK;TV;>;",
        "LblT",
        "<TK;TV;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 215
    invoke-direct {p0}, LbmL;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)Lbmu;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(TK;TV;)",
            "Lbmu",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 59
    const-string v0, "null key in entry: null=%s"

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LbiT;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    const-string v0, "null value in entry: %s=null"

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {p1, v0, v1}, LbiT;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    new-instance v0, Lbqc;

    invoke-direct {v0, p0, p1}, Lbqc;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static b()Lbmu;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Lbmu",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 52
    sget-object v0, Lbmd;->a:Lbmd;

    return-object v0
.end method


# virtual methods
.method public abstract a()Lbmu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbmu",
            "<TV;TK;>;"
        }
    .end annotation
.end method

.method public synthetic a()Lbmv;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, Lbmu;->a_()LbmY;

    move-result-object v0

    return-object v0
.end method

.method public a_()LbmY;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmY",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 232
    invoke-virtual {p0}, Lbmu;->a()Lbmu;

    move-result-object v0

    invoke-virtual {v0}, Lbmu;->c()LbmY;

    move-result-object v0

    return-object v0
.end method

.method public synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, Lbmu;->a_()LbmY;

    move-result-object v0

    return-object v0
.end method
