.class public final Laae;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LZU;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LZY;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laac;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LZV;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laab;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 41
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 42
    iput-object p1, p0, Laae;->a:LbrA;

    .line 43
    const-class v0, LZU;

    invoke-static {v0, v2}, Laae;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laae;->a:Lbsk;

    .line 46
    const-class v0, LZY;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, Laae;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laae;->b:Lbsk;

    .line 49
    const-class v0, Laac;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, Laae;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laae;->c:Lbsk;

    .line 52
    const-class v0, LZV;

    invoke-static {v0, v2}, Laae;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laae;->d:Lbsk;

    .line 55
    const-class v0, Laab;

    const-class v1, LbuO;

    invoke-static {v0, v1}, Laae;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laae;->e:Lbsk;

    .line 58
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 172
    sparse-switch p1, :sswitch_data_0

    .line 250
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 174
    :sswitch_0
    new-instance v4, LZY;

    iget-object v0, p0, Laae;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->b:Lbsk;

    .line 177
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Laae;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->b:Lbsk;

    .line 175
    invoke-static {v0, v1}, Laae;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Laae;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->S:Lbsk;

    .line 183
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Laae;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LalC;

    iget-object v2, v2, LalC;->S:Lbsk;

    .line 181
    invoke-static {v1, v2}, Laae;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaKM;

    iget-object v2, p0, Laae;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 189
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Laae;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LQH;

    iget-object v3, v3, LQH;->d:Lbsk;

    .line 187
    invoke-static {v2, v3}, Laae;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LQr;

    iget-object v3, p0, Laae;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LpG;

    iget-object v3, v3, LpG;->m:Lbsk;

    .line 195
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, Laae;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LpG;

    iget-object v5, v5, LpG;->m:Lbsk;

    .line 193
    invoke-static {v3, v5}, Laae;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LtK;

    invoke-direct {v4, v0, v1, v2, v3}, LZY;-><init>(Landroid/content/Context;LaKM;LQr;LtK;)V

    move-object v0, v4

    .line 248
    :goto_0
    return-object v0

    .line 202
    :sswitch_1
    new-instance v1, Laac;

    iget-object v0, p0, Laae;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->t:Lbsk;

    .line 205
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Laae;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->t:Lbsk;

    .line 203
    invoke-static {v0, v2}, Laae;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    invoke-direct {v1, v0}, Laac;-><init>(Laja;)V

    move-object v0, v1

    .line 210
    goto :goto_0

    .line 212
    :sswitch_2
    new-instance v0, LZV;

    iget-object v1, p0, Laae;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->b:Lbsk;

    .line 215
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Laae;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->b:Lbsk;

    .line 213
    invoke-static {v1, v2}, Laae;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, Laae;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Laae;

    iget-object v2, v2, Laae;->b:Lbsk;

    .line 221
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Laae;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Laae;

    iget-object v3, v3, Laae;->b:Lbsk;

    .line 219
    invoke-static {v2, v3}, Laae;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LZY;

    iget-object v3, p0, Laae;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->m:Lbsk;

    .line 227
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, Laae;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LalC;

    iget-object v4, v4, LalC;->m:Lbsk;

    .line 225
    invoke-static {v3, v4}, Laae;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lals;

    iget-object v4, p0, Laae;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lak;

    iget-object v4, v4, Lak;->a:Lbsk;

    .line 233
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, Laae;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Lak;

    iget-object v5, v5, Lak;->a:Lbsk;

    .line 231
    invoke-static {v4, v5}, Laae;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LM;

    iget-object v5, p0, Laae;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LaGH;

    iget-object v5, v5, LaGH;->l:Lbsk;

    .line 239
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, Laae;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LaGH;

    iget-object v6, v6, LaGH;->l:Lbsk;

    .line 237
    invoke-static {v5, v6}, Laae;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LaGM;

    invoke-direct/range {v0 .. v5}, LZV;-><init>(Landroid/content/Context;LZY;Lals;LM;LaGM;)V

    goto/16 :goto_0

    .line 246
    :sswitch_3
    new-instance v0, Laab;

    invoke-direct {v0}, Laab;-><init>()V

    goto/16 :goto_0

    .line 172
    :sswitch_data_0
    .sparse-switch
        0x66 -> :sswitch_0
        0x6b -> :sswitch_1
        0x6c -> :sswitch_2
        0x6f -> :sswitch_3
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 283
    .line 285
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 140
    const-class v0, Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x14

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Laae;->a(LbuP;LbuB;)V

    .line 143
    const-class v0, Lcom/google/android/apps/docs/receivers/AppPackageAddRemoveReceiver;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x15

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Laae;->a(LbuP;LbuB;)V

    .line 146
    const-class v0, Lcom/google/android/apps/docs/receivers/TransferNotificationActionReceiver;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x16

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, Laae;->a(LbuP;LbuB;)V

    .line 149
    const-class v0, LZU;

    iget-object v1, p0, Laae;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, Laae;->a(Ljava/lang/Class;Lbsk;)V

    .line 150
    const-class v0, LZY;

    iget-object v1, p0, Laae;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, Laae;->a(Ljava/lang/Class;Lbsk;)V

    .line 151
    const-class v0, Laac;

    iget-object v1, p0, Laae;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, Laae;->a(Ljava/lang/Class;Lbsk;)V

    .line 152
    const-class v0, LZV;

    iget-object v1, p0, Laae;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, Laae;->a(Ljava/lang/Class;Lbsk;)V

    .line 153
    const-class v0, Laab;

    iget-object v1, p0, Laae;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, Laae;->a(Ljava/lang/Class;Lbsk;)V

    .line 154
    iget-object v0, p0, Laae;->a:Lbsk;

    iget-object v1, p0, Laae;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LatG;

    iget-object v1, v1, LatG;->a:Lbsk;

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 156
    iget-object v0, p0, Laae;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x66

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 158
    iget-object v0, p0, Laae;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x6b

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 160
    iget-object v0, p0, Laae;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x6c

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 162
    iget-object v0, p0, Laae;->e:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x6f

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 164
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 257
    packed-switch p1, :pswitch_data_0

    .line 277
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 259
    :pswitch_0
    check-cast p2, Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;

    .line 261
    iget-object v0, p0, Laae;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laae;

    .line 262
    invoke-virtual {v0, p2}, Laae;->a(Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;)V

    .line 279
    :goto_0
    return-void

    .line 265
    :pswitch_1
    check-cast p2, Lcom/google/android/apps/docs/receivers/AppPackageAddRemoveReceiver;

    .line 267
    iget-object v0, p0, Laae;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laae;

    .line 268
    invoke-virtual {v0, p2}, Laae;->a(Lcom/google/android/apps/docs/receivers/AppPackageAddRemoveReceiver;)V

    goto :goto_0

    .line 271
    :pswitch_2
    check-cast p2, Lcom/google/android/apps/docs/receivers/TransferNotificationActionReceiver;

    .line 273
    iget-object v0, p0, Laae;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laae;

    .line 274
    invoke-virtual {v0, p2}, Laae;->a(Lcom/google/android/apps/docs/receivers/TransferNotificationActionReceiver;)V

    goto :goto_0

    .line 257
    :pswitch_data_0
    .packed-switch 0x14
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;)V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Laae;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laae;

    iget-object v0, v0, Laae;->a:Lbsk;

    .line 67
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Laae;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Laae;

    iget-object v1, v1, Laae;->a:Lbsk;

    .line 65
    invoke-static {v0, v1}, Laae;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZU;

    iput-object v0, p1, Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;->a:LZU;

    .line 71
    iget-object v0, p0, Laae;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 74
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Laae;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 72
    invoke-static {v0, v1}, Laae;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p1, Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;->a:LaGM;

    .line 78
    iget-object v0, p0, Laae;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LSK;

    iget-object v0, v0, LSK;->b:Lbsk;

    .line 81
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Laae;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LSK;

    iget-object v1, v1, LSK;->b:Lbsk;

    .line 79
    invoke-static {v0, v1}, Laae;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSF;

    iput-object v0, p1, Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;->a:LSF;

    .line 85
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/receivers/AppPackageAddRemoveReceiver;)V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Laae;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laae;

    iget-object v0, v0, Laae;->e:Lbsk;

    .line 92
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Laae;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Laae;

    iget-object v1, v1, Laae;->e:Lbsk;

    .line 90
    invoke-static {v0, v1}, Laae;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laab;

    iput-object v0, p1, Lcom/google/android/apps/docs/receivers/AppPackageAddRemoveReceiver;->a:Laab;

    .line 96
    iget-object v0, p0, Laae;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laae;

    iget-object v0, v0, Laae;->b:Lbsk;

    .line 99
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Laae;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Laae;

    iget-object v1, v1, Laae;->b:Lbsk;

    .line 97
    invoke-static {v0, v1}, Laae;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZY;

    iput-object v0, p1, Lcom/google/android/apps/docs/receivers/AppPackageAddRemoveReceiver;->a:LZY;

    .line 103
    iget-object v0, p0, Laae;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LUB;

    iget-object v0, v0, LUB;->c:Lbsk;

    .line 106
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Laae;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LUB;

    iget-object v1, v1, LUB;->c:Lbsk;

    .line 104
    invoke-static {v0, v1}, Laae;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUJ;

    iput-object v0, p1, Lcom/google/android/apps/docs/receivers/AppPackageAddRemoveReceiver;->a:LUJ;

    .line 110
    iget-object v0, p0, Laae;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->h:Lbsk;

    .line 113
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Laae;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->h:Lbsk;

    .line 111
    invoke-static {v0, v1}, Laae;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUI;

    iput-object v0, p1, Lcom/google/android/apps/docs/receivers/AppPackageAddRemoveReceiver;->a:LUI;

    .line 117
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/receivers/TransferNotificationActionReceiver;)V
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Laae;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LahE;

    iget-object v0, v0, LahE;->k:Lbsk;

    .line 124
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Laae;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LahE;

    iget-object v1, v1, LahE;->k:Lbsk;

    .line 122
    invoke-static {v0, v1}, Laae;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahK;

    iput-object v0, p1, Lcom/google/android/apps/docs/receivers/TransferNotificationActionReceiver;->a:LahK;

    .line 128
    iget-object v0, p0, Laae;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->B:Lbsk;

    .line 131
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Laae;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->B:Lbsk;

    .line 129
    invoke-static {v0, v1}, Laae;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamn;

    iput-object v0, p1, Lcom/google/android/apps/docs/receivers/TransferNotificationActionReceiver;->a:Lamn;

    .line 135
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 168
    return-void
.end method
