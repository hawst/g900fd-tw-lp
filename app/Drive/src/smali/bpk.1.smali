.class public final Lbpk;
.super Ljava/lang/Object;
.source "Multisets.java"


# static fields
.field private static final a:Lbpw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbpw",
            "<",
            "Lbpj",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1082
    new-instance v0, Lbpl;

    invoke-direct {v0}, Lbpl;-><init>()V

    sput-object v0, Lbpk;->a:Lbpw;

    return-void
.end method

.method static a(Lbpi;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbpi",
            "<*>;)I"
        }
    .end annotation

    .prologue
    .line 1064
    const-wide/16 v0, 0x0

    .line 1065
    invoke-interface {p0}, Lbpi;->a()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpj;

    .line 1066
    invoke-interface {v0}, Lbpj;->a()I

    move-result v0

    int-to-long v0, v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 1067
    goto :goto_0

    .line 1068
    :cond_0
    invoke-static {v2, v3}, Lbsy;->b(J)I

    move-result v0

    return v0
.end method

.method static a(Lbpi;Ljava/lang/Object;I)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lbpi",
            "<TE;>;TE;I)I"
        }
    .end annotation

    .prologue
    .line 886
    const-string v0, "count"

    invoke-static {p2, v0}, Lbpk;->a(ILjava/lang/String;)V

    .line 888
    invoke-interface {p0, p1}, Lbpi;->a(Ljava/lang/Object;)I

    move-result v0

    .line 890
    sub-int v1, p2, v0

    .line 891
    if-lez v1, :cond_1

    .line 892
    invoke-interface {p0, p1, v1}, Lbpi;->a(Ljava/lang/Object;I)I

    .line 897
    :cond_0
    :goto_0
    return v0

    .line 893
    :cond_1
    if-gez v1, :cond_0

    .line 894
    neg-int v1, v1

    invoke-interface {p0, p1, v1}, Lbpi;->b(Ljava/lang/Object;I)I

    goto :goto_0
.end method

.method static a(Ljava/lang/Iterable;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<*>;)I"
        }
    .end annotation

    .prologue
    .line 376
    instance-of v0, p0, Lbpi;

    if-eqz v0, :cond_0

    .line 377
    check-cast p0, Lbpi;

    invoke-interface {p0}, Lbpi;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    .line 379
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xb

    goto :goto_0
.end method

.method static a(Ljava/lang/Iterable;)Lbpi;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<TT;>;)",
            "Lbpi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1079
    check-cast p0, Lbpi;

    return-object p0
.end method

.method public static a(Ljava/lang/Object;I)Lbpj;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;I)",
            "Lbpj",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 219
    new-instance v0, Lbpq;

    invoke-direct {v0, p0, p1}, Lbpq;-><init>(Ljava/lang/Object;I)V

    return-object v0
.end method

.method static a(Lbpi;)Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lbpi",
            "<TE;>;)",
            "Ljava/util/Iterator",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 1013
    new-instance v0, Lbpr;

    invoke-interface {p0}, Lbpi;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lbpr;-><init>(Lbpi;Ljava/util/Iterator;)V

    return-object v0
.end method

.method static a(ILjava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1072
    if-ltz p0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "%s cannot be negative: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v2

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-static {v0, v3, v4}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1073
    return-void

    :cond_0
    move v0, v2

    .line 1072
    goto :goto_0
.end method

.method static a(Lbpi;Ljava/lang/Object;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbpi",
            "<*>;",
            "Ljava/lang/Object;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 819
    if-ne p1, p0, :cond_0

    move v0, v1

    .line 840
    :goto_0
    return v0

    .line 822
    :cond_0
    instance-of v0, p1, Lbpi;

    if-eqz v0, :cond_5

    .line 823
    check-cast p1, Lbpi;

    .line 830
    invoke-interface {p0}, Lbpi;->size()I

    move-result v0

    invoke-interface {p1}, Lbpi;->size()I

    move-result v3

    if-ne v0, v3, :cond_1

    invoke-interface {p0}, Lbpi;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-interface {p1}, Lbpi;->a()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    if-eq v0, v3, :cond_2

    :cond_1
    move v0, v2

    .line 831
    goto :goto_0

    .line 833
    :cond_2
    invoke-interface {p1}, Lbpi;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpj;

    .line 834
    invoke-interface {v0}, Lbpj;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p0, v4}, Lbpi;->a(Ljava/lang/Object;)I

    move-result v4

    invoke-interface {v0}, Lbpj;->a()I

    move-result v0

    if-eq v4, v0, :cond_3

    move v0, v2

    .line 835
    goto :goto_0

    :cond_4
    move v0, v1

    .line 838
    goto :goto_0

    :cond_5
    move v0, v2

    .line 840
    goto :goto_0
.end method

.method static a(Lbpi;Ljava/lang/Object;II)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lbpi",
            "<TE;>;TE;II)Z"
        }
    .end annotation

    .prologue
    .line 904
    const-string v0, "oldCount"

    invoke-static {p2, v0}, Lbpk;->a(ILjava/lang/String;)V

    .line 905
    const-string v0, "newCount"

    invoke-static {p3, v0}, Lbpk;->a(ILjava/lang/String;)V

    .line 907
    invoke-interface {p0, p1}, Lbpi;->a(Ljava/lang/Object;)I

    move-result v0

    if-ne v0, p2, :cond_0

    .line 908
    invoke-interface {p0, p1, p3}, Lbpi;->c(Ljava/lang/Object;I)I

    .line 909
    const/4 v0, 0x1

    .line 911
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(Lbpi;Ljava/util/Collection;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lbpi",
            "<TE;>;",
            "Ljava/util/Collection",
            "<+TE;>;)Z"
        }
    .end annotation

    .prologue
    .line 847
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 848
    const/4 v0, 0x0

    .line 858
    :goto_0
    return v0

    .line 850
    :cond_0
    instance-of v0, p1, Lbpi;

    if-eqz v0, :cond_1

    .line 851
    invoke-static {p1}, Lbpk;->a(Ljava/lang/Iterable;)Lbpi;

    move-result-object v0

    .line 852
    invoke-interface {v0}, Lbpi;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpj;

    .line 853
    invoke-interface {v0}, Lbpj;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Lbpj;->a()I

    move-result v0

    invoke-interface {p0, v2, v0}, Lbpi;->a(Ljava/lang/Object;I)I

    goto :goto_1

    .line 856
    :cond_1
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {p0, v0}, Lbnr;->a(Ljava/util/Collection;Ljava/util/Iterator;)Z

    .line 858
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static b(Lbpi;Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbpi",
            "<*>;",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 865
    instance-of v0, p1, Lbpi;

    if-eqz v0, :cond_0

    check-cast p1, Lbpi;

    invoke-interface {p1}, Lbpi;->b()Ljava/util/Set;

    move-result-object p1

    .line 868
    :cond_0
    invoke-interface {p0}, Lbpi;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method static c(Lbpi;Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbpi",
            "<*>;",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 875
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 876
    instance-of v0, p1, Lbpi;

    if-eqz v0, :cond_0

    check-cast p1, Lbpi;

    invoke-interface {p1}, Lbpi;->b()Ljava/util/Set;

    move-result-object p1

    .line 879
    :cond_0
    invoke-interface {p0}, Lbpi;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method
