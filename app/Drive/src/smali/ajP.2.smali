.class public LajP;
.super Ljava/lang/Object;
.source "AccountCapabilityFactoryImpl.java"

# interfaces
.implements LajO;


# instance fields
.field private final a:LaGM;

.field private final a:LaeH;

.field private final a:LafE;

.field private a:LajN;

.field private final a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LaFO;",
            "LajN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Laja;LaeH;LaGM;Ljava/util/Map;LafE;)V
    .locals 0
    .param p4    # Ljava/util/Map;
        .annotation runtime Lann;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LaeH;",
            "LaGM;",
            "Ljava/util/Map",
            "<",
            "LaFO;",
            "LajN;",
            ">;",
            "LafE;",
            ")V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, LajP;->a:Laja;

    .line 52
    iput-object p2, p0, LajP;->a:LaeH;

    .line 53
    iput-object p3, p0, LajP;->a:LaGM;

    .line 54
    iput-object p4, p0, LajP;->a:Ljava/util/Map;

    .line 55
    iput-object p5, p0, LajP;->a:LafE;

    .line 56
    return-void
.end method

.method private a()LajN;
    .locals 4

    .prologue
    .line 89
    iget-object v0, p0, LajP;->a:LajN;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, LajP;->a:LajN;

    .line 105
    :goto_0
    return-object v0

    .line 95
    :cond_0
    iget-object v0, p0, LajP;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 96
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxh;->base_line_account_metadata:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 99
    :try_start_0
    iget-object v1, p0, LajP;->a:LaeH;

    invoke-virtual {v1, v0}, LaeH;->a(Ljava/io/InputStream;)LaeG;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lbxk; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 101
    :try_start_1
    new-instance v2, LajN;

    .line 102
    invoke-virtual {v1}, LaeG;->b()Lbxb;

    move-result-object v0

    check-cast v0, LaJQ;

    sget-object v3, LaFP;->a:Ljava/util/Date;

    invoke-direct {v2, v0, v3}, LajN;-><init>(LaJQ;Ljava/util/Date;)V

    iput-object v2, p0, LajP;->a:LajN;

    .line 103
    iget-object v0, p0, LajP;->a:LajN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    :try_start_2
    invoke-virtual {v1}, LaeG;->a()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lbxk; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 107
    :catch_0
    move-exception v0

    .line 108
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot parse the standard base line account info"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v1}, LaeG;->a()V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lbxk; {:try_start_3 .. :try_end_3} :catch_1

    .line 109
    :catch_1
    move-exception v0

    .line 110
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot parse the standard base line account info"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public declared-synchronized a(LaFO;)LajN;
    .locals 3

    .prologue
    .line 63
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LajP;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LajN;

    .line 64
    if-nez v0, :cond_0

    .line 65
    iget-object v0, p0, LajP;->a:LaGM;

    invoke-interface {v0, p1}, LaGM;->a(LaFO;)LaFM;

    move-result-object v0

    .line 66
    iget-object v1, p0, LajP;->a:LaGM;

    invoke-interface {v1, v0}, LaGM;->a(LaFM;)LaFP;

    move-result-object v1

    .line 67
    invoke-virtual {v1}, LaFP;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 68
    invoke-direct {p0}, LajP;->a()LajN;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 85
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 72
    :cond_1
    :try_start_1
    iget-object v0, p0, LajP;->a:LafE;

    .line 73
    invoke-virtual {v1}, LaFP;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, LafE;->a(LaFO;Ljava/lang/String;)LaJQ;

    move-result-object v2

    .line 74
    new-instance v0, LajN;

    invoke-virtual {v1}, LaFP;->a()Ljava/util/Date;

    move-result-object v1

    invoke-direct {v0, v2, v1}, LajN;-><init>(LaJQ;Ljava/util/Date;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lbxk; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 82
    :goto_1
    :try_start_2
    iget-object v1, p0, LajP;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    :try_start_3
    const-string v1, "AccountCapabilityFactoryImpl"

    const-string v2, "Error parsing feed"

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 77
    invoke-direct {p0}, LajP;->a()LajN;

    move-result-object v0

    goto :goto_1

    .line 78
    :catch_1
    move-exception v0

    .line 79
    const-string v1, "AccountCapabilityFactoryImpl"

    const-string v2, "Error parsing feed"

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 80
    invoke-direct {p0}, LajP;->a()LajN;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    goto :goto_1
.end method
