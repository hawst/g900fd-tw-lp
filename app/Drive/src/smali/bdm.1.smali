.class public final Lbdm;
.super Ljava/lang/Object;
.source "BatchRequest.java"


# instance fields
.field private a:LbdY;

.field private final a:Lbej;

.field private a:LbfP;

.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbdo",
            "<**>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lbeq;Lbek;)V
    .locals 2

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    new-instance v0, LbdY;

    const-string v1, "https://www.googleapis.com/batch"

    invoke-direct {v0, v1}, LbdY;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lbdm;->a:LbdY;

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbdm;->a:Ljava/util/List;

    .line 109
    sget-object v0, LbfP;->a:LbfP;

    iput-object v0, p0, Lbdm;->a:LbfP;

    .line 135
    if-nez p2, :cond_0

    .line 136
    invoke-virtual {p1}, Lbeq;->a()Lbej;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lbdm;->a:Lbej;

    .line 137
    return-void

    .line 136
    :cond_0
    invoke-virtual {p1, p2}, Lbeq;->a(Lbek;)Lbej;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(LbdY;)Lbdm;
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lbdm;->a:LbdY;

    .line 145
    return-object p0
.end method

.method public a(Lbei;Ljava/lang/Class;Ljava/lang/Class;Lbdl;)Lbdm;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lbei;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Class",
            "<TE;>;",
            "Lbdl",
            "<TT;TE;>;)",
            "Lbdm;"
        }
    .end annotation

    .prologue
    .line 189
    invoke-static {p1}, LbfN;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    invoke-static {p4}, LbfN;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    invoke-static {p2}, LbfN;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    invoke-static {p3}, LbfN;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    iget-object v0, p0, Lbdm;->a:Ljava/util/List;

    new-instance v1, Lbdo;

    invoke-direct {v1, p4, p2, p3, p1}, Lbdo;-><init>(Lbdl;Ljava/lang/Class;Ljava/lang/Class;Lbei;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    return-object p0
.end method

.method public a()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 217
    iget-object v0, p0, Lbdm;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LbfN;->b(Z)V

    .line 218
    iget-object v0, p0, Lbdm;->a:Lbej;

    iget-object v3, p0, Lbdm;->a:LbdY;

    invoke-virtual {v0, v3, v14}, Lbej;->a(LbdY;LbdZ;)Lbei;

    move-result-object v7

    .line 220
    invoke-virtual {v7}, Lbei;->a()Lbec;

    move-result-object v0

    .line 221
    new-instance v3, Lbdn;

    invoke-direct {v3, p0, v0}, Lbdn;-><init>(Lbdm;Lbec;)V

    invoke-virtual {v7, v3}, Lbei;->a(Lbec;)Lbei;

    .line 222
    invoke-virtual {v7}, Lbei;->b()I

    move-result v0

    .line 223
    invoke-virtual {v7}, Lbei;->a()LbdT;

    move-result-object v8

    .line 225
    if-eqz v8, :cond_0

    .line 227
    invoke-interface {v8}, LbdT;->a()V

    :cond_0
    move v6, v0

    .line 231
    if-lez v6, :cond_2

    move v3, v1

    .line 232
    :goto_1
    new-instance v9, Lbev;

    invoke-direct {v9}, Lbev;-><init>()V

    .line 233
    invoke-virtual {v9}, Lbev;->a()Lbeh;

    move-result-object v0

    const-string v4, "mixed"

    invoke-virtual {v0, v4}, Lbeh;->b(Ljava/lang/String;)Lbeh;

    .line 235
    iget-object v0, p0, Lbdm;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v4, v1

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdo;

    .line 236
    new-instance v11, Lbew;

    new-instance v5, Lbed;

    invoke-direct {v5}, Lbed;-><init>()V

    .line 237
    invoke-virtual {v5, v14}, Lbed;->a(Ljava/lang/String;)Lbed;

    move-result-object v12

    const-string v13, "Content-ID"

    add-int/lit8 v5, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v12, v13, v4}, Lbed;->a(Ljava/lang/String;Ljava/lang/Object;)Lbed;

    move-result-object v4

    new-instance v12, Lbdu;

    iget-object v0, v0, Lbdo;->a:Lbei;

    invoke-direct {v12, v0}, Lbdu;-><init>(Lbei;)V

    invoke-direct {v11, v4, v12}, Lbew;-><init>(Lbed;LbdZ;)V

    .line 236
    invoke-virtual {v9, v11}, Lbev;->a(Lbew;)Lbev;

    move v4, v5

    .line 239
    goto :goto_2

    :cond_1
    move v0, v2

    .line 217
    goto :goto_0

    :cond_2
    move v3, v2

    .line 231
    goto :goto_1

    .line 240
    :cond_3
    invoke-virtual {v7, v9}, Lbei;->a(LbdZ;)Lbei;

    .line 241
    invoke-virtual {v7}, Lbei;->a()Lbel;

    move-result-object v4

    .line 245
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "--"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Lbel;->a()Lbeh;

    move-result-object v5

    const-string v9, "boundary"

    invoke-virtual {v5, v9}, Lbeh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 248
    invoke-virtual {v4}, Lbel;->a()Ljava/io/InputStream;

    move-result-object v5

    .line 249
    new-instance v9, Lbdp;

    iget-object v10, p0, Lbdm;->a:Ljava/util/List;

    invoke-direct {v9, v5, v0, v10, v3}, Lbdp;-><init>(Ljava/io/InputStream;Ljava/lang/String;Ljava/util/List;Z)V

    .line 252
    :goto_3
    iget-boolean v0, v9, Lbdp;->a:Z

    if-eqz v0, :cond_4

    .line 253
    invoke-virtual {v9}, Lbdp;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 256
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lbel;->b()V

    throw v0

    :cond_4
    invoke-virtual {v4}, Lbel;->b()V

    .line 259
    iget-object v0, v9, Lbdp;->a:Ljava/util/List;

    .line 260
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_6

    .line 261
    iput-object v0, p0, Lbdm;->a:Ljava/util/List;

    .line 263
    iget-boolean v0, v9, Lbdp;->b:Z

    if-eqz v0, :cond_5

    if-eqz v8, :cond_5

    .line 264
    invoke-interface {v8}, LbdT;->a()J

    move-result-wide v4

    .line 265
    const-wide/16 v10, -0x1

    cmp-long v0, v4, v10

    if-eqz v0, :cond_5

    .line 267
    :try_start_1
    iget-object v0, p0, Lbdm;->a:LbfP;

    invoke-interface {v0, v4, v5}, LbfP;->a(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 276
    :cond_5
    :goto_4
    add-int/lit8 v0, v6, -0x1

    .line 277
    if-nez v3, :cond_0

    .line 278
    :cond_6
    iget-object v0, p0, Lbdm;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 279
    return-void

    .line 268
    :catch_0
    move-exception v0

    goto :goto_4
.end method
