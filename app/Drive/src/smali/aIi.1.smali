.class LaIi;
.super Ljava/lang/Object;
.source "StorageApiThumbnailFetcher.java"

# interfaces
.implements Landroid/os/CancellationSignal$OnCancelListener;


# instance fields
.field final synthetic a:LaGo;

.field final synthetic a:LaIh;

.field final synthetic a:Ljava/util/concurrent/Future;


# direct methods
.method constructor <init>(LaIh;LaGo;Ljava/util/concurrent/Future;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, LaIi;->a:LaIh;

    iput-object p2, p0, LaIi;->a:LaGo;

    iput-object p3, p0, LaIi;->a:Ljava/util/concurrent/Future;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 3

    .prologue
    .line 62
    const-string v0, "StorageApiThumbnailFetcher"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cancel thumbnail fetching for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LaIi;->a:LaGo;

    invoke-interface {v2}, LaGo;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    iget-object v0, p0, LaIi;->a:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 64
    return-void
.end method
