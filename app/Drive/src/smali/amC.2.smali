.class LamC;
.super Ljava/lang/Object;
.source "RateLimitedExecutorImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:J

.field final synthetic a:Lamy;


# direct methods
.method constructor <init>(Lamy;J)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, LamC;->a:Lamy;

    iput-wide p2, p0, LamC;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 128
    :try_start_0
    iget-object v0, p0, LamC;->a:Lamy;

    invoke-static {v0}, Lamy;->a(Lamy;)I

    move-result v0

    int-to-long v0, v0

    iget-wide v2, p0, LamC;->a:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 129
    iget-object v0, p0, LamC;->a:Lamy;

    invoke-static {v0}, Lamy;->b(Lamy;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    :goto_0
    return-void

    .line 130
    :catch_0
    move-exception v0

    .line 131
    const-string v0, "RateLimitedExecutor"

    const-string v1, "Scheduling thread has been interrupted."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
