.class public Lqe;
.super Ljava/lang/Object;
.source "CyclopsAccountSwitcher.java"

# interfaces
.implements Lqd;


# instance fields
.field private a:LaCV;

.field private a:Lbap;

.field private a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/content/Context;)LaCV;
    .locals 3

    .prologue
    .line 180
    new-instance v0, LaZo;

    invoke-direct {v0}, LaZo;-><init>()V

    const/16 v1, 0x98

    .line 181
    invoke-virtual {v0, v1}, LaZo;->a(I)LaZo;

    move-result-object v0

    invoke-virtual {v0}, LaZo;->a()LaZn;

    move-result-object v0

    .line 183
    new-instance v1, LaCW;

    invoke-direct {v1, p1}, LaCW;-><init>(Landroid/content/Context;)V

    .line 184
    sget-object v2, LaZl;->a:LaCM;

    invoke-virtual {v1, v2, v0}, LaCW;->a(LaCM;LaCO;)LaCW;

    .line 185
    new-instance v0, Lqj;

    invoke-direct {v0, p0}, Lqj;-><init>(Lqe;)V

    invoke-virtual {v1, v0}, LaCW;->a(LaCX;)LaCW;

    .line 196
    new-instance v0, Lqk;

    invoke-direct {v0, p0}, Lqk;-><init>(Lqe;)V

    invoke-virtual {v1, v0}, LaCW;->a(LaCY;)LaCW;

    .line 204
    invoke-virtual {v1}, LaCW;->a()LaCV;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lqe;)LaCV;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lqe;->a:LaCV;

    return-object v0
.end method

.method static synthetic a(Lqe;Lbap;)Lbap;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lqe;->a:Lbap;

    return-object p1
.end method

.method static synthetic a(Lqe;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lqe;->e()V

    return-void
.end method

.method static synthetic b(Lqe;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lqe;->d()V

    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 141
    new-instance v0, LaZd;

    invoke-direct {v0}, LaZd;-><init>()V

    const/4 v1, 0x0

    .line 142
    invoke-virtual {v0, v1}, LaZd;->a(Z)LaZd;

    move-result-object v0

    .line 143
    sget-object v1, LaZl;->a:LaZc;

    iget-object v2, p0, Lqe;->a:LaCV;

    invoke-interface {v1, v2, v0}, LaZc;->a(LaCV;LaZd;)LaCZ;

    move-result-object v0

    new-instance v1, Lqi;

    invoke-direct {v1, p0}, Lqi;-><init>(Lqe;)V

    invoke-interface {v0, v1}, LaCZ;->a(LaDd;)V

    .line 151
    return-void
.end method

.method private e()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 154
    iget-object v0, p0, Lqe;->a:Lbap;

    if-nez v0, :cond_1

    .line 177
    :cond_0
    :goto_0
    return-void

    .line 158
    :cond_1
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 159
    iget-object v0, p0, Lqe;->a:Lbap;

    invoke-virtual {v0}, Lbap;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v1, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbao;

    .line 160
    if-nez v1, :cond_2

    invoke-interface {v0}, Lbao;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lqe;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 163
    invoke-interface {v0}, Lbao;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lqe;->b:Ljava/lang/String;

    .line 164
    invoke-interface {v0}, Lbao;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lqe;->a:Ljava/lang/String;

    :goto_2
    move-object v1, v0

    .line 169
    goto :goto_1

    .line 165
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x2

    if-ge v5, v6, :cond_3

    if-eqz v0, :cond_3

    .line 167
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    move-object v0, v1

    goto :goto_2

    .line 171
    :cond_4
    iget-object v0, p0, Lqe;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    iget-object v4, p0, Lqe;->a:Lbap;

    invoke-static {v4}, LbnG;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v0, v4, v1}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setAccounts(Ljava/util/List;Lbao;)V

    .line 172
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v8, :cond_5

    .line 173
    iget-object v2, p0, Lqe;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbao;

    invoke-interface {v3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbao;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setRecents(Lbao;Lbao;)V

    goto :goto_0

    .line 174
    :cond_5
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 175
    iget-object v1, p0, Lqe;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbao;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setRecents(Lbao;Lbao;)V

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lqe;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lqe;->a:LaCV;

    invoke-interface {v0}, LaCV;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lqe;->a:LaCV;

    invoke-interface {v0}, LaCV;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    iget-object v0, p0, Lqe;->a:LaCV;

    invoke-interface {v0}, LaCV;->a()V

    .line 101
    :cond_0
    return-void
.end method

.method public a(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;Lqc;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 54
    iput-object p4, p0, Lqe;->a:Ljava/lang/String;

    .line 55
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 57
    instance-of v0, v1, Landroid/app/Activity;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Landroid/app/Activity;

    .line 58
    :goto_0
    invoke-direct {p0, v1}, Lqe;->a(Landroid/content/Context;)LaCV;

    move-result-object v2

    iput-object v2, p0, Lqe;->a:LaCV;

    .line 59
    new-instance v2, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    invoke-direct {v2, v1}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lqe;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    .line 60
    iget-object v1, p0, Lqe;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 62
    iget-object v1, p0, Lqe;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    const v2, 0x106000b

    invoke-virtual {v1, v2}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setBackgroundResource(I)V

    .line 63
    iget-object v1, p0, Lqe;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    iget-object v2, p0, Lqe;->a:LaCV;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setClient(LaCV;)V

    .line 64
    iget-object v1, p0, Lqe;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setScrollingHeaderEnabled(Z)V

    .line 65
    iget-object v1, p0, Lqe;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    new-instance v2, Lqf;

    invoke-direct {v2, p0, p3}, Lqf;-><init>(Lqe;Lqc;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setAccountSelectedListener(LaZx;)V

    .line 72
    iget-object v1, p0, Lqe;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    new-instance v2, Lqg;

    invoke-direct {v2, p0, v0}, Lqg;-><init>(Lqe;Landroid/app/Activity;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setManageAccountsListener(LaZz;)V

    .line 83
    iget-object v1, p0, Lqe;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    new-instance v2, Lqh;

    invoke-direct {v2, p0, v0}, Lqh;-><init>(Lqe;Landroid/app/Activity;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setAddAccountListener(LaZy;)V

    .line 93
    iget-object v0, p0, Lqe;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 94
    return-void

    .line 57
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 133
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    iput-object p1, p0, Lqe;->c:Ljava/lang/String;

    .line 135
    iget-object v0, p0, Lqe;->a:LaCV;

    invoke-interface {v0}, LaCV;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    invoke-direct {p0}, Lqe;->e()V

    .line 138
    :cond_0
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lqe;->c:Ljava/lang/String;

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lqe;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a()V

    .line 106
    iget-object v0, p0, Lqe;->a:LaCV;

    invoke-interface {v0}, LaCV;->b()V

    .line 107
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 111
    return-void
.end method
