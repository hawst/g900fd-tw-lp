.class LOB;
.super Ljava/lang/Object;
.source "PhotoBackupSuggestHelpCard.java"

# interfaces
.implements LbsJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbsJ",
        "<",
        "Landroid/app/PendingIntent;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LOA;


# direct methods
.method constructor <init>(LOA;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, LOB;->a:LOA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/app/PendingIntent;)V
    .locals 6

    .prologue
    .line 96
    :try_start_0
    iget-object v0, p0, LOB;->a:LOA;

    invoke-static {v0}, LOA;->a(LOA;)Laja;

    move-result-object v0

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 97
    invoke-virtual {p1}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/app/Activity;->startIntentSender(Landroid/content/IntentSender;Landroid/content/Intent;III)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    :goto_0
    iget-object v0, p0, LOB;->a:LOA;

    iget-object v1, p0, LOB;->a:LOA;

    invoke-virtual {v1}, LOA;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LOA;->a(Landroid/content/Context;)Z

    .line 103
    return-void

    .line 98
    :catch_0
    move-exception v0

    .line 99
    iget-object v0, p0, LOB;->a:LOA;

    invoke-virtual {v0}, LOA;->a()Landroid/content/Context;

    move-result-object v0

    sget v1, LOV;->auto_backup_turn_on_failure_toast:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 92
    check-cast p1, Landroid/app/PendingIntent;

    invoke-virtual {p0, p1}, LOB;->a(Landroid/app/PendingIntent;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 107
    iget-object v0, p0, LOB;->a:LOA;

    invoke-virtual {v0}, LOA;->a()Landroid/content/Context;

    move-result-object v0

    .line 108
    iget-object v1, p0, LOB;->a:LOA;

    invoke-virtual {v1, v0}, LOA;->a(Landroid/content/Context;)Z

    .line 109
    sget v1, LOV;->auto_backup_turn_on_failure_toast:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 110
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 111
    return-void
.end method
