.class public LaGh;
.super Ljava/lang/Object;
.source "DatabaseModelLoaderImpl.java"

# interfaces
.implements LaGg;


# annotations
.annotation runtime Lbxz;
.end annotation


# instance fields
.field private final a:LaEz;

.field private final a:LaFN;

.field private final a:LaKM;

.field private final a:Lahf;

.field private final a:Lahj;

.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "LIh;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LtK;

.field private final a:Z

.field private final b:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "Lafz;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Z

.field private final c:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "Ladi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LaEz;LQr;LaKM;LtK;LaFN;Lahf;Lahj;Laja;Laja;Laja;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaEz;",
            "LQr;",
            "LaKM;",
            "LtK;",
            "LaFN;",
            "Lahf;",
            "Lahj;",
            "Laja",
            "<",
            "Ladi;",
            ">;",
            "Laja",
            "<",
            "LIh;",
            ">;",
            "Laja",
            "<",
            "Lafz;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 141
    invoke-direct/range {p0 .. p10}, LaGh;-><init>(LaEz;LQr;LaKM;LtK;LaFN;Lahf;Lahj;Lbxw;Lbxw;Lbxw;)V

    .line 144
    return-void
.end method

.method public constructor <init>(LaEz;LQr;LaKM;LtK;LaFN;Lahf;Lahj;Lbxw;Lbxw;Lbxw;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaEz;",
            "LQr;",
            "LaKM;",
            "LtK;",
            "LaFN;",
            "Lahf;",
            "Lahj;",
            "Lbxw",
            "<",
            "Ladi;",
            ">;",
            "Lbxw",
            "<",
            "LIh;",
            ">;",
            "Lbxw",
            "<",
            "Lafz;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    iput-object p1, p0, LaGh;->a:LaEz;

    .line 153
    iput-object p3, p0, LaGh;->a:LaKM;

    .line 154
    const-string v0, "enableDocumentContentChainAutofix"

    const/4 v1, 0x1

    .line 155
    invoke-interface {p2, v0, v1}, LQr;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, LaGh;->a:Z

    .line 156
    sget-object v0, Lry;->ab:Lry;

    .line 157
    invoke-interface {p4, v0}, LtK;->a(LtJ;)Z

    move-result v0

    iput-boolean v0, p0, LaGh;->b:Z

    .line 158
    iput-object p5, p0, LaGh;->a:LaFN;

    .line 159
    iput-object p4, p0, LaGh;->a:LtK;

    .line 160
    iput-object p6, p0, LaGh;->a:Lahf;

    .line 161
    iput-object p7, p0, LaGh;->a:Lahj;

    .line 162
    iput-object p8, p0, LaGh;->c:Lbxw;

    .line 163
    iput-object p9, p0, LaGh;->a:Lbxw;

    .line 164
    iput-object p10, p0, LaGh;->b:Lbxw;

    .line 165
    invoke-virtual {p0}, LaGh;->e()V

    .line 166
    return-void
.end method

.method private a(JJ)I
    .locals 7

    .prologue
    .line 1795
    invoke-virtual {p0}, LaGh;->a()V

    .line 1798
    :try_start_0
    invoke-static {}, LaEK;->a()LaEK;

    move-result-object v2

    sget-object v0, LaEL;->c:LaEL;

    invoke-virtual {v0}, LaEL;->a()LaFr;

    move-result-object v3

    .line 1799
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object v1, p0

    move-wide v4, p1

    .line 1797
    invoke-direct/range {v1 .. v6}, LaGh;->a(LaFy;LaFr;JLjava/lang/Long;)I

    move-result v0

    .line 1800
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v2

    sget-object v1, LaEJ;->r:LaEJ;

    .line 1801
    invoke-virtual {v1}, LaEJ;->a()LaFr;

    move-result-object v3

    .line 1802
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object v1, p0

    move-wide v4, p1

    .line 1800
    invoke-direct/range {v1 .. v6}, LaGh;->a(LaFy;LaFr;JLjava/lang/Long;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1803
    invoke-virtual {p0}, LaGh;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1806
    invoke-virtual {p0}, LaGh;->b()V

    return v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LaGh;->b()V

    throw v0
.end method

.method private a(LaFy;LaFr;JLjava/lang/Long;)I
    .locals 7

    .prologue
    .line 1815
    invoke-virtual {p2}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LaFy;->a(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LbiT;->a(Z)V

    .line 1816
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1817
    invoke-virtual {p2}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1818
    iget-object v1, p0, LaGh;->a:LaEz;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, LaFr;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 1819
    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 1818
    invoke-virtual {v1, p1, v0, v2, v3}, LaEz;->a(LaFy;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private a(J)LaFQ;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 264
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {}, LaEe;->a()LaEe;

    move-result-object v1

    invoke-virtual {v1}, LaEe;->c()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 265
    invoke-static {}, LaEe;->a()LaEe;

    move-result-object v4

    invoke-virtual {v4}, LaEe;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 266
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    .line 264
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 268
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 274
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v2

    .line 271
    :cond_0
    :try_start_1
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {v0, v1}, LaFQ;->a(LaEz;Landroid/database/Cursor;)LaFQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 274
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private a(LaFM;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)LaFZ;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 764
    iget-object v0, p0, LaGh;->a:LaEz;

    const-string v1, "CollectionView"

    .line 766
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    .line 767
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    .line 765
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 770
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 775
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v2

    .line 773
    :cond_0
    :try_start_1
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {v0, p1, v1}, LaFZ;->a(LaEz;LaFM;Landroid/database/Cursor;)LaFZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 775
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private a(J)LaGb;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1911
    invoke-virtual {p0, p1, p2}, LaGh;->a(J)J

    move-result-wide v0

    .line 1912
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    .line 1914
    iget-object v0, p0, LaGh;->a:LaEz;

    const-string v1, "DocumentView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LaEL;->c:LaEL;

    .line 1916
    invoke-virtual {v4}, LaEL;->a()LaFr;

    move-result-object v4

    invoke-virtual {v4}, LaFr;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=? OR "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, LaEL;->d:LaEL;

    .line 1917
    invoke-virtual {v4}, LaEL;->a()LaFr;

    move-result-object v4

    invoke-virtual {v4}, LaFr;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    const/4 v6, 0x1

    aput-object v5, v4, v6

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    .line 1914
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1923
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1924
    sget-object v0, LaES;->E:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, v1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1925
    invoke-virtual {p0, v4, v5}, LaGh;->a(J)LaFM;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1926
    if-nez v0, :cond_0

    .line 1934
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v2

    .line 1929
    :cond_0
    :try_start_1
    iget-object v2, p0, LaGh;->a:LaEz;

    invoke-static {v2, v0, v1}, LaGb;->a(LaEz;LaFM;Landroid/database/Cursor;)LaGb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 1934
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private a(LaFM;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)LaGb;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 567
    iget-object v0, p0, LaGh;->a:LaEz;

    const-string v1, "DocumentView"

    .line 568
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    .line 569
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    .line 567
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 572
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 589
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v2

    .line 575
    :cond_0
    :try_start_1
    sget-object v0, LaES;->E:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, v1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 576
    if-nez p1, :cond_1

    .line 577
    invoke-virtual {p0, v4, v5}, LaGh;->a(J)LaFM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object p1

    .line 584
    :goto_1
    if-nez p1, :cond_3

    .line 589
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 579
    :cond_1
    :try_start_2
    invoke-virtual {p1}, LaFM;->a()J

    move-result-wide v6

    .line 580
    cmp-long v0, v4, v6

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Account ids mismatch. Expected account id:"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " Document account id:"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LbiT;->a(ZLjava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 589
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 580
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 587
    :cond_3
    :try_start_3
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {v0, p1, v1}, LaGb;->a(LaEz;LaFM;Landroid/database/Cursor;)LaGb;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    .line 589
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;)LaGb;
    .locals 1

    .prologue
    .line 561
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, LaGh;->a(LaFM;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)LaGb;

    move-result-object v0

    return-object v0
.end method

.method private a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 4

    .prologue
    .line 942
    new-instance v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    iget-object v1, p0, LaGh;->a:LaKM;

    invoke-interface {v1}, LaKM;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, LaGp;->a(J)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private static a(J)Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 2

    .prologue
    .line 118
    sget-object v0, LaES;->E:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, LaFr;->b(J)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    return-object v0
.end method

.method static a(JJ)Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 8

    .prologue
    .line 122
    sget-object v0, LaFL;->a:LaFL;

    invoke-static {p0, p1}, LaGh;->a(J)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    const/4 v3, 0x0

    new-instance v4, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 123
    invoke-static {}, LaER;->a()LaER;

    move-result-object v6

    invoke-virtual {v6}, LaER;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "=? "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    .line 122
    invoke-virtual {v0, v1, v2}, LaFL;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;[Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    return-object v0
.end method

.method private static a(JLjava/lang/String;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 8

    .prologue
    .line 127
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v0, LaFL;->a:LaFL;

    .line 129
    invoke-static {p0, p1}, LaGh;->a(J)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    const/4 v3, 0x0

    new-instance v4, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, LaES;->p:LaES;

    .line 130
    invoke-virtual {v6}, LaES;->a()LaFr;

    move-result-object v6

    invoke-virtual {v6}, LaFr;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "=? "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, p2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    .line 128
    invoke-virtual {v0, v1, v2}, LaFL;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;[Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 131
    return-object v0
.end method

.method private static a(LaFM;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 5

    .prologue
    .line 2135
    sget-object v0, LaES;->y:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->b()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 2136
    invoke-virtual {p0}, LaFM;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, LaGh;->a(J)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v1

    .line 2137
    sget-object v2, LaFL;->a:LaFL;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {v2, v0, v3}, LaFL;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;[Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    return-object v0
.end method

.method private a(LaFO;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 4

    .prologue
    .line 1565
    invoke-direct {p0, p1}, LaGh;->a(LaFO;)Ljava/lang/Long;

    move-result-object v1

    .line 1566
    sget-object v0, LaFb;->g:LaFb;

    invoke-virtual {v0}, LaFb;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    .line 1567
    if-nez p1, :cond_0

    new-instance v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " IS NULL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1569
    :goto_0
    return-object v0

    .line 1567
    :cond_0
    new-instance v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = ?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1569
    invoke-virtual {v1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(LaFO;)Ljava/lang/Long;
    .locals 4

    .prologue
    .line 1452
    if-eqz p1, :cond_1

    .line 1453
    invoke-direct {p0, p1}, LaGh;->b(LaFO;)LaFQ;

    move-result-object v0

    .line 1454
    if-eqz v0, :cond_0

    .line 1455
    invoke-virtual {v0}, LaFQ;->c()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 1460
    :goto_0
    return-object v0

    .line 1457
    :cond_0
    const-string v0, "DatabaseModelLoader"

    const-string v1, "Unknown account %s."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1460
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Z)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;",
            "Z)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "LaGB;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 738
    new-instance v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LaEx;->a:LaEx;

    .line 739
    invoke-virtual {v3}, LaEx;->a()LaFr;

    move-result-object v3

    invoke-virtual {v3}, LaFr;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "=?"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 740
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    if-nez p2, :cond_2

    .line 742
    sget-object v1, LaFL;->a:LaFL;

    new-instance v3, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    const/4 v4, 0x0

    const-string v5, "root"

    .line 743
    invoke-static {v4, v5}, LaEu;->a(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 742
    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a(LaFL;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    move-object v4, v0

    .line 746
    :goto_0
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {}, LaEw;->a()LaEw;

    move-result-object v1

    invoke-virtual {v1}, LaEw;->c()Ljava/lang/String;

    move-result-object v1

    .line 747
    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    .line 746
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 750
    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 751
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 753
    :cond_0
    iget-object v2, p0, LaGh;->a:LaEz;

    invoke-static {v2, v1}, LaGB;->a(LaEz;Landroid/database/Cursor;)LaGB;

    move-result-object v2

    .line 754
    invoke-virtual {v2}, LaGB;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 755
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 759
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    move-object v4, v0

    goto :goto_0
.end method

.method private a(LaFM;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFM;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;"
        }
    .end annotation

    .prologue
    .line 686
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v0

    .line 687
    new-instance v1, LaGi;

    invoke-direct {v1, p0, v0}, LaGi;-><init>(LaGh;Ljava/util/Set;)V

    invoke-direct {p0, p1, v1}, LaGh;->a(LaFM;LaGk;)V

    .line 694
    return-object v0
.end method

.method private a(JLjava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1151
    const/4 v7, 0x0

    .line 1153
    :try_start_0
    invoke-static {}, LaEK;->a()LaEK;

    move-result-object v2

    sget-object v0, LaEL;->c:LaEL;

    .line 1154
    invoke-virtual {v0}, LaEL;->a()LaFr;

    move-result-object v3

    const/4 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    .line 1153
    invoke-direct/range {v1 .. v6}, LaGh;->a(LaFy;LaFr;JLjava/lang/Long;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    add-int/2addr v0, v7

    .line 1156
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v2

    sget-object v1, LaEJ;->r:LaEJ;

    .line 1157
    invoke-virtual {v1}, LaEJ;->a()LaFr;

    move-result-object v3

    move-object v1, p0

    move-wide v4, p1

    move-object v6, v8

    .line 1156
    invoke-direct/range {v1 .. v6}, LaGh;->a(LaFy;LaFr;JLjava/lang/Long;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1160
    iget-boolean v1, p0, LaGh;->b:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 1161
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Detected more than one reference to the document content: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1156
    :catchall_0
    move-exception v0

    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v2

    sget-object v1, LaEJ;->r:LaEJ;

    .line 1157
    invoke-virtual {v1}, LaEJ;->a()LaFr;

    move-result-object v3

    move-object v1, p0

    move-wide v4, p1

    move-object v6, v8

    .line 1156
    invoke-direct/range {v1 .. v6}, LaGh;->a(LaFy;LaFr;JLjava/lang/Long;)I

    move-result v1

    add-int/2addr v1, v7

    throw v0

    .line 1164
    :cond_0
    return-void
.end method

.method private a(LaFM;LaGk;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 703
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 705
    invoke-virtual {p1}, LaFM;->a()LaFO;

    move-result-object v8

    .line 707
    sget-object v0, LaEJ;->s:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->b()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v4

    .line 708
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v0

    invoke-virtual {v0}, LaEI;->d()Ljava/lang/String;

    move-result-object v9

    .line 709
    iget-object v0, p0, LaGh;->a:LaEz;

    .line 710
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v1

    invoke-virtual {v1}, LaEI;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v9, v2, v3

    .line 711
    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()[Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    move-object v7, v5

    .line 709
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 714
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 726
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 728
    :goto_0
    return-void

    .line 719
    :cond_0
    :try_start_1
    invoke-interface {v1, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 720
    invoke-direct {p0, v2, v3}, LaGh;->a(J)LaGb;

    move-result-object v0

    .line 721
    if-eqz v0, :cond_1

    invoke-interface {v0}, LaGo;->a()LaFM;

    move-result-object v2

    invoke-virtual {v2}, LaFM;->a()LaFO;

    move-result-object v2

    invoke-virtual {v2, v8}, LaFO;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 722
    invoke-interface {p2, v0}, LaGk;->a(LaGo;)V

    .line 724
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 726
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private a(LaGc;LaGZ;)V
    .locals 6

    .prologue
    .line 1264
    invoke-virtual {p2}, LaGZ;->a()LaGY;

    move-result-object v1

    .line 1266
    sget-object v0, LacY;->a:LacY;

    invoke-virtual {p1, v0}, LaGc;->a(LacY;)J

    move-result-wide v2

    .line 1267
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-gez v0, :cond_3

    const/4 v0, 0x0

    .line 1269
    :goto_0
    invoke-virtual {v1}, LaGY;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p2}, LaGZ;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 1270
    invoke-virtual {p2}, LaGZ;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1274
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LaGp;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1275
    invoke-virtual {v0}, LaGp;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, LaGZ;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1276
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p2}, LaGZ;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1278
    invoke-virtual {p2}, LaGZ;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LaGh;->a(Ljava/lang/String;)LaGr;

    move-result-object v1

    .line 1279
    invoke-virtual {v1, v0}, LaGr;->a(Ljava/io/File;)LaGr;

    move-result-object v1

    .line 1280
    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, LaGr;->b(Ljava/lang/Long;)LaGr;

    move-result-object v0

    .line 1281
    invoke-virtual {p2}, LaGZ;->a()Ljava/lang/Long;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1282
    new-instance v1, Ljava/util/Date;

    .line 1283
    invoke-virtual {p2}, LaGZ;->a()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 1282
    invoke-virtual {v0, v1}, LaGr;->a(Ljava/util/Date;)LaGr;

    .line 1286
    :cond_1
    invoke-virtual {v0}, LaGr;->b()LaGp;

    move-result-object v0

    .line 1287
    invoke-virtual {v0}, LaGp;->c()J

    move-result-wide v0

    sget-object v2, LacY;->a:LacY;

    invoke-virtual {p1, v0, v1, v2}, LaGc;->a(JLacY;)LaGc;

    .line 1288
    invoke-virtual {p1}, LaGc;->a()LaFM;

    move-result-object v0

    invoke-virtual {p0, v0}, LaGh;->b(LaFM;)V

    .line 1294
    :cond_2
    :goto_1
    return-void

    .line 1267
    :cond_3
    invoke-virtual {p0, v2, v3}, LaGh;->a(J)LaGp;

    move-result-object v0

    goto :goto_0

    .line 1290
    :cond_4
    invoke-virtual {v1}, LaGY;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1291
    const-wide/16 v0, -0x1

    sget-object v2, LacY;->a:LacY;

    invoke-virtual {p1, v0, v1, v2}, LaGc;->a(JLacY;)LaGc;

    .line 1292
    invoke-virtual {p1}, LaGc;->a()LaFM;

    move-result-object v0

    invoke-virtual {p0, v0}, LaGh;->b(LaFM;)V

    goto :goto_1
.end method

.method private a(LaGo;)V
    .locals 2

    .prologue
    .line 1167
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1169
    invoke-interface {p1}, LaGo;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1177
    :cond_0
    :goto_0
    return-void

    .line 1173
    :cond_1
    invoke-direct {p0, p1}, LaGh;->b(LaGo;)LaGp;

    move-result-object v0

    .line 1174
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LaGp;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1175
    invoke-virtual {p0, v0}, LaGh;->a(LaGp;)V

    goto :goto_0
.end method

.method private a(LaGp;LaGo;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1414
    invoke-interface {p2}, LaGo;->b()Ljava/lang/String;

    move-result-object v2

    .line 1415
    invoke-virtual {p1}, LaGp;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1416
    invoke-virtual {p1}, LaGp;->a()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    :cond_0
    invoke-static {v0}, LbiT;->b(Z)V

    move v0, v1

    .line 1428
    :cond_1
    :goto_0
    return v0

    .line 1419
    :cond_2
    invoke-virtual {p1}, LaGp;->a()LadY;

    move-result-object v1

    .line 1420
    if-eqz v1, :cond_3

    invoke-virtual {v1}, LadY;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 1421
    invoke-virtual {p1}, LaGp;->d()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1424
    :cond_3
    if-eqz v2, :cond_4

    .line 1425
    invoke-virtual {p1}, LaGp;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 1426
    :cond_4
    invoke-interface {p2}, LaGo;->b()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p1}, LaGp;->a()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Z
    .locals 11

    .prologue
    const/4 v0, 0x2

    const/4 v10, 0x0

    const/4 v2, 0x0

    const/4 v9, 0x1

    .line 966
    iget-boolean v1, p0, LaGh;->b:Z

    if-eqz v1, :cond_0

    move v1, v0

    .line 967
    :goto_0
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    .line 968
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()[Ljava/lang/String;

    move-result-object v4

    int-to-long v6, v1

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    move-object v1, p1

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    .line 967
    invoke-virtual/range {v0 .. v8}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 971
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 972
    iget-boolean v2, p0, LaGh;->b:Z

    if-eqz v2, :cond_1

    if-le v0, v9, :cond_1

    .line 973
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "File is referenced by more than one rows in %s: [filename: %s, reference count: %s]"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    .line 976
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()[Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v6, v6, v7

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    .line 974
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 981
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    move v1, v9

    .line 966
    goto :goto_0

    .line 979
    :cond_1
    if-lez v0, :cond_2

    .line 981
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return v9

    :cond_2
    move v9, v10

    .line 979
    goto :goto_1
.end method

.method private b()J
    .locals 2

    .prologue
    .line 1836
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {v0}, LaHd;->a(LaEz;)J

    move-result-wide v0

    return-wide v0
.end method

.method private b(J)J
    .locals 11

    .prologue
    const/4 v7, 0x0

    const/4 v9, 0x1

    const/4 v5, 0x0

    .line 2000
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v0

    invoke-virtual {v0}, LaEI;->d()Ljava/lang/String;

    move-result-object v8

    .line 2002
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v1

    invoke-virtual {v1}, LaEI;->c()Ljava/lang/String;

    move-result-object v1

    new-array v2, v9, [Ljava/lang/String;

    aput-object v8, v2, v7

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LaEJ;->r:LaEJ;

    .line 2004
    invoke-virtual {v4}, LaEJ;->a()LaFr;

    move-result-object v4

    invoke-virtual {v4}, LaFr;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v9, [Ljava/lang/String;

    .line 2005
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v7

    move-object v6, v5

    move-object v7, v5

    .line 2002
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 2010
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-le v0, v9, :cond_0

    .line 2011
    new-instance v0, LaGj;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LaGj;-><init>(LaGi;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2020
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    .line 2013
    :cond_0
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2014
    invoke-interface {v2, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 2015
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    .line 2020
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :goto_0
    return-wide v0

    .line 2017
    :cond_1
    const-wide/16 v0, -0x1

    .line 2020
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private b(LaFO;)LaFQ;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 249
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {}, LaEe;->a()LaEe;

    move-result-object v1

    invoke-virtual {v1}, LaEe;->c()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LaEf;->a:LaEf;

    .line 250
    invoke-virtual {v4}, LaEf;->a()LaFr;

    move-result-object v4

    invoke-virtual {v4}, LaFr;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 251
    invoke-virtual {p1}, LaFO;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    .line 249
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 253
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 258
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v2

    .line 256
    :cond_0
    :try_start_1
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {v0, v1}, LaFQ;->a(LaEz;Landroid/database/Cursor;)LaFQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 258
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaFZ;
    .locals 4

    .prologue
    .line 798
    iget-object v0, p1, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    invoke-virtual {p0, v0}, LaGh;->a(LaFO;)LaFM;

    move-result-object v0

    .line 800
    invoke-virtual {v0}, LaFM;->a()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v1}, LaGh;->a(JLjava/lang/String;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v1

    .line 801
    invoke-direct {p0, v0, v1}, LaGh;->a(LaFM;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)LaFZ;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGb;
    .locals 4

    .prologue
    .line 1054
    iget-object v0, p1, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    invoke-virtual {p0, v0}, LaGh;->a(LaFO;)LaFM;

    move-result-object v0

    .line 1056
    invoke-virtual {v0}, LaFM;->a()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v1}, LaGh;->a(JLjava/lang/String;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v1

    .line 1057
    invoke-direct {p0, v0, v1}, LaGh;->a(LaFM;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)LaGb;

    move-result-object v0

    return-object v0
.end method

.method private b(LaGo;)LaGp;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1326
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1328
    sget-object v1, LacY;->a:LacY;

    invoke-interface {p1, v1}, LaGo;->a(LacY;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 1330
    if-nez v1, :cond_1

    .line 1355
    :cond_0
    :goto_0
    return-object v0

    .line 1334
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, LaGh;->a(J)LaGp;

    move-result-object v2

    .line 1335
    if-eqz v2, :cond_0

    .line 1339
    invoke-virtual {v2}, LaGp;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1340
    invoke-virtual {v2}, LaGp;->a()Ljava/lang/Long;

    move-result-object v1

    .line 1341
    if-eqz v1, :cond_0

    .line 1345
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, LaGh;->a(J)LaGp;

    move-result-object v1

    .line 1346
    if-eqz v1, :cond_0

    .line 1349
    invoke-virtual {v1}, LaGp;->a()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "temporary content references another temporary content in document: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\nReferencing content:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\nReferenced content:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LbiT;->b(ZLjava/lang/Object;)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move-object v0, v2

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 352
    iget-object v0, p0, LaGh;->a:LaKM;

    invoke-interface {v0}, LaKM;->a()J

    move-result-wide v0

    .line 353
    iget-object v2, p0, LaGh;->a:LaEz;

    invoke-static {}, LaEs;->a()LaEs;

    move-result-object v3

    invoke-virtual {v3}, LaEs;->c()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LaEt;->c:LaEt;

    .line 354
    invoke-virtual {v5}, LaEt;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "<?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/String;

    .line 355
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    .line 353
    invoke-virtual {v2, v3, v4, v5}, LaEz;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 356
    iget-object v3, p0, LaGh;->a:LaKM;

    invoke-interface {v3}, LaKM;->a()J

    move-result-wide v4

    sub-long v0, v4, v0

    .line 357
    const-string v3, "DatabaseModelLoader"

    const-string v4, "Deleted %d old cachedSearches in %d msec"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v8

    invoke-static {v3, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 358
    return v2
.end method

.method public a(LaFM;)I
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 343
    invoke-virtual {p1}, LaFM;->a()J

    move-result-wide v4

    .line 344
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 345
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {}, LaFc;->a()LaFc;

    move-result-object v3

    invoke-virtual {v3}, LaFc;->c()Ljava/lang/String;

    move-result-object v3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, LaFd;->a:LaFd;

    .line 346
    invoke-virtual {v7}, LaFd;->a()LaFr;

    move-result-object v7

    invoke-virtual {v7}, LaFr;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "=?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-array v1, v1, [Ljava/lang/String;

    .line 347
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    .line 345
    invoke-virtual {v0, v3, v6, v1}, LaEz;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_0
    move v0, v2

    .line 344
    goto :goto_0
.end method

.method public a(LaFM;J)I
    .locals 12

    .prologue
    const/4 v10, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2046
    const-wide/16 v4, 0x0

    cmp-long v0, p2, v4

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 2047
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2050
    invoke-direct {p0, p1}, LaGh;->a(LaFM;)Ljava/util/Set;

    move-result-object v0

    .line 2052
    sget-object v3, LaFL;->a:LaFL;

    .line 2053
    invoke-virtual {p1}, LaFM;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, LaGh;->a(J)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v4

    new-array v5, v10, [Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    sget-object v6, LaES;->y:LaES;

    .line 2054
    invoke-virtual {v6}, LaES;->a()LaFr;

    move-result-object v6

    invoke-virtual {v6}, LaFr;->a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v6

    aput-object v6, v5, v2

    new-instance v6, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, LaES;->F:LaES;

    .line 2055
    invoke-virtual {v8}, LaES;->a()LaFr;

    move-result-object v8

    invoke-virtual {v8}, LaFr;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "<?"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2056
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v5, v1

    .line 2052
    invoke-virtual {v3, v4, v5}, LaFL;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;[Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v3

    .line 2057
    invoke-virtual {p0, p1, v3}, LaGh;->a(LaFM;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Ljava/util/Set;

    move-result-object v3

    .line 2059
    invoke-static {v3, v0}, LbpU;->c(Ljava/util/Set;Ljava/util/Set;)Lbqb;

    move-result-object v0

    .line 2061
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v4

    .line 2062
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v2

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 2063
    check-cast v0, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    .line 2064
    iget-object v6, p0, LaGh;->a:LaEz;

    .line 2065
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;->a()J

    move-result-wide v8

    invoke-static {}, LaER;->a()LaER;

    move-result-object v0

    sget-object v7, LaGe;->a:Landroid/net/Uri;

    .line 2064
    invoke-virtual {v6, v8, v9, v0, v7}, LaEz;->a(JLaFy;Landroid/net/Uri;)I

    move-result v0

    add-int/2addr v0, v3

    move v3, v0

    .line 2067
    goto :goto_1

    :cond_0
    move v0, v2

    .line 2046
    goto/16 :goto_0

    .line 2068
    :cond_1
    if-eq v3, v4, :cond_2

    .line 2069
    const-string v0, "DatabaseModelLoader"

    const-string v5, "Only %d of %d obsolete entries deleted successfully"

    new-array v6, v10, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v1

    invoke-static {v0, v5, v6}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 2071
    :cond_2
    return v3
.end method

.method public a()J
    .locals 2

    .prologue
    .line 2076
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-virtual {v0}, LaEz;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method a(J)J
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1950
    cmp-long v2, p1, v6

    if-ltz v2, :cond_1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 1951
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v2

    .line 1954
    invoke-virtual {p0}, LaGh;->a()V

    move-wide v0, p1

    .line 1956
    :goto_1
    cmp-long v3, v0, v6

    if-ltz v3, :cond_3

    .line 1957
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1960
    :try_start_1
    invoke-direct {p0, v0, v1}, LaGh;->b(J)J
    :try_end_1
    .catch LaGj; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v4

    .line 1969
    :try_start_2
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1972
    iget-boolean v0, p0, LaGh;->a:Z

    if-eqz v0, :cond_0

    .line 1973
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v2

    sget-object v0, LaEJ;->r:LaEJ;

    .line 1974
    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v3

    const/4 v6, 0x0

    move-object v1, p0

    .line 1973
    invoke-direct/range {v1 .. v6}, LaGh;->a(LaFy;LaFr;JLjava/lang/Long;)I

    .line 1977
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DocumentContents form a loop. The offending content sql id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1987
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LaGh;->c()V

    .line 1988
    invoke-virtual {p0}, LaGh;->b()V

    throw v0

    :cond_1
    move v0, v1

    .line 1950
    goto :goto_0

    .line 1961
    :catch_0
    move-exception v2

    .line 1962
    :try_start_3
    const-string v2, "sqlId: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1963
    iget-boolean v3, p0, LaGh;->a:Z

    if-eqz v3, :cond_2

    .line 1964
    invoke-direct {p0, v0, v1, v2}, LaGh;->a(JLjava/lang/String;)V

    .line 1966
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Detected more than one reference to the document content: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1987
    :cond_3
    invoke-virtual {p0}, LaGh;->c()V

    .line 1988
    invoke-virtual {p0}, LaGh;->b()V

    .line 1990
    return-wide p1

    :cond_4
    move-wide p1, v0

    move-wide v0, v4

    goto/16 :goto_1
.end method

.method public a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)J
    .locals 4

    .prologue
    .line 444
    invoke-static {}, LamV;->b()V

    .line 445
    new-instance v0, Lwu;

    invoke-direct {v0, p0}, Lwu;-><init>(LaGg;)V

    .line 446
    invoke-interface {p1, v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a(LvR;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwv;

    .line 447
    iget-object v0, v0, Lwv;->a:Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    .line 449
    if-nez v0, :cond_0

    .line 453
    sget-object v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->d:Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    .line 458
    :goto_0
    iget-object v1, p0, LaGh;->a:LaEz;

    const-string v2, "EntryView"

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    .line 459
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()[Ljava/lang/String;

    move-result-object v0

    .line 458
    invoke-virtual {v1, v2, v3, v0}, LaEz;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    return-wide v0

    .line 455
    :cond_0
    sget-object v1, LaFL;->a:LaFL;

    sget-object v2, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a:Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a(LaFL;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    goto :goto_0
.end method

.method public a(J)LaFM;
    .locals 7

    .prologue
    .line 208
    iget-object v0, p0, LaGh;->a:LaFN;

    invoke-virtual {v0, p1, p2}, LaFN;->a(J)LaFM;

    move-result-object v0

    .line 209
    if-nez v0, :cond_0

    .line 210
    invoke-direct {p0, p1, p2}, LaGh;->a(J)LaFQ;

    move-result-object v1

    .line 211
    if-nez v1, :cond_1

    .line 212
    const/4 v0, 0x0

    .line 217
    :cond_0
    :goto_0
    return-object v0

    .line 214
    :cond_1
    new-instance v0, LaFM;

    invoke-virtual {v1}, LaFQ;->a()LaFO;

    move-result-object v2

    invoke-virtual {v1}, LaFQ;->c()J

    move-result-wide v4

    invoke-direct {v0, v2, v4, v5}, LaFM;-><init>(LaFO;J)V

    .line 215
    iget-object v1, p0, LaGh;->a:LaFN;

    invoke-virtual {v1, v0}, LaFN;->a(LaFM;)V

    goto :goto_0
.end method

.method public a(LaFO;)LaFM;
    .locals 4

    .prologue
    .line 197
    iget-object v0, p0, LaGh;->a:LaFN;

    invoke-virtual {v0, p1}, LaFN;->a(LaFO;)LaFM;

    move-result-object v0

    .line 198
    if-nez v0, :cond_0

    .line 199
    invoke-virtual {p0, p1}, LaGh;->a(LaFO;)LaFQ;

    move-result-object v1

    .line 200
    new-instance v0, LaFM;

    invoke-virtual {v1}, LaFQ;->c()J

    move-result-wide v2

    invoke-direct {v0, p1, v2, v3}, LaFM;-><init>(LaFO;J)V

    .line 201
    iget-object v1, p0, LaGh;->a:LaFN;

    invoke-virtual {v1, v0}, LaFN;->a(LaFM;)V

    .line 203
    :cond_0
    return-object v0
.end method

.method public a(LaFM;)LaFP;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 301
    iget-object v0, p0, LaGh;->a:LaEz;

    .line 302
    invoke-static {}, LaEc;->a()LaEc;

    move-result-object v1

    invoke-virtual {v1}, LaEc;->c()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LaEd;->a:LaEd;

    .line 303
    invoke-virtual {v4}, LaEd;->a()LaFr;

    move-result-object v4

    invoke-virtual {v4}, LaFr;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 304
    invoke-virtual {p1}, LaFM;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    .line 302
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 306
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_0

    .line 307
    new-instance v0, LaFP;

    iget-object v2, p0, LaGh;->a:LaEz;

    invoke-virtual {p1}, LaFM;->a()J

    move-result-wide v4

    invoke-direct {v0, v2, v4, v5}, LaFP;-><init>(LaEz;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 312
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v0

    .line 310
    :cond_0
    :try_start_1
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {v0, v1}, LaFP;->a(LaEz;Landroid/database/Cursor;)LaFP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 312
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public a(LaFO;)LaFQ;
    .locals 2

    .prologue
    .line 280
    invoke-direct {p0, p1}, LaGh;->b(LaFO;)LaFQ;

    move-result-object v0

    .line 281
    if-nez v0, :cond_1

    .line 284
    invoke-virtual {p0}, LaGh;->a()V

    .line 286
    :try_start_0
    invoke-direct {p0, p1}, LaGh;->b(LaFO;)LaFQ;

    move-result-object v0

    .line 287
    if-nez v0, :cond_0

    .line 288
    new-instance v0, LaFQ;

    iget-object v1, p0, LaGh;->a:LaEz;

    invoke-direct {v0, v1, p1}, LaFQ;-><init>(LaEz;LaFO;)V

    .line 289
    invoke-virtual {v0}, LaFQ;->e()V

    .line 291
    :cond_0
    invoke-virtual {p0}, LaGh;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 293
    invoke-virtual {p0}, LaGh;->b()V

    .line 296
    :cond_1
    return-object v0

    .line 293
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LaGh;->b()V

    throw v0
.end method

.method public a(J)LaFR;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1433
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 1447
    :goto_0
    return-object v2

    .line 1436
    :cond_0
    iget-object v0, p0, LaGh;->a:LaEz;

    .line 1437
    invoke-static {}, LaEj;->a()LaEj;

    move-result-object v1

    invoke-virtual {v1}, LaEj;->c()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1438
    invoke-static {}, LaEj;->a()LaEj;

    move-result-object v4

    invoke-virtual {v4}, LaEj;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 1439
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    .line 1437
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1442
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 1447
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1445
    :cond_1
    :try_start_1
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {v0, v1}, LaFR;->a(LaEz;Landroid/database/Cursor;)LaFR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 1447
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)LaFR;
    .locals 6

    .prologue
    .line 1581
    new-instance v0, LaFR;

    iget-object v1, p0, LaGh;->a:LaEz;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LaFR;-><init>(LaEz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public a(LaFR;LaGp;)LaFS;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 1630
    invoke-virtual {p1}, LaFR;->c()J

    move-result-wide v2

    .line 1631
    invoke-virtual {p2}, LaGp;->c()J

    move-result-wide v4

    .line 1632
    cmp-long v0, v2, v6

    if-ltz v0, :cond_0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 1633
    new-instance v0, LaFS;

    iget-object v1, p0, LaGh;->a:LaEz;

    invoke-direct/range {v0 .. v5}, LaFS;-><init>(LaEz;JJ)V

    return-object v0

    .line 1632
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LaFM;Ljava/lang/String;J)LaFT;
    .locals 7

    .prologue
    .line 364
    new-instance v0, LaFT;

    iget-object v1, p0, LaGh;->a:LaEz;

    invoke-virtual {p1}, LaFM;->a()J

    move-result-wide v2

    move-object v4, p2

    move-wide v5, p3

    invoke-direct/range {v0 .. v6}, LaFT;-><init>(LaEz;JLjava/lang/String;J)V

    .line 365
    invoke-virtual {v0}, LaFT;->e()V

    .line 366
    return-object v0
.end method

.method public bridge synthetic a(LaFM;J)LaFV;
    .locals 2

    .prologue
    .line 94
    invoke-virtual {p0, p1, p2, p3}, LaGh;->a(LaFM;J)LaFZ;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFV;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0, p1}, LaGh;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFZ;

    move-result-object v0

    return-object v0
.end method

.method public a(LaFM;J)LaFZ;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1067
    iget-object v0, p0, LaGh;->a:LaEz;

    const-string v1, "CollectionView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LaES;->E:LaES;

    .line 1068
    invoke-virtual {v4}, LaES;->a()LaFr;

    move-result-object v4

    invoke-virtual {v4}, LaFr;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=? AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1069
    invoke-static {}, LaEu;->a()LaEu;

    move-result-object v4

    invoke-virtual {v4}, LaEu;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 1070
    invoke-virtual {p1}, LaFM;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    .line 1067
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1073
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 1078
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v2

    .line 1076
    :cond_0
    :try_start_1
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {v0, p1, v1}, LaFZ;->a(LaEz;LaFM;Landroid/database/Cursor;)LaFZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 1078
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFZ;
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 783
    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    .line 784
    iget-object v1, p1, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-virtual {p0, v1}, LaGh;->a(LaFO;)LaFM;

    move-result-object v1

    .line 786
    invoke-virtual {v1}, LaFM;->a()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;->a()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, LaGh;->a(JJ)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 787
    invoke-direct {p0, v1, v0}, LaGh;->a(LaFM;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)LaFZ;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaFZ;
    .locals 1

    .prologue
    .line 793
    invoke-static {}, LamV;->b()V

    .line 794
    invoke-direct {p0, p1}, LaGh;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaFZ;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;LIK;[Ljava/lang/String;)LaGA;
    .locals 1

    .prologue
    .line 407
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, LaGh;->a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;LIK;[Ljava/lang/String;Ljava/lang/String;)LaGA;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;LIK;[Ljava/lang/String;Ljava/lang/String;)LaGA;
    .locals 12

    .prologue
    .line 413
    invoke-static {}, LamV;->b()V

    .line 414
    new-instance v0, Lwu;

    invoke-direct {v0, p0}, Lwu;-><init>(LaGg;)V

    .line 415
    invoke-interface {p1, v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a(LvR;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwv;

    .line 416
    iget-object v1, v0, Lwv;->a:Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    .line 418
    if-nez v1, :cond_1

    .line 422
    sget-object v1, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->d:Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-object v2, v1

    .line 427
    :goto_0
    const/4 v7, 0x0

    .line 428
    if-eqz p2, :cond_0

    .line 429
    iget-object v1, p0, LaGh;->a:Lbxw;

    invoke-interface {v1}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LIh;

    invoke-interface {v1, p2}, LIh;->a(LIK;)LIf;

    move-result-object v1

    .line 430
    invoke-virtual {v1}, LIf;->d()Ljava/lang/String;

    move-result-object v7

    .line 432
    :cond_0
    iget-object v1, p0, LaGh;->b:Lbxw;

    invoke-interface {v1}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lafz;

    iget-object v3, v0, Lwv;->a:LaFO;

    .line 433
    invoke-virtual {p0, v3}, LaGh;->a(LaFO;)LaFM;

    move-result-object v3

    .line 432
    invoke-interface {v1, v3, p1}, Lafz;->a(LaFM;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)Laff;

    move-result-object v1

    .line 434
    if-eqz v1, :cond_2

    invoke-interface {v1}, Laff;->c()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    move v9, v1

    .line 436
    :goto_1
    new-instance v10, LAC;

    iget-object v11, v0, Lwv;->a:LaFO;

    iget-object v0, p0, LaGh;->a:LaEz;

    const-string v1, "EntryView"

    .line 437
    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    .line 438
    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p3

    move-object/from16 v8, p4

    .line 437
    invoke-virtual/range {v0 .. v8}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {v10, v11, v0, p0, v9}, LAC;-><init>(LaFO;Landroid/database/Cursor;LaGg;Z)V

    return-object v10

    .line 424
    :cond_1
    sget-object v2, LaFL;->a:LaFL;

    sget-object v3, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a:Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a(LaFL;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v1

    move-object v2, v1

    goto :goto_0

    .line 434
    :cond_2
    const/4 v1, 0x0

    move v9, v1

    goto :goto_1
.end method

.method public a(Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;LaFZ;)LaGB;
    .locals 6

    .prologue
    .line 1639
    new-instance v0, LaGB;

    iget-object v1, p0, LaGh;->a:LaEz;

    .line 1640
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;->a()J

    move-result-wide v2

    invoke-virtual {p2}, LaFZ;->a()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, LaGB;-><init>(LaEz;JJ)V

    return-object v0
.end method

.method public a(LaGL;)LaGK;
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 1465
    invoke-virtual {p1}, LaGL;->a()LaFO;

    move-result-object v0

    invoke-direct {p0, v0}, LaGh;->a(LaFO;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 1466
    sget-object v1, LaFL;->a:LaFL;

    new-instance v3, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LaFb;->a:LaFb;

    .line 1467
    invoke-virtual {v5}, LaFb;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1468
    invoke-virtual {p1}, LaGL;->a()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x3

    new-array v4, v4, [Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v5, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, LaFb;->b:LaFb;

    .line 1469
    invoke-virtual {v7}, LaFb;->a()LaFr;

    move-result-object v7

    invoke-virtual {v7}, LaFr;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "=?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1470
    invoke-virtual {p1}, LaGL;->b()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v4, v8

    aput-object v0, v4, v9

    sget-object v0, LaFb;->h:LaFb;

    .line 1472
    invoke-virtual {v0}, LaFb;->a()LaFr;

    move-result-object v0

    invoke-virtual {p1}, LaGL;->a()Z

    move-result v5

    invoke-virtual {v0, v5}, LaFr;->a(Z)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    aput-object v0, v4, v10

    .line 1466
    invoke-virtual {v1, v3, v4}, LaFL;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;[Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v4

    .line 1474
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {}, LaFa;->a()LaFa;

    move-result-object v1

    invoke-virtual {v1}, LaFa;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    .line 1475
    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()[Ljava/lang/String;

    move-result-object v4

    const-string v5, "%s=\'%s\' DESC"

    new-array v6, v10, [Ljava/lang/Object;

    sget-object v7, LaFb;->i:LaFb;

    .line 1477
    invoke-virtual {v7}, LaFb;->a()LaFr;

    move-result-object v7

    invoke-virtual {v7}, LaFr;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    .line 1478
    invoke-virtual {p1}, LaGL;->a()LaGs;

    move-result-object v7

    invoke-virtual {v7}, LaGs;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    .line 1476
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "1"

    move-object v5, v2

    move-object v6, v2

    .line 1474
    invoke-virtual/range {v0 .. v8}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1481
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 1486
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v2

    .line 1484
    :cond_0
    :try_start_1
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {v0, v1}, LaGK;->a(LaEz;Landroid/database/Cursor;)LaGK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 1486
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/util/Date;LaFO;LaGs;Z)LaGK;
    .locals 12

    .prologue
    .line 1593
    new-instance v1, LaGK;

    iget-object v2, p0, LaGh;->a:LaEz;

    .line 1598
    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const/4 v9, 0x0

    .line 1601
    move-object/from16 v0, p7

    invoke-direct {p0, v0}, LaGh;->a(LaFO;)Ljava/lang/Long;

    move-result-object v10

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object/from16 v6, p8

    move-object/from16 v8, p6

    move/from16 v11, p9

    invoke-direct/range {v1 .. v11}, LaGK;-><init>(LaEz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LaGs;Ljava/lang/Long;Ljava/util/Date;ZLjava/lang/Long;Z)V

    return-object v1
.end method

.method public a(LaFM;Ljava/lang/String;J)LaGW;
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 319
    invoke-virtual {p1}, LaFM;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "Not persisted: %s"

    new-array v5, v1, [Ljava/lang/Object;

    aput-object p1, v5, v3

    invoke-static {v0, v4, v5}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 320
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    sget-object v0, LaFL;->a:LaFL;

    new-instance v4, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, LaFd;->b:LaFd;

    .line 322
    invoke-virtual {v6}, LaFd;->a()LaFr;

    move-result-object v6

    invoke-virtual {v6}, LaFr;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "=?"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, p2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-array v1, v1, [Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v5, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, LaFd;->a:LaFd;

    .line 323
    invoke-virtual {v7}, LaFd;->a()LaFr;

    move-result-object v7

    invoke-virtual {v7}, LaFr;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "=?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 324
    invoke-virtual {p1}, LaFM;->a()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v1, v3

    .line 321
    invoke-virtual {v0, v4, v1}, LaFL;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;[Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v4

    .line 327
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {}, LaFc;->a()LaFc;

    move-result-object v1

    invoke-virtual {v1}, LaFc;->c()Ljava/lang/String;

    move-result-object v1

    .line 328
    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    .line 327
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 331
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 333
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-virtual {p1}, LaFM;->a()J

    move-result-wide v2

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v0, v2, v3, p2, v4}, LaGW;->a(LaEz;JLjava/lang/String;Ljava/lang/Long;)LaGW;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 337
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_1
    return-object v0

    :cond_0
    move v0, v3

    .line 319
    goto/16 :goto_0

    .line 335
    :cond_1
    :try_start_1
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {v0, v1}, LaGW;->a(LaEz;Landroid/database/Cursor;)LaGW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 337
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public a(LaFM;Ljava/lang/String;J)LaGX;
    .locals 7

    .prologue
    .line 1678
    new-instance v0, LaGX;

    iget-object v1, p0, LaGh;->a:LaEz;

    .line 1679
    invoke-virtual {p1}, LaFM;->a()J

    move-result-wide v2

    move-object v4, p2

    move-wide v5, p3

    invoke-direct/range {v0 .. v6}, LaGX;-><init>(LaEz;JLjava/lang/String;J)V

    .line 1680
    return-object v0
.end method

.method public a(LaFM;Ljava/lang/String;)LaGa;
    .locals 2

    .prologue
    .line 806
    invoke-virtual {p1}, LaFM;->a()J

    move-result-wide v0

    invoke-static {v0, v1, p2}, LaGh;->a(JLjava/lang/String;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 807
    invoke-direct {p0, p1, v0}, LaGh;->a(LaFM;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)LaFZ;

    move-result-object v0

    .line 808
    if-nez v0, :cond_0

    .line 809
    new-instance v0, LaGa;

    iget-object v1, p0, LaGh;->a:LaEz;

    invoke-direct {v0, v1, p1, p2}, LaGa;-><init>(LaEz;LaFM;Ljava/lang/String;)V

    .line 810
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LaGa;->c(Z)LaGe;

    .line 813
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, LaFZ;->a()LaGa;

    move-result-object v0

    goto :goto_0
.end method

.method public a(LaFM;Ljava/lang/String;Ljava/lang/String;LaHa;)LaGb;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 541
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 542
    sget-object v0, LaGv;->a:LaGv;

    invoke-static {p3}, LaGv;->a(Ljava/lang/String;)LaGv;

    move-result-object v2

    invoke-virtual {v0, v2}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 544
    new-instance v0, LaGc;

    iget-object v2, p0, LaGh;->a:LaEz;

    invoke-direct {v0, v2, p1, p3, p4}, LaGc;-><init>(LaEz;LaFM;Ljava/lang/String;LaHa;)V

    .line 546
    invoke-virtual {v0, p2}, LaGc;->b(Ljava/lang/String;)V

    .line 548
    invoke-virtual {p1}, LaFM;->a()LaFO;

    move-result-object v2

    invoke-virtual {v2}, LaFO;->b()Ljava/lang/String;

    move-result-object v2

    .line 549
    invoke-virtual {v0, v2}, LaGc;->d(Ljava/lang/String;)LaGe;

    .line 550
    invoke-virtual {v0, v2}, LaGc;->b(Ljava/lang/String;)LaGe;

    .line 551
    invoke-virtual {v0, v2}, LaGc;->c(Ljava/lang/String;)LaGe;

    .line 552
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 553
    invoke-virtual {v0, v2}, LaGc;->a(Ljava/util/Date;)LaGe;

    .line 554
    invoke-virtual {v0, v2}, LaGc;->b(Ljava/util/Date;)LaGe;

    .line 555
    invoke-virtual {v0, v1}, LaGc;->c(Z)V

    .line 556
    invoke-virtual {v0}, LaGc;->b()LaGb;

    move-result-object v0

    return-object v0

    .line 542
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LaGp;)LaGb;
    .locals 2

    .prologue
    .line 1907
    invoke-virtual {p1}, LaGp;->c()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, LaGh;->a(J)LaGb;

    move-result-object v0

    return-object v0
.end method

.method public a(LaHe;)LaGb;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2093
    iget-object v0, p0, LaGh;->a:LaEz;

    const-string v1, "DocumentView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 2095
    invoke-static {}, LaER;->a()LaER;

    move-result-object v4

    invoke-virtual {v4}, LaER;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 2096
    invoke-virtual {p1}, LaHe;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    .line 2093
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2101
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2102
    sget-object v0, LaES;->E:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, v1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 2103
    invoke-virtual {p0, v4, v5}, LaGh;->a(J)LaFM;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2104
    if-nez v0, :cond_0

    .line 2112
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v2

    .line 2107
    :cond_0
    :try_start_1
    iget-object v2, p0, LaGh;->a:LaEz;

    invoke-static {v2, v0, v1}, LaGb;->a(LaEz;LaFM;Landroid/database/Cursor;)LaGb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 2112
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;
    .locals 1

    .prologue
    .line 1019
    invoke-static {}, LamV;->b()V

    .line 1020
    invoke-virtual {p0, p1}, LaGh;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGb;
    .locals 1

    .prologue
    .line 1049
    invoke-static {}, LamV;->b()V

    .line 1050
    invoke-direct {p0, p1}, LaGh;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGb;

    move-result-object v0

    return-object v0
.end method

.method public a(LaFM;Ljava/lang/String;Ljava/lang/String;)LaGc;
    .locals 6

    .prologue
    .line 596
    invoke-virtual {p1}, LaFM;->a()J

    move-result-wide v0

    invoke-static {v0, v1, p3}, LaGh;->a(JLjava/lang/String;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 597
    invoke-direct {p0, p1, v0}, LaGh;->a(LaFM;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)LaGb;

    move-result-object v0

    .line 598
    if-nez v0, :cond_0

    .line 599
    new-instance v0, LaGc;

    iget-object v1, p0, LaGh;->a:LaEz;

    invoke-direct {v0, v1, p1, p2, p3}, LaGc;-><init>(LaEz;LaFM;Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    :goto_0
    return-object v0

    .line 601
    :cond_0
    invoke-virtual {v0}, LaGb;->a()LaGv;

    move-result-object v1

    invoke-static {p2}, LaGv;->a(Ljava/lang/String;)LaGv;

    move-result-object v2

    invoke-virtual {v1, v2}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 602
    const-string v1, "DatabaseModelLoader"

    const-string v2, "Fetching %s as kind %s:%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 603
    invoke-virtual {v0}, LaGb;->a()LaGv;

    move-result-object v5

    invoke-virtual {v5}, LaGv;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    const/4 v4, 0x2

    aput-object p3, v3, v4

    .line 602
    invoke-static {v1, v2, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 606
    :cond_1
    invoke-virtual {v0}, LaGb;->a()LaGc;

    move-result-object v0

    goto :goto_0
.end method

.method public a(LaFM;Ljava/lang/String;)LaGd;
    .locals 2

    .prologue
    .line 1889
    invoke-virtual {p1}, LaFM;->a()J

    move-result-wide v0

    invoke-static {v0, v1, p2}, LaGh;->a(JLjava/lang/String;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v1

    .line 1890
    invoke-direct {p0, p1, v1}, LaGh;->a(LaFM;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)LaGb;

    move-result-object v0

    .line 1891
    if-nez v0, :cond_0

    .line 1892
    invoke-direct {p0, p1, v1}, LaGh;->a(LaFM;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)LaFZ;

    move-result-object v0

    .line 1894
    :cond_0
    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 989
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 990
    invoke-virtual {p0, p1}, LaGh;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;

    move-result-object v0

    .line 991
    if-nez v0, :cond_0

    .line 992
    invoke-virtual {p0, p1}, LaGh;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFZ;

    move-result-object v0

    .line 994
    :cond_0
    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGd;
    .locals 1

    .prologue
    .line 1007
    invoke-static {}, LamV;->b()V

    .line 1008
    invoke-virtual {p0, p1}, LaGh;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGd;

    move-result-object v0

    return-object v0
.end method

.method public a(LaGp;)LaGo;
    .locals 4

    .prologue
    .line 636
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LaEL;->c:LaEL;

    invoke-virtual {v1}, LaEL;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 637
    new-instance v1, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    .line 638
    invoke-virtual {p1}, LaGp;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 640
    invoke-direct {p0, v1}, LaGh;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;)LaGb;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0, p1}, LaGh;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGo;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0, p1}, LaGh;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGb;

    move-result-object v0

    return-object v0
.end method

.method public a(J)LaGp;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1089
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 1103
    :goto_0
    return-object v2

    .line 1092
    :cond_0
    iget-object v0, p0, LaGh;->a:LaEz;

    .line 1093
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v1

    invoke-virtual {v1}, LaEI;->c()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1094
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v4

    invoke-virtual {v4}, LaEI;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 1095
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    .line 1093
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1098
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 1103
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1101
    :cond_1
    :try_start_1
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {v0, v1}, LaGp;->a(LaEz;Landroid/database/Cursor;)LaGp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 1103
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public a(LaGo;)LaGp;
    .locals 2

    .prologue
    .line 1361
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1363
    invoke-direct {p0, p1}, LaGh;->b(LaGo;)LaGp;

    move-result-object v0

    .line 1364
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LaGp;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LaGo;LacY;)LaGp;
    .locals 2

    .prologue
    .line 1109
    invoke-interface {p1, p2}, LaGo;->a(LacY;)J

    move-result-wide v0

    .line 1110
    invoke-virtual {p0, v0, v1}, LaGh;->a(J)LaGp;

    move-result-object v0

    .line 1111
    return-object v0
.end method

.method public a(LaGp;)LaGp;
    .locals 4

    .prologue
    .line 1299
    invoke-virtual {p1}, LaGp;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1316
    :cond_0
    :goto_0
    return-object p1

    .line 1304
    :cond_1
    :try_start_0
    invoke-virtual {p1}, LaGp;->c()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, LaGh;->b(J)J
    :try_end_0
    .catch LaGj; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 1309
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_2

    .line 1310
    invoke-virtual {p0, v0, v1}, LaGh;->a(J)LaGp;

    move-result-object p1

    .line 1311
    invoke-virtual {p1}, LaGp;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1316
    :cond_2
    const/4 p1, 0x0

    goto :goto_0

    .line 1305
    :catch_0
    move-exception v0

    .line 1306
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Detected more than one reference to the document content: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Landroid/database/Cursor;)LaGp;
    .locals 1

    .prologue
    .line 1084
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {v0, p1}, LaGp;->a(LaEz;Landroid/database/Cursor;)LaGp;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)LaGr;
    .locals 1

    .prologue
    .line 1321
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {v0, p1}, LaGp;->a(LaEz;Ljava/lang/String;)LaGr;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGu;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0, p1}, LaGh;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGu;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0, p1}, LaGh;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGd;

    move-result-object v0

    return-object v0
.end method

.method public a(LaGb;)LaHd;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1842
    invoke-virtual {p1}, LaGb;->b()J

    move-result-wide v6

    .line 1843
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {}, LaFi;->a()LaFi;

    move-result-object v1

    invoke-virtual {v1}, LaFi;->c()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LaFj;->a:LaFj;

    .line 1844
    invoke-virtual {v4}, LaFj;->a()LaFr;

    move-result-object v4

    invoke-virtual {v4}, LaFr;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 1845
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    .line 1843
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1847
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 1852
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v2

    .line 1850
    :cond_0
    :try_start_1
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {v0, v1}, LaHd;->a(LaEz;Landroid/database/Cursor;)LaHd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 1852
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public a()Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 891
    new-instance v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LaEL;->c:LaEL;

    .line 892
    invoke-virtual {v3}, LaEL;->a()LaFr;

    move-result-object v3

    invoke-virtual {v3}, LaFr;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 893
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v3

    invoke-virtual {v3}, LaEI;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 897
    new-instance v1, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LaEJ;->q:LaEJ;

    .line 898
    invoke-virtual {v4}, LaEJ;->a()LaFr;

    move-result-object v4

    invoke-virtual {v4}, LaFr;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " IS NULL"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3, v2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 900
    sget-object v3, LaFL;->a:LaFL;

    sget-object v4, LaES;->y:LaES;

    .line 901
    invoke-virtual {v4}, LaES;->a()LaFr;

    move-result-object v4

    invoke-virtual {v4}, LaFr;->a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v4

    new-array v5, v7, [Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, LaFL;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;[Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v1

    .line 903
    sget-object v3, LaFL;->b:LaFL;

    new-array v4, v7, [Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    sget-object v5, LaEJ;->q:LaEJ;

    .line 904
    invoke-virtual {v5}, LaEJ;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->b()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v5

    aput-object v5, v4, v6

    .line 903
    invoke-virtual {v3, v1, v4}, LaFL;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;[Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v1

    .line 906
    new-instance v3, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 907
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v5

    invoke-virtual {v5}, LaEI;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " IN ( "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " SELECT DISTINCT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, LaEL;->c:LaEL;

    .line 908
    invoke-virtual {v5}, LaEL;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " FROM "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 909
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v5

    invoke-virtual {v5}, LaEI;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " INNER JOIN "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "DocumentView"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ON "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 910
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " WHERE "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 911
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0, v2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 915
    sget-object v0, LaFL;->b:LaFL;

    new-instance v1, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 916
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v5

    invoke-virtual {v5}, LaEI;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " IN ( "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " SELECT DISTINCT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, LaEJ;->r:LaEJ;

    .line 917
    invoke-virtual {v5}, LaEJ;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " FROM "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 918
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v5

    invoke-virtual {v5}, LaEI;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " WHERE "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, LaEJ;->r:LaEJ;

    .line 919
    invoke-virtual {v5}, LaEJ;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " IS NOT NULL ) AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, LaEJ;->q:LaEJ;

    .line 921
    invoke-virtual {v5}, LaEJ;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->b()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4, v2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 915
    invoke-virtual {v3, v0, v1}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a(LaFL;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 924
    sget-object v1, LaFL;->b:LaFL;

    new-instance v3, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 925
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v5

    invoke-virtual {v5}, LaEI;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " IN ( "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " SELECT DISTINCT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, LaEL;->d:LaEL;

    .line 926
    invoke-virtual {v5}, LaEL;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " FROM "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "DocumentView"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " WHERE "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, LaEL;->d:LaEL;

    .line 928
    invoke-virtual {v5}, LaEL;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " IS NOT NULL )"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 924
    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a(LaFL;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 931
    sget-object v1, LaFL;->a:LaFL;

    invoke-direct {p0}, LaGh;->a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a(LaFL;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v4

    .line 933
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v1

    invoke-virtual {v1}, LaEI;->c()Ljava/lang/String;

    move-result-object v1

    .line 934
    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()[Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, LaEJ;->j:LaEJ;

    .line 935
    invoke-virtual {v6}, LaEJ;->a()LaFr;

    move-result-object v6

    invoke-virtual {v6}, LaFr;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ASC"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v5, v2

    move-object v6, v2

    .line 933
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 503
    sget-object v0, LaFj;->d:LaFj;

    invoke-virtual {v0}, LaFj;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 504
    sget-object v1, LaFj;->e:LaFj;

    invoke-virtual {v1}, LaFj;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v1

    .line 505
    sget-object v2, LaFj;->l:LaFj;

    .line 506
    invoke-virtual {v2}, LaFj;->a()LaFr;

    move-result-object v2

    int-to-long v4, p1

    invoke-virtual {v2, v4, v5}, LaFr;->a(J)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v2

    .line 507
    sget-object v3, LaFL;->a:LaFL;

    const/4 v4, 0x2

    new-array v4, v4, [Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object v2, v4, v1

    .line 508
    invoke-virtual {v3, v0, v4}, LaFL;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;[Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 509
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LaFj;->b:LaFj;

    invoke-virtual {v2}, LaFj;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ASC "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 511
    invoke-virtual {p0, v0, v1}, LaGh;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 465
    iget-object v0, p0, LaGh;->a:LaEz;

    const-string v1, "DocumentRequestedToSyncView"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 467
    invoke-static {}, LaER;->a()LaER;

    move-result-object v4

    invoke-virtual {v4}, LaER;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, LaES;->E:LaES;

    invoke-virtual {v4}, LaES;->a()LaFr;

    move-result-object v4

    invoke-virtual {v4}, LaFr;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 468
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    .line 469
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()[Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    move-object v7, p2

    .line 465
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public a()LbmF;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmF",
            "<",
            "LaHe;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 819
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v8

    .line 821
    iget-object v0, p0, LaGh;->a:LaEz;

    .line 822
    invoke-static {}, LaFg;->a()LaFg;

    move-result-object v1

    invoke-virtual {v1}, LaFg;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    new-array v4, v3, [Ljava/lang/String;

    move-object v3, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    .line 821
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 831
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 832
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {v0, v1}, LaHe;->a(LaEz;Landroid/database/Cursor;)LaHe;

    move-result-object v0

    .line 833
    invoke-virtual {v8, v0}, LbmH;->a(Ljava/lang/Object;)LbmH;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 836
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 839
    invoke-virtual {v8}, LbmH;->a()LbmF;

    move-result-object v0

    return-object v0
.end method

.method public a()LbmY;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;"
        }
    .end annotation

    .prologue
    .line 477
    invoke-direct {p0}, LaGh;->b()J

    move-result-wide v0

    .line 478
    sget-object v2, LaFj;->i:LaFj;

    .line 479
    invoke-virtual {v2}, LaFj;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, LaFr;->b(J)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 480
    sget-object v1, LaFL;->a:LaFL;

    sget-object v2, LaFj;->g:LaFj;

    .line 481
    invoke-virtual {v2}, LaFj;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v2

    .line 480
    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a(LaFL;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 482
    sget-object v1, LaFL;->a:LaFL;

    sget-object v2, LaFj;->e:LaFj;

    .line 483
    invoke-virtual {v2}, LaFj;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v2

    .line 482
    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a(LaFL;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 485
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LaGh;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 486
    new-instance v0, Lbna;

    invoke-direct {v0}, Lbna;-><init>()V

    .line 488
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 489
    invoke-virtual {p0, v1}, LaGh;->a(Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    .line 490
    if-eqz v2, :cond_0

    .line 491
    invoke-virtual {v0, v2}, Lbna;->a(Ljava/lang/Object;)Lbna;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 495
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 498
    invoke-virtual {v0}, Lbna;->a()LbmY;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LbmY;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ")",
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2200
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2201
    invoke-static {}, LbmY;->a()Lbna;

    move-result-object v1

    .line 2202
    invoke-virtual {p0, p1}, LaGh;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;

    move-result-object v0

    .line 2203
    if-eqz v0, :cond_1

    .line 2205
    invoke-interface {v0}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-virtual {p0, v2}, LaGh;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Ljava/util/Map;

    move-result-object v2

    .line 2206
    invoke-interface {v0}, LaGu;->a()LaFM;

    move-result-object v3

    .line 2207
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 2208
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p0, v3, v4, v5}, LaGh;->a(LaFM;J)LaFZ;

    move-result-object v0

    .line 2209
    if-eqz v0, :cond_0

    .line 2210
    invoke-interface {v0}, LaFV;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbna;->a(Ljava/lang/Object;)Lbna;

    goto :goto_0

    .line 2214
    :cond_1
    invoke-virtual {v1}, Lbna;->a()LbmY;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;
    .locals 1

    .prologue
    .line 1900
    invoke-virtual {p0, p1}, LaGh;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGd;

    move-result-object v0

    .line 1901
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, LaGd;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v0

    goto :goto_0
.end method

.method public a(LaFM;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 2

    .prologue
    .line 1745
    invoke-virtual {p0}, LaGh;->a()V

    .line 1747
    :try_start_0
    const-string v0, "root"

    invoke-virtual {p0, p1, v0}, LaGh;->a(LaFM;Ljava/lang/String;)LaGa;

    move-result-object v0

    .line 1748
    invoke-virtual {v0}, LaGa;->n()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1749
    invoke-virtual {v0}, LaGa;->e()V

    .line 1751
    :cond_0
    invoke-virtual {p0}, LaGh;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1753
    invoke-virtual {p0}, LaGh;->b()V

    .line 1757
    invoke-virtual {v0}, LaGa;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v0

    .line 1758
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    return-object v0

    .line 1753
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LaGh;->b()V

    throw v0
.end method

.method public a(LaFO;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 1763
    invoke-virtual {p0, p1}, LaGh;->a(LaFO;)LaFM;

    move-result-object v0

    .line 1764
    invoke-virtual {p0, v0}, LaGh;->a(LaFM;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    return-object v0
.end method

.method public a(LaFO;Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 2195
    invoke-static {p1, p2}, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;->a(LaFO;Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 2

    .prologue
    .line 2082
    sget-object v0, LaES;->E:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 2083
    invoke-virtual {p0, v0, v1}, LaGh;->a(J)LaFM;

    move-result-object v0

    .line 2084
    if-nez v0, :cond_0

    .line 2085
    const/4 v0, 0x0

    .line 2088
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, LaFM;->a()LaFO;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;->a(Landroid/database/Cursor;LaFO;)Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0, p1}, LaGh;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/gms/drive/database/data/ResourceSpec;
    .locals 1

    .prologue
    .line 2027
    invoke-virtual {p0, p1}, LaGh;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;

    move-result-object v0

    .line 2028
    if-nez v0, :cond_0

    .line 2029
    const/4 v0, 0x0

    .line 2031
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, LaGu;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    goto :goto_0
.end method

.method public a(LaGA;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2189
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-virtual {v0}, LaEz;->a()LaIm;

    move-result-object v0

    invoke-interface {p1, v0}, LaGA;->a(LaIm;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LaGp;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 845
    .line 847
    :try_start_0
    sget-object v0, LaFL;->a:LaFL;

    new-instance v1, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 848
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v3

    invoke-virtual {v3}, LaEI;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " NOT IN ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, LaEL;->c:LaEL;

    .line 849
    invoke-virtual {v3}, LaEL;->a()LaFr;

    move-result-object v3

    .line 850
    invoke-static {}, LaEK;->a()LaEK;

    move-result-object v4

    .line 849
    invoke-virtual {v3, v4}, LaFr;->a(LaFy;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x5

    new-array v2, v2, [Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    const/4 v3, 0x0

    new-instance v4, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 851
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v6

    invoke-virtual {v6}, LaEI;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " NOT IN ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, LaEL;->d:LaEL;

    .line 852
    invoke-virtual {v6}, LaEL;->a()LaFr;

    move-result-object v6

    .line 853
    invoke-static {}, LaEK;->a()LaEK;

    move-result-object v7

    .line 852
    invoke-virtual {v6, v7}, LaFr;->a(LaFy;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x1

    new-instance v4, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 854
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v6

    invoke-virtual {v6}, LaEI;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " NOT IN ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, LaEJ;->r:LaEJ;

    .line 855
    invoke-virtual {v6}, LaEJ;->a()LaFr;

    move-result-object v6

    .line 856
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v7

    .line 855
    invoke-virtual {v6, v7}, LaFr;->a(LaFy;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 857
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v6

    invoke-virtual {v6}, LaEI;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " NOT IN ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, LaEp;->b:LaEp;

    .line 858
    invoke-virtual {v6}, LaEp;->a()LaFr;

    move-result-object v6

    .line 859
    invoke-static {}, LaEo;->a()LaEo;

    move-result-object v7

    .line 858
    invoke-virtual {v6, v7}, LaFr;->b(LaFy;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x3

    .line 861
    invoke-direct {p0}, LaGh;->a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    new-instance v4, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 862
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v6

    invoke-virtual {v6}, LaEI;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " NOT IN ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, LaFj;->o:LaFj;

    .line 863
    invoke-virtual {v6}, LaFj;->a()LaFr;

    move-result-object v6

    .line 864
    invoke-static {}, LaFi;->a()LaFi;

    move-result-object v7

    .line 863
    invoke-virtual {v6, v7}, LaFr;->a(LaFy;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    .line 847
    invoke-virtual {v0, v1, v2}, LaFL;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;[Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v4

    .line 866
    iget-object v0, p0, LaGh;->a:LaEz;

    .line 867
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v1

    invoke-virtual {v1}, LaEI;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 868
    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 866
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 871
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_2

    .line 882
    if-eqz v1, :cond_0

    .line 883
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    move-object v0, v8

    :cond_1
    :goto_0
    return-object v0

    .line 875
    :cond_2
    :try_start_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 877
    :cond_3
    iget-object v2, p0, LaGh;->a:LaEz;

    invoke-static {v2, v1}, LaGp;->a(LaEz;Landroid/database/Cursor;)LaGp;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 878
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v2

    if-nez v2, :cond_3

    .line 882
    if-eqz v1, :cond_1

    .line 883
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 882
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v8, :cond_4

    .line 883
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 882
    :catchall_1
    move-exception v0

    move-object v8, v1

    goto :goto_1
.end method

.method public a(LaFM;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFM;",
            ")",
            "Ljava/util/List",
            "<",
            "LaGX;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1685
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1686
    new-instance v4, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LaFf;->a:LaFf;

    .line 1687
    invoke-virtual {v1}, LaFf;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1688
    invoke-virtual {p1}, LaFM;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v0, v1}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1690
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {}, LaFe;->a()LaFe;

    move-result-object v1

    invoke-virtual {v1}, LaFe;->c()Ljava/lang/String;

    move-result-object v1

    .line 1692
    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    .line 1693
    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    .line 1690
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1698
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1700
    :cond_0
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {p1, v0, v1}, LaGX;->a(LaFM;LaEz;Landroid/database/Cursor;)LaGX;

    move-result-object v0

    .line 1701
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1702
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 1706
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v8

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public a(LaFR;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFR;",
            ")",
            "Ljava/util/List",
            "<",
            "LaFS;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1607
    invoke-virtual {p1}, LaFR;->c()J

    move-result-wide v4

    .line 1608
    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 1609
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v8

    .line 1610
    sget-object v0, LaEp;->a:LaEp;

    invoke-virtual {v0}, LaEp;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v3

    .line 1611
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {}, LaEo;->a()LaEo;

    move-result-object v1

    invoke-virtual {v1}, LaEo;->c()Ljava/lang/String;

    move-result-object v1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LaEp;->b:LaEp;

    .line 1617
    invoke-virtual {v4}, LaEp;->a()LaFr;

    move-result-object v4

    invoke-virtual {v4}, LaFr;->a()Ljava/lang/String;

    move-result-object v7

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    .line 1611
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1619
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1620
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {v0, v1}, LaFS;->a(LaEz;Landroid/database/Cursor;)LaFS;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1623
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1608
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1623
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1625
    return-object v8
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "LaGB;",
            ">;"
        }
    .end annotation

    .prologue
    .line 732
    check-cast p1, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    .line 733
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LaGh;->a(Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Z)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/Set;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LaFO;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 645
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 646
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {}, LaEe;->a()LaEe;

    move-result-object v1

    invoke-virtual {v1}, LaEe;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    sget-object v5, LaEf;->a:LaEf;

    .line 647
    invoke-virtual {v5}, LaEf;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5}, LaFr;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    .line 646
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 650
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 651
    sget-object v0, LaEf;->a:LaEf;

    .line 652
    invoke-virtual {v0}, LaEf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 654
    :cond_0
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 655
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 659
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v8

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public a(LaFM;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Ljava/util/Set;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFM;",
            "Lcom/google/android/gms/drive/database/common/SqlWhereClause;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 665
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v9

    .line 666
    invoke-static {p1}, LDK;->a(LaFM;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    sget-object v1, LaFL;->a:LaFL;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a(LaFL;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v4

    .line 667
    iget-object v0, p0, LaGh;->a:LaEz;

    const-string v1, "EntryView"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 668
    invoke-static {}, LaER;->a()LaER;

    move-result-object v6

    invoke-virtual {v6}, LaER;->d()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v3

    .line 669
    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()[Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    .line 667
    invoke-virtual/range {v0 .. v8}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 672
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 673
    invoke-virtual {p1}, LaFM;->a()LaFO;

    move-result-object v0

    .line 675
    :cond_0
    invoke-static {v1, v0}, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;->a(Landroid/database/Cursor;LaFO;)Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v2

    .line 676
    invoke-interface {v9, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 677
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 681
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v9

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 1645
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-virtual {v0}, LaEz;->b()V

    .line 1646
    return-void
.end method

.method public a(LZS;)V
    .locals 1

    .prologue
    .line 2036
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-virtual {v0, p1}, LaEz;->a(LZS;)V

    .line 2037
    return-void
.end method

.method public a(LaFM;)V
    .locals 6

    .prologue
    .line 222
    invoke-virtual {p0}, LaGh;->a()V

    .line 224
    :try_start_0
    invoke-virtual {p1}, LaFM;->a()LaFO;

    move-result-object v0

    invoke-direct {p0, v0}, LaGh;->b(LaFO;)LaFQ;

    move-result-object v1

    .line 225
    invoke-virtual {v1}, LaFQ;->c()J

    move-result-wide v2

    invoke-virtual {p1}, LaFM;->a()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 227
    if-eqz v1, :cond_0

    .line 228
    invoke-virtual {v1}, LaFQ;->f()V

    .line 230
    :cond_0
    iget-object v0, p0, LaGh;->a:LaFN;

    invoke-virtual {v0, p1}, LaFN;->b(LaFM;)V

    .line 231
    invoke-virtual {p0}, LaGh;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 233
    invoke-virtual {p0}, LaGh;->b()V

    .line 235
    return-void

    .line 225
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 233
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LaGh;->b()V

    throw v0
.end method

.method public a(LaFT;)V
    .locals 4

    .prologue
    .line 244
    iget-object v0, p0, LaGh;->a:LaEz;

    sget-object v1, LaEG;->f:LaEG;

    invoke-virtual {v1}, LaEG;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1}, LaFT;->c()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, LaEz;->a(Landroid/net/Uri;J)V

    .line 245
    return-void
.end method

.method public a(LaGp;)V
    .locals 3

    .prologue
    .line 1146
    invoke-virtual {p1}, LaGp;->c()J

    move-result-wide v0

    invoke-virtual {p1}, LaGp;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LaGh;->a(JLjava/lang/String;)V

    .line 1147
    return-void
.end method

.method public a(LaGp;LaGp;)V
    .locals 4

    .prologue
    .line 1770
    invoke-virtual {p0}, LaGh;->a()V

    .line 1773
    :try_start_0
    invoke-virtual {p1}, LaGp;->c()J

    move-result-wide v0

    invoke-virtual {p2}, LaGp;->c()J

    move-result-wide v2

    invoke-direct {p0, v0, v1, v2, v3}, LaGh;->a(JJ)I

    move-result v0

    .line 1774
    const/4 v1, 0x1

    if-gt v0, v1, :cond_1

    .line 1775
    invoke-virtual {p0}, LaGh;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1786
    :cond_0
    invoke-virtual {p0}, LaGh;->b()V

    .line 1788
    return-void

    .line 1777
    :cond_1
    :try_start_1
    iget-boolean v0, p0, LaGh;->a:Z

    if-nez v0, :cond_2

    .line 1778
    invoke-virtual {p0}, LaGh;->c()V

    .line 1780
    :cond_2
    iget-boolean v0, p0, LaGh;->b:Z

    if-eqz v0, :cond_0

    .line 1781
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Detected more than one reference to the document content: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1786
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LaGh;->b()V

    throw v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaGY;)V
    .locals 4

    .prologue
    .line 1181
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1182
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1184
    invoke-virtual {p0}, LaGh;->a()V

    .line 1186
    :try_start_0
    invoke-virtual {p0, p1}, LaGh;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;

    move-result-object v0

    .line 1187
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LaGb;->f()Z

    move-result v1

    invoke-virtual {p2}, LaGY;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-ne v1, v2, :cond_1

    .line 1202
    :cond_0
    invoke-virtual {p0}, LaGh;->b()V

    .line 1204
    :goto_0
    return-void

    .line 1191
    :cond_1
    :try_start_1
    invoke-virtual {v0}, LaGb;->a()LaGc;

    move-result-object v0

    .line 1192
    invoke-virtual {v0, p2}, LaGc;->a(LaGY;)LaGe;

    .line 1193
    invoke-virtual {v0}, LaGc;->b()LaGb;

    move-result-object v1

    .line 1194
    invoke-virtual {p2}, LaGY;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1195
    invoke-direct {p0, v1}, LaGh;->a(LaGo;)V

    .line 1197
    :cond_2
    invoke-virtual {v0}, LaGc;->a()LaFM;

    move-result-object v1

    .line 1198
    iget-object v2, p0, LaGh;->a:Lahj;

    invoke-virtual {v1}, LaFM;->a()LaFO;

    move-result-object v3

    .line 1199
    invoke-virtual {v0}, LaGc;->g()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0, v1}, LaGh;->a(LaFM;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    .line 1198
    :goto_1
    invoke-interface {v2, v3, v0}, Lahj;->a(LaFO;Z)V

    .line 1200
    invoke-virtual {p0}, LaGh;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1202
    invoke-virtual {p0}, LaGh;->b()V

    goto :goto_0

    .line 1199
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 1202
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LaGh;->b()V

    throw v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaHf;Z)V
    .locals 6

    .prologue
    .line 2119
    check-cast p1, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    .line 2120
    new-instance v0, LaHe;

    iget-object v1, p0, LaGh;->a:LaEz;

    .line 2121
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;->a()J

    move-result-wide v2

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, LaHe;-><init>(LaEz;JLaHf;Z)V

    .line 2122
    invoke-virtual {v0}, LaHe;->e()V

    .line 2123
    return-void
.end method

.method public a(JLjava/util/Set;)Z
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 371
    const-string v0, "null resourceIds"

    invoke-static {p3, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 372
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    move v0, v8

    :goto_0
    const-string v1, "Invalid cachedSearchId: %s"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-static {v0, v1, v2}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 373
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-virtual {v0}, LaEz;->b()V

    .line 375
    :try_start_0
    invoke-static {}, LaEs;->a()LaEs;

    move-result-object v0

    .line 376
    new-instance v4, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, LaEs;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v0, v1}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    iget-object v0, p0, LaGh;->a:LaEz;

    .line 379
    invoke-static {}, LaEs;->a()LaEs;

    move-result-object v1

    invoke-virtual {v1}, LaEs;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 380
    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 379
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 384
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    .line 386
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 388
    if-nez v1, :cond_1

    .line 399
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-virtual {v0}, LaEz;->c()V

    :goto_1
    return v9

    :cond_0
    move v0, v9

    .line 372
    goto :goto_0

    .line 386
    :catchall_0
    move-exception v1

    :try_start_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 399
    :catchall_1
    move-exception v0

    iget-object v1, p0, LaGh;->a:LaEz;

    invoke-virtual {v1}, LaEz;->c()V

    throw v0

    .line 391
    :cond_1
    :try_start_4
    invoke-interface {p3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 392
    new-instance v2, LaFU;

    iget-object v3, p0, LaGh;->a:LaEz;

    invoke-direct {v2, v3, p1, p2, v0}, LaFU;-><init>(LaEz;JLjava/lang/String;)V

    .line 393
    invoke-virtual {v2}, LaFU;->e()V

    goto :goto_2

    .line 395
    :cond_2
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-virtual {v0}, LaEz;->d()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 399
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-virtual {v0}, LaEz;->c()V

    move v9, v8

    goto :goto_1
.end method

.method public a(LZS;)Z
    .locals 1

    .prologue
    .line 2041
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-virtual {v0, p1}, LaEz;->a(LZS;)Z

    move-result v0

    return v0
.end method

.method public a(LaFM;)Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 2171
    invoke-static {p1}, LaGh;->a(LaFM;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v4

    .line 2172
    iget-object v0, p0, LaGh;->a:LaEz;

    const-string v1, "DocumentView"

    .line 2174
    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    .line 2175
    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()[Ljava/lang/String;

    move-result-object v4

    const-string v8, "1"

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    .line 2172
    invoke-virtual/range {v0 .. v8}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2181
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 2183
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return v0

    .line 2181
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2183
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public a(LaGo;LacY;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1374
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1376
    invoke-interface {p1, p2}, LaGo;->a(LacY;)J

    move-result-wide v2

    .line 1378
    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1406
    :cond_0
    :goto_0
    return v0

    .line 1382
    :cond_1
    invoke-virtual {p0, v2, v3}, LaGh;->a(J)LaGp;

    move-result-object v1

    .line 1383
    if-eqz v1, :cond_0

    .line 1387
    invoke-direct {p0, v1, p1}, LaGh;->a(LaGp;LaGo;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1388
    const/4 v0, 0x1

    goto :goto_0

    .line 1394
    :cond_2
    invoke-virtual {v1}, LaGp;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1398
    invoke-virtual {v1}, LaGp;->a()Ljava/lang/Long;

    move-result-object v1

    .line 1399
    if-eqz v1, :cond_0

    .line 1402
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, LaGh;->a(J)LaGp;

    move-result-object v1

    .line 1403
    if-eqz v1, :cond_0

    .line 1406
    invoke-direct {p0, v1, p1}, LaGh;->a(LaGp;LaGo;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(LaGp;)Z
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 1116
    iget-object v0, p0, LaGh;->a:LaEz;

    const-string v1, "DocumentView"

    new-array v2, v8, [Ljava/lang/String;

    .line 1117
    invoke-static {}, LaEK;->a()LaEK;

    move-result-object v3

    invoke-virtual {v3}, LaEK;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LaEL;->c:LaEL;

    .line 1118
    invoke-virtual {v4}, LaEL;->a()LaFr;

    move-result-object v4

    invoke-virtual {v4}, LaFr;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/String;

    .line 1119
    invoke-virtual {p1}, LaGp;->c()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v9

    move-object v6, v5

    move-object v7, v5

    .line 1116
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1124
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_0

    .line 1140
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    move v0, v8

    :goto_0
    return v0

    .line 1127
    :cond_0
    :try_start_1
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v1

    invoke-virtual {v1}, LaEI;->c()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 1128
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v4

    invoke-virtual {v4}, LaEI;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LaEJ;->r:LaEJ;

    .line 1129
    invoke-virtual {v4}, LaEJ;->a()LaFr;

    move-result-object v4

    invoke-virtual {v4}, LaFr;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 1130
    invoke-virtual {p1}, LaGp;->c()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 1127
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 1135
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    .line 1137
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1140
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1137
    :catchall_0
    move-exception v0

    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1140
    :catchall_1
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaGZ;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 1209
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1210
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1212
    invoke-virtual {p2}, LaGZ;->a()LaGY;

    move-result-object v3

    .line 1214
    invoke-virtual {p0}, LaGh;->a()V

    .line 1216
    :try_start_0
    invoke-virtual {p0, p1}, LaGh;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 1217
    if-nez v4, :cond_0

    .line 1253
    invoke-virtual {p0}, LaGh;->b()V

    :goto_0
    return v0

    .line 1221
    :cond_0
    :try_start_1
    invoke-virtual {v4}, LaGb;->f()Z

    move-result v1

    invoke-virtual {v3}, LaGY;->a()Z

    move-result v5

    if-eq v1, v5, :cond_1

    move v1, v2

    .line 1224
    :goto_1
    invoke-virtual {v4}, LaGb;->d()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v3}, LaGY;->a()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/Date;->after(Ljava/util/Date;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v5

    if-eqz v5, :cond_2

    .line 1253
    invoke-virtual {p0}, LaGh;->b()V

    goto :goto_0

    :cond_1
    move v1, v0

    .line 1221
    goto :goto_1

    .line 1228
    :cond_2
    :try_start_2
    invoke-virtual {v3}, LaGY;->a()Z

    move-result v5

    if-nez v5, :cond_3

    .line 1229
    invoke-virtual {v3}, LaGY;->a()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v4}, LaGb;->d()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v5

    if-eqz v5, :cond_3

    .line 1253
    invoke-virtual {p0}, LaGh;->b()V

    goto :goto_0

    .line 1236
    :cond_3
    :try_start_3
    invoke-virtual {v4}, LaGb;->a()LaGc;

    move-result-object v5

    .line 1237
    iget-object v6, p0, LaGh;->a:Lahf;

    invoke-interface {v6, v4}, Lahf;->a(LaGo;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1238
    invoke-direct {p0, v5, p2}, LaGh;->a(LaGc;LaGZ;)V

    .line 1240
    :cond_4
    invoke-virtual {v5, v3}, LaGc;->a(LaGY;)LaGe;

    .line 1241
    invoke-virtual {v5}, LaGc;->b()LaGb;

    move-result-object v4

    .line 1243
    invoke-virtual {v3}, LaGY;->a()Z

    move-result v3

    if-nez v3, :cond_5

    .line 1244
    invoke-direct {p0, v4}, LaGh;->a(LaGo;)V

    .line 1246
    :cond_5
    invoke-virtual {v4}, LaGb;->a()LaFM;

    move-result-object v3

    .line 1247
    iget-object v5, p0, LaGh;->a:Lahj;

    invoke-virtual {v3}, LaFM;->a()LaFO;

    move-result-object v6

    .line 1248
    invoke-virtual {v4}, LaGb;->f()Z

    move-result v4

    if-nez v4, :cond_6

    invoke-virtual {p0, v3}, LaGh;->a(LaFM;)Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_6
    move v0, v2

    .line 1247
    :cond_7
    invoke-interface {v5, v6, v0}, Lahj;->a(LaFO;Z)V

    .line 1249
    invoke-virtual {p0}, LaGh;->c()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1253
    invoke-virtual {p0}, LaGh;->b()V

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LaGh;->b()V

    throw v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 947
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 948
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LaEJ;->g:LaEJ;

    .line 949
    invoke-virtual {v1}, LaEJ;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 950
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    .line 948
    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a(Ljava/lang/String;Ljava/util/Collection;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 952
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v1

    invoke-virtual {v1}, LaEI;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, LaGh;->a(Ljava/lang/String;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Z

    move-result v0

    return v0
.end method

.method public synthetic b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFV;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 94
    invoke-virtual {p0, p1}, LaGh;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFZ;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFZ;
    .locals 1

    .prologue
    .line 1013
    invoke-static {}, LamV;->b()V

    .line 1014
    invoke-virtual {p0, p1}, LaGh;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFZ;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1039
    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    .line 1040
    iget-object v1, p1, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-virtual {p0, v1}, LaGh;->a(LaFO;)LaFM;

    move-result-object v1

    .line 1042
    invoke-virtual {v1}, LaFM;->a()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;->a()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, LaGh;->a(JJ)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 1043
    invoke-direct {p0, v1, v0}, LaGh;->a(LaFM;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)LaGb;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;
    .locals 1

    .prologue
    .line 1000
    invoke-static {}, LamV;->b()V

    .line 1001
    invoke-virtual {p0, p1}, LaGh;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGd;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1027
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1028
    invoke-direct {p0, p1}, LaGh;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGb;

    move-result-object v0

    .line 1029
    if-nez v0, :cond_0

    .line 1030
    invoke-direct {p0, p1}, LaGh;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaFZ;

    move-result-object v0

    .line 1032
    :cond_0
    return-object v0
.end method

.method public synthetic b(LaGp;)LaGo;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0, p1}, LaGh;->a(LaGp;)LaGb;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 94
    invoke-virtual {p0, p1}, LaGh;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGu;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 94
    invoke-virtual {p0, p1}, LaGh;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGu;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 94
    invoke-virtual {p0, p1}, LaGh;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGd;

    move-result-object v0

    return-object v0
.end method

.method public b(LaGb;)LaHd;
    .locals 6

    .prologue
    .line 1824
    invoke-virtual {p1}, LaGb;->b()J

    move-result-wide v2

    .line 1825
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 1826
    invoke-virtual {p0, p1}, LaGh;->a(LaGo;)LaGp;

    move-result-object v0

    .line 1827
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LaGp;->c()J

    move-result-wide v4

    .line 1828
    :goto_1
    new-instance v0, LaHd;

    iget-object v1, p0, LaGh;->a:LaEz;

    invoke-direct/range {v0 .. v5}, LaHd;-><init>(LaEz;JJ)V

    .line 1831
    return-object v0

    .line 1825
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1827
    :cond_1
    const-wide/16 v4, -0x1

    goto :goto_1
.end method

.method public b()V
    .locals 1

    .prologue
    .line 1650
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-virtual {v0}, LaEz;->c()V

    .line 1651
    return-void
.end method

.method public b(LaFM;)V
    .locals 4

    .prologue
    .line 239
    iget-object v0, p0, LaGh;->a:LaEz;

    sget-object v1, LaEG;->b:LaEG;

    invoke-virtual {v1}, LaEG;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1}, LaFM;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, LaEz;->a(Landroid/net/Uri;J)V

    .line 240
    return-void
.end method

.method public b(LaFM;)Z
    .locals 4

    .prologue
    .line 1870
    invoke-static {}, LaHd;->a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    sget-object v1, LaFL;->a:LaFL;

    sget-object v2, LaFj;->d:LaFj;

    .line 1871
    invoke-virtual {v2}, LaFj;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v2

    .line 1870
    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a(LaFL;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 1874
    new-instance v1, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1875
    invoke-static {}, LaER;->a()LaER;

    move-result-object v3

    invoke-virtual {v3}, LaER;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " IN ( "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " SELECT "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, LaFj;->a:LaFj;

    .line 1876
    invoke-virtual {v3}, LaFj;->a()LaFr;

    move-result-object v3

    invoke-virtual {v3}, LaFr;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " FROM "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1877
    invoke-static {}, LaFi;->a()LaFi;

    move-result-object v3

    invoke-virtual {v3}, LaFi;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " WHERE "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1878
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, LaES;->E:LaES;

    .line 1879
    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " = ?"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, LaFM;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1880
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-static {}, LaER;->a()LaER;

    move-result-object v2

    invoke-virtual {v2}, LaER;->c()Ljava/lang/String;

    move-result-object v2

    .line 1881
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()[Ljava/lang/String;

    move-result-object v1

    .line 1880
    invoke-virtual {v0, v2, v3, v1}, LaEz;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1883
    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(LaGo;LacY;)Z
    .locals 1

    .prologue
    .line 2219
    iget-object v0, p0, LaGh;->c:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladi;

    .line 2220
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221
    invoke-interface {v0, p1, p2}, Ladi;->a(LaGo;LacY;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 957
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 958
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LaEm;->d:LaEm;

    .line 959
    invoke-virtual {v1}, LaEm;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 960
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    .line 958
    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a(Ljava/lang/String;Ljava/util/Collection;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 962
    invoke-static {}, LaEl;->a()LaEl;

    move-result-object v1

    invoke-virtual {v1}, LaEl;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, LaGh;->a(Ljava/lang/String;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 1655
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-virtual {v0}, LaEz;->d()V

    .line 1656
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 2127
    iget-object v0, p0, LaGh;->a:LtK;

    sget-object v1, Lry;->o:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2128
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-virtual {v0}, LaEz;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2129
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 2132
    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 1667
    iget-object v0, p0, LaGh;->a:LaEz;

    invoke-virtual {v0}, LaEz;->d()Z

    .line 1668
    return-void
.end method
