.class public final enum LapI;
.super Ljava/lang/Enum;
.source "DocThumbnailView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LapI;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LapI;

.field private static final synthetic a:[LapI;

.field public static final enum b:LapI;

.field public static final enum c:LapI;

.field public static final enum d:LapI;


# instance fields
.field private final a:[I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 21
    new-instance v0, LapI;

    const-string v1, "STATE_HAS_BACKGROUND"

    sget v2, LwX;->has_background:I

    invoke-direct {v0, v1, v3, v2}, LapI;-><init>(Ljava/lang/String;II)V

    sput-object v0, LapI;->a:LapI;

    .line 22
    new-instance v0, LapI;

    const-string v1, "STATE_HAS_GIF_ANIMATION"

    sget v2, LwX;->has_gif_animation:I

    invoke-direct {v0, v1, v4, v2}, LapI;-><init>(Ljava/lang/String;II)V

    sput-object v0, LapI;->b:LapI;

    .line 23
    new-instance v0, LapI;

    const-string v1, "STATE_HAS_VIDEO_ANIMATION"

    sget v2, LwX;->has_video_animation:I

    invoke-direct {v0, v1, v5, v2}, LapI;-><init>(Ljava/lang/String;II)V

    sput-object v0, LapI;->c:LapI;

    .line 24
    new-instance v0, LapI;

    const-string v1, "STATE_HAS_FOLDER_ICON"

    sget v2, LwX;->has_folder_icon:I

    invoke-direct {v0, v1, v6, v2}, LapI;-><init>(Ljava/lang/String;II)V

    sput-object v0, LapI;->d:LapI;

    .line 20
    const/4 v0, 0x4

    new-array v0, v0, [LapI;

    sget-object v1, LapI;->a:LapI;

    aput-object v1, v0, v3

    sget-object v1, LapI;->b:LapI;

    aput-object v1, v0, v4

    sget-object v1, LapI;->c:LapI;

    aput-object v1, v0, v5

    sget-object v1, LapI;->d:LapI;

    aput-object v1, v0, v6

    sput-object v0, LapI;->a:[LapI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 29
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p3, v0, v1

    iput-object v0, p0, LapI;->a:[I

    .line 30
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LapI;
    .locals 1

    .prologue
    .line 20
    const-class v0, LapI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LapI;

    return-object v0
.end method

.method public static values()[LapI;
    .locals 1

    .prologue
    .line 20
    sget-object v0, LapI;->a:[LapI;

    invoke-virtual {v0}, [LapI;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LapI;

    return-object v0
.end method


# virtual methods
.method public a()[I
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, LapI;->a:[I

    return-object v0
.end method
