.class public LLE;
.super LLQ;
.source "SelectionFunctionDoOpen.java"


# instance fields
.field private final a:LPp;

.field private final a:LaGM;

.field private final a:LaKR;


# direct methods
.method constructor <init>(LaGM;LPp;LaKR;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, LLQ;-><init>()V

    .line 31
    iput-object p1, p0, LLE;->a:LaGM;

    .line 32
    iput-object p2, p0, LLE;->a:LPp;

    .line 33
    iput-object p3, p0, LLE;->a:LaKR;

    .line 34
    return-void
.end method

.method private final a(LaGv;)LacY;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p1}, LaGv;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    sget-object v0, LacY;->b:LacY;

    .line 40
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LacY;->a:LacY;

    goto :goto_0
.end method

.method private a(LaGu;)Z
    .locals 2

    .prologue
    .line 45
    invoke-interface {p1}, LaGu;->a()LaGv;

    move-result-object v0

    invoke-direct {p0, v0}, LLE;->a(LaGv;)LacY;

    move-result-object v0

    .line 46
    instance-of v1, p1, LaGo;

    if-eqz v1, :cond_0

    iget-object v1, p0, LLE;->a:LaGM;

    check-cast p1, LaGo;

    .line 47
    invoke-interface {v1, p1, v0}, LaGM;->b(LaGo;LacY;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/Runnable;LaFM;LbmF;LbmF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "LaFM;",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-static {p3}, Lbnm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    .line 68
    iget-object v1, p0, LLE;->a:LPp;

    invoke-interface {v1, v0}, LPp;->c(LaGu;)V

    .line 69
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 70
    return-void
.end method

.method public a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 53
    invoke-super {p0, p1, p2}, LLQ;->a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 60
    :goto_0
    return v2

    .line 56
    :cond_0
    invoke-virtual {p1, v2}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    .line 58
    invoke-interface {v0}, LaGu;->f()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    move v3, v1

    .line 59
    :goto_1
    if-eqz v3, :cond_3

    iget-object v3, p0, LLE;->a:LaKR;

    .line 60
    invoke-interface {v3}, LaKR;->a()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-direct {p0, v0}, LLE;->a(LaGu;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    move v0, v1

    :goto_2
    move v2, v0

    goto :goto_0

    :cond_2
    move v3, v2

    .line 58
    goto :goto_1

    :cond_3
    move v0, v2

    .line 60
    goto :goto_2
.end method
