.class public final enum LKc;
.super Ljava/lang/Enum;
.source "SelectionModelItemValue.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LKc;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LKc;

.field private static final synthetic a:[LKc;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 30
    new-instance v0, LKc;

    const-string v1, "FOLDER_ITEM_IS_SELECTED"

    invoke-direct {v0, v1, v2}, LKc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LKc;->a:LKc;

    .line 28
    const/4 v0, 0x1

    new-array v0, v0, [LKc;

    sget-object v1, LKc;->a:LKc;

    aput-object v1, v0, v2

    sput-object v0, LKc;->a:[LKc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LKc;
    .locals 1

    .prologue
    .line 28
    const-class v0, LKc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LKc;

    return-object v0
.end method

.method public static values()[LKc;
    .locals 1

    .prologue
    .line 28
    sget-object v0, LKc;->a:[LKc;

    invoke-virtual {v0}, [LKc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LKc;

    return-object v0
.end method
