.class public LBI;
.super Landroid/widget/BaseAdapter;
.source "DocListSyncMoreSpinnerAdapter.java"


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field private final a:LBa;

.field private final a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z


# direct methods
.method public constructor <init>(Laja;LBa;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "Landroid/app/Activity;",
            ">;",
            "LBa;",
            ")V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, LBI;->a:Z

    .line 36
    iput-object p1, p0, LBI;->a:Laja;

    .line 37
    iput-object p2, p0, LBI;->a:LBa;

    .line 38
    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 64
    if-eqz p1, :cond_0

    .line 70
    :goto_0
    return-object p1

    .line 68
    :cond_0
    iget-object v0, p0, LBI;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 69
    sget v1, Lxe;->doc_list_view_sync_more_spinner:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    goto :goto_0
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, LBI;->a:Z

    return v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, LBI;->a:Z

    if-nez v0, :cond_0

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, LBI;->a:Z

    .line 79
    invoke-virtual {p0}, LBI;->notifyDataSetChanged()V

    .line 81
    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, LBI;->a:Z

    if-eqz v0, :cond_0

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, LBI;->a:Z

    .line 91
    invoke-virtual {p0}, LBI;->notifyDataSetChanged()V

    .line 93
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, LBI;->a:LBa;

    invoke-virtual {v0}, LBa;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, LBI;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 52
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 57
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 58
    invoke-direct {p0, p2, p3}, LBI;->a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 60
    return-object v0

    .line 57
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
