.class public final Lbib;
.super Ljava/lang/ref/PhantomReference;
.source "PictureTracker.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/ref/PhantomReference",
        "<",
        "Lcom/google/bionics/scanner/unveil/util/Picture;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/bionics/scanner/unveil/util/Picture;

.field final synthetic a:Lcom/google/bionics/scanner/unveil/util/PictureTracker;


# direct methods
.method public constructor <init>(Lcom/google/bionics/scanner/unveil/util/PictureTracker;Lcom/google/bionics/scanner/unveil/util/Picture;Ljava/lang/ref/ReferenceQueue;Lcom/google/bionics/scanner/unveil/util/Picture;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/bionics/scanner/unveil/util/Picture;",
            "Ljava/lang/ref/ReferenceQueue",
            "<-",
            "Lcom/google/bionics/scanner/unveil/util/Picture;",
            ">;",
            "Lcom/google/bionics/scanner/unveil/util/Picture;",
            ")V"
        }
    .end annotation

    .prologue
    .line 141
    iput-object p1, p0, Lbib;->a:Lcom/google/bionics/scanner/unveil/util/PictureTracker;

    .line 142
    invoke-direct {p0, p2, p3}, Ljava/lang/ref/PhantomReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 143
    iput-object p4, p0, Lbib;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    .line 144
    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 147
    iget-object v0, p0, Lbib;->a:Lcom/google/bionics/scanner/unveil/util/PictureTracker;

    iget-object v1, p0, Lbib;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    invoke-static {v0, v1}, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a(Lcom/google/bionics/scanner/unveil/util/PictureTracker;Lcom/google/bionics/scanner/unveil/util/Picture;)Ljava/lang/String;

    move-result-object v0

    .line 148
    iget-object v1, p0, Lbib;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    invoke-virtual {v1}, Lcom/google/bionics/scanner/unveil/util/Picture;->recycle()V

    .line 149
    invoke-static {}, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v1

    const-string v2, "Recycled %s, total %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    iget-object v4, p0, Lbib;->a:Lcom/google/bionics/scanner/unveil/util/PictureTracker;

    iget-object v4, v4, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 150
    return-void
.end method
