.class final LaBV;
.super Ljava/lang/Object;

# interfaces
.implements LaCS;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LaCS",
        "<",
        "LaQO;",
        "LaCb;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const v0, 0x7fffffff

    return v0
.end method

.method public bridge synthetic a(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/internal/gw;Ljava/lang/Object;LaCX;LaCY;)LaCR;
    .locals 7

    move-object v4, p4

    check-cast v4, LaCb;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, LaBV;->a(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/internal/gw;LaCb;LaCX;LaCY;)LaQO;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/internal/gw;LaCb;LaCX;LaCY;)LaQO;
    .locals 9

    const-string v0, "Setting the API options is required."

    invoke-static {p4, v0}, LaSc;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, LaQO;

    iget-object v3, p4, LaCb;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-static {p4}, LaCb;->a(LaCb;)I

    move-result v1

    int-to-long v4, v1

    iget-object v6, p4, LaCb;->a:LaCd;

    move-object v1, p1

    move-object v2, p2

    move-object v7, p5

    move-object v8, p6

    invoke-direct/range {v0 .. v8}, LaQO;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/cast/CastDevice;JLaCd;LaCX;LaCY;)V

    return-object v0
.end method
