.class LWf;
.super LEC;
.source "CommonPreferencesInstaller.java"


# instance fields
.field final synthetic a:LWa;


# direct methods
.method constructor <init>(LWa;)V
    .locals 0

    .prologue
    .line 257
    iput-object p1, p0, LWf;->a:LWa;

    invoke-direct {p0}, LEC;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 260
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxi;->about_dialog:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 261
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 262
    iget-object v2, p0, LWf;->a:LWa;

    iget-object v2, v2, LWa;->a:Landroid/app/Activity;

    invoke-static {v2}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v2

    const v3, 0x1080045

    .line 263
    invoke-virtual {v2, v3}, LEU;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    sget v3, Lxi;->app_name:I

    .line 264
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 265
    invoke-static {}, Laml;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x104000a

    const/4 v3, 0x0

    .line 266
    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v2, Lxi;->policy_privacy:I

    new-instance v3, LWh;

    invoke-direct {v3, p0, p1, v1}, LWh;-><init>(LWf;Landroid/content/Context;Ljava/lang/String;)V

    .line 267
    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v2, Lxi;->policy_terms:I

    new-instance v3, LWg;

    invoke-direct {v3, p0, p1, v1}, LWg;-><init>(LWf;Landroid/content/Context;Ljava/lang/String;)V

    .line 276
    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 286
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
