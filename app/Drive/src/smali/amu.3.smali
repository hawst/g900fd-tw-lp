.class public Lamu;
.super Ljava/lang/Object;
.source "QuirksDetection.java"


# static fields
.field private static final a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 27
    new-instance v0, Lbna;

    invoke-direct {v0}, Lbna;-><init>()V

    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "serif"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "times"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "times new roman"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "palatino"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "georgia"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "baskerville"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "goudy"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "fantasy"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "cursive"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "itc stone serif"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "monospace"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "courier"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "courier new"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "monaco"

    aput-object v3, v1, v2

    .line 28
    invoke-virtual {v0, v1}, Lbna;->a([Ljava/lang/Object;)Lbna;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lbna;->a()LbmY;

    move-result-object v0

    sput-object v0, Lamu;->a:LbmY;

    .line 27
    return-void
.end method

.method public constructor <init>(LQr;)V
    .locals 3

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const-string v1, "brokenItalicDetectionCutoff"

    const v2, 0x7fffffff

    .line 46
    invoke-interface {p1, v1, v2}, LQr;->a(Ljava/lang/String;I)I

    move-result v1

    if-ge v0, v1, :cond_0

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    .line 48
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 49
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "nexus"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lamu;->a:Z

    .line 50
    return-void

    .line 49
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
