.class public LalO;
.super Ljava/lang/Object;
.source "LauncherStartTime.java"


# instance fields
.field private final a:LaKM;

.field private final a:LpW;


# direct methods
.method public constructor <init>(LaKM;LpW;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKM;

    iput-object v0, p0, LalO;->a:LaKM;

    .line 36
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LpW;

    iput-object v0, p0, LalO;->a:LpW;

    .line 37
    return-void
.end method


# virtual methods
.method a(LaFO;)J
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 46
    :try_start_0
    iget-object v2, p0, LalO;->a:LpW;

    invoke-interface {v2, p1}, LpW;->a(LaFO;)LpU;

    move-result-object v2

    .line 47
    const-string v3, "startTimeLogKey"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, LpU;->a(Ljava/lang/String;J)J
    :try_end_0
    .catch LpX; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 50
    :goto_0
    return-wide v0

    .line 48
    :catch_0
    move-exception v2

    .line 49
    const-string v3, "LauncherStartTime"

    invoke-virtual {v2}, LpX;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public a(LaFO;Landroid/net/Uri$Builder;)Landroid/net/Uri$Builder;
    .locals 6

    .prologue
    .line 72
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    invoke-virtual {p0, p1}, LalO;->a(LaFO;)J

    move-result-wide v0

    .line 74
    iget-object v2, p0, LalO;->a:LaKM;

    invoke-interface {v2}, LaKM;->a()J

    move-result-wide v2

    sub-long/2addr v2, v0

    const-wide/32 v4, 0xa4cb800

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    .line 75
    const-string v2, "starttime"

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 78
    :cond_0
    return-object p2
.end method

.method public a(LaFO;)V
    .locals 4

    .prologue
    .line 59
    :try_start_0
    iget-object v0, p0, LalO;->a:LpW;

    invoke-interface {v0, p1}, LpW;->a(LaFO;)LpU;

    move-result-object v0

    .line 60
    const-string v1, "startTimeLogKey"

    iget-object v2, p0, LalO;->a:LaKM;

    invoke-interface {v2}, LaKM;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LpU;->a(Ljava/lang/String;J)V

    .line 61
    iget-object v1, p0, LalO;->a:LpW;

    invoke-interface {v1, v0}, LpW;->a(LpU;)V
    :try_end_0
    .catch LpX; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    :goto_0
    return-void

    .line 62
    :catch_0
    move-exception v0

    .line 63
    const-string v1, "LauncherStartTime"

    invoke-virtual {v0}, LpX;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
