.class public LRh;
.super Ljava/lang/Object;
.source "FooterViewActionsHandler.java"


# instance fields
.field private final a:LJK;

.field private final a:Landroid/content/Context;

.field private final a:Landroid/os/Handler;

.field private final a:LsC;

.field private final a:Lye;


# direct methods
.method constructor <init>(Landroid/content/Context;LsC;Lye;LJK;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LRh;->a:Landroid/os/Handler;

    .line 49
    iput-object p1, p0, LRh;->a:Landroid/content/Context;

    .line 50
    iput-object p2, p0, LRh;->a:LsC;

    .line 51
    iput-object p4, p0, LRh;->a:LJK;

    .line 52
    iput-object p3, p0, LRh;->a:Lye;

    .line 53
    return-void
.end method

.method static synthetic a(LRh;)LJK;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, LRh;->a:LJK;

    return-object v0
.end method

.method static synthetic a(LRh;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, LRh;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(LRh;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, LRh;->a:Landroid/os/Handler;

    return-object v0
.end method

.method private a(Landroid/view/LayoutInflater;LRR;)Landroid/view/View;
    .locals 4

    .prologue
    .line 123
    sget v0, Lxe;->sidebar_action_list_item:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 124
    sget v0, Lxc;->group_icon:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 125
    invoke-virtual {p2}, LRR;->b()I

    move-result v2

    .line 126
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 128
    sget v0, Lxc;->name:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 129
    iget-object v2, p0, LRh;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p2}, LRR;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 130
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 133
    return-object v1
.end method

.method static synthetic a(LRh;)Lye;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, LRh;->a:Lye;

    return-object v0
.end method

.method static synthetic a(LRh;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, LRh;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 41
    instance-of v0, p1, Lcom/google/android/apps/docs/app/DocListActivity;

    if-eqz v0, :cond_0

    .line 42
    check-cast p1, Lcom/google/android/apps/docs/app/DocListActivity;

    invoke-virtual {p1}, Lcom/google/android/apps/docs/app/DocListActivity;->s()V

    .line 44
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, LRh;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 95
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 96
    iget-object v0, p0, LRh;->a:LsC;

    invoke-interface {v0}, LsC;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LRR;

    .line 97
    invoke-direct {p0, v1, v0}, LRh;->a(Landroid/view/LayoutInflater;LRR;)Landroid/view/View;

    move-result-object v4

    .line 98
    new-instance v5, LRj;

    invoke-direct {v5, p0, v0}, LRj;-><init>(LRh;LRR;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Landroid/view/View;->setClickable(Z)V

    .line 116
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 119
    :cond_0
    return-object v2
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 59
    sget v0, Lxc;->navigate_to_drive:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 61
    if-nez v0, :cond_0

    .line 88
    :goto_0
    return-void

    .line 65
    :cond_0
    new-instance v1, LRi;

    invoke-direct {v1, p0}, LRi;-><init>(LRh;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    goto :goto_0
.end method
