.class public LEk;
.super Ljava/lang/Object;
.source "CreateDocumentActivity.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic a:Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, LEk;->a:Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;

    iput-object p2, p0, LEk;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 100
    iget-object v0, p0, LEk;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 102
    iget-object v0, p0, LEk;->a:Landroid/view/View;

    sget v1, Lxc;->choice_create_panel:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 103
    iget-object v1, p0, LEk;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 105
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    .line 106
    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v3

    .line 108
    new-array v4, v6, [F

    fill-array-data v4, :array_0

    invoke-static {v1, v4}, Lxm;->a(Ljava/lang/Object;[F)Landroid/animation/Animator;

    move-result-object v1

    invoke-static {v1}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v1

    new-array v4, v6, [F

    add-float/2addr v2, v3

    aput v2, v4, v5

    const/4 v2, 0x1

    aput v3, v4, v2

    .line 109
    invoke-static {v0, v4}, Lxm;->b(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v1, v0}, Lxr;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    iget-object v1, p0, LEk;->a:Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;

    .line 110
    invoke-virtual {v1}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxr;->a(Landroid/content/res/Resources;)Lxr;

    move-result-object v0

    iget-object v1, p0, LEk;->a:Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;

    .line 111
    invoke-virtual {v0, v1}, Lxr;->b(Landroid/content/Context;)Lxr;

    move-result-object v0

    .line 112
    invoke-virtual {v0}, Lxr;->a()Landroid/animation/Animator;

    .line 113
    return v5

    .line 108
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method
