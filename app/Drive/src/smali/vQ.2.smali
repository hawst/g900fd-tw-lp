.class public final LvQ;
.super Ljava/lang/Object;
.source "CriterionSetImpl.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;
    .locals 2

    .prologue
    .line 200
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 201
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 202
    new-instance v1, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;

    invoke-direct {v1, v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;-><init>(Ljava/util/Collection;)V

    return-object v1
.end method

.method public a(I)[Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;
    .locals 1

    .prologue
    .line 207
    new-array v0, p1, [Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 197
    invoke-virtual {p0, p1}, LvQ;->a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 197
    invoke-virtual {p0, p1}, LvQ;->a(I)[Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;

    move-result-object v0

    return-object v0
.end method
