.class public LJj;
.super Ljava/lang/Object;
.source "CommonFlowHelpCardSupplier.java"


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field private final a:LJf;

.field private final a:LbiP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiP",
            "<",
            "LJs;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LbsW;


# direct methods
.method public constructor <init>(LJf;LbiP;)V
    .locals 1
    .param p2    # LbiP;
        .annotation runtime Lbxv;
            a = "appFlowHelpCardSupplier"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LJf;",
            "LbiP",
            "<",
            "LJs;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, LJj;->a:LJf;

    .line 33
    invoke-static {}, Lalg;->a()LbsW;

    move-result-object v0

    iput-object v0, p0, LJj;->a:LbsW;

    .line 34
    iput-object p2, p0, LJj;->a:LbiP;

    .line 35
    return-void
.end method

.method static synthetic a(LJj;)LJf;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, LJj;->a:LJf;

    return-object v0
.end method

.method static synthetic a(LJj;)LbiP;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, LJj;->a:LbiP;

    return-object v0
.end method


# virtual methods
.method a(LCl;)LbsU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LCl;",
            ")",
            "LbsU",
            "<",
            "LJw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, LJj;->a:LbsW;

    new-instance v1, LJk;

    invoke-direct {v1, p0, p1}, LJk;-><init>(LJj;LCl;)V

    invoke-interface {v0, v1}, LbsW;->a(Ljava/util/concurrent/Callable;)LbsU;

    move-result-object v0

    return-object v0
.end method

.method a(LJt;)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, LJj;->a:LbiP;

    invoke-virtual {v0}, LbiP;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, LJj;->a:LbiP;

    invoke-virtual {v0}, LbiP;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJs;

    invoke-interface {v0, p1}, LJs;->a(LJt;)V

    .line 60
    :cond_0
    return-void
.end method
