.class public LagB;
.super Ljava/lang/Object;
.source "FeedProcessorDriverImpl.java"

# interfaces
.implements Lagx;


# static fields
.field private static final a:LagY;

.field static final synthetic a:Z


# instance fields
.field private final a:LagD;

.field private final a:Lagz;

.field private final a:Ljava/lang/String;

.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lagy;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "LaKa;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/regex/Pattern;

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LagX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, LagB;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LagB;->a:Z

    .line 69
    new-instance v0, LagC;

    invoke-direct {v0}, LagC;-><init>()V

    sput-object v0, LagB;->a:LagY;

    return-void

    .line 44
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lagz;LagD;LQr;)V
    .locals 2

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    iput-object v0, p0, LagB;->a:Ljava/util/concurrent/BlockingQueue;

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LagB;->a:Ljava/util/ArrayList;

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LagB;->b:Ljava/util/ArrayList;

    .line 90
    iput-object p1, p0, LagB;->a:Lagz;

    .line 91
    iput-object p2, p0, LagB;->a:LagD;

    .line 92
    const-string v0, "feedUriRewritePattern"

    const-string v1, "^https?://(docs.google.com/feeds/).*$"

    invoke-interface {p3, v0, v1}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 94
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LagB;->a:Ljava/util/regex/Pattern;

    .line 95
    const-string v0, "feedUriRewriteReplacement"

    const-string v1, "clients3.google.com/feeds/"

    invoke-interface {p3, v0, v1}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LagB;->a:Ljava/lang/String;

    .line 98
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 245
    iget-object v0, p0, LagB;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LagX;

    .line 246
    invoke-interface {v0}, LagX;->interrupt()V

    goto :goto_0

    .line 250
    :cond_0
    iget-object v0, p0, LagB;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LagX;

    .line 252
    :try_start_0
    invoke-interface {v0}, LagX;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 253
    :catch_0
    move-exception v2

    .line 254
    invoke-interface {v0}, LagX;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 255
    const-string v0, "FeedProcessorDriver"

    const-string v2, "Producer not cleaned up correctly"

    invoke-static {v0, v2}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 269
    :cond_2
    :try_start_1
    invoke-interface {v0}, LaKa;->a()V

    .line 262
    :cond_3
    iget-object v0, p0, LagB;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 264
    iget-object v0, p0, LagB;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKa;

    .line 266
    invoke-interface {v0}, LaKa;->a()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    if-nez v1, :cond_2

    .line 276
    :cond_4
    :goto_2
    return-void

    .line 271
    :catch_1
    move-exception v0

    .line 272
    iget-object v0, p0, LagB;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 273
    const-string v0, "FeedProcessorDriver"

    const-string v1, "Producer not cleaned up correctly"

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method


# virtual methods
.method a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 113
    iget-object v0, p0, LagB;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 114
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 116
    const/4 v2, 0x0

    invoke-virtual {v0, v4}, Ljava/util/regex/Matcher;->start(I)I

    move-result v3

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    iget-object v2, p0, LagB;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    invoke-virtual {v0, v4}, Ljava/util/regex/Matcher;->end(I)I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 121
    :cond_0
    return-object p1
.end method

.method public a(Landroid/content/SyncResult;)V
    .locals 11

    .prologue
    .line 145
    const/4 v1, 0x0

    .line 147
    const/4 v2, 0x0

    .line 148
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 150
    :try_start_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 151
    iget-object v0, p0, LagB;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lagy;

    .line 152
    invoke-interface {v0}, Lagy;->a()V

    .line 153
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LbwY; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lbxa; {:try_start_0 .. :try_end_0} :catch_3
    .catch LbwW; {:try_start_0 .. :try_end_0} :catch_4
    .catch LbwR; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Lbxk; {:try_start_0 .. :try_end_0} :catch_7
    .catch LbwO; {:try_start_0 .. :try_end_0} :catch_8
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_9
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_a
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_b
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_c
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    goto :goto_0

    .line 197
    :catch_0
    move-exception v0

    .line 198
    :goto_1
    const/4 v2, 0x1

    :try_start_1
    iput-boolean v2, p1, Landroid/content/SyncResult;->databaseError:Z

    .line 201
    new-instance v2, LQo;

    const-string v3, "Database corrupted"

    invoke-direct {v2, v3, v0}, LQo;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 236
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_2
    if-eqz v2, :cond_0

    .line 237
    invoke-interface {v2}, LaKa;->a()V

    .line 239
    :cond_0
    invoke-direct {p0}, LagB;->a()V

    throw v0

    .line 185
    :cond_1
    :try_start_2
    invoke-interface {v0}, LaKa;->a()V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_23
    .catch LbwY; {:try_start_2 .. :try_end_2} :catch_21
    .catch Lbxa; {:try_start_2 .. :try_end_2} :catch_1f
    .catch LbwW; {:try_start_2 .. :try_end_2} :catch_1d
    .catch LbwR; {:try_start_2 .. :try_end_2} :catch_1b
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_19
    .catch Lbxk; {:try_start_2 .. :try_end_2} :catch_17
    .catch LbwO; {:try_start_2 .. :try_end_2} :catch_15
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_13
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_11
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_f
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_d
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    .line 186
    const/4 v0, 0x0

    move v2, v1

    move-object v1, v0

    .line 156
    :cond_2
    :goto_3
    :try_start_3
    invoke-static {}, LpD;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 157
    new-instance v0, Ljava/lang/InterruptedException;

    const-string v2, "Interrupted during processing"

    invoke-direct {v0, v2}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_0
    .catch LbwY; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lbxa; {:try_start_3 .. :try_end_3} :catch_3
    .catch LbwW; {:try_start_3 .. :try_end_3} :catch_4
    .catch LbwR; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Lbxk; {:try_start_3 .. :try_end_3} :catch_7
    .catch LbwO; {:try_start_3 .. :try_end_3} :catch_8
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3 .. :try_end_3} :catch_9
    .catch Ljava/text/ParseException; {:try_start_3 .. :try_end_3} :catch_a
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_b
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_c
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 202
    :catch_1
    move-exception v0

    move-object v2, v1

    .line 203
    :goto_4
    :try_start_4
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v1, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v1, Landroid/content/SyncStats;->numParseExceptions:J

    .line 204
    new-instance v1, LQo;

    const-string v3, "Error getting feed data"

    invoke-direct {v1, v3, v0}, LQo;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 236
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 160
    :cond_3
    :try_start_5
    iget-object v0, p0, LagB;->a:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKa;
    :try_end_5
    .catch Landroid/database/SQLException; {:try_start_5 .. :try_end_5} :catch_0
    .catch LbwY; {:try_start_5 .. :try_end_5} :catch_1
    .catch Lbxa; {:try_start_5 .. :try_end_5} :catch_3
    .catch LbwW; {:try_start_5 .. :try_end_5} :catch_4
    .catch LbwR; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6
    .catch Lbxk; {:try_start_5 .. :try_end_5} :catch_7
    .catch LbwO; {:try_start_5 .. :try_end_5} :catch_8
    .catch Landroid/accounts/AuthenticatorException; {:try_start_5 .. :try_end_5} :catch_9
    .catch Ljava/text/ParseException; {:try_start_5 .. :try_end_5} :catch_a
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_b
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_c
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 162
    :try_start_6
    const-string v1, "FeedProcessorDriver"

    const-string v3, "Item %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v0, v7, v8

    invoke-static {v1, v3, v7}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 164
    invoke-interface {v0}, LaKa;->a()Z

    move-result v1

    if-nez v1, :cond_4

    .line 166
    add-int/lit8 v1, v2, 0x1

    .line 167
    invoke-interface {v0}, LaKa;->a()I

    move-result v2

    invoke-interface {v0}, LaKa;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 168
    const-string v2, "FeedProcessorDriver"

    const-string v3, "%s producers done"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v2, v3, v7}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 169
    iget-object v2, p0, LagB;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    move-result v2

    if-ne v1, v2, :cond_1

    .line 185
    :try_start_7
    invoke-interface {v0}, LaKa;->a()V
    :try_end_7
    .catch Landroid/database/SQLException; {:try_start_7 .. :try_end_7} :catch_23
    .catch LbwY; {:try_start_7 .. :try_end_7} :catch_21
    .catch Lbxa; {:try_start_7 .. :try_end_7} :catch_1f
    .catch LbwW; {:try_start_7 .. :try_end_7} :catch_1d
    .catch LbwR; {:try_start_7 .. :try_end_7} :catch_1b
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_19
    .catch Lbxk; {:try_start_7 .. :try_end_7} :catch_17
    .catch LbwO; {:try_start_7 .. :try_end_7} :catch_15
    .catch Landroid/accounts/AuthenticatorException; {:try_start_7 .. :try_end_7} :catch_13
    .catch Ljava/text/ParseException; {:try_start_7 .. :try_end_7} :catch_11
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_f
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_d
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 186
    const/4 v2, 0x0

    .line 190
    const/4 v0, 0x0

    move v3, v0

    :goto_5
    :try_start_8
    iget-object v0, p0, LagB;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_8

    .line 191
    iget-object v0, p0, LagB;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lagy;

    .line 192
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0, v1}, Lagy;->a(Ljava/lang/String;)V
    :try_end_8
    .catch Landroid/database/SQLException; {:try_start_8 .. :try_end_8} :catch_2
    .catch LbwY; {:try_start_8 .. :try_end_8} :catch_22
    .catch Lbxa; {:try_start_8 .. :try_end_8} :catch_20
    .catch LbwW; {:try_start_8 .. :try_end_8} :catch_1e
    .catch LbwR; {:try_start_8 .. :try_end_8} :catch_1c
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1a
    .catch Lbxk; {:try_start_8 .. :try_end_8} :catch_18
    .catch LbwO; {:try_start_8 .. :try_end_8} :catch_16
    .catch Landroid/accounts/AuthenticatorException; {:try_start_8 .. :try_end_8} :catch_14
    .catch Ljava/text/ParseException; {:try_start_8 .. :try_end_8} :catch_12
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_10
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_e
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 190
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    .line 175
    :cond_4
    :try_start_9
    invoke-static {}, LpD;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 176
    new-instance v1, Ljava/lang/InterruptedException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Interrupted while processing #"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 177
    invoke-interface {v0}, LaKa;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 185
    :catchall_2
    move-exception v1

    :try_start_a
    invoke-interface {v0}, LaKa;->a()V
    :try_end_a
    .catch Landroid/database/SQLException; {:try_start_a .. :try_end_a} :catch_23
    .catch LbwY; {:try_start_a .. :try_end_a} :catch_21
    .catch Lbxa; {:try_start_a .. :try_end_a} :catch_1f
    .catch LbwW; {:try_start_a .. :try_end_a} :catch_1d
    .catch LbwR; {:try_start_a .. :try_end_a} :catch_1b
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_19
    .catch Lbxk; {:try_start_a .. :try_end_a} :catch_17
    .catch LbwO; {:try_start_a .. :try_end_a} :catch_15
    .catch Landroid/accounts/AuthenticatorException; {:try_start_a .. :try_end_a} :catch_13
    .catch Ljava/text/ParseException; {:try_start_a .. :try_end_a} :catch_11
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_f
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_d
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    .line 186
    const/4 v2, 0x0

    :try_start_b
    throw v1
    :try_end_b
    .catch Landroid/database/SQLException; {:try_start_b .. :try_end_b} :catch_2
    .catch LbwY; {:try_start_b .. :try_end_b} :catch_22
    .catch Lbxa; {:try_start_b .. :try_end_b} :catch_20
    .catch LbwW; {:try_start_b .. :try_end_b} :catch_1e
    .catch LbwR; {:try_start_b .. :try_end_b} :catch_1c
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1a
    .catch Lbxk; {:try_start_b .. :try_end_b} :catch_18
    .catch LbwO; {:try_start_b .. :try_end_b} :catch_16
    .catch Landroid/accounts/AuthenticatorException; {:try_start_b .. :try_end_b} :catch_14
    .catch Ljava/text/ParseException; {:try_start_b .. :try_end_b} :catch_12
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_10
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_e
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 197
    :catch_2
    move-exception v0

    move-object v1, v2

    goto/16 :goto_1

    .line 180
    :cond_5
    :try_start_c
    sget-boolean v1, LagB;->a:Z

    if-nez v1, :cond_7

    invoke-interface {v0}, LaKa;->a()I

    move-result v1

    if-ltz v1, :cond_6

    .line 181
    invoke-interface {v0}, LaKa;->a()I

    move-result v1

    iget-object v3, p0, LagB;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_7

    .line 180
    :cond_6
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 182
    :cond_7
    iget-object v1, p0, LagB;->a:Ljava/util/ArrayList;

    invoke-interface {v0}, LaKa;->a()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lagy;

    .line 183
    iget-object v3, p0, LagB;->a:LagD;

    invoke-interface {v3, p1, v0, v1}, LagD;->a(Landroid/content/SyncResult;LaKa;Lagy;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 185
    :try_start_d
    invoke-interface {v0}, LaKa;->a()V
    :try_end_d
    .catch Landroid/database/SQLException; {:try_start_d .. :try_end_d} :catch_23
    .catch LbwY; {:try_start_d .. :try_end_d} :catch_21
    .catch Lbxa; {:try_start_d .. :try_end_d} :catch_1f
    .catch LbwW; {:try_start_d .. :try_end_d} :catch_1d
    .catch LbwR; {:try_start_d .. :try_end_d} :catch_1b
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_19
    .catch Lbxk; {:try_start_d .. :try_end_d} :catch_17
    .catch LbwO; {:try_start_d .. :try_end_d} :catch_15
    .catch Landroid/accounts/AuthenticatorException; {:try_start_d .. :try_end_d} :catch_13
    .catch Ljava/text/ParseException; {:try_start_d .. :try_end_d} :catch_11
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_f
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    .line 186
    const/4 v1, 0x0

    .line 187
    goto/16 :goto_3

    .line 195
    :cond_8
    :try_start_e
    const-string v0, "FeedProcessorDriver"

    const-string v1, "SyncResult so far in feed %s time taken: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    aput-object v7, v3, v6

    const/4 v6, 0x1

    .line 196
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v4, v8, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    .line 195
    invoke-static {v0, v1, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_e
    .catch Landroid/database/SQLException; {:try_start_e .. :try_end_e} :catch_2
    .catch LbwY; {:try_start_e .. :try_end_e} :catch_22
    .catch Lbxa; {:try_start_e .. :try_end_e} :catch_20
    .catch LbwW; {:try_start_e .. :try_end_e} :catch_1e
    .catch LbwR; {:try_start_e .. :try_end_e} :catch_1c
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_1a
    .catch Lbxk; {:try_start_e .. :try_end_e} :catch_18
    .catch LbwO; {:try_start_e .. :try_end_e} :catch_16
    .catch Landroid/accounts/AuthenticatorException; {:try_start_e .. :try_end_e} :catch_14
    .catch Ljava/text/ParseException; {:try_start_e .. :try_end_e} :catch_12
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_e} :catch_10
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 236
    if-eqz v2, :cond_9

    .line 237
    invoke-interface {v2}, LaKa;->a()V

    .line 239
    :cond_9
    invoke-direct {p0}, LagB;->a()V

    .line 241
    return-void

    .line 205
    :catch_3
    move-exception v0

    move-object v2, v1

    .line 206
    :goto_6
    :try_start_f
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v1, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v1, Landroid/content/SyncStats;->numParseExceptions:J

    .line 207
    new-instance v1, LQo;

    const-string v3, "Error getting feed data"

    invoke-direct {v1, v3, v0}, LQo;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 208
    :catch_4
    move-exception v0

    move-object v2, v1

    .line 209
    :goto_7
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v1, Landroid/content/SyncStats;->numIoExceptions:J

    .line 210
    throw v0

    .line 211
    :catch_5
    move-exception v0

    move-object v2, v1

    .line 212
    :goto_8
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 213
    new-instance v1, LQo;

    const-string v3, "Error getting feed data"

    invoke-direct {v1, v3, v0}, LQo;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 214
    :catch_6
    move-exception v0

    move-object v2, v1

    .line 215
    :goto_9
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v1, Landroid/content/SyncStats;->numIoExceptions:J

    .line 216
    throw v0

    .line 217
    :catch_7
    move-exception v0

    move-object v2, v1

    .line 218
    :goto_a
    iget-object v0, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v0, Landroid/content/SyncStats;->numIoExceptions:J

    .line 219
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 220
    :catch_8
    move-exception v0

    move-object v2, v1

    .line 221
    :goto_b
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 222
    throw v0

    .line 223
    :catch_9
    move-exception v0

    move-object v2, v1

    .line 224
    :goto_c
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 225
    throw v0

    .line 226
    :catch_a
    move-exception v0

    move-object v2, v1

    .line 227
    :goto_d
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v1, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v1, Landroid/content/SyncStats;->numParseExceptions:J

    .line 228
    new-instance v1, LQo;

    const-string v3, "Feed data contains error"

    invoke-direct {v1, v3, v0}, LQo;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 229
    :catch_b
    move-exception v0

    move-object v2, v1

    .line 230
    :goto_e
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v1, Landroid/content/SyncStats;->numIoExceptions:J

    .line 231
    throw v0

    .line 232
    :catch_c
    move-exception v0

    move-object v2, v1

    .line 233
    :goto_f
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v1, Landroid/content/SyncStats;->numIoExceptions:J

    .line 234
    new-instance v1, LQo;

    const-string v3, "Runtime Exception"

    invoke-direct {v1, v3, v0}, LQo;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 236
    :catchall_3
    move-exception v0

    move-object v2, v1

    goto/16 :goto_2

    :catchall_4
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto/16 :goto_2

    .line 232
    :catch_d
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_f

    :catch_e
    move-exception v0

    goto :goto_f

    .line 229
    :catch_f
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_e

    :catch_10
    move-exception v0

    goto :goto_e

    .line 226
    :catch_11
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_d

    :catch_12
    move-exception v0

    goto :goto_d

    .line 223
    :catch_13
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_c

    :catch_14
    move-exception v0

    goto :goto_c

    .line 220
    :catch_15
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_b

    :catch_16
    move-exception v0

    goto :goto_b

    .line 217
    :catch_17
    move-exception v1

    move-object v2, v0

    goto/16 :goto_a

    :catch_18
    move-exception v0

    goto/16 :goto_a

    .line 214
    :catch_19
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto/16 :goto_9

    :catch_1a
    move-exception v0

    goto/16 :goto_9

    .line 211
    :catch_1b
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto/16 :goto_8

    :catch_1c
    move-exception v0

    goto/16 :goto_8

    .line 208
    :catch_1d
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto/16 :goto_7

    :catch_1e
    move-exception v0

    goto/16 :goto_7

    .line 205
    :catch_1f
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto/16 :goto_6

    :catch_20
    move-exception v0

    goto/16 :goto_6

    .line 202
    :catch_21
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto/16 :goto_4

    :catch_22
    move-exception v0

    goto/16 :goto_4

    .line 197
    :catch_23
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    goto/16 :goto_1
.end method

.method public a(Ljava/lang/String;LaFO;Lagy;I)V
    .locals 6

    .prologue
    .line 102
    sget-object v5, LagB;->a:LagY;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, LagB;->a(Ljava/lang/String;LaFO;Lagy;ILagY;)V

    .line 103
    return-void
.end method

.method public a(Ljava/lang/String;LaFO;Lagy;ILagY;)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 127
    invoke-virtual {p0, p1}, LagB;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 128
    const-string v3, "FeedProcessorDriver"

    const-string v4, "Feed to process: %s"

    new-array v5, v0, [Ljava/lang/Object;

    aput-object v2, v5, v1

    invoke-static {v3, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 129
    iget-object v3, p0, LagB;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    iget-object v4, p0, LagB;->b:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ne v3, v4, :cond_0

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 130
    iget-object v0, p0, LagB;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 131
    iget-object v0, p0, LagB;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 132
    iget-object v0, p0, LagB;->a:Lagz;

    iget-object v4, p0, LagB;->a:Ljava/util/concurrent/BlockingQueue;

    move-object v1, p5

    move-object v3, p2

    move v5, p4

    .line 133
    invoke-interface/range {v0 .. v6}, Lagz;->a(LagY;Ljava/lang/String;LaFO;Ljava/util/concurrent/BlockingQueue;II)LagX;

    move-result-object v0

    .line 136
    invoke-interface {v0}, LagX;->a()V

    .line 138
    iget-object v1, p0, LagB;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    return-void

    :cond_0
    move v0, v1

    .line 129
    goto :goto_0
.end method
