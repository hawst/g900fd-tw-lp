.class public Lagm;
.super Ljava/lang/Object;
.source "DocEntrySynchronizerImpl.java"

# interfaces
.implements Lagl;


# instance fields
.field private final a:LZS;

.field private final a:LaGF;

.field private final a:LaGg;

.field private final a:Lagw;


# direct methods
.method public constructor <init>(LaGg;LaGF;Lagw;LZS;)V
    .locals 0
    .param p4    # LZS;
        .annotation runtime Lbxv;
            a = "wapiFeedProcessor"
        .end annotation
    .end param

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lagm;->a:LaGg;

    .line 46
    iput-object p2, p0, Lagm;->a:LaGF;

    .line 47
    iput-object p3, p0, Lagm;->a:Lagw;

    .line 48
    iput-object p4, p0, Lagm;->a:LZS;

    .line 49
    return-void
.end method

.method private a(LaFM;LaJT;Ljava/lang/Boolean;)LaGd;
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 117
    iget-object v2, p0, Lagm;->a:LaGg;

    invoke-interface {v2}, LaGg;->a()V

    .line 119
    :try_start_0
    invoke-interface {p2}, LaJT;->b()Ljava/lang/String;

    move-result-object v2

    .line 120
    iget-object v3, p0, Lagm;->a:LaGg;

    .line 121
    invoke-interface {p2}, LaJT;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, p1, v4, v2}, LaGg;->a(LaFM;Ljava/lang/String;Ljava/lang/String;)LaGc;

    move-result-object v3

    .line 122
    invoke-interface {p2}, LaJT;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, LaGc;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v0

    .line 123
    :cond_0
    invoke-virtual {v3}, LaGc;->k()Z

    move-result v4

    .line 125
    invoke-virtual {v3}, LaGc;->b()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 126
    const-string v5, "DocEntrySynchronizerImpl"

    const-string v6, "Feed contains a document (%s) which is local only. Probably the server created the document but we did not get the ack for it."

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v2, v7, v8

    invoke-static {v5, v6, v7}, LalV;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 131
    invoke-virtual {v3, v2}, LaGc;->h(Ljava/lang/String;)LaGe;

    .line 136
    :cond_1
    invoke-virtual {p1}, LaFM;->a()LaFO;

    move-result-object v2

    invoke-virtual {v2}, LaFO;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2}, LaJT;->q()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 137
    const-wide/16 v6, 0x0

    invoke-interface {p2, v6, v7}, LaJT;->b(J)V

    .line 140
    :cond_2
    iget-object v2, p0, Lagm;->a:LaGF;

    invoke-interface {v2, p2, v3}, LaGF;->a(LaJT;LaGc;)V

    .line 142
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, LaGc;->c(Z)LaGe;

    .line 144
    if-eqz p3, :cond_5

    .line 145
    invoke-virtual {v3}, LaGc;->i()Z

    move-result v2

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eq v2, v5, :cond_5

    .line 147
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v3, v1}, LaGc;->b(Z)V

    .line 149
    :goto_0
    invoke-virtual {v3}, LaGc;->b()LaGb;

    move-result-object v1

    .line 151
    if-nez v0, :cond_3

    if-eqz v4, :cond_4

    .line 153
    :cond_3
    iget-object v0, p0, Lagm;->a:Lagw;

    invoke-interface {v0, v1}, Lagw;->a(LaGu;)V

    .line 156
    :cond_4
    iget-object v0, p0, Lagm;->a:LaGg;

    invoke-interface {v0}, LaGg;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    iget-object v0, p0, Lagm;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V

    return-object v1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lagm;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    throw v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method private b(LaFM;LaJT;Ljava/lang/Boolean;)LaGd;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 167
    iget-object v2, p0, Lagm;->a:LaGg;

    invoke-interface {v2}, LaGg;->a()V

    .line 169
    :try_start_0
    iget-object v2, p0, Lagm;->a:LaGg;

    .line 170
    invoke-interface {p2}, LaJT;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, p1, v3}, LaGg;->a(LaFM;Ljava/lang/String;)LaGa;

    move-result-object v2

    .line 172
    invoke-interface {p2}, LaJT;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, LaGa;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v0

    .line 173
    :cond_0
    invoke-virtual {v2}, LaGa;->k()Z

    move-result v3

    .line 174
    if-nez v1, :cond_1

    if-eqz v3, :cond_2

    .line 175
    :cond_1
    iget-object v4, p0, Lagm;->a:LaGF;

    invoke-interface {v4, p2, v2}, LaGF;->a(LaJT;LaGa;)V

    .line 176
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, LaGa;->c(Z)LaGe;

    .line 179
    :cond_2
    if-eqz p3, :cond_5

    invoke-virtual {v2}, LaGa;->i()Z

    move-result v4

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eq v4, v5, :cond_5

    .line 181
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v2, v1}, LaGa;->b(Z)V

    .line 184
    :goto_0
    invoke-virtual {v2}, LaGa;->b()LaFZ;

    move-result-object v1

    .line 185
    if-nez v0, :cond_3

    if-eqz v3, :cond_4

    .line 187
    :cond_3
    iget-object v0, p0, Lagm;->a:Lagw;

    invoke-interface {v0, v1}, Lagw;->a(LaGu;)V

    .line 189
    :cond_4
    iget-object v0, p0, Lagm;->a:LaGg;

    invoke-interface {v0}, LaGg;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 192
    iget-object v0, p0, Lagm;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V

    return-object v1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lagm;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    throw v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(LaFM;J)I
    .locals 2

    .prologue
    .line 198
    iget-object v0, p0, Lagm;->a:LaGg;

    invoke-interface {v0, p1, p2, p3}, LaGg;->a(LaFM;J)I

    move-result v0

    return v0
.end method

.method public final a(LaFM;LaJT;Ljava/lang/Boolean;)V
    .locals 6

    .prologue
    .line 54
    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lagm;->a(LaFM;LaJT;Ljava/lang/Boolean;J)V

    .line 55
    return-void
.end method

.method public a(LaFM;LaJT;Ljava/lang/Boolean;J)V
    .locals 8

    .prologue
    .line 63
    iget-object v0, p0, Lagm;->a:LaGg;

    iget-object v1, p0, Lagm;->a:LZS;

    invoke-interface {v0, v1}, LaGg;->a(LZS;)V

    .line 64
    iget-object v0, p0, Lagm;->a:LaGg;

    invoke-interface {v0}, LaGg;->a()V

    .line 66
    :try_start_0
    sget-object v0, LaGv;->a:LaGv;

    invoke-virtual {v0}, LaGv;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2}, LaJT;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    invoke-direct {p0, p1, p2, p3}, Lagm;->b(LaFM;LaJT;Ljava/lang/Boolean;)LaGd;

    move-result-object v0

    move-object v2, v0

    .line 73
    :goto_0
    iget-object v0, p0, Lagm;->a:LaGg;

    .line 74
    invoke-virtual {v2}, LaGd;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v1

    .line 73
    invoke-interface {v0, v1}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Ljava/util/Map;

    move-result-object v3

    .line 75
    invoke-interface {p2}, LaJT;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaJU;

    .line 77
    iget-object v1, p0, Lagm;->a:LaGg;

    .line 78
    invoke-virtual {v0}, LaJU;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, p1, v5}, LaGg;->a(LaFM;Ljava/lang/String;)LaGa;

    move-result-object v5

    .line 79
    invoke-virtual {v5}, LaGa;->l()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 80
    invoke-virtual {v0}, LaJU;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, LaGa;->a(Ljava/lang/String;)LaGe;

    .line 81
    invoke-virtual {v5}, LaGa;->b()LaFZ;

    move-result-object v0

    move-object v1, v0

    .line 88
    :goto_2
    invoke-virtual {v5}, LaGa;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGB;

    .line 89
    if-nez v0, :cond_0

    .line 90
    iget-object v0, p0, Lagm;->a:LaGg;

    invoke-virtual {v2}, LaGd;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v5

    invoke-interface {v0, v5, v1}, LaGg;->a(Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;LaFZ;)LaGB;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, LaGB;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 101
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lagm;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    throw v0

    .line 69
    :cond_1
    :try_start_1
    invoke-direct {p0, p1, p2, p3}, Lagm;->a(LaFM;LaJT;Ljava/lang/Boolean;)LaGd;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    .line 82
    :cond_2
    invoke-virtual {v5}, LaGa;->e()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, p4

    if-gez v0, :cond_3

    .line 83
    invoke-virtual {v5}, LaGa;->b()LaFZ;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    .line 85
    :cond_3
    invoke-static {v5}, LaFZ;->a(LaGa;)LaFZ;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    .line 96
    :cond_4
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGB;

    .line 97
    invoke-virtual {v0}, LaGB;->f()V

    goto :goto_3

    .line 99
    :cond_5
    iget-object v0, p0, Lagm;->a:LaGg;

    invoke-interface {v0}, LaGg;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 101
    iget-object v0, p0, Lagm;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V

    .line 103
    return-void
.end method

.method public b(LaFM;LaJT;Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lagm;->a:LaGg;

    invoke-interface {p2}, LaJT;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LaGg;->a(LaFM;Ljava/lang/String;)LaGd;

    move-result-object v0

    .line 108
    if-eqz v0, :cond_0

    .line 109
    invoke-virtual {v0}, LaGd;->a()LaGe;

    move-result-object v0

    invoke-virtual {v0}, LaGe;->f()V

    .line 111
    :cond_0
    return-void
.end method
