.class LDW;
.super Ljava/lang/Object;
.source "SyncViewState.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:LaGM;

.field private final a:LahB;

.field private final a:Lcom/google/android/gms/drive/database/data/EntrySpec;


# direct methods
.method public constructor <init>(LaGM;LahB;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1

    .prologue
    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p0, LDW;->a:LaGM;

    .line 159
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, LDW;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 160
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahB;

    iput-object v0, p0, LDW;->a:LahB;

    .line 161
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, LDW;->a:LahB;

    iget-object v1, p0, LDW;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v1}, LahB;->d(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 166
    iget-object v0, p0, LDW;->a:LaGM;

    iget-object v1, p0, LDW;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v1, v1, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-interface {v0, v1}, LaGM;->a(LaFO;)LaFM;

    move-result-object v0

    .line 167
    iget-object v1, p0, LDW;->a:LaGM;

    invoke-interface {v1, v0}, LaGM;->b(LaFM;)V

    .line 168
    return-void
.end method
