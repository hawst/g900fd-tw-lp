.class public abstract Lbnk;
.super Lbnl;
.source "ImmutableSortedSet.java"

# interfaces
.implements Lbqf;
.implements Ljava/util/SortedSet;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lbnl",
        "<TE;>;",
        "Lbqf",
        "<TE;>;",
        "Ljava/util/SortedSet",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final a:Lbnk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbnk",
            "<",
            "Ljava/lang/Comparable;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/Comparable;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final transient a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<-TE;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 98
    invoke-static {}, Lbpw;->d()Lbpw;

    move-result-object v0

    sput-object v0, Lbnk;->b:Ljava/util/Comparator;

    .line 100
    new-instance v0, Lbmj;

    sget-object v1, Lbnk;->b:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Lbmj;-><init>(Ljava/util/Comparator;)V

    sput-object v0, Lbnk;->a:Lbnk;

    return-void
.end method

.method constructor <init>(Ljava/util/Comparator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TE;>;)V"
        }
    .end annotation

    .prologue
    .line 595
    invoke-direct {p0}, Lbnl;-><init>()V

    .line 596
    iput-object p1, p0, Lbnk;->a:Ljava/util/Comparator;

    .line 597
    return-void
.end method

.method static varargs a(Ljava/util/Comparator;I[Ljava/lang/Object;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TE;>;I[TE;)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 407
    if-nez p1, :cond_0

    .line 423
    :goto_0
    return v1

    :cond_0
    move v2, v1

    .line 410
    :goto_1
    if-ge v2, p1, :cond_1

    .line 411
    aget-object v3, p2, v2

    invoke-static {v3, v2}, Lbpv;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    .line 410
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 413
    :cond_1
    invoke-static {p2, v1, p1, p0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    move v2, v0

    move v1, v0

    .line 415
    :goto_2
    if-ge v2, p1, :cond_2

    .line 416
    aget-object v3, p2, v2

    .line 417
    add-int/lit8 v0, v1, -0x1

    aget-object v0, p2, v0

    .line 418
    invoke-interface {p0, v3, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-eqz v0, :cond_3

    .line 419
    add-int/lit8 v0, v1, 0x1

    aput-object v3, p2, v1

    .line 415
    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_2

    .line 422
    :cond_2
    const/4 v0, 0x0

    invoke-static {p2, v1, p1, v0}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_3
.end method

.method static a(Ljava/util/Comparator;Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<*>;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ")I"
        }
    .end annotation

    .prologue
    .line 589
    .line 590
    invoke-interface {p0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method private static a()Lbnk;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "Lbnk",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 105
    sget-object v0, Lbnk;->a:Lbnk;

    return-object v0
.end method

.method public static a(Ljava/lang/Comparable;)Lbnk;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Ljava/lang/Comparable",
            "<-TE;>;>(TE;)",
            "Lbnk",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 127
    new-instance v0, LbpQ;

    invoke-static {p0}, LbmF;->a(Ljava/lang/Object;)LbmF;

    move-result-object v1

    invoke-static {}, Lbpw;->d()Lbpw;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LbpQ;-><init>(LbmF;Ljava/util/Comparator;)V

    return-object v0
.end method

.method static a(Ljava/util/Comparator;)Lbnk;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TE;>;)",
            "Lbnk",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 109
    sget-object v0, Lbnk;->b:Ljava/util/Comparator;

    invoke-interface {v0, p0}, Ljava/util/Comparator;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    invoke-static {}, Lbnk;->a()Lbnk;

    move-result-object v0

    .line 112
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lbmj;

    invoke-direct {v0, p0}, Lbmj;-><init>(Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method static varargs a(Ljava/util/Comparator;I[Ljava/lang/Object;)Lbnk;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TE;>;I[TE;)",
            "Lbnk",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 439
    invoke-static {p0, p1, p2}, Lbnk;->a(Ljava/util/Comparator;I[Ljava/lang/Object;)I

    move-result v0

    .line 440
    if-nez v0, :cond_0

    .line 441
    invoke-static {p0}, Lbnk;->a(Ljava/util/Comparator;)Lbnk;

    move-result-object v0

    .line 445
    :goto_0
    return-object v0

    .line 442
    :cond_0
    array-length v1, p2

    if-ge v0, v1, :cond_1

    .line 443
    invoke-static {p2, v0}, Lbpv;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p2

    .line 445
    :cond_1
    new-instance v0, LbpQ;

    invoke-static {p2}, LbmF;->b([Ljava/lang/Object;)LbmF;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LbpQ;-><init>(LbmF;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method public static a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lbnk;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TE;>;",
            "Ljava/lang/Iterable",
            "<+TE;>;)",
            "Lbnk",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 330
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 331
    invoke-static {p0, p1}, Lbqg;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v0

    .line 333
    if-eqz v0, :cond_0

    instance-of v0, p1, Lbnk;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 335
    check-cast v0, Lbnk;

    .line 336
    invoke-virtual {v0}, Lbnk;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 343
    :goto_0
    return-object v0

    .line 342
    :cond_0
    invoke-static {p1}, Lbnm;->a(Ljava/lang/Iterable;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 343
    array-length v1, v0

    invoke-static {p0, v1, v0}, Lbnk;->a(Ljava/util/Comparator;I[Ljava/lang/Object;)Lbnk;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/Comparator;Ljava/util/Collection;)Lbnk;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Comparator",
            "<-TE;>;",
            "Ljava/util/Collection",
            "<+TE;>;)",
            "Lbnk",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 366
    invoke-static {p0, p1}, Lbnk;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lbnk;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method abstract a(Ljava/lang/Object;)I
.end method

.method a(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 581
    iget-object v0, p0, Lbnk;->a:Ljava/util/Comparator;

    invoke-static {v0, p1, p2}, Lbnk;->a(Ljava/util/Comparator;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/Object;)Lbnk;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "Lbnk",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 628
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lbnk;->c(Ljava/lang/Object;Z)Lbnk;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Lbnk;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;TE;)",
            "Lbnk",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 654
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Lbnk;->b(Ljava/lang/Object;ZLjava/lang/Object;Z)Lbnk;

    move-result-object v0

    return-object v0
.end method

.method abstract a(Ljava/lang/Object;Z)Lbnk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)",
            "Lbnk",
            "<TE;>;"
        }
    .end annotation
.end method

.method abstract a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lbnk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;ZTE;Z)",
            "Lbnk",
            "<TE;>;"
        }
    .end annotation
.end method

.method public abstract a()Lbqv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbqv",
            "<TE;>;"
        }
    .end annotation
.end method

.method public b(Ljava/lang/Object;)Lbnk;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "Lbnk",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 682
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lbnk;->d(Ljava/lang/Object;Z)Lbnk;

    move-result-object v0

    return-object v0
.end method

.method abstract b(Ljava/lang/Object;Z)Lbnk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)",
            "Lbnk",
            "<TE;>;"
        }
    .end annotation
.end method

.method public b(Ljava/lang/Object;ZLjava/lang/Object;Z)Lbnk;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;ZTE;Z)",
            "Lbnk",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 663
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 664
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 665
    iget-object v0, p0, Lbnk;->a:Ljava/util/Comparator;

    invoke-interface {v0, p1, p3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 666
    invoke-virtual {p0, p1, p2, p3, p4}, Lbnk;->a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lbnk;

    move-result-object v0

    return-object v0

    .line 665
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract b()Lbqv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbqv",
            "<TE;>;"
        }
    .end annotation
.end method

.method public c(Ljava/lang/Object;Z)Lbnk;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)",
            "Lbnk",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 636
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lbnk;->a(Ljava/lang/Object;Z)Lbnk;

    move-result-object v0

    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<-TE;>;"
        }
    .end annotation

    .prologue
    .line 608
    iget-object v0, p0, Lbnk;->a:Ljava/util/Comparator;

    return-object v0
.end method

.method public d(Ljava/lang/Object;Z)Lbnk;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;Z)",
            "Lbnk",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 690
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lbnk;->b(Ljava/lang/Object;Z)Lbnk;

    move-result-object v0

    return-object v0
.end method

.method public first()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 737
    invoke-virtual {p0}, Lbnk;->a()Lbqv;

    move-result-object v0

    invoke-virtual {v0}, Lbqv;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public synthetic headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0, p1}, Lbnk;->a(Ljava/lang/Object;)Lbnk;

    move-result-object v0

    return-object v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Lbnk;->a()Lbqv;

    move-result-object v0

    return-object v0
.end method

.method public last()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 741
    invoke-virtual {p0}, Lbnk;->b()Lbqv;

    move-result-object v0

    invoke-virtual {v0}, Lbqv;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public synthetic subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0, p1, p2}, Lbnk;->a(Ljava/lang/Object;Ljava/lang/Object;)Lbnk;

    move-result-object v0

    return-object v0
.end method

.method public synthetic tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0, p1}, Lbnk;->b(Ljava/lang/Object;)Lbnk;

    move-result-object v0

    return-object v0
.end method
