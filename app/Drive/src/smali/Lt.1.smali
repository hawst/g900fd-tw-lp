.class public LLt;
.super Ljava/lang/Object;
.source "SelectionDropZoneManager.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private a:I

.field private a:LCU;

.field private a:LLf;

.field private final a:Landroid/view/View;

.field private a:Landroid/view/animation/Animation;

.field private a:Z

.field private b:I

.field private final b:Landroid/view/View;

.field private b:Z

.field private final c:Landroid/view/View;

.field private c:Z

.field private final d:Landroid/view/View;


# direct methods
.method public constructor <init>(LCU;Landroid/view/View;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput v0, p0, LLt;->a:I

    .line 38
    iput v0, p0, LLt;->b:I

    .line 40
    iput-boolean v0, p0, LLt;->c:Z

    .line 176
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCU;

    iput-object v0, p0, LLt;->a:LCU;

    .line 177
    sget v0, Lxc;->selection_drop_zone_dismiss_area:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LLt;->b:Landroid/view/View;

    .line 178
    sget v0, Lxc;->selection_drop_zone2_this_folder:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LLt;->c:Landroid/view/View;

    .line 179
    sget v0, Lxc;->selection_drop_zone2_new_folder:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LLt;->d:Landroid/view/View;

    .line 180
    sget v0, Lxc;->selection_drop_zone_group_view:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LLt;->a:Landroid/view/View;

    .line 181
    invoke-direct {p0}, LLt;->b()V

    .line 182
    return-void
.end method

.method private a()F
    .locals 4

    .prologue
    .line 192
    new-instance v0, Landroid/view/animation/Transformation;

    invoke-direct {v0}, Landroid/view/animation/Transformation;-><init>()V

    .line 193
    iget-object v1, p0, LLt;->a:Landroid/view/animation/Animation;

    const-wide/16 v2, -0x1

    invoke-virtual {v1, v2, v3, v0}, Landroid/view/animation/Animation;->getTransformation(JLandroid/view/animation/Transformation;)Z

    .line 194
    invoke-virtual {v0}, Landroid/view/animation/Transformation;->getAlpha()F

    move-result v0

    return v0
.end method

.method static synthetic a(LLt;)I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, LLt;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LLt;->b:I

    return v0
.end method

.method static synthetic a(LLt;Landroid/view/animation/Animation;)Landroid/view/animation/Animation;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, LLt;->a:Landroid/view/animation/Animation;

    return-object p1
.end method

.method private a(F)V
    .locals 10

    .prologue
    const-wide/16 v2, 0xfa

    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 315
    iget-boolean v0, p0, LLt;->a:Z

    if-nez v0, :cond_1

    .line 316
    invoke-direct {p0}, LLt;->c()V

    .line 327
    :cond_0
    :goto_0
    return-void

    .line 319
    :cond_1
    invoke-direct {p0, v4}, LLt;->a(Z)V

    .line 320
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, LLt;->b:Landroid/view/View;

    aput-object v1, v0, v8

    iget-object v1, p0, LLt;->c:Landroid/view/View;

    aput-object v1, v0, v4

    const/4 v1, 0x2

    iget-object v4, p0, LLt;->d:Landroid/view/View;

    aput-object v4, v0, v1

    invoke-static {v8, v0}, LLt;->a(I[Landroid/view/View;)V

    .line 321
    iget-object v0, p0, LLt;->c:Landroid/view/View;

    invoke-virtual {p0, v0}, LLt;->a(Landroid/view/View;)V

    .line 322
    iget-object v0, p0, LLt;->d:Landroid/view/View;

    invoke-virtual {p0, v0}, LLt;->a(Landroid/view/View;)V

    .line 323
    iget-boolean v0, p0, LLt;->c:Z

    if-eqz v0, :cond_0

    .line 324
    const v6, 0x3e19999a    # 0.15f

    const/4 v9, 0x0

    move-object v1, p0

    move-wide v4, v2

    move v7, p1

    invoke-direct/range {v1 .. v9}, LLt;->a(JJFFZLjava/lang/Runnable;)V

    goto :goto_0
.end method

.method private static varargs a(I[Landroid/view/View;)V
    .locals 3

    .prologue
    .line 249
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p1, v0

    .line 250
    if-eqz v2, :cond_0

    .line 251
    invoke-virtual {v2, p0}, Landroid/view/View;->setVisibility(I)V

    .line 249
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 254
    :cond_1
    return-void
.end method

.method private a(JFFLjava/lang/Runnable;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 225
    iget-object v0, p0, LLt;->a:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    .line 226
    invoke-direct {p0}, LLt;->a()F

    move-result p3

    .line 229
    :cond_0
    iget-object v0, p0, LLt;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 232
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p3, p4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 233
    new-instance v1, Landroid/view/animation/AnimationSet;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 234
    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 235
    invoke-virtual {v1, p1, p2}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 236
    invoke-virtual {v1, v3}, Landroid/view/animation/AnimationSet;->setFillBefore(Z)V

    .line 237
    invoke-virtual {v1, v3}, Landroid/view/animation/AnimationSet;->setFillEnabled(Z)V

    .line 238
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 240
    new-instance v0, LLv;

    invoke-direct {v0, p0, p5}, LLv;-><init>(LLt;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 242
    iget-object v0, p0, LLt;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 243
    return-void
.end method

.method private a(JJFFZLjava/lang/Runnable;)V
    .locals 9

    .prologue
    .line 200
    iget-object v0, p0, LLt;->a:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    .line 202
    iget v0, p0, LLt;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LLt;->b:I

    .line 203
    add-long v2, p1, p3

    move-object v1, p0

    move v4, p5

    move v5, p6

    move-object/from16 v6, p8

    invoke-direct/range {v1 .. v6}, LLt;->a(JFFLjava/lang/Runnable;)V

    .line 219
    :goto_0
    return-void

    .line 206
    :cond_0
    new-instance v1, LLu;

    move-object v2, p0

    move/from16 v3, p7

    move-wide v4, p3

    move v6, p5

    move v7, p6

    move-object/from16 v8, p8

    invoke-direct/range {v1 .. v8}, LLu;-><init>(LLt;ZJFFLjava/lang/Runnable;)V

    .line 218
    iget-object v0, p0, LLt;->a:Landroid/view/View;

    invoke-virtual {v0, v1, p1, p2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method static synthetic a(LLt;JFFLjava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct/range {p0 .. p5}, LLt;->a(JFFLjava/lang/Runnable;)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 298
    iget-object v0, p0, LLt;->a:LLf;

    if-nez v0, :cond_0

    .line 303
    :goto_0
    return-void

    .line 301
    :cond_0
    iget-object v0, p0, LLt;->a:LLf;

    iget-object v1, p0, LLt;->d:Landroid/view/View;

    invoke-virtual {v0, v1, p1}, LLf;->a(Landroid/view/View;Z)V

    .line 302
    iget-object v0, p0, LLt;->a:LLf;

    iget-object v1, p0, LLt;->c:Landroid/view/View;

    invoke-virtual {v0, v1, p1}, LLf;->a(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method static synthetic a(LLt;Z)Z
    .locals 0

    .prologue
    .line 25
    iput-boolean p1, p0, LLt;->c:Z

    return p1
.end method

.method static synthetic b(LLt;)I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, LLt;->b:I

    return v0
.end method

.method private b()V
    .locals 0

    .prologue
    .line 188
    invoke-direct {p0}, LLt;->c()V

    .line 189
    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 339
    const/4 v0, 0x4

    const/4 v1, 0x3

    new-array v1, v1, [Landroid/view/View;

    const/4 v2, 0x0

    iget-object v3, p0, LLt;->b:Landroid/view/View;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LLt;->c:Landroid/view/View;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, LLt;->d:Landroid/view/View;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LLt;->a(I[Landroid/view/View;)V

    .line 340
    iget-object v0, p0, LLt;->c:Landroid/view/View;

    invoke-virtual {p0, v0}, LLt;->a(Landroid/view/View;)V

    .line 341
    iget-object v0, p0, LLt;->d:Landroid/view/View;

    invoke-virtual {p0, v0}, LLt;->a(Landroid/view/View;)V

    .line 342
    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 356
    const/4 v0, 0x0

    iput-object v0, p0, LLt;->a:LLf;

    .line 357
    invoke-direct {p0}, LLt;->c()V

    .line 358
    iget v0, p0, LLt;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LLt;->a:I

    .line 359
    iget v0, p0, LLt;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LLt;->b:I

    .line 360
    return-void
.end method

.method public a(LLf;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 348
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LLf;

    iput-object v0, p0, LLt;->a:LLf;

    .line 349
    iget-object v0, p0, LLt;->a:LCU;

    invoke-virtual {v0}, LCU;->b()Z

    move-result v0

    iput-boolean v0, p0, LLt;->a:Z

    .line 350
    iget-object v0, p0, LLt;->b:Landroid/view/View;

    invoke-virtual {p1, v0, v1}, LLf;->b(Landroid/view/View;Z)V

    .line 351
    iput-boolean v1, p0, LLt;->b:Z

    .line 352
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, v0}, LLt;->a(F)V

    .line 353
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 267
    sget v0, Lxb;->selection_drop_zone_bg:I

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 268
    return-void
.end method
