.class public final enum LsL;
.super Lcom/google/android/apps/docs/app/DocumentOpenMethod;
.source "DocumentOpenMethod.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;ILacY;Ljava/lang/String;I)V
    .locals 7

    .prologue
    .line 50
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;-><init>(Ljava/lang/String;ILacY;Ljava/lang/String;ILsK;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 54
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 55
    if-eqz p4, :cond_0

    invoke-static {p1}, LakQ;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 58
    :cond_0
    return-object v0
.end method

.method public a(Landroid/content/Intent;Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 67
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const-string v0, "android.intent.extra.STREAM"

    .line 68
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_1

    .line 69
    :cond_0
    const-string v0, "android.intent.extra.STREAM"

    invoke-static {p2}, LsL;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 71
    :cond_1
    return-void
.end method
