.class public LavV;
.super Ljava/lang/Object;
.source "CycleRange.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I


# direct methods
.method private constructor <init>(IIZ)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    if-lez p2, :cond_1

    move v0, v1

    :goto_0
    const-string v2, "size must be > 0"

    invoke-static {v0, v2}, LauV;->b(ZLjava/lang/String;)V

    .line 32
    rem-int v0, p1, p2

    add-int/2addr v0, p2

    rem-int/2addr v0, p2

    iput v0, p0, LavV;->a:I

    .line 33
    iput p2, p0, LavV;->b:I

    .line 34
    if-eqz p3, :cond_0

    const/4 v1, -0x1

    :cond_0
    iput v1, p0, LavV;->c:I

    .line 35
    return-void

    .line 31
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(LavV;)I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, LavV;->b:I

    return v0
.end method

.method public static a(IIZ)LavV;
    .locals 1

    .prologue
    .line 27
    new-instance v0, LavV;

    invoke-direct {v0, p0, p1, p2}, LavV;-><init>(IIZ)V

    return-object v0
.end method

.method static synthetic b(LavV;)I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, LavV;->c:I

    return v0
.end method

.method static synthetic c(LavV;)I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, LavV;->a:I

    return v0
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    new-instance v0, LavW;

    invoke-direct {v0, p0}, LavW;-><init>(LavV;)V

    return-object v0
.end method
