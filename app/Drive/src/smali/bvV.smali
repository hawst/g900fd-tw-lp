.class final LbvV;
.super LbvT;
.source "Multibinder.java"

# interfaces
.implements LbuC;
.implements LbwE;


# instance fields
.field private a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "Lbup",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private final a:LbuP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbuP",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final a:Lbuv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbuv",
            "<",
            "Ljava/util/Set",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private a:Lcom/google/inject/Binder;

.field private final a:Ljava/lang/String;

.field private a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lbwt",
            "<*>;>;"
        }
    .end annotation
.end field

.field private a:Z

.field private final b:Lbuv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbuv",
            "<",
            "Ljava/util/Collection",
            "<",
            "LbuE",
            "<TT;>;>;>;"
        }
    .end annotation
.end field

.field private final c:Lbuv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbuv",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/inject/Binder;LbuP;Lbuv;Lbuv;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/inject/Binder;",
            "LbuP",
            "<TT;>;",
            "Lbuv",
            "<",
            "Ljava/util/Set",
            "<TT;>;>;",
            "Lbuv",
            "<",
            "Ljava/util/Collection",
            "<",
            "LbuE",
            "<TT;>;>;>;)V"
        }
    .end annotation

    .prologue
    .line 263
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LbvT;-><init>(LbvU;)V

    .line 264
    const-string v0, "binder"

    invoke-static {p1, v0}, LbvV;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/inject/Binder;

    iput-object v0, p0, LbvV;->a:Lcom/google/inject/Binder;

    .line 265
    const-string v0, "elementType"

    invoke-static {p2, v0}, LbvV;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuP;

    iput-object v0, p0, LbvV;->a:LbuP;

    .line 266
    const-string v0, "setKey"

    invoke-static {p3, v0}, LbvV;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbuv;

    iput-object v0, p0, LbvV;->a:Lbuv;

    .line 267
    const-string v0, "collectionOfProviders"

    .line 268
    invoke-static {p4, v0}, LbvV;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbuv;

    iput-object v0, p0, LbvV;->b:Lbuv;

    .line 269
    invoke-static {p3}, Lbwj;->a(Lbuv;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LbvV;->a:Ljava/lang/String;

    .line 270
    const-class v0, Ljava/lang/Boolean;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LbvV;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " permits duplicates"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbwo;->a(Ljava/lang/String;)Lbwm;

    move-result-object v1

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iput-object v0, p0, LbvV;->c:Lbuv;

    .line 271
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/inject/Binder;LbuP;Lbuv;Lbuv;LbvU;)V
    .locals 0

    .prologue
    .line 242
    invoke-direct {p0, p1, p2, p3, p4}, LbvV;-><init>(Lcom/google/inject/Binder;LbuP;Lbuv;Lbuv;)V

    return-void
.end method

.method private static a(I)I
    .locals 2

    .prologue
    .line 321
    const/4 v0, 0x3

    if-ge p0, v0, :cond_0

    .line 322
    add-int/lit8 v0, p0, 0x1

    .line 326
    :goto_0
    return v0

    .line 323
    :cond_0
    const/high16 v0, 0x40000000    # 2.0f

    if-ge p0, v0, :cond_1

    .line 324
    int-to-float v0, p0

    const/high16 v1, 0x3f400000    # 0.75f

    div-float/2addr v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0

    .line 326
    :cond_1
    const v0, 0x7fffffff

    goto :goto_0
.end method

.method static synthetic a(LbvV;)LbmF;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, LbvV;->a:LbmF;

    return-object v0
.end method

.method static synthetic a(LbvV;)Lbuv;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, LbvV;->b:Lbuv;

    return-object v0
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, LbvV;->a:Lcom/google/inject/Binder;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lbuv;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbuv",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 334
    invoke-virtual {p1}, Lbuv;->a()LbuP;

    move-result-object v0

    iget-object v1, p0, LbvV;->a:LbuP;

    invoke-virtual {v0, v1}, LbuP;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 335
    invoke-virtual {p1}, Lbuv;->a()Ljava/lang/annotation/Annotation;

    move-result-object v0

    instance-of v0, v0, LbvK;

    if-eqz v0, :cond_0

    .line 336
    invoke-virtual {p1}, Lbuv;->a()Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, LbvK;

    invoke-interface {v0}, LbvK;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LbvV;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337
    invoke-virtual {p1}, Lbuv;->a()Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, LbvK;

    invoke-interface {v0}, LbvK;->a()LbvL;

    move-result-object v0

    sget-object v1, LbvL;->b:LbvL;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(LbvV;)Z
    .locals 1

    .prologue
    .line 242
    invoke-direct {p0}, LbvV;->a()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()LbuT;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbuT",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 287
    invoke-direct {p0}, LbvV;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Multibinder was already initialized"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, LbvV;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 289
    iget-object v0, p0, LbvV;->a:Lcom/google/inject/Binder;

    iget-object v1, p0, LbvV;->a:LbuP;

    new-instance v2, Lbwj;

    iget-object v3, p0, LbvV;->a:Ljava/lang/String;

    sget-object v4, LbvL;->b:LbvL;

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Lbwj;-><init>(Ljava/lang/String;LbvL;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/inject/Binder;->a(Lbuv;)LbuT;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    .line 287
    goto :goto_0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 242
    invoke-virtual {p0}, LbvV;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/Set;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 345
    invoke-direct {p0}, LbvV;->a()Z

    move-result v0

    const-string v1, "Multibinder is not initialized"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, LbvV;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 347
    new-instance v3, Ljava/util/LinkedHashMap;

    iget-object v0, p0, LbvV;->a:LbmF;

    invoke-virtual {v0}, LbmF;->size()I

    move-result v0

    invoke-static {v0}, LbvV;->a(I)I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/LinkedHashMap;-><init>(I)V

    .line 348
    iget-object v0, p0, LbvV;->a:LbmF;

    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbup;

    .line 349
    invoke-interface {v0}, Lbup;->a()LbuE;

    move-result-object v1

    invoke-interface {v1}, LbuE;->a()Ljava/lang/Object;

    move-result-object v5

    .line 350
    if-eqz v5, :cond_1

    const/4 v1, 0x1

    :goto_0
    const-string v6, "Set injection failed due to null element"

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v1, v6, v7}, LbvV;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 351
    invoke-interface {v3, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbup;

    .line 352
    iget-boolean v6, p0, LbvV;->a:Z

    if-nez v6, :cond_0

    if-eqz v1, :cond_0

    .line 353
    invoke-static {v3, v0, v5, v1}, LbvT;->a(Ljava/util/Map;Lbup;Ljava/lang/Object;Lbup;)Lbur;

    move-result-object v0

    throw v0

    :cond_1
    move v1, v2

    .line 350
    goto :goto_0

    .line 356
    :cond_2
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v0

    return-object v0
.end method

.method a(Lbuu;)V
    .locals 7

    .prologue
    .line 298
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 299
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v2

    .line 300
    new-instance v3, LbvQ;

    invoke-direct {v3, p1}, LbvQ;-><init>(Lbuu;)V

    .line 301
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 302
    iget-object v0, p0, LbvV;->a:LbuP;

    invoke-interface {p1, v0}, Lbuu;->a(LbuP;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbup;

    .line 303
    invoke-interface {v0}, Lbup;->a()Lbuv;

    move-result-object v6

    invoke-direct {p0, v6}, LbvV;->a(Lbuv;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 306
    invoke-interface {v0, v3}, Lbup;->a(Lbwq;)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 307
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 308
    invoke-interface {v0}, Lbup;->a()Lbuv;

    move-result-object v0

    invoke-static {v0}, Lbwt;->a(Lbuv;)Lbwt;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 313
    :cond_1
    invoke-static {v1}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    iput-object v0, p0, LbvV;->a:LbmF;

    .line 314
    invoke-static {v4}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v0

    iput-object v0, p0, LbvV;->a:Ljava/util/Set;

    .line 315
    invoke-virtual {p0, p1}, LbvV;->a(Lbuu;)Z

    move-result v0

    iput-boolean v0, p0, LbvV;->a:Z

    .line 316
    const/4 v0, 0x0

    iput-object v0, p0, LbvV;->a:Lcom/google/inject/Binder;

    .line 317
    return-void
.end method

.method public a(Lcom/google/inject/Binder;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 274
    invoke-direct {p0}, LbvV;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Multibinder was already initialized"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, LbvV;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 276
    iget-object v0, p0, LbvV;->a:Lbuv;

    invoke-interface {p1, v0}, Lcom/google/inject/Binder;->a(Lbuv;)LbuT;

    move-result-object v0

    invoke-interface {v0, p0}, LbuT;->a(LbuE;)LbuU;

    .line 277
    iget-object v0, p0, LbvV;->b:Lbuv;

    invoke-interface {p1, v0}, Lcom/google/inject/Binder;->a(Lbuv;)LbuT;

    move-result-object v0

    new-instance v1, LbvW;

    invoke-direct {v1, p0}, LbvW;-><init>(LbvV;)V

    invoke-interface {v0, v1}, LbuT;->a(LbuE;)LbuU;

    .line 279
    return-void

    :cond_0
    move v0, v1

    .line 274
    goto :goto_0
.end method

.method a(Lbuu;)Z
    .locals 2

    .prologue
    .line 330
    invoke-interface {p1}, Lbuu;->a()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, LbvV;->c:Lbuv;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 421
    instance-of v0, p1, LbvV;

    if-eqz v0, :cond_0

    check-cast p1, LbvV;

    iget-object v0, p1, LbvV;->a:Lbuv;

    iget-object v1, p0, LbvV;->a:Lbuv;

    .line 422
    invoke-virtual {v0, v1}, Lbuv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, LbvV;->a:Lbuv;

    invoke-virtual {v0}, Lbuv;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 430
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, LbvV;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Multibinder<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LbvV;->a:LbuP;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LbvV;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
