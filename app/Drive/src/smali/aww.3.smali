.class public abstract Laww;
.super Landroid/app/Fragment;
.source "Viewer.java"


# instance fields
.field protected a:Landroid/view/ViewGroup;

.field protected a:Lawh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lawh",
            "<",
            "LawA;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/lang/Runnable;

.field private final a:Z

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Laww;-><init>(Z)V

    .line 119
    return-void
.end method

.method protected constructor <init>(Z)V
    .locals 1

    .prologue
    .line 128
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 109
    sget-object v0, LawA;->a:LawA;

    invoke-static {v0}, Lawg;->a(Ljava/lang/Object;)Lawh;

    move-result-object v0

    iput-object v0, p0, Laww;->a:Lawh;

    .line 129
    iput-boolean p1, p0, Laww;->a:Z

    .line 130
    return-void
.end method

.method static synthetic a(Laww;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Laww;->a:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic a(Laww;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Laww;->b:Z

    return v0
.end method

.method static synthetic a(Laww;Z)Z
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Laww;->b:Z

    return p1
.end method


# virtual methods
.method public a()Lawe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lawe",
            "<",
            "LawA;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Laww;->a:Lawh;

    return-object v0
.end method

.method public a(LauW;)Laww;
    .locals 2

    .prologue
    .line 134
    iget-boolean v0, p0, Laww;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Can\'t feed a Viewer twice"

    invoke-static {v0, v1}, LauV;->a(ZLjava/lang/String;)V

    .line 135
    invoke-virtual {p0, p1}, Laww;->a(LauW;)V

    .line 136
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Laww;->b(LauW;Landroid/os/Bundle;)V

    .line 137
    return-object p0

    .line 134
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract a()Ljava/lang/String;
.end method

.method public a()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 205
    iget-boolean v0, p0, Laww;->e:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "Already has delayed enter"

    invoke-static {v0, v2}, LauV;->a(ZLjava/lang/String;)V

    .line 206
    iget-boolean v0, p0, Laww;->c:Z

    if-eqz v0, :cond_1

    .line 207
    iput-boolean v1, p0, Laww;->d:Z

    .line 208
    invoke-virtual {p0}, Laww;->b()V

    .line 212
    :goto_1
    return-void

    .line 205
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 210
    :cond_1
    iput-boolean v1, p0, Laww;->e:Z

    goto :goto_1
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 313
    invoke-virtual {p0}, Laww;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 314
    if-eqz v0, :cond_0

    .line 315
    const-string v1, "data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 316
    new-instance v1, Lawy;

    invoke-direct {v1, p0, v0, p1}, Lawy;-><init>(Laww;Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 329
    new-instance v2, Lavb;

    invoke-direct {v2, v0, v1}, Lavb;-><init>(Landroid/os/Bundle;Lava;)V

    invoke-virtual {v2}, Lavb;->a()LauW;

    move-result-object v0

    .line 330
    if-eqz v0, :cond_0

    .line 331
    invoke-virtual {p0}, Laww;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Restore contents %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    invoke-virtual {p0, v0, p1}, Laww;->b(LauW;Landroid/os/Bundle;)V

    .line 335
    :cond_0
    return-void
.end method

.method protected a(LauW;)V
    .locals 3

    .prologue
    .line 303
    invoke-virtual {p0}, Laww;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 304
    if-nez v0, :cond_0

    .line 305
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 306
    invoke-virtual {p0, v0}, Laww;->setArguments(Landroid/os/Bundle;)V

    .line 308
    :cond_0
    const-string v1, "data"

    invoke-virtual {p1}, LauW;->a()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 309
    invoke-virtual {p0}, Laww;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Saved arg "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, LauW;->a()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    return-void
.end method

.method protected abstract a(LauW;Landroid/os/Bundle;)V
.end method

.method protected b()V
    .locals 0

    .prologue
    .line 221
    return-void
.end method

.method protected b(LauW;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 278
    iget-boolean v0, p0, Laww;->b:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Already has contents, why a second copy?"

    invoke-static {v0, v3}, LauV;->a(ZLjava/lang/String;)V

    .line 279
    iget-object v0, p0, Laww;->a:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    move v2, v1

    :cond_0
    const-string v0, "Already waits for contents"

    invoke-static {v2, v0}, LauV;->a(ZLjava/lang/String;)V

    .line 281
    iget-boolean v0, p0, Laww;->c:Z

    if-eqz v0, :cond_2

    .line 282
    invoke-virtual {p0}, Laww;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Got contents (direct) "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    invoke-virtual {p0, p1, p2}, Laww;->a(LauW;Landroid/os/Bundle;)V

    .line 284
    iput-boolean v1, p0, Laww;->b:Z

    .line 296
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 278
    goto :goto_0

    .line 286
    :cond_2
    new-instance v0, Lawx;

    invoke-direct {v0, p0, p1, p2}, Lawx;-><init>(Laww;LauW;Landroid/os/Bundle;)V

    iput-object v0, p0, Laww;->a:Ljava/lang/Runnable;

    goto :goto_1
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 180
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 181
    invoke-virtual {p0}, Laww;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onActivityCreated "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    iget-object v0, p0, Laww;->a:Lawh;

    sget-object v1, LawA;->b:LawA;

    invoke-virtual {v0, v1}, Lawh;->b(Ljava/lang/Object;)V

    .line 183
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 158
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 159
    invoke-virtual {p0}, Laww;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 164
    if-nez p2, :cond_0

    .line 165
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t recreate Viewer, make sure the file frame exists."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 167
    :cond_0
    iput-object p2, p0, Laww;->a:Landroid/view/ViewGroup;

    .line 169
    invoke-virtual {p0}, Laww;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreateView "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    iget-boolean v0, p0, Laww;->b:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Laww;->a:Ljava/lang/Runnable;

    if-nez v0, :cond_1

    .line 172
    invoke-virtual {p0, p3}, Laww;->a(Landroid/os/Bundle;)V

    .line 175
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 250
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 251
    invoke-virtual {p0}, Laww;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onDestroy "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 239
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 240
    invoke-virtual {p0}, Laww;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onDestroyView "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    const/4 v0, 0x0

    iput-object v0, p0, Laww;->a:Landroid/view/ViewGroup;

    .line 242
    iget-boolean v0, p0, Laww;->a:Z

    if-nez v0, :cond_0

    .line 243
    const/4 v0, 0x0

    iput-boolean v0, p0, Laww;->b:Z

    .line 245
    :cond_0
    iget-object v0, p0, Laww;->a:Lawh;

    sget-object v1, LawA;->a:LawA;

    invoke-virtual {v0, v1}, Lawh;->b(Ljava/lang/Object;)V

    .line 246
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 187
    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    .line 188
    invoke-virtual {p0}, Laww;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OnStart "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    iput-boolean v2, p0, Laww;->c:Z

    .line 190
    iget-object v0, p0, Laww;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Laww;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 193
    :cond_0
    iget-boolean v0, p0, Laww;->e:Z

    if-eqz v0, :cond_1

    .line 194
    const/4 v0, 0x0

    iput-boolean v0, p0, Laww;->e:Z

    .line 195
    iput-boolean v2, p0, Laww;->d:Z

    .line 196
    invoke-virtual {p0}, Laww;->b()V

    .line 198
    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 229
    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    .line 230
    invoke-virtual {p0}, Laww;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OnStop "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    const/4 v0, 0x0

    iput-boolean v0, p0, Laww;->c:Z

    .line 232
    iget-object v0, p0, Laww;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 233
    invoke-virtual {p0}, Laww;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Why is there still a pending contentsAvailable here ?? "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    :cond_0
    return-void
.end method
