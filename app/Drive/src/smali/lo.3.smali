.class final Llo;
.super Ljava/lang/Object;
.source "MediaRouter.java"

# interfaces
.implements LlZ;
.implements Lmm;


# instance fields
.field private final a:Landroid/content/Context;

.field private final a:Lcc;

.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Llj;",
            ">;>;"
        }
    .end annotation
.end field

.field private a:LkQ;

.field private a:LkW;

.field private a:LlW;

.field private final a:Llp;

.field private a:Llq;

.field private final a:Lls;

.field private a:Llv;

.field private final a:Lmb;

.field private final a:Lmc;

.field private final a:Z

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Llv;",
            ">;"
        }
    .end annotation
.end field

.field private b:Llv;

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Llu;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Llt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1503
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Llo;->a:Ljava/util/ArrayList;

    .line 1505
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Llo;->b:Ljava/util/ArrayList;

    .line 1506
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Llo;->c:Ljava/util/ArrayList;

    .line 1508
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Llo;->d:Ljava/util/ArrayList;

    .line 1510
    new-instance v0, Lmb;

    invoke-direct {v0}, Lmb;-><init>()V

    iput-object v0, p0, Llo;->a:Lmb;

    .line 1512
    new-instance v0, Lls;

    invoke-direct {v0, p0, v1}, Lls;-><init>(Llo;Llk;)V

    iput-object v0, p0, Llo;->a:Lls;

    .line 1513
    new-instance v0, Llp;

    invoke-direct {v0, p0, v1}, Llp;-><init>(Llo;Llk;)V

    iput-object v0, p0, Llo;->a:Llp;

    .line 1526
    iput-object p1, p0, Llo;->a:Landroid/content/Context;

    .line 1527
    invoke-static {p1}, Lcc;->a(Landroid/content/Context;)Lcc;

    move-result-object v0

    iput-object v0, p0, Llo;->a:Lcc;

    .line 1528
    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-static {v0}, Lv;->a(Landroid/app/ActivityManager;)Z

    move-result v0

    iput-boolean v0, p0, Llo;->a:Z

    .line 1535
    invoke-static {p1, p0}, Lmc;->a(Landroid/content/Context;Lmm;)Lmc;

    move-result-object v0

    iput-object v0, p0, Llo;->a:Lmc;

    .line 1536
    iget-object v0, p0, Llo;->a:Lmc;

    invoke-virtual {p0, v0}, Llo;->a(LkR;)V

    .line 1537
    return-void
.end method

.method private a(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 1925
    iget-object v0, p0, Llo;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1926
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1927
    iget-object v0, p0, Llo;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llv;

    invoke-static {v0}, Llv;->b(Llv;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1931
    :goto_1
    return v0

    .line 1926
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1931
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private a(LkR;)I
    .locals 3

    .prologue
    .line 1791
    iget-object v0, p0, Llo;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1792
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1793
    iget-object v0, p0, Llo;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llu;

    invoke-static {v0}, Llu;->a(Llu;)LkR;

    move-result-object v0

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 1797
    :goto_1
    return v0

    .line 1792
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1797
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private a(Llu;Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x2

    .line 1911
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Llu;->a()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1913
    invoke-direct {p0, v2}, Llo;->a(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_0

    move-object v0, v2

    .line 1919
    :goto_0
    return-object v0

    :cond_0
    move v0, v1

    .line 1917
    :goto_1
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s_%d"

    new-array v5, v1, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v6, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1918
    invoke-direct {p0, v3}, Llo;->a(Ljava/lang/String;)I

    move-result v4

    if-gez v4, :cond_1

    move-object v0, v3

    .line 1919
    goto :goto_0

    .line 1916
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method static synthetic a(Llo;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 1499
    iget-object v0, p0, Llo;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic a(Llo;)Lmb;
    .locals 1

    .prologue
    .line 1499
    iget-object v0, p0, Llo;->a:Lmb;

    return-object v0
.end method

.method static synthetic a(Llo;)Lmc;
    .locals 1

    .prologue
    .line 1499
    iget-object v0, p0, Llo;->a:Lmc;

    return-object v0
.end method

.method private a(LkR;LkX;)V
    .locals 2

    .prologue
    .line 1782
    invoke-direct {p0, p1}, Llo;->a(LkR;)I

    move-result v0

    .line 1783
    if-ltz v0, :cond_0

    .line 1785
    iget-object v1, p0, Llo;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llu;

    .line 1786
    invoke-direct {p0, v0, p2}, Llo;->a(Llu;LkX;)V

    .line 1788
    :cond_0
    return-void
.end method

.method static synthetic a(Llo;LkR;LkX;)V
    .locals 0

    .prologue
    .line 1499
    invoke-direct {p0, p1, p2}, Llo;->a(LkR;LkX;)V

    return-void
.end method

.method private a(Llu;LkX;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1802
    invoke-virtual {p1, p2}, Llu;->a(LkX;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1807
    if-eqz p2, :cond_a

    .line 1808
    invoke-virtual {p2}, LkX;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1809
    invoke-virtual {p2}, LkX;->a()Ljava/util/List;

    move-result-object v6

    .line 1811
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    move v5, v2

    move v3, v2

    .line 1812
    :goto_0
    if-ge v5, v7, :cond_b

    .line 1813
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LkN;

    .line 1814
    invoke-virtual {v0}, LkN;->a()Ljava/lang/String;

    move-result-object v1

    .line 1815
    invoke-virtual {p1, v1}, Llu;->a(Ljava/lang/String;)I

    move-result v8

    .line 1816
    if-gez v8, :cond_1

    .line 1818
    invoke-direct {p0, p1, v1}, Llo;->a(Llu;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1819
    new-instance v8, Llv;

    invoke-direct {v8, p1, v1, v4}, Llv;-><init>(Llu;Ljava/lang/String;Ljava/lang/String;)V

    .line 1820
    invoke-static {p1}, Llu;->a(Llu;)Ljava/util/ArrayList;

    move-result-object v4

    add-int/lit8 v1, v3, 0x1

    invoke-virtual {v4, v3, v8}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1821
    iget-object v3, p0, Llo;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1823
    invoke-virtual {v8, v0}, Llv;->a(LkN;)I

    .line 1825
    invoke-static {}, Llj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1826
    const-string v0, "MediaRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Route added: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1828
    :cond_0
    iget-object v0, p0, Llo;->a:Llp;

    const/16 v3, 0x101

    invoke-virtual {v0, v3, v8}, Llp;->a(ILjava/lang/Object;)V

    move v0, v2

    .line 1812
    :goto_1
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v3, v1

    move v2, v0

    goto :goto_0

    .line 1829
    :cond_1
    if-ge v8, v3, :cond_2

    .line 1830
    const-string v1, "MediaRouter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Ignoring route descriptor with duplicate id: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    move v1, v3

    goto :goto_1

    .line 1834
    :cond_2
    invoke-static {p1}, Llu;->a(Llu;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Llv;

    .line 1835
    invoke-static {p1}, Llu;->a(Llu;)Ljava/util/ArrayList;

    move-result-object v9

    add-int/lit8 v4, v3, 0x1

    invoke-static {v9, v8, v3}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 1838
    invoke-virtual {v1, v0}, Llv;->a(LkN;)I

    move-result v0

    .line 1840
    if-eqz v0, :cond_11

    .line 1841
    and-int/lit8 v3, v0, 0x1

    if-eqz v3, :cond_4

    .line 1842
    invoke-static {}, Llj;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1843
    const-string v3, "MediaRouter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Route changed: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1845
    :cond_3
    iget-object v3, p0, Llo;->a:Llp;

    const/16 v8, 0x103

    invoke-virtual {v3, v8, v1}, Llp;->a(ILjava/lang/Object;)V

    .line 1848
    :cond_4
    and-int/lit8 v3, v0, 0x2

    if-eqz v3, :cond_6

    .line 1849
    invoke-static {}, Llj;->a()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1850
    const-string v3, "MediaRouter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Route volume changed: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1852
    :cond_5
    iget-object v3, p0, Llo;->a:Llp;

    const/16 v8, 0x104

    invoke-virtual {v3, v8, v1}, Llp;->a(ILjava/lang/Object;)V

    .line 1855
    :cond_6
    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_8

    .line 1856
    invoke-static {}, Llj;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1857
    const-string v0, "MediaRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Route presentation display changed: "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1860
    :cond_7
    iget-object v0, p0, Llo;->a:Llp;

    const/16 v3, 0x105

    invoke-virtual {v0, v3, v1}, Llp;->a(ILjava/lang/Object;)V

    .line 1863
    :cond_8
    iget-object v0, p0, Llo;->b:Llv;

    if-ne v1, v0, :cond_11

    .line 1864
    const/4 v0, 0x1

    move v1, v4

    goto/16 :goto_1

    .line 1870
    :cond_9
    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Ignoring invalid provider descriptor: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    move v3, v2

    .line 1875
    :cond_b
    invoke-static {p1}, Llu;->a(Llu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-lt v1, v3, :cond_c

    .line 1877
    invoke-static {p1}, Llu;->a(Llu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llv;

    .line 1878
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Llv;->a(LkN;)I

    .line 1880
    iget-object v4, p0, Llo;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1875
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 1884
    :cond_c
    invoke-direct {p0, v2}, Llo;->a(Z)V

    .line 1891
    invoke-static {p1}, Llu;->a(Llu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_3
    if-lt v1, v3, :cond_e

    .line 1892
    invoke-static {p1}, Llu;->a(Llu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llv;

    .line 1893
    invoke-static {}, Llj;->a()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1894
    const-string v2, "MediaRouter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Route removed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1896
    :cond_d
    iget-object v2, p0, Llo;->a:Llp;

    const/16 v4, 0x102

    invoke-virtual {v2, v4, v0}, Llp;->a(ILjava/lang/Object;)V

    .line 1891
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_3

    .line 1900
    :cond_e
    invoke-static {}, Llj;->a()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1901
    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Provider changed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1903
    :cond_f
    iget-object v0, p0, Llo;->a:Llp;

    const/16 v1, 0x203

    invoke-virtual {v0, v1, p1}, Llp;->a(ILjava/lang/Object;)V

    .line 1905
    :cond_10
    return-void

    :cond_11
    move v0, v2

    move v1, v4

    goto/16 :goto_1
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1936
    iget-object v0, p0, Llo;->a:Llv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Llo;->a:Llv;

    invoke-direct {p0, v0}, Llo;->b(Llv;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1937
    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Clearing the default route because it is no longer selectable: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Llo;->a:Llv;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1939
    iput-object v3, p0, Llo;->a:Llv;

    .line 1941
    :cond_0
    iget-object v0, p0, Llo;->a:Llv;

    if-nez v0, :cond_2

    iget-object v0, p0, Llo;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1942
    iget-object v0, p0, Llo;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llv;

    .line 1943
    invoke-direct {p0, v0}, Llo;->c(Llv;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0, v0}, Llo;->b(Llv;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1944
    iput-object v0, p0, Llo;->a:Llv;

    .line 1945
    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Found default route: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Llo;->a:Llv;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1952
    :cond_2
    iget-object v0, p0, Llo;->b:Llv;

    if-eqz v0, :cond_3

    iget-object v0, p0, Llo;->b:Llv;

    invoke-direct {p0, v0}, Llo;->b(Llv;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1953
    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unselecting the current route because it is no longer selectable: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Llo;->b:Llv;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1955
    invoke-direct {p0, v3}, Llo;->b(Llv;)V

    .line 1957
    :cond_3
    iget-object v0, p0, Llo;->b:Llv;

    if-nez v0, :cond_5

    .line 1961
    invoke-direct {p0}, Llo;->c()Llv;

    move-result-object v0

    invoke-direct {p0, v0}, Llo;->b(Llv;)V

    .line 1966
    :cond_4
    :goto_0
    return-void

    .line 1962
    :cond_5
    if-eqz p1, :cond_4

    .line 1964
    invoke-direct {p0}, Llo;->c()V

    goto :goto_0
.end method

.method private a(Llv;)Z
    .locals 2

    .prologue
    .line 1984
    invoke-virtual {p1}, Llv;->a()LkR;

    move-result-object v0

    iget-object v1, p0, Llo;->a:Lmc;

    if-ne v0, v1, :cond_0

    const-string v0, "android.media.intent.category.LIVE_AUDIO"

    invoke-virtual {p1, v0}, Llv;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "android.media.intent.category.LIVE_VIDEO"

    invoke-virtual {p1, v0}, Llv;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Llv;)V
    .locals 3

    .prologue
    .line 2002
    iget-object v0, p0, Llo;->b:Llv;

    if-eq v0, p1, :cond_5

    .line 2003
    iget-object v0, p0, Llo;->b:Llv;

    if-eqz v0, :cond_1

    .line 2004
    invoke-static {}, Llj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2005
    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Route unselected: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Llo;->b:Llv;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2007
    :cond_0
    iget-object v0, p0, Llo;->a:Llp;

    const/16 v1, 0x107

    iget-object v2, p0, Llo;->b:Llv;

    invoke-virtual {v0, v1, v2}, Llp;->a(ILjava/lang/Object;)V

    .line 2008
    iget-object v0, p0, Llo;->a:LkW;

    if-eqz v0, :cond_1

    .line 2009
    iget-object v0, p0, Llo;->a:LkW;

    invoke-virtual {v0}, LkW;->c()V

    .line 2010
    iget-object v0, p0, Llo;->a:LkW;

    invoke-virtual {v0}, LkW;->a()V

    .line 2011
    const/4 v0, 0x0

    iput-object v0, p0, Llo;->a:LkW;

    .line 2015
    :cond_1
    iput-object p1, p0, Llo;->b:Llv;

    .line 2017
    iget-object v0, p0, Llo;->b:Llv;

    if-eqz v0, :cond_4

    .line 2018
    invoke-virtual {p1}, Llv;->a()LkR;

    move-result-object v0

    invoke-static {p1}, Llv;->a(Llv;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LkR;->a(Ljava/lang/String;)LkW;

    move-result-object v0

    iput-object v0, p0, Llo;->a:LkW;

    .line 2020
    iget-object v0, p0, Llo;->a:LkW;

    if-eqz v0, :cond_2

    .line 2021
    iget-object v0, p0, Llo;->a:LkW;

    invoke-virtual {v0}, LkW;->b()V

    .line 2023
    :cond_2
    invoke-static {}, Llj;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2024
    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Route selected: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Llo;->b:Llv;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2026
    :cond_3
    iget-object v0, p0, Llo;->a:Llp;

    const/16 v1, 0x106

    iget-object v2, p0, Llo;->b:Llv;

    invoke-virtual {v0, v1, v2}, Llp;->a(ILjava/lang/Object;)V

    .line 2029
    :cond_4
    invoke-direct {p0}, Llo;->c()V

    .line 2031
    :cond_5
    return-void
.end method

.method private b(Llv;)Z
    .locals 1

    .prologue
    .line 1992
    invoke-static {p1}, Llv;->a(Llv;)LkN;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Llv;->a(Llv;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()Llv;
    .locals 3

    .prologue
    .line 1973
    iget-object v0, p0, Llo;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llv;

    .line 1974
    iget-object v2, p0, Llo;->a:Llv;

    if-eq v0, v2, :cond_0

    invoke-direct {p0, v0}, Llo;->a(Llv;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v0}, Llo;->b(Llv;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1980
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Llo;->a:Llv;

    goto :goto_0
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2086
    iget-object v0, p0, Llo;->b:Llv;

    if-eqz v0, :cond_1

    .line 2087
    iget-object v0, p0, Llo;->a:Lmb;

    iget-object v2, p0, Llo;->b:Llv;

    invoke-virtual {v2}, Llv;->d()I

    move-result v2

    iput v2, v0, Lmb;->a:I

    .line 2088
    iget-object v0, p0, Llo;->a:Lmb;

    iget-object v2, p0, Llo;->b:Llv;

    invoke-virtual {v2}, Llv;->e()I

    move-result v2

    iput v2, v0, Lmb;->b:I

    .line 2089
    iget-object v0, p0, Llo;->a:Lmb;

    iget-object v2, p0, Llo;->b:Llv;

    invoke-virtual {v2}, Llv;->c()I

    move-result v2

    iput v2, v0, Lmb;->c:I

    .line 2090
    iget-object v0, p0, Llo;->a:Lmb;

    iget-object v2, p0, Llo;->b:Llv;

    invoke-virtual {v2}, Llv;->b()I

    move-result v2

    iput v2, v0, Lmb;->d:I

    .line 2091
    iget-object v0, p0, Llo;->a:Lmb;

    iget-object v2, p0, Llo;->b:Llv;

    invoke-virtual {v2}, Llv;->a()I

    move-result v2

    iput v2, v0, Lmb;->e:I

    .line 2093
    iget-object v0, p0, Llo;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    .line 2094
    :goto_0
    if-ge v2, v3, :cond_0

    .line 2095
    iget-object v0, p0, Llo;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llt;

    .line 2096
    invoke-virtual {v0}, Llt;->a()V

    .line 2094
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2098
    :cond_0
    iget-object v0, p0, Llo;->a:Llq;

    if-eqz v0, :cond_1

    .line 2100
    iget-object v0, p0, Llo;->a:Lmb;

    iget v0, v0, Lmb;->c:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    .line 2102
    const/4 v0, 0x2

    .line 2104
    :goto_1
    iget-object v1, p0, Llo;->a:Llq;

    iget-object v2, p0, Llo;->a:Lmb;

    iget v2, v2, Lmb;->b:I

    iget-object v3, p0, Llo;->a:Lmb;

    iget v3, v3, Lmb;->a:I

    invoke-virtual {v1, v0, v2, v3}, Llq;->a(III)V

    .line 2108
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private c(Llv;)Z
    .locals 2

    .prologue
    .line 1996
    invoke-virtual {p1}, Llv;->a()LkR;

    move-result-object v0

    iget-object v1, p0, Llo;->a:Lmc;

    if-ne v0, v1, :cond_0

    invoke-static {p1}, Llv;->a(Llv;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "DEFAULT_ROUTE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Llv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1607
    iget-object v0, p0, Llo;->b:Ljava/util/ArrayList;

    return-object v0
.end method

.method public a(Landroid/content/Context;)Llj;
    .locals 3

    .prologue
    .line 1549
    iget-object v0, p0, Llo;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-ltz v1, :cond_1

    .line 1550
    iget-object v0, p0, Llo;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llj;

    .line 1551
    if-nez v0, :cond_0

    .line 1552
    iget-object v0, p0, Llo;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move v0, v1

    goto :goto_0

    .line 1553
    :cond_0
    iget-object v2, v0, Llj;->a:Landroid/content/Context;

    if-ne v2, p1, :cond_2

    .line 1559
    :goto_1
    return-object v0

    .line 1557
    :cond_1
    new-instance v0, Llj;

    invoke-direct {v0, p1}, Llj;-><init>(Landroid/content/Context;)V

    .line 1558
    iget-object v1, p0, Llo;->a:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a()Llv;
    .locals 2

    .prologue
    .line 1615
    iget-object v0, p0, Llo;->a:Llv;

    if-nez v0, :cond_0

    .line 1619
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is no default route.  The media router has not yet been fully initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1622
    :cond_0
    iget-object v0, p0, Llo;->a:Llv;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Llv;
    .locals 2

    .prologue
    .line 2035
    iget-object v0, p0, Llo;->a:Lmc;

    invoke-direct {p0, v0}, Llo;->a(LkR;)I

    move-result v0

    .line 2036
    if-ltz v0, :cond_0

    .line 2037
    iget-object v1, p0, Llo;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llu;

    .line 2038
    invoke-virtual {v0, p1}, Llu;->a(Ljava/lang/String;)I

    move-result v1

    .line 2039
    if-ltz v1, :cond_0

    .line 2040
    invoke-static {v0}, Llu;->a(Llu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llv;

    .line 2043
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 1542
    new-instance v0, LlW;

    iget-object v1, p0, Llo;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, LlW;-><init>(Landroid/content/Context;LlZ;)V

    iput-object v0, p0, Llo;->a:LlW;

    .line 1544
    iget-object v0, p0, Llo;->a:LlW;

    invoke-virtual {v0}, LlW;->a()V

    .line 1545
    return-void
.end method

.method public a(LkR;)V
    .locals 4

    .prologue
    .line 1742
    invoke-direct {p0, p1}, Llo;->a(LkR;)I

    move-result v0

    .line 1743
    if-gez v0, :cond_1

    .line 1745
    new-instance v0, Llu;

    invoke-direct {v0, p1}, Llu;-><init>(LkR;)V

    .line 1746
    iget-object v1, p0, Llo;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1747
    invoke-static {}, Llj;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1748
    const-string v1, "MediaRouter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Provider added: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1750
    :cond_0
    iget-object v1, p0, Llo;->a:Llp;

    const/16 v2, 0x201

    invoke-virtual {v1, v2, v0}, Llp;->a(ILjava/lang/Object;)V

    .line 1752
    invoke-virtual {p1}, LkR;->a()LkX;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Llo;->a(Llu;LkX;)V

    .line 1754
    iget-object v0, p0, Llo;->a:Lls;

    invoke-virtual {p1, v0}, LkR;->a(LkT;)V

    .line 1756
    iget-object v0, p0, Llo;->a:LkQ;

    invoke-virtual {p1, v0}, LkR;->a(LkQ;)V

    .line 1758
    :cond_1
    return-void
.end method

.method public a(Llv;)V
    .locals 3

    .prologue
    .line 1637
    iget-object v0, p0, Llo;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1638
    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ignoring attempt to select removed route: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1647
    :goto_0
    return-void

    .line 1641
    :cond_0
    invoke-static {p1}, Llv;->a(Llv;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1642
    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ignoring attempt to select disabled route: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1646
    :cond_1
    invoke-direct {p0, p1}, Llo;->b(Llv;)V

    goto :goto_0
.end method

.method public a(Llv;I)V
    .locals 1

    .prologue
    .line 1595
    iget-object v0, p0, Llo;->b:Llv;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Llo;->a:LkW;

    if-eqz v0, :cond_0

    .line 1596
    iget-object v0, p0, Llo;->a:LkW;

    invoke-virtual {v0, p2}, LkW;->a(I)V

    .line 1598
    :cond_0
    return-void
.end method

.method public a(Llg;I)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1650
    invoke-virtual {p1}, Llg;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1673
    :goto_0
    return v0

    .line 1655
    :cond_0
    and-int/lit8 v0, p2, 0x2

    if-nez v0, :cond_1

    iget-boolean v0, p0, Llo;->a:Z

    if-eqz v0, :cond_1

    move v0, v2

    .line 1656
    goto :goto_0

    .line 1660
    :cond_1
    iget-object v0, p0, Llo;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v1

    .line 1661
    :goto_1
    if-ge v3, v4, :cond_4

    .line 1662
    iget-object v0, p0, Llo;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llv;

    .line 1663
    and-int/lit8 v5, p2, 0x1

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Llv;->d()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1661
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 1667
    :cond_3
    invoke-virtual {v0, p1}, Llv;->a(Llg;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 1668
    goto :goto_0

    :cond_4
    move v0, v1

    .line 1673
    goto :goto_0
.end method

.method public b()Llv;
    .locals 2

    .prologue
    .line 1626
    iget-object v0, p0, Llo;->b:Llv;

    if-nez v0, :cond_0

    .line 1630
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is no currently selected route.  The media router has not yet been fully initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1633
    :cond_0
    iget-object v0, p0, Llo;->b:Llv;

    return-object v0
.end method

.method public b()V
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 1678
    .line 1680
    new-instance v8, Lli;

    invoke-direct {v8}, Lli;-><init>()V

    .line 1681
    iget-object v0, p0, Llo;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v2, v5

    move v4, v5

    :goto_0
    add-int/lit8 v7, v0, -0x1

    if-ltz v7, :cond_5

    .line 1682
    iget-object v0, p0, Llo;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llj;

    .line 1683
    if-nez v0, :cond_1

    .line 1684
    iget-object v0, p0, Llo;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    move v0, v7

    .line 1704
    goto :goto_0

    .line 1686
    :cond_1
    iget-object v1, v0, Llj;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v6, v5

    .line 1687
    :goto_1
    if-ge v6, v9, :cond_0

    .line 1688
    iget-object v1, v0, Llj;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Llm;

    .line 1689
    iget-object v10, v1, Llm;->a:Llg;

    invoke-virtual {v8, v10}, Lli;->a(Llg;)Lli;

    .line 1690
    iget v10, v1, Llm;->a:I

    and-int/lit8 v10, v10, 0x1

    if-eqz v10, :cond_2

    move v2, v3

    move v4, v3

    .line 1694
    :cond_2
    iget v10, v1, Llm;->a:I

    and-int/lit8 v10, v10, 0x4

    if-eqz v10, :cond_3

    .line 1695
    iget-boolean v10, p0, Llo;->a:Z

    if-nez v10, :cond_3

    move v4, v3

    .line 1699
    :cond_3
    iget v1, v1, Llm;->a:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    move v4, v3

    .line 1687
    :cond_4
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_1

    .line 1705
    :cond_5
    if-eqz v4, :cond_7

    invoke-virtual {v8}, Lli;->a()Llg;

    move-result-object v0

    .line 1708
    :goto_2
    iget-object v1, p0, Llo;->a:LkQ;

    if-eqz v1, :cond_8

    iget-object v1, p0, Llo;->a:LkQ;

    invoke-virtual {v1}, LkQ;->a()Llg;

    move-result-object v1

    invoke-virtual {v1, v0}, Llg;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Llo;->a:LkQ;

    invoke-virtual {v1}, LkQ;->a()Z

    move-result v1

    if-ne v1, v2, :cond_8

    .line 1738
    :cond_6
    return-void

    .line 1705
    :cond_7
    sget-object v0, Llg;->a:Llg;

    goto :goto_2

    .line 1713
    :cond_8
    invoke-virtual {v0}, Llg;->a()Z

    move-result v1

    if-eqz v1, :cond_b

    if-nez v2, :cond_b

    .line 1715
    iget-object v0, p0, Llo;->a:LkQ;

    if-eqz v0, :cond_6

    .line 1718
    const/4 v0, 0x0

    iput-object v0, p0, Llo;->a:LkQ;

    .line 1723
    :goto_3
    invoke-static {}, Llj;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1724
    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Updated discovery request: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Llo;->a:LkQ;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1726
    :cond_9
    if-eqz v4, :cond_a

    if-nez v2, :cond_a

    iget-boolean v0, p0, Llo;->a:Z

    if-eqz v0, :cond_a

    .line 1727
    const-string v0, "MediaRouter"

    const-string v1, "Forcing passive route discovery on a low-RAM device, system performance may be affected.  Please consider using CALLBACK_FLAG_REQUEST_DISCOVERY instead of CALLBACK_FLAG_FORCE_DISCOVERY."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1734
    :cond_a
    iget-object v0, p0, Llo;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v5

    .line 1735
    :goto_4
    if-ge v1, v2, :cond_6

    .line 1736
    iget-object v0, p0, Llo;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llu;

    invoke-static {v0}, Llu;->a(Llu;)LkR;

    move-result-object v0

    iget-object v3, p0, Llo;->a:LkQ;

    invoke-virtual {v0, v3}, LkR;->a(LkQ;)V

    .line 1735
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1721
    :cond_b
    new-instance v1, LkQ;

    invoke-direct {v1, v0, v2}, LkQ;-><init>(Llg;Z)V

    iput-object v1, p0, Llo;->a:LkQ;

    goto :goto_3
.end method

.method public b(LkR;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1762
    invoke-direct {p0, p1}, Llo;->a(LkR;)I

    move-result v1

    .line 1763
    if-ltz v1, :cond_1

    .line 1765
    invoke-virtual {p1, v2}, LkR;->a(LkT;)V

    .line 1767
    invoke-virtual {p1, v2}, LkR;->a(LkQ;)V

    .line 1769
    iget-object v0, p0, Llo;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llu;

    .line 1770
    invoke-direct {p0, v0, v2}, Llo;->a(Llu;LkX;)V

    .line 1772
    invoke-static {}, Llj;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1773
    const-string v2, "MediaRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Provider removed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1775
    :cond_0
    iget-object v2, p0, Llo;->a:Llp;

    const/16 v3, 0x202

    invoke-virtual {v2, v3, v0}, Llp;->a(ILjava/lang/Object;)V

    .line 1776
    iget-object v0, p0, Llo;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1778
    :cond_1
    return-void
.end method

.method public b(Llv;I)V
    .locals 1

    .prologue
    .line 1601
    iget-object v0, p0, Llo;->b:Llv;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Llo;->a:LkW;

    if-eqz v0, :cond_0

    .line 1602
    iget-object v0, p0, Llo;->a:LkW;

    invoke-virtual {v0, p2}, LkW;->b(I)V

    .line 1604
    :cond_0
    return-void
.end method
