.class public Lanm;
.super Ljava/lang/Object;
.source "UtilitiesModule.java"

# interfaces
.implements LbuC;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    return-void
.end method


# virtual methods
.method public a(Lcom/google/inject/Binder;)V
    .locals 1

    .prologue
    .line 177
    new-instance v0, Lale;

    invoke-direct {v0}, Lale;-><init>()V

    invoke-interface {p1, v0}, Lcom/google/inject/Binder;->a(LbuC;)V

    .line 178
    return-void
.end method

.method provideAccountCapabilityCache()Ljava/util/Map;
    .locals 1
    .annotation runtime Lann;
    .end annotation

    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "LaFO;",
            "LajN;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    invoke-static {}, LboS;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method provideAccountCapabilityFactory(LajP;)LajO;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .prologue
    .line 101
    return-object p1
.end method

.method provideAccountSwitcher(LajR;)LajQ;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 140
    return-object p1
.end method

.method provideBitmapUtilities()Lcom/google/android/apps/docs/utils/BitmapUtilities;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 54
    new-instance v0, Laks;

    invoke-direct {v0}, Laks;-><init>()V

    return-object v0
.end method

.method provideCipherUtilities()Lakz;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 128
    new-instance v0, LakA;

    invoke-direct {v0}, LakA;-><init>()V

    return-object v0
.end method

.method provideClock()LaKM;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 60
    sget-object v0, LaKN;->a:LaKN;

    return-object v0
.end method

.method provideConnectivity(LakO;)LaKR;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 158
    return-object p1
.end method

.method provideFeedbackReporter()Lall;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .prologue
    .line 116
    new-instance v0, Lalk;

    invoke-direct {v0}, Lalk;-><init>()V

    return-object v0
.end method

.method provideFileUtilities()Lalo;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .prologue
    .line 48
    new-instance v0, Lalp;

    invoke-direct {v0}, Lalp;-><init>()V

    return-object v0
.end method

.method provideNetworkUtilities(Lami;)Lamh;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 146
    return-object p1
.end method

.method providePreferenceUtils(Lamp;)Lamn;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .prologue
    .line 94
    return-object p1
.end method

.method providePreviewPageFetcher(Landroid/content/Context;)LaqA;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 172
    const-class v0, LaqA;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaqA;

    return-object v0
.end method

.method provideRealTimeClock()LaKM;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxv;
        a = "RealtimeClock"
    .end annotation

    .prologue
    .line 74
    sget-object v0, LaKN;->c:LaKN;

    return-object v0
.end method

.method provideStorageUtilities()LamL;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .prologue
    .line 81
    new-instance v0, LamM;

    invoke-direct {v0}, LamM;-><init>()V

    return-object v0
.end method

.method provideThumbnailFetchHelper(LamX;)LamW;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 152
    return-object p1
.end method

.method provideThumbnailFetcher(Lapi;)Lapd;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .prologue
    .line 166
    invoke-virtual {p1}, Lapi;->a()Lapd;

    move-result-object v0

    return-object v0
.end method

.method provideThumbnailFetcherImpl(Lana;)LamY;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 122
    return-object p1
.end method

.method provideTiledBlurEvaluator()Lanc;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 134
    new-instance v0, Lcom/google/android/apps/docs/utils/TiledBlurEvaluatorFactoryImpl;

    invoke-direct {v0}, Lcom/google/android/apps/docs/utils/TiledBlurEvaluatorFactoryImpl;-><init>()V

    return-object v0
.end method

.method provideUptimeClock()LaKM;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxv;
        a = "UptimeClock"
    .end annotation

    .prologue
    .line 67
    sget-object v0, LaKN;->b:LaKN;

    return-object v0
.end method

.method provideWaitingRateLimiter(LZN;)LZS;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 87
    return-object p1
.end method
