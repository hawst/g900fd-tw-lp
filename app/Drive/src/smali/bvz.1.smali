.class public final Lbvz;
.super Ljava/lang/Object;
.source "ProviderMethodsModule.java"

# interfaces
.implements LbuC;


# static fields
.field private static final a:Lbuv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbuv",
            "<",
            "Ljava/util/logging/Logger;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LbuP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbuP",
            "<*>;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/Object;

.field private final a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Ljava/util/logging/Logger;

    invoke-static {v0}, Lbuv;->a(Ljava/lang/Class;)Lbuv;

    move-result-object v0

    sput-object v0, Lbvz;->a:Lbuv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const-string v0, "delegate"

    invoke-static {p1, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lbvz;->a:Ljava/lang/Object;

    .line 59
    iget-object v0, p0, Lbvz;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    iput-object v0, p0, Lbvz;->a:LbuP;

    .line 60
    iput-boolean p2, p0, Lbvz;->a:Z

    .line 61
    return-void
.end method

.method public static a(LbuC;)LbuC;
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lbvz;->a(Ljava/lang/Object;Z)LbuC;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Z)LbuC;
    .locals 1

    .prologue
    .line 83
    instance-of v0, p0, Lbvz;

    if-eqz v0, :cond_0

    .line 84
    sget-object v0, LbwF;->a:LbuC;

    .line 87
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lbvz;

    invoke-direct {v0, p0, p1}, Lbvz;-><init>(Ljava/lang/Object;Z)V

    goto :goto_0
.end method

.method static synthetic a(Lbvz;)LbuP;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lbvz;->a:LbuP;

    return-object v0
.end method

.method private a(Lcom/google/inject/Binder;Ljava/lang/reflect/Method;)Lbvw;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/inject/Binder;",
            "Ljava/lang/reflect/Method;",
            ")",
            "Lbvw",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 201
    invoke-interface {p1, p2}, Lcom/google/inject/Binder;->a(Ljava/lang/Object;)Lcom/google/inject/Binder;

    move-result-object v2

    .line 202
    new-instance v3, Lbvd;

    invoke-direct {v3, p2}, Lbvd;-><init>(Ljava/lang/Object;)V

    .line 205
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 206
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 207
    iget-object v0, p0, Lbvz;->a:LbuP;

    invoke-virtual {v0, p2}, LbuP;->a(Ljava/lang/reflect/Member;)Ljava/util/List;

    move-result-object v5

    .line 208
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getParameterAnnotations()[[Ljava/lang/annotation/Annotation;

    move-result-object v7

    .line 209
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 210
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuP;

    aget-object v8, v7, v1

    invoke-virtual {p0, v3, v0, p2, v8}, Lbvz;->a(Lbvd;LbuP;Ljava/lang/reflect/Member;[Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 211
    sget-object v8, Lbvz;->a:Lbuv;

    invoke-virtual {v0, v8}, Lbuv;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 215
    const-class v0, Ljava/util/logging/Logger;

    invoke-static {}, LbvC;->a()Ljava/lang/annotation/Annotation;

    move-result-object v8

    invoke-static {v0, v8}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 216
    invoke-interface {v2, v0}, Lcom/google/inject/Binder;->a(Lbuv;)LbuT;

    move-result-object v8

    new-instance v9, LbvA;

    invoke-direct {v9, p2}, LbvA;-><init>(Ljava/lang/reflect/Method;)V

    invoke-interface {v8, v9}, LbuT;->a(LbuE;)LbuU;

    .line 219
    :cond_0
    invoke-static {v0}, Lbwt;->a(Lbuv;)Lbwt;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 220
    invoke-interface {v2, v0}, Lcom/google/inject/Binder;->a(Lbuv;)LbuE;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 224
    :cond_1
    iget-object v0, p0, Lbvz;->a:LbuP;

    invoke-virtual {v0, p2}, LbuP;->a(Ljava/lang/reflect/Method;)LbuP;

    move-result-object v0

    .line 226
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v1

    invoke-virtual {p0, v3, v0, p2, v1}, Lbvz;->a(Lbvd;LbuP;Ljava/lang/reflect/Member;[Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 228
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v1

    invoke-static {v3, v1}, LbuW;->a(Lbvd;[Ljava/lang/annotation/Annotation;)Ljava/lang/Class;

    move-result-object v5

    .line 230
    invoke-virtual {v3}, Lbvd;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbwy;

    .line 231
    invoke-interface {v2, v1}, Lcom/google/inject/Binder;->a(Lbwy;)V

    goto :goto_1

    .line 234
    :cond_2
    iget-object v2, p0, Lbvz;->a:Ljava/lang/Object;

    invoke-static {v6}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v3

    iget-boolean v6, p0, Lbvz;->a:Z

    move-object v1, p2

    invoke-static/range {v0 .. v6}, Lbvw;->a(Lbuv;Ljava/lang/reflect/Method;Ljava/lang/Object;LbmY;Ljava/util/List;Ljava/lang/Class;Z)Lbvw;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/reflect/Method;)Z
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Ljava/lang/reflect/Method;->isBridge()Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    invoke-virtual {p0}, Ljava/lang/reflect/Method;->isSynthetic()Z

    move-result v0

    if-nez v0, :cond_0

    const-class v0, LbuF;

    .line 149
    invoke-virtual {p0, v0}, Ljava/lang/reflect/Method;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/reflect/Method;Ljava/lang/reflect/Method;)Z
    .locals 2

    .prologue
    .line 189
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getModifiers()I

    move-result v0

    .line 190
    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isPublic(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isProtected(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 191
    :cond_0
    const/4 v0, 0x1

    .line 197
    :goto_0
    return v0

    .line 193
    :cond_1
    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isPrivate(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 194
    const/4 v0, 0x0

    goto :goto_0

    .line 197
    :cond_2
    invoke-virtual {p0}, Ljava/lang/reflect/Method;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method a(Lbvd;LbuP;Ljava/lang/reflect/Member;[Ljava/lang/annotation/Annotation;)Lbuv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbvd;",
            "LbuP",
            "<TT;>;",
            "Ljava/lang/reflect/Member;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            ")",
            "Lbuv",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 239
    invoke-static {p1, p3, p4}, LbuW;->a(Lbvd;Ljava/lang/reflect/Member;[Ljava/lang/annotation/Annotation;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    .line 240
    if-nez v0, :cond_0

    invoke-static {p2}, Lbuv;->a(LbuP;)Lbuv;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p2, v0}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/google/inject/Binder;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/inject/Binder;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbvw",
            "<*>;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 97
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 98
    invoke-static {}, Lbmq;->a()Lbmq;

    move-result-object v4

    .line 99
    iget-object v0, p0, Lbvz;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    :goto_0
    const-class v1, Ljava/lang/Object;

    if-eq v0, v1, :cond_3

    .line 100
    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v5

    array-length v6, v5

    move v1, v2

    :goto_1
    if-ge v1, v6, :cond_2

    aget-object v7, v5, v1

    .line 106
    invoke-virtual {v7}, Ljava/lang/reflect/Method;->getModifiers()I

    move-result v8

    and-int/lit8 v8, v8, 0xa

    if-nez v8, :cond_0

    .line 107
    invoke-virtual {v7}, Ljava/lang/reflect/Method;->isBridge()Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {v7}, Ljava/lang/reflect/Method;->isSynthetic()Z

    move-result v8

    if-nez v8, :cond_0

    .line 108
    new-instance v8, LbvB;

    invoke-direct {v8, p0, v7}, LbvB;-><init>(Lbvz;Ljava/lang/reflect/Method;)V

    invoke-interface {v4, v8, v7}, Lbph;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 110
    :cond_0
    invoke-static {v7}, Lbvz;->a(Ljava/lang/reflect/Method;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 111
    invoke-direct {p0, p1, v7}, Lbvz;->a(Lcom/google/inject/Binder;Ljava/lang/reflect/Method;)Lbvw;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 99
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 118
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvw;

    .line 119
    invoke-virtual {v0}, Lbvw;->a()Ljava/lang/reflect/Method;

    move-result-object v5

    .line 120
    new-instance v0, LbvB;

    invoke-direct {v0, p0, v5}, LbvB;-><init>(Lbvz;Ljava/lang/reflect/Method;)V

    invoke-interface {v4, v0}, Lbph;->b(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Method;

    .line 123
    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 127
    invoke-static {v0, v5}, Lbvz;->a(Ljava/lang/reflect/Method;Ljava/lang/reflect/Method;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 128
    const-string v6, "Overriding @Provides methods is not allowed.\n\t@Provides method: %s\n\toverridden by: %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v5, v7, v2

    const/4 v5, 0x1

    aput-object v0, v7, v5

    invoke-interface {p1, v6, v7}, Lcom/google/inject/Binder;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 137
    :cond_6
    return-object v3
.end method

.method public declared-synchronized a(Lcom/google/inject/Binder;)V
    .locals 2

    .prologue
    .line 91
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lbvz;->a(Lcom/google/inject/Binder;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvw;

    .line 92
    invoke-virtual {v0, p1}, Lbvw;->a(Lcom/google/inject/Binder;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 94
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 244
    instance-of v0, p1, Lbvz;

    if-eqz v0, :cond_0

    check-cast p1, Lbvz;

    iget-object v0, p1, Lbvz;->a:Ljava/lang/Object;

    iget-object v1, p0, Lbvz;->a:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lbvz;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method
