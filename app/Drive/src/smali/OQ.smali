.class public LOQ;
.super Ljava/lang/Object;
.source "PsynchoHelperImpl.java"

# interfaces
.implements LVP;


# instance fields
.field private final a:LOD;

.field private final a:LOx;

.field private final a:LVQ;

.field private final a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "LOG;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "LOy;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "LOt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LVQ;LOx;Laja;Laja;Laja;LOD;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LVQ;",
            "LOx;",
            "Laja",
            "<",
            "LOG;",
            ">;",
            "Laja",
            "<",
            "LOy;",
            ">;",
            "Laja",
            "<",
            "LOt;",
            ">;",
            "LOD;",
            ")V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, LOQ;->a:LVQ;

    .line 48
    iput-object p2, p0, LOQ;->a:LOx;

    .line 49
    iput-object p3, p0, LOQ;->a:Laja;

    .line 50
    iput-object p4, p0, LOQ;->b:Laja;

    .line 51
    iput-object p5, p0, LOQ;->c:Laja;

    .line 52
    iput-object p6, p0, LOQ;->a:LOD;

    .line 53
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x3

    return v0
.end method

.method public a()LBt;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, LOQ;->c:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBt;

    return-object v0
.end method

.method public a()LJs;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, LOQ;->b:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJs;

    return-object v0
.end method

.method public a()Lrg;
    .locals 1

    .prologue
    .line 72
    sget-object v0, LOE;->b:Lrg;

    return-object v0
.end method

.method public a()LzQ;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, LOQ;->a:LOD;

    return-object v0
.end method

.method public a(LaFO;)V
    .locals 6

    .prologue
    .line 58
    :try_start_0
    invoke-static {}, LbmL;->a()LbmM;

    move-result-object v1

    .line 59
    invoke-static {}, LVR;->values()[LVR;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 60
    iget-object v5, p0, LOQ;->a:LVQ;

    invoke-interface {v5, p1, v4}, LVQ;->a(LaFO;LVR;)Z

    move-result v5

    .line 61
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    .line 59
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 64
    :cond_0
    iget-object v0, p0, LOQ;->a:LOx;

    invoke-virtual {v1}, LbmM;->a()LbmL;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LOx;->a(LaFO;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    :goto_1
    return-void

    .line 65
    :catch_0
    move-exception v0

    .line 66
    const-string v1, "PsynchoHelperImpl"

    const-string v2, "Error fetching or saving configuration"

    invoke-static {v1, v0, v2}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public a(Landroid/content/Context;ILaFO;)Z
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, LOQ;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LOG;

    .line 84
    invoke-virtual {v0, p1, p2, p3}, LOG;->a(Landroid/content/Context;ILaFO;)Z

    move-result v0

    return v0
.end method
