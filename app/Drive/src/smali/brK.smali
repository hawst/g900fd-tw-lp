.class LbrK;
.super Ljava/lang/Object;
.source "GellyAnnotatedBindingBuilder.java"

# interfaces
.implements LbuQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LbuQ",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:LbrP;

.field private a:LbuP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbuP",
            "<TT;>;"
        }
    .end annotation
.end field

.field private a:Lbuv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbuv",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LbuP;LbrP;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbuP",
            "<TT;>;",
            "LbrP;",
            ")V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuP;

    iput-object v0, p0, LbrK;->a:LbuP;

    .line 32
    invoke-static {p2}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbrP;

    iput-object v0, p0, LbrK;->a:LbrP;

    .line 33
    return-void
.end method

.method constructor <init>(Lbuv;LbrP;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbuv",
            "<TT;>;",
            "LbrP;",
            ")V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbuv;

    iput-object v0, p0, LbrK;->a:Lbuv;

    .line 26
    invoke-static {p2}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbrP;

    iput-object v0, p0, LbrK;->a:LbrP;

    .line 27
    return-void
.end method

.method private a()Lbuv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbuv",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, LbrK;->a:Lbuv;

    if-nez v0, :cond_0

    .line 136
    iget-object v0, p0, LbrK;->a:LbuP;

    invoke-static {v0}, Lbuv;->a(LbuP;)Lbuv;

    move-result-object v0

    iput-object v0, p0, LbrK;->a:Lbuv;

    .line 138
    :cond_0
    iget-object v0, p0, LbrK;->a:Lbuv;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Class;)LbuT;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LbuT",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, LbrK;->a:Lbuv;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbso;->a(Z)V

    .line 123
    iget-object v0, p0, LbrK;->a:LbuP;

    invoke-static {v0, p1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iput-object v0, p0, LbrK;->a:Lbuv;

    .line 124
    return-object p0

    .line 122
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/annotation/Annotation;)LbuT;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/annotation/Annotation;",
            ")",
            "LbuT",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 129
    iget-object v0, p0, LbrK;->a:Lbuv;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbso;->a(Z)V

    .line 130
    iget-object v0, p0, LbrK;->a:LbuP;

    invoke-static {v0, p1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iput-object v0, p0, LbrK;->a:Lbuv;

    .line 131
    return-object p0

    .line 129
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LbuE;)LbuU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbuE",
            "<+TT;>;)",
            "LbuU;"
        }
    .end annotation

    .prologue
    .line 74
    invoke-virtual {p0, p1}, LbrK;->a(Lbxw;)LbuU;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbuv;)LbuU;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbuv",
            "<+TT;>;)",
            "LbuU;"
        }
    .end annotation

    .prologue
    .line 50
    return-object p0
.end method

.method public a(Lbxw;)LbuU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbxw",
            "<+TT;>;)",
            "LbuU;"
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, LbrK;->a:LbrP;

    invoke-direct {p0}, LbrK;->a()Lbuv;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LbrP;->a(Lbuv;Lbxw;)V

    .line 81
    return-object p0
.end method

.method public a(Ljava/lang/Class;)LbuU;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+TT;>;)",
            "LbuU;"
        }
    .end annotation

    .prologue
    .line 38
    return-object p0
.end method

.method public a(LbuH;)V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, LbrK;->a:LbrP;

    invoke-direct {p0}, LbrK;->a()Lbuv;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LbrP;->a(Lbuv;LbuH;)V

    .line 118
    return-void
.end method

.method public a(Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 113
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, LbrK;->a:LbrP;

    invoke-direct {p0}, LbrK;->a()Lbuv;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LbrP;->a(Lbuv;Ljava/lang/Object;)V

    .line 70
    return-void
.end method

.method public b(Ljava/lang/Class;)LbuU;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lbxw",
            "<+TT;>;>;)",
            "LbuU;"
        }
    .end annotation

    .prologue
    .line 88
    return-object p0
.end method
