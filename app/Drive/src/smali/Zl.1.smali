.class public final enum LZl;
.super Ljava/lang/Enum;
.source "PunchWebViewFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LZl;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LZl;

.field private static a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LZl;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic a:[LZl;

.field public static final enum b:LZl;

.field public static final enum c:LZl;

.field public static final enum d:LZl;


# instance fields
.field private final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 228
    new-instance v1, LZl;

    const-string v2, "EMPTY"

    invoke-direct {v1, v2, v0, v3}, LZl;-><init>(Ljava/lang/String;II)V

    sput-object v1, LZl;->a:LZl;

    .line 229
    new-instance v1, LZl;

    const-string v2, "LOADED"

    invoke-direct {v1, v2, v3, v4}, LZl;-><init>(Ljava/lang/String;II)V

    sput-object v1, LZl;->b:LZl;

    .line 230
    new-instance v1, LZl;

    const-string v2, "LOADING"

    invoke-direct {v1, v2, v4, v5}, LZl;-><init>(Ljava/lang/String;II)V

    sput-object v1, LZl;->c:LZl;

    .line 231
    new-instance v1, LZl;

    const-string v2, "ERROR"

    invoke-direct {v1, v2, v5, v6}, LZl;-><init>(Ljava/lang/String;II)V

    sput-object v1, LZl;->d:LZl;

    .line 227
    new-array v1, v6, [LZl;

    sget-object v2, LZl;->a:LZl;

    aput-object v2, v1, v0

    sget-object v2, LZl;->b:LZl;

    aput-object v2, v1, v3

    sget-object v2, LZl;->c:LZl;

    aput-object v2, v1, v4

    sget-object v2, LZl;->d:LZl;

    aput-object v2, v1, v5

    sput-object v1, LZl;->a:[LZl;

    .line 235
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    sput-object v1, LZl;->a:Landroid/util/SparseArray;

    .line 237
    invoke-static {}, LZl;->values()[LZl;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 238
    sget-object v4, LZl;->a:Landroid/util/SparseArray;

    iget v5, v3, LZl;->a:I

    invoke-virtual {v4, v5, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 237
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 240
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 252
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 253
    iput p3, p0, LZl;->a:I

    .line 254
    return-void
.end method

.method public static a(I)LZl;
    .locals 1

    .prologue
    .line 249
    sget-object v0, LZl;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZl;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LZl;
    .locals 1

    .prologue
    .line 227
    const-class v0, LZl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LZl;

    return-object v0
.end method

.method public static values()[LZl;
    .locals 1

    .prologue
    .line 227
    sget-object v0, LZl;->a:[LZl;

    invoke-virtual {v0}, [LZl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LZl;

    return-object v0
.end method
