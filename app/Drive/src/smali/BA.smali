.class public final LBA;
.super Ljava/lang/Object;
.source "DocListQuery.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/docs/doclist/DocListQuery;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/doclist/DocListQuery;
    .locals 2

    .prologue
    .line 79
    new-instance v0, Lcom/google/android/apps/docs/doclist/DocListQuery;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/docs/doclist/DocListQuery;-><init>(Landroid/os/Parcel;LBA;)V

    return-object v0
.end method

.method public a(I)[Lcom/google/android/apps/docs/doclist/DocListQuery;
    .locals 1

    .prologue
    .line 84
    new-array v0, p1, [Lcom/google/android/apps/docs/doclist/DocListQuery;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 76
    invoke-virtual {p0, p1}, LBA;->a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/doclist/DocListQuery;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 76
    invoke-virtual {p0, p1}, LBA;->a(I)[Lcom/google/android/apps/docs/doclist/DocListQuery;

    move-result-object v0

    return-object v0
.end method
