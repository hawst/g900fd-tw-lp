.class public Lzz;
.super Ljava/lang/Object;
.source "PreviewCardUpdater.java"


# instance fields
.field private final a:Lapd;

.field private final a:LaqQ;

.field private final a:Lzq;


# direct methods
.method constructor <init>(Lzq;Lapd;LaqQ;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lzq;

    iput-object v0, p0, Lzz;->a:Lzq;

    .line 43
    iput-object p3, p0, Lzz;->a:LaqQ;

    .line 44
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapd;

    iput-object v0, p0, Lzz;->a:Lapd;

    .line 45
    return-void
.end method

.method static synthetic a(IILakD;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 28
    invoke-static {p0, p1, p2}, Lzz;->b(IILakD;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lzz;)Lapd;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lzz;->a:Lapd;

    return-object v0
.end method

.method static synthetic a(Lzz;)LaqQ;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lzz;->a:LaqQ;

    return-object v0
.end method

.method private a(LaGu;)Lzx;
    .locals 1

    .prologue
    .line 100
    new-instance v0, LzC;

    invoke-direct {v0, p0, p1}, LzC;-><init>(Lzz;LaGu;)V

    return-object v0
.end method

.method private a(LaGu;)Lzy;
    .locals 1

    .prologue
    .line 60
    new-instance v0, LzA;

    invoke-direct {v0, p0, p1}, LzA;-><init>(Lzz;LaGu;)V

    return-object v0
.end method

.method private static b(IILakD;)Landroid/graphics/Bitmap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "LakD",
            "<",
            "Lcom/google/android/apps/docs/utils/RawPixelData;",
            ">;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 113
    :try_start_0
    invoke-virtual {p2}, LakD;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/utils/RawPixelData;

    .line 114
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p0, p1, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 115
    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/utils/RawPixelData;->b(Landroid/graphics/Bitmap;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    if-eqz p2, :cond_0

    .line 122
    invoke-virtual {p2}, LakD;->close()V

    :cond_0
    move-object v0, v1

    :cond_1
    :goto_0
    return-object v0

    .line 117
    :catch_0
    move-exception v0

    .line 119
    const/4 v0, 0x0

    .line 121
    if-eqz p2, :cond_1

    .line 122
    invoke-virtual {p2}, LakD;->close()V

    goto :goto_0

    .line 121
    :catchall_0
    move-exception v0

    if-eqz p2, :cond_2

    .line 122
    invoke-virtual {p2}, LakD;->close()V

    :cond_2
    throw v0
.end method


# virtual methods
.method public a(LaGu;)V
    .locals 7

    .prologue
    .line 48
    iget-object v0, p0, Lzz;->a:Lzq;

    invoke-interface {p1}, LaGu;->a()LaGv;

    move-result-object v1

    .line 49
    invoke-interface {p1}, LaGu;->g()Ljava/lang/String;

    move-result-object v2

    .line 50
    invoke-interface {p1}, LaGu;->d()Z

    move-result v3

    .line 51
    invoke-interface {p1}, LaGu;->c()Ljava/lang/String;

    move-result-object v4

    .line 52
    invoke-direct {p0, p1}, Lzz;->a(LaGu;)Lzy;

    move-result-object v5

    .line 53
    invoke-direct {p0, p1}, Lzz;->a(LaGu;)Lzx;

    move-result-object v6

    .line 48
    invoke-virtual/range {v0 .. v6}, Lzq;->a(LaGv;Ljava/lang/String;ZLjava/lang/String;Lzy;Lzx;)V

    .line 54
    return-void
.end method
