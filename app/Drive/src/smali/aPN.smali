.class public abstract LaPN;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/lang/Runnable;

.field private volatile a:Ljava/lang/Thread;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, LaPO;

    invoke-direct {v0, p0}, LaPO;-><init>(LaPN;)V

    iput-object v0, p0, LaPN;->a:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(LaPN;Ljava/lang/Thread;)Ljava/lang/Thread;
    .locals 0

    iput-object p1, p0, LaPN;->a:Ljava/lang/Thread;

    return-object p1
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract b()V
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, LaPN;->a:Ljava/lang/Runnable;

    invoke-static {v0}, LaPP;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final e()V
    .locals 1

    invoke-virtual {p0}, LaPN;->b()V

    iget-object v0, p0, LaPN;->a:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaPN;->a:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_0
    return-void
.end method
