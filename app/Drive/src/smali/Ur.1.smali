.class public LUr;
.super Ljava/lang/Object;
.source "TrackingMetadataChanger.java"

# interfaces
.implements LUi;


# instance fields
.field private final a:LUi;

.field private final a:LqK;


# direct methods
.method public constructor <init>(LUi;LqK;)V
    .locals 0
    .param p1    # LUi;
        .annotation runtime LUk;
        .end annotation
    .end param

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, LUr;->a:LUi;

    .line 23
    iput-object p2, p0, LUr;->a:LqK;

    .line 24
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, LUr;->a:LUi;

    invoke-interface {v0, p1}, LUi;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 43
    return-void
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaHy;)V
    .locals 3

    .prologue
    .line 68
    iget-object v0, p0, LUr;->a:LUi;

    invoke-interface {v0, p1, p2}, LUi;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaHy;)V

    .line 69
    iget-object v0, p0, LUr;->a:LqK;

    const-string v1, "doclist"

    const-string v2, "untrashEvent"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    return-void
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;LbmY;LbmY;LaHy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;",
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;",
            "LaHy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, LUr;->a:LUi;

    invoke-interface {v0, p1, p2, p3, p4}, LUi;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LbmY;LbmY;LaHy;)V

    .line 57
    return-void
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;LaHy;)V
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, LUr;->a:LUi;

    invoke-interface {v0, p1, p2, p3}, LUi;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;LaHy;)V

    .line 63
    iget-object v0, p0, LUr;->a:LqK;

    const-string v1, "doclist"

    const-string v2, "removeEvent"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    return-void
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;LaHy;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LUr;->a:LUi;

    invoke-interface {v0, p1, p2, p3, p4}, LUi;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;LaHy;)V

    .line 50
    return-void
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;Ljava/lang/String;LaHy;)V
    .locals 3

    .prologue
    .line 28
    iget-object v0, p0, LUr;->a:LUi;

    invoke-interface {v0, p1, p2, p3}, LUi;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;Ljava/lang/String;LaHy;)V

    .line 29
    iget-object v0, p0, LUr;->a:LqK;

    const-string v1, "doclist"

    const-string v2, "updateTitleEvent"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    return-void
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;ZLaHy;)V
    .locals 3

    .prologue
    .line 35
    iget-object v0, p0, LUr;->a:LUi;

    invoke-interface {v0, p1, p2, p3}, LUi;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;ZLaHy;)V

    .line 36
    iget-object v1, p0, LUr;->a:LqK;

    const-string v2, "doclist"

    if-eqz p2, :cond_0

    const-string v0, "starEvent"

    :goto_0
    invoke-virtual {v1, v2, v0}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    return-void

    .line 36
    :cond_0
    const-string v0, "unstarEvent"

    goto :goto_0
.end method

.method public b(Lcom/google/android/gms/drive/database/data/EntrySpec;LaHy;)V
    .locals 3

    .prologue
    .line 74
    iget-object v0, p0, LUr;->a:LUi;

    invoke-interface {v0, p1, p2}, LUi;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;LaHy;)V

    .line 75
    iget-object v0, p0, LUr;->a:LqK;

    const-string v1, "doclist"

    const-string v2, "deleteEvent"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method public b(Lcom/google/android/gms/drive/database/data/EntrySpec;ZLaHy;)V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, LUr;->a:LUi;

    invoke-interface {v0, p1, p2, p3}, LUi;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;ZLaHy;)V

    .line 83
    return-void
.end method
