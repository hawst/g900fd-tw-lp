.class public LacJ;
.super Ljava/lang/Object;
.source "SimpleAlphaIndexer.java"

# interfaces
.implements Landroid/widget/SectionIndexer;


# instance fields
.field private a:[I

.field private a:[Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "LacK;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 47
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 48
    const/4 v1, 0x0

    move v2, v3

    .line 49
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 50
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacK;

    invoke-interface {v0}, LacK;->a()Ljava/lang/String;

    move-result-object v0

    .line 52
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v0, " "

    .line 53
    :goto_1
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 54
    :cond_0
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 52
    :cond_1
    const/4 v6, 0x1

    invoke-virtual {v0, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 59
    :cond_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, LacJ;->a:[Ljava/lang/String;

    .line 60
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, LacJ;->a:[I

    .line 61
    :goto_3
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 62
    iget-object v1, p0, LacJ;->a:[I

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v1, v3

    .line 61
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 64
    :cond_3
    return-void

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method


# virtual methods
.method public a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, LacJ;->a:[Ljava/lang/String;

    return-object v0
.end method

.method public getPositionForSection(I)I
    .locals 2

    .prologue
    .line 68
    if-gez p1, :cond_0

    .line 69
    const/4 v0, 0x0

    .line 73
    :goto_0
    return v0

    .line 70
    :cond_0
    iget-object v0, p0, LacJ;->a:[I

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 71
    iget-object v0, p0, LacJ;->a:[I

    iget-object v1, p0, LacJ;->a:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    goto :goto_0

    .line 73
    :cond_1
    iget-object v0, p0, LacJ;->a:[I

    aget v0, v0, p1

    goto :goto_0
.end method

.method public getSectionForPosition(I)I
    .locals 1

    .prologue
    .line 79
    if-gez p1, :cond_1

    const/4 v0, 0x0

    .line 80
    :goto_0
    if-gez v0, :cond_0

    add-int/lit8 v0, v0, 0x2

    neg-int v0, v0

    :cond_0
    return v0

    .line 79
    :cond_1
    iget-object v0, p0, LacJ;->a:[I

    invoke-static {v0, p1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v0

    goto :goto_0
.end method

.method public synthetic getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, LacJ;->a()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
