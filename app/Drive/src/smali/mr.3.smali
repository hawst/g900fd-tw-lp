.class public final Lmr;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f0d0009

.field public static final abc_action_bar_home_description_format:I = 0x7f0d000c

.field public static final abc_action_bar_home_subtitle_description_format:I = 0x7f0d000d

.field public static final abc_action_bar_up_description:I = 0x7f0d000a

.field public static final abc_action_menu_overflow_description:I = 0x7f0d000b

.field public static final abc_action_mode_done:I = 0x7f0d0008

.field public static final abc_activity_chooser_view_see_all:I = 0x7f0d0014

.field public static final abc_activitychooserview_choose_application:I = 0x7f0d0013

.field public static final abc_searchview_description_clear:I = 0x7f0d0010

.field public static final abc_searchview_description_query:I = 0x7f0d000f

.field public static final abc_searchview_description_search:I = 0x7f0d000e

.field public static final abc_searchview_description_submit:I = 0x7f0d0011

.field public static final abc_searchview_description_voice:I = 0x7f0d0012

.field public static final abc_shareactionprovider_share_with:I = 0x7f0d0016

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f0d0015

.field public static final about_dialog:I = 0x7f0d00ad

.field public static final account_item:I = 0x7f0d0005

.field public static final account_list:I = 0x7f0d0007

.field public static final acl_badge_description:I = 0x7f0d00ae

.field public static final action_add_people:I = 0x7f0d031e

.field public static final action_add_to_drive:I = 0x7f0d031b

.field public static final action_card_download:I = 0x7f0d00af

.field public static final action_card_download_content_desc:I = 0x7f0d00b0

.field public static final action_card_export:I = 0x7f0d00b1

.field public static final action_card_export_content_desc:I = 0x7f0d00b2

.field public static final action_card_get_link:I = 0x7f0d00b3

.field public static final action_card_get_link_content_desc:I = 0x7f0d00b4

.field public static final action_card_move:I = 0x7f0d00b5

.field public static final action_card_move_content_desc:I = 0x7f0d00b6

.field public static final action_card_print:I = 0x7f0d00b7

.field public static final action_card_print_content_desc:I = 0x7f0d00b8

.field public static final action_card_remove:I = 0x7f0d00b9

.field public static final action_card_remove_content_desc:I = 0x7f0d00ba

.field public static final action_card_rename:I = 0x7f0d00bb

.field public static final action_card_rename_content_desc:I = 0x7f0d00bc

.field public static final action_card_share:I = 0x7f0d00bd

.field public static final action_card_share_content_desc:I = 0x7f0d00be

.field public static final action_card_share_link:I = 0x7f0d00bf

.field public static final action_card_share_link_content_desc:I = 0x7f0d00c0

.field public static final action_card_star:I = 0x7f0d00c1

.field public static final action_card_star_content_desc:I = 0x7f0d00c2

.field public static final action_card_unstar:I = 0x7f0d00c3

.field public static final action_card_unstar_content_desc:I = 0x7f0d00c4

.field public static final action_details:I = 0x7f0d0318

.field public static final action_download:I = 0x7f0d031c

.field public static final action_edit:I = 0x7f0d0319

.field public static final action_find:I = 0x7f0d0321

.field public static final action_print:I = 0x7f0d0320

.field public static final action_send:I = 0x7f0d031f

.field public static final action_share:I = 0x7f0d031a

.field public static final action_share_link:I = 0x7f0d031d

.field public static final add_account:I = 0x7f0d0000

.field public static final add_collaborator_accept:I = 0x7f0d0310

.field public static final add_collaborators:I = 0x7f0d00c5

.field public static final add_collaborators_default:I = 0x7f0d0311

.field public static final add_collaborators_description:I = 0x7f0d030f

.field public static final add_collaborators_hint:I = 0x7f0d00c7

.field public static final add_collaborators_name:I = 0x7f0d00c6

.field public static final add_collaborators_permission:I = 0x7f0d00c8

.field public static final alphabet_set:I = 0x7f0d00c9

.field public static final announce_account_switch:I = 0x7f0d00ca

.field public static final announce_create_new:I = 0x7f0d00cb

.field public static final announce_path_navigation:I = 0x7f0d00cc

.field public static final app_installation_in_progress:I = 0x7f0d00cd

.field public static final app_installation_in_progress_toast:I = 0x7f0d00ce

.field public static final app_installed_dialog_drive_installed_title:I = 0x7f0d00cf

.field public static final app_installed_dialog_kix_editor_installed_title:I = 0x7f0d00d0

.field public static final app_installed_dialog_message:I = 0x7f0d00d1

.field public static final app_installed_dialog_open_button:I = 0x7f0d00d2

.field public static final app_installed_dialog_punch_editor_installed_title:I = 0x7f0d00d3

.field public static final app_installed_dialog_sketchy_editor_installed_title:I = 0x7f0d00d4

.field public static final app_installed_dialog_trix_editor_installed_title:I = 0x7f0d00d5

.field public static final app_installed_toast_docs_and_sheets:I = 0x7f0d00d6

.field public static final app_name:I = 0x7f0d02fa

.field public static final app_name_drive:I = 0x7f0d00d7

.field public static final ask_confirmation_for_document_deletion:I = 0x7f0d00d8

.field public static final ask_confirmation_for_folder_deletion:I = 0x7f0d00d9

.field public static final authentication_error:I = 0x7f0d00da

.field public static final auto_backup_open_settings_page_failure_toast:I = 0x7f0d00db

.field public static final auto_backup_turn_on_failure_toast:I = 0x7f0d00dc

.field public static final button_create_contact:I = 0x7f0d00dd

.field public static final button_exit:I = 0x7f0d0347

.field public static final button_open:I = 0x7f0d0348

.field public static final button_retry:I = 0x7f0d00de

.field public static final button_save_sharing:I = 0x7f0d00df

.field public static final camera_ocr_blur_title:I = 0x7f0d007f

.field public static final camera_ocr_blur_warning:I = 0x7f0d0080

.field public static final camera_ocr_default_name:I = 0x7f0d0081

.field public static final camera_ocr_error_capture:I = 0x7f0d0082

.field public static final camera_ocr_evaluating_message:I = 0x7f0d0083

.field public static final camera_ocr_evaluating_skip_button:I = 0x7f0d0084

.field public static final camera_ocr_no_sdcard:I = 0x7f0d0085

.field public static final camera_ocr_warning_continue:I = 0x7f0d0086

.field public static final camera_ocr_warning_retake:I = 0x7f0d0087

.field public static final casting_to_device:I = 0x7f0d034e

.field public static final clear_cache:I = 0x7f0d00e0

.field public static final clear_cache_cleared_message:I = 0x7f0d00e1

.field public static final clear_cache_message:I = 0x7f0d00e2

.field public static final common_google_play_services_enable_button:I = 0x7f0d0028

.field public static final common_google_play_services_enable_text:I = 0x7f0d0027

.field public static final common_google_play_services_enable_title:I = 0x7f0d0026

.field public static final common_google_play_services_error_notification_requested_by_msg:I = 0x7f0d0021

.field public static final common_google_play_services_install_button:I = 0x7f0d0025

.field public static final common_google_play_services_install_text_phone:I = 0x7f0d0023

.field public static final common_google_play_services_install_text_tablet:I = 0x7f0d0024

.field public static final common_google_play_services_install_title:I = 0x7f0d0022

.field public static final common_google_play_services_invalid_account_text:I = 0x7f0d002e

.field public static final common_google_play_services_invalid_account_title:I = 0x7f0d002d

.field public static final common_google_play_services_needs_enabling_title:I = 0x7f0d0020

.field public static final common_google_play_services_network_error_text:I = 0x7f0d002c

.field public static final common_google_play_services_network_error_title:I = 0x7f0d002b

.field public static final common_google_play_services_notification_needs_installation_title:I = 0x7f0d001e

.field public static final common_google_play_services_notification_needs_update_title:I = 0x7f0d001f

.field public static final common_google_play_services_notification_ticker:I = 0x7f0d001d

.field public static final common_google_play_services_unknown_issue:I = 0x7f0d002f

.field public static final common_google_play_services_unsupported_date_text:I = 0x7f0d0032

.field public static final common_google_play_services_unsupported_text:I = 0x7f0d0031

.field public static final common_google_play_services_unsupported_title:I = 0x7f0d0030

.field public static final common_google_play_services_update_button:I = 0x7f0d0033

.field public static final common_google_play_services_update_text:I = 0x7f0d002a

.field public static final common_google_play_services_update_title:I = 0x7f0d0029

.field public static final common_signin_button_text:I = 0x7f0d0034

.field public static final common_signin_button_text_long:I = 0x7f0d0035

.field public static final contact_sharing_commenter:I = 0x7f0d00e3

.field public static final contact_sharing_no_access:I = 0x7f0d00e4

.field public static final contact_sharing_reader:I = 0x7f0d00e5

.field public static final contact_sharing_writer:I = 0x7f0d00e6

.field public static final create_new:I = 0x7f0d00e7

.field public static final create_new_button_content_description:I = 0x7f0d00e8

.field public static final create_new_choice_document:I = 0x7f0d00e9

.field public static final create_new_choice_folder:I = 0x7f0d00ea

.field public static final create_new_choice_scan:I = 0x7f0d00eb

.field public static final create_new_choice_sheet:I = 0x7f0d00ec

.field public static final create_new_choice_slide:I = 0x7f0d00ed

.field public static final create_new_choice_title:I = 0x7f0d00ee

.field public static final create_new_choice_upload:I = 0x7f0d00ef

.field public static final create_new_doc_title:I = 0x7f0d0088

.field public static final create_new_drawing_doc:I = 0x7f0d00f0

.field public static final create_new_error_document:I = 0x7f0d00f1

.field public static final create_new_error_drawing:I = 0x7f0d00f2

.field public static final create_new_error_folder:I = 0x7f0d00f3

.field public static final create_new_error_presentation:I = 0x7f0d00f4

.field public static final create_new_error_spreadsheet:I = 0x7f0d00f5

.field public static final create_new_folder:I = 0x7f0d00f6

.field public static final create_new_from_camera:I = 0x7f0d0089

.field public static final create_new_from_scan:I = 0x7f0d00f7

.field public static final create_new_kix_doc:I = 0x7f0d00f8

.field public static final create_new_ocr_doc:I = 0x7f0d0358

.field public static final create_new_punch_doc:I = 0x7f0d00f9

.field public static final create_new_trix_doc:I = 0x7f0d00fa

.field public static final create_new_warning_folder_permission_missing:I = 0x7f0d00fb

.field public static final create_shortcut_label:I = 0x7f0d0359

.field public static final create_shortcut_title:I = 0x7f0d035a

.field public static final creating_document:I = 0x7f0d00fc

.field public static final creating_drawing:I = 0x7f0d00fd

.field public static final creating_folder:I = 0x7f0d00fe

.field public static final creating_presentation:I = 0x7f0d00ff

.field public static final creating_spreadsheet:I = 0x7f0d0100

.field public static final cross_app_promo_dismiss_button_text:I = 0x7f0d0101

.field public static final cross_app_promo_install_button_text:I = 0x7f0d0102

.field public static final cross_app_promo_view_only_button_text:I = 0x7f0d0103

.field public static final decrypting_progress_message:I = 0x7f0d0104

.field public static final default_new_drawing_title:I = 0x7f0d0105

.field public static final default_new_folder_title:I = 0x7f0d0106

.field public static final default_new_kix_title:I = 0x7f0d0107

.field public static final default_new_punch_title:I = 0x7f0d0108

.field public static final default_new_trix_title:I = 0x7f0d0109

.field public static final default_scope_contact_name:I = 0x7f0d010a

.field public static final desc_file_type:I = 0x7f0d032f

.field public static final desc_find_next:I = 0x7f0d0324

.field public static final desc_find_prev:I = 0x7f0d0323

.field public static final desc_image_preview:I = 0x7f0d0340

.field public static final desc_page:I = 0x7f0d0341

.field public static final desc_page_range:I = 0x7f0d032c

.field public static final desc_page_single:I = 0x7f0d032a

.field public static final desc_password:I = 0x7f0d0349

.field public static final desc_password_incorrect:I = 0x7f0d034a

.field public static final desc_password_incorrect_message:I = 0x7f0d034b

.field public static final desc_selection_start:I = 0x7f0d0342

.field public static final desc_selection_stop:I = 0x7f0d0343

.field public static final desc_zoom:I = 0x7f0d032e

.field public static final description_icon_stale_items:I = 0x7f0d010b

.field public static final detail_fragment_general_info_modified:I = 0x7f0d010c

.field public static final detail_fragment_general_info_modified_by_me:I = 0x7f0d010d

.field public static final detail_fragment_general_info_opened_by_me:I = 0x7f0d010e

.field public static final detail_fragment_general_info_quota_used:I = 0x7f0d010f

.field public static final detail_fragment_general_info_title:I = 0x7f0d0110

.field public static final detail_fragment_item_in_trash:I = 0x7f0d0111

.field public static final detail_fragment_offline_title:I = 0x7f0d0112

.field public static final detail_fragment_sharing_title:I = 0x7f0d0113

.field public static final detail_fragment_title:I = 0x7f0d0114

.field public static final detail_preview_created_time:I = 0x7f0d0115

.field public static final detail_preview_date_performed_by:I = 0x7f0d0116

.field public static final detail_preview_folder_location:I = 0x7f0d0117

.field public static final detail_preview_kind:I = 0x7f0d0118

.field public static final detail_preview_last_modified_date:I = 0x7f0d0119

.field public static final detail_preview_last_opened_date:I = 0x7f0d011a

.field public static final detail_preview_type_with_size:I = 0x7f0d011b

.field public static final dialog_confirm_sharing:I = 0x7f0d011c

.field public static final dialog_confirm_sharing_message:I = 0x7f0d011d

.field public static final dialog_confirm_sharing_message_multiple:I = 0x7f0d011e

.field public static final dialog_confirm_start_presenting:I = 0x7f0d008a

.field public static final dialog_confirm_stop_presenting:I = 0x7f0d008b

.field public static final dialog_contact_sharing:I = 0x7f0d011f

.field public static final dialog_select:I = 0x7f0d0120

.field public static final dialog_select_folder:I = 0x7f0d0121

.field public static final dialog_sharing_options:I = 0x7f0d0122

.field public static final doclist_accessibility_changed_to_grid:I = 0x7f0d0123

.field public static final doclist_accessibility_changed_to_list:I = 0x7f0d0124

.field public static final doclist_close_detail_drawer_content_description:I = 0x7f0d0125

.field public static final doclist_close_navigation_drawer_content_description:I = 0x7f0d0126

.field public static final doclist_date_created_label:I = 0x7f0d0127

.field public static final doclist_date_edited_label:I = 0x7f0d0128

.field public static final doclist_date_modified_label:I = 0x7f0d0129

.field public static final doclist_date_never_label:I = 0x7f0d012a

.field public static final doclist_date_opened_label:I = 0x7f0d012b

.field public static final doclist_date_shared_label:I = 0x7f0d012c

.field public static final doclist_detail_fragment_close_button_content_description:I = 0x7f0d012d

.field public static final doclist_detail_fragment_content_description:I = 0x7f0d012e

.field public static final doclist_open_navigation_drawer_content_description:I = 0x7f0d012f

.field public static final doclist_quota_used_label:I = 0x7f0d0130

.field public static final doclist_sortby_creation_time:I = 0x7f0d0131

.field public static final doclist_sortby_edited_title:I = 0x7f0d0132

.field public static final doclist_sortby_modified_title:I = 0x7f0d0133

.field public static final doclist_sortby_opened_title:I = 0x7f0d0134

.field public static final doclist_sortby_quota_used:I = 0x7f0d0135

.field public static final doclist_sortby_shared_title:I = 0x7f0d0136

.field public static final doclist_star_cb_content_description:I = 0x7f0d0137

.field public static final doclist_starred_state:I = 0x7f0d0138

.field public static final doclist_unstarred_state:I = 0x7f0d0139

.field public static final document_preparing_to_open_progress:I = 0x7f0d013a

.field public static final document_preparing_to_send_progress:I = 0x7f0d013b

.field public static final document_preview_page_description:I = 0x7f0d013c

.field public static final document_preview_page_error_description:I = 0x7f0d013d

.field public static final document_type_audio:I = 0x7f0d013e

.field public static final document_type_file:I = 0x7f0d013f

.field public static final document_type_folder:I = 0x7f0d0140

.field public static final document_type_google_document:I = 0x7f0d0141

.field public static final document_type_google_drawing:I = 0x7f0d0142

.field public static final document_type_google_form:I = 0x7f0d0143

.field public static final document_type_google_presentation:I = 0x7f0d0144

.field public static final document_type_google_site:I = 0x7f0d0145

.field public static final document_type_google_spreadsheet:I = 0x7f0d0146

.field public static final document_type_google_table:I = 0x7f0d0147

.field public static final document_type_ms_excel:I = 0x7f0d0148

.field public static final document_type_ms_powerpoint:I = 0x7f0d0149

.field public static final document_type_ms_word:I = 0x7f0d014a

.field public static final document_type_pdf:I = 0x7f0d014b

.field public static final document_type_picture:I = 0x7f0d014c

.field public static final document_type_unknown:I = 0x7f0d014d

.field public static final document_type_video:I = 0x7f0d014e

.field public static final document_type_zip_archive:I = 0x7f0d014f

.field public static final domain_scope_contact_name:I = 0x7f0d0150

.field public static final download_bytes_format:I = 0x7f0d0151

.field public static final download_bytes_format_short:I = 0x7f0d0152

.field public static final download_from_drive_complete:I = 0x7f0d0153

.field public static final download_from_drive_failed:I = 0x7f0d0154

.field public static final download_kilobytes_format:I = 0x7f0d0155

.field public static final download_kilobytes_format_short:I = 0x7f0d0156

.field public static final download_megabytes_format:I = 0x7f0d0157

.field public static final download_megabytes_format_short:I = 0x7f0d0158

.field public static final download_pause:I = 0x7f0d0159

.field public static final download_progress_dialog_message:I = 0x7f0d015a

.field public static final download_remove:I = 0x7f0d015b

.field public static final download_resume:I = 0x7f0d015c

.field public static final downloaded_from_drive:I = 0x7f0d015d

.field public static final ds_app_name:I = 0x7f0d003c

.field public static final ds_click_to_rename:I = 0x7f0d003d

.field public static final ds_dialog_cancel_button_text:I = 0x7f0d004f

.field public static final ds_dialog_exit_button_text:I = 0x7f0d0054

.field public static final ds_dialog_msg_cancel_scan:I = 0x7f0d004d

.field public static final ds_dialog_msg_delete_page:I = 0x7f0d004b

.field public static final ds_dialog_msg_pdf_library_load_failed:I = 0x7f0d0053

.field public static final ds_dialog_msg_storage_unavailable:I = 0x7f0d0051

.field public static final ds_dialog_ok_button_text:I = 0x7f0d004e

.field public static final ds_dialog_title_cancel_scan:I = 0x7f0d004c

.field public static final ds_dialog_title_pdf_library_load_failed:I = 0x7f0d0052

.field public static final ds_dialog_title_storage_unavailable:I = 0x7f0d0050

.field public static final ds_disable_torch:I = 0x7f0d005d

.field public static final ds_display_real_time_quads_default:I = 0x7f0d006a

.field public static final ds_display_real_time_quads_key:I = 0x7f0d0038

.field public static final ds_document_editor_msg_no_pages:I = 0x7f0d0055

.field public static final ds_document_editor_view_content_description:I = 0x7f0d0068

.field public static final ds_edit_control_add_page:I = 0x7f0d0044

.field public static final ds_edit_control_confirm_crop:I = 0x7f0d0045

.field public static final ds_edit_control_finish_document:I = 0x7f0d0047

.field public static final ds_edit_control_retake_page:I = 0x7f0d0046

.field public static final ds_enable_torch:I = 0x7f0d005c

.field public static final ds_enhance_context_menu_title:I = 0x7f0d004a

.field public static final ds_image_enhancement_method_default:I = 0x7f0d0069

.field public static final ds_image_enhancement_method_key:I = 0x7f0d0037

.field public static final ds_image_enhancement_method_summary:I = 0x7f0d0061

.field public static final ds_image_enhancement_method_title:I = 0x7f0d0060

.field public static final ds_jpeg_quality_default:I = 0x7f0d006e

.field public static final ds_jpeg_quality_key:I = 0x7f0d003b

.field public static final ds_jpeg_quality_summary:I = 0x7f0d0067

.field public static final ds_jpeg_quality_title:I = 0x7f0d0066

.field public static final ds_make_shutter_sound_title:I = 0x7f0d005f

.field public static final ds_menu_crop:I = 0x7f0d003e

.field public static final ds_menu_delete_page:I = 0x7f0d0058

.field public static final ds_menu_edit_page:I = 0x7f0d0057

.field public static final ds_menu_enhance:I = 0x7f0d003f

.field public static final ds_menu_enhance_bw:I = 0x7f0d0041

.field public static final ds_menu_enhance_color:I = 0x7f0d0042

.field public static final ds_menu_enhance_color_drawing:I = 0x7f0d0043

.field public static final ds_menu_enhance_none:I = 0x7f0d0040

.field public static final ds_menu_settings:I = 0x7f0d0056

.field public static final ds_pdf_paper_orientation_default:I = 0x7f0d006d

.field public static final ds_pdf_paper_orientation_key:I = 0x7f0d003a

.field public static final ds_pdf_paper_orientation_summary:I = 0x7f0d0065

.field public static final ds_pdf_paper_orientation_title:I = 0x7f0d0064

.field public static final ds_pdf_paper_size_default:I = 0x7f0d006b

.field public static final ds_pdf_paper_size_default_non_us:I = 0x7f0d006c

.field public static final ds_pdf_paper_size_key:I = 0x7f0d0039

.field public static final ds_pdf_paper_size_summary:I = 0x7f0d0063

.field public static final ds_pdf_paper_size_title:I = 0x7f0d0062

.field public static final ds_preferences_screen_title:I = 0x7f0d005e

.field public static final ds_rename_scan:I = 0x7f0d0048

.field public static final ds_scanned_file_name:I = 0x7f0d0049

.field public static final ds_shutter:I = 0x7f0d005b

.field public static final ds_title_activity_capture:I = 0x7f0d0059

.field public static final ds_title_activity_editor:I = 0x7f0d005a

.field public static final empty_doclist:I = 0x7f0d015e

.field public static final empty_doclist_for_google_photos_view:I = 0x7f0d015f

.field public static final empty_doclist_for_google_photos_view_details:I = 0x7f0d0160

.field public static final empty_doclist_for_incoming_view:I = 0x7f0d0161

.field public static final empty_doclist_for_my_drive_view:I = 0x7f0d0162

.field public static final empty_doclist_for_my_drive_view_details:I = 0x7f0d0163

.field public static final empty_doclist_for_pinned_view:I = 0x7f0d0164

.field public static final empty_doclist_for_pinned_view_details:I = 0x7f0d0165

.field public static final empty_doclist_for_recent_view:I = 0x7f0d0166

.field public static final empty_doclist_for_starred_view:I = 0x7f0d0167

.field public static final empty_doclist_for_starred_view_details:I = 0x7f0d0168

.field public static final empty_doclist_for_uploads_view:I = 0x7f0d0169

.field public static final empty_doclist_for_uploads_view_details:I = 0x7f0d016a

.field public static final empty_recent_doclist_message_subtitle:I = 0x7f0d02fc

.field public static final empty_recent_doclist_message_title:I = 0x7f0d02fb

.field public static final empty_sharing_list:I = 0x7f0d016b

.field public static final enter_new_name:I = 0x7f0d016c

.field public static final error_access_denied_html:I = 0x7f0d016d

.field public static final error_document_not_available:I = 0x7f0d016e

.field public static final error_failed_to_create_account:I = 0x7f0d016f

.field public static final error_fetch_more_retry:I = 0x7f0d0170

.field public static final error_fetch_more_retry_in_file_picker:I = 0x7f0d0171

.field public static final error_file_format_pdf:I = 0x7f0d034f

.field public static final error_internal_error_html:I = 0x7f0d0172

.field public static final error_network_error_html:I = 0x7f0d0173

.field public static final error_no_viewer_available:I = 0x7f0d0174

.field public static final error_open:I = 0x7f0d034d

.field public static final error_opening_document:I = 0x7f0d0175

.field public static final error_opening_document_for_html:I = 0x7f0d0176

.field public static final error_page_title:I = 0x7f0d0177

.field public static final error_print_failed:I = 0x7f0d0178

.field public static final error_print_too_large:I = 0x7f0d034c

.field public static final error_ssl_generic_template:I = 0x7f0d0179

.field public static final error_ssl_idmismatch_template:I = 0x7f0d017a

.field public static final error_ssl_validity_template:I = 0x7f0d017b

.field public static final error_video_not_available:I = 0x7f0d017c

.field public static final exporting_document:I = 0x7f0d017d

.field public static final fast_scroll_time_grouper_earlier:I = 0x7f0d017e

.field public static final fast_scroll_time_grouper_last_month:I = 0x7f0d017f

.field public static final fast_scroll_time_grouper_last_week:I = 0x7f0d0180

.field public static final fast_scroll_time_grouper_last_year:I = 0x7f0d0181

.field public static final fast_scroll_time_grouper_this_month:I = 0x7f0d0182

.field public static final fast_scroll_time_grouper_this_week:I = 0x7f0d0183

.field public static final fast_scroll_time_grouper_this_year:I = 0x7f0d0184

.field public static final fast_scroll_time_grouper_today:I = 0x7f0d0185

.field public static final fast_scroll_time_grouper_yesterday:I = 0x7f0d0186

.field public static final fast_scroll_title_grouper_collections:I = 0x7f0d0187

.field public static final file_too_large_for_upload:I = 0x7f0d0188

.field public static final file_too_large_for_upload_okbtn:I = 0x7f0d0189

.field public static final file_too_large_for_upload_title:I = 0x7f0d018a

.field public static final file_type_archive:I = 0x7f0d0330

.field public static final file_type_audio:I = 0x7f0d0331

.field public static final file_type_docs:I = 0x7f0d0333

.field public static final file_type_drawings:I = 0x7f0d0332

.field public static final file_type_excel:I = 0x7f0d0334

.field public static final file_type_forms:I = 0x7f0d0335

.field public static final file_type_html:I = 0x7f0d0336

.field public static final file_type_image:I = 0x7f0d0337

.field public static final file_type_pdf:I = 0x7f0d0338

.field public static final file_type_powerpoint:I = 0x7f0d0339

.field public static final file_type_sheets:I = 0x7f0d033a

.field public static final file_type_slides:I = 0x7f0d033b

.field public static final file_type_text:I = 0x7f0d033c

.field public static final file_type_unknown:I = 0x7f0d033f

.field public static final file_type_video:I = 0x7f0d033d

.field public static final file_type_word:I = 0x7f0d033e

.field public static final gallery_select_error:I = 0x7f0d008c

.field public static final getting_authentication_information:I = 0x7f0d018b

.field public static final gf_feedback:I = 0x7f0d018c

.field public static final gf_optional_description:I = 0x7f0d018d

.field public static final google_account_missing:I = 0x7f0d018e

.field public static final google_account_needed:I = 0x7f0d018f

.field public static final grid_sync_download_label_format:I = 0x7f0d0190

.field public static final grid_sync_upload_label_format:I = 0x7f0d0191

.field public static final help_card_button_label_got_it:I = 0x7f0d0192

.field public static final help_card_button_label_got_it_exclamation:I = 0x7f0d008d

.field public static final help_card_button_label_no_thanks:I = 0x7f0d0193

.field public static final help_card_button_label_not_now:I = 0x7f0d0194

.field public static final help_card_drive_welcome_content_message:I = 0x7f0d008e

.field public static final help_card_drive_welcome_content_title:I = 0x7f0d008f

.field public static final help_card_drive_welcome_positive_button_label:I = 0x7f0d0090

.field public static final help_card_photo_backup_suggest_content_learn_more:I = 0x7f0d0073

.field public static final help_card_photo_backup_suggest_content_message:I = 0x7f0d0074

.field public static final help_card_photo_backup_suggest_content_title:I = 0x7f0d0075

.field public static final help_card_photo_backup_suggest_positive_button_label:I = 0x7f0d0076

.field public static final help_card_scan_content_message:I = 0x7f0d0091

.field public static final help_card_scan_content_title:I = 0x7f0d0092

.field public static final help_card_scan_positive_button_label:I = 0x7f0d0093

.field public static final hide_account_list:I = 0x7f0d0004

.field public static final hint_find:I = 0x7f0d0322

.field public static final home_starred:I = 0x7f0d035b

.field public static final incoming_open:I = 0x7f0d0195

.field public static final incoming_tab_label_from_google_photos:I = 0x7f0d0196

.field public static final incoming_tab_label_from_people:I = 0x7f0d0197

.field public static final internal_release_dialog_line_1:I = 0x7f0d030e

.field public static final internal_release_dialog_line_2:I = 0x7f0d0198

.field public static final internal_release_dialog_ok:I = 0x7f0d0199

.field public static final internal_release_dialog_title:I = 0x7f0d019a

.field public static final label_page_range:I = 0x7f0d032b

.field public static final label_page_single:I = 0x7f0d0329

.field public static final label_password_first:I = 0x7f0d0345

.field public static final label_password_incorrect:I = 0x7f0d0346

.field public static final label_zoom:I = 0x7f0d032d

.field public static final launch_drive:I = 0x7f0d019b

.field public static final loading:I = 0x7f0d019c

.field public static final loading_printers:I = 0x7f0d019d

.field public static final logo_title:I = 0x7f0d02fd

.field public static final m_font_family_title:I = 0x7f0d0072

.field public static final manage_accounts:I = 0x7f0d0001

.field public static final manifest_app_pdfviewer:I = 0x7f0d0315

.field public static final manifest_app_projector:I = 0x7f0d0314

.field public static final manifest_intent_view_pdf:I = 0x7f0d0316

.field public static final manifest_intent_view_pdf_drive:I = 0x7f0d0317

.field public static final menu_account_settings:I = 0x7f0d019e

.field public static final menu_create_new_doc:I = 0x7f0d019f

.field public static final menu_create_new_from_upload:I = 0x7f0d01a0

.field public static final menu_create_shortcut:I = 0x7f0d01a1

.field public static final menu_delete:I = 0x7f0d01a2

.field public static final menu_doc_view_grid_mode:I = 0x7f0d01a3

.field public static final menu_doc_view_list_mode:I = 0x7f0d01a4

.field public static final menu_download:I = 0x7f0d01a5

.field public static final menu_download_qo_format:I = 0x7f0d02fe

.field public static final menu_dump_database:I = 0x7f0d01a6

.field public static final menu_edit:I = 0x7f0d01a7

.field public static final menu_filter_by:I = 0x7f0d01a8

.field public static final menu_full_screen:I = 0x7f0d01a9

.field public static final menu_help:I = 0x7f0d01aa

.field public static final menu_help_and_feedback:I = 0x7f0d01ab

.field public static final menu_incoming:I = 0x7f0d01ac

.field public static final menu_move_to:I = 0x7f0d01ad

.field public static final menu_my_drive:I = 0x7f0d01ae

.field public static final menu_offline:I = 0x7f0d01af

.field public static final menu_open_with:I = 0x7f0d01b0

.field public static final menu_overflow_content_description:I = 0x7f0d01b1

.field public static final menu_print:I = 0x7f0d01b2

.field public static final menu_quick_hints:I = 0x7f0d01b3

.field public static final menu_refresh:I = 0x7f0d01b4

.field public static final menu_rename:I = 0x7f0d01b5

.field public static final menu_search:I = 0x7f0d01b6

.field public static final menu_search_cancel:I = 0x7f0d01b7

.field public static final menu_search_clear:I = 0x7f0d01b8

.field public static final menu_selection_all:I = 0x7f0d01b9

.field public static final menu_selection_clear:I = 0x7f0d01ba

.field public static final menu_selection_start:I = 0x7f0d01bb

.field public static final menu_send:I = 0x7f0d01bc

.field public static final menu_send_link:I = 0x7f0d01bd

.field public static final menu_settings:I = 0x7f0d01be

.field public static final menu_sharing:I = 0x7f0d01bf

.field public static final menu_show_detail:I = 0x7f0d01c0

.field public static final menu_show_drawing:I = 0x7f0d01c1

.field public static final menu_show_folder:I = 0x7f0d01c2

.field public static final menu_show_kix:I = 0x7f0d01c3

.field public static final menu_show_live_editing:I = 0x7f0d01c4

.field public static final menu_show_movie:I = 0x7f0d01c5

.field public static final menu_show_owned_by_me:I = 0x7f0d01c6

.field public static final menu_show_pdf:I = 0x7f0d01c7

.field public static final menu_show_picture:I = 0x7f0d01c8

.field public static final menu_show_pinned:I = 0x7f0d01c9

.field public static final menu_show_punch:I = 0x7f0d01ca

.field public static final menu_show_recent:I = 0x7f0d01cb

.field public static final menu_show_starred:I = 0x7f0d01cc

.field public static final menu_show_trash:I = 0x7f0d01cd

.field public static final menu_show_trix:I = 0x7f0d01ce

.field public static final menu_show_upload:I = 0x7f0d01cf

.field public static final menu_sort_by:I = 0x7f0d01d0

.field public static final menu_sort_creation_time:I = 0x7f0d01d1

.field public static final menu_sort_last_modified:I = 0x7f0d01d2

.field public static final menu_sort_quota_used:I = 0x7f0d01d3

.field public static final menu_sort_recently_edited:I = 0x7f0d01d4

.field public static final menu_sort_recently_opened:I = 0x7f0d01d5

.field public static final menu_sort_title:I = 0x7f0d01d6

.field public static final menu_switch_account:I = 0x7f0d01d7

.field public static final menu_trash_delete_forever:I = 0x7f0d01d8

.field public static final menu_trash_restore:I = 0x7f0d01d9

.field public static final menu_unpin:I = 0x7f0d01da

.field public static final message_no_matches_found:I = 0x7f0d0325

.field public static final move:I = 0x7f0d01db

.field public static final move_confirm_dialog_title:I = 0x7f0d01dc

.field public static final move_confirm_dialog_title_unshare:I = 0x7f0d01dd

.field public static final move_dialog_title:I = 0x7f0d01de

.field public static final move_multi_parent_file:I = 0x7f0d01df

.field public static final move_multi_parent_folder:I = 0x7f0d01e0

.field public static final move_shared_to_shared:I = 0x7f0d01e1

.field public static final move_shared_to_unshared:I = 0x7f0d01e2

.field public static final move_toast_no_source_folder:I = 0x7f0d01e3

.field public static final move_toast_with_source_folder:I = 0x7f0d01e4

.field public static final move_unshared_to_shared:I = 0x7f0d01e5

.field public static final mr_media_route_button_content_description:I = 0x7f0d0019

.field public static final mr_media_route_chooser_searching:I = 0x7f0d001b

.field public static final mr_media_route_chooser_title:I = 0x7f0d001a

.field public static final mr_media_route_controller_disconnect:I = 0x7f0d001c

.field public static final mr_system_route_name:I = 0x7f0d0017

.field public static final mr_user_route_category_name:I = 0x7f0d0018

.field public static final nav_drawer_title:I = 0x7f0d01e6

.field public static final nav_panel_google_drive_button:I = 0x7f0d01e7

.field public static final nav_panel_google_drive_button_description:I = 0x7f0d01e8

.field public static final nav_panel_storage_percent:I = 0x7f0d01e9

.field public static final nav_panel_storage_summary:I = 0x7f0d01ea

.field public static final nav_panel_storage_summary_unlimited:I = 0x7f0d01eb

.field public static final navigation_all_items:I = 0x7f0d02ff

.field public static final navigation_filter:I = 0x7f0d0312

.field public static final navigation_search_results:I = 0x7f0d0313

.field public static final new_account:I = 0x7f0d01ec

.field public static final new_drawing_title:I = 0x7f0d01ed

.field public static final new_folder_title:I = 0x7f0d01ee

.field public static final new_kix_title:I = 0x7f0d01ef

.field public static final new_punch_title:I = 0x7f0d01f0

.field public static final new_trix_title:I = 0x7f0d01f1

.field public static final no_account_support_this_upload:I = 0x7f0d01f2

.field public static final notification_extra_text_is_too_long:I = 0x7f0d01f3

.field public static final ocr_image_description:I = 0x7f0d0094

.field public static final ocr_image_title:I = 0x7f0d0095

.field public static final open_document_error_item_cannot_be_exported:I = 0x7f0d01f4

.field public static final open_document_in_browser:I = 0x7f0d01f5

.field public static final open_pinned_version:I = 0x7f0d01f6

.field public static final open_url_authentication_error:I = 0x7f0d01f7

.field public static final open_url_error_access_denied:I = 0x7f0d01f8

.field public static final open_url_getting_entry:I = 0x7f0d0301

.field public static final open_url_io_error:I = 0x7f0d01f9

.field public static final open_with_dialog_title:I = 0x7f0d01fa

.field public static final open_with_native_drive_app_item_subtitle:I = 0x7f0d01fb

.field public static final open_with_picker_dialog_open_button:I = 0x7f0d01fc

.field public static final open_with_web_app_item_subtitle:I = 0x7f0d01fd

.field public static final opened_document:I = 0x7f0d01fe

.field public static final opening_document:I = 0x7f0d01ff

.field public static final opening_in_app:I = 0x7f0d0300

.field public static final operation_on_starring_failed:I = 0x7f0d0200

.field public static final operation_retry_error:I = 0x7f0d0201

.field public static final operation_retry_exceeded_message:I = 0x7f0d0202

.field public static final operation_retry_exceeded_retry:I = 0x7f0d0203

.field public static final operation_sync_error:I = 0x7f0d0204

.field public static final operation_sync_network_error:I = 0x7f0d0205

.field public static final ouch_authorize_database_dump:I = 0x7f0d0209

.field public static final ouch_button_close:I = 0x7f0d0206

.field public static final ouch_button_report:I = 0x7f0d0207

.field public static final ouch_msg_sync_error:I = 0x7f0d0302

.field public static final ouch_msg_unhandled_exception:I = 0x7f0d0303

.field public static final ouch_title_sawwrie:I = 0x7f0d0208

.field public static final owned_by_me:I = 0x7f0d020a

.field public static final page_1_description:I = 0x7f0d0350

.field public static final page_1_heading:I = 0x7f0d0351

.field public static final page_2_description:I = 0x7f0d0352

.field public static final page_2_heading:I = 0x7f0d0353

.field public static final page_3_description:I = 0x7f0d0354

.field public static final page_3_heading:I = 0x7f0d0355

.field public static final page_counter_format:I = 0x7f0d020b

.field public static final perm_sync_status:I = 0x7f0d020c

.field public static final perm_sync_status_desc:I = 0x7f0d0304

.field public static final permsearch:I = 0x7f0d020d

.field public static final permsearch_desc:I = 0x7f0d020e

.field public static final phonesky_connecting_to_play_store_message:I = 0x7f0d020f

.field public static final phonesky_no_internet_connection_alert_dialog_dismiss:I = 0x7f0d0210

.field public static final phonesky_no_internet_connection_alert_dialog_message:I = 0x7f0d0211

.field public static final phonesky_no_internet_connection_alert_dialog_title:I = 0x7f0d0212

.field public static final pick_entry_create_new_folder:I = 0x7f0d0213

.field public static final pick_entry_current_folder:I = 0x7f0d0214

.field public static final pick_entry_dialog_title:I = 0x7f0d0215

.field public static final pick_entry_dialog_title_location:I = 0x7f0d0216

.field public static final pick_entry_navigate_up:I = 0x7f0d0217

.field public static final pin_checkbox_label:I = 0x7f0d0218

.field public static final pin_downloading:I = 0x7f0d0219

.field public static final pin_encryption_continue:I = 0x7f0d021a

.field public static final pin_encryption_message:I = 0x7f0d021b

.field public static final pin_encryption_title:I = 0x7f0d021c

.field public static final pin_error_external_storage_not_ready:I = 0x7f0d021d

.field public static final pin_not_available:I = 0x7f0d021e

.field public static final pin_notification_sync_fail:I = 0x7f0d021f

.field public static final pin_offline:I = 0x7f0d0220

.field public static final pin_out_of_date:I = 0x7f0d0221

.field public static final pin_paused:I = 0x7f0d0222

.field public static final pin_sync_broadband_warning_learn_more:I = 0x7f0d0223

.field public static final pin_sync_broadband_warning_message:I = 0x7f0d0224

.field public static final pin_sync_broadband_warning_title:I = 0x7f0d0225

.field public static final pin_up_to_date:I = 0x7f0d0226

.field public static final pin_update:I = 0x7f0d0227

.field public static final pin_waiting:I = 0x7f0d0228

.field public static final pin_waiting_for_network:I = 0x7f0d0229

.field public static final pin_warning_external_storage_not_ready:I = 0x7f0d022a

.field public static final pin_warning_external_storage_not_ready_ok:I = 0x7f0d022b

.field public static final plus_photo_item_remove_dialog_confirm_button:I = 0x7f0d022c

.field public static final plus_photo_item_remove_dialog_title:I = 0x7f0d022d

.field public static final policy_privacy:I = 0x7f0d022e

.field public static final policy_privacy_local_url_format_string:I = 0x7f0d022f

.field public static final policy_terms:I = 0x7f0d0230

.field public static final policy_terms_local_url_format_string:I = 0x7f0d0231

.field public static final prefs_about:I = 0x7f0d0232

.field public static final prefs_about_category_key:I = 0x7f0d0233

.field public static final prefs_about_category_title:I = 0x7f0d0234

.field public static final prefs_abuse_program_policies:I = 0x7f0d0096

.field public static final prefs_additional_filters:I = 0x7f0d0097

.field public static final prefs_additional_filters_summary:I = 0x7f0d0098

.field public static final prefs_auto_add:I = 0x7f0d0077

.field public static final prefs_auto_add_disabled_message:I = 0x7f0d0078

.field public static final prefs_auto_add_summary_template:I = 0x7f0d0079

.field public static final prefs_auto_backup:I = 0x7f0d007a

.field public static final prefs_auto_backup_summary:I = 0x7f0d007b

.field public static final prefs_cache_category_title:I = 0x7f0d0235

.field public static final prefs_cache_size:I = 0x7f0d0236

.field public static final prefs_cache_size_choice_format_string:I = 0x7f0d0237

.field public static final prefs_cache_size_summary_format_string:I = 0x7f0d0238

.field public static final prefs_category_content_description:I = 0x7f0d0239

.field public static final prefs_change_error:I = 0x7f0d007c

.field public static final prefs_clear_cache_summary:I = 0x7f0d023a

.field public static final prefs_data_usage_category:I = 0x7f0d023b

.field public static final prefs_enable_pin_encryption_summary:I = 0x7f0d023c

.field public static final prefs_enable_pin_encryption_title:I = 0x7f0d023d

.field public static final prefs_encryption_category:I = 0x7f0d023e

.field public static final prefs_google_photos_category_title:I = 0x7f0d007d

.field public static final prefs_legal:I = 0x7f0d023f

.field public static final prefs_product_tour:I = 0x7f0d0099

.field public static final prefs_storage_add_summary:I = 0x7f0d009a

.field public static final prefs_storage_add_title:I = 0x7f0d009b

.field public static final prefs_storage_category_title:I = 0x7f0d009c

.field public static final prefs_streaming_decryption_summary:I = 0x7f0d0240

.field public static final prefs_streaming_decryption_title:I = 0x7f0d0241

.field public static final prefs_sync_over_wifi_only_summary:I = 0x7f0d0242

.field public static final prefs_sync_over_wifi_only_title:I = 0x7f0d0243

.field public static final prefs_title:I = 0x7f0d0244

.field public static final preview_click_to_open:I = 0x7f0d0245

.field public static final preview_document_available_in_docs_app:I = 0x7f0d0246

.field public static final preview_document_available_in_sheets_app:I = 0x7f0d0247

.field public static final preview_open_in_docs_app_description:I = 0x7f0d0248

.field public static final preview_open_in_sheets_app_description:I = 0x7f0d0249

.field public static final print_error:I = 0x7f0d024a

.field public static final punch_approximate_transitions_notification:I = 0x7f0d009d

.field public static final punch_drag_knob_button_content_description_close_slide_picker:I = 0x7f0d009e

.field public static final punch_drag_knob_button_content_description_open_slide_picker:I = 0x7f0d009f

.field public static final punch_missing_features_notification:I = 0x7f0d00a0

.field public static final punch_no_title_slide_content_description:I = 0x7f0d00a1

.field public static final punch_open_failed:I = 0x7f0d00a2

.field public static final punch_open_failed_expanded:I = 0x7f0d00a3

.field public static final punch_speaker_notes_absent:I = 0x7f0d00a4

.field public static final punch_speaker_notes_absent_content_description:I = 0x7f0d00a5

.field public static final punch_speaker_notes_close_button_content_description:I = 0x7f0d00a6

.field public static final punch_speaker_notes_present_content_description:I = 0x7f0d00a7

.field public static final punch_speaker_notes_title:I = 0x7f0d00a8

.field public static final punch_which_slide_is_displayed:I = 0x7f0d00a9

.field public static final quota_range_large:I = 0x7f0d024b

.field public static final quota_range_larger:I = 0x7f0d024c

.field public static final quota_range_largest:I = 0x7f0d024d

.field public static final quota_range_medium:I = 0x7f0d024e

.field public static final quota_range_small:I = 0x7f0d024f

.field public static final quota_range_undefined:I = 0x7f0d0250

.field public static final quota_zero:I = 0x7f0d0251

.field public static final recent_activity_button_show_all:I = 0x7f0d0252

.field public static final recent_activity_button_show_more_files:I = 0x7f0d0253

.field public static final recent_activity_card_title:I = 0x7f0d0254

.field public static final recent_activity_empty:I = 0x7f0d0255

.field public static final recent_activity_end:I = 0x7f0d0256

.field public static final recent_activity_failed:I = 0x7f0d0257

.field public static final recent_activity_users_one:I = 0x7f0d0258

.field public static final recent_activity_users_two:I = 0x7f0d0259

.field public static final remove_button_confirm:I = 0x7f0d025a

.field public static final remove_collection:I = 0x7f0d025b

.field public static final remove_document:I = 0x7f0d025c

.field public static final rename_collection:I = 0x7f0d025d

.field public static final rename_document:I = 0x7f0d025e

.field public static final rename_drawing:I = 0x7f0d025f

.field public static final rename_file:I = 0x7f0d0260

.field public static final rename_presentation:I = 0x7f0d0261

.field public static final rename_spreadsheet:I = 0x7f0d0262

.field public static final search_description:I = 0x7f0d0305

.field public static final search_hint:I = 0x7f0d030d

.field public static final search_hint_short:I = 0x7f0d0263

.field public static final search_showing_local_results_only:I = 0x7f0d0264

.field public static final select_account:I = 0x7f0d0265

.field public static final selected_account:I = 0x7f0d0006

.field public static final selection_action_bar_drop_to_current_folder_header:I = 0x7f0d0266

.field public static final selection_drop_zone_move_new_folder:I = 0x7f0d0268

.field public static final selection_drop_zone_move_prefix:I = 0x7f0d0267

.field public static final selection_drop_zone_move_this_folder:I = 0x7f0d0269

.field public static final selection_menu_download:I = 0x7f0d026a

.field public static final selection_menu_download_content_desc:I = 0x7f0d026b

.field public static final selection_menu_move_to:I = 0x7f0d026c

.field public static final selection_menu_move_to_content_desc:I = 0x7f0d026d

.field public static final selection_menu_move_to_current_folder:I = 0x7f0d026e

.field public static final selection_menu_move_to_current_folder_content_desc:I = 0x7f0d026f

.field public static final selection_menu_overflow_content_desc:I = 0x7f0d0270

.field public static final selection_menu_pin:I = 0x7f0d0271

.field public static final selection_menu_pin_content_desc:I = 0x7f0d0272

.field public static final selection_menu_print:I = 0x7f0d0273

.field public static final selection_menu_print_content_desc:I = 0x7f0d0274

.field public static final selection_menu_remove:I = 0x7f0d0275

.field public static final selection_menu_remove_content_desc:I = 0x7f0d0276

.field public static final selection_menu_rename:I = 0x7f0d0277

.field public static final selection_menu_rename_content_desc:I = 0x7f0d0278

.field public static final selection_menu_share:I = 0x7f0d0279

.field public static final selection_menu_share_content_desc:I = 0x7f0d027a

.field public static final selection_menu_star:I = 0x7f0d027b

.field public static final selection_menu_star_content_desc:I = 0x7f0d027c

.field public static final selection_menu_unpin:I = 0x7f0d027d

.field public static final selection_menu_unpin_content_desc:I = 0x7f0d027e

.field public static final selection_menu_unstar:I = 0x7f0d027f

.field public static final selection_menu_unstar_content_desc:I = 0x7f0d0280

.field public static final selection_undo_button:I = 0x7f0d0281

.field public static final selection_undo_message_remove:I = 0x7f0d0282

.field public static final selection_undo_move_message:I = 0x7f0d0283

.field public static final send_to_clipboard_activity_title:I = 0x7f0d00aa

.field public static final send_to_clipboard_error_reading_stream:I = 0x7f0d0284

.field public static final send_to_clipboard_success:I = 0x7f0d0285

.field public static final share_card_enabled_link_sharing_toast:I = 0x7f0d0286

.field public static final share_card_link_shared_and_copied:I = 0x7f0d0287

.field public static final share_card_link_sharing_is_off:I = 0x7f0d0288

.field public static final share_card_link_sharing_is_on:I = 0x7f0d0289

.field public static final share_card_title:I = 0x7f0d028a

.field public static final share_card_turn_link_sharing_off:I = 0x7f0d028b

.field public static final share_psyncho_warning_description:I = 0x7f0d028c

.field public static final share_psyncho_warning_header:I = 0x7f0d028d

.field public static final share_psyncho_warning_learnmore:I = 0x7f0d028e

.field public static final shared_status:I = 0x7f0d028f

.field public static final shared_with_me:I = 0x7f0d0290

.field public static final sharing_activity_title:I = 0x7f0d0291

.field public static final sharing_cannot_change:I = 0x7f0d0292

.field public static final sharing_cannot_change_option:I = 0x7f0d0293

.field public static final sharing_cannot_change_owner:I = 0x7f0d0294

.field public static final sharing_error:I = 0x7f0d0295

.field public static final sharing_info_loading:I = 0x7f0d0296

.field public static final sharing_list_may_not_be_completed:I = 0x7f0d0297

.field public static final sharing_list_offline:I = 0x7f0d0298

.field public static final sharing_list_updated:I = 0x7f0d0299

.field public static final sharing_message_hint:I = 0x7f0d029a

.field public static final sharing_message_saved:I = 0x7f0d029b

.field public static final sharing_message_unable_to_change:I = 0x7f0d029c

.field public static final sharing_offline:I = 0x7f0d029d

.field public static final sharing_option_anyone:I = 0x7f0d029e

.field public static final sharing_option_anyone_can_comment:I = 0x7f0d029f

.field public static final sharing_option_anyone_can_edit:I = 0x7f0d02a0

.field public static final sharing_option_anyone_can_view:I = 0x7f0d02a1

.field public static final sharing_option_anyone_description:I = 0x7f0d02a2

.field public static final sharing_option_anyone_from:I = 0x7f0d02a3

.field public static final sharing_option_anyone_from_can_comment:I = 0x7f0d02a4

.field public static final sharing_option_anyone_from_can_edit:I = 0x7f0d02a5

.field public static final sharing_option_anyone_from_can_view:I = 0x7f0d02a6

.field public static final sharing_option_anyone_from_description:I = 0x7f0d02a7

.field public static final sharing_option_anyone_from_with_link:I = 0x7f0d02a8

.field public static final sharing_option_anyone_from_with_link_can_comment:I = 0x7f0d02a9

.field public static final sharing_option_anyone_from_with_link_can_edit:I = 0x7f0d02aa

.field public static final sharing_option_anyone_from_with_link_can_view:I = 0x7f0d02ab

.field public static final sharing_option_anyone_from_with_link_description:I = 0x7f0d02ac

.field public static final sharing_option_anyone_with_link:I = 0x7f0d02ad

.field public static final sharing_option_anyone_with_link_can_comment:I = 0x7f0d02ae

.field public static final sharing_option_anyone_with_link_can_edit:I = 0x7f0d02af

.field public static final sharing_option_anyone_with_link_can_view:I = 0x7f0d02b0

.field public static final sharing_option_anyone_with_link_description:I = 0x7f0d02b1

.field public static final sharing_option_private:I = 0x7f0d02b2

.field public static final sharing_option_specific_people:I = 0x7f0d02b3

.field public static final sharing_option_unknown:I = 0x7f0d02b4

.field public static final sharing_progress_loading_message:I = 0x7f0d02b5

.field public static final sharing_progress_saving_message:I = 0x7f0d02b6

.field public static final sharing_role_commenter:I = 0x7f0d02b7

.field public static final sharing_role_no_access:I = 0x7f0d02b8

.field public static final sharing_role_owner:I = 0x7f0d02b9

.field public static final sharing_role_reader:I = 0x7f0d02ba

.field public static final sharing_role_unknown:I = 0x7f0d02bb

.field public static final sharing_role_writer:I = 0x7f0d02bc

.field public static final shortcut_created:I = 0x7f0d02bd

.field public static final show_account_list:I = 0x7f0d0003

.field public static final sign_in:I = 0x7f0d0002

.field public static final slider_title_all_items:I = 0x7f0d02be

.field public static final splash_description:I = 0x7f0d0356

.field public static final splash_heading:I = 0x7f0d0357

.field public static final sync_failed:I = 0x7f0d0306

.field public static final sync_more:I = 0x7f0d02bf

.field public static final sync_more_error:I = 0x7f0d02c0

.field public static final sync_more_in_progress:I = 0x7f0d02c1

.field public static final thumbnail_open:I = 0x7f0d02c2

.field public static final thumbnail_preview:I = 0x7f0d02c3

.field public static final time_range_last_year:I = 0x7f0d02c4

.field public static final time_range_older:I = 0x7f0d02c5

.field public static final time_range_this_month:I = 0x7f0d02c6

.field public static final time_range_this_week:I = 0x7f0d02c7

.field public static final time_range_this_year:I = 0x7f0d02c8

.field public static final time_range_today:I = 0x7f0d02c9

.field public static final time_range_yesterday:I = 0x7f0d02ca

.field public static final title_dialog_password:I = 0x7f0d0344

.field public static final title_grouper_files:I = 0x7f0d02cb

.field public static final title_print:I = 0x7f0d0327

.field public static final title_send_intent:I = 0x7f0d0328

.field public static final title_show_files:I = 0x7f0d0326

.field public static final transfer_notification_waiting_content:I = 0x7f0d02cc

.field public static final trash_delete_forever_confirm:I = 0x7f0d02cd

.field public static final trash_delete_forever_question:I = 0x7f0d02ce

.field public static final trash_delete_forever_success:I = 0x7f0d02cf

.field public static final trash_delete_forever_warning:I = 0x7f0d02d0

.field public static final upload_account:I = 0x7f0d02d1

.field public static final upload_cancel:I = 0x7f0d02d2

.field public static final upload_conversion_options:I = 0x7f0d02d3

.field public static final upload_convert_image:I = 0x7f0d02d4

.field public static final upload_document_title:I = 0x7f0d02d5

.field public static final upload_error_no_data_supplied:I = 0x7f0d02d6

.field public static final upload_folder:I = 0x7f0d02d7

.field public static final upload_incomplete:I = 0x7f0d02d8

.field public static final upload_multiple_document_titles:I = 0x7f0d02d9

.field public static final upload_notification_failure_no_retry_title:I = 0x7f0d02da

.field public static final upload_notification_failure_ticker:I = 0x7f0d00ab

.field public static final upload_notification_failure_title:I = 0x7f0d00ac

.field public static final upload_pause:I = 0x7f0d02db

.field public static final upload_paused:I = 0x7f0d02dc

.field public static final upload_remove:I = 0x7f0d02dd

.field public static final upload_resume:I = 0x7f0d02de

.field public static final upload_select_account:I = 0x7f0d02df

.field public static final upload_shared_item_confirm:I = 0x7f0d02e0

.field public static final upload_shared_item_title:I = 0x7f0d02e1

.field public static final upload_shared_item_title_convert:I = 0x7f0d02e2

.field public static final upload_status_in_doc_list:I = 0x7f0d02e3

.field public static final upload_untitled_file_title:I = 0x7f0d02e4

.field public static final upload_waiting:I = 0x7f0d02e5

.field public static final upload_waiting_for_network:I = 0x7f0d02e6

.field public static final upload_waiting_for_wifi:I = 0x7f0d02e7

.field public static final version_too_old:I = 0x7f0d0307

.field public static final version_too_old_close:I = 0x7f0d02e8

.field public static final version_too_old_title:I = 0x7f0d02e9

.field public static final version_too_old_upgrade:I = 0x7f0d02ea

.field public static final video_controller_pause:I = 0x7f0d02eb

.field public static final video_controller_play:I = 0x7f0d02ec

.field public static final video_controller_replay:I = 0x7f0d02ed

.field public static final video_hide_subtitles:I = 0x7f0d02ee

.field public static final video_show_subtitles:I = 0x7f0d02ef

.field public static final wallet_buy_button_place_holder:I = 0x7f0d0036

.field public static final warm_welcome_done_button:I = 0x7f0d0071

.field public static final warm_welcome_next_button:I = 0x7f0d0070

.field public static final warm_welcome_skip_button:I = 0x7f0d006f

.field public static final welcome_button_close:I = 0x7f0d0308

.field public static final welcome_button_continue:I = 0x7f0d02f0

.field public static final welcome_offer_button_close:I = 0x7f0d02f1

.field public static final welcome_offer_checking:I = 0x7f0d02f2

.field public static final welcome_offer_fail_account:I = 0x7f0d02f3

.field public static final welcome_offer_fail_account_unlimited:I = 0x7f0d02f4

.field public static final welcome_offer_fail_bug:I = 0x7f0d0309

.field public static final welcome_offer_fail_connect:I = 0x7f0d02f5

.field public static final welcome_offer_fail_disabled:I = 0x7f0d02f6

.field public static final welcome_offer_fail_redeemed:I = 0x7f0d02f7

.field public static final welcome_offer_failed_title:I = 0x7f0d02f8

.field public static final welcome_photo_backup_suggest_positive_button_label:I = 0x7f0d007e

.field public static final welcome_title:I = 0x7f0d030a

.field public static final welcome_title_announce:I = 0x7f0d030b

.field public static final welcome_title_highlights:I = 0x7f0d030c

.field public static final widget_account_missing:I = 0x7f0d035c

.field public static final widget_configuration_missing:I = 0x7f0d035d

.field public static final widget_initializing:I = 0x7f0d035e

.field public static final widget_scan_to_drive:I = 0x7f0d035f

.field public static final widget_scan_to_drive_title:I = 0x7f0d0360

.field public static final widget_upload:I = 0x7f0d0361

.field public static final write_access_denied:I = 0x7f0d02f9


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
