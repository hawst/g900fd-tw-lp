.class final enum Lvo;
.super Lvn;
.source "SendTextToClipboardActivity.java"


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lvn;-><init>(Ljava/lang/String;ILvm;)V

    return-void
.end method


# virtual methods
.method a(Landroid/text/ClipboardManager;Ljava/lang/String;Landroid/content/Context;Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 46
    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 48
    :try_start_0
    invoke-virtual {v0, p4}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 50
    :try_start_1
    new-instance v0, Lalp;

    invoke-direct {v0}, Lalp;-><init>()V

    invoke-virtual {v0, v1}, Lalp;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 51
    new-instance v2, Ljava/lang/String;

    sget-object v3, LbiB;->c:Ljava/nio/charset/Charset;

    invoke-virtual {v3}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 52
    invoke-virtual {p0, p1, p2, v2}, Lvo;->a(Landroid/text/ClipboardManager;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 56
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 62
    :goto_0
    return-void

    .line 53
    :catch_0
    move-exception v0

    .line 54
    :try_start_3
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "UTF-8 not supported"

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 56
    :catchall_0
    move-exception v0

    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 58
    :catch_1
    move-exception v0

    .line 59
    sget v0, Lxi;->send_to_clipboard_error_reading_stream:I

    const/4 v1, 0x0

    invoke-static {p3, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 60
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method a(Landroid/text/ClipboardManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 41
    invoke-virtual {p1, p3}, Landroid/text/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    .line 42
    return-void
.end method
