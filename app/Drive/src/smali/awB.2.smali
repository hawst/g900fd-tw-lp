.class public LawB;
.super Ljava/lang/Object;
.source "ViewerManager.java"


# instance fields
.field private final a:Landroid/app/FragmentManager;

.field private final a:LawC;


# direct methods
.method public constructor <init>(Landroid/app/FragmentManager;LawC;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {p1}, LauV;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/FragmentManager;

    iput-object v0, p0, LawB;->a:Landroid/app/FragmentManager;

    .line 31
    invoke-static {p2}, LauV;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LawC;

    iput-object v0, p0, LawB;->a:LawC;

    .line 32
    return-void
.end method

.method private a(I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 119
    const-string v0, "Viewer #%d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 120
    return-object v0
.end method


# virtual methods
.method public a(I)Laww;
    .locals 2

    .prologue
    .line 65
    invoke-direct {p0, p1}, LawB;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 66
    iget-object v1, p0, LawB;->a:Landroid/app/FragmentManager;

    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 67
    if-eqz v0, :cond_0

    .line 68
    check-cast v0, Laww;

    .line 71
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(ILandroid/view/ViewGroup;LauW;)Laww;
    .locals 4

    .prologue
    .line 46
    invoke-virtual {p0, p1}, LawB;->a(I)Laww;

    move-result-object v0

    .line 47
    if-eqz v0, :cond_0

    .line 48
    invoke-virtual {v0}, Laww;->a()V

    .line 61
    :goto_0
    return-object v0

    .line 52
    :cond_0
    iget-object v0, p0, LawB;->a:LawC;

    iget-object v1, p3, LauW;->a:Lavg;

    invoke-interface {v0, v1}, LawC;->a(Lavg;)Laww;

    move-result-object v0

    .line 53
    iget-object v1, p0, LawB;->a:Landroid/app/FragmentManager;

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 54
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getId()I

    move-result v2

    invoke-direct {p0, p1}, LawB;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 57
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 58
    invoke-virtual {v0, p3}, Laww;->a(LauW;)Laww;

    .line 59
    invoke-virtual {v0}, Laww;->a()V

    .line 60
    const-string v1, "ViewerManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Create and feed a new Viewer "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
