.class public LqJ;
.super Ljava/lang/Object;
.source "RocketEventTracker.java"


# instance fields
.field private a:LaFO;

.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(LqH;)V
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    invoke-virtual {p1}, LqH;->name()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LqJ;->a:Ljava/lang/String;

    .line 107
    invoke-static {p1}, LqH;->a(LqH;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LqJ;->b:Ljava/lang/String;

    .line 108
    return-void
.end method

.method private a(LaGv;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    if-eqz p1, :cond_0

    .line 156
    invoke-static {}, LqF;->a()LbmL;

    move-result-object v0

    invoke-virtual {v0, p1}, LbmL;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 158
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()LqI;
    .locals 7

    .prologue
    .line 151
    new-instance v0, LqI;

    iget-object v1, p0, LqJ;->a:Ljava/lang/String;

    iget-object v2, p0, LqJ;->b:Ljava/lang/String;

    iget-object v3, p0, LqJ;->a:LaFO;

    iget-object v4, p0, LqJ;->c:Ljava/lang/String;

    iget-object v5, p0, LqJ;->d:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, LqI;-><init>(Ljava/lang/String;Ljava/lang/String;LaFO;Ljava/lang/String;Ljava/lang/String;LqG;)V

    return-object v0
.end method

.method public a(LaFO;)LqJ;
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, LqJ;->a:LaFO;

    .line 117
    return-object p0
.end method

.method public a(LaGv;)LqJ;
    .locals 1

    .prologue
    .line 126
    invoke-direct {p0, p1}, LqJ;->a(LaGv;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LqJ;->c:Ljava/lang/String;

    .line 127
    return-object p0
.end method
