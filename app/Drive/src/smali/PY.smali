.class public LPY;
.super LPK;
.source "MoveEventBinder.java"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/appsactivity/model/Parent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LPD;LaGu;)V
    .locals 4

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, LPK;-><init>(LPD;LaGu;)V

    .line 29
    invoke-virtual {p1}, LPD;->a()Lcom/google/api/services/appsactivity/model/Event;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/appsactivity/model/Event;->getMove()Lcom/google/api/services/appsactivity/model/Move;

    move-result-object v0

    .line 30
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v1

    .line 31
    invoke-virtual {v0}, Lcom/google/api/services/appsactivity/model/Move;->getAddedParents()Ljava/util/List;

    move-result-object v0

    .line 32
    if-eqz v0, :cond_1

    .line 33
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/Parent;

    .line 34
    invoke-virtual {v0}, Lcom/google/api/services/appsactivity/model/Parent;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 35
    invoke-virtual {v1, v0}, LbmH;->a(Ljava/lang/Object;)LbmH;

    goto :goto_0

    .line 39
    :cond_1
    invoke-virtual {v1}, LbmH;->a()LbmF;

    move-result-object v0

    iput-object v0, p0, LPY;->a:Ljava/util/List;

    .line 40
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, LPY;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, LPY;->a:LPD;

    invoke-virtual {v0}, LPD;->a()Lcom/google/api/services/appsactivity/model/Event;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/appsactivity/model/Event;->getMove()Lcom/google/api/services/appsactivity/model/Move;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Lcom/google/api/services/appsactivity/model/Move;->getAddedParents()Ljava/util/List;

    move-result-object v0

    .line 93
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 45
    invoke-virtual {p0}, LPY;->a()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 47
    :goto_0
    if-eqz v0, :cond_1

    sget v2, Lxg;->recent_activity_type_moved_extra_one_user:I

    .line 49
    :goto_1
    if-eqz v0, :cond_2

    sget v3, Lxg;->recent_activity_type_moved_extra_many_users:I

    .line 51
    :goto_2
    if-eqz v0, :cond_3

    sget v4, Lxg;->recent_activity_type_moved_this_file_extra:I

    .line 53
    :goto_3
    if-eqz v0, :cond_4

    sget v5, Lxg;->recent_activity_type_moved_this_folder_extra:I

    :goto_4
    move-object v0, p0

    move-object v1, p1

    .line 56
    invoke-virtual/range {v0 .. v5}, LPY;->a(Landroid/content/res/Resources;IIII)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 45
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 47
    :cond_1
    sget v2, Lxg;->recent_activity_type_moved_one_user:I

    goto :goto_1

    .line 49
    :cond_2
    sget v3, Lxg;->recent_activity_type_moved_many_users:I

    goto :goto_2

    .line 51
    :cond_3
    sget v4, Lxg;->recent_activity_type_moved_this_file:I

    goto :goto_3

    .line 53
    :cond_4
    sget v5, Lxg;->recent_activity_type_moved_this_folder:I

    goto :goto_4
.end method

.method public a(Landroid/view/View;I)V
    .locals 4

    .prologue
    .line 66
    invoke-super {p0, p1, p2}, LPK;->a(Landroid/view/View;I)V

    .line 67
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPW;

    .line 69
    iget-object v1, p0, LPY;->a:LPD;

    invoke-virtual {v1}, LPD;->a()Lcom/google/api/services/appsactivity/model/Event;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/api/services/appsactivity/model/Event;->getMove()Lcom/google/api/services/appsactivity/model/Move;

    move-result-object v1

    .line 70
    invoke-virtual {v1}, Lcom/google/api/services/appsactivity/model/Move;->getAddedParents()Ljava/util/List;

    move-result-object v1

    .line 71
    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/appsactivity/model/Parent;

    .line 72
    invoke-virtual {v1}, Lcom/google/api/services/appsactivity/model/Parent;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 73
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 75
    iget-object v2, v0, LPW;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/google/api/services/appsactivity/model/Parent;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    :goto_0
    sget-object v1, LaGv;->a:LaGv;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, LaGt;->b(LaGv;Ljava/lang/String;Z)I

    move-result v1

    .line 81
    iget-object v0, v0, LPW;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 82
    return-void

    .line 77
    :cond_0
    iget-object v1, v0, LPW;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x1

    return v0
.end method
