.class public abstract Lvr;
.super Ljava/lang/Object;
.source "ShortcutDefinition.java"


# instance fields
.field private final a:I

.field private final a:LCl;

.field private final a:Ljava/lang/String;

.field private final a:Lrx;

.field private final b:I


# direct methods
.method protected constructor <init>(LCl;IILrx;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lvr;->a:LCl;

    .line 50
    iput p2, p0, Lvr;->a:I

    .line 51
    iput p3, p0, Lvr;->b:I

    .line 52
    iput-object p4, p0, Lvr;->a:Lrx;

    .line 53
    iput-object p5, p0, Lvr;->a:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public static a(Lrx;LbmF;)LbmF;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx;",
            "LbmF",
            "<",
            "Lvr;",
            ">;)",
            "LbmF",
            "<",
            "Lvr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 109
    invoke-virtual {p1}, LbmF;->a()Lbqv;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvr;

    .line 110
    invoke-virtual {v0}, Lvr;->a()Lrx;

    move-result-object v3

    invoke-virtual {p0, v3}, Lrx;->a(Lrx;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 111
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 114
    :cond_1
    invoke-static {v1}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lvr;->b:I

    return v0
.end method

.method public a()LCl;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lvr;->a:LCl;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lvr;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()Lrx;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lvr;->a:Lrx;

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lvr;->a:I

    return v0
.end method
