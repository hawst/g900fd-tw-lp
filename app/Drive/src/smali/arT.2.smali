.class public LarT;
.super Ljava/lang/Object;
.source "BuiltInBookshop.java"

# interfaces
.implements Lati;


# static fields
.field private static final a:LarV;

.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LarV;

.field private static final c:LarV;


# instance fields
.field private final a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lakz;

.field a:LasP;
    .annotation runtime LQS;
    .end annotation
.end field

.field private final a:LasR;

.field private final a:Lasy;

.field private final a:LbmL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmL",
            "<",
            "Lrg;",
            "LasP;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lrx;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 58
    invoke-static {}, LboS;->a()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, LarT;->a:Ljava/util/Map;

    .line 61
    new-instance v0, LarV;

    const-string v1, "Drive"

    invoke-direct {v0, v1}, LarV;-><init>(Ljava/lang/String;)V

    const-string v1, "38918a453d07199354f8b19af05ec6562ced5788"

    .line 62
    invoke-virtual {v0, v1}, LarV;->a(Ljava/lang/String;)LarV;

    move-result-object v0

    const-string v1, "58e1c4133f7441ec3d2c270270a14802da47ba0e"

    .line 63
    invoke-virtual {v0, v1}, LarV;->b(Ljava/lang/String;)LarV;

    move-result-object v0

    sput-object v0, LarT;->a:LarV;

    .line 66
    new-instance v0, LarV;

    const-string v1, "Motorola"

    invoke-direct {v0, v1}, LarV;-><init>(Ljava/lang/String;)V

    const-string v1, "d670d099a3a6e657ba839dce0746fe1a31f4547e"

    .line 67
    invoke-virtual {v0, v1}, LarV;->a(Ljava/lang/String;)LarV;

    move-result-object v0

    const-string v1, "4f9f19228947355cc45b39d22b5780f0d0480db7"

    .line 68
    invoke-virtual {v0, v1}, LarV;->b(Ljava/lang/String;)LarV;

    move-result-object v0

    sput-object v0, LarT;->b:LarV;

    .line 71
    new-instance v0, LarV;

    const-string v1, "Htc"

    invoke-direct {v0, v1}, LarV;-><init>(Ljava/lang/String;)V

    const-string v1, "1052f733fa71da5c2803611cb336f064a8728b36"

    .line 72
    invoke-virtual {v0, v1}, LarV;->a(Ljava/lang/String;)LarV;

    move-result-object v0

    const-string v1, "27196e386b875e76adf700e7ea84e4c6eee33dfa"

    .line 73
    invoke-virtual {v0, v1}, LarV;->b(Ljava/lang/String;)LarV;

    move-result-object v0

    sput-object v0, LarT;->c:LarV;

    .line 71
    return-void
.end method

.method public constructor <init>(Lrx;LasR;Lakz;Laja;Lasy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx;",
            "LasR;",
            "Lakz;",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Lasy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object p1, p0, LarT;->a:Lrx;

    .line 109
    iput-object p2, p0, LarT;->a:LasR;

    .line 110
    iput-object p3, p0, LarT;->a:Lakz;

    .line 111
    iput-object p4, p0, LarT;->a:Laja;

    .line 112
    iput-object p5, p0, LarT;->a:Lasy;

    .line 113
    invoke-static {}, LbmL;->a()LbmL;

    move-result-object v0

    iput-object v0, p0, LarT;->a:LbmL;

    .line 114
    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 214
    sget-object v0, LarT;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a()Ljava/util/Map;
    .locals 1

    .prologue
    .line 32
    sget-object v0, LarT;->a:Ljava/util/Map;

    return-object v0
.end method

.method private a(Lrx;)Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202
    sget-object v0, LarU;->a:[I

    invoke-virtual {p1}, Lrx;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 209
    sget-object v0, LarT;->a:LarV;

    iget-object v0, v0, LarV;->b:Ljava/lang/String;

    sget-object v1, LarT;->b:LarV;

    iget-object v1, v1, LarV;->b:Ljava/lang/String;

    sget-object v2, LarT;->c:LarV;

    iget-object v2, v2, LarV;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmY;

    move-result-object v0

    :goto_0
    return-object v0

    .line 206
    :pswitch_0
    sget-object v0, LarT;->a:LarV;

    iget-object v0, v0, LarV;->b:Ljava/lang/String;

    sget-object v1, LarT;->a:LarV;

    iget-object v1, v1, LarV;->c:Ljava/lang/String;

    sget-object v2, LarT;->b:LarV;

    iget-object v2, v2, LarV;->b:Ljava/lang/String;

    sget-object v3, LarT;->b:LarV;

    iget-object v3, v3, LarV;->c:Ljava/lang/String;

    sget-object v4, LarT;->c:LarV;

    iget-object v4, v4, LarV;->b:Ljava/lang/String;

    sget-object v5, LarT;->c:LarV;

    iget-object v5, v5, LarV;->c:Ljava/lang/String;

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/String;

    invoke-static/range {v0 .. v6}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LbmY;

    move-result-object v0

    goto :goto_0

    .line 202
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private a(Lakz;Landroid/content/Context;)Z
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 148
    iget-object v2, p0, LarT;->a:Lasy;

    invoke-virtual {v2}, Lasy;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 149
    const-string v1, "BuiltInBookshop"

    const-string v2, "Offer is not currently available"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 193
    :goto_0
    return v0

    .line 153
    :cond_0
    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 154
    const-string v3, "com.google.android.apps.docs.oem.welcome"

    invoke-virtual {v2, v3, v0}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v3

    .line 155
    if-nez v3, :cond_1

    .line 157
    const-string v2, "BuiltInBookshop"

    const-string v3, "No external provider registered at %s"

    new-array v1, v1, [Ljava/lang/Object;

    const-string v4, "com.google.android.apps.docs.oem.welcome"

    aput-object v4, v1, v0

    invoke-static {v2, v3, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 161
    :cond_1
    iget-object v3, v3, Landroid/content/pm/ProviderInfo;->packageName:Ljava/lang/String;

    .line 163
    invoke-virtual {p2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4, v3}, Landroid/content/pm/PackageManager;->checkSignatures(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_2

    .line 165
    const-string v2, "BuiltInBookshop"

    const-string v4, "Trusted provider with same signature as ours: %s(%s)"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v0

    .line 166
    invoke-virtual {p2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    .line 165
    invoke-static {v2, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v1

    .line 167
    goto :goto_0

    .line 172
    :cond_2
    const-string v4, "android"

    invoke-virtual {v2, v4, v3}, Landroid/content/pm/PackageManager;->checkSignatures(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_3

    .line 173
    const-string v2, "BuiltInBookshop"

    const-string v4, "Trusted provider with system signature: %s"

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v3, v5, v0

    invoke-static {v2, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v1

    .line 174
    goto :goto_0

    .line 178
    :cond_3
    const/16 v4, 0x40

    :try_start_0
    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 179
    iget-object v4, v2, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .line 180
    iget-object v2, p0, LarT;->a:Lrx;

    invoke-direct {p0, v2}, LarT;->a(Lrx;)Ljava/util/Set;

    move-result-object v5

    .line 182
    array-length v6, v4

    move v2, v0

    :goto_1
    if-ge v2, v6, :cond_5

    aget-object v7, v4, v2

    .line 183
    invoke-virtual {v7}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v7

    invoke-interface {p1, v7, v5}, Lakz;->a([BLjava/util/Set;)Ljava/lang/String;

    move-result-object v7

    .line 184
    if-eqz v7, :cond_4

    .line 185
    const-string v2, "BuiltInBookshop"

    const-string v4, "Trusted external provider: %s(%s)"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-direct {p0, v7}, LarT;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v3, v5, v6

    invoke-static {v2, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v1

    .line 186
    goto/16 :goto_0

    .line 182
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 189
    :cond_5
    const-string v1, "BuiltInBookshop"

    const-string v2, "The external app which offers welcome page is not our friend: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    invoke-static {v1, v2, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 190
    :catch_0
    move-exception v1

    goto/16 :goto_0
.end method


# virtual methods
.method public a()LasP;
    .locals 3

    .prologue
    .line 118
    iget-object v0, p0, LarT;->a:LasR;

    const-string v1, "welcome"

    const-string v2, "welcome.toc"

    invoke-virtual {v0, v1, v2}, LasR;->a(Ljava/lang/String;Ljava/lang/String;)LasP;

    move-result-object v0

    return-object v0
.end method

.method public a(Lrg;)LasP;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, LarT;->a:LbmL;

    invoke-virtual {v0, p1}, LbmL;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LasP;

    return-object v0
.end method

.method public b()LasP;
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, LarT;->a:LasP;

    if-eqz v0, :cond_0

    .line 135
    iget-object v1, p0, LarT;->a:Lakz;

    iget-object v0, p0, LarT;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {p0, v1, v0}, LarT;->a(Lakz;Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, LarT;->a:LasP;

    .line 139
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()LasP;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, LarT;->a:LasR;

    invoke-virtual {v0}, LasR;->a()LasV;

    move-result-object v0

    return-object v0
.end method
