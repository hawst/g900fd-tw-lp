.class abstract Lbot;
.super Ljava/lang/Object;
.source "MapMakerInternalMap.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TE;>;"
    }
.end annotation


# instance fields
.field a:I

.field a:LboR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbnY",
            "<TK;TV;>.boR;"
        }
    .end annotation
.end field

.field a:Lbox;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field a:Lboy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lboy",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field a:Ljava/util/concurrent/atomic/AtomicReferenceArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReferenceArray",
            "<",
            "Lbox",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field b:I

.field final synthetic b:LbnY;

.field b:LboR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbnY",
            "<TK;TV;>.boR;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LbnY;)V
    .locals 1

    .prologue
    .line 3494
    iput-object p1, p0, Lbot;->b:LbnY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3495
    iget-object v0, p1, LbnY;->a:[Lboy;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbot;->a:I

    .line 3496
    const/4 v0, -0x1

    iput v0, p0, Lbot;->b:I

    .line 3497
    invoke-virtual {p0}, Lbot;->a()V

    .line 3498
    return-void
.end method


# virtual methods
.method a()LboR;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbnY",
            "<TK;TV;>.boR;"
        }
    .end annotation

    .prologue
    .line 3578
    iget-object v0, p0, Lbot;->a:LboR;

    if-nez v0, :cond_0

    .line 3579
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 3581
    :cond_0
    iget-object v0, p0, Lbot;->a:LboR;

    iput-object v0, p0, Lbot;->b:LboR;

    .line 3582
    invoke-virtual {p0}, Lbot;->a()V

    .line 3583
    iget-object v0, p0, Lbot;->b:LboR;

    return-object v0
.end method

.method final a()V
    .locals 3

    .prologue
    .line 3503
    const/4 v0, 0x0

    iput-object v0, p0, Lbot;->a:LboR;

    .line 3505
    invoke-virtual {p0}, Lbot;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3523
    :cond_0
    :goto_0
    return-void

    .line 3509
    :cond_1
    invoke-virtual {p0}, Lbot;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3513
    :cond_2
    iget v0, p0, Lbot;->a:I

    if-ltz v0, :cond_0

    .line 3514
    iget-object v0, p0, Lbot;->b:LbnY;

    iget-object v0, v0, LbnY;->a:[Lboy;

    iget v1, p0, Lbot;->a:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lbot;->a:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lbot;->a:Lboy;

    .line 3515
    iget-object v0, p0, Lbot;->a:Lboy;

    iget v0, v0, Lboy;->a:I

    if-eqz v0, :cond_2

    .line 3516
    iget-object v0, p0, Lbot;->a:Lboy;

    iget-object v0, v0, Lboy;->a:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iput-object v0, p0, Lbot;->a:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3517
    iget-object v0, p0, Lbot;->a:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbot;->b:I

    .line 3518
    invoke-virtual {p0}, Lbot;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0
.end method

.method a()Z
    .locals 1

    .prologue
    .line 3529
    iget-object v0, p0, Lbot;->a:Lbox;

    if-eqz v0, :cond_1

    .line 3530
    iget-object v0, p0, Lbot;->a:Lbox;

    invoke-interface {v0}, Lbox;->a()Lbox;

    move-result-object v0

    iput-object v0, p0, Lbot;->a:Lbox;

    :goto_0
    iget-object v0, p0, Lbot;->a:Lbox;

    if-eqz v0, :cond_1

    .line 3531
    iget-object v0, p0, Lbot;->a:Lbox;

    invoke-virtual {p0, v0}, Lbot;->a(Lbox;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3532
    const/4 v0, 0x1

    .line 3536
    :goto_1
    return v0

    .line 3530
    :cond_0
    iget-object v0, p0, Lbot;->a:Lbox;

    invoke-interface {v0}, Lbox;->a()Lbox;

    move-result-object v0

    iput-object v0, p0, Lbot;->a:Lbox;

    goto :goto_0

    .line 3536
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method a(Lbox;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbox",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 3559
    :try_start_0
    invoke-interface {p1}, Lbox;->a()Ljava/lang/Object;

    move-result-object v0

    .line 3560
    iget-object v1, p0, Lbot;->b:LbnY;

    invoke-virtual {v1, p1}, LbnY;->a(Lbox;)Ljava/lang/Object;

    move-result-object v1

    .line 3561
    if-eqz v1, :cond_0

    .line 3562
    new-instance v2, LboR;

    iget-object v3, p0, Lbot;->b:LbnY;

    invoke-direct {v2, v3, v0, v1}, LboR;-><init>(LbnY;Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v2, p0, Lbot;->a:LboR;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3563
    const/4 v0, 0x1

    .line 3569
    iget-object v1, p0, Lbot;->a:Lboy;

    invoke-virtual {v1}, Lboy;->m()V

    :goto_0
    return v0

    .line 3566
    :cond_0
    const/4 v0, 0x0

    .line 3569
    iget-object v1, p0, Lbot;->a:Lboy;

    invoke-virtual {v1}, Lboy;->m()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbot;->a:Lboy;

    invoke-virtual {v1}, Lboy;->m()V

    throw v0
.end method

.method b()Z
    .locals 3

    .prologue
    .line 3543
    :cond_0
    iget v0, p0, Lbot;->b:I

    if-ltz v0, :cond_2

    .line 3544
    iget-object v0, p0, Lbot;->a:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iget v1, p0, Lbot;->b:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lbot;->b:I

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbox;

    iput-object v0, p0, Lbot;->a:Lbox;

    if-eqz v0, :cond_0

    .line 3545
    iget-object v0, p0, Lbot;->a:Lbox;

    invoke-virtual {p0, v0}, Lbot;->a(Lbox;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lbot;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3546
    :cond_1
    const/4 v0, 0x1

    .line 3550
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 3574
    iget-object v0, p0, Lbot;->a:LboR;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 3587
    iget-object v0, p0, Lbot;->b:LboR;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 3588
    iget-object v0, p0, Lbot;->b:LbnY;

    iget-object v1, p0, Lbot;->b:LboR;

    invoke-virtual {v1}, LboR;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, LbnY;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3589
    const/4 v0, 0x0

    iput-object v0, p0, Lbot;->b:LboR;

    .line 3590
    return-void

    .line 3587
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
