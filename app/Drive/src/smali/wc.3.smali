.class public final Lwc;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LvM;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lwh;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LvW;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LvP;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lwp;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lwb;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lwm;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lwa;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LvL;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LvU;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lwg;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LvY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 48
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 49
    iput-object p1, p0, Lwc;->a:LbrA;

    .line 50
    const-class v0, LvM;

    invoke-static {v0, v2}, Lwc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lwc;->a:Lbsk;

    .line 53
    const-class v0, Lwh;

    const-class v1, LaiC;

    invoke-static {v0, v1}, Lwc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lwc;->b:Lbsk;

    .line 56
    const-class v0, LvW;

    const-class v1, LaiC;

    invoke-static {v0, v1}, Lwc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lwc;->c:Lbsk;

    .line 59
    const-class v0, LvP;

    invoke-static {v0, v2}, Lwc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lwc;->d:Lbsk;

    .line 62
    const-class v0, Lwp;

    const-class v1, LaiC;

    invoke-static {v0, v1}, Lwc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lwc;->e:Lbsk;

    .line 65
    const-class v0, Lwb;

    const-class v1, LaiC;

    invoke-static {v0, v1}, Lwc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lwc;->f:Lbsk;

    .line 68
    const-class v0, Lwm;

    invoke-static {v0, v2}, Lwc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lwc;->g:Lbsk;

    .line 71
    const-class v0, Lwa;

    invoke-static {v0, v2}, Lwc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lwc;->h:Lbsk;

    .line 74
    const-class v0, LvL;

    invoke-static {v0, v2}, Lwc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lwc;->i:Lbsk;

    .line 77
    const-class v0, LvU;

    invoke-static {v0, v2}, Lwc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lwc;->j:Lbsk;

    .line 80
    const-class v0, Lwg;

    invoke-static {v0, v2}, Lwc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lwc;->k:Lbsk;

    .line 83
    const-class v0, LvY;

    invoke-static {v0, v2}, Lwc;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lwc;->l:Lbsk;

    .line 86
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 137
    sparse-switch p1, :sswitch_data_0

    .line 205
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 139
    :sswitch_0
    new-instance v0, LvM;

    invoke-direct {v0}, LvM;-><init>()V

    .line 203
    :goto_0
    return-object v0

    .line 143
    :sswitch_1
    new-instance v0, Lwh;

    invoke-direct {v0}, Lwh;-><init>()V

    goto :goto_0

    .line 147
    :sswitch_2
    new-instance v4, LvW;

    iget-object v0, p0, Lwc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->f:Lbsk;

    .line 150
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lwc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->f:Lbsk;

    .line 148
    invoke-static {v0, v1}, Lwc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGR;

    iget-object v1, p0, Lwc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lwc;

    iget-object v1, v1, Lwc;->i:Lbsk;

    .line 156
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lwc;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lwc;

    iget-object v2, v2, Lwc;->i:Lbsk;

    .line 154
    invoke-static {v1, v2}, Lwc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LvL;

    iget-object v2, p0, Lwc;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lwc;

    iget-object v2, v2, Lwc;->g:Lbsk;

    .line 162
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lwc;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lwc;

    iget-object v3, v3, Lwc;->g:Lbsk;

    .line 160
    invoke-static {v2, v3}, Lwc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lwm;

    iget-object v3, p0, Lwc;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaEV;

    iget-object v3, v3, LaEV;->d:Lbsk;

    .line 168
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, Lwc;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LaEV;

    iget-object v5, v5, LaEV;->d:Lbsk;

    .line 166
    invoke-static {v3, v5}, Lwc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaEP;

    invoke-direct {v4, v0, v1, v2, v3}, LvW;-><init>(LaGR;LvL;Lwm;LaEP;)V

    move-object v0, v4

    .line 173
    goto :goto_0

    .line 175
    :sswitch_3
    new-instance v3, LvP;

    iget-object v0, p0, Lwc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lwc;

    iget-object v0, v0, Lwc;->i:Lbsk;

    .line 178
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lwc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lwc;

    iget-object v1, v1, Lwc;->i:Lbsk;

    .line 176
    invoke-static {v0, v1}, Lwc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvL;

    iget-object v1, p0, Lwc;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->aa:Lbsk;

    .line 184
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lwc;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LCw;

    iget-object v2, v2, LCw;->aa:Lbsk;

    .line 182
    invoke-static {v1, v2}, Lwc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LsC;

    iget-object v2, p0, Lwc;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LCw;

    iget-object v2, v2, LCw;->am:Lbsk;

    .line 190
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, Lwc;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LCw;

    iget-object v4, v4, LCw;->am:Lbsk;

    .line 188
    invoke-static {v2, v4}, Lwc;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LCl;

    invoke-direct {v3, v0, v1, v2}, LvP;-><init>(LvL;LsC;LCl;)V

    move-object v0, v3

    .line 195
    goto/16 :goto_0

    .line 197
    :sswitch_4
    new-instance v0, Lwp;

    invoke-direct {v0}, Lwp;-><init>()V

    goto/16 :goto_0

    .line 201
    :sswitch_5
    new-instance v0, Lwb;

    invoke-direct {v0}, Lwb;-><init>()V

    goto/16 :goto_0

    .line 137
    nop

    :sswitch_data_0
    .sparse-switch
        0x10b -> :sswitch_3
        0x516 -> :sswitch_4
        0x517 -> :sswitch_0
        0x518 -> :sswitch_1
        0x519 -> :sswitch_2
        0x51b -> :sswitch_5
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 220
    sparse-switch p2, :sswitch_data_0

    .line 276
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 222
    :sswitch_0
    check-cast p1, Lwl;

    .line 224
    iget-object v0, p0, Lwc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lwc;

    iget-object v0, v0, Lwc;->e:Lbsk;

    .line 227
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwp;

    .line 224
    invoke-virtual {p1, v0}, Lwl;->provideNavigationPath(Lwp;)Lwm;

    move-result-object v0

    .line 269
    :goto_0
    return-object v0

    .line 231
    :sswitch_1
    check-cast p1, Lwl;

    .line 233
    iget-object v0, p0, Lwc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lwc;

    iget-object v0, v0, Lwc;->f:Lbsk;

    .line 236
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwb;

    .line 233
    invoke-virtual {p1, v0}, Lwl;->provideEntryListSourceSetter(Lwb;)Lwa;

    move-result-object v0

    goto :goto_0

    .line 240
    :sswitch_2
    check-cast p1, Lwl;

    .line 242
    iget-object v0, p0, Lwc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lwc;

    iget-object v0, v0, Lwc;->a:Lbsk;

    .line 245
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvM;

    .line 242
    invoke-virtual {p1, v0}, Lwl;->provideCriterionFactory(LvM;)LvL;

    move-result-object v0

    goto :goto_0

    .line 249
    :sswitch_3
    check-cast p1, Lwl;

    .line 251
    iget-object v0, p0, Lwc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lwc;

    iget-object v0, v0, Lwc;->c:Lbsk;

    .line 254
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvW;

    .line 251
    invoke-virtual {p1, v0}, Lwl;->provideEntryInformationImpl(LvW;)LvU;

    move-result-object v0

    goto :goto_0

    .line 258
    :sswitch_4
    check-cast p1, Lwl;

    .line 260
    iget-object v0, p0, Lwc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lwc;

    iget-object v0, v0, Lwc;->b:Lbsk;

    .line 263
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwh;

    .line 260
    invoke-virtual {p1, v0}, Lwl;->provideListViewState(Lwh;)Lwg;

    move-result-object v0

    goto :goto_0

    .line 267
    :sswitch_5
    check-cast p1, Lwl;

    .line 269
    iget-object v0, p0, Lwc;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lwc;

    iget-object v0, v0, Lwc;->f:Lbsk;

    .line 272
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwb;

    .line 269
    invoke-virtual {p1, v0}, Lwl;->provideEntryListSource(Lwb;)LvY;

    move-result-object v0

    goto :goto_0

    .line 220
    :sswitch_data_0
    .sparse-switch
        0x19 -> :sswitch_2
        0x1f -> :sswitch_0
        0x4f -> :sswitch_3
        0x95 -> :sswitch_1
        0x1da -> :sswitch_5
        0x2e2 -> :sswitch_4
    .end sparse-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 93
    const-class v0, LvM;

    iget-object v1, p0, Lwc;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, Lwc;->a(Ljava/lang/Class;Lbsk;)V

    .line 94
    const-class v0, Lwh;

    iget-object v1, p0, Lwc;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, Lwc;->a(Ljava/lang/Class;Lbsk;)V

    .line 95
    const-class v0, LvW;

    iget-object v1, p0, Lwc;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, Lwc;->a(Ljava/lang/Class;Lbsk;)V

    .line 96
    const-class v0, LvP;

    iget-object v1, p0, Lwc;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, Lwc;->a(Ljava/lang/Class;Lbsk;)V

    .line 97
    const-class v0, Lwp;

    iget-object v1, p0, Lwc;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, Lwc;->a(Ljava/lang/Class;Lbsk;)V

    .line 98
    const-class v0, Lwb;

    iget-object v1, p0, Lwc;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, Lwc;->a(Ljava/lang/Class;Lbsk;)V

    .line 99
    const-class v0, Lwm;

    iget-object v1, p0, Lwc;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, Lwc;->a(Ljava/lang/Class;Lbsk;)V

    .line 100
    const-class v0, Lwa;

    iget-object v1, p0, Lwc;->h:Lbsk;

    invoke-virtual {p0, v0, v1}, Lwc;->a(Ljava/lang/Class;Lbsk;)V

    .line 101
    const-class v0, LvL;

    iget-object v1, p0, Lwc;->i:Lbsk;

    invoke-virtual {p0, v0, v1}, Lwc;->a(Ljava/lang/Class;Lbsk;)V

    .line 102
    const-class v0, LvU;

    iget-object v1, p0, Lwc;->j:Lbsk;

    invoke-virtual {p0, v0, v1}, Lwc;->a(Ljava/lang/Class;Lbsk;)V

    .line 103
    const-class v0, Lwg;

    iget-object v1, p0, Lwc;->k:Lbsk;

    invoke-virtual {p0, v0, v1}, Lwc;->a(Ljava/lang/Class;Lbsk;)V

    .line 104
    const-class v0, LvY;

    iget-object v1, p0, Lwc;->l:Lbsk;

    invoke-virtual {p0, v0, v1}, Lwc;->a(Ljava/lang/Class;Lbsk;)V

    .line 105
    iget-object v0, p0, Lwc;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x517

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 107
    iget-object v0, p0, Lwc;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x518

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 109
    iget-object v0, p0, Lwc;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x519

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 111
    iget-object v0, p0, Lwc;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x10b

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 113
    iget-object v0, p0, Lwc;->e:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x516

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 115
    iget-object v0, p0, Lwc;->f:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x51b

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 117
    iget-object v0, p0, Lwc;->g:Lbsk;

    const-class v1, Lwl;

    const/16 v2, 0x1f

    invoke-virtual {p0, v1, v2}, Lwc;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 119
    iget-object v0, p0, Lwc;->h:Lbsk;

    const-class v1, Lwl;

    const/16 v2, 0x95

    invoke-virtual {p0, v1, v2}, Lwc;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 121
    iget-object v0, p0, Lwc;->i:Lbsk;

    const-class v1, Lwl;

    const/16 v2, 0x19

    invoke-virtual {p0, v1, v2}, Lwc;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 123
    iget-object v0, p0, Lwc;->j:Lbsk;

    const-class v1, Lwl;

    const/16 v2, 0x4f

    invoke-virtual {p0, v1, v2}, Lwc;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 125
    iget-object v0, p0, Lwc;->k:Lbsk;

    const-class v1, Lwl;

    const/16 v2, 0x2e2

    invoke-virtual {p0, v1, v2}, Lwc;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 127
    iget-object v0, p0, Lwc;->l:Lbsk;

    const-class v1, Lwl;

    const/16 v2, 0x1da

    invoke-virtual {p0, v1, v2}, Lwc;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 129
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 212
    .line 214
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 133
    return-void
.end method
