.class public LaGa;
.super LaGe;
.source "DatabaseCollectionEditor.java"


# instance fields
.field private a:J


# direct methods
.method private constructor <init>(LaEz;LaFM;Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3}, LaGe;-><init>(LaEz;LaFM;Landroid/database/Cursor;)V

    .line 20
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LaGa;->a:J

    .line 24
    invoke-virtual {p0}, LaGa;->m()Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 26
    invoke-static {}, LaEu;->a()LaEu;

    move-result-object v0

    invoke-virtual {v0}, LaEu;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 25
    invoke-interface {p3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, LaGa;->a(J)V

    .line 27
    return-void
.end method

.method public constructor <init>(LaEz;LaFM;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 30
    sget-object v0, LaGv;->a:LaGv;

    invoke-virtual {v0}, LaGv;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0, p3}, LaGe;-><init>(LaEz;LaFM;Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LaGa;->a:J

    .line 31
    return-void
.end method

.method public static a(LaEz;LaFM;Landroid/database/Cursor;)LaGa;
    .locals 1

    .prologue
    .line 54
    new-instance v0, LaGa;

    invoke-direct {v0, p0, p1, p2}, LaGa;-><init>(LaEz;LaFM;Landroid/database/Cursor;)V

    .line 55
    return-object v0
.end method

.method private a(J)Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 62
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 63
    sget-object v1, LaEv;->c:LaEv;

    invoke-virtual {v1}, LaEv;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 64
    return-object v0
.end method

.method private a(J)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 44
    cmp-long v0, p1, v2

    if-ltz v0, :cond_0

    iget-wide v0, p0, LaGa;->a:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    iget-wide v0, p0, LaGa;->a:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 46
    iput-wide p1, p0, LaGa;->a:J

    .line 47
    return-void

    .line 44
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 37
    iget-wide v0, p0, LaGa;->a:J

    return-wide v0
.end method

.method public a()LaFZ;
    .locals 1

    .prologue
    .line 87
    invoke-static {p0}, LaFZ;->a(LaGa;)LaFZ;

    move-result-object v0

    return-object v0
.end method

.method protected a()LaGa;
    .locals 1

    .prologue
    .line 98
    invoke-super {p0}, LaGe;->a()LaGe;

    move-result-object v0

    check-cast v0, LaGa;

    return-object v0
.end method

.method public bridge synthetic a()LaGd;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, LaGa;->a()LaFZ;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a()LaGe;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, LaGa;->a()LaGa;

    move-result-object v0

    return-object v0
.end method

.method protected a(LaEz;)V
    .locals 4

    .prologue
    .line 69
    invoke-virtual {p0}, LaGa;->a()J

    move-result-wide v0

    invoke-static {}, LaEu;->a()LaEu;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, LaEz;->a(JLaFy;Landroid/net/Uri;)I

    .line 70
    const-wide/16 v0, -0x1

    invoke-direct {p0, v0, v1}, LaGa;->a(J)V

    .line 71
    return-void
.end method

.method protected a(LaEz;J)V
    .locals 8

    .prologue
    .line 76
    .line 77
    invoke-virtual {p0}, LaGa;->a()J

    move-result-wide v2

    invoke-static {}, LaEu;->a()LaEu;

    move-result-object v4

    .line 78
    invoke-direct {p0, p2, p3}, LaGa;->a(J)Landroid/content/ContentValues;

    move-result-object v5

    const/4 v6, 0x0

    move-object v1, p1

    .line 77
    invoke-virtual/range {v1 .. v6}, LaEz;->a(JLaFy;Landroid/content/ContentValues;Landroid/net/Uri;)J

    move-result-wide v0

    .line 79
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 80
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Error saving document"

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_0
    invoke-direct {p0, v0, v1}, LaGa;->a(J)V

    .line 83
    return-void
.end method

.method public b()LaFZ;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, LaGa;->e()V

    .line 93
    invoke-virtual {p0}, LaGa;->a()LaFZ;

    move-result-object v0

    return-object v0
.end method

.method public b()LaGa;
    .locals 1

    .prologue
    .line 104
    :try_start_0
    invoke-virtual {p0}, LaGa;->a()LaGa;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 105
    :catch_0
    move-exception v0

    .line 106
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public bridge synthetic b()LaGd;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, LaGa;->b()LaFZ;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, LaGa;->a()LaGa;

    move-result-object v0

    return-object v0
.end method
