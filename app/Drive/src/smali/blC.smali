.class LblC;
.super Ljava/lang/Object;
.source "AbstractMapBasedMultimap.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TV;>;"
    }
.end annotation


# instance fields
.field final synthetic a:LblB;

.field final a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation
.end field

.field final a:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LblB;)V
    .locals 2

    .prologue
    .line 452
    iput-object p1, p0, LblC;->a:LblB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450
    iget-object v0, p0, LblC;->a:LblB;

    iget-object v0, v0, LblB;->a:Ljava/util/Collection;

    iput-object v0, p0, LblC;->a:Ljava/util/Collection;

    .line 453
    iget-object v0, p1, LblB;->b:Lbls;

    iget-object v1, p1, LblB;->a:Ljava/util/Collection;

    invoke-static {v0, v1}, Lbls;->a(Lbls;Ljava/util/Collection;)Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, LblC;->a:Ljava/util/Iterator;

    .line 454
    return-void
.end method

.method constructor <init>(LblB;Ljava/util/Iterator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 456
    iput-object p1, p0, LblC;->a:LblB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450
    iget-object v0, p0, LblC;->a:LblB;

    iget-object v0, v0, LblB;->a:Ljava/util/Collection;

    iput-object v0, p0, LblC;->a:Ljava/util/Collection;

    .line 457
    iput-object p2, p0, LblC;->a:Ljava/util/Iterator;

    .line 458
    return-void
.end method


# virtual methods
.method a()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 488
    invoke-virtual {p0}, LblC;->a()V

    .line 489
    iget-object v0, p0, LblC;->a:Ljava/util/Iterator;

    return-object v0
.end method

.method a()V
    .locals 2

    .prologue
    .line 465
    iget-object v0, p0, LblC;->a:LblB;

    invoke-virtual {v0}, LblB;->a()V

    .line 466
    iget-object v0, p0, LblC;->a:LblB;

    iget-object v0, v0, LblB;->a:Ljava/util/Collection;

    iget-object v1, p0, LblC;->a:Ljava/util/Collection;

    if-eq v0, v1, :cond_0

    .line 467
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 469
    :cond_0
    return-void
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 472
    invoke-virtual {p0}, LblC;->a()V

    .line 473
    iget-object v0, p0, LblC;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 477
    invoke-virtual {p0}, LblC;->a()V

    .line 478
    iget-object v0, p0, LblC;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, LblC;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 483
    iget-object v0, p0, LblC;->a:LblB;

    iget-object v0, v0, LblB;->b:Lbls;

    invoke-static {v0}, Lbls;->a(Lbls;)I

    .line 484
    iget-object v0, p0, LblC;->a:LblB;

    invoke-virtual {v0}, LblB;->b()V

    .line 485
    return-void
.end method
