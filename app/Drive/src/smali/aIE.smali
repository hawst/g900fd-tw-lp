.class public LaIE;
.super Ljava/lang/Object;
.source "GuidCrypter.java"


# instance fields
.field private final a:Lakz;

.field private final a:Landroid/content/Context;

.field private a:Ljavax/crypto/SecretKey;

.field private a:[B

.field private b:[B


# direct methods
.method public constructor <init>(Landroid/content/Context;Lakz;)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p1, p0, LaIE;->a:Landroid/content/Context;

    .line 83
    iput-object p2, p0, LaIE;->a:Lakz;

    .line 84
    return-void
.end method

.method private a()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 122
    iget-object v0, p0, LaIE;->a:Landroid/content/Context;

    const-string v1, "DocumentContentSpecCrypter.GuidKeyStore.V2"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 5

    .prologue
    .line 149
    const-class v1, LaIE;

    monitor-enter v1

    .line 150
    :try_start_0
    iget-object v0, p0, LaIE;->a:Ljavax/crypto/SecretKey;

    if-eqz v0, :cond_0

    .line 151
    monitor-exit v1

    .line 170
    :goto_0
    return-void

    .line 153
    :cond_0
    invoke-direct {p0}, LaIE;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 154
    invoke-direct {p0, v0}, LaIE;->a(Landroid/content/SharedPreferences;)V

    .line 155
    iget-object v2, p0, LaIE;->a:Ljavax/crypto/SecretKey;

    if-eqz v2, :cond_1

    .line 156
    monitor-exit v1

    goto :goto_0

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 159
    :cond_1
    :try_start_1
    iget-object v2, p0, LaIE;->a:Lakz;

    invoke-interface {v2}, Lakz;->a()Ljavax/crypto/SecretKey;

    move-result-object v2

    iput-object v2, p0, LaIE;->a:Ljavax/crypto/SecretKey;

    .line 160
    const/16 v2, 0x10

    new-array v2, v2, [B

    iput-object v2, p0, LaIE;->a:[B

    .line 161
    const/16 v2, 0x14

    new-array v2, v2, [B

    iput-object v2, p0, LaIE;->b:[B

    .line 162
    new-instance v2, Ljava/security/SecureRandom;

    invoke-direct {v2}, Ljava/security/SecureRandom;-><init>()V

    .line 163
    iget-object v3, p0, LaIE;->a:[B

    invoke-virtual {v2, v3}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 164
    iget-object v3, p0, LaIE;->b:[B

    invoke-virtual {v2, v3}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 166
    iget-object v2, p0, LaIE;->a:Ljavax/crypto/SecretKey;

    iget-object v3, p0, LaIE;->b:[B

    iget-object v4, p0, LaIE;->a:[B

    invoke-direct {p0, v2, v3, v4, v0}, LaIE;->a(Ljavax/crypto/SecretKey;[B[BLandroid/content/SharedPreferences;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 167
    new-instance v0, LQm;

    invoke-direct {v0}, LQm;-><init>()V

    throw v0

    .line 169
    :cond_2
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private a(Landroid/content/SharedPreferences;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 88
    const-string v0, "algorithm"

    invoke-interface {p1, v0, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 89
    const-string v1, "encodedForm"

    invoke-interface {p1, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 90
    const-string v2, "ivBytes"

    invoke-interface {p1, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 91
    const-string v3, "macKey"

    invoke-interface {p1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 93
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    if-nez v3, :cond_1

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 98
    :cond_1
    invoke-static {v1}, LaID;->a(Ljava/lang/String;)[B

    move-result-object v1

    .line 99
    new-instance v4, Ljavax/crypto/spec/SecretKeySpec;

    invoke-direct {v4, v1, v0}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    iput-object v4, p0, LaIE;->a:Ljavax/crypto/SecretKey;

    .line 100
    invoke-static {v2}, LaID;->a(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, LaIE;->a:[B

    .line 101
    invoke-static {v3}, LaID;->a(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, LaIE;->b:[B

    goto :goto_0
.end method

.method private a(Ljavax/crypto/SecretKey;[B[BLandroid/content/SharedPreferences;)Z
    .locals 4

    .prologue
    .line 110
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    invoke-interface {p1}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 113
    invoke-interface {p4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 114
    const-string v2, "algorithm"

    invoke-interface {p1}, Ljavax/crypto/SecretKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 115
    const-string v2, "encodedForm"

    invoke-static {v0}, LaID;->a([B)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 116
    const-string v0, "macKey"

    invoke-static {p2}, LaID;->a([B)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 117
    const-string v0, "ivBytes"

    invoke-static {p3}, LaID;->a([B)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 118
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()Ljavax/crypto/SecretKey;
    .locals 1

    .prologue
    .line 173
    invoke-direct {p0}, LaIE;->a()V

    .line 174
    iget-object v0, p0, LaIE;->a:Ljavax/crypto/SecretKey;

    return-object v0
.end method

.method public a()[B
    .locals 1

    .prologue
    .line 178
    invoke-direct {p0}, LaIE;->a()V

    .line 179
    iget-object v0, p0, LaIE;->b:[B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method public b()[B
    .locals 1

    .prologue
    .line 183
    invoke-direct {p0}, LaIE;->a()V

    .line 184
    iget-object v0, p0, LaIE;->a:[B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method
