.class public final enum LbdE;
.super Ljava/lang/Enum;
.source "MediaHttpUploader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LbdE;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LbdE;

.field private static final synthetic a:[LbdE;

.field public static final enum b:LbdE;

.field public static final enum c:LbdE;

.field public static final enum d:LbdE;

.field public static final enum e:LbdE;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 113
    new-instance v0, LbdE;

    const-string v1, "NOT_STARTED"

    invoke-direct {v0, v1, v2}, LbdE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbdE;->a:LbdE;

    .line 116
    new-instance v0, LbdE;

    const-string v1, "INITIATION_STARTED"

    invoke-direct {v0, v1, v3}, LbdE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbdE;->b:LbdE;

    .line 119
    new-instance v0, LbdE;

    const-string v1, "INITIATION_COMPLETE"

    invoke-direct {v0, v1, v4}, LbdE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbdE;->c:LbdE;

    .line 122
    new-instance v0, LbdE;

    const-string v1, "MEDIA_IN_PROGRESS"

    invoke-direct {v0, v1, v5}, LbdE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbdE;->d:LbdE;

    .line 125
    new-instance v0, LbdE;

    const-string v1, "MEDIA_COMPLETE"

    invoke-direct {v0, v1, v6}, LbdE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbdE;->e:LbdE;

    .line 111
    const/4 v0, 0x5

    new-array v0, v0, [LbdE;

    sget-object v1, LbdE;->a:LbdE;

    aput-object v1, v0, v2

    sget-object v1, LbdE;->b:LbdE;

    aput-object v1, v0, v3

    sget-object v1, LbdE;->c:LbdE;

    aput-object v1, v0, v4

    sget-object v1, LbdE;->d:LbdE;

    aput-object v1, v0, v5

    sget-object v1, LbdE;->e:LbdE;

    aput-object v1, v0, v6

    sput-object v0, LbdE;->a:[LbdE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 111
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LbdE;
    .locals 1

    .prologue
    .line 111
    const-class v0, LbdE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LbdE;

    return-object v0
.end method

.method public static values()[LbdE;
    .locals 1

    .prologue
    .line 111
    sget-object v0, LbdE;->a:[LbdE;

    invoke-virtual {v0}, [LbdE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LbdE;

    return-object v0
.end method
