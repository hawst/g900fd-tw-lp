.class public LanV;
.super Ljava/lang/Object;
.source "ChainedImageDownloadFetcher.java"


# instance fields
.field private final a:I

.field final synthetic a:LanU;

.field private final a:LanX;

.field private final a:Ljava/lang/String;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LakH",
            "<",
            "Ljava/lang/Long;",
            "*>;>;"
        }
    .end annotation
.end field

.field private final b:I

.field private final b:Ljava/lang/String;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LalD",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final c:I

.field private final d:I


# direct methods
.method constructor <init>(LanU;Ljava/lang/String;LanX;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 139
    iput-object p1, p0, LanV;->a:LanU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LanV;->a:Ljava/util/List;

    .line 123
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LanV;->b:Ljava/util/List;

    .line 125
    const/16 v0, 0x32

    iput v0, p0, LanV;->a:I

    .line 126
    const/4 v0, 0x5

    iput v0, p0, LanV;->b:I

    .line 127
    const/4 v0, 0x1

    iput v0, p0, LanV;->c:I

    .line 140
    iput p5, p0, LanV;->d:I

    .line 141
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LanV;->a:Ljava/lang/String;

    .line 142
    iput-object p3, p0, LanV;->a:LanX;

    .line 143
    iput-object p4, p0, LanV;->b:Ljava/lang/String;

    .line 144
    return-void
.end method

.method private a()LanN;
    .locals 3

    .prologue
    .line 182
    .line 183
    invoke-direct {p0}, LanV;->a()LaoH;

    move-result-object v0

    .line 184
    iget-object v1, p0, LanV;->a:LanU;

    .line 185
    invoke-static {v1}, LanU;->a(LanU;)LakR;

    move-result-object v1

    iget-object v2, p0, LanV;->a:Ljava/lang/String;

    .line 184
    invoke-static {v1, v2, v0}, LanN;->a(LakR;Ljava/lang/String;Laoo;)LanN;

    move-result-object v0

    .line 186
    return-object v0
.end method

.method private a()LaoH;
    .locals 4

    .prologue
    .line 173
    .line 174
    invoke-direct {p0}, LanV;->a()Laok;

    move-result-object v0

    .line 176
    const/4 v1, 0x1

    invoke-direct {p0, v1}, LanV;->a(I)LaoX;

    move-result-object v1

    .line 177
    iget-object v2, p0, LanV;->a:LanU;

    invoke-static {v2}, LanU;->a(LanU;)LaoJ;

    move-result-object v2

    iget-object v3, p0, LanV;->a:LanX;

    invoke-static {v3}, LanX;->a(LanX;)Z

    move-result v3

    invoke-virtual {v2, v3, v1, v0}, LaoJ;->a(ZLaop;Laoo;)LaoH;

    move-result-object v0

    return-object v0
.end method

.method private a(I)LaoX;
    .locals 3

    .prologue
    .line 147
    const-wide/16 v0, 0x0

    const/16 v2, 0x32

    .line 148
    invoke-static {v0, v1, v2}, LakH;->a(JI)LakH;

    move-result-object v0

    .line 149
    iget-object v1, p0, LanV;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    invoke-static {p1}, Lalg;->a(I)LbsW;

    move-result-object v1

    invoke-static {v1, v0}, LalE;->a(Ljava/util/concurrent/ExecutorService;Lalf;)LalE;

    move-result-object v0

    .line 153
    iget-object v1, p0, LanV;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    new-instance v1, LaoX;

    invoke-direct {v1, v0}, LaoX;-><init>(LalD;)V

    return-object v1
.end method

.method private a()Laok;
    .locals 3

    .prologue
    .line 167
    invoke-direct {p0}, LanV;->a()Lapj;

    move-result-object v0

    .line 169
    iget-object v1, p0, LanV;->a:LanU;

    invoke-static {v1}, LanU;->a(LanU;)Laom;

    move-result-object v1

    iget-object v2, p0, LanV;->a:LanU;

    invoke-static {v2}, LanU;->a(LanU;)LZS;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Laom;->a(Laoo;LZS;)Laok;

    move-result-object v0

    return-object v0
.end method

.method private a()Lapj;
    .locals 5

    .prologue
    .line 159
    const/4 v0, 0x5

    invoke-direct {p0, v0}, LanV;->a(I)LaoX;

    move-result-object v0

    .line 161
    iget-object v1, p0, LanV;->a:LanU;

    invoke-static {v1}, LanU;->a(LanU;)Lapl;

    move-result-object v1

    iget-object v2, p0, LanV;->a:LanX;

    .line 162
    invoke-virtual {v2}, LanX;->a()Z

    move-result v2

    iget-object v3, p0, LanV;->a:LanU;

    invoke-static {v3}, LanU;->a(LanU;)LZS;

    move-result-object v3

    iget-object v4, p0, LanV;->b:Ljava/lang/String;

    .line 161
    invoke-virtual {v1, v0, v2, v3, v4}, Lapl;->a(Laop;ZLZS;Ljava/lang/String;)Lapj;

    move-result-object v0

    .line 163
    return-object v0
.end method


# virtual methods
.method a()LanS;
    .locals 6

    .prologue
    .line 193
    invoke-direct {p0}, LanV;->a()LanN;

    move-result-object v2

    .line 196
    iget v0, p0, LanV;->d:I

    if-eqz v0, :cond_0

    .line 197
    new-instance v0, LanY;

    iget v1, p0, LanV;->d:I

    iget-object v3, p0, LanV;->a:LanU;

    .line 198
    invoke-static {v3}, LanU;->b(LanU;)LZS;

    move-result-object v3

    invoke-direct {v0, v2, v1, v3}, LanY;-><init>(Laoo;ILZS;)V

    .line 207
    :goto_0
    const/4 v1, 0x1

    invoke-direct {p0, v1}, LanV;->a(I)LaoX;

    move-result-object v3

    .line 209
    new-instance v1, LanW;

    invoke-direct {v1, p0, v0, v3}, LanW;-><init>(LanV;Laoo;Laop;)V

    .line 218
    iget-object v0, p0, LanV;->a:Ljava/util/List;

    iget-object v3, p0, LanV;->a:LanU;

    invoke-static {v3}, LanU;->a(LanU;)LaoG;

    move-result-object v3

    invoke-virtual {v3}, LaoG;->a()LakH;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 220
    new-instance v0, LanS;

    iget-object v3, p0, LanV;->b:Ljava/util/List;

    iget-object v4, p0, LanV;->a:Ljava/util/List;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LanS;-><init>(Laoo;LanN;Ljava/util/List;Ljava/util/List;LanT;)V

    return-object v0

    :cond_0
    move-object v0, v2

    .line 201
    goto :goto_0
.end method
