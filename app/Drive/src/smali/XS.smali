.class public LXS;
.super Ljava/lang/Object;
.source "PunchSimpleSvgViewer.java"

# interfaces
.implements LXQ;


# instance fields
.field a:LTO;

.field a:LXD;

.field private a:LXU;

.field a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "LXR;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method


# virtual methods
.method a(ILcom/google/android/apps/docs/punchwebview/PunchSvgWebView;LXP;)LXB;
    .locals 1

    .prologue
    .line 81
    if-nez p2, :cond_0

    .line 82
    const/4 v0, 0x0

    .line 85
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p2, p1, p3}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(ILXP;)LXB;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/widget/FrameLayout;LYO;LXR;LqK;)Landroid/webkit/WebView;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 90
    iget-object v0, p0, LXS;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    if-eqz v0, :cond_0

    .line 91
    const-string v0, "PunchStateSvgViewer"

    const-string v1, "SKIPPING Creation of svgView as not null"

    invoke-static {v0, v1}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    iget-object v0, p0, LXS;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    .line 107
    :goto_0
    return-object v0

    .line 95
    :cond_0
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LbiT;->b(Z)V

    .line 96
    iget-object v1, p0, LXS;->a:LXD;

    iget-object v2, p0, LXS;->a:LTO;

    iget-object v6, p0, LXS;->a:Laja;

    move-object v0, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v7, p0

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Landroid/view/ViewGroup;LXD;LTO;LYO;LXR;LqK;Lbxw;LXQ;)Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    move-result-object v0

    iput-object v0, p0, LXS;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    .line 105
    iget-object v0, p0, LXS;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-virtual {v0, v8}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->setIsZoomable(Z)V

    .line 107
    iget-object v0, p0, LXS;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    goto :goto_0

    :cond_1
    move v0, v8

    .line 95
    goto :goto_1
.end method

.method public a()V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, LXS;->a:LXU;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 126
    const/4 v0, 0x0

    iput-object v0, p0, LXS;->a:LXU;

    .line 127
    return-void

    .line 125
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(ILcom/google/android/apps/docs/punchwebview/PunchSvgWebView;LXP;)V
    .locals 3

    .prologue
    .line 135
    .line 136
    invoke-virtual {p0, p1, p2, p3}, LXS;->a(ILcom/google/android/apps/docs/punchwebview/PunchSvgWebView;LXP;)LXB;

    move-result-object v0

    .line 137
    if-eqz v0, :cond_0

    .line 138
    sget-object v1, LXT;->a:[I

    invoke-virtual {p3}, LXP;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 140
    :pswitch_0
    iget-object v1, p0, LXS;->a:LXD;

    invoke-virtual {v1, v0}, LXD;->b(LXB;)V

    goto :goto_0

    .line 143
    :pswitch_1
    iget-object v1, p0, LXS;->a:LXD;

    invoke-virtual {v1, v0}, LXD;->a(LXB;)V

    goto :goto_0

    .line 146
    :pswitch_2
    iget-object v1, p0, LXS;->a:LXD;

    invoke-virtual {v1, v0}, LXD;->c(LXB;)V

    goto :goto_0

    .line 138
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(LXU;)V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, LXS;->a:LXU;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 121
    iput-object p1, p0, LXS;->a:LXU;

    .line 122
    return-void

    .line 120
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, LXS;->a:LXU;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, LXS;->a:LXU;

    invoke-interface {v0}, LXU;->a()V

    .line 117
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, LXS;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, LXS;->a:Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Ljava/lang/String;)V

    .line 72
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x1

    return v0
.end method
