.class final Lbom;
.super Ljava/util/AbstractSet;
.source "MapMakerInternalMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractSet",
        "<",
        "Ljava/util/Map$Entry",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:LbnY;


# direct methods
.method constructor <init>(LbnY;)V
    .locals 0

    .prologue
    .line 3725
    iput-object p1, p0, Lbom;->a:LbnY;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 3769
    iget-object v0, p0, Lbom;->a:LbnY;

    invoke-virtual {v0}, LbnY;->clear()V

    .line 3770
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 3734
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-nez v1, :cond_1

    .line 3744
    :cond_0
    :goto_0
    return v0

    .line 3737
    :cond_1
    check-cast p1, Ljava/util/Map$Entry;

    .line 3738
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 3739
    if-eqz v1, :cond_0

    .line 3742
    iget-object v2, p0, Lbom;->a:LbnY;

    invoke-virtual {v2, v1}, LbnY;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 3744
    if-eqz v1, :cond_0

    iget-object v2, p0, Lbom;->a:LbnY;

    iget-object v2, v2, LbnY;->b:LbiD;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, LbiD;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 3764
    iget-object v0, p0, Lbom;->a:LbnY;

    invoke-virtual {v0}, LbnY;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 3729
    new-instance v0, Lbol;

    iget-object v1, p0, Lbom;->a:LbnY;

    invoke-direct {v0, v1}, Lbol;-><init>(LbnY;)V

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 3749
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-nez v1, :cond_1

    .line 3754
    :cond_0
    :goto_0
    return v0

    .line 3752
    :cond_1
    check-cast p1, Ljava/util/Map$Entry;

    .line 3753
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 3754
    if-eqz v1, :cond_0

    iget-object v2, p0, Lbom;->a:LbnY;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LbnY;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 3759
    iget-object v0, p0, Lbom;->a:LbnY;

    invoke-virtual {v0}, LbnY;->size()I

    move-result v0

    return v0
.end method
