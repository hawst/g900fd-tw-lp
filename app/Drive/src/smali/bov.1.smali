.class final Lbov;
.super Ljava/util/AbstractSet;
.source "MapMakerInternalMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractSet",
        "<TK;>;"
    }
.end annotation


# instance fields
.field final synthetic a:LbnY;


# direct methods
.method constructor <init>(LbnY;)V
    .locals 0

    .prologue
    .line 3664
    iput-object p1, p0, Lbov;->a:LbnY;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 3693
    iget-object v0, p0, Lbov;->a:LbnY;

    invoke-virtual {v0}, LbnY;->clear()V

    .line 3694
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 3683
    iget-object v0, p0, Lbov;->a:LbnY;

    invoke-virtual {v0, p1}, LbnY;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 3678
    iget-object v0, p0, Lbov;->a:LbnY;

    invoke-virtual {v0}, LbnY;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 3668
    new-instance v0, Lbou;

    iget-object v1, p0, Lbov;->a:LbnY;

    invoke-direct {v0, v1}, Lbou;-><init>(LbnY;)V

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 3688
    iget-object v0, p0, Lbov;->a:LbnY;

    invoke-virtual {v0, p1}, LbnY;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 3673
    iget-object v0, p0, Lbov;->a:LbnY;

    invoke-virtual {v0}, LbnY;->size()I

    move-result v0

    return v0
.end method
