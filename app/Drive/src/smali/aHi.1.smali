.class public LaHi;
.super Ljava/lang/Object;
.source "AppliedOperation.java"


# instance fields
.field private final a:LaHo;

.field protected final a:Ljava/lang/String;

.field private final b:LaHo;


# direct methods
.method public constructor <init>(LaHo;LaHo;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, LaHi;->a:LaHo;

    .line 35
    iput-object p2, p0, LaHi;->b:LaHo;

    .line 36
    iput-object p3, p0, LaHi;->a:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public static a(LaFM;LaGg;Lorg/json/JSONObject;)LaHi;
    .locals 4

    .prologue
    .line 58
    const-string v0, "Forward"

    .line 59
    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {p0, p1, v0}, LaHz;->a(LaFM;LaGg;Lorg/json/JSONObject;)LaHo;

    move-result-object v1

    .line 60
    const-string v0, "Reverse"

    .line 61
    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {p0, p1, v0}, LaHz;->a(LaFM;LaGg;Lorg/json/JSONObject;)LaHo;

    move-result-object v2

    .line 62
    const-string v0, "eTag"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "eTag"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 63
    :goto_0
    new-instance v3, LaHi;

    invoke-direct {v3, v1, v2, v0}, LaHi;-><init>(LaHo;LaHo;Ljava/lang/String;)V

    return-object v3

    .line 62
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()LaHo;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, LaHi;->a:LaHo;

    return-object v0
.end method

.method public a()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 67
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 68
    const-string v1, "eTag"

    iget-object v2, p0, LaHi;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 69
    const-string v1, "Forward"

    invoke-virtual {p0}, LaHi;->a()LaHo;

    move-result-object v2

    invoke-interface {v2}, LaHo;->a()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 70
    const-string v1, "Reverse"

    invoke-virtual {p0}, LaHi;->b()LaHo;

    move-result-object v2

    invoke-interface {v2}, LaHo;->a()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 71
    return-object v0
.end method

.method public b()LaHo;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, LaHi;->b:LaHo;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 76
    const-string v0, "AppliedOperation[%s, undo=%s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LaHi;->a:LaHo;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LaHi;->b:LaHo;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
