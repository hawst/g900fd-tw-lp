.class Lbhv;
.super Ljava/lang/Object;
.source "EditorActionBarHoneycomb.java"


# instance fields
.field private final a:Landroid/view/MenuItem;

.field final synthetic a:Lbhu;

.field private final a:[Landroid/view/MenuItem;

.field private final b:Landroid/view/MenuItem;

.field private final c:Landroid/view/MenuItem;

.field private final d:Landroid/view/MenuItem;


# direct methods
.method constructor <init>(Lbhu;Landroid/view/Menu;)V
    .locals 3

    .prologue
    .line 36
    iput-object p1, p0, Lbhv;->a:Lbhu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_menu_edit_page:I

    invoke-interface {p2, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lbhw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    iput-object v0, p0, Lbhv;->a:Landroid/view/MenuItem;

    .line 38
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_menu_delete_page:I

    invoke-interface {p2, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lbhw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    iput-object v0, p0, Lbhv;->b:Landroid/view/MenuItem;

    .line 39
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_menu_enhance:I

    invoke-interface {p2, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lbhw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    iput-object v0, p0, Lbhv;->c:Landroid/view/MenuItem;

    .line 40
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_menu_rename_scan:I

    invoke-interface {p2, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lbhw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    iput-object v0, p0, Lbhv;->d:Landroid/view/MenuItem;

    .line 42
    invoke-static {}, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->values()[Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [Landroid/view/MenuItem;

    iput-object v0, p0, Lbhv;->a:[Landroid/view/MenuItem;

    .line 43
    iget-object v1, p0, Lbhv;->a:[Landroid/view/MenuItem;

    sget-object v0, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->NONE:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->ordinal()I

    move-result v2

    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_menu_enhance_none:I

    .line 44
    invoke-interface {p2, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lbhw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    aput-object v0, v1, v2

    .line 45
    iget-object v1, p0, Lbhv;->a:[Landroid/view/MenuItem;

    sget-object v0, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->OPTIMIZE_FOR_COLOR:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->ordinal()I

    move-result v2

    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_menu_enhance_color:I

    .line 46
    invoke-interface {p2, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lbhw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    aput-object v0, v1, v2

    .line 47
    iget-object v1, p0, Lbhv;->a:[Landroid/view/MenuItem;

    sget-object v0, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->OPTIMIZE_FOR_BW:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->ordinal()I

    move-result v2

    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_menu_enhance_bw:I

    .line 48
    invoke-interface {p2, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lbhw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    aput-object v0, v1, v2

    .line 49
    iget-object v1, p0, Lbhv;->a:[Landroid/view/MenuItem;

    sget-object v0, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->OPTIMIZE_FOR_COLOR_DRAWING:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->ordinal()I

    move-result v2

    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_menu_enhance_color_drawing:I

    .line 50
    invoke-interface {p2, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {v0}, Lbhw;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    aput-object v0, v1, v2

    .line 51
    return-void
.end method


# virtual methods
.method a(Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 68
    move v0, v1

    :goto_0
    invoke-static {}, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->values()[Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    move-result-object v2

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 69
    iget-object v2, p0, Lbhv;->a:[Landroid/view/MenuItem;

    aget-object v3, v2, v0

    .line 70
    if-eqz v3, :cond_2

    .line 71
    invoke-virtual {p1}, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->ordinal()I

    move-result v2

    if-ne v2, v0, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 68
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v2, v1

    .line 71
    goto :goto_1

    .line 72
    :cond_2
    invoke-virtual {p1}, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->ordinal()I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 73
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported enhancement method encountered: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 74
    invoke-virtual {p1}, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :cond_3
    return-void
.end method

.method a(Z)V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lbhv;->a:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 55
    iget-object v0, p0, Lbhv;->b:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 56
    iget-object v0, p0, Lbhv;->c:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 57
    iget-object v0, p0, Lbhv;->d:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 58
    return-void
.end method

.method b(Z)V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lbhv;->a:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 62
    iget-object v0, p0, Lbhv;->b:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 63
    iget-object v0, p0, Lbhv;->c:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 64
    iget-object v0, p0, Lbhv;->d:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 65
    return-void
.end method
