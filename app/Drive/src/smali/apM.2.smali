.class public LapM;
.super Ljava/lang/Object;
.source "GestureImageView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:F

.field private final a:J

.field private final a:Landroid/view/animation/Interpolator;

.field final synthetic a:Lcom/google/android/apps/docs/view/GestureImageView;

.field private final a:Z

.field private final b:F

.field private b:Z

.field private final c:F

.field private final d:F

.field private final e:F

.field private final f:F

.field private final g:F


# direct methods
.method private constructor <init>(Lcom/google/android/apps/docs/view/GestureImageView;FLandroid/graphics/Matrix;Landroid/graphics/Matrix;Landroid/view/animation/Interpolator;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 79
    iput-object p1, p0, LapM;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LapM;->a:J

    .line 73
    iput-boolean v2, p0, LapM;->b:Z

    .line 80
    iput p2, p0, LapM;->a:F

    .line 81
    invoke-static {p3}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Landroid/graphics/Matrix;)[F

    move-result-object v0

    .line 82
    aget v1, v0, v2

    iput v1, p0, LapM;->d:F

    .line 83
    aget v0, v0, v3

    iput v0, p0, LapM;->e:F

    .line 84
    invoke-static {p3}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Landroid/graphics/Matrix;)F

    move-result v0

    iput v0, p0, LapM;->b:F

    .line 85
    invoke-static {p4}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Landroid/graphics/Matrix;)[F

    move-result-object v0

    .line 86
    aget v1, v0, v2

    iput v1, p0, LapM;->f:F

    .line 87
    aget v0, v0, v3

    iput v0, p0, LapM;->g:F

    .line 88
    invoke-static {p4}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Landroid/graphics/Matrix;)F

    move-result v0

    iput v0, p0, LapM;->c:F

    .line 89
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/animation/Interpolator;

    iput-object v0, p0, LapM;->a:Landroid/view/animation/Interpolator;

    .line 90
    iput-boolean p6, p0, LapM;->a:Z

    .line 91
    invoke-static {p1}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 92
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/docs/view/GestureImageView;FLandroid/graphics/Matrix;Landroid/graphics/Matrix;Landroid/view/animation/Interpolator;ZLapL;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct/range {p0 .. p6}, LapM;-><init>(Lcom/google/android/apps/docs/view/GestureImageView;FLandroid/graphics/Matrix;Landroid/graphics/Matrix;Landroid/view/animation/Interpolator;Z)V

    return-void
.end method

.method private a(F)V
    .locals 5

    .prologue
    .line 109
    iget-object v0, p0, LapM;->a:Landroid/view/animation/Interpolator;

    invoke-interface {v0, p1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    .line 110
    iget v1, p0, LapM;->c:F

    iget v2, p0, LapM;->b:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, v0

    iget v2, p0, LapM;->b:F

    add-float/2addr v1, v2

    .line 111
    iget v2, p0, LapM;->f:F

    iget v3, p0, LapM;->d:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, v0

    iget v3, p0, LapM;->d:F

    add-float/2addr v2, v3

    .line 112
    iget v3, p0, LapM;->g:F

    iget v4, p0, LapM;->e:F

    sub-float/2addr v3, v4

    mul-float/2addr v0, v3

    iget v3, p0, LapM;->e:F

    add-float/2addr v0, v3

    .line 113
    iget-object v3, p0, LapM;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    invoke-static {v3, v4}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    .line 114
    iget-object v3, p0, LapM;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v3}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {v3, v1, v1}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 115
    iget-object v1, p0, LapM;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v1}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v1, v2, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 116
    iget-boolean v0, p0, LapM;->a:Z

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, LapM;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    iget-object v1, p0, LapM;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v1}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/graphics/Matrix;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;Landroid/graphics/Matrix;)V

    .line 119
    :cond_0
    iget-object v0, p0, LapM;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/GestureImageView;->invalidate()V

    .line 120
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x1

    iput-boolean v0, p0, LapM;->b:Z

    .line 124
    iget-object v0, p0, LapM;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v0}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 125
    return-void
.end method

.method public run()V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 96
    iget-boolean v0, p0, LapM;->b:Z

    if-eqz v0, :cond_1

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 99
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, LapM;->a:J

    sub-long/2addr v0, v2

    long-to-float v0, v0

    .line 100
    iget v1, p0, LapM;->a:F

    div-float/2addr v0, v1

    invoke-static {v4, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 102
    invoke-direct {p0, v0}, LapM;->a(F)V

    .line 103
    cmpg-float v0, v0, v4

    if-gez v0, :cond_0

    .line 104
    iget-object v0, p0, LapM;->a:Lcom/google/android/apps/docs/view/GestureImageView;

    invoke-static {v0}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0xa

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
