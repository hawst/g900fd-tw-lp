.class public LIL;
.super Ljava/lang/Object;
.source "SortValue.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "LIL;",
        ">;"
    }
.end annotation


# static fields
.field private static a:Lbjv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjv",
            "<",
            "Ljava/text/Collator;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, LIM;

    invoke-direct {v0}, LIM;-><init>()V

    invoke-static {v0}, Lbjw;->a(Lbjv;)Lbjv;

    move-result-object v0

    sput-object v0, LIL;->a:Lbjv;

    return-void
.end method

.method private constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, LIL;->a:Ljava/util/List;

    .line 53
    return-void
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 71
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Ljava/lang/Comparable;

    if-eqz v0, :cond_0

    .line 73
    check-cast p1, Ljava/lang/Comparable;

    .line 75
    invoke-interface {p1, p2}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    .line 79
    :goto_0
    return v0

    :cond_0
    sget-object v0, LIL;->a:Lbjv;

    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/Collator;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public static varargs a([Ljava/lang/Object;)LIL;
    .locals 2

    .prologue
    .line 48
    new-instance v0, LIL;

    invoke-static {p0}, LbmF;->a([Ljava/lang/Object;)LbmF;

    move-result-object v1

    invoke-direct {v0, v1}, LIL;-><init>(Ljava/util/List;)V

    return-object v0
.end method


# virtual methods
.method public a(LIL;)I
    .locals 4

    .prologue
    .line 57
    iget-object v0, p0, LIL;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p1, LIL;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 59
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 60
    iget-object v0, p0, LIL;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v3, p1, LIL;->a:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-direct {p0, v0, v3}, LIL;->a(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 61
    if-eqz v0, :cond_0

    .line 67
    :goto_1
    return v0

    .line 59
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 66
    :cond_1
    iget-object v0, p0, LIL;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p1, LIL;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v0, v1

    .line 67
    goto :goto_1
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 30
    check-cast p1, LIL;

    invoke-virtual {p0, p1}, LIL;->a(LIL;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 84
    if-ne p0, p1, :cond_0

    .line 85
    const/4 v0, 0x1

    .line 90
    :goto_0
    return v0

    .line 86
    :cond_0
    instance-of v0, p1, LIL;

    if-nez v0, :cond_1

    .line 87
    const/4 v0, 0x0

    goto :goto_0

    .line 89
    :cond_1
    check-cast p1, LIL;

    .line 90
    iget-object v0, p0, LIL;->a:Ljava/util/List;

    iget-object v1, p1, LIL;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, LIL;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 101
    const-class v0, LIL;

    invoke-static {v0}, LbiL;->a(Ljava/lang/Class;)LbiN;

    move-result-object v0

    const-string v1, "elements"

    iget-object v2, p0, LIL;->a:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;Ljava/lang/Object;)LbiN;

    move-result-object v0

    invoke-virtual {v0}, LbiN;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
