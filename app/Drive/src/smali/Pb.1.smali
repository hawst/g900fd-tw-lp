.class public LPb;
.super Ljava/lang/Object;
.source "WarmWelcomeLauncher.java"

# interfaces
.implements LatC;


# instance fields
.field a:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 21
    iget-object v0, p0, LPb;->a:Landroid/content/Context;

    new-instance v1, LxJ;

    invoke-direct {v1}, LxJ;-><init>()V

    sget v2, LPa;->splash:I

    .line 22
    invoke-virtual {v1, v2}, LxJ;->a(I)LxJ;

    move-result-object v1

    sget v2, LPa;->page_1:I

    sget v3, LOZ;->warm_welcome_page_1:I

    .line 23
    invoke-virtual {v1, v2, v3}, LxJ;->a(II)LxJ;

    move-result-object v1

    sget v2, LPa;->page_2:I

    sget v3, LOZ;->warm_welcome_page_2:I

    .line 24
    invoke-virtual {v1, v2, v3}, LxJ;->a(II)LxJ;

    move-result-object v1

    sget v2, LPa;->page_3:I

    sget v3, LOZ;->warm_welcome_page_3:I

    .line 25
    invoke-virtual {v1, v2, v3}, LxJ;->a(II)LxJ;

    move-result-object v1

    .line 21
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a(Landroid/content/Context;LxJ;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
