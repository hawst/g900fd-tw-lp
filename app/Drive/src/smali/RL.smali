.class LRL;
.super Landroid/os/AsyncTask;
.source "NavigationListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LRK;

.field final synthetic a:LRN;

.field final synthetic a:Landroid/content/Context;

.field final synthetic a:Landroid/view/View;

.field final synthetic a:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(LRK;LRN;Landroid/widget/ImageView;Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, LRL;->a:LRK;

    iput-object p2, p0, LRL;->a:LRN;

    iput-object p3, p0, LRL;->a:Landroid/widget/ImageView;

    iput-object p4, p0, LRL;->a:Landroid/content/Context;

    iput-object p5, p0, LRL;->a:Landroid/view/View;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 90
    iget-object v0, p0, LRL;->a:LRK;

    invoke-static {v0}, LRK;->a(LRK;)LaGM;

    move-result-object v0

    iget-object v2, p0, LRL;->a:LRK;

    invoke-static {v2}, LRK;->a(LRK;)LaFO;

    move-result-object v2

    invoke-interface {v0, v2}, LaGM;->a(LaFO;)LaFM;

    move-result-object v2

    .line 92
    sget-object v0, LRM;->a:[I

    iget-object v3, p0, LRL;->a:LRN;

    invoke-virtual {v3}, LRN;->a()LCl;

    move-result-object v3

    invoke-interface {v3}, LCl;->a()LCn;

    move-result-object v3

    invoke-virtual {v3}, LCn;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 104
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 94
    :pswitch_0
    iget-object v0, p0, LRL;->a:LRK;

    invoke-static {v0}, LRK;->a(LRK;)Lahh;

    move-result-object v0

    instance-of v0, v0, LagO;

    if-eqz v0, :cond_0

    iget-object v0, p0, LRL;->a:LRK;

    .line 95
    invoke-static {v0}, LRK;->a(LRK;)Lahh;

    move-result-object v0

    check-cast v0, LagO;

    invoke-virtual {v0, v2, v1}, LagO;->a(LaFM;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_0

    .line 98
    :pswitch_1
    iget-object v0, p0, LRL;->a:LRK;

    invoke-static {v0}, LRK;->a(LRK;)LaGM;

    move-result-object v0

    invoke-interface {v0, v2}, LaGM;->b(LaFM;)Z

    move-result v0

    goto :goto_0

    .line 92
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 108
    iget-object v1, p0, LRL;->a:Landroid/widget/ImageView;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 110
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, LRL;->a:LRK;

    iget-object v1, p0, LRL;->a:Landroid/content/Context;

    iget-object v2, p0, LRL;->a:LRN;

    invoke-static {v0, v1, v2}, LRK;->a(LRK;Landroid/content/Context;LRN;)Ljava/lang/String;

    move-result-object v0

    .line 112
    iget-object v1, p0, LRL;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 114
    :cond_0
    return-void

    .line 108
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 87
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, LRL;->a([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 87
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, LRL;->a(Ljava/lang/Boolean;)V

    return-void
.end method
