.class public LbhD;
.super Ljava/lang/Object;
.source "RenameDialogFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/EditText;

.field final synthetic a:Lcom/google/bionics/scanner/ui/RenameDialogFragment;


# direct methods
.method public constructor <init>(Lcom/google/bionics/scanner/ui/RenameDialogFragment;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, LbhD;->a:Lcom/google/bionics/scanner/ui/RenameDialogFragment;

    iput-object p2, p0, LbhD;->a:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 69
    iget-object v0, p0, LbhD;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 70
    invoke-static {}, Lcom/google/bionics/scanner/ui/RenameDialogFragment;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v1

    const-string v2, "onClick called on OK. text: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 72
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 79
    :goto_0
    return-void

    .line 75
    :cond_0
    iget-object v1, p0, LbhD;->a:Lcom/google/bionics/scanner/ui/RenameDialogFragment;

    invoke-static {v1, v0}, Lcom/google/bionics/scanner/ui/RenameDialogFragment;->a(Lcom/google/bionics/scanner/ui/RenameDialogFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 76
    iget-object v0, p0, LbhD;->a:Lcom/google/bionics/scanner/ui/RenameDialogFragment;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/ui/RenameDialogFragment;->a()LH;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/EditorActivity;

    .line 77
    iget-object v1, p0, LbhD;->a:Lcom/google/bionics/scanner/ui/RenameDialogFragment;

    invoke-static {v1}, Lcom/google/bionics/scanner/ui/RenameDialogFragment;->a(Lcom/google/bionics/scanner/ui/RenameDialogFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/bionics/scanner/EditorActivity;->b(Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, LbhD;->a:Lcom/google/bionics/scanner/ui/RenameDialogFragment;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/ui/RenameDialogFragment;->a()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0
.end method
