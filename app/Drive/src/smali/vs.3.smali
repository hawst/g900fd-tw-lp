.class public Lvs;
.super Ljava/lang/Object;
.source "TrixZipFileExposer.java"

# interfaces
.implements LvB;


# instance fields
.field private final a:LQr;

.field private final a:Laeb;

.field private final a:Lalo;

.field private final a:Lamn;

.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lalo;LQr;Landroid/content/Context;Laeb;Lamn;)V
    .locals 1
    .param p3    # Landroid/content/Context;
        .annotation runtime Lajg;
        .end annotation
    .end param

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lvs;->a:Lalo;

    .line 74
    iput-object p2, p0, Lvs;->a:LQr;

    .line 75
    iput-object p4, p0, Lvs;->a:Laeb;

    .line 76
    iput-object p5, p0, Lvs;->a:Lamn;

    .line 77
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lvs;->a:Landroid/content/res/Resources;

    .line 78
    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 135
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 136
    const/4 v0, 0x0

    .line 137
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 138
    invoke-virtual {p1, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v2

    .line 139
    invoke-static {v2}, Ljava/lang/Character;->charCount(I)I

    move-result v3

    add-int/2addr v0, v3

    .line 140
    const-string v3, "&#"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 142
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/zip/ZipFile;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/zip/ZipFile;",
            ")",
            "Ljava/util/List",
            "<",
            "Lvt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 154
    const-string v0, "resources/sheets.js"

    invoke-virtual {p1, v0}, Ljava/util/zip/ZipFile;->getEntry(Ljava/lang/String;)Ljava/util/zip/ZipEntry;

    move-result-object v0

    .line 155
    if-nez v0, :cond_0

    .line 156
    const/4 v0, 0x0

    .line 174
    :goto_0
    return-object v0

    .line 159
    :cond_0
    iget-object v2, p0, Lvs;->a:Lalo;

    invoke-virtual {p1, v0}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v0

    invoke-interface {v2, v0}, Lalo;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 160
    new-instance v2, Ljava/lang/String;

    sget-object v3, LbiB;->c:Ljava/nio/charset/Charset;

    invoke-virtual {v3}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 161
    const/16 v0, 0x3d

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    .line 162
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 161
    invoke-virtual {v2, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 163
    new-instance v2, Lbtm;

    invoke-direct {v2}, Lbtm;-><init>()V

    .line 164
    invoke-virtual {v2, v0}, Lbtm;->a(Ljava/lang/String;)Lbth;

    move-result-object v0

    invoke-virtual {v0}, Lbth;->a()Lbtk;

    move-result-object v0

    .line 165
    const-string v2, "sheets"

    invoke-virtual {v0, v2}, Lbtk;->a(Ljava/lang/String;)Lbth;

    move-result-object v0

    invoke-virtual {v0}, Lbth;->a()Lbtg;

    move-result-object v2

    .line 166
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v2}, Lbtg;->a()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 167
    invoke-virtual {v2, v0}, Lbtg;->a(I)Lbth;

    move-result-object v3

    invoke-virtual {v3}, Lbth;->a()Lbtk;

    move-result-object v3

    .line 168
    const-string v4, "sheet"

    invoke-virtual {v3, v4}, Lbtk;->a(Ljava/lang/String;)Lbth;

    move-result-object v4

    invoke-virtual {v4}, Lbth;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 169
    const-string v5, "file"

    .line 170
    invoke-virtual {v3, v5}, Lbtk;->a(Ljava/lang/String;)Lbth;

    move-result-object v3

    invoke-virtual {v3}, Lbth;->a()Ljava/lang/String;

    move-result-object v3

    sget-object v5, LbiB;->c:Ljava/nio/charset/Charset;

    invoke-virtual {v5}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v5

    .line 169
    invoke-static {v3, v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 171
    new-instance v5, Lvt;

    invoke-direct {v5, v4, v3}, Lvt;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 174
    goto :goto_0
.end method

.method private b(Ljava/util/zip/ZipFile;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/zip/ZipFile;",
            ")",
            "Ljava/util/List",
            "<",
            "Lvt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 187
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 189
    invoke-virtual {p1}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v2

    .line 190
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 191
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/zip/ZipEntry;

    .line 192
    invoke-virtual {v0}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v3

    .line 194
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, ".html"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 199
    new-instance v4, Ljava/util/Scanner;

    invoke-virtual {p1, v0}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v0

    sget-object v5, LbiB;->c:Ljava/nio/charset/Charset;

    invoke-virtual {v5}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v0, v5}, Ljava/util/Scanner;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 202
    :try_start_0
    const-string v0, "<title>.*</title>"

    const/16 v5, 0x1f4

    invoke-virtual {v4, v0, v5}, Ljava/util/Scanner;->findWithinHorizon(Ljava/lang/String;I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 204
    invoke-virtual {v4}, Ljava/util/Scanner;->close()V

    .line 207
    if-nez v0, :cond_1

    .line 208
    const/4 v0, 0x0

    .line 209
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    const-string v5, ".html"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v4, v5

    .line 208
    invoke-virtual {v3, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 217
    :goto_1
    :try_start_1
    new-instance v4, Lvt;

    const-string v5, "UTF-8"

    invoke-static {v3, v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v0, v3}, Lvt;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 218
    :catch_0
    move-exception v0

    .line 219
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 204
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Ljava/util/Scanner;->close()V

    throw v0

    .line 211
    :cond_1
    const-string v4, "<title>"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    .line 212
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    const-string v6, "</title>"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    sub-int/2addr v5, v6

    .line 211
    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 223
    :cond_2
    return-object v1
.end method


# virtual methods
.method public a(Ljava/io/File;LaGo;)Landroid/net/Uri;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 82
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    new-instance v4, LadN;

    new-instance v0, Laee;

    iget-object v1, p0, Lvs;->a:Lalo;

    invoke-direct {v0, v1, p1}, Laee;-><init>(Lalo;Ljava/io/File;)V

    invoke-direct {v4, v0}, LadN;-><init>(LadJ;)V

    .line 87
    iget-object v0, p0, Lvs;->a:Laeb;

    iget-object v1, p0, Lvs;->a:Lamn;

    invoke-static {v0, v1, v4}, LadD;->a(Laeb;Lamn;LadJ;)LadA;

    move-result-object v0

    .line 88
    invoke-static {v0}, Lcom/google/android/apps/docs/sync/filemanager/FileProvider;->a(LadA;)Landroid/net/Uri;

    move-result-object v5

    .line 90
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 91
    new-instance v1, Ljava/util/zip/ZipFile;

    invoke-direct {v1, p1}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V

    .line 92
    const/4 v0, 0x0

    .line 93
    iget-object v3, p0, Lvs;->a:LQr;

    const-string v7, "offlineKixReadFromIndexJson"

    invoke-interface {v3, v7, v2}, LQr;->a(Ljava/lang/String;Z)Z

    move-result v3

    .line 95
    if-eqz v3, :cond_0

    .line 96
    invoke-direct {p0, v1}, Lvs;->a(Ljava/util/zip/ZipFile;)Ljava/util/List;

    move-result-object v0

    .line 98
    :cond_0
    if-nez v0, :cond_1

    .line 100
    invoke-direct {p0, v1}, Lvs;->b(Ljava/util/zip/ZipFile;)Ljava/util/List;

    move-result-object v0

    .line 103
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v1, v2

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvt;

    .line 104
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "<li id=\"sheet%d\"><a href=\"%s\">%s</a></li>"

    const/4 v3, 0x3

    new-array v10, v3, [Ljava/lang/Object;

    add-int/lit8 v3, v1, 0x1

    .line 105
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v10, v2

    const/4 v1, 0x1

    invoke-static {v0}, Lvt;->a(Lvt;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v1

    const/4 v1, 0x2

    invoke-static {v0}, Lvt;->b(Lvt;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v10, v1

    .line 104
    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 106
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v3

    .line 107
    goto :goto_0

    .line 111
    :cond_2
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lvs;->a:Lalo;

    iget-object v2, p0, Lvs;->a:Landroid/content/res/Resources;

    sget v3, Lxh;->trix_offline:I

    .line 112
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    .line 111
    invoke-interface {v1, v2}, Lalo;->a(Ljava/io/InputStream;)[B

    move-result-object v1

    sget-object v2, LbiB;->c:Ljava/nio/charset/Charset;

    .line 112
    invoke-virtual {v2}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    const-string v1, "TRIX_TITLE"

    .line 113
    invoke-interface {p2}, LaGo;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lvs;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "TRIX_ZIP"

    .line 114
    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "<li>TRIX_SHEETS</li>"

    .line 115
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 117
    const-string v1, "trix_offline/index.html"

    sget-object v2, LbiB;->c:Ljava/nio/charset/Charset;

    .line 118
    invoke-virtual {v2}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    const-string v2, "text/html"

    const-string v3, "index.html"

    .line 117
    invoke-virtual {v4, v1, v0, v2, v3}, LadN;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v0, "trix_offline/header.png"

    iget-object v1, p0, Lvs;->a:Lalo;

    iget-object v2, p0, Lvs;->a:Landroid/content/res/Resources;

    sget v3, Lxh;->trix_offline_header:I

    .line 120
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    invoke-interface {v1, v2}, Lalo;->a(Ljava/io/InputStream;)[B

    move-result-object v1

    const-string v2, "image/png"

    const-string v3, "header.png"

    .line 119
    invoke-virtual {v4, v0, v1, v2, v3}, LadN;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;)V

    .line 124
    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "trix_offline/index.html"

    .line 125
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 228
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    invoke-static {p1}, Lcom/google/android/apps/docs/sync/filemanager/FileProvider;->a(Landroid/net/Uri;)V

    .line 230
    return-void
.end method
