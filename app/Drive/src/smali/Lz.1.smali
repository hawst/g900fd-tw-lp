.class public LLz;
.super Ljava/lang/Object;
.source "SelectionFunctionDoMove.java"

# interfaces
.implements LLw;


# instance fields
.field private final a:LPp;

.field private final a:LtK;


# direct methods
.method constructor <init>(LtK;LPp;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, LLz;->a:LtK;

    .line 29
    iput-object p2, p0, LLz;->a:LPp;

    .line 30
    return-void
.end method


# virtual methods
.method public a(LaFM;LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFM;",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ")V"
        }
    .end annotation

    .prologue
    .line 51
    return-void
.end method

.method public a(Ljava/lang/Runnable;LaFM;LbmF;LbmF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "LaFM;",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 57
    new-instance v0, LLA;

    invoke-direct {v0, p0}, LLA;-><init>(LLz;)V

    .line 58
    invoke-static {p4, v0}, LblV;->a(Ljava/util/Collection;LbiG;)Ljava/util/Collection;

    move-result-object v0

    .line 65
    iget-object v1, p0, LLz;->a:LPp;

    invoke-static {v0}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v0

    invoke-interface {v1, v0}, LPp;->a(LbmY;)V

    .line 66
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 67
    return-void
.end method

.method public a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 35
    iget-object v0, p0, LLz;->a:LtK;

    sget-object v2, Lry;->T:Lry;

    invoke-interface {v0, v2}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    invoke-virtual {p1}, LbmF;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 46
    :goto_0
    return v0

    .line 40
    :cond_1
    invoke-virtual {p1}, LbmF;->a()Lbqv;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    .line 41
    invoke-interface {v0}, LaGu;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 42
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 46
    goto :goto_0
.end method
