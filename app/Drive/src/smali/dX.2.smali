.class public LdX;
.super Ljava/lang/Object;
.source "VelocityTrackerCompat.java"


# static fields
.field static final a:Lea;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 67
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 68
    new-instance v0, LdZ;

    invoke-direct {v0}, LdZ;-><init>()V

    sput-object v0, LdX;->a:Lea;

    .line 72
    :goto_0
    return-void

    .line 70
    :cond_0
    new-instance v0, LdY;

    invoke-direct {v0}, LdY;-><init>()V

    sput-object v0, LdX;->a:Lea;

    goto :goto_0
.end method

.method public static a(Landroid/view/VelocityTracker;I)F
    .locals 1

    .prologue
    .line 82
    sget-object v0, LdX;->a:Lea;

    invoke-interface {v0, p0, p1}, Lea;->a(Landroid/view/VelocityTracker;I)F

    move-result v0

    return v0
.end method

.method public static b(Landroid/view/VelocityTracker;I)F
    .locals 1

    .prologue
    .line 91
    sget-object v0, LdX;->a:Lea;

    invoke-interface {v0, p0, p1}, Lea;->b(Landroid/view/VelocityTracker;I)F

    move-result v0

    return v0
.end method
