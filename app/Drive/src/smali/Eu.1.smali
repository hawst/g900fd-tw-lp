.class public LEu;
.super Ljava/lang/Object;
.source "SwipablePanelLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, LEu;->a:Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, LEu;->a:Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;)Landroid/widget/Scroller;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_1

    .line 173
    iget-object v0, p0, LEu;->a:Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;)Landroid/widget/Scroller;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    .line 174
    iget-object v0, p0, LEu;->a:Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;

    iget-object v1, p0, LEu;->a:Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;

    invoke-static {v1}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;)Landroid/widget/Scroller;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->setTranslationY(F)V

    .line 175
    iget-object v0, p0, LEu;->a:Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->post(Ljava/lang/Runnable;)Z

    .line 181
    :cond_0
    :goto_0
    iget-object v0, p0, LEu;->a:Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;

    invoke-static {v0}, Lec;->a(Landroid/view/View;)V

    .line 182
    return-void

    .line 177
    :cond_1
    iget-object v0, p0, LEu;->a:Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;)Landroid/widget/Scroller;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    iget-object v1, p0, LEu;->a:Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->getHeight()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 178
    iget-object v0, p0, LEu;->a:Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;)LEv;

    move-result-object v0

    invoke-interface {v0}, LEv;->a()V

    goto :goto_0
.end method
