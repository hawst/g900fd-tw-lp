.class public Lakq;
.super Ljava/lang/Object;
.source "BitmapManipulator.java"


# instance fields
.field private a:Landroid/graphics/Bitmap;


# direct methods
.method private constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lakq;->a:Landroid/graphics/Bitmap;

    .line 36
    return-void
.end method

.method public static a(Landroid/graphics/Bitmap;)Lakq;
    .locals 1

    .prologue
    .line 357
    new-instance v0, Lakq;

    invoke-direct {v0, p0}, Lakq;-><init>(Landroid/graphics/Bitmap;)V

    return-object v0
.end method

.method public static a(Ljava/io/InputStream;I)Lakq;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 373
    if-lt p1, v1, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Need sampleSize >= 1"

    invoke-static {v0, v3}, LbiT;->a(ZLjava/lang/Object;)V

    .line 375
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 376
    iput p1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 377
    const/4 v3, 0x0

    invoke-static {p0, v3, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 379
    if-eqz v0, :cond_1

    :goto_1
    const-string v2, "Failed decoding bitmap"

    invoke-static {v1, v2}, LbiT;->a(ZLjava/lang/Object;)V

    .line 380
    invoke-static {v0}, Lakq;->a(Landroid/graphics/Bitmap;)Lakq;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 373
    goto :goto_0

    :cond_1
    move v1, v2

    .line 379
    goto :goto_1
.end method

.method private a()Landroid/graphics/Bitmap$Config;
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lakq;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    .line 306
    if-nez v0, :cond_0

    .line 307
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 309
    :cond_0
    return-object v0
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 343
    iput-object p1, p0, Lakq;->a:Landroid/graphics/Bitmap;

    .line 344
    return-void
.end method

.method public static a(Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)Z
    .locals 3

    .prologue
    .line 322
    invoke-virtual {p0}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 323
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 325
    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Lakq;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 74
    const-string v0, "BitmapManipulator"

    const-string v1, "Converting bitmap to grayscale"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    iget-object v0, p0, Lakq;->a:Landroid/graphics/Bitmap;

    .line 76
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, Lakq;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 77
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 79
    new-instance v2, Landroid/graphics/ColorMatrix;

    invoke-direct {v2}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 80
    invoke-virtual {v2, v4}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 81
    new-instance v3, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v3, v2}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    .line 82
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 83
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 85
    iget-object v3, p0, Lakq;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v3, v4, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 86
    invoke-direct {p0, v0}, Lakq;->a(Landroid/graphics/Bitmap;)V

    .line 87
    return-object p0
.end method

.method public a(I)Lakq;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 42
    rem-int/lit16 v0, p1, 0x168

    if-eqz v0, :cond_0

    .line 43
    const-string v0, "BitmapManipulator"

    const-string v2, "Rotating image by %d deg"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 45
    int-to-float v0, p1

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 47
    iget-object v0, p0, Lakq;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 48
    iget-object v0, p0, Lakq;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 50
    iget-object v0, p0, Lakq;->a:Landroid/graphics/Bitmap;

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0}, Lakq;->a(Landroid/graphics/Bitmap;)V

    .line 52
    :cond_0
    return-object p0
.end method

.method public a(Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;I)Lakq;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 245
    .line 248
    invoke-virtual {p0}, Lakq;->a()Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    move-result-object v0

    invoke-static {v0, p1}, Lakq;->a(Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, Lakq;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 253
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lakq;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v0, v3

    sub-float v0, v2, v0

    float-to-int v0, v0

    div-int/lit8 v0, v0, 0x2

    move v2, v1

    .line 262
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v3

    .line 263
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v4

    .line 264
    invoke-direct {p0}, Lakq;->a()Landroid/graphics/Bitmap$Config;

    move-result-object v5

    .line 262
    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 265
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 266
    invoke-virtual {v3, p2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 267
    iget-object v5, p0, Lakq;->a:Landroid/graphics/Bitmap;

    new-instance v6, Landroid/graphics/Rect;

    iget-object v7, p0, Lakq;->a:Landroid/graphics/Bitmap;

    .line 268
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    iget-object v8, p0, Lakq;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    invoke-direct {v6, v1, v1, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v1, Landroid/graphics/Rect;

    .line 270
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v7

    sub-int/2addr v7, v2

    .line 271
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v8

    sub-int/2addr v8, v0

    invoke-direct {v1, v2, v0, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 267
    invoke-virtual {v4, v5, v6, v1, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 274
    invoke-direct {p0, v3}, Lakq;->a(Landroid/graphics/Bitmap;)V

    .line 275
    return-object p0

    .line 258
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, Lakq;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 259
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lakq;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v0, v3

    sub-float v0, v2, v0

    float-to-int v0, v0

    div-int/lit8 v0, v0, 0x2

    move v2, v0

    move v0, v1

    goto :goto_0
.end method

.method public a()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lakq;->a:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public a()Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;
    .locals 3

    .prologue
    .line 332
    new-instance v0, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    iget-object v1, p0, Lakq;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Lakq;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;-><init>(II)V

    return-object v0
.end method

.method public b(Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;I)Lakq;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 284
    invoke-virtual {p0}, Lakq;->a()Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    move-result-object v0

    .line 285
    invoke-virtual {v0}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v2

    if-gt v1, v2, :cond_0

    .line 286
    invoke-virtual {v0}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v2

    if-le v1, v2, :cond_1

    .line 287
    :cond_0
    const-string v1, "BitmapManipulator"

    const-string v2, "New dimension is not bigger than the bitmap dimension: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v8

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 301
    :goto_0
    return-object p0

    .line 289
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v1

    invoke-virtual {v0}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 290
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v0

    sub-int v0, v2, v0

    div-int/lit8 v0, v0, 0x2

    .line 292
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v3

    invoke-direct {p0}, Lakq;->a()Landroid/graphics/Bitmap$Config;

    move-result-object v4

    .line 291
    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 293
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 294
    invoke-virtual {v2, p2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 295
    iget-object v4, p0, Lakq;->a:Landroid/graphics/Bitmap;

    new-instance v5, Landroid/graphics/Rect;

    iget-object v6, p0, Lakq;->a:Landroid/graphics/Bitmap;

    .line 296
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    iget-object v7, p0, Lakq;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-direct {v5, v8, v8, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v6, Landroid/graphics/Rect;

    .line 297
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v7

    sub-int/2addr v7, v1

    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v8

    sub-int/2addr v8, v0

    invoke-direct {v6, v1, v0, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 295
    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 299
    invoke-direct {p0, v2}, Lakq;->a(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
