.class public final enum LahS;
.super Ljava/lang/Enum;
.source "TaskInfo.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LahS;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LahS;

.field private static final synthetic a:[LahS;

.field public static final enum b:LahS;

.field public static final enum c:LahS;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20
    new-instance v0, LahS;

    const-string v1, "DOWNLOAD"

    invoke-direct {v0, v1, v2}, LahS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LahS;->a:LahS;

    .line 21
    new-instance v0, LahS;

    const-string v1, "UPLOAD"

    invoke-direct {v0, v1, v3}, LahS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LahS;->b:LahS;

    .line 23
    new-instance v0, LahS;

    const-string v1, "UNSET"

    invoke-direct {v0, v1, v4}, LahS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LahS;->c:LahS;

    .line 19
    const/4 v0, 0x3

    new-array v0, v0, [LahS;

    sget-object v1, LahS;->a:LahS;

    aput-object v1, v0, v2

    sget-object v1, LahS;->b:LahS;

    aput-object v1, v0, v3

    sget-object v1, LahS;->c:LahS;

    aput-object v1, v0, v4

    sput-object v0, LahS;->a:[LahS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LahS;
    .locals 1

    .prologue
    .line 19
    const-class v0, LahS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LahS;

    return-object v0
.end method

.method public static values()[LahS;
    .locals 1

    .prologue
    .line 19
    sget-object v0, LahS;->a:[LahS;

    invoke-virtual {v0}, [LahS;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LahS;

    return-object v0
.end method
