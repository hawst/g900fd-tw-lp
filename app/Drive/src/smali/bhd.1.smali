.class public Lbhd;
.super Ljava/lang/Object;
.source "RectifiedImageCache.java"


# static fields
.field private static final a:Lcom/google/bionics/scanner/unveil/util/Logger;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/bionics/scanner/unveil/util/Picture;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>()V

    sput-object v0, Lbhd;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Lbhe;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lbhe;-><init>(I)V

    .line 21
    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lbhd;->a:Ljava/util/Map;

    .line 23
    return-void
.end method

.method static synthetic a()Lcom/google/bionics/scanner/unveil/util/Logger;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lbhd;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/google/bionics/scanner/unveil/util/Picture;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 33
    iget-object v0, p0, Lbhd;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    sget-object v0, Lbhd;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Image cache hit for: %s"

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 35
    iget-object v0, p0, Lbhd;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/util/Picture;

    .line 40
    :goto_0
    return-object v0

    .line 37
    :cond_0
    sget-object v0, Lbhd;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Loading image into cache from: %s"

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v3}, Lcom/google/bionics/scanner/unveil/util/PictureFactory;->loadBitmap(Ljava/io/File;I)Lcom/google/bionics/scanner/unveil/util/Picture;

    move-result-object v0

    .line 39
    iget-object v1, p0, Lbhd;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lbhd;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 59
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 50
    iget-object v0, p0, Lbhd;->a:Ljava/util/Map;

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/google/bionics/scanner/unveil/util/PictureFactory;->loadBitmap(Ljava/io/File;I)Lcom/google/bionics/scanner/unveil/util/Picture;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 52
    return-void
.end method
