.class public LNv;
.super Ljava/lang/Object;
.source "DriveWidgetConfiguration.java"


# static fields
.field private static final a:LatE;

.field public static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LatE;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LatE;

.field private static final c:LatE;

.field private static final d:LatE;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 22
    new-instance v0, LNw;

    const-string v1, "HOME_SCREEN"

    sget v2, LatM;->widget_home:I

    invoke-direct {v0, v1, v2}, LNw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LNv;->a:LatE;

    .line 29
    new-instance v0, LNx;

    const-string v1, "NEW_DOC"

    sget v2, LatM;->widget_newdoc:I

    invoke-direct {v0, v1, v2}, LNx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LNv;->b:LatE;

    .line 38
    new-instance v0, LNy;

    const-string v1, "NEW_DOC_FROM_CAMERA"

    sget v2, LatM;->widget_newdocfromcamera:I

    invoke-direct {v0, v1, v2}, LNy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LNv;->c:LatE;

    .line 51
    new-instance v0, LNz;

    const-string v1, "UPLOAD"

    sget v2, LatM;->widget_upload:I

    invoke-direct {v0, v1, v2}, LNz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LNv;->d:LatE;

    .line 61
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v0

    sget-object v1, LNv;->a:LatE;

    .line 62
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    sget-object v1, LNv;->b:LatE;

    .line 63
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    sget-object v1, LNv;->c:LatE;

    .line 64
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    sget-object v1, LNv;->d:LatE;

    .line 65
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, LbmH;->a()LbmF;

    move-result-object v0

    sput-object v0, LNv;->a:Ljava/util/List;

    .line 61
    return-void
.end method
