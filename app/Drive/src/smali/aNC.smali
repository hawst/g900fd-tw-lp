.class public final LaNC;
.super Ljava/lang/Object;


# instance fields
.field private final a:LaNF;

.field private a:LaNI;

.field private final a:LaNO;

.field private final a:Landroid/content/Context;

.field private final a:Lcom/google/android/gms/internal/dx;

.field private final a:Ljava/lang/Object;

.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/dx;LaNO;LaNF;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LaNC;->a:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, LaNC;->a:Z

    iput-object p1, p0, LaNC;->a:Landroid/content/Context;

    iput-object p2, p0, LaNC;->a:Lcom/google/android/gms/internal/dx;

    iput-object p3, p0, LaNC;->a:LaNO;

    iput-object p4, p0, LaNC;->a:LaNF;

    return-void
.end method


# virtual methods
.method public a(JJ)LaNK;
    .locals 17

    const-string v4, "Starting mediation."

    invoke-static {v4}, LaQc;->a(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, LaNC;->a:LaNF;

    iget-object v4, v4, LaNF;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LaNE;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Trying mediation network: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v9, LaNE;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LaQc;->c(Ljava/lang/String;)V

    iget-object v4, v9, LaNE;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_1
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, LaNC;->a:Ljava/lang/Object;

    monitor-enter v15

    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v4, v0, LaNC;->a:Z

    if-eqz v4, :cond_2

    new-instance v4, LaNK;

    const/4 v5, -0x1

    invoke-direct {v4, v5}, LaNK;-><init>(I)V

    monitor-exit v15

    :goto_1
    return-object v4

    :cond_2
    new-instance v4, LaNI;

    move-object/from16 v0, p0

    iget-object v5, v0, LaNC;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v7, v0, LaNC;->a:LaNO;

    move-object/from16 v0, p0

    iget-object v8, v0, LaNC;->a:LaNF;

    move-object/from16 v0, p0

    iget-object v10, v0, LaNC;->a:Lcom/google/android/gms/internal/dx;

    iget-object v10, v10, Lcom/google/android/gms/internal/dx;->a:Lcom/google/android/gms/internal/an;

    move-object/from16 v0, p0

    iget-object v11, v0, LaNC;->a:Lcom/google/android/gms/internal/dx;

    iget-object v11, v11, Lcom/google/android/gms/internal/dx;->a:Lcom/google/android/gms/internal/aq;

    move-object/from16 v0, p0

    iget-object v12, v0, LaNC;->a:Lcom/google/android/gms/internal/dx;

    iget-object v12, v12, Lcom/google/android/gms/internal/dx;->a:Lcom/google/android/gms/internal/fa;

    invoke-direct/range {v4 .. v12}, LaNI;-><init>(Landroid/content/Context;Ljava/lang/String;LaNO;LaNF;LaNE;Lcom/google/android/gms/internal/an;Lcom/google/android/gms/internal/aq;Lcom/google/android/gms/internal/fa;)V

    move-object/from16 v0, p0

    iput-object v4, v0, LaNC;->a:LaNI;

    monitor-exit v15
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object/from16 v0, p0

    iget-object v4, v0, LaNC;->a:LaNI;

    move-wide/from16 v0, p1

    move-wide/from16 v2, p3

    invoke-virtual {v4, v0, v1, v2, v3}, LaNI;->a(JJ)LaNK;

    move-result-object v4

    iget v5, v4, LaNK;->a:I

    if-nez v5, :cond_3

    const-string v5, "Adapter succeeded."

    invoke-static {v5}, LaQc;->a(Ljava/lang/String;)V

    goto :goto_1

    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v15
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    :cond_3
    iget-object v5, v4, LaNK;->a:LaNR;

    if-eqz v5, :cond_1

    sget-object v5, LaQb;->a:Landroid/os/Handler;

    new-instance v6, LaND;

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v4}, LaND;-><init>(LaNC;LaNK;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_4
    new-instance v4, LaNK;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, LaNK;-><init>(I)V

    goto :goto_1
.end method

.method public a()V
    .locals 2

    iget-object v1, p0, LaNC;->a:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LaNC;->a:Z

    iget-object v0, p0, LaNC;->a:LaNI;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaNC;->a:LaNI;

    invoke-virtual {v0}, LaNI;->a()V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
