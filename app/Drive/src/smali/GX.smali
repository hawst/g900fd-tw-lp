.class abstract LGX;
.super LHf;
.source "AbstractDriveAppOpenerOption.java"


# instance fields
.field protected final a:LGZ;

.field protected final a:Landroid/content/Context;

.field protected final a:Ljava/lang/String;


# direct methods
.method constructor <init>(LHh;Landroid/content/Context;LGZ;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1, p5, p6}, LHf;-><init>(LHh;Ljava/lang/String;Z)V

    .line 34
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LGX;->a:Landroid/content/Context;

    .line 35
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LGZ;

    iput-object v0, p0, LGX;->a:LGZ;

    .line 36
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LGX;->a:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method protected abstract a(LaGo;Landroid/net/Uri;)Landroid/content/Intent;
.end method

.method public final a(LFT;LaGo;Landroid/os/Bundle;)LbsU;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LFT;",
            "LaGo;",
            "Landroid/os/Bundle;",
            ")",
            "LbsU",
            "<",
            "LDL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    invoke-interface {p2}, LaGo;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v4

    .line 45
    if-nez v4, :cond_0

    .line 46
    const/4 v0, 0x0

    invoke-static {v0}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    .line 82
    :goto_0
    return-object v0

    .line 48
    :cond_0
    invoke-interface {p2}, LaGo;->c()Ljava/lang/String;

    move-result-object v2

    .line 49
    new-instance v0, LGY;

    move-object v1, p0

    move-object v3, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LGY;-><init>(LGX;Ljava/lang/String;LFT;Lcom/google/android/gms/drive/database/data/ResourceSpec;LaGo;)V

    .line 82
    invoke-static {v0}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    goto :goto_0
.end method

.method final a(ZZ)Z
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x1

    return v0
.end method
