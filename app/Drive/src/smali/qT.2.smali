.class LqT;
.super LaGN;
.source "AbstractPickAccountAndEntryActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGN",
        "<",
        "Lcom/google/android/gms/drive/database/data/EntrySpec;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LaFO;

.field final synthetic a:LqS;


# direct methods
.method constructor <init>(LqS;LaFO;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, LqT;->a:LqS;

    iput-object p2, p0, LqT;->a:LaFO;

    invoke-direct {p0}, LaGN;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LaGM;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, LqT;->a:LaFO;

    invoke-interface {p1, v0}, LaGM;->a(LaFO;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(LaGM;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0, p1}, LqT;->a(LaGM;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 3

    .prologue
    .line 77
    iget-object v0, p0, LqT;->a:LqS;

    iget-object v1, p0, LqT;->a:LaFO;

    .line 78
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/app/PickEntryActivity;->a(Landroid/content/Context;LaFO;)LuW;

    move-result-object v0

    .line 79
    invoke-virtual {v0, p1}, LuW;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LuW;

    move-result-object v0

    iget-object v1, p0, LqT;->a:LqS;

    .line 80
    invoke-virtual {v1}, LqS;->a()Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, LuW;->a(Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;)LuW;

    move-result-object v0

    .line 81
    iget-object v1, p0, LqT;->a:LqS;

    invoke-virtual {v1, v0}, LqS;->a(LuW;)V

    .line 82
    invoke-virtual {v0}, LuW;->a()Landroid/content/Intent;

    move-result-object v0

    .line 87
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 88
    iget-object v1, p0, LqT;->a:LqS;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LqS;->startActivityForResult(Landroid/content/Intent;I)V

    .line 89
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 69
    check-cast p1, Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {p0, p1}, LqT;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    return-void
.end method
