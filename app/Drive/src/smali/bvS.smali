.class LbvS;
.super Ljava/lang/Object;
.source "Indexer.java"


# instance fields
.field final a:LbuP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbuP",
            "<*>;"
        }
    .end annotation
.end field

.field final a:LbvL;

.field final a:LbvR;

.field final a:Ljava/lang/Object;

.field final a:Ljava/lang/String;

.field final b:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lbup;LbvR;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbup",
            "<*>;",
            "LbvR;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p3, p0, LbvS;->a:Ljava/lang/Object;

    .line 67
    iput-object p2, p0, LbvS;->a:LbvR;

    .line 68
    iput-object p4, p0, LbvS;->b:Ljava/lang/Object;

    .line 69
    invoke-interface {p1}, Lbup;->a()Lbuv;

    move-result-object v0

    invoke-virtual {v0}, Lbuv;->a()LbuP;

    move-result-object v0

    iput-object v0, p0, LbvS;->a:LbuP;

    .line 70
    invoke-interface {p1}, Lbup;->a()Lbuv;

    move-result-object v0

    invoke-virtual {v0}, Lbuv;->a()Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, LbvK;

    .line 71
    invoke-interface {v0}, LbvK;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LbvS;->a:Ljava/lang/String;

    .line 72
    invoke-interface {v0}, LbvK;->a()LbvL;

    move-result-object v0

    iput-object v0, p0, LbvS;->a:LbvL;

    .line 73
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 76
    instance-of v1, p1, LbvS;

    if-nez v1, :cond_1

    .line 85
    :cond_0
    :goto_0
    return v0

    .line 79
    :cond_1
    check-cast p1, LbvS;

    .line 80
    iget-object v1, p0, LbvS;->a:LbvR;

    iget-object v2, p1, LbvS;->a:LbvR;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LbvS;->a:Ljava/lang/Object;

    iget-object v2, p1, LbvS;->a:Ljava/lang/Object;

    .line 81
    invoke-static {v1, v2}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LbvS;->a:LbuP;

    iget-object v2, p1, LbvS;->a:LbuP;

    .line 82
    invoke-virtual {v1, v2}, LbuP;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LbvS;->a:LbvL;

    iget-object v2, p1, LbvS;->a:LbvL;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LbvS;->a:Ljava/lang/String;

    iget-object v2, p1, LbvS;->a:Ljava/lang/String;

    .line 84
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LbvS;->b:Ljava/lang/Object;

    iget-object v2, p1, LbvS;->b:Ljava/lang/Object;

    .line 85
    invoke-static {v1, v2}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 89
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LbvS;->a:LbvR;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LbvS;->a:Ljava/lang/Object;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LbvS;->a:LbuP;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LbvS;->a:LbvL;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, LbvS;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, LbvS;->b:Ljava/lang/Object;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
