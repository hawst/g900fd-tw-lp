.class public Lbaj;
.super Ljava/lang/Object;
.source "SelectedAccountNavigationView.java"

# interfaces
.implements Lban;


# instance fields
.field final synthetic a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;)V
    .locals 0

    .prologue
    .line 1043
    iput-object p1, p0, Lbaj;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;Lbab;)V
    .locals 0

    .prologue
    .line 1043
    invoke-direct {p0, p1}, Lbaj;-><init>(Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)Lbam;
    .locals 2

    .prologue
    .line 1046
    new-instance v1, Lbam;

    invoke-direct {v1}, Lbam;-><init>()V

    .line 1047
    iput-object p1, v1, Lbam;->b:Landroid/view/View;

    .line 1048
    sget v0, LaZX;->account_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbam;->c:Landroid/view/View;

    .line 1049
    sget v0, LaZX;->avatar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbam;->d:Landroid/view/View;

    .line 1050
    iget-object v0, v1, Lbam;->d:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbam;->b:Landroid/widget/ImageView;

    .line 1051
    sget v0, LaZX;->account_display_name:I

    .line 1052
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbam;->a:Landroid/widget/TextView;

    .line 1053
    sget v0, LaZX;->account_address:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbam;->b:Landroid/widget/TextView;

    .line 1054
    sget v0, LaZX;->cover_photo:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbam;->a:Landroid/widget/ImageView;

    .line 1055
    sget v0, LaZX;->account_list_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    iput-object v0, v1, Lbam;->a:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    .line 1056
    sget v0, LaZX;->scrim:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbam;->a:Landroid/view/View;

    .line 1057
    iget-object v0, p0, Lbaj;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-static {v0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a(Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1058
    sget v0, LaZX;->avatar_recents_one:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbam;->e:Landroid/view/View;

    .line 1059
    iget-object v0, v1, Lbam;->e:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbam;->c:Landroid/widget/ImageView;

    .line 1060
    sget v0, LaZX;->avatar_recents_two:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbam;->f:Landroid/view/View;

    .line 1061
    iget-object v0, v1, Lbam;->f:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbam;->d:Landroid/widget/ImageView;

    .line 1062
    sget v0, LaZX;->offscreen_avatar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbam;->h:Landroid/view/View;

    .line 1063
    iget-object v0, v1, Lbam;->h:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbam;->f:Landroid/widget/ImageView;

    .line 1064
    sget v0, LaZX;->offscreen_cover_photo:I

    .line 1065
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbam;->e:Landroid/widget/ImageView;

    .line 1066
    sget v0, LaZX;->offscreen_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbam;->g:Landroid/view/View;

    .line 1067
    sget v0, LaZX;->offscreen_account_display_name:I

    .line 1068
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbam;->c:Landroid/widget/TextView;

    .line 1069
    sget v0, LaZX;->offscreen_account_address:I

    .line 1070
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lbam;->d:Landroid/widget/TextView;

    .line 1071
    sget v0, LaZX;->crossfade_avatar_recents_one:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbam;->i:Landroid/view/View;

    .line 1072
    iget-object v0, v1, Lbam;->i:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbam;->g:Landroid/widget/ImageView;

    .line 1073
    sget v0, LaZX;->crossfade_avatar_recents_two:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lbam;->j:Landroid/view/View;

    .line 1074
    iget-object v0, v1, Lbam;->j:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lbam;->h:Landroid/widget/ImageView;

    .line 1077
    :cond_0
    return-object v1
.end method
