.class public LaoH;
.super Ljava/lang/Object;
.source "PreviewImageFetcher.java"

# interfaces
.implements Laoo;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Laoo",
        "<",
        "Lcom/google/android/apps/docs/utils/FetchSpec;",
        "LakD",
        "<",
        "Ljava/io/File;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LaGM;

.field private final a:LaKR;

.field private final a:Ladi;

.field private final a:Laeb;

.field private final a:Laks;

.field private final a:Lalo;

.field private final a:Laoo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoo",
            "<",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;>;"
        }
    .end annotation
.end field

.field private final a:Laop;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laop",
            "<-",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Z


# direct methods
.method private constructor <init>(LaGM;Ladi;Laeb;Lalo;LaKR;ZLaop;Laoo;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGM;",
            "Ladi;",
            "Laeb;",
            "Lalo;",
            "LaKR;",
            "Z",
            "Laop",
            "<-",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            ">;",
            "Laoo",
            "<",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    new-instance v0, Laks;

    invoke-direct {v0}, Laks;-><init>()V

    iput-object v0, p0, LaoH;->a:Laks;

    .line 106
    iput-object p1, p0, LaoH;->a:LaGM;

    .line 107
    iput-object p2, p0, LaoH;->a:Ladi;

    .line 108
    iput-object p3, p0, LaoH;->a:Laeb;

    .line 109
    iput-object p4, p0, LaoH;->a:Lalo;

    .line 110
    iput-object p5, p0, LaoH;->a:LaKR;

    .line 111
    iput-boolean p6, p0, LaoH;->a:Z

    .line 112
    iput-object p7, p0, LaoH;->a:Laop;

    .line 113
    iput-object p8, p0, LaoH;->a:Laoo;

    .line 114
    return-void
.end method

.method synthetic constructor <init>(LaGM;Ladi;Laeb;Lalo;LaKR;ZLaop;Laoo;LaoI;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct/range {p0 .. p8}, LaoH;-><init>(LaGM;Ladi;Laeb;Lalo;LaKR;ZLaop;Laoo;)V

    return-void
.end method

.method private a(LaGo;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)LakD;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGo;",
            "Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;",
            ")",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    iget-object v0, p0, LaoH;->a:Ladi;

    sget-object v1, LacY;->a:LacY;

    .line 164
    invoke-interface {v0, p1, v1}, Ladi;->a(LaGo;LacY;)LbsU;

    move-result-object v0

    invoke-interface {v0}, LbsU;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladj;

    .line 166
    :try_start_0
    invoke-interface {v0}, Ladj;->a()LadY;

    move-result-object v1

    .line 167
    iget-boolean v2, p0, LaoH;->a:Z

    if-eqz v2, :cond_0

    .line 168
    invoke-direct {p0, v1, p2}, LaoH;->a(LadY;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v2

    .line 169
    invoke-virtual {v1}, LadY;->a()Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v3

    .line 171
    const/4 v1, 0x0

    :try_start_1
    invoke-static {v3, v1, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 172
    invoke-direct {p0, v1, p2}, LaoH;->a(Landroid/graphics/Bitmap;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 173
    invoke-direct {p0, v1}, LaoH;->a(Landroid/graphics/Bitmap;)LakD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 175
    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 181
    invoke-interface {v0}, Ladj;->close()V

    move-object v0, v1

    :goto_0
    return-object v0

    .line 175
    :catchall_0
    move-exception v1

    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 181
    :catchall_1
    move-exception v1

    invoke-interface {v0}, Ladj;->close()V

    throw v1

    .line 178
    :cond_0
    :try_start_4
    invoke-direct {p0, v1}, LaoH;->a(LadY;)LakD;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v1

    .line 181
    invoke-interface {v0}, Ladj;->close()V

    move-object v0, v1

    goto :goto_0
.end method

.method private a(LadY;)LakD;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LadY;",
            ")",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 235
    .line 236
    iget-object v0, p0, LaoH;->a:Laeb;

    invoke-interface {v0}, Laeb;->a()LakD;

    move-result-object v1

    .line 238
    :try_start_0
    iget-object v2, p0, LaoH;->a:Lalo;

    invoke-virtual {v1}, LakD;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-interface {v2, p1, v0}, Lalo;->a(LadY;Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 243
    return-object v1

    .line 242
    :catchall_0
    move-exception v0

    .line 243
    invoke-virtual {v1}, LakD;->close()V

    throw v0
.end method

.method private a(Landroid/graphics/Bitmap;)LakD;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            ")",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 217
    invoke-static {}, Lbrm;->a()Lbrm;

    move-result-object v1

    .line 219
    iget-object v0, p0, LaoH;->a:Laeb;

    invoke-interface {v0}, Laeb;->a()LakD;

    move-result-object v2

    .line 221
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-virtual {v2}, LakD;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v1, v3}, Lbrm;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;

    .line 222
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    invoke-virtual {p1, v3, v4, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 226
    invoke-virtual {v1}, Lbrm;->close()V

    .line 228
    return-object v2

    .line 226
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lbrm;->close()V

    .line 228
    invoke-virtual {v2}, LakD;->close()V

    throw v0
.end method

.method static synthetic a(LaoH;LaGo;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)LakD;
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, LaoH;->a(LaGo;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)LakD;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/graphics/Bitmap;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 203
    .line 204
    invoke-virtual {p2}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v0

    invoke-virtual {p2}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 205
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 206
    invoke-virtual {p2}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 207
    invoke-virtual {p2}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 208
    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 209
    invoke-virtual {v1, v2, v2}, Landroid/graphics/Canvas;->scale(FF)V

    .line 210
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 211
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 212
    invoke-virtual {v1, p1, v5, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 213
    return-object v0
.end method

.method private a(LadY;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)Landroid/graphics/BitmapFactory$Options;
    .locals 5

    .prologue
    .line 187
    invoke-virtual {p1}, LadY;->a()Ljava/io/InputStream;

    move-result-object v1

    .line 189
    :try_start_0
    iget-object v0, p0, LaoH;->a:Laks;

    invoke-virtual {v0, v1}, Laks;->a(Ljava/io/InputStream;)Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    move-result-object v0

    .line 190
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 191
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v3, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 192
    invoke-virtual {v0}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p2}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 193
    invoke-virtual {v0}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v0, v4

    .line 194
    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 195
    float-to-int v0, v0

    iput v0, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 198
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    return-object v2

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
.end method

.method private a(Lcom/google/android/apps/docs/utils/FetchSpec;LaGo;)LbsU;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            "LaGo;",
            ")",
            "LbsU",
            "<",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 137
    invoke-static {}, Laou;->a()Laou;

    move-result-object v0

    .line 139
    iget-object v1, p0, LaoH;->a:Laop;

    new-instance v2, LaoI;

    invoke-direct {v2, p0, p2, p1, v0}, LaoI;-><init>(LaoH;LaGo;Lcom/google/android/apps/docs/utils/FetchSpec;Laou;)V

    .line 140
    invoke-interface {v1, p1, v2}, Laop;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;)LbsU;

    move-result-object v1

    .line 157
    invoke-virtual {v0, v1}, Laou;->b(LbsU;)V

    .line 158
    return-object v1
.end method


# virtual methods
.method public a(Lcom/google/android/apps/docs/utils/FetchSpec;)LbsU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            ")",
            "LbsU",
            "<",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 119
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/FetchSpec;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 121
    iget-object v1, p0, LaoH;->a:LaGM;

    invoke-interface {v1, v0}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v0

    .line 122
    if-nez v0, :cond_0

    .line 123
    new-instance v0, LaoW;

    invoke-direct {v0}, LaoW;-><init>()V

    invoke-static {v0}, LbsK;->a(Ljava/lang/Throwable;)LbsU;

    move-result-object v0

    .line 129
    :goto_0
    return-object v0

    .line 125
    :cond_0
    invoke-interface {v0}, LaGo;->b()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, LaoH;->a:LaKR;

    invoke-interface {v1}, LaKR;->a()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {v0}, LaGo;->f()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 126
    :cond_1
    invoke-direct {p0, p1, v0}, LaoH;->a(Lcom/google/android/apps/docs/utils/FetchSpec;LaGo;)LbsU;

    move-result-object v0

    goto :goto_0

    .line 129
    :cond_2
    iget-object v0, p0, LaoH;->a:Laoo;

    invoke-interface {v0, p1}, Laoo;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)LbsU;
    .locals 1

    .prologue
    .line 46
    check-cast p1, Lcom/google/android/apps/docs/utils/FetchSpec;

    invoke-virtual {p0, p1}, LaoH;->a(Lcom/google/android/apps/docs/utils/FetchSpec;)LbsU;

    move-result-object v0

    return-object v0
.end method
