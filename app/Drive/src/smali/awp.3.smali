.class Lawp;
.super Ljava/lang/Object;
.source "TileBoard.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field final synthetic a:Lawo;

.field private b:I


# direct methods
.method constructor <init>(Lawo;)V
    .locals 1

    .prologue
    .line 178
    iput-object p1, p0, Lawp;->a:Lawo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    iget-object v0, p0, Lawp;->a:Lawo;

    iget-object v0, v0, Lawo;->a:Lawq;

    invoke-static {v0}, Lawq;->a(Lawq;)I

    move-result v0

    iput v0, p0, Lawp;->a:I

    .line 181
    iget-object v0, p0, Lawp;->a:Lawo;

    iget-object v0, v0, Lawo;->a:Lawq;

    invoke-static {v0}, Lawq;->b(Lawq;)I

    move-result v0

    iput v0, p0, Lawp;->b:I

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 196
    iget v0, p0, Lawp;->b:I

    iget-object v1, p0, Lawp;->a:Lawo;

    iget-object v1, v1, Lawo;->a:Lawq;

    invoke-static {v1}, Lawq;->d(Lawq;)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 197
    iget v0, p0, Lawp;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lawp;->b:I

    .line 202
    :goto_0
    return-void

    .line 199
    :cond_0
    iget v0, p0, Lawp;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lawp;->a:I

    .line 200
    iget-object v0, p0, Lawp;->a:Lawo;

    iget-object v0, v0, Lawo;->a:Lawq;

    invoke-static {v0}, Lawq;->b(Lawq;)I

    move-result v0

    iput v0, p0, Lawp;->b:I

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lawp;->a:Lawo;

    iget-object v0, v0, Lawo;->a:Lawn;

    invoke-static {v0}, Lawn;->a(Lawn;)I

    move-result v0

    iget v1, p0, Lawp;->a:I

    mul-int/2addr v0, v1

    iget v1, p0, Lawp;->b:I

    add-int/2addr v0, v1

    .line 191
    invoke-direct {p0}, Lawp;->a()V

    .line 192
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public hasNext()Z
    .locals 2

    .prologue
    .line 185
    iget v0, p0, Lawp;->a:I

    iget-object v1, p0, Lawp;->a:Lawo;

    iget-object v1, v1, Lawo;->a:Lawq;

    invoke-static {v1}, Lawq;->c(Lawq;)I

    move-result v1

    if-gt v0, v1, :cond_0

    iget v0, p0, Lawp;->b:I

    iget-object v1, p0, Lawp;->a:Lawo;

    iget-object v1, v1, Lawo;->a:Lawq;

    invoke-static {v1}, Lawq;->d(Lawq;)I

    move-result v1

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 178
    invoke-virtual {p0}, Lawp;->a()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 0

    .prologue
    .line 205
    return-void
.end method
