.class LLW;
.super LaGN;
.source "SelectionFunctionManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGN",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LLS;

.field private final a:Landroid/view/View$OnTouchListener;

.field private final a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;"
        }
    .end annotation
.end field

.field private final a:Lcom/google/android/apps/docs/doclist/SelectionItem;

.field private final a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;


# direct methods
.method constructor <init>(LLS;Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;Landroid/view/View$OnTouchListener;Lcom/google/android/apps/docs/doclist/SelectionItem;)V
    .locals 1

    .prologue
    .line 84
    iput-object p1, p0, LLW;->a:LLS;

    invoke-direct {p0}, LaGN;-><init>()V

    .line 85
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    iput-object v0, p0, LLW;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    .line 86
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View$OnTouchListener;

    iput-object v0, p0, LLW;->a:Landroid/view/View$OnTouchListener;

    .line 87
    invoke-static {p1}, LLS;->a(LLS;)LJR;

    move-result-object v0

    invoke-virtual {v0}, LJR;->a()LbmF;

    move-result-object v0

    iput-object v0, p0, LLW;->a:LbmF;

    .line 88
    iput-object p4, p0, LLW;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    .line 89
    return-void
.end method


# virtual methods
.method public bridge synthetic a(LaGM;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 76
    invoke-virtual {p0, p1}, LLW;->a(LaGM;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public a(LaGM;)Ljava/lang/Void;
    .locals 4

    .prologue
    .line 94
    iget-object v0, p0, LLW;->a:LLS;

    iget-object v1, p0, LLW;->a:LbmF;

    invoke-static {v0, p1, v1}, LLS;->a(LLS;LaGM;LbmF;)LbmF;

    move-result-object v1

    .line 97
    iget-object v0, p0, LLW;->a:LLS;

    invoke-static {v0}, LLS;->a(LLS;)LbmF;

    move-result-object v0

    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKP;

    .line 98
    iget-object v3, p0, LLW;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    invoke-interface {v0, v1, v3}, LKP;->a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z

    goto :goto_0

    .line 100
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 76
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, LLW;->a(Ljava/lang/Void;)V

    return-void
.end method

.method public a(Ljava/lang/Void;)V
    .locals 3

    .prologue
    .line 105
    iget-object v0, p0, LLW;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    sget v1, Lxc;->selection_actions_container:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 106
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 107
    iget-object v0, p0, LLW;->a:LLS;

    iget-object v1, p0, LLW;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    iget-object v2, p0, LLW;->a:Landroid/view/View$OnTouchListener;

    invoke-static {v0, v1, v2}, LLS;->a(LLS;Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;Landroid/view/View$OnTouchListener;)V

    .line 108
    iget-object v0, p0, LLW;->a:LLS;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LLS;->a(LLS;LLW;)LLW;

    .line 109
    return-void
.end method
