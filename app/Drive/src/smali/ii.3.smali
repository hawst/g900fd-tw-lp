.class Lii;
.super Ljava/lang/Object;
.source "MediaRouteControllerDialog.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field final synthetic a:Lih;

.field private final a:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lih;)V
    .locals 1

    .prologue
    .line 144
    iput-object p1, p0, Lii;->a:Lih;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    new-instance v0, Lij;

    invoke-direct {v0, p0}, Lij;-><init>(Lii;)V

    iput-object v0, p0, Lii;->a:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 1

    .prologue
    .line 174
    if-eqz p3, :cond_0

    .line 175
    iget-object v0, p0, Lii;->a:Lih;

    invoke-static {v0}, Lih;->a(Lih;)Llv;

    move-result-object v0

    invoke-virtual {v0, p2}, Llv;->a(I)V

    .line 177
    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lii;->a:Lih;

    invoke-static {v0}, Lih;->a(Lih;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lii;->a:Lih;

    invoke-static {v0}, Lih;->a(Lih;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lii;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 162
    :goto_0
    return-void

    .line 160
    :cond_0
    iget-object v0, p0, Lii;->a:Lih;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lih;->a(Lih;Z)Z

    goto :goto_0
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 4

    .prologue
    .line 169
    iget-object v0, p0, Lii;->a:Lih;

    invoke-static {v0}, Lih;->a(Lih;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lii;->a:Ljava/lang/Runnable;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/SeekBar;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 170
    return-void
.end method
