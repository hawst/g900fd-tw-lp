.class public final Lbhf;
.super Ljava/lang/Object;
.source "ScanSession.java"


# static fields
.field private static a:Lbhf;

.field private static final a:Lcom/google/bionics/scanner/unveil/util/Logger;


# instance fields
.field private final a:LbgS;

.field private final a:LbgX;

.field private final a:LbgZ;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 33
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "ScanSession"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lbhf;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V
    .locals 3

    .prologue
    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    new-instance v0, LbgX;

    invoke-direct {v0}, LbgX;-><init>()V

    iput-object v0, p0, Lbhf;->a:LbgX;

    .line 190
    new-instance v0, LbgZ;

    invoke-direct {v0, p1}, LbgZ;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lbhf;->a:LbgZ;

    .line 192
    invoke-static {}, LbgZ;->b()V

    .line 194
    iget-object v0, p0, Lbhf;->a:LbgZ;

    invoke-virtual {v0}, LbgZ;->a()V

    .line 196
    new-instance v0, LbgS;

    iget-object v1, p0, Lbhf;->a:LbgZ;

    invoke-direct {v0, p1, v1}, LbgS;-><init>(Landroid/content/Context;LbgZ;)V

    iput-object v0, p0, Lbhf;->a:LbgS;

    .line 199
    iget-object v0, p0, Lbhf;->a:LbgX;

    sget v1, Lcom/google/bionics/scanner/docscanner/R$string;->ds_image_enhancement_method_key:I

    .line 200
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/bionics/scanner/docscanner/R$string;->ds_image_enhancement_method_default:I

    .line 201
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 200
    invoke-interface {p2, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 199
    invoke-static {v1}, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->valueOf(Ljava/lang/String;)Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    move-result-object v1

    invoke-virtual {v0, v1}, LbgX;->a(Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;)V

    .line 201
    return-void
.end method

.method static synthetic a(Lbhf;)LbgS;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lbhf;->a:LbgS;

    return-object v0
.end method

.method static synthetic a(Lbhf;)LbgX;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lbhf;->a:LbgX;

    return-object v0
.end method

.method static synthetic a(Lbhf;)LbgZ;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lbhf;->a:LbgZ;

    return-object v0
.end method

.method public static a()Lbhf;
    .locals 1

    .prologue
    .line 225
    sget-object v0, Lbhf;->a:Lbhf;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/content/SharedPreferences;)Lbhf;
    .locals 1

    .prologue
    .line 210
    invoke-static {}, LbgZ;->c()V

    .line 211
    sget-object v0, Lbhf;->a:Lbhf;

    if-nez v0, :cond_0

    .line 212
    new-instance v0, Lbhf;

    invoke-direct {v0, p0, p1}, Lbhf;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    sput-object v0, Lbhf;->a:Lbhf;

    .line 214
    :cond_0
    sget-object v0, Lbhf;->a:Lbhf;

    return-object v0
.end method

.method static synthetic a()Lcom/google/bionics/scanner/unveil/util/Logger;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lbhf;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lbhk;Lbhj;)V
    .locals 4

    .prologue
    .line 283
    sget-object v0, Lbhk;->b:Lbhk;

    if-ne p1, v0, :cond_0

    .line 284
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 285
    sget v1, Lcom/google/bionics/scanner/docscanner/R$string;->ds_dialog_title_storage_unavailable:I

    .line 286
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/bionics/scanner/docscanner/R$string;->ds_dialog_msg_storage_unavailable:I

    .line 287
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    .line 288
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/bionics/scanner/docscanner/R$string;->ds_dialog_ok_button_text:I

    new-instance v3, Lbhg;

    invoke-direct {v3, p2}, Lbhg;-><init>(Lbhj;)V

    .line 289
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 298
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 301
    :cond_0
    return-void
.end method


# virtual methods
.method public a()LbgX;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lbhf;->a:LbgX;

    return-object v0
.end method

.method public a()LbgZ;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lbhf;->a:LbgZ;

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lbhf;->a:LbgS;

    invoke-virtual {v0}, LbgS;->a()V

    .line 251
    return-void
.end method

.method public a(LbgU;)V
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lbhf;->a:LbgS;

    invoke-virtual {v0, p1}, LbgS;->a(LbgU;)V

    .line 244
    return-void
.end method

.method public a(LbgY;Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;Lbhi;)V
    .locals 4

    .prologue
    .line 328
    new-instance v1, Lbhh;

    invoke-virtual {p1}, LbgY;->a()Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    move-result-object v0

    invoke-direct {v1, p0, p1, v0, p3}, Lbhh;-><init>(Lbhf;LbgY;Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;Lbhi;)V

    const/4 v0, 0x1

    new-array v2, v0, [Lcom/google/bionics/scanner/unveil/util/Picture;

    const/4 v3, 0x0

    const/4 v0, 0x0

    check-cast v0, Lcom/google/bionics/scanner/unveil/util/Picture;

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lbhh;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 329
    return-void
.end method

.method public a(Lcom/google/bionics/scanner/unveil/util/Picture;Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;Lbhi;)V
    .locals 3

    .prologue
    .line 314
    new-instance v0, Lbhh;

    invoke-direct {v0, p0, p2, p3}, Lbhh;-><init>(Lbhf;Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;Lbhi;)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/bionics/scanner/unveil/util/Picture;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lbhh;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 315
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lbhf;->a:LbgX;

    invoke-virtual {v0}, LbgX;->a()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 257
    sget-object v0, Lbhf;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Cleaning up scan session..."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 258
    iget-object v0, p0, Lbhf;->a:LbgS;

    invoke-virtual {v0}, LbgS;->b()V

    .line 259
    iget-object v0, p0, Lbhf;->a:LbgX;

    invoke-virtual {v0}, LbgX;->a()V

    .line 260
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 261
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 267
    invoke-virtual {p0}, Lbhf;->b()V

    .line 268
    const/4 v0, 0x0

    sput-object v0, Lbhf;->a:Lbhf;

    .line 269
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 270
    return-void
.end method
