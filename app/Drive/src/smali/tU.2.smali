.class public LtU;
.super Ljava/lang/Object;
.source "GetContentActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:LFV;

.field final synthetic a:Lcom/google/android/apps/docs/app/GetContentActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/app/GetContentActivity;LFV;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, LtU;->a:Lcom/google/android/apps/docs/app/GetContentActivity;

    iput-object p2, p0, LtU;->a:LFV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 205
    iget-object v0, p0, LtU;->a:LFV;

    invoke-virtual {v0}, LFV;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LtU;->a:Lcom/google/android/apps/docs/app/GetContentActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/GetContentActivity;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, LtU;->a:LFV;

    invoke-virtual {v0}, LFV;->a()I

    move-result v0

    .line 207
    iget-object v1, p0, LtU;->a:Lcom/google/android/apps/docs/app/GetContentActivity;

    sget v2, Lxi;->error_page_title:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/docs/app/GetContentActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 208
    iget-object v1, p0, LtU;->a:Lcom/google/android/apps/docs/app/GetContentActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/app/GetContentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 209
    iget-object v0, p0, LtU;->a:Lcom/google/android/apps/docs/app/GetContentActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/GetContentActivity;->a()LM;

    move-result-object v0

    const/4 v1, 0x0

    check-cast v1, Lcom/google/android/gms/drive/database/data/EntrySpec;

    sget-object v2, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->c:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a(LM;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 218
    :goto_0
    return-void

    .line 216
    :cond_0
    iget-object v0, p0, LtU;->a:Lcom/google/android/apps/docs/app/GetContentActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/GetContentActivity;->finish()V

    goto :goto_0
.end method
