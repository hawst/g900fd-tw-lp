.class public LtR;
.super Ljava/lang/Object;
.source "GetContentActivity.java"

# interfaces
.implements LbsJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbsJ",
        "<",
        "LDL;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LaGo;

.field final synthetic a:Lcom/google/android/apps/docs/app/GetContentActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/app/GetContentActivity;LaGo;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, LtR;->a:Lcom/google/android/apps/docs/app/GetContentActivity;

    iput-object p2, p0, LtR;->a:LaGo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LDL;)V
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, LtR;->a:Lcom/google/android/apps/docs/app/GetContentActivity;

    iget-object v1, p0, LtR;->a:LaGo;

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/docs/app/GetContentActivity;->a(Lcom/google/android/apps/docs/app/GetContentActivity;LDL;LaGo;)V

    .line 121
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 110
    check-cast p1, LDL;

    invoke-virtual {p0, p1}, LtR;->a(LDL;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 112
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    instance-of v0, p1, Ljava/lang/InterruptedException;

    if-eqz v0, :cond_1

    .line 113
    :cond_0
    iget-object v0, p0, LtR;->a:Lcom/google/android/apps/docs/app/GetContentActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/GetContentActivity;->finish()V

    .line 117
    :goto_0
    return-void

    .line 115
    :cond_1
    iget-object v0, p0, LtR;->a:Lcom/google/android/apps/docs/app/GetContentActivity;

    sget-object v1, LFV;->h:LFV;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/docs/app/GetContentActivity;->a(LFV;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
