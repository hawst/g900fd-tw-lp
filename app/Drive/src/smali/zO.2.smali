.class public final enum LzO;
.super Ljava/lang/Enum;
.source "ArrangementMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LzO;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LzO;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum a:LzO;

.field private static final synthetic a:[LzO;

.field public static final enum b:LzO;

.field public static final enum c:LzO;

.field public static final enum d:LzO;


# instance fields
.field private final a:I

.field private final a:Ljava/lang/String;

.field private final a:LzP;

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, -0x1

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, LzO;

    const-string v1, "LIST"

    sget-object v3, LzP;->a:LzP;

    const-string v4, "listMode"

    sget v5, Lxi;->doclist_accessibility_changed_to_list:I

    move v6, v2

    invoke-direct/range {v0 .. v6}, LzO;-><init>(Ljava/lang/String;ILzP;Ljava/lang/String;II)V

    sput-object v0, LzO;->a:LzO;

    .line 24
    new-instance v3, LzO;

    const-string v4, "GRID"

    sget-object v6, LzP;->b:LzP;

    const-string v7, "gridMode"

    sget v8, Lxi;->doclist_accessibility_changed_to_grid:I

    move v5, v10

    move v9, v10

    invoke-direct/range {v3 .. v9}, LzO;-><init>(Ljava/lang/String;ILzP;Ljava/lang/String;II)V

    sput-object v3, LzO;->b:LzO;

    .line 29
    new-instance v3, LzO;

    const-string v4, "INCOMING_LIST"

    sget-object v6, LzP;->a:LzP;

    const/4 v7, 0x0

    move v5, v11

    move v8, v13

    move v9, v11

    invoke-direct/range {v3 .. v9}, LzO;-><init>(Ljava/lang/String;ILzP;Ljava/lang/String;II)V

    sput-object v3, LzO;->c:LzO;

    .line 34
    new-instance v3, LzO;

    const-string v4, "INCOMING_GRID"

    sget-object v6, LzP;->b:LzP;

    const/4 v7, 0x0

    move v5, v12

    move v8, v13

    move v9, v12

    invoke-direct/range {v3 .. v9}, LzO;-><init>(Ljava/lang/String;ILzP;Ljava/lang/String;II)V

    sput-object v3, LzO;->d:LzO;

    .line 19
    const/4 v0, 0x4

    new-array v0, v0, [LzO;

    sget-object v1, LzO;->a:LzO;

    aput-object v1, v0, v2

    sget-object v1, LzO;->b:LzO;

    aput-object v1, v0, v10

    sget-object v1, LzO;->c:LzO;

    aput-object v1, v0, v11

    sget-object v1, LzO;->d:LzO;

    aput-object v1, v0, v12

    sput-object v0, LzO;->a:[LzO;

    .line 49
    invoke-static {}, LboS;->a()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, LzO;->a:Ljava/util/Map;

    .line 52
    invoke-static {}, LzO;->values()[LzO;

    move-result-object v0

    array-length v1, v0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 53
    sget-object v4, LzO;->a:Ljava/util/Map;

    iget v5, v3, LzO;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 55
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILzP;Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LzP;",
            "Ljava/lang/String;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 73
    iput-object p3, p0, LzO;->a:LzP;

    .line 74
    iput-object p4, p0, LzO;->a:Ljava/lang/String;

    .line 75
    iput p5, p0, LzO;->a:I

    .line 76
    iput p6, p0, LzO;->b:I

    .line 77
    return-void
.end method

.method public static a(I)LzO;
    .locals 2

    .prologue
    .line 58
    sget-object v0, LzO;->a:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LzO;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LzO;
    .locals 1

    .prologue
    .line 19
    const-class v0, LzO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LzO;

    return-object v0
.end method

.method public static values()[LzO;
    .locals 1

    .prologue
    .line 19
    sget-object v0, LzO;->a:[LzO;

    invoke-virtual {v0}, [LzO;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LzO;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, LzO;->b:I

    return v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, LzO;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()LzP;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, LzO;->a:LzP;

    return-object v0
.end method

.method public final a(Landroid/view/View;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 94
    iget v0, p0, LzO;->a:I

    if-ltz v0, :cond_0

    .line 95
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, LzO;->a:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 96
    invoke-virtual {p1, v0}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 98
    :cond_0
    return-void
.end method

.method public a(LpU;)Z
    .locals 3

    .prologue
    .line 101
    iget-object v0, p0, LzO;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 102
    :goto_0
    if-eqz v0, :cond_0

    .line 103
    const-string v1, "docListViewArrangementMode"

    iget-object v2, p0, LzO;->a:Ljava/lang/String;

    invoke-interface {p1, v1, v2}, LpU;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :cond_0
    return v0

    .line 101
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
