.class public LDc;
.super Ljava/lang/Object;
.source "SelectionMoveOperation.java"


# instance fields
.field private final a:LVg;

.field private final a:LaGM;

.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LaGM;LVg;Laja;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGM;",
            "LVg;",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, LDc;->a:LaGM;

    .line 32
    iput-object p2, p0, LDc;->a:LVg;

    .line 33
    iput-object p3, p0, LDc;->a:Lbxw;

    .line 34
    return-void
.end method


# virtual methods
.method public a(LbmY;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 38
    invoke-virtual {p1}, LbmY;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 42
    :cond_1
    invoke-static {p1, v2}, Lbnm;->a(Ljava/lang/Iterable;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 43
    iget-object v1, p0, LDc;->a:LVg;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    .line 44
    invoke-virtual {v1, v0}, LVg;->a(LaFO;)LVf;

    move-result-object v3

    .line 45
    iget-object v0, p0, LDc;->a:LaGM;

    invoke-interface {v0, p2}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFV;

    move-result-object v4

    .line 46
    if-eqz v4, :cond_0

    .line 48
    invoke-virtual {p1}, LbmY;->a()Lbqv;

    move-result-object v5

    move v1, v2

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 49
    iget-object v6, p0, LDc;->a:LaGM;

    .line 50
    invoke-interface {v6, v0}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LbmY;

    move-result-object v6

    .line 51
    invoke-interface {v4}, LaFV;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v7

    invoke-virtual {v6, v7}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 52
    invoke-interface {v4}, LaFV;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v7

    invoke-virtual {v3, v0, v6, v7}, LVf;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LbmY;Lcom/google/android/gms/drive/database/data/EntrySpec;)LVf;

    .line 53
    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    .line 55
    goto :goto_1

    .line 56
    :cond_2
    if-lez v1, :cond_0

    .line 57
    iget-object v0, p0, LDc;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 58
    sget v1, Lxi;->selection_undo_move_message:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    .line 59
    invoke-interface {v4}, LaFV;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v2

    invoke-virtual {v0, v1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 60
    iget-object v1, p0, LDc;->a:LVg;

    invoke-virtual {v3}, LVf;->a()LVd;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LVg;->a(LVd;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method
