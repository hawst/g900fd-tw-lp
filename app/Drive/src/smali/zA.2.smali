.class LzA;
.super Ljava/lang/Object;
.source "PreviewCardUpdater.java"

# interfaces
.implements Lzy;


# instance fields
.field final synthetic a:LaGu;

.field final synthetic a:Lzz;


# direct methods
.method constructor <init>(Lzz;LaGu;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, LzA;->a:Lzz;

    iput-object p2, p0, LzA;->a:LaGu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(II)LbsU;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "LbsU",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 64
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 65
    :cond_0
    invoke-static {v4}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    .line 85
    :goto_0
    return-object v0

    .line 67
    :cond_1
    new-instance v1, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    invoke-direct {v1, p1, p2}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;-><init>(II)V

    .line 68
    iget-object v2, p0, LzA;->a:LaGu;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/docs/utils/FetchSpec;->a(LaGu;ILcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)Lcom/google/android/apps/docs/utils/FetchSpec;

    move-result-object v1

    .line 71
    iget-object v2, p0, LzA;->a:LaGu;

    invoke-interface {v2}, LaGu;->a()LaGx;

    move-result-object v2

    sget-object v3, LaGx;->b:LaGx;

    if-ne v2, v3, :cond_2

    const/4 v0, 0x1

    .line 72
    :cond_2
    iget-object v2, p0, LzA;->a:Lzz;

    invoke-static {v2}, Lzz;->a(Lzz;)Lapd;

    move-result-object v2

    invoke-virtual {v2, v1}, Lapd;->a(Lcom/google/android/apps/docs/utils/FetchSpec;)LakD;

    move-result-object v2

    .line 74
    if-eqz v2, :cond_3

    .line 75
    invoke-static {p1, p2, v2}, Lzz;->a(IILakD;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    goto :goto_0

    .line 77
    :cond_3
    if-nez v0, :cond_4

    .line 78
    invoke-static {v4}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    goto :goto_0

    .line 82
    :cond_4
    iget-object v0, p0, LzA;->a:Lzz;

    .line 83
    invoke-static {v0}, Lzz;->a(Lzz;)Lapd;

    move-result-object v0

    invoke-virtual {v0, v1}, Lapd;->a(Lcom/google/android/apps/docs/utils/FetchSpec;)LbsU;

    move-result-object v0

    .line 85
    new-instance v1, LzB;

    invoke-direct {v1, p0, p1, p2}, LzB;-><init>(LzA;II)V

    invoke-static {v0, v1}, LbsK;->a(LbsU;LbiG;)LbsU;

    move-result-object v0

    goto :goto_0
.end method
