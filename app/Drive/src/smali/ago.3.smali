.class public Lago;
.super Ljava/lang/Object;
.source "DownloadUriFetcher.java"


# instance fields
.field private final a:LQr;

.field private final a:Laer;

.field private final a:Lahr;


# direct methods
.method constructor <init>(Lahr;Laer;LQr;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lago;->a:Lahr;

    .line 59
    iput-object p2, p0, Lago;->a:Laer;

    .line 60
    iput-object p3, p0, Lago;->a:LQr;

    .line 61
    return-void
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 203
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "exportFormat"

    .line 204
    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "format"

    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 205
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 206
    return-object v0
.end method


# virtual methods
.method public a(LaGo;LacY;)Lagq;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 112
    invoke-interface {p1}, LaGo;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v1

    .line 113
    if-nez v1, :cond_1

    .line 199
    :cond_0
    :goto_0
    return-object v0

    .line 117
    :cond_1
    invoke-interface {p1}, LaGo;->a()LaGn;

    move-result-object v3

    .line 118
    sget-object v2, LaGn;->c:LaGn;

    invoke-virtual {v2, v3}, LaGn;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 120
    :try_start_0
    iget-object v2, p0, Lago;->a:Lahr;

    const/4 v4, 0x1

    invoke-interface {v2, v1, v4}, Lahr;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Z)Ljava/lang/String;
    :try_end_0
    .catch Lahs; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 127
    :goto_1
    if-eqz v1, :cond_0

    .line 131
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 148
    sget-object v0, Lagp;->a:[I

    invoke-interface {p1}, LaGo;->a()LaGv;

    move-result-object v1

    invoke-virtual {v1}, LaGv;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 192
    sget-object v0, LaGn;->c:LaGn;

    invoke-virtual {v0, v3}, LaGn;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 193
    iget-object v0, p0, Lago;->a:Lahr;

    invoke-interface {v0}, Lahr;->a()Ljava/lang/String;

    move-result-object v1

    move-object v0, v2

    .line 199
    :goto_2
    invoke-static {v0, v1}, Lagq;->a(Landroid/net/Uri;Ljava/lang/String;)Lagq;

    move-result-object v0

    goto :goto_0

    .line 121
    :catch_0
    move-exception v1

    move-object v1, v0

    .line 123
    goto :goto_1

    .line 125
    :cond_2
    iget-object v2, p0, Lago;->a:Laer;

    invoke-interface {v2, v1}, Laer;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 150
    :pswitch_0
    sget-object v0, LacY;->a:LacY;

    invoke-virtual {v0, p2}, LacY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 151
    const-string v0, "zip"

    .line 152
    invoke-interface {p1}, LaGo;->a()LaGv;

    move-result-object v1

    invoke-static {v1, v5}, LaGp;->a(LaGv;Z)Ljava/lang/String;

    move-result-object v1

    .line 158
    :goto_3
    invoke-direct {p0, v2, v0}, Lago;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 165
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "honorPageSize"

    const-string v3, "false"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "footnotesAsEndnotes"

    const-string v3, "true"

    .line 166
    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "showComments"

    const-string v3, "true"

    .line 167
    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_2

    .line 154
    :cond_3
    sget-object v0, LacY;->b:LacY;

    invoke-virtual {v0, p2}, LacY;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LbiT;->a(Z)V

    .line 155
    const-string v0, "pdf"

    .line 156
    const-string v1, "application/pdf"

    goto :goto_3

    .line 170
    :pswitch_1
    const-string v0, "pdf"

    invoke-direct {p0, v2, v0}, Lago;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 171
    const-string v1, "application/pdf"

    goto :goto_2

    .line 174
    :pswitch_2
    const-string v0, "pdf"

    invoke-direct {p0, v2, v0}, Lago;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 175
    const-string v1, "application/pdf"

    goto :goto_2

    .line 178
    :pswitch_3
    sget-object v0, LacY;->a:LacY;

    invoke-virtual {v0, p2}, LacY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 179
    const-string v0, "zip"

    .line 180
    invoke-interface {p1}, LaGo;->a()LaGv;

    move-result-object v1

    invoke-static {v1, v5}, LaGp;->a(LaGv;Z)Ljava/lang/String;

    move-result-object v1

    .line 186
    :goto_4
    iget-object v3, p0, Lago;->a:LQr;

    const-string v4, "offlineKixReadFromIndexJson"

    .line 187
    invoke-interface {v3, v4, v5}, LQr;->a(Ljava/lang/String;Z)Z

    move-result v3

    .line 188
    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "exportFormat"

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "idxp"

    .line 189
    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_2

    .line 182
    :cond_4
    sget-object v0, LacY;->b:LacY;

    invoke-virtual {v0, p2}, LacY;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LbiT;->a(Z)V

    .line 183
    const-string v0, "pdf"

    .line 184
    const-string v1, "application/pdf"

    goto :goto_4

    .line 195
    :cond_5
    invoke-interface {p1}, LaGo;->f()Ljava/lang/String;

    move-result-object v1

    move-object v0, v2

    goto/16 :goto_2

    .line 148
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(LaGu;LacY;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 66
    invoke-interface {p1}, LaGu;->a()LaGn;

    move-result-object v0

    .line 68
    sget-object v1, Lagp;->a:[I

    invoke-interface {p1}, LaGu;->a()LaGv;

    move-result-object v2

    invoke-virtual {v2}, LaGv;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 92
    sget-object v1, LaGn;->c:LaGn;

    invoke-virtual {v1, v0}, LaGn;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 93
    iget-object v0, p0, Lago;->a:Lahr;

    invoke-interface {v0}, Lahr;->a()Ljava/lang/String;

    move-result-object v0

    .line 99
    :goto_0
    return-object v0

    .line 70
    :pswitch_0
    sget-object v0, LacY;->a:LacY;

    invoke-virtual {v0, p2}, LacY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    invoke-interface {p1}, LaGu;->a()LaGv;

    move-result-object v0

    invoke-static {v0, v3}, LaGp;->a(LaGv;Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 73
    :cond_0
    sget-object v0, LacY;->b:LacY;

    invoke-virtual {v0, p2}, LacY;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LbiT;->a(Z)V

    .line 74
    const-string v0, "application/pdf"

    goto :goto_0

    .line 78
    :pswitch_1
    const-string v0, "application/pdf"

    goto :goto_0

    .line 81
    :pswitch_2
    const-string v0, "application/pdf"

    goto :goto_0

    .line 84
    :pswitch_3
    sget-object v0, LacY;->a:LacY;

    invoke-virtual {v0, p2}, LacY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 85
    invoke-interface {p1}, LaGu;->a()LaGv;

    move-result-object v0

    invoke-static {v0, v3}, LaGp;->a(LaGv;Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 87
    :cond_1
    sget-object v0, LacY;->b:LacY;

    invoke-virtual {v0, p2}, LacY;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LbiT;->a(Z)V

    .line 88
    const-string v0, "application/pdf"

    goto :goto_0

    .line 95
    :cond_2
    invoke-interface {p1}, LaGu;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 68
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
