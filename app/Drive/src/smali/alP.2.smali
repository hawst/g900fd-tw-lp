.class public abstract LalP;
.super Landroid/content/ContentProvider;
.source "LazyInjectionContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/content/ContentProvider;"
    }
.end annotation


# instance fields
.field private volatile a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, LalP;->a:Ljava/lang/Object;

    .line 33
    if-nez v0, :cond_1

    .line 34
    monitor-enter p0

    .line 35
    :try_start_0
    iget-object v0, p0, LalP;->a:Ljava/lang/Object;

    .line 36
    if-nez v0, :cond_0

    .line 38
    invoke-virtual {p0}, LalP;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LaiZ;->b(Landroid/content/Context;)LaiY;

    move-result-object v0

    .line 39
    invoke-virtual {p0, v0}, LalP;->a(Lbuu;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LalP;->a:Ljava/lang/Object;

    .line 41
    :cond_0
    monitor-exit p0

    .line 43
    :cond_1
    return-object v0

    .line 41
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public abstract a(Lbuu;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbuu;",
            ")TT;"
        }
    .end annotation
.end method

.method public a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LalP;->a:Ljava/lang/Object;

    .line 29
    return-void
.end method
