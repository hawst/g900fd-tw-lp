.class public final enum LaFj;
.super Ljava/lang/Enum;
.source "SyncRequestTable.java"

# interfaces
.implements Lbjv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaFj;",
        ">;",
        "Lbjv",
        "<",
        "LaFr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaFj;

.field private static final synthetic a:[LaFj;

.field public static final enum b:LaFj;

.field public static final enum c:LaFj;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum d:LaFj;

.field public static final enum e:LaFj;

.field public static final enum f:LaFj;

.field public static final enum g:LaFj;

.field public static final enum h:LaFj;

.field public static final enum i:LaFj;

.field public static final enum j:LaFj;

.field public static final enum k:LaFj;

.field public static final enum l:LaFj;

.field public static final enum m:LaFj;

.field public static final enum n:LaFj;

.field public static final enum o:LaFj;


# instance fields
.field private final a:LaFr;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/16 v8, 0x28

    const/4 v9, 0x0

    .line 45
    new-instance v0, LaFj;

    const-string v1, "ENTRY_ID"

    invoke-static {}, LaFi;->b()LaFi;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "entryId"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 46
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    .line 47
    invoke-static {}, LaER;->a()LaER;

    move-result-object v4

    invoke-virtual {v3, v4}, LaFG;->a(LaFy;)LaFG;

    move-result-object v3

    .line 46
    invoke-virtual {v2, v8, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, LaFj;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFj;->a:LaFj;

    .line 49
    new-instance v0, LaFj;

    const-string v1, "REQUEST_TIME"

    invoke-static {}, LaFi;->b()LaFi;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "requestTime"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 50
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, LaFj;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFj;->b:LaFj;

    .line 52
    new-instance v0, LaFj;

    const-string v1, "__LEGACY_SYNC_TYPE"

    .line 53
    invoke-static {}, LaFi;->b()LaFi;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    const/16 v3, 0x3a

    new-instance v4, LaFG;

    const-string v5, "syncType"

    sget-object v6, LaFI;->c:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 54
    invoke-virtual {v4}, LaFG;->b()LaFG;

    move-result-object v4

    const-string v5, "BINARY"

    .line 55
    invoke-virtual {v4, v5}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v4

    .line 54
    invoke-virtual {v2, v3, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    const/16 v3, 0x5d

    .line 56
    invoke-virtual {v2, v3}, LaFt;->b(I)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, LaFj;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFj;->c:LaFj;

    .line 58
    new-instance v0, LaFj;

    const-string v1, "IS_COMPLETED"

    invoke-static {}, LaFi;->b()LaFi;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "isCompleted"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 59
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v12, v2}, LaFj;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFj;->d:LaFj;

    .line 61
    new-instance v0, LaFj;

    const-string v1, "IS_PAUSED_MANUALLY"

    const/4 v2, 0x4

    invoke-static {}, LaFi;->b()LaFi;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x29

    new-instance v5, LaFG;

    const-string v6, "isPausedManually"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 62
    invoke-virtual {v5}, LaFG;->b()LaFG;

    move-result-object v5

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v5

    .line 61
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaFj;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFj;->e:LaFj;

    .line 63
    new-instance v0, LaFj;

    const-string v1, "IS_UPLOAD_REQUESTED_EVER"

    const/4 v2, 0x5

    invoke-static {}, LaFi;->b()LaFi;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x2b

    new-instance v5, LaFG;

    const-string v6, "isUploadRequestedEver"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 65
    invoke-virtual {v5}, LaFG;->b()LaFG;

    move-result-object v5

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v5

    .line 63
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaFj;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFj;->f:LaFj;

    .line 66
    new-instance v0, LaFj;

    const-string v1, "IS_IMPLICIT"

    const/4 v2, 0x6

    invoke-static {}, LaFi;->b()LaFi;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x38

    new-instance v5, LaFG;

    const-string v6, "isImplicit"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 67
    invoke-virtual {v5}, LaFG;->b()LaFG;

    move-result-object v5

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaFj;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFj;->g:LaFj;

    .line 69
    new-instance v0, LaFj;

    const-string v1, "IS_CONNECTIVITY_POLICY_OVERRIDEN"

    const/4 v2, 0x7

    invoke-static {}, LaFi;->b()LaFi;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x52

    new-instance v5, LaFG;

    const-string v6, "isConnectivityPolicyOverriden"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 70
    invoke-virtual {v5}, LaFG;->b()LaFG;

    move-result-object v5

    .line 71
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v5

    .line 69
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaFj;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFj;->h:LaFj;

    .line 84
    new-instance v0, LaFj;

    const-string v1, "BATCH_NUMBER"

    const/16 v2, 0x8

    invoke-static {}, LaFi;->b()LaFi;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x41

    new-instance v5, LaFG;

    const-string v6, "batchNumber"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 85
    invoke-virtual {v5}, LaFG;->b()LaFG;

    move-result-object v5

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v5

    invoke-virtual {v5}, LaFG;->a()LaFG;

    move-result-object v5

    .line 84
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaFj;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFj;->i:LaFj;

    .line 93
    new-instance v0, LaFj;

    const-string v1, "SYNC_DIRECTION_IN_BATCH"

    const/16 v2, 0x9

    invoke-static {}, LaFi;->b()LaFi;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x42

    new-instance v5, LaFG;

    const-string v6, "syncDirectionInBatch"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 94
    invoke-virtual {v5}, LaFG;->b()LaFG;

    move-result-object v5

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v5

    .line 93
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaFj;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFj;->j:LaFj;

    .line 95
    new-instance v0, LaFj;

    const-string v1, "BYTES_TRANSFERRED"

    const/16 v2, 0xa

    invoke-static {}, LaFi;->b()LaFi;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x43

    new-instance v5, LaFG;

    const-string v6, "bytesTransferred"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 96
    invoke-virtual {v5}, LaFG;->b()LaFG;

    move-result-object v5

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v5

    .line 95
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaFj;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFj;->k:LaFj;

    .line 97
    new-instance v0, LaFj;

    const-string v1, "ATTEMPT_COUNT"

    const/16 v2, 0xb

    invoke-static {}, LaFi;->b()LaFi;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "attemptCount"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 98
    invoke-virtual {v4}, LaFG;->b()LaFG;

    move-result-object v4

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v4

    invoke-virtual {v3, v8, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaFj;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFj;->l:LaFj;

    .line 104
    new-instance v0, LaFj;

    const-string v1, "UPLOAD_URI"

    const/16 v2, 0xc

    invoke-static {}, LaFi;->b()LaFi;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x2d

    new-instance v5, LaFG;

    const-string v6, "uploadUri"

    sget-object v7, LaFI;->c:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 105
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaFj;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFj;->m:LaFj;

    .line 112
    new-instance v0, LaFj;

    const-string v1, "UPLOAD_SNAPSHOT_LAST_MODIFIED_TIME"

    const/16 v2, 0xd

    invoke-static {}, LaFi;->b()LaFi;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x44

    new-instance v5, LaFG;

    const-string v6, "uploadSnapshotLastModifiedTime"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 113
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaFj;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFj;->n:LaFj;

    .line 122
    new-instance v0, LaFj;

    const-string v1, "DOCUMENT_CONTENT_ID"

    const/16 v2, 0xe

    invoke-static {}, LaFi;->b()LaFi;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x37

    new-instance v5, LaFG;

    const-string v6, "documentContentId"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 124
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(LaFy;)LaFG;

    move-result-object v5

    .line 123
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    const/16 v4, 0x45

    .line 125
    invoke-virtual {v3, v4}, LaFt;->b(I)LaFt;

    move-result-object v3

    const/16 v4, 0x45

    new-instance v5, LaFG;

    const-string v6, "documentContentId"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 127
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v6

    const/4 v7, 0x0

    sget-object v8, LaFH;->b:LaFH;

    .line 126
    invoke-virtual {v5, v6, v7, v8}, LaFG;->a(LaFy;LaFr;LaFH;)LaFG;

    move-result-object v5

    .line 128
    invoke-virtual {v5}, LaFG;->a()LaFG;

    move-result-object v5

    .line 126
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaFj;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFj;->o:LaFj;

    .line 44
    const/16 v0, 0xf

    new-array v0, v0, [LaFj;

    sget-object v1, LaFj;->a:LaFj;

    aput-object v1, v0, v9

    sget-object v1, LaFj;->b:LaFj;

    aput-object v1, v0, v10

    sget-object v1, LaFj;->c:LaFj;

    aput-object v1, v0, v11

    sget-object v1, LaFj;->d:LaFj;

    aput-object v1, v0, v12

    const/4 v1, 0x4

    sget-object v2, LaFj;->e:LaFj;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LaFj;->f:LaFj;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LaFj;->g:LaFj;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LaFj;->h:LaFj;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LaFj;->i:LaFj;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LaFj;->j:LaFj;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LaFj;->k:LaFj;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LaFj;->l:LaFj;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LaFj;->m:LaFj;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LaFj;->n:LaFj;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LaFj;->o:LaFj;

    aput-object v2, v0, v1

    sput-object v0, LaFj;->a:[LaFj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaFt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 132
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 133
    invoke-virtual {p3}, LaFt;->a()LaFr;

    move-result-object v0

    iput-object v0, p0, LaFj;->a:LaFr;

    .line 134
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaFj;
    .locals 1

    .prologue
    .line 44
    const-class v0, LaFj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaFj;

    return-object v0
.end method

.method public static values()[LaFj;
    .locals 1

    .prologue
    .line 44
    sget-object v0, LaFj;->a:[LaFj;

    invoke-virtual {v0}, [LaFj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaFj;

    return-object v0
.end method


# virtual methods
.method public a()LaFr;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, LaFj;->a:LaFr;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, LaFj;->a()LaFr;

    move-result-object v0

    return-object v0
.end method
