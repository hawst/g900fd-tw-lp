.class public LyB;
.super LyR;
.source "ActivityCard.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements LyX;


# instance fields
.field private a:I

.field private final a:LPB;

.field private a:LaGu;

.field private final a:LaKM;

.field private final a:Lamw;

.field private final a:Landroid/content/Context;

.field private final a:Landroid/database/DataSetObserver;

.field private final a:Landroid/view/View$OnClickListener;

.field private final a:Landroid/view/View;

.field private a:Laqs;

.field private final a:LbsJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbsJ",
            "<",
            "Ljava/util/List",
            "<",
            "LPD;",
            ">;>;"
        }
    .end annotation
.end field

.field private a:LbsU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbsU",
            "<+",
            "Ljava/util/List",
            "<",
            "LPD;",
            ">;>;"
        }
    .end annotation
.end field

.field private final a:LqK;

.field private final a:LtK;

.field private final a:LyN;

.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LaKM;LtK;LyW;LyV;LPB;LyN;LqK;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 154
    invoke-direct {p0}, LyR;-><init>()V

    .line 67
    new-instance v0, LyC;

    invoke-direct {v0, p0}, LyC;-><init>(LyB;)V

    iput-object v0, p0, LyB;->a:LbsJ;

    .line 98
    new-instance v0, LyD;

    invoke-direct {v0, p0}, LyD;-><init>(LyB;)V

    iput-object v0, p0, LyB;->a:Landroid/database/DataSetObserver;

    .line 112
    new-instance v0, LyE;

    invoke-direct {v0, p0}, LyE;-><init>(LyB;)V

    iput-object v0, p0, LyB;->a:Landroid/view/View$OnClickListener;

    .line 134
    new-instance v0, Laqs;

    .line 135
    invoke-static {}, LbmF;->c()LbmF;

    move-result-object v1

    invoke-direct {v0, v1}, Laqs;-><init>(LbmF;)V

    iput-object v0, p0, LyB;->a:Laqs;

    .line 138
    invoke-static {}, LbmF;->c()LbmF;

    move-result-object v0

    invoke-static {v0}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    iput-object v0, p0, LyB;->a:LbsU;

    .line 143
    const/4 v0, -0x1

    iput v0, p0, LyB;->a:I

    .line 144
    iput-boolean v3, p0, LyB;->a:Z

    .line 155
    iput-object p1, p0, LyB;->a:Landroid/content/Context;

    .line 156
    iput-object p3, p0, LyB;->a:LtK;

    .line 157
    iput-object p2, p0, LyB;->a:LaKM;

    .line 158
    iput-object p6, p0, LyB;->a:LPB;

    .line 159
    iput-object p8, p0, LyB;->a:LqK;

    .line 161
    invoke-virtual {p5, p0}, LyV;->a(LyX;)V

    .line 162
    invoke-virtual {p4, p0}, LyW;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 164
    const-string v0, "layout_inflater"

    .line 165
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 166
    sget v1, Lxe;->detail_card_activity_bottom:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LyB;->a:Landroid/view/View;

    .line 168
    new-instance v0, LyF;

    invoke-direct {v0, p0}, LyF;-><init>(LyB;)V

    .line 174
    sget-object v1, Lamy;->a:Lamx;

    const/16 v2, 0x3e8

    .line 175
    invoke-static {}, Lanj;->a()Ljava/util/concurrent/Executor;

    move-result-object v3

    const-string v4, "avatarRefreshLimiter"

    .line 174
    invoke-interface {v1, v0, v2, v3, v4}, Lamx;->a(Ljava/lang/Runnable;ILjava/util/concurrent/Executor;Ljava/lang/String;)Lamw;

    move-result-object v0

    iput-object v0, p0, LyB;->a:Lamw;

    .line 177
    iput-object p7, p0, LyB;->a:LyN;

    .line 178
    return-void
.end method

.method static synthetic a(LyB;I)I
    .locals 0

    .prologue
    .line 50
    iput p1, p0, LyB;->a:I

    return p1
.end method

.method static synthetic a(LyB;)LaGu;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, LyB;->a:LaGu;

    return-object v0
.end method

.method static synthetic a(LyB;)Lamw;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, LyB;->a:Lamw;

    return-object v0
.end method

.method static synthetic a(LyB;)Landroid/database/DataSetObserver;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, LyB;->a:Landroid/database/DataSetObserver;

    return-object v0
.end method

.method private a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 229
    if-nez p1, :cond_0

    .line 230
    iget-object v0, p0, LyB;->a:Landroid/content/Context;

    const-string v1, "layout_inflater"

    .line 231
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 232
    sget v1, Lxe;->detail_card_activity_top:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 236
    :cond_0
    return-object p1
.end method

.method static synthetic a(LyB;)Landroid/view/View;
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, LyB;->b()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LyB;)Laqs;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, LyB;->a:Laqs;

    return-object v0
.end method

.method static synthetic a(LyB;Laqs;)Laqs;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, LyB;->a:Laqs;

    return-object p1
.end method

.method static synthetic a(LyB;)LqK;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, LyB;->a:LqK;

    return-object v0
.end method

.method static synthetic a(LyB;)LtK;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, LyB;->a:LtK;

    return-object v0
.end method

.method private a()LyH;
    .locals 2

    .prologue
    .line 349
    iget-object v0, p0, LyB;->a:LPB;

    iget-object v1, p0, LyB;->a:LaGu;

    invoke-interface {v1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-virtual {v0, v1}, LPB;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LPy;

    move-result-object v0

    .line 350
    iget-object v1, p0, LyB;->a:LbsU;

    invoke-interface {v1}, LbsU;->isDone()Z

    move-result v1

    if-nez v1, :cond_0

    .line 351
    sget-object v0, LyH;->d:LyH;

    .line 367
    :goto_0
    return-object v0

    .line 355
    :cond_0
    :try_start_0
    iget-object v1, p0, LyB;->a:LbsU;

    invoke-interface {v1}, LbsU;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 362
    iget-object v1, p0, LyB;->a:Laqs;

    invoke-virtual {v1}, Laqs;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 363
    sget-object v0, LyH;->b:LyH;

    goto :goto_0

    .line 356
    :catch_0
    move-exception v0

    .line 357
    sget-object v0, LyH;->c:LyH;

    goto :goto_0

    .line 358
    :catch_1
    move-exception v0

    .line 359
    sget-object v0, LyH;->c:LyH;

    goto :goto_0

    .line 364
    :cond_1
    invoke-virtual {v0}, LPy;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 365
    sget-object v0, LyH;->e:LyH;

    goto :goto_0

    .line 367
    :cond_2
    sget-object v0, LyH;->a:LyH;

    goto :goto_0
.end method

.method static synthetic a(LyB;)LyN;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, LyB;->a:LyN;

    return-object v0
.end method

.method private a()V
    .locals 5

    .prologue
    .line 337
    iget-object v0, p0, LyB;->a:LaGu;

    if-nez v0, :cond_0

    .line 346
    :goto_0
    return-void

    .line 341
    :cond_0
    const-string v0, "ActivityCard"

    const-string v1, "Loading data from %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, LyB;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 342
    iget-object v0, p0, LyB;->a:LPB;

    iget-object v1, p0, LyB;->a:LaGu;

    invoke-interface {v1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-virtual {v0, v1}, LPB;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LPy;

    move-result-object v0

    .line 343
    iget v1, p0, LyB;->a:I

    invoke-virtual {v0, v1}, LPy;->a(I)LbsU;

    move-result-object v0

    iput-object v0, p0, LyB;->a:LbsU;

    .line 344
    iget-object v0, p0, LyB;->a:LbsU;

    iget-object v1, p0, LyB;->a:LbsJ;

    invoke-static {}, Lanj;->a()Ljava/util/concurrent/Executor;

    move-result-object v2

    invoke-static {v0, v1, v2}, LbsK;->a(LbsU;LbsJ;Ljava/util/concurrent/Executor;)V

    .line 345
    invoke-virtual {p0}, LyB;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x8

    .line 270
    iget-object v0, p0, LyB;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 271
    iget-object v0, p0, LyB;->a:LPB;

    iget-object v1, p0, LyB;->a:LaGu;

    invoke-interface {v1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-virtual {v0, v1}, LPB;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LPy;

    move-result-object v3

    .line 273
    sget v0, Lxc;->progress:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 274
    sget v1, Lxc;->recents_status:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 276
    sget-object v4, LyG;->a:[I

    invoke-direct {p0}, LyB;->a()LyH;

    move-result-object v5

    invoke-virtual {v5}, LyH;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 301
    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 302
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 304
    :goto_0
    return-void

    .line 278
    :pswitch_0
    invoke-virtual {v0, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 279
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 282
    :pswitch_1
    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 283
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 284
    sget v0, Lxi;->recent_activity_empty:I

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 287
    :pswitch_2
    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 288
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 289
    sget v0, Lxi;->recent_activity_failed:I

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 292
    :pswitch_3
    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 293
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 294
    iget-object v4, p0, LyB;->a:LaKM;

    invoke-interface {v4}, LaKM;->a()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 295
    iget-object v0, p0, LyB;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->getLongDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 296
    invoke-virtual {v3}, LPy;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 297
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 298
    sget v3, Lxi;->recent_activity_end:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 276
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(LyB;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, LyB;->a()V

    return-void
.end method

.method static synthetic a(LyB;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, LyB;->a:Z

    return v0
.end method

.method static synthetic a(LyB;Z)Z
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, LyB;->a:Z

    return p1
.end method

.method private b()Landroid/view/View;
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 240
    iget-object v0, p0, LyB;->a:LaGu;

    if-nez v0, :cond_0

    .line 241
    iget-object v0, p0, LyB;->a:Landroid/view/View;

    .line 266
    :goto_0
    return-object v0

    .line 244
    :cond_0
    iget-object v0, p0, LyB;->a:Landroid/view/View;

    sget v3, Lxc;->recent_activity_button_load_more:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 245
    iget-object v0, p0, LyB;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 247
    iget-object v0, p0, LyB;->a:Landroid/view/View;

    const v3, 0x1020004

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 249
    iget-object v0, p0, LyB;->a:LPB;

    iget-object v3, p0, LyB;->a:LaGu;

    invoke-interface {v3}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v3

    invoke-virtual {v0, v3}, LPB;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LPy;

    move-result-object v6

    .line 251
    invoke-direct {p0}, LyB;->a()LyH;

    move-result-object v7

    .line 252
    iget-object v0, p0, LyB;->a:Laqs;

    invoke-virtual {v0}, Laqs;->a()I

    move-result v0

    if-ge v0, v8, :cond_3

    move v0, v1

    .line 253
    :goto_1
    iget v3, p0, LyB;->a:I

    if-gt v3, v8, :cond_4

    move v3, v1

    .line 255
    :goto_2
    invoke-virtual {v6}, LPy;->a()Z

    move-result v6

    if-eqz v6, :cond_5

    if-nez v0, :cond_1

    if-eqz v3, :cond_5

    .line 257
    :cond_1
    :goto_3
    iget-boolean v0, p0, LyB;->a:Z

    if-nez v0, :cond_2

    sget-object v0, LyH;->d:LyH;

    invoke-virtual {v7, v0}, LyH;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz v1, :cond_6

    .line 258
    :cond_2
    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 259
    invoke-virtual {v5, v2}, Landroid/view/View;->setVisibility(I)V

    .line 260
    invoke-direct {p0, v5}, LyB;->a(Landroid/view/View;)V

    .line 266
    :goto_4
    iget-object v0, p0, LyB;->a:Landroid/view/View;

    goto :goto_0

    :cond_3
    move v0, v2

    .line 252
    goto :goto_1

    :cond_4
    move v3, v2

    .line 253
    goto :goto_2

    :cond_5
    move v1, v2

    .line 255
    goto :goto_3

    .line 262
    :cond_6
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 263
    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4
.end method

.method static synthetic b(LyB;)Landroid/view/View;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, LyB;->a:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method protected a()Landroid/view/View;
    .locals 1

    .prologue
    .line 373
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(LaGu;)V
    .locals 2

    .prologue
    .line 322
    iget-object v0, p0, LyB;->a:LaGu;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 334
    :goto_0
    return-void

    .line 326
    :cond_0
    iput-object p1, p0, LyB;->a:LaGu;

    .line 327
    const/4 v0, 0x0

    iput-boolean v0, p0, LyB;->a:Z

    .line 328
    const/4 v0, -0x1

    iput v0, p0, LyB;->a:I

    .line 329
    new-instance v0, Laqs;

    invoke-static {}, LbmF;->c()LbmF;

    move-result-object v1

    invoke-direct {v0, v1}, Laqs;-><init>(LbmF;)V

    iput-object v0, p0, LyB;->a:Laqs;

    .line 331
    invoke-direct {p0}, LyB;->a()V

    .line 333
    invoke-virtual {p0}, LyB;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 393
    const-string v0, "ActivityCard_expanded"

    iget-boolean v1, p0, LyB;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 394
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, LyB;->a:LaGu;

    if-eqz v0, :cond_0

    iget-object v0, p0, LyB;->a:LaGu;

    invoke-interface {v0}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 398
    const-string v0, "ActivityCard"

    const-string v1, "in onRestoreInstanceState"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 399
    if-eqz p1, :cond_0

    .line 400
    const-string v0, "ActivityCard_expanded"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, LyB;->a:Z

    .line 402
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 313
    invoke-virtual {p0}, LyB;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 314
    const/4 v0, 0x0

    .line 317
    :goto_0
    return v0

    .line 316
    :cond_0
    iget-object v0, p0, LyB;->a:Laqs;

    invoke-virtual {v0}, Laqs;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x1

    .line 317
    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 212
    if-nez p1, :cond_0

    .line 213
    const/4 v0, 0x0

    .line 217
    :goto_0
    return v0

    .line 214
    :cond_0
    add-int/lit8 v0, p1, -0x1

    iget-object v1, p0, LyB;->a:Laqs;

    invoke-virtual {v1}, Laqs;->getCount()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 215
    const/4 v0, 0x1

    goto :goto_0

    .line 217
    :cond_1
    iget-object v0, p0, LyB;->a:Laqs;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Laqs;->a(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 182
    invoke-virtual {p0, p1}, LyB;->getItemViewType(I)I

    move-result v0

    .line 183
    if-nez v0, :cond_0

    .line 184
    invoke-direct {p0, p2, p3}, LyB;->a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 188
    :goto_0
    return-object v0

    .line 185
    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 186
    invoke-direct {p0}, LyB;->b()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 188
    :cond_1
    iget-object v0, p0, LyB;->a:Laqs;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1, p2, p3}, Laqs;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 224
    const/4 v0, 0x6

    return v0
.end method

.method public isEnabled(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 194
    invoke-virtual {p0, p1}, LyB;->getItemViewType(I)I

    move-result v2

    .line 195
    if-nez v2, :cond_0

    .line 202
    :goto_0
    return v1

    .line 198
    :cond_0
    if-ne v2, v0, :cond_2

    .line 199
    iget-boolean v2, p0, LyB;->a:Z

    if-nez v2, :cond_1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 201
    :cond_2
    iget-object v0, p0, LyB;->a:Laqs;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Laqs;->isEnabled(I)Z

    move-result v1

    goto :goto_0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 389
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 3

    .prologue
    .line 378
    if-nez p2, :cond_0

    iget-boolean v0, p0, LyB;->a:Z

    if-eqz v0, :cond_0

    .line 379
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getCount()I

    move-result v0

    invoke-virtual {p1}, Landroid/widget/AbsListView;->getLastVisiblePosition()I

    move-result v1

    sub-int/2addr v0, v1

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    .line 380
    iget-object v0, p0, LyB;->a:LqK;

    const-string v1, "activityStream"

    const-string v2, "showMoreActivityEvent"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    invoke-direct {p0}, LyB;->a()V

    .line 383
    invoke-direct {p0}, LyB;->b()Landroid/view/View;

    .line 385
    :cond_0
    return-void
.end method
