.class public final LJV;
.super Ljava/lang/Object;
.source "Reason.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/apps/docs/doclist/selection/ItemKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<*>;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lcom/google/android/apps/docs/doclist/selection/ItemKey;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, LJV;->a:Ljava/lang/Object;

    .line 19
    iput-object p2, p0, LJV;->a:Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    .line 20
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 37
    if-ne p0, p1, :cond_1

    .line 43
    :cond_0
    :goto_0
    return v0

    .line 39
    :cond_1
    instance-of v2, p1, LJV;

    if-eqz v2, :cond_3

    .line 40
    check-cast p1, LJV;

    .line 41
    iget-object v2, p0, LJV;->a:Ljava/lang/Object;

    iget-object v3, p1, LJV;->a:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, LJV;->a:Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    iget-object v3, p1, LJV;->a:Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    invoke-static {v2, v3}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 43
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 32
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LJV;->a:Ljava/lang/Object;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LJV;->a:Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
