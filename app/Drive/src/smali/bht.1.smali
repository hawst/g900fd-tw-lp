.class Lbht;
.super Landroid/view/MenuInflater;
.source "EditorActionBarBase.java"


# instance fields
.field a:Landroid/view/MenuInflater;

.field final synthetic a:Lbhp;


# direct methods
.method public constructor <init>(Lbhp;Landroid/content/Context;Landroid/view/MenuInflater;)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, Lbht;->a:Lbhp;

    .line 289
    invoke-direct {p0, p2}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    .line 290
    iput-object p3, p0, Lbht;->a:Landroid/view/MenuInflater;

    .line 291
    return-void
.end method

.method private a(I)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 305
    const/4 v1, 0x0

    .line 307
    :try_start_0
    iget-object v3, p0, Lbht;->a:Lbhp;

    iget-object v3, v3, Lbhp;->a:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v1

    .line 309
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v3

    .line 314
    :goto_0
    if-nez v0, :cond_3

    .line 315
    packed-switch v3, :pswitch_data_0

    .line 340
    :cond_0
    :goto_1
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v3

    goto :goto_0

    .line 317
    :pswitch_0
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "item"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 321
    const-string v3, "http://schemas.android.com/apk/res/android"

    const-string v4, "id"

    const/4 v5, 0x0

    invoke-interface {v1, v3, v4, v5}, Landroid/content/res/XmlResourceParser;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v3

    .line 323
    if-eqz v3, :cond_0

    .line 327
    const-string v4, "http://schemas.android.com/apk/res/android"

    const-string v5, "showAsAction"

    const/4 v6, -0x1

    invoke-interface {v1, v4, v5, v6}, Landroid/content/res/XmlResourceParser;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v4

    .line 329
    const/4 v5, 0x2

    if-eq v4, v5, :cond_1

    if-ne v4, v2, :cond_0

    .line 331
    :cond_1
    iget-object v4, p0, Lbht;->a:Lbhp;

    iget-object v4, v4, Lbhp;->a:Ljava/util/Set;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 342
    :catch_0
    move-exception v0

    .line 343
    :try_start_1
    new-instance v2, Landroid/view/InflateException;

    const-string v3, "Error inflating menu XML"

    invoke-direct {v2, v3, v0}, Landroid/view/InflateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 347
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 348
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    :cond_2
    throw v0

    :pswitch_1
    move v0, v2

    .line 336
    goto :goto_1

    .line 347
    :cond_3
    if-eqz v1, :cond_4

    .line 348
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    .line 351
    :cond_4
    return-void

    .line 344
    :catch_1
    move-exception v0

    .line 345
    :try_start_2
    new-instance v2, Landroid/view/InflateException;

    const-string v3, "Error inflating menu XML"

    invoke-direct {v2, v3, v0}, Landroid/view/InflateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 315
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public inflate(ILandroid/view/Menu;)V
    .locals 1

    .prologue
    .line 295
    invoke-direct {p0, p1}, Lbht;->a(I)V

    .line 296
    iget-object v0, p0, Lbht;->a:Landroid/view/MenuInflater;

    invoke-virtual {v0, p1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 297
    return-void
.end method
