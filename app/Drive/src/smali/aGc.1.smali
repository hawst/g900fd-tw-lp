.class public LaGc;
.super LaGe;
.source "DatabaseDocumentEditor.java"


# instance fields
.field private a:J

.field private a:Ljava/lang/Long;

.field private a:Ljava/lang/String;

.field private a:Z

.field private b:J

.field private b:Ljava/lang/Long;

.field private b:Ljava/lang/String;

.field private c:J


# direct methods
.method private constructor <init>(LaEz;LaFM;Landroid/database/Cursor;)V
    .locals 4

    .prologue
    const-wide/16 v0, -0x1

    .line 56
    invoke-direct {p0, p1, p2, p3}, LaGe;-><init>(LaEz;LaFM;Landroid/database/Cursor;)V

    .line 30
    iput-wide v0, p0, LaGc;->a:J

    .line 33
    iput-wide v0, p0, LaGc;->b:J

    .line 34
    iput-wide v0, p0, LaGc;->c:J

    .line 58
    sget-object v0, LaEL;->b:LaEL;

    invoke-virtual {v0}, LaEL;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 59
    invoke-virtual {p0}, LaGc;->c()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 61
    sget-object v0, LaEL;->a:LaEL;

    invoke-virtual {v0}, LaEL;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LaGc;->a:Z

    .line 63
    sget-object v0, LaEL;->c:LaEL;

    invoke-virtual {v0}, LaEL;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    .line 64
    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sget-object v2, LacY;->a:LacY;

    invoke-virtual {p0, v0, v1, v2}, LaGc;->a(JLacY;)LaGc;

    .line 68
    :cond_0
    sget-object v0, LaEL;->d:LaEL;

    invoke-virtual {v0}, LaEL;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    .line 69
    if-eqz v0, :cond_1

    .line 70
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sget-object v2, LacY;->b:LacY;

    invoke-virtual {p0, v0, v1, v2}, LaGc;->a(JLacY;)LaGc;

    .line 73
    :cond_1
    invoke-static {}, LaEK;->a()LaEK;

    move-result-object v0

    invoke-virtual {v0}, LaEK;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, v0}, LaFr;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LaGc;->a(J)V

    .line 75
    sget-object v0, LaEL;->e:LaEL;

    invoke-virtual {v0}, LaEL;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LaGc;->a(Ljava/lang/String;)LaGc;

    .line 77
    sget-object v0, LaEL;->h:LaEL;

    invoke-virtual {v0}, LaEL;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LaGc;->a(Ljava/lang/String;)V

    .line 78
    sget-object v0, LaEL;->l:LaEL;

    invoke-virtual {v0}, LaEL;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, LaGc;->a(Ljava/lang/Long;)LaGc;

    .line 79
    sget-object v0, LaEL;->m:LaEL;

    invoke-virtual {v0}, LaEL;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p3}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    .line 80
    if-eqz v0, :cond_2

    .line 81
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LaGc;->b(J)V

    .line 83
    :cond_2
    return-void

    .line 59
    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method protected constructor <init>(LaEz;LaFM;Ljava/lang/String;LaHa;)V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 52
    invoke-direct {p0, p1, p2, p3, p4}, LaGe;-><init>(LaEz;LaFM;Ljava/lang/String;LaHa;)V

    .line 30
    iput-wide v0, p0, LaGc;->a:J

    .line 33
    iput-wide v0, p0, LaGc;->b:J

    .line 34
    iput-wide v0, p0, LaGc;->c:J

    .line 53
    return-void
.end method

.method public constructor <init>(LaEz;LaFM;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 44
    invoke-direct {p0, p1, p2, p3, p4}, LaGe;-><init>(LaEz;LaFM;Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    iput-wide v0, p0, LaGc;->a:J

    .line 33
    iput-wide v0, p0, LaGc;->b:J

    .line 34
    iput-wide v0, p0, LaGc;->c:J

    .line 45
    return-void
.end method

.method public static a(LaEz;LaFM;Landroid/database/Cursor;)LaGc;
    .locals 1

    .prologue
    .line 227
    new-instance v0, LaGc;

    invoke-direct {v0, p0, p1, p2}, LaGc;-><init>(LaEz;LaFM;Landroid/database/Cursor;)V

    return-object v0
.end method

.method private a(J)Landroid/content/ContentValues;
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 195
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 196
    sget-object v0, LaEL;->a:LaEL;

    invoke-virtual {v0}, LaEL;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    iget-boolean v0, p0, LaGc;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 197
    sget-object v0, LaEL;->b:LaEL;

    invoke-virtual {v0}, LaEL;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 198
    sget-object v0, LaEL;->e:LaEL;

    invoke-virtual {v0}, LaEL;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaGc;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    sget-object v0, LaEL;->h:LaEL;

    invoke-virtual {v0}, LaEL;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaGc;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    sget-object v0, LaEL;->l:LaEL;

    invoke-virtual {v0}, LaEL;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaGc;->a()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 201
    sget-object v0, LaEL;->m:LaEL;

    invoke-virtual {v0}, LaEL;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaGc;->b()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 204
    sget-object v0, LacY;->a:LacY;

    invoke-virtual {p0, v0}, LaGc;->a(LacY;)J

    move-result-wide v2

    .line 205
    cmp-long v0, v2, v4

    if-ltz v0, :cond_1

    .line 206
    sget-object v0, LaEL;->c:LaEL;

    invoke-virtual {v0}, LaEL;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 211
    :goto_1
    sget-object v0, LacY;->b:LacY;

    invoke-virtual {p0, v0}, LaGc;->a(LacY;)J

    move-result-wide v2

    .line 212
    cmp-long v0, v2, v4

    if-ltz v0, :cond_2

    .line 213
    sget-object v0, LaEL;->d:LaEL;

    invoke-virtual {v0}, LaEL;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 218
    :goto_2
    return-object v1

    .line 196
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 208
    :cond_1
    sget-object v0, LaEL;->c:LaEL;

    invoke-virtual {v0}, LaEL;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_1

    .line 215
    :cond_2
    sget-object v0, LaEL;->d:LaEL;

    invoke-virtual {v0}, LaEL;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 89
    iget-wide v0, p0, LaGc;->a:J

    return-wide v0
.end method

.method public a(LacY;)J
    .locals 2

    .prologue
    .line 123
    sget-object v0, LacY;->a:LacY;

    if-ne p1, v0, :cond_0

    .line 124
    iget-wide v0, p0, LaGc;->b:J

    .line 126
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, LaGc;->c:J

    goto :goto_0
.end method

.method public a()LaGb;
    .locals 1

    .prologue
    .line 257
    invoke-static {p0}, LaGb;->a(LaGc;)LaGb;

    move-result-object v0

    return-object v0
.end method

.method protected a()LaGc;
    .locals 1

    .prologue
    .line 268
    invoke-super {p0}, LaGe;->a()LaGe;

    move-result-object v0

    check-cast v0, LaGc;

    return-object v0
.end method

.method public a(JLacY;)LaGc;
    .locals 1

    .prologue
    .line 134
    sget-object v0, LacY;->a:LacY;

    if-ne p3, v0, :cond_0

    .line 135
    iput-wide p1, p0, LaGc;->b:J

    .line 139
    :goto_0
    return-object p0

    .line 137
    :cond_0
    iput-wide p1, p0, LaGc;->c:J

    goto :goto_0
.end method

.method public a(Ljava/lang/Long;)LaGc;
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, LaGc;->a:Ljava/lang/Long;

    .line 184
    return-object p0
.end method

.method public a(Ljava/lang/String;)LaGc;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, LaGc;->a:Ljava/lang/String;

    .line 116
    return-object p0
.end method

.method public bridge synthetic a()LaGd;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, LaGc;->a()LaGb;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a()LaGe;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, LaGc;->a()LaGc;

    move-result-object v0

    return-object v0
.end method

.method public a(LaGp;)LacY;
    .locals 4

    .prologue
    .line 148
    invoke-virtual {p1}, LaGp;->c()J

    move-result-wide v0

    .line 149
    iget-wide v2, p0, LaGc;->b:J

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 150
    sget-object v0, LacY;->a:LacY;

    .line 154
    :goto_0
    return-object v0

    .line 151
    :cond_0
    iget-wide v2, p0, LaGc;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 152
    sget-object v0, LacY;->b:LacY;

    goto :goto_0

    .line 154
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, LaGc;->a:Ljava/lang/Long;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, LaGc;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(J)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 98
    cmp-long v0, p1, v2

    if-ltz v0, :cond_0

    iget-wide v0, p0, LaGc;->a:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    iget-wide v0, p0, LaGc;->a:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 100
    iput-wide p1, p0, LaGc;->a:J

    .line 101
    return-void

    .line 98
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(LaEz;)V
    .locals 4

    .prologue
    .line 244
    invoke-virtual {p0}, LaGc;->a()J

    move-result-wide v0

    invoke-static {}, LaEK;->a()LaEK;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, LaEz;->a(JLaFy;Landroid/net/Uri;)I

    .line 245
    const-wide/16 v0, -0x1

    invoke-virtual {p0, v0, v1}, LaGc;->a(J)V

    .line 246
    return-void
.end method

.method protected a(LaEz;J)V
    .locals 8

    .prologue
    .line 233
    .line 234
    invoke-virtual {p0}, LaGc;->a()J

    move-result-wide v2

    invoke-static {}, LaEK;->a()LaEK;

    move-result-object v4

    .line 235
    invoke-direct {p0, p2, p3}, LaGc;->a(J)Landroid/content/ContentValues;

    move-result-object v5

    sget-object v0, LaEG;->a:LaEG;

    invoke-virtual {v0}, LaEG;->a()Landroid/net/Uri;

    move-result-object v6

    move-object v1, p1

    .line 234
    invoke-virtual/range {v1 .. v6}, LaEz;->a(JLaFy;Landroid/content/ContentValues;Landroid/net/Uri;)J

    move-result-wide v0

    .line 236
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 237
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Error saving document"

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 239
    :cond_0
    invoke-virtual {p0, v0, v1}, LaGc;->a(J)V

    .line 240
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, LaGc;->b:Ljava/lang/String;

    .line 170
    return-void
.end method

.method public b()LaGb;
    .locals 1

    .prologue
    .line 262
    invoke-virtual {p0}, LaGc;->e()V

    .line 263
    invoke-virtual {p0}, LaGc;->a()LaGb;

    move-result-object v0

    return-object v0
.end method

.method public b()LaGc;
    .locals 1

    .prologue
    .line 274
    :try_start_0
    invoke-virtual {p0}, LaGc;->a()LaGc;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 275
    :catch_0
    move-exception v0

    .line 276
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public bridge synthetic b()LaGd;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, LaGc;->b()LaGb;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, LaGc;->b:Ljava/lang/Long;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, LaGc;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 188
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LaGc;->b:Ljava/lang/Long;

    .line 189
    return-void
.end method

.method protected synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, LaGc;->a()LaGc;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 250
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "Document(super=%s, documentSqlId=%d, htmlUri=%s, documentContentId=%d"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 251
    invoke-super {p0}, LaGe;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, LaGc;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, LaGc;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-wide v4, p0, LaGc;->b:J

    .line 252
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    .line 250
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
