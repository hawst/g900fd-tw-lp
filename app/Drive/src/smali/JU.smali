.class public LJU;
.super Ljava/lang/Object;
.source "LongClickDragStarter.java"

# interfaces
.implements LaqU;


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field private final a:F

.field private a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

.field private b:F

.field private c:F

.field private d:F

.field private e:F


# direct methods
.method public constructor <init>(LaqT;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-virtual {p1, p0}, LaqT;->a(LaqU;)V

    .line 34
    invoke-static {p2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v0

    int-to-float v0, v0

    .line 35
    mul-float/2addr v0, v0

    iput v0, p0, LJU;->a:F

    .line 36
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 66
    iget-object v0, p0, LJU;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    if-eqz v0, :cond_0

    .line 67
    iget v0, p0, LJU;->d:F

    iget v1, p0, LJU;->b:F

    sub-float/2addr v0, v1

    .line 68
    iget v1, p0, LJU;->e:F

    iget v2, p0, LJU;->c:F

    sub-float/2addr v1, v2

    .line 69
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    iget v1, p0, LJU;->a:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 70
    iget-object v0, p0, LJU;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a(Z)V

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, LJU;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    .line 74
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/view/DragEvent;)V
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p1}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    .line 41
    invoke-virtual {p1}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    and-int/lit8 v0, v0, 0x3

    if-nez v0, :cond_0

    .line 42
    invoke-virtual {p1}, Landroid/view/DragEvent;->getX()F

    move-result v0

    iput v0, p0, LJU;->b:F

    .line 43
    invoke-virtual {p1}, Landroid/view/DragEvent;->getY()F

    move-result v0

    iput v0, p0, LJU;->c:F

    .line 44
    invoke-direct {p0}, LJU;->a()V

    .line 48
    :goto_0
    return-void

    .line 46
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LJU;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    goto :goto_0
.end method

.method public a(Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 52
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 53
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, LJU;->b:F

    .line 54
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, LJU;->c:F

    .line 55
    invoke-direct {p0}, LJU;->a()V

    .line 57
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)V
    .locals 1

    .prologue
    .line 60
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    iput-object v0, p0, LJU;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    .line 61
    iget v0, p0, LJU;->b:F

    iput v0, p0, LJU;->d:F

    .line 62
    iget v0, p0, LJU;->c:F

    iput v0, p0, LJU;->e:F

    .line 63
    return-void
.end method
