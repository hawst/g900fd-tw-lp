.class public LYS;
.super Ljava/lang/Object;
.source "PunchUiModelImpl.java"

# interfaces
.implements LYO;


# instance fields
.field private a:LYK;

.field private a:LYQ;

.field private a:LYR;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LYP;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, LYS;->a:Ljava/util/Set;

    .line 18
    sget-object v0, LYQ;->a:LYQ;

    iput-object v0, p0, LYS;->a:LYQ;

    .line 19
    sget-object v0, LYR;->a:LYR;

    iput-object v0, p0, LYS;->a:LYR;

    .line 20
    sget-object v0, LYK;->a:LYK;

    iput-object v0, p0, LYS;->a:LYK;

    .line 33
    iput-boolean v1, p0, LYS;->c:Z

    .line 38
    iput-boolean v1, p0, LYS;->d:Z

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, LYS;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LYP;

    .line 165
    invoke-interface {v0}, LYP;->a()V

    goto :goto_0

    .line 167
    :cond_0
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, LYS;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LYP;

    .line 184
    invoke-interface {v0}, LYP;->b()V

    goto :goto_0

    .line 186
    :cond_0
    return-void
.end method

.method private b(LYR;)V
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, LYS;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LYP;

    .line 175
    invoke-interface {v0, p1}, LYP;->a(LYR;)V

    goto :goto_0

    .line 177
    :cond_0
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, LYS;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LYP;

    .line 193
    invoke-interface {v0}, LYP;->c()V

    goto :goto_0

    .line 195
    :cond_0
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, LYS;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LYP;

    .line 202
    invoke-interface {v0}, LYP;->d()V

    goto :goto_0

    .line 204
    :cond_0
    return-void
.end method


# virtual methods
.method public a()LYK;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, LYS;->a:LYK;

    return-object v0
.end method

.method public a()LYQ;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, LYS;->a:LYQ;

    return-object v0
.end method

.method public a()LYR;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, LYS;->a:LYR;

    return-object v0
.end method

.method public a(LYK;)V
    .locals 1

    .prologue
    .line 122
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    iget-object v0, p0, LYS;->a:LYK;

    invoke-virtual {v0, p1}, LYK;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    iput-object p1, p0, LYS;->a:LYK;

    .line 125
    invoke-direct {p0}, LYS;->b()V

    .line 127
    :cond_0
    return-void
.end method

.method public a(LYP;)V
    .locals 1

    .prologue
    .line 42
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    iget-object v0, p0, LYS;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 44
    iget-object v0, p0, LYS;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 45
    return-void

    .line 43
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LYQ;)V
    .locals 1

    .prologue
    .line 56
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    iget-object v0, p0, LYS;->a:LYQ;

    if-eq v0, p1, :cond_0

    .line 58
    iput-object p1, p0, LYS;->a:LYQ;

    .line 59
    invoke-direct {p0}, LYS;->a()V

    .line 61
    :cond_0
    return-void
.end method

.method public a(LYR;)V
    .locals 1

    .prologue
    .line 70
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    iget-object v0, p0, LYS;->a:LYR;

    if-eq v0, p1, :cond_0

    .line 72
    iget-object v0, p0, LYS;->a:LYR;

    .line 73
    iput-object p1, p0, LYS;->a:LYR;

    .line 74
    invoke-direct {p0, v0}, LYS;->b(LYR;)V

    .line 76
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, LYS;->c:Z

    .line 89
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, LYS;->c:Z

    return v0
.end method

.method public b(LYP;)V
    .locals 1

    .prologue
    .line 49
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    iget-object v0, p0, LYS;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 51
    iget-object v0, p0, LYS;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 52
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 104
    iput-boolean p1, p0, LYS;->d:Z

    .line 105
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, LYS;->d:Z

    return v0
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p0, LYS;->a:Z

    if-eq v0, p1, :cond_0

    .line 137
    iput-boolean p1, p0, LYS;->a:Z

    .line 138
    invoke-direct {p0}, LYS;->c()V

    .line 140
    :cond_0
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 144
    iget-boolean v0, p0, LYS;->a:Z

    return v0
.end method

.method public d(Z)V
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, LYS;->b:Z

    if-eq v0, p1, :cond_0

    .line 150
    iput-boolean p1, p0, LYS;->b:Z

    .line 151
    invoke-direct {p0}, LYS;->d()V

    .line 153
    :cond_0
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, LYS;->b:Z

    return v0
.end method
