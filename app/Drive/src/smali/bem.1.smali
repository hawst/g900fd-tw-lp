.class public Lbem;
.super Ljava/io/IOException;
.source "HttpResponseException.java"


# instance fields
.field private final a:I

.field private final transient a:Lbed;

.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lbel;)V
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lben;

    invoke-direct {v0, p1}, Lben;-><init>(Lbel;)V

    invoke-direct {p0, v0}, Lbem;-><init>(Lben;)V

    .line 69
    return-void
.end method

.method protected constructor <init>(Lben;)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p1, Lben;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 78
    iget v0, p1, Lben;->a:I

    iput v0, p0, Lbem;->a:I

    .line 79
    iget-object v0, p1, Lben;->a:Ljava/lang/String;

    iput-object v0, p0, Lbem;->a:Ljava/lang/String;

    .line 80
    iget-object v0, p1, Lben;->a:Lbed;

    iput-object v0, p0, Lbem;->a:Lbed;

    .line 81
    iget-object v0, p1, Lben;->b:Ljava/lang/String;

    iput-object v0, p0, Lbem;->b:Ljava/lang/String;

    .line 82
    return-void
.end method

.method public static a(Lbel;)Ljava/lang/StringBuilder;
    .locals 3

    .prologue
    .line 294
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 295
    invoke-virtual {p0}, Lbel;->a()I

    move-result v1

    .line 296
    if-eqz v1, :cond_0

    .line 297
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 299
    :cond_0
    invoke-virtual {p0}, Lbel;->b()Ljava/lang/String;

    move-result-object v2

    .line 300
    if-eqz v2, :cond_2

    .line 301
    if-eqz v1, :cond_1

    .line 302
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 304
    :cond_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    :cond_2
    return-object v0
.end method
