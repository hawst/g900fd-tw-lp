.class public LUN;
.super Ljava/lang/Object;
.source "RitzUriUtils.java"


# static fields
.field private static final a:Ljava/lang/String;

.field public static final a:Ljava/util/regex/Pattern;

.field public static final b:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-string v0, "/spreadsheets/d/([^/]*)/.*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LUN;->a:Ljava/util/regex/Pattern;

    .line 30
    const-string v0, "/spreadsheet/ccc"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LUN;->b:Ljava/util/regex/Pattern;

    .line 32
    const-class v0, LUN;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LUN;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/net/Uri;LTF;)LbsU;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "LTF;",
            ")",
            "LbsU",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    invoke-static {p0}, LUN;->b(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    invoke-static {p0}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    .line 83
    :goto_0
    return-object v0

    .line 56
    :cond_0
    const-string v0, "/spreadsheet/convert/currenturl"

    invoke-static {p0, v0}, LFO;->a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 57
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 59
    sget-object v1, LUN;->a:Ljava/lang/String;

    const-string v2, "Checking Trix URI %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 62
    invoke-static {}, Lalg;->a()LbsW;

    move-result-object v1

    invoke-static {v1}, LbsY;->a(Ljava/util/concurrent/ExecutorService;)LbsW;

    move-result-object v1

    .line 63
    new-instance v2, LUO;

    invoke-direct {v2, p1, v0, p0}, LUO;-><init>(LTF;Landroid/net/Uri;Landroid/net/Uri;)V

    invoke-interface {v1, v2}, LbsW;->a(Ljava/util/concurrent/Callable;)LbsU;

    move-result-object v0

    .line 82
    invoke-interface {v1}, LbsW;->shutdown()V

    goto :goto_0
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    sget-object v0, LUN;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static a(LaGo;)Z
    .locals 1

    .prologue
    .line 39
    invoke-interface {p0}, LaGo;->a()Ljava/lang/String;

    move-result-object v0

    .line 40
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LUN;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 94
    sget-object v0, LUN;->a:Ljava/util/regex/Pattern;

    invoke-static {p0, v0}, LUN;->a(Landroid/net/Uri;Ljava/util/regex/Pattern;)Z

    move-result v0

    return v0
.end method

.method private static a(Landroid/net/Uri;Ljava/util/regex/Pattern;)Z
    .locals 1

    .prologue
    .line 98
    invoke-static {p0}, LFO;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 99
    if-nez v0, :cond_0

    .line 100
    const/4 v0, 0x0

    .line 103
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    goto :goto_0
.end method

.method private static b(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 87
    sget-object v0, LUN;->b:Ljava/util/regex/Pattern;

    invoke-static {p0, v0}, LUN;->a(Landroid/net/Uri;Ljava/util/regex/Pattern;)Z

    move-result v0

    return v0
.end method
