.class Lary;
.super Ljava/lang/Object;
.source "HoneycombActionBarHelper.java"

# interfaces
.implements Landroid/widget/SearchView$OnSuggestionListener;


# instance fields
.field final synthetic a:Landroid/view/MenuItem;

.field final synthetic a:Lart;


# direct methods
.method constructor <init>(Lart;Landroid/view/MenuItem;)V
    .locals 0

    .prologue
    .line 381
    iput-object p1, p0, Lary;->a:Lart;

    iput-object p2, p0, Lary;->a:Landroid/view/MenuItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuggestionClick(I)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 384
    iget-object v1, p0, Lary;->a:Lart;

    invoke-static {v1}, Lart;->a(Lart;)Landroid/widget/SearchView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SearchView;->getSuggestionsAdapter()Landroid/widget/CursorAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    .line 385
    if-nez v2, :cond_0

    .line 408
    :goto_0
    return v0

    .line 389
    :cond_0
    :try_start_0
    invoke-interface {v2, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 390
    const-string v1, "suggest_intent_action"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 391
    const/4 v1, 0x0

    .line 392
    if-ltz v3, :cond_1

    .line 393
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 397
    :cond_1
    if-eqz v1, :cond_2

    const-string v3, "android.intent.action.SEARCH"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_2

    .line 398
    const/4 v0, 0x0

    .line 408
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 401
    :cond_2
    :try_start_1
    const-string v1, "suggest_intent_query"

    invoke-static {v2, v1}, LaFr;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 402
    iget-object v3, p0, Lary;->a:Lart;

    iget-object v4, p0, Lary;->a:Landroid/view/MenuItem;

    invoke-static {v3, v1, v4}, Lart;->a(Lart;Ljava/lang/String;Landroid/view/MenuItem;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 408
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public onSuggestionSelect(I)Z
    .locals 1

    .prologue
    .line 415
    const/4 v0, 0x0

    return v0
.end method
