.class public LsT;
.super Landroid/os/AsyncTask;
.source "DocumentOpenerActivityDelegate.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "LFR;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LaGo;

.field final synthetic a:Landroid/os/Bundle;

.field final synthetic a:Lbtd;

.field final synthetic a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;Landroid/os/Bundle;LaGo;Lbtd;)V
    .locals 0

    .prologue
    .line 360
    iput-object p1, p0, LsT;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    iput-object p2, p0, LsT;->a:Landroid/os/Bundle;

    iput-object p3, p0, LsT;->a:LaGo;

    iput-object p4, p0, LsT;->a:Lbtd;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)LFR;
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 363
    iget-object v0, p0, LsT;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    const/4 v3, 0x0

    invoke-static {v0, v3}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;LDL;)LDL;

    .line 365
    iget-object v0, p0, LsT;->a:Landroid/os/Bundle;

    invoke-static {v0}, LFU;->a(Landroid/os/Bundle;)Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    move-result-object v3

    .line 366
    iget-object v0, p0, LsT;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LaKR;

    .line 367
    invoke-interface {v0}, LaKR;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LsT;->a:Landroid/os/Bundle;

    const-string v4, "openOfflineVersion"

    .line 368
    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    .line 370
    :goto_0
    iget-object v4, p0, LsT;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    iget-object v4, v4, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LFX;

    iget-object v5, p0, LsT;->a:LaGo;

    .line 371
    invoke-interface {v4, v5, v3, v0}, LFX;->a(LaGo;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Z)LFR;

    move-result-object v0

    .line 373
    if-nez v0, :cond_1

    .line 374
    const-string v3, "DocumentOpenerActivityDelegate"

    const-string v4, "Cannot open %s"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, LsT;->a:LaGo;

    aput-object v5, v2, v1

    invoke-static {v3, v4, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 375
    iget-object v1, p0, LsT;->a:Lbtd;

    new-instance v2, Ljava/lang/Exception;

    const-string v3, "Failed to open the document"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lbtd;->a(Ljava/lang/Throwable;)Z

    .line 378
    :cond_1
    return-object v0

    :cond_2
    move v0, v1

    .line 368
    goto :goto_0
.end method

.method protected a(LFR;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 383
    if-nez p1, :cond_0

    .line 384
    iget-object v0, p0, LsT;->a:Lbtd;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbtd;->a(Ljava/lang/Object;)Z

    .line 406
    :goto_0
    return-void

    .line 386
    :cond_0
    const-string v0, "DocumentOpenerActivityDelegate"

    const-string v1, "calling opener.openFile %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 387
    iget-object v0, p0, LsT;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    iget-object v1, p0, LsT;->a:LaGo;

    iget-object v2, p0, LsT;->a:Landroid/os/Bundle;

    .line 388
    invoke-interface {p1, v0, v1, v2}, LFR;->a(LFT;LaGo;Landroid/os/Bundle;)LbsU;

    move-result-object v0

    .line 389
    new-instance v1, LsU;

    invoke-direct {v1, p0}, LsU;-><init>(LsT;)V

    .line 404
    iget-object v2, p0, LsT;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    invoke-static {v2}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;)Ljava/util/concurrent/Executor;

    move-result-object v2

    invoke-static {v0, v1, v2}, LbsK;->a(LbsU;LbsJ;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 360
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, LsT;->a([Ljava/lang/Void;)LFR;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 360
    check-cast p1, LFR;

    invoke-virtual {p0, p1}, LsT;->a(LFR;)V

    return-void
.end method
