.class LaKV;
.super Ljava/util/concurrent/ThreadPoolExecutor;
.source "FixedSizeWorkerPool.java"


# instance fields
.field final synthetic a:LaKU;


# direct methods
.method public constructor <init>(LaKU;IJ)V
    .locals 9

    .prologue
    .line 38
    iput-object p1, p0, LaKV;->a:LaKU;

    .line 39
    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    move-object v1, p0

    move v2, p2

    move v3, p2

    move-wide v4, p3

    invoke-direct/range {v1 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    .line 42
    return-void
.end method

.method private a()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .prologue
    .line 46
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    .line 47
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LaKV;->allowCoreThreadTimeOut(Z)V

    .line 49
    :cond_0
    return-void
.end method

.method static synthetic a(LaKV;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, LaKV;->a()V

    return-void
.end method


# virtual methods
.method protected afterExecute(Ljava/lang/Runnable;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-super {p0, p1, p2}, Ljava/util/concurrent/ThreadPoolExecutor;->afterExecute(Ljava/lang/Runnable;Ljava/lang/Throwable;)V

    .line 63
    if-nez p2, :cond_0

    instance-of v0, p1, Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 65
    :try_start_0
    check-cast p1, Ljava/util/concurrent/Future;

    invoke-interface {p1}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    .line 75
    :cond_0
    :goto_0
    if-eqz p2, :cond_2

    .line 76
    invoke-static {}, Laml;->a()Lrx;

    move-result-object v0

    sget-object v2, Lrx;->c:Lrx;

    invoke-virtual {v0, v2}, Lrx;->a(Lrx;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FixedSizeWorkerPool: A worker threw an exception: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 78
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 66
    :catch_0
    move-exception v0

    move-object p2, v0

    .line 72
    goto :goto_0

    .line 68
    :catch_1
    move-exception v0

    .line 69
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object p2

    goto :goto_0

    .line 70
    :catch_2
    move-exception v0

    .line 71
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 80
    :cond_1
    const-string v0, "FixedSizeWorkerPool"

    const-string v2, "A worker has thrown an exception."

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, p2, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 85
    :cond_2
    iget-object v2, p0, LaKV;->a:LaKU;

    monitor-enter v2

    .line 86
    :try_start_1
    iget-object v0, p0, LaKV;->a:LaKU;

    invoke-static {v0}, LaKU;->b(LaKU;)I

    .line 88
    iget-object v0, p0, LaKV;->a:LaKU;

    invoke-static {v0}, LaKU;->c(LaKU;)I

    move-result v0

    if-lez v0, :cond_4

    .line 89
    iget-object v0, p0, LaKV;->a:LaKU;

    invoke-static {v0}, LaKU;->d(LaKU;)I

    .line 90
    iget-object v0, p0, LaKV;->a:LaKU;

    invoke-static {v0}, LaKU;->a(LaKU;)LaKW;

    move-result-object v0

    invoke-interface {v0}, LaKW;->a()Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0}, LaKV;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move v0, v1

    .line 94
    :goto_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 96
    if-eqz v0, :cond_3

    .line 97
    iget-object v0, p0, LaKV;->a:LaKU;

    invoke-static {v0}, LaKU;->a(LaKU;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 99
    :cond_3
    return-void

    .line 91
    :cond_4
    :try_start_2
    iget-object v0, p0, LaKV;->a:LaKU;

    invoke-static {v0}, LaKU;->e(LaKU;)I

    move-result v0

    if-nez v0, :cond_5

    .line 92
    const/4 v0, 0x1

    goto :goto_1

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method protected beforeExecute(Ljava/lang/Thread;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 53
    invoke-super {p0, p1, p2}, Ljava/util/concurrent/ThreadPoolExecutor;->beforeExecute(Ljava/lang/Thread;Ljava/lang/Runnable;)V

    .line 54
    iget-object v1, p0, LaKV;->a:LaKU;

    monitor-enter v1

    .line 55
    :try_start_0
    iget-object v0, p0, LaKV;->a:LaKU;

    invoke-static {v0}, LaKU;->a(LaKU;)I

    .line 56
    monitor-exit v1

    .line 57
    return-void

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
