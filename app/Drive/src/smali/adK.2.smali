.class public final LadK;
.super Ljava/lang/Object;
.source "FileSpec.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/docs/sync/filemanager/FileSpec;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/sync/filemanager/FileSpec;
    .locals 5

    .prologue
    .line 20
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 21
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 22
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, LacY;

    .line 23
    new-instance v4, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;

    invoke-direct {v4, v0, v2, v3, v1}, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;JLacY;)V

    return-object v4
.end method

.method public a(I)[Lcom/google/android/apps/docs/sync/filemanager/FileSpec;
    .locals 1

    .prologue
    .line 28
    new-array v0, p1, [Lcom/google/android/apps/docs/sync/filemanager/FileSpec;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0, p1}, LadK;->a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/sync/filemanager/FileSpec;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0, p1}, LadK;->a(I)[Lcom/google/android/apps/docs/sync/filemanager/FileSpec;

    move-result-object v0

    return-object v0
.end method
