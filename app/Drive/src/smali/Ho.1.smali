.class public LHo;
.super LGX;
.source "WebDriveAppOpenerOption.java"


# direct methods
.method private constructor <init>(LHh;Landroid/content/Context;LGZ;LaHJ;)V
    .locals 7

    .prologue
    .line 34
    invoke-virtual {p4}, LaHJ;->a()Ljava/lang/String;

    move-result-object v4

    const-string v5, "WebDriveApp"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, LGX;-><init>(LHh;Landroid/content/Context;LGZ;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 36
    return-void
.end method

.method public static a(LaGo;Landroid/content/Context;LaHL;LGZ;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGo;",
            "Landroid/content/Context;",
            "LaHL;",
            "LGZ;",
            ")",
            "Ljava/util/List",
            "<",
            "LHo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    invoke-static {p0}, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;->a(LaGo;)Ljava/lang/String;

    move-result-object v0

    .line 52
    invoke-interface {p0}, LaGo;->a()LaFM;

    move-result-object v1

    invoke-virtual {v1}, LaFM;->a()LaFO;

    move-result-object v1

    .line 53
    invoke-interface {p2, v1}, LaHL;->a(LaFO;)LaHK;

    move-result-object v1

    .line 54
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    .line 55
    invoke-interface {p0}, LaGo;->g()Ljava/lang/String;

    move-result-object v3

    .line 56
    if-eqz v3, :cond_0

    .line 57
    invoke-interface {v1, v3}, LaHK;->b(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 59
    :cond_0
    if-eqz v0, :cond_1

    .line 60
    invoke-interface {v1, v0}, LaHK;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 63
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxb;->ic_drive_app_web_launcher:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 64
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 65
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaHJ;

    .line 66
    sget v4, Lxi;->open_with_web_app_item_subtitle:I

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 67
    new-instance v5, LHh;

    invoke-virtual {v0}, LaHJ;->b()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v1, v6, v4}, LHh;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 68
    new-instance v4, LHo;

    invoke-direct {v4, v5, p1, p3, v0}, LHo;-><init>(LHh;Landroid/content/Context;LGZ;LaHJ;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 70
    :cond_2
    return-object v3
.end method


# virtual methods
.method protected a(LaGo;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 41
    invoke-virtual {v0, p2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 46
    return-object v0
.end method
