.class public final LaNf;
.super Ljava/lang/Object;


# instance fields
.field private final a:LaMN;

.field private a:LaMU;

.field private final a:LaNN;

.field private a:Landroid/view/ViewGroup;

.field private a:LayK;

.field private a:LayM;

.field private a:Lays;

.field private a:Layz;

.field private a:Ljava/lang/String;

.field private a:[Layw;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {}, LaMN;->a()LaMN;

    move-result-object v2

    invoke-direct {p0, p1, v0, v1, v2}, LaNf;-><init>(Landroid/view/ViewGroup;Landroid/util/AttributeSet;ZLaMN;)V

    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup;Landroid/util/AttributeSet;Z)V
    .locals 1

    invoke-static {}, LaMN;->a()LaMN;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, LaNf;-><init>(Landroid/view/ViewGroup;Landroid/util/AttributeSet;ZLaMN;)V

    return-void
.end method

.method constructor <init>(Landroid/view/ViewGroup;Landroid/util/AttributeSet;ZLaMN;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, LaNN;

    invoke-direct {v0}, LaNN;-><init>()V

    iput-object v0, p0, LaNf;->a:LaNN;

    iput-object p1, p0, LaNf;->a:Landroid/view/ViewGroup;

    iput-object p4, p0, LaNf;->a:LaMN;

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    :try_start_0
    new-instance v0, LaMQ;

    invoke-direct {v0, v1, p2}, LaMQ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {v0, p3}, LaMQ;->a(Z)[Layw;

    move-result-object v2

    iput-object v2, p0, LaNf;->a:[Layw;

    invoke-virtual {v0}, LaMQ;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaNf;->a:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/internal/aq;

    iget-object v2, p0, LaNf;->a:[Layw;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/internal/aq;-><init>(Landroid/content/Context;Layw;)V

    const-string v1, "Ads by Google"

    invoke-static {p1, v0, v1}, LaQb;->a(Landroid/view/ViewGroup;Lcom/google/android/gms/internal/aq;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v2, Lcom/google/android/gms/internal/aq;

    sget-object v3, Layw;->a:Layw;

    invoke-direct {v2, v1, v3}, Lcom/google/android/gms/internal/aq;-><init>(Landroid/content/Context;Layw;)V

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v2, v1, v0}, LaQb;->a(Landroid/view/ViewGroup;Lcom/google/android/gms/internal/aq;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private d()V
    .locals 2

    :try_start_0
    iget-object v0, p0, LaNf;->a:LaMU;

    invoke-interface {v0}, LaMU;->a()LaLm;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, LaNf;->a:Landroid/view/ViewGroup;

    invoke-static {v0}, LaLp;->a(LaLm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Failed to get an ad frame."

    invoke-static {v1, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private e()V
    .locals 4

    iget-object v0, p0, LaNf;->a:[Layw;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaNf;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, LaNf;->a:LaMU;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The ad size and ad unit ID must be set before loadAd is called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, LaNf;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/internal/aq;

    iget-object v2, p0, LaNf;->a:[Layw;

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/internal/aq;-><init>(Landroid/content/Context;[Layw;)V

    iget-object v2, p0, LaNf;->a:Ljava/lang/String;

    iget-object v3, p0, LaNf;->a:LaNN;

    invoke-static {v0, v1, v2, v3}, LaML;->a(Landroid/content/Context;Lcom/google/android/gms/internal/aq;Ljava/lang/String;LaNN;)LaMU;

    move-result-object v0

    iput-object v0, p0, LaNf;->a:LaMU;

    iget-object v0, p0, LaNf;->a:Lays;

    if-eqz v0, :cond_2

    iget-object v0, p0, LaNf;->a:LaMU;

    new-instance v1, LaMK;

    iget-object v2, p0, LaNf;->a:Lays;

    invoke-direct {v1, v2}, LaMK;-><init>(Lays;)V

    invoke-interface {v0, v1}, LaMU;->a(LaMR;)V

    :cond_2
    iget-object v0, p0, LaNf;->a:Layz;

    if-eqz v0, :cond_3

    iget-object v0, p0, LaNf;->a:LaMU;

    new-instance v1, LaMP;

    iget-object v2, p0, LaNf;->a:Layz;

    invoke-direct {v1, v2}, LaMP;-><init>(Layz;)V

    invoke-interface {v0, v1}, LaMU;->a(LaNa;)V

    :cond_3
    iget-object v0, p0, LaNf;->a:LayK;

    if-eqz v0, :cond_4

    iget-object v0, p0, LaNf;->a:LaMU;

    new-instance v1, LaOS;

    iget-object v2, p0, LaNf;->a:LayK;

    invoke-direct {v1, v2}, LaOS;-><init>(LayK;)V

    invoke-interface {v0, v1}, LaMU;->a(LaOH;)V

    :cond_4
    iget-object v0, p0, LaNf;->a:LayM;

    if-eqz v0, :cond_5

    iget-object v0, p0, LaNf;->a:LaMU;

    new-instance v1, LaOV;

    iget-object v2, p0, LaNf;->a:LayM;

    invoke-direct {v1, v2}, LaOV;-><init>(LayM;)V

    iget-object v2, p0, LaNf;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LaMU;->a(LaOP;Ljava/lang/String;)V

    :cond_5
    invoke-direct {p0}, LaNf;->d()V

    return-void
.end method


# virtual methods
.method public a()Layw;
    .locals 2

    :try_start_0
    iget-object v0, p0, LaNf;->a:LaMU;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaNf;->a:LaMU;

    invoke-interface {v0}, LaMU;->a()Lcom/google/android/gms/internal/aq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/aq;->a()Layw;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "Failed to get the current AdSize."

    invoke-static {v1, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    iget-object v0, p0, LaNf;->a:[Layw;

    if-eqz v0, :cond_1

    iget-object v0, p0, LaNf;->a:[Layw;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 2

    :try_start_0
    iget-object v0, p0, LaNf;->a:LaMU;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaNf;->a:LaMU;

    invoke-interface {v0}, LaMU;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Failed to destroy AdView."

    invoke-static {v1, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public a(LaNd;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, LaNf;->a:LaMU;

    if-nez v0, :cond_0

    invoke-direct {p0}, LaNf;->e()V

    :cond_0
    iget-object v0, p0, LaNf;->a:LaMU;

    iget-object v1, p0, LaNf;->a:LaMN;

    iget-object v2, p0, LaNf;->a:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, LaMN;->a(Landroid/content/Context;LaNd;)Lcom/google/android/gms/internal/an;

    move-result-object v1

    invoke-interface {v0, v1}, LaMU;->a(Lcom/google/android/gms/internal/an;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LaNf;->a:LaNN;

    invoke-virtual {p1}, LaNd;->a()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, LaNN;->a(Ljava/util/Map;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Failed to load ad."

    invoke-static {v1, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public a(LayK;)V
    .locals 2

    iget-object v0, p0, LaNf;->a:LayM;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Play store purchase parameter has already been set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_0
    iput-object p1, p0, LaNf;->a:LayK;

    iget-object v0, p0, LaNf;->a:LaMU;

    if-eqz v0, :cond_1

    iget-object v1, p0, LaNf;->a:LaMU;

    if-eqz p1, :cond_2

    new-instance v0, LaOS;

    invoke-direct {v0, p1}, LaOS;-><init>(LayK;)V

    :goto_0
    invoke-interface {v1, v0}, LaMU;->a(LaOH;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Failed to set the InAppPurchaseListener."

    invoke-static {v1, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public a(LayM;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, LaNf;->a:LayK;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "InAppPurchaseListener has already been set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_0
    iput-object p1, p0, LaNf;->a:LayM;

    iput-object p2, p0, LaNf;->b:Ljava/lang/String;

    iget-object v0, p0, LaNf;->a:LaMU;

    if-eqz v0, :cond_1

    iget-object v1, p0, LaNf;->a:LaMU;

    if-eqz p1, :cond_2

    new-instance v0, LaOV;

    invoke-direct {v0, p1}, LaOV;-><init>(LayM;)V

    :goto_0
    invoke-interface {v1, v0, p2}, LaMU;->a(LaOP;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Failed to set the play store purchase parameter."

    invoke-static {v1, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public a(Lays;)V
    .locals 2

    :try_start_0
    iput-object p1, p0, LaNf;->a:Lays;

    iget-object v0, p0, LaNf;->a:LaMU;

    if-eqz v0, :cond_0

    iget-object v1, p0, LaNf;->a:LaMU;

    if-eqz p1, :cond_1

    new-instance v0, LaMK;

    invoke-direct {v0, p1}, LaMK;-><init>(Lays;)V

    :goto_0
    invoke-interface {v1, v0}, LaMU;->a(LaMR;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Failed to set the AdListener."

    invoke-static {v1, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public a(Layz;)V
    .locals 2

    :try_start_0
    iput-object p1, p0, LaNf;->a:Layz;

    iget-object v0, p0, LaNf;->a:LaMU;

    if-eqz v0, :cond_0

    iget-object v1, p0, LaNf;->a:LaMU;

    if-eqz p1, :cond_1

    new-instance v0, LaMP;

    invoke-direct {v0, p1}, LaMP;-><init>(Layz;)V

    :goto_0
    invoke-interface {v1, v0}, LaMU;->a(LaNa;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Failed to set the AppEventListener."

    invoke-static {v1, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, LaNf;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The ad unit ID can only be set once on AdView."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, LaNf;->a:Ljava/lang/String;

    return-void
.end method

.method public varargs a([Layw;)V
    .locals 2

    iget-object v0, p0, LaNf;->a:[Layw;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The ad size can only be set once on AdView."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0, p1}, LaNf;->b([Layw;)V

    return-void
.end method

.method public b()V
    .locals 2

    :try_start_0
    iget-object v0, p0, LaNf;->a:LaMU;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaNf;->a:LaMU;

    invoke-interface {v0}, LaMU;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Failed to call pause."

    invoke-static {v1, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public varargs b([Layw;)V
    .locals 4

    iput-object p1, p0, LaNf;->a:[Layw;

    :try_start_0
    iget-object v0, p0, LaNf;->a:LaMU;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaNf;->a:LaMU;

    new-instance v1, Lcom/google/android/gms/internal/aq;

    iget-object v2, p0, LaNf;->a:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LaNf;->a:[Layw;

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/internal/aq;-><init>(Landroid/content/Context;[Layw;)V

    invoke-interface {v0, v1}, LaMU;->a(Lcom/google/android/gms/internal/aq;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, LaNf;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "Failed to set the ad size."

    invoke-static {v1, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public c()V
    .locals 2

    :try_start_0
    iget-object v0, p0, LaNf;->a:LaMU;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaNf;->a:LaMU;

    invoke-interface {v0}, LaMU;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Failed to call resume."

    invoke-static {v1, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
