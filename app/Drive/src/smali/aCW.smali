.class public final LaCW;
.super Ljava/lang/Object;


# instance fields
.field private a:I

.field private a:LH;

.field private a:LaCY;

.field private final a:Landroid/content/Context;

.field private a:Landroid/os/Looper;

.field private a:Landroid/view/View;

.field private a:Ljava/lang/String;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LaCM",
            "<*>;",
            "LaCN;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/String;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LaCX;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LaCY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LaCW;->a:Ljava/util/Set;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LaCW;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LaCW;->b:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LaCW;->c:Ljava/util/Set;

    iput-object p1, p0, LaCW;->a:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, LaCW;->a:Landroid/os/Looper;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaCW;->b:Ljava/lang/String;

    return-void
.end method

.method private a()Lcom/google/android/gms/common/api/d;
    .locals 4

    iget-object v0, p0, LaCW;->a:LH;

    invoke-virtual {v0}, LH;->a()LM;

    move-result-object v2

    invoke-virtual {v2}, LM;->a()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v2}, LM;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    instance-of v1, v0, Lcom/google/android/gms/common/api/d;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Lcom/google/android/gms/common/api/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/d;->l()Z

    move-result v1

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/android/gms/common/api/d;

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/android/gms/common/api/d;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/d;-><init>()V

    invoke-virtual {v2}, LM;->a()Lac;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lac;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lac;

    move-result-object v1

    invoke-virtual {v1}, Lac;->a()I

    goto :goto_0
.end method


# virtual methods
.method public a()LaCV;
    .locals 8

    iget-object v0, p0, LaCW;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    const-string v1, "must call addApi() to add at least one API"

    invoke-static {v0, v1}, LaSc;->b(ZLjava/lang/Object;)V

    const/4 v5, 0x0

    iget-object v0, p0, LaCW;->a:LH;

    if-eqz v0, :cond_0

    invoke-direct {p0}, LaCW;->a()Lcom/google/android/gms/common/api/d;

    move-result-object v5

    :cond_0
    new-instance v0, LaDl;

    iget-object v1, p0, LaCW;->a:Landroid/content/Context;

    iget-object v2, p0, LaCW;->a:Landroid/os/Looper;

    invoke-virtual {p0}, LaCW;->a()Lcom/google/android/gms/internal/gw;

    move-result-object v3

    iget-object v4, p0, LaCW;->a:Ljava/util/Map;

    iget-object v6, p0, LaCW;->b:Ljava/util/Set;

    iget-object v7, p0, LaCW;->c:Ljava/util/Set;

    invoke-direct/range {v0 .. v7}, LaDl;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/internal/gw;Ljava/util/Map;Landroid/support/v4/app/Fragment;Ljava/util/Set;Ljava/util/Set;)V

    if-eqz v5, :cond_1

    iget-object v1, p0, LaCW;->a:LaCY;

    invoke-virtual {v5, v0, v1}, Lcom/google/android/gms/common/api/d;->a(LaCV;LaCY;)V

    :cond_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LaCM;)LaCW;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaCM",
            "<+",
            "LaCQ;",
            ">;)",
            "LaCW;"
        }
    .end annotation

    iget-object v0, p0, LaCW;->a:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, LaCM;->a()Ljava/util/List;

    move-result-object v2

    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v4, p0, LaCW;->a:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaDe;

    invoke-virtual {v0}, LaDe;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public a(LaCM;LaCO;)LaCW;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<O::",
            "LaCO;",
            ">(",
            "LaCM",
            "<TO;>;TO;)",
            "LaCW;"
        }
    .end annotation

    const-string v0, "Null options are not permitted for this Api"

    invoke-static {p2, v0}, LaSc;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, LaCW;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, LaCM;->a()Ljava/util/List;

    move-result-object v2

    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v4, p0, LaCW;->a:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaDe;

    invoke-virtual {v0}, LaDe;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public a(LaCX;)LaCW;
    .locals 1

    iget-object v0, p0, LaCW;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public a(LaCY;)LaCW;
    .locals 1

    iget-object v0, p0, LaCW;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public a(Ljava/lang/String;)LaCW;
    .locals 0

    iput-object p1, p0, LaCW;->a:Ljava/lang/String;

    return-object p0
.end method

.method public a()Lcom/google/android/gms/internal/gw;
    .locals 6

    new-instance v0, Lcom/google/android/gms/internal/gw;

    iget-object v1, p0, LaCW;->a:Ljava/lang/String;

    iget-object v2, p0, LaCW;->a:Ljava/util/Set;

    iget v3, p0, LaCW;->a:I

    iget-object v4, p0, LaCW;->a:Landroid/view/View;

    iget-object v5, p0, LaCW;->b:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/gw;-><init>(Ljava/lang/String;Ljava/util/Collection;ILandroid/view/View;Ljava/lang/String;)V

    return-object v0
.end method
