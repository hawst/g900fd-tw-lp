.class public abstract Lbih;
.super Ljava/lang/Object;
.source "CharMatcher.java"

# interfaces
.implements LbiU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbiU",
        "<",
        "Ljava/lang/Character;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lbih;

.field public static final b:Lbih;

.field private static final b:Ljava/lang/String;

.field public static final c:Lbih;

.field public static final d:Lbih;

.field public static final e:Lbih;

.field public static final f:Lbih;

.field public static final g:Lbih;

.field public static final h:Lbih;

.field public static final i:Lbih;

.field public static final j:Lbih;

.field public static final k:Lbih;

.field public static final l:Lbih;

.field public static final m:Lbih;

.field public static final n:Lbih;


# instance fields
.field final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0x7f

    const/4 v1, 0x0

    .line 67
    new-instance v0, Lbii;

    invoke-direct {v0}, Lbii;-><init>()V

    sput-object v0, Lbih;->a:Lbih;

    .line 101
    const-string v0, "CharMatcher.ASCII"

    invoke-static {v1, v5, v0}, Lbih;->a(CCLjava/lang/String;)Lbih;

    move-result-object v0

    sput-object v0, Lbih;->b:Lbih;

    .line 139
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "0\u0660\u06f0\u07c0\u0966\u09e6\u0a66\u0ae6\u0b66\u0be6\u0c66\u0ce6\u0d66\u0e50\u0ed0\u0f20\u1040\u1090\u17e0\u1810\u1946\u19d0\u1b50\u1bb0\u1c40\u1c50\ua620\ua8d0\ua900\uaa50\uff10"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    move v0, v1

    .line 140
    :goto_0
    const-string v3, "0\u0660\u06f0\u07c0\u0966\u09e6\u0a66\u0ae6\u0b66\u0be6\u0c66\u0ce6\u0d66\u0e50\u0ed0\u0f20\u1040\u1090\u17e0\u1810\u1946\u19d0\u1b50\u1bb0\u1c40\u1c50\ua620\ua8d0\ua900\uaa50\uff10"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 141
    const-string v3, "0\u0660\u06f0\u07c0\u0966\u09e6\u0a66\u0ae6\u0b66\u0be6\u0c66\u0ce6\u0d66\u0e50\u0ed0\u0f20\u1040\u1090\u17e0\u1810\u1946\u19d0\u1b50\u1bb0\u1c40\u1c50\ua620\ua8d0\ua900\uaa50\uff10"

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    add-int/lit8 v3, v3, 0x9

    int-to-char v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 140
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 143
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbih;->b:Ljava/lang/String;

    .line 150
    new-instance v0, LbiA;

    const-string v2, "CharMatcher.DIGIT"

    const-string v3, "0\u0660\u06f0\u07c0\u0966\u09e6\u0a66\u0ae6\u0b66\u0be6\u0c66\u0ce6\u0d66\u0e50\u0ed0\u0f20\u1040\u1090\u17e0\u1810\u1946\u19d0\u1b50\u1bb0\u1c40\u1c50\ua620\ua8d0\ua900\uaa50\uff10"

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    sget-object v4, Lbih;->b:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    invoke-direct {v0, v2, v3, v4}, LbiA;-><init>(Ljava/lang/String;[C[C)V

    sput-object v0, Lbih;->c:Lbih;

    .line 157
    new-instance v0, Lbio;

    const-string v2, "CharMatcher.JAVA_DIGIT"

    invoke-direct {v0, v2}, Lbio;-><init>(Ljava/lang/String;)V

    sput-object v0, Lbih;->d:Lbih;

    .line 169
    new-instance v0, Lbip;

    const-string v2, "CharMatcher.JAVA_LETTER"

    invoke-direct {v0, v2}, Lbip;-><init>(Ljava/lang/String;)V

    sput-object v0, Lbih;->e:Lbih;

    .line 180
    new-instance v0, Lbiq;

    const-string v2, "CharMatcher.JAVA_LETTER_OR_DIGIT"

    invoke-direct {v0, v2}, Lbiq;-><init>(Ljava/lang/String;)V

    sput-object v0, Lbih;->f:Lbih;

    .line 192
    new-instance v0, Lbir;

    const-string v2, "CharMatcher.JAVA_UPPER_CASE"

    invoke-direct {v0, v2}, Lbir;-><init>(Ljava/lang/String;)V

    sput-object v0, Lbih;->g:Lbih;

    .line 203
    new-instance v0, Lbis;

    const-string v2, "CharMatcher.JAVA_LOWER_CASE"

    invoke-direct {v0, v2}, Lbis;-><init>(Ljava/lang/String;)V

    sput-object v0, Lbih;->h:Lbih;

    .line 214
    const/16 v0, 0x1f

    invoke-static {v1, v0}, Lbih;->a(CC)Lbih;

    move-result-object v0

    const/16 v1, 0x9f

    invoke-static {v5, v1}, Lbih;->a(CC)Lbih;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbih;->a(Lbih;)Lbih;

    move-result-object v0

    const-string v1, "CharMatcher.JAVA_ISO_CONTROL"

    invoke-virtual {v0, v1}, Lbih;->a(Ljava/lang/String;)Lbih;

    move-result-object v0

    sput-object v0, Lbih;->i:Lbih;

    .line 222
    new-instance v0, LbiA;

    const-string v1, "CharMatcher.INVISIBLE"

    const-string v2, "\u0000\u007f\u00ad\u0600\u06dd\u070f\u1680\u180e\u2000\u2028\u205f\u206a\u3000\ud800\ufeff\ufff9\ufffa"

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    const-string v3, " \u00a0\u00ad\u0604\u06dd\u070f\u1680\u180e\u200f\u202f\u2064\u206f\u3000\uf8ff\ufeff\ufff9\ufffb"

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LbiA;-><init>(Ljava/lang/String;[C[C)V

    sput-object v0, Lbih;->j:Lbih;

    .line 247
    new-instance v0, LbiA;

    const-string v1, "CharMatcher.SINGLE_WIDTH"

    const-string v2, "\u0000\u05be\u05d0\u05f3\u0600\u0750\u0e00\u1e00\u2100\ufb50\ufe70\uff61"

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    const-string v3, "\u04f9\u05be\u05ea\u05f4\u06ff\u077f\u0e7f\u20af\u213a\ufdff\ufeff\uffdc"

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LbiA;-><init>(Ljava/lang/String;[C[C)V

    sput-object v0, Lbih;->k:Lbih;

    .line 252
    new-instance v0, Lbit;

    const-string v1, "CharMatcher.ANY"

    invoke-direct {v0, v1}, Lbit;-><init>(Ljava/lang/String;)V

    sput-object v0, Lbih;->l:Lbih;

    .line 342
    new-instance v0, Lbiu;

    const-string v1, "CharMatcher.NONE"

    invoke-direct {v0, v1}, Lbiu;-><init>(Ljava/lang/String;)V

    sput-object v0, Lbih;->m:Lbih;

    .line 1416
    new-instance v0, Lbin;

    const-string v1, "CharMatcher.WHITESPACE"

    invoke-direct {v0, v1}, Lbin;-><init>(Ljava/lang/String;)V

    sput-object v0, Lbih;->n:Lbih;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 638
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 639
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbih;->a:Ljava/lang/String;

    .line 640
    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 630
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 631
    iput-object p1, p0, Lbih;->a:Ljava/lang/String;

    .line 632
    return-void
.end method

.method public static a(C)Lbih;
    .locals 2

    .prologue
    .line 433
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CharMatcher.is(\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lbih;->a(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 434
    new-instance v1, Lbiv;

    invoke-direct {v1, v0, p0}, Lbiv;-><init>(Ljava/lang/String;C)V

    return-object v1
.end method

.method public static a(CC)Lbih;
    .locals 2

    .prologue
    .line 578
    if-lt p1, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 579
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CharMatcher.inRange(\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lbih;->a(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lbih;->a(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 581
    invoke-static {p0, p1, v0}, Lbih;->a(CCLjava/lang/String;)Lbih;

    move-result-object v0

    return-object v0

    .line 578
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(CCLjava/lang/String;)Lbih;
    .locals 1

    .prologue
    .line 585
    new-instance v0, Lbim;

    invoke-direct {v0, p2, p0, p1}, Lbim;-><init>(Ljava/lang/String;CC)V

    return-object v0
.end method

.method public static a(Ljava/lang/CharSequence;)Lbih;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 510
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 521
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 522
    invoke-static {v1}, Ljava/util/Arrays;->sort([C)V

    .line 523
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CharMatcher.anyOf(\""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 524
    array-length v3, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-char v4, v1, v0

    .line 525
    invoke-static {v4}, Lbih;->a(C)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 524
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 512
    :pswitch_0
    sget-object v0, Lbih;->m:Lbih;

    .line 528
    :goto_1
    return-object v0

    .line 514
    :pswitch_1
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lbih;->a(C)Lbih;

    move-result-object v0

    goto :goto_1

    .line 516
    :pswitch_2
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/4 v1, 0x1

    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-static {v0, v1}, Lbih;->b(CC)Lbih;

    move-result-object v0

    goto :goto_1

    .line 527
    :cond_0
    const-string v0, "\")"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 528
    new-instance v0, Lbik;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lbik;-><init>(Ljava/lang/String;[C)V

    goto :goto_1

    .line 510
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(C)Ljava/lang/String;
    .locals 5

    .prologue
    .line 229
    const-string v1, "0123456789ABCDEF"

    .line 230
    const/4 v0, 0x6

    new-array v2, v0, [C

    fill-array-data v2, :array_0

    .line 231
    const/4 v0, 0x0

    :goto_0
    const/4 v3, 0x4

    if-ge v0, v3, :cond_0

    .line 232
    rsub-int/lit8 v3, v0, 0x5

    and-int/lit8 v4, p0, 0xf

    invoke-virtual {v1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    aput-char v4, v2, v3

    .line 233
    shr-int/lit8 v3, p0, 0x4

    int-to-char p0, v3

    .line 231
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 235
    :cond_0
    invoke-static {v2}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 230
    nop

    :array_0
    .array-data 2
        0x5cs
        0x75s
        0x0s
        0x0s
        0x0s
        0x0s
    .end array-data
.end method

.method private a(Ljava/lang/CharSequence;IICLjava/lang/StringBuilder;Z)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1340
    move v0, p6

    :goto_0
    if-ge p2, p3, :cond_2

    .line 1341
    invoke-interface {p1, p2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    .line 1342
    invoke-virtual {p0, v1}, Lbih;->a(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1343
    if-nez v0, :cond_0

    .line 1344
    invoke-virtual {p5, p4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1345
    const/4 v0, 0x1

    .line 1340
    :cond_0
    :goto_1
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 1348
    :cond_1
    invoke-virtual {p5, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1349
    const/4 v0, 0x0

    goto :goto_1

    .line 1352
    :cond_2
    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(C)Lbih;
    .locals 2

    .prologue
    .line 474
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CharMatcher.isNot("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 475
    new-instance v1, Lbij;

    invoke-direct {v1, v0, p0}, Lbij;-><init>(Ljava/lang/String;C)V

    return-object v1
.end method

.method private static b(CC)Lbih;
    .locals 2

    .prologue
    .line 545
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CharMatcher.anyOf(\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lbih;->a(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lbih;->a(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 547
    new-instance v1, Lbil;

    invoke-direct {v1, v0, p0, p1}, Lbil;-><init>(Ljava/lang/String;CC)V

    return-object v1
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;)I
    .locals 3

    .prologue
    .line 1009
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 1010
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 1011
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2}, Lbih;->a(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1015
    :goto_1
    return v0

    .line 1010
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1015
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public a(Ljava/lang/CharSequence;I)I
    .locals 3

    .prologue
    .line 1034
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 1035
    invoke-static {p2, v1}, LbiT;->b(II)I

    move v0, p2

    .line 1036
    :goto_0
    if-ge v0, v1, :cond_1

    .line 1037
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2}, Lbih;->a(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1041
    :goto_1
    return v0

    .line 1036
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1041
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public a()Lbih;
    .locals 1

    .prologue
    .line 653
    new-instance v0, Lbiy;

    invoke-direct {v0, p0}, Lbiy;-><init>(Lbih;)V

    return-object v0
.end method

.method public a(Lbih;)Lbih;
    .locals 2

    .prologue
    .line 755
    new-instance v1, Lbiz;

    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbih;

    invoke-direct {v1, p0, v0}, Lbiz;-><init>(Lbih;Lbih;)V

    return-object v1
.end method

.method a(Ljava/lang/String;)Lbih;
    .locals 1

    .prologue
    .line 810
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1223
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    .line 1227
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1228
    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-virtual {p0, v0}, Lbih;->a(C)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1232
    :cond_0
    add-int/lit8 v0, v2, -0x1

    :goto_1
    if-le v0, v1, :cond_1

    .line 1233
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2}, Lbih;->a(C)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1238
    :cond_1
    add-int/lit8 v0, v0, 0x1

    invoke-interface {p1, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1227
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1232
    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method public a(Ljava/lang/CharSequence;C)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1145
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1146
    invoke-virtual {p0, v0}, Lbih;->a(Ljava/lang/CharSequence;)I

    move-result v1

    .line 1147
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 1157
    :goto_0
    return-object v0

    .line 1150
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    .line 1151
    aput-char p2, v2, v1

    .line 1152
    add-int/lit8 v0, v1, 0x1

    :goto_1
    array-length v1, v2

    if-ge v0, v1, :cond_2

    .line 1153
    aget-char v1, v2, v0

    invoke-virtual {p0, v1}, Lbih;->a(C)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1154
    aput-char p2, v2, v0

    .line 1152
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1157
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([C)V

    goto :goto_0
.end method

.method public abstract a(C)Z
.end method

.method public a(Ljava/lang/Character;)Z
    .locals 1

    .prologue
    .line 1362
    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-virtual {p0, v0}, Lbih;->a(C)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 55
    check-cast p1, Ljava/lang/Character;

    invoke-virtual {p0, p1}, Lbih;->a(Ljava/lang/Character;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/CharSequence;)I
    .locals 2

    .prologue
    .line 1055
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 1056
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-virtual {p0, v1}, Lbih;->a(C)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1060
    :goto_1
    return v0

    .line 1055
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1060
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public b(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1270
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 1271
    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 1272
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-virtual {p0, v1}, Lbih;->a(C)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1273
    const/4 v1, 0x0

    add-int/lit8 v0, v0, 0x1

    invoke-interface {p1, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1276
    :goto_1
    return-object v0

    .line 1271
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1276
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method public b(Ljava/lang/CharSequence;C)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1300
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    move v0, v1

    .line 1301
    :goto_0
    if-ge v0, v3, :cond_3

    .line 1302
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    .line 1303
    invoke-virtual {p0, v2}, Lbih;->a(C)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1304
    if-ne v2, p2, :cond_2

    add-int/lit8 v2, v3, -0x1

    if-eq v0, v2, :cond_0

    add-int/lit8 v2, v0, 0x1

    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2}, Lbih;->a(C)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1306
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 1301
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1308
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-interface {p1, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1310
    add-int/lit8 v2, v0, 0x1

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    invoke-direct/range {v0 .. v6}, Lbih;->a(Ljava/lang/CharSequence;IICLjava/lang/StringBuilder;Z)Ljava/lang/String;

    move-result-object v0

    .line 1315
    :goto_1
    return-object v0

    :cond_3
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1372
    iget-object v0, p0, Lbih;->a:Ljava/lang/String;

    return-object v0
.end method
