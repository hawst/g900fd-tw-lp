.class LEA;
.super Landroid/os/Handler;
.source "CustomProgressDialog.java"


# instance fields
.field final synthetic a:LEz;


# direct methods
.method constructor <init>(LEz;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, LEA;->a:LEz;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 79
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 80
    iget-object v0, p0, LEA;->a:LEz;

    invoke-static {v0}, LEz;->a(LEz;)Landroid/widget/ProgressBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, LEA;->a:LEz;

    invoke-static {v0}, LEz;->a(LEz;)I

    move-result v0

    if-lez v0, :cond_2

    .line 82
    iget-object v0, p0, LEA;->a:LEz;

    invoke-static {v0}, LEz;->a(LEz;)Landroid/widget/ProgressBar;

    move-result-object v0

    iget-object v1, p0, LEA;->a:LEz;

    invoke-static {v1}, LEz;->a(LEz;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 83
    iget-object v0, p0, LEA;->a:LEz;

    invoke-static {v0}, LEz;->a(LEz;)Landroid/widget/ProgressBar;

    move-result-object v0

    iget-object v1, p0, LEA;->a:LEz;

    invoke-static {v1}, LEz;->b(LEz;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 89
    :cond_0
    :goto_0
    iget-object v0, p0, LEA;->a:LEz;

    invoke-static {v0}, LEz;->a(LEz;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LEA;->a:LEz;

    invoke-static {v0}, LEz;->a(LEz;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 90
    iget-object v0, p0, LEA;->a:LEz;

    invoke-static {v0}, LEz;->a(LEz;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, LEA;->a:LEz;

    invoke-static {v1}, LEz;->a(LEz;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    :cond_1
    return-void

    .line 85
    :cond_2
    iget-object v0, p0, LEA;->a:LEz;

    invoke-static {v0}, LEz;->a(LEz;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    goto :goto_0
.end method
