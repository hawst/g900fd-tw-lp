.class Latn;
.super Ljava/lang/Object;
.source "WelcomeController.java"


# instance fields
.field private a:I

.field private a:Ljava/lang/String;

.field private a:Z


# direct methods
.method public constructor <init>(ZILjava/lang/String;)V
    .locals 0

    .prologue
    .line 208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 209
    iput-boolean p1, p0, Latn;->a:Z

    .line 210
    iput p2, p0, Latn;->a:I

    .line 211
    iput-object p3, p0, Latn;->a:Ljava/lang/String;

    .line 212
    return-void
.end method

.method static synthetic a(Latn;)I
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Latn;->a:I

    return v0
.end method

.method static synthetic a(Latn;I)I
    .locals 0

    .prologue
    .line 198
    iput p1, p0, Latn;->a:I

    return p1
.end method

.method public static a(Ljava/lang/String;I)Latn;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 217
    :try_start_0
    new-instance v0, Lbtm;

    invoke-direct {v0}, Lbtm;-><init>()V

    invoke-virtual {v0, p0}, Lbtm;->a(Ljava/lang/String;)Lbth;

    move-result-object v0

    invoke-virtual {v0}, Lbth;->a()Lbtk;

    move-result-object v4

    .line 218
    new-instance v0, Latn;

    const-string v2, "welcome"

    .line 219
    invoke-virtual {v4, v2}, Lbtk;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "welcome"

    invoke-virtual {v4, v2}, Lbtk;->a(Ljava/lang/String;)Lbth;

    move-result-object v2

    invoke-virtual {v2}, Lbth;->a()Z

    move-result v2

    move v3, v2

    :goto_0
    const-string v2, "highlights"

    .line 220
    invoke-virtual {v4, v2}, Lbtk;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v1, "highlights"

    invoke-virtual {v4, v1}, Lbtk;->a(Ljava/lang/String;)Lbth;

    move-result-object v1

    invoke-virtual {v1}, Lbth;->b()I

    move-result v1

    move v2, v1

    :goto_1
    const-string v1, "announce"

    .line 221
    invoke-virtual {v4, v1}, Lbtk;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "announce"

    invoke-virtual {v4, v1}, Lbtk;->a(Ljava/lang/String;)Lbth;

    move-result-object v1

    invoke-virtual {v1}, Lbth;->a()Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-direct {v0, v3, v2, v1}, Latn;-><init>(ZILjava/lang/String;)V

    .line 228
    :goto_3
    return-object v0

    :cond_0
    move v3, v1

    .line 219
    goto :goto_0

    :cond_1
    move v2, v1

    .line 220
    goto :goto_1

    .line 221
    :cond_2
    const-string v1, ""
    :try_end_0
    .catch Lbto; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 222
    :catch_0
    move-exception v0

    .line 223
    const-string v1, "WelcomeController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Corrupted LastShownStories json : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 226
    new-instance v0, Latn;

    const/4 v1, 0x1

    const-string v2, ""

    invoke-direct {v0, v1, p1, v2}, Latn;-><init>(ZILjava/lang/String;)V

    goto :goto_3
.end method

.method static synthetic a(Latn;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Latn;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Latn;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Latn;->a:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Latn;)Z
    .locals 1

    .prologue
    .line 198
    iget-boolean v0, p0, Latn;->a:Z

    return v0
.end method

.method static synthetic a(Latn;Z)Z
    .locals 0

    .prologue
    .line 198
    iput-boolean p1, p0, Latn;->a:Z

    return p1
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 232
    new-instance v0, Lbtk;

    invoke-direct {v0}, Lbtk;-><init>()V

    .line 233
    const-string v1, "welcome"

    iget-boolean v2, p0, Latn;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbtk;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 234
    const-string v1, "highlights"

    iget v2, p0, Latn;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbtk;->a(Ljava/lang/String;Ljava/lang/Number;)V

    .line 235
    const-string v1, "announce"

    iget-object v2, p0, Latn;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lbtk;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    invoke-virtual {v0}, Lbtk;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 241
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LastShownStories: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Latn;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
