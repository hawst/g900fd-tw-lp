.class public final enum LaEJ;
.super Ljava/lang/Enum;
.source "DocumentContentTable.java"

# interfaces
.implements Lbjv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaEJ;",
        ">;",
        "Lbjv",
        "<",
        "LaFr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaEJ;

.field private static final synthetic a:[LaEJ;

.field public static final enum b:LaEJ;

.field public static final enum c:LaEJ;

.field public static final enum d:LaEJ;

.field public static final enum e:LaEJ;

.field public static final enum f:LaEJ;

.field public static final enum g:LaEJ;

.field public static final enum h:LaEJ;

.field public static final enum i:LaEJ;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum j:LaEJ;

.field public static final enum k:LaEJ;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum l:LaEJ;

.field public static final enum m:LaEJ;

.field public static final enum n:LaEJ;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum o:LaEJ;

.field public static final enum p:LaEJ;

.field public static final enum q:LaEJ;

.field public static final enum r:LaEJ;

.field public static final enum s:LaEJ;

.field public static final enum t:LaEJ;

.field public static final enum u:LaEJ;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum v:LaEJ;

.field public static final enum w:LaEJ;

.field public static final enum x:LaEJ;


# instance fields
.field private final a:LaFr;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/16 v8, 0x30

    const/4 v10, 0x0

    const/16 v9, 0xe

    .line 44
    new-instance v0, LaEJ;

    const-string v1, "CONTENT_ETAG"

    invoke-static {}, LaEI;->b()LaEI;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "contentETag"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 45
    invoke-virtual {v2, v9, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, LaEJ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEJ;->a:LaEJ;

    .line 47
    new-instance v0, LaEJ;

    const-string v1, "CONTENT_TYPE"

    invoke-static {}, LaEI;->b()LaEI;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "contentType"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 48
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-virtual {v2, v9, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, LaEJ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEJ;->b:LaEJ;

    .line 50
    new-instance v0, LaEJ;

    const-string v1, "ENCRYPTION_KEY"

    invoke-static {}, LaEI;->b()LaEI;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "encryptionKey"

    sget-object v5, LaFI;->d:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 51
    invoke-virtual {v2, v9, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v12, v2}, LaEJ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEJ;->c:LaEJ;

    .line 53
    new-instance v0, LaEJ;

    const-string v1, "ENCRYPTION_ALGORITHM"

    const/4 v2, 0x3

    invoke-static {}, LaEI;->b()LaEI;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "encryptionAlgorithm"

    sget-object v6, LaFI;->c:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 54
    invoke-virtual {v3, v9, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEJ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEJ;->d:LaEJ;

    .line 62
    new-instance v0, LaEJ;

    const-string v1, "HAS_PENDING_CHANGES"

    const/4 v2, 0x4

    invoke-static {}, LaEI;->b()LaEI;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x5b

    new-instance v5, LaFG;

    const-string v6, "hasPendingChanges"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 64
    invoke-virtual {v5}, LaFG;->b()LaFG;

    move-result-object v5

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v5

    .line 63
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEJ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEJ;->e:LaEJ;

    .line 70
    new-instance v0, LaEJ;

    const-string v1, "HAS_PENDING_COMMENTS"

    const/4 v2, 0x5

    invoke-static {}, LaEI;->b()LaEI;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x5e

    new-instance v5, LaFG;

    const-string v6, "hasPendingComments"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 72
    invoke-virtual {v5}, LaFG;->b()LaFG;

    move-result-object v5

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v5

    .line 71
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEJ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEJ;->f:LaEJ;

    .line 74
    new-instance v0, LaEJ;

    const-string v1, "OWNED_FILE_PATH"

    const/4 v2, 0x6

    invoke-static {}, LaEI;->b()LaEI;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "filePath"

    sget-object v6, LaFI;->c:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 75
    invoke-virtual {v3, v9, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEJ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEJ;->g:LaEJ;

    .line 80
    new-instance v0, LaEJ;

    const-string v1, "NOT_OWNED_FILE_PATH"

    const/4 v2, 0x7

    invoke-static {}, LaEI;->b()LaEI;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x40

    new-instance v5, LaFG;

    const-string v6, "notOwnedFilePath"

    sget-object v7, LaFI;->c:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 81
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEJ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEJ;->h:LaEJ;

    .line 83
    new-instance v0, LaEJ;

    const-string v1, "__LEGACY_COLUMN_CACHE_PATH"

    const/16 v2, 0x8

    .line 84
    invoke-static {}, LaEI;->b()LaEI;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x25

    new-instance v5, LaFG;

    const-string v6, "cachePath"

    sget-object v7, LaFI;->c:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 85
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    .line 86
    invoke-virtual {v3, v8}, LaFt;->b(I)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEJ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEJ;->i:LaEJ;

    .line 88
    new-instance v0, LaEJ;

    const-string v1, "LAST_OPENED_TIME"

    const/16 v2, 0x9

    invoke-static {}, LaEI;->b()LaEI;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "lastOpenedTime"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 89
    invoke-virtual {v3, v9, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEJ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEJ;->j:LaEJ;

    .line 91
    new-instance v0, LaEJ;

    const-string v1, "__LEGACY_COLUMN_CACHE_LAST_MODIFIED_TIME"

    const/16 v2, 0xa

    .line 92
    invoke-static {}, LaEI;->b()LaEI;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x25

    new-instance v5, LaFG;

    const-string v6, "cacheLastModifiedTime"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 93
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    .line 94
    invoke-virtual {v3, v8}, LaFt;->b(I)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEJ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEJ;->k:LaEJ;

    .line 96
    new-instance v0, LaEJ;

    const-string v1, "LAST_MODIFIED_TIME"

    const/16 v2, 0xb

    invoke-static {}, LaEI;->b()LaEI;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "lastModifiedTime"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 97
    invoke-virtual {v3, v8, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEJ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEJ;->l:LaEJ;

    .line 103
    new-instance v0, LaEJ;

    const-string v1, "SERVER_SIDE_LAST_MODIFIED_TIME"

    const/16 v2, 0xc

    invoke-static {}, LaEI;->b()LaEI;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x3e

    new-instance v5, LaFG;

    const-string v6, "serverSideLastModifiedTime"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 104
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEJ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEJ;->m:LaEJ;

    .line 110
    new-instance v0, LaEJ;

    const-string v1, "__LAST_SYNCED_TIME"

    const/16 v2, 0xd

    .line 111
    invoke-static {}, LaEI;->b()LaEI;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x46

    new-instance v5, LaFG;

    const-string v6, "lastSyncedTime"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 112
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    const/16 v4, 0x5b

    .line 113
    invoke-virtual {v3, v4}, LaFt;->b(I)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEJ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEJ;->n:LaEJ;

    .line 119
    new-instance v0, LaEJ;

    const-string v1, "MD5_CHECKSUM"

    invoke-static {}, LaEI;->b()LaEI;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    const/16 v3, 0x3e

    new-instance v4, LaFG;

    const-string v5, "md5Checksum"

    sget-object v6, LaFI;->c:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 120
    invoke-virtual {v2, v3, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, LaEJ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEJ;->o:LaEJ;

    .line 127
    new-instance v0, LaEJ;

    const-string v1, "IS_DOCUMENT_SNAPSHOTTED"

    const/16 v2, 0xf

    invoke-static {}, LaEI;->b()LaEI;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x54

    new-instance v5, LaFG;

    const-string v6, "isDocumentSnapshotted"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 129
    invoke-virtual {v5}, LaFG;->b()LaFG;

    move-result-object v5

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v5

    .line 128
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEJ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEJ;->p:LaEJ;

    .line 136
    new-instance v0, LaEJ;

    const-string v1, "IS_TEMPORARY"

    const/16 v2, 0x10

    invoke-static {}, LaEI;->b()LaEI;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "isTemporary"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 137
    invoke-virtual {v3, v8, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEJ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEJ;->q:LaEJ;

    .line 139
    new-instance v0, LaEJ;

    const-string v1, "REFERENCED_CONTENT_ID"

    const/16 v2, 0x11

    invoke-static {}, LaEI;->b()LaEI;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "referencedContentId"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 141
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v5

    const/4 v6, 0x0

    sget-object v7, LaFH;->b:LaFH;

    .line 140
    invoke-virtual {v4, v5, v6, v7}, LaFG;->a(LaFy;LaFr;LaFH;)LaFG;

    move-result-object v4

    invoke-virtual {v3, v8, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    const/16 v4, 0x32

    .line 142
    invoke-virtual {v3, v4}, LaFt;->b(I)LaFt;

    move-result-object v3

    const/16 v4, 0x32

    new-instance v5, LaFG;

    const-string v6, "referencedContentId"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 144
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v6

    const/4 v7, 0x0

    sget-object v8, LaFH;->b:LaFH;

    .line 143
    invoke-virtual {v5, v6, v7, v8}, LaFG;->a(LaFy;LaFr;LaFH;)LaFG;

    move-result-object v5

    .line 145
    invoke-virtual {v5}, LaFG;->a()LaFG;

    move-result-object v5

    .line 143
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEJ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEJ;->r:LaEJ;

    .line 154
    new-instance v0, LaEJ;

    const-string v1, "IS_DIRTY"

    const/16 v2, 0x12

    invoke-static {}, LaEI;->b()LaEI;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x31

    new-instance v5, LaFG;

    const-string v6, "isDirty"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 155
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v5

    invoke-virtual {v5}, LaFG;->b()LaFG;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEJ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEJ;->s:LaEJ;

    .line 158
    new-instance v0, LaEJ;

    const-string v1, "GC_LOCK_EXPIRY_TIME"

    const/16 v2, 0x13

    invoke-static {}, LaEI;->b()LaEI;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x34

    new-instance v5, LaFG;

    const-string v6, "gcLockExpiryTime"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 159
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v5

    invoke-virtual {v5}, LaFG;->b()LaFG;

    move-result-object v5

    .line 158
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEJ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEJ;->t:LaEJ;

    .line 160
    new-instance v0, LaEJ;

    const-string v1, "__LEGACY_JOBSET_ID"

    const/16 v2, 0x14

    .line 161
    invoke-static {}, LaEI;->b()LaEI;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x36

    new-instance v5, LaFG;

    const-string v6, "jobsetId"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 163
    invoke-static {}, LaEW;->a()LaEW;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(LaFy;)LaFG;

    move-result-object v5

    .line 162
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    const/16 v4, 0x3f

    .line 164
    invoke-virtual {v3, v4}, LaFt;->b(I)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEJ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEJ;->u:LaEJ;

    .line 166
    new-instance v0, LaEJ;

    const-string v1, "MANIFEST_ID"

    const/16 v2, 0x15

    invoke-static {}, LaEI;->b()LaEI;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x3f

    new-instance v5, LaFG;

    const-string v6, "manifestId"

    sget-object v7, LaFI;->a:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 168
    invoke-static {}, LaFa;->a()LaFa;

    move-result-object v6

    invoke-virtual {v5, v6}, LaFG;->a(LaFy;)LaFG;

    move-result-object v5

    .line 167
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEJ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEJ;->v:LaEJ;

    .line 176
    new-instance v0, LaEJ;

    const-string v1, "DOCUMENT_ID"

    const/16 v2, 0x16

    invoke-static {}, LaEI;->b()LaEI;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x60

    new-instance v5, LaFG;

    const-string v6, "documentId"

    sget-object v7, LaFI;->c:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 177
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEJ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEJ;->w:LaEJ;

    .line 182
    new-instance v0, LaEJ;

    const-string v1, "REFERENCED_FONT_FAMILIES"

    const/16 v2, 0x17

    invoke-static {}, LaEI;->b()LaEI;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    const/16 v4, 0x64

    new-instance v5, LaFG;

    const-string v6, "referencedFontFamilies"

    sget-object v7, LaFI;->c:LaFI;

    invoke-direct {v5, v6, v7}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 183
    invoke-virtual {v3, v4, v5}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEJ;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEJ;->x:LaEJ;

    .line 43
    const/16 v0, 0x18

    new-array v0, v0, [LaEJ;

    sget-object v1, LaEJ;->a:LaEJ;

    aput-object v1, v0, v10

    sget-object v1, LaEJ;->b:LaEJ;

    aput-object v1, v0, v11

    sget-object v1, LaEJ;->c:LaEJ;

    aput-object v1, v0, v12

    const/4 v1, 0x3

    sget-object v2, LaEJ;->d:LaEJ;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LaEJ;->e:LaEJ;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LaEJ;->f:LaEJ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LaEJ;->g:LaEJ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LaEJ;->h:LaEJ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LaEJ;->i:LaEJ;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LaEJ;->j:LaEJ;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LaEJ;->k:LaEJ;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LaEJ;->l:LaEJ;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LaEJ;->m:LaEJ;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LaEJ;->n:LaEJ;

    aput-object v2, v0, v1

    sget-object v1, LaEJ;->o:LaEJ;

    aput-object v1, v0, v9

    const/16 v1, 0xf

    sget-object v2, LaEJ;->p:LaEJ;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LaEJ;->q:LaEJ;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LaEJ;->r:LaEJ;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LaEJ;->s:LaEJ;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LaEJ;->t:LaEJ;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LaEJ;->u:LaEJ;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LaEJ;->v:LaEJ;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LaEJ;->w:LaEJ;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LaEJ;->x:LaEJ;

    aput-object v2, v0, v1

    sput-object v0, LaEJ;->a:[LaEJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaFt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 188
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 189
    invoke-virtual {p3}, LaFt;->a()LaFr;

    move-result-object v0

    iput-object v0, p0, LaEJ;->a:LaFr;

    .line 190
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaEJ;
    .locals 1

    .prologue
    .line 43
    const-class v0, LaEJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaEJ;

    return-object v0
.end method

.method public static values()[LaEJ;
    .locals 1

    .prologue
    .line 43
    sget-object v0, LaEJ;->a:[LaEJ;

    invoke-virtual {v0}, [LaEJ;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaEJ;

    return-object v0
.end method


# virtual methods
.method public a()LaFr;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, LaEJ;->a:LaFr;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, LaEJ;->a()LaFr;

    move-result-object v0

    return-object v0
.end method
