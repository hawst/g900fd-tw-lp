.class public LaeM;
.super Lbxl;
.source "DocsListGDataParser.java"


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Lbxl;-><init>(Ljava/io/InputStream;Lorg/xmlpull/v1/XmlPullParser;)V

    .line 72
    return-void
.end method


# virtual methods
.method protected a()LaeD;
    .locals 1

    .prologue
    .line 81
    new-instance v0, LaeD;

    invoke-direct {v0}, LaeD;-><init>()V

    return-object v0
.end method

.method protected a()LaeF;
    .locals 1

    .prologue
    .line 76
    new-instance v0, LaeF;

    invoke-direct {v0}, LaeF;-><init>()V

    return-object v0
.end method

.method public a(Lbxb;)LaeF;
    .locals 1

    .prologue
    .line 273
    invoke-super {p0, p1}, Lbxl;->a(Lbxb;)Lbxb;

    move-result-object v0

    check-cast v0, LaeF;

    return-object v0
.end method

.method protected bridge synthetic a()Lbxb;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, LaeM;->a()LaeF;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lbxb;)Lbxb;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1}, LaeM;->a(Lbxb;)LaeF;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a()Lbxc;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, LaeM;->a()LaeD;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lbxb;)V
    .locals 6

    .prologue
    const/16 v5, 0x3a

    const/4 v1, 0x0

    .line 181
    invoke-virtual {p0}, LaeM;->a()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 183
    instance-of v0, p1, LaeF;

    if-nez v0, :cond_0

    .line 184
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected GdataDocEntry!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 187
    :cond_0
    check-cast p1, LaeF;

    .line 188
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 189
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v3

    .line 190
    const-string v4, "http://schemas.google.com/g/2005"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 191
    const-string v3, "resourceId"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 192
    invoke-static {v2}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 196
    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 197
    const/4 v1, 0x0

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 198
    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 201
    :cond_1
    invoke-virtual {p1, v0}, LaeF;->c(Ljava/lang/String;)V

    .line 202
    invoke-virtual {p1, v1}, LaeF;->d(Ljava/lang/String;)V

    .line 269
    :cond_2
    :goto_0
    return-void

    .line 203
    :cond_3
    const-string v1, "lastViewed"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 204
    invoke-static {v2}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LaeF;->f(Ljava/lang/String;)V

    goto :goto_0

    .line 205
    :cond_4
    const-string v1, "lastModifiedBy"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 206
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v1

    .line 207
    invoke-static {v2, v1}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_2

    .line 209
    const-string v3, "name"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 210
    invoke-static {v2}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 211
    invoke-virtual {p1, v0}, LaeF;->i(Ljava/lang/String;)V

    .line 208
    :cond_5
    :goto_2
    invoke-static {v2, v1}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 212
    :cond_6
    const-string v3, "email"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 213
    invoke-static {v2}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 214
    invoke-virtual {p1, v0}, LaeF;->j(Ljava/lang/String;)V

    goto :goto_2

    .line 217
    :cond_7
    const-string v1, "quotaBytesUsed"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 218
    invoke-static {v2}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 219
    if-eqz v0, :cond_2

    .line 220
    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LaeF;->b(J)V

    goto :goto_0

    .line 223
    :cond_8
    const-string v4, "http://schemas.google.com/docs/2007"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 224
    const-string v3, "removed"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 225
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LaeF;->e(Z)V

    goto :goto_0

    .line 226
    :cond_9
    const-string v3, "changestamp"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 227
    const-string v0, "value"

    invoke-interface {v2, v1, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 228
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, LaeF;->a(I)V

    goto/16 :goto_0

    .line 229
    :cond_a
    const-string v3, "md5Checksum"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 230
    invoke-static {v2}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 231
    invoke-virtual {p1, v0}, LaeF;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 232
    :cond_b
    const-string v3, "sharedWithMeDate"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 233
    invoke-static {v2}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 234
    invoke-virtual {p1, v0}, LaeF;->g(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 235
    :cond_c
    const-string v3, "modifiedByMeDate"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 236
    invoke-static {v2}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 237
    invoke-virtual {p1, v0}, LaeF;->h(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 238
    :cond_d
    const-string v3, "isShareable"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 239
    const-string v0, "value"

    invoke-interface {v2, v1, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 240
    if-eqz v0, :cond_2

    .line 245
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LaeF;->b(Z)V

    goto/16 :goto_0

    .line 247
    :cond_e
    const-string v3, "isExplicitlyTrashed"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 248
    const-string v0, "value"

    invoke-interface {v2, v1, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 249
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LaeF;->f(Z)V

    goto/16 :goto_0

    .line 250
    :cond_f
    const-string v3, "size"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 251
    invoke-static {v2}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 252
    if-eqz v0, :cond_2

    .line 253
    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LaeF;->a(J)V

    goto/16 :goto_0

    .line 255
    :cond_10
    const-string v3, "plusPhotosRootFolder"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 256
    const-string v0, "value"

    invoke-interface {v2, v1, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 257
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LaeF;->g(Z)V

    goto/16 :goto_0

    .line 258
    :cond_11
    const-string v3, "plusPhotosFolder"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 259
    const-string v0, "value"

    invoke-interface {v2, v1, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 260
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LaeF;->h(Z)V

    goto/16 :goto_0

    .line 261
    :cond_12
    const-string v3, "plusMediaFile"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 262
    const-string v0, "value"

    invoke-interface {v2, v1, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 263
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LaeF;->h(Z)V

    goto/16 :goto_0

    .line 264
    :cond_13
    const-string v1, "externalAppEntryMimeType"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 265
    invoke-static {v2}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 266
    invoke-virtual {p1, v0}, LaeF;->a(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lbxb;)V
    .locals 6

    .prologue
    const/16 v3, 0x3a

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 146
    const-string v0, "http://schemas.google.com/docs/2007#parent"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 147
    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 149
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    .line 151
    const-string v0, ""

    .line 153
    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 154
    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    invoke-virtual {v1, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 155
    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 158
    :cond_0
    sget-object v2, LaGv;->a:LaGv;

    invoke-virtual {v2}, LaGv;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 159
    invoke-virtual {p0}, LaeM;->a()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    const/4 v2, 0x0

    sget-object v3, Lbxq;->q:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 160
    if-eqz v0, :cond_2

    .line 161
    check-cast p4, LaJT;

    .line 162
    invoke-interface {p4, v1, v0}, LaJT;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :cond_1
    :goto_0
    return-void

    .line 165
    :cond_2
    const-string v0, "DocsListGDataParser"

    const-string v2, "Parent folder with resourceId %s has no title: "

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v1, v3, v4

    invoke-static {v0, v2, v3}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 168
    :cond_3
    const-string v0, "DocsListGDataParser"

    const-string v1, "Error parsing parent entry: %s"

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p3, v2, v4

    invoke-static {v0, v1, v2}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 170
    :cond_4
    const-string v0, "http://schemas.google.com/docs/2007/thumbnail"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 171
    check-cast p4, LaJT;

    .line 172
    invoke-interface {p4, p3}, LaJT;->k(Ljava/lang/String;)V

    goto :goto_0

    .line 173
    :cond_5
    instance-of v0, p4, LaeF;

    if-eqz v0, :cond_1

    .line 174
    check-cast p4, LaJT;

    .line 175
    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-interface {p4, p1, v0}, LaJT;->a(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method protected a(Lbxb;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 109
    invoke-virtual {p0}, LaeM;->a()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 111
    instance-of v0, p1, LaeF;

    if-nez v0, :cond_0

    .line 112
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected GdataDocEntry!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p1

    .line 115
    check-cast v0, LaeF;

    .line 116
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 118
    sget-object v4, Lbxq;->w:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 119
    const/4 v3, 0x0

    sget-object v4, Lbxq;->x:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 120
    const-string v3, "http://schemas.google.com/g/2005/labels#starred"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 121
    invoke-virtual {v0, v1}, LaeF;->a(Z)V

    move v0, v1

    .line 141
    :goto_0
    return v0

    .line 125
    :cond_1
    const-string v3, "http://schemas.google.com/g/2005/labels#shared"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 126
    invoke-virtual {v0, v1}, LaeF;->c(Z)V

    move v0, v1

    .line 127
    goto :goto_0

    .line 130
    :cond_2
    const-string v3, "http://schemas.google.com/g/2005/labels#hidden"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 131
    invoke-virtual {v0, v1}, LaeF;->d(Z)V

    move v0, v1

    .line 132
    goto :goto_0

    .line 135
    :cond_3
    const-string v3, "http://schemas.google.com/g/2005/labels#trashed"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 136
    invoke-virtual {v0, v1}, LaeF;->i(Z)V

    move v0, v1

    .line 137
    goto :goto_0

    .line 141
    :cond_4
    invoke-super {p0, p1}, Lbxl;->a(Lbxb;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lbxc;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 86
    invoke-virtual {p0}, LaeM;->a()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v1

    .line 88
    instance-of v0, p1, LaeD;

    if-nez v0, :cond_0

    .line 89
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected DocEntry!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p1

    .line 92
    check-cast v0, LaeD;

    .line 93
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 95
    sget-object v3, Lbxq;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 96
    sget-object v2, Lbxq;->h:Ljava/lang/String;

    invoke-interface {v1, v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 97
    const-string v3, "next"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 98
    sget-object v2, Lbxq;->j:Ljava/lang/String;

    invoke-interface {v1, v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 99
    invoke-virtual {v0, v1}, LaeD;->a(Ljava/lang/String;)V

    .line 100
    const/4 v0, 0x1

    .line 103
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Lbxl;->a(Lbxc;)Z

    move-result v0

    goto :goto_0
.end method
