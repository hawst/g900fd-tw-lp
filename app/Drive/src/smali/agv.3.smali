.class public Lagv;
.super Ljava/lang/Object;
.source "EditorOfflineDbSyncableFactory.java"

# interfaces
.implements Lahn;


# instance fields
.field private final a:LaGM;

.field private final a:LadQ;

.field a:LbiP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiP",
            "<",
            "LacS;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LaGM;LadQ;LbiP;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGM;",
            "LadQ;",
            "LbiP",
            "<",
            "LacS;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p0, Lagv;->a:LaGM;

    .line 26
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LadQ;

    iput-object v0, p0, Lagv;->a:LadQ;

    .line 27
    iput-object p3, p0, Lagv;->a:LbiP;

    .line 28
    return-void
.end method


# virtual methods
.method public a(LahJ;LagH;)Lahm;
    .locals 6

    .prologue
    .line 32
    iget-object v0, p0, Lagv;->a:LbiP;

    invoke-virtual {v0}, LbiP;->a()Z

    move-result v0

    const-string v1, "Cannot create a EditorOfflineDbSyncable when OfflineSyncer is not bound."

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    .line 34
    new-instance v0, Lagt;

    iget-object v1, p0, Lagv;->a:LbiP;

    invoke-virtual {v1}, LbiP;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LacS;

    iget-object v3, p0, Lagv;->a:LaGM;

    iget-object v5, p0, Lagv;->a:LadQ;

    move-object v1, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lagt;-><init>(LahJ;LacS;LaGM;LagH;LadQ;)V

    return-object v0
.end method

.method public a(LahJ;)Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    return v0
.end method
