.class Laaq;
.super Ljava/lang/Object;
.source "OnlineSearchProcessor.java"

# interfaces
.implements Lagy;


# instance fields
.field private a:I

.field private final a:Laar;

.field private a:Z


# direct methods
.method public constructor <init>(Laar;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput v0, p0, Laaq;->a:I

    .line 37
    iput-boolean v0, p0, Laaq;->a:Z

    .line 40
    const-string v0, "null listener"

    invoke-static {p1, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laar;

    iput-object v0, p0, Laaq;->a:Laar;

    .line 41
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 54
    return-void
.end method

.method public a(LaeD;)V
    .locals 0

    .prologue
    .line 45
    return-void
.end method

.method public a(LaeD;LaJT;)V
    .locals 2

    .prologue
    .line 64
    iget v0, p0, Laaq;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Laaq;->a:I

    .line 65
    invoke-interface {p2}, LaJT;->b()Ljava/lang/String;

    move-result-object v0

    .line 66
    const-string v1, "null resourceId"

    invoke-static {v0, v1}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    iget-object v0, p0, Laaq;->a:Laar;

    invoke-interface {v0, p2}, Laar;->a(LaJT;)V

    .line 70
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 58
    const-string v1, "OnlineSearchProcessor"

    const-string v2, "%d results for search %s %s"

    const/4 v0, 0x3

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget v4, p0, Laaq;->a:I

    .line 59
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x1

    iget-object v4, p0, Laaq;->a:Laar;

    aput-object v4, v3, v0

    const/4 v4, 0x2

    iget-boolean v0, p0, Laaq;->a:Z

    if-eqz v0, :cond_0

    const-string v0, " (canceled)"

    :goto_0
    aput-object v0, v3, v4

    .line 58
    invoke-static {v1, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 60
    return-void

    .line 59
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public b(LaeD;)V
    .locals 2

    .prologue
    .line 49
    iget-boolean v1, p0, Laaq;->a:Z

    iget-object v0, p0, Laaq;->a:Laar;

    invoke-interface {v0}, Laar;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v1

    iput-boolean v0, p0, Laaq;->a:Z

    .line 50
    return-void

    .line 49
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 74
    const-string v1, "OnlineSearchProcessor[%s%s]"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-boolean v0, p0, Laaq;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "canceled, "

    :goto_0
    aput-object v0, v2, v3

    const/4 v0, 0x1

    iget-object v3, p0, Laaq;->a:Laar;

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
