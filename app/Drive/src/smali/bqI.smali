.class final LbqI;
.super LbqG;
.source "ChecksumHashFunction.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final a:I

.field private final a:Lbjv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjv",
            "<+",
            "Ljava/util/zip/Checksum;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/String;


# direct methods
.method constructor <init>(Lbjv;ILjava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbjv",
            "<+",
            "Ljava/util/zip/Checksum;",
            ">;I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, LbqG;-><init>()V

    .line 37
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjv;

    iput-object v0, p0, LbqI;->a:Lbjv;

    .line 38
    const/16 v0, 0x20

    if-eq p2, v0, :cond_0

    const/16 v0, 0x40

    if-ne p2, v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "bits (%s) must be either 32 or 64"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v1

    invoke-static {v0, v3, v2}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 39
    iput p2, p0, LbqI;->a:I

    .line 40
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LbqI;->a:Ljava/lang/String;

    .line 41
    return-void

    :cond_1
    move v0, v1

    .line 38
    goto :goto_0
.end method

.method static synthetic a(LbqI;)I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, LbqI;->a:I

    return v0
.end method


# virtual methods
.method public a()LbqR;
    .locals 3

    .prologue
    .line 48
    new-instance v1, LbqK;

    iget-object v0, p0, LbqI;->a:Lbjv;

    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/zip/Checksum;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v0, v2}, LbqK;-><init>(LbqI;Ljava/util/zip/Checksum;LbqJ;)V

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, LbqI;->a:Ljava/lang/String;

    return-object v0
.end method
