.class public final enum LBg;
.super Ljava/lang/Enum;
.source "DocListEntrySyncState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LBg;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LBg;

.field private static final synthetic a:[LBg;

.field public static final enum b:LBg;

.field public static final enum c:LBg;

.field public static final enum d:LBg;


# instance fields
.field final a:I

.field final a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 20
    new-instance v0, LBg;

    const-string v1, "UP_TO_DATE"

    sget v2, Lxi;->pin_up_to_date:I

    invoke-direct {v0, v1, v3, v2, v4}, LBg;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, LBg;->a:LBg;

    .line 25
    new-instance v0, LBg;

    const-string v1, "OUT_OF_DATE"

    sget v2, Lxi;->pin_out_of_date:I

    invoke-direct {v0, v1, v4, v2, v4}, LBg;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, LBg;->b:LBg;

    .line 31
    new-instance v0, LBg;

    const-string v1, "NOT_YET_AVAILABLE"

    sget v2, Lxi;->pin_not_available:I

    invoke-direct {v0, v1, v5, v2, v3}, LBg;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, LBg;->c:LBg;

    .line 35
    new-instance v0, LBg;

    const-string v1, "NOT_PINNED"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v6, v2, v3}, LBg;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, LBg;->d:LBg;

    .line 18
    const/4 v0, 0x4

    new-array v0, v0, [LBg;

    sget-object v1, LBg;->a:LBg;

    aput-object v1, v0, v3

    sget-object v1, LBg;->b:LBg;

    aput-object v1, v0, v4

    sget-object v1, LBg;->c:LBg;

    aput-object v1, v0, v5

    sget-object v1, LBg;->d:LBg;

    aput-object v1, v0, v6

    sput-object v0, LBg;->a:[LBg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput p3, p0, LBg;->a:I

    .line 42
    iput-boolean p4, p0, LBg;->a:Z

    .line 43
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LBg;
    .locals 1

    .prologue
    .line 18
    const-class v0, LBg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LBg;

    return-object v0
.end method

.method public static values()[LBg;
    .locals 1

    .prologue
    .line 18
    sget-object v0, LBg;->a:[LBg;

    invoke-virtual {v0}, [LBg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LBg;

    return-object v0
.end method
