.class public final LbfZ;
.super Ljava/lang/Object;
.source "CharEscapers.java"


# static fields
.field private static final a:Lbga;

.field private static final b:Lbga;

.field private static final c:Lbga;

.field private static final d:Lbga;

.field private static final e:Lbga;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 28
    new-instance v0, Lbgb;

    const-string v1, "-_.*"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lbgb;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LbfZ;->a:Lbga;

    .line 31
    new-instance v0, Lbgb;

    const-string v1, "-_.!~*\'()@:$&,;="

    invoke-direct {v0, v1, v3}, Lbgb;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LbfZ;->b:Lbga;

    .line 34
    new-instance v0, Lbgb;

    const-string v1, "-_.!~*\'()@:$&,;=+/?"

    invoke-direct {v0, v1, v3}, Lbgb;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LbfZ;->c:Lbga;

    .line 37
    new-instance v0, Lbgb;

    const-string v1, "-_.!~*\'():$&,;="

    invoke-direct {v0, v1, v3}, Lbgb;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LbfZ;->d:Lbga;

    .line 40
    new-instance v0, Lbgb;

    const-string v1, "-_.!~*\'()@:$,;/?:"

    invoke-direct {v0, v1, v3}, Lbgb;-><init>(Ljava/lang/String;Z)V

    sput-object v0, LbfZ;->e:Lbga;

    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    sget-object v0, LbfZ;->a:Lbga;

    invoke-virtual {v0, p0}, Lbga;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 92
    :try_start_0
    const-string v0, "UTF-8"

    invoke-static {p0, v0}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 93
    :catch_0
    move-exception v0

    .line 95
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    sget-object v0, LbfZ;->b:Lbga;

    invoke-virtual {v0, p0}, Lbga;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    sget-object v0, LbfZ;->c:Lbga;

    invoke-virtual {v0, p0}, Lbga;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static e(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    sget-object v0, LbfZ;->d:Lbga;

    invoke-virtual {v0, p0}, Lbga;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static f(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    sget-object v0, LbfZ;->e:Lbga;

    invoke-virtual {v0, p0}, Lbga;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
