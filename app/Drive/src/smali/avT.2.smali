.class public abstract LavT;
.super Ljava/lang/Object;
.source "Analytics.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 103
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 105
    const-string v1, "content"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 106
    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    .line 116
    :goto_0
    return-object v0

    .line 107
    :cond_0
    const-string v1, "file"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 108
    invoke-static {p0}, LavY;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 109
    const-string v0, "drive"

    goto :goto_0

    .line 111
    :cond_1
    const-string v0, "file"

    goto :goto_0

    .line 114
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "other-scheme-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(Landroid/app/Activity;)V
.end method

.method public a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 71
    sget-object v0, LavQ;->b:LavQ;

    const-string v1, "src"

    invoke-static {p1}, LavT;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, LavT;->a(LavQ;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    return-void
.end method

.method protected abstract a(LavQ;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method protected abstract a(LavQ;Ljava/lang/String;Ljava/lang/String;J)V
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 76
    sget-object v0, LavQ;->a:LavQ;

    const-string v1, ""

    invoke-virtual {p0, v0, p1, v1}, LavT;->a(LavQ;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 86
    sget-object v1, LavQ;->a:LavQ;

    const-string v3, ""

    int-to-long v4, p2

    move-object v0, p0

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, LavT;->a(LavQ;Ljava/lang/String;Ljava/lang/String;J)V

    .line 87
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 81
    sget-object v0, LavQ;->a:LavQ;

    invoke-virtual {p0, v0, p1, p2}, LavT;->a(LavQ;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    return-void
.end method

.method public b(Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 96
    sget-object v1, LavQ;->c:LavQ;

    const-string v3, ""

    int-to-long v4, p2

    move-object v0, p0

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, LavT;->a(LavQ;Ljava/lang/String;Ljava/lang/String;J)V

    .line 97
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 91
    sget-object v0, LavQ;->c:LavQ;

    invoke-virtual {p0, v0, p1, p2}, LavT;->a(LavQ;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    return-void
.end method
