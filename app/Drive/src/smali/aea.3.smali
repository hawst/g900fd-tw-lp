.class Laea;
.super Ljava/lang/Object;
.source "TempFileManagerImpl.java"

# interfaces
.implements LadC;


# instance fields
.field private a:I

.field final synthetic a:LadZ;

.field private a:Ljava/io/File;

.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(LadZ;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 34
    iput-object p1, p0, Laea;->a:LadZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Laea;->a:I

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Laea;->a:Ljava/io/File;

    .line 35
    iput-object p2, p0, Laea;->a:Ljava/lang/String;

    .line 36
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Laea;->a:LadZ;

    invoke-static {v0}, LadZ;->a(LadZ;)LadJ;

    move-result-object v0

    iget-object v1, p0, Laea;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LadJ;->a(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(LbmY;)Landroid/os/ParcelFileDescriptor;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "LadI;",
            ">;)",
            "Landroid/os/ParcelFileDescriptor;"
        }
    .end annotation

    .prologue
    .line 61
    sget-object v0, LadI;->b:LadI;

    invoke-virtual {p1, v0}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LadI;->c:LadI;

    invoke-virtual {p1, v0}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    :cond_0
    new-instance v0, LadB;

    const-string v1, "Writing is not supported"

    invoke-direct {v0, v1}, LadB;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_1
    invoke-static {p1}, LadI;->a(LbmY;)I

    move-result v0

    .line 66
    if-nez v0, :cond_2

    .line 67
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "Unsupported permission"

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_2
    iget-object v1, p0, Laea;->a:Ljava/io/File;

    if-eqz v1, :cond_3

    .line 71
    iget-object v1, p0, Laea;->a:Ljava/io/File;

    invoke-static {v1, v0}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 83
    :goto_0
    return-object v0

    .line 73
    :cond_3
    iget-object v1, p0, Laea;->a:LadZ;

    invoke-static {v1}, LadZ;->a(LadZ;)Laeb;

    move-result-object v1

    invoke-interface {v1}, Laeb;->a()Ljava/io/File;

    move-result-object v1

    iput-object v1, p0, Laea;->a:Ljava/io/File;

    .line 74
    iget-object v1, p0, Laea;->a:LadZ;

    iget-object v2, p0, Laea;->a:Ljava/io/File;

    invoke-virtual {v1, v2}, LadZ;->a(Ljava/io/File;)Ljava/io/OutputStream;

    move-result-object v1

    .line 77
    :try_start_0
    iget-object v2, p0, Laea;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->deleteOnExit()V

    .line 79
    iget-object v2, p0, Laea;->a:LadZ;

    invoke-static {v2}, LadZ;->a(LadZ;)LadJ;

    move-result-object v2

    iget-object v3, p0, Laea;->a:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, LadJ;->a(Ljava/lang/String;Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 83
    iget-object v1, p0, Laea;->a:Ljava/io/File;

    invoke-static {v1, v0}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_0

    .line 81
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    throw v0
.end method

.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Laea;->a:LadZ;

    invoke-static {v0}, LadZ;->a(LadZ;)LadJ;

    move-result-object v0

    iget-object v1, p0, Laea;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LadJ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized a()V
    .locals 1

    .prologue
    .line 39
    monitor-enter p0

    :try_start_0
    iget v0, p0, Laea;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Laea;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    monitor-exit p0

    return-void

    .line 39
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Laea;->a:LadZ;

    invoke-static {v0}, LadZ;->a(LadZ;)LadJ;

    move-result-object v0

    iget-object v1, p0, Laea;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LadJ;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized close()V
    .locals 2

    .prologue
    .line 44
    monitor-enter p0

    :try_start_0
    iget v0, p0, Laea;->a:I

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 45
    iget v0, p0, Laea;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Laea;->a:I

    .line 46
    iget v0, p0, Laea;->a:I

    if-nez v0, :cond_1

    .line 47
    iget-object v0, p0, Laea;->a:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Laea;->a:LadZ;

    invoke-static {v0}, LadZ;->a(LadZ;)Laeb;

    move-result-object v0

    iget-object v1, p0, Laea;->a:Ljava/io/File;

    invoke-interface {v0, v1}, Laeb;->a(Ljava/io/File;)V

    .line 50
    :cond_0
    iget-object v0, p0, Laea;->a:LadZ;

    invoke-static {v0}, LadZ;->a(LadZ;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Laea;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    :cond_1
    monitor-exit p0

    return-void

    .line 44
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
