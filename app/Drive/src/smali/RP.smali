.class public LRP;
.super Ljava/lang/Object;
.source "OnlineSearchFragment.java"

# interfaces
.implements LRO;


# instance fields
.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;


# direct methods
.method constructor <init>(Laja;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, LRP;->a:Lbxw;

    .line 66
    return-void
.end method

.method private a()LM;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, LRP;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LH;

    .line 121
    invoke-virtual {v0}, LH;->a()LM;

    move-result-object v0

    return-object v0
.end method

.method private a()Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, LRP;->a:Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, LRP;->a:Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;

    .line 115
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, LRP;->a()LM;

    move-result-object v0

    const-string v1, "OnlineSearchFragment"

    invoke-virtual {v0, v1}, LM;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;

    goto :goto_0
.end method


# virtual methods
.method public a()Laat;
    .locals 1

    .prologue
    .line 107
    invoke-direct {p0}, LRP;->a()Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;

    move-result-object v0

    .line 108
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a(Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;)Laat;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Laat;->a:Laat;

    goto :goto_0
.end method

.method public a(LaFO;Ljava/lang/String;)LbsU;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFO;",
            "Ljava/lang/String;",
            ")",
            "LbsU",
            "<",
            "Laay;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    iget-object v0, p0, LRP;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrm;

    .line 75
    invoke-virtual {v0}, Lrm;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    new-instance v0, LtO;

    invoke-direct {v0}, LtO;-><init>()V

    invoke-static {v0}, LbsK;->a(Ljava/lang/Throwable;)LbsU;

    move-result-object v0

    .line 102
    :goto_0
    return-object v0

    .line 79
    :cond_0
    invoke-direct {p0}, LRP;->a()Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;

    move-result-object v0

    .line 80
    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a(Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;)LaFO;

    move-result-object v1

    invoke-virtual {p1, v1}, LaFO;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 81
    invoke-static {v0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a(Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 82
    iput-object v0, p0, LRP;->a:Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;

    .line 102
    :goto_1
    iget-object v0, p0, LRP;->a:Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a(Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;)LbsU;

    move-result-object v0

    goto :goto_0

    .line 84
    :cond_1
    invoke-direct {p0}, LRP;->a()LM;

    move-result-object v1

    .line 86
    if-eqz v0, :cond_2

    .line 87
    invoke-virtual {v1}, LM;->a()Lac;

    move-result-object v2

    invoke-virtual {v2, v0}, Lac;->a(Landroid/support/v4/app/Fragment;)Lac;

    move-result-object v0

    invoke-virtual {v0}, Lac;->a()I

    .line 90
    :cond_2
    new-instance v0, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;-><init>()V

    .line 91
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 92
    const-string v3, "accountName"

    invoke-virtual {p1}, LaFO;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const-string v3, "query"

    invoke-virtual {v2, v3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->e(Landroid/os/Bundle;)V

    .line 95
    invoke-virtual {v1}, LM;->a()Lac;

    move-result-object v1

    const-string v2, "OnlineSearchFragment"

    invoke-virtual {v1, v0, v2}, Lac;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lac;

    move-result-object v1

    invoke-virtual {v1}, Lac;->a()I

    .line 99
    iput-object v0, p0, LRP;->a:Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;

    goto :goto_1
.end method
