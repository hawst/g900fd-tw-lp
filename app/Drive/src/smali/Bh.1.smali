.class public final enum LBh;
.super Ljava/lang/Enum;
.source "DocListEntrySyncState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LBh;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LBh;

.field private static final synthetic a:[LBh;

.field public static final enum b:LBh;

.field public static final enum c:LBh;

.field public static final enum d:LBh;

.field public static final enum e:LBh;

.field public static final enum f:LBh;

.field public static final enum g:LBh;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 49
    new-instance v0, LBh;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v3}, LBh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LBh;->a:LBh;

    .line 51
    new-instance v0, LBh;

    const-string v1, "WAITING_FOR_NETWORK"

    invoke-direct {v0, v1, v4}, LBh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LBh;->b:LBh;

    .line 53
    new-instance v0, LBh;

    const-string v1, "WAITING_FOR_WIFI"

    invoke-direct {v0, v1, v5}, LBh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LBh;->c:LBh;

    .line 58
    new-instance v0, LBh;

    const-string v1, "PAUSED_MANUALLY"

    invoke-direct {v0, v1, v6}, LBh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LBh;->d:LBh;

    .line 60
    new-instance v0, LBh;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v7}, LBh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LBh;->e:LBh;

    .line 62
    new-instance v0, LBh;

    const-string v1, "IN_PROGRESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LBh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LBh;->f:LBh;

    .line 64
    new-instance v0, LBh;

    const-string v1, "NO_TRANSFER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LBh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LBh;->g:LBh;

    .line 47
    const/4 v0, 0x7

    new-array v0, v0, [LBh;

    sget-object v1, LBh;->a:LBh;

    aput-object v1, v0, v3

    sget-object v1, LBh;->b:LBh;

    aput-object v1, v0, v4

    sget-object v1, LBh;->c:LBh;

    aput-object v1, v0, v5

    sget-object v1, LBh;->d:LBh;

    aput-object v1, v0, v6

    sget-object v1, LBh;->e:LBh;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LBh;->f:LBh;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LBh;->g:LBh;

    aput-object v2, v0, v1

    sput-object v0, LBh;->a:[LBh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LBh;
    .locals 1

    .prologue
    .line 47
    const-class v0, LBh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LBh;

    return-object v0
.end method

.method public static values()[LBh;
    .locals 1

    .prologue
    .line 47
    sget-object v0, LBh;->a:[LBh;

    invoke-virtual {v0}, [LBh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LBh;

    return-object v0
.end method
