.class public LaFE;
.super Ljava/lang/Object;
.source "FieldDefinition.java"


# instance fields
.field public final a:LaFH;

.field public final a:LaFI;

.field public final a:LaFr;

.field public final a:LaFy;

.field public final a:Ljava/lang/Object;

.field public final a:Ljava/lang/String;

.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final a:Z

.field public final b:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;LaFI;ZLjava/util/Set;ZLjava/lang/Object;LaFy;LaFr;LaFH;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LaFI;",
            "Z",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "Ljava/lang/Object;",
            "LaFy;",
            "LaFr;",
            "LaFH;",
            ")V"
        }
    .end annotation

    .prologue
    .line 232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 233
    invoke-static {p9}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    iput-object p1, p0, LaFE;->a:Ljava/lang/String;

    .line 236
    iput-object p2, p0, LaFE;->a:LaFI;

    .line 237
    invoke-static {p4}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LaFE;->a:Ljava/util/Set;

    .line 238
    iput-boolean p5, p0, LaFE;->b:Z

    .line 239
    iput-object p6, p0, LaFE;->a:Ljava/lang/Object;

    .line 240
    iput-object p7, p0, LaFE;->a:LaFy;

    .line 241
    iput-object p8, p0, LaFE;->a:LaFr;

    .line 242
    iput-object p9, p0, LaFE;->a:LaFH;

    .line 243
    iput-boolean p3, p0, LaFE;->a:Z

    .line 244
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;LaFI;ZLjava/util/Set;ZLjava/lang/Object;LaFy;LaFr;LaFH;LaFF;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct/range {p0 .. p9}, LaFE;-><init>(Ljava/lang/String;LaFI;ZLjava/util/Set;ZLjava/lang/Object;LaFy;LaFr;LaFH;)V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 248
    const-string v0, "FieldDefinition[%s, %s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LaFE;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LaFE;->a:LaFI;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
