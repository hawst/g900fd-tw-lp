.class public LHQ;
.super LHq;
.source "MainEntryViewHolder.java"

# interfaces
.implements LCO;


# instance fields
.field private final a:LLp;

.field private final a:Landroid/view/View;

.field private final a:Landroid/widget/ImageView;

.field private final a:Lcom/google/android/apps/docs/view/FixedSizeTextView;

.field private a:Z


# direct methods
.method public constructor <init>(Lapd;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;Lapc;Landroid/view/View;LDk;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0, p1, p3, p4, p5}, LHq;-><init>(Lapd;Lapc;Landroid/view/View;LDk;)V

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, LHQ;->a:Z

    .line 31
    sget v0, Lxc;->title:I

    .line 32
    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/FixedSizeTextView;

    iput-object v0, p0, LHQ;->a:Lcom/google/android/apps/docs/view/FixedSizeTextView;

    .line 33
    sget v0, Lxc;->doc_icon:I

    .line 34
    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 33
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LHQ;->a:Landroid/widget/ImageView;

    .line 35
    sget v0, Lxc;->title_container:I

    .line 36
    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 35
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, LHQ;->a:Landroid/view/View;

    .line 37
    new-instance v0, LLp;

    iget-object v1, p0, LHQ;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-direct {v0, p4, v1}, LLp;-><init>(Landroid/view/View;I)V

    iput-object v0, p0, LHQ;->a:LLp;

    .line 38
    invoke-virtual {p2}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 39
    iget-object v1, p0, LHQ;->a:Lcom/google/android/apps/docs/view/FixedAspectRatioFrameLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/view/FixedAspectRatioFrameLayout;->setAspectRatio(F)V

    .line 40
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, LHQ;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 47
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, LHQ;->a:Lcom/google/android/apps/docs/view/FixedSizeTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/docs/view/FixedSizeTextView;->setTextAndTypefaceNoLayout(Ljava/lang/String;Landroid/graphics/Typeface;)V

    .line 43
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, LHQ;->a:LLp;

    invoke-virtual {v0, p1}, LLp;->a(Z)V

    .line 84
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, LHQ;->a:Z

    return v0
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 58
    invoke-virtual {p0, v2}, LHQ;->c(Z)V

    .line 59
    iget-boolean v0, p0, LHQ;->a:Z

    if-nez v0, :cond_0

    .line 60
    iget-object v0, p0, LHQ;->a:LEd;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LEd;->b(Z)V

    .line 61
    iget-object v0, p0, LHQ;->a:LEd;

    invoke-virtual {v0, v2}, LEd;->d(Z)V

    .line 63
    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 4

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 67
    iget-boolean v0, p0, LHQ;->a:Z

    if-eqz v0, :cond_1

    .line 68
    iget-object v0, p0, LHQ;->a:Lcom/google/android/apps/docs/view/FixedAspectRatioFrameLayout;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Lcom/google/android/apps/docs/view/FixedAspectRatioFrameLayout;->setVisibility(I)V

    .line 72
    :goto_0
    iget-object v3, p0, LHQ;->a:Landroid/view/View;

    if-eqz p1, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 73
    iget-object v0, p0, LHQ;->a:Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;

    if-eqz p1, :cond_0

    move v2, v1

    :cond_0
    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;->setVisibility(I)V

    .line 74
    iget-object v0, p0, LHQ;->a:Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;->setClickable(Z)V

    .line 75
    iget-object v0, p0, LHQ;->a:LEd;

    invoke-virtual {v0, v1}, LEd;->c(Z)V

    .line 76
    iget-object v0, p0, LHQ;->a:LEd;

    invoke-virtual {v0, v1}, LEd;->d(Z)V

    .line 78
    invoke-virtual {p0}, LHQ;->a()V

    .line 79
    return-void

    .line 70
    :cond_1
    iget-object v3, p0, LHQ;->a:Lcom/google/android/apps/docs/view/FixedAspectRatioFrameLayout;

    if-eqz p1, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lcom/google/android/apps/docs/view/FixedAspectRatioFrameLayout;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    .line 72
    goto :goto_1
.end method

.method public e(Z)V
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, LHQ;->a:Z

    .line 51
    return-void
.end method
