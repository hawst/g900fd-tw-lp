.class public LajB;
.super LajD;
.source "ScopedInjector.java"

# interfaces
.implements Lbuu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        ">",
        "LajD",
        "<TS;>;",
        "Lbuu;"
    }
.end annotation


# instance fields
.field private final a:Lbuu;


# direct methods
.method constructor <init>(Lbuu;Lajh;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbuu;",
            "Lajh",
            "<TS;>;TS;)V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0, p2, p3}, LajD;-><init>(Lajh;Ljava/lang/Object;)V

    .line 44
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbuu;

    iput-object v0, p0, LajB;->a:Lbuu;

    .line 45
    return-void
.end method


# virtual methods
.method public final a(Lbuv;)Lbup;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbuv",
            "<TT;>;)",
            "Lbup",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 119
    invoke-virtual {p0}, LajB;->b()Ljava/lang/Object;

    move-result-object v1

    .line 121
    :try_start_0
    iget-object v0, p0, LajB;->a:Lbuu;

    invoke-interface {v0, p1}, Lbuu;->a(Lbuv;)Lbup;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 123
    invoke-virtual {p0, v1}, LajB;->b(Ljava/lang/Object;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0, v1}, LajB;->b(Ljava/lang/Object;)V

    throw v0
.end method

.method public final a(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 139
    invoke-virtual {p0}, LajB;->b()Ljava/lang/Object;

    move-result-object v1

    .line 141
    :try_start_0
    iget-object v0, p0, LajB;->a:Lbuu;

    invoke-interface {v0, p1}, Lbuu;->a(Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 143
    invoke-virtual {p0, v1}, LajB;->b(Ljava/lang/Object;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0, v1}, LajB;->b(Ljava/lang/Object;)V

    throw v0
.end method

.method public final a(LbuP;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbuP",
            "<TT;>;)",
            "Ljava/util/List",
            "<",
            "Lbup",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 69
    invoke-virtual {p0}, LajB;->b()Ljava/lang/Object;

    move-result-object v1

    .line 71
    :try_start_0
    iget-object v0, p0, LajB;->a:Lbuu;

    invoke-interface {v0, p1}, Lbuu;->a(LbuP;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 73
    invoke-virtual {p0, v1}, LajB;->b(Ljava/lang/Object;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0, v1}, LajB;->b(Ljava/lang/Object;)V

    throw v0
.end method

.method public final a()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lbuv",
            "<*>;",
            "Lbup",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 109
    invoke-virtual {p0}, LajB;->b()Ljava/lang/Object;

    move-result-object v1

    .line 111
    :try_start_0
    iget-object v0, p0, LajB;->a:Lbuu;

    invoke-interface {v0}, Lbuu;->a()Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 113
    invoke-virtual {p0, v1}, LajB;->b(Ljava/lang/Object;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0, v1}, LajB;->b(Ljava/lang/Object;)V

    throw v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 214
    invoke-virtual {p0}, LajB;->b()Ljava/lang/Object;

    move-result-object v1

    .line 216
    :try_start_0
    iget-object v0, p0, LajB;->a:Lbuu;

    invoke-interface {v0, p1}, Lbuu;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 218
    invoke-virtual {p0, v1}, LajB;->b(Ljava/lang/Object;)V

    .line 220
    return-void

    .line 218
    :catchall_0
    move-exception v0

    invoke-virtual {p0, v1}, LajB;->b(Ljava/lang/Object;)V

    throw v0
.end method
