.class public Ltv;
.super Ljava/lang/Object;
.source "DriveNewEntryCreatorsFactory.java"

# interfaces
.implements LFC;


# instance fields
.field a:LEt;

.field a:LtK;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()LCN;
    .locals 1

    .prologue
    .line 67
    new-instance v0, Ltw;

    invoke-direct {v0, p0}, Ltw;-><init>(Ltv;)V

    return-object v0
.end method

.method private a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 55
    invoke-static {p1}, Lamt;->a(Landroid/content/Context;)Z

    move-result v0

    .line 56
    if-nez v0, :cond_0

    iget-object v0, p0, Ltv;->a:LtK;

    sget-object v1, LNn;->a:LNn;

    .line 57
    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    invoke-static {p1}, LUs;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Ltv;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1}, Ltv;->a(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private b()LCN;
    .locals 1

    .prologue
    .line 99
    new-instance v0, Ltx;

    invoke-direct {v0, p0}, Ltx;-><init>(Ltv;)V

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Ltv;->a:LtK;

    sget-object v1, LNn;->a:LNn;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, LpR;->create_new:I

    :goto_0
    return v0

    :cond_0
    sget v0, LpR;->create_new_doc_title:I

    goto :goto_0
.end method

.method public a()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LCN;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 47
    iget-object v1, p0, Ltv;->a:LEt;

    invoke-virtual {v1}, LEt;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 48
    invoke-direct {p0}, Ltv;->b()LCN;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    invoke-direct {p0}, Ltv;->a()LCN;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    return-object v0
.end method
