.class LbpP;
.super Lbmt;
.source "RegularImmutableSortedMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbmt",
        "<",
        "Ljava/util/Map$Entry",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field private final a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<TK;>;"
        }
    .end annotation
.end field

.field final synthetic a:LbpO;


# direct methods
.method constructor <init>(LbpO;)V
    .locals 1

    .prologue
    .line 61
    iput-object p1, p0, LbpP;->a:LbpO;

    invoke-direct {p0}, Lbmt;-><init>()V

    .line 63
    iget-object v0, p0, LbpP;->a:LbpO;

    iget-object v0, v0, LbpO;->a:LbpM;

    invoke-virtual {v0}, LbpM;->a()Lbnk;

    move-result-object v0

    invoke-virtual {v0}, Lbnk;->a()LbmF;

    move-result-object v0

    iput-object v0, p0, LbpP;->a:LbmF;

    return-void
.end method


# virtual methods
.method a()Lbmv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbmv",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, LbpP;->a:LbpO;

    return-object v0
.end method

.method public a(I)Ljava/util/Map$Entry;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, LbpP;->a:LbmF;

    invoke-virtual {v0, p1}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LbpP;->a:LbpO;

    iget-object v1, v1, LbpO;->a:LbpM;

    invoke-static {v1}, LbpM;->a(LbpM;)LbmF;

    move-result-object v1

    invoke-virtual {v1, p1}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, LboS;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public synthetic get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0, p1}, LbpP;->a(I)Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method
