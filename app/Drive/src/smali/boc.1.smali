.class abstract enum Lboc;
.super Ljava/lang/Enum;
.source "MapMakerInternalMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lboc;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lboc;

.field private static final synthetic a:[Lboc;

.field static final a:[[Lboc;

.field public static final enum b:Lboc;

.field public static final enum c:Lboc;

.field public static final enum d:Lboc;

.field public static final enum e:Lboc;

.field public static final enum f:Lboc;

.field public static final enum g:Lboc;

.field public static final enum h:Lboc;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 352
    new-instance v0, Lbod;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v3}, Lbod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lboc;->a:Lboc;

    .line 360
    new-instance v0, Lboe;

    const-string v1, "STRONG_EXPIRABLE"

    invoke-direct {v0, v1, v4}, Lboe;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lboc;->b:Lboc;

    .line 376
    new-instance v0, Lbof;

    const-string v1, "STRONG_EVICTABLE"

    invoke-direct {v0, v1, v5}, Lbof;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lboc;->c:Lboc;

    .line 392
    new-instance v0, Lbog;

    const-string v1, "STRONG_EXPIRABLE_EVICTABLE"

    invoke-direct {v0, v1, v6}, Lbog;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lboc;->d:Lboc;

    .line 410
    new-instance v0, Lboh;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v7}, Lboh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lboc;->e:Lboc;

    .line 418
    new-instance v0, Lboi;

    const-string v1, "WEAK_EXPIRABLE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lboi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lboc;->f:Lboc;

    .line 434
    new-instance v0, Lboj;

    const-string v1, "WEAK_EVICTABLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lboj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lboc;->g:Lboc;

    .line 450
    new-instance v0, Lbok;

    const-string v1, "WEAK_EXPIRABLE_EVICTABLE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lbok;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lboc;->h:Lboc;

    .line 351
    const/16 v0, 0x8

    new-array v0, v0, [Lboc;

    sget-object v1, Lboc;->a:Lboc;

    aput-object v1, v0, v3

    sget-object v1, Lboc;->b:Lboc;

    aput-object v1, v0, v4

    sget-object v1, Lboc;->c:Lboc;

    aput-object v1, v0, v5

    sget-object v1, Lboc;->d:Lboc;

    aput-object v1, v0, v6

    sget-object v1, Lboc;->e:Lboc;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lboc;->f:Lboc;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lboc;->g:Lboc;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lboc;->h:Lboc;

    aput-object v2, v0, v1

    sput-object v0, Lboc;->a:[Lboc;

    .line 478
    new-array v0, v6, [[Lboc;

    new-array v1, v7, [Lboc;

    sget-object v2, Lboc;->a:Lboc;

    aput-object v2, v1, v3

    sget-object v2, Lboc;->b:Lboc;

    aput-object v2, v1, v4

    sget-object v2, Lboc;->c:Lboc;

    aput-object v2, v1, v5

    sget-object v2, Lboc;->d:Lboc;

    aput-object v2, v1, v6

    aput-object v1, v0, v3

    new-array v1, v3, [Lboc;

    aput-object v1, v0, v4

    new-array v1, v7, [Lboc;

    sget-object v2, Lboc;->e:Lboc;

    aput-object v2, v1, v3

    sget-object v2, Lboc;->f:Lboc;

    aput-object v2, v1, v4

    sget-object v2, Lboc;->g:Lboc;

    aput-object v2, v1, v5

    sget-object v2, Lboc;->h:Lboc;

    aput-object v2, v1, v6

    aput-object v1, v0, v5

    sput-object v0, Lboc;->a:[[Lboc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 351
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILbnZ;)V
    .locals 0

    .prologue
    .line 351
    invoke-direct {p0, p1, p2}, Lboc;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(LboA;ZZ)Lboc;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 486
    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz p2, :cond_0

    const/4 v0, 0x2

    :cond_0
    or-int/2addr v0, v1

    .line 487
    sget-object v1, Lboc;->a:[[Lboc;

    invoke-virtual {p0}, LboA;->ordinal()I

    move-result v2

    aget-object v1, v1, v2

    aget-object v0, v1, v0

    return-object v0

    :cond_1
    move v1, v0

    .line 486
    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lboc;
    .locals 1

    .prologue
    .line 351
    const-class v0, Lboc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lboc;

    return-object v0
.end method

.method public static values()[Lboc;
    .locals 1

    .prologue
    .line 351
    sget-object v0, Lboc;->a:[Lboc;

    invoke-virtual {v0}, [Lboc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lboc;

    return-object v0
.end method


# virtual methods
.method a(Lboy;Lbox;Lbox;)Lbox;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lboy",
            "<TK;TV;>;",
            "Lbox",
            "<TK;TV;>;",
            "Lbox",
            "<TK;TV;>;)",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 510
    invoke-interface {p2}, Lbox;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2}, Lbox;->a()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1, p3}, Lboc;->a(Lboy;Ljava/lang/Object;ILbox;)Lbox;

    move-result-object v0

    return-object v0
.end method

.method abstract a(Lboy;Ljava/lang/Object;ILbox;)Lbox;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lboy",
            "<TK;TV;>;TK;I",
            "Lbox",
            "<TK;TV;>;)",
            "Lbox",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method a(Lbox;Lbox;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lbox",
            "<TK;TV;>;",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 517
    invoke-interface {p1}, Lbox;->a()J

    move-result-wide v0

    invoke-interface {p2, v0, v1}, Lbox;->a(J)V

    .line 519
    invoke-interface {p1}, Lbox;->c()Lbox;

    move-result-object v0

    invoke-static {v0, p2}, LbnY;->a(Lbox;Lbox;)V

    .line 520
    invoke-interface {p1}, Lbox;->b()Lbox;

    move-result-object v0

    invoke-static {p2, v0}, LbnY;->a(Lbox;Lbox;)V

    .line 522
    invoke-static {p1}, LbnY;->b(Lbox;)V

    .line 523
    return-void
.end method

.method b(Lbox;Lbox;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lbox",
            "<TK;TV;>;",
            "Lbox",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 529
    invoke-interface {p1}, Lbox;->e()Lbox;

    move-result-object v0

    invoke-static {v0, p2}, LbnY;->b(Lbox;Lbox;)V

    .line 530
    invoke-interface {p1}, Lbox;->d()Lbox;

    move-result-object v0

    invoke-static {p2, v0}, LbnY;->b(Lbox;Lbox;)V

    .line 532
    invoke-static {p1}, LbnY;->c(Lbox;)V

    .line 533
    return-void
.end method
