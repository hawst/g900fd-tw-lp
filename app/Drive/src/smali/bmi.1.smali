.class final Lbmi;
.super Lbng;
.source "EmptyImmutableSortedMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lbng",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final transient a:Lbnk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbnk",
            "<TK;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/Comparator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TK;>;)V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Lbng;-><init>()V

    .line 37
    invoke-static {p1}, Lbnk;->a(Ljava/util/Comparator;)Lbnk;

    move-result-object v0

    iput-object v0, p0, Lbmi;->a:Lbnk;

    .line 38
    return-void
.end method


# virtual methods
.method public a()LbmY;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmY",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 82
    invoke-static {}, LbmY;->a()LbmY;

    move-result-object v0

    return-object v0
.end method

.method public a()Lbmv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbmv",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 67
    invoke-static {}, LbmF;->c()LbmF;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Z)Lbng;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;Z)",
            "Lbng",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 92
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    return-object p0
.end method

.method public a()Lbnk;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbnk",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lbmi;->a:Lbnk;

    return-object v0
.end method

.method a()Z
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return v0
.end method

.method b()LbmY;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmY",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 87
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should never be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public b(Ljava/lang/Object;Z)Lbng;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;Z)",
            "Lbng",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 98
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    return-object p0
.end method

.method public synthetic c()LbmY;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lbmi;->a()Lbnk;

    move-result-object v0

    return-object v0
.end method

.method public synthetic entrySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lbmi;->a()LbmY;

    move-result-object v0

    return-object v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 47
    const/4 v0, 0x0

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lbmi;->a()Lbnk;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    const-string v0, "{}"

    return-object v0
.end method

.method public synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lbmi;->a()Lbmv;

    move-result-object v0

    return-object v0
.end method
