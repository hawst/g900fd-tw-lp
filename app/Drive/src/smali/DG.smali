.class public LDG;
.super LzM;
.source "SortedMergeAdapterVisibleRange.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LDC;",
        ">",
        "LzM;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final a:Lbjv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjv",
            "<",
            "LDE",
            "<TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LzN;Lbjv;I)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LzN;",
            "Lbjv",
            "<",
            "LDE",
            "<TT;>;>;I)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 19
    invoke-direct {p0, p1}, LzM;-><init>(LzN;)V

    .line 20
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjv;

    iput-object v0, p0, LDG;->a:Lbjv;

    .line 21
    if-ltz p3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "adapterIndex=%s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 22
    iput p3, p0, LDG;->a:I

    .line 23
    return-void

    :cond_0
    move v0, v2

    .line 21
    goto :goto_0
.end method


# virtual methods
.method protected a(LalS;)LalS;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 27
    invoke-virtual {p1}, LalS;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    invoke-static {v6, v6}, LalS;->b(II)LalS;

    move-result-object v0

    .line 65
    :goto_0
    return-object v0

    .line 31
    :cond_0
    iget-object v0, p0, LDG;->a:Lbjv;

    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDE;

    .line 32
    invoke-virtual {p1}, LalS;->b()I

    move-result v1

    invoke-virtual {v0}, LDE;->a()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 35
    invoke-virtual {p1}, LalS;->a()I

    move-result v1

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_1

    .line 37
    invoke-virtual {v0, v2}, LDE;->a(I)I

    move-result v1

    .line 38
    iget v4, p0, LDG;->a:I

    if-ne v1, v4, :cond_2

    .line 43
    :cond_1
    if-lt v2, v3, :cond_3

    .line 44
    invoke-static {v6, v6}, LalS;->b(II)LalS;

    move-result-object v0

    goto :goto_0

    .line 36
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 47
    :cond_3
    iget v1, p0, LDG;->a:I

    .line 48
    invoke-virtual {v0, v1, v2}, LDE;->a(II)I

    move-result v4

    .line 51
    add-int/lit8 v1, v3, -0x1

    :goto_2
    if-lt v1, v2, :cond_4

    .line 53
    invoke-virtual {v0, v1}, LDE;->a(I)I

    move-result v3

    .line 54
    iget v5, p0, LDG;->a:I

    if-ne v3, v5, :cond_5

    .line 59
    :cond_4
    if-ge v1, v2, :cond_6

    .line 60
    invoke-static {v4, v6}, LalS;->b(II)LalS;

    move-result-object v0

    goto :goto_0

    .line 52
    :cond_5
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 63
    :cond_6
    iget v2, p0, LDG;->a:I

    .line 64
    invoke-virtual {v0, v2, v1}, LDE;->a(II)I

    move-result v0

    .line 65
    add-int/lit8 v0, v0, 0x1

    invoke-static {v4, v0}, LalS;->a(II)LalS;

    move-result-object v0

    goto :goto_0
.end method
