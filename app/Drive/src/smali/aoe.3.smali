.class public Laoe;
.super LanC;
.source "DocumentFileUriFetcher.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LanC",
        "<",
        "Lcom/google/android/apps/docs/utils/FetchSpec;",
        "LakD",
        "<",
        "Ljava/io/File;",
        ">;",
        "LakD",
        "<",
        "Landroid/net/Uri;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LaGM;

.field private final a:LanZ;

.field private final a:Luf;

.field private final a:Lvs;


# direct methods
.method constructor <init>(LaGM;Luf;Lvs;LanZ;)V
    .locals 1

    .prologue
    .line 47
    invoke-static {}, LaoT;->a()LaoT;

    move-result-object v0

    invoke-direct {p0, v0, p4}, LanC;-><init>(Laop;Laoo;)V

    .line 49
    iput-object p1, p0, Laoe;->a:LaGM;

    .line 50
    iput-object p2, p0, Laoe;->a:Luf;

    .line 51
    iput-object p3, p0, Laoe;->a:Lvs;

    .line 52
    iput-object p4, p0, Laoe;->a:LanZ;

    .line 53
    return-void
.end method

.method private a(LakD;LaGo;LvB;)LakD;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;",
            "LaGo;",
            "LvB;",
            ")",
            "LakD",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    .line 85
    invoke-static {p1}, LakD;->a(LakD;)LakD;

    move-result-object v1

    .line 87
    :try_start_0
    invoke-virtual {p1}, LakD;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-interface {p3, v0, p2}, LvB;->a(Ljava/io/File;LaGo;)Landroid/net/Uri;

    move-result-object v0

    .line 88
    new-instance v2, Laof;

    invoke-direct {v2, p0, p3, v1}, Laof;-><init>(Laoe;LvB;LakD;)V

    .line 89
    invoke-static {v0, v2}, LakD;->a(Ljava/lang/Object;LakG;)LakD;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 100
    return-object v0

    .line 99
    :catchall_0
    move-exception v0

    .line 100
    invoke-virtual {v1}, LakD;->close()V

    throw v0
.end method


# virtual methods
.method protected a(Lcom/google/android/apps/docs/utils/FetchSpec;LakD;)LakD;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;)",
            "LakD",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    :try_start_0
    iget-object v0, p0, Laoe;->a:LaGM;

    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/FetchSpec;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-interface {v0, v1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v0

    .line 61
    if-eqz v0, :cond_2

    .line 62
    invoke-interface {v0}, LaGo;->a()LaGv;

    move-result-object v1

    .line 63
    sget-object v2, LaGv;->b:LaGv;

    invoke-virtual {v2, v1}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 64
    iget-object v1, p0, Laoe;->a:Luf;

    invoke-direct {p0, p2, v0, v1}, Laoe;->a(LakD;LaGo;LvB;)LakD;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 74
    invoke-virtual {p2}, LakD;->close()V

    :goto_0
    return-object v0

    .line 65
    :cond_0
    :try_start_1
    sget-object v2, LaGv;->i:LaGv;

    invoke-virtual {v2, v1}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 66
    iget-object v1, p0, Laoe;->a:Lvs;

    invoke-direct {p0, p2, v0, v1}, Laoe;->a(LakD;LaGo;LvB;)LakD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 74
    invoke-virtual {p2}, LakD;->close()V

    goto :goto_0

    .line 68
    :cond_1
    :try_start_2
    new-instance v0, Laog;

    invoke-direct {v0, v1}, Laog;-><init>(LaGv;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 74
    :catchall_0
    move-exception v0

    invoke-virtual {p2}, LakD;->close()V

    throw v0

    .line 71
    :cond_2
    :try_start_3
    new-instance v0, Ladt;

    invoke-direct {v0}, Ladt;-><init>()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    check-cast p1, Lcom/google/android/apps/docs/utils/FetchSpec;

    check-cast p2, LakD;

    invoke-virtual {p0, p1, p2}, Laoe;->a(Lcom/google/android/apps/docs/utils/FetchSpec;LakD;)LakD;

    move-result-object v0

    return-object v0
.end method

.method protected a(LakD;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 111
    invoke-virtual {p1}, LakD;->close()V

    .line 112
    return-void
.end method

.method protected synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, LakD;

    invoke-virtual {p0, p1}, Laoe;->b(LakD;)V

    return-void
.end method

.method protected b(LakD;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakD",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 116
    invoke-virtual {p1}, LakD;->close()V

    .line 117
    return-void
.end method

.method protected synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, LakD;

    invoke-virtual {p0, p1}, Laoe;->a(LakD;)V

    return-void
.end method
