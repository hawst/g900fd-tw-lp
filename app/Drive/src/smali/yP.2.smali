.class LyP;
.super LaGN;
.source "ActivityEntryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGN",
        "<",
        "Lcom/google/android/gms/drive/database/data/EntrySpec;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field final synthetic a:LyI;


# direct methods
.method public constructor <init>(LyI;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, LyP;->a:LyI;

    invoke-direct {p0}, LaGN;-><init>()V

    .line 173
    iput-object p2, p0, LyP;->a:Ljava/lang/String;

    .line 174
    return-void
.end method


# virtual methods
.method public a(LaGM;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, LyP;->a:LyI;

    invoke-static {v0}, LyI;->a(LyI;)LaGu;

    move-result-object v0

    invoke-interface {v0}, LaGu;->a()LaFM;

    move-result-object v0

    invoke-virtual {v0}, LaFM;->a()LaFO;

    move-result-object v0

    .line 180
    iget-object v1, p0, LyP;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a(LaFO;Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v1

    .line 181
    invoke-interface {p1, v1}, LaGM;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 183
    if-nez v0, :cond_0

    .line 184
    iget-object v0, p0, LyP;->a:LyI;

    invoke-static {v0}, LyI;->a(LyI;)LagZ;

    move-result-object v0

    invoke-interface {v0, v1}, LagZ;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)V

    .line 185
    invoke-interface {p1, v1}, LaGM;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 188
    :cond_0
    return-object v0
.end method

.method public bridge synthetic a(LaGM;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 169
    invoke-virtual {p0, p1}, LyP;->a(LaGM;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 3

    .prologue
    .line 193
    if-eqz p1, :cond_0

    .line 194
    iget-object v0, p0, LyP;->a:LyI;

    invoke-static {v0}, LyI;->a(LyI;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LyP;->a:LyI;

    invoke-static {v1}, LyI;->a(LyI;)LaGM;

    move-result-object v1

    iget-object v2, p0, LyP;->a:LyI;

    .line 195
    invoke-static {v2}, LyI;->a(LyI;)LtK;

    move-result-object v2

    .line 194
    invoke-static {v0, v1, v2, p1}, Lala;->a(Landroid/content/Context;LaGM;LtK;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    .line 196
    if-eqz v0, :cond_0

    .line 197
    iget-object v1, p0, LyP;->a:LyI;

    invoke-static {v1}, LyI;->a(LyI;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 204
    :goto_0
    return-void

    .line 203
    :cond_0
    iget-object v0, p0, LyP;->a:LyI;

    invoke-static {v0}, LyI;->a(LyI;)Landroid/content/Context;

    move-result-object v0

    sget v1, Lxi;->error_opening_document:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 169
    check-cast p1, Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {p0, p1}, LyP;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    return-void
.end method
