.class LWj;
.super LEC;
.source "CommonPreferencesInstaller.java"


# instance fields
.field final synthetic a:LWa;


# direct methods
.method constructor <init>(LWa;)V
    .locals 0

    .prologue
    .line 309
    iput-object p1, p0, LWj;->a:LWa;

    invoke-direct {p0}, LEC;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 339
    iget-object v0, p0, LWj;->a:LWa;

    invoke-static {v0}, LWa;->b(LWa;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    .line 340
    iget-object v1, p0, LWj;->a:LWa;

    invoke-static {v1}, LWa;->b(LWa;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 341
    return-void

    .line 340
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(LWj;)V
    .locals 0

    .prologue
    .line 309
    invoke-direct {p0}, LWj;->a()V

    return-void
.end method

.method private b()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 345
    iget-object v0, p0, LWj;->a:LWa;

    iget-object v0, v0, LWa;->a:LaGg;

    invoke-interface {v0}, LaGg;->a()V

    .line 347
    :try_start_0
    iget-object v0, p0, LWj;->a:LWa;

    iget-object v0, v0, LWa;->a:LSF;

    invoke-interface {v0}, LSF;->a()[Landroid/accounts/Account;

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_3

    aget-object v0, v3, v2

    .line 348
    iget-object v5, p0, LWj;->a:LWa;

    iget-object v5, v5, LWa;->a:LaGg;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 349
    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    invoke-interface {v5, v0}, LaGg;->a(LaFO;)LaFM;

    move-result-object v0

    .line 351
    iget-object v5, p0, LWj;->a:LWa;

    iget-object v5, v5, LWa;->a:LaGg;

    invoke-static {}, LaER;->a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v6

    invoke-interface {v5, v0, v6}, LaGg;->a(LaFM;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 352
    iget-object v6, p0, LWj;->a:LWa;

    iget-object v6, v6, LWa;->a:LaGg;

    invoke-interface {v6, v0}, LaGg;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;

    move-result-object v6

    .line 353
    if-eqz v6, :cond_0

    .line 354
    if-eqz v6, :cond_1

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, LbiT;->b(Z)V

    .line 355
    invoke-virtual {v6}, LaGb;->a()LaGc;

    move-result-object v0

    const-wide/16 v6, -0x1

    sget-object v8, LacY;->a:LacY;

    .line 356
    invoke-virtual {v0, v6, v7, v8}, LaGc;->a(JLacY;)LaGc;

    move-result-object v0

    .line 357
    invoke-virtual {v0}, LaGc;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 363
    :catchall_0
    move-exception v0

    iget-object v1, p0, LWj;->a:LWa;

    iget-object v1, v1, LWa;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    throw v0

    :cond_1
    move v0, v1

    .line 354
    goto :goto_2

    .line 347
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 361
    :cond_3
    :try_start_1
    iget-object v0, p0, LWj;->a:LWa;

    iget-object v0, v0, LWa;->a:LaGg;

    invoke-interface {v0}, LaGg;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 363
    iget-object v0, p0, LWj;->a:LWa;

    iget-object v0, v0, LWa;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V

    .line 365
    iget-object v0, p0, LWj;->a:LWa;

    iget-object v0, v0, LWa;->a:LadL;

    invoke-interface {v0}, LadL;->a()V

    .line 366
    return-void
.end method

.method static synthetic b(LWj;)V
    .locals 0

    .prologue
    .line 309
    invoke-direct {p0}, LWj;->b()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 312
    .line 313
    invoke-static {p1}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v0

    const v1, 0x1080045

    .line 314
    invoke-virtual {v0, v1}, LEU;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lxi;->pin_encryption_title:I

    .line 315
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lxi;->pin_encryption_message:I

    .line 316
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lxi;->pin_encryption_continue:I

    new-instance v2, LWk;

    invoke-direct {v2, p0}, LWk;-><init>(LWj;)V

    .line 317
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    const/4 v2, 0x0

    .line 328
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 330
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;Landroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 335
    invoke-direct {p0}, LWj;->a()V

    .line 336
    return-void
.end method
