.class public LHN;
.super LHq;
.source "IncomingPhotosEntryViewHolder.java"


# direct methods
.method public constructor <init>(Lapd;Lapc;Landroid/view/View;LDk;)V
    .locals 2

    .prologue
    .line 17
    invoke-direct {p0, p1, p2, p3, p4}, LHq;-><init>(Lapd;Lapc;Landroid/view/View;LDk;)V

    .line 18
    iget-object v0, p0, LHN;->a:Lcom/google/android/apps/docs/view/FixedAspectRatioFrameLayout;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/FixedAspectRatioFrameLayout;->setAspectRatio(F)V

    .line 19
    return-void
.end method


# virtual methods
.method public c(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 23
    iget-object v2, p0, LHN;->a:Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;->setVisibility(I)V

    .line 24
    iget-object v0, p0, LHN;->a:Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;->setClickable(Z)V

    .line 25
    iget-object v0, p0, LHN;->a:LEd;

    invoke-virtual {v0, v1}, LEd;->c(Z)V

    .line 26
    return-void

    .line 23
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method
