.class public final Laah;
.super Ljava/lang/Object;
.source "CachedAsyncSearchHandler.java"

# interfaces
.implements Laas;


# instance fields
.field private a:Laaf;

.field private final a:Laaj;

.field private final a:Laau;

.field private final a:Lagl;

.field private final a:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Laat;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Laaj;Lagl;Laau;)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, Laat;->a:Laat;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Laah;->a:Ljava/util/concurrent/atomic/AtomicReference;

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Laah;->a:Laaf;

    .line 40
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laaj;

    iput-object v0, p0, Laah;->a:Laaj;

    .line 41
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lagl;

    iput-object v0, p0, Laah;->a:Lagl;

    .line 42
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laau;

    iput-object v0, p0, Laah;->a:Laau;

    .line 43
    return-void
.end method

.method static synthetic a(Laah;Laaf;)Laaf;
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Laah;->a:Laaf;

    return-object p1
.end method

.method static synthetic a(Laah;)Laaj;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Laah;->a:Laaj;

    return-object v0
.end method

.method static synthetic a(Laah;)Lagl;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Laah;->a:Lagl;

    return-object v0
.end method

.method static synthetic a(Laah;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Laah;->a:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method


# virtual methods
.method public a()Laat;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Laah;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laat;

    return-object v0
.end method

.method public a(LaFM;Ljava/lang/String;J)Laay;
    .locals 5

    .prologue
    .line 53
    invoke-virtual {p0}, Laah;->a()V

    .line 55
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    :cond_0
    invoke-static {}, Laay;->a()Laay;

    move-result-object v0

    .line 57
    iget-object v1, p0, Laah;->a:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v2, Laat;->e:Laat;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 119
    :goto_0
    return-object v0

    .line 59
    :cond_1
    iget-object v0, p0, Laah;->a:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, Laat;->b:Laat;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 65
    iget-object v0, p0, Laah;->a:Laaj;

    invoke-interface {v0, p1, p2, p3, p4}, Laaj;->a(LaFM;Ljava/lang/String;J)LaFT;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, LaFT;->c()J

    move-result-wide v2

    .line 68
    new-instance v1, Laai;

    invoke-direct {v1, p0, p1, v0}, Laai;-><init>(Laah;LaFM;LaFT;)V

    .line 115
    invoke-virtual {p1}, LaFM;->a()LaFO;

    move-result-object v0

    .line 116
    iget-object v4, p0, Laah;->a:Laau;

    invoke-interface {v4, v0, p2, v1}, Laau;->a(LaFO;Ljava/lang/String;Laag;)Laaf;

    move-result-object v0

    iput-object v0, p0, Laah;->a:Laaf;

    .line 117
    invoke-static {p2, v2, v3}, Laay;->a(Ljava/lang/String;J)Laay;

    move-result-object v0

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Laah;->a:Laaf;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Laah;->a:Laaf;

    invoke-interface {v0}, Laaf;->a()V

    .line 126
    const/4 v0, 0x0

    iput-object v0, p0, Laah;->a:Laaf;

    .line 128
    :cond_0
    iget-object v0, p0, Laah;->a:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, Laat;->d:Laat;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 129
    return-void
.end method
