.class LbrL;
.super Ljava/lang/Object;
.source "GellyBinder.java"

# interfaces
.implements LbrB;
.implements Lcom/google/inject/Binder;


# instance fields
.field private final a:LbrP;


# direct methods
.method constructor <init>(LbrP;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbrP;

    iput-object v0, p0, LbrL;->a:LbrP;

    .line 41
    return-void
.end method


# virtual methods
.method public a(Lbuv;)LbuE;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbuv",
            "<TT;>;)",
            "LbuE",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 122
    new-instance v0, LbwB;

    invoke-direct {v0, p0, p1}, LbwB;-><init>(Ljava/lang/Object;Lbuv;)V

    .line 123
    iget-object v1, p0, LbrL;->a:LbrP;

    invoke-virtual {v1, v0}, LbrP;->a(LbwB;)V

    .line 124
    invoke-virtual {v0}, LbwB;->a()LbuE;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Class;)LbuE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LbuE",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 129
    invoke-static {p1}, Lbuv;->a(Ljava/lang/Class;)Lbuv;

    move-result-object v0

    invoke-virtual {p0, v0}, LbrL;->a(Lbuv;)LbuE;

    move-result-object v0

    return-object v0
.end method

.method public a(LbuP;)LbuQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbuP",
            "<TT;>;)",
            "LbuQ",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 65
    new-instance v0, LbrK;

    iget-object v1, p0, LbrL;->a:LbrP;

    invoke-direct {v0, p1, v1}, LbrK;-><init>(LbuP;LbrP;)V

    return-object v0
.end method

.method public a(Ljava/lang/Class;)LbuQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LbuQ",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 70
    new-instance v0, LbrK;

    invoke-static {p1}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v1

    iget-object v2, p0, LbrL;->a:LbrP;

    invoke-direct {v0, v1, v2}, LbrK;-><init>(LbuP;LbrP;)V

    return-object v0
.end method

.method public a()LbuR;
    .locals 2

    .prologue
    .line 75
    new-instance v0, LbrM;

    iget-object v1, p0, LbrL;->a:LbrP;

    invoke-direct {v0, v1}, LbrM;-><init>(LbrP;)V

    return-object v0
.end method

.method public a(Lbuv;)LbuT;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbuv",
            "<TT;>;)",
            "LbuT",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 60
    new-instance v0, LbrK;

    iget-object v1, p0, LbrL;->a:LbrP;

    invoke-direct {v0, p1, v1}, LbrK;-><init>(Lbuv;LbrP;)V

    return-object v0
.end method

.method public a(Ljava/lang/Object;)Lcom/google/inject/Binder;
    .locals 0

    .prologue
    .line 185
    return-object p0
.end method

.method public varargs a([Ljava/lang/Class;)Lcom/google/inject/Binder;
    .locals 0

    .prologue
    .line 180
    return-object p0
.end method

.method public a(LbuC;)V
    .locals 0

    .prologue
    .line 134
    invoke-interface {p1, p0}, LbuC;->a(Lcom/google/inject/Binder;)V

    .line 135
    invoke-virtual {p0, p1}, LbrL;->b(LbuC;)V

    .line 136
    return-void
.end method

.method public a(Lbwy;)V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, LbrL;->a:LbrP;

    invoke-virtual {v0, p1}, LbrP;->a(Lbwy;)V

    .line 51
    return-void
.end method

.method public a(Ljava/lang/Class;LbuH;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "LbuH;",
            ")V"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, LbrL;->a:LbrP;

    invoke-virtual {v0, p1, p2}, LbrP;->a(Ljava/lang/Class;LbuH;)V

    .line 91
    return-void
.end method

.method public varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 55
    iget-object v0, p0, LbrL;->a:LbrP;

    new-instance v1, Lbwy;

    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lbwy;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LbrP;->a(Lbwy;)V

    .line 56
    return-void
.end method

.method public b(LbuC;)V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, LbrL;->a:LbrP;

    invoke-virtual {v0, p1}, LbrP;->a(Ljava/lang/Object;)V

    .line 141
    return-void
.end method
