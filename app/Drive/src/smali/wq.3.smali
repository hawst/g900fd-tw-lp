.class public Lwq;
.super Ljava/lang/Object;
.source "NavigationPathSerializer.java"


# direct methods
.method public static a(Landroid/os/Bundle;)LbmF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            ")",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    const-string v0, "navigationPath"

    .line 34
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 35
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/os/Bundle;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    const-string v0, "navigationPath"

    invoke-static {p1}, LbnG;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 47
    return-void
.end method
