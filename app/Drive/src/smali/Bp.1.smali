.class abstract enum LBp;
.super Ljava/lang/Enum;
.source "DocListGroupingAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LBp;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LBp;

.field private static final synthetic a:[LBp;

.field public static final enum b:LBp;

.field public static final enum c:LBp;


# instance fields
.field private final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    new-instance v0, LBq;

    const-string v1, "SECTION_HEADER"

    invoke-direct {v0, v1, v2, v2}, LBq;-><init>(Ljava/lang/String;II)V

    sput-object v0, LBp;->a:LBp;

    .line 47
    new-instance v0, LBr;

    const-string v1, "EMPTY_SECTION_HEADER"

    invoke-direct {v0, v1, v3, v3}, LBr;-><init>(Ljava/lang/String;II)V

    sput-object v0, LBp;->b:LBp;

    .line 54
    new-instance v0, LBs;

    const-string v1, "ENTRY_ROW"

    invoke-direct {v0, v1, v4, v4}, LBs;-><init>(Ljava/lang/String;II)V

    sput-object v0, LBp;->c:LBp;

    .line 39
    const/4 v0, 0x3

    new-array v0, v0, [LBp;

    sget-object v1, LBp;->a:LBp;

    aput-object v1, v0, v2

    sget-object v1, LBp;->b:LBp;

    aput-object v1, v0, v3

    sget-object v1, LBp;->c:LBp;

    aput-object v1, v0, v4

    sput-object v0, LBp;->a:[LBp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 65
    iput p3, p0, LBp;->a:I

    .line 66
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IILBk;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, LBp;-><init>(Ljava/lang/String;II)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LBp;
    .locals 1

    .prologue
    .line 39
    const-class v0, LBp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LBp;

    return-object v0
.end method

.method public static values()[LBp;
    .locals 1

    .prologue
    .line 39
    sget-object v0, LBp;->a:[LBp;

    invoke-virtual {v0}, [LBp;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LBp;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, LBp;->a:I

    return v0
.end method

.method public abstract a(LBj;ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end method
