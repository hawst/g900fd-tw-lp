.class public abstract LaDh;
.super Ljava/lang/Object;

# interfaces
.implements LaCZ;
.implements LaDk;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R::",
        "LaDc;",
        ">",
        "Ljava/lang/Object;",
        "LaCZ",
        "<TR;>;",
        "LaDk",
        "<TR;>;"
    }
.end annotation


# instance fields
.field private volatile a:LaDc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TR;"
        }
    .end annotation
.end field

.field private a:LaDd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaDd",
            "<TR;>;"
        }
    .end annotation
.end field

.field private a:LaDj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaDj",
            "<TR;>;"
        }
    .end annotation
.end field

.field private a:LaRM;

.field private final a:Ljava/lang/Object;

.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LaDa;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/concurrent/CountDownLatch;

.field private volatile a:Z

.field private b:Z

.field private c:Z


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LaDh;->a:Ljava/lang/Object;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, LaDh;->a:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LaDh;->a:Ljava/util/ArrayList;

    return-void
.end method

.method private a()LaDc;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TR;"
        }
    .end annotation

    iget-object v1, p0, LaDh;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, LaDh;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Result has already been consumed."

    invoke-static {v0, v2}, LaSc;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0}, LaDh;->a()Z

    move-result v0

    const-string v2, "Result is not ready."

    invoke-static {v0, v2}, LaSc;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, LaDh;->a:LaDc;

    invoke-virtual {p0}, LaDh;->b()V

    monitor-exit v1

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic a(LaDh;)V
    .locals 0

    invoke-direct {p0}, LaDh;->c()V

    return-void
.end method

.method private b(LaDc;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    iput-object p1, p0, LaDh;->a:LaDc;

    const/4 v0, 0x0

    iput-object v0, p0, LaDh;->a:LaRM;

    iget-object v0, p0, LaDh;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    iget-object v0, p0, LaDh;->a:LaDc;

    invoke-interface {v0}, LaDc;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    iget-object v0, p0, LaDh;->a:LaDd;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaDh;->a:LaDj;

    invoke-virtual {v0}, LaDj;->a()V

    iget-boolean v0, p0, LaDh;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LaDh;->a:LaDj;

    iget-object v2, p0, LaDh;->a:LaDd;

    invoke-direct {p0}, LaDh;->a()LaDc;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LaDj;->a(LaDd;LaDc;)V

    :cond_0
    iget-object v0, p0, LaDh;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaDa;

    invoke-interface {v0, v1}, LaDa;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, LaDh;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method private c()V
    .locals 2

    iget-object v1, p0, LaDh;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, LaDh;->a()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/common/api/Status;->d:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, LaDh;->a(Lcom/google/android/gms/common/api/Status;)LaDc;

    move-result-object v0

    invoke-virtual {p0, v0}, LaDh;->a(LaDc;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, LaDh;->c:Z

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method protected abstract a(Lcom/google/android/gms/common/api/Status;)LaDc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/Status;",
            ")TR;"
        }
    .end annotation
.end method

.method public a()V
    .locals 2

    iget-object v1, p0, LaDh;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, LaDh;->b:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LaDh;->a:Z

    if-eqz v0, :cond_1

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LaDh;->a:LaRM;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    :try_start_1
    iget-object v0, p0, LaDh;->a:LaRM;

    invoke-interface {v0}, LaRM;->a()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    :goto_1
    :try_start_2
    iget-object v0, p0, LaDh;->a:LaDc;

    invoke-static {v0}, LaDg;->a(LaDc;)V

    const/4 v0, 0x0

    iput-object v0, p0, LaDh;->a:LaDd;

    const/4 v0, 0x1

    iput-boolean v0, p0, LaDh;->b:Z

    sget-object v0, Lcom/google/android/gms/common/api/Status;->e:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, LaDh;->a(Lcom/google/android/gms/common/api/Status;)LaDc;

    move-result-object v0

    invoke-direct {p0, v0}, LaDh;->b(LaDc;)V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public final a(LaDc;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, LaDh;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-boolean v2, p0, LaDh;->c:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, LaDh;->b:Z

    if-eqz v2, :cond_1

    :cond_0
    invoke-static {p1}, LaDg;->a(LaDc;)V

    monitor-exit v3

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, LaDh;->a()Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v0

    :goto_1
    const-string v4, "Results have already been set"

    invoke-static {v2, v4}, LaSc;->a(ZLjava/lang/Object;)V

    iget-boolean v2, p0, LaDh;->a:Z

    if-nez v2, :cond_3

    :goto_2
    const-string v1, "Result has already been consumed"

    invoke-static {v0, v1}, LaSc;->a(ZLjava/lang/Object;)V

    invoke-direct {p0, p1}, LaDh;->b(LaDc;)V

    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final a(LaDd;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaDd",
            "<TR;>;)V"
        }
    .end annotation

    iget-boolean v0, p0, LaDh;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Result has already been consumed."

    invoke-static {v0, v1}, LaSc;->a(ZLjava/lang/Object;)V

    iget-object v1, p0, LaDh;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, LaDh;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    monitor-exit v1

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, LaDh;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LaDh;->a:LaDj;

    invoke-direct {p0}, LaDh;->a()LaDc;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, LaDj;->a(LaDd;LaDc;)V

    :goto_2
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    iput-object p1, p0, LaDh;->a:LaDd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method protected a(LaDj;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaDj",
            "<TR;>;)V"
        }
    .end annotation

    iput-object p1, p0, LaDh;->a:LaDj;

    return-void
.end method

.method protected final a(LaRM;)V
    .locals 2

    iget-object v1, p0, LaDh;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, LaDh;->a:LaRM;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, LaDc;

    invoke-virtual {p0, p1}, LaDh;->a(LaDc;)V

    return-void
.end method

.method public final a()Z
    .locals 4

    iget-object v0, p0, LaDh;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, LaDh;->a:Z

    iput-object v1, p0, LaDh;->a:LaDc;

    iput-object v1, p0, LaDh;->a:LaDd;

    return-void
.end method

.method public b()Z
    .locals 2

    iget-object v1, p0, LaDh;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, LaDh;->b:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
