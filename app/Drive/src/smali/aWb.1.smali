.class public LaWb;
.super LaLa;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaLa",
        "<",
        "LaWc;",
        ">;"
    }
.end annotation


# instance fields
.field protected a:LaLq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaLq",
            "<",
            "LaWc;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Landroid/content/Context;

.field private final a:Landroid/view/ViewGroup;

.field private final a:Lcom/google/android/gms/maps/StreetViewPanoramaOptions;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Landroid/content/Context;Lcom/google/android/gms/maps/StreetViewPanoramaOptions;)V
    .locals 0

    invoke-direct {p0}, LaLa;-><init>()V

    iput-object p1, p0, LaWb;->a:Landroid/view/ViewGroup;

    iput-object p2, p0, LaWb;->a:Landroid/content/Context;

    iput-object p3, p0, LaWb;->a:Lcom/google/android/gms/maps/StreetViewPanoramaOptions;

    return-void
.end method


# virtual methods
.method protected a(LaLq;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaLq",
            "<",
            "LaWc;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, LaWb;->a:LaLq;

    invoke-virtual {p0}, LaWb;->h()V

    return-void
.end method

.method public h()V
    .locals 4

    iget-object v0, p0, LaWb;->a:LaLq;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LaWb;->a()LaKZ;

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, LaWb;->a:Landroid/content/Context;

    invoke-static {v0}, LaXQ;->a(Landroid/content/Context;)LaWQ;

    move-result-object v0

    iget-object v1, p0, LaWb;->a:Landroid/content/Context;

    invoke-static {v1}, LaLp;->a(Ljava/lang/Object;)LaLm;

    move-result-object v1

    iget-object v2, p0, LaWb;->a:Lcom/google/android/gms/maps/StreetViewPanoramaOptions;

    invoke-interface {v0, v1, v2}, LaWQ;->a(LaLm;Lcom/google/android/gms/maps/StreetViewPanoramaOptions;)LaWG;

    move-result-object v0

    iget-object v1, p0, LaWb;->a:LaLq;

    new-instance v2, LaWc;

    iget-object v3, p0, LaWb;->a:Landroid/view/ViewGroup;

    invoke-direct {v2, v3, v0}, LaWc;-><init>(Landroid/view/ViewGroup;LaWG;)V

    invoke-interface {v1, v2}, LaLq;->a(LaKZ;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LaCH; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, LaYd;

    invoke-direct {v1, v0}, LaYd;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :catch_1
    move-exception v0

    goto :goto_0
.end method
