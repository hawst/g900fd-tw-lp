.class Lxu;
.super Ljava/lang/Object;
.source "Interpolators.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field private final a:[F

.field private final b:[F


# direct methods
.method private constructor <init>([F[F)V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    iput-object p1, p0, Lxu;->a:[F

    .line 101
    iput-object p2, p0, Lxu;->b:[F

    .line 102
    return-void
.end method

.method synthetic constructor <init>([F[FLxt;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Lxu;-><init>([F[F)V

    return-void
.end method

.method private a(F)F
    .locals 6

    .prologue
    .line 117
    const/4 v2, 0x0

    .line 118
    iget-object v0, p0, Lxu;->a:[F

    array-length v0, v0

    add-int/lit8 v1, v0, -0x1

    .line 120
    :goto_0
    sub-int v0, v1, v2

    const/4 v3, 0x1

    if-le v0, v3, :cond_1

    .line 121
    add-int v0, v2, v1

    div-int/lit8 v0, v0, 0x2

    .line 122
    iget-object v3, p0, Lxu;->a:[F

    aget v3, v3, v0

    cmpg-float v3, p1, v3

    if-gez v3, :cond_0

    move v1, v2

    :goto_1
    move v2, v1

    move v1, v0

    .line 127
    goto :goto_0

    :cond_0
    move v5, v1

    move v1, v0

    move v0, v5

    .line 125
    goto :goto_1

    .line 129
    :cond_1
    iget-object v0, p0, Lxu;->a:[F

    aget v0, v0, v1

    iget-object v3, p0, Lxu;->a:[F

    aget v3, v3, v2

    sub-float/2addr v0, v3

    .line 130
    const/4 v3, 0x0

    cmpl-float v3, v0, v3

    if-nez v3, :cond_2

    .line 132
    iget-object v0, p0, Lxu;->b:[F

    aget v0, v0, v2

    .line 135
    :goto_2
    return v0

    :cond_2
    iget-object v3, p0, Lxu;->b:[F

    aget v3, v3, v2

    iget-object v4, p0, Lxu;->a:[F

    aget v4, v4, v2

    sub-float v4, p1, v4

    div-float v0, v4, v0

    iget-object v4, p0, Lxu;->b:[F

    aget v1, v4, v1

    iget-object v4, p0, Lxu;->b:[F

    aget v2, v4, v2

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    add-float/2addr v0, v3

    goto :goto_2
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 3

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 107
    cmpg-float v2, p1, v0

    if-gtz v2, :cond_0

    .line 112
    :goto_0
    return v0

    .line 109
    :cond_0
    cmpl-float v0, p1, v1

    if-ltz v0, :cond_1

    move v0, v1

    .line 110
    goto :goto_0

    .line 112
    :cond_1
    invoke-direct {p0, p1}, Lxu;->a(F)F

    move-result v0

    goto :goto_0
.end method
