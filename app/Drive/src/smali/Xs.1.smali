.class public LXs;
.super LdN;
.source "PunchCompleteSvgViewer.java"


# instance fields
.field final synthetic a:LXq;


# direct methods
.method public constructor <init>(LXq;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, LXs;->a:LXq;

    invoke-direct {p0}, LdN;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, LXs;->a:LXq;

    iget-object v0, v0, LXq;->a:LXD;

    invoke-virtual {v0}, LXD;->c()I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 10

    .prologue
    const/4 v4, -0x1

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 93
    iget-object v0, p0, LXs;->a:LXq;

    iget-object v0, v0, LXq;->a:Ljava/lang/String;

    const-string v1, "in instantiateItem slidePosition=%s"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 98
    invoke-static {}, LakQ;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LXs;->a:LXq;

    invoke-static {v0}, LXq;->a(LXq;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    :cond_0
    iget-object v0, p0, LXs;->a:LXq;

    iget-object v1, v0, LXq;->a:LXD;

    iget-object v0, p0, LXs;->a:LXq;

    iget-object v2, v0, LXq;->a:LTO;

    iget-object v0, p0, LXs;->a:LXq;

    iget-object v3, v0, LXq;->a:LYO;

    iget-object v0, p0, LXs;->a:LXq;

    iget-object v4, v0, LXq;->a:LXR;

    iget-object v0, p0, LXs;->a:LXq;

    iget-object v5, v0, LXq;->a:LqK;

    iget-object v0, p0, LXs;->a:LXq;

    iget-object v6, v0, LXq;->a:Laja;

    iget-object v7, p0, LXs;->a:LXq;

    move-object v0, p1

    .line 100
    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Landroid/view/ViewGroup;LXD;LTO;LYO;LXR;LqK;Lbxw;LXQ;)Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    move-result-object v0

    .line 109
    iget-object v1, p0, LXs;->a:LXq;

    iget-object v1, v1, LXq;->a:Ljava/lang/String;

    const-string v2, "has created=%s"

    new-array v3, v8, [Ljava/lang/Object;

    aput-object v0, v3, v9

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 118
    :goto_0
    sget-object v1, LXP;->b:LXP;

    invoke-virtual {v0, p2, v1}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(ILXP;)V

    .line 119
    iget-object v1, p0, LXs;->a:LXq;

    invoke-static {v1}, LXq;->a(LXq;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    .line 120
    invoke-static {v1}, LbiT;->b(Z)V

    .line 122
    return-object v0

    .line 111
    :cond_1
    iget-object v0, p0, LXs;->a:LXq;

    invoke-static {v0}, LXq;->a(LXq;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    .line 112
    iget-object v1, p0, LXs;->a:LXq;

    iget-object v1, v1, LXq;->a:Ljava/lang/String;

    const-string v2, "in instantiateItem reusing=%s"

    new-array v3, v8, [Ljava/lang/Object;

    aput-object v0, v3, v9

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 113
    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_2

    move v1, v8

    :goto_1
    invoke-static {v1}, LbiT;->b(Z)V

    .line 114
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_2
    move v1, v9

    .line 113
    goto :goto_1
.end method

.method public a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 77
    instance-of v0, p3, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    invoke-static {v0}, LbiT;->b(Z)V

    .line 78
    check-cast p3, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    .line 79
    iget-object v0, p0, LXs;->a:LXq;

    iget-object v0, v0, LXq;->a:Ljava/lang/String;

    const-string v3, "in destroyItem position=%s for: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    aput-object p3, v4, v1

    invoke-static {v0, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 80
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 81
    iget-object v0, p0, LXs;->a:LXq;

    invoke-static {v0}, LXq;->a(LXq;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 82
    invoke-static {v0}, LbiT;->b(Z)V

    .line 83
    invoke-static {}, LakQ;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    iget-object v0, p0, LXs;->a:LXq;

    invoke-static {v0}, LXq;->a(LXq;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 85
    iget-object v0, p0, LXs;->a:LXq;

    invoke-static {v0}, LXq;->a(LXq;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 86
    iget-object v0, p0, LXs;->a:LXq;

    iget-object v0, v0, LXq;->a:Ljava/lang/String;

    const-string v3, "freeViews size=%s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, LXs;->a:LXq;

    invoke-static {v4}, LXq;->a(LXq;)Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 87
    invoke-virtual {p3}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->c()V

    .line 89
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 84
    goto :goto_0
.end method

.method public a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 72
    check-cast p2, Landroid/view/View;

    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
