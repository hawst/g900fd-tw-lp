.class LakT;
.super Ljava/lang/Object;
.source "DiskCache.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "LakT;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final a:Ljava/io/File;

.field private final a:Z


# direct methods
.method private constructor <init>(Ljava/io/File;Z)V
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, LakT;->a:Ljava/io/File;

    .line 62
    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    iput-wide v0, p0, LakT;->a:J

    .line 63
    iput-boolean p2, p0, LakT;->a:Z

    .line 64
    return-void
.end method

.method public static a(Ljava/io/File;Z)LakT;
    .locals 1

    .prologue
    .line 67
    new-instance v0, LakT;

    invoke-direct {v0, p0, p1}, LakT;-><init>(Ljava/io/File;Z)V

    return-object v0
.end method


# virtual methods
.method public a(LakT;)I
    .locals 4

    .prologue
    .line 81
    iget-boolean v0, p0, LakT;->a:Z

    iget-boolean v1, p1, LakT;->a:Z

    if-eq v0, v1, :cond_1

    .line 82
    iget-boolean v0, p0, LakT;->a:Z

    iget-boolean v1, p1, LakT;->a:Z

    invoke-static {v0, v1}, Lbsw;->a(ZZ)I

    move-result v0

    .line 86
    :cond_0
    :goto_0
    return v0

    .line 85
    :cond_1
    iget-wide v0, p0, LakT;->a:J

    iget-wide v2, p1, LakT;->a:J

    invoke-static {v0, v1, v2, v3}, LbsA;->a(JJ)I

    move-result v0

    .line 86
    if-nez v0, :cond_0

    iget-object v0, p0, LakT;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, LakT;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public a()Ljava/io/File;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, LakT;->a:Ljava/io/File;

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, LakT;->a:Z

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 54
    check-cast p1, LakT;

    invoke-virtual {p0, p1}, LakT;->a(LakT;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 92
    instance-of v1, p1, LakT;

    if-nez v1, :cond_1

    .line 97
    :cond_0
    :goto_0
    return v0

    .line 95
    :cond_1
    check-cast p1, LakT;

    .line 96
    iget-boolean v1, p0, LakT;->a:Z

    iget-boolean v2, p1, LakT;->a:Z

    if-ne v1, v2, :cond_0

    iget-wide v2, p0, LakT;->a:J

    iget-wide v4, p1, LakT;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-object v1, p0, LakT;->a:Ljava/io/File;

    .line 97
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, LakT;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 102
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, LakT;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, LakT;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LakT;->a:Ljava/io/File;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
