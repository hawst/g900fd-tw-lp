.class public final LTz;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LTO;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LTw;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LTQ;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lajw",
            "<",
            "LTO;",
            ">;>;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laja",
            "<",
            "LTO;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 41
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 42
    iput-object p1, p0, LTz;->a:LbrA;

    .line 43
    const-class v0, LTO;

    sget-object v1, LQH;->f:Ljava/lang/Class;

    .line 44
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 43
    invoke-static {v0, v3}, LTz;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LTz;->a:Lbsk;

    .line 46
    const-class v0, LTw;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LTz;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LTz;->b:Lbsk;

    .line 49
    const-class v0, LTQ;

    invoke-static {v0, v3}, LTz;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LTz;->c:Lbsk;

    .line 52
    const-class v0, Lajw;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LTO;

    aput-object v2, v1, v4

    .line 53
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->f:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 52
    invoke-static {v0, v3}, LTz;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LTz;->d:Lbsk;

    .line 55
    const-class v0, Laja;

    new-array v1, v5, [Ljava/lang/reflect/Type;

    const-class v2, LTO;

    aput-object v2, v1, v4

    .line 56
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->f:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 55
    invoke-static {v0, v3}, LTz;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LTz;->e:Lbsk;

    .line 58
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 124
    sparse-switch p1, :sswitch_data_0

    .line 154
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 126
    :sswitch_0
    new-instance v3, LTw;

    iget-object v0, p0, LTz;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->i:Lbsk;

    .line 129
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LTz;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTk;

    iget-object v1, v1, LTk;->i:Lbsk;

    .line 127
    invoke-static {v0, v1}, LTz;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTO;

    iget-object v1, p0, LTz;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->t:Lbsk;

    .line 135
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LTz;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->t:Lbsk;

    .line 133
    invoke-static {v1, v2}, LTz;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laja;

    iget-object v2, p0, LTz;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 141
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LTz;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LQH;

    iget-object v4, v4, LQH;->d:Lbsk;

    .line 139
    invoke-static {v2, v4}, LTz;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LQr;

    invoke-direct {v3, v0, v1, v2}, LTw;-><init>(LTO;Laja;LQr;)V

    move-object v0, v3

    .line 152
    :goto_0
    return-object v0

    .line 148
    :sswitch_1
    new-instance v0, LTQ;

    invoke-direct {v0}, LTQ;-><init>()V

    .line 150
    iget-object v1, p0, LTz;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LTz;

    .line 151
    invoke-virtual {v1, v0}, LTz;->a(LTQ;)V

    goto :goto_0

    .line 124
    :sswitch_data_0
    .sparse-switch
        0x97 -> :sswitch_0
        0x9c -> :sswitch_1
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 181
    packed-switch p2, :pswitch_data_0

    .line 209
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 183
    :pswitch_0
    check-cast p1, LTv;

    .line 185
    iget-object v0, p0, LTz;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTz;

    iget-object v0, v0, LTz;->e:Lbsk;

    .line 188
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    .line 185
    invoke-virtual {p1, v0}, LTv;->getLazy1(Laja;)Lajw;

    move-result-object v0

    .line 194
    :goto_0
    return-object v0

    .line 192
    :pswitch_1
    check-cast p1, LTv;

    .line 194
    iget-object v0, p0, LTz;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTz;

    iget-object v2, v0, LTz;->a:Lbsk;

    iget-object v0, p0, LTz;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->a:Lbsk;

    .line 201
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiU;

    iget-object v1, p0, LTz;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->c:Lbsk;

    .line 205
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaiW;

    .line 194
    invoke-virtual {p1, v2, v0, v1}, LTv;->get1(Lbxw;LaiU;LaiW;)Laja;

    move-result-object v0

    goto :goto_0

    .line 181
    :pswitch_data_0
    .packed-switch 0x2cc
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 94
    const-class v0, LTQ;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x6e

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LTz;->a(LbuP;LbuB;)V

    .line 97
    const-class v0, LTu;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x23

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LTz;->a(LbuP;LbuB;)V

    .line 100
    const-class v0, LTO;

    sget-object v1, LQH;->f:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LTz;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LTz;->a(Lbuv;Lbsk;)V

    .line 101
    const-class v0, LTw;

    iget-object v1, p0, LTz;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LTz;->a(Ljava/lang/Class;Lbsk;)V

    .line 102
    const-class v0, LTQ;

    iget-object v1, p0, LTz;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LTz;->a(Ljava/lang/Class;Lbsk;)V

    .line 103
    const-class v0, Lajw;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LTO;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->f:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LTz;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LTz;->a(Lbuv;Lbsk;)V

    .line 104
    const-class v0, Laja;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LTO;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    sget-object v1, LQH;->f:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LTz;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LTz;->a(Lbuv;Lbsk;)V

    .line 105
    iget-object v0, p0, LTz;->a:Lbsk;

    iget-object v1, p0, LTz;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LNA;

    iget-object v1, v1, LNA;->a:Lbsk;

    invoke-static {v1}, LTz;->a(Lbxw;)LbuE;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 108
    iget-object v0, p0, LTz;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x97

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 110
    iget-object v0, p0, LTz;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x9c

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 112
    iget-object v0, p0, LTz;->d:Lbsk;

    const-class v1, LTv;

    const/16 v2, 0x2cc

    invoke-virtual {p0, v1, v2}, LTz;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 114
    iget-object v0, p0, LTz;->e:Lbsk;

    const-class v1, LTv;

    const/16 v2, 0x2cd

    invoke-virtual {p0, v1, v2}, LTz;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 116
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 161
    sparse-switch p1, :sswitch_data_0

    .line 175
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163
    :sswitch_0
    check-cast p2, LTQ;

    .line 165
    iget-object v0, p0, LTz;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTz;

    .line 166
    invoke-virtual {v0, p2}, LTz;->a(LTQ;)V

    .line 177
    :goto_0
    return-void

    .line 169
    :sswitch_1
    check-cast p2, LTu;

    .line 171
    iget-object v0, p0, LTz;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTz;

    .line 172
    invoke-virtual {v0, p2}, LTz;->a(LTu;)V

    goto :goto_0

    .line 161
    :sswitch_data_0
    .sparse-switch
        0x23 -> :sswitch_1
        0x6e -> :sswitch_0
    .end sparse-switch
.end method

.method public a(LTQ;)V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, LTz;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->b:Lbsk;

    .line 67
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LTz;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->b:Lbsk;

    .line 65
    invoke-static {v0, v1}, LTz;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p1, LTQ;->a:Landroid/content/Context;

    .line 71
    iget-object v0, p0, LTz;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LbiH;

    iget-object v0, v0, LbiH;->aT:Lbsk;

    .line 74
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LTz;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LbiH;

    iget-object v1, v1, LbiH;->aT:Lbsk;

    .line 72
    invoke-static {v0, v1}, LTz;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbiP;

    iput-object v0, p1, LTQ;->a:LbiP;

    .line 78
    return-void
.end method

.method public a(LTu;)V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, LTz;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->b:Lbsk;

    .line 85
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LTz;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->b:Lbsk;

    .line 83
    invoke-static {v0, v1}, LTz;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p1, LTu;->a:Landroid/content/Context;

    .line 89
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 120
    return-void
.end method
