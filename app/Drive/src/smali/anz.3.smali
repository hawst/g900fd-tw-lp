.class Lanz;
.super Ljava/lang/Object;
.source "AbstractStoringFetcher.java"


# instance fields
.field private final a:Lakh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lakh",
            "<TV;>;"
        }
    .end annotation
.end field

.field private a:LanB;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lanv",
            "<TK;TC;TV;>.anz.anB<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic a:Lanv;

.field private final a:Laou;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laou",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TC;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation
.end field

.field private a:Z

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lanv",
            "<TK;TC;TV;>.anz.anB<TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lanv;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;)V"
        }
    .end annotation

    .prologue
    .line 97
    iput-object p1, p0, Lanz;->a:Lanv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lanz;->a:Ljava/util/Set;

    .line 87
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lanz;->b:Ljava/util/Set;

    .line 89
    iget-object v0, p0, Lanz;->a:Lanv;

    .line 90
    invoke-static {v0}, Lanv;->a(Lanv;)Laoy;

    move-result-object v0

    invoke-static {v0}, Laou;->a(Laoy;)Laou;

    move-result-object v0

    iput-object v0, p0, Lanz;->a:Laou;

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lanz;->a:LanB;

    .line 94
    const/4 v0, 0x0

    iput-boolean v0, p0, Lanz;->a:Z

    .line 98
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lanz;->a:Ljava/lang/Object;

    .line 99
    new-instance v0, Lakh;

    invoke-direct {v0}, Lakh;-><init>()V

    iput-object v0, p0, Lanz;->a:Lakh;

    .line 100
    iget-object v0, p0, Lanz;->a:Laou;

    iget-object v1, p0, Lanz;->a:Lakh;

    invoke-virtual {v0, v1}, Laou;->b(LbsU;)V

    .line 102
    new-instance v0, LanA;

    invoke-direct {v0, p0, p1}, LanA;-><init>(Lanz;Lanv;)V

    .line 114
    iget-object v1, p0, Lanz;->a:Lakh;

    invoke-static {v1, v0}, LbsK;->a(LbsU;LbsJ;)V

    .line 115
    return-void
.end method

.method private a(LanB;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lanv",
            "<TK;TC;TV;>.anz.anB<*>;Z)V"
        }
    .end annotation

    .prologue
    .line 155
    iget-object v1, p0, Lanz;->a:Lanv;

    monitor-enter v1

    .line 156
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 157
    :try_start_1
    iget-object v0, p0, Lanz;->a:LanB;

    if-ne p1, v0, :cond_2

    .line 158
    const/4 v0, 0x0

    iput-object v0, p0, Lanz;->a:LanB;

    .line 162
    :goto_0
    iget-object v0, p0, Lanz;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lanz;->a:LanB;

    if-nez v0, :cond_0

    .line 163
    const/4 v0, 0x1

    iput-boolean v0, p0, Lanz;->a:Z

    .line 164
    iget-object v0, p0, Lanz;->a:Lanv;

    iget-object v2, p0, Lanz;->a:Ljava/lang/Object;

    invoke-static {v0, v2}, Lanv;->a(Lanv;Ljava/lang/Object;)V

    .line 166
    :cond_0
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 167
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 168
    iget-boolean v0, p0, Lanz;->a:Z

    if-eqz v0, :cond_1

    .line 169
    iget-object v0, p0, Lanz;->a:Lakh;

    invoke-virtual {v0, p2}, Lakh;->cancel(Z)Z

    .line 171
    :cond_1
    return-void

    .line 160
    :cond_2
    :try_start_3
    iget-object v0, p0, Lanz;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 166
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    .line 167
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method static synthetic a(Lanz;LanB;Z)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lanz;->a(LanB;Z)V

    return-void
.end method

.method static synthetic a(Lanz;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lanz;->b(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lanz;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lanz;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method private a(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 180
    iget-object v2, p0, Lanz;->a:Lanv;

    invoke-virtual {v2, p1}, Lanv;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 181
    iget-object v3, p0, Lanz;->a:Ljava/lang/Object;

    invoke-virtual {v3, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v2}, LbiT;->a(Z)V

    .line 183
    monitor-enter p0

    .line 184
    :try_start_0
    iget-boolean v2, p0, Lanz;->a:Z

    if-nez v2, :cond_1

    move v2, v0

    :goto_0
    invoke-static {v2}, LbiT;->b(Z)V

    .line 185
    iget-object v2, p0, Lanz;->a:Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 187
    iget-object v1, p0, Lanz;->a:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 189
    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 190
    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lanz;->a:Lanv;

    invoke-static {v0}, Lanv;->a(Lanv;)Laoo;

    move-result-object v0

    invoke-interface {v0, p1}, Laoo;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    .line 192
    iget-object v1, p0, Lanz;->a:Lakh;

    invoke-virtual {v1, v0}, Lakh;->a(LbsU;)Z

    .line 193
    iget-object v1, p0, Lanz;->a:Laou;

    invoke-virtual {v1, v0}, Laou;->a(LbsU;)V

    .line 195
    :cond_0
    return-void

    :cond_1
    move v2, v1

    .line 184
    goto :goto_0

    .line 189
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 202
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    iget-object v0, p0, Lanz;->a:Lanv;

    iget-object v1, p0, Lanz;->a:Ljava/lang/Object;

    invoke-static {v0, v1}, Lanv;->a(Lanv;Ljava/lang/Object;)V

    .line 207
    monitor-enter p0

    .line 208
    :try_start_0
    iget-object v0, p0, Lanz;->b:Ljava/util/Set;

    invoke-static {v0}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v0

    .line 209
    iget-object v1, p0, Lanz;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 210
    const/4 v1, 0x1

    iput-boolean v1, p0, Lanz;->a:Z

    .line 211
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LanB;

    .line 214
    invoke-virtual {v0, p1}, LanB;->a(Ljava/lang/Throwable;)Z

    goto :goto_0

    .line 211
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 216
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/Object;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 224
    iget-object v1, p0, Lanz;->a:Lanv;

    iget-object v2, p0, Lanz;->a:Ljava/lang/Object;

    invoke-static {v1, v2}, Lanv;->a(Lanv;Ljava/lang/Object;)V

    .line 228
    monitor-enter p0

    .line 229
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lanz;->a:Z

    .line 230
    iget-object v1, p0, Lanz;->b:Ljava/util/Set;

    invoke-static {v1}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v2

    .line 231
    iget-object v1, p0, Lanz;->a:LanB;

    .line 232
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234
    if-eqz v1, :cond_0

    .line 235
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, LanB;->a(Ljava/lang/Object;)Z

    .line 239
    :cond_0
    :try_start_1
    iget-object v1, p0, Lanz;->a:Lanv;

    iget-object v3, p0, Lanz;->a:Ljava/lang/Object;

    invoke-virtual {v2}, LbmY;->size()I

    move-result v4

    invoke-virtual {v1, v3, p1, v4}, Lanv;->a(Ljava/lang/Object;Ljava/lang/Object;I)LbmF;

    move-result-object v3

    .line 240
    invoke-virtual {v3}, LbmF;->size()I

    move-result v1

    invoke-virtual {v2}, LbmY;->size()I

    move-result v4

    if-eq v1, v4, :cond_1

    .line 241
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Exepected %d values, got %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 242
    invoke-virtual {v2}, LbmY;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v3}, LbmF;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v4, v5

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 253
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 254
    invoke-virtual {v2}, LbmY;->a()Lbqv;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LanB;

    .line 255
    invoke-virtual {v0, v1}, LanB;->a(Ljava/lang/Throwable;)Z

    goto :goto_0

    .line 232
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 246
    :cond_1
    :try_start_3
    invoke-virtual {v2}, LbmY;->a()Lbqv;

    move-result-object v4

    move v1, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LanB;

    .line 247
    invoke-virtual {v3, v1}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v5

    .line 248
    invoke-virtual {v0, v5}, LanB;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 249
    iget-object v0, p0, Lanz;->a:Lanv;

    invoke-static {v0}, Lanv;->a(Lanv;)Laoy;

    move-result-object v0

    invoke-interface {v0, v5}, Laoy;->a(Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 251
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 252
    goto :goto_1

    .line 258
    :cond_3
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)LbsU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "LbsU",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    .line 123
    monitor-enter p0

    .line 124
    :try_start_0
    iget-boolean v0, p0, Lanz;->a:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 125
    iget-object v0, p0, Lanz;->a:LanB;

    if-nez v0, :cond_0

    .line 126
    new-instance v0, LanB;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LanB;-><init>(Lanz;Lanw;)V

    iput-object v0, p0, Lanz;->a:LanB;

    .line 128
    :cond_0
    iget-object v0, p0, Lanz;->a:LanB;

    .line 129
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    invoke-direct {p0, p1}, Lanz;->a(Ljava/lang/Object;)V

    .line 131
    return-object v0

    .line 124
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 129
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b(Ljava/lang/Object;)LbsU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "LbsU",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 139
    .line 140
    monitor-enter p0

    .line 141
    :try_start_0
    iget-boolean v0, p0, Lanz;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 142
    new-instance v0, LanB;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LanB;-><init>(Lanz;Lanw;)V

    .line 143
    iget-object v1, p0, Lanz;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 144
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    invoke-direct {p0, p1}, Lanz;->a(Ljava/lang/Object;)V

    .line 146
    return-object v0

    .line 141
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 144
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
