.class public LaGD;
.super Ljava/util/AbstractSet;
.source "EntrySpecSet.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractSet",
        "<",
        "Lcom/google/android/gms/drive/database/data/EntrySpec;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LaGD;


# instance fields
.field private final a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    new-instance v0, LaGD;

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, LaGD;-><init>(Ljava/util/Collection;)V

    sput-object v0, LaGD;->a:LaGD;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    .line 22
    invoke-static {p1}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v0

    iput-object v0, p0, LaGD;->a:LbmY;

    .line 23
    return-void
.end method

.method private constructor <init>(Ljava/util/Collection;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    .line 26
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    invoke-static {}, LbmY;->a()LbmY;

    move-result-object v0

    iput-object v0, p0, LaGD;->a:LbmY;

    .line 38
    :goto_0
    return-void

    .line 29
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 30
    invoke-static {}, LbmY;->a()Lbna;

    move-result-object v2

    .line 31
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 32
    iget-object v4, v1, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    iget-object v5, v0, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-virtual {v4, v5}, LaFO;->equals(Ljava/lang/Object;)Z

    move-result v4

    const-string v5, "Account mismatch: %s vs. %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    const/4 v7, 0x1

    aput-object v1, v6, v7

    .line 33
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 32
    invoke-static {v4, v5}, LbiT;->a(ZLjava/lang/Object;)V

    .line 34
    invoke-virtual {v2, v1}, Lbna;->a(Ljava/lang/Object;)Lbna;

    goto :goto_1

    .line 36
    :cond_1
    invoke-virtual {v2}, Lbna;->a()LbmY;

    move-result-object v0

    iput-object v0, p0, LaGD;->a:LbmY;

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGD;
    .locals 1

    .prologue
    .line 51
    new-instance v0, LaGD;

    invoke-direct {v0, p0}, LaGD;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    return-object v0
.end method

.method public static a(Ljava/util/Collection;)LaGD;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;)",
            "LaGD;"
        }
    .end annotation

    .prologue
    .line 44
    instance-of v0, p0, LaGD;

    if-eqz v0, :cond_0

    .line 45
    check-cast p0, LaGD;

    .line 47
    :goto_0
    return-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LaGD;->a:LaGD;

    :goto_1
    move-object p0, v0

    goto :goto_0

    :cond_1
    new-instance v0, LaGD;

    invoke-direct {v0, p0}, LaGD;-><init>(Ljava/util/Collection;)V

    goto :goto_1
.end method


# virtual methods
.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 66
    instance-of v0, p1, Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-nez v0, :cond_0

    .line 67
    const/4 v0, 0x0

    .line 70
    :goto_0
    return v0

    .line 69
    :cond_0
    check-cast p1, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 70
    iget-object v0, p0, LaGD;->a:LbmY;

    invoke-virtual {v0, p1}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, LaGD;->a:LbmY;

    invoke-virtual {v0}, LbmY;->a()Lbqv;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, LaGD;->a:LbmY;

    invoke-virtual {v0}, LbmY;->size()I

    move-result v0

    return v0
.end method
