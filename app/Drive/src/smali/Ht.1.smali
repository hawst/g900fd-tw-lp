.class public abstract LHt;
.super Ljava/lang/Object;
.source "BaseGridViewBinder.java"

# interfaces
.implements LHK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "LHq;",
        ">",
        "Ljava/lang/Object;",
        "LHK;"
    }
.end annotation


# instance fields
.field private final a:LBf;

.field private final a:LCl;

.field private final a:LDM;

.field private final a:LDg;

.field private final a:LIK;

.field private final a:LIf;

.field private final a:LamF;

.field private final a:Landroid/content/res/Resources;

.field protected final a:Lapc;

.field protected final a:Lapd;

.field protected final a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

.field private final a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private final a:Ljava/lang/String;

.field protected final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LHq;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;LBf;LDU;LamF;LQX;Lapd;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;Lapc;LDg;)V
    .locals 3

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    .line 50
    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LHt;->a:Ljava/util/Set;

    .line 78
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBf;

    iput-object v0, p0, LHt;->a:LBf;

    .line 79
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LamF;

    iput-object v0, p0, LHt;->a:LamF;

    .line 81
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LHt;->a:Landroid/content/res/Resources;

    .line 82
    iget-object v0, p0, LHt;->a:Landroid/content/res/Resources;

    sget v1, Lxi;->grid_sync_upload_label_format:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LHt;->a:Ljava/lang/String;

    .line 83
    iget-object v0, p0, LHt;->a:Landroid/content/res/Resources;

    sget v1, Lxi;->grid_sync_download_label_format:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LHt;->b:Ljava/lang/String;

    .line 85
    invoke-virtual {p5}, LQX;->a()LIf;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIf;

    iput-object v0, p0, LHt;->a:LIf;

    .line 86
    invoke-virtual {p5}, LQX;->a()LIK;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIK;

    iput-object v0, p0, LHt;->a:LIK;

    .line 87
    invoke-virtual {p5}, LQX;->a()LCl;

    move-result-object v0

    iput-object v0, p0, LHt;->a:LCl;

    .line 88
    iget-object v0, p0, LHt;->a:LCl;

    invoke-interface {v0}, LCl;->a()LCn;

    move-result-object v0

    iget-object v1, p0, LHt;->a:Ljava/lang/String;

    iget-object v2, p0, LHt;->b:Ljava/lang/String;

    invoke-virtual {p3, p2, v0, v1, v2}, LDU;->a(LBf;LCn;Ljava/lang/String;Ljava/lang/String;)LDM;

    move-result-object v0

    iput-object v0, p0, LHt;->a:LDM;

    .line 90
    iput-object p6, p0, LHt;->a:Lapd;

    .line 91
    iput-object p7, p0, LHt;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    .line 92
    iput-object p8, p0, LHt;->a:Lapc;

    .line 93
    iput-object p9, p0, LHt;->a:LDg;

    .line 94
    invoke-virtual {p5}, LQX;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    iput-object v0, p0, LHt;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 95
    return-void
.end method

.method private a(LaGA;I)LCv;
    .locals 1

    .prologue
    .line 180
    invoke-interface {p1, p2}, LaGA;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private a(LCv;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 151
    iget-object v0, p0, LHt;->a:LIf;

    invoke-virtual {v0, p1}, LIf;->a(LCv;)Ljava/lang/Long;

    move-result-object v0

    .line 152
    if-nez v0, :cond_0

    .line 153
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 156
    :cond_0
    iget-object v1, p0, LHt;->a:LamF;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LamF;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 157
    iget-object v1, p0, LHt;->a:Landroid/content/res/Resources;

    iget-object v2, p0, LHt;->a:LIK;

    invoke-virtual {v2}, LIK;->c()I

    move-result v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(LCv;)Z
    .locals 2

    .prologue
    .line 161
    sget-object v0, LCe;->o:LCe;

    iget-object v1, p0, LHt;->a:LCl;

    invoke-virtual {v0, v1}, LCe;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LHt;->a:LIK;

    sget-object v1, LIK;->h:LIK;

    .line 162
    invoke-virtual {v0, v1}, LIK;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    const/4 v0, 0x0

    .line 166
    :goto_0
    return v0

    .line 165
    :cond_0
    invoke-interface {p1}, LCv;->a()LaGv;

    move-result-object v0

    .line 166
    sget-object v1, LaGv;->a:LaGv;

    invoke-virtual {v1, v0}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(ILandroid/view/View;Landroid/view/ViewGroup;Z)LHq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/view/View;",
            "Landroid/view/ViewGroup;",
            "Z)TT;"
        }
    .end annotation
.end method

.method public final a(LaGA;IIILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    .line 108
    invoke-direct {p0, p1, p2}, LHt;->a(LaGA;I)LCv;

    move-result-object v1

    .line 110
    invoke-direct {p0, v1}, LHt;->a(LCv;)Z

    move-result v2

    .line 111
    invoke-virtual {p0, p4, p5, p6, v2}, LHt;->a(ILandroid/view/View;Landroid/view/ViewGroup;Z)LHq;

    move-result-object v3

    .line 113
    invoke-interface {v1}, LCv;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v4

    .line 114
    invoke-virtual {v3, p1, p2}, LHq;->a(LaGA;I)V

    .line 115
    invoke-interface {v1}, LCv;->a()LaGv;

    move-result-object v5

    .line 116
    sget-object v0, LaGv;->a:LaGv;

    invoke-virtual {v0, v5}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 117
    invoke-virtual {p0, p1, p2}, LHt;->a(LaGA;I)Lcom/google/android/apps/docs/utils/FetchSpec;

    move-result-object v0

    .line 118
    invoke-virtual {v3, v0}, LHq;->a(Lcom/google/android/apps/docs/utils/FetchSpec;)V

    .line 120
    :cond_0
    invoke-interface {v1}, LCv;->e()Ljava/lang/String;

    move-result-object v0

    .line 121
    if-eqz v0, :cond_2

    sget-object v6, LaGn;->c:LaGn;

    .line 122
    invoke-static {v0}, LaGn;->a(Ljava/lang/String;)LaGn;

    move-result-object v0

    invoke-virtual {v6, v0}, LaGn;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 124
    :goto_0
    invoke-virtual {v3, v0}, LHq;->b(Z)V

    .line 126
    invoke-interface {v1}, LCv;->a()Ljava/lang/String;

    move-result-object v0

    .line 127
    const-string v6, "%s %s %s"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v0, v7, v8

    const/4 v0, 0x1

    iget-object v8, p0, LHt;->a:Landroid/content/res/Resources;

    .line 128
    invoke-virtual {v5}, LaGv;->f()I

    move-result v5

    invoke-virtual {v8, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v0

    const/4 v0, 0x2

    .line 129
    invoke-direct {p0, v1}, LHt;->a(LCv;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v0

    .line 127
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 130
    invoke-virtual {v3}, LHq;->a()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v0, p0, LHt;->a:LBf;

    invoke-interface {v0, v1}, LBf;->a(LCv;)V

    .line 133
    iget-object v0, p0, LHt;->a:LDM;

    invoke-virtual {v3}, LHq;->a()LDT;

    move-result-object v5

    invoke-virtual {v0, v5, v4}, LDM;->a(LDT;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 134
    iget-object v0, p0, LHt;->a:LDM;

    invoke-virtual {v0}, LDM;->a()Z

    move-result v0

    invoke-virtual {v3, v0}, LHq;->d(Z)V

    .line 136
    iget-object v0, p0, LHt;->a:LDg;

    if-eqz v0, :cond_1

    .line 137
    iget-object v0, p0, LHt;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 139
    :goto_1
    new-instance v5, Lcom/google/android/apps/docs/doclist/SelectionItem;

    invoke-direct {v5, v4, v2, v0}, Lcom/google/android/apps/docs/doclist/SelectionItem;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;ZLcom/google/android/apps/docs/doclist/SelectionItem;)V

    .line 141
    invoke-virtual {v3}, LHq;->a()LDi;

    move-result-object v0

    .line 142
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    iget-object v2, p0, LHt;->a:LDg;

    .line 144
    invoke-interface {p1}, LaGA;->a()LaGv;

    move-result-object v4

    .line 143
    invoke-interface {v2, v0, v5, p2, v4}, LDg;->a(LDi;Lcom/google/android/apps/docs/doclist/SelectionItem;ILaGv;)V

    .line 146
    :cond_1
    invoke-virtual {p0, v1, v3}, LHt;->a(LCv;LHq;)V

    .line 147
    invoke-virtual {v3}, LHq;->a()Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 122
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 137
    :cond_3
    new-instance v0, Lcom/google/android/apps/docs/doclist/SelectionItem;

    iget-object v5, p0, LHt;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct {v0, v5, v6, v7}, Lcom/google/android/apps/docs/doclist/SelectionItem;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;ZLcom/google/android/apps/docs/doclist/SelectionItem;)V

    goto :goto_1
.end method

.method public final a(LaGA;I)Lcom/google/android/apps/docs/utils/FetchSpec;
    .locals 5

    .prologue
    .line 171
    invoke-interface {p1, p2}, LaGA;->a(I)Z

    move-result v0

    .line 172
    const-string v1, "cursor.getCount()=%s, position=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {p1}, LaGA;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 173
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 172
    invoke-static {v0, v1, v2}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 174
    iget-object v0, p0, LHt;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    .line 175
    invoke-static {p1, v0}, Lcom/google/android/apps/docs/utils/FetchSpec;->a(LaGA;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)Lcom/google/android/apps/docs/utils/FetchSpec;

    move-result-object v0

    .line 176
    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, LHt;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHq;

    .line 186
    invoke-virtual {v0}, LHq;->a()LEd;

    move-result-object v0

    invoke-virtual {v0}, LEd;->a()V

    goto :goto_0

    .line 188
    :cond_0
    iget-object v0, p0, LHt;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 189
    return-void
.end method

.method public a(LCv;LHq;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LCv;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 99
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, LHt;->a:LDg;

    if-eqz v0, :cond_0

    .line 194
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 195
    if-eqz v0, :cond_0

    instance-of v1, v0, LHq;

    if-eqz v1, :cond_0

    .line 196
    check-cast v0, LHq;

    .line 197
    invoke-virtual {v0}, LHq;->a()LDi;

    move-result-object v0

    .line 198
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    iget-object v1, p0, LHt;->a:LDg;

    invoke-interface {v1, v0}, LDg;->a(LDi;)V

    .line 202
    :cond_0
    return-void
.end method
