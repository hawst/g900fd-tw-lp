.class Lbpz;
.super Lbmt;
.source "RegularImmutableAsList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lbmt",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<+TE;>;"
        }
    .end annotation
.end field

.field private final b:Lbmv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbmv",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lbmv;LbmF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbmv",
            "<TE;>;",
            "LbmF",
            "<+TE;>;)V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0}, Lbmt;-><init>()V

    .line 35
    iput-object p1, p0, Lbpz;->b:Lbmv;

    .line 36
    iput-object p2, p0, Lbpz;->a:LbmF;

    .line 37
    return-void
.end method

.method constructor <init>(Lbmv;[Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbmv",
            "<TE;>;[",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-static {p2}, LbmF;->b([Ljava/lang/Object;)LbmF;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lbpz;-><init>(Lbmv;LbmF;)V

    .line 41
    return-void
.end method


# virtual methods
.method a()Lbmv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbmv",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lbpz;->b:Lbmv;

    return-object v0
.end method

.method public a(I)Lbqw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lbqw",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lbpz;->a:LbmF;

    invoke-virtual {v0, p1}, LbmF;->a(I)Lbqw;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lbpz;->a:LbmF;

    invoke-virtual {v0, p1}, LbmF;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lbpz;->a:LbmF;

    invoke-virtual {v0, p1}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lbpz;->a:LbmF;

    invoke-virtual {v0}, LbmF;->hashCode()I

    move-result v0

    return v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lbpz;->a:LbmF;

    invoke-virtual {v0, p1}, LbmF;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lbpz;->a:LbmF;

    invoke-virtual {v0, p1}, LbmF;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public synthetic listIterator(I)Ljava/util/ListIterator;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lbpz;->a(I)Lbqw;

    move-result-object v0

    return-object v0
.end method

.method public toArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lbpz;->a:LbmF;

    invoke-virtual {v0}, LbmF;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lbpz;->a:LbmF;

    invoke-virtual {v0, p1}, LbmF;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
