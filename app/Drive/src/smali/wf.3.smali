.class public Lwf;
.super Ljava/lang/Object;
.source "LabelCriterionVisitor.java"

# interfaces
.implements LvR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LvR",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LaGM;

.field private final a:Landroid/content/Context;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LaGM;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lwf;->a:Ljava/util/List;

    .line 31
    iput-object p1, p0, Lwf;->a:LaGM;

    .line 32
    iput-object p2, p0, Lwf;->a:Landroid/content/Context;

    .line 33
    return-void
.end method


# virtual methods
.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lwf;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 113
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 115
    const/4 v0, 0x0

    .line 116
    iget-object v1, p0, Lwf;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 117
    if-eqz v1, :cond_0

    .line 118
    const-string v1, ", "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    :cond_0
    const/4 v1, 0x1

    .line 122
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 125
    :cond_1
    if-nez v1, :cond_2

    .line 126
    const/4 v0, 0x0

    .line 129
    :goto_1
    return-object v0

    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public a()V
    .locals 0

    .prologue
    .line 78
    return-void
.end method

.method public a(LCl;Z)V
    .locals 6

    .prologue
    .line 42
    invoke-interface {p1}, LCl;->a()I

    move-result v0

    .line 43
    if-eqz v0, :cond_0

    iget-object v1, p0, Lwf;->a:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 44
    iget-object v1, p0, Lwf;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 45
    if-eqz p2, :cond_1

    .line 46
    iget-object v1, p0, Lwf;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    iget-object v1, p0, Lwf;->a:Ljava/util/List;

    iget-object v2, p0, Lwf;->a:Landroid/content/Context;

    sget v3, Lxi;->navigation_filter:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(LaFO;)V
    .locals 0

    .prologue
    .line 38
    return-void
.end method

.method public a(Laay;)V
    .locals 6

    .prologue
    .line 60
    iget-object v0, p0, Lwf;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lwf;->a:Ljava/util/List;

    iget-object v1, p0, Lwf;->a:Landroid/content/Context;

    sget v2, Lxi;->navigation_search_results:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Laay;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    :cond_0
    return-void
.end method

.method public a(LbmY;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "LaGv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 98
    return-void
.end method

.method public a(LbmY;LbmY;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "LaGv;",
            ">;",
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 104
    return-void
.end method

.method public a(LbmY;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 56
    return-void
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lwf;->a:LaGM;

    invoke-interface {v0, p1}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFV;

    move-result-object v0

    .line 68
    if-nez v0, :cond_0

    .line 73
    :goto_0
    return-void

    .line 72
    :cond_0
    iget-object v1, p0, Lwf;->a:Ljava/util/List;

    invoke-interface {v0}, LaFV;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 83
    return-void
.end method

.method public b(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 0

    .prologue
    .line 93
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 88
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 109
    return-void
.end method
