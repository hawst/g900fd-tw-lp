.class public abstract Lbre;
.super Ljava/lang/Object;
.source "ByteSource.java"


# static fields
.field private static final a:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 158
    const/16 v0, 0x1000

    new-array v0, v0, [B

    sput-object v0, Lbre;->a:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298
    return-void
.end method


# virtual methods
.method public a(Ljava/io/OutputStream;)J
    .locals 4

    .prologue
    .line 177
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    invoke-static {}, Lbrm;->a()Lbrm;

    move-result-object v1

    .line 181
    :try_start_0
    invoke-virtual {p0}, Lbre;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbrm;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    .line 182
    invoke-static {v0, p1}, Lbrh;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 186
    invoke-virtual {v1}, Lbrm;->close()V

    return-wide v2

    .line 183
    :catch_0
    move-exception v0

    .line 184
    :try_start_1
    invoke-virtual {v1, v0}, Lbrm;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 186
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lbrm;->close()V

    throw v0
.end method

.method public a(Ljava/nio/charset/Charset;)Lbrj;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Lbrg;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lbrg;-><init>(Lbre;Ljava/nio/charset/Charset;Lbrf;)V

    return-object v0
.end method

.method public abstract a()Ljava/io/InputStream;
.end method
