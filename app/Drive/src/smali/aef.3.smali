.class final enum Laef;
.super Ljava/lang/Enum;
.source "ZippedFileSource.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Laef;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Laef;

.field private static final synthetic a:[Laef;

.field public static final enum b:Laef;

.field public static final enum c:Laef;

.field public static final enum d:Laef;

.field public static final enum e:Laef;

.field public static final enum f:Laef;

.field public static final enum g:Laef;


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 35
    new-instance v0, Laef;

    const-string v1, "HTML"

    const-string v2, ".html"

    const-string v3, "text/html"

    invoke-direct {v0, v1, v5, v2, v3}, Laef;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Laef;->a:Laef;

    .line 36
    new-instance v0, Laef;

    const-string v1, "CSS"

    const-string v2, ".css"

    const-string v3, "text/css"

    invoke-direct {v0, v1, v6, v2, v3}, Laef;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Laef;->b:Laef;

    .line 37
    new-instance v0, Laef;

    const-string v1, "JS"

    const-string v2, ".js"

    const-string v3, "text/javascript"

    invoke-direct {v0, v1, v7, v2, v3}, Laef;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Laef;->c:Laef;

    .line 38
    new-instance v0, Laef;

    const-string v1, "JPG"

    const-string v2, ".jpg"

    const-string v3, "image/jpeg"

    invoke-direct {v0, v1, v8, v2, v3}, Laef;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Laef;->d:Laef;

    .line 39
    new-instance v0, Laef;

    const-string v1, "JPEG"

    const-string v2, ".jpeg"

    const-string v3, "image/jpeg"

    invoke-direct {v0, v1, v9, v2, v3}, Laef;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Laef;->e:Laef;

    .line 40
    new-instance v0, Laef;

    const-string v1, "PNG"

    const/4 v2, 0x5

    const-string v3, ".png"

    const-string v4, "image/png"

    invoke-direct {v0, v1, v2, v3, v4}, Laef;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Laef;->f:Laef;

    .line 41
    new-instance v0, Laef;

    const-string v1, "GIF"

    const/4 v2, 0x6

    const-string v3, ".gif"

    const-string v4, "image/gif"

    invoke-direct {v0, v1, v2, v3, v4}, Laef;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Laef;->g:Laef;

    .line 34
    const/4 v0, 0x7

    new-array v0, v0, [Laef;

    sget-object v1, Laef;->a:Laef;

    aput-object v1, v0, v5

    sget-object v1, Laef;->b:Laef;

    aput-object v1, v0, v6

    sget-object v1, Laef;->c:Laef;

    aput-object v1, v0, v7

    sget-object v1, Laef;->d:Laef;

    aput-object v1, v0, v8

    sget-object v1, Laef;->e:Laef;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Laef;->f:Laef;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Laef;->g:Laef;

    aput-object v2, v0, v1

    sput-object v0, Laef;->a:[Laef;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 47
    iput-object p3, p0, Laef;->a:Ljava/lang/String;

    .line 48
    iput-object p4, p0, Laef;->b:Ljava/lang/String;

    .line 49
    return-void
.end method

.method static a(Ljava/lang/String;)Laef;
    .locals 6

    .prologue
    .line 68
    const-string v0, "."

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, ""

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 69
    sget-object v0, Laef;->a:Laef;

    .line 75
    :cond_0
    return-object v0

    .line 72
    :cond_1
    invoke-static {p0}, Laef;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 73
    invoke-static {}, Laef;->values()[Laef;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    aget-object v0, v3, v1

    .line 74
    iget-object v5, v0, Laef;->a:Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 73
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 78
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 54
    const-string v0, "."

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    .line 55
    if-ltz v3, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "expected name containing \'.\', got %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {v0, v4, v1}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 56
    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 57
    return-object v0

    :cond_0
    move v0, v2

    .line 55
    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Laef;
    .locals 1

    .prologue
    .line 34
    const-class v0, Laef;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Laef;

    return-object v0
.end method

.method public static values()[Laef;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Laef;->a:[Laef;

    invoke-virtual {v0}, [Laef;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laef;

    return-object v0
.end method
