.class public final LWJ;
.super LGJ;
.source "LegacyPrintActivity.java"


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/print/LegacyPrintActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/print/LegacyPrintActivity;Landroid/content/Context;LGH;LaFO;LQr;Ljava/lang/Class;LUT;Landroid/content/SharedPreferences;LTd;Landroid/os/Handler;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LGH;",
            "LaFO;",
            "LQr;",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;",
            "LUT;",
            "Landroid/content/SharedPreferences;",
            "LTd;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 382
    iput-object p1, p0, LWJ;->a:Lcom/google/android/apps/docs/print/LegacyPrintActivity;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    .line 383
    invoke-direct/range {v0 .. v9}, LGJ;-><init>(Landroid/content/Context;LGH;LaFO;LQr;Ljava/lang/Class;LUT;Landroid/content/SharedPreferences;LTd;Landroid/os/Handler;)V

    .line 392
    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 396
    invoke-super {p0, p1, p2}, LGJ;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 397
    const-string v0, "PrintDialogActivity"

    const-string v1, "Finished page loading of %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 398
    iget-object v0, p0, LWJ;->a:Lcom/google/android/apps/docs/print/LegacyPrintActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a(Lcom/google/android/apps/docs/print/LegacyPrintActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 401
    const-string v0, "javascript:window.addEventListener(\'message\',function(evt){window.AndroidPrintDialog.onPostMessage(evt.data)}, false)"

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 404
    iget-object v0, p0, LWJ;->a:Lcom/google/android/apps/docs/print/LegacyPrintActivity;

    invoke-static {v0, v3}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a(Lcom/google/android/apps/docs/print/LegacyPrintActivity;Z)V

    .line 406
    :cond_0
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 411
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LFO;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 412
    iget-object v2, p0, LWJ;->a:Lcom/google/android/apps/docs/print/LegacyPrintActivity;

    .line 413
    invoke-static {v2}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a(Lcom/google/android/apps/docs/print/LegacyPrintActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 412
    invoke-static {v2}, LFO;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 414
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 415
    const-string v1, "PrintDialogActivity"

    const-string v2, "In shouldOverrideUrlLoading with url %s and returning false"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v0

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 419
    :goto_0
    return v0

    .line 418
    :cond_0
    const-string v1, "PrintDialogActivity"

    const-string v2, "In shouldOverrideUrlLoading with url %s and calling super"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v0

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 419
    invoke-super {p0, p1, p2}, LGJ;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
