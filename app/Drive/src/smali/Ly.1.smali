.class public LLy;
.super LLQ;
.source "SelectionFunctionDoLink.java"


# instance fields
.field private final a:LPp;

.field private final a:LtK;


# direct methods
.method constructor <init>(LPp;LtK;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, LLQ;-><init>()V

    .line 25
    iput-object p1, p0, LLy;->a:LPp;

    .line 26
    iput-object p2, p0, LLy;->a:LtK;

    .line 27
    return-void
.end method

.method private a(LaGu;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 46
    iget-object v1, p0, LLy;->a:LtK;

    sget-object v2, Lry;->F:Lry;

    invoke-interface {v1, v2}, LtK;->a(LtJ;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 49
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1}, LaGu;->a()LaGv;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, LaGu;->a()LaGv;

    move-result-object v1

    invoke-virtual {v1}, LaGv;->a()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/Runnable;LaFM;LbmF;LbmF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "LaFM;",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-static {p3}, Lbnm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    .line 41
    iget-object v1, p0, LLy;->a:LPp;

    invoke-interface {v1, v0}, LPp;->f(LaGu;)V

    .line 42
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 43
    return-void
.end method

.method public a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-virtual {p1, v1}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    .line 33
    invoke-super {p0, p1, p2}, LLQ;->a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, LaGu;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 34
    invoke-direct {p0, v0}, LLy;->a(LaGu;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
