.class public LaqO;
.super Ljava/lang/Object;
.source "SwipeIntentDetector.java"


# instance fields
.field private a:F

.field private a:I

.field private a:J

.field private final a:LaqP;

.field private a:Z

.field private b:F

.field private b:J

.field private c:F

.field private d:F

.field private e:F

.field private f:F


# direct methods
.method public constructor <init>(LaqP;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-boolean v0, p0, LaqO;->a:Z

    .line 59
    iput v0, p0, LaqO;->a:I

    .line 80
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    iput-object p1, p0, LaqO;->a:LaqP;

    .line 82
    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 169
    iget v0, p0, LaqO;->a:I

    if-eq p1, v0, :cond_0

    .line 170
    iput p1, p0, LaqO;->a:I

    .line 171
    iget-object v0, p0, LaqO;->a:LaqP;

    invoke-interface {v0, p1}, LaqP;->a(I)V

    .line 173
    :cond_0
    return-void
.end method

.method private b(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 85
    const-string v0, "SwipeIntentDetector"

    const-string v1, "onStartMove"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    const/4 v0, 0x1

    iput-boolean v0, p0, LaqO;->a:Z

    .line 87
    const/4 v0, 0x0

    iput v0, p0, LaqO;->a:I

    .line 88
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, LaqO;->a:F

    .line 89
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, LaqO;->b:F

    .line 90
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LaqO;->b:J

    .line 91
    iput v2, p0, LaqO;->e:F

    .line 92
    iput v2, p0, LaqO;->f:F

    .line 93
    iget v0, p0, LaqO;->a:F

    iput v0, p0, LaqO;->c:F

    .line 94
    iget v0, p0, LaqO;->b:F

    iput v0, p0, LaqO;->d:F

    .line 95
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iput-wide v0, p0, LaqO;->a:J

    .line 96
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 179
    iput-boolean v0, p0, LaqO;->a:Z

    .line 180
    iput v0, p0, LaqO;->a:I

    .line 181
    return-void
.end method

.method public a(Landroid/view/MotionEvent;)V
    .locals 14

    .prologue
    .line 105
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 106
    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 108
    :pswitch_0
    iget-boolean v0, p0, LaqO;->a:Z

    if-eqz v0, :cond_0

    .line 112
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    .line 113
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    .line 114
    iget v0, p0, LaqO;->c:F

    sub-float v3, v1, v0

    .line 115
    iget v0, p0, LaqO;->d:F

    sub-float v4, v2, v0

    .line 116
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    .line 117
    iget-wide v8, p0, LaqO;->a:J

    sub-long v8, v6, v8

    .line 119
    float-to-double v10, v3

    const-wide/16 v12, 0x0

    cmpl-double v0, v10, v12

    if-nez v0, :cond_1

    float-to-double v10, v4

    const-wide/16 v12, 0x0

    cmpl-double v0, v10, v12

    if-eqz v0, :cond_4

    :cond_1
    const/4 v0, 0x1

    .line 121
    :goto_1
    if-nez v0, :cond_2

    const-wide/16 v10, 0x32

    cmp-long v5, v8, v10

    if-lez v5, :cond_3

    .line 122
    :cond_2
    iput-wide v8, p0, LaqO;->b:J

    .line 123
    iput v3, p0, LaqO;->e:F

    .line 124
    iput v4, p0, LaqO;->f:F

    .line 125
    iput v1, p0, LaqO;->c:F

    .line 126
    iput v2, p0, LaqO;->d:F

    .line 127
    iput-wide v6, p0, LaqO;->a:J

    .line 130
    :cond_3
    if-eqz v0, :cond_0

    .line 134
    iget v0, p0, LaqO;->e:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 135
    iget v1, p0, LaqO;->f:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 137
    cmpl-float v2, v0, v1

    if-lez v2, :cond_6

    .line 138
    iget-wide v2, p0, LaqO;->b:J

    long-to-float v1, v2

    div-float/2addr v0, v1

    .line 139
    const v1, 0x3dcccccd    # 0.1f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 140
    iget v0, p0, LaqO;->e:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    const/4 v0, 0x1

    .line 141
    :goto_2
    invoke-direct {p0, v0}, LaqO;->a(I)V

    goto :goto_0

    .line 119
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 140
    :cond_5
    const/4 v0, -0x1

    goto :goto_2

    .line 144
    :cond_6
    iget-wide v2, p0, LaqO;->b:J

    long-to-float v0, v2

    div-float v0, v1, v0

    .line 145
    const v1, 0x3dcccccd    # 0.1f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 146
    iget v0, p0, LaqO;->f:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_7

    const/4 v0, 0x1

    .line 147
    :goto_3
    invoke-direct {p0, v0}, LaqO;->a(I)V

    goto/16 :goto_0

    .line 146
    :cond_7
    const/4 v0, -0x1

    goto :goto_3

    .line 153
    :pswitch_1
    invoke-direct {p0, p1}, LaqO;->b(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 156
    :pswitch_2
    invoke-virtual {p0}, LaqO;->a()V

    goto/16 :goto_0

    .line 159
    :pswitch_3
    iget v0, p0, LaqO;->a:I

    .line 160
    invoke-virtual {p0}, LaqO;->a()V

    .line 161
    if-eqz v0, :cond_0

    .line 162
    iget-object v1, p0, LaqO;->a:LaqP;

    invoke-interface {v1, v0}, LaqP;->b(I)V

    goto/16 :goto_0

    .line 106
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
