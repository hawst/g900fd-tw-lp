.class final LxS;
.super Ljava/lang/Object;
.source "CrossAppPromoCatalogInfo.java"


# static fields
.field static final a:LxS;


# instance fields
.field private final a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "LxP;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "LxO;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "LxO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 32
    invoke-static {}, LbmY;->a()LbmY;

    move-result-object v0

    invoke-static {}, LbmY;->a()LbmY;

    move-result-object v1

    .line 33
    invoke-static {}, LbmY;->a()LbmY;

    move-result-object v2

    .line 31
    invoke-static {v0, v1, v2}, LxS;->a(LbmY;LbmY;LbmY;)LxS;

    move-result-object v0

    sput-object v0, LxS;->a:LxS;

    return-void
.end method

.method private constructor <init>(LbmY;LbmY;LbmY;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "LxP;",
            ">;",
            "LbmY",
            "<",
            "LxO;",
            ">;",
            "LbmY",
            "<",
            "LxO;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbmY;

    iput-object v0, p0, LxS;->a:LbmY;

    .line 43
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbmY;

    iput-object v0, p0, LxS;->b:LbmY;

    .line 44
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbmY;

    iput-object v0, p0, LxS;->c:LbmY;

    .line 45
    return-void
.end method

.method static a(LbmY;LbmY;LbmY;)LxS;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "LxP;",
            ">;",
            "LbmY",
            "<",
            "LxO;",
            ">;",
            "LbmY",
            "<",
            "LxO;",
            ">;)",
            "LxS;"
        }
    .end annotation

    .prologue
    .line 51
    new-instance v0, LxS;

    invoke-direct {v0, p0, p1, p2}, LxS;-><init>(LbmY;LbmY;LbmY;)V

    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LxP;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, LxS;->a:LbmY;

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LxO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, LxS;->b:LbmY;

    return-object v0
.end method

.method public c()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LxO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, LxS;->c:LbmY;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 69
    instance-of v1, p1, LxS;

    if-nez v1, :cond_1

    .line 76
    :cond_0
    :goto_0
    return v0

    .line 73
    :cond_1
    check-cast p1, LxS;

    .line 74
    iget-object v1, p0, LxS;->a:LbmY;

    iget-object v2, p1, LxS;->a:LbmY;

    invoke-virtual {v1, v2}, LbmY;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LxS;->b:LbmY;

    iget-object v2, p1, LxS;->b:LbmY;

    .line 75
    invoke-virtual {v1, v2}, LbmY;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LxS;->c:LbmY;

    iget-object v2, p1, LxS;->c:LbmY;

    .line 76
    invoke-virtual {v1, v2}, LbmY;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 81
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LxS;->a:LbmY;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LxS;->b:LbmY;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LxS;->c:LbmY;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 86
    const-string v0, "CrossAppPromoCatalogInfo[%s, %s, %s]"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LxS;->a:LbmY;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LxS;->b:LbmY;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, LxS;->c:LbmY;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
