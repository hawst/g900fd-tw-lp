.class public Laeg;
.super Laee;
.source "ZippedKixHtmlFileSource.java"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lalo;Ljava/io/File;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Laee;-><init>(Lalo;Ljava/io/File;)V

    .line 37
    iput-object p3, p0, Laeg;->a:Ljava/lang/String;

    .line 38
    iget-object v0, p0, Laeg;->a:Ljava/util/Collection;

    invoke-static {v0}, Laeg;->a(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laeg;->b:Ljava/lang/String;

    .line 39
    return-void
.end method

.method static a(Ljava/util/Collection;)Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 50
    const/4 v1, 0x0

    .line 51
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 52
    invoke-static {v0}, Laef;->a(Ljava/lang/String;)Laef;

    move-result-object v2

    .line 54
    sget-object v6, Laef;->a:Laef;

    invoke-virtual {v2, v6}, Laef;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 55
    if-nez v1, :cond_0

    move v2, v3

    :goto_1
    const-string v6, "More than one html entry: %s, %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v1, v7, v4

    aput-object v0, v7, v3

    invoke-static {v2, v6, v7}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    :goto_2
    move-object v1, v0

    .line 59
    goto :goto_0

    :cond_0
    move v2, v4

    .line 55
    goto :goto_1

    .line 60
    :cond_1
    if-eqz v1, :cond_2

    :goto_3
    const-string v0, "No html entry"

    invoke-static {v3, v0}, LbiT;->a(ZLjava/lang/Object;)V

    .line 61
    return-object v1

    :cond_2
    move v3, v4

    .line 60
    goto :goto_3

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Laeg;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    iget-object p1, p0, Laeg;->b:Ljava/lang/String;

    .line 70
    :cond_0
    invoke-super {p0, p1, p2}, Laee;->a(Ljava/lang/String;Ljava/io/OutputStream;)V

    .line 71
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 75
    const-string v0, "ZippedKixHtmlFileSource[%s, %s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Laeg;->a:Ljava/io/File;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Laeg;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
