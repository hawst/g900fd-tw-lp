.class public LmK;
.super Ljava/lang/Object;
.source "ListPopupWindow.java"


# static fields
.field private static a:Ljava/lang/reflect/Method;


# instance fields
.field a:I

.field private a:Landroid/content/Context;

.field private a:Landroid/database/DataSetObserver;

.field private a:Landroid/graphics/Rect;

.field private a:Landroid/graphics/drawable/Drawable;

.field private a:Landroid/os/Handler;

.field private a:Landroid/view/View;

.field private a:Landroid/widget/AdapterView$OnItemClickListener;

.field private a:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private a:Landroid/widget/ListAdapter;

.field private a:Landroid/widget/PopupWindow;

.field private a:Ljava/lang/Runnable;

.field private a:LmO;

.field private final a:LmS;

.field private final a:LmU;

.field private final a:LmV;

.field private final a:LmW;

.field private a:Z

.field private b:I

.field private b:Landroid/view/View;

.field private b:Z

.field private c:I

.field private c:Z

.field private d:I

.field private d:Z

.field private e:I

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 80
    :try_start_0
    const-class v0, Landroid/widget/PopupWindow;

    const-string v1, "setClipToScreenEnabled"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, LmK;->a:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    :goto_0
    return-void

    .line 82
    :catch_0
    move-exception v0

    .line 83
    const-string v0, "ListPopupWindow"

    const-string v1, "Could not find method setClipToScreenEnabled() on PopupWindow. Oh well."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 192
    const/4 v0, 0x0

    sget v1, Liq;->listPopupWindowStyle:I

    invoke-direct {p0, p1, v0, v1}, LmK;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 193
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, -0x2

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput v0, p0, LmK;->b:I

    .line 93
    iput v0, p0, LmK;->c:I

    .line 98
    iput v2, p0, LmK;->f:I

    .line 100
    iput-boolean v2, p0, LmK;->b:Z

    .line 101
    iput-boolean v2, p0, LmK;->c:Z

    .line 102
    const v0, 0x7fffffff

    iput v0, p0, LmK;->a:I

    .line 105
    iput v2, p0, LmK;->g:I

    .line 116
    new-instance v0, LmW;

    invoke-direct {v0, p0, v1}, LmW;-><init>(LmK;LmL;)V

    iput-object v0, p0, LmK;->a:LmW;

    .line 117
    new-instance v0, LmV;

    invoke-direct {v0, p0, v1}, LmV;-><init>(LmK;LmL;)V

    iput-object v0, p0, LmK;->a:LmV;

    .line 118
    new-instance v0, LmU;

    invoke-direct {v0, p0, v1}, LmU;-><init>(LmK;LmL;)V

    iput-object v0, p0, LmK;->a:LmU;

    .line 119
    new-instance v0, LmS;

    invoke-direct {v0, p0, v1}, LmS;-><init>(LmK;LmL;)V

    iput-object v0, p0, LmK;->a:LmS;

    .line 122
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LmK;->a:Landroid/os/Handler;

    .line 124
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LmK;->a:Landroid/graphics/Rect;

    .line 215
    iput-object p1, p0, LmK;->a:Landroid/content/Context;

    .line 217
    sget-object v0, LiA;->ListPopupWindow:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 219
    sget v1, LiA;->ListPopupWindow_android_dropDownHorizontalOffset:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, LmK;->d:I

    .line 221
    sget v1, LiA;->ListPopupWindow_android_dropDownVerticalOffset:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, LmK;->e:I

    .line 223
    iget v1, p0, LmK;->e:I

    if-eqz v1, :cond_0

    .line 224
    iput-boolean v3, p0, LmK;->a:Z

    .line 226
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 228
    new-instance v0, Lkd;

    invoke-direct {v0, p1, p2, p3}, Lkd;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, LmK;->a:Landroid/widget/PopupWindow;

    .line 229
    iget-object v0, p0, LmK;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 232
    iget-object v0, p0, LmK;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 233
    invoke-static {v0}, LcC;->a(Ljava/util/Locale;)I

    move-result v0

    iput v0, p0, LmK;->h:I

    .line 234
    return-void
.end method

.method private a()I
    .locals 10

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v8, -0x80000000

    const/4 v1, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 1036
    .line 1038
    iget-object v0, p0, LmK;->a:LmO;

    if-nez v0, :cond_5

    .line 1039
    iget-object v5, p0, LmK;->a:Landroid/content/Context;

    .line 1047
    new-instance v0, LmM;

    invoke-direct {v0, p0}, LmM;-><init>(LmK;)V

    iput-object v0, p0, LmK;->a:Ljava/lang/Runnable;

    .line 1057
    new-instance v4, LmO;

    iget-boolean v0, p0, LmK;->d:Z

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    invoke-direct {v4, v5, v0}, LmO;-><init>(Landroid/content/Context;Z)V

    iput-object v4, p0, LmK;->a:LmO;

    .line 1058
    iget-object v0, p0, LmK;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1059
    iget-object v0, p0, LmK;->a:LmO;

    iget-object v4, p0, LmK;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v4}, LmO;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 1061
    :cond_0
    iget-object v0, p0, LmK;->a:LmO;

    iget-object v4, p0, LmK;->a:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v4}, LmO;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1062
    iget-object v0, p0, LmK;->a:LmO;

    iget-object v4, p0, LmK;->a:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v4}, LmO;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1063
    iget-object v0, p0, LmK;->a:LmO;

    invoke-virtual {v0, v1}, LmO;->setFocusable(Z)V

    .line 1064
    iget-object v0, p0, LmK;->a:LmO;

    invoke-virtual {v0, v1}, LmO;->setFocusableInTouchMode(Z)V

    .line 1065
    iget-object v0, p0, LmK;->a:LmO;

    new-instance v4, LmN;

    invoke-direct {v4, p0}, LmN;-><init>(LmK;)V

    invoke-virtual {v0, v4}, LmO;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 1081
    iget-object v0, p0, LmK;->a:LmO;

    iget-object v4, p0, LmK;->a:LmU;

    invoke-virtual {v0, v4}, LmO;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1083
    iget-object v0, p0, LmK;->a:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_1

    .line 1084
    iget-object v0, p0, LmK;->a:LmO;

    iget-object v4, p0, LmK;->a:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v0, v4}, LmO;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 1087
    :cond_1
    iget-object v0, p0, LmK;->a:LmO;

    .line 1089
    iget-object v6, p0, LmK;->a:Landroid/view/View;

    .line 1090
    if-eqz v6, :cond_b

    .line 1093
    new-instance v4, Landroid/widget/LinearLayout;

    invoke-direct {v4, v5}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1094
    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1096
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v1, v3, v2, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 1100
    iget v5, p0, LmK;->g:I

    packed-switch v5, :pswitch_data_0

    .line 1112
    const-string v0, "ListPopupWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid hint position "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v5, p0, LmK;->g:I

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1118
    :goto_1
    iget v0, p0, LmK;->c:I

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1120
    invoke-virtual {v6, v0, v2}, Landroid/view/View;->measure(II)V

    .line 1122
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1123
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget v5, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    add-int/2addr v1, v5

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    move-object v1, v4

    .line 1129
    :goto_2
    iget-object v4, p0, LmK;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v4, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    move v6, v0

    .line 1144
    :goto_3
    iget-object v0, p0, LmK;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1145
    if-eqz v0, :cond_6

    .line 1146
    iget-object v1, p0, LmK;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 1147
    iget-object v0, p0, LmK;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, LmK;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    .line 1151
    iget-boolean v1, p0, LmK;->a:Z

    if-nez v1, :cond_9

    .line 1152
    iget-object v1, p0, LmK;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    neg-int v1, v1

    iput v1, p0, LmK;->e:I

    move v7, v0

    .line 1159
    :goto_4
    iget-object v0, p0, LmK;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 1161
    :cond_2
    iget-object v0, p0, LmK;->a:Landroid/widget/PopupWindow;

    invoke-virtual {p0}, LmK;->a()Landroid/view/View;

    move-result-object v1

    iget v4, p0, LmK;->e:I

    invoke-virtual {v0, v1, v4}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;I)I

    move-result v4

    .line 1164
    iget-boolean v0, p0, LmK;->b:Z

    if-nez v0, :cond_3

    iget v0, p0, LmK;->b:I

    if-ne v0, v3, :cond_7

    .line 1165
    :cond_3
    add-int v0, v4, v7

    .line 1193
    :goto_5
    return v0

    :cond_4
    move v0, v2

    .line 1057
    goto/16 :goto_0

    .line 1102
    :pswitch_0
    invoke-virtual {v4, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1103
    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 1107
    :pswitch_1
    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1108
    invoke-virtual {v4, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 1131
    :cond_5
    iget-object v0, p0, LmK;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1132
    iget-object v1, p0, LmK;->a:Landroid/view/View;

    .line 1133
    if-eqz v1, :cond_a

    .line 1134
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1136
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget v4, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    add-int/2addr v1, v4

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    move v6, v0

    goto :goto_3

    .line 1155
    :cond_6
    iget-object v0, p0, LmK;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    move v7, v2

    goto :goto_4

    .line 1169
    :cond_7
    iget v0, p0, LmK;->c:I

    packed-switch v0, :pswitch_data_1

    .line 1183
    iget v0, p0, LmK;->c:I

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1187
    :goto_6
    iget-object v0, p0, LmK;->a:LmO;

    sub-int/2addr v4, v6

    move v5, v3

    invoke-virtual/range {v0 .. v5}, LmO;->a(IIIII)I

    move-result v0

    .line 1191
    if-lez v0, :cond_8

    add-int/2addr v6, v7

    .line 1193
    :cond_8
    add-int/2addr v0, v6

    goto :goto_5

    .line 1171
    :pswitch_2
    iget-object v0, p0, LmK;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v1, p0, LmK;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, LmK;->a:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v5

    sub-int/2addr v0, v1

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    goto :goto_6

    .line 1177
    :pswitch_3
    iget-object v0, p0, LmK;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v1, p0, LmK;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, LmK;->a:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v5

    sub-int/2addr v0, v1

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    goto :goto_6

    :cond_9
    move v7, v0

    goto/16 :goto_4

    :cond_a
    move v6, v2

    goto/16 :goto_3

    :cond_b
    move-object v1, v0

    move v0, v2

    goto/16 :goto_2

    .line 1100
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 1169
    :pswitch_data_1
    .packed-switch -0x2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(LmK;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, LmK;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(LmK;)Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, LmK;->a:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic a(LmK;)LmO;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, LmK;->a:LmO;

    return-object v0
.end method

.method static synthetic a(LmK;)LmW;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, LmK;->a:LmW;

    return-object v0
.end method

.method private b(Z)V
    .locals 5

    .prologue
    .line 1756
    sget-object v0, LmK;->a:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    .line 1758
    :try_start_0
    sget-object v0, LmK;->a:Ljava/lang/reflect/Method;

    iget-object v1, p0, LmK;->a:Landroid/widget/PopupWindow;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1763
    :cond_0
    :goto_0
    return-void

    .line 1759
    :catch_0
    move-exception v0

    .line 1760
    const-string v0, "ListPopupWindow"

    const-string v1, "Could not call setClipToScreenEnabled() on PopupWindow. Oh well."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 675
    iget-object v0, p0, LmK;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 676
    iget-object v0, p0, LmK;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 677
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 678
    check-cast v0, Landroid/view/ViewGroup;

    .line 679
    iget-object v1, p0, LmK;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 682
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, LmK;->b:Landroid/view/View;

    return-object v0
.end method

.method public a()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 832
    iget-object v0, p0, LmK;->a:LmO;

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 658
    iget-object v0, p0, LmK;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 659
    invoke-direct {p0}, LmK;->d()V

    .line 660
    iget-object v0, p0, LmK;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 661
    iput-object v1, p0, LmK;->a:LmO;

    .line 662
    iget-object v0, p0, LmK;->a:Landroid/os/Handler;

    iget-object v1, p0, LmK;->a:LmW;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 663
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 268
    iput p1, p0, LmK;->g:I

    .line 269
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, LmK;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 386
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 422
    iput-object p1, p0, LmK;->b:Landroid/view/View;

    .line 423
    return-void
.end method

.method public a(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0

    .prologue
    .line 528
    iput-object p1, p0, LmK;->a:Landroid/widget/AdapterView$OnItemClickListener;

    .line 529
    return-void
.end method

.method public a(Landroid/widget/ListAdapter;)V
    .locals 2

    .prologue
    .line 243
    iget-object v0, p0, LmK;->a:Landroid/database/DataSetObserver;

    if-nez v0, :cond_3

    .line 244
    new-instance v0, LmT;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LmT;-><init>(LmK;LmL;)V

    iput-object v0, p0, LmK;->a:Landroid/database/DataSetObserver;

    .line 248
    :cond_0
    :goto_0
    iput-object p1, p0, LmK;->a:Landroid/widget/ListAdapter;

    .line 249
    iget-object v0, p0, LmK;->a:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_1

    .line 250
    iget-object v0, p0, LmK;->a:Landroid/database/DataSetObserver;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 253
    :cond_1
    iget-object v0, p0, LmK;->a:LmO;

    if-eqz v0, :cond_2

    .line 254
    iget-object v0, p0, LmK;->a:LmO;

    iget-object v1, p0, LmK;->a:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v1}, LmO;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 256
    :cond_2
    return-void

    .line 245
    :cond_3
    iget-object v0, p0, LmK;->a:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, LmK;->a:Landroid/widget/ListAdapter;

    iget-object v1, p0, LmK;->a:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_0
.end method

.method public a(Landroid/widget/PopupWindow$OnDismissListener;)V
    .locals 1

    .prologue
    .line 671
    iget-object v0, p0, LmK;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 672
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 291
    iput-boolean p1, p0, LmK;->d:Z

    .line 292
    iget-object v0, p0, LmK;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 293
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 747
    iget-object v0, p0, LmK;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v7, -0x2

    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 571
    invoke-direct {p0}, LmK;->a()I

    move-result v5

    .line 576
    invoke-virtual {p0}, LmK;->b()Z

    move-result v2

    .line 578
    iget-object v4, p0, LmK;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v4}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 579
    iget v4, p0, LmK;->c:I

    if-ne v4, v0, :cond_3

    move v4, v0

    .line 589
    :goto_0
    iget v6, p0, LmK;->b:I

    if-ne v6, v0, :cond_9

    .line 592
    if-eqz v2, :cond_5

    .line 593
    :goto_1
    if-eqz v2, :cond_7

    .line 594
    iget-object v2, p0, LmK;->a:Landroid/widget/PopupWindow;

    iget v6, p0, LmK;->c:I

    if-ne v6, v0, :cond_6

    :goto_2
    invoke-virtual {v2, v0, v1}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    .line 609
    :cond_0
    :goto_3
    iget-object v0, p0, LmK;->a:Landroid/widget/PopupWindow;

    iget-boolean v2, p0, LmK;->c:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, LmK;->b:Z

    if-nez v2, :cond_1

    move v1, v3

    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 611
    iget-object v0, p0, LmK;->a:Landroid/widget/PopupWindow;

    invoke-virtual {p0}, LmK;->a()Landroid/view/View;

    move-result-object v1

    iget v2, p0, LmK;->d:I

    iget v3, p0, LmK;->e:I

    invoke-virtual/range {v0 .. v5}, Landroid/widget/PopupWindow;->update(Landroid/view/View;IIII)V

    .line 652
    :cond_2
    :goto_4
    return-void

    .line 583
    :cond_3
    iget v4, p0, LmK;->c:I

    if-ne v4, v7, :cond_4

    .line 584
    invoke-virtual {p0}, LmK;->a()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    goto :goto_0

    .line 586
    :cond_4
    iget v4, p0, LmK;->c:I

    goto :goto_0

    :cond_5
    move v5, v0

    .line 592
    goto :goto_1

    :cond_6
    move v0, v1

    .line 594
    goto :goto_2

    .line 598
    :cond_7
    iget-object v6, p0, LmK;->a:Landroid/widget/PopupWindow;

    iget v2, p0, LmK;->c:I

    if-ne v2, v0, :cond_8

    move v2, v0

    :goto_5
    invoke-virtual {v6, v2, v0}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    goto :goto_3

    :cond_8
    move v2, v1

    goto :goto_5

    .line 603
    :cond_9
    iget v0, p0, LmK;->b:I

    if-eq v0, v7, :cond_0

    .line 606
    iget v5, p0, LmK;->b:I

    goto :goto_3

    .line 614
    :cond_a
    iget v2, p0, LmK;->c:I

    if-ne v2, v0, :cond_d

    move v2, v0

    .line 624
    :goto_6
    iget v4, p0, LmK;->b:I

    if-ne v4, v0, :cond_f

    move v4, v0

    .line 634
    :goto_7
    iget-object v5, p0, LmK;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v5, v2, v4}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    .line 635
    invoke-direct {p0, v3}, LmK;->b(Z)V

    .line 639
    iget-object v2, p0, LmK;->a:Landroid/widget/PopupWindow;

    iget-boolean v4, p0, LmK;->c:Z

    if-nez v4, :cond_11

    iget-boolean v4, p0, LmK;->b:Z

    if-nez v4, :cond_11

    :goto_8
    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 640
    iget-object v1, p0, LmK;->a:Landroid/widget/PopupWindow;

    iget-object v2, p0, LmK;->a:LmV;

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setTouchInterceptor(Landroid/view/View$OnTouchListener;)V

    .line 641
    iget-object v1, p0, LmK;->a:Landroid/widget/PopupWindow;

    invoke-virtual {p0}, LmK;->a()Landroid/view/View;

    move-result-object v2

    iget v3, p0, LmK;->d:I

    iget v4, p0, LmK;->e:I

    iget v5, p0, LmK;->f:I

    invoke-static {v1, v2, v3, v4, v5}, LgW;->a(Landroid/widget/PopupWindow;Landroid/view/View;III)V

    .line 643
    iget-object v1, p0, LmK;->a:LmO;

    invoke-virtual {v1, v0}, LmO;->setSelection(I)V

    .line 645
    iget-boolean v0, p0, LmK;->d:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, LmK;->a:LmO;

    invoke-virtual {v0}, LmO;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 646
    :cond_b
    invoke-virtual {p0}, LmK;->c()V

    .line 648
    :cond_c
    iget-boolean v0, p0, LmK;->d:Z

    if-nez v0, :cond_2

    .line 649
    iget-object v0, p0, LmK;->a:Landroid/os/Handler;

    iget-object v1, p0, LmK;->a:LmS;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_4

    .line 617
    :cond_d
    iget v2, p0, LmK;->c:I

    if-ne v2, v7, :cond_e

    .line 618
    iget-object v2, p0, LmK;->a:Landroid/widget/PopupWindow;

    invoke-virtual {p0}, LmK;->a()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/PopupWindow;->setWidth(I)V

    move v2, v1

    goto :goto_6

    .line 620
    :cond_e
    iget-object v2, p0, LmK;->a:Landroid/widget/PopupWindow;

    iget v4, p0, LmK;->c:I

    invoke-virtual {v2, v4}, Landroid/widget/PopupWindow;->setWidth(I)V

    move v2, v1

    goto :goto_6

    .line 627
    :cond_f
    iget v4, p0, LmK;->b:I

    if-ne v4, v7, :cond_10

    .line 628
    iget-object v4, p0, LmK;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v4, v5}, Landroid/widget/PopupWindow;->setHeight(I)V

    move v4, v1

    goto :goto_7

    .line 630
    :cond_10
    iget-object v4, p0, LmK;->a:Landroid/widget/PopupWindow;

    iget v5, p0, LmK;->b:I

    invoke-virtual {v4, v5}, Landroid/widget/PopupWindow;->setHeight(I)V

    move v4, v1

    goto :goto_7

    :cond_11
    move v3, v1

    .line 639
    goto :goto_8
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 468
    iput p1, p0, LmK;->f:I

    .line 469
    return-void
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 755
    iget-object v0, p0, LmK;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 734
    iget-object v0, p0, LmK;->a:LmO;

    .line 735
    if-eqz v0, :cond_0

    .line 737
    const/4 v1, 0x1

    invoke-static {v0, v1}, LmO;->a(LmO;Z)Z

    .line 739
    invoke-virtual {v0}, LmO;->requestLayout()V

    .line 741
    :cond_0
    return-void
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 485
    iput p1, p0, LmK;->c:I

    .line 486
    return-void
.end method

.method public d(I)V
    .locals 2

    .prologue
    .line 495
    iget-object v0, p0, LmK;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 496
    if-eqz v0, :cond_0

    .line 497
    iget-object v1, p0, LmK;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 498
    iget-object v0, p0, LmK;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, LmK;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    iput v0, p0, LmK;->c:I

    .line 502
    :goto_0
    return-void

    .line 500
    :cond_0
    invoke-virtual {p0, p1}, LmK;->c(I)V

    goto :goto_0
.end method

.method public e(I)V
    .locals 1

    .prologue
    .line 697
    iget-object v0, p0, LmK;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 698
    return-void
.end method
