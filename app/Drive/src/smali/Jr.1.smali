.class public LJr;
.super LJG;
.source "DriveWelcomeHelpCard.java"


# direct methods
.method public constructor <init>(LJc;)V
    .locals 7

    .prologue
    .line 22
    const-string v1, "DriveWelcomeHelpCard"

    sget v2, LpP;->drive_welcome_help_card:I

    sget v3, LpR;->help_card_drive_welcome_positive_button_label:I

    sget v4, LpM;->editors_nav_icon_drive_inactive:I

    const/4 v5, 0x0

    sget-object v6, LJb;->a:LJb;

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, LJc;->a(Ljava/lang/String;IIIZLJb;)LIU;

    move-result-object v0

    invoke-direct {p0, v0}, LJG;-><init>(LIU;)V

    .line 29
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 6

    .prologue
    .line 33
    invoke-virtual {p0}, LJr;->a()Landroid/content/Context;

    move-result-object v0

    .line 34
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 35
    invoke-virtual {p0}, LJr;->a()LaFO;

    move-result-object v2

    .line 36
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mailto:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 37
    const-string v3, "message/rfc822"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 38
    const/high16 v3, 0x10000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 39
    const-string v3, "android.intent.extra.SUBJECT"

    sget v4, LpR;->help_card_drive_welcome_content_title:I

    .line 40
    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 39
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 41
    const-string v3, "android.intent.extra.TEXT"

    const-string v4, "http://www.google.com"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 42
    const-string v3, "android.intent.extra.EMAIL"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v2}, LaFO;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 44
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 45
    return-void
.end method
