.class public abstract enum LAu;
.super Ljava/lang/Enum;
.source "CreateDocumentItemEnum.java"

# interfaces
.implements LAt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LAu;",
        ">;",
        "LAt;"
    }
.end annotation


# static fields
.field public static final enum a:LAu;

.field private static final synthetic a:[LAu;

.field public static final enum b:LAu;

.field public static final enum c:LAu;

.field public static final enum d:LAu;

.field public static final enum e:LAu;

.field public static final enum f:LAu;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 26
    new-instance v0, LAv;

    const-string v1, "UPLOAD"

    invoke-direct {v0, v1, v3}, LAv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LAu;->a:LAu;

    .line 39
    new-instance v0, LAw;

    const-string v1, "FOLDER"

    invoke-direct {v0, v1, v4}, LAw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LAu;->b:LAu;

    .line 52
    new-instance v0, LAx;

    const-string v1, "SCAN"

    invoke-direct {v0, v1, v5}, LAx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LAu;->c:LAu;

    .line 74
    new-instance v0, LAy;

    const-string v1, "DOCUMENT"

    invoke-direct {v0, v1, v6}, LAy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LAu;->d:LAu;

    .line 92
    new-instance v0, LAz;

    const-string v1, "SHEET"

    invoke-direct {v0, v1, v7}, LAz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LAu;->e:LAu;

    .line 110
    new-instance v0, LAA;

    const-string v1, "PRESENTATION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LAA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LAu;->f:LAu;

    .line 25
    const/4 v0, 0x6

    new-array v0, v0, [LAu;

    sget-object v1, LAu;->a:LAu;

    aput-object v1, v0, v3

    sget-object v1, LAu;->b:LAu;

    aput-object v1, v0, v4

    sget-object v1, LAu;->c:LAu;

    aput-object v1, v0, v5

    sget-object v1, LAu;->d:LAu;

    aput-object v1, v0, v6

    sget-object v1, LAu;->e:LAu;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LAu;->f:LAu;

    aput-object v2, v0, v1

    sput-object v0, LAu;->a:[LAu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILAv;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, LAu;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LAt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    invoke-static {}, LAu;->values()[LAu;

    move-result-object v0

    invoke-static {v0}, LbmF;->a([Ljava/lang/Object;)LbmF;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LAu;
    .locals 1

    .prologue
    .line 25
    const-class v0, LAu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LAu;

    return-object v0
.end method

.method public static values()[LAu;
    .locals 1

    .prologue
    .line 25
    sget-object v0, LAu;->a:[LAu;

    invoke-virtual {v0}, [LAu;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LAu;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;LTT;)Z
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x1

    return v0
.end method
