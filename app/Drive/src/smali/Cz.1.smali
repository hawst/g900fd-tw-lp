.class LCz;
.super Ljava/lang/Object;
.source "GroupingAdapterVisibleEntryViewLookup.java"

# interfaces
.implements LHp;


# instance fields
.field private final a:LHp;

.field private final a:Lbjv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjv",
            "<",
            "LBC;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LHp;Lbjv;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LHp;",
            "Lbjv",
            "<",
            "LBC;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHp;

    iput-object v0, p0, LCz;->a:LHp;

    .line 20
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjv;

    iput-object v0, p0, LCz;->a:Lbjv;

    .line 21
    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, LCz;->a:Lbjv;

    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBC;

    invoke-virtual {v0, p1}, LBC;->a(I)I

    move-result v0

    .line 26
    iget-object v1, p0, LCz;->a:LHp;

    invoke-interface {v1, v0}, LHp;->a(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 31
    const-class v0, LCz;

    invoke-static {v0}, LbiL;->a(Ljava/lang/Class;)LbiN;

    move-result-object v0

    const-string v1, "parent"

    iget-object v2, p0, LCz;->a:LHp;

    .line 32
    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;Ljava/lang/Object;)LbiN;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, LbiN;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
