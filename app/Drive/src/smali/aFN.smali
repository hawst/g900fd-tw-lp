.class public LaFN;
.super Ljava/lang/Object;
.source "AccountCache.java"


# instance fields
.field private final a:LbjF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbjF",
            "<",
            "LaFO;",
            "LaFM;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LbjF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbjF",
            "<",
            "Ljava/lang/Long;",
            "LaFM;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {}, LbjG;->a()LbjG;

    move-result-object v0

    invoke-virtual {v0, v1}, LbjG;->a(I)LbjG;

    move-result-object v0

    invoke-virtual {v0}, LbjG;->a()LbjF;

    move-result-object v0

    iput-object v0, p0, LaFN;->a:LbjF;

    .line 23
    invoke-static {}, LbjG;->a()LbjG;

    move-result-object v0

    invoke-virtual {v0, v1}, LbjG;->a(I)LbjG;

    move-result-object v0

    invoke-virtual {v0}, LbjG;->a()LbjF;

    move-result-object v0

    iput-object v0, p0, LaFN;->b:LbjF;

    .line 24
    return-void
.end method


# virtual methods
.method public a(J)LaFM;
    .locals 3

    .prologue
    .line 31
    iget-object v0, p0, LaFN;->b:LbjF;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, LbjF;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFM;

    return-object v0
.end method

.method public a(LaFO;)LaFM;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, LaFN;->a:LbjF;

    invoke-interface {v0, p1}, LbjF;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFM;

    return-object v0
.end method

.method public a(LaFM;)V
    .locals 4

    .prologue
    .line 35
    invoke-virtual {p1}, LaFM;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 36
    iget-object v0, p0, LaFN;->a:LbjF;

    invoke-virtual {p1}, LaFM;->a()LaFO;

    move-result-object v1

    invoke-interface {v0, v1, p1}, LbjF;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 37
    iget-object v0, p0, LaFN;->b:LbjF;

    invoke-virtual {p1}, LaFM;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, p1}, LbjF;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 38
    return-void

    .line 35
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(LaFM;)V
    .locals 4

    .prologue
    .line 41
    iget-object v0, p0, LaFN;->a:LbjF;

    invoke-virtual {p1}, LaFM;->a()LaFO;

    move-result-object v1

    invoke-interface {v0, v1}, LbjF;->a(Ljava/lang/Object;)V

    .line 42
    iget-object v0, p0, LaFN;->b:LbjF;

    invoke-virtual {p1}, LaFM;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, LbjF;->a(Ljava/lang/Object;)V

    .line 43
    return-void
.end method
