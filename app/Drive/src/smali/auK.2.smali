.class public final LauK;
.super Ljava/lang/Object;
.source "AuthenticatedUri.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/viewer/client/AuthenticatedUri;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/viewer/client/AuthenticatedUri;
    .locals 3

    .prologue
    .line 47
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 48
    const-class v1, Lcom/google/android/apps/viewer/client/TokenSource;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/viewer/client/TokenSource;

    .line 49
    new-instance v2, Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/viewer/client/AuthenticatedUri;-><init>(Landroid/net/Uri;Lcom/google/android/apps/viewer/client/TokenSource;)V

    return-object v2
.end method

.method public a(I)[Lcom/google/android/apps/viewer/client/AuthenticatedUri;
    .locals 1

    .prologue
    .line 42
    new-array v0, p1, [Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0, p1}, LauK;->a(Landroid/os/Parcel;)Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0, p1}, LauK;->a(I)[Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    move-result-object v0

    return-object v0
.end method
