.class public final Laic;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laig;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LahU;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laid;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LahY;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LagG;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laie;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 42
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 43
    iput-object p1, p0, Laic;->a:LbrA;

    .line 44
    const-class v0, Laig;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, Laic;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laic;->a:Lbsk;

    .line 47
    const-class v0, LahU;

    invoke-static {v0, v2}, Laic;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laic;->b:Lbsk;

    .line 50
    const-class v0, Laid;

    invoke-static {v0, v2}, Laic;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laic;->c:Lbsk;

    .line 53
    const-class v0, LahY;

    invoke-static {v0, v2}, Laic;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laic;->d:Lbsk;

    .line 56
    const-class v0, LagG;

    invoke-static {v0, v2}, Laic;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laic;->e:Lbsk;

    .line 59
    const-class v0, Laie;

    invoke-static {v0, v2}, Laic;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laic;->f:Lbsk;

    .line 62
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 95
    packed-switch p1, :pswitch_data_0

    .line 197
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :pswitch_1
    new-instance v2, Laig;

    iget-object v0, p0, Laic;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 100
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Laic;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 98
    invoke-static {v0, v1}, Laic;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iget-object v1, p0, Laic;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->K:Lbsk;

    .line 106
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, Laic;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->K:Lbsk;

    .line 104
    invoke-static {v1, v3}, Laic;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lalo;

    invoke-direct {v2, v0, v1}, Laig;-><init>(LQr;Lalo;)V

    move-object v0, v2

    .line 195
    :goto_0
    return-object v0

    .line 113
    :pswitch_2
    new-instance v3, LahU;

    iget-object v0, p0, Laic;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 116
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Laic;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 114
    invoke-static {v0, v1}, Laic;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iget-object v1, p0, Laic;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaoA;

    iget-object v1, v1, LaoA;->j:Lbsk;

    .line 122
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Laic;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaoA;

    iget-object v2, v2, LaoA;->j:Lbsk;

    .line 120
    invoke-static {v1, v2}, Laic;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laoj;

    iget-object v2, p0, Laic;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Laic;

    iget-object v2, v2, Laic;->f:Lbsk;

    .line 128
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, Laic;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Laic;

    iget-object v4, v4, Laic;->f:Lbsk;

    .line 126
    invoke-static {v2, v4}, Laic;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Laie;

    invoke-direct {v3, v0, v1, v2}, LahU;-><init>(LaGM;Laoj;Laie;)V

    move-object v0, v3

    .line 133
    goto :goto_0

    .line 135
    :pswitch_3
    new-instance v4, Laid;

    iget-object v0, p0, Laic;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LadM;

    iget-object v0, v0, LadM;->m:Lbsk;

    .line 138
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Laic;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LadM;

    iget-object v1, v1, LadM;->m:Lbsk;

    .line 136
    invoke-static {v0, v1}, Laic;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladi;

    iget-object v1, p0, Laic;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->N:Lbsk;

    .line 144
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Laic;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LalC;

    iget-object v2, v2, LalC;->N:Lbsk;

    .line 142
    invoke-static {v1, v2}, Laic;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LamL;

    iget-object v2, p0, Laic;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Laic;

    iget-object v2, v2, Laic;->f:Lbsk;

    .line 150
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Laic;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Laic;

    iget-object v3, v3, Laic;->f:Lbsk;

    .line 148
    invoke-static {v2, v3}, Laic;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Laie;

    iget-object v3, p0, Laic;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Laic;

    iget-object v3, v3, Laic;->b:Lbsk;

    .line 156
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, Laic;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Laic;

    iget-object v5, v5, Laic;->b:Lbsk;

    .line 154
    invoke-static {v3, v5}, Laic;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LahU;

    invoke-direct {v4, v0, v1, v2, v3}, Laid;-><init>(Ladi;LamL;Laie;LahU;)V

    move-object v0, v4

    .line 161
    goto/16 :goto_0

    .line 163
    :pswitch_4
    new-instance v0, LahY;

    iget-object v1, p0, Laic;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 166
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Laic;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaGH;

    iget-object v2, v2, LaGH;->l:Lbsk;

    .line 164
    invoke-static {v1, v2}, Laic;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGM;

    iget-object v2, p0, Laic;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LadM;

    iget-object v2, v2, LadM;->m:Lbsk;

    .line 172
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Laic;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LadM;

    iget-object v3, v3, LadM;->m:Lbsk;

    .line 170
    invoke-static {v2, v3}, Laic;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ladi;

    iget-object v3, p0, Laic;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->N:Lbsk;

    .line 178
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, Laic;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LalC;

    iget-object v4, v4, LalC;->N:Lbsk;

    .line 176
    invoke-static {v3, v4}, Laic;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LamL;

    iget-object v4, p0, Laic;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Laic;

    iget-object v4, v4, Laic;->f:Lbsk;

    .line 184
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, Laic;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Laic;

    iget-object v5, v5, Laic;->f:Lbsk;

    .line 182
    invoke-static {v4, v5}, Laic;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Laie;

    iget-object v5, p0, Laic;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Laic;

    iget-object v5, v5, Laic;->b:Lbsk;

    .line 190
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, Laic;->a:LbrA;

    iget-object v6, v6, LbrA;->a:Laic;

    iget-object v6, v6, Laic;->b:Lbsk;

    .line 188
    invoke-static {v5, v6}, Laic;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LahU;

    invoke-direct/range {v0 .. v5}, LahY;-><init>(LaGM;Ladi;LamL;Laie;LahU;)V

    goto/16 :goto_0

    .line 95
    nop

    :pswitch_data_0
    .packed-switch 0x14e
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 212
    sparse-switch p2, :sswitch_data_0

    .line 236
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 214
    :sswitch_0
    check-cast p1, Laib;

    .line 216
    iget-object v0, p0, Laic;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laic;

    iget-object v0, v0, Laic;->d:Lbsk;

    .line 219
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahY;

    iget-object v1, p0, Laic;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LbiH;

    iget-object v1, v1, LbiH;->X:Lbsk;

    .line 223
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LbiP;

    .line 216
    invoke-virtual {p1, v0, v1}, Laib;->provideFileDownloader(LahY;LbiP;)LagG;

    move-result-object v0

    .line 229
    :goto_0
    return-object v0

    .line 227
    :sswitch_1
    check-cast p1, Laib;

    .line 229
    iget-object v0, p0, Laic;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laic;

    iget-object v0, v0, Laic;->a:Lbsk;

    .line 232
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laig;

    .line 229
    invoke-virtual {p1, v0}, Laib;->providePartialDownloadedFileStore(Laig;)Laie;

    move-result-object v0

    goto :goto_0

    .line 212
    :sswitch_data_0
    .sparse-switch
        0xb9 -> :sswitch_0
        0x153 -> :sswitch_1
    .end sparse-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 69
    const-class v0, Laig;

    iget-object v1, p0, Laic;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, Laic;->a(Ljava/lang/Class;Lbsk;)V

    .line 70
    const-class v0, LahU;

    iget-object v1, p0, Laic;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, Laic;->a(Ljava/lang/Class;Lbsk;)V

    .line 71
    const-class v0, Laid;

    iget-object v1, p0, Laic;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, Laic;->a(Ljava/lang/Class;Lbsk;)V

    .line 72
    const-class v0, LahY;

    iget-object v1, p0, Laic;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, Laic;->a(Ljava/lang/Class;Lbsk;)V

    .line 73
    const-class v0, LagG;

    iget-object v1, p0, Laic;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, Laic;->a(Ljava/lang/Class;Lbsk;)V

    .line 74
    const-class v0, Laie;

    iget-object v1, p0, Laic;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, Laic;->a(Ljava/lang/Class;Lbsk;)V

    .line 75
    iget-object v0, p0, Laic;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x14e

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 77
    iget-object v0, p0, Laic;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x151

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 79
    iget-object v0, p0, Laic;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x154

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 81
    iget-object v0, p0, Laic;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x14f

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 83
    iget-object v0, p0, Laic;->e:Lbsk;

    const-class v1, Laib;

    const/16 v2, 0xb9

    invoke-virtual {p0, v1, v2}, Laic;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 85
    iget-object v0, p0, Laic;->f:Lbsk;

    const-class v1, Laib;

    const/16 v2, 0x153

    invoke-virtual {p0, v1, v2}, Laic;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 87
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 204
    .line 206
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 91
    return-void
.end method
