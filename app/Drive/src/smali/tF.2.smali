.class public LtF;
.super Ljava/lang/Object;
.source "ErrorNotificationActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/app/ErrorNotificationActivity;

.field final synthetic a:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/app/ErrorNotificationActivity;Z)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, LtF;->a:Lcom/google/android/apps/docs/app/ErrorNotificationActivity;

    iput-boolean p2, p0, LtF;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 107
    new-instance v1, Lali;

    iget-object v2, p0, LtF;->a:Lcom/google/android/apps/docs/app/ErrorNotificationActivity;

    iget-boolean v0, p0, LtF;->a:Z

    if-eqz v0, :cond_2

    const-string v0, "android_docs_dump"

    :goto_0
    invoke-direct {v1, v2, v0}, Lali;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, LtF;->a:Lcom/google/android/apps/docs/app/ErrorNotificationActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a(Lcom/google/android/apps/docs/app/ErrorNotificationActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 111
    const-string v0, "stack_trace"

    iget-object v2, p0, LtF;->a:Lcom/google/android/apps/docs/app/ErrorNotificationActivity;

    invoke-static {v2}, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a(Lcom/google/android/apps/docs/app/ErrorNotificationActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lali;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    :cond_0
    iget-boolean v0, p0, LtF;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LtF;->a:Lcom/google/android/apps/docs/app/ErrorNotificationActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:LaEz;

    if-eqz v0, :cond_1

    .line 114
    iget-object v0, p0, LtF;->a:Lcom/google/android/apps/docs/app/ErrorNotificationActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:LaEz;

    invoke-virtual {v0}, LaEz;->a()Ljava/lang/String;

    move-result-object v0

    .line 116
    const-string v2, "dumpDatabase"

    invoke-virtual {v1, v2, v0}, Lali;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_1
    iget-object v0, p0, LtF;->a:Lcom/google/android/apps/docs/app/ErrorNotificationActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:Lall;

    invoke-interface {v0, v1}, Lall;->a(Lali;)V

    .line 119
    iget-object v0, p0, LtF;->a:Lcom/google/android/apps/docs/app/ErrorNotificationActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->finish()V

    .line 120
    return-void

    .line 107
    :cond_2
    const-string v0, "android_docs"

    goto :goto_0
.end method
