.class public final LaKX;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laja",
            "<",
            "LaKM;",
            ">;>;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lajw",
            "<",
            "LaKM;",
            ">;>;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lajw",
            "<",
            "LaKM;",
            ">;>;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laja",
            "<",
            "LaKM;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 40
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 41
    iput-object p1, p0, LaKX;->a:LbrA;

    .line 42
    const-class v0, Laja;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LaKM;

    aput-object v2, v1, v3

    .line 43
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "RealtimeClock"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 42
    invoke-static {v0, v5}, LaKX;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaKX;->a:Lbsk;

    .line 45
    const-class v0, Lajw;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LaKM;

    aput-object v2, v1, v3

    .line 46
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "UptimeClock"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 45
    invoke-static {v0, v5}, LaKX;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaKX;->b:Lbsk;

    .line 48
    const-class v0, Lajw;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LaKM;

    aput-object v2, v1, v3

    .line 49
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "RealtimeClock"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 48
    invoke-static {v0, v5}, LaKX;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaKX;->c:Lbsk;

    .line 51
    const-class v0, Laja;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LaKM;

    aput-object v2, v1, v3

    .line 52
    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "UptimeClock"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 51
    invoke-static {v0, v5}, LaKX;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaKX;->d:Lbsk;

    .line 54
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 81
    .line 83
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 98
    packed-switch p2, :pswitch_data_0

    .line 152
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :pswitch_0
    check-cast p1, LaKT;

    .line 102
    iget-object v0, p0, LaKX;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v2, v0, LalC;->z:Lbsk;

    iget-object v0, p0, LaKX;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->a:Lbsk;

    .line 109
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiU;

    iget-object v1, p0, LaKX;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->c:Lbsk;

    .line 113
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaiW;

    .line 102
    invoke-virtual {p1, v2, v0, v1}, LaKT;->get1(Lbxw;LaiU;LaiW;)Laja;

    move-result-object v0

    .line 137
    :goto_0
    return-object v0

    .line 117
    :pswitch_1
    check-cast p1, LaKT;

    .line 119
    iget-object v0, p0, LaKX;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaKX;

    iget-object v0, v0, LaKX;->d:Lbsk;

    .line 122
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    .line 119
    invoke-virtual {p1, v0}, LaKT;->getLazy2(Laja;)Lajw;

    move-result-object v0

    goto :goto_0

    .line 126
    :pswitch_2
    check-cast p1, LaKT;

    .line 128
    iget-object v0, p0, LaKX;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaKX;

    iget-object v0, v0, LaKX;->a:Lbsk;

    .line 131
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    .line 128
    invoke-virtual {p1, v0}, LaKT;->getLazy1(Laja;)Lajw;

    move-result-object v0

    goto :goto_0

    .line 135
    :pswitch_3
    check-cast p1, LaKT;

    .line 137
    iget-object v0, p0, LaKX;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v2, v0, LalC;->H:Lbsk;

    iget-object v0, p0, LaKX;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->a:Lbsk;

    .line 144
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiU;

    iget-object v1, p0, LaKX;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->c:Lbsk;

    .line 148
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaiW;

    .line 137
    invoke-virtual {p1, v2, v0, v1}, LaKT;->get2(Lbxw;LaiU;LaiW;)Laja;

    move-result-object v0

    goto :goto_0

    .line 98
    :pswitch_data_0
    .packed-switch 0x29f
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 61
    const-class v0, Laja;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LaKM;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "RealtimeClock"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LaKX;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LaKX;->a(Lbuv;Lbsk;)V

    .line 62
    const-class v0, Lajw;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LaKM;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "UptimeClock"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LaKX;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LaKX;->a(Lbuv;Lbsk;)V

    .line 63
    const-class v0, Lajw;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LaKM;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "RealtimeClock"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LaKX;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LaKX;->a(Lbuv;Lbsk;)V

    .line 64
    const-class v0, Laja;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, LaKM;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LbwL;->a(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;

    move-result-object v0

    invoke-static {v0}, LbuP;->a(Ljava/lang/reflect/Type;)LbuP;

    move-result-object v0

    new-instance v1, Lbwl;

    const-string v2, "UptimeClock"

    invoke-direct {v1, v2}, Lbwl;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LaKX;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LaKX;->a(Lbuv;Lbsk;)V

    .line 65
    iget-object v0, p0, LaKX;->a:Lbsk;

    const-class v1, LaKT;

    const/16 v2, 0x29f

    invoke-virtual {p0, v1, v2}, LaKX;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 67
    iget-object v0, p0, LaKX;->b:Lbsk;

    const-class v1, LaKT;

    const/16 v2, 0x2a0

    invoke-virtual {p0, v1, v2}, LaKX;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 69
    iget-object v0, p0, LaKX;->c:Lbsk;

    const-class v1, LaKT;

    const/16 v2, 0x2a2

    invoke-virtual {p0, v1, v2}, LaKX;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 71
    iget-object v0, p0, LaKX;->d:Lbsk;

    const-class v1, LaKT;

    const/16 v2, 0x2a1

    invoke-virtual {p0, v1, v2}, LaKX;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 73
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 90
    .line 92
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method
