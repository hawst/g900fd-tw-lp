.class public Lajs;
.super Landroid/preference/PreferenceActivity;
.source "GuicePreferenceActivity.java"

# interfaces
.implements Laju;


# instance fields
.field private a:LaiN;

.field protected a:LaiU;

.field private a:Lbuu;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lajs;->a:LaiN;

    return-void
.end method


# virtual methods
.method public a()Lbuu;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lajs;->a:Lbuu;

    invoke-static {v0}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbuu;

    return-object v0
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 32
    invoke-static {p0}, LaiZ;->b(Landroid/content/Context;)LaiY;

    move-result-object v0

    iput-object v0, p0, Lajs;->a:Lbuu;

    .line 33
    iget-object v0, p0, Lajs;->a:Lbuu;

    const-class v1, LaiG;

    invoke-interface {v0, v1}, Lbuu;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiG;

    .line 34
    invoke-interface {v0, p0}, LaiG;->a(Landroid/app/Activity;)LaiN;

    move-result-object v0

    iput-object v0, p0, Lajs;->a:LaiN;

    .line 35
    iget-object v0, p0, Lajs;->a:LaiN;

    invoke-interface {v0, p1}, LaiN;->a(Landroid/os/Bundle;)V

    .line 36
    iget-object v0, p0, Lajs;->a:Lbuu;

    const-class v1, LaiU;

    invoke-interface {v0, v1}, Lbuu;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiU;

    iput-object v0, p0, Lajs;->a:LaiU;

    .line 37
    iget-object v0, p0, Lajs;->a:Lbuu;

    invoke-interface {v0, p0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 38
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lajs;->a(Landroid/os/Bundle;)V

    .line 28
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 67
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 68
    iget-object v0, p0, Lajs;->a:LaiN;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lajs;->a:LaiN;

    invoke-interface {v0}, LaiN;->b()V

    .line 74
    :cond_0
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 42
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 43
    iget-object v0, p0, Lajs;->a:LaiN;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lajs;->a:LaiN;

    invoke-interface {v0}, LaiN;->a()V

    .line 46
    :cond_0
    return-void
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lajs;->a:LaiN;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lajs;->a:LaiN;

    invoke-interface {v0}, LaiN;->a()Ljava/lang/Object;

    move-result-object v0

    .line 61
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onRetainNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 50
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 51
    iget-object v0, p0, Lajs;->a:LaiN;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lajs;->a:LaiN;

    invoke-interface {v0, p1}, LaiN;->b(Landroid/os/Bundle;)V

    .line 54
    :cond_0
    return-void
.end method
