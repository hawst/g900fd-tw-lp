.class public LJX;
.super Ljava/lang/Object;
.source "SelectionModelImpl.java"

# interfaces
.implements Lcom/google/android/apps/docs/doclist/selection/SelectionModel;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K::",
        "Landroid/os/Parcelable;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/docs/doclist/selection/SelectionModel",
        "<TK;>;"
    }
.end annotation


# instance fields
.field private a:I

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<TK;>;",
            "LJY",
            "<TK;>;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LKe",
            "<TK;>;>;"
        }
    .end annotation
.end field

.field private a:Z

.field private b:I

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<TK;>;",
            "LJY",
            "<TK;>;>;"
        }
    .end annotation
.end field

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-static {}, LboS;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LJX;->a:Ljava/util/Map;

    .line 29
    invoke-static {}, LboS;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LJX;->b:Ljava/util/Map;

    .line 30
    iput v1, p0, LJX;->a:I

    .line 31
    iput v1, p0, LJX;->b:I

    .line 32
    iput-boolean v1, p0, LJX;->a:Z

    .line 33
    iput-boolean v1, p0, LJX;->b:Z

    .line 35
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LJX;->a:Ljava/util/Set;

    return-void
.end method

.method private a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;)LJY;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<TK;>;)",
            "LJY",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 39
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    iget-object v0, p0, LJX;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJY;

    .line 41
    if-nez v0, :cond_0

    .line 42
    new-instance v0, LJY;

    invoke-direct {v0, p1, p0}, LJY;-><init>(Lcom/google/android/apps/docs/doclist/selection/ItemKey;LJX;)V

    .line 43
    iget-object v1, p0, LJX;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    :cond_0
    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 70
    iget-object v0, p0, LJX;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJY;

    .line 71
    invoke-virtual {v0, v1}, LJY;->a(Z)V

    goto :goto_0

    .line 73
    :cond_0
    iget v0, p0, LJX;->a:I

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LbiT;->b(Z)V

    .line 74
    return-void

    :cond_1
    move v0, v1

    .line 73
    goto :goto_1
.end method

.method private f()V
    .locals 3

    .prologue
    .line 135
    iget-object v0, p0, LJX;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    .line 136
    iget-object v1, p0, LJX;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 137
    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJY;

    .line 138
    invoke-virtual {v0}, LJY;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 139
    iget-object v2, p0, LJX;->a:Ljava/util/Map;

    invoke-virtual {v0}, LJY;->a()Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 142
    :cond_1
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 161
    iget-boolean v0, p0, LJX;->b:Z

    if-nez v0, :cond_1

    .line 174
    :cond_0
    return-void

    .line 164
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LJX;->b:Z

    .line 168
    iget-object v0, p0, LJX;->a:Ljava/util/Set;

    invoke-static {v0}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    .line 171
    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKe;

    .line 172
    invoke-interface {v0}, LKe;->c_()V

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, LJX;->a:I

    return v0
.end method

.method public a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;)LKb;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<TK;>;)",
            "LKb;"
        }
    .end annotation

    .prologue
    .line 196
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    iget-object v0, p0, LJX;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJY;

    .line 198
    if-nez v0, :cond_0

    .line 199
    const/4 v0, 0x0

    .line 201
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, LJY;->a()LKb;

    move-result-object v0

    goto :goto_0
.end method

.method public a()LbmF;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<TK;>;>;"
        }
    .end annotation

    .prologue
    .line 50
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v1

    .line 51
    iget-object v0, p0, LJX;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJY;

    .line 52
    invoke-virtual {v0}, LJY;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 53
    invoke-virtual {v0}, LJY;->a()Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    move-result-object v0

    invoke-virtual {v1, v0}, LbmH;->a(Ljava/lang/Object;)LbmH;

    goto :goto_0

    .line 56
    :cond_1
    invoke-virtual {v1}, LbmH;->a()LbmF;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, LJX;->b()V

    .line 95
    :try_start_0
    invoke-direct {p0}, LJX;->e()V

    .line 96
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LJX;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    invoke-virtual {p0}, LJX;->c()V

    .line 100
    return-void

    .line 98
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LJX;->c()V

    throw v0
.end method

.method a(LJY;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LJY",
            "<TK;>;)V"
        }
    .end annotation

    .prologue
    .line 130
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    iget-object v0, p0, LJX;->b:Ljava/util/Map;

    invoke-virtual {p1}, LJY;->a()Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    return-void
.end method

.method public a(LKe;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LKe",
            "<TK;>;)V"
        }
    .end annotation

    .prologue
    .line 183
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    iget-object v0, p0, LJX;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 185
    invoke-interface {p1}, LKe;->c_()V

    .line 186
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;LKc;Lcom/google/android/apps/docs/doclist/selection/ItemKey;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<TK;>;",
            "LKc;",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<TK;>;Z)V"
        }
    .end annotation

    .prologue
    .line 236
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    invoke-direct {p0, p1}, LJX;->a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;)LJY;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4}, LJY;->a(LKc;Lcom/google/android/apps/docs/doclist/selection/ItemKey;Z)V

    .line 239
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;LKd;Lcom/google/android/apps/docs/doclist/selection/ItemKey;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<TK;>;",
            "LKd;",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<TK;>;Z)V"
        }
    .end annotation

    .prologue
    .line 227
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    invoke-direct {p0, p1}, LJX;->a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;)LJY;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4}, LJY;->a(LKd;Lcom/google/android/apps/docs/doclist/selection/ItemKey;Z)V

    .line 230
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<TK;>;Z)V"
        }
    .end annotation

    .prologue
    .line 206
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    invoke-virtual {p0}, LJX;->b()V

    .line 209
    if-nez p2, :cond_1

    .line 210
    :try_start_0
    iget-object v0, p0, LJX;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJY;

    .line 211
    if-eqz v0, :cond_0

    .line 212
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LJY;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 219
    :cond_0
    :goto_0
    invoke-virtual {p0}, LJX;->c()V

    .line 221
    return-void

    .line 215
    :cond_1
    :try_start_1
    invoke-direct {p0, p1}, LJX;->a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;)LJY;

    move-result-object v0

    .line 216
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LJY;->a(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 219
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LJX;->c()V

    throw v0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 114
    iget-boolean v0, p0, LJX;->a:Z

    if-eq v0, p1, :cond_0

    .line 115
    iput-boolean p1, p0, LJX;->a:Z

    .line 116
    invoke-virtual {p0}, LJX;->b()V

    .line 118
    :try_start_0
    invoke-virtual {p0}, LJX;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    invoke-virtual {p0}, LJX;->c()V

    .line 123
    :cond_0
    return-void

    .line 120
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LJX;->c()V

    throw v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 66
    iget v0, p0, LJX;->a:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<TK;>;)Z"
        }
    .end annotation

    .prologue
    .line 243
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    :goto_0
    if-eqz p1, :cond_1

    .line 246
    iget-object v0, p0, LJX;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LJY;

    .line 247
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LJY;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    const/4 v0, 0x1

    .line 252
    :goto_1
    return v0

    .line 250
    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/docs/doclist/selection/ItemKey;->a()Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    move-result-object p1

    goto :goto_0

    .line 252
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public b()V
    .locals 1

    .prologue
    .line 146
    iget v0, p0, LJX;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LJX;->b:I

    .line 147
    return-void
.end method

.method public b(LKe;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LKe",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 190
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    iget-object v0, p0, LJX;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 192
    return-void
.end method

.method b(Z)V
    .locals 2

    .prologue
    .line 276
    iget v1, p0, LJX;->a:I

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    iput v0, p0, LJX;->a:I

    .line 277
    return-void

    .line 276
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 178
    iget-boolean v0, p0, LJX;->a:Z

    return v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 151
    iget v0, p0, LJX;->b:I

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 152
    iget v0, p0, LJX;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LJX;->b:I

    if-nez v0, :cond_2

    .line 153
    :goto_1
    iget-object v0, p0, LJX;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LJX;->b:Z

    if-eqz v0, :cond_2

    .line 154
    :cond_0
    invoke-direct {p0}, LJX;->f()V

    .line 155
    invoke-direct {p0}, LJX;->g()V

    goto :goto_1

    .line 151
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 158
    :cond_2
    return-void
.end method

.method d()V
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x1

    iput-boolean v0, p0, LJX;->b:Z

    .line 127
    return-void
.end method
