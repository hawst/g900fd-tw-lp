.class public LUJ;
.super Ljava/lang/Object;
.source "OpenUrlAliasManager.java"


# instance fields
.field private final a:LUI;

.field private final a:Landroid/content/Context;

.field private final a:Landroid/content/pm/PackageManager;

.field private final a:LtK;


# direct methods
.method constructor <init>(Landroid/content/Context;LtK;Landroid/content/pm/PackageManager;LUI;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lajg;
        .end annotation
    .end param

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, LUJ;->a:Landroid/content/Context;

    .line 43
    iput-object p2, p0, LUJ;->a:LtK;

    .line 44
    iput-object p3, p0, LUJ;->a:Landroid/content/pm/PackageManager;

    .line 45
    iput-object p4, p0, LUJ;->a:LUI;

    .line 46
    return-void
.end method

.method private a()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/PackageInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 92
    iget-object v0, p0, LUJ;->a:LUI;

    invoke-interface {v0}, LUI;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 95
    :try_start_0
    iget-object v3, p0, LUJ;->a:Landroid/content/pm/PackageManager;

    const/4 v4, 0x1

    invoke-virtual {v3, v0, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 100
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 103
    :cond_0
    invoke-static {v1}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    return-object v0

    .line 96
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/PackageInfo;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/PackageInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 127
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    .line 128
    invoke-direct {p0, p1, v0}, LUJ;->a(Ljava/lang/String;Landroid/content/pm/PackageInfo;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 129
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 133
    :cond_1
    invoke-static {v1}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/util/List;Ljava/util/Set;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/PackageInfo;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 138
    move v1, v2

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 139
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    .line 140
    if-nez v1, :cond_0

    const/4 v3, 0x1

    .line 141
    :goto_1
    iget-object v4, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-direct {p0, p1, v4}, LUJ;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    .line 142
    if-ne v3, v4, :cond_1

    .line 138
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v3, v2

    .line 140
    goto :goto_1

    .line 147
    :cond_1
    iget-object v4, p0, LUJ;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 149
    invoke-direct {p0, p1, v3}, LUJ;->a(Ljava/lang/String;Z)V

    goto :goto_2

    .line 153
    :cond_2
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {p3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 156
    :cond_3
    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    .line 177
    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, p0, LUJ;->a:Landroid/content/Context;

    invoke-direct {v2, v3, p1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 179
    const-string v3, "OpenUrlActivityAliasManager"

    const-string v4, "Updating alias setting for: %s enabled = %s"

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v3, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 181
    iget-object v3, p0, LUJ;->a:Landroid/content/pm/PackageManager;

    if-eqz p2, :cond_0

    move v0, v1

    :cond_0
    invoke-virtual {v3, v2, v0, v1}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 184
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/content/pm/PackageInfo;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 107
    if-nez p2, :cond_1

    .line 121
    :cond_0
    :goto_0
    return v0

    .line 111
    :cond_1
    iget-object v2, p2, Landroid/content/pm/PackageInfo;->activities:[Landroid/content/pm/ActivityInfo;

    .line 112
    if-eqz v2, :cond_0

    .line 115
    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 116
    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 117
    const/4 v0, 0x1

    goto :goto_0

    .line 115
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 159
    new-instance v3, Landroid/content/ComponentName;

    invoke-direct {v3, p2, p1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    :try_start_0
    iget-object v2, p0, LUJ;->a:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 171
    if-eqz v2, :cond_0

    if-ne v2, v1, :cond_1

    :cond_0
    move v0, v1

    .line 173
    :cond_1
    :goto_0
    return v0

    .line 163
    :catch_0
    move-exception v2

    .line 165
    const-string v4, "OpenUrlActivityAliasManager"

    const-string v5, "Cannot retrieved enable settings for %s. Maybe the app has been uninstalled?"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v0

    invoke-static {v4, v2, v5, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 49
    iget-object v0, p0, LUJ;->a:LtK;

    sget-object v1, Lry;->aM:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 54
    :cond_1
    invoke-direct {p0}, LUJ;->a()Ljava/util/List;

    move-result-object v2

    .line 55
    const/4 v1, 0x0

    .line 56
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    .line 57
    iget-object v4, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    iget-object v5, p0, LUJ;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    :goto_2
    move-object v1, v0

    .line 60
    goto :goto_1

    .line 62
    :cond_2
    if-nez v1, :cond_3

    .line 64
    const-string v0, "OpenUrlActivityAliasManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid package configuration: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LUJ;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 69
    :cond_3
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v3

    .line 71
    iget-object v0, p0, LUJ;->a:LUI;

    invoke-interface {v0}, LUI;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 72
    invoke-direct {p0, v0, v1}, LUJ;->a(Ljava/lang/String;Landroid/content/pm/PackageInfo;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 78
    invoke-direct {p0, v0, v2}, LUJ;->a(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    .line 79
    invoke-direct {p0, v0, v5, v3}, LUJ;->a(Ljava/lang/String;Ljava/util/List;Ljava/util/Set;)V

    goto :goto_3

    .line 82
    :cond_5
    iget-object v0, p0, LUJ;->a:LtK;

    sget-object v1, Lry;->aN:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 85
    iget-object v2, p0, LUJ;->a:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/google/android/apps/docs/receivers/AppPackageAddRemoveReceiver;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_4

    :cond_6
    move-object v0, v1

    goto :goto_2
.end method
