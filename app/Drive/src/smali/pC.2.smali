.class LpC;
.super LpI;
.source "ApplicationModule.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LpI",
        "<",
        "Ljava/lang/Class",
        "<+",
        "Landroid/app/Activity;",
        ">;>;"
    }
.end annotation


# instance fields
.field a:LbiP;
    .annotation runtime Lbxv;
        a = "StarDriveActivityOverride"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiP",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 91
    const-class v0, Lcom/google/android/apps/docs/app/DocListActivity;

    const-class v1, Lcom/google/android/apps/docs/app/DocListActivity;

    invoke-direct {p0, p1, v0, v1}, LpI;-><init>(Landroid/content/Context;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 92
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, LpC;->a:LbiP;

    invoke-virtual {v0}, LbiP;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, LpC;->a:LbiP;

    invoke-virtual {v0}, LbiP;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 99
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, LpI;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    goto :goto_0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0}, LpC;->a()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method
