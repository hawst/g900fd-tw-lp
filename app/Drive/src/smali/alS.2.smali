.class public final LalS;
.super Ljava/lang/Object;
.source "ListRange.java"


# instance fields
.field private final a:I

.field private final b:I


# direct methods
.method private constructor <init>(II)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput p1, p0, LalS;->a:I

    .line 19
    iput p2, p0, LalS;->b:I

    .line 20
    return-void
.end method

.method public static a(II)LalS;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 23
    if-gt p0, p1, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "min %s > max %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-static {v0, v3, v4}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 24
    new-instance v0, LalS;

    sub-int v1, p1, p0

    invoke-direct {v0, p0, v1}, LalS;-><init>(II)V

    return-object v0

    :cond_0
    move v0, v2

    .line 23
    goto :goto_0
.end method

.method public static b(II)LalS;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 28
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "size %s is less than zero"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 29
    new-instance v0, LalS;

    invoke-direct {v0, p0, p1}, LalS;-><init>(II)V

    return-object v0

    :cond_0
    move v0, v2

    .line 28
    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, LalS;->a:I

    return v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 45
    iget v0, p0, LalS;->b:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 2

    .prologue
    .line 41
    iget v0, p0, LalS;->a:I

    iget v1, p0, LalS;->b:I

    add-int/2addr v0, v1

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, LalS;->b:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 54
    if-ne p0, p1, :cond_1

    .line 60
    :cond_0
    :goto_0
    return v0

    .line 56
    :cond_1
    instance-of v2, p1, LalS;

    if-nez v2, :cond_2

    move v0, v1

    .line 57
    goto :goto_0

    .line 59
    :cond_2
    check-cast p1, LalS;

    .line 60
    iget v2, p0, LalS;->a:I

    iget v3, p1, LalS;->a:I

    if-ne v2, v3, :cond_3

    iget v2, p0, LalS;->b:I

    iget v3, p1, LalS;->b:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 66
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, LalS;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, LalS;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 71
    const-string v0, "ListRange"

    invoke-static {v0}, LbiL;->a(Ljava/lang/String;)LbiN;

    move-result-object v0

    const-string v1, "min"

    iget v2, p0, LalS;->a:I

    .line 72
    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;I)LbiN;

    move-result-object v0

    const-string v1, "size"

    iget v2, p0, LalS;->b:I

    .line 73
    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;I)LbiN;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, LbiN;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
