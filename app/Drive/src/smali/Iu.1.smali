.class public final LIu;
.super Ljava/lang/Object;
.source "FoldersThenTitleGrouper.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "LIu;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final a:Ljava/text/Collator;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/Locale;Ljava/text/Collator;)V
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    iput-object p3, p0, LIu;->a:Ljava/text/Collator;

    .line 100
    sget-object v0, Lbih;->n:Lbih;

    invoke-virtual {v0, p1}, Lbih;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LIu;->b:Ljava/lang/String;

    .line 101
    iget-object v0, p0, LIu;->b:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LIu;->a:Ljava/lang/String;

    .line 102
    return-void
.end method


# virtual methods
.method public a(LIu;)I
    .locals 3

    .prologue
    .line 106
    iget-object v0, p0, LIu;->a:Ljava/text/Collator;

    iget-object v1, p0, LIu;->a:Ljava/lang/String;

    iget-object v2, p1, LIu;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 107
    if-nez v0, :cond_0

    .line 108
    iget-object v0, p0, LIu;->a:Ljava/text/Collator;

    iget-object v1, p0, LIu;->b:Ljava/lang/String;

    iget-object v2, p1, LIu;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 110
    :cond_0
    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 93
    check-cast p1, LIu;

    invoke-virtual {p0, p1}, LIu;->a(LIu;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 115
    const-class v0, LIu;

    invoke-static {v0}, LbiL;->a(Ljava/lang/Class;)LbiN;

    move-result-object v0

    const-string v1, "upperTitle"

    iget-object v2, p0, LIu;->a:Ljava/lang/String;

    .line 116
    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;Ljava/lang/Object;)LbiN;

    move-result-object v0

    const-string v1, "trimmedTitle"

    iget-object v2, p0, LIu;->b:Ljava/lang/String;

    .line 117
    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;Ljava/lang/Object;)LbiN;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, LbiN;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
