.class public LaaZ;
.super LaGN;
.source "UploadSharedItemActivityDelegate.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGN",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic a:LaFO;

.field final synthetic a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;LaFO;I)V
    .locals 0

    .prologue
    .line 983
    iput-object p1, p0, LaaZ;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iput-object p2, p0, LaaZ;->a:LaFO;

    iput p3, p0, LaaZ;->a:I

    invoke-direct {p0}, LaGN;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(LaGM;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 983
    invoke-virtual {p0, p1}, LaaZ;->a(LaGM;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(LaGM;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 986
    iget-object v0, p0, LaaZ;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    iget-object v1, p0, LaaZ;->a:LaFO;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;LaFO;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 988
    iget-object v1, p0, LaaZ;->a:LaFO;

    invoke-interface {p1, v1}, LaGM;->a(LaFO;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    .line 989
    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 990
    :cond_0
    iget-object v0, p0, LaaZ;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)Ljava/lang/String;

    move-result-object v0

    .line 999
    :goto_0
    return-object v0

    .line 992
    :cond_1
    invoke-interface {p1, v0}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFV;

    move-result-object v0

    .line 993
    if-nez v0, :cond_2

    .line 994
    iget-object v0, p0, LaaZ;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-static {v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 996
    :cond_2
    invoke-interface {v0}, LaFV;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 983
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, LaaZ;->a(Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1004
    iget-object v0, p0, LaaZ;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxg;->upload_toast_message:I

    iget v2, p0, LaaZ;->a:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, LaaZ;->a:I

    .line 1005
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    aput-object p1, v3, v6

    .line 1004
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1006
    iget-object v1, p0, LaaZ;->a:Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 1007
    invoke-static {v1, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1008
    return-void
.end method
