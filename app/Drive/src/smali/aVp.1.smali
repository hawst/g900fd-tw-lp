.class final LaVp;
.super Ljava/lang/Object;


# instance fields
.field public a:LaMR;

.field public a:LaNa;

.field public a:LaOC;

.field public a:LaOH;

.field public a:LaOP;

.field public a:LaPF;

.field public a:LaPG;

.field public a:LaPL;

.field public a:LaPN;

.field public final a:LaSP;

.field public final a:LaVn;

.field public final a:Landroid/content/Context;

.field public a:Lcom/google/android/gms/internal/aq;

.field public final a:Lcom/google/android/gms/internal/fa;

.field public final a:Ljava/lang/String;

.field private a:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "LaPG;",
            ">;"
        }
    .end annotation
.end field

.field public a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/aq;Ljava/lang/String;Lcom/google/android/gms/internal/fa;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, LaVp;->a:LaPL;

    const/4 v0, 0x0

    iput-boolean v0, p0, LaVp;->a:Z

    iput-object v1, p0, LaVp;->a:Ljava/util/HashSet;

    iget-boolean v0, p2, Lcom/google/android/gms/internal/aq;->a:Z

    if-eqz v0, :cond_0

    iput-object v1, p0, LaVp;->a:LaVn;

    :goto_0
    iput-object p2, p0, LaVp;->a:Lcom/google/android/gms/internal/aq;

    iput-object p3, p0, LaVp;->a:Ljava/lang/String;

    iput-object p1, p0, LaVp;->a:Landroid/content/Context;

    iput-object p4, p0, LaVp;->a:Lcom/google/android/gms/internal/fa;

    new-instance v0, LaSP;

    new-instance v1, LaVo;

    invoke-direct {v1, p0}, LaVo;-><init>(LaVp;)V

    invoke-direct {v0, v1}, LaSP;-><init>(LaQX;)V

    iput-object v0, p0, LaVp;->a:LaSP;

    return-void

    :cond_0
    new-instance v0, LaVn;

    invoke-direct {v0, p1}, LaVn;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LaVp;->a:LaVn;

    iget-object v0, p0, LaVp;->a:LaVn;

    iget v1, p2, Lcom/google/android/gms/internal/aq;->e:I

    invoke-virtual {v0, v1}, LaVn;->setMinimumWidth(I)V

    iget-object v0, p0, LaVp;->a:LaVn;

    iget v1, p2, Lcom/google/android/gms/internal/aq;->c:I

    invoke-virtual {v0, v1}, LaVn;->setMinimumHeight(I)V

    iget-object v0, p0, LaVp;->a:LaVn;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, LaVn;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "LaPG;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, LaVp;->a:Ljava/util/HashSet;

    return-object v0
.end method

.method public a(Ljava/util/HashSet;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "LaPG;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, LaVp;->a:Ljava/util/HashSet;

    return-void
.end method
