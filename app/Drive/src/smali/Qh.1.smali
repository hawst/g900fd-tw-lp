.class public LQh;
.super Landroid/widget/ArrayAdapter;
.source "PickEntryDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "LQl;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;Landroid/content/Context;II[LQl;)V
    .locals 0

    .prologue
    .line 449
    iput-object p1, p0, LQh;->a:Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 452
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 453
    invoke-static {}, LQl;->b()[LQl;

    move-result-object v0

    aget-object v2, v0, p1

    .line 454
    sget v0, Lxc;->navigation_icon:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 455
    invoke-static {v2}, LQl;->a(LQl;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 457
    sget v0, Lxc;->navigation_name:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 458
    invoke-static {v2}, LQl;->a(LQl;)LCl;

    move-result-object v2

    invoke-interface {v2}, LCl;->a()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 459
    return-object v1
.end method
