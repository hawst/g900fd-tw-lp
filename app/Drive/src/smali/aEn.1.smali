.class LaEn;
.super Ljava/lang/Object;
.source "AppSplitOfflineUpgradeStep.java"

# interfaces
.implements LaFv;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/16 v6, 0x5f

    .line 25
    sget-object v0, LaEL;->c:LaEL;

    .line 26
    invoke-virtual {v0}, LaEL;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, v6}, LaFr;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 28
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 29
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 31
    const-string v2, "%s IN (SELECT %s FROM %s WHERE %s = \'%s\')"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    sget-object v4, LaEL;->b:LaEL;

    .line 33
    invoke-virtual {v4}, LaEL;->a()LaFr;

    move-result-object v4

    invoke-virtual {v4, v6}, LaFr;->a(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    .line 34
    invoke-static {}, LaER;->a()LaER;

    move-result-object v4

    invoke-virtual {v4}, LaER;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    const/4 v4, 0x2

    .line 35
    invoke-static {}, LaER;->a()LaER;

    move-result-object v5

    invoke-virtual {v5, v6}, LaER;->a(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    sget-object v5, LaES;->t:LaES;

    .line 36
    invoke-virtual {v5}, LaES;->a()LaFr;

    move-result-object v5

    invoke-virtual {v5, v6}, LaFr;->a(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    sget-object v5, LaGv;->b:LaGv;

    .line 37
    invoke-virtual {v5}, LaGv;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 31
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 39
    sget-object v3, LaFL;->a:LaFL;

    new-instance v4, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " IS NOT NULL"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0, v9}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-array v0, v8, [Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v5, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    invoke-direct {v5, v2, v9}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v0, v7

    invoke-virtual {v3, v4, v0}, LaFL;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;[Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 43
    invoke-static {}, LaEK;->a()LaEK;

    move-result-object v2

    invoke-virtual {v2, v6}, LaEK;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 44
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()[Ljava/lang/String;

    move-result-object v0

    .line 43
    invoke-virtual {p1, v2, v1, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 46
    const-string v1, "AppSplitOfflineUpgradeStep"

    const-string v2, "%d document(s) have their offline content deleted"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v7

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 47
    return-void
.end method
