.class LbrE;
.super Ljava/lang/Object;
.source "DefaultConstructorProviderLoader.java"


# static fields
.field private static final a:LbuE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbuE",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LbrI;

.field private final a:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "LbuE",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-static {v0}, LbwI;->a(Ljava/lang/Object;)LbuE;

    move-result-object v0

    sput-object v0, LbrE;->a:LbuE;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    new-instance v0, LbrI;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LbrI;-><init>(LbrF;)V

    iput-object v0, p0, LbrE;->a:LbrI;

    .line 107
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, LbrE;->a:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method private a(Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ljava/lang/reflect/Constructor",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 94
    invoke-virtual {p1}, Ljava/lang/Class;->getDeclaredConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 95
    invoke-virtual {v0}, Ljava/lang/reflect/Constructor;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v4

    array-length v4, v4

    if-nez v4, :cond_0

    .line 98
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 103
    :goto_1
    return-object v0

    .line 94
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 103
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(Ljava/lang/Class;)LbuE;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LbuE",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 82
    invoke-direct {p0, p1}, LbrE;->a(Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    .line 84
    if-eqz v1, :cond_0

    .line 85
    new-instance v0, LbrG;

    iget-object v2, p0, LbrE;->a:LbrI;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, LbrG;-><init>(Ljava/lang/reflect/Constructor;LbrI;LbrF;)V

    .line 90
    :goto_0
    return-object v0

    .line 87
    :cond_0
    sget-object v0, LbrE;->a:LbuE;

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/Class;)LbuE;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LbuE",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, LbrE;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuE;

    .line 116
    if-nez v0, :cond_1

    .line 117
    monitor-enter p0

    .line 118
    :try_start_0
    iget-object v0, p0, LbrE;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuE;

    .line 119
    if-nez v0, :cond_0

    .line 120
    invoke-direct {p0, p1}, LbrE;->b(Ljava/lang/Class;)LbuE;

    move-result-object v0

    .line 121
    iget-object v1, p0, LbrE;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    :cond_1
    sget-object v1, LbrE;->a:LbuE;

    if-ne v0, v1, :cond_2

    .line 127
    const/4 v0, 0x0

    .line 131
    :cond_2
    return-object v0

    .line 123
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(LbrH;)V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, LbrE;->a:LbrI;

    invoke-virtual {v0, p1}, LbrI;->a(LbrH;)V

    .line 112
    return-void
.end method
