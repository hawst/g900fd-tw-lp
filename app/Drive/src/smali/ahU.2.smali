.class LahU;
.super Ljava/lang/Object;
.source "BaseBinaryFileDownloader.java"


# instance fields
.field private final a:LaGM;

.field private final a:Laie;

.field private final a:Laoj;


# direct methods
.method constructor <init>(LaGM;Laoj;Laie;)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-object p1, p0, LahU;->a:LaGM;

    .line 87
    iput-object p2, p0, LahU;->a:Laoj;

    .line 88
    iput-object p3, p0, LahU;->a:Laie;

    .line 89
    return-void
.end method

.method private a(Ljava/io/InputStream;Laif;J)J
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 283
    :goto_0
    invoke-interface {p2}, Laif;->a()J

    move-result-wide v2

    cmp-long v2, p3, v2

    if-gez v2, :cond_1

    .line 284
    invoke-static {}, LamS;->a()V

    .line 285
    invoke-interface {p2}, Laif;->a()J

    move-result-wide v2

    sub-long/2addr v2, p3

    const-wide/16 v4, 0x1000

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v2, v2

    .line 286
    const/16 v3, 0x1000

    new-array v3, v3, [B

    .line 287
    invoke-virtual {p1, v3, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 289
    const-string v3, "AbstractBinaryFileSynchronizer"

    const-string v4, "Dumping data from stream %s target %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    .line 290
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-interface {p2}, Laif;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v0

    .line 289
    invoke-static {v3, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 291
    if-ltz v2, :cond_0

    .line 292
    int-to-long v2, v2

    add-long/2addr p3, v2

    goto :goto_0

    .line 294
    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "File truncated"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 298
    :cond_1
    invoke-interface {p2}, Laif;->a()J

    move-result-wide v2

    cmp-long v2, p3, v2

    if-nez v2, :cond_2

    :goto_1
    invoke-static {v0}, LbiT;->b(Z)V

    .line 299
    return-wide p3

    :cond_2
    move v0, v1

    .line 298
    goto :goto_1
.end method

.method private a(Ljava/lang/String;LahW;Lorg/apache/http/HttpResponse;Laif;)LahX;
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 184
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    const-wide/16 v2, 0x0

    .line 191
    invoke-interface {p3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    const/16 v5, 0xce

    if-eq v1, v5, :cond_5

    .line 192
    if-eqz p4, :cond_0

    .line 193
    invoke-interface {p4}, Laif;->close()V

    .line 194
    const-string v1, "AbstractBinaryFileSynchronizer"

    const-string v5, "Recovery failed, status code %s"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-interface {p3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v7

    invoke-interface {v7}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-static {v1, v5, v6}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    move-object v5, v0

    .line 199
    :goto_0
    if-nez v5, :cond_3

    .line 201
    invoke-interface {p2, p1, p3}, LahW;->a(Ljava/lang/String;Lorg/apache/http/HttpResponse;)Laif;

    move-result-object v5

    .line 203
    if-nez v5, :cond_1

    .line 204
    const-string v1, "AbstractBinaryFileSynchronizer"

    const-string v2, "Failed to open document for writing"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 233
    :goto_1
    return-object v0

    .line 208
    :cond_1
    invoke-interface {p3}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v0

    long-to-int v4, v0

    .line 233
    :cond_2
    :goto_2
    new-instance v0, LahX;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LahX;-><init>(LahU;JILaif;)V

    goto :goto_1

    .line 211
    :cond_3
    const-string v0, "Content-Range"

    invoke-interface {p3, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 212
    if-eqz v0, :cond_2

    .line 214
    :try_start_0
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LMP;->a(Ljava/lang/String;)LMP;

    move-result-object v0

    .line 215
    iget-wide v2, v0, LMP;->a:J

    .line 217
    iget-wide v0, v0, LMP;->b:J

    long-to-int v0, v0

    add-int/lit8 v4, v0, 0x1

    .line 219
    invoke-interface {v5}, Laif;->a()J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-lez v0, :cond_4

    .line 220
    new-instance v0, Ljava/text/ParseException;

    const-string v1, "Invalid parsed result"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    :catch_0
    move-exception v0

    .line 226
    invoke-interface {v5}, Laif;->close()V

    .line 228
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 223
    :cond_4
    :try_start_1
    const-string v0, "AbstractBinaryFileSynchronizer"

    const-string v1, "Recovered location %s size %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v0, v1, v6}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :cond_5
    move-object v5, p4

    goto :goto_0
.end method

.method private a(LagH;Laif;Lorg/apache/http/HttpResponse;JI)Ljava/io/InputStream;
    .locals 8

    .prologue
    .line 240
    invoke-static {}, LamS;->a()V

    .line 242
    invoke-interface {p3}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 243
    if-nez v0, :cond_0

    .line 244
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Response entity is null."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 246
    :cond_0
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v2

    .line 248
    invoke-direct {p0, v2, p2, p4, p5}, LahU;->a(Ljava/io/InputStream;Laif;J)J

    move-result-wide v6

    .line 250
    if-eqz p1, :cond_2

    .line 251
    new-instance v1, Laik;

    int-to-long v4, p6

    move-object v3, p1

    invoke-direct/range {v1 .. v7}, Laik;-><init>(Ljava/io/InputStream;LagH;JJ)V

    .line 255
    :goto_0
    invoke-static {}, LamS;->a()V

    .line 258
    :try_start_0
    invoke-interface {p2, v1}, Laif;->a(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 260
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 263
    invoke-static {}, LamS;->a()V

    .line 268
    invoke-interface {p2}, Laif;->a()V

    .line 269
    if-eqz p1, :cond_1

    .line 270
    invoke-interface {p1}, LagH;->b()V

    .line 272
    :cond_1
    return-object v1

    .line 260
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0

    :cond_2
    move-object v1, v2

    goto :goto_0
.end method

.method private a(LaFO;Ljava/lang/String;Laif;)Lorg/apache/http/HttpResponse;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 311
    .line 314
    if-eqz p3, :cond_1

    .line 315
    invoke-interface {p3}, Laif;->a()Ljava/lang/String;

    move-result-object v1

    .line 316
    new-instance v0, LMP;

    invoke-interface {p3}, Laif;->a()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, LMP;-><init>(J)V

    .line 319
    :goto_0
    iget-object v2, p0, LahU;->a:Laoj;

    .line 320
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, p1, v3, v1, v0}, Laoj;->a(LaFO;Landroid/net/Uri;Ljava/lang/String;LMP;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 322
    invoke-static {}, LamS;->a()V

    .line 323
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    .line 324
    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    const/16 v3, 0xc8

    if-eq v2, v3, :cond_0

    .line 325
    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    const/16 v3, 0xce

    if-eq v2, v3, :cond_0

    .line 326
    new-instance v0, Ljava/io/IOException;

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 329
    :cond_0
    return-object v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method private a(LagH;Ljava/lang/String;Ljava/lang/Exception;LFV;)V
    .locals 3

    .prologue
    .line 334
    const-string v0, "AbstractBinaryFileSynchronizer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error synching "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p3, v1}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 336
    if-eqz p1, :cond_0

    .line 337
    invoke-virtual {p4}, LFV;->a()LafT;

    move-result-object v0

    invoke-interface {p1, v0}, LagH;->a(LafT;)V

    .line 339
    :cond_0
    return-void
.end method


# virtual methods
.method a(LaFO;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LahW;LahV;LagH;)Laif;
    .locals 11

    .prologue
    .line 108
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    invoke-static/range {p5 .. p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    invoke-static/range {p6 .. p6}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    const/4 v10, 0x0

    .line 116
    const/4 v9, 0x0

    .line 120
    if-eqz p7, :cond_0

    .line 121
    :try_start_0
    invoke-interface/range {p7 .. p7}, LagH;->a()V

    .line 123
    :cond_0
    invoke-static {}, LamS;->a()V

    .line 125
    iget-object v2, p0, LahU;->a:LaGM;

    invoke-interface {v2, p1}, LaGM;->a(LaFO;)LaFM;

    .line 127
    invoke-static {}, LamS;->a()V

    .line 129
    iget-object v2, p0, LahU;->a:Laie;

    invoke-interface {v2, p4}, Laie;->a(Ljava/lang/String;)Laif;
    :try_end_0
    .catch LQp; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catch LTr; {:try_start_0 .. :try_end_0} :catch_8
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_a
    .catch LQm; {:try_start_0 .. :try_end_0} :catch_c
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_e
    .catch LTt; {:try_start_0 .. :try_end_0} :catch_10
    .catchall {:try_start_0 .. :try_end_0} :catchall_a

    move-result-object v9

    .line 131
    :try_start_1
    invoke-direct {p0, p1, p2, v9}, LahU;->a(LaFO;Ljava/lang/String;Laif;)Lorg/apache/http/HttpResponse;

    move-result-object v5

    .line 136
    move-object/from16 v0, p5

    invoke-direct {p0, p3, v0, v5, v9}, LahU;->a(Ljava/lang/String;LahW;Lorg/apache/http/HttpResponse;Laif;)LahX;
    :try_end_1
    .catch LQp; {:try_start_1 .. :try_end_1} :catch_1a
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_19
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_18
    .catch LTr; {:try_start_1 .. :try_end_1} :catch_17
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_16
    .catch LQm; {:try_start_1 .. :try_end_1} :catch_15
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_14
    .catch LTt; {:try_start_1 .. :try_end_1} :catch_13
    .catchall {:try_start_1 .. :try_end_1} :catchall_a

    move-result-object v2

    .line 137
    if-nez v2, :cond_2

    .line 138
    const/4 v2, 0x0

    .line 163
    if-eqz v10, :cond_1

    .line 164
    :try_start_2
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 169
    :cond_1
    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    .line 173
    :goto_0
    return-object v2

    .line 166
    :catch_0
    move-exception v3

    .line 167
    :try_start_3
    const-string v4, "AbstractBinaryFileSynchronizer"

    const-string v5, "Error on syncDown: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    invoke-static {v4, v5, v6}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 169
    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    goto :goto_0

    :catchall_0
    move-exception v2

    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    throw v2

    .line 140
    :cond_2
    :try_start_4
    iget-object v9, v2, LahX;->a:Laif;

    .line 142
    iget-object v4, v2, LahX;->a:Laif;

    iget-wide v6, v2, LahX;->a:J

    iget v8, v2, LahX;->a:I

    move-object v2, p0

    move-object/from16 v3, p7

    invoke-direct/range {v2 .. v8}, LahU;->a(LagH;Laif;Lorg/apache/http/HttpResponse;JI)Ljava/io/InputStream;
    :try_end_4
    .catch LQp; {:try_start_4 .. :try_end_4} :catch_1a
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_4 .. :try_end_4} :catch_19
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_18
    .catch LTr; {:try_start_4 .. :try_end_4} :catch_17
    .catch Landroid/accounts/AuthenticatorException; {:try_start_4 .. :try_end_4} :catch_16
    .catch LQm; {:try_start_4 .. :try_end_4} :catch_15
    .catch Ljava/net/URISyntaxException; {:try_start_4 .. :try_end_4} :catch_14
    .catch LTt; {:try_start_4 .. :try_end_4} :catch_13
    .catchall {:try_start_4 .. :try_end_4} :catchall_a

    move-result-object v2

    .line 163
    if-eqz v2, :cond_3

    .line 164
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 169
    :cond_3
    iget-object v2, p0, LahU;->a:Laoj;

    invoke-virtual {v2}, Laoj;->a()V

    :goto_1
    move-object v2, v9

    .line 171
    goto :goto_0

    .line 166
    :catch_1
    move-exception v2

    .line 167
    :try_start_6
    const-string v3, "AbstractBinaryFileSynchronizer"

    const-string v4, "Error on syncDown: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-static {v3, v4, v5}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 169
    iget-object v2, p0, LahU;->a:Laoj;

    invoke-virtual {v2}, Laoj;->a()V

    goto :goto_1

    :catchall_1
    move-exception v2

    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    throw v2

    .line 144
    :catch_2
    move-exception v2

    move-object v3, v2

    move-object v2, v9

    .line 145
    :goto_2
    :try_start_7
    sget-object v4, LFV;->a:LFV;

    move-object/from16 v0, p7

    invoke-direct {p0, v0, p2, v3, v4}, LahU;->a(LagH;Ljava/lang/String;Ljava/lang/Exception;LFV;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_a

    .line 163
    if-eqz v10, :cond_4

    .line 164
    :try_start_8
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 169
    :cond_4
    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    goto :goto_0

    .line 166
    :catch_3
    move-exception v3

    .line 167
    :try_start_9
    const-string v4, "AbstractBinaryFileSynchronizer"

    const-string v5, "Error on syncDown: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    invoke-static {v4, v5, v6}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 169
    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    goto :goto_0

    :catchall_2
    move-exception v2

    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    throw v2

    .line 146
    :catch_4
    move-exception v2

    move-object v3, v2

    move-object v2, v9

    .line 147
    :goto_3
    :try_start_a
    sget-object v4, LFV;->h:LFV;

    move-object/from16 v0, p7

    invoke-direct {p0, v0, p2, v3, v4}, LahU;->a(LagH;Ljava/lang/String;Ljava/lang/Exception;LFV;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_a

    .line 163
    if-eqz v10, :cond_5

    .line 164
    :try_start_b
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    .line 169
    :cond_5
    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    goto/16 :goto_0

    .line 166
    :catch_5
    move-exception v3

    .line 167
    :try_start_c
    const-string v4, "AbstractBinaryFileSynchronizer"

    const-string v5, "Error on syncDown: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    invoke-static {v4, v5, v6}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    .line 169
    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    goto/16 :goto_0

    :catchall_3
    move-exception v2

    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    throw v2

    .line 148
    :catch_6
    move-exception v2

    move-object v3, v2

    move-object v2, v9

    .line 149
    :goto_4
    :try_start_d
    move-object/from16 v0, p6

    invoke-interface {v0, v3}, LahV;->a(Ljava/io/IOException;)LFV;

    move-result-object v4

    .line 150
    move-object/from16 v0, p7

    invoke-direct {p0, v0, p2, v3, v4}, LahU;->a(LagH;Ljava/lang/String;Ljava/lang/Exception;LFV;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_a

    .line 163
    if-eqz v10, :cond_6

    .line 164
    :try_start_e
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_7
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    .line 169
    :cond_6
    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    goto/16 :goto_0

    .line 166
    :catch_7
    move-exception v3

    .line 167
    :try_start_f
    const-string v4, "AbstractBinaryFileSynchronizer"

    const-string v5, "Error on syncDown: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    invoke-static {v4, v5, v6}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    .line 169
    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    goto/16 :goto_0

    :catchall_4
    move-exception v2

    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    throw v2

    .line 151
    :catch_8
    move-exception v2

    move-object v3, v2

    move-object v2, v9

    .line 152
    :goto_5
    :try_start_10
    sget-object v4, LFV;->f:LFV;

    move-object/from16 v0, p7

    invoke-direct {p0, v0, p2, v3, v4}, LahU;->a(LagH;Ljava/lang/String;Ljava/lang/Exception;LFV;)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_a

    .line 163
    if-eqz v10, :cond_7

    .line 164
    :try_start_11
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_9
    .catchall {:try_start_11 .. :try_end_11} :catchall_5

    .line 169
    :cond_7
    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    goto/16 :goto_0

    .line 166
    :catch_9
    move-exception v3

    .line 167
    :try_start_12
    const-string v4, "AbstractBinaryFileSynchronizer"

    const-string v5, "Error on syncDown: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    invoke-static {v4, v5, v6}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_5

    .line 169
    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    goto/16 :goto_0

    :catchall_5
    move-exception v2

    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    throw v2

    .line 153
    :catch_a
    move-exception v2

    move-object v3, v2

    move-object v2, v9

    .line 154
    :goto_6
    :try_start_13
    sget-object v4, LFV;->f:LFV;

    move-object/from16 v0, p7

    invoke-direct {p0, v0, p2, v3, v4}, LahU;->a(LagH;Ljava/lang/String;Ljava/lang/Exception;LFV;)V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_a

    .line 163
    if-eqz v10, :cond_8

    .line 164
    :try_start_14
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_b
    .catchall {:try_start_14 .. :try_end_14} :catchall_6

    .line 169
    :cond_8
    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    goto/16 :goto_0

    .line 166
    :catch_b
    move-exception v3

    .line 167
    :try_start_15
    const-string v4, "AbstractBinaryFileSynchronizer"

    const-string v5, "Error on syncDown: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    invoke-static {v4, v5, v6}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_6

    .line 169
    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    goto/16 :goto_0

    :catchall_6
    move-exception v2

    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    throw v2

    .line 155
    :catch_c
    move-exception v2

    move-object v3, v2

    move-object v2, v9

    .line 156
    :goto_7
    :try_start_16
    sget-object v4, LFV;->h:LFV;

    move-object/from16 v0, p7

    invoke-direct {p0, v0, p2, v3, v4}, LahU;->a(LagH;Ljava/lang/String;Ljava/lang/Exception;LFV;)V
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_a

    .line 163
    if-eqz v10, :cond_9

    .line 164
    :try_start_17
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_d
    .catchall {:try_start_17 .. :try_end_17} :catchall_7

    .line 169
    :cond_9
    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    goto/16 :goto_0

    .line 166
    :catch_d
    move-exception v3

    .line 167
    :try_start_18
    const-string v4, "AbstractBinaryFileSynchronizer"

    const-string v5, "Error on syncDown: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    invoke-static {v4, v5, v6}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_7

    .line 169
    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    goto/16 :goto_0

    :catchall_7
    move-exception v2

    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    throw v2

    .line 157
    :catch_e
    move-exception v2

    move-object v3, v2

    move-object v2, v9

    .line 158
    :goto_8
    :try_start_19
    sget-object v4, LFV;->g:LFV;

    move-object/from16 v0, p7

    invoke-direct {p0, v0, p2, v3, v4}, LahU;->a(LagH;Ljava/lang/String;Ljava/lang/Exception;LFV;)V
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_a

    .line 163
    if-eqz v10, :cond_a

    .line 164
    :try_start_1a
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_f
    .catchall {:try_start_1a .. :try_end_1a} :catchall_8

    .line 169
    :cond_a
    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    goto/16 :goto_0

    .line 166
    :catch_f
    move-exception v3

    .line 167
    :try_start_1b
    const-string v4, "AbstractBinaryFileSynchronizer"

    const-string v5, "Error on syncDown: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    invoke-static {v4, v5, v6}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_8

    .line 169
    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    goto/16 :goto_0

    :catchall_8
    move-exception v2

    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    throw v2

    .line 159
    :catch_10
    move-exception v2

    move-object v3, v2

    move-object v2, v9

    .line 160
    :goto_9
    :try_start_1c
    sget-object v4, LFV;->g:LFV;

    move-object/from16 v0, p7

    invoke-direct {p0, v0, p2, v3, v4}, LahU;->a(LagH;Ljava/lang/String;Ljava/lang/Exception;LFV;)V
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_a

    .line 163
    if-eqz v10, :cond_b

    .line 164
    :try_start_1d
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_1d
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_1d} :catch_11
    .catchall {:try_start_1d .. :try_end_1d} :catchall_9

    .line 169
    :cond_b
    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    goto/16 :goto_0

    .line 166
    :catch_11
    move-exception v3

    .line 167
    :try_start_1e
    const-string v4, "AbstractBinaryFileSynchronizer"

    const-string v5, "Error on syncDown: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    invoke-static {v4, v5, v6}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_9

    .line 169
    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    goto/16 :goto_0

    :catchall_9
    move-exception v2

    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    throw v2

    .line 162
    :catchall_a
    move-exception v2

    .line 163
    if-eqz v10, :cond_c

    .line 164
    :try_start_1f
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_1f
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_1f} :catch_12
    .catchall {:try_start_1f .. :try_end_1f} :catchall_b

    .line 169
    :cond_c
    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    :goto_a
    throw v2

    .line 166
    :catch_12
    move-exception v3

    .line 167
    :try_start_20
    const-string v4, "AbstractBinaryFileSynchronizer"

    const-string v5, "Error on syncDown: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    invoke-static {v4, v5, v6}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_b

    .line 169
    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    goto :goto_a

    :catchall_b
    move-exception v2

    iget-object v3, p0, LahU;->a:Laoj;

    invoke-virtual {v3}, Laoj;->a()V

    throw v2

    .line 159
    :catch_13
    move-exception v2

    move-object v3, v2

    move-object v2, v9

    goto :goto_9

    .line 157
    :catch_14
    move-exception v2

    move-object v3, v2

    move-object v2, v9

    goto/16 :goto_8

    .line 155
    :catch_15
    move-exception v2

    move-object v3, v2

    move-object v2, v9

    goto/16 :goto_7

    .line 153
    :catch_16
    move-exception v2

    move-object v3, v2

    move-object v2, v9

    goto/16 :goto_6

    .line 151
    :catch_17
    move-exception v2

    move-object v3, v2

    move-object v2, v9

    goto/16 :goto_5

    .line 148
    :catch_18
    move-exception v2

    move-object v3, v2

    move-object v2, v9

    goto/16 :goto_4

    .line 146
    :catch_19
    move-exception v2

    move-object v3, v2

    move-object v2, v9

    goto/16 :goto_3

    .line 144
    :catch_1a
    move-exception v2

    move-object v3, v2

    move-object v2, v9

    goto/16 :goto_2
.end method
