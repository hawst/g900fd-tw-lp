.class Laut;
.super Landroid/os/AsyncTask;
.source "LoginWebViewClient.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/accounts/AccountManagerFuture;

.field final synthetic a:Landroid/net/Uri;

.field final synthetic a:Landroid/webkit/WebView;

.field final synthetic a:Laus;


# direct methods
.method constructor <init>(Laus;Landroid/accounts/AccountManagerFuture;Landroid/webkit/WebView;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Laut;->a:Laus;

    iput-object p2, p0, Laut;->a:Landroid/accounts/AccountManagerFuture;

    iput-object p3, p0, Laut;->a:Landroid/webkit/WebView;

    iput-object p4, p0, Laut;->a:Landroid/net/Uri;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 123
    :try_start_0
    iget-object v0, p0, Laut;->a:Landroid/accounts/AccountManagerFuture;

    invoke-interface {v0}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    const-string v1, "authtoken"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 124
    :catch_0
    move-exception v0

    .line 125
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 133
    if-eqz p1, :cond_0

    .line 135
    iget-object v0, p0, Laut;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 140
    :goto_0
    return-void

    .line 138
    :cond_0
    iget-object v0, p0, Laut;->a:Landroid/webkit/WebView;

    iget-object v1, p0, Laut;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 117
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Laut;->a([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 117
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Laut;->a(Ljava/lang/String;)V

    return-void
.end method
