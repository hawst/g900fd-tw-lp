.class public LafG;
.super Ljava/lang/Object;
.source "AutoSyncRateLimiter.java"


# static fields
.field private static final a:J


# instance fields
.field private final a:LQr;

.field private final a:LaGM;

.field private final a:LaKM;

.field private final a:LafE;

.field private final a:LpW;

.field private final a:LtK;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 33
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xc

    sget-object v1, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    .line 34
    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    sput-wide v0, LafG;->a:J

    .line 33
    return-void
.end method

.method public constructor <init>(LaKM;LQr;LpW;LaGM;LafE;LtK;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, LafG;->a:LaKM;

    .line 48
    iput-object p2, p0, LafG;->a:LQr;

    .line 49
    iput-object p3, p0, LafG;->a:LpW;

    .line 50
    iput-object p4, p0, LafG;->a:LaGM;

    .line 51
    iput-object p5, p0, LafG;->a:LafE;

    .line 52
    iput-object p6, p0, LafG;->a:LtK;

    .line 53
    return-void
.end method

.method private a()J
    .locals 4

    .prologue
    .line 85
    iget-object v0, p0, LafG;->a:LQr;

    const-string v1, "autoDocsSyncRequestPeriodSeconds"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    .line 88
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v0, v1, v3}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(LaFO;)J
    .locals 4

    .prologue
    .line 80
    iget-object v0, p0, LafG;->a:LpW;

    invoke-interface {v0, p1}, LpW;->a(LaFO;)LpU;

    move-result-object v0

    .line 81
    const-string v1, "lastDocsSyncRequestTimeMs"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, LpU;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(LaFO;J)V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, LafG;->a:LpW;

    invoke-interface {v0, p1}, LpW;->a(LaFO;)LpU;

    move-result-object v0

    .line 94
    const-string v1, "lastDocsSyncRequestTimeMs"

    invoke-interface {v0, v1, p2, p3}, LpU;->a(Ljava/lang/String;J)V

    .line 95
    iget-object v1, p0, LafG;->a:LpW;

    invoke-interface {v1, v0}, LpW;->a(LpU;)V

    .line 96
    return-void
.end method

.method private a(LaFO;LaFQ;LaFP;)Z
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 149
    :try_start_0
    iget-object v2, p0, LafG;->a:LafE;

    .line 150
    invoke-virtual {p3}, LaFP;->a()Ljava/lang/String;

    move-result-object v3

    .line 149
    invoke-virtual {v2, p1, v3}, LafE;->a(LaFO;Ljava/lang/String;)LaJQ;
    :try_end_0
    .catch Lbxk; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 161
    invoke-virtual {p2}, LaFQ;->a()I

    move-result v3

    int-to-long v4, v3

    .line 162
    invoke-interface {v2}, LaJQ;->c()J

    move-result-wide v2

    .line 164
    const-string v6, "AutoSyncRateLimiter"

    const-string v7, "AccountStateLastChangeStamp=%s, MetadataLargestChangeStamp=%s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    .line 165
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v0

    .line 164
    invoke-static {v6, v7, v8}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 166
    cmp-long v2, v4, v2

    if-gez v2, :cond_0

    :goto_0
    return v0

    .line 151
    :catch_0
    move-exception v2

    .line 152
    const-string v3, "AutoSyncRateLimiter"

    const-string v4, "Failed to parse account capability."

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v3, v2, v4, v5}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 153
    const-string v3, "AutoSyncRateLimiter"

    const-string v4, "Failed to parse: %s"

    new-array v5, v0, [Ljava/lang/Object;

    invoke-virtual {p3}, LaFP;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v3, v2, v4, v5}, LalV;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 155
    :catch_1
    move-exception v2

    .line 156
    const-string v3, "AutoSyncRateLimiter"

    const-string v4, "Failed to parse account capability."

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v3, v2, v4, v5}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 157
    const-string v3, "AutoSyncRateLimiter"

    const-string v4, "Failed to parse: %s"

    new-array v5, v0, [Ljava/lang/Object;

    invoke-virtual {p3}, LaFP;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v3, v2, v4, v5}, LalV;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_0
    move v0, v1

    .line 166
    goto :goto_0
.end method

.method private a(LaFQ;LaFP;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 126
    invoke-virtual {p2}, LaFP;->a()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    .line 127
    iget-object v4, p0, LafG;->a:LaKM;

    invoke-interface {v4}, LaKM;->a()J

    move-result-wide v4

    .line 129
    cmp-long v6, v2, v4

    if-gtz v6, :cond_0

    sub-long v2, v4, v2

    .line 130
    invoke-direct {p0}, LafG;->b()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 131
    :cond_0
    const-string v2, "AutoSyncRateLimiter"

    const-string v3, "ChangeStamp expired. LastSyncTime=%s Now=%s ExpirePeriodMs=%s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    .line 132
    invoke-virtual {p1}, LaFQ;->a()Ljava/util/Date;

    move-result-object v5

    aput-object v5, v4, v0

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    aput-object v0, v4, v1

    const/4 v0, 0x2

    invoke-direct {p0}, LafG;->b()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v0

    .line 131
    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v1

    .line 135
    :cond_1
    return v0
.end method

.method private b()J
    .locals 4

    .prologue
    .line 170
    iget-object v0, p0, LafG;->a:LQr;

    const-string v1, "accountMetadataExpirePeriodSeconds"

    sget-wide v2, LafG;->a:J

    long-to-int v2, v2

    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    .line 173
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 174
    invoke-virtual {v2, v0, v1, v3}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    .line 175
    return-wide v0
.end method

.method private b(LaFO;)Z
    .locals 3

    .prologue
    .line 112
    iget-object v0, p0, LafG;->a:LaGM;

    invoke-interface {v0, p1}, LaGM;->a(LaFO;)LaFM;

    move-result-object v0

    .line 113
    iget-object v1, p0, LafG;->a:LaGM;

    invoke-interface {v1, p1}, LaGM;->a(LaFO;)LaFQ;

    move-result-object v1

    .line 114
    iget-object v2, p0, LafG;->a:LaGM;

    invoke-interface {v2, v0}, LaGM;->a(LaFM;)LaFP;

    move-result-object v0

    .line 116
    invoke-direct {p0, v1, v0}, LafG;->a(LaFQ;LaFP;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 117
    invoke-direct {p0, p1, v1, v0}, LafG;->a(LaFO;LaFQ;LaFP;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(LaFO;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 57
    :try_start_0
    invoke-direct {p0, p1}, LafG;->a(LaFO;)J

    move-result-wide v2

    .line 58
    invoke-direct {p0}, LafG;->a()J

    move-result-wide v4

    .line 59
    iget-object v6, p0, LafG;->a:LaKM;

    invoke-interface {v6}, LaKM;->a()J

    move-result-wide v6

    .line 61
    sub-long v2, v6, v2

    .line 62
    neg-long v8, v4

    cmp-long v8, v2, v8

    if-ltz v8, :cond_0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_2

    .line 63
    :cond_0
    invoke-direct {p0, p1, v6, v7}, LafG;->a(LaFO;J)V

    .line 75
    :cond_1
    :goto_0
    return v0

    .line 67
    :cond_2
    iget-object v2, p0, LafG;->a:LtK;

    sget-object v3, Lry;->aq:Lry;

    invoke-interface {v2, v3}, LtK;->a(LtJ;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 68
    invoke-direct {p0, p1}, LafG;->b(LaFO;)Z
    :try_end_0
    .catch LpX; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_1

    :cond_3
    move v0, v1

    .line 72
    goto :goto_0

    .line 73
    :catch_0
    move-exception v2

    .line 74
    const-string v3, "AutoSyncRateLimiter"

    const-string v4, "Failed to check sync period: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v2, v1, v0

    invoke-static {v3, v4, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method
