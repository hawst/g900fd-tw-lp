.class public LaHe;
.super LaGm;
.source "SyncRequestJournalEntry.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGm",
        "<",
        "LaFg;",
        "LaEz;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final a:LaHf;

.field private final a:Z


# direct methods
.method public constructor <init>(LaEz;JLaHf;Z)V
    .locals 2

    .prologue
    .line 58
    invoke-static {}, LaFg;->a()LaFg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LaGm;-><init>(LaFm;LaFy;Landroid/net/Uri;)V

    .line 59
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 60
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    iput-wide p2, p0, LaHe;->a:J

    .line 62
    iput-boolean p5, p0, LaHe;->a:Z

    .line 63
    iput-object p4, p0, LaHe;->a:LaHf;

    .line 64
    return-void

    .line 59
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LaEz;Landroid/database/Cursor;)LaHe;
    .locals 6

    .prologue
    .line 95
    sget-object v0, LaFh;->a:LaFh;

    invoke-virtual {v0}, LaFh;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 96
    sget-object v0, LaFh;->c:LaFh;

    .line 97
    invoke-virtual {v0}, LaFh;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 96
    invoke-static {v0}, LaHf;->a(Ljava/lang/String;)LaHf;

    move-result-object v4

    .line 99
    sget-object v0, LaFh;->d:LaFh;

    .line 100
    invoke-virtual {v0}, LaFh;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 102
    new-instance v0, LaHe;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LaHe;-><init>(LaEz;JLaHf;Z)V

    .line 105
    invoke-static {}, LaFg;->a()LaFg;

    move-result-object v1

    invoke-virtual {v1}, LaFg;->d()Ljava/lang/String;

    move-result-object v1

    .line 104
    invoke-static {p1, v1}, LaFr;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LaHe;->c(J)V

    .line 106
    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 67
    iget-wide v0, p0, LaHe;->a:J

    return-wide v0
.end method

.method public a()LaHf;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, LaHe;->a:LaHf;

    return-object v0
.end method

.method protected a(Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 85
    sget-object v0, LaFh;->a:LaFh;

    invoke-virtual {v0}, LaFh;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, LaHe;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 86
    sget-object v0, LaFh;->d:LaFh;

    invoke-virtual {v0}, LaFh;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    iget-boolean v0, p0, LaHe;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 87
    sget-object v0, LaFh;->c:LaFh;

    invoke-virtual {v0}, LaFh;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaHe;->a:LaHf;

    .line 88
    invoke-virtual {v1}, LaHf;->a()Ljava/lang/String;

    move-result-object v1

    .line 87
    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    return-void

    .line 86
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, LaHe;->a:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 111
    const-string v0, "SyncRequestJournalEntry[sqlId=%d, entrySqlId=%d, direction=%s, isImplicit=%s]"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 113
    invoke-virtual {p0}, LaHe;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 114
    invoke-virtual {p0}, LaHe;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, LaHe;->a:LaHf;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-boolean v3, p0, LaHe;->a:Z

    .line 116
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .line 111
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
