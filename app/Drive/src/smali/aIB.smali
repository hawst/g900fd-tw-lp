.class public final LaIB;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaIF;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaIE;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaID;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 40
    iput-object p1, p0, LaIB;->a:LbrA;

    .line 41
    const-class v0, LaIF;

    invoke-static {v0, v1}, LaIB;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaIB;->a:Lbsk;

    .line 44
    const-class v0, LaIE;

    invoke-static {v0, v1}, LaIB;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaIB;->b:Lbsk;

    .line 47
    const-class v0, LaID;

    invoke-static {v0, v1}, LaIB;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaIB;->c:Lbsk;

    .line 50
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 74
    sparse-switch p1, :sswitch_data_0

    .line 130
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :sswitch_0
    new-instance v4, LaIF;

    iget-object v0, p0, LaIB;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 79
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaIB;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 77
    invoke-static {v0, v1}, LaIB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iget-object v1, p0, LaIB;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaHX;

    iget-object v1, v1, LaHX;->a:Lbsk;

    .line 85
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LaIB;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaHX;

    iget-object v2, v2, LaHX;->a:Lbsk;

    .line 83
    invoke-static {v1, v2}, LaIB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaIy;

    iget-object v2, p0, LaIB;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->t:Lbsk;

    .line 91
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LaIB;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lajo;

    iget-object v3, v3, Lajo;->t:Lbsk;

    .line 89
    invoke-static {v2, v3}, LaIB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Laja;

    iget-object v3, p0, LaIB;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lwc;

    iget-object v3, v3, Lwc;->i:Lbsk;

    .line 97
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, LaIB;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Lwc;

    iget-object v5, v5, Lwc;->i:Lbsk;

    .line 95
    invoke-static {v3, v5}, LaIB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LvL;

    invoke-direct {v4, v0, v1, v2, v3}, LaIF;-><init>(LaGM;LaIy;Laja;LvL;)V

    move-object v0, v4

    .line 128
    :goto_0
    return-object v0

    .line 104
    :sswitch_1
    new-instance v2, LaIE;

    iget-object v0, p0, LaIB;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 107
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaIB;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->a:Lbsk;

    .line 105
    invoke-static {v0, v1}, LaIB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, LaIB;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->D:Lbsk;

    .line 113
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LaIB;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->D:Lbsk;

    .line 111
    invoke-static {v1, v3}, LaIB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lakz;

    invoke-direct {v2, v0, v1}, LaIE;-><init>(Landroid/content/Context;Lakz;)V

    move-object v0, v2

    .line 118
    goto :goto_0

    .line 120
    :sswitch_2
    new-instance v1, LaID;

    iget-object v0, p0, LaIB;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaIB;

    iget-object v0, v0, LaIB;->b:Lbsk;

    .line 123
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LaIB;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaIB;

    iget-object v2, v2, LaIB;->b:Lbsk;

    .line 121
    invoke-static {v0, v2}, LaIB;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaIE;

    invoke-direct {v1, v0}, LaID;-><init>(LaIE;)V

    move-object v0, v1

    .line 128
    goto :goto_0

    .line 74
    nop

    :sswitch_data_0
    .sparse-switch
        0x15 -> :sswitch_0
        0x1a -> :sswitch_1
        0x1d -> :sswitch_2
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 145
    .line 147
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 57
    const-class v0, LaIF;

    iget-object v1, p0, LaIB;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LaIB;->a(Ljava/lang/Class;Lbsk;)V

    .line 58
    const-class v0, LaIE;

    iget-object v1, p0, LaIB;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LaIB;->a(Ljava/lang/Class;Lbsk;)V

    .line 59
    const-class v0, LaID;

    iget-object v1, p0, LaIB;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LaIB;->a(Ljava/lang/Class;Lbsk;)V

    .line 60
    iget-object v0, p0, LaIB;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x15

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 62
    iget-object v0, p0, LaIB;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1a

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 64
    iget-object v0, p0, LaIB;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1d

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 66
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 137
    .line 139
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 70
    return-void
.end method
