.class LauD;
.super Ljava/lang/Object;
.source "CastManager.java"

# interfaces
.implements LaDd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LaDd",
        "<",
        "LaCr;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LauC;


# direct methods
.method constructor <init>(LauC;)V
    .locals 0

    .prologue
    .line 411
    iput-object p1, p0, LauD;->a:LauC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LaCr;)V
    .locals 3

    .prologue
    .line 414
    const-string v0, "Casting"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Load Result Status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, LaCr;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    invoke-interface {p1}, LaCr;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 417
    const-string v0, "Casting"

    const-string v1, "Media loaded successfully"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    iget-object v0, p0, LauD;->a:LauC;

    const-string v1, "Success to casting file :)"

    invoke-static {v0, v1}, LauC;->a(LauC;Ljava/lang/String;)V

    .line 425
    :goto_0
    return-void

    .line 421
    :cond_0
    const-string v0, "Casting"

    const-string v1, "Media loaded unsuccessfully"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    iget-object v0, p0, LauD;->a:LauC;

    const-string v1, "Fail to cast file"

    invoke-static {v0, v1}, LauC;->a(LauC;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic a(LaDc;)V
    .locals 0

    .prologue
    .line 411
    check-cast p1, LaCr;

    invoke-virtual {p0, p1}, LauD;->a(LaCr;)V

    return-void
.end method
