.class public LCH;
.super Ljava/lang/Object;
.source "ListViewToAdapterVisibleEntryViewLookup.java"

# interfaces
.implements LHp;


# instance fields
.field private final a:Landroid/widget/ListView;


# direct methods
.method public constructor <init>(Landroid/widget/ListView;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, LCH;->a:Landroid/widget/ListView;

    .line 19
    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, LCH;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    sub-int v0, p1, v0

    .line 25
    iget-object v1, p0, LCH;->a:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 26
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 31
    const-class v0, LCH;

    invoke-static {v0}, LbiL;->a(Ljava/lang/Class;)LbiN;

    move-result-object v0

    const-string v1, "firstVisiblePosition"

    iget-object v2, p0, LCH;->a:Landroid/widget/ListView;

    .line 32
    invoke-virtual {v2}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;I)LbiN;

    move-result-object v0

    const-string v1, "lastVisiblePosition"

    iget-object v2, p0, LCH;->a:Landroid/widget/ListView;

    .line 33
    invoke-virtual {v2}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;I)LbiN;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, LbiN;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
