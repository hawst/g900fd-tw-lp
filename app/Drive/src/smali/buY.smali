.class final LbuY;
.super Ljava/lang/Object;
.source "Annotations.java"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# instance fields
.field final synthetic a:Ljava/lang/Class;

.field final synthetic a:Ljava/util/Map;


# direct methods
.method constructor <init>(Ljava/lang/Class;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, LbuY;->a:Ljava/lang/Class;

    iput-object p2, p0, LbuY;->a:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 101
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v0

    .line 102
    const-string v1, "annotationType"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 103
    iget-object v0, p0, LbuY;->a:Ljava/lang/Class;

    .line 111
    :goto_0
    return-object v0

    .line 104
    :cond_0
    const-string v1, "toString"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 105
    iget-object v0, p0, LbuY;->a:Ljava/lang/Class;

    iget-object v1, p0, LbuY;->a:Ljava/util/Map;

    invoke-static {v0, v1}, LbuW;->a(Ljava/lang/Class;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 106
    :cond_1
    const-string v1, "hashCode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 107
    iget-object v0, p0, LbuY;->a:Ljava/lang/Class;

    iget-object v1, p0, LbuY;->a:Ljava/util/Map;

    invoke-static {v0, v1}, LbuW;->a(Ljava/lang/Class;Ljava/util/Map;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 108
    :cond_2
    const-string v1, "equals"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 109
    iget-object v0, p0, LbuY;->a:Ljava/lang/Class;

    iget-object v1, p0, LbuY;->a:Ljava/util/Map;

    const/4 v2, 0x0

    aget-object v2, p3, v2

    invoke-static {v0, v1, v2}, LbuW;->a(Ljava/lang/Class;Ljava/util/Map;Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 111
    :cond_3
    iget-object v1, p0, LbuY;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method
