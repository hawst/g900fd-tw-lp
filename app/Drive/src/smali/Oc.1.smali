.class public LOc;
.super Ljava/lang/Object;
.source "VideoPlayerActivity.java"

# interfaces
.implements LbsJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbsJ",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;)V
    .locals 0

    .prologue
    .line 258
    iput-object p1, p0, LOc;->a:Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 258
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, LOc;->a(Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, LOc;->a:Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 283
    :goto_0
    return-void

    .line 282
    :cond_0
    iget-object v0, p0, LOc;->a:Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, LOc;->a:Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 275
    :goto_0
    return-void

    .line 264
    :cond_0
    instance-of v0, p1, Ljava/io/IOException;

    if-eqz v0, :cond_1

    .line 265
    iget-object v0, p0, LOc;->a:Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;

    sget-object v1, LFV;->g:LFV;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;LFV;)V

    goto :goto_0

    .line 266
    :cond_1
    instance-of v0, p1, Landroid/accounts/AuthenticatorException;

    if-eqz v0, :cond_2

    .line 267
    iget-object v0, p0, LOc;->a:Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;

    sget-object v1, LFV;->f:LFV;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;LFV;)V

    goto :goto_0

    .line 268
    :cond_2
    instance-of v0, p1, LTr;

    if-eqz v0, :cond_3

    .line 269
    iget-object v0, p0, LOc;->a:Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;

    sget-object v1, LFV;->f:LFV;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;LFV;)V

    goto :goto_0

    .line 270
    :cond_3
    instance-of v0, p1, Lahs;

    if-eqz v0, :cond_4

    .line 271
    iget-object v0, p0, LOc;->a:Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;

    sget-object v1, LFV;->d:LFV;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;LFV;)V

    goto :goto_0

    .line 273
    :cond_4
    iget-object v0, p0, LOc;->a:Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;

    sget-object v1, LFV;->h:LFV;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;LFV;)V

    goto :goto_0
.end method
