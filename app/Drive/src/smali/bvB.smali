.class final LbvB;
.super Ljava/lang/Object;
.source "ProviderMethodsModule.java"


# instance fields
.field final a:I

.field final synthetic a:Lbvz;

.field final a:Ljava/lang/String;

.field final a:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lbvz;Ljava/lang/reflect/Method;)V
    .locals 4

    .prologue
    .line 157
    iput-object p1, p0, LbvB;->a:Lbvz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LbvB;->a:Ljava/lang/String;

    .line 164
    invoke-static {p1}, Lbvz;->a(Lbvz;)LbuP;

    move-result-object v0

    invoke-virtual {v0, p2}, LbuP;->a(Ljava/lang/reflect/Member;)Ljava/util/List;

    move-result-object v0

    .line 165
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/Class;

    iput-object v1, p0, LbvB;->a:[Ljava/lang/Class;

    .line 166
    const/4 v1, 0x0

    .line 167
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuP;

    .line 168
    iget-object v3, p0, LbvB;->a:[Ljava/lang/Class;

    invoke-virtual {v0}, LbuP;->a()Ljava/lang/Class;

    move-result-object v0

    aput-object v0, v3, v1

    goto :goto_0

    .line 170
    :cond_0
    iget-object v0, p0, LbvB;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget-object v1, p0, LbvB;->a:[Ljava/lang/Class;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    iput v0, p0, LbvB;->a:I

    .line 171
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 174
    instance-of v1, p1, LbvB;

    if-eqz v1, :cond_0

    .line 175
    check-cast p1, LbvB;

    .line 176
    iget-object v1, p1, LbvB;->a:Ljava/lang/String;

    iget-object v2, p0, LbvB;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LbvB;->a:[Ljava/lang/Class;

    iget-object v2, p1, LbvB;->a:[Ljava/lang/Class;

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 178
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 182
    iget v0, p0, LbvB;->a:I

    return v0
.end method
