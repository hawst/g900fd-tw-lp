.class public LatR;
.super Ljava/lang/Object;
.source "WidgetConfigurator.java"


# static fields
.field private static final a:I


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LatE;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    invoke-static {}, LatF;->a()I

    move-result v0

    sput v0, LatR;->a:I

    return-void
.end method

.method constructor <init>(Ljava/util/List;)V
    .locals 0
    .param p1    # Ljava/util/List;
        .annotation runtime LatI;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LatE;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, LatR;->a:Ljava/util/List;

    .line 33
    return-void
.end method


# virtual methods
.method a()I
    .locals 1

    .prologue
    .line 82
    sget v0, LatR;->a:I

    return v0
.end method

.method a(Landroid/content/Context;[Landroid/accounts/Account;LaFO;)Landroid/widget/RemoteViews;
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 38
    array-length v2, p2

    .line 39
    invoke-static {p2, p3}, LSH;->a([Landroid/accounts/Account;LaFO;)I

    move-result v0

    .line 42
    if-gez v0, :cond_1

    .line 43
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    sget v2, LatN;->app_widget_broken:I

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 44
    if-nez p3, :cond_0

    .line 45
    sget v1, LatM;->widget_broken_title:I

    sget v2, LatO;->widget_configuration_missing:I

    .line 46
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 45
    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 77
    :goto_0
    return-object v0

    .line 48
    :cond_0
    sget v1, LatO;->widget_account_missing:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 49
    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p3}, LaFO;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 50
    sget v2, LatM;->widget_broken_title:I

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 52
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.SYNC_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 54
    invoke-virtual {p0}, LatR;->a()I

    move-result v2

    .line 53
    invoke-static {p1, v2, v1, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 55
    sget v2, LatM;->widget_broken_title:I

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto :goto_0

    .line 58
    :cond_1
    if-nez p3, :cond_2

    .line 59
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "null accountId"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 61
    :cond_2
    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    sget v3, LatN;->app_widget:I

    invoke-direct {v1, v0, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 62
    iget-object v0, p0, LatR;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LatE;

    .line 63
    invoke-virtual {v0, v1, p1, p3}, LatE;->a(Landroid/widget/RemoteViews;Landroid/content/Context;LaFO;)V

    goto :goto_1

    .line 66
    :cond_3
    if-le v2, v5, :cond_5

    .line 67
    sget v2, LatM;->widget_account_name:I

    if-eqz p3, :cond_4

    .line 68
    invoke-virtual {p3}, LaFO;->b()Ljava/lang/String;

    move-result-object v0

    .line 67
    :goto_2
    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 69
    sget v0, LatM;->widget_app_title_only:I

    invoke-virtual {v1, v0, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 70
    sget v0, LatM;->widget_app_title_account_container:I

    invoke-virtual {v1, v0, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    move-object v0, v1

    goto :goto_0

    .line 68
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 72
    :cond_5
    sget v0, LatM;->widget_account_name:I

    const-string v2, ""

    invoke-virtual {v1, v0, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 73
    sget v0, LatM;->widget_app_title_only:I

    invoke-virtual {v1, v0, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 74
    sget v0, LatM;->widget_app_title_account_container:I

    invoke-virtual {v1, v0, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    move-object v0, v1

    goto/16 :goto_0
.end method

.method public a(Landroid/appwidget/AppWidgetManager;ILandroid/content/Context;LaFO;)V
    .locals 5

    .prologue
    .line 87
    const-string v0, "WidgetConfigurator"

    const-string v1, "Configuring AppWidget %d [%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-static {p3}, LajS;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    .line 89
    invoke-virtual {p0, p3, v0, p4}, LatR;->a(Landroid/content/Context;[Landroid/accounts/Account;LaFO;)Landroid/widget/RemoteViews;

    move-result-object v0

    .line 90
    invoke-virtual {p1, p2, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 91
    return-void
.end method
