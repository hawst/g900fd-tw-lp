.class LadW;
.super Ljava/lang/Object;
.source "PipeExposer.java"

# interfaces
.implements LadC;


# instance fields
.field final synthetic a:LadV;

.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(LadV;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, LadW;->a:LadV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p2, p0, LadW;->a:Ljava/lang/String;

    .line 37
    return-void
.end method

.method static synthetic a(LadW;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, LadW;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, LadW;->a:LadV;

    invoke-static {v0}, LadV;->a(LadV;)LadJ;

    move-result-object v0

    iget-object v1, p0, LadW;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LadJ;->a(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(LbmY;)Landroid/os/ParcelFileDescriptor;
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "LadI;",
            ">;)",
            "Landroid/os/ParcelFileDescriptor;"
        }
    .end annotation

    .prologue
    .line 51
    sget-object v0, LadI;->b:LadI;

    invoke-virtual {p1, v0}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LadI;->c:LadI;

    invoke-virtual {p1, v0}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    :cond_0
    new-instance v0, LadB;

    const-string v1, "Writing is not supported"

    invoke-direct {v0, v1}, LadB;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_1
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 56
    new-instance v1, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    const/4 v2, 0x1

    aget-object v2, v0, v2

    invoke-direct {v1, v2}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    .line 57
    new-instance v2, LadX;

    const-string v3, "Decrypt and copy the content"

    invoke-direct {v2, p0, v3, v1}, LadX;-><init>(LadW;Ljava/lang/String;Ljava/io/OutputStream;)V

    .line 72
    invoke-virtual {v2}, LadX;->start()V

    .line 74
    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, LadW;->a:LadV;

    invoke-static {v0}, LadV;->a(LadV;)LadJ;

    move-result-object v0

    iget-object v1, p0, LadW;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LadJ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, LadW;->a:LadV;

    invoke-static {v0}, LadV;->a(LadV;)LadJ;

    move-result-object v0

    iget-object v1, p0, LadW;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LadJ;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 0

    .prologue
    .line 41
    return-void
.end method
