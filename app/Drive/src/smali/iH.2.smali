.class public LiH;
.super Lmu;
.source "WindowDecorActionBar.java"

# interfaces
.implements Ljc;


# instance fields
.field final synthetic a:LiD;

.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljb;

.field private a:Lmv;


# direct methods
.method public constructor <init>(LiD;Lmv;)V
    .locals 2

    .prologue
    .line 951
    iput-object p1, p0, LiH;->a:LiD;

    invoke-direct {p0}, Lmu;-><init>()V

    .line 952
    iput-object p2, p0, LiH;->a:Lmv;

    .line 953
    new-instance v0, Ljb;

    invoke-virtual {p1}, LiD;->a()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Ljb;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljb;->a(I)Ljb;

    move-result-object v0

    iput-object v0, p0, LiH;->a:Ljb;

    .line 955
    iget-object v0, p0, LiH;->a:Ljb;

    invoke-virtual {v0, p0}, Ljb;->a(Ljc;)V

    .line 956
    return-void
.end method


# virtual methods
.method public a()Landroid/view/Menu;
    .locals 1

    .prologue
    .line 965
    iget-object v0, p0, LiH;->a:Ljb;

    return-object v0
.end method

.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 1067
    iget-object v0, p0, LiH;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LiH;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1046
    iget-object v0, p0, LiH;->a:LiD;

    invoke-static {v0}, LiD;->a(LiD;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->a()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 970
    iget-object v0, p0, LiH;->a:LiD;

    iget-object v0, v0, LiD;->a:LiH;

    if-eq v0, p0, :cond_0

    .line 997
    :goto_0
    return-void

    .line 979
    :cond_0
    iget-object v0, p0, LiH;->a:LiD;

    invoke-static {v0}, LiD;->b(LiD;)Z

    move-result v0

    iget-object v1, p0, LiH;->a:LiD;

    invoke-static {v1}, LiD;->c(LiD;)Z

    move-result v1

    invoke-static {v0, v1, v2}, LiD;->a(ZZZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 982
    iget-object v0, p0, LiH;->a:LiD;

    iput-object p0, v0, LiD;->a:Lmu;

    .line 983
    iget-object v0, p0, LiH;->a:LiD;

    iget-object v1, p0, LiH;->a:Lmv;

    iput-object v1, v0, LiD;->a:Lmv;

    .line 987
    :goto_1
    iput-object v3, p0, LiH;->a:Lmv;

    .line 988
    iget-object v0, p0, LiH;->a:LiD;

    invoke-virtual {v0, v2}, LiD;->k(Z)V

    .line 991
    iget-object v0, p0, LiH;->a:LiD;

    invoke-static {v0}, LiD;->a(LiD;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->a()V

    .line 992
    iget-object v0, p0, LiH;->a:LiD;

    invoke-static {v0}, LiD;->a(LiD;)Lkf;

    move-result-object v0

    invoke-interface {v0}, Lkf;->a()Landroid/view/ViewGroup;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->sendAccessibilityEvent(I)V

    .line 994
    iget-object v0, p0, LiH;->a:LiD;

    invoke-static {v0}, LiD;->a(LiD;)Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    move-result-object v0

    iget-object v1, p0, LiH;->a:LiD;

    iget-boolean v1, v1, LiD;->a:Z

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->setHideOnContentScrollEnabled(Z)V

    .line 996
    iget-object v0, p0, LiH;->a:LiD;

    iput-object v3, v0, LiD;->a:LiH;

    goto :goto_0

    .line 985
    :cond_1
    iget-object v0, p0, LiH;->a:Lmv;

    invoke-interface {v0, p0}, Lmv;->a(Lmu;)V

    goto :goto_1
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 1036
    iget-object v0, p0, LiH;->a:LiD;

    invoke-static {v0}, LiD;->a(LiD;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LiH;->b(Ljava/lang/CharSequence;)V

    .line 1037
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1020
    iget-object v0, p0, LiH;->a:LiD;

    invoke-static {v0}, LiD;->a(LiD;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->setCustomView(Landroid/view/View;)V

    .line 1021
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LiH;->a:Ljava/lang/ref/WeakReference;

    .line 1022
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1026
    iget-object v0, p0, LiH;->a:LiD;

    invoke-static {v0}, LiD;->a(LiD;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 1027
    return-void
.end method

.method public a(Ljb;)V
    .locals 1

    .prologue
    .line 1098
    iget-object v0, p0, LiH;->a:Lmv;

    if-nez v0, :cond_0

    .line 1103
    :goto_0
    return-void

    .line 1101
    :cond_0
    invoke-virtual {p0}, LiH;->b()V

    .line 1102
    iget-object v0, p0, LiH;->a:LiD;

    invoke-static {v0}, LiD;->a(LiD;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->a()Z

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 1056
    invoke-super {p0, p1}, Lmu;->a(Z)V

    .line 1057
    iget-object v0, p0, LiH;->a:LiD;

    invoke-static {v0}, LiD;->a(LiD;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->setTitleOptional(Z)V

    .line 1058
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 1010
    iget-object v0, p0, LiH;->a:Ljb;

    invoke-virtual {v0}, Ljb;->b()V

    .line 1012
    :try_start_0
    iget-object v0, p0, LiH;->a:Lmv;

    iget-object v1, p0, LiH;->a:Ljb;

    invoke-interface {v0, p0, v1}, Lmv;->a(Lmu;Landroid/view/Menu;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1014
    iget-object v1, p0, LiH;->a:Ljb;

    invoke-virtual {v1}, Ljb;->c()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LiH;->a:Ljb;

    invoke-virtual {v1}, Ljb;->c()V

    throw v0
.end method

.method public a(Ljb;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 1071
    iget-object v0, p0, LiH;->a:Lmv;

    if-eqz v0, :cond_0

    .line 1072
    iget-object v0, p0, LiH;->a:Lmv;

    invoke-interface {v0, p0, p2}, Lmv;->a(Lmu;Landroid/view/MenuItem;)Z

    move-result v0

    .line 1074
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1051
    iget-object v0, p0, LiH;->a:LiD;

    invoke-static {v0}, LiD;->a(LiD;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->b()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 1001
    iget-object v0, p0, LiH;->a:Ljb;

    invoke-virtual {v0}, Ljb;->b()V

    .line 1003
    :try_start_0
    iget-object v0, p0, LiH;->a:Lmv;

    iget-object v1, p0, LiH;->a:Ljb;

    invoke-interface {v0, p0, v1}, Lmv;->b(Lmu;Landroid/view/Menu;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1005
    iget-object v0, p0, LiH;->a:Ljb;

    invoke-virtual {v0}, Ljb;->c()V

    .line 1007
    return-void

    .line 1005
    :catchall_0
    move-exception v0

    iget-object v1, p0, LiH;->a:Ljb;

    invoke-virtual {v1}, Ljb;->c()V

    throw v0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 1041
    iget-object v0, p0, LiH;->a:LiD;

    invoke-static {v0}, LiD;->a(LiD;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LiH;->a(Ljava/lang/CharSequence;)V

    .line 1042
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1031
    iget-object v0, p0, LiH;->a:LiD;

    invoke-static {v0}, LiD;->a(LiD;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->setTitle(Ljava/lang/CharSequence;)V

    .line 1032
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 1062
    iget-object v0, p0, LiH;->a:LiD;

    invoke-static {v0}, LiD;->a(LiD;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->b()Z

    move-result v0

    return v0
.end method
