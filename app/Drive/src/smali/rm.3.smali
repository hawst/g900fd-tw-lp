.class public Lrm;
.super LpH;
.source "BaseActivity.java"

# interfaces
.implements LFi;
.implements LqV;
.implements Lrl;


# instance fields
.field a:LVB;

.field private a:LaqY;

.field a:LaqZ;

.field public a:Latd;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lrq;",
            ">;"
        }
    .end annotation
.end field

.field a:LqA;

.field a:LqW;

.field public a:LsI;

.field private b:Landroid/os/Handler;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lro;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lrp;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z

.field private j:Z

.field private volatile k:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, LpH;-><init>()V

    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lrm;->i:Z

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lrm;->k:Z

    .line 93
    invoke-static {}, LbpU;->a()Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    iput-object v0, p0, Lrm;->a:Ljava/util/Set;

    .line 95
    invoke-static {}, LbpU;->a()Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    iput-object v0, p0, Lrm;->b:Ljava/util/Set;

    .line 96
    invoke-static {}, LbpU;->a()Ljava/util/concurrent/CopyOnWriteArraySet;

    move-result-object v0

    iput-object v0, p0, Lrm;->c:Ljava/util/Set;

    return-void
.end method

.method private o()V
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lrm;->a:LaqY;

    if-nez v0, :cond_0

    .line 202
    iget-object v0, p0, Lrm;->a:LaqZ;

    invoke-virtual {p0}, Lrm;->a()LarB;

    move-result-object v1

    invoke-interface {v0, p0, v1}, LaqZ;->a(Landroid/app/Activity;LarB;)LaqY;

    move-result-object v0

    iput-object v0, p0, Lrm;->a:LaqY;

    .line 204
    :cond_0
    return-void
.end method

.method private p()V
    .locals 1

    .prologue
    .line 329
    iget-boolean v0, p0, Lrm;->k:Z

    if-nez v0, :cond_0

    .line 330
    const/4 v0, 0x1

    iput-boolean v0, p0, Lrm;->k:Z

    .line 331
    invoke-virtual {p0}, Lrm;->i()V

    .line 333
    :cond_0
    return-void
.end method

.method private q()V
    .locals 2

    .prologue
    .line 366
    invoke-virtual {p0}, Lrm;->a()LaFO;

    move-result-object v0

    .line 367
    if-eqz v0, :cond_0

    .line 368
    iget-object v1, p0, Lrm;->a:LqW;

    invoke-interface {v1, v0}, LqW;->a(LaFO;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369
    invoke-virtual {p0}, Lrm;->finish()V

    .line 372
    :cond_0
    return-void
.end method


# virtual methods
.method protected a()LFf;
    .locals 2

    .prologue
    .line 400
    invoke-virtual {p0}, Lrm;->a()Lbuu;

    move-result-object v0

    const-class v1, LFf;

    invoke-interface {v0, v1}, Lbuu;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LFf;

    return-object v0
.end method

.method public a()LaFO;
    .locals 2

    .prologue
    .line 346
    invoke-virtual {p0}, Lrm;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 347
    const-string v0, "accountName"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    .line 348
    if-nez v0, :cond_0

    .line 349
    const-string v0, "entrySpec.v2"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 350
    if-eqz v0, :cond_1

    .line 351
    iget-object v0, v0, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    .line 359
    :cond_0
    :goto_0
    return-object v0

    .line 355
    :cond_1
    const-string v0, "resourceSpec"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/ResourceSpec;

    .line 356
    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lrm;->b:Landroid/os/Handler;

    return-object v0
.end method

.method public a(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 214
    invoke-super {p0, p1}, LpH;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 215
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    return-object v0
.end method

.method public a()LaqY;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lrm;->a:LaqY;

    return-object v0
.end method

.method public a()LarB;
    .locals 3

    .prologue
    .line 221
    new-instance v0, Lsd;

    iget-object v1, p0, Lrm;->a:LsI;

    iget-object v2, p0, Lrm;->a:LqK;

    invoke-direct {v0, p0, v1, v2}, Lsd;-><init>(Landroid/app/Activity;LsI;LqK;)V

    return-object v0
.end method

.method public a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 382
    const-class v0, LaqY;

    if-ne p1, v0, :cond_0

    .line 384
    iget-object v0, p0, Lrm;->a:LaqY;

    .line 388
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, LpH;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public a()LsI;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lrm;->a:LsI;

    return-object v0
.end method

.method public a(Lro;)V
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lrm;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 460
    return-void
.end method

.method public a(Lrp;)V
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lrm;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 468
    return-void
.end method

.method public a(Lrq;)V
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lrm;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 452
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x1

    return v0
.end method

.method public a_()V
    .locals 0

    .prologue
    .line 211
    return-void
.end method

.method public b(Lro;)V
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lrm;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 464
    return-void
.end method

.method public b(Lrp;)V
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lrm;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 472
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lrm;->a:Latd;

    invoke-virtual {v0}, Latd;->a()V

    .line 313
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 138
    iget-boolean v0, p0, Lrm;->j:Z

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 170
    iget-boolean v0, p0, Lrm;->i:Z

    return v0
.end method

.method public d()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 376
    invoke-virtual {p0}, Lrm;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 377
    if-eqz v1, :cond_0

    const-string v2, "appLaunch"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lrm;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrp;

    .line 131
    invoke-interface {v0}, Lrp;->a()V

    goto :goto_0

    .line 134
    :cond_0
    invoke-super {p0, p1}, LpH;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 427
    iget-boolean v0, p0, Lrm;->k:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lrm;->a:LaqY;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lrm;->a:LaqY;

    invoke-interface {v0}, LaqY;->a()V

    .line 145
    :cond_0
    return-void
.end method

.method protected h()V
    .locals 1

    .prologue
    .line 263
    invoke-super {p0}, LpH;->h()V

    .line 266
    iget-object v0, p0, Lrm;->a:LaqZ;

    invoke-interface {v0, p0}, LaqZ;->a(Landroid/app/Activity;)V

    .line 267
    return-void
.end method

.method public i()V
    .locals 0

    .prologue
    .line 342
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 2

    .prologue
    .line 112
    invoke-super {p0}, LpH;->onAttachedToWindow()V

    .line 114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lrm;->j:Z

    .line 116
    iget-object v0, p0, Lrm;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lro;

    .line 117
    invoke-interface {v0}, Lro;->a()V

    goto :goto_0

    .line 119
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 271
    invoke-super {p0, p1}, LpH;->onCreate(Landroid/os/Bundle;)V

    .line 272
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lrm;->requestWindowFeature(I)Z

    .line 273
    const/4 v0, 0x0

    iput-boolean v0, p0, Lrm;->k:Z

    .line 274
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lrm;->b:Landroid/os/Handler;

    .line 275
    if-nez p1, :cond_0

    .line 276
    invoke-virtual {p0}, Lrm;->a()LFf;

    move-result-object v0

    .line 277
    invoke-virtual {p0}, Lrm;->a()LM;

    move-result-object v1

    .line 276
    invoke-virtual {v0, v1, p0}, LFf;->a(LM;Landroid/content/Context;)Z

    move-result v0

    .line 278
    if-nez v0, :cond_0

    .line 279
    iget-object v0, p0, Lrm;->b:Landroid/os/Handler;

    new-instance v1, Lrn;

    invoke-direct {v1, p0}, Lrn;-><init>(Lrm;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 287
    :cond_0
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 123
    invoke-super {p0}, LpH;->onDetachedFromWindow()V

    .line 125
    const/4 v0, 0x0

    iput-boolean v0, p0, Lrm;->j:Z

    .line 126
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lrm;->a:LaqY;

    invoke-interface {v0, p1}, LaqY;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, LpH;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 164
    invoke-super {p0}, LpH;->onPause()V

    .line 165
    iget-object v0, p0, Lrm;->a:LVB;

    invoke-virtual {v0, p0}, LVB;->b(Lrm;)V

    .line 166
    const/4 v0, 0x1

    iput-boolean v0, p0, Lrm;->i:Z

    .line 167
    return-void
.end method

.method public onPostResume()V
    .locals 2

    .prologue
    .line 153
    invoke-super {p0}, LpH;->onPostResume()V

    .line 154
    const/4 v0, 0x0

    iput-boolean v0, p0, Lrm;->k:Z

    .line 155
    invoke-virtual {p0}, Lrm;->a()LFf;

    move-result-object v0

    invoke-virtual {p0}, Lrm;->a()LM;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, LFf;->a(LM;Landroid/content/Context;)V

    .line 157
    iget-object v0, p0, Lrm;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrq;

    .line 158
    invoke-interface {v0}, Lrq;->a()V

    goto :goto_0

    .line 160
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 100
    invoke-super {p0}, LpH;->onResume()V

    .line 102
    iget-object v0, p0, Lrm;->a:LVB;

    invoke-virtual {v0, p0}, LVB;->a(Lrm;)V

    .line 103
    const/4 v0, 0x0

    iput-boolean v0, p0, Lrm;->i:Z

    .line 105
    invoke-virtual {p0}, Lrm;->g()V

    .line 106
    invoke-direct {p0}, Lrm;->q()V

    .line 107
    iget-object v0, p0, Lrm;->a:LqA;

    iget-object v1, p0, Lrm;->a:LqK;

    invoke-interface {v0, v1}, LqA;->a(LqK;)V

    .line 108
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 318
    const-string v0, "BaseIsRestart"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 319
    invoke-super {p0, p1}, LpH;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 323
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 324
    invoke-direct {p0}, Lrm;->p()V

    .line 326
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 291
    invoke-super {p0}, LpH;->onStart()V

    .line 292
    const/4 v0, 0x0

    iput-boolean v0, p0, Lrm;->k:Z

    .line 293
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 297
    invoke-direct {p0}, Lrm;->p()V

    .line 298
    invoke-super {p0}, LpH;->onStop()V

    .line 299
    return-void
.end method

.method public setContentView(I)V
    .locals 0

    .prologue
    .line 177
    invoke-super {p0, p1}, LpH;->setContentView(I)V

    .line 178
    invoke-virtual {p0}, Lrm;->a_()V

    .line 179
    invoke-direct {p0}, Lrm;->o()V

    .line 180
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 186
    invoke-super {p0, p1}, LpH;->setContentView(Landroid/view/View;)V

    .line 187
    invoke-virtual {p0}, Lrm;->a_()V

    .line 188
    invoke-direct {p0}, Lrm;->o()V

    .line 189
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 195
    invoke-super {p0, p1, p2}, LpH;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 196
    invoke-virtual {p0}, Lrm;->a_()V

    .line 197
    invoke-direct {p0}, Lrm;->o()V

    .line 198
    return-void
.end method

.method public startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V
    .locals 6

    .prologue
    .line 235
    iget-object v0, p0, Lrm;->a:LaqY;

    invoke-virtual {p0}, Lrm;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, LaqY;->a(Ljava/lang/String;ZLandroid/content/ComponentName;Landroid/os/Bundle;Z)V

    .line 237
    return-void
.end method
