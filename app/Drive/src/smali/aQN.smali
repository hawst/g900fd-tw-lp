.class public abstract LaQN;
.super Ljava/lang/Object;


# instance fields
.field protected final a:LaRe;

.field private a:LaRg;

.field private final a:Ljava/lang/String;


# virtual methods
.method protected final a()J
    .locals 2

    iget-object v0, p0, LaQN;->a:LaRg;

    invoke-interface {v0}, LaRg;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(JI)V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method protected final a(Ljava/lang/String;JLjava/lang/String;)V
    .locals 8

    iget-object v0, p0, LaQN;->a:LaRe;

    const-string v1, "Sending text message: %s to: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p4, v2, v3

    invoke-virtual {v0, v1, v2}, LaRe;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, LaQN;->a:LaRg;

    iget-object v2, p0, LaQN;->a:Ljava/lang/String;

    move-object v3, p1

    move-wide v4, p2

    move-object v6, p4

    invoke-interface/range {v1 .. v6}, LaRg;->a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    return-void
.end method
