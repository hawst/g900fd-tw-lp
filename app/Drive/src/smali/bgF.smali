.class public LbgF;
.super LbgN;
.source "PdfImage.java"


# instance fields
.field private a:F

.field private a:I

.field private final a:J

.field private final a:Landroid/content/res/AssetManager;

.field private final a:Ljava/lang/String;

.field private final a:Z

.field private b:F

.field private b:I

.field private c:F

.field private final c:I

.field private d:F

.field private d:I

.field private e:F

.field private f:F


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 87
    invoke-direct {p0}, LbgN;-><init>()V

    .line 53
    const/16 v0, 0x8

    iput v0, p0, LbgF;->c:I

    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, LbgF;->a:Landroid/content/res/AssetManager;

    .line 89
    iput-object p1, p0, LbgF;->a:Ljava/lang/String;

    .line 90
    iput-boolean p2, p0, LbgF;->a:Z

    .line 91
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, LbgF;->a:J

    .line 92
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 93
    invoke-direct {p0, v0}, LbgF;->a(Ljava/io/InputStream;)V

    .line 94
    return-void
.end method

.method private a(Ljava/io/InputStream;)V
    .locals 2

    .prologue
    .line 148
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 149
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 150
    const/4 v1, 0x0

    invoke-static {p1, v1, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 151
    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iput v1, p0, LbgF;->a:I

    .line 152
    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iput v0, p0, LbgF;->b:I

    .line 153
    iget v0, p0, LbgF;->a:I

    int-to-float v0, v0

    iput v0, p0, LbgF;->e:F

    .line 154
    iget v0, p0, LbgF;->b:I

    int-to-float v0, v0

    iput v0, p0, LbgF;->f:F

    .line 155
    return-void
.end method


# virtual methods
.method public a()F
    .locals 1

    .prologue
    .line 201
    iget v0, p0, LbgF;->a:F

    return v0
.end method

.method public a()I
    .locals 1

    .prologue
    .line 163
    iget v0, p0, LbgF;->a:I

    return v0
.end method

.method protected a(Ljava/io/OutputStream;)I
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 215
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "<</Type/XObject/Subtype/Image/Width %d/Height %d/BitsPerComponent %d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, LbgF;->a:I

    .line 216
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    iget v3, p0, LbgF;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    const/16 v3, 0x8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 215
    invoke-static {v0}, LbgI;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 217
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 218
    array-length v1, v0

    .line 219
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "/ColorSpace/%s/Length %d/Filter/DCTDecode>>"

    new-array v4, v4, [Ljava/lang/Object;

    iget-boolean v0, p0, LbgF;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "DeviceGray"

    :goto_0
    aput-object v0, v4, v5

    iget-wide v6, p0, LbgF;->a:J

    .line 220
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v8

    .line 219
    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LbgI;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 221
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 222
    array-length v0, v0

    add-int/2addr v1, v0

    .line 224
    iget-object v0, p0, LbgF;->a:Landroid/content/res/AssetManager;

    if-eqz v0, :cond_1

    .line 225
    iget-object v0, p0, LbgF;->a:Landroid/content/res/AssetManager;

    iget-object v2, p0, LbgF;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 229
    :goto_1
    invoke-virtual {p0, p1, v0}, LbgF;->a(Ljava/io/OutputStream;Ljava/io/InputStream;)I

    move-result v2

    add-int/2addr v1, v2

    .line 230
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 238
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 239
    return v1

    .line 219
    :cond_0
    const-string v0, "DeviceRGB"

    goto :goto_0

    .line 227
    :cond_1
    new-instance v0, Ljava/io/FileInputStream;

    iget-object v2, p0, LbgF;->a:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 248
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "img%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, LbgF;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(F)V
    .locals 4

    .prologue
    .line 259
    const/high16 v0, 0x43340000    # 180.0f

    div-float v0, p1, v0

    const v1, 0x40490fdb    # (float)Math.PI

    mul-float/2addr v0, v1

    .line 260
    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    .line 261
    iget v1, p0, LbgF;->c:F

    add-float/2addr v0, v1

    float-to-double v0, v0

    rem-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, LbgF;->d:F

    .line 262
    iget v0, p0, LbgF;->d:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 263
    iget v0, p0, LbgF;->d:F

    float-to-double v0, v0

    add-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, LbgF;->d:F

    .line 265
    :cond_0
    invoke-virtual {p0}, LbgF;->a()[F

    .line 266
    return-void
.end method

.method public a(FF)V
    .locals 0

    .prologue
    .line 191
    iput p1, p0, LbgF;->a:F

    .line 192
    iput p2, p0, LbgF;->b:F

    .line 193
    return-void
.end method

.method a(I)V
    .locals 0

    .prologue
    .line 139
    iput p1, p0, LbgF;->d:I

    .line 140
    return-void
.end method

.method public a()[F
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 286
    const/16 v0, 0x8

    new-array v0, v0, [F

    .line 287
    iget v1, p0, LbgF;->d:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    double-to-float v1, v2

    .line 288
    iget v2, p0, LbgF;->d:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v2, v2

    .line 289
    iget v3, p0, LbgF;->e:F

    mul-float/2addr v3, v1

    aput v3, v0, v6

    .line 290
    iget v3, p0, LbgF;->e:F

    mul-float/2addr v3, v2

    aput v3, v0, v7

    .line 291
    iget v3, p0, LbgF;->f:F

    neg-float v3, v3

    mul-float/2addr v2, v3

    aput v2, v0, v8

    .line 292
    iget v2, p0, LbgF;->f:F

    mul-float/2addr v1, v2

    aput v1, v0, v9

    .line 293
    iget v1, p0, LbgF;->d:F

    float-to-double v2, v1

    const-wide v4, 0x3ff921fb54442d18L    # 1.5707963267948966

    cmpg-double v1, v2, v4

    if-gez v1, :cond_0

    .line 294
    const/4 v1, 0x4

    aget v2, v0, v8

    aput v2, v0, v1

    .line 295
    const/4 v1, 0x5

    aput v10, v0, v1

    .line 296
    const/4 v1, 0x6

    aget v2, v0, v6

    aput v2, v0, v1

    .line 297
    const/4 v1, 0x7

    aget v2, v0, v7

    aget v3, v0, v9

    add-float/2addr v2, v3

    aput v2, v0, v1

    .line 314
    :goto_0
    return-object v0

    .line 298
    :cond_0
    iget v1, p0, LbgF;->d:F

    float-to-double v2, v1

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    cmpg-double v1, v2, v4

    if-gez v1, :cond_1

    .line 299
    const/4 v1, 0x4

    aget v2, v0, v6

    aget v3, v0, v8

    add-float/2addr v2, v3

    aput v2, v0, v1

    .line 300
    const/4 v1, 0x5

    aget v2, v0, v9

    aput v2, v0, v1

    .line 301
    const/4 v1, 0x6

    aput v10, v0, v1

    .line 302
    const/4 v1, 0x7

    aget v2, v0, v7

    aput v2, v0, v1

    goto :goto_0

    .line 303
    :cond_1
    iget v1, p0, LbgF;->d:F

    float-to-double v2, v1

    const-wide v4, 0x4012d97c7f3321d2L    # 4.71238898038469

    cmpg-double v1, v2, v4

    if-gez v1, :cond_2

    .line 304
    const/4 v1, 0x4

    aget v2, v0, v6

    aput v2, v0, v1

    .line 305
    const/4 v1, 0x5

    aget v2, v0, v7

    aget v3, v0, v9

    add-float/2addr v2, v3

    aput v2, v0, v1

    .line 306
    const/4 v1, 0x6

    aget v2, v0, v8

    aput v2, v0, v1

    .line 307
    const/4 v1, 0x7

    aput v10, v0, v1

    goto :goto_0

    .line 309
    :cond_2
    const/4 v1, 0x4

    aput v10, v0, v1

    .line 310
    const/4 v1, 0x5

    aget v2, v0, v7

    aput v2, v0, v1

    .line 311
    const/4 v1, 0x6

    aget v2, v0, v6

    aget v3, v0, v8

    add-float/2addr v2, v3

    aput v2, v0, v1

    .line 312
    const/4 v1, 0x7

    aget v2, v0, v9

    aput v2, v0, v1

    goto :goto_0
.end method

.method public b()F
    .locals 1

    .prologue
    .line 210
    iget v0, p0, LbgF;->b:F

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 172
    iget v0, p0, LbgF;->b:I

    return v0
.end method

.method public b(FF)V
    .locals 0

    .prologue
    .line 275
    iput p1, p0, LbgF;->e:F

    .line 276
    iput p2, p0, LbgF;->f:F

    .line 277
    invoke-virtual {p0}, LbgF;->a()[F

    .line 278
    return-void
.end method
