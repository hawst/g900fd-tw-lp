.class LYH;
.super Ljava/lang/Object;
.source "PunchThumbnailGeneratorFactoryImpl.java"

# interfaces
.implements LXB;


# instance fields
.field final synthetic a:I

.field final synthetic a:LYB;


# direct methods
.method constructor <init>(LYB;I)V
    .locals 0

    .prologue
    .line 274
    iput-object p1, p0, LYH;->a:LYB;

    iput p2, p0, LYH;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 284
    const-string v0, "PunchThumbnailGeneratorFactoryImpl"

    const-string v1, "t=%s onSlideContentRequestFailed: slideIndex=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LYH;->a:LYB;

    .line 285
    invoke-static {v4}, LYB;->b(LYB;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, LYH;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 284
    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 286
    iget-object v0, p0, LYH;->a:LYB;

    iget-object v0, v0, LYB;->a:LYA;

    invoke-static {v0}, LYA;->a(LYA;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LYI;

    invoke-direct {v1, p0}, LYI;-><init>(LYH;)V

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 292
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 277
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    const-string v0, "PunchThumbnailGeneratorFactoryImpl"

    const-string v1, "t=%s Received content: slideIndex=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LYH;->a:LYB;

    invoke-static {v4}, LYB;->b(LYB;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, LYH;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 279
    iget-object v0, p0, LYH;->a:LYB;

    invoke-static {v0, p1, p2}, LYB;->a(LYB;Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    return-void
.end method
