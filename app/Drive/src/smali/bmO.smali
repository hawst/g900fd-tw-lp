.class final LbmO;
.super LbmY;
.source "ImmutableMapKeySet.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LbmY",
        "<TK;>;"
    }
.end annotation


# instance fields
.field private final a:LbmL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmL",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LbmL;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmL",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0}, LbmY;-><init>()V

    .line 38
    iput-object p1, p0, LbmO;->a:LbmL;

    .line 39
    return-void
.end method


# virtual methods
.method public a()Lbqv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbqv",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 47
    invoke-virtual {p0}, LbmO;->a()LbmF;

    move-result-object v0

    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v0

    return-object v0
.end method

.method a()Z
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x1

    return v0
.end method

.method b()LbmF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmF",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, LbmO;->a:LbmL;

    invoke-virtual {v0}, LbmL;->a()LbmY;

    move-result-object v0

    invoke-virtual {v0}, LbmY;->a()LbmF;

    move-result-object v0

    .line 58
    new-instance v1, LbmP;

    invoke-direct {v1, p0, v0}, LbmP;-><init>(LbmO;LbmF;)V

    return-object v1
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, LbmO;->a:LbmL;

    invoke-virtual {v0, p1}, LbmL;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, LbmO;->a()Lbqv;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, LbmO;->a:LbmL;

    invoke-virtual {v0}, LbmL;->size()I

    move-result v0

    return v0
.end method
