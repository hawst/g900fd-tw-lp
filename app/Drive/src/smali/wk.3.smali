.class public final enum Lwk;
.super Ljava/lang/Enum;
.source "MimeTypeFilter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lwk;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lwk;

.field private static final synthetic a:[Lwk;

.field public static final enum b:Lwk;

.field public static final enum c:Lwk;

.field public static final enum d:Lwk;

.field public static final enum e:Lwk;

.field public static final enum f:Lwk;

.field public static final enum g:Lwk;

.field public static final enum h:Lwk;


# instance fields
.field private final a:LaGv;

.field private final a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 21
    new-instance v0, Lwk;

    const-string v1, "COLLECTION"

    sget-object v2, LaGv;->a:LaGv;

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "vnd.android.cursor.dir/doc"

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v5, v2, v3}, Lwk;-><init>(Ljava/lang/String;ILaGv;[Ljava/lang/String;)V

    sput-object v0, Lwk;->a:Lwk;

    .line 22
    new-instance v0, Lwk;

    const-string v1, "DOCUMENT"

    sget-object v2, LaGv;->b:LaGv;

    new-array v3, v5, [Ljava/lang/String;

    invoke-direct {v0, v1, v6, v2, v3}, Lwk;-><init>(Ljava/lang/String;ILaGv;[Ljava/lang/String;)V

    sput-object v0, Lwk;->b:Lwk;

    .line 23
    new-instance v0, Lwk;

    const-string v1, "DRAWING"

    sget-object v2, LaGv;->c:LaGv;

    new-array v3, v5, [Ljava/lang/String;

    invoke-direct {v0, v1, v7, v2, v3}, Lwk;-><init>(Ljava/lang/String;ILaGv;[Ljava/lang/String;)V

    sput-object v0, Lwk;->c:Lwk;

    .line 24
    new-instance v0, Lwk;

    const-string v1, "FORM"

    sget-object v2, LaGv;->e:LaGv;

    new-array v3, v5, [Ljava/lang/String;

    invoke-direct {v0, v1, v8, v2, v3}, Lwk;-><init>(Ljava/lang/String;ILaGv;[Ljava/lang/String;)V

    sput-object v0, Lwk;->d:Lwk;

    .line 25
    new-instance v0, Lwk;

    const-string v1, "PRESENTATION"

    sget-object v2, LaGv;->g:LaGv;

    new-array v3, v5, [Ljava/lang/String;

    invoke-direct {v0, v1, v9, v2, v3}, Lwk;-><init>(Ljava/lang/String;ILaGv;[Ljava/lang/String;)V

    sput-object v0, Lwk;->e:Lwk;

    .line 26
    new-instance v0, Lwk;

    const-string v1, "SITE"

    const/4 v2, 0x5

    sget-object v3, LaGv;->h:LaGv;

    new-array v4, v5, [Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lwk;-><init>(Ljava/lang/String;ILaGv;[Ljava/lang/String;)V

    sput-object v0, Lwk;->f:Lwk;

    .line 27
    new-instance v0, Lwk;

    const-string v1, "SPREADSHEET"

    const/4 v2, 0x6

    sget-object v3, LaGv;->i:LaGv;

    new-array v4, v5, [Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lwk;-><init>(Ljava/lang/String;ILaGv;[Ljava/lang/String;)V

    sput-object v0, Lwk;->g:Lwk;

    .line 28
    new-instance v0, Lwk;

    const-string v1, "TABLE"

    const/4 v2, 0x7

    sget-object v3, LaGv;->j:LaGv;

    new-array v4, v5, [Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lwk;-><init>(Ljava/lang/String;ILaGv;[Ljava/lang/String;)V

    sput-object v0, Lwk;->h:Lwk;

    .line 18
    const/16 v0, 0x8

    new-array v0, v0, [Lwk;

    sget-object v1, Lwk;->a:Lwk;

    aput-object v1, v0, v5

    sget-object v1, Lwk;->b:Lwk;

    aput-object v1, v0, v6

    sget-object v1, Lwk;->c:Lwk;

    aput-object v1, v0, v7

    sget-object v1, Lwk;->d:Lwk;

    aput-object v1, v0, v8

    sget-object v1, Lwk;->e:Lwk;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lwk;->f:Lwk;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lwk;->g:Lwk;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lwk;->h:Lwk;

    aput-object v2, v0, v1

    sput-object v0, Lwk;->a:[Lwk;

    return-void
.end method

.method private varargs constructor <init>(Ljava/lang/String;ILaGv;[Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGv;",
            "[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 35
    iput-object p3, p0, Lwk;->a:LaGv;

    .line 36
    invoke-static {}, LbmY;->a()Lbna;

    move-result-object v0

    .line 37
    invoke-virtual {v0, p4}, Lbna;->a([Ljava/lang/Object;)Lbna;

    move-result-object v0

    invoke-virtual {p3}, LaGv;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbna;->a(Ljava/lang/Object;)Lbna;

    move-result-object v0

    invoke-virtual {v0}, Lbna;->a()LbmY;

    move-result-object v0

    iput-object v0, p0, Lwk;->a:LbmY;

    .line 38
    return-void
.end method

.method static synthetic a(Lwk;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lwk;->b()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 52
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lwk;->a:LbmY;

    invoke-virtual {v0, p1}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lwk;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lwk;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private b()Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 3

    .prologue
    .line 48
    new-instance v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LaES;->t:LaES;

    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "!=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lwk;->a:LaGv;

    invoke-virtual {v2}, LaGv;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lwk;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lwk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lwk;

    return-object v0
.end method

.method public static values()[Lwk;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lwk;->a:[Lwk;

    invoke-virtual {v0}, [Lwk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lwk;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 3

    .prologue
    .line 44
    new-instance v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LaES;->t:LaES;

    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lwk;->a:LaGv;

    invoke-virtual {v2}, LaGv;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
