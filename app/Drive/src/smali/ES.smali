.class public LES;
.super Ljava/lang/Object;
.source "DocumentOpenerErrorDialogFragment.java"


# instance fields
.field private final a:LM;

.field private final a:Landroid/os/Bundle;


# direct methods
.method private constructor <init>(LM;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 189
    iput-object p1, p0, LES;->a:LM;

    .line 190
    invoke-static {p2, p3, p4}, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a(Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, LES;->a:Landroid/os/Bundle;

    .line 191
    return-void
.end method

.method public constructor <init>(LM;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 201
    invoke-direct {p0, p1, p3, p4, p5}, LES;-><init>(LM;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iget-object v0, p0, LES;->a:Landroid/os/Bundle;

    const-string v1, "entrySpec.v2"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 203
    return-void
.end method

.method public constructor <init>(LM;Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 195
    invoke-direct {p0, p1, p3, p4, p5}, LES;-><init>(LM;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, LES;->a:Landroid/os/Bundle;

    const-string v1, "resourceSpec"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 197
    return-void
.end method


# virtual methods
.method public a(Z)LES;
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, LES;->a:Landroid/os/Bundle;

    const-string v1, "canRetry"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 226
    return-object p0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 234
    iget-object v0, p0, LES;->a:LM;

    iget-object v1, p0, LES;->a:Landroid/os/Bundle;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a(LM;Landroid/os/Bundle;)V

    .line 235
    return-void
.end method
