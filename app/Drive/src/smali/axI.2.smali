.class public LaxI;
.super Ljava/lang/Object;
.source "PdfPageLoader.java"


# instance fields
.field a:I

.field a:LaxJ;

.field a:LaxK;

.field a:LaxL;

.field a:LaxN;

.field a:LaxO;

.field private final a:Laxz;

.field a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LaxM;",
            ">;"
        }
    .end annotation
.end field

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 51
    invoke-static {}, Lcom/google/android/apps/viewer/util/ParcelUtils;->a()V

    .line 52
    return-void
.end method

.method constructor <init>(Laxz;I)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LaxI;->a:Ljava/util/Map;

    .line 54
    iput-object p1, p0, LaxI;->a:Laxz;

    .line 55
    iput p2, p0, LaxI;->b:I

    .line 56
    return-void
.end method

.method static synthetic a(LaxI;)I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, LaxI;->b:I

    return v0
.end method

.method static synthetic a(LaxI;)Laxz;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, LaxI;->a:Laxz;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, LaxI;->a:LaxJ;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, LaxI;->a:LaxJ;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LaxJ;->cancel(Z)Z

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, LaxI;->a:LaxJ;

    .line 70
    :cond_0
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, LaxI;->a:LaxL;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, LaxI;->a:LaxL;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LaxL;->cancel(Z)Z

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, LaxI;->a:LaxL;

    .line 87
    :cond_0
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, LaxI;->a:LaxK;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, LaxI;->a:LaxK;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LaxK;->cancel(Z)Z

    .line 134
    const/4 v0, 0x0

    iput-object v0, p0, LaxI;->a:LaxK;

    .line 136
    :cond_0
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, LaxI;->a:LaxN;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, LaxI;->a:LaxN;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LaxN;->cancel(Z)Z

    .line 151
    const/4 v0, 0x0

    iput-object v0, p0, LaxI;->a:LaxN;

    .line 153
    :cond_0
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, LaxI;->a:LaxO;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, LaxI;->a:LaxO;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LaxO;->cancel(Z)Z

    .line 174
    const/4 v0, 0x0

    iput-object v0, p0, LaxI;->a:LaxO;

    .line 176
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, LaxI;->a:LaxJ;

    if-nez v0, :cond_0

    .line 60
    new-instance v0, LaxJ;

    invoke-direct {v0, p0}, LaxJ;-><init>(LaxI;)V

    iput-object v0, p0, LaxI;->a:LaxJ;

    .line 61
    iget-object v0, p0, LaxI;->a:Laxz;

    iget-object v0, v0, Laxz;->a:LaxP;

    iget-object v1, p0, LaxI;->a:LaxJ;

    invoke-virtual {v0, v1}, LaxP;->a(Laxy;)V

    .line 63
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/viewer/client/Dimensions;)V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, LaxI;->a:LaxL;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaxI;->a:LaxL;

    iget-object v0, v0, LaxL;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v0, v0, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    iget v1, p1, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    if-ge v0, v1, :cond_0

    .line 74
    invoke-direct {p0}, LaxI;->f()V

    .line 76
    :cond_0
    iget-object v0, p0, LaxI;->a:LaxL;

    if-nez v0, :cond_1

    .line 77
    new-instance v0, LaxL;

    invoke-direct {v0, p0, p1}, LaxL;-><init>(LaxI;Lcom/google/android/apps/viewer/client/Dimensions;)V

    iput-object v0, p0, LaxI;->a:LaxL;

    .line 78
    iget-object v0, p0, LaxI;->a:Laxz;

    iget-object v0, v0, Laxz;->a:LaxP;

    iget-object v1, p0, LaxI;->a:LaxL;

    invoke-virtual {v0, v1}, LaxP;->a(Laxy;)V

    .line 80
    :cond_1
    return-void
.end method

.method public a(Lcom/google/android/apps/viewer/client/Dimensions;Ljava/lang/Iterable;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/viewer/client/Dimensions;",
            "Ljava/lang/Iterable",
            "<",
            "Lawr;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, LaxI;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, LaxI;->a:I

    iget v1, p1, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    if-eq v0, v1, :cond_0

    .line 95
    invoke-virtual {p0}, LaxI;->b()V

    .line 97
    :cond_0
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawr;

    .line 98
    new-instance v2, LaxM;

    invoke-direct {v2, p0, p1, v0}, LaxM;-><init>(LaxI;Lcom/google/android/apps/viewer/client/Dimensions;Lawr;)V

    .line 99
    iget-object v3, p0, LaxI;->a:Ljava/util/Map;

    invoke-virtual {v0}, Lawr;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 100
    iget-object v3, p0, LaxI;->a:Ljava/util/Map;

    invoke-virtual {v0}, Lawr;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    iget-object v0, p0, LaxI;->a:Laxz;

    iget-object v0, v0, Laxz;->a:LaxP;

    invoke-virtual {v0, v2}, LaxP;->a(Laxy;)V

    goto :goto_0

    .line 104
    :cond_2
    iget v0, p1, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    iput v0, p0, LaxI;->a:I

    .line 105
    return-void
.end method

.method public a(Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;)V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, LaxI;->a:LaxO;

    if-eqz v0, :cond_0

    .line 159
    if-ne p1, p2, :cond_1

    .line 160
    invoke-direct {p0}, LaxI;->i()V

    .line 165
    :cond_0
    iget-object v0, p0, LaxI;->a:LaxO;

    if-nez v0, :cond_1

    .line 166
    new-instance v0, LaxO;

    invoke-direct {v0, p0, p1, p2}, LaxO;-><init>(LaxI;Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;)V

    iput-object v0, p0, LaxI;->a:LaxO;

    .line 167
    iget-object v0, p0, LaxI;->a:Laxz;

    iget-object v0, v0, Laxz;->a:LaxP;

    iget-object v1, p0, LaxI;->a:LaxO;

    invoke-virtual {v0, v1}, LaxP;->a(Laxy;)V

    .line 169
    :cond_1
    return-void
.end method

.method a(Ljava/lang/Iterable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 108
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 109
    iget-object v2, p0, LaxI;->a:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaxM;

    .line 110
    if-eqz v0, :cond_0

    .line 111
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LaxM;->cancel(Z)Z

    goto :goto_0

    .line 114
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, LaxI;->a:LaxN;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaxI;->a:LaxN;

    invoke-static {v0}, LaxN;->a(LaxN;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 140
    invoke-direct {p0}, LaxI;->h()V

    .line 142
    :cond_0
    iget-object v0, p0, LaxI;->a:LaxN;

    if-nez v0, :cond_1

    .line 143
    new-instance v0, LaxN;

    invoke-direct {v0, p0, p1}, LaxN;-><init>(LaxI;Ljava/lang/String;)V

    iput-object v0, p0, LaxI;->a:LaxN;

    .line 144
    iget-object v0, p0, LaxI;->a:Laxz;

    iget-object v0, v0, Laxz;->a:LaxP;

    iget-object v1, p0, LaxI;->a:LaxN;

    invoke-virtual {v0, v1}, LaxP;->a(Laxy;)V

    .line 146
    :cond_1
    return-void
.end method

.method b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 117
    iget-object v0, p0, LaxI;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaxM;

    .line 118
    invoke-virtual {v0, v2}, LaxM;->cancel(Z)Z

    goto :goto_0

    .line 120
    :cond_0
    iget-object v0, p0, LaxI;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 121
    iput v2, p0, LaxI;->a:I

    .line 122
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, LaxI;->a:LaxK;

    if-nez v0, :cond_0

    .line 126
    new-instance v0, LaxK;

    invoke-direct {v0, p0}, LaxK;-><init>(LaxI;)V

    iput-object v0, p0, LaxI;->a:LaxK;

    .line 127
    iget-object v0, p0, LaxI;->a:Laxz;

    iget-object v0, v0, Laxz;->a:LaxP;

    iget-object v1, p0, LaxI;->a:LaxK;

    invoke-virtual {v0, v1}, LaxP;->a(Laxy;)V

    .line 129
    :cond_0
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 179
    invoke-direct {p0}, LaxI;->e()V

    .line 180
    invoke-direct {p0}, LaxI;->f()V

    .line 181
    invoke-virtual {p0}, LaxI;->b()V

    .line 182
    invoke-direct {p0}, LaxI;->g()V

    .line 183
    invoke-direct {p0}, LaxI;->h()V

    .line 184
    invoke-direct {p0}, LaxI;->i()V

    .line 185
    return-void
.end method
