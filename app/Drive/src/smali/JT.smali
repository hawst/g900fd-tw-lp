.class public final LJT;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LJU;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LJS;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LJR;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 40
    iput-object p1, p0, LJT;->a:LbrA;

    .line 41
    const-class v0, LJU;

    const-class v1, LaiC;

    invoke-static {v0, v1}, LJT;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJT;->a:Lbsk;

    .line 44
    const-class v0, LJS;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LJT;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJT;->b:Lbsk;

    .line 47
    const-class v0, LJR;

    const-class v1, LaiC;

    invoke-static {v0, v1}, LJT;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LJT;->c:Lbsk;

    .line 50
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 74
    sparse-switch p1, :sswitch_data_0

    .line 112
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :sswitch_0
    new-instance v2, LJU;

    iget-object v0, p0, LJT;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LapK;

    iget-object v0, v0, LapK;->c:Lbsk;

    .line 79
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LJT;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LapK;

    iget-object v1, v1, LapK;->c:Lbsk;

    .line 77
    invoke-static {v0, v1}, LJT;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaqT;

    iget-object v1, p0, LJT;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->a:Lbsk;

    .line 85
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LJT;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lc;

    iget-object v3, v3, Lc;->a:Lbsk;

    .line 83
    invoke-static {v1, v3}, LJT;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-direct {v2, v0, v1}, LJU;-><init>(LaqT;Landroid/content/Context;)V

    move-object v0, v2

    .line 110
    :goto_0
    return-object v0

    .line 92
    :sswitch_1
    new-instance v0, LJS;

    invoke-direct {v0}, LJS;-><init>()V

    goto :goto_0

    .line 96
    :sswitch_2
    new-instance v2, LJR;

    iget-object v0, p0, LJT;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->j:Lbsk;

    .line 99
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LJT;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->j:Lbsk;

    .line 97
    invoke-static {v0, v1}, LJT;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFO;

    iget-object v1, p0, LJT;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LJT;

    iget-object v1, v1, LJT;->b:Lbsk;

    .line 105
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LJT;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LJT;

    iget-object v3, v3, LJT;->b:Lbsk;

    .line 103
    invoke-static {v1, v3}, LJT;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LJS;

    invoke-direct {v2, v0, v1}, LJR;-><init>(LaFO;LJS;)V

    move-object v0, v2

    .line 110
    goto :goto_0

    .line 74
    :sswitch_data_0
    .sparse-switch
        0xe7 -> :sswitch_2
        0x1d2 -> :sswitch_0
        0x1d4 -> :sswitch_1
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 127
    .line 129
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 57
    const-class v0, LJU;

    iget-object v1, p0, LJT;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LJT;->a(Ljava/lang/Class;Lbsk;)V

    .line 58
    const-class v0, LJS;

    iget-object v1, p0, LJT;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LJT;->a(Ljava/lang/Class;Lbsk;)V

    .line 59
    const-class v0, LJR;

    iget-object v1, p0, LJT;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LJT;->a(Ljava/lang/Class;Lbsk;)V

    .line 60
    iget-object v0, p0, LJT;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1d2

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 62
    iget-object v0, p0, LJT;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x1d4

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 64
    iget-object v0, p0, LJT;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xe7

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 66
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 119
    .line 121
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 70
    return-void
.end method
