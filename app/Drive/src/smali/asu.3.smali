.class Lasu;
.super Ljava/lang/Object;
.source "PhotoBackupAnnouncementHelper.java"

# interfaces
.implements LbsJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbsJ",
        "<",
        "Landroid/app/PendingIntent;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lass;

.field final synthetic a:Lasv;


# direct methods
.method constructor <init>(Lass;Lasv;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lasu;->a:Lass;

    iput-object p2, p0, Lasu;->a:Lasv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/app/PendingIntent;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 107
    :try_start_0
    iget-object v0, p0, Lasu;->a:Lass;

    invoke-static {v0}, Lass;->a(Lass;)Laja;

    move-result-object v0

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 108
    invoke-virtual {p1}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/app/Activity;->startIntentSender(Landroid/content/IntentSender;Landroid/content/Intent;III)V

    .line 109
    iget-object v0, p0, Lasu;->a:Lasv;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lasv;->a(Z)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    :goto_0
    return-void

    .line 110
    :catch_0
    move-exception v0

    .line 111
    const-string v1, "PhotoBackupAnnouncementHelper"

    const-string v2, "Failed to start photo backup promo activity."

    invoke-static {v1, v0, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 112
    iget-object v0, p0, Lasu;->a:Lasv;

    invoke-interface {v0, v6}, Lasv;->a(Z)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 103
    check-cast p1, Landroid/app/PendingIntent;

    invoke-virtual {p0, p1}, Lasu;->a(Landroid/app/PendingIntent;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 118
    const-string v0, "PhotoBackupAnnouncementHelper"

    const-string v1, "Failed to start photo backup promo activity."

    invoke-static {v0, p1, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 119
    iget-object v0, p0, Lasu;->a:Lasv;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lasv;->a(Z)V

    .line 120
    return-void
.end method
