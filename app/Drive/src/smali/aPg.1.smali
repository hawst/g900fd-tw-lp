.class public final LaPg;
.super Ljava/lang/Object;


# direct methods
.method public static a(Landroid/content/Context;Lcom/google/android/gms/internal/dx;LaPh;)LaPN;
    .locals 1

    iget-object v0, p1, Lcom/google/android/gms/internal/dx;->a:Lcom/google/android/gms/internal/fa;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/fa;->a:Z

    if-eqz v0, :cond_0

    invoke-static {p0, p1, p2}, LaPg;->b(Landroid/content/Context;Lcom/google/android/gms/internal/dx;LaPh;)LaPN;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1, p2}, LaPg;->c(Landroid/content/Context;Lcom/google/android/gms/internal/dx;LaPh;)LaPN;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;Lcom/google/android/gms/internal/dx;LaPh;)LaPN;
    .locals 1

    const-string v0, "Fetching ad response from local ad request service."

    invoke-static {v0}, LaQc;->a(Ljava/lang/String;)V

    new-instance v0, LaPj;

    invoke-direct {v0, p0, p1, p2}, LaPj;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/dx;LaPh;)V

    invoke-virtual {v0}, LaPj;->d()V

    return-object v0
.end method

.method private static c(Landroid/content/Context;Lcom/google/android/gms/internal/dx;LaPh;)LaPN;
    .locals 1

    const-string v0, "Fetching ad response from remote ad request service."

    invoke-static {v0}, LaQc;->a(Ljava/lang/String;)V

    invoke-static {p0}, LaCJ;->a(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Failed to connect to remote ad request service."

    invoke-static {v0}, LaQc;->e(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LaPk;

    invoke-direct {v0, p0, p1, p2}, LaPk;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/dx;LaPh;)V

    goto :goto_0
.end method
