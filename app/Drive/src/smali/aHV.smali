.class public LaHV;
.super Ljava/lang/Object;
.source "DocumentCursorRowFactory.java"


# static fields
.field private static a:LbmL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmL",
            "<",
            "LaGv;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    invoke-static {}, LbmL;->a()LbmM;

    move-result-object v0

    const-string v1, "_id"

    const/4 v2, 0x0

    .line 36
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    const-string v1, "document_id"

    const/4 v2, 0x1

    .line 37
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    const-string v1, "_display_name"

    const/4 v2, 0x2

    .line 38
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    const-string v1, "_size"

    const/4 v2, 0x4

    .line 39
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    const-string v1, "mime_type"

    const/4 v2, 0x3

    .line 40
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    const-string v1, "flags"

    const/4 v2, 0x6

    .line 41
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    const-string v1, "last_modified"

    const/4 v2, 0x5

    .line 42
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    const-string v1, "icon"

    const/4 v2, 0x7

    .line 43
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, LbmM;->a()LbmL;

    move-result-object v0

    sput-object v0, LaHV;->a:Ljava/util/Map;

    .line 47
    invoke-static {}, LbmL;->a()LbmM;

    move-result-object v0

    sget-object v1, LaGv;->b:LaGv;

    sget v2, Lxb;->ic_type_doc:I

    .line 48
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, LaGv;->c:LaGv;

    sget v2, Lxb;->ic_type_drawing:I

    .line 49
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, LaGv;->d:LaGv;

    sget v2, Lxb;->ic_type_file:I

    .line 50
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, LaGv;->e:LaGv;

    sget v2, Lxb;->ic_type_form:I

    .line 51
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, LaGv;->f:LaGv;

    sget v2, Lxb;->ic_type_pdf:I

    .line 52
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, LaGv;->g:LaGv;

    sget v2, Lxb;->ic_type_presentation:I

    .line 53
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, LaGv;->h:LaGv;

    sget v2, Lxb;->ic_type_site:I

    .line 54
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, LaGv;->i:LaGv;

    sget v2, Lxb;->ic_type_sheet:I

    .line 55
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, LaGv;->j:LaGv;

    sget v2, Lxb;->ic_type_fusion:I

    .line 56
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, LbmM;->a()LbmL;

    move-result-object v0

    sput-object v0, LaHV;->a:LbmL;

    .line 46
    return-void
.end method

.method public constructor <init>([Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    array-length v0, p1

    new-array v0, v0, [I

    iput-object v0, p0, LaHV;->a:[I

    .line 86
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, p1

    if-ge v1, v0, :cond_1

    .line 87
    sget-object v0, LaHV;->a:Ljava/util/Map;

    aget-object v2, p1, v1

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 88
    if-nez v0, :cond_0

    .line 89
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown column requested: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v1, p1, v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_0
    iget-object v2, p0, LaHV;->a:[I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v2, v1

    .line 86
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 93
    :cond_1
    return-void
.end method

.method public static a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LaGv;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;LaHW;)Landroid/database/Cursor;
    .locals 10

    .prologue
    .line 182
    new-instance v0, LaHV;

    invoke-direct {v0, p0}, LaHV;-><init>([Ljava/lang/String;)V

    .line 183
    new-instance v9, Landroid/database/MatrixCursor;

    const/4 v1, 0x1

    invoke-direct {v9, p0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    .line 184
    invoke-virtual/range {v0 .. v8}, LaHV;->a(Ljava/lang/String;Ljava/lang/String;LaGv;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;LaHW;)[Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 186
    return-object v9
.end method

.method public static final a()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 97
    sget-object v0, LaHV;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;LaGv;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;LaHW;)[Ljava/lang/Object;
    .locals 5

    .prologue
    const-wide/16 v0, 0x0

    .line 112
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    const/4 v2, 0x1

    .line 119
    sget-object v3, LaGv;->a:LaGv;

    invoke-virtual {p3, v3}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 120
    const-string p4, "vnd.android.document/directory"

    .line 122
    invoke-virtual {p8}, LaHW;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    const-wide/16 v0, 0x8

    .line 135
    :cond_0
    :goto_0
    iget-object v2, p0, LaHV;->a:[I

    array-length v2, v2

    new-array v3, v2, [Ljava/lang/Object;

    .line 136
    const/4 v2, 0x0

    :goto_1
    iget-object v4, p0, LaHV;->a:[I

    array-length v4, v4

    if-ge v2, v4, :cond_6

    .line 137
    iget-object v4, p0, LaHV;->a:[I

    aget v4, v4, v2

    .line 138
    packed-switch v4, :pswitch_data_0

    .line 162
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "should never happen"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :cond_1
    sget-object v3, LaGv;->d:LaGv;

    invoke-virtual {p3, v3}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    sget-object v3, LaGv;->f:LaGv;

    invoke-virtual {p3, v3}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 125
    :cond_2
    invoke-virtual {p8}, LaHW;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    const-wide/16 v0, 0x2

    .line 126
    :cond_3
    int-to-long v2, v2

    or-long/2addr v0, v2

    .line 127
    goto :goto_0

    :cond_4
    sget-object v3, LaGv;->k:LaGv;

    invoke-virtual {p3, v3}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 128
    const/4 p4, 0x0

    goto :goto_0

    .line 131
    :cond_5
    sget-object v0, LaHV;->a:LbmL;

    invoke-virtual {v0, p3}, LbmL;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 132
    int-to-long v2, v2

    move-object p7, v0

    move-wide v0, v2

    goto :goto_0

    .line 141
    :pswitch_0
    aput-object p1, v3, v2

    .line 136
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 144
    :pswitch_1
    aput-object p2, v3, v2

    goto :goto_2

    .line 147
    :pswitch_2
    aput-object p4, v3, v2

    goto :goto_2

    .line 150
    :pswitch_3
    aput-object p5, v3, v2

    goto :goto_2

    .line 153
    :pswitch_4
    aput-object p6, v3, v2

    goto :goto_2

    .line 156
    :pswitch_5
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v2

    goto :goto_2

    .line 159
    :pswitch_6
    aput-object p7, v3, v2

    goto :goto_2

    .line 165
    :cond_6
    return-object v3

    .line 138
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
