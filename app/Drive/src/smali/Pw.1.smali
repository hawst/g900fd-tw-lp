.class public LPw;
.super Ljava/lang/Object;
.source "OnlineEntryCreator.java"


# instance fields
.field private final a:LaGM;

.field private final a:Laeu;

.field private final a:LafB;

.field private final a:Lagl;


# direct methods
.method public constructor <init>(Lagl;Laeu;LaGM;LafB;)V
    .locals 0
    .param p2    # Laeu;
        .annotation runtime Lbxv;
            a = "DocFeed"
        .end annotation
    .end param

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, LPw;->a:Lagl;

    .line 62
    iput-object p2, p0, LPw;->a:Laeu;

    .line 63
    iput-object p3, p0, LPw;->a:LaGM;

    .line 64
    iput-object p4, p0, LPw;->a:LafB;

    .line 65
    return-void
.end method

.method private a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 136
    const-string v0, "https://docs.google.com/feeds/default/private/full/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 137
    if-eqz p1, :cond_0

    .line 138
    const/4 v1, 0x1

    invoke-static {p1, v1}, Lafi;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Z)Lafi;

    move-result-object v1

    invoke-virtual {v1, v0}, Lafi;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 140
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "showdeleted=true&showroot=true"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 141
    return-object v0
.end method

.method private a(LaGv;Lcom/google/android/gms/drive/database/data/ResourceSpec;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 109
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    sget-object v0, LaGv;->a:LaGv;

    invoke-virtual {p1, v0}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 117
    iget-object v0, p0, LPw;->a:LaGM;

    iget-object v1, p2, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    invoke-interface {v0, v1}, LaGM;->a(LaFO;)LaFM;

    move-result-object v0

    .line 119
    invoke-static {p2}, LaeZ;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaeZ;

    move-result-object v1

    .line 120
    iget-object v2, p0, LPw;->a:LafB;

    invoke-virtual {v2, v0, v1}, LafB;->a(LaFM;LaeZ;)Lafe;

    move-result-object v0

    .line 121
    invoke-virtual {v0}, Lafe;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGW;

    .line 122
    if-eqz v0, :cond_0

    .line 123
    invoke-virtual {v0, v3, v3}, LaGW;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 124
    invoke-virtual {v0}, LaGW;->e()V

    goto :goto_0

    .line 128
    :cond_1
    return-void
.end method


# virtual methods
.method public a(LaFO;Ljava/lang/String;LaGv;Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 5

    .prologue
    .line 75
    :try_start_0
    iget-object v0, p0, LPw;->a:LaGM;

    invoke-interface {v0, p1}, LaGM;->a(LaFO;)LaFM;

    move-result-object v1

    .line 76
    new-instance v0, LaeF;

    invoke-direct {v0}, LaeF;-><init>()V

    .line 77
    invoke-virtual {v0, p2}, LaeF;->l(Ljava/lang/String;)V

    .line 78
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "http://schemas.google.com/docs/2007#"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, LaGv;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LaeF;->p(Ljava/lang/String;)V

    .line 79
    const-string v2, "http://schemas.google.com/g/2005#kind"

    invoke-virtual {v0, v2}, LaeF;->q(Ljava/lang/String;)V

    .line 81
    invoke-direct {p0, p4}, LPw;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Landroid/net/Uri;

    move-result-object v2

    .line 83
    iget-object v3, p0, LPw;->a:Laeu;

    .line 84
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2, p1, v0}, Laeu;->a(Ljava/lang/String;LaFO;Lbxb;)Lbxb;

    move-result-object v0

    .line 85
    instance-of v2, v0, LaeF;

    if-eqz v2, :cond_0

    .line 86
    check-cast v0, LaeF;

    .line 87
    invoke-virtual {v0}, LaeF;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LaGv;->a(Ljava/lang/String;)LaGv;

    move-result-object v2

    .line 88
    iget-object v3, p0, LPw;->a:Lagl;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v3, v1, v0, v4}, Lagl;->a(LaFM;LaJT;Ljava/lang/Boolean;)V

    .line 90
    invoke-virtual {v0}, LaeF;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a(LaFO;Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    .line 91
    iget-object v1, p0, LPw;->a:LaGM;

    invoke-interface {v1, v0}, LaGM;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    .line 92
    invoke-direct {p0, v2, v0}, LPw;->a(LaGv;Lcom/google/android/gms/drive/database/data/ResourceSpec;)V

    .line 93
    return-object v1

    .line 95
    :cond_0
    new-instance v1, Lbxk;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected Entry class: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lbxk;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch LbwO; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lbxk; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_3

    .line 97
    :catch_0
    move-exception v0

    .line 98
    new-instance v1, LPx;

    invoke-direct {v1, v0}, LPx;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 99
    :catch_1
    move-exception v0

    .line 100
    new-instance v1, LPx;

    invoke-direct {v1, v0}, LPx;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 101
    :catch_2
    move-exception v0

    .line 102
    new-instance v1, LPx;

    invoke-direct {v1, v0}, LPx;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 103
    :catch_3
    move-exception v0

    .line 104
    new-instance v1, LPx;

    invoke-direct {v1, v0}, LPx;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
