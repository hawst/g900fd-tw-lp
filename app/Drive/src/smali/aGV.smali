.class public LaGV;
.super Ljava/lang/Object;
.source "ObsoleteDataCleanerImpl.java"

# interfaces
.implements LaGU;


# instance fields
.field private final a:LSF;

.field private final a:LaGM;

.field private final a:LaIm;

.field private final a:LadL;

.field private final a:LakW;

.field private final a:LpW;


# direct methods
.method public constructor <init>(LaGM;LSF;LpW;LadL;LakW;LaIm;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, LaGV;->a:LaGM;

    .line 50
    iput-object p2, p0, LaGV;->a:LSF;

    .line 51
    iput-object p3, p0, LaGV;->a:LpW;

    .line 52
    iput-object p4, p0, LaGV;->a:LadL;

    .line 53
    iput-object p5, p0, LaGV;->a:LakW;

    .line 54
    iput-object p6, p0, LaGV;->a:LaIm;

    .line 55
    return-void
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v2

    .line 59
    new-instance v3, Ljava/util/HashSet;

    iget-object v0, p0, LaGV;->a:LaGM;

    invoke-interface {v0}, LaGM;->a()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 60
    iget-object v0, p0, LaGV;->a:LSF;

    invoke-interface {v0}, LSF;->a()[Landroid/accounts/Account;

    move-result-object v4

    array-length v5, v4

    move v0, v1

    :goto_0
    if-ge v0, v5, :cond_0

    aget-object v6, v4, v0

    .line 61
    iget-object v6, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v6}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v6

    .line 62
    invoke-interface {v3, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 63
    invoke-interface {v2, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 60
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 65
    :cond_0
    const-string v0, "ObsoleteDataCleanerImpl"

    const-string v4, "Purging %d accounts: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v1, 0x1

    aput-object v3, v5, v1

    invoke-static {v0, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 66
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 67
    invoke-static {}, LbmY;->a()Lbna;

    move-result-object v4

    .line 69
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFO;

    .line 70
    iget-object v6, p0, LaGV;->a:LaGM;

    invoke-interface {v6, v0}, LaGM;->a(LaFO;)LaFM;

    move-result-object v6

    .line 71
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    invoke-virtual {v6}, LaFM;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v6}, Lbna;->a(Ljava/lang/Object;)Lbna;

    .line 74
    :try_start_0
    iget-object v6, p0, LaGV;->a:LpW;

    invoke-interface {v6, v0}, LpW;->a(LaFO;)V
    :try_end_0
    .catch LpX; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 75
    :catch_0
    move-exception v0

    .line 76
    const-string v6, "ObsoleteDataCleanerImpl"

    const-string v7, "Error deleting Android preferences of deleted account."

    invoke-static {v6, v0, v7}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_1

    .line 80
    :cond_1
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 81
    iget-object v0, p0, LaGV;->a:LaIm;

    invoke-virtual {v4}, Lbna;->a()LbmY;

    move-result-object v3

    invoke-interface {v0, v3}, LaIm;->a(Ljava/util/Set;)V

    .line 82
    iget-object v0, p0, LaGV;->a:LakW;

    invoke-virtual {v0, v2}, LakW;->a(Ljava/util/Set;)V

    .line 83
    iget-object v0, p0, LaGV;->a:LadL;

    invoke-interface {v0}, LadL;->a()V

    .line 86
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFM;

    .line 87
    iget-object v2, p0, LaGV;->a:LaGM;

    invoke-interface {v2, v0}, LaGM;->a(LaFM;)V

    goto :goto_2

    .line 89
    :cond_3
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, LaGV;->a:LaGM;

    instance-of v0, v0, Laaj;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, LaGV;->a:LaGM;

    check-cast v0, Laaj;

    invoke-interface {v0}, Laaj;->a()I

    .line 97
    :cond_0
    invoke-direct {p0}, LaGV;->b()V

    .line 98
    iget-object v0, p0, LaGV;->a:LakW;

    invoke-virtual {v0}, LakW;->a()V

    .line 99
    return-void
.end method
