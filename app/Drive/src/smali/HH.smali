.class public final LHH;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LHG;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LHz;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LHM;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 40
    iput-object p1, p0, LHH;->a:LbrA;

    .line 41
    const-class v0, LHG;

    invoke-static {v0, v1}, LHH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LHH;->a:Lbsk;

    .line 44
    const-class v0, LHz;

    invoke-static {v0, v1}, LHH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LHH;->b:Lbsk;

    .line 47
    const-class v0, LHM;

    invoke-static {v0, v1}, LHH;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LHH;->c:Lbsk;

    .line 50
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 14

    .prologue
    .line 74
    packed-switch p1, :pswitch_data_0

    .line 196
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :pswitch_0
    new-instance v1, LHG;

    iget-object v0, p0, LHH;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->I:Lbsk;

    .line 79
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LHH;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LalC;

    iget-object v2, v2, LalC;->I:Lbsk;

    .line 77
    invoke-static {v0, v2}, LHH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapd;

    invoke-direct {v1, v0}, LHG;-><init>(Lapd;)V

    move-object v0, v1

    .line 194
    :goto_0
    return-object v0

    .line 86
    :pswitch_1
    new-instance v0, LHz;

    iget-object v1, p0, LHH;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->t:Lbsk;

    .line 89
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LHH;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->t:Lbsk;

    .line 87
    invoke-static {v1, v2}, LHH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laja;

    iget-object v2, p0, LHH;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LalC;

    iget-object v2, v2, LalC;->I:Lbsk;

    .line 95
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LHH;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->I:Lbsk;

    .line 93
    invoke-static {v2, v3}, LHH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lapd;

    iget-object v3, p0, LHH;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LJT;

    iget-object v3, v3, LJT;->c:Lbsk;

    .line 101
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LHH;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LJT;

    iget-object v4, v4, LJT;->c:Lbsk;

    .line 99
    invoke-static {v3, v4}, LHH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LJR;

    iget-object v4, p0, LHH;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LIv;

    iget-object v4, v4, LIv;->d:Lbsk;

    .line 107
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LHH;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LIv;

    iget-object v5, v5, LIv;->d:Lbsk;

    .line 105
    invoke-static {v4, v5}, LHH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LII;

    iget-object v5, p0, LHH;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LCw;

    iget-object v5, v5, LCw;->aa:Lbsk;

    .line 113
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LHH;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LCw;

    iget-object v6, v6, LCw;->aa:Lbsk;

    .line 111
    invoke-static {v5, v6}, LHH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LsC;

    invoke-direct/range {v0 .. v5}, LHz;-><init>(Laja;Lapd;LJR;LII;LsC;)V

    goto :goto_0

    .line 120
    :pswitch_2
    new-instance v0, LHM;

    iget-object v1, p0, LHH;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->t:Lbsk;

    .line 123
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LHH;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->t:Lbsk;

    .line 121
    invoke-static {v1, v2}, LHH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laja;

    iget-object v2, p0, LHH;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LCw;

    iget-object v2, v2, LCw;->O:Lbsk;

    .line 129
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LHH;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LCw;

    iget-object v3, v3, LCw;->O:Lbsk;

    .line 127
    invoke-static {v2, v3}, LHH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LBf;

    iget-object v3, p0, LHH;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LCw;

    iget-object v3, v3, LCw;->l:Lbsk;

    .line 135
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LHH;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LCw;

    iget-object v4, v4, LCw;->l:Lbsk;

    .line 133
    invoke-static {v3, v4}, LHH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LDU;

    iget-object v4, p0, LHH;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LCw;

    iget-object v4, v4, LCw;->y:Lbsk;

    .line 141
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LHH;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LCw;

    iget-object v5, v5, LCw;->y:Lbsk;

    .line 139
    invoke-static {v4, v5}, LHH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LCU;

    iget-object v5, p0, LHH;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LCw;

    iget-object v5, v5, LCw;->w:Lbsk;

    .line 147
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LHH;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LCw;

    iget-object v6, v6, LCw;->w:Lbsk;

    .line 145
    invoke-static {v5, v6}, LHH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LDk;

    iget-object v6, p0, LHH;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LCw;

    iget-object v6, v6, LCw;->af:Lbsk;

    .line 153
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LHH;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LCw;

    iget-object v7, v7, LCw;->af:Lbsk;

    .line 151
    invoke-static {v6, v7}, LHH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Laqw;

    iget-object v7, p0, LHH;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LpG;

    iget-object v7, v7, LpG;->m:Lbsk;

    .line 159
    invoke-virtual {v7}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v7

    iget-object v8, p0, LHH;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LpG;

    iget-object v8, v8, LpG;->m:Lbsk;

    .line 157
    invoke-static {v7, v8}, LHH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LtK;

    iget-object v8, p0, LHH;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LalC;

    iget-object v8, v8, LalC;->S:Lbsk;

    .line 165
    invoke-virtual {v8}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v8

    iget-object v9, p0, LHH;->a:LbrA;

    iget-object v9, v9, LbrA;->a:LalC;

    iget-object v9, v9, LalC;->S:Lbsk;

    .line 163
    invoke-static {v8, v9}, LHH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LaKM;

    iget-object v9, p0, LHH;->a:LbrA;

    iget-object v9, v9, LbrA;->a:LCw;

    iget-object v9, v9, LCw;->z:Lbsk;

    .line 171
    invoke-virtual {v9}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v9

    iget-object v10, p0, LHH;->a:LbrA;

    iget-object v10, v10, LbrA;->a:LCw;

    iget-object v10, v10, LCw;->z:Lbsk;

    .line 169
    invoke-static {v9, v10}, LHH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LDn;

    iget-object v10, p0, LHH;->a:LbrA;

    iget-object v10, v10, LbrA;->a:LalC;

    iget-object v10, v10, LalC;->I:Lbsk;

    .line 177
    invoke-virtual {v10}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v10

    iget-object v11, p0, LHH;->a:LbrA;

    iget-object v11, v11, LbrA;->a:LalC;

    iget-object v11, v11, LalC;->I:Lbsk;

    .line 175
    invoke-static {v10, v11}, LHH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lapd;

    iget-object v11, p0, LHH;->a:LbrA;

    iget-object v11, v11, LbrA;->a:LaoV;

    iget-object v11, v11, LaoV;->d:Lbsk;

    .line 183
    invoke-virtual {v11}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v11

    iget-object v12, p0, LHH;->a:LbrA;

    iget-object v12, v12, LbrA;->a:LaoV;

    iget-object v12, v12, LaoV;->d:Lbsk;

    .line 181
    invoke-static {v11, v12}, LHH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, LaoY;

    iget-object v12, p0, LHH;->a:LbrA;

    iget-object v12, v12, LbrA;->a:LCw;

    iget-object v12, v12, LCw;->aa:Lbsk;

    .line 189
    invoke-virtual {v12}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v12

    iget-object v13, p0, LHH;->a:LbrA;

    iget-object v13, v13, LbrA;->a:LCw;

    iget-object v13, v13, LCw;->aa:Lbsk;

    .line 187
    invoke-static {v12, v13}, LHH;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, LsC;

    invoke-direct/range {v0 .. v12}, LHM;-><init>(Laja;LBf;LDU;LCU;LDk;Laqw;LtK;LaKM;LDn;Lapd;LaoY;LsC;)V

    goto/16 :goto_0

    .line 74
    :pswitch_data_0
    .packed-switch 0xe0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 211
    .line 213
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 57
    const-class v0, LHG;

    iget-object v1, p0, LHH;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LHH;->a(Ljava/lang/Class;Lbsk;)V

    .line 58
    const-class v0, LHz;

    iget-object v1, p0, LHH;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LHH;->a(Ljava/lang/Class;Lbsk;)V

    .line 59
    const-class v0, LHM;

    iget-object v1, p0, LHH;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LHH;->a(Ljava/lang/Class;Lbsk;)V

    .line 60
    iget-object v0, p0, LHH;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xe1

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 62
    iget-object v0, p0, LHH;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xe0

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 64
    iget-object v0, p0, LHH;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xe2

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 66
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 203
    .line 205
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 70
    return-void
.end method
