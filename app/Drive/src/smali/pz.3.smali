.class public Lpz;
.super Ljava/lang/Object;
.source "ApplicationModule.java"

# interfaces
.implements LbuC;


# static fields
.field private static final a:Lbuv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbuv",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LqA;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 74
    new-instance v0, LpA;

    invoke-direct {v0}, LpA;-><init>()V

    const-class v1, Laix;

    .line 75
    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    sput-object v0, Lpz;->a:Lbuv;

    .line 74
    return-void
.end method

.method public constructor <init>(LqA;)V
    .locals 1

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqA;

    iput-object v0, p0, Lpz;->a:LqA;

    .line 136
    return-void
.end method


# virtual methods
.method public a(Lcom/google/inject/Binder;)V
    .locals 2

    .prologue
    .line 262
    const-class v0, LtN;

    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Ljava/lang/Class;)LbvX;

    .line 263
    const-class v0, LtB;

    const-class v1, LQP;

    .line 264
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 263
    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Lbuv;)LbvX;

    .line 265
    const-class v0, LsF;

    const-class v1, LQM;

    .line 266
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 265
    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Lbuv;)LbvX;

    .line 267
    const-class v0, LBt;

    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Ljava/lang/Class;)LbvX;

    .line 268
    const-class v0, Landroid/app/Activity;

    .line 269
    invoke-static {v0}, Lbsp;->a(Ljava/lang/Class;)LbuP;

    move-result-object v0

    const-string v1, "StarDriveActivityOverride"

    .line 270
    invoke-static {v1}, Lbwo;->a(Ljava/lang/String;)Lbwm;

    move-result-object v1

    .line 269
    invoke-static {v0, v1}, Lbuv;->a(LbuP;Ljava/lang/annotation/Annotation;)Lbuv;

    move-result-object v0

    .line 268
    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Lbuv;)LbvX;

    .line 272
    new-instance v0, Lvz;

    invoke-direct {v0}, Lvz;-><init>()V

    invoke-interface {p1, v0}, Lcom/google/inject/Binder;->a(LbuC;)V

    .line 274
    const-class v0, LBt;

    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Ljava/lang/Class;)LbvX;

    .line 276
    const-class v0, LVP;

    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Ljava/lang/Class;)LbvX;

    .line 278
    const-class v0, LSu;

    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Ljava/lang/Class;)LbvX;

    .line 279
    return-void
.end method

.method provideAccountDeletionChecker(LqX;)LqW;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 147
    return-object p1
.end method

.method provideAccountId(LqY;)LaFO;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 204
    invoke-virtual {p1}, LqY;->a()LaFO;

    move-result-object v0

    return-object v0
.end method

.method provideAppVersionCode()I
    .locals 1
    .annotation runtime LQK;
    .end annotation

    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 217
    invoke-static {}, Laml;->a()I

    move-result v0

    return v0
.end method

.method provideApplicationStartupTracker()LqA;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 198
    iget-object v0, p0, Lpz;->a:LqA;

    return-object v0
.end method

.method provideBaseActivity(Landroid/content/Context;)Lrm;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 241
    invoke-static {p1}, Lajt;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lrm;

    return-object v0
.end method

.method provideClientMode()Lrx;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 210
    invoke-static {}, Laml;->a()Lrx;

    move-result-object v0

    return-object v0
.end method

.method provideDefaultHomeActivity(LpB;)Ljava/lang/Class;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxv;
        a = "DocListActivity"
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LpB;",
            ")",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 257
    invoke-virtual {p1}, LpB;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    return-object v0
.end method

.method provideDelayWaitingRateLimiter(LZP;)LZP;
    .locals 0
    .param p1    # LZP;
        .annotation runtime Lbxz;
        .end annotation
    .end param
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxv;
        a = "wapiFeedProcessor"
    .end annotation

    .prologue
    .line 157
    return-object p1
.end method

.method provideDocsCentricTaskCompat(LsG;)LsF;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 235
    invoke-virtual {p1}, LsG;->a()LsF;

    move-result-object v0

    return-object v0
.end method

.method provideDriveEnabledChecker(Lts;)Ltt;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 192
    return-object p1
.end method

.method provideEditorsOfflineSwitch(LtC;)LtB;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .prologue
    .line 224
    invoke-virtual {p1}, LtC;->a()LtB;

    move-result-object v0

    return-object v0
.end method

.method provideFeatureChecker(LtM;)LtK;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 229
    return-object p1
.end method

.method provideIntentStarter(Lud;)Luc;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 180
    return-object p1
.end method

.method provideInternalReleaseDisplayer()LFf;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 186
    sget-object v0, LFf;->b:LFf;

    return-object v0
.end method

.method providePrinter(LWM;)LWL;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 174
    invoke-virtual {p1}, LWM;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LWL;

    return-object v0
.end method

.method provideRateLimitedExecutorFactory()Lamx;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 141
    sget-object v0, Lamy;->a:Lamx;

    return-object v0
.end method

.method provideStartingActivity(LpC;)Ljava/lang/Class;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxv;
        a = "StartingActivityOnLaunch"
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LpC;",
            ")",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 249
    invoke-virtual {p1}, LpC;->a()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method provideWaitingRateLimiter(LZP;)LZS;
    .locals 0
    .param p1    # LZP;
        .annotation runtime Lbxv;
            a = "wapiFeedProcessor"
        .end annotation
    .end param
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxv;
        a = "wapiFeedProcessor"
    .end annotation

    .prologue
    .line 168
    return-object p1
.end method
