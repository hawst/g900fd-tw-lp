.class abstract LbtA;
.super Ljava/lang/Object;
.source "LinkedTreeMap.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field a:I

.field a:LbtB;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbtB",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final synthetic a:Lbtu;

.field b:LbtB;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbtB",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lbtu;)V
    .locals 1

    .prologue
    .line 526
    iput-object p1, p0, LbtA;->a:Lbtu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 527
    iget-object v0, p0, LbtA;->a:Lbtu;

    iget-object v0, v0, Lbtu;->b:LbtB;

    iget-object v0, v0, LbtB;->d:LbtB;

    iput-object v0, p0, LbtA;->a:LbtB;

    .line 528
    const/4 v0, 0x0

    iput-object v0, p0, LbtA;->b:LbtB;

    .line 529
    iget-object v0, p0, LbtA;->a:Lbtu;

    iget v0, v0, Lbtu;->b:I

    iput v0, p0, LbtA;->a:I

    return-void
.end method

.method synthetic constructor <init>(Lbtu;Lbtv;)V
    .locals 0

    .prologue
    .line 526
    invoke-direct {p0, p1}, LbtA;-><init>(Lbtu;)V

    return-void
.end method


# virtual methods
.method final a()LbtB;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbtB",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 536
    iget-object v0, p0, LbtA;->a:LbtB;

    .line 537
    iget-object v1, p0, LbtA;->a:Lbtu;

    iget-object v1, v1, Lbtu;->b:LbtB;

    if-ne v0, v1, :cond_0

    .line 538
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 540
    :cond_0
    iget-object v1, p0, LbtA;->a:Lbtu;

    iget v1, v1, Lbtu;->b:I

    iget v2, p0, LbtA;->a:I

    if-eq v1, v2, :cond_1

    .line 541
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 543
    :cond_1
    iget-object v1, v0, LbtB;->d:LbtB;

    iput-object v1, p0, LbtA;->a:LbtB;

    .line 544
    iput-object v0, p0, LbtA;->b:LbtB;

    return-object v0
.end method

.method public final hasNext()Z
    .locals 2

    .prologue
    .line 532
    iget-object v0, p0, LbtA;->a:LbtB;

    iget-object v1, p0, LbtA;->a:Lbtu;

    iget-object v1, v1, Lbtu;->b:LbtB;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final remove()V
    .locals 3

    .prologue
    .line 548
    iget-object v0, p0, LbtA;->b:LbtB;

    if-nez v0, :cond_0

    .line 549
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 551
    :cond_0
    iget-object v0, p0, LbtA;->a:Lbtu;

    iget-object v1, p0, LbtA;->b:LbtB;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lbtu;->a(LbtB;Z)V

    .line 552
    const/4 v0, 0x0

    iput-object v0, p0, LbtA;->b:LbtB;

    .line 553
    iget-object v0, p0, LbtA;->a:Lbtu;

    iget v0, v0, Lbtu;->b:I

    iput v0, p0, LbtA;->a:I

    .line 554
    return-void
.end method
