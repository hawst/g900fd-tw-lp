.class public Laew;
.super Ljava/lang/Object;
.source "GDataClientImpl.java"

# interfaces
.implements LbwS;


# instance fields
.field private final a:LTA;

.field private final a:Landroid/content/ContentResolver;

.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;LQr;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    new-instance v0, LTA;

    const/4 v1, 0x1

    invoke-direct {v0, p1, p2, p3, v1}, LTA;-><init>(Landroid/content/Context;LQr;Ljava/lang/String;Z)V

    iput-object v0, p0, Laew;->a:LTA;

    .line 178
    iget-object v0, p0, Laew;->a:LTA;

    const-string v1, "GDataClient"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, LTA;->a(Ljava/lang/String;I)V

    .line 179
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Laew;->a:Landroid/content/ContentResolver;

    .line 180
    iput-object p4, p0, Laew;->a:Ljava/lang/String;

    .line 181
    return-void
.end method

.method private a(Laey;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;
    .locals 18

    .prologue
    .line 222
    const/4 v7, 0x0

    .line 223
    const/16 v6, 0x1f4

    .line 224
    const/16 v5, 0xa

    .line 228
    :try_start_0
    new-instance v4, Ljava/net/URI;

    move-object/from16 v0, p2

    invoke-direct {v4, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 236
    const-wide/16 v2, 0x0

    move-object/from16 v16, v4

    move v4, v6

    move-object/from16 v6, v16

    move/from16 v17, v5

    move-object v5, v7

    move/from16 v7, v17

    .line 243
    :goto_0
    if-lez v7, :cond_c

    .line 245
    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Laey;->a(Ljava/net/URI;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v5

    .line 247
    invoke-static {v5}, LTC;->a(Lorg/apache/http/HttpRequest;)V

    .line 252
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 253
    const-string v4, "Authorization"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "GoogleLogin auth="

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p3

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v4, v6}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Laew;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 260
    const-string v4, "GData-Version"

    move-object/from16 v0, p0

    iget-object v6, v0, Laew;->a:Ljava/lang/String;

    invoke-interface {v5, v4, v6}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    :cond_1
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 265
    invoke-interface {v5}, Lorg/apache/http/client/methods/HttpUriRequest;->getMethod()Ljava/lang/String;

    move-result-object v4

    .line 266
    const-string v6, "X-HTTP-Method-Override"

    invoke-interface {v5, v6}, Lorg/apache/http/client/methods/HttpUriRequest;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v6

    .line 267
    if-eqz v6, :cond_2

    .line 268
    invoke-interface {v6}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 270
    :cond_2
    const-string v6, "GET"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 273
    const-string v4, "If-None-Match"

    move-object/from16 v0, p4

    invoke-interface {v5, v4, v0}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    :cond_3
    :goto_1
    const-string v4, "GDataClient"

    const/4 v6, 0x2

    invoke-static {v4, v6}, LalV;->a(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 285
    invoke-interface {v5}, Lorg/apache/http/client/methods/HttpUriRequest;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v6

    array-length v8, v6

    const/4 v4, 0x0

    :goto_2
    if-ge v4, v8, :cond_6

    aget-object v9, v6, v4

    .line 286
    const-string v10, "GDataClient"

    const-string v11, "%s: %s"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-interface {v9}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    invoke-interface {v9}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v12, v13

    invoke-static {v10, v11, v12}, LalV;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 285
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 229
    :catch_0
    move-exception v2

    .line 230
    const-string v3, "GDataClient"

    const-string v4, "Unable to parse %s as URI."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    invoke-static {v3, v2, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 231
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to parse "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " as URI: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 274
    :cond_4
    const-string v6, "DELETE"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    const-string v6, "PUT"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 278
    :cond_5
    const-string v4, "W/"

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 279
    const-string v4, "If-Match"

    move-object/from16 v0, p4

    invoke-interface {v5, v4, v0}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 290
    :cond_6
    const-string v4, "GDataClient"

    const/4 v6, 0x3

    invoke-static {v4, v6}, LalV;->a(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 291
    const-string v4, "GDataClient"

    const-string v6, "Executing %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-interface {v5}, Lorg/apache/http/client/methods/HttpUriRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v4, v6, v8}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 297
    :cond_7
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Laew;->a:LTA;

    invoke-virtual {v4, v5}, LTA;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v5

    .line 303
    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v6

    .line 304
    if-nez v6, :cond_8

    .line 305
    const-string v2, "GDataClient"

    const-string v3, "StatusLine is null."

    invoke-static {v2, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "StatusLine is null -- should not happen."

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 298
    :catch_1
    move-exception v2

    .line 299
    const-string v3, "GDataClient"

    const-string v4, "Unable to execute HTTP request."

    invoke-static {v3, v2, v4}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 300
    throw v2

    .line 309
    :cond_8
    const-string v4, "GDataClient"

    const/4 v8, 0x3

    invoke-static {v4, v8}, LalV;->a(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 310
    const-string v4, "GDataClient"

    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v8

    array-length v9, v8

    const/4 v4, 0x0

    :goto_3
    if-ge v4, v9, :cond_9

    aget-object v10, v8, v4

    .line 312
    const-string v11, "GDataClient"

    const-string v12, "%s: %s"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-interface {v10}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-interface {v10}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v13, v14

    invoke-static {v11, v12, v13}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 311
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 315
    :cond_9
    invoke-interface {v6}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v4

    .line 317
    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v6

    .line 319
    const/16 v8, 0xc8

    if-lt v4, v8, :cond_b

    const/16 v8, 0x12c

    if-ge v4, v8, :cond_b

    if-eqz v6, :cond_b

    .line 320
    invoke-static {v6}, LTC;->a(Lorg/apache/http/HttpEntity;)Ljava/io/InputStream;

    move-result-object v2

    .line 321
    const-string v3, "GDataClient"

    const/4 v4, 0x3

    invoke-static {v3, v4}, LalV;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 322
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Laew;->a(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v2

    .line 324
    :cond_a
    return-object v2

    .line 331
    :cond_b
    const/16 v8, 0x12e

    if-ne v4, v8, :cond_11

    .line 333
    invoke-interface {v6}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 334
    const-string v6, "Location"

    invoke-interface {v5, v6}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v8

    .line 335
    if-nez v8, :cond_f

    .line 336
    const-string v6, "GDataClient"

    const/4 v7, 0x3

    invoke-static {v6, v7}, LalV;->a(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 337
    const-string v6, "GDataClient"

    const-string v7, "Redirect requested but no Location specified."

    invoke-static {v6, v7}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    :cond_c
    :goto_4
    const-string v6, "GDataClient"

    const/4 v7, 0x2

    invoke-static {v6, v7}, LalV;->a(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 384
    const-string v6, "GDataClient"

    const-string v7, "Received %s status code."

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, LalV;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 386
    :cond_d
    const/4 v6, 0x0

    .line 387
    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v7

    .line 389
    if-eqz v5, :cond_14

    if-eqz v7, :cond_14

    .line 390
    :try_start_2
    invoke-static {v7}, LTC;->a(Lorg/apache/http/HttpEntity;)Ljava/io/InputStream;

    move-result-object v5

    .line 391
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 392
    const/16 v8, 0x2000

    new-array v8, v8, [B

    .line 394
    :goto_5
    invoke-virtual {v5, v8}, Ljava/io/InputStream;->read([B)I

    move-result v9

    const/4 v10, -0x1

    if-eq v9, v10, :cond_12

    .line 395
    const/4 v10, 0x0

    invoke-virtual {v6, v8, v10, v9}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_5

    .line 404
    :catchall_0
    move-exception v2

    if-eqz v7, :cond_e

    .line 405
    invoke-interface {v7}, Lorg/apache/http/HttpEntity;->consumeContent()V

    :cond_e
    throw v2

    .line 341
    :cond_f
    const-string v6, "GDataClient"

    const/4 v9, 0x3

    invoke-static {v6, v9}, LalV;->a(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 342
    const-string v6, "GDataClient"

    const-string v9, "Following redirect to %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-interface {v8}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v6, v9, v10}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 345
    :cond_10
    :try_start_3
    new-instance v6, Ljava/net/URI;

    invoke-interface {v8}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v9}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/net/URISyntaxException; {:try_start_3 .. :try_end_3} :catch_2

    .line 353
    add-int/lit8 v7, v7, -0x1

    .line 354
    goto/16 :goto_0

    .line 346
    :catch_2
    move-exception v6

    .line 347
    const-string v7, "GDataClient"

    const/4 v9, 0x3

    invoke-static {v7, v9}, LalV;->a(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 348
    const-string v2, "GDataClient"

    const-string v3, "Unable to parse %s as URI."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {v8}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    invoke-static {v2, v6, v3, v4}, LalV;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 349
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to parse "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v8}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " as URI."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 354
    :cond_11
    const/16 v6, 0x1f7

    if-ne v4, v6, :cond_c

    .line 355
    const-string v6, "Retry-After"

    invoke-interface {v5, v6}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v6

    .line 356
    if-eqz v6, :cond_c

    .line 361
    invoke-interface {v6}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v7

    .line 365
    :try_start_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_3

    move-result-wide v2

    add-long/2addr v2, v8

    goto/16 :goto_4

    .line 366
    :catch_3
    move-exception v6

    .line 369
    :try_start_5
    new-instance v6, Landroid/text/format/Time;

    invoke-direct {v6}, Landroid/text/format/Time;-><init>()V

    .line 370
    invoke-virtual {v6, v7}, Landroid/text/format/Time;->parse3339(Ljava/lang/String;)Z

    .line 371
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long v2, v8, v10
    :try_end_5
    .catch Landroid/util/TimeFormatException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_4

    .line 372
    :catch_4
    move-exception v6

    .line 373
    const-string v8, "GDataClient"

    const-string v9, "Unable to parse %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v7, v10, v11

    invoke-static {v8, v6, v9, v10}, LalV;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_4

    .line 398
    :cond_12
    :try_start_6
    new-instance v5, Ljava/lang/String;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>([B)V

    .line 399
    const-string v6, "GDataClient"

    const/4 v8, 0x2

    invoke-static {v6, v8}, LalV;->a(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 400
    const-string v6, "GDataClient"

    invoke-static {v6, v5}, LalV;->a(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_13
    move-object v6, v5

    .line 404
    :cond_14
    if-eqz v7, :cond_15

    .line 405
    invoke-interface {v7}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 408
    :cond_15
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Received "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " status code"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 409
    if-eqz v6, :cond_16

    .line 410
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ": "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 412
    :cond_16
    new-instance v6, LbwW;

    const/4 v7, 0x0

    invoke-direct {v6, v5, v4, v7}, LbwW;-><init>(Ljava/lang/String;ILjava/io/InputStream;)V

    .line 414
    const/16 v5, 0x1f7

    if-ne v4, v5, :cond_17

    .line 415
    invoke-virtual {v6, v2, v3}, LbwW;->a(J)V

    .line 417
    :cond_17
    throw v6
.end method

.method private a(Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x4000

    .line 446
    if-nez p1, :cond_0

    .line 473
    :goto_0
    return-object p1

    .line 458
    :cond_0
    new-instance v0, Ljava/io/BufferedInputStream;

    invoke-direct {v0, p1, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 459
    invoke-virtual {v0, v1}, Ljava/io/BufferedInputStream;->mark(I)V

    .line 462
    new-array v4, v1, [B

    move v3, v1

    move v1, v2

    .line 463
    :goto_1
    if-lez v3, :cond_1

    .line 464
    invoke-virtual {v0, v4, v1, v3}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v5

    .line 465
    if-gtz v5, :cond_2

    .line 471
    :cond_1
    const-string v3, "GDataClient"

    new-instance v5, Ljava/lang/String;

    const-string v6, "UTF-8"

    invoke-direct {v5, v4, v2, v1, v6}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-static {v3, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->reset()V

    move-object p1, v0

    .line 473
    goto :goto_0

    .line 468
    :cond_2
    sub-int/2addr v3, v5

    .line 469
    add-int/2addr v1, v5

    .line 470
    goto :goto_1
.end method

.method private a(Lbxs;I)Lorg/apache/http/HttpEntity;
    .locals 7

    .prologue
    .line 566
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 568
    :try_start_0
    invoke-interface {p1, v0, p2}, Lbxs;->a(Ljava/io/OutputStream;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lbxk; {:try_start_0 .. :try_end_0} :catch_1

    .line 577
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 579
    if-eqz v0, :cond_0

    const-string v1, "GDataClient"

    const/4 v2, 0x3

    invoke-static {v1, v2}, LalV;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 581
    :try_start_1
    const-string v1, "GDataClient"

    const-string v2, "Serialized entry: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/String;

    const-string v6, "UTF-8"

    invoke-direct {v5, v0, v6}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_2

    .line 589
    :cond_0
    iget-object v1, p0, Laew;->a:Landroid/content/ContentResolver;

    invoke-static {v0, v1}, LTC;->a([BLandroid/content/ContentResolver;)Lorg/apache/http/entity/AbstractHttpEntity;

    move-result-object v0

    .line 590
    invoke-interface {p1}, Lbxs;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/http/entity/AbstractHttpEntity;->setContentType(Ljava/lang/String;)V

    .line 591
    return-object v0

    .line 569
    :catch_0
    move-exception v0

    .line 570
    const-string v1, "GDataClient"

    const-string v2, "Unable to serialize entry."

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 571
    throw v0

    .line 572
    :catch_1
    move-exception v0

    .line 573
    const-string v1, "GDataClient"

    const-string v2, "Unable to serialize entry."

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 574
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to serialize entry: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lbxk;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 582
    :catch_2
    move-exception v0

    .line 584
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "UTF-8 should be supported!"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lbxs;)Ljava/io/InputStream;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 499
    const/4 v0, 0x1

    invoke-direct {p0, p4, v0}, Laew;->a(Lbxs;I)Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 500
    new-instance v1, Laez;

    invoke-direct {v1, v4, v0}, Laez;-><init>(Ljava/lang/String;Lorg/apache/http/HttpEntity;)V

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    .line 501
    invoke-direct/range {v0 .. v5}, Laew;->a(Laey;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 503
    if-eqz v0, :cond_0

    .line 504
    return-object v0

    .line 506
    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unable to create entry."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;
    .locals 6

    .prologue
    .line 429
    new-instance v1, Laex;

    invoke-direct {v1}, Laex;-><init>()V

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 430
    invoke-direct/range {v0 .. v5}, Laew;->a(Laey;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 431
    if-eqz v0, :cond_0

    .line 432
    return-object v0

    .line 434
    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unable to access feed."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lbxs;)Ljava/io/InputStream;
    .locals 6

    .prologue
    .line 517
    const/4 v0, 0x2

    invoke-direct {p0, p5, v0}, Laew;->a(Lbxs;I)Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 518
    invoke-interface {p5}, Lbxs;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PATCH"

    .line 519
    :goto_0
    new-instance v1, Laez;

    invoke-direct {v1, v0, v2}, Laez;-><init>(Ljava/lang/String;Lorg/apache/http/HttpEntity;)V

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 520
    invoke-direct/range {v0 .. v5}, Laew;->a(Laey;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 522
    if-eqz v0, :cond_1

    .line 523
    return-object v0

    .line 518
    :cond_0
    const-string v0, "PUT"

    goto :goto_0

    .line 525
    :cond_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unable to update entry."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 536
    invoke-static {p1}, Lbxd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 537
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "you must specify an non-empty edit url"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 539
    :cond_0
    new-instance v1, Laez;

    const-string v0, "DELETE"

    invoke-direct {v1, v0, v5}, Laez;-><init>(Ljava/lang/String;Lorg/apache/http/HttpEntity;)V

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    .line 540
    invoke-direct/range {v0 .. v5}, Laew;->a(Laey;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 542
    if-nez v0, :cond_1

    .line 543
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unable to delete entry."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 546
    :cond_1
    :try_start_0
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 550
    :goto_0
    return-void

    .line 547
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lbxs;)Ljava/io/InputStream;
    .locals 6

    .prologue
    .line 613
    const/4 v0, 0x3

    invoke-direct {p0, p4, v0}, Laew;->a(Lbxs;I)Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 614
    new-instance v1, Laez;

    const-string v2, "POST"

    invoke-direct {v1, v2, v0}, Laez;-><init>(Ljava/lang/String;Lorg/apache/http/HttpEntity;)V

    const/4 v4, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    .line 615
    invoke-direct/range {v0 .. v5}, Laew;->a(Laey;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 617
    if-eqz v0, :cond_0

    .line 618
    return-object v0

    .line 620
    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unable to process batch request."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
