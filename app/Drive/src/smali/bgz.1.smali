.class public Lbgz;
.super Ljava/lang/Object;
.source "TooltipUtil.java"


# direct methods
.method public static a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 55
    new-instance v0, LbgA;

    invoke-direct {v0, p1}, LbgA;-><init>(I)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 61
    return-void
.end method

.method static synthetic a(Landroid/view/View;Ljava/lang/CharSequence;)Z
    .locals 1

    .prologue
    .line 21
    invoke-static {p0, p1}, Lbgz;->b(Landroid/view/View;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private static b(Landroid/view/View;Ljava/lang/CharSequence;)Z
    .locals 11

    .prologue
    const/16 v10, 0x31

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 95
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 131
    :goto_0
    return v0

    .line 99
    :cond_0
    const/4 v2, 0x2

    new-array v2, v2, [I

    .line 100
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 101
    invoke-virtual {p0, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 102
    invoke-virtual {p0, v3}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 104
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 105
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v5

    .line 106
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v6

    .line 107
    aget v7, v2, v0

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v5, v7

    .line 108
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v7, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 109
    const/high16 v8, 0x42400000    # 48.0f

    .line 110
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v8, v9

    float-to-int v8, v8

    .line 112
    invoke-static {v4, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    .line 113
    aget v9, v2, v1

    if-ge v9, v8, :cond_1

    move v0, v1

    .line 114
    :cond_1
    if-eqz v0, :cond_2

    .line 117
    div-int/lit8 v0, v7, 0x2

    sub-int v0, v5, v0

    aget v2, v2, v1

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    add-int/2addr v2, v6

    invoke-virtual {v4, v10, v0, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 130
    :goto_1
    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    move v0, v1

    .line 131
    goto :goto_0

    .line 125
    :cond_2
    div-int/lit8 v0, v7, 0x2

    sub-int v0, v5, v0

    aget v2, v2, v1

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    sub-int/2addr v2, v8

    invoke-virtual {v4, v10, v0, v2}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_1
.end method
