.class Lzv;
.super Ljava/lang/Object;
.source "PreviewCard.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/widget/ImageView;

.field final synthetic a:Lzq;


# direct methods
.method constructor <init>(Lzq;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lzv;->a:Lzq;

    iput-object p2, p0, Lzv;->a:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 195
    invoke-static {}, LamV;->a()V

    .line 198
    :try_start_0
    iget-object v0, p0, Lzv;->a:Lzq;

    invoke-static {v0}, Lzq;->a(Lzq;)LbsU;

    move-result-object v0

    invoke-interface {v0}, LbsU;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 205
    iget-object v1, p0, Lzv;->a:Lzq;

    invoke-static {v1, v5}, Lzq;->a(Lzq;LbsU;)LbsU;

    .line 208
    if-eqz v0, :cond_0

    .line 209
    iget-object v1, p0, Lzv;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 210
    iget-object v1, p0, Lzv;->a:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 211
    iget-object v1, p0, Lzv;->a:Lzq;

    invoke-static {v1}, Lzq;->a(Lzq;)LyY;

    move-result-object v1

    invoke-virtual {v1, v0}, LyY;->a(Landroid/graphics/Bitmap;)V

    .line 215
    :goto_0
    return-void

    .line 199
    :catch_0
    move-exception v0

    .line 202
    :try_start_1
    const-string v1, "PreviewCard"

    const-string v2, "Exception while generating thumbnail:"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 205
    iget-object v0, p0, Lzv;->a:Lzq;

    invoke-static {v0, v5}, Lzq;->a(Lzq;LbsU;)LbsU;

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lzv;->a:Lzq;

    invoke-static {v1, v5}, Lzq;->a(Lzq;LbsU;)LbsU;

    throw v0

    .line 213
    :cond_0
    iget-object v0, p0, Lzv;->a:Lzq;

    iget-object v1, p0, Lzv;->a:Landroid/widget/ImageView;

    invoke-static {v0, v1}, Lzq;->a(Lzq;Landroid/widget/ImageView;)V

    goto :goto_0
.end method
