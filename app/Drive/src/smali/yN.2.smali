.class public LyN;
.super Ljava/lang/Object;
.source "ActivityEntryAdapter.java"


# instance fields
.field private final a:LPH;

.field private final a:LaGM;

.field private final a:LaGR;

.field private final a:LaKM;

.field private final a:LagZ;

.field private final a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LqK;

.field private final a:LtK;


# direct methods
.method public constructor <init>(Laja;LaGM;LaGR;LagZ;LtK;LaKM;LPH;LqK;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LaGM;",
            "LaGR;",
            "LagZ;",
            "LtK;",
            "LaKM;",
            "LPH;",
            "LqK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p0, LyN;->a:Laja;

    .line 110
    invoke-static {p6}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKM;

    iput-object v0, p0, LyN;->a:LaKM;

    .line 111
    invoke-static {p7}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPH;

    iput-object v0, p0, LyN;->a:LPH;

    .line 112
    iput-object p2, p0, LyN;->a:LaGM;

    .line 113
    iput-object p5, p0, LyN;->a:LtK;

    .line 114
    iput-object p3, p0, LyN;->a:LaGR;

    .line 115
    iput-object p4, p0, LyN;->a:LagZ;

    .line 116
    iput-object p8, p0, LyN;->a:LqK;

    .line 117
    return-void
.end method


# virtual methods
.method public a(LPD;LaGu;Lamw;)LyI;
    .locals 13

    .prologue
    .line 121
    iget-object v0, p0, LyN;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    .line 122
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 123
    iget-object v2, p0, LyN;->a:LaKM;

    invoke-interface {v2}, LaKM;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 124
    new-instance v8, LamF;

    invoke-direct {v8, v1, v0}, LamF;-><init>(Landroid/content/Context;Landroid/text/format/Time;)V

    .line 125
    new-instance v0, LyI;

    iget-object v2, p0, LyN;->a:LaGM;

    iget-object v3, p0, LyN;->a:LaGR;

    iget-object v4, p0, LyN;->a:LagZ;

    iget-object v5, p0, LyN;->a:LtK;

    iget-object v9, p0, LyN;->a:LPH;

    iget-object v11, p0, LyN;->a:LqK;

    const/4 v12, 0x0

    move-object v6, p1

    move-object v7, p2

    move-object/from16 v10, p3

    invoke-direct/range {v0 .. v12}, LyI;-><init>(Landroid/content/Context;LaGM;LaGR;LagZ;LtK;LPD;LaGu;LamF;LPH;Lamw;LqK;LyJ;)V

    return-object v0
.end method
