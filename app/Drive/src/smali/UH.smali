.class public LUH;
.super Ljava/lang/Object;
.source "OpenUrlActivityDelegate.java"

# interfaces
.implements LbsJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbsJ",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;)V
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, LUH;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 249
    iget-object v1, p0, LUH;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    .line 250
    iget-object v0, p0, LUH;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    iget-object v0, v0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LUT;

    invoke-interface {v0, v1, p1}, LUT;->a(Landroid/content/Context;Landroid/net/Uri;)LVa;

    move-result-object v2

    .line 251
    iget-object v0, p0, LUH;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    iget-object v0, v0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LUQ;

    .line 252
    invoke-virtual {v2}, LVa;->a()LVc;

    move-result-object v3

    invoke-virtual {v0, v3}, LUQ;->a(LVc;)LUP;

    move-result-object v0

    .line 254
    const-string v3, "[kind=%s, Activity=%s]"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    .line 255
    invoke-virtual {v2}, LVa;->a()LaGv;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 256
    iget-object v4, p0, LUH;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    invoke-static {v4}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;)LqK;

    move-result-object v4

    const-string v5, "documentOpener"

    const-string v6, "useAppToOpenUrl"

    invoke-virtual {v4, v5, v6, v3}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    iget-object v3, p0, LUH;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    .line 261
    invoke-virtual {v3}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "requestCameFromExternalApp"

    invoke-virtual {v3, v4, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 262
    invoke-virtual {v2}, LVa;->a()Ljava/lang/String;

    move-result-object v3

    .line 265
    invoke-virtual {v2}, LVa;->a()LaGv;

    move-result-object v2

    sget-object v4, LaGv;->e:LaGv;

    if-ne v2, v4, :cond_0

    .line 266
    iget-object v0, p0, LUH;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    invoke-static {v0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;)V

    .line 279
    :goto_0
    return-void

    .line 270
    :cond_0
    if-eqz v3, :cond_1

    .line 271
    iget-object v1, p0, LUH;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    invoke-static {v1}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;)LaFO;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a(LaFO;Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v1

    .line 272
    iget-object v2, p0, LUH;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    invoke-static {v2, v0, v1, p1, v5}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;LUP;Lcom/google/android/gms/drive/database/data/ResourceSpec;Landroid/net/Uri;Z)V

    goto :goto_0

    .line 275
    :cond_1
    iget-object v2, p0, LUH;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    .line 276
    invoke-static {v2}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;)LaFO;

    move-result-object v3

    const/4 v4, 0x0

    move-object v2, p1

    .line 275
    invoke-virtual/range {v0 .. v5}, LUP;->a(Landroid/content/Context;Landroid/net/Uri;LaFO;LaGu;Z)Landroid/content/Intent;

    move-result-object v0

    .line 277
    iget-object v1, p0, LUH;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    invoke-static {v1, v0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 246
    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1}, LUH;->a(Landroid/net/Uri;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, LUH;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;Ljava/lang/Throwable;)V

    .line 284
    return-void
.end method
