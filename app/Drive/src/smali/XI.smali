.class public final LXI;
.super Landroid/app/Presentation;
.source "PunchMultiScreenModeFragment.java"


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;Landroid/content/Context;Landroid/view/Display;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, LXI;->a:Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;

    .line 44
    invoke-direct {p0, p2, p3}, Landroid/app/Presentation;-><init>(Landroid/content/Context;Landroid/view/Display;)V

    .line 45
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 50
    invoke-super {p0, p1}, Landroid/app/Presentation;->onCreate(Landroid/os/Bundle;)V

    .line 51
    sget v0, LpP;->punch_presentation_content:I

    invoke-virtual {p0, v0}, LXI;->setContentView(I)V

    .line 54
    iget-object v0, p0, LXI;->a:Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->b:Landroid/view/ViewGroup;

    iget-object v1, p0, LXI;->a:Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;

    iget-object v1, v1, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 55
    sget v0, LpN;->punch_presentation_container:I

    invoke-virtual {p0, v0}, LXI;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 56
    iget-object v1, p0, LXI;->a:Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;

    iget-object v1, v1, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 59
    iget-object v0, p0, LXI;->a:Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 60
    iget-object v0, p0, LXI;->a:Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 61
    return-void
.end method
