.class public LvP;
.super Ljava/lang/Object;
.source "CriterionSetFactoryImpl.java"

# interfaces
.implements LvO;


# instance fields
.field private final a:LCl;

.field private final a:Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

.field private final a:LvL;


# direct methods
.method public constructor <init>(LvL;Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;LCl;)V
    .locals 0
    .param p3    # LCl;
        .annotation runtime LQL;
        .end annotation
    .end param

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, LvP;->a:LvL;

    .line 37
    iput-object p2, p0, LvP;->a:Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    .line 38
    iput-object p3, p0, LvP;->a:LCl;

    .line 39
    return-void
.end method

.method public constructor <init>(LvL;LsC;LCl;)V
    .locals 1
    .param p3    # LCl;
        .annotation runtime LQL;
        .end annotation
    .end param

    .prologue
    .line 27
    .line 28
    invoke-interface {p2}, LsC;->a()Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    move-result-object v0

    .line 27
    invoke-direct {p0, p1, v0, p3}, LvP;-><init>(LvL;Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;LCl;)V

    .line 30
    return-void
.end method


# virtual methods
.method public a(LaFO;)Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, LvP;->a:LCl;

    invoke-virtual {p0, p1, v0}, LvP;->a(LaFO;LCl;)Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    return-object v0
.end method

.method public a(LaFO;LCl;)Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;
    .locals 2

    .prologue
    .line 48
    invoke-virtual {p0, p1}, LvP;->a(LaFO;)LvN;

    move-result-object v0

    .line 49
    iget-object v1, p0, LvP;->a:LvL;

    invoke-interface {v1, p2}, LvL;->b(LCl;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v1

    invoke-virtual {v0, v1}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    .line 50
    invoke-virtual {v0}, LvN;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;
    .locals 2

    .prologue
    .line 55
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    iget-object v0, p1, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-virtual {p0, v0}, LvP;->a(LaFO;)LvN;

    move-result-object v0

    .line 58
    iget-object v1, p0, LvP;->a:LvL;

    invoke-interface {v1, p1}, LvL;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v1

    invoke-virtual {v0, v1}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    .line 59
    iget-object v1, p0, LvP;->a:LvL;

    invoke-interface {v1}, LvL;->a()Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v1

    invoke-virtual {v0, v1}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    .line 60
    invoke-virtual {v0}, LvN;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    return-object v0
.end method

.method public a(LaFO;)LvN;
    .locals 3

    .prologue
    .line 65
    new-instance v0, LvN;

    invoke-direct {v0}, LvN;-><init>()V

    .line 66
    iget-object v1, p0, LvP;->a:LvL;

    invoke-interface {v1, p1}, LvL;->a(LaFO;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v1

    invoke-virtual {v0, v1}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    .line 67
    iget-object v1, p0, LvP;->a:Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    iget-object v2, p0, LvP;->a:LvL;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->a(LvL;LvN;)LvN;

    .line 68
    return-object v0
.end method
