.class public Laz;
.super Ljava/lang/Object;
.source "NotificationCompat.java"


# static fields
.field private static final a:LaG;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 785
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 786
    new-instance v0, LaI;

    invoke-direct {v0}, LaI;-><init>()V

    sput-object v0, Laz;->a:LaG;

    .line 802
    :goto_0
    return-void

    .line 787
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_1

    .line 788
    new-instance v0, LaH;

    invoke-direct {v0}, LaH;-><init>()V

    sput-object v0, Laz;->a:LaG;

    goto :goto_0

    .line 789
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_2

    .line 790
    new-instance v0, LaO;

    invoke-direct {v0}, LaO;-><init>()V

    sput-object v0, Laz;->a:LaG;

    goto :goto_0

    .line 791
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_3

    .line 792
    new-instance v0, LaN;

    invoke-direct {v0}, LaN;-><init>()V

    sput-object v0, Laz;->a:LaG;

    goto :goto_0

    .line 793
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_4

    .line 794
    new-instance v0, LaM;

    invoke-direct {v0}, LaM;-><init>()V

    sput-object v0, Laz;->a:LaG;

    goto :goto_0

    .line 795
    :cond_4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_5

    .line 796
    new-instance v0, LaL;

    invoke-direct {v0}, LaL;-><init>()V

    sput-object v0, Laz;->a:LaG;

    goto :goto_0

    .line 797
    :cond_5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_6

    .line 798
    new-instance v0, LaK;

    invoke-direct {v0}, LaK;-><init>()V

    sput-object v0, Laz;->a:LaG;

    goto :goto_0

    .line 800
    :cond_6
    new-instance v0, LaJ;

    invoke-direct {v0}, LaJ;-><init>()V

    sput-object v0, Laz;->a:LaG;

    goto :goto_0
.end method

.method static synthetic a()LaG;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Laz;->a:LaG;

    return-object v0
.end method

.method static synthetic a(Lax;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 41
    invoke-static {p0, p1}, Laz;->b(Lax;Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic a(Lay;LaP;)V
    .locals 0

    .prologue
    .line 41
    invoke-static {p0, p1}, Laz;->b(Lay;LaP;)V

    return-void
.end method

.method private static b(Lax;Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lax;",
            "Ljava/util/ArrayList",
            "<",
            "LaA;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 749
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaA;

    .line 750
    invoke-interface {p0, v0}, Lax;->a(LaV;)V

    goto :goto_0

    .line 752
    :cond_0
    return-void
.end method

.method private static b(Lay;LaP;)V
    .locals 7

    .prologue
    .line 756
    if-eqz p1, :cond_0

    .line 757
    instance-of v0, p1, LaD;

    if-eqz v0, :cond_1

    .line 758
    check-cast p1, LaD;

    .line 759
    iget-object v0, p1, LaD;->b:Ljava/lang/CharSequence;

    iget-boolean v1, p1, LaD;->b:Z

    iget-object v2, p1, LaD;->c:Ljava/lang/CharSequence;

    iget-object v3, p1, LaD;->a:Ljava/lang/CharSequence;

    invoke-static {p0, v0, v1, v2, v3}, Lba;->a(Lay;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 782
    :cond_0
    :goto_0
    return-void

    .line 764
    :cond_1
    instance-of v0, p1, LaF;

    if-eqz v0, :cond_2

    .line 765
    check-cast p1, LaF;

    .line 766
    iget-object v0, p1, LaF;->b:Ljava/lang/CharSequence;

    iget-boolean v1, p1, LaF;->b:Z

    iget-object v2, p1, LaF;->c:Ljava/lang/CharSequence;

    iget-object v3, p1, LaF;->a:Ljava/util/ArrayList;

    invoke-static {p0, v0, v1, v2, v3}, Lba;->a(Lay;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 771
    :cond_2
    instance-of v0, p1, LaC;

    if-eqz v0, :cond_0

    .line 772
    check-cast p1, LaC;

    .line 773
    iget-object v1, p1, LaC;->b:Ljava/lang/CharSequence;

    iget-boolean v2, p1, LaC;->b:Z

    iget-object v3, p1, LaC;->c:Ljava/lang/CharSequence;

    iget-object v4, p1, LaC;->a:Landroid/graphics/Bitmap;

    iget-object v5, p1, LaC;->b:Landroid/graphics/Bitmap;

    iget-boolean v6, p1, LaC;->a:Z

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lba;->a(Lay;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Z)V

    goto :goto_0
.end method
