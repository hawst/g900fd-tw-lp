.class public LaOf;
.super LaOq;


# static fields
.field private static final a:I


# instance fields
.field private a:LaOj;

.field private final a:Landroid/app/Activity;

.field private a:Landroid/webkit/WebChromeClient$CustomViewCallback;

.field private a:Landroid/widget/FrameLayout;

.field private a:Landroid/widget/RelativeLayout;

.field private a:Lcom/google/android/gms/internal/cm;

.field private a:Lcom/google/android/gms/internal/co;

.field private a:Lcom/google/android/gms/internal/cp;

.field private a:Lcom/google/android/gms/internal/fc;

.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0, v0, v0, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, LaOf;->a:I

    return-void
.end method

.method private static a(IIII)Landroid/widget/RelativeLayout$LayoutParams;
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p2, p3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, p0, p1, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/internal/cm;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms.ads.AdActivity"

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.ads.internal.overlay.useClientJar"

    iget-object v2, p1, Lcom/google/android/gms/internal/cm;->a:Lcom/google/android/gms/internal/fa;

    iget-boolean v2, v2, Lcom/google/android/gms/internal/fa;->a:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {v0, p1}, Lcom/google/android/gms/internal/cm;->a(Landroid/content/Intent;Lcom/google/android/gms/internal/cm;)V

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    instance-of v1, p0, Landroid/app/Activity;

    if-nez v1, :cond_0

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private c(Z)V
    .locals 13

    const/16 v3, 0x400

    const/4 v12, -0x1

    const/4 v2, 0x1

    const/4 v4, 0x0

    iget-boolean v0, p0, LaOf;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LaOf;->a:Landroid/app/Activity;

    invoke-virtual {v0, v2}, Landroid/app/Activity;->requestWindowFeature(I)Z

    :cond_0
    iget-object v0, p0, LaOf;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget-boolean v1, p0, LaOf;->d:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v1, v1, Lcom/google/android/gms/internal/cm;->a:Lcom/google/android/gms/internal/aa;

    iget-boolean v1, v1, Lcom/google/android/gms/internal/aa;->b:Z

    if-eqz v1, :cond_2

    :cond_1
    invoke-virtual {v0, v3, v3}, Landroid/view/Window;->setFlags(II)V

    :cond_2
    iget-object v1, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget v1, v1, Lcom/google/android/gms/internal/cm;->b:I

    invoke-virtual {p0, v1}, LaOf;->a(I)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v1, v3, :cond_3

    const-string v1, "Enabling hardware acceleration on the AdActivity window."

    invoke-static {v1}, LaQc;->a(Ljava/lang/String;)V

    invoke-static {v0}, LaPY;->a(Landroid/view/Window;)V

    :cond_3
    new-instance v0, LaOi;

    iget-object v1, p0, LaOf;->a:Landroid/app/Activity;

    iget-object v3, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v3, v3, Lcom/google/android/gms/internal/cm;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v3}, LaOi;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, LaOf;->a:Landroid/widget/RelativeLayout;

    iget-boolean v0, p0, LaOf;->d:Z

    if-nez v0, :cond_7

    iget-object v0, p0, LaOf;->a:Landroid/widget/RelativeLayout;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    :goto_0
    iget-object v0, p0, LaOf;->a:Landroid/app/Activity;

    iget-object v1, p0, LaOf;->a:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    invoke-virtual {p0}, LaOf;->i()V

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v0, v0, Lcom/google/android/gms/internal/cm;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->a()LaQu;

    move-result-object v0

    invoke-virtual {v0}, LaQu;->a()Z

    move-result v3

    if-eqz p1, :cond_a

    iget-object v0, p0, LaOf;->a:Landroid/app/Activity;

    iget-object v1, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v1, v1, Lcom/google/android/gms/internal/cm;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/fc;->a()Lcom/google/android/gms/internal/aq;

    move-result-object v1

    iget-object v5, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v5, v5, Lcom/google/android/gms/internal/cm;->a:Lcom/google/android/gms/internal/fa;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/internal/fc;->a(Landroid/content/Context;Lcom/google/android/gms/internal/aq;ZZLaSP;Lcom/google/android/gms/internal/fa;)Lcom/google/android/gms/internal/fc;

    move-result-object v0

    iput-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->a()LaQu;

    move-result-object v5

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v8, v0, Lcom/google/android/gms/internal/cm;->a:LaNk;

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v9, v0, Lcom/google/android/gms/internal/cm;->a:LaOo;

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v11, v0, Lcom/google/android/gms/internal/cm;->a:LaNv;

    move-object v6, v4

    move-object v7, v4

    move v10, v2

    invoke-virtual/range {v5 .. v11}, LaQu;->a(LaVk;LaOl;LaNk;LaOo;ZLaNv;)V

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->a()LaQu;

    move-result-object v0

    new-instance v1, LaOg;

    invoke-direct {v1, p0}, LaOg;-><init>(LaOf;)V

    invoke-virtual {v0, v1}, LaQu;->a(LaQw;)V

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v0, v0, Lcom/google/android/gms/internal/cm;->c:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    iget-object v1, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v1, v1, Lcom/google/android/gms/internal/cm;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/fc;->loadUrl(Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/fc;->a(LaOf;)V

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_4

    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_4

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_4
    iget-boolean v0, p0, LaOf;->d:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    sget v1, LaOf;->a:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/fc;->setBackgroundColor(I)V

    :cond_5
    iget-object v0, p0, LaOf;->a:Landroid/widget/RelativeLayout;

    iget-object v1, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0, v1, v12, v12}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;II)V

    if-nez p1, :cond_6

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->c()V

    :cond_6
    invoke-virtual {p0, v3}, LaOf;->a(Z)V

    return-void

    :cond_7
    iget-object v0, p0, LaOf;->a:Landroid/widget/RelativeLayout;

    sget v1, LaOf;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v0, v0, Lcom/google/android/gms/internal/cm;->b:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v5, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v6, v0, Lcom/google/android/gms/internal/cm;->a:Ljava/lang/String;

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v7, v0, Lcom/google/android/gms/internal/cm;->b:Ljava/lang/String;

    const-string v8, "text/html"

    const-string v9, "UTF-8"

    move-object v10, v4

    invoke-virtual/range {v5 .. v10}, Lcom/google/android/gms/internal/fc;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_9
    new-instance v0, LaOh;

    const-string v1, "No URL or HTML to display in ad overlay."

    invoke-direct {v0, v1}, LaOh;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v0, v0, Lcom/google/android/gms/internal/cm;->a:Lcom/google/android/gms/internal/fc;

    iput-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    iget-object v1, p0, LaOf;->a:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/fc;->setContext(Landroid/content/Context;)V

    goto :goto_1
.end method

.method private k()V
    .locals 4

    iget-object v0, p0, LaOf;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LaOf;->c:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LaOf;->c:Z

    iget-object v0, p0, LaOf;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    if-eqz v0, :cond_2

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->b()V

    iget-object v0, p0, LaOf;->a:Landroid/widget/RelativeLayout;

    iget-object v1, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    iget-object v0, p0, LaOf;->a:LaOj;

    if-eqz v0, :cond_2

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/fc;->a(Z)V

    iget-object v0, p0, LaOf;->a:LaOj;

    iget-object v0, v0, LaOj;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    iget-object v2, p0, LaOf;->a:LaOj;

    iget v2, v2, LaOj;->a:I

    iget-object v3, p0, LaOf;->a:LaOj;

    iget-object v3, v3, LaOj;->a:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v0, v0, Lcom/google/android/gms/internal/cm;->a:LaOl;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v0, v0, Lcom/google/android/gms/internal/cm;->a:LaOl;

    invoke-interface {v0}, LaOl;->l()V

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/google/android/gms/internal/co;
    .locals 1

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/co;

    return-object v0
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, LaOf;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public a(I)V
    .locals 1

    iget-object v0, p0, LaOf;->a:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    return-void
.end method

.method public a(IIII)V
    .locals 2

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/co;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/co;

    invoke-static {p1, p2, p3, p4}, LaOf;->a(IIII)Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/co;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const-string v1, "com.google.android.gms.ads.internal.overlay.hasResumed"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    iput-boolean v0, p0, LaOf;->b:Z

    :try_start_0
    iget-object v0, p0, LaOf;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/cm;->a(Landroid/content/Intent;)Lcom/google/android/gms/internal/cm;

    move-result-object v0

    iput-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    if-nez v0, :cond_2

    new-instance v0, LaOh;

    const-string v1, "Could not get info for ad overlay."

    invoke-direct {v0, v1}, LaOh;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch LaOh; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, LaOh;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaQc;->e(Ljava/lang/String;)V

    iget-object v0, p0, LaOf;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v0, v0, Lcom/google/android/gms/internal/cm;->a:Lcom/google/android/gms/internal/aa;

    if-eqz v0, :cond_5

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v0, v0, Lcom/google/android/gms/internal/cm;->a:Lcom/google/android/gms/internal/aa;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/aa;->a:Z

    iput-boolean v0, p0, LaOf;->d:Z

    :goto_1
    if-nez p1, :cond_4

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v0, v0, Lcom/google/android/gms/internal/cm;->a:LaOl;

    if-eqz v0, :cond_3

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v0, v0, Lcom/google/android/gms/internal/cm;->a:LaOl;

    invoke-interface {v0}, LaOl;->m()V

    :cond_3
    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget v0, v0, Lcom/google/android/gms/internal/cm;->c:I

    if-eq v0, v2, :cond_4

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v0, v0, Lcom/google/android/gms/internal/cm;->a:LaVk;

    if-eqz v0, :cond_4

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v0, v0, Lcom/google/android/gms/internal/cm;->a:LaVk;

    invoke-interface {v0}, LaVk;->o()V

    :cond_4
    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget v0, v0, Lcom/google/android/gms/internal/cm;->c:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, LaOh;

    const-string v1, "Could not determine ad overlay type."

    invoke-direct {v0, v1}, LaOh;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, LaOf;->d:Z

    goto :goto_1

    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LaOf;->c(Z)V

    goto :goto_0

    :pswitch_1
    new-instance v0, LaOj;

    iget-object v1, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v1, v1, Lcom/google/android/gms/internal/cm;->a:Lcom/google/android/gms/internal/fc;

    invoke-direct {v0, v1}, LaOj;-><init>(Lcom/google/android/gms/internal/fc;)V

    iput-object v0, p0, LaOf;->a:LaOj;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, LaOf;->c(Z)V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LaOf;->c(Z)V

    goto :goto_0

    :pswitch_3
    iget-boolean v0, p0, LaOf;->b:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, LaOf;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_6
    iget-object v0, p0, LaOf;->a:Landroid/app/Activity;

    iget-object v1, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v1, v1, Lcom/google/android/gms/internal/cm;->a:Lcom/google/android/gms/internal/cj;

    iget-object v2, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-object v2, v2, Lcom/google/android/gms/internal/cm;->a:LaOo;

    invoke-static {v0, v1, v2}, LaOd;->a(Landroid/content/Context;Lcom/google/android/gms/internal/cj;LaOo;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LaOf;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V
    :try_end_1
    .catch LaOh; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V
    .locals 3

    const/4 v2, -0x1

    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, LaOf;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LaOf;->a:Landroid/widget/FrameLayout;

    iget-object v0, p0, LaOf;->a:Landroid/widget/FrameLayout;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    iget-object v0, p0, LaOf;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1, v2, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;II)V

    iget-object v0, p0, LaOf;->a:Landroid/app/Activity;

    iget-object v1, p0, LaOf;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    invoke-virtual {p0}, LaOf;->i()V

    iput-object p2, p0, LaOf;->a:Landroid/webkit/WebChromeClient$CustomViewCallback;

    return-void
.end method

.method public a(Z)V
    .locals 4

    const/4 v3, -0x2

    if-eqz p1, :cond_0

    const/16 v0, 0x32

    :goto_0
    new-instance v1, Lcom/google/android/gms/internal/cp;

    iget-object v2, p0, LaOf;->a:Landroid/app/Activity;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/internal/cp;-><init>(Landroid/app/Activity;I)V

    iput-object v1, p0, LaOf;->a:Lcom/google/android/gms/internal/cp;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    if-eqz p1, :cond_1

    const/16 v0, 0xb

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cp;

    iget-object v2, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget-boolean v2, v2, Lcom/google/android/gms/internal/cm;->a:Z

    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/cp;->a(Z)V

    iget-object v0, p0, LaOf;->a:Landroid/widget/RelativeLayout;

    iget-object v2, p0, LaOf;->a:Lcom/google/android/gms/internal/cp;

    invoke-virtual {v0, v2, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_0
    const/16 v0, 0x20

    goto :goto_0

    :cond_1
    const/16 v0, 0x9

    goto :goto_1
.end method

.method public b()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget v0, v0, Lcom/google/android/gms/internal/cm;->b:I

    invoke-virtual {p0, v0}, LaOf;->a(I)V

    :cond_0
    iget-object v0, p0, LaOf;->a:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, LaOf;->a:Landroid/app/Activity;

    iget-object v1, p0, LaOf;->a:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    invoke-virtual {p0}, LaOf;->i()V

    iget-object v0, p0, LaOf;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    iput-object v2, p0, LaOf;->a:Landroid/widget/FrameLayout;

    :cond_1
    iget-object v0, p0, LaOf;->a:Landroid/webkit/WebChromeClient$CustomViewCallback;

    if-eqz v0, :cond_2

    iget-object v0, p0, LaOf;->a:Landroid/webkit/WebChromeClient$CustomViewCallback;

    invoke-interface {v0}, Landroid/webkit/WebChromeClient$CustomViewCallback;->onCustomViewHidden()V

    iput-object v2, p0, LaOf;->a:Landroid/webkit/WebChromeClient$CustomViewCallback;

    :cond_2
    return-void
.end method

.method public b(IIII)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/co;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/internal/co;

    iget-object v1, p0, LaOf;->a:Landroid/app/Activity;

    iget-object v2, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/internal/co;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/fc;)V

    iput-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/co;

    iget-object v0, p0, LaOf;->a:Landroid/widget/RelativeLayout;

    iget-object v1, p0, LaOf;->a:Lcom/google/android/gms/internal/co;

    invoke-static {p1, p2, p3, p4}, LaOf;->a(IIII)Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fc;->a()LaQu;

    move-result-object v0

    invoke-virtual {v0, v3}, LaQu;->a(Z)V

    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "com.google.android.gms.ads.internal.overlay.hasResumed"

    iget-boolean v1, p0, LaOf;->b:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public b(Z)V
    .locals 1

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cp;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cp;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/cp;->a(Z)V

    :cond_0
    return-void
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method public d()V
    .locals 0

    return-void
.end method

.method public e()V
    .locals 2

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/cm;

    iget v0, v0, Lcom/google/android/gms/internal/cm;->c:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, LaOf;->b:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LaOf;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    :goto_0
    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    if-eqz v0, :cond_1

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    invoke-static {v0}, LaPS;->b(Landroid/webkit/WebView;)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, LaOf;->b:Z

    goto :goto_0
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/co;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/co;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/co;->c()V

    :cond_0
    invoke-virtual {p0}, LaOf;->b()V

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    if-eqz v0, :cond_2

    iget-object v0, p0, LaOf;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LaOf;->a:LaOj;

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    invoke-static {v0}, LaPS;->a(Landroid/webkit/WebView;)V

    :cond_2
    invoke-direct {p0}, LaOf;->k()V

    return-void
.end method

.method public g()V
    .locals 0

    invoke-direct {p0}, LaOf;->k()V

    return-void
.end method

.method public h()V
    .locals 2

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/co;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/co;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/co;->a()V

    :cond_0
    iget-object v0, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    if-eqz v0, :cond_1

    iget-object v0, p0, LaOf;->a:Landroid/widget/RelativeLayout;

    iget-object v1, p0, LaOf;->a:Lcom/google/android/gms/internal/fc;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    :cond_1
    invoke-direct {p0}, LaOf;->k()V

    return-void
.end method

.method public i()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, LaOf;->a:Z

    return-void
.end method

.method public j()V
    .locals 2

    iget-object v0, p0, LaOf;->a:Landroid/widget/RelativeLayout;

    iget-object v1, p0, LaOf;->a:Lcom/google/android/gms/internal/cp;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LaOf;->a(Z)V

    return-void
.end method
