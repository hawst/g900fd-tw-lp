.class public Lwr;
.super Ljava/lang/Object;
.source "NavigationPathUtil.java"


# direct methods
.method public static a(Ljava/util/List;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)LbmF;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;",
            "Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;",
            ")",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v1

    .line 44
    invoke-virtual {v1, p0}, LbmH;->a(Ljava/lang/Iterable;)LbmH;

    .line 45
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 46
    invoke-interface {p1}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()LvN;

    move-result-object v2

    .line 47
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    .line 48
    invoke-interface {v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    .line 49
    invoke-interface {v0}, Lcom/google/android/apps/docs/app/model/navigation/Criterion;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 50
    invoke-virtual {v2, v0}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    goto :goto_0

    .line 53
    :cond_1
    new-instance v0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-virtual {v2}, LvN;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;-><init>(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V

    invoke-virtual {v1, v0}, LbmH;->a(Ljava/lang/Object;)LbmH;

    .line 58
    :goto_1
    invoke-virtual {v1}, LbmH;->a()LbmF;

    move-result-object v0

    return-object v0

    .line 55
    :cond_2
    new-instance v0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-direct {v0, p1}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;-><init>(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V

    invoke-virtual {v1, v0}, LbmH;->a(Ljava/lang/Object;)LbmH;

    goto :goto_1
.end method

.method public static a(Ljava/util/List;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;)",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;"
        }
    .end annotation

    .prologue
    .line 63
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 65
    invoke-static {p0}, Lbnm;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    return-object v0

    .line 64
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 89
    invoke-static {p0}, Lbnm;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    .line 90
    invoke-interface {v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    .line 91
    instance-of v2, v0, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;

    if-eqz v2, :cond_0

    .line 92
    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;

    .line 93
    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;->a()Ljava/lang/String;

    move-result-object v0

    .line 96
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lwm;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    invoke-interface {p0}, Lwm;->a()LbmF;

    move-result-object v0

    invoke-static {v0}, Lwr;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaFO;LvO;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            "LaFO;",
            "LvO;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    new-instance v0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    .line 76
    invoke-interface {p2, p1}, LvO;->a(LaFO;)Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;-><init>(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V

    .line 75
    invoke-static {v0}, LbmF;->a(Ljava/lang/Object;)LbmF;

    move-result-object v0

    .line 77
    invoke-interface {p2, p0}, LvO;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v1

    invoke-static {v0, v1}, Lwr;->a(Ljava/util/List;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)LbmF;

    move-result-object v0

    .line 79
    return-object v0
.end method
