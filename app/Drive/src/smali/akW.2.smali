.class public LakW;
.super Ljava/lang/Object;
.source "DiskCacheDir.java"


# instance fields
.field private final a:LaGM;

.field private final a:Ljava/io/File;


# direct methods
.method public constructor <init>(Landroid/app/Application;LaGM;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    invoke-static {p1}, LakU;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, LakW;->a:Ljava/io/File;

    .line 67
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p0, LakW;->a:LaGM;

    .line 68
    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 98
    iget-object v0, p0, LakW;->a:Ljava/io/File;

    const-string v1, "diskCache"

    invoke-static {v0, v1}, LakU;->a(Ljava/io/File;Ljava/lang/String;)V

    .line 99
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LakW;->a:Ljava/io/File;

    const-string v2, "diskCache"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 101
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 102
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LakY;->a(Ljava/lang/String;)LakY;

    move-result-object v4

    if-nez v4, :cond_0

    .line 103
    invoke-static {v3}, LakU;->a(Ljava/io/File;)V

    .line 101
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 106
    :cond_1
    return-void
.end method

.method public a(Ljava/util/Set;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LaFO;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 74
    invoke-static {}, LbmY;->a()Lbna;

    move-result-object v1

    .line 75
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFO;

    .line 76
    iget-object v3, p0, LakW;->a:LaGM;

    invoke-static {v3, v0}, LakU;->a(LaGM;LaFO;)Ljava/lang/String;

    move-result-object v0

    .line 77
    invoke-virtual {v1, v0}, Lbna;->a(Ljava/lang/Object;)Lbna;

    goto :goto_0

    .line 80
    :cond_0
    invoke-virtual {v1}, Lbna;->a()LbmY;

    move-result-object v1

    .line 82
    iget-object v0, p0, LakW;->a:Ljava/io/File;

    const-string v2, "diskCache"

    invoke-static {v0, v2}, LakU;->a(Ljava/io/File;Ljava/lang/String;)V

    .line 83
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, LakW;->a:Ljava/io/File;

    const-string v3, "diskCache"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 85
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 86
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 87
    invoke-static {v4, v1}, LakU;->a(Ljava/io/File;LbmY;)V

    .line 85
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 89
    :cond_1
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    goto :goto_2

    .line 92
    :cond_2
    return-void
.end method
