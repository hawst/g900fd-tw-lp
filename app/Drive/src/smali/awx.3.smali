.class Lawx;
.super Ljava/lang/Object;
.source "Viewer.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic a:LauW;

.field final synthetic a:Laww;


# direct methods
.method constructor <init>(Laww;LauW;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lawx;->a:Laww;

    iput-object p2, p0, Lawx;->a:LauW;

    iput-object p3, p0, Lawx;->a:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 288
    iget-object v0, p0, Lawx;->a:Laww;

    invoke-static {v0}, Laww;->a(Laww;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "Received contents while restoring another copy"

    invoke-static {v0, v2}, LauV;->a(ZLjava/lang/String;)V

    .line 289
    iget-object v0, p0, Lawx;->a:Laww;

    iget-object v2, p0, Lawx;->a:LauW;

    iget-object v3, p0, Lawx;->a:Landroid/os/Bundle;

    invoke-virtual {v0, v2, v3}, Laww;->a(LauW;Landroid/os/Bundle;)V

    .line 290
    iget-object v0, p0, Lawx;->a:Laww;

    invoke-virtual {v0}, Laww;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Got contents (delayed)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lawx;->a:LauW;

    iget-object v3, v3, LauW;->a:Lavg;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    iget-object v0, p0, Lawx;->a:Laww;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Laww;->a(Laww;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 292
    iget-object v0, p0, Lawx;->a:Laww;

    invoke-static {v0, v1}, Laww;->a(Laww;Z)Z

    .line 293
    return-void

    .line 288
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
