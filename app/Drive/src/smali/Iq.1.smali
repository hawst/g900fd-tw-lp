.class public abstract LIq;
.super LIf;
.source "FoldersThenSortedFieldGrouper.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LIy;",
        ">",
        "LIf;"
    }
.end annotation


# instance fields
.field private final a:LIr;


# direct methods
.method constructor <init>(Ljava/lang/String;LIg;)V
    .locals 2

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, LIf;-><init>(Ljava/lang/String;LIg;)V

    .line 42
    new-instance v0, LIr;

    sget v1, Lxi;->fast_scroll_title_grouper_collections:I

    invoke-direct {v0, v1}, LIr;-><init>(I)V

    iput-object v0, p0, LIq;->a:LIr;

    .line 47
    return-void
.end method

.method private a(LCv;)Z
    .locals 2

    .prologue
    .line 50
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    invoke-interface {p1}, LCv;->a()LaGv;

    move-result-object v0

    .line 52
    sget-object v1, LaGv;->a:LaGv;

    invoke-virtual {v1, v0}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(LCv;)LIL;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 84
    invoke-direct {p0, p1}, LIq;->a(LCv;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 85
    :goto_0
    invoke-virtual {p0, p1}, LIq;->a(LCv;)Ljava/lang/Object;

    move-result-object v3

    .line 86
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v4, v2

    aput-object v3, v4, v1

    invoke-static {v4}, LIL;->a([Ljava/lang/Object;)LIL;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 84
    goto :goto_0
.end method

.method protected abstract a(LCv;)LIy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LCv;",
            ")TT;"
        }
    .end annotation
.end method

.method protected abstract a(LCv;)Ljava/lang/Object;
.end method

.method protected abstract a()Ljava/lang/String;
.end method

.method protected a()Z
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x1

    return v0
.end method

.method public b(LCv;)LIy;
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0, p1}, LIq;->a(LCv;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LIq;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, LIq;->a:LIr;

    .line 76
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, LIq;->a(LCv;)LIy;

    move-result-object v0

    goto :goto_0
.end method

.method protected c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60
    invoke-virtual {p0}, LIq;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LaES;->t:LaES;

    invoke-virtual {v1}, LaES;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " <> \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LaGv;->a:LaGv;

    invoke-virtual {v1}, LaGv;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 65
    invoke-virtual {p0}, LIq;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 67
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LIq;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
