.class public LaFu;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "DatabaseHelper.java"


# static fields
.field public static final a:Lbng;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbng",
            "<",
            "Ljava/lang/Integer;",
            "LaFv;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:I

.field private final a:Landroid/content/Context;

.field private final a:Ljava/lang/String;

.field private final a:[Lbjv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lbjv",
            "<+",
            "LaFy;",
            ">;"
        }
    .end annotation
.end field

.field private final b:I

.field private final b:Lbng;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbng",
            "<",
            "Ljava/lang/Integer;",
            "LaFv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    invoke-static {}, Lbng;->a()Lbng;

    move-result-object v0

    sput-object v0, LaFu;->a:Lbng;

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;[Lbjv;IILbng;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "[",
            "Lbjv",
            "<+",
            "LaFy;",
            ">;II",
            "Lbng",
            "<",
            "Ljava/lang/Integer;",
            "LaFv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 98
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p4}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 99
    iput-object p1, p0, LaFu;->a:Landroid/content/Context;

    .line 100
    iput-object p3, p0, LaFu;->a:[Lbjv;

    .line 101
    iput p4, p0, LaFu;->a:I

    .line 102
    iput p5, p0, LaFu;->b:I

    .line 103
    iput-object p2, p0, LaFu;->a:Ljava/lang/String;

    .line 104
    iput-object p6, p0, LaFu;->b:Lbng;

    .line 105
    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1

    .prologue
    .line 227
    add-int/lit8 v0, p2, 0x1

    :goto_0
    if-gt v0, p3, :cond_0

    .line 228
    invoke-direct {p0, p1, v0}, LaFu;->e(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 227
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 230
    :cond_0
    invoke-virtual {p0, p1}, LaFu;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 231
    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;IIZ)V
    .locals 1

    .prologue
    .line 199
    invoke-virtual {p0, p1}, LaFu;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 200
    invoke-virtual {p0, p1, p3}, LaFu;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 203
    add-int/lit8 v0, p2, 0x1

    :goto_0
    if-ge v0, p3, :cond_0

    .line 204
    invoke-virtual {p0, p1, v0}, LaFu;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 203
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 207
    :cond_0
    invoke-direct {p0, p1, p3}, LaFu;->d(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 209
    invoke-virtual {p0, p1}, LaFu;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 210
    invoke-direct {p0, p1, p2}, LaFu;->e(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 212
    if-eqz p4, :cond_1

    .line 213
    invoke-virtual {p0, p1}, LaFu;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 221
    :cond_1
    return-void
.end method

.method private a(Ljava/io/File;)Z
    .locals 2

    .prologue
    .line 341
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 342
    invoke-static {p1}, Landroid/database/sqlite/SQLiteDatabase;->deleteDatabase(Ljava/io/File;)Z

    move-result v0

    .line 344
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v0

    goto :goto_0
.end method

.method private d(Landroid/database/sqlite/SQLiteDatabase;I)V
    .locals 5

    .prologue
    .line 262
    iget-object v2, p0, LaFu;->a:[Lbjv;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 263
    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFy;

    .line 264
    invoke-virtual {v0, p2}, LaFy;->a(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 265
    invoke-virtual {v0, p1, p2}, LaFy;->c(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 262
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 268
    :cond_1
    return-void
.end method

.method private e(Landroid/database/sqlite/SQLiteDatabase;I)V
    .locals 5

    .prologue
    .line 309
    iget-object v2, p0, LaFu;->a:[Lbjv;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 310
    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFy;

    .line 311
    invoke-virtual {v0, p2}, LaFy;->a(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 312
    invoke-virtual {v0, p1, p2}, LaFy;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 309
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 315
    :cond_1
    return-void
.end method


# virtual methods
.method protected a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 135
    return-void
.end method

.method a(Landroid/database/sqlite/SQLiteDatabase;I)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 121
    const-string v1, "DatabaseHelper"

    const-string v2, "Creating a new database at version %s for %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x1

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 122
    iget-object v2, p0, LaFu;->a:[Lbjv;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 123
    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFy;

    .line 124
    invoke-virtual {v0, p2}, LaFy;->a(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 125
    invoke-virtual {v0, p1, p2}, LaFy;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 122
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 128
    :cond_1
    return-void
.end method

.method public a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 332
    iget-object v0, p0, LaFu;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 333
    return-void
.end method

.method b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 291
    const-string v1, "SQLITE_MASTER"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "name"

    aput-object v0, v2, v3

    const-string v3, "type == \'view\'"

    move-object v0, p1

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 294
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 295
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_0

    .line 296
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 297
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DROP VIEW "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, LaFr;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 298
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 301
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 303
    return-void
.end method

.method b(Landroid/database/sqlite/SQLiteDatabase;I)V
    .locals 5

    .prologue
    .line 276
    iget-object v2, p0, LaFu;->a:[Lbjv;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 277
    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFy;

    .line 278
    invoke-virtual {v0, p2}, LaFy;->a(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 281
    invoke-virtual {v0, p1, p2}, LaFy;->d(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 276
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 284
    :cond_1
    return-void
.end method

.method c(Landroid/database/sqlite/SQLiteDatabase;I)V
    .locals 1

    .prologue
    .line 322
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->getVersion()I

    move-result v0

    .line 323
    invoke-direct {p0, p1, v0}, LaFu;->e(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 324
    invoke-virtual {p0, p1}, LaFu;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 325
    invoke-virtual {p0, p1, p2}, LaFu;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 326
    iget v0, p0, LaFu;->a:I

    if-ne p2, v0, :cond_0

    .line 327
    invoke-virtual {p0, p1}, LaFu;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 329
    :cond_0
    return-void
.end method

.method public getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 5

    .prologue
    .line 236
    :try_start_0
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch LaFw; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 244
    :goto_0
    return-object v0

    .line 237
    :catch_0
    move-exception v0

    .line 240
    invoke-virtual {v0}, LaFw;->a()Ljava/io/File;

    move-result-object v0

    .line 241
    invoke-direct {p0, v0}, LaFu;->a(Ljava/io/File;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 242
    const-string v1, "DatabaseHelper"

    const-string v2, "Failed to delete database file: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 244
    :cond_0
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 111
    :try_start_0
    iget v0, p0, LaFu;->a:I

    invoke-virtual {p0, p1, v0}, LaFu;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 112
    invoke-virtual {p0, p1}, LaFu;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 113
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 117
    return-void

    .line 115
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 5

    .prologue
    .line 250
    const-string v0, "DatabaseHelper"

    const-string v1, "Resetting the database due to downgrade. Old version: %s, new version: %s."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 251
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 250
    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 254
    new-instance v0, LaFw;

    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, LaFw;-><init>(Ljava/io/File;)V

    throw v0
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 139
    const-string v0, "DatabaseHelper"

    const-string v3, "Upgrading database %s from version %s to %s databaseName=%s"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    .line 140
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x3

    iget-object v6, p0, LaFu;->a:Ljava/lang/String;

    aput-object v6, v4, v5

    .line 139
    invoke-static {v0, v3, v4}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 141
    iget v0, p0, LaFu;->a:I

    if-ne p3, v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Cannot upgrade database to version other than latest."

    invoke-static {v0, v3}, LbiT;->a(ZLjava/lang/Object;)V

    .line 143
    iget v0, p0, LaFu;->b:I

    if-ge p2, v0, :cond_2

    .line 144
    const-string v0, "DatabaseHelper"

    const-string v3, "Current database is too old to upgrade (%s < %s). Wiping all existing data."

    new-array v4, v7, [Ljava/lang/Object;

    .line 145
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    iget v2, p0, LaFu;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v1

    .line 144
    invoke-static {v0, v3, v4}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 146
    invoke-virtual {p0, p1, p3}, LaFu;->c(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 192
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 141
    goto :goto_0

    .line 148
    :cond_2
    const-string v0, "PRAGMA foreign_keys=OFF;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 150
    :try_start_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 153
    :try_start_1
    invoke-direct {p0, p1, p2, p3}, LaFu;->a(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 156
    iget-object v0, p0, LaFu;->b:Lbng;

    add-int/lit8 v3, p2, 0x1

    .line 157
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lbng;->b(Ljava/lang/Object;)Lbng;

    move-result-object v0

    .line 158
    invoke-interface {v0}, Ljava/util/SortedMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 159
    if-le v3, p3, :cond_5

    .line 167
    :cond_3
    if-ge p2, p3, :cond_4

    .line 168
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, LaFu;->a(Landroid/database/sqlite/SQLiteDatabase;IIZ)V

    .line 170
    :cond_4
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 176
    :try_start_2
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 187
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    const-string v0, "PRAGMA foreign_keys=ON;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto :goto_1

    .line 162
    :cond_5
    if-ne v3, p3, :cond_6

    move v0, v1

    :goto_3
    :try_start_3
    invoke-direct {p0, p1, p2, v3, v0}, LaFu;->a(Landroid/database/sqlite/SQLiteDatabase;IIZ)V

    .line 163
    iget-object v0, p0, LaFu;->b:Lbng;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Lbng;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFv;

    .line 164
    invoke-interface {v0, p1}, LaFv;->a(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move p2, v3

    .line 166
    goto :goto_2

    :cond_6
    move v0, v2

    .line 162
    goto :goto_3

    .line 172
    :catch_0
    move-exception v0

    .line 173
    :try_start_4
    const-string v1, "DatabaseHelper"

    const-string v2, "An exception occured during database upgrade."

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 174
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 176
    :catchall_0
    move-exception v0

    :try_start_5
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 178
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 179
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 181
    invoke-direct {p0, v1}, LaFu;->a(Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 182
    const-string v2, "DatabaseHelper"

    const-string v3, "Failed to delete database file: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 184
    :cond_7
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 187
    :catchall_1
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 188
    const-string v1, "PRAGMA foreign_keys=ON;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    :cond_8
    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 337
    const-string v0, "DatabaseHelper[%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LaFu;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
