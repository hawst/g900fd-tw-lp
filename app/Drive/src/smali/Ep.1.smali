.class public LEp;
.super LaGN;
.source "CreateDocumentLayerFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGN",
        "<",
        "Lcom/google/android/gms/drive/database/data/EntrySpec;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;

.field final synthetic a:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, LEp;->a:Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;

    iput-object p2, p0, LEp;->a:Ljava/util/List;

    invoke-direct {p0}, LaGN;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LaGM;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, LEp;->a:Ljava/util/List;

    iget-object v1, p0, LEp;->a:Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;

    iget-object v1, v1, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a:LaFO;

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/docs/app/DocListActivity;->a(LaGM;Ljava/util/List;LaFO;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(LaGM;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0, p1}, LEp;->a(LaGM;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 3

    .prologue
    .line 114
    iget-object v0, p0, LEp;->a:Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a()LH;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_0

    .line 116
    iget-object v1, p0, LEp;->a:Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;

    iget-object v1, v1, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a:LaFO;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;->a(Landroid/content/Context;LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    .line 117
    iget-object v1, p0, LEp;->a:Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a()LH;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LH;->startActivityForResult(Landroid/content/Intent;I)V

    .line 119
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 106
    check-cast p1, Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {p0, p1}, LEp;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    return-void
.end method
