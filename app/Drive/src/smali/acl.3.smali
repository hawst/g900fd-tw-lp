.class Lacl;
.super Ljava/lang/Object;
.source "SharingHelper.java"

# interfaces
.implements LbsJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbsJ",
        "<",
        "Lacr;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lacj;

.field final synthetic a:Lcom/google/android/gms/drive/database/data/EntrySpec;


# direct methods
.method constructor <init>(Lacj;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Lacl;->a:Lacj;

    iput-object p2, p0, Lacl;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lacr;)V
    .locals 2

    .prologue
    .line 256
    iget-object v0, p0, Lacl;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lacl;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v1, p0, Lacl;->a:Lacj;

    iget-object v1, v1, Lacj;->a:Lacq;

    .line 257
    invoke-virtual {v1}, Lacq;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 262
    :cond_0
    :goto_0
    return-void

    .line 260
    :cond_1
    iget-object v0, p0, Lacl;->a:Lacj;

    iget-object v0, v0, Lacj;->a:Lacq;

    invoke-virtual {v0, p1}, Lacq;->a(Lacr;)V

    .line 261
    iget-object v0, p0, Lacl;->a:Lacj;

    invoke-static {v0, p1}, Lacj;->a(Lacj;Lacr;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 239
    check-cast p1, Lacr;

    invoke-virtual {p0, p1}, Lacl;->a(Lacr;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 242
    instance-of v0, p1, LQn;

    if-eqz v0, :cond_1

    .line 244
    iget-object v0, p0, Lacl;->a:Lacj;

    invoke-static {v0, v3}, Lacj;->a(Lacj;Lacr;)V

    .line 250
    :cond_0
    :goto_0
    return-void

    .line 245
    :cond_1
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    .line 246
    const-string v0, "SharingHelper"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 247
    iget-object v0, p0, Lacl;->a:Lacj;

    iget-object v0, v0, Lacj;->a:Lald;

    iget-object v1, p0, Lacl;->a:Lacj;

    invoke-static {v1}, Lacj;->a(Lacj;)Landroid/content/Context;

    move-result-object v1

    sget v2, Lxi;->sharing_error:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lald;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 248
    iget-object v0, p0, Lacl;->a:Lacj;

    invoke-static {v0, v3}, Lacj;->a(Lacj;Lacr;)V

    goto :goto_0
.end method
