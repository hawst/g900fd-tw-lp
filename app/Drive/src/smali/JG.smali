.class public abstract LJG;
.super Ljava/lang/Object;
.source "WrapperHelpCard.java"

# interfaces
.implements LJw;


# instance fields
.field private final a:LIU;


# direct methods
.method public constructor <init>(LIU;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, LJG;->a:LIU;

    .line 20
    new-instance v0, LJH;

    invoke-direct {v0, p0}, LJH;-><init>(LJG;)V

    invoke-virtual {p1, v0}, LIU;->a(Ljava/lang/Runnable;)V

    .line 27
    new-instance v0, LJI;

    invoke-direct {v0, p0}, LJI;-><init>(LJG;)V

    invoke-virtual {p1, v0}, LIU;->b(Ljava/lang/Runnable;)V

    .line 33
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, LJG;->a:LIU;

    invoke-virtual {v0, p1}, LIU;->a(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public a()LaFO;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, LJG;->a:LIU;

    invoke-virtual {v0}, LIU;->a()LaFO;

    move-result-object v0

    return-object v0
.end method

.method public a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, LJG;->a:LIU;

    invoke-virtual {v0}, LIU;->a()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, LJG;->a:LIU;

    invoke-virtual {v0, p1, p2}, LIU;->a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, LJG;->a:LIU;

    invoke-virtual {v0}, LIU;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract a()V
.end method

.method public a(LJx;)V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, LJG;->a:LIU;

    invoke-virtual {v0, p1}, LIU;->a(LJx;)V

    .line 53
    return-void
.end method

.method public a(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, LJG;->a:LIU;

    invoke-virtual {v0, p1}, LIU;->a(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method protected b()V
    .locals 0

    .prologue
    .line 70
    return-void
.end method
