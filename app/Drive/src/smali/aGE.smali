.class public LaGE;
.super Ljava/lang/Object;
.source "ForwardingEntryCursor.java"

# interfaces
.implements LaGA;


# instance fields
.field protected final a:LaGA;


# direct methods
.method protected constructor <init>(LaGA;)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGA;

    iput-object v0, p0, LaGE;->a:LaGA;

    .line 17
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->a()I

    move-result v0

    return v0
.end method

.method public a()J
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public a()LaGu;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->a()LaGu;

    move-result-object v0

    return-object v0
.end method

.method public a()LaGv;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->a()LaGv;

    move-result-object v0

    return-object v0
.end method

.method public a()LaGw;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->a()LaGw;

    move-result-object v0

    return-object v0
.end method

.method public a()Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    return-object v0
.end method

.method public a()Lcom/google/android/gms/drive/database/data/ResourceSpec;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->a()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(LaIm;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0, p1}, LaGA;->a(LaIm;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->a()V

    .line 178
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->a()Z

    move-result v0

    return v0
.end method

.method public a(I)Z
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0, p1}, LaGA;->a(I)Z

    move-result v0

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->b()I

    move-result v0

    return v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->b()J

    move-result-wide v0

    return-wide v0
.end method

.method public b()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->b()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->b()Z

    move-result v0

    return v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method public c()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->c()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->c()Z

    move-result v0

    return v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->d()Z

    move-result v0

    return v0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->e()J

    move-result-wide v0

    return-wide v0
.end method

.method public e()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 157
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->e()Z

    move-result v0

    return v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->f()J

    move-result-wide v0

    return-wide v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->f()Z

    move-result v0

    return v0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->g()J

    move-result-wide v0

    return-wide v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->g()Z

    move-result v0

    return v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->h()Z

    move-result v0

    return v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->i()Z

    move-result v0

    return v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->j()Z

    move-result v0

    return v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->k()Z

    move-result v0

    return v0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->l()Z

    move-result v0

    return v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, LaGE;->a:LaGA;

    invoke-interface {v0}, LaGA;->m()Z

    move-result v0

    return v0
.end method
