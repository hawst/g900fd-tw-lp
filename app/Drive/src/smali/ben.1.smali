.class public Lben;
.super Ljava/lang/Object;
.source "HttpResponseException.java"


# instance fields
.field a:I

.field a:Lbed;

.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Lbed;)V
    .locals 0

    .prologue
    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163
    invoke-virtual {p0, p1}, Lben;->a(I)Lben;

    .line 164
    invoke-virtual {p0, p2}, Lben;->b(Ljava/lang/String;)Lben;

    .line 165
    invoke-virtual {p0, p3}, Lben;->a(Lbed;)Lben;

    .line 166
    return-void
.end method

.method public constructor <init>(Lbel;)V
    .locals 3

    .prologue
    .line 172
    invoke-virtual {p1}, Lbel;->a()I

    move-result v0

    invoke-virtual {p1}, Lbel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbel;->a()Lbed;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lben;-><init>(ILjava/lang/String;Lbed;)V

    .line 175
    :try_start_0
    invoke-virtual {p1}, Lbel;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lben;->b:Ljava/lang/String;

    .line 176
    iget-object v0, p0, Lben;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 177
    const/4 v0, 0x0

    iput-object v0, p0, Lben;->b:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 184
    :cond_0
    :goto_0
    invoke-static {p1}, Lbem;->a(Lbel;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 185
    iget-object v1, p0, Lben;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 186
    sget-object v1, LbfS;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lben;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lben;->c:Ljava/lang/String;

    .line 189
    return-void

    .line 179
    :catch_0
    move-exception v0

    .line 181
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public a(I)Lben;
    .locals 1

    .prologue
    .line 223
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbfN;->a(Z)V

    .line 224
    iput p1, p0, Lben;->a:I

    .line 225
    return-object p0

    .line 223
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lbed;)Lben;
    .locals 1

    .prologue
    .line 260
    invoke-static {p1}, LbfN;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbed;

    iput-object v0, p0, Lben;->a:Lbed;

    .line 261
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lben;
    .locals 0

    .prologue
    .line 205
    iput-object p1, p0, Lben;->c:Ljava/lang/String;

    .line 206
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lben;
    .locals 0

    .prologue
    .line 242
    iput-object p1, p0, Lben;->a:Ljava/lang/String;

    .line 243
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lben;
    .locals 0

    .prologue
    .line 278
    iput-object p1, p0, Lben;->b:Ljava/lang/String;

    .line 279
    return-object p0
.end method
