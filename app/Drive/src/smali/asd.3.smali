.class public Lasd;
.super Ljava/lang/Object;
.source "Offer.java"


# instance fields
.field private a:LaFO;

.field private a:Ljava/lang/Boolean;

.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lasd;->a:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public static a(Landroid/os/Bundle;)Lasd;
    .locals 2

    .prologue
    .line 82
    new-instance v1, Lasd;

    const-string v0, "voucher"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lasd;-><init>(Ljava/lang/String;)V

    .line 83
    const-string v0, "account"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    iput-object v0, v1, Lasd;->a:LaFO;

    .line 84
    const-string v0, "granted"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "granted"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    iput-object v0, v1, Lasd;->a:Ljava/lang/Boolean;

    .line 85
    return-object v1

    .line 84
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()LaFO;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lasd;->a:LaFO;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 66
    if-nez p1, :cond_0

    .line 67
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 69
    :cond_0
    const-string v0, "voucher"

    iget-object v1, p0, Lasd;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v0, "account"

    iget-object v1, p0, Lasd;->a:LaFO;

    invoke-static {v1}, LaFO;->a(LaFO;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    invoke-virtual {p0}, Lasd;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    const-string v0, "granted"

    iget-object v1, p0, Lasd;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 74
    :cond_1
    return-object p1
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lasd;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(LaFO;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lasd;->a:LaFO;

    .line 42
    return-void
.end method

.method public a(Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lasd;->a:Ljava/lang/Boolean;

    .line 58
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lasd;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lasd;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0}, Lasd;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lasd;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lasd;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 90
    const-string v0, "Voucher %s, redeemed (%s) on account (%s)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lasd;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lasd;->a:Ljava/lang/Boolean;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lasd;->a:LaFO;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
