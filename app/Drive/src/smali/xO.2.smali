.class LxO;
.super Ljava/lang/Object;
.source "CatalogFeatureInfo.java"


# instance fields
.field private final a:Ljava/lang/String;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LxO;->a:Ljava/lang/String;

    .line 31
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, LxO;->a:Ljava/util/Map;

    .line 32
    return-void
.end method

.method private static a(Ljava/lang/String;)Lbtk;
    .locals 3

    .prologue
    .line 76
    new-instance v0, Lbtm;

    invoke-direct {v0}, Lbtm;-><init>()V

    .line 78
    :try_start_0
    invoke-virtual {v0, p0}, Lbtm;->a(Ljava/lang/String;)Lbth;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Lbth;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 80
    invoke-virtual {v0}, Lbth;->a()Lbtk;
    :try_end_0
    .catch Lbtl; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 85
    :goto_0
    return-object v0

    .line 82
    :catch_0
    move-exception v0

    .line 83
    const-string v1, "CatalogFeatureInfo"

    const-string v2, "Info file is not a valid JSON."

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 85
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Lbtk;)LxO;
    .locals 3

    .prologue
    .line 35
    invoke-static {}, LbfJ;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 37
    const-string v1, "promoKey"

    invoke-virtual {p1, v1}, Lbtk;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 38
    const-string v1, "promoKey"

    invoke-virtual {p1, v1}, Lbtk;->a(Ljava/lang/String;)Lbth;

    move-result-object v1

    invoke-virtual {v1}, Lbth;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 39
    const-string v2, "promoKey"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    :cond_0
    const-string v1, "packageNameToInstall"

    invoke-virtual {p1, v1}, Lbtk;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 43
    const-string v1, "packageNameToInstall"

    invoke-virtual {p1, v1}, Lbtk;->a(Ljava/lang/String;)Lbth;

    move-result-object v1

    invoke-virtual {v1}, Lbth;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 44
    const-string v2, "packageNameToInstall"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    :cond_1
    const-string v1, "createEnabled"

    invoke-virtual {p1, v1}, Lbtk;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 48
    const-string v1, "createEnabled"

    invoke-virtual {p1, v1}, Lbtk;->a(Ljava/lang/String;)Lbth;

    move-result-object v1

    invoke-virtual {v1}, Lbth;->a()Z

    move-result v1

    .line 49
    const-string v2, "createEnabled"

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    :cond_2
    new-instance v1, LxO;

    invoke-direct {v1, p0, v0}, LxO;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    return-object v1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)LxO;
    .locals 1

    .prologue
    .line 89
    invoke-static {p1}, LxO;->a(Ljava/lang/String;)Lbtk;

    move-result-object v0

    invoke-static {p0, v0}, LxO;->a(Ljava/lang/String;Lbtk;)LxO;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 96
    new-instance v2, Lbtk;

    invoke-direct {v2}, Lbtk;-><init>()V

    .line 98
    iget-object v0, p0, LxO;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 99
    iget-object v1, p0, LxO;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 100
    instance-of v4, v1, Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 101
    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lbtk;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 102
    :cond_1
    instance-of v4, v1, Ljava/lang/Boolean;

    if-eqz v4, :cond_0

    .line 103
    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v2, v0, v1}, Lbtk;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0

    .line 107
    :cond_2
    invoke-virtual {v2}, Lbtk;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, LxO;->a:Ljava/util/Map;

    const-string v1, "createEnabled"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, LxO;->a:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, LxO;->a:Ljava/util/Map;

    const-string v1, "promoKey"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, LxO;->a:Ljava/util/Map;

    const-string v1, "packageNameToInstall"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 128
    instance-of v1, p1, LxO;

    if-nez v1, :cond_1

    .line 134
    :cond_0
    :goto_0
    return v0

    .line 132
    :cond_1
    check-cast p1, LxO;

    .line 133
    iget-object v1, p0, LxO;->a:Ljava/lang/String;

    iget-object v2, p1, LxO;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LxO;->a:Ljava/util/Map;

    iget-object v2, p1, LxO;->a:Ljava/util/Map;

    .line 134
    invoke-interface {v1, v2}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 139
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LxO;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LxO;->a:Ljava/util/Map;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 144
    const-string v0, "CatalogFeatureInfo[%s, %s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LxO;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LxO;->a:Ljava/util/Map;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
