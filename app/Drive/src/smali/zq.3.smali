.class public final Lzq;
.super LyR;
.source "PreviewCard.java"


# annotations
.annotation runtime LaiC;
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final a:Landroid/view/View;

.field private a:LbsU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbsU",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/concurrent/Executor;

.field private final a:LqK;

.field private final a:LyY;

.field private a:Lzw;


# direct methods
.method public constructor <init>(Landroid/content/Context;LyY;LQU;LqK;)V
    .locals 3

    .prologue
    .line 97
    invoke-direct {p0}, LyR;-><init>()V

    .line 98
    iput-object p1, p0, Lzq;->a:Landroid/content/Context;

    .line 99
    iput-object p4, p0, Lzq;->a:LqK;

    .line 100
    iput-object p2, p0, Lzq;->a:LyY;

    .line 102
    new-instance v0, LalI;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, v1}, LalI;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lzq;->a:Ljava/util/concurrent/Executor;

    .line 103
    const-string v0, "layout_inflater"

    .line 104
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 105
    sget v1, Lxe;->detail_card_preview:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lzq;->a:Landroid/view/View;

    .line 106
    iget-object v0, p0, Lzq;->a:Landroid/view/View;

    invoke-virtual {p2, v0}, LyY;->b(Landroid/view/View;)V

    .line 108
    iget-object v0, p0, Lzq;->a:Landroid/view/View;

    sget v1, Lxc;->close_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 109
    if-eqz v0, :cond_0

    .line 110
    new-instance v1, Lzr;

    invoke-direct {v1, p0, p3}, Lzr;-><init>(Lzq;LQU;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    :cond_0
    iget-object v0, p0, Lzq;->a:Landroid/view/View;

    sget v1, Lxc;->thumbnail:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 119
    new-instance v1, Lzs;

    invoke-direct {v1, p0}, Lzs;-><init>(Lzq;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 144
    return-void
.end method

.method private a()Landroid/widget/ImageView;
    .locals 2

    .prologue
    .line 220
    iget-object v0, p0, Lzq;->a:Landroid/view/View;

    sget v1, Lxc;->thumbnail:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic a(Lzq;)LbsU;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lzq;->a:LbsU;

    return-object v0
.end method

.method static synthetic a(Lzq;LbsU;)LbsU;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lzq;->a:LbsU;

    return-object p1
.end method

.method static synthetic a(Lzq;)Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lzq;->a:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic a(Lzq;)LqK;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lzq;->a:LqK;

    return-object v0
.end method

.method static synthetic a(Lzq;)LyY;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lzq;->a:LyY;

    return-object v0
.end method

.method static synthetic a(Lzq;)Lzw;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lzq;->a:Lzw;

    return-object v0
.end method

.method private a(Landroid/widget/ImageView;)V
    .locals 3

    .prologue
    .line 224
    iget-object v0, p0, Lzq;->a:Lzw;

    iget-object v0, v0, Lzw;->a:LaGv;

    iget-object v1, p0, Lzq;->a:Lzw;

    iget-object v1, v1, Lzw;->a:Ljava/lang/String;

    iget-object v2, p0, Lzq;->a:Lzw;

    iget-boolean v2, v2, Lzw;->a:Z

    invoke-static {v0, v1, v2}, LaGt;->c(LaGv;Ljava/lang/String;Z)I

    move-result v0

    .line 227
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 228
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 229
    iget-object v1, p0, Lzq;->a:LyY;

    invoke-virtual {v1, v0}, LyY;->a(I)V

    .line 230
    return-void
.end method

.method static synthetic a(Lzq;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lzq;->b()V

    return-void
.end method

.method static synthetic a(Lzq;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lzq;->a(Landroid/widget/ImageView;)V

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lzq;->a:Lzw;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    invoke-direct {p0}, Lzq;->d()V

    .line 174
    invoke-direct {p0}, Lzq;->e()V

    .line 175
    invoke-direct {p0}, Lzq;->c()V

    .line 176
    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 180
    invoke-static {}, LamV;->a()V

    .line 181
    invoke-direct {p0}, Lzq;->a()Landroid/widget/ImageView;

    move-result-object v0

    .line 182
    new-instance v1, Lzu;

    invoke-direct {v1, p0}, Lzu;-><init>(Lzq;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 190
    iget-object v1, p0, Lzq;->a:Lzw;

    iget-object v1, v1, Lzw;->a:Lzy;

    .line 191
    invoke-virtual {v0}, Landroid/widget/ImageView;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/widget/ImageView;->getHeight()I

    move-result v3

    invoke-interface {v1, v2, v3}, Lzy;->a(II)LbsU;

    move-result-object v1

    iput-object v1, p0, Lzq;->a:LbsU;

    .line 192
    iget-object v1, p0, Lzq;->a:LbsU;

    new-instance v2, Lzv;

    invoke-direct {v2, p0, v0}, Lzv;-><init>(Lzq;Landroid/widget/ImageView;)V

    iget-object v0, p0, Lzq;->a:Ljava/util/concurrent/Executor;

    invoke-interface {v1, v2, v0}, LbsU;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 217
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 233
    iget-object v0, p0, Lzq;->a:Lzw;

    iget-object v0, v0, Lzw;->a:LaGv;

    iget-object v1, p0, Lzq;->a:Lzw;

    iget-object v1, v1, Lzw;->a:Ljava/lang/String;

    iget-object v2, p0, Lzq;->a:Lzw;

    iget-boolean v2, v2, Lzw;->a:Z

    invoke-static {v0, v1, v2}, LaGt;->b(LaGv;Ljava/lang/String;Z)I

    move-result v0

    .line 234
    iget-object v1, p0, Lzq;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 235
    iget-object v0, p0, Lzq;->a:Landroid/view/View;

    sget v2, Lxc;->icon:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 236
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 237
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 240
    iget-object v0, p0, Lzq;->a:Landroid/view/View;

    sget v1, Lxc;->title:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 241
    iget-object v1, p0, Lzq;->a:Lzw;

    iget-object v1, v1, Lzw;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 242
    iget-object v0, p0, Lzq;->a:LyY;

    iget-object v1, p0, Lzq;->a:Lzw;

    iget-object v1, v1, Lzw;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LyY;->a(Ljava/lang/String;)V

    .line 243
    return-void
.end method


# virtual methods
.method protected a()Landroid/view/View;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lzq;->a:Landroid/view/View;

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 252
    invoke-static {}, LamV;->a()V

    .line 253
    iget-object v0, p0, Lzq;->a:LbsU;

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lzq;->a:LbsU;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LbsU;->cancel(Z)Z

    .line 256
    :cond_0
    return-void
.end method

.method public a(LaGv;Ljava/lang/String;ZLjava/lang/String;Lzy;Lzx;)V
    .locals 8

    .prologue
    .line 161
    new-instance v0, Lzw;

    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LaGv;

    .line 164
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 165
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lzy;

    .line 166
    invoke-static {p6}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lzx;

    move-object v1, p0

    move-object v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v7}, Lzw;-><init>(Lzq;LaGv;Ljava/lang/String;ZLjava/lang/String;Lzy;Lzx;)V

    iput-object v0, p0, Lzq;->a:Lzw;

    .line 168
    invoke-direct {p0}, Lzq;->b()V

    .line 169
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 247
    const/4 v0, 0x1

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 263
    const/4 v0, 0x0

    return v0
.end method
