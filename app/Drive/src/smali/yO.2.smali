.class LyO;
.super Landroid/graphics/drawable/Drawable;
.source "ActivityEntryAdapter.java"


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private final a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbmF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 595
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 592
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LyO;->a:Landroid/graphics/Paint;

    .line 596
    iput-object p1, p0, LyO;->a:LbmF;

    .line 597
    return-void
.end method

.method private a(DLandroid/graphics/Rect;)V
    .locals 7

    .prologue
    .line 616
    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-double v0, v0

    mul-double/2addr v0, p1

    double-to-int v1, v0

    .line 617
    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-double v2, v0

    mul-double/2addr v2, p1

    double-to-int v2, v2

    .line 618
    iget-object v0, p0, LyO;->a:LbmF;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    iget v3, p3, Landroid/graphics/Rect;->left:I

    iget v4, p3, Landroid/graphics/Rect;->top:I

    iget v5, p3, Landroid/graphics/Rect;->left:I

    add-int/2addr v5, v1

    iget v6, p3, Landroid/graphics/Rect;->top:I

    add-int/2addr v6, v2

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 620
    iget-object v0, p0, LyO;->a:LbmF;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    iget v3, p3, Landroid/graphics/Rect;->right:I

    sub-int v1, v3, v1

    iget v3, p3, Landroid/graphics/Rect;->bottom:I

    sub-int v2, v3, v2

    iget v3, p3, Landroid/graphics/Rect;->right:I

    iget v4, p3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 622
    return-void
.end method

.method private a(ILandroid/graphics/Rect;)V
    .locals 9

    .prologue
    .line 628
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v0

    div-int v4, v0, p1

    .line 629
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v0

    div-int v5, v0, p1

    .line 631
    const/4 v1, 0x0

    .line 632
    iget v0, p2, Landroid/graphics/Rect;->left:I

    move v8, v0

    move v0, v1

    move v1, v8

    :goto_0
    iget v2, p2, Landroid/graphics/Rect;->right:I

    if-ge v1, v2, :cond_0

    .line 633
    iget v2, p2, Landroid/graphics/Rect;->top:I

    move v3, v2

    move v2, v0

    :goto_1
    iget v0, p2, Landroid/graphics/Rect;->bottom:I

    if-ge v3, v0, :cond_2

    .line 634
    iget-object v0, p0, LyO;->a:LbmF;

    invoke-virtual {v0, v2}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    add-int v6, v1, v4

    add-int v7, v3, v5

    invoke-virtual {v0, v1, v3, v6, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 635
    add-int/lit8 v2, v2, 0x1

    .line 636
    iget-object v0, p0, LyO;->a:LbmF;

    invoke-virtual {v0}, LbmF;->size()I

    move-result v0

    if-lt v2, v0, :cond_1

    .line 641
    :cond_0
    return-void

    .line 633
    :cond_1
    add-int v0, v3, v5

    move v3, v0

    goto :goto_1

    .line 632
    :cond_2
    add-int v0, v1, v4

    move v1, v0

    move v0, v2

    goto :goto_0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 645
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LyO;->a:LbmF;

    invoke-virtual {v0}, LbmF;->size()I

    move-result v0

    const/4 v2, 0x4

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 646
    iget-object v0, p0, LyO;->a:LbmF;

    invoke-virtual {v0, v1}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 645
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 648
    :cond_0
    return-void
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 652
    iget-object v0, p0, LyO;->a:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    return v0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 601
    iget-object v0, p0, LyO;->a:LbmF;

    invoke-virtual {v0}, LbmF;->size()I

    move-result v0

    .line 603
    if-ne v0, v1, :cond_0

    .line 604
    const-wide v0, 0x3fe54fdf3b645a1dL    # 0.666

    invoke-direct {p0, v0, v1, p1}, LyO;->a(DLandroid/graphics/Rect;)V

    .line 608
    :goto_0
    return-void

    .line 606
    :cond_0
    invoke-direct {p0, v1, p1}, LyO;->a(ILandroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public setAlpha(I)V
    .locals 1

    .prologue
    .line 657
    iget-object v0, p0, LyO;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 658
    invoke-virtual {p0}, LyO;->invalidateSelf()V

    .line 659
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 663
    iget-object v0, p0, LyO;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 664
    invoke-virtual {p0}, LyO;->invalidateSelf()V

    .line 665
    return-void
.end method
