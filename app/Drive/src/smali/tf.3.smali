.class public Ltf;
.super Ljava/lang/Object;
.source "DocumentPreviewActivity.java"


# instance fields
.field private a:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

.field final synthetic a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;)V
    .locals 0

    .prologue
    .line 999
    iput-object p1, p0, Ltf;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;Ltb;)V
    .locals 0

    .prologue
    .line 999
    invoke-direct {p0, p1}, Ltf;-><init>(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;)V

    return-void
.end method

.method private a()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 1004
    invoke-static {}, LakQ;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1015
    :goto_0
    return-void

    .line 1007
    :cond_0
    new-instance v0, Ltg;

    invoke-direct {v0, p0}, Ltg;-><init>(Ltf;)V

    iput-object v0, p0, Ltf;->a:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    .line 1013
    iget-object v0, p0, Ltf;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    invoke-static {v0}, LUs;->a(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    iget-object v1, p0, Ltf;->a:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    .line 1014
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->addAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z

    goto :goto_0
.end method

.method public static synthetic a(Ltf;)V
    .locals 0

    .prologue
    .line 999
    invoke-direct {p0}, Ltf;->a()V

    return-void
.end method

.method private b()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 1019
    invoke-static {}, LakQ;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltf;->a:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    if-nez v0, :cond_1

    .line 1024
    :cond_0
    :goto_0
    return-void

    .line 1022
    :cond_1
    iget-object v0, p0, Ltf;->a:Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    invoke-static {v0}, LUs;->a(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    iget-object v1, p0, Ltf;->a:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    .line 1023
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->removeAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z

    goto :goto_0
.end method

.method public static synthetic b(Ltf;)V
    .locals 0

    .prologue
    .line 999
    invoke-direct {p0}, Ltf;->b()V

    return-void
.end method
