.class LasW;
.super Ljava/lang/Object;
.source "StoryShops.java"

# interfaces
.implements LasP;


# instance fields
.field protected final a:Landroid/net/Uri;

.field final synthetic a:LasR;

.field protected final b:Landroid/net/Uri;


# direct methods
.method constructor <init>(LasR;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 154
    iput-object p1, p0, LasW;->a:LasR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LasW;->b:Landroid/net/Uri;

    .line 156
    iget-object v0, p0, LasW;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 157
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sub-int v0, v1, v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p2, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LasW;->a:Landroid/net/Uri;

    .line 158
    return-void
.end method


# virtual methods
.method public a()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, LasW;->a:Landroid/net/Uri;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, LasW;->a:LasR;

    invoke-static {v0}, LasR;->a(LasR;)LTl;

    move-result-object v0

    iget-object v1, p0, LasW;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LTl;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
