.class public abstract enum Lvn;
.super Ljava/lang/Enum;
.source "SendTextToClipboardActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lvn;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lvn;

.field private static final synthetic a:[Lvn;

.field public static final enum b:Lvn;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38
    new-instance v0, Lvo;

    const-string v1, "PRE_HONEYCOMB"

    invoke-direct {v0, v1, v2}, Lvo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lvn;->a:Lvn;

    .line 64
    new-instance v0, Lvp;

    const-string v1, "HONEYCOMB"

    invoke-direct {v0, v1, v3}, Lvp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lvn;->b:Lvn;

    .line 37
    const/4 v0, 0x2

    new-array v0, v0, [Lvn;

    sget-object v1, Lvn;->a:Lvn;

    aput-object v1, v0, v2

    sget-object v1, Lvn;->b:Lvn;

    aput-object v1, v0, v3

    sput-object v0, Lvn;->a:[Lvn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILvm;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lvn;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a()Lvn;
    .locals 1

    .prologue
    .line 93
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v0}, Lvn;->a(I)Lvn;

    move-result-object v0

    return-object v0
.end method

.method public static a(I)Lvn;
    .locals 1

    .prologue
    .line 98
    const/16 v0, 0xb

    if-lt p0, v0, :cond_0

    .line 99
    sget-object v0, Lvn;->b:Lvn;

    .line 101
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lvn;->a:Lvn;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lvn;
    .locals 1

    .prologue
    .line 37
    const-class v0, Lvn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lvn;

    return-object v0
.end method

.method public static values()[Lvn;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lvn;->a:[Lvn;

    invoke-virtual {v0}, [Lvn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lvn;

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/text/ClipboardManager;Ljava/lang/String;Landroid/content/Context;Landroid/net/Uri;)V
.end method

.method public abstract a(Landroid/text/ClipboardManager;Ljava/lang/String;Ljava/lang/String;)V
.end method
