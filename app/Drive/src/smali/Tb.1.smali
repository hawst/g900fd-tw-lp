.class public LTb;
.super Ljava/lang/Object;
.source "AccountAuthenticatorImpl.java"

# interfaces
.implements LSZ;


# annotations
.annotation runtime Lbxz;
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final a:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "LaFO;",
            "Landroid/accounts/Account;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "LTa;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Laja;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, LTb;->a:Ljava/util/concurrent/ConcurrentHashMap;

    .line 37
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, LTb;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 44
    invoke-virtual {p1}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LTb;->a:Landroid/content/Context;

    .line 45
    return-void
.end method

.method private a(LaFO;)Landroid/accounts/Account;
    .locals 5

    .prologue
    .line 98
    iget-object v0, p0, LTb;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 99
    if-eqz v0, :cond_0

    .line 108
    :goto_0
    return-object v0

    .line 102
    :cond_0
    invoke-direct {p0}, LTb;->a()Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 103
    invoke-virtual {p1, v0}, LaFO;->a(Landroid/accounts/Account;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 104
    iget-object v1, p0, LTb;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 102
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 108
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a()Landroid/accounts/AccountManager;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, LTb;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(LaFO;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 61
    invoke-direct {p0, p1}, LTb;->a(LaFO;)Landroid/accounts/Account;

    move-result-object v1

    .line 62
    if-nez v1, :cond_0

    .line 64
    new-instance v0, Landroid/accounts/AuthenticatorException;

    invoke-direct {v0}, Landroid/accounts/AuthenticatorException;-><init>()V

    throw v0

    .line 68
    :cond_0
    invoke-direct {p0}, LTb;->a()Landroid/accounts/AccountManager;

    move-result-object v0

    iget-object v2, p0, LTb;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArraySet;->isEmpty()Z

    move-result v4

    move-object v2, p2

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;ZLandroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v0

    .line 69
    invoke-interface {v0}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 71
    const-string v1, "authtoken"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 72
    if-nez v1, :cond_2

    .line 73
    const-string v1, "intent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 74
    iget-object v1, p0, LTb;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LTa;

    .line 75
    invoke-interface {v1, p1, v0}, LTa;->a(LaFO;Landroid/content/Intent;)V

    goto :goto_0

    .line 77
    :cond_1
    new-instance v0, LTr;

    invoke-direct {v0}, LTr;-><init>()V

    throw v0

    .line 79
    :cond_2
    return-object v1
.end method

.method public a()V
    .locals 0

    .prologue
    .line 112
    return-void
.end method

.method public a(LTa;)V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, LTb;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    .line 50
    return-void
.end method

.method public a(LaFO;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 90
    invoke-direct {p0}, LTb;->a()Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-direct {p0, p1}, LTb;->a(LaFO;)Landroid/accounts/Account;

    move-result-object v1

    iget-object v1, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 115
    return-void
.end method

.method public b(LTa;)V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, LTb;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->remove(Ljava/lang/Object;)Z

    .line 55
    return-void
.end method
