.class public final Lws;
.super Ljava/lang/Object;
.source "SearchCriterion.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;
    .locals 4

    .prologue
    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 75
    new-instance v1, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;

    invoke-static {v0, v2, v3}, Laay;->a(Ljava/lang/String;J)Laay;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;-><init>(Laay;)V

    return-object v1
.end method

.method public a(I)[Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;
    .locals 1

    .prologue
    .line 80
    new-array v0, p1, [Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0, p1}, Lws;->a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0, p1}, Lws;->a(I)[Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;

    move-result-object v0

    return-object v0
.end method
