.class public LagO;
.super Ljava/lang/Object;
.source "LegacySyncManager.java"

# interfaces
.implements Lahh;


# annotations
.annotation runtime Lbxz;
.end annotation


# instance fields
.field private final a:LQB;

.field private final a:LQr;

.field private final a:LSF;

.field private final a:LaGg;

.field private final a:LaKR;

.field private final a:Ladi;

.field private final a:LafE;

.field private final a:LafG;

.field private final a:LagS;

.field private final a:LagW;

.field private final a:LahB;

.field private final a:LahK;

.field private final a:LahT;

.field private final a:Lahf;

.field private final a:Lahj;

.field private final a:Lair;

.field private final a:LajO;

.field private final a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LamL;

.field private final a:Lamh;

.field private final a:Lamn;

.field private final a:Landroid/app/NotificationManager;

.field a:LbiP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiP",
            "<",
            "LagR;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LaFO;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Landroid/accounts/Account;",
            "Ljava/lang/Thread;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LpW;

.field private final a:LqK;

.field private final a:LtK;

.field a:LvO;

.field private final a:Lvv;

.field private final a:Lyq;

.field private final b:LafE;

.field b:LbiP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiP",
            "<",
            "LafF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LagS;Lair;LaGg;Ladi;LagW;LaKR;LSF;LQB;Lvv;LQr;LafE;LqK;LpW;Lamn;LamL;LajO;LahB;LahT;LahK;Lahf;Lahj;Lyq;LtK;Laja;LafE;Lamh;LafG;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LagS;",
            "Lair;",
            "LaGg;",
            "Ladi;",
            "LagW;",
            "LaKR;",
            "LSF;",
            "LQB;",
            "Lvv;",
            "LQr;",
            "LafE;",
            "LqK;",
            "LpW;",
            "Lamn;",
            "LamL;",
            "LajO;",
            "LahB;",
            "LahT;",
            "LahK;",
            "Lahf;",
            "Lahj;",
            "Lyq;",
            "LtK;",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LafE;",
            "Lamh;",
            "LafG;",
            ")V"
        }
    .end annotation

    .prologue
    .line 266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 215
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 216
    invoke-static {v1}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, LagO;->a:Ljava/util/Set;

    .line 227
    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v1, p0, LagO;->a:Ljava/util/concurrent/ConcurrentHashMap;

    .line 267
    iput-object p1, p0, LagO;->a:LagS;

    .line 268
    iput-object p3, p0, LagO;->a:LaGg;

    .line 269
    iput-object p2, p0, LagO;->a:Lair;

    .line 270
    iput-object p4, p0, LagO;->a:Ladi;

    .line 271
    iput-object p5, p0, LagO;->a:LagW;

    .line 272
    iput-object p6, p0, LagO;->a:LaKR;

    .line 273
    iput-object p7, p0, LagO;->a:LSF;

    .line 274
    iput-object p8, p0, LagO;->a:LQB;

    .line 275
    iput-object p9, p0, LagO;->a:Lvv;

    .line 276
    iput-object p10, p0, LagO;->a:LQr;

    .line 277
    iput-object p11, p0, LagO;->a:LafE;

    .line 278
    iput-object p12, p0, LagO;->a:LqK;

    .line 279
    move-object/from16 v0, p13

    iput-object v0, p0, LagO;->a:LpW;

    .line 280
    move-object/from16 v0, p14

    iput-object v0, p0, LagO;->a:Lamn;

    .line 281
    move-object/from16 v0, p15

    iput-object v0, p0, LagO;->a:LamL;

    .line 282
    move-object/from16 v0, p16

    iput-object v0, p0, LagO;->a:LajO;

    .line 283
    move-object/from16 v0, p17

    iput-object v0, p0, LagO;->a:LahB;

    .line 284
    move-object/from16 v0, p18

    iput-object v0, p0, LagO;->a:LahT;

    .line 285
    move-object/from16 v0, p19

    iput-object v0, p0, LagO;->a:LahK;

    .line 286
    move-object/from16 v0, p20

    iput-object v0, p0, LagO;->a:Lahf;

    .line 287
    move-object/from16 v0, p21

    iput-object v0, p0, LagO;->a:Lahj;

    .line 288
    move-object/from16 v0, p22

    iput-object v0, p0, LagO;->a:Lyq;

    .line 289
    move-object/from16 v0, p23

    iput-object v0, p0, LagO;->a:LtK;

    .line 290
    move-object/from16 v0, p24

    iput-object v0, p0, LagO;->a:Laja;

    .line 292
    invoke-virtual/range {p24 .. p24}, Laja;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const-string v2, "notification"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    iput-object v1, p0, LagO;->a:Landroid/app/NotificationManager;

    .line 293
    move-object/from16 v0, p25

    iput-object v0, p0, LagO;->b:LafE;

    .line 294
    move-object/from16 v0, p26

    iput-object v0, p0, LagO;->a:Lamh;

    .line 295
    move-object/from16 v0, p27

    iput-object v0, p0, LagO;->a:LafG;

    .line 296
    return-void
.end method

.method private a(LaFO;)J
    .locals 4

    .prologue
    .line 747
    iget-object v0, p0, LagO;->a:LpW;

    invoke-interface {v0, p1}, LpW;->a(LaFO;)LpU;

    move-result-object v0

    .line 748
    const-string v1, "lastContentSyncMilliseconds_v2"

    invoke-interface {v0, v1}, LpU;->a(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, LbiT;->b(Z)V

    .line 749
    const-string v1, "lastContentSyncMilliseconds_v2"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, LpU;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic a(LagO;)Laja;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, LagO;->a:Laja;

    return-object v0
.end method

.method static synthetic a(LagO;)Lamh;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, LagO;->a:Lamh;

    return-object v0
.end method

.method static synthetic a(LagO;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, LagO;->a:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method private a(LaFO;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 786
    iget-object v0, p0, LagO;->a:LamL;

    invoke-interface {v0}, LamL;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LagO;->a:LaKR;

    invoke-interface {v0}, LaKR;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 787
    iget-object v0, p0, LagO;->a:LaGg;

    invoke-interface {v0, p1}, LaGg;->a(LaFO;)LaFM;

    move-result-object v0

    .line 788
    iget-object v3, p0, LagO;->a:LaGg;

    .line 789
    invoke-static {}, LaER;->a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v4

    invoke-interface {v3, v0, v4}, LaGg;->a(LaFM;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Ljava/util/Set;

    move-result-object v0

    .line 790
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 791
    iget-object v4, p0, LagO;->a:LaGg;

    invoke-interface {v4, v0}, LaGg;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;

    move-result-object v4

    .line 792
    if-eqz v4, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, LbiT;->b(Z)V

    .line 793
    iget-object v0, p0, LagO;->a:LagW;

    invoke-virtual {v0, v4}, LagW;->a(LaGd;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LagO;->a:LagW;

    .line 794
    invoke-virtual {v0, v4}, LagW;->a(LaGo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 795
    :cond_1
    if-nez p2, :cond_3

    iget-object v0, p0, LagO;->a:Ladi;

    sget-object v5, LacY;->a:LacY;

    .line 796
    invoke-interface {v0, v4, v5}, Ladi;->d(LaGo;LacY;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 797
    :goto_2
    if-eqz v0, :cond_4

    .line 798
    iget-object v0, p0, LagO;->a:LahT;

    .line 799
    invoke-virtual {v4}, LaGb;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v4

    .line 798
    invoke-interface {v0, v4}, LahT;->e(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 792
    goto :goto_1

    :cond_3
    move v0, v2

    .line 796
    goto :goto_2

    .line 801
    :cond_4
    iget-object v0, p0, LagO;->a:LahT;

    .line 802
    invoke-virtual {v4}, LaGb;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v4

    .line 801
    invoke-interface {v0, v4}, LahT;->f(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    goto :goto_0

    .line 807
    :cond_5
    return-void
.end method

.method static synthetic a(LagO;Landroid/accounts/Account;Ljava/lang/String;Landroid/content/SyncResult;)V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0, p1, p2, p3}, LagO;->a(Landroid/accounts/Account;Ljava/lang/String;Landroid/content/SyncResult;)V

    return-void
.end method

.method private a(Landroid/accounts/Account;Ljava/lang/String;Landroid/content/SyncResult;)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const-wide/16 v10, 0x1

    .line 455
    iget-object v0, p0, LagO;->a:LqK;

    invoke-virtual {v0}, LqK;->a()V

    .line 456
    iget-object v0, p0, LagO;->a:LqK;

    invoke-virtual {v0, p0}, LqK;->a(Ljava/lang/Object;)V

    .line 460
    :try_start_0
    invoke-static {p1, p2}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    .line 461
    if-eqz v0, :cond_0

    iget-object v0, p0, LagO;->a:LtK;

    sget-object v3, Lry;->M:Lry;

    invoke-interface {v0, v3}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v3, v1

    .line 462
    :goto_0
    iget-object v0, p0, LagO;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0, p1, v3}, Lahe;->a(Landroid/content/Context;Landroid/accounts/Account;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 465
    :try_start_1
    iget-object v0, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v4, 0x0

    iput-wide v4, v0, Landroid/content/SyncStats;->numEntries:J

    .line 466
    iget-object v0, p0, LagO;->a:Landroid/app/NotificationManager;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/app/NotificationManager;->cancel(I)V

    .line 467
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, LagO;->b(LaFO;Landroid/content/SyncResult;)V

    .line 469
    iget-object v0, p0, LagO;->a:LqK;

    const-string v3, "sync"

    const-string v4, "entriesChanged"

    const/4 v5, 0x0

    iget-object v6, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v6, Landroid/content/SyncStats;->numEntries:J

    .line 470
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 469
    invoke-virtual {v0, v3, v4, v5, v6}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_1
    .catch LQo; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_3
    .catch LbwW; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catch LbwO; {:try_start_1 .. :try_end_1} :catch_6
    .catch LTr; {:try_start_1 .. :try_end_1} :catch_7
    .catch Lbxk; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 472
    :try_start_2
    iget-object v0, p0, LagO;->a:LqK;

    const-string v3, "sync"

    const-string v4, "entriesTotal"

    const/4 v5, 0x0

    iget-object v6, p0, LagO;->a:LaGg;

    iget-object v7, p0, LagO;->a:LvO;

    iget-object v8, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 474
    invoke-static {v8}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v8

    invoke-interface {v7, v8}, LvO;->a(LaFO;)LvN;

    move-result-object v7

    invoke-virtual {v7}, LvN;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v7

    .line 473
    invoke-interface {v6, v7}, LaGg;->a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 472
    invoke-virtual {v0, v3, v4, v5, v6}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_2
    .catch LaGT; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_1
    .catch LQo; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_3
    .catch LbwW; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catch LbwO; {:try_start_2 .. :try_end_2} :catch_6
    .catch LTr; {:try_start_2 .. :try_end_2} :catch_7
    .catch Lbxk; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    move v0, v1

    .line 523
    :goto_2
    if-eqz v0, :cond_1

    .line 524
    iget-object v0, p0, LagO;->a:LqK;

    const-string v1, "metadataSyncFinished"

    const-string v2, "Success"

    invoke-virtual {v0, p0, v1, v2}, LqK;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    :goto_3
    iget-object v0, p0, LagO;->a:LqK;

    invoke-virtual {v0}, LqK;->b()V

    .line 532
    return-void

    :cond_0
    move v3, v2

    .line 461
    goto :goto_0

    .line 475
    :catch_0
    move-exception v0

    .line 477
    :try_start_3
    const-string v3, "SyncManager"

    const-string v4, "ModelLoaderException"

    invoke-static {v3, v0, v4}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3 .. :try_end_3} :catch_1
    .catch LQo; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_3
    .catch LbwW; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catch LbwO; {:try_start_3 .. :try_end_3} :catch_6
    .catch LTr; {:try_start_3 .. :try_end_3} :catch_7
    .catch Lbxk; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 480
    :catch_1
    move-exception v0

    .line 481
    :try_start_4
    iget-object v1, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v4, v10

    iput-wide v4, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 482
    const-string v1, "SyncManager"

    const-string v3, "AuthenticatorException"

    invoke-static {v1, v0, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 483
    iget-object v0, p0, LagO;->a:LqK;

    const-string v1, "sync"

    const-string v3, "error"

    const-string v4, "AuthenticatorException"

    invoke-virtual {v0, v1, v3, v4}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 521
    goto :goto_2

    .line 485
    :catch_2
    move-exception v0

    .line 486
    iget-object v1, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v1, Landroid/content/SyncStats;->numIoExceptions:J

    add-long/2addr v4, v10

    iput-wide v4, v1, Landroid/content/SyncStats;->numIoExceptions:J

    .line 488
    invoke-virtual {v0}, LQo;->getMessage()Ljava/lang/String;

    move-result-object v1

    sget v3, Lxi;->ouch_msg_sync_error:I

    invoke-direct {p0, v1, v0, v3}, LagO;->a(Ljava/lang/String;Ljava/lang/Exception;I)V

    .line 489
    const-string v1, "SyncManager"

    const-string v3, "SyncException"

    invoke-static {v1, v0, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 490
    iget-object v0, p0, LagO;->a:LqK;

    const-string v1, "sync"

    const-string v3, "error"

    const-string v4, "SyncException"

    invoke-virtual {v0, v1, v3, v4}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 521
    goto :goto_2

    .line 492
    :catch_3
    move-exception v0

    .line 493
    const-string v1, "SyncManager"

    const-string v3, "Interrupted"

    invoke-static {v1, v0, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 494
    iget-object v0, p0, LagO;->a:LqK;

    const-string v1, "sync"

    const-string v3, "error"

    const-string v4, "InterruptedException"

    invoke-virtual {v0, v1, v3, v4}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 521
    goto :goto_2

    .line 496
    :catch_4
    move-exception v0

    .line 497
    iget-object v1, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v1, Landroid/content/SyncStats;->numIoExceptions:J

    add-long/2addr v4, v10

    iput-wide v4, v1, Landroid/content/SyncStats;->numIoExceptions:J

    .line 498
    const-string v1, "SyncManager"

    const-string v3, "Network error"

    invoke-static {v1, v0, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 499
    iget-object v0, p0, LagO;->a:LqK;

    const-string v1, "sync"

    const-string v3, "error"

    const-string v4, "HttpException"

    invoke-virtual {v0, v1, v3, v4}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 521
    goto/16 :goto_2

    .line 501
    :catch_5
    move-exception v0

    .line 502
    iget-object v1, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v1, Landroid/content/SyncStats;->numIoExceptions:J

    add-long/2addr v4, v10

    iput-wide v4, v1, Landroid/content/SyncStats;->numIoExceptions:J

    .line 503
    const-string v1, "SyncManager"

    const-string v3, "Network error"

    invoke-static {v1, v0, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 504
    iget-object v0, p0, LagO;->a:LqK;

    const-string v1, "sync"

    const-string v3, "error"

    const-string v4, "IOException"

    invoke-virtual {v0, v1, v3, v4}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 521
    goto/16 :goto_2

    .line 506
    :catch_6
    move-exception v0

    .line 507
    iget-object v1, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v4, v10

    iput-wide v4, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 508
    const-string v1, "SyncManager"

    const-string v3, "AuthenticationException"

    invoke-static {v1, v0, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 509
    iget-object v0, p0, LagO;->a:LqK;

    const-string v1, "sync"

    const-string v3, "error"

    const-string v4, "AuthenticationException"

    invoke-virtual {v0, v1, v3, v4}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 521
    goto/16 :goto_2

    .line 511
    :catch_7
    move-exception v0

    .line 512
    iget-object v1, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v4, v10

    iput-wide v4, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 513
    const-string v1, "SyncManager"

    const-string v3, "Invalid credentials"

    invoke-static {v1, v0, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 514
    iget-object v0, p0, LagO;->a:LqK;

    const-string v1, "sync"

    const-string v3, "error"

    const-string v4, "InvalidCredentialsException"

    invoke-virtual {v0, v1, v3, v4}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 521
    goto/16 :goto_2

    .line 516
    :catch_8
    move-exception v0

    .line 517
    iget-object v1, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v1, Landroid/content/SyncStats;->numParseExceptions:J

    add-long/2addr v4, v10

    iput-wide v4, v1, Landroid/content/SyncStats;->numParseExceptions:J

    .line 518
    const-string v1, "SyncManager"

    const-string v3, "Error parsing gdata XML"

    invoke-static {v1, v0, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 519
    iget-object v0, p0, LagO;->a:LqK;

    const-string v1, "sync"

    const-string v3, "error"

    const-string v4, "ParseException"

    invoke-virtual {v0, v1, v3, v4}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v0, v2

    goto/16 :goto_2

    .line 527
    :cond_1
    iget-object v0, p0, LagO;->a:LqK;

    const-string v1, "metadataSyncFinished"

    const-string v2, "Error"

    invoke-virtual {v0, p0, v1, v2}, LqK;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 523
    :catchall_0
    move-exception v0

    .line 527
    iget-object v1, p0, LagO;->a:LqK;

    const-string v2, "metadataSyncFinished"

    const-string v3, "Error"

    invoke-virtual {v1, p0, v2, v3}, LqK;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    iget-object v1, p0, LagO;->a:LqK;

    invoke-virtual {v1}, LqK;->b()V

    throw v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/Exception;I)V
    .locals 5

    .prologue
    .line 536
    :try_start_0
    new-instance v1, Landroid/app/Notification;

    const v0, 0x1080078

    .line 537
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v1, v0, p1, v2, v3}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 538
    const/4 v0, 0x1

    iput v0, v1, Landroid/app/Notification;->flags:I

    .line 540
    iget-object v0, p0, LagO;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 541
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.BUG_REPORT"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 542
    const-class v3, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 543
    const-string v3, "notification_message"

    invoke-virtual {v2, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 545
    const-string v3, "stack_trace"

    .line 546
    invoke-static {p2}, LalV;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    .line 545
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 548
    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    invoke-static {v0, v3, v2, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 551
    sget v3, Lxi;->sync_failed:I

    .line 552
    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 551
    invoke-virtual {v1, v0, v3, p1, v2}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 554
    iget-object v0, p0, LagO;->a:Landroid/app/NotificationManager;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 558
    return-void

    .line 555
    :catch_0
    move-exception v0

    .line 556
    throw v0
.end method

.method private a(LaFO;Landroid/content/SyncResult;)Z
    .locals 2

    .prologue
    .line 663
    invoke-direct {p0, p1}, LagO;->b(LaFO;)V

    .line 666
    iget-object v0, p0, LagO;->a:Lvv;

    invoke-interface {v0}, Lvv;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 667
    new-instance v0, LagU;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LagU;-><init>(LagP;)V

    throw v0

    .line 670
    :cond_0
    invoke-virtual {p0}, LagO;->a()V

    .line 671
    const/4 v0, 0x0

    .line 672
    iget-object v1, p0, LagO;->a:LbiP;

    invoke-virtual {v1}, LbiP;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 673
    iget-object v0, p0, LagO;->a:LbiP;

    .line 674
    invoke-virtual {v0}, LbiP;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LagR;

    invoke-interface {v0, p1, p2}, LagR;->a(LaFO;Landroid/content/SyncResult;)Z

    move-result v0

    move v1, v0

    .line 677
    :goto_0
    iget-object v0, p0, LagO;->b:LbiP;

    invoke-virtual {v0}, LbiP;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 678
    iget-object v0, p0, LagO;->b:LbiP;

    invoke-virtual {v0}, LbiP;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LafF;

    invoke-interface {v0, p1, p2}, LafF;->a(LaFO;Landroid/content/SyncResult;)V

    .line 682
    :cond_1
    invoke-direct {p0}, LagO;->b()V

    .line 684
    return v1

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method private a(Landroid/content/SyncResult;Laiq;)Z
    .locals 10

    .prologue
    const/4 v5, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 891
    .line 893
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 895
    :try_start_0
    iget-object v0, p0, LagO;->a:LagS;

    invoke-interface {v0}, LagS;->a()Lagx;

    move-result-object v0

    .line 896
    invoke-interface {p2, v0, p1}, Laiq;->a(Lagx;Landroid/content/SyncResult;)V

    .line 897
    invoke-interface {v0, p1}, Lagx;->a(Landroid/content/SyncResult;)V

    .line 899
    const/4 v0, 0x1

    .line 901
    invoke-interface {p2, p1, v0}, Laiq;->a(Landroid/content/SyncResult;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 903
    const-string v0, "SyncManager"

    const-string v1, "Time taken to sync doc list only %s result %s"

    new-array v4, v5, [Ljava/lang/Object;

    .line 904
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v2, v6, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v9

    aput-object p1, v4, v8

    .line 903
    invoke-static {v0, v1, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 906
    return v8

    .line 903
    :catchall_0
    move-exception v0

    const-string v1, "SyncManager"

    const-string v4, "Time taken to sync doc list only %s result %s"

    new-array v5, v5, [Ljava/lang/Object;

    .line 904
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v2, v6, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v9

    aput-object p1, v5, v8

    .line 903
    invoke-static {v1, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    throw v0
.end method

.method private a(ZLaFM;Landroid/content/SyncResult;)Z
    .locals 10

    .prologue
    .line 822
    invoke-virtual {p2}, LaFM;->a()LaFO;

    move-result-object v6

    .line 823
    const-string v0, "SyncManager"

    const-string v1, "Starting sync for %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v6, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 825
    iget-object v0, p0, LagO;->a:LQr;

    const-string v1, "changelogSyncLimit"

    const/16 v2, 0x9c4

    .line 826
    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    .line 827
    iget-object v1, p0, LagO;->a:LaGg;

    invoke-interface {v1, v6}, LaGg;->a(LaFO;)LaFQ;

    move-result-object v1

    .line 828
    iget-object v2, p0, LagO;->a:LafE;

    .line 829
    invoke-virtual {v1}, LaFQ;->a()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v4, v0, 0x1

    .line 828
    invoke-virtual {v2, v6, v3, v4}, LafE;->a(LaFO;II)LaJQ;

    move-result-object v2

    .line 833
    invoke-interface {v2}, LaJQ;->a()Z

    move-result v3

    if-nez v3, :cond_1

    .line 834
    iget-object v0, p0, LagO;->a:LqK;

    const-string v1, "sync"

    const-string v2, "error"

    const-string v3, "Error fetching remainingChangestamps"

    invoke-virtual {v0, v1, v2, v3}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 836
    const/4 v0, 0x0

    .line 881
    :cond_0
    :goto_0
    return v0

    .line 838
    :cond_1
    invoke-virtual {v1}, LaFQ;->a()Z

    move-result v1

    .line 840
    invoke-interface {v2}, LaJQ;->d()J

    move-result-wide v8

    .line 841
    if-nez v1, :cond_2

    int-to-long v0, v0

    cmp-long v0, v8, v0

    if-lez v0, :cond_4

    :cond_2
    const/4 v4, 0x1

    .line 842
    :goto_1
    if-nez v4, :cond_3

    if-nez p1, :cond_5

    :cond_3
    const/4 v0, 0x1

    .line 846
    :goto_2
    const-string v1, "SyncManager"

    const-string v3, "performSync[account=%s, remainingChangestamps=%s, fullSync=%s]"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    const/4 v7, 0x1

    .line 847
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v5, v7

    const/4 v7, 0x2

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v5, v7

    .line 846
    invoke-static {v1, v3, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 849
    if-eqz v0, :cond_6

    .line 850
    iget-object v0, p0, LagO;->a:LQr;

    const-string v1, "syncstarMaxFeedsToRetrieve"

    const/16 v3, 0x14

    .line 851
    invoke-interface {v0, v1, v3}, LQr;->a(Ljava/lang/String;I)I

    move-result v3

    .line 855
    invoke-interface {v2}, LaJQ;->c()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v5

    .line 856
    iget-object v0, p0, LagO;->a:Lair;

    iget-object v2, p0, LagO;->a:LaGg;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lair;->a(LaFM;LaGg;IZI)Lain;

    move-result-object v0

    .line 858
    invoke-direct {p0, p3, v0}, LagO;->a(Landroid/content/SyncResult;Laiq;)Z

    move-result v0

    .line 869
    :goto_3
    const-string v1, "SyncManager"

    const-string v2, "sync finished[account=%s, success=%s]"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v6, v3, v4

    const/4 v4, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 870
    if-eqz v0, :cond_0

    .line 871
    iget-object v1, p0, LagO;->a:LaGg;

    invoke-interface {v1}, LaGg;->a()V

    .line 873
    :try_start_0
    iget-object v1, p0, LagO;->a:LaGg;

    invoke-virtual {p2}, LaFM;->a()LaFO;

    move-result-object v2

    invoke-interface {v1, v2}, LaGg;->a(LaFO;)LaFQ;

    move-result-object v1

    .line 874
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, LaFQ;->a(Ljava/util/Date;)V

    .line 875
    invoke-virtual {v1}, LaFQ;->e()V

    .line 876
    iget-object v1, p0, LagO;->a:LaGg;

    invoke-interface {v1}, LaGg;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 878
    iget-object v1, p0, LagO;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    goto/16 :goto_0

    .line 841
    :cond_4
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 842
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 860
    :cond_6
    invoke-interface {v2}, LaJQ;->d()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_7

    .line 864
    iget-object v0, p0, LagO;->a:Lair;

    iget-object v1, p0, LagO;->a:LaGg;

    invoke-virtual {v0, p2, v1}, Lair;->a(LaFM;LaGM;)Laim;

    move-result-object v0

    .line 865
    invoke-direct {p0, p3, v0}, LagO;->a(Landroid/content/SyncResult;Laiq;)Z

    move-result v0

    goto :goto_3

    .line 867
    :cond_7
    const/4 v0, 0x1

    goto :goto_3

    .line 878
    :catchall_0
    move-exception v0

    iget-object v1, p0, LagO;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    throw v0
.end method

.method private b()V
    .locals 5

    .prologue
    .line 691
    iget-object v0, p0, LagO;->a:LaGg;

    invoke-interface {v0}, LaGg;->a()V

    .line 693
    :try_start_0
    iget-object v0, p0, LagO;->a:LaGg;

    invoke-interface {v0}, LaGg;->a()Ljava/util/Set;

    move-result-object v0

    .line 694
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFO;

    .line 695
    iget-object v2, p0, LagO;->a:LaGg;

    invoke-interface {v2, v0}, LaGg;->a(LaFO;)LaFM;

    move-result-object v2

    .line 696
    iget-object v3, p0, LagO;->a:Lahj;

    iget-object v4, p0, LagO;->a:LaGg;

    .line 697
    invoke-interface {v4, v2}, LaGg;->a(LaFM;)Z

    move-result v2

    .line 696
    invoke-interface {v3, v0, v2}, Lahj;->a(LaFO;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 701
    :catchall_0
    move-exception v0

    iget-object v1, p0, LagO;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    throw v0

    .line 699
    :cond_0
    :try_start_1
    iget-object v0, p0, LagO;->a:LaGg;

    invoke-interface {v0}, LaGg;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 701
    iget-object v0, p0, LagO;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V

    .line 703
    return-void
.end method

.method private b(LaFO;)V
    .locals 4

    .prologue
    .line 300
    :try_start_0
    iget-object v0, p0, LagO;->a:LQB;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LQB;->a(LaFO;Ljava/lang/String;)V
    :try_end_0
    .catch LQC; {:try_start_0 .. :try_end_0} :catch_0

    .line 306
    :goto_0
    return-void

    .line 301
    :catch_0
    move-exception v0

    .line 302
    const-string v1, "SyncManager"

    const-string v2, "ClientFlagSyncException"

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 303
    iget-object v0, p0, LagO;->a:LqK;

    const-string v1, "sync"

    const-string v2, "error"

    const-string v3, "ClientFlagSyncException"

    invoke-virtual {v0, v1, v2, v3}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b(LaFO;)Z
    .locals 3

    .prologue
    .line 730
    iget-object v0, p0, LagO;->a:LpW;

    invoke-interface {v0, p1}, LpW;->a(LaFO;)LpU;

    move-result-object v0

    .line 731
    const-string v1, "haveMinimalMetadataSync"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LpU;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 706
    sget-object v0, LaFj;->d:LaFj;

    invoke-virtual {v0}, LaFj;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 707
    iget-object v1, p0, LagO;->a:LaGg;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, LaGg;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 709
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 710
    iget-object v0, p0, LagO;->a:LahK;

    invoke-interface {v0, v1}, LahK;->a(Landroid/database/Cursor;)LahF;

    move-result-object v0

    .line 711
    if-eqz v0, :cond_0

    invoke-interface {v0}, LahF;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 712
    const/4 v2, 0x0

    invoke-interface {v0, v2}, LahF;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 716
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 718
    return-void
.end method

.method private c(LaFO;)V
    .locals 3

    .prologue
    .line 735
    iget-object v0, p0, LagO;->a:LpW;

    invoke-interface {v0, p1}, LpW;->a(LaFO;)LpU;

    move-result-object v0

    .line 736
    const-string v1, "haveMinimalMetadataSync"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LpU;->a(Ljava/lang/String;Z)V

    .line 737
    iget-object v1, p0, LagO;->a:LpW;

    invoke-interface {v1, v0}, LpW;->a(LpU;)V

    .line 738
    return-void
.end method

.method private c(LaFO;)Z
    .locals 2

    .prologue
    .line 753
    iget-object v0, p0, LagO;->a:LpW;

    invoke-interface {v0, p1}, LpW;->a(LaFO;)LpU;

    move-result-object v0

    .line 754
    const-string v1, "lastContentSyncMilliseconds_v2"

    invoke-interface {v0, v1}, LpU;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private d(LaFO;)V
    .locals 4

    .prologue
    .line 741
    iget-object v0, p0, LagO;->a:LpW;

    invoke-interface {v0, p1}, LpW;->a(LaFO;)LpU;

    move-result-object v0

    .line 742
    const-string v1, "lastContentSyncMilliseconds_v2"

    sget-object v2, LaKN;->a:LaKN;

    invoke-virtual {v2}, LaKN;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LpU;->a(Ljava/lang/String;J)V

    .line 743
    iget-object v1, p0, LagO;->a:LpW;

    invoke-interface {v1, v0}, LpW;->a(LpU;)V

    .line 744
    return-void
.end method


# virtual methods
.method public a(Landroid/accounts/Account;Ljava/lang/String;Landroid/content/SyncResult;)Ljava/lang/Thread;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 410
    new-instance v0, LagP;

    const-string v2, "SyncManager"

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LagP;-><init>(LagO;Ljava/lang/String;Landroid/accounts/Account;Ljava/lang/String;Landroid/content/SyncResult;)V

    .line 440
    iget-object v1, p0, LagO;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Thread;

    .line 441
    if-nez v1, :cond_0

    .line 443
    const-string v1, "SyncManager"

    const-string v2, "Syncing thread requested: %s %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v6

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-static {v1, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 444
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 448
    :goto_0
    const-string v1, "SyncManager"

    const-string v2, "Start sync completed."

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 450
    return-object v0

    .line 446
    :cond_0
    const-string v0, "SyncManager"

    const-string v2, "Sync already started, exiting"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v1

    goto :goto_0
.end method

.method public a()V
    .locals 5

    .prologue
    .line 313
    iget-object v0, p0, LagO;->a:LSF;

    invoke-interface {v0}, LSF;->a()[Landroid/accounts/Account;

    move-result-object v1

    .line 314
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 315
    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v3}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v3

    .line 316
    iget-object v4, p0, LagO;->a:LajO;

    .line 317
    invoke-interface {v4, v3}, LajO;->a(LaFO;)LajN;

    move-result-object v4

    .line 318
    invoke-virtual {v4}, LajN;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 320
    iget-object v4, p0, LagO;->a:LafE;

    invoke-virtual {v4, v3}, LafE;->a(LaFO;)LaJQ;

    .line 314
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 323
    :cond_1
    return-void
.end method

.method public a(LaFO;)V
    .locals 1

    .prologue
    .line 721
    iget-object v0, p0, LagO;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 722
    return-void
.end method

.method public a(LaFO;Landroid/content/SyncResult;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 332
    iget-object v0, p0, LagO;->a:LaGg;

    invoke-interface {v0, p1}, LaGg;->a(LaFO;)LaFM;

    .line 335
    :try_start_0
    invoke-direct {p0, p1}, LagO;->b(LaFO;)Z
    :try_end_0
    .catch LpX; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    :goto_0
    return-void

    .line 338
    :catch_0
    move-exception v0

    .line 339
    const-string v1, "SyncManager"

    const-string v2, "Account store exception"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 345
    :cond_0
    :try_start_1
    invoke-direct {p0, p1, p2}, LagO;->a(LaFO;Landroid/content/SyncResult;)Z
    :try_end_1
    .catch LagU; {:try_start_1 .. :try_end_1} :catch_2

    move-result v0

    .line 352
    iget-object v1, p0, LagO;->a:LbiP;

    invoke-virtual {v1}, LbiP;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    iget-object v0, p0, LagO;->a:LaKR;

    .line 353
    invoke-interface {v0}, LaKR;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 354
    iget-object v0, p0, LagO;->a:LbiP;

    invoke-virtual {v0}, LbiP;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LagR;

    invoke-interface {v0, p1, p2}, LagR;->a(LaFO;Landroid/content/SyncResult;)V

    .line 358
    :cond_1
    :try_start_2
    invoke-direct {p0, p1}, LagO;->c(LaFO;)V
    :try_end_2
    .catch LpX; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 359
    :catch_1
    move-exception v0

    .line 360
    const-string v1, "SyncManager"

    const-string v2, "Failed to store haveMinimalMetadataSync"

    invoke-static {v1, v0, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0

    .line 346
    :catch_2
    move-exception v0

    .line 347
    const-string v1, "SyncManager"

    const-string v2, "Invalid version"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public a(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/SyncResult;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 367
    const-string v0, "SyncManager"

    const-string v1, "in onPerformSync: %s %s extras %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    aput-object p3, v2, v4

    aput-object p2, v2, v5

    invoke-static {v0, v1, v2}, LalV;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 368
    const-string v0, "upload"

    invoke-virtual {p2, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 397
    :cond_0
    :goto_0
    return-void

    .line 372
    :cond_1
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    .line 373
    iget-object v1, p0, LagO;->a:LtK;

    sget-object v2, Lry;->aq:Lry;

    invoke-interface {v1, v2}, LtK;->a(LtJ;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 374
    iget-object v1, p0, LagO;->b:LafE;

    invoke-virtual {v1, v0}, LafE;->a(LaFO;)V

    .line 377
    :cond_2
    const-string v1, "force"

    invoke-virtual {p2, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, LagO;->a:LafG;

    .line 378
    invoke-virtual {v1, v0}, LafG;->a(LaFO;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 383
    :cond_3
    invoke-virtual {p4}, Landroid/content/SyncResult;->clear()V

    .line 386
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 387
    const-string v0, "SyncManager"

    const-string v1, "Sync canceled: %s %s"

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p1, v2, v3

    aput-object p3, v2, v4

    invoke-static {v0, v1, v2}, LalV;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 391
    :cond_4
    invoke-virtual {p0, p1, p3, p4}, LagO;->a(Landroid/accounts/Account;Ljava/lang/String;Landroid/content/SyncResult;)Ljava/lang/Thread;

    move-result-object v0

    .line 393
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 394
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(LaFM;Z)Z
    .locals 4

    .prologue
    .line 764
    iget-object v0, p0, LagO;->a:LaGg;

    invoke-static {}, LaER;->a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LaGg;->a(LaFM;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Ljava/util/Set;

    move-result-object v0

    .line 765
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 766
    iget-object v2, p0, LagO;->a:LaGg;

    invoke-interface {v2, v0}, LaGg;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGb;

    move-result-object v2

    .line 767
    if-eqz v2, :cond_0

    .line 770
    if-eqz p2, :cond_1

    iget-object v3, p0, LagO;->a:LahB;

    invoke-interface {v3, v0}, LahB;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 774
    :cond_1
    iget-object v0, p0, LagO;->a:Lahf;

    invoke-interface {v0, v2}, Lahf;->a(LaGo;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 775
    iget-object v0, p0, LagO;->a:Ladi;

    sget-object v3, LacY;->a:LacY;

    invoke-interface {v0, v2, v3}, Ladi;->d(LaGo;LacY;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LagO;->a:Ladi;

    sget-object v3, LacY;->a:LacY;

    .line 776
    invoke-interface {v0, v2, v3}, Ladi;->b(LaGo;LacY;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 777
    :cond_2
    const/4 v0, 0x1

    .line 781
    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LaFO;)Z
    .locals 2

    .prologue
    .line 725
    iget-object v0, p0, LagO;->a:LpW;

    invoke-interface {v0, p1}, LpW;->a(LaFO;)LpU;

    move-result-object v0

    .line 726
    const-string v1, "lastContentSyncMilliseconds_v2"

    invoke-interface {v0, v1}, LpU;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public b(LaFO;Landroid/content/SyncResult;)V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 569
    const-string v0, "SyncManager"

    const-string v3, "in performSync for %s"

    new-array v4, v2, [Ljava/lang/Object;

    aput-object p1, v4, v1

    invoke-static {v0, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 570
    iget-object v0, p0, LagO;->a:LSF;

    invoke-interface {v0, p1, v1}, LSF;->a(LaFO;Z)V

    .line 571
    iget-object v0, p0, LagO;->a:LSF;

    invoke-interface {v0, p1}, LSF;->a(LaFO;)V

    .line 573
    :try_start_0
    iget-object v0, p0, LagO;->a:LaGg;

    invoke-interface {v0, p1}, LaGg;->a(LaFO;)LaFM;

    move-result-object v3

    .line 575
    iget-object v0, p0, LagO;->a:LaGg;

    invoke-interface {v0, p1}, LaGg;->a(LaFO;)LaFQ;

    move-result-object v0

    .line 576
    invoke-virtual {v0}, LaFQ;->c()Ljava/util/Date;

    move-result-object v0

    .line 577
    if-eqz v0, :cond_0

    .line 578
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    const-wide v6, 0x7fffffffffffffffL

    cmp-long v0, v4, v6

    if-eqz v0, :cond_7

    :cond_0
    move v0, v2

    .line 582
    :goto_0
    :try_start_1
    invoke-direct {p0, p1, p2}, LagO;->a(LaFO;Landroid/content/SyncResult;)Z
    :try_end_1
    .catch LagU; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    .line 588
    :try_start_2
    invoke-direct {p0, v0, v3, p2}, LagO;->a(ZLaFM;Landroid/content/SyncResult;)Z

    .line 593
    iget-object v0, p0, LagO;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v3

    .line 594
    sget-object v0, LaKN;->a:LaKN;

    invoke-virtual {v0}, LaKN;->a()J

    move-result-wide v6

    .line 595
    iget-object v0, p0, LagO;->a:LQr;

    const-string v5, "autoContentSyncIntervalSeconds"

    const/16 v8, 0xe10

    invoke-interface {v0, v5, v8}, LQr;->a(Ljava/lang/String;I)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    .line 599
    :try_start_3
    invoke-direct {p0, p1}, LagO;->c(LaFO;)Z
    :try_end_3
    .catch LpX; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v5

    if-nez v5, :cond_8

    .line 620
    :cond_1
    :goto_1
    :try_start_4
    iget-object v0, p0, LagO;->a:LaKR;

    invoke-interface {v0}, LaKR;->a()LaKS;

    move-result-object v0

    .line 621
    iget-object v1, p0, LagO;->a:Lamn;

    invoke-interface {v1, v0}, Lamn;->a(LaKS;)Z

    move-result v0

    .line 622
    if-eqz v3, :cond_2

    .line 623
    invoke-direct {p0}, LagO;->c()V

    .line 625
    :cond_2
    if-nez v3, :cond_3

    if-eqz v2, :cond_6

    if-eqz v0, :cond_6

    .line 627
    :cond_3
    iget-object v0, p0, LagO;->a:LbiP;

    invoke-virtual {v0}, LbiP;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    if-eqz v4, :cond_4

    iget-object v0, p0, LagO;->a:LaKR;

    .line 628
    invoke-interface {v0}, LaKR;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 629
    iget-object v0, p0, LagO;->a:LbiP;

    invoke-virtual {v0}, LbiP;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LagR;

    invoke-interface {v0, p1, p2}, LagR;->a(LaFO;Landroid/content/SyncResult;)V

    .line 631
    :cond_4
    invoke-direct {p0, p1, v3}, LagO;->a(LaFO;Z)V

    .line 632
    iget-object v0, p0, LagO;->a:LbiP;

    invoke-virtual {v0}, LbiP;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 633
    iget-object v0, p0, LagO;->a:LbiP;

    invoke-virtual {v0}, LbiP;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LagR;

    invoke-interface {v0, p1}, LagR;->a(LaFO;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 636
    :cond_5
    :try_start_5
    invoke-direct {p0, p1}, LagO;->d(LaFO;)V
    :try_end_5
    .catch LpX; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 641
    :goto_2
    :try_start_6
    invoke-direct {p0, p1}, LagO;->c(LaFO;)V
    :try_end_6
    .catch LpX; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 648
    :cond_6
    :goto_3
    :try_start_7
    iget-object v0, p0, LagO;->a:Lyq;

    invoke-interface {v0}, Lyq;->a()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 650
    iget-object v0, p0, LagO;->a:LSF;

    invoke-interface {v0, p1}, LSF;->b(LaFO;)V

    .line 652
    :goto_4
    return-void

    :cond_7
    move v0, v1

    .line 578
    goto :goto_0

    .line 583
    :catch_0
    move-exception v0

    .line 584
    :try_start_8
    const-string v1, "SyncManager"

    const-string v2, "Invalid version"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 650
    iget-object v0, p0, LagO;->a:LSF;

    invoke-interface {v0, p1}, LSF;->b(LaFO;)V

    goto :goto_4

    .line 603
    :cond_8
    :try_start_9
    invoke-direct {p0, p1}, LagO;->a(LaFO;)J

    move-result-wide v8

    sub-long/2addr v6, v8

    .line 604
    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-gez v5, :cond_9

    .line 609
    const-string v0, "SyncManager"

    const-string v5, "The persisted last sync time is bigger than the current time."

    invoke-static {v0, v5}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catch LpX; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_1

    .line 615
    :catch_1
    move-exception v0

    .line 616
    :try_start_a
    const-string v2, "SyncManager"

    const-string v5, "Failed to get lastContentSyncMillisecond"

    invoke-static {v2, v0, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move v2, v1

    goto/16 :goto_1

    .line 612
    :cond_9
    const-wide/16 v8, 0x3e8

    :try_start_b
    div-long/2addr v6, v8
    :try_end_b
    .catch LpX; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    int-to-long v8, v0

    cmp-long v0, v6, v8

    if-gtz v0, :cond_1

    move v2, v1

    goto/16 :goto_1

    .line 637
    :catch_2
    move-exception v0

    .line 638
    :try_start_c
    const-string v1, "SyncManager"

    const-string v2, "Failed to store lastContentSyncMilliseconds"

    invoke-static {v1, v0, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto :goto_2

    .line 650
    :catchall_0
    move-exception v0

    iget-object v1, p0, LagO;->a:LSF;

    invoke-interface {v1, p1}, LSF;->b(LaFO;)V

    throw v0

    .line 642
    :catch_3
    move-exception v0

    .line 643
    :try_start_d
    const-string v1, "SyncManager"

    const-string v2, "Failed to store haveMinimalMetadataSync"

    invoke-static {v1, v0, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto :goto_3
.end method
