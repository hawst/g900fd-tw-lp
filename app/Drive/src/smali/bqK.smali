.class final LbqK;
.super LbqE;
.source "ChecksumHashFunction.java"


# instance fields
.field final synthetic a:LbqI;

.field private final a:Ljava/util/zip/Checksum;


# direct methods
.method private constructor <init>(LbqI;Ljava/util/zip/Checksum;)V
    .locals 1

    .prologue
    .line 63
    iput-object p1, p0, LbqK;->a:LbqI;

    invoke-direct {p0}, LbqE;-><init>()V

    .line 64
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/zip/Checksum;

    iput-object v0, p0, LbqK;->a:Ljava/util/zip/Checksum;

    .line 65
    return-void
.end method

.method synthetic constructor <init>(LbqI;Ljava/util/zip/Checksum;LbqJ;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, LbqK;-><init>(LbqI;Ljava/util/zip/Checksum;)V

    return-void
.end method


# virtual methods
.method public a()LbqL;
    .locals 4

    .prologue
    .line 78
    iget-object v0, p0, LbqK;->a:Ljava/util/zip/Checksum;

    invoke-interface {v0}, Ljava/util/zip/Checksum;->getValue()J

    move-result-wide v0

    .line 79
    iget-object v2, p0, LbqK;->a:LbqI;

    invoke-static {v2}, LbqI;->a(LbqI;)I

    move-result v2

    const/16 v3, 0x20

    if-ne v2, v3, :cond_0

    .line 85
    long-to-int v0, v0

    invoke-static {v0}, LbqM;->a(I)LbqL;

    move-result-object v0

    .line 87
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0, v1}, LbqM;->a(J)LbqL;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(B)V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, LbqK;->a:Ljava/util/zip/Checksum;

    invoke-interface {v0, p1}, Ljava/util/zip/Checksum;->update(I)V

    .line 70
    return-void
.end method

.method protected a([BII)V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, LbqK;->a:Ljava/util/zip/Checksum;

    invoke-interface {v0, p1, p2, p3}, Ljava/util/zip/Checksum;->update([BII)V

    .line 75
    return-void
.end method
