.class public LMe;
.super Ljava/lang/Object;
.source "SelectionTextViewColorUpdater.java"

# interfaces
.implements LLs;


# instance fields
.field private final a:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput p1, p0, LMe;->a:I

    .line 17
    return-void
.end method

.method private a(Landroid/widget/TextView;Z)V
    .locals 2

    .prologue
    .line 25
    sget v0, Lxc;->view_tag_key_selection_original_resource:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 26
    if-eqz p2, :cond_1

    .line 27
    sget v0, Lxc;->view_tag_key_selection_original_resource:I

    invoke-virtual {p1}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    .line 28
    iget v0, p0, LMe;->a:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 35
    :cond_0
    :goto_0
    return-void

    .line 30
    :cond_1
    if-eqz v0, :cond_0

    .line 31
    check-cast v0, Landroid/content/res/ColorStateList;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 32
    sget v0, Lxc;->view_tag_key_selection_original_resource:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Landroid/widget/TextView;

    invoke-direct {p0, p1, p2}, LMe;->a(Landroid/widget/TextView;Z)V

    .line 22
    return-void
.end method
