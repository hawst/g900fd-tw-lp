.class public LaFQ;
.super LaGm;
.source "AccountState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGm",
        "<",
        "LaEe;",
        "LaEz;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private a:J

.field private final a:LaFO;

.field private a:Ljava/lang/Long;

.field private a:Ljava/util/Date;

.field private a:Z

.field private b:Ljava/util/Date;

.field private b:Z

.field private c:Ljava/util/Date;


# direct methods
.method public constructor <init>(LaEz;LaFO;)V
    .locals 8

    .prologue
    const-wide v6, 0x7fffffffffffffffL

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 55
    invoke-static {}, LaEe;->a()LaEe;

    move-result-object v0

    sget-object v1, LaEG;->b:LaEG;

    invoke-virtual {v1}, LaEG;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, LaGm;-><init>(LaFm;LaFy;Landroid/net/Uri;)V

    .line 40
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, LaFQ;->a:Ljava/util/Date;

    .line 41
    iput-wide v4, p0, LaFQ;->a:J

    .line 42
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v6, v7}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, LaFQ;->b:Ljava/util/Date;

    .line 43
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v6, v7}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, LaFQ;->c:Ljava/util/Date;

    .line 47
    iput v2, p0, LaFQ;->a:I

    .line 49
    iput-boolean v2, p0, LaFQ;->a:Z

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, LaFQ;->a:Ljava/lang/Long;

    .line 56
    iput-object p2, p0, LaFQ;->a:LaFO;

    .line 57
    return-void
.end method

.method public static a(LaEz;Landroid/database/Cursor;)LaFQ;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 219
    new-instance v2, LaFQ;

    sget-object v0, LaEf;->a:LaEf;

    .line 221
    invoke-virtual {v0}, LaEf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    invoke-direct {v2, p0, v0}, LaFQ;-><init>(LaEz;LaFO;)V

    .line 222
    invoke-static {}, LaEe;->a()LaEe;

    move-result-object v0

    invoke-virtual {v0}, LaEe;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LaFr;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, LaFQ;->c(J)V

    .line 223
    sget-object v0, LaEf;->b:LaEf;

    invoke-virtual {v0}, LaEf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Z

    move-result v0

    iput-boolean v0, v2, LaFQ;->b:Z

    .line 225
    sget-object v0, LaEf;->c:LaEf;

    invoke-virtual {v0}, LaEf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 226
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v0}, LaFQ;->a(Ljava/util/Date;)V

    .line 228
    sget-object v0, LaEf;->i:LaEf;

    .line 229
    invoke-virtual {v0}, LaEf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 230
    invoke-virtual {v2, v4, v5}, LaFQ;->a(J)V

    .line 231
    sget-object v0, LaEf;->d:LaEf;

    invoke-virtual {v0}, LaEf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v3

    .line 232
    if-nez v3, :cond_2

    move-object v0, v1

    :goto_0
    invoke-virtual {v2, v0}, LaFQ;->b(Ljava/util/Date;)V

    .line 234
    sget-object v0, LaEf;->e:LaEf;

    invoke-virtual {v0}, LaEf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    .line 235
    if-nez v0, :cond_3

    :goto_1
    invoke-virtual {v2, v1}, LaFQ;->c(Ljava/util/Date;)V

    .line 236
    sget-object v0, LaEf;->g:LaEf;

    .line 237
    invoke-virtual {v0}, LaEf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    .line 236
    invoke-virtual {v2, v0}, LaFQ;->a(I)V

    .line 239
    sget-object v0, LaEf;->h:LaEf;

    .line 240
    invoke-virtual {v0}, LaEf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    .line 241
    if-eqz v0, :cond_0

    .line 242
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v2, v0}, LaFQ;->b(Z)V

    .line 245
    :cond_0
    sget-object v0, LaEf;->l:LaEf;

    invoke-virtual {v0}, LaEf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    .line 246
    if-eqz v0, :cond_1

    .line 247
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, LaFQ;->b(J)V

    .line 249
    :cond_1
    return-object v2

    .line 232
    :cond_2
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    goto :goto_0

    .line 235
    :cond_3
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Ljava/util/Date;-><init>(J)V

    goto :goto_1
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 141
    iget v0, p0, LaFQ;->a:I

    return v0
.end method

.method public a()J
    .locals 2

    .prologue
    .line 83
    iget-wide v0, p0, LaFQ;->a:J

    return-wide v0
.end method

.method public a()LaFO;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, LaFQ;->a:LaFO;

    return-object v0
.end method

.method public a()Lafe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lafe",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation

    .prologue
    .line 134
    invoke-virtual {p0}, LaFQ;->c()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p0}, LaFQ;->b()Ljava/util/Date;

    move-result-object v1

    invoke-static {v0, v1}, Lafe;->a(Ljava/lang/Object;Ljava/lang/Object;)Lafe;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, LaFQ;->a:Ljava/lang/Long;

    return-object v0
.end method

.method public a()Ljava/util/Date;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, LaFQ;->a:Ljava/util/Date;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 148
    iput p1, p0, LaFQ;->a:I

    .line 149
    return-void
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 87
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 88
    iput-wide p1, p0, LaFQ;->a:J

    .line 89
    return-void

    .line 87
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 192
    sget-object v0, LaEf;->a:LaEf;

    invoke-virtual {v0}, LaEf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaFQ;->a()LaFO;

    move-result-object v1

    invoke-virtual {v1}, LaFO;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    sget-object v0, LaEf;->b:LaEf;

    invoke-virtual {v0}, LaEf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, LaFQ;->b:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 194
    sget-object v0, LaEf;->c:LaEf;

    invoke-virtual {v0}, LaEf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaFQ;->a()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 195
    sget-object v0, LaEf;->i:LaEf;

    invoke-virtual {v0}, LaEf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    .line 196
    invoke-virtual {p0}, LaFQ;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 195
    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 197
    invoke-virtual {p0}, LaFQ;->b()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 198
    sget-object v0, LaEf;->d:LaEf;

    invoke-virtual {v0}, LaEf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaFQ;->b()Ljava/util/Date;

    move-result-object v1

    .line 199
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    .line 198
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 203
    :goto_0
    invoke-virtual {p0}, LaFQ;->c()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 204
    sget-object v0, LaEf;->e:LaEf;

    invoke-virtual {v0}, LaEf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    .line 205
    invoke-virtual {p0}, LaFQ;->c()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 204
    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 209
    :goto_1
    sget-object v0, LaEf;->g:LaEf;

    invoke-virtual {v0}, LaEf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaFQ;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 210
    sget-object v0, LaEf;->h:LaEf;

    invoke-virtual {v0}, LaEf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, LaFQ;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 211
    sget-object v0, LaEf;->l:LaEf;

    invoke-virtual {v0}, LaEf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    .line 212
    invoke-virtual {p0}, LaFQ;->a()Ljava/lang/Long;

    move-result-object v1

    .line 211
    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 213
    return-void

    .line 201
    :cond_0
    sget-object v0, LaEf;->d:LaEf;

    invoke-virtual {v0}, LaEf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_0

    .line 207
    :cond_1
    sget-object v0, LaEf;->e:LaEf;

    invoke-virtual {v0}, LaEf;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_1

    .line 210
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public a(Ljava/util/Date;)V
    .locals 0

    .prologue
    .line 75
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    iput-object p1, p0, LaFQ;->a:Ljava/util/Date;

    .line 77
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 64
    iput-boolean p1, p0, LaFQ;->b:Z

    .line 65
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, LaFQ;->b:Z

    return v0
.end method

.method public b()Ljava/util/Date;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, LaFQ;->b:Ljava/util/Date;

    return-object v0
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 184
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LaFQ;->a:Ljava/lang/Long;

    .line 185
    return-void
.end method

.method public b(Ljava/util/Date;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, LaFQ;->b:Ljava/util/Date;

    .line 107
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 162
    iput-boolean p1, p0, LaFQ;->a:Z

    .line 163
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 155
    iget-boolean v0, p0, LaFQ;->a:Z

    return v0
.end method

.method public c()Ljava/util/Date;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, LaFQ;->c:Ljava/util/Date;

    return-object v0
.end method

.method public c(Ljava/util/Date;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, LaFQ;->c:Ljava/util/Date;

    .line 128
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 254
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Account[%s, sqlId=%d%s%s]"

    const/4 v0, 0x4

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v4, p0, LaFQ;->a:LaFO;

    aput-object v4, v3, v0

    const/4 v0, 0x1

    .line 257
    invoke-virtual {p0}, LaFQ;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x2

    iget-boolean v0, p0, LaFQ;->a:Z

    if-eqz v0, :cond_0

    const-string v0, ", syncing"

    :goto_0
    aput-object v0, v3, v4

    const/4 v4, 0x3

    iget-object v0, p0, LaFQ;->c:Ljava/util/Date;

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_1
    aput-object v0, v3, v4

    .line 254
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 257
    :cond_0
    const-string v0, ""

    goto :goto_0

    :cond_1
    const-string v0, ", clipped"

    goto :goto_1
.end method
