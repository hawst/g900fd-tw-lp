.class public LaWe;
.super LaLa;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaLa",
        "<",
        "LaWd;",
        ">;"
    }
.end annotation


# instance fields
.field protected a:LaLq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaLq",
            "<",
            "LaWd;",
            ">;"
        }
    .end annotation
.end field

.field private a:Landroid/app/Activity;

.field private final a:Landroid/support/v4/app/Fragment;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/Fragment;)V
    .locals 0

    invoke-direct {p0}, LaLa;-><init>()V

    iput-object p1, p0, LaWe;->a:Landroid/support/v4/app/Fragment;

    return-void
.end method

.method public static synthetic a(LaWe;Landroid/app/Activity;)V
    .locals 0

    invoke-direct {p0, p1}, LaWe;->a(Landroid/app/Activity;)V

    return-void
.end method

.method private a(Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, LaWe;->a:Landroid/app/Activity;

    invoke-virtual {p0}, LaWe;->h()V

    return-void
.end method


# virtual methods
.method protected a(LaLq;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaLq",
            "<",
            "LaWd;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, LaWe;->a:LaLq;

    invoke-virtual {p0}, LaWe;->h()V

    return-void
.end method

.method public h()V
    .locals 4

    iget-object v0, p0, LaWe;->a:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaWe;->a:LaLq;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LaWe;->a()LaKZ;

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, LaWe;->a:Landroid/app/Activity;

    invoke-static {v0}, LaVZ;->a(Landroid/content/Context;)I

    iget-object v0, p0, LaWe;->a:Landroid/app/Activity;

    invoke-static {v0}, LaXQ;->a(Landroid/content/Context;)LaWQ;

    move-result-object v0

    iget-object v1, p0, LaWe;->a:Landroid/app/Activity;

    invoke-static {v1}, LaLp;->a(Ljava/lang/Object;)LaLm;

    move-result-object v1

    invoke-interface {v0, v1}, LaWQ;->a(LaLm;)LaWr;

    move-result-object v0

    iget-object v1, p0, LaWe;->a:LaLq;

    new-instance v2, LaWd;

    iget-object v3, p0, LaWe;->a:Landroid/support/v4/app/Fragment;

    invoke-direct {v2, v3, v0}, LaWd;-><init>(Landroid/support/v4/app/Fragment;LaWr;)V

    invoke-interface {v1, v2}, LaLq;->a(LaKZ;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LaCH; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, LaYd;

    invoke-direct {v1, v0}, LaYd;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :catch_1
    move-exception v0

    goto :goto_0
.end method
