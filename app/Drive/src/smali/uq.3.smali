.class public abstract enum Luq;
.super Ljava/lang/Enum;
.source "MoveEntryActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Luq;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Luq;

.field private static final synthetic a:[Luq;

.field public static final enum b:Luq;

.field public static final enum c:Luq;

.field public static final enum d:Luq;

.field public static final enum e:Luq;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 56
    new-instance v0, Lur;

    const-string v1, "LAUNCH_PICK_ENTRY_DIALOG"

    invoke-direct {v0, v1, v2}, Lur;-><init>(Ljava/lang/String;I)V

    sput-object v0, Luq;->a:Luq;

    .line 62
    new-instance v0, Lus;

    const-string v1, "SELECTING_TARGET"

    invoke-direct {v0, v1, v3}, Lus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Luq;->b:Luq;

    .line 69
    new-instance v0, Lut;

    const-string v1, "WARNING_DIALOG"

    invoke-direct {v0, v1, v4}, Lut;-><init>(Ljava/lang/String;I)V

    sput-object v0, Luq;->c:Luq;

    .line 75
    new-instance v0, Luu;

    const-string v1, "PERFORM_MOVE"

    invoke-direct {v0, v1, v5}, Luu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Luq;->d:Luq;

    .line 81
    new-instance v0, Luv;

    const-string v1, "FINISH"

    invoke-direct {v0, v1, v6}, Luv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Luq;->e:Luq;

    .line 55
    const/4 v0, 0x5

    new-array v0, v0, [Luq;

    sget-object v1, Luq;->a:Luq;

    aput-object v1, v0, v2

    sget-object v1, Luq;->b:Luq;

    aput-object v1, v0, v3

    sget-object v1, Luq;->c:Luq;

    aput-object v1, v0, v4

    sget-object v1, Luq;->d:Luq;

    aput-object v1, v0, v5

    sget-object v1, Luq;->e:Luq;

    aput-object v1, v0, v6

    sput-object v0, Luq;->a:[Luq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILum;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Luq;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Luq;
    .locals 1

    .prologue
    .line 55
    const-class v0, Luq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Luq;

    return-object v0
.end method

.method public static values()[Luq;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Luq;->a:[Luq;

    invoke-virtual {v0}, [Luq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Luq;

    return-object v0
.end method


# virtual methods
.method abstract a(Lcom/google/android/apps/docs/app/MoveEntryActivity;)Luq;
.end method
