.class public Laxz;
.super Ljava/lang/Object;
.source "PdfLoader.java"


# instance fields
.field private final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LaxI;",
            ">;"
        }
    .end annotation
.end field

.field private a:LauW;

.field final a:LavU;

.field final a:LaxE;

.field private a:LaxF;

.field final a:LaxP;

.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LaxH;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LaxE;LauW;LavU;LaxH;)V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Laxz;->a:LaxE;

    .line 72
    iput-object p2, p0, Laxz;->a:LauW;

    .line 73
    new-instance v0, LaxP;

    invoke-direct {v0}, LaxP;-><init>()V

    iput-object v0, p0, Laxz;->a:LaxP;

    .line 74
    iput-object p3, p0, Laxz;->a:LavU;

    .line 75
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Laxz;->a:Ljava/lang/ref/WeakReference;

    .line 76
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Laxz;->a:Landroid/util/SparseArray;

    .line 77
    return-void
.end method

.method static synthetic a(Laxz;)LauW;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Laxz;->a:LauW;

    return-object v0
.end method

.method static synthetic a(Laxz;LauW;)LauW;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Laxz;->a:LauW;

    return-object p1
.end method

.method static synthetic a(Laxz;LaxF;)LaxF;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Laxz;->a:LaxF;

    return-object p1
.end method

.method public static a(Landroid/content/Context;LauW;LavU;LaxH;)Laxz;
    .locals 3

    .prologue
    .line 57
    new-instance v0, LaxE;

    invoke-direct {v0, p0}, LaxE;-><init>(Landroid/content/Context;)V

    .line 58
    new-instance v1, Laxz;

    invoke-direct {v1, v0, p1, p2, p3}, Laxz;-><init>(LaxE;LauW;LavU;LaxH;)V

    .line 59
    new-instance v2, LaxA;

    invoke-direct {v2, v1}, LaxA;-><init>(Laxz;)V

    invoke-virtual {v0, v2}, LaxE;->a(Ljava/lang/Runnable;)V

    .line 66
    iget-object v2, p1, LauW;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    iget-object v2, v2, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-static {v0, v2}, LaxE;->a(LaxE;Landroid/net/Uri;)V

    .line 67
    return-object v1
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, -0x1

    .line 86
    :try_start_0
    iget-object v1, p0, Laxz;->a:LaxE;

    invoke-virtual {v1}, LaxE;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Laxz;->a:LaxE;

    invoke-static {v1}, LaxE;->a(LaxE;)LavE;

    move-result-object v1

    invoke-interface {v1}, LavE;->b()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 89
    :cond_0
    :goto_0
    return v0

    .line 87
    :catch_0
    move-exception v1

    .line 88
    const-string v2, "PdfLoader"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RemoteException during call to numPages() "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method a()LavE;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Laxz;->a:LaxE;

    invoke-static {v0}, LaxE;->a(LaxE;)LavE;

    move-result-object v0

    return-object v0
.end method

.method a()LaxH;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Laxz;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaxH;

    return-object v0
.end method

.method a(I)LaxI;
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Laxz;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaxI;

    .line 207
    if-nez v0, :cond_0

    .line 208
    new-instance v0, LaxI;

    invoke-direct {v0, p0, p1}, LaxI;-><init>(Laxz;I)V

    .line 209
    iget-object v1, p0, Laxz;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 211
    :cond_0
    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Laxz;->a:LaxF;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Laxz;->a:LaxF;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LaxF;->cancel(Z)Z

    .line 176
    const/4 v0, 0x0

    iput-object v0, p0, Laxz;->a:LaxF;

    .line 178
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0, p1}, Laxz;->a(I)LaxI;

    move-result-object v0

    invoke-virtual {v0}, LaxI;->d()V

    .line 96
    return-void
.end method

.method public a(ILcom/google/android/apps/viewer/client/Dimensions;)V
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0, p1}, Laxz;->a(I)LaxI;

    move-result-object v0

    invoke-virtual {v0, p2}, LaxI;->a(Lcom/google/android/apps/viewer/client/Dimensions;)V

    .line 112
    return-void
.end method

.method public a(ILcom/google/android/apps/viewer/client/Dimensions;Ljava/lang/Iterable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/apps/viewer/client/Dimensions;",
            "Ljava/lang/Iterable",
            "<",
            "Lawr;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 119
    invoke-virtual {p0, p1}, Laxz;->a(I)LaxI;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, LaxI;->a(Lcom/google/android/apps/viewer/client/Dimensions;Ljava/lang/Iterable;)V

    .line 120
    return-void
.end method

.method public a(ILcom/google/android/apps/viewer/pdflib/SelectionBoundary;Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;)V
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0, p1}, Laxz;->a(I)LaxI;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, LaxI;->a(Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;)V

    .line 154
    return-void
.end method

.method public a(ILjava/lang/Iterable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 129
    invoke-virtual {p0, p1}, Laxz;->a(I)LaxI;

    move-result-object v0

    invoke-virtual {v0, p2}, LaxI;->a(Ljava/lang/Iterable;)V

    .line 130
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0, p1}, Laxz;->a(I)LaxI;

    move-result-object v0

    invoke-virtual {v0, p2}, LaxI;->a(Ljava/lang/String;)V

    .line 146
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Laxz;->a:LaxP;

    new-instance v1, LaxC;

    invoke-direct {v1, p0, p1}, LaxC;-><init>(Laxz;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LaxP;->a(Laxy;)V

    .line 82
    return-void
.end method

.method public a(Ljava/lang/String;IZ)V
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Laxz;->a:LaxF;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laxz;->a:LaxF;

    invoke-static {v0}, LaxF;->a(LaxF;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 164
    invoke-virtual {p0}, Laxz;->a()V

    .line 166
    :cond_0
    iget-object v0, p0, Laxz;->a:LaxF;

    if-nez v0, :cond_1

    .line 167
    new-instance v0, LaxF;

    invoke-direct {v0, p0, p1, p2, p3}, LaxF;-><init>(Laxz;Ljava/lang/String;IZ)V

    iput-object v0, p0, Laxz;->a:LaxF;

    .line 168
    iget-object v0, p0, Laxz;->a:LaxP;

    iget-object v1, p0, Laxz;->a:LaxF;

    invoke-virtual {v0, v1}, LaxP;->a(Laxy;)V

    .line 170
    :cond_1
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 182
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Laxz;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 183
    iget-object v0, p0, Laxz;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaxI;

    invoke-virtual {v0}, LaxI;->d()V

    .line 182
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 185
    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0, p1}, Laxz;->a(I)LaxI;

    move-result-object v0

    invoke-virtual {v0}, LaxI;->a()V

    .line 104
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Laxz;->a:LaxP;

    new-instance v1, LaxD;

    invoke-direct {v1, p0}, LaxD;-><init>(Laxz;)V

    invoke-virtual {v0, v1}, LaxP;->a(Laxy;)V

    .line 190
    return-void
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 124
    invoke-virtual {p0, p1}, Laxz;->a(I)LaxI;

    move-result-object v0

    invoke-virtual {v0}, LaxI;->b()V

    .line 125
    return-void
.end method

.method public d(I)V
    .locals 1

    .prologue
    .line 137
    invoke-virtual {p0, p1}, Laxz;->a(I)LaxI;

    move-result-object v0

    invoke-virtual {v0}, LaxI;->c()V

    .line 138
    return-void
.end method
