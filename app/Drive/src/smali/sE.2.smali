.class LsE;
.super Ljava/lang/Object;
.source "DocListAppConfigurationProvider.java"

# interfaces
.implements LsC;


# instance fields
.field private final a:Landroid/net/Uri;

.field private final a:Lrx;

.field private final a:LtK;

.field private final a:LzQ;

.field private final a:Z

.field private final b:Z

.field private final c:Z

.field private final d:Z


# direct methods
.method constructor <init>(Lrx;LtK;ZZLandroid/net/Uri;ZLzQ;Z)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, LsE;->a:Lrx;

    .line 72
    iput-object p2, p0, LsE;->a:LtK;

    .line 73
    iput-boolean p3, p0, LsE;->a:Z

    .line 74
    iput-boolean p4, p0, LsE;->c:Z

    .line 75
    iput-object p5, p0, LsE;->a:Landroid/net/Uri;

    .line 76
    iput-boolean p6, p0, LsE;->b:Z

    .line 77
    iput-object p7, p0, LsE;->a:LzQ;

    .line 78
    iput-boolean p8, p0, LsE;->d:Z

    .line 79
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 106
    sget v0, Lxe;->navigation_list_footerview_drive:I

    return v0
.end method

.method public a()LCl;
    .locals 1

    .prologue
    .line 93
    sget-object v0, LCe;->q:LCe;

    return-object v0
.end method

.method public a()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, LsE;->a:Landroid/net/Uri;

    return-object v0
.end method

.method public a()LbmF;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmF",
            "<",
            "Lvr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    iget-object v7, p0, LsE;->a:Lrx;

    sget-object v0, Lty;->a:Lvr;

    sget-object v1, Lty;->b:Lvr;

    sget-object v2, Lty;->c:Lvr;

    sget-object v3, Lty;->d:Lvr;

    sget-object v4, Lty;->e:Lvr;

    sget-object v5, Lty;->f:Lvr;

    sget-object v6, Lty;->i:Lvr;

    .line 153
    invoke-static/range {v0 .. v6}, LbmF;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmF;

    move-result-object v0

    .line 152
    invoke-static {v7, v0}, Lvr;->a(Lrx;LbmF;)LbmF;

    move-result-object v0

    return-object v0
.end method

.method public a()Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->a:Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    return-object v0
.end method

.method public a(Landroid/content/res/Resources;)Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;
    .locals 3

    .prologue
    .line 111
    sget v0, Lxa;->doc_grid_thumbnail_width:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 112
    sget v1, Lxa;->doc_grid_thumbnail_height:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 113
    new-instance v2, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;-><init>(II)V

    return-object v2
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    const-string v0, "helpDriveUrlTemplate"

    return-object v0
.end method

.method public a(Lwm;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 182
    if-eqz p1, :cond_0

    .line 183
    invoke-interface {p1}, Lwm;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()LCl;

    move-result-object v0

    .line 184
    if-eqz v0, :cond_0

    .line 185
    invoke-interface {v0}, LCl;->b()Ljava/lang/String;

    move-result-object v0

    .line 186
    if-nez v0, :cond_1

    .line 187
    const-string v0, "DocListAppConfigurationProvider"

    const-string v1, "unexpected null contextHelpName"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 194
    :cond_0
    const-string v0, "mobile_my_drive"

    :cond_1
    return-object v0
.end method

.method public a()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LRR;",
            ">;"
        }
    .end annotation

    .prologue
    .line 98
    iget-boolean v0, p0, LsE;->a:Z

    if-eqz v0, :cond_0

    sget-object v0, LRR;->a:LRR;

    sget-object v1, LRR;->d:LRR;

    invoke-static {v0, v1}, LbmF;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmF;

    move-result-object v0

    .line 100
    :goto_0
    return-object v0

    .line 98
    :cond_0
    sget-object v0, LRR;->a:LRR;

    sget-object v1, LRR;->c:LRR;

    sget-object v2, LRR;->b:LRR;

    .line 100
    invoke-static {v0, v1, v2}, LbmF;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmF;

    move-result-object v0

    goto :goto_0
.end method

.method public a()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LCK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    const-class v0, LCK;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 126
    sget-object v1, LCK;->o:LCK;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 127
    return-object v0
.end method

.method public a()LzO;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, LsE;->a:LzQ;

    invoke-interface {v0}, LzQ;->a()LzO;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 137
    sget v0, Lxf;->menu_doclist_activity:I

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    const-string v0, "http://support.google.com/drive/?hl=%s&p=android_drive_help"

    return-object v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x1

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x1

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 175
    iget-boolean v2, p0, LsE;->b:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, LsE;->d:Z

    if-eqz v2, :cond_1

    :cond_0
    iget-boolean v2, p0, LsE;->c:Z

    if-eqz v2, :cond_2

    :cond_1
    move v2, v0

    .line 177
    :goto_0
    if-nez v2, :cond_3

    :goto_1
    return v0

    :cond_2
    move v2, v1

    .line 175
    goto :goto_0

    :cond_3
    move v0, v1

    .line 177
    goto :goto_1
.end method
