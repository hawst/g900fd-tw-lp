.class public final Lbsk;
.super Ljava/lang/Object;
.source "RuntimeProvider.java"

# interfaces
.implements LbuE;
.implements LbwA;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LbuE",
        "<TT;>;",
        "LbwA",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private volatile a:LbuB;

.field private a:LbuH;

.field private volatile a:Lbuv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbuv",
            "<TT;>;"
        }
    .end annotation
.end field

.field private a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<+TT;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field private volatile a:Ljava/lang/Object;

.field private a:Z

.field private final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lbuv;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbuv",
            "<TT;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbuv;

    iput-object v0, p0, Lbsk;->a:Lbuv;

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lbsk;->a:Ljava/lang/Class;

    .line 54
    iput-object p2, p0, Lbsk;->b:Ljava/lang/Class;

    .line 55
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lbsk;->a:Ljava/lang/Class;

    .line 59
    iput-object p2, p0, Lbsk;->b:Ljava/lang/Class;

    .line 60
    return-void
.end method

.method private static a(Lbxw;)LbuE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbxw",
            "<+TT;>;)",
            "LbuE",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 88
    instance-of v0, p0, LbuE;

    if-eqz v0, :cond_0

    .line 89
    check-cast p0, LbuE;

    .line 91
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lbsl;

    invoke-direct {v0, p0}, Lbsl;-><init>(Lbxw;)V

    move-object p0, v0

    goto :goto_0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lbsk;->a:LbuB;

    if-eqz v0, :cond_1

    .line 140
    monitor-enter p0

    .line 141
    :try_start_0
    iget-object v0, p0, Lbsk;->a:LbuB;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lbsk;->a:LbuB;

    iget-object v1, p0, Lbsk;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, LbuB;->a(Ljava/lang/Object;)V

    .line 143
    const/4 v0, 0x0

    iput-object v0, p0, Lbsk;->a:LbuB;

    .line 144
    const/4 v0, 0x0

    iput-object v0, p0, Lbsk;->a:Ljava/lang/Object;

    .line 146
    :cond_0
    monitor-exit p0

    .line 148
    :cond_1
    return-void

    .line 146
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(ZLjava/lang/String;)V
    .locals 4

    .prologue
    .line 178
    if-nez p1, :cond_0

    .line 179
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lbsk;->a()Lbuv;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :cond_0
    return-void
.end method

.method private b()Lbxw;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbxw",
            "<+TT;>;"
        }
    .end annotation

    .prologue
    .line 170
    iget-object v0, p0, Lbsk;->a:Lbxw;

    if-nez v0, :cond_0

    .line 171
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Provider of %s not initialized."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 172
    invoke-virtual {p0}, Lbsk;->a()Lbuv;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 174
    :cond_0
    iget-object v0, p0, Lbsk;->a:Lbxw;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 155
    iget-boolean v0, p0, Lbsk;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Provider of %s has already been scoped."

    invoke-direct {p0, v0, v1}, Lbsk;->a(ZLjava/lang/String;)V

    .line 156
    return-void

    .line 155
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()LbuE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbuE",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 166
    invoke-direct {p0}, Lbsk;->b()Lbxw;

    move-result-object v0

    invoke-static {v0}, Lbsk;->a(Lbxw;)LbuE;

    move-result-object v0

    return-object v0
.end method

.method public a()Lbuv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbuv",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lbsk;->a:Lbuv;

    if-nez v0, :cond_0

    .line 81
    iget-object v0, p0, Lbsk;->a:Ljava/lang/Class;

    invoke-static {v0}, Lbuv;->a(Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iput-object v0, p0, Lbsk;->a:Lbuv;

    .line 83
    :cond_0
    iget-object v0, p0, Lbsk;->a:Lbuv;

    return-object v0
.end method

.method public a()Lbxw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbxw",
            "<+TT;>;"
        }
    .end annotation

    .prologue
    .line 241
    invoke-direct {p0}, Lbsk;->b()Lbxw;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Lbsk;->b:Ljava/lang/Class;

    return-object v0
.end method

.method public a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 160
    invoke-direct {p0}, Lbsk;->a()V

    .line 161
    invoke-direct {p0}, Lbsk;->b()Lbxw;

    move-result-object v0

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbwp;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lbwp",
            "<TV;>;)TV;"
        }
    .end annotation

    .prologue
    .line 211
    iget-object v0, p0, Lbsk;->b:Ljava/lang/Class;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lbsk;->b:Ljava/lang/Class;

    invoke-interface {p1, v0}, Lbwp;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 214
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbsk;->a:LbuH;

    invoke-interface {p1, v0}, Lbwp;->a(LbuH;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lbwq;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lbwq",
            "<-TT;TV;>;)TV;"
        }
    .end annotation

    .prologue
    .line 220
    invoke-interface {p1, p0}, Lbwq;->a(LbwA;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method declared-synchronized a(LbuB;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbuB",
            "<*>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 133
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuB;

    iput-object v0, p0, Lbsk;->a:LbuB;

    .line 134
    invoke-static {p2}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lbsk;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    monitor-exit p0

    return-void

    .line 133
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(LbuH;)V
    .locals 2

    .prologue
    .line 68
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lbsk;->b()V

    .line 70
    iget-object v0, p0, Lbsk;->a:Lbxw;

    if-eqz v0, :cond_0

    .line 71
    invoke-virtual {p0}, Lbsk;->a()Lbuv;

    move-result-object v0

    invoke-direct {p0}, Lbsk;->b()Lbxw;

    move-result-object v1

    invoke-static {v1}, Lbsk;->a(Lbxw;)LbuE;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LbuH;->a(Lbuv;LbuE;)LbuE;

    move-result-object v0

    iput-object v0, p0, Lbsk;->a:Lbxw;

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbsk;->a:Z

    .line 74
    :cond_0
    iput-object p1, p0, Lbsk;->a:LbuH;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75
    monitor-exit p0

    return-void

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lbxw;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbxw",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 124
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lbsk;->b()V

    .line 125
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxw;

    iput-object v0, p0, Lbsk;->a:Lbxw;

    .line 126
    iget-object v0, p0, Lbsk;->a:LbuH;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lbsk;->a:LbuH;

    invoke-virtual {p0, v0}, Lbsk;->a(LbuH;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 129
    :cond_0
    monitor-exit p0

    return-void

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lbsk;->a:Lbxw;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 206
    const/4 v0, 0x0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 185
    const-string v0, "RuntimeProvider[key=%s, scopeAnnotation=%s, delegate=%s, scope=%s, scoped=%s]"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 187
    invoke-virtual {p0}, Lbsk;->a()Lbuv;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lbsk;->b:Ljava/lang/Class;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lbsk;->a:Lbxw;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lbsk;->a:LbuH;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-boolean v3, p0, Lbsk;->a:Z

    .line 191
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .line 185
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
