.class public LbvH;
.super Ljava/lang/Object;
.source "StackTraceElements.java"


# static fields
.field private static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:[LbvI;

.field private static final a:[Ljava/lang/StackTraceElement;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 36
    new-array v0, v1, [Ljava/lang/StackTraceElement;

    sput-object v0, LbvH;->a:[Ljava/lang/StackTraceElement;

    .line 37
    new-array v0, v1, [LbvI;

    sput-object v0, LbvH;->a:[LbvI;

    .line 55
    new-instance v0, LbnN;

    invoke-direct {v0}, LbnN;-><init>()V

    invoke-virtual {v0}, LbnN;->a()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    sput-object v0, LbvH;->a:Ljava/util/Map;

    return-void
.end method

.method public static a(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 88
    const/4 v0, 0x0

    .line 89
    const/4 v1, -0x1

    .line 92
    new-instance v2, Ljava/lang/StackTraceElement;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "class"

    invoke-direct {v2, v3, v4, v0, v1}, Ljava/lang/StackTraceElement;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v2
.end method

.method public static a(Ljava/lang/reflect/Member;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 59
    if-nez p0, :cond_0

    .line 60
    sget-object v0, LbvG;->a:Ljava/lang/Object;

    .line 78
    :goto_0
    return-object v0

    .line 63
    :cond_0
    invoke-interface {p0}, Ljava/lang/reflect/Member;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v2

    .line 72
    const/4 v3, 0x0

    .line 73
    const/4 v4, -0x1

    .line 76
    invoke-static {p0}, LbvF;->a(Ljava/lang/reflect/Member;)Ljava/lang/Class;

    move-result-object v0

    .line 77
    const-class v1, Ljava/lang/reflect/Constructor;

    if-ne v0, v1, :cond_1

    const-string v0, "<init>"

    .line 78
    :goto_1
    new-instance v1, Ljava/lang/StackTraceElement;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0, v3, v4}, Ljava/lang/StackTraceElement;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    move-object v0, v1

    goto :goto_0

    .line 77
    :cond_1
    invoke-interface {p0}, Ljava/lang/reflect/Member;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
