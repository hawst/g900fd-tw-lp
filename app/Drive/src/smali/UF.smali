.class public final enum LUF;
.super Ljava/lang/Enum;
.source "OpenEntryLookupHelper.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LUF;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LUF;

.field private static final synthetic a:[LUF;

.field public static final enum b:LUF;

.field public static final enum c:LUF;

.field public static final enum d:LUF;


# instance fields
.field private final a:I

.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 37
    new-instance v0, LUF;

    const-string v1, "AUTH_ERROR"

    sget v2, Lxi;->open_url_authentication_error:I

    const-class v3, LbwO;

    invoke-direct {v0, v1, v4, v2, v3}, LUF;-><init>(Ljava/lang/String;IILjava/lang/Class;)V

    sput-object v0, LUF;->a:LUF;

    .line 42
    new-instance v0, LUF;

    const-string v1, "ACCESS_DENIED"

    sget v2, Lxi;->open_url_error_access_denied:I

    const-class v3, Lbxk;

    invoke-direct {v0, v1, v5, v2, v3}, LUF;-><init>(Ljava/lang/String;IILjava/lang/Class;)V

    sput-object v0, LUF;->b:LUF;

    .line 44
    new-instance v0, LUF;

    const-string v1, "IO_ERROR"

    sget v2, Lxi;->open_url_io_error:I

    const-class v3, Ljava/io/IOException;

    invoke-direct {v0, v1, v6, v2, v3}, LUF;-><init>(Ljava/lang/String;IILjava/lang/Class;)V

    sput-object v0, LUF;->c:LUF;

    .line 46
    new-instance v0, LUF;

    const-string v1, "INVALID_FEED"

    sget v2, Lxi;->open_url_io_error:I

    const-class v3, Ljava/text/ParseException;

    invoke-direct {v0, v1, v7, v2, v3}, LUF;-><init>(Ljava/lang/String;IILjava/lang/Class;)V

    sput-object v0, LUF;->d:LUF;

    .line 35
    const/4 v0, 0x4

    new-array v0, v0, [LUF;

    sget-object v1, LUF;->a:LUF;

    aput-object v1, v0, v4

    sget-object v1, LUF;->b:LUF;

    aput-object v1, v0, v5

    sget-object v1, LUF;->c:LUF;

    aput-object v1, v0, v6

    sget-object v1, LUF;->d:LUF;

    aput-object v1, v0, v7

    sput-object v0, LUF;->a:[LUF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/Throwable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 52
    iput p3, p0, LUF;->a:I

    .line 53
    iput-object p4, p0, LUF;->a:Ljava/lang/Class;

    .line 54
    return-void
.end method

.method public static final a(Ljava/lang/Throwable;)LUF;
    .locals 5

    .prologue
    .line 66
    invoke-static {}, LUF;->values()[LUF;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 67
    iget-object v4, v3, LUF;->a:Ljava/lang/Class;

    invoke-virtual {v4, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 68
    return-object v3

    .line 66
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 72
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Error looking up entry"

    invoke-direct {v0, v1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LUF;
    .locals 1

    .prologue
    .line 35
    const-class v0, LUF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LUF;

    return-object v0
.end method

.method public static values()[LUF;
    .locals 1

    .prologue
    .line 35
    sget-object v0, LUF;->a:[LUF;

    invoke-virtual {v0}, [LUF;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LUF;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, LUF;->a:I

    return v0
.end method
