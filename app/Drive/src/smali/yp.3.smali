.class public Lyp;
.super Ljava/lang/Object;
.source "CsiModule.java"

# interfaces
.implements LbuC;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/inject/Binder;)V
    .locals 0

    .prologue
    .line 46
    return-void
.end method

.method provideCsiRequestQueue(LQr;Landroid/app/Application;)Lamm;
    .locals 3
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .annotation runtime Lyw;
    .end annotation

    .prologue
    .line 31
    const-string v0, "csiOfflineRequestQueueCapacity"

    const/16 v1, 0x3e8

    invoke-interface {p1, v0, v1}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    .line 33
    new-instance v1, LamI;

    const-string v2, "CsiRequestQueue.db"

    invoke-direct {v1, v0, v2, p2}, LamI;-><init>(ILjava/lang/String;Landroid/content/Context;)V

    return-object v1
.end method

.method provideCsiSyncer(Lyr;)Lyq;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .prologue
    .line 40
    return-object p1
.end method
