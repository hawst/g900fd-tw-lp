.class LLG;
.super LLQ;
.source "SelectionFunctionDoPrint.java"


# instance fields
.field private final a:LPp;

.field private final a:LWL;


# direct methods
.method constructor <init>(LPp;LWL;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, LLQ;-><init>()V

    .line 24
    iput-object p1, p0, LLG;->a:LPp;

    .line 25
    iput-object p2, p0, LLG;->a:LWL;

    .line 26
    return-void
.end method


# virtual methods
.method public a(LaFM;LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFM;",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ")V"
        }
    .end annotation

    .prologue
    .line 40
    return-void
.end method

.method public a(Ljava/lang/Runnable;LaFM;LbmF;LbmF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "LaFM;",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-static {p3}, Lbnm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    .line 46
    iget-object v1, p0, LLG;->a:LPp;

    invoke-interface {v1, v0}, LPp;->b(LaGu;)V

    .line 47
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 48
    return-void
.end method

.method public a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 31
    invoke-super {p0, p1, p2}, LLQ;->a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 32
    const/4 v0, 0x0

    .line 35
    :goto_0
    return v0

    .line 34
    :cond_0
    invoke-static {p1}, Lbnm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    .line 35
    iget-object v1, p0, LLG;->a:LWL;

    invoke-interface {v1, v0}, LWL;->a(LaGu;)Z

    move-result v0

    goto :goto_0
.end method
