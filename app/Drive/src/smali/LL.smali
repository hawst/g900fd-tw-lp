.class public LLL;
.super LLQ;
.source "SelectionFunctionDoSend.java"


# instance fields
.field private final a:LPp;

.field private final a:LaKR;


# direct methods
.method constructor <init>(LPp;LaKR;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, LLQ;-><init>()V

    .line 24
    iput-object p1, p0, LLL;->a:LPp;

    .line 25
    iput-object p2, p0, LLL;->a:LaKR;

    .line 26
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Runnable;LaFM;LbmF;LbmF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "LaFM;",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-static {p3}, Lbnm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    .line 43
    iget-object v1, p0, LLL;->a:LPp;

    invoke-interface {v1, v0}, LPp;->d(LaGu;)V

    .line 44
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 45
    return-void
.end method

.method public a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 31
    invoke-super {p0, p1, p2}, LLQ;->a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 36
    :goto_0
    return v2

    .line 34
    :cond_0
    invoke-virtual {p1, v2}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    .line 35
    invoke-interface {v0}, LaGu;->f()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    move v3, v1

    .line 36
    :goto_1
    if-eqz v3, :cond_3

    invoke-interface {v0}, LaGu;->b()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-interface {v0}, LaGu;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LLL;->a:LaKR;

    invoke-interface {v0}, LaKR;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    move v0, v1

    :goto_2
    move v2, v0

    goto :goto_0

    :cond_2
    move v3, v2

    .line 35
    goto :goto_1

    :cond_3
    move v0, v2

    .line 36
    goto :goto_2
.end method
