.class public final enum LaEU;
.super Ljava/lang/Enum;
.source "ExposedContentTable.java"

# interfaces
.implements Lbjv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaEU;",
        ">;",
        "Lbjv",
        "<",
        "LaFr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaEU;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final synthetic a:[LaEU;

.field public static final enum b:LaEU;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum c:LaEU;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field private final a:LaFr;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/16 v11, 0x2f

    const/16 v10, 0x2e

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/16 v7, 0x2c

    .line 39
    new-instance v0, LaEU;

    const-string v1, "DOCUMENT_CONTENT_ID"

    .line 40
    invoke-static {}, LaET;->b()LaET;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "documentContentId"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 41
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    .line 42
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v4

    invoke-virtual {v3, v4}, LaFG;->a(LaFy;)LaFG;

    move-result-object v3

    invoke-virtual {v3}, LaFG;->a()LaFG;

    move-result-object v3

    .line 40
    invoke-virtual {v2, v7, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    .line 43
    invoke-virtual {v2, v10}, LaFt;->b(I)LaFt;

    move-result-object v2

    .line 44
    invoke-virtual {v2, v11, v7}, LaFt;->a(II)LaFt;

    move-result-object v2

    const/16 v3, 0x35

    invoke-virtual {v2, v3}, LaFt;->b(I)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, LaEU;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEU;->a:LaEU;

    .line 49
    new-instance v0, LaEU;

    const-string v1, "RANDOM_ID"

    .line 50
    invoke-static {}, LaET;->b()LaET;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "randomId"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 51
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    .line 52
    invoke-virtual {v3}, LaFG;->a()LaFG;

    move-result-object v3

    new-array v4, v9, [Ljava/lang/String;

    const-string v5, "documentContentId"

    aput-object v5, v4, v8

    invoke-virtual {v3, v4}, LaFG;->a([Ljava/lang/String;)LaFG;

    move-result-object v3

    .line 50
    invoke-virtual {v2, v7, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    .line 52
    invoke-virtual {v2, v10}, LaFt;->b(I)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "randomId"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 53
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/String;

    invoke-virtual {v3, v4}, LaFG;->a([Ljava/lang/String;)LaFG;

    move-result-object v3

    invoke-virtual {v2, v11, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    const/16 v3, 0x35

    invoke-virtual {v2, v3}, LaFt;->b(I)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, LaEU;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEU;->b:LaEU;

    .line 54
    new-instance v0, LaEU;

    const-string v1, "EXPIRY_TIME"

    const/4 v2, 0x2

    .line 56
    invoke-static {}, LaET;->b()LaET;

    move-result-object v3

    .line 55
    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "expiryTime"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 56
    invoke-virtual {v4}, LaFG;->b()LaFG;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    .line 57
    invoke-virtual {v3, v10}, LaFt;->b(I)LaFt;

    move-result-object v3

    invoke-virtual {v3, v11, v7}, LaFt;->a(II)LaFt;

    move-result-object v3

    const/16 v4, 0x35

    invoke-virtual {v3, v4}, LaFt;->b(I)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaEU;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaEU;->c:LaEU;

    .line 38
    const/4 v0, 0x3

    new-array v0, v0, [LaEU;

    sget-object v1, LaEU;->a:LaEU;

    aput-object v1, v0, v8

    sget-object v1, LaEU;->b:LaEU;

    aput-object v1, v0, v9

    const/4 v1, 0x2

    sget-object v2, LaEU;->c:LaEU;

    aput-object v2, v0, v1

    sput-object v0, LaEU;->a:[LaEU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaFt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 62
    invoke-virtual {p3}, LaFt;->a()LaFr;

    move-result-object v0

    iput-object v0, p0, LaEU;->a:LaFr;

    .line 63
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaEU;
    .locals 1

    .prologue
    .line 38
    const-class v0, LaEU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaEU;

    return-object v0
.end method

.method public static values()[LaEU;
    .locals 1

    .prologue
    .line 38
    sget-object v0, LaEU;->a:[LaEU;

    invoke-virtual {v0}, [LaEU;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaEU;

    return-object v0
.end method


# virtual methods
.method public a()LaFr;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, LaEU;->a:LaFr;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0}, LaEU;->a()LaFr;

    move-result-object v0

    return-object v0
.end method
