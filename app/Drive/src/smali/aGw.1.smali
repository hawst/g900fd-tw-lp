.class public final enum LaGw;
.super Ljava/lang/Enum;
.source "Entry.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaGw;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaGw;

.field private static final synthetic a:[LaGw;

.field public static final enum b:LaGw;

.field public static final enum c:LaGw;


# instance fields
.field private final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 43
    new-instance v0, LaGw;

    const-string v1, "NOT_PLUS_MEDIA_ITEM"

    invoke-direct {v0, v1, v2, v2}, LaGw;-><init>(Ljava/lang/String;II)V

    sput-object v0, LaGw;->a:LaGw;

    .line 44
    new-instance v0, LaGw;

    const-string v1, "PLUS_MEDIA_ROOT_FOLDER"

    invoke-direct {v0, v1, v3, v3}, LaGw;-><init>(Ljava/lang/String;II)V

    sput-object v0, LaGw;->b:LaGw;

    .line 45
    new-instance v0, LaGw;

    const-string v1, "PLUS_MEDIA_ITEM"

    invoke-direct {v0, v1, v4, v4}, LaGw;-><init>(Ljava/lang/String;II)V

    sput-object v0, LaGw;->c:LaGw;

    .line 42
    const/4 v0, 0x3

    new-array v0, v0, [LaGw;

    sget-object v1, LaGw;->a:LaGw;

    aput-object v1, v0, v2

    sget-object v1, LaGw;->b:LaGw;

    aput-object v1, v0, v3

    sget-object v1, LaGw;->c:LaGw;

    aput-object v1, v0, v4

    sput-object v0, LaGw;->a:[LaGw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 49
    iput p3, p0, LaGw;->a:I

    .line 50
    return-void
.end method

.method public static a(I)LaGw;
    .locals 5

    .prologue
    .line 60
    invoke-static {}, LaGw;->values()[LaGw;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 61
    invoke-virtual {v3}, LaGw;->a()I

    move-result v4

    if-ne v4, p0, :cond_0

    .line 62
    return-object v3

    .line 60
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 65
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LaGw;
    .locals 1

    .prologue
    .line 42
    const-class v0, LaGw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaGw;

    return-object v0
.end method

.method public static values()[LaGw;
    .locals 1

    .prologue
    .line 42
    sget-object v0, LaGw;->a:[LaGw;

    invoke-virtual {v0}, [LaGw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaGw;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, LaGw;->a:I

    return v0
.end method
