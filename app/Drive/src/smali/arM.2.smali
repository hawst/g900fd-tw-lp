.class public LarM;
.super Ljava/lang/Object;
.source "ViewIterator.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;LarN;)V
    .locals 2

    .prologue
    .line 22
    invoke-interface {p2, p1}, LarN;->a(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 32
    :cond_0
    return-void

    .line 26
    :cond_1
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 27
    check-cast p1, Landroid/view/ViewGroup;

    .line 28
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 29
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1, p2}, LarM;->a(Landroid/view/View;LarN;)V

    .line 28
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
