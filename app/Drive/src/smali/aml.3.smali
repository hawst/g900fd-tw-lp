.class public Laml;
.super Ljava/lang/Object;
.source "PackageInfoHelper.java"


# static fields
.field private static a:Landroid/content/pm/PackageInfo;

.field private static a:Lrx;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lrx;->e:Lrx;

    sput-object v0, Laml;->a:Lrx;

    .line 25
    const/4 v0, 0x0

    sput-object v0, Laml;->a:Landroid/content/pm/PackageInfo;

    return-void
.end method

.method public static declared-synchronized a()I
    .locals 2

    .prologue
    .line 80
    const-class v1, Laml;

    monitor-enter v1

    :try_start_0
    sget-object v0, Laml;->a:Landroid/content/pm/PackageInfo;

    if-eqz v0, :cond_0

    sget-object v0, Laml;->a:Landroid/content/pm/PackageInfo;

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 73
    const-class v1, Laml;

    monitor-enter v1

    :try_start_0
    sget-object v0, Laml;->a:Landroid/content/pm/PackageInfo;

    if-eqz v0, :cond_0

    sget-object v0, Laml;->a:Landroid/content/pm/PackageInfo;

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    :try_start_1
    const-string v0, "unknown"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a()Lrx;
    .locals 2

    .prologue
    .line 66
    const-class v0, Laml;

    monitor-enter v0

    :try_start_0
    sget-object v1, Laml;->a:Lrx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized a(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 35
    const-class v2, Laml;

    monitor-enter v2

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 38
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    move-object v1, v0

    .line 43
    :goto_0
    if-eqz v1, :cond_0

    :try_start_2
    iget-object v0, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-static {v0}, Lrx;->a(Ljava/lang/String;)Lrx;

    move-result-object v0

    :goto_1
    sput-object v0, Laml;->a:Lrx;

    .line 45
    sput-object v1, Laml;->a:Landroid/content/pm/PackageInfo;

    .line 46
    const-string v0, "PackageInfoHelper"

    const-string v1, "Provided clientMode=%s, packageInfo=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Laml;->a:Lrx;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Laml;->a:Landroid/content/pm/PackageInfo;

    aput-object v5, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->c(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 47
    monitor-exit v2

    return-void

    .line 39
    :catch_0
    move-exception v0

    .line 40
    :try_start_3
    const-string v0, "PackageInfoHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t get info for package: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    .line 43
    :cond_0
    sget-object v0, Lrx;->a:Lrx;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 35
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public static declared-synchronized b()I
    .locals 2

    .prologue
    .line 85
    const-class v1, Laml;

    monitor-enter v1

    :try_start_0
    sget-object v0, Laml;->a:Landroid/content/pm/PackageInfo;

    if-eqz v0, :cond_0

    sget-object v0, Laml;->a:Landroid/content/pm/PackageInfo;

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->icon:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
