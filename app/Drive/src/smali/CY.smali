.class public LCY;
.super Ljava/lang/Object;
.source "SelectionModeActionMenuImpl.java"

# interfaces
.implements LCV;


# instance fields
.field private final a:LCW;

.field private final a:LCX;

.field private final a:Landroid/app/Activity;

.field a:Landroid/view/ActionMode;


# direct methods
.method public constructor <init>(LCW;LCX;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, LCY;->a:LCW;

    .line 52
    iput-object p2, p0, LCY;->a:LCX;

    .line 53
    iput-object p3, p0, LCY;->a:Landroid/app/Activity;

    .line 54
    return-void
.end method

.method static synthetic a(LCY;Landroid/view/ActionMode;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, LCY;->a(Landroid/view/ActionMode;)V

    return-void
.end method

.method private a(Landroid/view/ActionMode;)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, LCY;->a:LCX;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, LCY;->a:LCX;

    invoke-interface {v0}, LCX;->l()V

    .line 95
    :cond_0
    monitor-enter p0

    .line 96
    if-eqz p1, :cond_1

    :try_start_0
    iget-object v0, p0, LCY;->a:Landroid/view/ActionMode;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 97
    const/4 v0, 0x0

    iput-object v0, p0, LCY;->a:Landroid/view/ActionMode;

    .line 99
    :cond_1
    monitor-exit p0

    .line 100
    return-void

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic a(LCY;Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, LCY;->a(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method private a(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 60
    monitor-enter p0

    .line 61
    :try_start_0
    iput-object p1, p0, LCY;->a:Landroid/view/ActionMode;

    .line 62
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    iget-object v0, p0, LCY;->a:LCW;

    invoke-interface {v0, p2}, LCW;->a(Landroid/view/Menu;)V

    .line 65
    const/4 v0, 0x1

    return v0

    .line 62
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic b(LCY;Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, LCY;->b(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method private b(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, LCY;->a:LCW;

    invoke-interface {v0, p2}, LCW;->b_(Landroid/view/Menu;)V

    .line 73
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 149
    monitor-enter p0

    .line 150
    :try_start_0
    iget-object v0, p0, LCY;->a:Landroid/view/ActionMode;

    if-nez v0, :cond_0

    .line 151
    monitor-exit p0

    .line 156
    :goto_0
    return-void

    .line 154
    :cond_0
    iget-object v0, p0, LCY;->a:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    .line 155
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized a()Z
    .locals 1

    .prologue
    .line 160
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LCY;->a:Landroid/view/ActionMode;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method a(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, LCY;->a:LCX;

    if-nez v0, :cond_0

    .line 82
    const/4 v0, 0x0

    .line 84
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LCY;->a:LCX;

    invoke-interface {v0, p2}, LCX;->a(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 110
    monitor-enter p0

    .line 111
    :try_start_0
    iget-object v0, p0, LCY;->a:Landroid/app/Activity;

    new-instance v1, LCZ;

    invoke-direct {v1, p0}, LCZ;-><init>(LCY;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, LCY;->a:Landroid/view/ActionMode;

    .line 132
    monitor-exit p0

    .line 133
    return-void

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 137
    monitor-enter p0

    .line 138
    :try_start_0
    iget-object v0, p0, LCY;->a:Landroid/view/ActionMode;

    if-nez v0, :cond_0

    .line 139
    monitor-exit p0

    .line 145
    :goto_0
    return-void

    .line 142
    :cond_0
    iget-object v0, p0, LCY;->a:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 143
    const/4 v0, 0x0

    iput-object v0, p0, LCY;->a:Landroid/view/ActionMode;

    .line 144
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
