.class LMQ;
.super Ljava/io/FilterInputStream;
.source "CancelableInputStream.java"


# instance fields
.field private final a:LMR;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;LMR;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 28
    iput-object p2, p0, LMQ;->a:LMR;

    .line 29
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, LMQ;->a:LMR;

    invoke-interface {v0}, LMR;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    new-instance v0, LMS;

    invoke-direct {v0}, LMS;-><init>()V

    throw v0

    .line 57
    :cond_0
    return-void
.end method


# virtual methods
.method public read()I
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, LMQ;->a()V

    .line 34
    invoke-super {p0}, Ljava/io/FilterInputStream;->read()I

    move-result v0

    .line 35
    invoke-direct {p0}, LMQ;->a()V

    .line 36
    return v0
.end method

.method public read([B)I
    .locals 2

    .prologue
    .line 49
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, LMQ;->read([BII)I

    move-result v0

    .line 50
    return v0
.end method

.method public read([BII)I
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, LMQ;->a()V

    .line 42
    invoke-super {p0, p1, p2, p3}, Ljava/io/FilterInputStream;->read([BII)I

    move-result v0

    .line 43
    invoke-direct {p0}, LMQ;->a()V

    .line 44
    return v0
.end method
