.class public final Lbsy;
.super Ljava/lang/Object;
.source "Ints.java"


# direct methods
.method public static a(I)I
    .locals 0

    .prologue
    .line 75
    return p0
.end method

.method public static a(II)I
    .locals 1

    .prologue
    .line 120
    if-ge p0, p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    if-le p0, p1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(J)I
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 87
    long-to-int v3, p0

    .line 88
    int-to-long v4, v3

    cmp-long v0, v4, p0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "Out of range: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-static {v0, v4, v1}, LbiT;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 89
    return v3

    :cond_0
    move v0, v2

    .line 88
    goto :goto_0
.end method

.method static synthetic a([IIII)I
    .locals 1

    .prologue
    .line 51
    invoke-static {p0, p1, p2, p3}, Lbsy;->c([IIII)I

    move-result v0

    return v0
.end method

.method public static varargs a([I)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 456
    array-length v0, p0

    if-nez v0, :cond_0

    .line 457
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 459
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lbsz;

    invoke-direct {v0, p0}, Lbsz;-><init>([I)V

    goto :goto_0
.end method

.method public static b(J)I
    .locals 2

    .prologue
    .line 101
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p0, v0

    if-lez v0, :cond_0

    .line 102
    const v0, 0x7fffffff

    .line 107
    :goto_0
    return v0

    .line 104
    :cond_0
    const-wide/32 v0, -0x80000000

    cmp-long v0, p0, v0

    if-gez v0, :cond_1

    .line 105
    const/high16 v0, -0x80000000

    goto :goto_0

    .line 107
    :cond_1
    long-to-int v0, p0

    goto :goto_0
.end method

.method static synthetic b([IIII)I
    .locals 1

    .prologue
    .line 51
    invoke-static {p0, p1, p2, p3}, Lbsy;->d([IIII)I

    move-result v0

    return v0
.end method

.method private static c([IIII)I
    .locals 2

    .prologue
    .line 156
    move v0, p2

    :goto_0
    if-ge v0, p3, :cond_1

    .line 157
    aget v1, p0, v0

    if-ne v1, p1, :cond_0

    .line 161
    :goto_1
    return v0

    .line 156
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 161
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private static d([IIII)I
    .locals 2

    .prologue
    .line 208
    add-int/lit8 v0, p3, -0x1

    :goto_0
    if-lt v0, p2, :cond_1

    .line 209
    aget v1, p0, v0

    if-ne v1, p1, :cond_0

    .line 213
    :goto_1
    return v0

    .line 208
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 213
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method
