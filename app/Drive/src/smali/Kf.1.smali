.class public LKf;
.super LKS;
.source "ActionCardFunctionManager.java"


# instance fields
.field private a:I

.field private final a:LCU;

.field private final a:LKj;

.field private final a:LaGR;

.field private a:Landroid/view/View;

.field private a:Landroid/widget/LinearLayout;

.field private a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "LaGu;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/Runnable;

.field private b:I

.field private b:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "LKP;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LtK;LaGR;LCU;LKU;)V
    .locals 18

    .prologue
    .line 189
    invoke-direct/range {p0 .. p0}, LKS;-><init>()V

    .line 165
    new-instance v1, LKj;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, LKj;-><init>(LKg;)V

    move-object/from16 v0, p0

    iput-object v1, v0, LKf;->a:LKj;

    .line 168
    new-instance v1, LKg;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, LKg;-><init>(LKf;)V

    move-object/from16 v0, p0

    iput-object v1, v0, LKf;->a:Ljava/lang/Runnable;

    .line 176
    invoke-static {}, LbmF;->c()LbmF;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, LKf;->a:LbmF;

    .line 177
    invoke-static {}, LbmF;->c()LbmF;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, LKf;->b:LbmF;

    .line 181
    const/4 v1, 0x4

    move-object/from16 v0, p0

    iput v1, v0, LKf;->a:I

    .line 182
    const/4 v1, 0x2

    move-object/from16 v0, p0

    iput v1, v0, LKf;->b:I

    .line 190
    invoke-static/range {p2 .. p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGR;

    move-object/from16 v0, p0

    iput-object v1, v0, LKf;->a:LaGR;

    .line 191
    invoke-static/range {p3 .. p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LCU;

    move-object/from16 v0, p0

    iput-object v1, v0, LKf;->a:LCU;

    .line 192
    invoke-static/range {p4 .. p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    new-instance v1, LKk;

    move-object/from16 v0, p4

    iget-object v3, v0, LKU;->a:LLM;

    sget v4, Lxb;->ic_addpeople_alpha:I

    sget v5, Lxi;->action_card_share:I

    sget v6, Lxi;->action_card_share_content_desc:I

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v6}, LKk;-><init>(LKf;LLw;III)V

    .line 201
    new-instance v2, LKk;

    move-object/from16 v0, p4

    iget-object v4, v0, LKU;->a:LLK;

    sget v5, Lxb;->ic_rename_alpha:I

    sget v6, Lxi;->action_card_rename:I

    sget v7, Lxi;->action_card_rename_content_desc:I

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, LKk;-><init>(LKf;LLw;III)V

    .line 208
    new-instance v3, LKk;

    move-object/from16 v0, p4

    iget-object v5, v0, LKU;->a:LLP;

    sget v6, Lxb;->ic_starred_alpha:I

    sget v7, Lxi;->action_card_unstar:I

    sget v8, Lxi;->action_card_unstar_content_desc:I

    move-object/from16 v0, p4

    iget-object v9, v0, LKU;->a:LLN;

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v9}, LKk;-><init>(LKf;LLw;IIILjava/lang/Object;)V

    .line 216
    new-instance v4, LKk;

    move-object/from16 v0, p4

    iget-object v6, v0, LKU;->a:LLN;

    sget v7, Lxb;->ic_unstarred_alpha:I

    sget v8, Lxi;->action_card_star:I

    sget v9, Lxi;->action_card_star_content_desc:I

    move-object/from16 v5, p0

    invoke-direct/range {v4 .. v9}, LKk;-><init>(LKf;LLw;III)V

    .line 223
    new-instance v14, LKR;

    const/4 v5, 0x2

    new-array v5, v5, [LKP;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    const/4 v4, 0x1

    aput-object v3, v5, v4

    invoke-direct {v14, v5}, LKR;-><init>([LKP;)V

    .line 226
    sget-object v3, Lry;->U:Lry;

    .line 227
    move-object/from16 v0, p1

    invoke-interface {v0, v3}, LtK;->a(LtJ;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v0, p4

    iget-object v5, v0, LKU;->a:LLH;

    .line 229
    :goto_0
    new-instance v3, LKk;

    sget v6, Lxb;->ic_delete_alpha:I

    sget v7, Lxi;->action_card_remove:I

    sget v8, Lxi;->action_card_remove_content_desc:I

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v8}, LKk;-><init>(LKf;LLw;III)V

    .line 236
    new-instance v4, LKk;

    move-object/from16 v0, p4

    iget-object v6, v0, LKU;->a:LLL;

    sget v7, Lxb;->ic_send_file_alpha:I

    sget v8, Lxi;->action_card_export:I

    sget v9, Lxi;->action_card_export_content_desc:I

    move-object/from16 v5, p0

    invoke-direct/range {v4 .. v9}, LKk;-><init>(LKf;LLw;III)V

    .line 243
    new-instance v5, LKk;

    move-object/from16 v0, p4

    iget-object v7, v0, LKU;->a:LLG;

    sget v8, Lxb;->ic_print_alpha:I

    sget v9, Lxi;->action_card_print:I

    sget v10, Lxi;->action_card_print_content_desc:I

    move-object/from16 v6, p0

    invoke-direct/range {v5 .. v10}, LKk;-><init>(LKf;LLw;III)V

    .line 250
    new-instance v6, LKk;

    move-object/from16 v0, p4

    iget-object v8, v0, LKU;->a:LLy;

    sget v9, Lxb;->ic_link_alpha:I

    sget v10, Lxi;->action_card_share_link:I

    sget v11, Lxi;->action_card_share_link_content_desc:I

    move-object/from16 v7, p0

    invoke-direct/range {v6 .. v11}, LKk;-><init>(LKf;LLw;III)V

    .line 261
    new-instance v7, LKk;

    move-object/from16 v0, p4

    iget-object v9, v0, LKU;->a:LLz;

    sget v10, Lxb;->ic_move_alpha:I

    sget v11, Lxi;->action_card_move:I

    sget v12, Lxi;->action_card_move_content_desc:I

    move-object/from16 v8, p0

    invoke-direct/range {v7 .. v12}, LKk;-><init>(LKf;LLw;III)V

    .line 268
    new-instance v8, LKk;

    move-object/from16 v0, p4

    iget-object v10, v0, LKU;->a:LLx;

    sget v11, Lxb;->ic_download_alpha:I

    sget v12, Lxi;->action_card_download:I

    sget v13, Lxi;->action_card_download_content_desc:I

    move-object/from16 v9, p0

    invoke-direct/range {v8 .. v13}, LKk;-><init>(LKf;LLw;III)V

    move-object v9, v1

    move-object v10, v6

    move-object v11, v14

    move-object v12, v7

    move-object v13, v4

    move-object v14, v8

    move-object v15, v2

    move-object/from16 v16, v3

    move-object/from16 v17, v5

    .line 278
    invoke-static/range {v9 .. v17}, LbmF;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmF;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, LKf;->c:LbmF;

    .line 288
    return-void

    .line 227
    :cond_0
    move-object/from16 v0, p4

    iget-object v5, v0, LKU;->a:LLI;

    goto :goto_0
.end method

.method private a()Landroid/view/ViewGroup;
    .locals 5

    .prologue
    .line 462
    iget-object v0, p0, LKf;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 463
    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 466
    iget-object v1, p0, LKf;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    .line 467
    sget v2, Lxe;->action_card_row_template:I

    iget-object v3, p0, LKf;->a:Landroid/widget/LinearLayout;

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 468
    iget-object v0, p0, LKf;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private a(LbmF;)Landroid/view/ViewGroup;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LKP;",
            ">;)",
            "Landroid/view/ViewGroup;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 436
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 437
    invoke-virtual {p1}, LbmF;->size()I

    move-result v0

    invoke-virtual {p0}, LKf;->a()I

    move-result v1

    if-gt v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 442
    invoke-virtual {p1}, LbmF;->a()Lbqv;

    move-result-object v3

    move-object v1, v2

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKP;

    .line 443
    if-nez v1, :cond_1

    .line 444
    invoke-direct {p0}, LKf;->a()Landroid/view/ViewGroup;

    move-result-object v1

    .line 446
    :cond_1
    invoke-interface {v0, v1, v2}, LKP;->a(Landroid/view/ViewGroup;Landroid/view/View$OnTouchListener;)Landroid/view/View;

    move-result-object v4

    .line 447
    iget-object v5, p0, LKf;->a:LKj;

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 448
    invoke-interface {v0}, LKP;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 450
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    iget v4, p0, LKf;->a:I

    if-lt v0, v4, :cond_0

    move-object v1, v2

    .line 451
    goto :goto_1

    .line 437
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 455
    :cond_3
    return-object v1
.end method

.method static synthetic a(LKf;)LbmF;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, LKf;->a:LbmF;

    return-object v0
.end method

.method private a()LbmY;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmY",
            "<",
            "LKP;",
            ">;"
        }
    .end annotation

    .prologue
    .line 475
    invoke-static {}, LbmY;->a()Lbna;

    move-result-object v1

    .line 476
    iget-object v0, p0, LKf;->c:LbmF;

    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKP;

    .line 477
    iget-object v3, p0, LKf;->a:LbmF;

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, LKP;->a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 478
    invoke-virtual {v1, v0}, Lbna;->a(Ljava/lang/Object;)Lbna;

    goto :goto_0

    .line 481
    :cond_1
    invoke-virtual {v1}, Lbna;->a()LbmY;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LKf;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, LKf;->a:Ljava/lang/Runnable;

    return-object v0
.end method

.method private a()V
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 339
    invoke-virtual {p0}, LKf;->a()I

    move-result v0

    .line 342
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 343
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v7

    .line 346
    iget-object v1, p0, LKf;->c:LbmF;

    invoke-virtual {v1}, LbmF;->a()Lbqv;

    move-result-object v8

    move v1, v2

    move v3, v2

    move v4, v0

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKP;

    .line 347
    invoke-interface {v0}, LKP;->a()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 351
    if-lez v4, :cond_1

    .line 352
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 353
    add-int/lit8 v4, v4, -0x1

    move v0, v1

    move v1, v3

    move v3, v4

    :goto_1
    move v4, v3

    move v3, v1

    move v1, v0

    .line 368
    goto :goto_0

    .line 356
    :cond_1
    invoke-virtual {v7, v0}, LbmH;->a(Ljava/lang/Object;)LbmH;

    .line 357
    if-nez v3, :cond_7

    .line 359
    if-gtz v4, :cond_2

    .line 361
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v6, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKP;

    .line 362
    invoke-virtual {v7, v0}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move v0, v5

    move v1, v5

    move v3, v4

    .line 363
    goto :goto_1

    .line 364
    :cond_2
    add-int/lit8 v4, v4, -0x1

    move v0, v5

    move v1, v5

    move v3, v4

    goto :goto_1

    .line 370
    :cond_3
    invoke-static {v6}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    invoke-direct {p0, v0}, LKf;->a(LbmF;)Landroid/view/ViewGroup;

    move-result-object v3

    .line 371
    if-eqz v1, :cond_4

    .line 372
    invoke-virtual {v7}, LbmH;->a()LbmF;

    move-result-object v0

    invoke-direct {p0, v0, v3}, LKf;->a(LbmF;Landroid/view/ViewGroup;)V

    .line 376
    :cond_4
    if-eqz v3, :cond_6

    .line 377
    iget v0, p0, LKf;->a:I

    .line 378
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v4

    if-eqz v1, :cond_5

    :goto_2
    add-int v1, v4, v5

    iget v4, p0, LKf;->a:I

    rem-int/2addr v1, v4

    sub-int/2addr v0, v1

    .line 379
    iget v1, p0, LKf;->a:I

    rem-int v1, v0, v1

    .line 380
    if-lez v1, :cond_6

    .line 381
    iget-object v0, p0, LKf;->a:Landroid/widget/LinearLayout;

    .line 382
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v4, "layout_inflater"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 385
    :goto_3
    if-ge v2, v1, :cond_6

    .line 386
    sget v4, Lxe;->action_card_button_placeholder:I

    invoke-virtual {v0, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 385
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_5
    move v5, v2

    .line 378
    goto :goto_2

    .line 390
    :cond_6
    return-void

    :cond_7
    move v0, v5

    move v1, v3

    move v3, v4

    goto :goto_1
.end method

.method private a(LbmF;Landroid/view/ViewGroup;)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LKP;",
            ">;",
            "Landroid/view/ViewGroup;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 397
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 398
    invoke-virtual {p1}, LbmF;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 399
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 401
    iget-object v0, p0, LKf;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 402
    const-string v0, "layout_inflater"

    .line 403
    invoke-virtual {v2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 405
    sget v3, Lxe;->action_card_overflow_menu_button:I

    invoke-virtual {v0, v3, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 406
    sget v0, Lxc;->action_card_overflow_icon:I

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 408
    new-instance v3, Landroid/widget/ListPopupWindow;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    .line 410
    new-instance v4, LKT;

    invoke-direct {v4, v3, p1}, LKT;-><init>(Landroid/widget/ListPopupWindow;LbmF;)V

    .line 411
    invoke-virtual {v3, v4}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 412
    invoke-virtual {v3, v1}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    .line 414
    iget-object v4, p0, LKf;->a:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 416
    new-instance v4, LKh;

    invoke-direct {v4, p0, v3}, LKh;-><init>(LKf;Landroid/widget/ListPopupWindow;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 423
    invoke-static {v2}, Lamt;->a(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 424
    invoke-virtual {v0}, Landroid/widget/Button;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v1

    .line 425
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, LwZ;->m_icon_secondary_tint:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 426
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 428
    :cond_0
    return-void

    .line 398
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(LKf;)LbmF;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, LKf;->b:LbmF;

    return-object v0
.end method


# virtual methods
.method protected a()I
    .locals 2

    .prologue
    .line 292
    iget v0, p0, LKf;->a:I

    iget v1, p0, LKf;->b:I

    mul-int/2addr v0, v1

    return v0
.end method

.method public a(LLw;)V
    .locals 2

    .prologue
    .line 299
    iget-object v0, p0, LKf;->a:LbmF;

    invoke-virtual {v0}, LbmF;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, LKf;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 306
    :goto_0
    return-void

    .line 303
    :cond_0
    new-instance v0, LKi;

    iget-object v1, p0, LKf;->a:LCU;

    .line 304
    invoke-virtual {v1}, LCU;->a()Lcom/google/android/apps/docs/doclist/SelectionItem;

    move-result-object v1

    invoke-direct {v0, p0, p1, v1}, LKi;-><init>(LKf;LLw;Lcom/google/android/apps/docs/doclist/SelectionItem;)V

    .line 305
    iget-object v1, p0, LKf;->a:LaGR;

    invoke-virtual {v1, v0}, LaGR;->a(LaGN;)V

    goto :goto_0
.end method

.method public a(LaGu;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 312
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 313
    invoke-static {p1}, LbmF;->a(Ljava/lang/Object;)LbmF;

    move-result-object v1

    iput-object v1, p0, LKf;->a:LbmF;

    .line 314
    new-instance v1, Lcom/google/android/apps/docs/doclist/SelectionItem;

    .line 315
    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-interface {p1}, LaGu;->j()Z

    move-result v3

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/apps/docs/doclist/SelectionItem;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;ZLcom/google/android/apps/docs/doclist/SelectionItem;)V

    .line 316
    invoke-static {v1}, LbmF;->a(Ljava/lang/Object;)LbmF;

    move-result-object v1

    iput-object v1, p0, LKf;->b:LbmF;

    .line 317
    invoke-direct {p0}, LKf;->a()LbmY;

    .line 321
    iget-object v1, p0, LKf;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->findFocus()Landroid/view/View;

    move-result-object v1

    .line 322
    if-eqz v1, :cond_0

    .line 323
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 325
    :cond_0
    iget-object v1, p0, LKf;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 326
    invoke-direct {p0}, LKf;->a()V

    .line 327
    if-eqz v0, :cond_1

    .line 328
    iget-object v1, p0, LKf;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 329
    if-eqz v0, :cond_1

    .line 330
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 333
    :cond_1
    return-void
.end method

.method public a(Landroid/widget/LinearLayout;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 488
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LKf;->a:Landroid/widget/LinearLayout;

    .line 489
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, LKf;->a:Landroid/view/View;

    .line 490
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 491
    sget v1, Lxd;->action_card_max_items_per_row:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, LKf;->a:I

    .line 492
    sget v1, Lxd;->action_card_rows:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, LKf;->b:I

    .line 493
    return-void
.end method
