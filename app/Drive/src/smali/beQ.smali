.class public LbeQ;
.super Ljava/lang/Object;
.source "JsonObjectParser.java"

# interfaces
.implements LbfL;


# instance fields
.field private final a:LbeO;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(LbeR;)V
    .locals 2

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iget-object v0, p1, LbeR;->a:LbeO;

    iput-object v0, p0, LbeQ;->a:LbeO;

    .line 75
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p1, LbeR;->a:Ljava/util/Collection;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LbeQ;->a:Ljava/util/Set;

    .line 76
    return-void
.end method

.method private a(LbeS;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 121
    iget-object v2, p0, LbeQ;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 135
    :goto_0
    return-void

    .line 126
    :cond_0
    :try_start_0
    iget-object v2, p0, LbeQ;->a:Ljava/util/Set;

    invoke-virtual {p1, v2}, LbeS;->a(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v2

    .line 127
    if-eqz v2, :cond_1

    invoke-virtual {p1}, LbeS;->b()LbeX;

    move-result-object v2

    sget-object v3, LbeX;->d:LbeX;

    if-eq v2, v3, :cond_1

    :goto_1
    const-string v1, "wrapper key(s) not found: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LbeQ;->a:Ljava/util/Set;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LbfN;->a(ZLjava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 131
    :catchall_0
    move-exception v0

    .line 132
    invoke-virtual {p1}, LbeS;->a()V

    throw v0

    :cond_1
    move v0, v1

    .line 127
    goto :goto_1
.end method


# virtual methods
.method public final a()LbeO;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, LbeQ;->a:LbeO;

    return-object v0
.end method

.method public a(Ljava/io/InputStream;Ljava/nio/charset/Charset;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/io/InputStream;",
            "Ljava/nio/charset/Charset;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 81
    invoke-virtual {p0, p1, p2, p3}, LbeQ;->a(Ljava/io/InputStream;Ljava/nio/charset/Charset;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/io/InputStream;Ljava/nio/charset/Charset;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, LbeQ;->a:LbeO;

    invoke-virtual {v0, p1, p2}, LbeO;->a(Ljava/io/InputStream;Ljava/nio/charset/Charset;)LbeS;

    move-result-object v0

    .line 86
    invoke-direct {p0, v0}, LbeQ;->a(LbeS;)V

    .line 87
    const/4 v1, 0x1

    invoke-virtual {v0, p3, v1}, LbeS;->a(Ljava/lang/reflect/Type;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, LbeQ;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
