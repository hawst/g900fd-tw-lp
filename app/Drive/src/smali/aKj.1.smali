.class public final LaKj;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaKi;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaKd;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaKg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 39
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 40
    iput-object p1, p0, LaKj;->a:LbrA;

    .line 41
    const-class v0, LaKi;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LaKj;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaKj;->a:Lbsk;

    .line 44
    const-class v0, LaKd;

    invoke-static {v0, v2}, LaKj;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaKj;->b:Lbsk;

    .line 47
    const-class v0, LaKg;

    invoke-static {v0, v2}, LaKj;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaKj;->c:Lbsk;

    .line 50
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 74
    sparse-switch p1, :sswitch_data_0

    .line 112
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :sswitch_0
    new-instance v1, LaKi;

    iget-object v0, p0, LaKj;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->k:Lbsk;

    .line 79
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LaKj;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LTk;

    iget-object v2, v2, LTk;->k:Lbsk;

    .line 77
    invoke-static {v0, v2}, LaKj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTd;

    invoke-direct {v1, v0}, LaKi;-><init>(LTd;)V

    move-object v0, v1

    .line 110
    :goto_0
    return-object v0

    .line 86
    :sswitch_1
    new-instance v0, LaKd;

    invoke-direct {v0}, LaKd;-><init>()V

    goto :goto_0

    .line 90
    :sswitch_2
    new-instance v3, LaKg;

    iget-object v0, p0, LaKj;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaKj;

    iget-object v0, v0, LaKj;->a:Lbsk;

    .line 93
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaKj;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaKj;

    iget-object v1, v1, LaKj;->a:Lbsk;

    .line 91
    invoke-static {v0, v1}, LaKj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKi;

    iget-object v1, p0, LaKj;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaJW;

    iget-object v1, v1, LaJW;->c:Lbsk;

    .line 99
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LaKj;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaJW;

    iget-object v2, v2, LaJW;->c:Lbsk;

    .line 97
    invoke-static {v1, v2}, LaKj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaJR;

    iget-object v2, p0, LaKj;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 105
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LaKj;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LQH;

    iget-object v4, v4, LQH;->d:Lbsk;

    .line 103
    invoke-static {v2, v4}, LaKj;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LQr;

    invoke-direct {v3, v0, v1, v2}, LaKg;-><init>(LaKi;LaJR;LQr;)V

    move-object v0, v3

    .line 110
    goto :goto_0

    .line 74
    :sswitch_data_0
    .sparse-switch
        0xa0 -> :sswitch_2
        0xa3 -> :sswitch_1
        0x461 -> :sswitch_0
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 127
    .line 129
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 57
    const-class v0, LaKi;

    iget-object v1, p0, LaKj;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LaKj;->a(Ljava/lang/Class;Lbsk;)V

    .line 58
    const-class v0, LaKd;

    iget-object v1, p0, LaKj;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LaKj;->a(Ljava/lang/Class;Lbsk;)V

    .line 59
    const-class v0, LaKg;

    iget-object v1, p0, LaKj;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LaKj;->a(Ljava/lang/Class;Lbsk;)V

    .line 60
    iget-object v0, p0, LaKj;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x461

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 62
    iget-object v0, p0, LaKj;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xa3

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 64
    iget-object v0, p0, LaKj;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xa0

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 66
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 119
    .line 121
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 70
    return-void
.end method
