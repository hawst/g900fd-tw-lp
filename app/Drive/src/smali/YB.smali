.class LYB;
.super Ljava/lang/Object;
.source "PunchThumbnailGeneratorFactoryImpl.java"


# instance fields
.field private final a:I

.field final synthetic a:LYA;

.field private final a:Landroid/webkit/WebView;

.field private a:Z

.field private b:I

.field private volatile b:Z

.field private c:I

.field private volatile c:Z

.field private d:Z


# direct methods
.method private constructor <init>(LYA;Landroid/webkit/WebView;I)V
    .locals 3

    .prologue
    .line 133
    iput-object p1, p0, LYB;->a:LYA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    const/4 v0, 0x0

    iput-boolean v0, p0, LYB;->a:Z

    .line 134
    iput-object p2, p0, LYB;->a:Landroid/webkit/WebView;

    .line 135
    iput p3, p0, LYB;->a:I

    .line 136
    iget-object v0, p0, LYB;->a:Landroid/webkit/WebView;

    invoke-static {}, LYy;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 139
    iget-object v0, p0, LYB;->a:Landroid/webkit/WebView;

    new-instance v1, LYC;

    invoke-direct {v1, p0, p1, p3}, LYC;-><init>(LYB;LYA;I)V

    const-string v2, "ThumbnailApi"

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    new-instance v0, LYD;

    invoke-direct {v0, p0, p1, p3}, LYD;-><init>(LYB;LYA;I)V

    .line 176
    invoke-virtual {p2, v0}, Landroid/webkit/WebView;->setPictureListener(Landroid/webkit/WebView$PictureListener;)V

    .line 179
    new-instance v0, LYF;

    invoke-direct {v0, p0, p1, p3}, LYF;-><init>(LYB;LYA;I)V

    invoke-virtual {p2, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 193
    new-instance v0, LYG;

    invoke-direct {v0, p0, p1}, LYG;-><init>(LYB;LYA;)V

    invoke-virtual {p2, v0}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 199
    return-void
.end method

.method synthetic constructor <init>(LYA;Landroid/webkit/WebView;ILYz;)V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0, p1, p2, p3}, LYB;-><init>(LYA;Landroid/webkit/WebView;I)V

    return-void
.end method

.method private a()I
    .locals 7

    .prologue
    const/4 v2, -0x1

    .line 232
    iget-object v0, p0, LYB;->a:LYA;

    invoke-static {v0}, LYA;->c(LYA;)I

    move-result v0

    .line 233
    iget-object v1, p0, LYB;->a:LYA;

    iget-object v1, v1, LYA;->a:LYy;

    iget v1, v1, LYy;->a:I

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 234
    iget-object v1, p0, LYB;->a:LYA;

    iget-object v1, v1, LYA;->a:LYy;

    invoke-static {v1}, LYy;->a(LYy;)LXz;

    move-result-object v1

    invoke-interface {v1}, LXz;->c()I

    move-result v1

    iget-object v3, p0, LYB;->a:LYA;

    iget-object v3, v3, LYA;->a:LYy;

    iget v3, v3, LYy;->a:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v1, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 238
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LYB;->a:LYA;

    iget-object v1, v1, LYA;->a:LYy;

    iget v1, v1, LYy;->a:I

    if-ge v0, v1, :cond_3

    move v3, v2

    .line 240
    :goto_1
    const/4 v1, 0x1

    if-gt v3, v1, :cond_2

    .line 241
    iget-object v1, p0, LYB;->a:LYA;

    invoke-static {v1}, LYA;->c(LYA;)I

    move-result v1

    mul-int v5, v3, v0

    add-int/2addr v1, v5

    .line 242
    if-ltz v1, :cond_0

    iget-object v5, p0, LYB;->a:LYA;

    iget-object v5, v5, LYA;->a:LYy;

    invoke-static {v5}, LYy;->a(LYy;)LXz;

    move-result-object v5

    invoke-interface {v5}, LXz;->c()I

    move-result v5

    if-lt v1, v5, :cond_1

    .line 240
    :cond_0
    add-int/lit8 v1, v3, 0x2

    move v3, v1

    goto :goto_1

    .line 246
    :cond_1
    sub-int v5, v1, v4

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    iget-object v6, p0, LYB;->a:LYA;

    iget-object v6, v6, LYA;->a:LYy;

    iget v6, v6, LYy;->a:I

    div-int/lit8 v6, v6, 0x2

    if-ge v5, v6, :cond_0

    .line 250
    invoke-direct {p0, v1}, LYB;->a(I)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, LYB;->a:LYA;

    iget-object v5, v5, LYA;->a:LYy;

    .line 251
    invoke-static {v5}, LYy;->a(LYy;)LXz;

    move-result-object v5

    invoke-interface {v5, v1}, LXz;->a(I)Z

    move-result v5

    if-eqz v5, :cond_0

    move v0, v1

    .line 256
    :goto_2
    return v0

    .line 239
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 256
    goto :goto_2
.end method

.method static synthetic a(LYB;)I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, LYB;->b:I

    return v0
.end method

.method private a()Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 297
    iget-object v0, p0, LYB;->a:LYA;

    invoke-static {v0}, LYA;->a(LYA;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->size()I

    move-result v0

    iget-object v1, p0, LYB;->a:LYA;

    iget-object v1, v1, LYA;->a:LYy;

    iget v1, v1, LYy;->a:I

    if-lt v0, v1, :cond_2

    .line 298
    iget-object v0, p0, LYB;->a:LYA;

    invoke-static {v0}, LYA;->a(LYA;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 299
    iget-object v0, p0, LYB;->a:LYA;

    invoke-static {v0}, LYA;->a(LYA;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 300
    iget-object v2, p0, LYB;->a:LYA;

    invoke-static {v2}, LYA;->c(LYA;)I

    move-result v2

    sub-int v2, v0, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    iget-object v5, p0, LYB;->a:LYA;

    .line 301
    invoke-static {v5}, LYA;->c(LYA;)I

    move-result v5

    sub-int v5, v1, v5

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    if-le v2, v5, :cond_0

    move v2, v3

    .line 302
    :goto_0
    if-eqz v2, :cond_1

    .line 303
    :goto_1
    const-string v1, "PunchThumbnailGeneratorFactoryImpl"

    const-string v2, "removing thumbnail %s"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 304
    iget-object v1, p0, LYB;->a:LYA;

    invoke-static {v1}, LYA;->a(LYA;)Ljava/util/SortedMap;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/SortedMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 307
    :goto_2
    return-object v0

    :cond_0
    move v2, v4

    .line 301
    goto :goto_0

    :cond_1
    move v0, v1

    .line 302
    goto :goto_1

    .line 307
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private a()V
    .locals 2

    .prologue
    .line 202
    iget-boolean v0, p0, LYB;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LYB;->a:LYA;

    invoke-static {v0}, LYA;->a(LYA;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 209
    :cond_0
    :goto_0
    return-void

    .line 206
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LYB;->a:Z

    .line 208
    invoke-direct {p0}, LYB;->c()V

    goto :goto_0
.end method

.method private a(I)V
    .locals 5

    .prologue
    .line 264
    const-string v0, "PunchThumbnailGeneratorFactoryImpl"

    const-string v1, "generateThumbnail(t=%s): cache.size()=%s slideIndex=%s slideIndexToGenerate=%s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, LYB;->a:I

    .line 266
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LYB;->a:LYA;

    invoke-static {v4}, LYA;->a(LYA;)Ljava/util/SortedMap;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/SortedMap;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, LYB;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 264
    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 267
    iput p1, p0, LYB;->b:I

    .line 268
    invoke-direct {p0}, LYB;->a()Landroid/graphics/Bitmap;

    .line 272
    iget-object v0, p0, LYB;->a:LYA;

    invoke-static {v0}, LYA;->a(LYA;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 274
    iget-object v0, p0, LYB;->a:LYA;

    iget-object v0, v0, LYA;->a:LYy;

    invoke-static {v0}, LYy;->a(LYy;)LXz;

    move-result-object v0

    new-instance v1, LYH;

    invoke-direct {v1, p0, p1}, LYH;-><init>(LYB;I)V

    invoke-interface {v0, p1, v1}, LXz;->a(ILXB;)V

    .line 294
    return-void
.end method

.method static synthetic a(LYB;)V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, LYB;->d()V

    return-void
.end method

.method static synthetic a(LYB;I)V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0, p1}, LYB;->a(I)V

    return-void
.end method

.method static synthetic a(LYB;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0, p1, p2}, LYB;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 311
    const-string v0, "PunchThumbnailGeneratorFactoryImpl"

    const-string v1, "t=%s Creating thumbnail: slideIndex=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, LYB;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget v3, p0, LYB;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 313
    iput-boolean v4, p0, LYB;->b:Z

    .line 314
    iput-boolean v4, p0, LYB;->d:Z

    .line 316
    const-string v0, "text/html"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 317
    if-eqz p2, :cond_1

    .line 320
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, LYy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 321
    iget-object v1, p0, LYB;->a:LYA;

    iget-object v1, v1, LYA;->a:LYy;

    invoke-static {v1}, LYy;->a(LYy;)LQr;

    move-result-object v1

    const-string v2, "punchThumbnailHtmlTemplate"

    const-string v3, "<head>\n  <script type=\"text/javascript\">\n    function initialize() {\n      var imagesRemaining = -1;\n\n      document.getElementById(\"container\").innerHTML =\n        __ESCAPED_CONTENT__;\n\n      document.body.style.margin = \'0px\';\n      document.body.style.padding = \'0px\';\n\n      imagesRemaining = 0;\n      var i;\n      for (i in document.images) {\n        var img = document.images[i]\n        if (img.src != undefined) {\n          imagesRemaining++;\n          img.style.width = \'100%\'\n          img.style.height = \'100%\'\n          img.onload = function() {\n            imagesRemaining--;\n            if (imagesRemaining == 0) {\n              window.ThumbnailApi.onLoadImage();\n            }\n          };        }\n      }\n    }\n  </script>\n</head>\n<body onload=\"initialize();\">\n  <div id=\"container\" />\n</body>\n"

    invoke-interface {v1, v2, v3}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 323
    const-string v2, "__ESCAPED_CONTENT__"

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    move-object v2, p2

    .line 330
    :goto_0
    iput-boolean v5, p0, LYB;->c:Z

    .line 331
    iget-object v0, p0, LYB;->a:Landroid/webkit/WebView;

    const-string v1, "fake-url"

    sget-object v3, Landroid/util/Xml$Encoding;->UTF_8:Landroid/util/Xml$Encoding;

    invoke-virtual {v3}, Landroid/util/Xml$Encoding;->name()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    return-void

    .line 326
    :cond_0
    iput-boolean v5, p0, LYB;->d:Z

    :cond_1
    move-object v2, p2

    goto :goto_0
.end method

.method private a(I)Z
    .locals 2

    .prologue
    .line 260
    iget-object v0, p0, LYB;->a:LYA;

    invoke-static {v0}, LYA;->a(LYA;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/SortedMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(LYB;)Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, LYB;->c:Z

    return v0
.end method

.method static synthetic a(LYB;Z)Z
    .locals 0

    .prologue
    .line 108
    iput-boolean p1, p0, LYB;->b:Z

    return p1
.end method

.method static synthetic b(LYB;)I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, LYB;->a:I

    return v0
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 212
    iget-object v0, p0, LYB;->a:Landroid/webkit/WebView;

    iget-object v1, p0, LYB;->a:LYA;

    invoke-static {v1}, LYA;->a(LYA;)I

    move-result v1

    iget-object v2, p0, LYB;->a:LYA;

    invoke-static {v2}, LYA;->b(LYA;)I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/webkit/WebView;->layout(IIII)V

    .line 213
    return-void
.end method

.method static synthetic b(LYB;)V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, LYB;->a()V

    return-void
.end method

.method static synthetic b(LYB;)Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, LYB;->b:Z

    return v0
.end method

.method static synthetic b(LYB;Z)Z
    .locals 0

    .prologue
    .line 108
    iput-boolean p1, p0, LYB;->c:Z

    return p1
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 216
    iput v2, p0, LYB;->c:I

    .line 218
    invoke-direct {p0}, LYB;->a()I

    move-result v0

    .line 219
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 220
    iput-boolean v2, p0, LYB;->a:Z

    .line 225
    :goto_0
    return-void

    .line 224
    :cond_0
    invoke-direct {p0, v0}, LYB;->a(I)V

    goto :goto_0
.end method

.method static synthetic c(LYB;)V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, LYB;->b()V

    return-void
.end method

.method static synthetic c(LYB;)Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, LYB;->d:Z

    return v0
.end method

.method private d()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 335
    const-string v0, "PunchThumbnailGeneratorFactoryImpl"

    const-string v1, "t=%s content ready: slideIndex=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, LYB;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    iget v3, p0, LYB;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 337
    iget-object v0, p0, LYB;->a:LYA;

    invoke-static {v0}, LYA;->a(LYA;)I

    move-result v0

    .line 338
    iget-object v1, p0, LYB;->a:LYA;

    invoke-static {v1}, LYA;->b(LYA;)I

    move-result v1

    .line 340
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 341
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 342
    iget-object v4, p0, LYB;->a:Landroid/webkit/WebView;

    invoke-virtual {v4, v3}, Landroid/webkit/WebView;->draw(Landroid/graphics/Canvas;)V

    .line 344
    iget v3, p0, LYB;->c:I

    const/16 v4, 0xa

    if-ge v3, v4, :cond_0

    .line 351
    add-int/lit8 v0, v0, -0xa

    add-int/lit8 v1, v1, -0xa

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    .line 352
    iget v0, p0, LYB;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LYB;->c:I

    .line 353
    const-string v0, "PunchThumbnailGeneratorFactoryImpl"

    const-string v1, "Failed to get a thumbnail. Retrying: %s"

    new-array v2, v6, [Ljava/lang/Object;

    iget v3, p0, LYB;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 354
    iget-object v0, p0, LYB;->a:LYA;

    invoke-static {v0}, LYA;->a(LYA;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LYJ;

    invoke-direct {v1, p0}, LYJ;-><init>(LYB;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 369
    :goto_0
    return-void

    .line 364
    :cond_0
    iget-object v0, p0, LYB;->a:LYA;

    invoke-static {v0}, LYA;->a(LYA;)Ljava/util/SortedMap;

    move-result-object v0

    iget v1, p0, LYB;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    iget-object v0, p0, LYB;->a:LYA;

    invoke-static {v0}, LYA;->a(LYA;)LYx;

    move-result-object v0

    iget v1, p0, LYB;->b:I

    invoke-interface {v0, v1}, LYx;->c(I)V

    .line 367
    invoke-direct {p0}, LYB;->e()V

    .line 368
    invoke-direct {p0}, LYB;->c()V

    goto :goto_0
.end method

.method private e()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 373
    iput-boolean v0, p0, LYB;->d:Z

    .line 374
    iput-boolean v0, p0, LYB;->b:Z

    .line 375
    invoke-static {}, LakQ;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 376
    iget-object v0, p0, LYB;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearView()V

    .line 378
    :cond_0
    return-void
.end method
