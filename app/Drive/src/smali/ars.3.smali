.class public final Lars;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LarA;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lare;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laro;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Larp;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaqY;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Larh;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Larg;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaqZ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 44
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 45
    iput-object p1, p0, Lars;->a:LbrA;

    .line 46
    const-class v0, LarA;

    invoke-static {v0, v2}, Lars;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lars;->a:Lbsk;

    .line 49
    const-class v0, Lare;

    const-class v1, LaiC;

    invoke-static {v0, v1}, Lars;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lars;->b:Lbsk;

    .line 52
    const-class v0, Laro;

    const-class v1, LaiC;

    invoke-static {v0, v1}, Lars;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lars;->c:Lbsk;

    .line 55
    const-class v0, Larp;

    invoke-static {v0, v2}, Lars;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lars;->d:Lbsk;

    .line 58
    const-class v0, LaqY;

    invoke-static {v0, v2}, Lars;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lars;->e:Lbsk;

    .line 61
    const-class v0, Larh;

    invoke-static {v0, v2}, Lars;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lars;->f:Lbsk;

    .line 64
    const-class v0, Larg;

    invoke-static {v0, v2}, Lars;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lars;->g:Lbsk;

    .line 67
    const-class v0, LaqZ;

    invoke-static {v0, v2}, Lars;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Lars;->h:Lbsk;

    .line 70
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 109
    sparse-switch p1, :sswitch_data_0

    .line 203
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 111
    :sswitch_0
    new-instance v0, LarA;

    iget-object v1, p0, Lars;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LqD;

    iget-object v1, v1, LqD;->c:Lbsk;

    .line 114
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lars;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LqD;

    iget-object v2, v2, LqD;->c:Lbsk;

    .line 112
    invoke-static {v1, v2}, Lars;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LqK;

    iget-object v2, p0, Lars;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LCw;

    iget-object v2, v2, LCw;->X:Lbsk;

    .line 120
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lars;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LCw;

    iget-object v3, v3, LCw;->X:Lbsk;

    .line 118
    invoke-static {v2, v3}, Lars;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LsI;

    iget-object v3, p0, Lars;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LabP;

    iget-object v3, v3, LabP;->r:Lbsk;

    .line 126
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, Lars;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LabP;

    iget-object v4, v4, LabP;->r:Lbsk;

    .line 124
    invoke-static {v3, v4}, Lars;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LabF;

    iget-object v4, p0, Lars;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LalC;

    iget-object v4, v4, LalC;->a:Lbsk;

    .line 132
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, Lars;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LalC;

    iget-object v5, v5, LalC;->a:Lbsk;

    .line 130
    invoke-static {v4, v5}, Lars;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LalO;

    iget-object v5, p0, Lars;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Lajo;

    iget-object v5, v5, Lajo;->n:Lbsk;

    .line 138
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Laja;

    iget-object v6, p0, Lars;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LpG;

    iget-object v6, v6, LpG;->m:Lbsk;

    .line 142
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, Lars;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LpG;

    iget-object v7, v7, LpG;->m:Lbsk;

    .line 140
    invoke-static {v6, v7}, Lars;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LtK;

    invoke-direct/range {v0 .. v6}, LarA;-><init>(LqK;LsI;LabF;LalO;Laja;LtK;)V

    .line 201
    :goto_0
    return-object v0

    .line 149
    :sswitch_1
    new-instance v2, Lare;

    iget-object v0, p0, Lars;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LSW;

    iget-object v0, v0, LSW;->a:Lbsk;

    .line 152
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lars;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LSW;

    iget-object v1, v1, LSW;->a:Lbsk;

    .line 150
    invoke-static {v0, v1}, Lars;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSL;

    iget-object v1, p0, Lars;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LSW;

    iget-object v1, v1, LSW;->b:Lbsk;

    .line 158
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, Lars;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LSW;

    iget-object v3, v3, LSW;->b:Lbsk;

    .line 156
    invoke-static {v1, v3}, Lars;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LSV;

    invoke-direct {v2, v0, v1}, Lare;-><init>(LSL;LSV;)V

    move-object v0, v2

    .line 163
    goto :goto_0

    .line 165
    :sswitch_2
    new-instance v0, Laro;

    invoke-direct {v0}, Laro;-><init>()V

    goto :goto_0

    .line 169
    :sswitch_3
    new-instance v0, Larp;

    iget-object v1, p0, Lars;->a:LbrA;

    iget-object v1, v1, LbrA;->a:La;

    iget-object v1, v1, La;->a:Lbsk;

    .line 172
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lars;->a:LbrA;

    iget-object v2, v2, LbrA;->a:La;

    iget-object v2, v2, La;->a:Lbsk;

    .line 170
    invoke-static {v1, v2}, Lars;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    iget-object v2, p0, Lars;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LapK;

    iget-object v2, v2, LapK;->f:Lbsk;

    .line 178
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lars;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LapK;

    iget-object v3, v3, LapK;->f:Lbsk;

    .line 176
    invoke-static {v2, v3}, Lars;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lapn;

    iget-object v3, p0, Lars;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lwc;

    iget-object v3, v3, Lwc;->g:Lbsk;

    .line 184
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, Lars;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lwc;

    iget-object v4, v4, Lwc;->g:Lbsk;

    .line 182
    invoke-static {v3, v4}, Lars;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lwm;

    iget-object v4, p0, Lars;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lars;

    iget-object v4, v4, Lars;->b:Lbsk;

    .line 190
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, Lars;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Lars;

    iget-object v5, v5, Lars;->b:Lbsk;

    .line 188
    invoke-static {v4, v5}, Lars;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lare;

    iget-object v5, p0, Lars;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Lars;

    iget-object v5, v5, Lars;->c:Lbsk;

    .line 196
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, Lars;->a:LbrA;

    iget-object v6, v6, LbrA;->a:Lars;

    iget-object v6, v6, Lars;->c:Lbsk;

    .line 194
    invoke-static {v5, v6}, Lars;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Laro;

    invoke-direct/range {v0 .. v5}, Larp;-><init>(Landroid/app/Activity;Lapn;Lwm;Lare;Laro;)V

    goto/16 :goto_0

    .line 109
    :sswitch_data_0
    .sparse-switch
        0x29 -> :sswitch_2
        0x2a -> :sswitch_0
        0x2f -> :sswitch_1
        0x33 -> :sswitch_3
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 218
    sparse-switch p2, :sswitch_data_0

    .line 256
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 220
    :sswitch_0
    check-cast p1, Larq;

    .line 222
    iget-object v0, p0, Lars;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 225
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 222
    invoke-virtual {p1, v0}, Larq;->provideActionBarHelper(Landroid/content/Context;)LaqY;

    move-result-object v0

    .line 249
    :goto_0
    return-object v0

    .line 229
    :sswitch_1
    check-cast p1, Larq;

    .line 231
    iget-object v0, p0, Lars;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lars;

    iget-object v0, v0, Lars;->c:Lbsk;

    .line 234
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laro;

    .line 231
    invoke-virtual {p1, v0}, Larq;->provideActionBarIconsAlpha(Laro;)Larh;

    move-result-object v0

    goto :goto_0

    .line 238
    :sswitch_2
    check-cast p1, Larq;

    .line 240
    iget-object v0, p0, Lars;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->a:Lbsk;

    .line 243
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 240
    invoke-virtual {p1, v0}, Larq;->provideActionBarModeSwitcher(Landroid/content/Context;)Larg;

    move-result-object v0

    goto :goto_0

    .line 247
    :sswitch_3
    check-cast p1, Larq;

    .line 249
    iget-object v0, p0, Lars;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lars;

    iget-object v0, v0, Lars;->a:Lbsk;

    .line 252
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LarA;

    .line 249
    invoke-virtual {p1, v0}, Larq;->provideActionBarHelperFactory(LarA;)LaqZ;

    move-result-object v0

    goto :goto_0

    .line 218
    :sswitch_data_0
    .sparse-switch
        0x27 -> :sswitch_0
        0x28 -> :sswitch_1
        0x2e -> :sswitch_2
        0x32 -> :sswitch_3
    .end sparse-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 77
    const-class v0, LarA;

    iget-object v1, p0, Lars;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, Lars;->a(Ljava/lang/Class;Lbsk;)V

    .line 78
    const-class v0, Lare;

    iget-object v1, p0, Lars;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, Lars;->a(Ljava/lang/Class;Lbsk;)V

    .line 79
    const-class v0, Laro;

    iget-object v1, p0, Lars;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, Lars;->a(Ljava/lang/Class;Lbsk;)V

    .line 80
    const-class v0, Larp;

    iget-object v1, p0, Lars;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, Lars;->a(Ljava/lang/Class;Lbsk;)V

    .line 81
    const-class v0, LaqY;

    iget-object v1, p0, Lars;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, Lars;->a(Ljava/lang/Class;Lbsk;)V

    .line 82
    const-class v0, Larh;

    iget-object v1, p0, Lars;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, Lars;->a(Ljava/lang/Class;Lbsk;)V

    .line 83
    const-class v0, Larg;

    iget-object v1, p0, Lars;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, Lars;->a(Ljava/lang/Class;Lbsk;)V

    .line 84
    const-class v0, LaqZ;

    iget-object v1, p0, Lars;->h:Lbsk;

    invoke-virtual {p0, v0, v1}, Lars;->a(Ljava/lang/Class;Lbsk;)V

    .line 85
    iget-object v0, p0, Lars;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2a

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 87
    iget-object v0, p0, Lars;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x2f

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 89
    iget-object v0, p0, Lars;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x29

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 91
    iget-object v0, p0, Lars;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x33

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 93
    iget-object v0, p0, Lars;->e:Lbsk;

    const-class v1, Larq;

    const/16 v2, 0x27

    invoke-virtual {p0, v1, v2}, Lars;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 95
    iget-object v0, p0, Lars;->f:Lbsk;

    const-class v1, Larq;

    const/16 v2, 0x28

    invoke-virtual {p0, v1, v2}, Lars;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 97
    iget-object v0, p0, Lars;->g:Lbsk;

    const-class v1, Larq;

    const/16 v2, 0x2e

    invoke-virtual {p0, v1, v2}, Lars;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 99
    iget-object v0, p0, Lars;->h:Lbsk;

    const-class v1, Larq;

    const/16 v2, 0x32

    invoke-virtual {p0, v1, v2}, Lars;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 101
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 210
    .line 212
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 105
    return-void
.end method
