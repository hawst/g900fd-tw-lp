.class public LKn;
.super Ljava/lang/Object;
.source "AnimatedFloatingHandleShadow.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private final a:F

.field private a:J

.field private final a:LKY;

.field private a:Landroid/animation/Animator;

.field private final a:Landroid/graphics/Point;

.field private final a:Landroid/view/View;

.field private final a:Landroid/widget/ImageView;

.field private b:F

.field private final b:Landroid/graphics/Point;

.field private c:F

.field private d:F


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;Landroid/view/View;F)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, LKn;->a:Landroid/graphics/Point;

    .line 76
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, LKn;->b:Landroid/graphics/Point;

    .line 77
    iput v1, p0, LKn;->b:F

    .line 78
    iput v1, p0, LKn;->c:F

    .line 79
    iput v1, p0, LKn;->d:F

    .line 80
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LKn;->a:J

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, LKn;->a:Landroid/animation/Animator;

    .line 87
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LKn;->a:Landroid/widget/ImageView;

    .line 88
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, LKn;->a:Landroid/view/View;

    .line 89
    new-instance v0, LKY;

    const v1, 0x3f4ccccd    # 0.8f

    invoke-direct {v0, v1, p2}, LKY;-><init>(FLandroid/view/View;)V

    iput-object v0, p0, LKn;->a:LKY;

    .line 90
    iput p3, p0, LKn;->a:F

    .line 91
    return-void
.end method

.method private a(Ljava/lang/Runnable;)Landroid/animation/Animator$AnimatorListener;
    .locals 1

    .prologue
    .line 225
    new-instance v0, LKo;

    invoke-direct {v0, p0, p1}, LKo;-><init>(LKn;Ljava/lang/Runnable;)V

    return-object v0
.end method

.method static synthetic a(LKn;Landroid/animation/Animator;)Landroid/animation/Animator;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, LKn;->a:Landroid/animation/Animator;

    return-object p1
.end method

.method static synthetic a(LKn;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, LKn;->a:Landroid/widget/ImageView;

    return-object v0
.end method

.method private a(FFFFFFLjava/lang/Runnable;)V
    .locals 7

    .prologue
    .line 191
    float-to-double v0, p1

    float-to-double v2, p2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 192
    const/high16 v1, 0x3f800000    # 1.0f

    div-float/2addr v0, v1

    float-to-int v0, v0

    .line 193
    invoke-static {}, Lxm;->a()Lxr;

    move-result-object v1

    iget-object v2, p0, LKn;->a:Landroid/widget/ImageView;

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    aput p5, v3, v4

    const/4 v4, 0x1

    aput p6, v3, v4

    .line 194
    invoke-static {v2, v3}, Lxm;->d(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v2

    invoke-static {v2}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v2

    iget-object v3, p0, LKn;->a:Landroid/widget/ImageView;

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    aput p5, v4, v5

    const/4 v5, 0x1

    aput p6, v4, v5

    .line 195
    invoke-static {v3, v4}, Lxm;->e(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v3

    invoke-virtual {v2, v3}, Lxr;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v2

    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 196
    invoke-virtual {v2, v3}, Lxr;->a(Landroid/view/animation/Interpolator;)Lxr;

    move-result-object v2

    .line 194
    invoke-virtual {v1, v2}, Lxr;->a(Lxr;)Lxr;

    move-result-object v1

    iget-object v2, p0, LKn;->a:Landroid/widget/ImageView;

    const/4 v3, 0x1

    new-array v3, v3, [F

    const/4 v4, 0x0

    iget-object v5, p0, LKn;->a:Landroid/widget/ImageView;

    .line 198
    invoke-virtual {v5}, Landroid/widget/ImageView;->getX()F

    move-result v5

    add-float/2addr v5, p1

    aput v5, v3, v4

    invoke-static {v2, v3}, Lxm;->c(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v2

    invoke-static {v2}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v2

    iget-object v3, p0, LKn;->a:Landroid/widget/ImageView;

    const/4 v4, 0x1

    new-array v4, v4, [F

    const/4 v5, 0x0

    iget-object v6, p0, LKn;->a:Landroid/widget/ImageView;

    .line 199
    invoke-virtual {v6}, Landroid/widget/ImageView;->getY()F

    move-result v6

    add-float/2addr v6, p2

    aput v6, v4, v5

    invoke-static {v3, v4}, Lxm;->b(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v3

    invoke-virtual {v2, v3}, Lxr;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v2

    new-instance v3, Landroid/view/animation/OvershootInterpolator;

    const/high16 v4, 0x3f000000    # 0.5f

    invoke-direct {v3, v4}, Landroid/view/animation/OvershootInterpolator;-><init>(F)V

    .line 200
    invoke-virtual {v2, v3}, Lxr;->a(Landroid/view/animation/Interpolator;)Lxr;

    move-result-object v2

    .line 198
    invoke-virtual {v1, v2}, Lxr;->a(Lxr;)Lxr;

    move-result-object v1

    iget-object v2, p0, LKn;->a:Landroid/widget/ImageView;

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    aput p3, v3, v4

    const/4 v4, 0x1

    aput p4, v3, v4

    .line 202
    invoke-static {v2, v3}, Lxm;->a(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v2

    invoke-static {v2}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v2

    .line 203
    invoke-virtual {v2}, Lxr;->a()Lxr;

    move-result-object v2

    .line 202
    invoke-virtual {v1, v2}, Lxr;->a(Lxr;)Lxr;

    move-result-object v1

    .line 205
    invoke-virtual {v1, v0}, Lxr;->a(I)Lxr;

    move-result-object v0

    .line 206
    invoke-direct {p0, p7}, LKn;->a(Ljava/lang/Runnable;)Landroid/animation/Animator$AnimatorListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxr;->a(Landroid/animation/Animator$AnimatorListener;)Lxr;

    move-result-object v0

    .line 207
    invoke-virtual {v0}, Lxr;->a()Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, LKn;->a:Landroid/animation/Animator;

    .line 208
    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    const v3, 0x3f4ccccd    # 0.8f

    .line 172
    iget-object v0, p0, LKn;->a:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, LKn;->a:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 174
    const/4 v0, 0x0

    iput-object v0, p0, LKn;->a:Landroid/animation/Animator;

    .line 177
    :cond_0
    invoke-direct {p0}, LKn;->f()V

    .line 178
    iget-object v0, p0, LKn;->a:Landroid/view/View;

    .line 179
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iget-object v1, p0, LKn;->a:Landroid/view/View;

    .line 180
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 178
    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 181
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 182
    iget-object v2, p0, LKn;->a:LKY;

    invoke-virtual {v2, v1}, LKY;->a(Landroid/graphics/Canvas;)V

    .line 183
    iget-object v1, p0, LKn;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 184
    iget-object v0, p0, LKn;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 185
    iget-object v0, p0, LKn;->a:Landroid/widget/ImageView;

    iget-object v1, p0, LKn;->b:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    div-float/2addr v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setPivotX(F)V

    .line 186
    iget-object v0, p0, LKn;->a:Landroid/widget/ImageView;

    iget-object v1, p0, LKn;->b:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    div-float/2addr v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setPivotY(F)V

    .line 187
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    const v3, 0x3f4ccccd    # 0.8f

    .line 211
    iget-object v0, p0, LKn;->a:Landroid/widget/ImageView;

    iget v1, p0, LKn;->b:F

    iget-object v2, p0, LKn;->b:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setX(F)V

    .line 212
    iget-object v0, p0, LKn;->a:Landroid/widget/ImageView;

    iget v1, p0, LKn;->c:F

    iget-object v2, p0, LKn;->b:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setY(F)V

    .line 213
    iget-object v0, p0, LKn;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 214
    iget-object v0, p0, LKn;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 215
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 218
    iget-object v0, p0, LKn;->a:Landroid/widget/ImageView;

    iget-object v1, p0, LKn;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getX()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setX(F)V

    .line 219
    iget-object v0, p0, LKn;->a:Landroid/widget/ImageView;

    iget-object v1, p0, LKn;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getY()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setY(F)V

    .line 220
    iget-object v0, p0, LKn;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 221
    iget-object v0, p0, LKn;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 222
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 229
    iget-object v0, p0, LKn;->a:LKY;

    iget-object v1, p0, LKn;->a:Landroid/graphics/Point;

    iget-object v2, p0, LKn;->b:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, LKY;->onProvideShadowMetrics(Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 230
    return-void
.end method


# virtual methods
.method public a()Landroid/view/View$DragShadowBuilder;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, LKn;->a:LKY;

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 127
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LKn;->a:J

    .line 128
    return-void
.end method

.method public a(FF)V
    .locals 6

    .prologue
    .line 94
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 95
    iget-wide v2, p0, LKn;->a:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 96
    iget-wide v2, p0, LKn;->a:J

    sub-long v2, v0, v2

    long-to-float v2, v2

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    .line 97
    iget v3, p0, LKn;->b:F

    sub-float v3, p1, v3

    div-float v2, v3, v2

    iput v2, p0, LKn;->d:F

    .line 99
    :cond_0
    iput-wide v0, p0, LKn;->a:J

    .line 100
    iput p1, p0, LKn;->b:F

    .line 101
    iput p2, p0, LKn;->c:F

    .line 102
    return-void
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 5

    .prologue
    .line 153
    invoke-direct {p0}, LKn;->c()V

    .line 154
    invoke-direct {p0}, LKn;->d()V

    .line 157
    iget v0, p0, LKn;->d:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 158
    iget-object v0, p0, LKn;->a:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    .line 163
    :goto_0
    iget-object v1, p0, LKn;->a:Landroid/widget/ImageView;

    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    iget-object v4, p0, LKn;->a:Landroid/widget/ImageView;

    .line 164
    invoke-virtual {v4}, Landroid/widget/ImageView;->getX()F

    move-result v4

    add-float/2addr v0, v4

    aput v0, v2, v3

    invoke-static {v1, v2}, Lxm;->c(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v0

    invoke-static {v0}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    .line 165
    invoke-virtual {v0}, Lxr;->a()Lxr;

    move-result-object v0

    .line 166
    invoke-direct {p0, p1}, LKn;->a(Ljava/lang/Runnable;)Landroid/animation/Animator$AnimatorListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxr;->a(Landroid/animation/Animator$AnimatorListener;)Lxr;

    move-result-object v0

    iget-object v1, p0, LKn;->a:Landroid/widget/ImageView;

    .line 167
    invoke-virtual {v1}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxr;->a(Landroid/content/res/Resources;)Lxr;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Lxr;->a()Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, LKn;->a:Landroid/animation/Animator;

    .line 169
    return-void

    .line 160
    :cond_0
    iget-object v0, p0, LKn;->a:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    neg-int v0, v0

    int-to-float v0, v0

    goto :goto_0
.end method

.method public a(ZLjava/lang/Runnable;)V
    .locals 8

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const v6, 0x3f4ccccd    # 0.8f

    .line 135
    invoke-direct {p0}, LKn;->c()V

    .line 136
    invoke-direct {p0}, LKn;->e()V

    .line 138
    iget v0, p0, LKn;->b:F

    iget-object v1, p0, LKn;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    iget-object v1, p0, LKn;->b:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    div-float/2addr v1, v6

    sub-float v1, v0, v1

    .line 139
    iget v0, p0, LKn;->c:F

    iget-object v2, p0, LKn;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getY()F

    move-result v2

    sub-float/2addr v0, v2

    iget-object v2, p0, LKn;->b:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    div-float/2addr v2, v6

    sub-float v2, v0, v2

    .line 140
    if-eqz p1, :cond_0

    move v4, v3

    :goto_0
    move-object v0, p0

    move v5, v3

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, LKn;->a(FFFFFFLjava/lang/Runnable;)V

    .line 141
    return-void

    .line 140
    :cond_0
    const v4, 0x3e99999a    # 0.3f

    goto :goto_0
.end method

.method public a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 117
    iget v1, p0, LKn;->d:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v2, 0x43960000    # 300.0f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 118
    invoke-direct {p0}, LKn;->f()V

    .line 119
    iget v1, p0, LKn;->b:F

    iget-object v2, p0, LKn;->b:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_0

    iget v1, p0, LKn;->b:F

    iget-object v2, p0, LKn;->b:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget-object v2, p0, LKn;->a:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, LKn;->a:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 122
    :cond_1
    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, LKn;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 132
    return-void
.end method

.method public b(ZLjava/lang/Runnable;)V
    .locals 8

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 144
    invoke-direct {p0}, LKn;->c()V

    .line 145
    invoke-direct {p0}, LKn;->d()V

    .line 147
    iget-object v0, p0, LKn;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v0

    iget-object v1, p0, LKn;->a:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getX()F

    move-result v1

    sub-float v1, v0, v1

    .line 148
    iget-object v0, p0, LKn;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getY()F

    move-result v0

    iget-object v2, p0, LKn;->a:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getY()F

    move-result v2

    sub-float v2, v0, v2

    .line 149
    if-eqz p1, :cond_0

    move v3, v4

    :goto_0
    const v5, 0x3f4ccccd    # 0.8f

    move-object v0, p0

    move v6, v4

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, LKn;->a(FFFFFFLjava/lang/Runnable;)V

    .line 150
    return-void

    .line 149
    :cond_0
    const v3, 0x3e99999a    # 0.3f

    goto :goto_0
.end method
