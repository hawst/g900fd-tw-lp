.class LIS;
.super Ljava/lang/Object;
.source "TitleSectionIndexerFactory.java"


# instance fields
.field private final a:Landroid/content/Context;

.field private final a:Ljava/lang/String;

.field private final a:Ljava/text/Collator;

.field private final a:Ljava/util/Locale;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation runtime Lajg;
        .end annotation
    .end param

    .prologue
    .line 41
    sget v0, Lxi;->alphabet_set:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, LIS;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/Locale;)V

    .line 42
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/Locale;)V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, LIS;->a:Landroid/content/Context;

    .line 50
    iput-object p2, p0, LIS;->a:Ljava/lang/String;

    .line 51
    iput-object p3, p0, LIS;->a:Ljava/util/Locale;

    .line 54
    invoke-static {p3}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, LIS;->a:Ljava/text/Collator;

    .line 55
    iget-object v0, p0, LIS;->a:Ljava/text/Collator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/text/Collator;->setStrength(I)V

    .line 56
    return-void
.end method

.method private a(Ljava/lang/String;ZLjava/lang/String;)LIO;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 91
    if-nez p2, :cond_0

    move v0, v1

    .line 92
    :goto_0
    new-instance v3, LIO;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v4, v2

    new-instance v0, LIu;

    iget-object v2, p0, LIS;->a:Ljava/util/Locale;

    iget-object v5, p0, LIS;->a:Ljava/text/Collator;

    invoke-direct {v0, p3, v2, v5}, LIu;-><init>(Ljava/lang/String;Ljava/util/Locale;Ljava/text/Collator;)V

    aput-object v0, v4, v1

    invoke-static {v4}, LIL;->a([Ljava/lang/Object;)LIL;

    move-result-object v0

    invoke-direct {v3, p1, v0}, LIO;-><init>(Ljava/lang/String;LIL;)V

    return-object v3

    :cond_0
    move v0, v2

    .line 91
    goto :goto_0
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, LIS;->a:Landroid/content/Context;

    sget v1, Lxi;->fast_scroll_title_grouper_collections:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method a()Landroid/util/Pair;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<[",
            "LIO;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LIS;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 64
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v4, v1, v0}, Ljava/lang/String;->codePointCount(II)I

    move-result v0

    new-array v5, v0, [Ljava/lang/String;

    move v0, v1

    move v3, v1

    .line 67
    :goto_0
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v3, v2, :cond_0

    .line 68
    invoke-virtual {v4, v3}, Ljava/lang/String;->codePointAt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->charCount(I)I

    move-result v6

    .line 69
    add-int/lit8 v2, v0, 0x1

    add-int v7, v3, v6

    invoke-virtual {v4, v3, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v0

    .line 70
    add-int v0, v3, v6

    move v3, v0

    move v0, v2

    .line 71
    goto :goto_0

    .line 73
    :cond_0
    iget-object v0, p0, LIS;->a:Ljava/text/Collator;

    invoke-static {v5, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 77
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 78
    invoke-direct {p0}, LIS;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x1

    const-string v4, ""

    invoke-direct {p0, v0, v3, v4}, LIS;->a(Ljava/lang/String;ZLjava/lang/String;)LIO;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    array-length v3, v5

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v5, v0

    .line 80
    invoke-direct {p0, v4, v1, v4}, LIS;->a(Ljava/lang/String;ZLjava/lang/String;)LIO;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 82
    :cond_1
    new-array v0, v1, [LIO;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LIO;

    .line 87
    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, v0, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method

.method public a(LDC;)Landroid/widget/SectionIndexer;
    .locals 2

    .prologue
    .line 101
    new-instance v1, LIN;

    invoke-virtual {p0}, LIS;->a()Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, [LIw;

    invoke-direct {v1, p1, v0}, LIN;-><init>(LDC;[LIw;)V

    return-object v1
.end method
