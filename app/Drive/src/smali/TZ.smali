.class public LTZ;
.super Ljava/lang/Object;
.source "RedirectUtils.java"


# instance fields
.field private final a:Lakk;


# direct methods
.method constructor <init>(Lakk;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, LTZ;->a:Lakk;

    .line 52
    return-void
.end method

.method private a(Landroid/content/Intent;)Lcom/google/android/apps/docs/app/DocumentOpenMethod;
    .locals 6

    .prologue
    .line 170
    const-string v0, "documentOpenMethod.Name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 173
    :try_start_0
    invoke-static {v0}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/docs/app/DocumentOpenMethod;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 176
    :goto_0
    return-object v0

    .line 174
    :catch_0
    move-exception v1

    .line 175
    const-string v2, "RedirectUtils"

    const-string v3, "Invalid DocumentOpenMethod value %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v2, v1, v3, v4}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 176
    sget-object v0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;)Lcom/google/android/gms/drive/database/data/ResourceSpec;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 147
    const-string v0, "accountName"

    .line 148
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    .line 149
    if-nez v0, :cond_0

    .line 150
    const-string v0, "resourceSpec.AccountName"

    .line 151
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    .line 154
    :cond_0
    const-string v1, "resourceId"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 155
    if-nez v1, :cond_3

    .line 156
    const-string v1, "resourceSpec.ResourceId"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    .line 159
    :goto_0
    if-eqz v0, :cond_1

    move v1, v2

    :goto_1
    invoke-static {v1}, LbiT;->b(Z)V

    .line 160
    if-eqz v4, :cond_2

    :goto_2
    invoke-static {v2}, LbiT;->b(Z)V

    .line 162
    invoke-static {v0, v4}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a(LaFO;Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    return-object v0

    :cond_1
    move v1, v3

    .line 159
    goto :goto_1

    :cond_2
    move v2, v3

    .line 160
    goto :goto_2

    :cond_3
    move-object v4, v1

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;LaFO;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 138
    const-string v0, "accountName"

    invoke-virtual {p2}, LaFO;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 139
    const-string v0, "resourceId"

    invoke-virtual {p1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 142
    const-string v0, "resourceSpec.AccountName"

    invoke-virtual {p2}, LaFO;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 143
    const-string v0, "resourceSpec.ResourceId"

    invoke-virtual {p1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 144
    return-void
.end method

.method private a(Landroid/content/Intent;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V
    .locals 2

    .prologue
    .line 166
    const-string v0, "documentOpenMethod.Name"

    invoke-virtual {p2}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 167
    return-void
.end method


# virtual methods
.method public a(LTY;LaFO;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 123
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    invoke-virtual {p1}, LTY;->d()Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 127
    iget-object v0, p0, LTZ;->a:Lakk;

    invoke-virtual {p1}, LTY;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lakk;->b(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 129
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.docs.DRIVE_CREATE_REDIRECT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 130
    invoke-virtual {p1}, LTY;->b()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 131
    invoke-virtual {v0, p3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 132
    const-string v1, "accountName"

    invoke-virtual {p2}, LaFO;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 134
    return-object v0
.end method

.method public a(LaGu;LTY;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 66
    const-string v0, "entry"

    invoke-static {p1, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    const-string v0, "appInfo"

    invoke-static {p2, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    const-string v0, "original intent"

    invoke-static {p3, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    iget-object v0, p0, LTZ;->a:Lakk;

    .line 70
    invoke-virtual {p2}, LTY;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lakk;->b(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "Untrusted package"

    .line 69
    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    .line 72
    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    const-string v1, "Resource spec unavailable"

    .line 71
    invoke-static {v0, v1}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    invoke-virtual {p2}, LTY;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.docs.DRIVE_OPEN_REDIRECT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 78
    :goto_0
    invoke-virtual {p2}, LTY;->a()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 79
    const-string v1, "originalAction"

    invoke-virtual {p3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 81
    const-string v1, "uri"

    invoke-static {v0, p3, v1}, LalN;->a(Landroid/content/Intent;Landroid/content/Intent;Ljava/lang/String;)V

    .line 82
    const-string v1, "accountName"

    invoke-static {v0, p3, v1}, LalN;->a(Landroid/content/Intent;Landroid/content/Intent;Ljava/lang/String;)V

    .line 83
    const-string v1, "editMode"

    invoke-static {v0, p3, v1}, LalN;->b(Landroid/content/Intent;Landroid/content/Intent;Ljava/lang/String;)V

    .line 84
    const-string v1, "requestCameFromExternalApp"

    invoke-static {v0, p3, v1}, LalN;->b(Landroid/content/Intent;Landroid/content/Intent;Ljava/lang/String;)V

    .line 89
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, LFU;->a(Landroid/os/Bundle;)Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    move-result-object v1

    .line 90
    invoke-direct {p0, v0, v1}, LTZ;->a(Landroid/content/Intent;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V

    .line 94
    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v1

    .line 95
    iget-object v2, v1, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v2, v1}, LTZ;->a(Landroid/content/Intent;LaFO;Ljava/lang/String;)V

    .line 97
    return-object v0

    .line 74
    :cond_0
    new-instance v0, Landroid/content/Intent;

    .line 76
    invoke-virtual {p3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 104
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    const-string v0, "originalAction"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 107
    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 110
    :goto_0
    const-string v1, "uri"

    invoke-static {v0, p1, v1}, LalN;->a(Landroid/content/Intent;Landroid/content/Intent;Ljava/lang/String;)V

    .line 111
    const-string v1, "accountName"

    invoke-static {v0, p1, v1}, LalN;->a(Landroid/content/Intent;Landroid/content/Intent;Ljava/lang/String;)V

    .line 112
    const-string v1, "editMode"

    invoke-static {v0, p1, v1}, LalN;->b(Landroid/content/Intent;Landroid/content/Intent;Ljava/lang/String;)V

    .line 113
    const-string v1, "requestCameFromExternalApp"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 115
    invoke-direct {p0, p1}, LTZ;->a(Landroid/content/Intent;)Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    move-result-object v1

    invoke-static {v0, v1}, LFU;->a(Landroid/content/Intent;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V

    .line 116
    const-string v1, "resourceSpec"

    invoke-direct {p0, p1}, LTZ;->a(Landroid/content/Intent;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 117
    return-object v0

    .line 107
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
