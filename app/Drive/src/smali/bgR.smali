.class public final LbgR;
.super Ljava/lang/Object;
.source "Quadrilateral.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/bionics/scanner/rectifier/Quadrilateral;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 686
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/bionics/scanner/rectifier/Quadrilateral;
    .locals 2

    .prologue
    .line 689
    new-instance v0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;-><init>(Landroid/os/Parcel;LbgR;)V

    return-object v0
.end method

.method public a(I)[Lcom/google/bionics/scanner/rectifier/Quadrilateral;
    .locals 1

    .prologue
    .line 694
    new-array v0, p1, [Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 686
    invoke-virtual {p0, p1}, LbgR;->a(Landroid/os/Parcel;)Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 686
    invoke-virtual {p0, p1}, LbgR;->a(I)[Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    move-result-object v0

    return-object v0
.end method
