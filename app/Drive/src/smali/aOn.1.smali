.class LaOn;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:LaOm;

.field final synthetic a:Lcom/google/android/gms/internal/co;

.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/gms/internal/co;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LaOm;Lcom/google/android/gms/internal/co;)V
    .locals 2

    iput-object p1, p0, LaOn;->a:LaOm;

    iput-object p2, p0, LaOn;->a:Lcom/google/android/gms/internal/co;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, LaOn;->a:Lcom/google/android/gms/internal/co;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LaOn;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, LaOn;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/co;

    iget-object v1, p0, LaOn;->a:LaOm;

    invoke-static {v1}, LaOm;->a(LaOm;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/co;->e()V

    iget-object v0, p0, LaOn;->a:LaOm;

    invoke-virtual {v0}, LaOm;->b()V

    :cond_0
    return-void
.end method
