.class LaHP;
.super Ljava/util/AbstractSet;
.source "DriveAppSetImpl.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractSet",
        "<",
        "LaHJ;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LaHM;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LaHJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LaHM;)V
    .locals 1

    .prologue
    .line 66
    iput-object p1, p0, LaHP;->a:LaHM;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    .line 67
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, LaHP;->a:Ljava/util/Set;

    return-void
.end method

.method synthetic constructor <init>(LaHM;LaHN;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1}, LaHP;-><init>(LaHM;)V

    return-void
.end method


# virtual methods
.method public a(LaHJ;)Z
    .locals 7

    .prologue
    .line 79
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    iget-object v0, p0, LaHP;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    .line 81
    if-eqz v1, :cond_2

    .line 82
    invoke-virtual {p1}, LaHJ;->c()Ljava/util/List;

    move-result-object v0

    .line 83
    invoke-virtual {p1}, LaHJ;->d()Ljava/util/List;

    move-result-object v2

    .line 82
    invoke-static {v0, v2}, Lbnm;->a(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 87
    invoke-static {v0}, LaHM;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 88
    const-string v3, "DriveAppSetImpl"

    const-string v4, "Invalid extension %s for %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v6, 0x1

    aput-object p1, v5, v6

    invoke-static {v3, v4, v5}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 90
    :cond_0
    iget-object v3, p0, LaHP;->a:LaHM;

    invoke-static {v3}, LaHM;->a(LaHM;)LbpT;

    move-result-object v3

    invoke-interface {v3, v0, p1}, LbpT;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0

    .line 92
    :cond_1
    invoke-virtual {p1}, LaHJ;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, LaHJ;->b()Ljava/util/List;

    move-result-object v2

    invoke-static {v0, v2}, Lbnm;->a(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 93
    iget-object v3, p0, LaHP;->a:LaHM;

    invoke-static {v3}, LaHM;->b(LaHM;)LbpT;

    move-result-object v3

    invoke-interface {v3, v0, p1}, LbpT;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_1

    .line 96
    :cond_2
    return v1
.end method

.method public synthetic add(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 66
    check-cast p1, LaHJ;

    invoke-virtual {p0, p1}, LaHP;->a(LaHJ;)Z

    move-result v0

    return v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, LaHP;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 72
    iget-object v0, p0, LaHP;->a:LaHM;

    invoke-static {v0}, LaHM;->a(LaHM;)LbpT;

    move-result-object v0

    invoke-interface {v0}, LbpT;->a()V

    .line 73
    iget-object v0, p0, LaHP;->a:LaHM;

    invoke-static {v0}, LaHM;->b(LaHM;)LbpT;

    move-result-object v0

    invoke-interface {v0}, LbpT;->a()V

    .line 74
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, LaHP;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "LaHJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, LaHP;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lbnr;->a(Ljava/util/Iterator;)Lbqv;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 111
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 116
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, LaHP;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method
