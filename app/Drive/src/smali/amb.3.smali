.class final enum Lamb;
.super Ljava/lang/Enum;
.source "MediaStoreUtilities.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lamb;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lamb;

.field private static final synthetic a:[Lamb;

.field public static final enum b:Lamb;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 47
    new-instance v0, Lamb;

    const-string v1, "INTERNAL"

    const-string v2, "internal"

    invoke-direct {v0, v1, v3, v2}, Lamb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lamb;->a:Lamb;

    .line 48
    new-instance v0, Lamb;

    const-string v1, "EXTERNAL"

    const-string v2, "external"

    invoke-direct {v0, v1, v4, v2}, Lamb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lamb;->b:Lamb;

    .line 46
    const/4 v0, 0x2

    new-array v0, v0, [Lamb;

    sget-object v1, Lamb;->a:Lamb;

    aput-object v1, v0, v3

    sget-object v1, Lamb;->b:Lamb;

    aput-object v1, v0, v4

    sput-object v0, Lamb;->a:[Lamb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 53
    iput-object p3, p0, Lamb;->a:Ljava/lang/String;

    .line 54
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lamb;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lamb;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lamb;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lamb;
    .locals 1

    .prologue
    .line 46
    const-class v0, Lamb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lamb;

    return-object v0
.end method

.method public static values()[Lamb;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lamb;->a:[Lamb;

    invoke-virtual {v0}, [Lamb;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lamb;

    return-object v0
.end method
