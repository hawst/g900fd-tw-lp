.class LKv;
.super Ljava/lang/Object;
.source "DropToThisFolderListener.java"

# interfaces
.implements LbsF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LbsF",
        "<",
        "Lcom/google/android/gms/drive/database/data/EntrySpec;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LKs;


# direct methods
.method constructor <init>(LKs;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, LKv;->a:LKs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LbsU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ")",
            "LbsU",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    if-eqz p1, :cond_2

    .line 138
    iget-object v0, p0, LKv;->a:LKs;

    invoke-static {v0}, LKs;->a(LKs;)LJR;

    move-result-object v0

    invoke-virtual {v0}, LJR;->a()LbmF;

    move-result-object v0

    .line 139
    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    .line 140
    invoke-interface {v0}, Lcom/google/android/apps/docs/doclist/selection/ItemKey;->a()Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    move-result-object v0

    .line 141
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/google/android/apps/docs/doclist/selection/ItemKey;->a()Landroid/os/Parcelable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 142
    :cond_1
    iget-object v0, p0, LKv;->a:LKs;

    invoke-static {v0}, LKs;->a(LKs;)Lamd;

    move-result-object v0

    invoke-virtual {v0, p1}, Lamd;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LbsU;

    move-result-object v0

    .line 143
    new-instance v1, LKw;

    invoke-direct {v1, p0}, LKw;-><init>(LKv;)V

    invoke-static {v0, v1}, LbsK;->a(LbsU;LbiG;)LbsU;

    move-result-object v0

    .line 153
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    invoke-static {v0}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)LbsU;
    .locals 1

    .prologue
    .line 133
    check-cast p1, Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {p0, p1}, LKv;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LbsU;

    move-result-object v0

    return-object v0
.end method
