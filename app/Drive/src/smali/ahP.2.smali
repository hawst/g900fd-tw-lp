.class public LahP;
.super Ljava/lang/Object;
.source "SyncTaskUploadHelper.java"


# instance fields
.field private final a:LNc;

.field private final a:LaGM;

.field private final a:Laeb;

.field private final a:LajO;

.field private final a:Lalo;

.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "Ladi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Laeb;Lalo;LaGM;Laja;LajO;LNc;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laeb;",
            "Lalo;",
            "LaGM;",
            "Laja",
            "<",
            "Ladi;",
            ">;",
            "LajO;",
            "LNc;",
            ")V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, LahP;->a:Laeb;

    .line 60
    iput-object p2, p0, LahP;->a:Lalo;

    .line 61
    iput-object p3, p0, LahP;->a:LaGM;

    .line 62
    iput-object p4, p0, LahP;->a:Lbxw;

    .line 63
    iput-object p5, p0, LahP;->a:LajO;

    .line 64
    iput-object p6, p0, LahP;->a:LNc;

    .line 65
    return-void
.end method

.method private a(Ljava/lang/Throwable;)LNh;
    .locals 3

    .prologue
    .line 173
    const-string v0, "SyncTaskUploadHelper"

    const-string v1, "Failed to create item to upload."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, p1, v1, v2}, LalV;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 174
    new-instance v0, LNh;

    const-string v1, "Failed to create item to upload: "

    invoke-direct {v0, v1, p1}, LNh;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v0
.end method


# virtual methods
.method public a(LaGo;LaHd;)LMZ;
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 70
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    invoke-interface {p1}, LaGo;->a()LaFM;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, LaFM;->a()LaFO;

    move-result-object v0

    .line 75
    invoke-interface {p1}, LaGo;->f()Ljava/lang/String;

    move-result-object v3

    .line 76
    invoke-static {v3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    iget-object v4, p0, LahP;->a:LNc;

    invoke-virtual {v4}, LNc;->a()LNb;

    move-result-object v4

    .line 79
    invoke-virtual {v4, v0}, LNb;->a(LaFO;)LNb;

    move-result-object v5

    .line 80
    invoke-interface {p1}, LaGo;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LNb;->a(Ljava/lang/String;)LNb;

    move-result-object v5

    .line 81
    invoke-virtual {v5, v3}, LNb;->d(Ljava/lang/String;)LNb;

    move-result-object v5

    .line 82
    invoke-interface {p1}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v6

    invoke-virtual {v5, v6}, LNb;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LNb;

    .line 84
    invoke-virtual {p0, v0, v3}, LahP;->a(LaFO;Ljava/lang/String;)LaGv;

    move-result-object v0

    .line 85
    invoke-interface {p1}, LaGo;->a()LaGv;

    move-result-object v3

    .line 86
    invoke-virtual {v3, v0}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v4, v0}, LNb;->a(Z)LNb;

    .line 88
    iget-object v0, p0, LahP;->a:LaGM;

    .line 89
    invoke-interface {p1}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v3

    invoke-interface {v0, v3}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LbmY;

    move-result-object v0

    .line 90
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 91
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 92
    invoke-virtual {v4, v0}, LNb;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LNb;

    .line 95
    :cond_0
    invoke-virtual {p2}, LaHd;->b()J

    move-result-wide v6

    .line 96
    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-ltz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 97
    iget-object v0, p0, LahP;->a:LaGM;

    invoke-interface {v0, v6, v7}, LaGM;->a(J)LaGp;

    move-result-object v1

    .line 98
    if-nez v1, :cond_2

    .line 99
    new-instance v0, LNh;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Document content not found for id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LNh;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 105
    :cond_2
    :try_start_0
    iget-object v0, p0, LahP;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladi;

    sget-object v3, LacY;->a:LacY;

    sget-object v5, Ladk;->a:Ladk;

    invoke-interface {v0, v1, v3, v5, p1}, Ladi;->a(LaGp;LacY;Lamr;LaGo;)LbsU;

    move-result-object v0

    .line 106
    invoke-interface {v0}, LbsU;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladj;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    .line 108
    :try_start_1
    invoke-interface {v0}, Ladj;->a()LaGp;

    move-result-object v1

    invoke-virtual {v1}, LaGp;->a()Z

    move-result v1

    if-nez v1, :cond_5

    .line 109
    new-instance v1, Ljava/lang/AssertionError;

    const-string v3, "DocumentFileManager.openDocumentContentFile returned the main content file."

    invoke-direct {v1, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_5

    .line 142
    :catch_0
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    .line 143
    :goto_1
    :try_start_2
    invoke-direct {p0, v0}, LahP;->a(Ljava/lang/Throwable;)LNh;

    move-result-object v0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 149
    :catchall_0
    move-exception v0

    move-object v3, v2

    move-object v2, v1

    .line 150
    :goto_2
    if-eqz v2, :cond_3

    .line 151
    :try_start_3
    invoke-interface {v2}, Ladj;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 154
    :cond_3
    if-eqz v3, :cond_4

    .line 155
    iget-object v1, p0, LahP;->a:Laeb;

    invoke-interface {v1, v3}, Laeb;->a(Ljava/io/File;)V

    :cond_4
    throw v0

    .line 113
    :cond_5
    :try_start_4
    invoke-interface {v0}, Ladj;->a()LadY;

    move-result-object v1

    .line 114
    iget-object v3, p0, LahP;->a:Laeb;

    invoke-interface {v3}, Laeb;->a()Ljava/io/File;
    :try_end_4
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_5

    move-result-object v3

    .line 120
    :cond_6
    :try_start_5
    invoke-virtual {v1}, LadY;->a()J

    move-result-wide v6

    .line 122
    iget-object v5, p0, LahP;->a:Lalo;

    invoke-interface {v5, v1, v3}, Lalo;->a(LadY;Ljava/io/File;)V

    .line 124
    invoke-virtual {v1}, LadY;->a()J

    move-result-wide v8

    .line 125
    cmp-long v5, v8, v6

    if-nez v5, :cond_6

    .line 127
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 128
    invoke-virtual {p2}, LaHd;->a()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 129
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, LaHd;->a(Ljava/lang/String;)V

    .line 132
    :cond_7
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p2, v1}, LaHd;->a(Ljava/lang/Long;)V

    .line 134
    new-instance v1, LalU;

    invoke-direct {v1, v3}, LalU;-><init>(Ljava/io/File;)V

    .line 135
    invoke-virtual {v4, v1}, LNb;->a(LalU;)LNb;

    .line 136
    const/4 v1, 0x1

    invoke-virtual {v4, v1}, LNb;->b(Z)LNb;

    .line 137
    invoke-virtual {v4}, LNb;->a()LMZ;

    move-result-object v1

    .line 138
    invoke-virtual {p2}, LaHd;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, LMZ;->a(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_5 .. :try_end_5} :catch_8
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_6
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_6

    .line 150
    if-eqz v0, :cond_8

    .line 151
    :try_start_6
    invoke-interface {v0}, Ladj;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 154
    :cond_8
    if-eqz v2, :cond_9

    .line 155
    iget-object v0, p0, LahP;->a:Laeb;

    invoke-interface {v0, v2}, Laeb;->a(Ljava/io/File;)V

    :cond_9
    return-object v1

    .line 154
    :catchall_1
    move-exception v0

    if-eqz v2, :cond_a

    .line 155
    iget-object v1, p0, LahP;->a:Laeb;

    invoke-interface {v1, v2}, Laeb;->a(Ljava/io/File;)V

    :cond_a
    throw v0

    .line 144
    :catch_1
    move-exception v0

    move-object v3, v2

    .line 145
    :goto_3
    :try_start_7
    invoke-direct {p0, v0}, LahP;->a(Ljava/lang/Throwable;)LNh;

    move-result-object v0

    throw v0

    .line 149
    :catchall_2
    move-exception v0

    goto :goto_2

    .line 146
    :catch_2
    move-exception v0

    move-object v3, v2

    .line 147
    :goto_4
    invoke-direct {p0, v0}, LahP;->a(Ljava/lang/Throwable;)LNh;

    move-result-object v0

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 154
    :catchall_3
    move-exception v0

    if-eqz v3, :cond_b

    .line 155
    iget-object v1, p0, LahP;->a:Laeb;

    invoke-interface {v1, v3}, Laeb;->a(Ljava/io/File;)V

    :cond_b
    throw v0

    .line 149
    :catchall_4
    move-exception v0

    move-object v3, v2

    goto/16 :goto_2

    :catchall_5
    move-exception v1

    move-object v3, v2

    move-object v2, v0

    move-object v0, v1

    goto/16 :goto_2

    :catchall_6
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto/16 :goto_2

    .line 146
    :catch_3
    move-exception v1

    move-object v3, v2

    move-object v2, v0

    move-object v0, v1

    goto :goto_4

    :catch_4
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_4

    .line 144
    :catch_5
    move-exception v1

    move-object v3, v2

    move-object v2, v0

    move-object v0, v1

    goto :goto_3

    :catch_6
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_3

    .line 142
    :catch_7
    move-exception v0

    move-object v1, v2

    goto/16 :goto_1

    :catch_8
    move-exception v1

    move-object v2, v3

    move-object v10, v0

    move-object v0, v1

    move-object v1, v10

    goto/16 :goto_1
.end method

.method public a(LaFO;Ljava/lang/String;)LaGv;
    .locals 3

    .prologue
    .line 163
    iget-object v0, p0, LahP;->a:LajO;

    .line 164
    invoke-interface {v0, p1}, LajO;->a(LaFO;)LajN;

    move-result-object v0

    .line 165
    invoke-virtual {v0, p2}, LajN;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    .line 166
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 167
    invoke-static {v0}, Lbnm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LaGv;->a(Ljava/lang/String;)LaGv;

    move-result-object v0

    .line 169
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
