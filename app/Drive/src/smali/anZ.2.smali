.class public LanZ;
.super LaoL;
.source "DocumentFileFetcher.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaoL",
        "<",
        "Lcom/google/android/apps/docs/utils/FetchSpec;",
        "LakD",
        "<",
        "Ljava/io/File;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final a:Laoy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoy",
            "<",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LaGM;

.field private final a:LaKR;

.field private final a:Ladi;

.field private final a:Laoc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Laoa;

    invoke-direct {v0}, Laoa;-><init>()V

    sput-object v0, LanZ;->a:Laoy;

    return-void
.end method

.method constructor <init>(LQr;LaGM;LaKR;Ladi;Laoc;LZS;)V
    .locals 2
    .param p6    # LZS;
        .annotation runtime LaoE;
        .end annotation
    .end param

    .prologue
    .line 62
    invoke-static {p1}, LanZ;->a(LQr;)I

    move-result v0

    sget-object v1, LanZ;->a:Laoy;

    invoke-direct {p0, p5, v0, v1, p6}, LaoL;-><init>(Laoo;ILaoy;LZS;)V

    .line 64
    iput-object p2, p0, LanZ;->a:LaGM;

    .line 65
    iput-object p3, p0, LanZ;->a:LaKR;

    .line 66
    iput-object p4, p0, LanZ;->a:Ladi;

    .line 67
    iput-object p5, p0, LanZ;->a:Laoc;

    .line 68
    return-void
.end method

.method private static a(LQr;)I
    .locals 2

    .prologue
    .line 43
    const-string v0, "fetchingMaxNumberOfDocumentDownloadRetries"

    const/4 v1, 0x3

    invoke-interface {p0, v0, v1}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    .line 46
    return v0
.end method

.method private a(LaGo;)LbsU;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGo;",
            ")",
            "LbsU",
            "<",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 90
    :try_start_0
    iget-object v0, p0, LanZ;->a:Ladi;

    sget-object v1, LacY;->a:LacY;

    .line 91
    invoke-interface {v0, p1, v1}, Ladi;->a(LaGo;LacY;)LbsU;

    move-result-object v0

    invoke-interface {v0}, LbsU;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladj;

    .line 92
    invoke-interface {v0}, Ladj;->a()Ljava/io/File;

    move-result-object v1

    .line 93
    new-instance v2, Laob;

    invoke-direct {v2, p0, v0}, Laob;-><init>(LanZ;Ladj;)V

    .line 94
    invoke-static {v1, v2}, LakD;->a(Ljava/lang/Object;LakG;)LakD;

    move-result-object v0

    invoke-static {v0}, LbsK;->a(Ljava/lang/Object;)LbsU;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 107
    :goto_0
    return-object v0

    .line 104
    :catch_0
    move-exception v0

    .line 105
    invoke-static {v0}, LbsK;->a(Ljava/lang/Throwable;)LbsU;

    move-result-object v0

    goto :goto_0

    .line 106
    :catch_1
    move-exception v0

    .line 107
    invoke-static {v0}, LbsK;->a(Ljava/lang/Throwable;)LbsU;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/apps/docs/utils/FetchSpec;)LbsU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            ")",
            "LbsU",
            "<",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 77
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/FetchSpec;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 79
    iget-object v1, p0, LanZ;->a:LaGM;

    invoke-interface {v1, v0}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v0

    .line 80
    iget-object v1, p0, LanZ;->a:LaKR;

    invoke-interface {v1}, LaKR;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LaGo;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 81
    invoke-direct {p0, v0}, LanZ;->a(LaGo;)LbsU;

    move-result-object v0

    .line 83
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, LaoL;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)LbsU;
    .locals 1

    .prologue
    .line 30
    check-cast p1, Lcom/google/android/apps/docs/utils/FetchSpec;

    invoke-virtual {p0, p1}, LanZ;->a(Lcom/google/android/apps/docs/utils/FetchSpec;)LbsU;

    move-result-object v0

    return-object v0
.end method
