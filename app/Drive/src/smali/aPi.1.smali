.class public abstract LaPi;
.super LaPN;


# instance fields
.field private final a:LaPh;

.field private final a:Lcom/google/android/gms/internal/dx;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/dx;LaPh;)V
    .locals 0

    invoke-direct {p0}, LaPN;-><init>()V

    iput-object p1, p0, LaPi;->a:Lcom/google/android/gms/internal/dx;

    iput-object p2, p0, LaPi;->a:LaPh;

    return-void
.end method

.method private static a(LaPq;Lcom/google/android/gms/internal/dx;)Lcom/google/android/gms/internal/dz;
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p0, p1}, LaPq;->a(Lcom/google/android/gms/internal/dx;)Lcom/google/android/gms/internal/dz;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    const-string v2, "Could not fetch ad response from ad request service."

    invoke-static {v2, v1}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v2, "Could not fetch ad response from ad request service due to an Exception."

    invoke-static {v2, v1}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_2
    move-exception v1

    const-string v2, "Could not fetch ad response from ad request service due to an Exception."

    invoke-static {v2, v1}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public abstract a()LaPq;
.end method

.method public final a()V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, LaPi;->a()LaPq;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/internal/dz;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/dz;-><init>(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    invoke-virtual {p0}, LaPi;->c()V

    iget-object v1, p0, LaPi;->a:LaPh;

    invoke-interface {v1, v0}, LaPh;->a(Lcom/google/android/gms/internal/dz;)V

    return-void

    :cond_1
    :try_start_1
    iget-object v1, p0, LaPi;->a:Lcom/google/android/gms/internal/dx;

    invoke-static {v0, v1}, LaPi;->a(LaPq;Lcom/google/android/gms/internal/dx;)Lcom/google/android/gms/internal/dz;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/internal/dz;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/dz;-><init>(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LaPi;->c()V

    throw v0
.end method

.method public final b()V
    .locals 0

    invoke-virtual {p0}, LaPi;->c()V

    return-void
.end method

.method public abstract c()V
.end method
