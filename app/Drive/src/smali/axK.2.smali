.class LaxK;
.super Laxy;
.source "PdfPageLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laxy",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LaxI;


# direct methods
.method constructor <init>(LaxI;)V
    .locals 2

    .prologue
    .line 318
    iput-object p1, p0, LaxK;->a:LaxI;

    .line 319
    invoke-static {p1}, LaxI;->a(LaxI;)Laxz;

    move-result-object v0

    sget-object v1, LaxR;->g:LaxR;

    invoke-direct {p0, v0, v1}, Laxy;-><init>(Laxz;LaxR;)V

    .line 320
    return-void
.end method


# virtual methods
.method protected bridge synthetic a(LavE;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 317
    invoke-virtual {p0, p1}, LaxK;->a(LavE;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(LavE;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, LaxK;->a:LaxI;

    invoke-static {v0}, LaxI;->a(LaxI;)I

    move-result v0

    invoke-interface {p1, v0}, LavE;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 317
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, LaxK;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 337
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "characters"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(LaxH;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 317
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, LaxK;->a(LaxH;Ljava/lang/String;)V

    return-void
.end method

.method protected a(LaxH;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 328
    iget-object v0, p0, LaxK;->a:LaxI;

    invoke-static {v0}, LaxI;->a(LaxI;)I

    move-result v0

    invoke-interface {p1, v0, p2}, LaxH;->a(ILjava/lang/String;)V

    .line 329
    iget-object v0, p0, LaxK;->a:LaxI;

    const/4 v1, 0x0

    iput-object v1, v0, LaxI;->a:LaxK;

    .line 330
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 333
    const-string v0, "GetPageTextTask(page=%d)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LaxK;->a:LaxI;

    invoke-static {v3}, LaxI;->a(LaxI;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
