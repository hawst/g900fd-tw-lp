.class public final Laug;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f0b0113

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f0b0114

.field public static final abc_input_method_navigation_guard:I = 0x7f0b002f

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f0b0115

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f0b0116

.field public static final abc_primary_text_material_dark:I = 0x7f0b0117

.field public static final abc_primary_text_material_light:I = 0x7f0b0118

.field public static final abc_search_url_text:I = 0x7f0b0119

.field public static final abc_search_url_text_normal:I = 0x7f0b002c

.field public static final abc_search_url_text_pressed:I = 0x7f0b002e

.field public static final abc_search_url_text_selected:I = 0x7f0b002d

.field public static final abc_secondary_text_material_dark:I = 0x7f0b011a

.field public static final abc_secondary_text_material_light:I = 0x7f0b011b

.field public static final accent_material_dark:I = 0x7f0b003b

.field public static final accent_material_light:I = 0x7f0b003a

.field public static final account_background_dark:I = 0x7f0b008a

.field public static final account_background_lite:I = 0x7f0b008b

.field public static final account_display_name:I = 0x7f0b0063

.field public static final account_display_name_highlighted:I = 0x7f0b0064

.field public static final account_info_banner_background:I = 0x7f0b005e

.field public static final account_secondary_text:I = 0x7f0b0065

.field public static final account_secondary_text_highlighted:I = 0x7f0b0066

.field public static final account_text_color:I = 0x7f0b0003

.field public static final action_bar_background_color:I = 0x7f0b00b5

.field public static final action_bar_overlay:I = 0x7f0b010a

.field public static final activity_card_secondary_text:I = 0x7f0b0067

.field public static final add_accounts_text_color:I = 0x7f0b0002

.field public static final background_floating_material_dark:I = 0x7f0b0032

.field public static final background_floating_material_light:I = 0x7f0b0033

.field public static final background_material_dark:I = 0x7f0b0030

.field public static final background_material_light:I = 0x7f0b0031

.field public static final bright_foreground_disabled_material_dark:I = 0x7f0b0042

.field public static final bright_foreground_disabled_material_light:I = 0x7f0b0043

.field public static final bright_foreground_inverse_material_dark:I = 0x7f0b0044

.field public static final bright_foreground_inverse_material_light:I = 0x7f0b0045

.field public static final bright_foreground_material_dark:I = 0x7f0b0040

.field public static final bright_foreground_material_light:I = 0x7f0b0041

.field public static final button_focused:I = 0x7f0b00bb

.field public static final button_material_dark:I = 0x7f0b003c

.field public static final button_material_light:I = 0x7f0b003d

.field public static final button_pressed:I = 0x7f0b00bc

.field public static final common_action_bar_splitter:I = 0x7f0b000d

.field public static final common_signin_btn_dark_text_default:I = 0x7f0b0004

.field public static final common_signin_btn_dark_text_disabled:I = 0x7f0b0006

.field public static final common_signin_btn_dark_text_focused:I = 0x7f0b0007

.field public static final common_signin_btn_dark_text_pressed:I = 0x7f0b0005

.field public static final common_signin_btn_default_background:I = 0x7f0b000c

.field public static final common_signin_btn_light_text_default:I = 0x7f0b0008

.field public static final common_signin_btn_light_text_disabled:I = 0x7f0b000a

.field public static final common_signin_btn_light_text_focused:I = 0x7f0b000b

.field public static final common_signin_btn_light_text_pressed:I = 0x7f0b0009

.field public static final common_signin_btn_text_dark:I = 0x7f0b011c

.field public static final common_signin_btn_text_light:I = 0x7f0b011d

.field public static final create_bar_bevel:I = 0x7f0b0060

.field public static final create_bar_button_background_focused:I = 0x7f0b0062

.field public static final create_bar_button_background_pressed:I = 0x7f0b0061

.field public static final create_entry_dialog_list_background:I = 0x7f0b005f

.field public static final default_account_details_color:I = 0x7f0b0000

.field public static final default_list_divider:I = 0x7f0b0098

.field public static final default_text_color:I = 0x7f0b0068

.field public static final default_text_highlighted_color:I = 0x7f0b0069

.field public static final detail_card_header_text:I = 0x7f0b006a

.field public static final detail_card_outline:I = 0x7f0b006b

.field public static final detail_fragment_avatar_background_gray:I = 0x7f0b006c

.field public static final detail_fragment_avatar_background_green:I = 0x7f0b006d

.field public static final detail_fragment_background:I = 0x7f0b006e

.field public static final detail_fragment_horizontal_rule:I = 0x7f0b006f

.field public static final detail_fragment_preview_card_text:I = 0x7f0b0072

.field public static final detail_fragment_preview_card_text_shadow:I = 0x7f0b0073

.field public static final detail_fragment_preview_overlay:I = 0x7f0b0071

.field public static final detail_fragment_primary_text:I = 0x7f0b0070

.field public static final dialog_disabled_button_text:I = 0x7f0b0075

.field public static final dialog_enabled_button_text:I = 0x7f0b0074

.field public static final dim_foreground_disabled_material_dark:I = 0x7f0b0048

.field public static final dim_foreground_disabled_material_light:I = 0x7f0b0049

.field public static final dim_foreground_material_dark:I = 0x7f0b0046

.field public static final dim_foreground_material_light:I = 0x7f0b0047

.field public static final doc_grid_entry_background:I = 0x7f0b0076

.field public static final doc_grid_entry_shadow:I = 0x7f0b0077

.field public static final doc_grid_entry_title_background:I = 0x7f0b0078

.field public static final doc_grid_focus:I = 0x7f0b00c8

.field public static final doclist_background:I = 0x7f0b0079

.field public static final doclist_group_header_text:I = 0x7f0b00ae

.field public static final doclist_horizontal_rule:I = 0x7f0b00af

.field public static final doclist_primary_text:I = 0x7f0b011e

.field public static final doclist_secondary_text:I = 0x7f0b011f

.field public static final doclist_sticky_header_background:I = 0x7f0b007a

.field public static final doclist_text:I = 0x7f0b007b

.field public static final dragshadow_background:I = 0x7f0b00b1

.field public static final dragshadow_text:I = 0x7f0b00b2

.field public static final drop_shadow_start:I = 0x7f0b00d0

.field public static final ds_actionbar_background_color:I = 0x7f0b001b

.field public static final ds_actionbar_title_color:I = 0x7f0b001c

.field public static final ds_camera_control_background:I = 0x7f0b001d

.field public static final ds_default_list_divider:I = 0x7f0b0025

.field public static final ds_default_list_vertical_divider:I = 0x7f0b0026

.field public static final ds_editor_panel_backfill:I = 0x7f0b001e

.field public static final ds_list_entry_activated:I = 0x7f0b0020

.field public static final ds_list_entry_disabled_focused:I = 0x7f0b0021

.field public static final ds_list_entry_focused:I = 0x7f0b0022

.field public static final ds_list_entry_pressed:I = 0x7f0b0023

.field public static final ds_list_entry_selected:I = 0x7f0b0024

.field public static final ds_primary_text_color:I = 0x7f0b001f

.field public static final font_black:I = 0x7f0b0081

.field public static final grey:I = 0x7f0b0082

.field public static final grey_disabled_quick_action_icons:I = 0x7f0b0083

.field public static final grid_sync_background:I = 0x7f0b0088

.field public static final grid_sync_offlinestatus_text:I = 0x7f0b0089

.field public static final helpcard_primary:I = 0x7f0b007c

.field public static final helpcard_secondary:I = 0x7f0b007d

.field public static final helpcard_text_color:I = 0x7f0b007e

.field public static final highlighted_text_material_dark:I = 0x7f0b004c

.field public static final highlighted_text_material_light:I = 0x7f0b004d

.field public static final hint_foreground_material_dark:I = 0x7f0b004a

.field public static final hint_foreground_material_light:I = 0x7f0b004b

.field public static final holo_blue_light:I = 0x7f0b00bd

.field public static final home_icons:I = 0x7f0b007f

.field public static final home_icons_label:I = 0x7f0b0080

.field public static final incoming_background:I = 0x7f0b0084

.field public static final internal_release_dialog_background:I = 0x7f0b00c3

.field public static final internal_release_dialog_button:I = 0x7f0b00c4

.field public static final internal_release_dialog_text_color:I = 0x7f0b00c5

.field public static final kix_webview_background:I = 0x7f0b00b0

.field public static final light_grey:I = 0x7f0b0085

.field public static final link_text_material_dark:I = 0x7f0b004e

.field public static final link_text_material_light:I = 0x7f0b004f

.field public static final list_entry_activated:I = 0x7f0b009d

.field public static final list_entry_disabled_focused:I = 0x7f0b009e

.field public static final list_entry_focused:I = 0x7f0b00a0

.field public static final list_entry_pressed:I = 0x7f0b00a1

.field public static final list_entry_selected:I = 0x7f0b009f

.field public static final m_action_button_text:I = 0x7f0b00f5

.field public static final m_actionbar_background:I = 0x7f0b00de

.field public static final m_actionbar_icon:I = 0x7f0b00e0

.field public static final m_actionbar_separator:I = 0x7f0b00df

.field public static final m_actionbar_text:I = 0x7f0b002a

.field public static final m_add_collaborator_background_alpha_hack:I = 0x7f0b00e4

.field public static final m_app_background:I = 0x7f0b00db

.field public static final m_app_divider_color:I = 0x7f0b00e6

.field public static final m_app_overlay_shadow:I = 0x7f0b00d5

.field public static final m_app_primary:I = 0x7f0b002b

.field public static final m_app_primary_action_text:I = 0x7f0b00e3

.field public static final m_app_primary_inverse_text:I = 0x7f0b00e2

.field public static final m_app_primary_text:I = 0x7f0b00e1

.field public static final m_app_secondary:I = 0x7f0b00d3

.field public static final m_app_secondary_background:I = 0x7f0b00dc

.field public static final m_app_secondary_text:I = 0x7f0b00e5

.field public static final m_app_status_bar:I = 0x7f0b00d4

.field public static final m_cool_grey_70:I = 0x7f0b0101

.field public static final m_dialog_primary_button_text:I = 0x7f0b0120

.field public static final m_doclist_background:I = 0x7f0b00dd

.field public static final m_entry:I = 0x7f0b00ed

.field public static final m_entry_active:I = 0x7f0b00ee

.field public static final m_entry_disabled_focused:I = 0x7f0b00f2

.field public static final m_entry_focused:I = 0x7f0b00f0

.field public static final m_entry_pressed:I = 0x7f0b00ef

.field public static final m_entry_selected:I = 0x7f0b00f1

.field public static final m_entry_text_primary:I = 0x7f0b00f3

.field public static final m_entry_text_secondary:I = 0x7f0b00f4

.field public static final m_google_blue_200:I = 0x7f0b0103

.field public static final m_google_blue_50:I = 0x7f0b0102

.field public static final m_google_blue_500:I = 0x7f0b0104

.field public static final m_icon_action_bar:I = 0x7f0b0029

.field public static final m_icon_primary_inverse_tint:I = 0x7f0b00e8

.field public static final m_icon_primary_tint:I = 0x7f0b00e9

.field public static final m_icon_search_bar:I = 0x7f0b00ea

.field public static final m_icon_secondary_tint:I = 0x7f0b00eb

.field public static final m_icon_tertiary_tint:I = 0x7f0b00ec

.field public static final m_material_button_dark:I = 0x7f0b0105

.field public static final m_material_button_light:I = 0x7f0b0106

.field public static final m_material_grey_100:I = 0x7f0b00f9

.field public static final m_material_grey_200:I = 0x7f0b00fa

.field public static final m_material_grey_300:I = 0x7f0b00fb

.field public static final m_material_grey_300_with_tiny_alpha:I = 0x7f0b00fc

.field public static final m_material_grey_400:I = 0x7f0b00fd

.field public static final m_material_grey_50:I = 0x7f0b00f8

.field public static final m_material_grey_500:I = 0x7f0b00fe

.field public static final m_material_grey_600:I = 0x7f0b00ff

.field public static final m_material_grey_700:I = 0x7f0b0100

.field public static final m_material_snackbar_bg:I = 0x7f0b0107

.field public static final m_notification_icon_background:I = 0x7f0b00f7

.field public static final m_progress_bg:I = 0x7f0b00da

.field public static final m_progress_primary:I = 0x7f0b00d8

.field public static final m_progress_secondary:I = 0x7f0b00d9

.field public static final m_ripple_dark:I = 0x7f0b00d7

.field public static final m_ripple_light:I = 0x7f0b00d6

.field public static final m_search_bar_background:I = 0x7f0b00e7

.field public static final m_toast_background:I = 0x7f0b00f6

.field public static final main_background:I = 0x7f0b0109

.field public static final main_overlay:I = 0x7f0b0108

.field public static final manage_accounts_text_color:I = 0x7f0b0001

.field public static final material_blue_grey_800:I = 0x7f0b005a

.field public static final material_blue_grey_900:I = 0x7f0b005b

.field public static final material_blue_grey_950:I = 0x7f0b005c

.field public static final material_deep_teal_200:I = 0x7f0b0058

.field public static final material_deep_teal_500:I = 0x7f0b0059

.field public static final menu_item_default:I = 0x7f0b00c0

.field public static final menu_item_pressed:I = 0x7f0b00c1

.field public static final menu_item_selected:I = 0x7f0b00c2

.field public static final migration_font_color:I = 0x7f0b00b3

.field public static final navigation_background:I = 0x7f0b00a2

.field public static final navigation_item_text:I = 0x7f0b00a4

.field public static final navigation_line:I = 0x7f0b009c

.field public static final navigation_shadow_bottom:I = 0x7f0b00a6

.field public static final navigation_shadow_top:I = 0x7f0b00a5

.field public static final navigation_text:I = 0x7f0b00a3

.field public static final operation_dialog_button_text:I = 0x7f0b0121

.field public static final operation_dialog_error:I = 0x7f0b005d

.field public static final operation_dialog_syncing:I = 0x7f0b00d1

.field public static final overlay_back:I = 0x7f0b010b

.field public static final overlay_text:I = 0x7f0b010c

.field public static final pick_entry_dialog_header_background:I = 0x7f0b00b6

.field public static final pick_entry_dialog_header_divider:I = 0x7f0b00b9

.field public static final pick_entry_dialog_header_text:I = 0x7f0b00b7

.field public static final pick_entry_dialog_selected:I = 0x7f0b00b8

.field public static final pin_button_text_color:I = 0x7f0b00b4

.field public static final primary_dark_material_dark:I = 0x7f0b0036

.field public static final primary_dark_material_light:I = 0x7f0b0037

.field public static final primary_material_dark:I = 0x7f0b0034

.field public static final primary_material_light:I = 0x7f0b0035

.field public static final primary_text:I = 0x7f0b00ac

.field public static final primary_text_default_material_dark:I = 0x7f0b0052

.field public static final primary_text_default_material_light:I = 0x7f0b0050

.field public static final primary_text_disabled_material_dark:I = 0x7f0b0056

.field public static final primary_text_disabled_material_light:I = 0x7f0b0054

.field public static final progress_background:I = 0x7f0b00be

.field public static final progress_blue:I = 0x7f0b0091

.field public static final progress_dark_green:I = 0x7f0b0093

.field public static final progress_green:I = 0x7f0b0092

.field public static final progress_indicator:I = 0x7f0b00bf

.field public static final progress_title_background:I = 0x7f0b0095

.field public static final progress_title_foreground:I = 0x7f0b0096

.field public static final progress_yellow:I = 0x7f0b0094

.field public static final projector_progress_bg:I = 0x7f0b00d2

.field public static final punch_grey_background:I = 0x7f0b00a9

.field public static final punch_speaker_note_title:I = 0x7f0b00a7

.field public static final punch_speaker_note_title_underline:I = 0x7f0b00a8

.field public static final punch_status_text_color:I = 0x7f0b00aa

.field public static final recent_empty_list_message_subtitle_color:I = 0x7f0b0087

.field public static final recent_empty_list_message_title_color:I = 0x7f0b0086

.field public static final ripple_material_dark:I = 0x7f0b0038

.field public static final ripple_material_light:I = 0x7f0b0039

.field public static final secondary_text:I = 0x7f0b00ad

.field public static final secondary_text_default_material_dark:I = 0x7f0b0053

.field public static final secondary_text_default_material_light:I = 0x7f0b0051

.field public static final secondary_text_disabled_material_dark:I = 0x7f0b0057

.field public static final secondary_text_disabled_material_light:I = 0x7f0b0055

.field public static final selection_active_drop_target_bg:I = 0x7f0b00cc

.field public static final selection_active_drop_target_fg:I = 0x7f0b00cd

.field public static final selection_drop_zone_bg:I = 0x7f0b00c9

.field public static final selection_highlight_solid:I = 0x7f0b00ca

.field public static final selection_highlight_solid_non_transparent:I = 0x7f0b00cb

.field public static final selection_selected_mask:I = 0x7f0b00ce

.field public static final selection_selected_thumbnail_mask:I = 0x7f0b00cf

.field public static final sharing_grouper_background:I = 0x7f0b0090

.field public static final sharing_list_background:I = 0x7f0b008c

.field public static final sharing_list_email:I = 0x7f0b008f

.field public static final sharing_list_empty_text:I = 0x7f0b008e

.field public static final sharing_list_name:I = 0x7f0b008d

.field public static final slide_thumbnail_activated:I = 0x7f0b00ab

.field public static final snackbar_background:I = 0x7f0b00c6

.field public static final swipe_to_refresh_text_color:I = 0x7f0b0028

.field public static final switch_thumb_normal_material_dark:I = 0x7f0b003e

.field public static final switch_thumb_normal_material_light:I = 0x7f0b003f

.field public static final text_default:I = 0x7f0b010e

.field public static final text_error:I = 0x7f0b010f

.field public static final text_underline:I = 0x7f0b010d

.field public static final thumbnail_border:I = 0x7f0b009a

.field public static final thumbnail_no_thumbnail_background:I = 0x7f0b009b

.field public static final thumbnail_open_button_background:I = 0x7f0b0099

.field public static final undo_button_background_pressed:I = 0x7f0b00c7

.field public static final upload_dialog_header_text:I = 0x7f0b00ba

.field public static final wallet_bright_foreground_disabled_holo_light:I = 0x7f0b0013

.field public static final wallet_bright_foreground_holo_dark:I = 0x7f0b000e

.field public static final wallet_bright_foreground_holo_light:I = 0x7f0b0014

.field public static final wallet_dim_foreground_disabled_holo_dark:I = 0x7f0b0010

.field public static final wallet_dim_foreground_holo_dark:I = 0x7f0b000f

.field public static final wallet_dim_foreground_inverse_disabled_holo_dark:I = 0x7f0b0012

.field public static final wallet_dim_foreground_inverse_holo_dark:I = 0x7f0b0011

.field public static final wallet_highlighted_text_holo_dark:I = 0x7f0b0018

.field public static final wallet_highlighted_text_holo_light:I = 0x7f0b0017

.field public static final wallet_hint_foreground_holo_dark:I = 0x7f0b0016

.field public static final wallet_hint_foreground_holo_light:I = 0x7f0b0015

.field public static final wallet_holo_blue_light:I = 0x7f0b0019

.field public static final wallet_link_text_light:I = 0x7f0b001a

.field public static final wallet_primary_text_holo_light:I = 0x7f0b0122

.field public static final wallet_secondary_text_holo_dark:I = 0x7f0b0123

.field public static final warm_welcome_fake_page:I = 0x7f0b0027

.field public static final warm_welcome_page_1:I = 0x7f0b0110

.field public static final warm_welcome_page_2:I = 0x7f0b0111

.field public static final warm_welcome_page_3:I = 0x7f0b0112

.field public static final widget_text:I = 0x7f0b0097


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2268
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
