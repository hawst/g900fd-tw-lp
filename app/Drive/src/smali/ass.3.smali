.class public Lass;
.super Ljava/lang/Object;
.source "PhotoBackupAnnouncementHelper.java"


# instance fields
.field private final a:LSu;

.field private final a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(LbiP;Laja;LSF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbiP",
            "<",
            "LSu;",
            ">;",
            "Laja",
            "<",
            "Landroid/app/Activity;",
            ">;",
            "LSF;",
            ")V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    invoke-virtual {p1}, LbiP;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LbiP;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSu;

    :goto_0
    iput-object v0, p0, Lass;->a:LSu;

    .line 55
    iput-object p2, p0, Lass;->a:Laja;

    .line 56
    invoke-interface {p3}, LSF;->a()Landroid/accounts/Account;

    move-result-object v0

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v0, p0, Lass;->a:Ljava/lang/String;

    .line 57
    return-void

    .line 54
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lass;)Laja;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lass;->a:Laja;

    return-object v0
.end method


# virtual methods
.method a(Lasv;)V
    .locals 3

    .prologue
    .line 94
    invoke-static {}, LamV;->a()V

    .line 95
    iget-object v0, p0, Lass;->a:LSu;

    if-nez v0, :cond_0

    .line 98
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "AutoBackup is null when showing the photo backup announcement."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :cond_0
    iget-object v0, p0, Lass;->a:LSu;

    iget-object v1, p0, Lass;->a:Ljava/lang/String;

    .line 102
    invoke-interface {v0, v1}, LSu;->b(Ljava/lang/String;)LbsU;

    move-result-object v0

    .line 103
    new-instance v1, Lasu;

    invoke-direct {v1, p0, p1}, Lasu;-><init>(Lass;Lasv;)V

    .line 122
    invoke-static {}, Lanj;->a()Ljava/util/concurrent/Executor;

    move-result-object v2

    invoke-static {v0, v1, v2}, LbsK;->a(LbsU;LbsJ;Ljava/util/concurrent/Executor;)V

    .line 123
    return-void
.end method

.method public a(Lasw;)V
    .locals 3

    .prologue
    .line 65
    invoke-static {}, LamV;->a()V

    .line 66
    iget-object v0, p0, Lass;->a:LSu;

    if-nez v0, :cond_0

    .line 69
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "AutoBackup is null when showing the photo backup announcement."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_0
    iget-object v0, p0, Lass;->a:LSu;

    iget-object v1, p0, Lass;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LSu;->a(Ljava/lang/String;)LbsU;

    move-result-object v0

    .line 73
    new-instance v1, Last;

    invoke-direct {v1, p0, p1}, Last;-><init>(Lass;Lasw;)V

    .line 85
    invoke-static {}, Lanj;->a()Ljava/util/concurrent/Executor;

    move-result-object v2

    invoke-static {v0, v1, v2}, LbsK;->a(LbsU;LbsJ;Ljava/util/concurrent/Executor;)V

    .line 86
    return-void
.end method
