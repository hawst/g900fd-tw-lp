.class public final LahE;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LahP;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LahL;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LahC;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LahQ;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LahI;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LahD;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lahw;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LahB;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LahO;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Lahy;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LahK;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LahT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 48
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 49
    iput-object p1, p0, LahE;->a:LbrA;

    .line 50
    const-class v0, LahP;

    invoke-static {v0, v2}, LahE;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LahE;->a:Lbsk;

    .line 53
    const-class v0, LahL;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LahE;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LahE;->b:Lbsk;

    .line 56
    const-class v0, LahC;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LahE;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LahE;->c:Lbsk;

    .line 59
    const-class v0, LahQ;

    const-class v1, LbuO;

    invoke-static {v0, v1}, LahE;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LahE;->d:Lbsk;

    .line 62
    const-class v0, LahI;

    invoke-static {v0, v2}, LahE;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LahE;->e:Lbsk;

    .line 65
    const-class v0, LahD;

    invoke-static {v0, v2}, LahE;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LahE;->f:Lbsk;

    .line 68
    const-class v0, Lahw;

    invoke-static {v0, v2}, LahE;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LahE;->g:Lbsk;

    .line 71
    const-class v0, LahB;

    invoke-static {v0, v2}, LahE;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LahE;->h:Lbsk;

    .line 74
    const-class v0, LahO;

    invoke-static {v0, v2}, LahE;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LahE;->i:Lbsk;

    .line 77
    const-class v0, Lahy;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LahE;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LahE;->j:Lbsk;

    .line 80
    const-class v0, LahK;

    invoke-static {v0, v2}, LahE;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LahE;->k:Lbsk;

    .line 83
    const-class v0, LahT;

    invoke-static {v0, v2}, LahE;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LahE;->l:Lbsk;

    .line 86
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 165
    sparse-switch p1, :sswitch_data_0

    .line 413
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 167
    :sswitch_0
    new-instance v0, LahP;

    iget-object v1, p0, LahE;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LadM;

    iget-object v1, v1, LadM;->p:Lbsk;

    .line 170
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LahE;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LadM;

    iget-object v2, v2, LadM;->p:Lbsk;

    .line 168
    invoke-static {v1, v2}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laeb;

    iget-object v2, p0, LahE;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LalC;

    iget-object v2, v2, LalC;->K:Lbsk;

    .line 176
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LahE;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->K:Lbsk;

    .line 174
    invoke-static {v2, v3}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lalo;

    iget-object v3, p0, LahE;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaGH;

    iget-object v3, v3, LaGH;->l:Lbsk;

    .line 182
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LahE;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaGH;

    iget-object v4, v4, LaGH;->l:Lbsk;

    .line 180
    invoke-static {v3, v4}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaGM;

    iget-object v4, p0, LahE;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lajo;

    iget-object v4, v4, Lajo;->X:Lbsk;

    .line 188
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LahE;->a:LbrA;

    iget-object v5, v5, LbrA;->a:Lajo;

    iget-object v5, v5, Lajo;->X:Lbsk;

    .line 186
    invoke-static {v4, v5}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Laja;

    iget-object v5, p0, LahE;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LalC;

    iget-object v5, v5, LalC;->O:Lbsk;

    .line 194
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LahE;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LalC;

    iget-object v6, v6, LalC;->O:Lbsk;

    .line 192
    invoke-static {v5, v6}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LajO;

    iget-object v6, p0, LahE;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LMY;

    iget-object v6, v6, LMY;->b:Lbsk;

    .line 200
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LahE;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LMY;

    iget-object v7, v7, LMY;->b:Lbsk;

    .line 198
    invoke-static {v6, v7}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LNc;

    invoke-direct/range {v0 .. v6}, LahP;-><init>(Laeb;Lalo;LaGM;Laja;LajO;LNc;)V

    .line 411
    :goto_0
    return-object v0

    .line 207
    :sswitch_1
    new-instance v0, LahL;

    iget-object v1, p0, LahE;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->j:Lbsk;

    .line 210
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LahE;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaGH;

    iget-object v2, v2, LaGH;->j:Lbsk;

    .line 208
    invoke-static {v1, v2}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGg;

    iget-object v2, p0, LahE;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LahE;

    iget-object v2, v2, LahE;->i:Lbsk;

    .line 216
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LahE;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LahE;

    iget-object v3, v3, LahE;->i:Lbsk;

    .line 214
    invoke-static {v2, v3}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LahO;

    iget-object v3, p0, LahE;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LahE;

    iget-object v3, v3, LahE;->e:Lbsk;

    .line 222
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LahE;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LahE;

    iget-object v4, v4, LahE;->e:Lbsk;

    .line 220
    invoke-static {v3, v4}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LahI;

    iget-object v4, p0, LahE;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LQH;

    iget-object v4, v4, LQH;->d:Lbsk;

    .line 228
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LahE;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LQH;

    iget-object v5, v5, LQH;->d:Lbsk;

    .line 226
    invoke-static {v4, v5}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LQr;

    iget-object v5, p0, LahE;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LagN;

    iget-object v5, v5, LagN;->L:Lbsk;

    .line 234
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LahE;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LagN;

    iget-object v6, v6, LagN;->L:Lbsk;

    .line 232
    invoke-static {v5, v6}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lagd;

    iget-object v6, p0, LahE;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LalC;

    iget-object v6, v6, LalC;->F:Lbsk;

    .line 240
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LahE;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LalC;

    iget-object v7, v7, LalC;->F:Lbsk;

    .line 238
    invoke-static {v6, v7}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LZS;

    invoke-direct/range {v0 .. v6}, LahL;-><init>(LaGg;LahO;LahI;LQr;Lagd;LZS;)V

    goto/16 :goto_0

    .line 247
    :sswitch_2
    new-instance v0, LahC;

    iget-object v1, p0, LahE;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->j:Lbsk;

    .line 250
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LahE;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaGH;

    iget-object v2, v2, LaGH;->j:Lbsk;

    .line 248
    invoke-static {v1, v2}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGg;

    iget-object v2, p0, LahE;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaGH;

    iget-object v2, v2, LaGH;->i:Lbsk;

    .line 256
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LahE;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaGH;

    iget-object v3, v3, LaGH;->i:Lbsk;

    .line 254
    invoke-static {v2, v3}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LaHa;

    iget-object v3, p0, LahE;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Lajo;

    iget-object v3, v3, Lajo;->X:Lbsk;

    .line 262
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LahE;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Lajo;

    iget-object v4, v4, Lajo;->X:Lbsk;

    .line 260
    invoke-static {v3, v4}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Laja;

    iget-object v4, p0, LahE;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LahE;

    iget-object v4, v4, LahE;->k:Lbsk;

    .line 268
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LahE;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LahE;

    iget-object v5, v5, LahE;->k:Lbsk;

    .line 266
    invoke-static {v4, v5}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LahK;

    iget-object v5, p0, LahE;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LahE;

    iget-object v5, v5, LahE;->a:Lbsk;

    .line 274
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LahE;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LahE;

    iget-object v6, v6, LahE;->a:Lbsk;

    .line 272
    invoke-static {v5, v6}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LahP;

    iget-object v6, p0, LahE;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LagN;

    iget-object v6, v6, LagN;->L:Lbsk;

    .line 280
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LahE;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LagN;

    iget-object v7, v7, LagN;->L:Lbsk;

    .line 278
    invoke-static {v6, v7}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lagd;

    iget-object v7, p0, LahE;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LahE;

    iget-object v7, v7, LahE;->i:Lbsk;

    .line 286
    invoke-virtual {v7}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v7

    iget-object v8, p0, LahE;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LahE;

    iget-object v8, v8, LahE;->i:Lbsk;

    .line 284
    invoke-static {v7, v8}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LahO;

    invoke-direct/range {v0 .. v7}, LahC;-><init>(LaGg;LaHa;Laja;LahK;LahP;Lagd;LahO;)V

    goto/16 :goto_0

    .line 293
    :sswitch_3
    new-instance v0, LahQ;

    invoke-direct {v0}, LahQ;-><init>()V

    .line 295
    iget-object v1, p0, LahE;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LahE;

    .line 296
    invoke-virtual {v1, v0}, LahE;->a(LahQ;)V

    goto/16 :goto_0

    .line 299
    :sswitch_4
    new-instance v0, LahI;

    iget-object v1, p0, LahE;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LagN;

    iget-object v1, v1, LagN;->L:Lbsk;

    .line 302
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LahE;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LagN;

    iget-object v2, v2, LagN;->L:Lbsk;

    .line 300
    invoke-static {v1, v2}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lagd;

    iget-object v2, p0, LahE;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LalC;

    iget-object v2, v2, LalC;->S:Lbsk;

    .line 308
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LahE;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->S:Lbsk;

    .line 306
    invoke-static {v2, v3}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LaKM;

    iget-object v3, p0, LahE;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaGH;

    iget-object v3, v3, LaGH;->j:Lbsk;

    .line 314
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LahE;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaGH;

    iget-object v4, v4, LaGH;->j:Lbsk;

    .line 312
    invoke-static {v3, v4}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaGg;

    iget-object v4, p0, LahE;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LahE;

    iget-object v4, v4, LahE;->f:Lbsk;

    .line 320
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LahE;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LahE;

    iget-object v5, v5, LahE;->f:Lbsk;

    .line 318
    invoke-static {v4, v5}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LahD;

    iget-object v5, p0, LahE;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LahE;

    iget-object v5, v5, LahE;->i:Lbsk;

    .line 326
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LahE;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LahE;

    iget-object v6, v6, LahE;->i:Lbsk;

    .line 324
    invoke-static {v5, v6}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LahO;

    iget-object v6, p0, LahE;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LahE;

    iget-object v6, v6, LahE;->a:Lbsk;

    .line 332
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LahE;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LahE;

    iget-object v7, v7, LahE;->a:Lbsk;

    .line 330
    invoke-static {v6, v7}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LahP;

    iget-object v7, p0, LahE;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LalC;

    iget-object v7, v7, LalC;->L:Lbsk;

    .line 338
    invoke-virtual {v7}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v7

    iget-object v8, p0, LahE;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LalC;

    iget-object v8, v8, LalC;->L:Lbsk;

    .line 336
    invoke-static {v7, v8}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LaKR;

    iget-object v8, p0, LahE;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LalC;

    iget-object v8, v8, LalC;->B:Lbsk;

    .line 344
    invoke-virtual {v8}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v8

    iget-object v9, p0, LahE;->a:LbrA;

    iget-object v9, v9, LbrA;->a:LalC;

    iget-object v9, v9, LalC;->B:Lbsk;

    .line 342
    invoke-static {v8, v9}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lamn;

    iget-object v9, p0, LahE;->a:LbrA;

    iget-object v9, v9, LbrA;->a:Lajo;

    iget-object v9, v9, Lajo;->A:Lbsk;

    .line 350
    invoke-virtual {v9}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v9

    iget-object v10, p0, LahE;->a:LbrA;

    iget-object v10, v10, LbrA;->a:Lajo;

    iget-object v10, v10, Lajo;->A:Lbsk;

    .line 348
    invoke-static {v9, v10}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Laja;

    iget-object v10, p0, LahE;->a:LbrA;

    iget-object v10, v10, LbrA;->a:LpG;

    iget-object v10, v10, LpG;->m:Lbsk;

    .line 356
    invoke-virtual {v10}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v10

    iget-object v11, p0, LahE;->a:LbrA;

    iget-object v11, v11, LbrA;->a:LpG;

    iget-object v11, v11, LpG;->m:Lbsk;

    .line 354
    invoke-static {v10, v11}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LtK;

    iget-object v11, p0, LahE;->a:LbrA;

    iget-object v11, v11, LbrA;->a:LqD;

    iget-object v11, v11, LqD;->c:Lbsk;

    .line 362
    invoke-virtual {v11}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v11

    iget-object v12, p0, LahE;->a:LbrA;

    iget-object v12, v12, LbrA;->a:LqD;

    iget-object v12, v12, LqD;->c:Lbsk;

    .line 360
    invoke-static {v11, v12}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, LqK;

    invoke-direct/range {v0 .. v11}, LahI;-><init>(Lagd;LaKM;LaGg;LahD;LahO;LahP;LaKR;Lamn;Laja;LtK;LqK;)V

    goto/16 :goto_0

    .line 369
    :sswitch_5
    new-instance v4, LahD;

    iget-object v0, p0, LahE;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 372
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LahE;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 370
    invoke-static {v0, v1}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iget-object v1, p0, LahE;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->S:Lbsk;

    .line 378
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LahE;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lajo;

    iget-object v2, v2, Lajo;->S:Lbsk;

    .line 376
    invoke-static {v1, v2}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laja;

    iget-object v2, p0, LahE;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Laek;

    iget-object v2, v2, Laek;->h:Lbsk;

    .line 384
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LahE;->a:LbrA;

    iget-object v3, v3, LbrA;->a:Laek;

    iget-object v3, v3, Laek;->h:Lbsk;

    .line 382
    invoke-static {v2, v3}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Laer;

    iget-object v3, p0, LahE;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LagN;

    iget-object v3, v3, LagN;->J:Lbsk;

    .line 390
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, LahE;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LagN;

    iget-object v5, v5, LagN;->J:Lbsk;

    .line 388
    invoke-static {v3, v5}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lagl;

    invoke-direct {v4, v0, v1, v2, v3}, LahD;-><init>(LaGM;Laja;Laer;Lagl;)V

    move-object v0, v4

    .line 395
    goto/16 :goto_0

    .line 397
    :sswitch_6
    new-instance v2, Lahw;

    iget-object v0, p0, LahE;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->al:Lbsk;

    .line 400
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LahE;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->al:Lbsk;

    .line 398
    invoke-static {v0, v1}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iget-object v1, p0, LahE;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->L:Lbsk;

    .line 406
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LahE;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->L:Lbsk;

    .line 404
    invoke-static {v1, v3}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaKR;

    invoke-direct {v2, v0, v1}, Lahw;-><init>(Laja;LaKR;)V

    move-object v0, v2

    .line 411
    goto/16 :goto_0

    .line 165
    nop

    :sswitch_data_0
    .sparse-switch
        0x155 -> :sswitch_0
        0x15a -> :sswitch_2
        0x15b -> :sswitch_1
        0x15d -> :sswitch_4
        0x165 -> :sswitch_3
        0x166 -> :sswitch_5
        0x169 -> :sswitch_6
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 434
    sparse-switch p2, :sswitch_data_0

    .line 485
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 436
    :sswitch_0
    check-cast p1, LahA;

    .line 438
    iget-object v0, p0, LahE;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LahE;

    iget-object v0, v0, LahE;->c:Lbsk;

    .line 441
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahC;

    .line 438
    invoke-virtual {p1, v0}, LahA;->provideContentSyncScheduler(LahC;)LahB;

    move-result-object v0

    .line 478
    :goto_0
    return-object v0

    .line 445
    :sswitch_1
    check-cast p1, LahA;

    .line 447
    iget-object v0, p0, LahE;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->j:Lbsk;

    .line 450
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGg;

    .line 447
    invoke-virtual {p1, v0}, LahA;->provideSyncRequestModelLoader(LaGg;)LahO;

    move-result-object v0

    goto :goto_0

    .line 454
    :sswitch_2
    check-cast p1, LahA;

    .line 456
    iget-object v0, p0, LahE;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LahE;

    iget-object v0, v0, LahE;->b:Lbsk;

    .line 459
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahL;

    iget-object v1, p0, LahE;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LbiH;

    iget-object v1, v1, LbiH;->aU:Lbsk;

    .line 463
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LbiP;

    .line 456
    invoke-virtual {p1, v0, v1}, LahA;->provideContentSyncInformation(LahL;LbiP;)Lahy;

    move-result-object v0

    goto :goto_0

    .line 467
    :sswitch_3
    check-cast p1, LahA;

    .line 469
    iget-object v0, p0, LahE;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LahE;

    iget-object v0, v0, LahE;->b:Lbsk;

    .line 472
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahL;

    .line 469
    invoke-virtual {p1, v0}, LahA;->provideSyncTaskQueue(LahL;)LahK;

    move-result-object v0

    goto :goto_0

    .line 476
    :sswitch_4
    check-cast p1, LahA;

    .line 478
    iget-object v0, p0, LahE;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LahE;

    iget-object v0, v0, LahE;->c:Lbsk;

    .line 481
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahC;

    .line 478
    invoke-virtual {p1, v0}, LahA;->provideTransactionSafeContentSyncScheduler(LahC;)LahT;

    move-result-object v0

    goto :goto_0

    .line 434
    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x72 -> :sswitch_3
        0xfc -> :sswitch_2
        0x15c -> :sswitch_1
        0x164 -> :sswitch_4
    .end sparse-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 118
    const-class v0, LahQ;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x28

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LahE;->a(LbuP;LbuB;)V

    .line 121
    const-class v0, LahP;

    iget-object v1, p0, LahE;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LahE;->a(Ljava/lang/Class;Lbsk;)V

    .line 122
    const-class v0, LahL;

    iget-object v1, p0, LahE;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LahE;->a(Ljava/lang/Class;Lbsk;)V

    .line 123
    const-class v0, LahC;

    iget-object v1, p0, LahE;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LahE;->a(Ljava/lang/Class;Lbsk;)V

    .line 124
    const-class v0, LahQ;

    iget-object v1, p0, LahE;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LahE;->a(Ljava/lang/Class;Lbsk;)V

    .line 125
    const-class v0, LahI;

    iget-object v1, p0, LahE;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LahE;->a(Ljava/lang/Class;Lbsk;)V

    .line 126
    const-class v0, LahD;

    iget-object v1, p0, LahE;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, LahE;->a(Ljava/lang/Class;Lbsk;)V

    .line 127
    const-class v0, Lahw;

    iget-object v1, p0, LahE;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, LahE;->a(Ljava/lang/Class;Lbsk;)V

    .line 128
    const-class v0, LahB;

    iget-object v1, p0, LahE;->h:Lbsk;

    invoke-virtual {p0, v0, v1}, LahE;->a(Ljava/lang/Class;Lbsk;)V

    .line 129
    const-class v0, LahO;

    iget-object v1, p0, LahE;->i:Lbsk;

    invoke-virtual {p0, v0, v1}, LahE;->a(Ljava/lang/Class;Lbsk;)V

    .line 130
    const-class v0, Lahy;

    iget-object v1, p0, LahE;->j:Lbsk;

    invoke-virtual {p0, v0, v1}, LahE;->a(Ljava/lang/Class;Lbsk;)V

    .line 131
    const-class v0, LahK;

    iget-object v1, p0, LahE;->k:Lbsk;

    invoke-virtual {p0, v0, v1}, LahE;->a(Ljava/lang/Class;Lbsk;)V

    .line 132
    const-class v0, LahT;

    iget-object v1, p0, LahE;->l:Lbsk;

    invoke-virtual {p0, v0, v1}, LahE;->a(Ljava/lang/Class;Lbsk;)V

    .line 133
    iget-object v0, p0, LahE;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x155

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 135
    iget-object v0, p0, LahE;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x15b

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 137
    iget-object v0, p0, LahE;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x15a

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 139
    iget-object v0, p0, LahE;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x165

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 141
    iget-object v0, p0, LahE;->e:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x15d

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 143
    iget-object v0, p0, LahE;->f:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x166

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 145
    iget-object v0, p0, LahE;->g:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x169

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 147
    iget-object v0, p0, LahE;->h:Lbsk;

    const-class v1, LahA;

    const/16 v2, 0x13

    invoke-virtual {p0, v1, v2}, LahE;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 149
    iget-object v0, p0, LahE;->i:Lbsk;

    const-class v1, LahA;

    const/16 v2, 0x15c

    invoke-virtual {p0, v1, v2}, LahE;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 151
    iget-object v0, p0, LahE;->j:Lbsk;

    const-class v1, LahA;

    const/16 v2, 0xfc

    invoke-virtual {p0, v1, v2}, LahE;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 153
    iget-object v0, p0, LahE;->k:Lbsk;

    const-class v1, LahA;

    const/16 v2, 0x72

    invoke-virtual {p0, v1, v2}, LahE;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 155
    iget-object v0, p0, LahE;->l:Lbsk;

    const-class v1, LahA;

    const/16 v2, 0x164

    invoke-virtual {p0, v1, v2}, LahE;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 157
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 420
    packed-switch p1, :pswitch_data_0

    .line 428
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 422
    :pswitch_0
    check-cast p2, LahQ;

    .line 424
    iget-object v0, p0, LahE;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LahE;

    .line 425
    invoke-virtual {v0, p2}, LahE;->a(LahQ;)V

    .line 430
    return-void

    .line 420
    :pswitch_data_0
    .packed-switch 0x28
        :pswitch_0
    .end packed-switch
.end method

.method public a(LahQ;)V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, LahE;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LagN;

    iget-object v0, v0, LagN;->E:Lbsk;

    .line 95
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LahE;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LagN;

    iget-object v1, v1, LagN;->E:Lbsk;

    .line 93
    invoke-static {v0, v1}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LagZ;

    iput-object v0, p1, LahQ;->a:LagZ;

    .line 99
    iget-object v0, p0, LahE;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LMY;

    iget-object v0, v0, LMY;->c:Lbsk;

    .line 102
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LahE;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LMY;

    iget-object v1, v1, LMY;->c:Lbsk;

    .line 100
    invoke-static {v0, v1}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNe;

    iput-object v0, p1, LahQ;->a:LNe;

    .line 106
    iget-object v0, p0, LahE;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->j:Lbsk;

    .line 109
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LahE;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->j:Lbsk;

    .line 107
    invoke-static {v0, v1}, LahE;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGg;

    iput-object v0, p1, LahQ;->a:LaGg;

    .line 113
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 161
    return-void
.end method
