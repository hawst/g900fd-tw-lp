.class public final Lwi;
.super Ljava/lang/Object;
.source "MimeTypeCriterion.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 25
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v1

    .line 26
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 27
    new-instance v3, Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;

    .line 28
    invoke-static {v1}, LbmY;->a([Ljava/lang/Object;)LbmY;

    move-result-object v1

    if-ne v2, v0, :cond_0

    :goto_0
    invoke-direct {v3, v1, v0}, Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;-><init>(LbmY;Z)V

    return-object v3

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)[Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;
    .locals 1

    .prologue
    .line 33
    new-array v0, p1, [Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lwi;->a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lwi;->a(I)[Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;

    move-result-object v0

    return-object v0
.end method
