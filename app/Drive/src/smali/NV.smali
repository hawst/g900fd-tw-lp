.class public abstract enum LNV;
.super Ljava/lang/Enum;
.source "VideoController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LNV;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LNV;

.field private static final synthetic a:[LNV;

.field public static final enum b:LNV;

.field public static final enum c:LNV;


# instance fields
.field private final a:I

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 61
    new-instance v0, LNW;

    const-string v1, "PLAYING"

    sget v2, LNI;->ic_vidcontrol_pause:I

    sget v3, LNN;->video_controller_pause:I

    invoke-direct {v0, v1, v4, v2, v3}, LNW;-><init>(Ljava/lang/String;III)V

    sput-object v0, LNV;->a:LNV;

    .line 67
    new-instance v0, LNX;

    const-string v1, "PAUSED"

    sget v2, LNI;->ic_vidcontrol_play:I

    sget v3, LNN;->video_controller_play:I

    invoke-direct {v0, v1, v5, v2, v3}, LNX;-><init>(Ljava/lang/String;III)V

    sput-object v0, LNV;->b:LNV;

    .line 73
    new-instance v0, LNY;

    const-string v1, "COMPLETED"

    sget v2, LNI;->ic_vidcontrol_reload:I

    sget v3, LNN;->video_controller_replay:I

    invoke-direct {v0, v1, v6, v2, v3}, LNY;-><init>(Ljava/lang/String;III)V

    sput-object v0, LNV;->c:LNV;

    .line 60
    const/4 v0, 0x3

    new-array v0, v0, [LNV;

    sget-object v1, LNV;->a:LNV;

    aput-object v1, v0, v4

    sget-object v1, LNV;->b:LNV;

    aput-object v1, v0, v5

    sget-object v1, LNV;->c:LNV;

    aput-object v1, v0, v6

    sput-object v0, LNV;->a:[LNV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 84
    iput p3, p0, LNV;->a:I

    .line 85
    iput p4, p0, LNV;->b:I

    .line 86
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IIILNQ;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3, p4}, LNV;-><init>(Ljava/lang/String;III)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LNV;
    .locals 1

    .prologue
    .line 60
    const-class v0, LNV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LNV;

    return-object v0
.end method

.method public static values()[LNV;
    .locals 1

    .prologue
    .line 60
    sget-object v0, LNV;->a:[LNV;

    invoke-virtual {v0}, [LNV;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LNV;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, LNV;->a:I

    return v0
.end method

.method public a(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 93
    iget v0, p0, LNV;->b:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(Lcom/google/android/apps/docs/drive/media/VideoController;)V
.end method
