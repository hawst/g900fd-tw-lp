.class public LayV;
.super Lazf;


# static fields
.field private static a:LayV;

.field private static a:Z


# instance fields
.field private a:Landroid/content/Context;

.field private a:LazA;

.field private a:Lazb;

.field private a:Lazm;

.field private volatile a:Ljava/lang/Boolean;

.field private a:Ljava/lang/String;

.field private a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LayW;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/String;

.field private b:Z

.field private c:Z


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LayR;->a(Landroid/content/Context;)LayR;

    move-result-object v0

    invoke-static {}, LayO;->a()LayO;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, LayV;-><init>(Landroid/content/Context;LazA;Lazm;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;LazA;Lazm;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lazf;-><init>()V

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LayV;->a:Ljava/lang/Boolean;

    iput-boolean v1, p0, LayV;->c:Z

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LayV;->a:Landroid/content/Context;

    iput-object p2, p0, LayV;->a:LazA;

    iput-object p3, p0, LayV;->a:Lazm;

    iget-object v0, p0, LayV;->a:Landroid/content/Context;

    invoke-static {v0}, LazB;->a(Landroid/content/Context;)V

    iget-object v0, p0, LayV;->a:Landroid/content/Context;

    invoke-static {v0}, Lazl;->a(Landroid/content/Context;)V

    iget-object v0, p0, LayV;->a:Landroid/content/Context;

    invoke-static {v0}, LazC;->a(Landroid/content/Context;)V

    new-instance v0, LazM;

    invoke-direct {v0}, LazM;-><init>()V

    iput-object v0, p0, LayV;->a:Lazb;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LayV;->a:Ljava/util/Set;

    invoke-direct {p0}, LayV;->b()V

    return-void
.end method

.method private a(Ljava/lang/String;)I
    .locals 2

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "verbose"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const-string v1, "info"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const-string v1, "warning"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    const-string v1, "error"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x3

    goto :goto_0

    :cond_3
    const/4 v0, -0x1

    goto :goto_0
.end method

.method static a()LayV;
    .locals 2

    const-class v1, LayV;

    monitor-enter v1

    :try_start_0
    sget-object v0, LayV;->a:LayV;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Landroid/content/Context;)LayV;
    .locals 2

    const-class v1, LayV;

    monitor-enter v1

    :try_start_0
    sget-object v0, LayV;->a:LayV;

    if-nez v0, :cond_0

    new-instance v0, LayV;

    invoke-direct {v0, p0}, LayV;-><init>(Landroid/content/Context;)V

    sput-object v0, LayV;->a:LayV;

    :cond_0
    sget-object v0, LayV;->a:LayV;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Lazc;)Lazc;
    .locals 2

    iget-object v0, p0, LayV;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "&an"

    iget-object v1, p0, LayV;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lazc;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, LayV;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "&av"

    iget-object v1, p0, LayV;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lazc;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-object p1
.end method

.method private b()V
    .locals 4

    sget-boolean v0, LayV;->a:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, LayV;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v2, p0, LayV;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x81

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    if-nez v0, :cond_2

    const-string v0, "Couldn\'t get ApplicationInfo to load gloabl config."

    invoke-static {v0}, LaAj;->d(Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PackageManager doesn\'t know about package: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaAj;->c(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_1

    :cond_2
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    const-string v1, "com.google.android.gms.analytics.globalConfigResource"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    new-instance v1, LaAd;

    iget-object v2, p0, LayV;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, LaAd;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, LaAd;->a(I)LazJ;

    move-result-object v0

    check-cast v0, LaAf;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, LayV;->a(LaAf;)V

    goto :goto_0
.end method


# virtual methods
.method public a()Lazb;
    .locals 1

    iget-object v0, p0, LayV;->a:Lazb;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lazc;
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, LaAb;->a()LaAb;

    move-result-object v0

    sget-object v1, LaAc;->N:LaAc;

    invoke-virtual {v0, v1}, LaAb;->a(LaAc;)V

    new-instance v0, Lazc;

    iget-object v1, p0, LayV;->a:Landroid/content/Context;

    invoke-direct {v0, p1, p0, v1}, Lazc;-><init>(Ljava/lang/String;Lazf;Landroid/content/Context;)V

    invoke-direct {p0, v0}, LayV;->a(Lazc;)Lazc;

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, LayV;->a:Lazm;

    invoke-virtual {v0}, Lazm;->a()V

    return-void
.end method

.method a(LaAf;)V
    .locals 3

    const-string v0, "Loading global config values."

    invoke-static {v0}, LaAj;->c(Ljava/lang/String;)V

    invoke-virtual {p1}, LaAf;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LaAf;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LayV;->b:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "app name loaded: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LayV;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaAj;->c(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, LaAf;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, LaAf;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LayV;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "app version loaded: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LayV;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaAj;->c(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, LaAf;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, LaAf;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LayV;->a(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "log level loaded: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LaAj;->c(Ljava/lang/String;)V

    invoke-virtual {p0}, LayV;->a()Lazb;

    move-result-object v1

    invoke-interface {v1, v0}, Lazb;->a(I)V

    :cond_2
    invoke-virtual {p1}, LaAf;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LayV;->a:Lazm;

    invoke-virtual {p1}, LaAf;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lazm;->a(I)V

    :cond_3
    invoke-virtual {p1}, LaAf;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, LaAf;->f()Z

    move-result v0

    invoke-virtual {p0, v0}, LayV;->a(Z)V

    :cond_4
    return-void
.end method

.method a(LayW;)V
    .locals 1

    iget-object v0, p0, LayV;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method a(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "hit cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    const-string v0, "&ul"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Lazp;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lazp;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "&sr"

    invoke-static {}, Lazl;->a()Lazl;

    move-result-object v1

    const-string v2, "&sr"

    invoke-virtual {v1, v2}, Lazl;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lazp;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "&_u"

    invoke-static {}, LaAb;->a()LaAb;

    move-result-object v1

    invoke-virtual {v1}, LaAb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, LaAb;->a()LaAb;

    move-result-object v0

    invoke-virtual {v0}, LaAb;->a()Ljava/lang/String;

    iget-object v0, p0, LayV;->a:LazA;

    invoke-interface {v0, p1}, LazA;->a(Ljava/util/Map;)V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public a(Z)V
    .locals 2

    invoke-static {}, LaAb;->a()LaAb;

    move-result-object v0

    sget-object v1, LaAc;->ac:LaAc;

    invoke-virtual {v0, v1}, LaAb;->a(LaAc;)V

    iput-boolean p1, p0, LayV;->b:Z

    return-void
.end method

.method public a()Z
    .locals 2

    invoke-static {}, LaAb;->a()LaAb;

    move-result-object v0

    sget-object v1, LaAc;->ad:LaAc;

    invoke-virtual {v0, v1}, LaAb;->a(LaAc;)V

    iget-boolean v0, p0, LayV;->b:Z

    return v0
.end method

.method b(LayW;)V
    .locals 1

    iget-object v0, p0, LayV;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public b()Z
    .locals 2

    invoke-static {}, LaAb;->a()LaAb;

    move-result-object v0

    sget-object v1, LaAc;->R:LaAc;

    invoke-virtual {v0, v1}, LaAb;->a(LaAc;)V

    iget-object v0, p0, LayV;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
