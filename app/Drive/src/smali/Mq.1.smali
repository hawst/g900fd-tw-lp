.class public LMq;
.super Ljava/lang/Object;
.source "CrossAppStateSyncer.java"


# annotations
.annotation runtime Lbxz;
.end annotation


# instance fields
.field private final a:LQr;

.field private final a:LTT;

.field private final a:LaGg;

.field a:LagO;

.field private final a:LahB;

.field private final a:Ljava/util/concurrent/ExecutorService;

.field private final a:LqK;

.field private final a:LtK;


# direct methods
.method public constructor <init>(LaGg;LahB;LTT;LqK;LQr;LtK;)V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, LMq;->a:Ljava/util/concurrent/ExecutorService;

    .line 90
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGg;

    iput-object v0, p0, LMq;->a:LaGg;

    .line 91
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LahB;

    iput-object v0, p0, LMq;->a:LahB;

    .line 92
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTT;

    iput-object v0, p0, LMq;->a:LTT;

    .line 93
    iput-object p4, p0, LMq;->a:LqK;

    .line 94
    iput-object p5, p0, LMq;->a:LQr;

    .line 95
    iput-object p6, p0, LMq;->a:LtK;

    .line 96
    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)I
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 199
    sget-object v0, LMl;->c:LMl;

    invoke-virtual {v0, p2}, LMl;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 200
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 201
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 202
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 203
    const-string v1, "CrossAppStateSyncer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Provider version is: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    :goto_0
    return v0

    :cond_0
    move v0, v6

    goto :goto_0
.end method

.method private a(LMn;)LaGo;
    .locals 6

    .prologue
    .line 261
    invoke-virtual {p1}, LMn;->a()Ljava/lang/String;

    move-result-object v1

    .line 262
    invoke-virtual {p1}, LMn;->a()LaFO;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a(LaFO;Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    .line 264
    iget-object v2, p0, LMq;->a:LaGg;

    invoke-interface {v2}, LaGg;->a()V

    .line 266
    :try_start_0
    iget-object v2, p0, LMq;->a:LaGg;

    invoke-interface {v2, v0}, LaGg;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGb;

    move-result-object v0

    .line 267
    if-nez v0, :cond_1

    .line 269
    iget-object v0, p0, LMq;->a:LaGg;

    invoke-virtual {p1}, LMn;->a()LaFO;

    move-result-object v2

    invoke-interface {v0, v2}, LaGg;->a(LaFO;)LaFM;

    move-result-object v0

    .line 270
    iget-object v2, p0, LMq;->a:LaGg;

    .line 271
    invoke-virtual {p1}, LMn;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v0, v3, v1}, LaGg;->a(LaFM;Ljava/lang/String;Ljava/lang/String;)LaGc;

    move-result-object v0

    .line 273
    invoke-virtual {p1}, LMn;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LaGc;->a(Ljava/lang/String;)LaGc;

    move-result-object v1

    const/4 v2, 0x1

    .line 274
    invoke-virtual {v1, v2}, LaGc;->c(Z)LaGe;

    move-result-object v1

    .line 275
    invoke-virtual {p1}, LMn;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LaGe;->e(Ljava/lang/String;)LaGe;

    .line 276
    invoke-virtual {v0}, LaGc;->b()LaGb;

    move-result-object v0

    .line 277
    iget-object v1, p0, LMq;->a:LaGg;

    invoke-interface {v1}, LaGg;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 286
    :cond_0
    :goto_0
    iget-object v1, p0, LMq;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    return-object v0

    .line 278
    :cond_1
    :try_start_1
    invoke-virtual {v0}, LaGb;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 279
    const-string v2, "CrossAppStateSyncer"

    const-string v3, "Received a pinned document (%s) from a different app which is local only on this app. Probably this app missed the ACK from the server."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 286
    :catchall_0
    move-exception v0

    iget-object v1, p0, LMq;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    throw v0
.end method

.method private a(JI)Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 180
    iget-object v1, p0, LMq;->a:LtK;

    sget-object v2, Lry;->n:Lry;

    invoke-interface {v1, v2}, LtK;->a(LtJ;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-lt p3, v6, :cond_0

    .line 182
    new-instance v1, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LaES;->A:LaES;

    .line 183
    invoke-virtual {v3}, LaES;->a()LaFr;

    move-result-object v3

    invoke-virtual {v3}, LaFr;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " > "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    new-instance v2, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LaES;->z:LaES;

    .line 187
    invoke-virtual {v4}, LaES;->a()LaFr;

    move-result-object v4

    invoke-virtual {v4}, LaFr;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " > "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    sget-object v0, LaES;->y:LaES;

    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->b()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 192
    sget-object v3, LaFL;->b:LaFL;

    const/4 v4, 0x2

    new-array v4, v4, [Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    aput-object v0, v4, v6

    invoke-virtual {v3, v1, v4}, LaFL;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;[Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 194
    :cond_0
    return-object v0
.end method

.method private a(Landroid/database/Cursor;Ljava/util/Set;)Ljava/lang/Long;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/Set",
            "<",
            "LaFO;",
            ">;)",
            "Ljava/lang/Long;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 221
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    :try_start_0
    new-instance v6, LMn;

    invoke-direct {v6, p1}, LMn;-><init>(Landroid/database/Cursor;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 231
    invoke-virtual {v6}, LMn;->a()LaFO;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 257
    :goto_0
    return-object v0

    .line 226
    :catch_0
    move-exception v1

    .line 227
    const-string v2, "CrossAppStateSyncer"

    const-string v3, "StateSyncer cursor does not contain expected columns."

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 235
    :cond_0
    new-instance v1, Landroid/content/SyncResult;

    invoke-direct {v1}, Landroid/content/SyncResult;-><init>()V

    .line 237
    :try_start_1
    iget-object v0, p0, LMq;->a:LagO;

    invoke-virtual {v6}, LMn;->a()LaFO;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, LagO;->a(LaFO;Landroid/content/SyncResult;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 242
    :goto_1
    invoke-virtual {v1}, Landroid/content/SyncResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 243
    const-string v0, "CrossAppStateSyncer"

    const-string v2, "Error performing minimal sync: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v5

    invoke-static {v0, v2, v3}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 249
    :cond_1
    new-instance v2, Ljava/util/Date;

    invoke-virtual {v6}, LMn;->a()J

    move-result-wide v0

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 250
    invoke-direct {p0, v6}, LMq;->a(LMn;)LaGo;

    move-result-object v7

    .line 252
    new-instance v0, LaGZ;

    invoke-virtual {v6}, LMn;->a()Z

    move-result v1

    .line 253
    invoke-virtual {v6}, LMn;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6}, LMn;->f()Ljava/lang/String;

    move-result-object v4

    .line 254
    invoke-virtual {v6}, LMn;->a()Ljava/lang/Long;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LaGZ;-><init>(ZLjava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 255
    iget-object v1, p0, LMq;->a:LahB;

    invoke-interface {v7}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-interface {v1, v2, v0}, LahB;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaGZ;)V

    .line 257
    invoke-virtual {v6}, LMn;->a()J

    move-result-wide v0

    invoke-virtual {v6}, LMn;->b()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 238
    :catch_1
    move-exception v0

    .line 239
    const-string v2, "CrossAppStateSyncer"

    const-string v3, "Exception performing minimal sync"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1
.end method

.method static synthetic a(LMq;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1}, LMq;->b(Landroid/content/Context;)V

    return-void
.end method

.method public static a(Ljava/lang/RuntimeException;LqK;LQr;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CrossAppStateSyncer "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 77
    invoke-virtual {p1, v0, v2}, LqK;->a(Ljava/lang/String;Z)V

    .line 79
    const-string v0, "crossAppStateSyncerHideExceptions"

    .line 80
    invoke-interface {p2, v0, v2}, LQr;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 81
    if-nez v0, :cond_0

    .line 82
    throw p0

    .line 84
    :cond_0
    return-void
.end method

.method private a(I)Z
    .locals 1

    .prologue
    .line 212
    const/4 v0, 0x1

    return v0
.end method

.method private b(Landroid/content/Context;)V
    .locals 14

    .prologue
    .line 117
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 118
    sget-object v0, LaKN;->a:LaKN;

    invoke-virtual {v0}, LaKN;->a()J

    move-result-wide v10

    .line 120
    iget-object v0, p0, LMq;->a:LTT;

    invoke-interface {v0}, LTT;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_0
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/lang/String;

    .line 121
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CROSS_APP_SYNC_LAST_QUERY_TIME_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 124
    const-wide/16 v0, -0x1

    invoke-interface {v7, v13, v0, v1}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 126
    const-wide/16 v8, 0x0

    .line 128
    :try_start_0
    invoke-direct {p0, p1, v6}, LMq;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    .line 129
    invoke-direct {p0, v2}, LMq;->a(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 130
    const-string v0, "CrossAppStateSyncer"

    const-string v1, "Target provider (%s) version incompatible"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v6, v2, v3

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 169
    :catch_0
    move-exception v0

    .line 170
    const-string v1, "CrossAppStateSyncer"

    const-string v2, "Permission denied for provider %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v6, v3, v4

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 171
    iget-object v0, p0, LMq;->a:LqK;

    const-string v1, "CrossAppStateSyncer SecurityException"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 135
    :cond_1
    :try_start_1
    invoke-direct {p0, v0, v1, v2}, LMq;->a(JI)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 136
    if-nez v0, :cond_2

    const/4 v3, 0x0

    .line 139
    :goto_1
    sget-object v0, LMl;->a:LMl;

    invoke-virtual {v0, v6}, LMl;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 141
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 142
    if-eqz v2, :cond_0

    .line 146
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 147
    invoke-static {p1}, LajS;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v4, v1

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v4, :cond_6

    aget-object v5, v1, v0

    .line 148
    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v5}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 147
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 137
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 151
    :goto_3
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 152
    invoke-direct {p0, v2, v3}, LMq;->a(Landroid/database/Cursor;Ljava/util/Set;)Ljava/lang/Long;

    move-result-object v4

    .line 153
    if-nez v4, :cond_3

    .line 154
    const-string v4, "CrossAppStateSyncer"

    const-string v5, "Could not process entry, skipping."

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v4, v5, v8}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    .line 172
    :catch_1
    move-exception v0

    .line 173
    iget-object v1, p0, LMq;->a:LqK;

    iget-object v2, p0, LMq;->a:LQr;

    invoke-static {v0, v1, v2}, LMq;->a(Ljava/lang/RuntimeException;LqK;LQr;)V

    goto/16 :goto_0

    .line 157
    :cond_3
    :try_start_2
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 160
    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_3

    .line 164
    :cond_4
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 165
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 166
    invoke-interface {v2, v13, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 167
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 176
    :cond_5
    return-void

    :cond_6
    move-wide v0, v8

    goto :goto_3
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 105
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 108
    iget-object v1, p0, LMq;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v2, LMr;

    invoke-direct {v2, p0, v0}, LMr;-><init>(LMq;Landroid/content/Context;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 114
    return-void
.end method
