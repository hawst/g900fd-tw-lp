.class public LMM;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "SwipableDocListLayout.java"


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)V
    .locals 0

    .prologue
    .line 324
    iput-object p1, p0, LMM;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;LMI;)V
    .locals 0

    .prologue
    .line 324
    invoke-direct {p0, p1}, LMM;-><init>(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 328
    iget-object v0, p0, LMM;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    sget-object v1, LML;->c:LML;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;LML;)LML;

    .line 329
    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3

    .prologue
    .line 364
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget-object v1, p0, LMM;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v1}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->c(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 365
    const/4 v0, 0x0

    .line 367
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LMM;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-lez v0, :cond_1

    iget-object v0, p0, LMM;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->d(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)I

    move-result v0

    :goto_1
    const/4 v2, -0x1

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;II)Z

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, LMM;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->d(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)I

    move-result v0

    neg-int v0, v0

    goto :goto_1
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 334
    iget-object v2, p0, LMM;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v2}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->b(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 359
    :cond_0
    :goto_0
    return v0

    .line 337
    :cond_1
    iget-object v2, p0, LMM;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v2}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)LML;

    move-result-object v2

    sget-object v3, LML;->c:LML;

    if-ne v2, v3, :cond_3

    .line 340
    invoke-static {p3}, LML;->a(F)LML;

    move-result-object v3

    .line 342
    sget-object v2, LML;->a:LML;

    if-ne v3, v2, :cond_4

    iget-object v2, p0, LMM;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v2}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->b(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 343
    :goto_1
    if-ltz v2, :cond_0

    iget-object v4, p0, LMM;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v4}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)LMK;

    move-result-object v4

    invoke-interface {v4}, LMK;->a()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 347
    iget-object v0, p0, LMM;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v0, v3}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;LML;)LML;

    .line 348
    sget-object v0, LML;->a:LML;

    iget-object v3, p0, LMM;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v3}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)LML;

    move-result-object v3

    if-ne v0, v3, :cond_2

    .line 349
    iget-object v0, p0, LMM;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->c(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)Landroid/view/View;

    move-result-object v0

    iget-object v3, p0, LMM;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v3}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setTranslationX(F)V

    .line 351
    :cond_2
    iget-object v0, p0, LMM;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)LMJ;

    move-result-object v0

    iget-object v3, p0, LMM;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v3}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->b(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)I

    move-result v3

    invoke-interface {v0, v3, v2}, LMJ;->a(II)V

    .line 353
    :cond_3
    iget-object v0, p0, LMM;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    .line 354
    goto :goto_0

    .line 342
    :cond_4
    iget-object v2, p0, LMM;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v2}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->b(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 356
    :cond_5
    iget-object v0, p0, LMM;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->c(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)Landroid/view/View;

    move-result-object v0

    .line 357
    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v2

    sub-float/2addr v2, p3

    .line 358
    iget-object v3, p0, LMM;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-static {v3, v2}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;F)F

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationX(F)V

    move v0, v1

    .line 359
    goto :goto_0
.end method
