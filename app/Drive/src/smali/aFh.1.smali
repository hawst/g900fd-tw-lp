.class public final enum LaFh;
.super Ljava/lang/Enum;
.source "SyncRequestJournalEntryTable.java"

# interfaces
.implements Lbjv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaFh;",
        ">;",
        "Lbjv",
        "<",
        "LaFr;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaFh;

.field private static final synthetic a:[LaFh;

.field public static final enum b:LaFh;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum c:LaFh;

.field public static final enum d:LaFh;

.field public static final enum e:LaFh;


# instance fields
.field private final a:LaFr;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x0

    const/4 v9, 0x1

    const/16 v8, 0x49

    .line 44
    new-instance v0, LaFh;

    const-string v1, "ENTRY_ID"

    invoke-static {}, LaFg;->b()LaFg;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "entryId"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 45
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    .line 46
    invoke-static {}, LaER;->a()LaER;

    move-result-object v4

    invoke-virtual {v3, v4}, LaFG;->a(LaFy;)LaFG;

    move-result-object v3

    .line 45
    invoke-virtual {v2, v8, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, LaFh;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFh;->a:LaFh;

    .line 48
    new-instance v0, LaFh;

    const-string v1, "__LEGACY_SYNC_TYPE"

    .line 49
    invoke-static {}, LaFg;->b()LaFg;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "syncType"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 50
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    const-string v4, "BINARY"

    .line 51
    invoke-virtual {v3, v4}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v3

    .line 50
    invoke-virtual {v2, v8, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    const/16 v3, 0x5d

    .line 52
    invoke-virtual {v2, v3}, LaFt;->b(I)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, LaFh;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFh;->b:LaFh;

    .line 54
    new-instance v0, LaFh;

    const-string v1, "SYNC_DIRECTION"

    invoke-static {}, LaFg;->b()LaFg;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "syncDirection"

    sget-object v5, LaFI;->c:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 55
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    sget-object v4, LaHf;->b:LaHf;

    .line 56
    invoke-virtual {v4}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v3

    .line 55
    invoke-virtual {v2, v8, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, LaFh;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFh;->c:LaFh;

    .line 58
    new-instance v0, LaFh;

    const-string v1, "IS_IMPLICIT"

    invoke-static {}, LaFg;->b()LaFg;

    move-result-object v2

    invoke-static {v2}, LaFt;->a(LaFy;)LaFt;

    move-result-object v2

    new-instance v3, LaFG;

    const-string v4, "isImplicit"

    sget-object v5, LaFI;->a:LaFI;

    invoke-direct {v3, v4, v5}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 59
    invoke-virtual {v3}, LaFG;->b()LaFG;

    move-result-object v3

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, LaFG;->a(Ljava/lang/Object;)LaFG;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v2

    invoke-direct {v0, v1, v12, v2}, LaFh;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFh;->d:LaFh;

    .line 61
    new-instance v0, LaFh;

    const-string v1, "DOCUMENT_CONTENT_ID"

    const/4 v2, 0x4

    invoke-static {}, LaFg;->b()LaFg;

    move-result-object v3

    invoke-static {v3}, LaFt;->a(LaFy;)LaFt;

    move-result-object v3

    new-instance v4, LaFG;

    const-string v5, "documentContentId"

    sget-object v6, LaFI;->a:LaFI;

    invoke-direct {v4, v5, v6}, LaFG;-><init>(Ljava/lang/String;LaFI;)V

    .line 63
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v5

    const/4 v6, 0x0

    sget-object v7, LaFH;->b:LaFH;

    .line 62
    invoke-virtual {v4, v5, v6, v7}, LaFG;->a(LaFy;LaFr;LaFH;)LaFG;

    move-result-object v4

    invoke-virtual {v3, v8, v4}, LaFt;->a(ILaFG;)LaFt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LaFh;-><init>(Ljava/lang/String;ILaFt;)V

    sput-object v0, LaFh;->e:LaFh;

    .line 43
    const/4 v0, 0x5

    new-array v0, v0, [LaFh;

    sget-object v1, LaFh;->a:LaFh;

    aput-object v1, v0, v10

    sget-object v1, LaFh;->b:LaFh;

    aput-object v1, v0, v9

    sget-object v1, LaFh;->c:LaFh;

    aput-object v1, v0, v11

    sget-object v1, LaFh;->d:LaFh;

    aput-object v1, v0, v12

    const/4 v1, 0x4

    sget-object v2, LaFh;->e:LaFh;

    aput-object v2, v0, v1

    sput-object v0, LaFh;->a:[LaFh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaFt;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 68
    invoke-virtual {p3}, LaFt;->a()LaFr;

    move-result-object v0

    iput-object v0, p0, LaFh;->a:LaFr;

    .line 69
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaFh;
    .locals 1

    .prologue
    .line 43
    const-class v0, LaFh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaFh;

    return-object v0
.end method

.method public static values()[LaFh;
    .locals 1

    .prologue
    .line 43
    sget-object v0, LaFh;->a:[LaFh;

    invoke-virtual {v0}, [LaFh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaFh;

    return-object v0
.end method


# virtual methods
.method public a()LaFr;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, LaFh;->a:LaFr;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, LaFh;->a()LaFr;

    move-result-object v0

    return-object v0
.end method
