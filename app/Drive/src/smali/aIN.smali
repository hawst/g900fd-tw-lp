.class public final LaIN;
.super LaVc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaVc",
        "<",
        "LaIN;",
        ">;"
    }
.end annotation


# instance fields
.field public a:I

.field public a:J

.field public a:Ljava/lang/String;

.field public b:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, LaVc;-><init>()V

    invoke-virtual {p0}, LaIN;->a()LaIN;

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 4

    invoke-super {p0}, LaVc;->a()I

    move-result v0

    const/4 v1, 0x1

    iget v2, p0, LaIN;->a:I

    invoke-static {v1, v2}, LaVa;->a(II)I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LaIN;->a:Ljava/lang/String;

    invoke-static {v1, v2}, LaVa;->a(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x3

    iget-wide v2, p0, LaIN;->a:J

    invoke-static {v1, v2, v3}, LaVa;->a(IJ)I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x4

    iget-wide v2, p0, LaIN;->b:J

    invoke-static {v1, v2, v3}, LaVa;->a(IJ)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public a()LaIN;
    .locals 4

    const-wide/16 v2, -0x1

    const/4 v0, 0x1

    iput v0, p0, LaIN;->a:I

    const-string v0, ""

    iput-object v0, p0, LaIN;->a:Ljava/lang/String;

    iput-wide v2, p0, LaIN;->a:J

    iput-wide v2, p0, LaIN;->b:J

    const/4 v0, 0x0

    iput-object v0, p0, LaIN;->a:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, LaIN;->b:I

    return-object p0
.end method

.method public a(LaVa;)V
    .locals 4

    const/4 v0, 0x1

    iget v1, p0, LaIN;->a:I

    invoke-virtual {p1, v0, v1}, LaVa;->a(II)V

    const/4 v0, 0x2

    iget-object v1, p0, LaIN;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, LaVa;->a(ILjava/lang/String;)V

    const/4 v0, 0x3

    iget-wide v2, p0, LaIN;->a:J

    invoke-virtual {p1, v0, v2, v3}, LaVa;->b(IJ)V

    const/4 v0, 0x4

    iget-wide v2, p0, LaIN;->b:J

    invoke-virtual {p1, v0, v2, v3}, LaVa;->b(IJ)V

    invoke-super {p0, p1}, LaVc;->a(LaVa;)V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-ne p1, p0, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, LaIN;

    if-eqz v2, :cond_0

    check-cast p1, LaIN;

    iget v2, p0, LaIN;->a:I

    iget v3, p1, LaIN;->a:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, LaIN;->a:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, LaIN;->a:Ljava/lang/String;

    if-nez v2, :cond_0

    :cond_2
    iget-wide v2, p0, LaIN;->a:J

    iget-wide v4, p1, LaIN;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, LaIN;->b:J

    iget-wide v4, p1, LaIN;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-object v2, p0, LaIN;->a:Ljava/util/List;

    if-eqz v2, :cond_3

    iget-object v2, p0, LaIN;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_3
    iget-object v2, p1, LaIN;->a:Ljava/util/List;

    if-eqz v2, :cond_4

    iget-object v2, p1, LaIN;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, LaIN;->a:Ljava/lang/String;

    iget-object v3, p1, LaIN;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0

    :cond_6
    iget-object v0, p0, LaIN;->a:Ljava/util/List;

    iget-object v1, p1, LaIN;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    const/16 v6, 0x20

    const/4 v1, 0x0

    iget v0, p0, LaIN;->a:I

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, LaIN;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, LaIN;->a:J

    iget-wide v4, p0, LaIN;->a:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, LaIN;->b:J

    iget-wide v4, p0, LaIN;->b:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, LaIN;->a:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, LaIN;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_1
    iget-object v0, p0, LaIN;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v1, p0, LaIN;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
