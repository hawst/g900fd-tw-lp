.class LDy;
.super Ljava/lang/Object;
.source "SelectionViewStateImpl.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field private final a:LDi;

.field final synthetic a:LDt;

.field private final a:Lcom/google/android/apps/docs/doclist/SelectionItem;


# direct methods
.method constructor <init>(LDt;Lcom/google/android/apps/docs/doclist/SelectionItem;LDi;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, LDy;->a:LDt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p2, p0, LDy;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    .line 68
    iput-object p3, p0, LDy;->a:LDi;

    .line 69
    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 73
    iget-object v0, p0, LDy;->a:LDt;

    invoke-static {v0}, LDt;->a(LDt;)LJR;

    move-result-object v0

    iget-object v1, p0, LDy;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    invoke-virtual {v0, v1}, LJR;->a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;)LKb;

    move-result-object v0

    .line 74
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LKb;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, LDy;->a:LDt;

    invoke-static {v0}, LDt;->a(LDt;)LJR;

    move-result-object v0

    iget-object v1, p0, LDy;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LJR;->a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;Z)V

    .line 76
    iget-object v0, p0, LDy;->a:LDt;

    invoke-static {v0}, LDt;->a(LDt;)LKZ;

    move-result-object v0

    iget-object v1, p0, LDy;->a:LDi;

    iget-object v1, v1, LDi;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, LKZ;->a(Landroid/view/View;)V

    .line 86
    :goto_0
    return v3

    .line 78
    :cond_0
    iget-object v0, p0, LDy;->a:LDt;

    invoke-static {v0}, LDt;->a(LDt;)LJR;

    move-result-object v0

    iget-object v1, p0, LDy;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    invoke-virtual {v0, v1, v3}, LJR;->a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;Z)V

    .line 79
    iget-object v0, p0, LDy;->a:LDt;

    invoke-static {v0}, LDt;->a(LDt;)LJR;

    move-result-object v0

    invoke-virtual {v0, v3}, LJR;->a(Z)V

    .line 81
    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    sget v1, Lxc;->selection_floating_handle:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    .line 83
    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->f()V

    .line 84
    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->d()V

    goto :goto_0
.end method
