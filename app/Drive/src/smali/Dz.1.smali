.class public LDz;
.super Ljava/lang/Object;
.source "SlidingDrawerLayoutHelper.java"

# interfaces
.implements LBu;


# instance fields
.field private a:F

.field private final a:LabI;

.field private final a:Landroid/support/v4/widget/DrawerLayout;

.field private final a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

.field private final a:Lf;

.field private final a:Lrm;

.field private final a:Lwm;


# direct methods
.method public constructor <init>(Lrm;LDB;Landroid/view/View;Lcom/google/android/apps/docs/fragment/NavigationFragment;Lwm;Larh;)V
    .locals 3

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrm;

    iput-object v0, p0, LDz;->a:Lrm;

    .line 64
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget v0, Lxc;->nav_drawer:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    iput-object v0, p0, LDz;->a:Landroid/support/v4/widget/DrawerLayout;

    .line 67
    iget-object v0, p0, LDz;->a:Landroid/support/v4/widget/DrawerLayout;

    sget v1, Lxb;->gradient_menu:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/DrawerLayout;->setDrawerShadow(II)V

    .line 68
    iget-object v0, p0, LDz;->a:Landroid/support/v4/widget/DrawerLayout;

    sget v1, Lxb;->gradient_details:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/DrawerLayout;->setDrawerShadow(II)V

    .line 70
    invoke-direct {p0, p1}, LDz;->a(Landroid/app/Activity;)Lf;

    move-result-object v0

    iput-object v0, p0, LDz;->a:Lf;

    .line 75
    iget-object v0, p0, LDz;->a:Lf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lf;->a(Z)V

    .line 78
    iget-object v0, p0, LDz;->a:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->requestLayout()V

    .line 80
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iput-object v0, p0, LDz;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    .line 82
    invoke-virtual {p4}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()LabI;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LabI;

    iput-object v0, p0, LDz;->a:LabI;

    .line 83
    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwm;

    iput-object v0, p0, LDz;->a:Lwm;

    .line 85
    iget-object v0, p0, LDz;->a:Landroid/support/v4/widget/DrawerLayout;

    new-instance v1, LDA;

    invoke-direct {v1, p0, p4, p6, p2}, LDA;-><init>(LDz;Lcom/google/android/apps/docs/fragment/NavigationFragment;Larh;LDB;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerListener(Lgz;)V

    .line 126
    return-void
.end method

.method static synthetic a(LDz;F)F
    .locals 0

    .prologue
    .line 33
    iput p1, p0, LDz;->a:F

    return p1
.end method

.method static synthetic a(LDz;)Landroid/support/v4/widget/DrawerLayout;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, LDz;->a:Landroid/support/v4/widget/DrawerLayout;

    return-object v0
.end method

.method static synthetic a(LDz;)Lf;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, LDz;->a:Lf;

    return-object v0
.end method

.method private a(Landroid/app/Activity;)Lf;
    .locals 6

    .prologue
    .line 129
    invoke-static {p1}, Lamt;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    :try_start_0
    const-class v0, Lf;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Landroid/app/Activity;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Landroid/support/v4/widget/DrawerLayout;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    .line 134
    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 136
    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LDz;->a:Landroid/support/v4/widget/DrawerLayout;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget v3, Lxb;->ic_drawer_light:I

    .line 137
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    sget v3, Lxi;->doclist_open_navigation_drawer_content_description:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    sget v3, Lxi;->doclist_close_navigation_drawer_content_description:I

    .line 138
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 136
    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    :goto_0
    return-object v0

    .line 139
    :catch_0
    move-exception v0

    .line 140
    const-string v0, "SlidingDrawerLayoutHelper"

    const-string v1, "Incorrect support lib version for Quantum builds. Couldn\'t find correct ActionBarDrawerToggle constructor."

    invoke-static {v0, v1}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    :cond_0
    new-instance v0, Lf;

    iget-object v2, p0, LDz;->a:Landroid/support/v4/widget/DrawerLayout;

    sget v3, Lxb;->ic_drawer_light:I

    sget v4, Lxi;->doclist_open_navigation_drawer_content_description:I

    sget v5, Lxi;->doclist_close_navigation_drawer_content_description:I

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lf;-><init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;III)V

    goto :goto_0
.end method

.method static synthetic a(LDz;Landroid/view/View;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1}, LDz;->a(Landroid/view/View;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/view/View;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 155
    iget-object v0, p0, LDz;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 159
    iget-object v0, p0, LDz;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 160
    iget-object v0, p0, LDz;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->c()Ljava/lang/String;

    move-result-object v0

    .line 161
    if-nez v0, :cond_0

    .line 162
    const-string v0, ""

    .line 165
    :cond_0
    iget-object v1, p0, LDz;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->b()Ljava/lang/String;

    move-result-object v1

    .line 166
    if-nez v1, :cond_1

    .line 167
    const-string v1, ""

    .line 179
    :cond_1
    :goto_0
    iget-object v2, p0, LDz;->a:Lrm;

    sget v3, Lxi;->nav_drawer_title:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, ","

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 180
    invoke-virtual {v2, v3, v4}, Lrm;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 181
    return-object v0

    .line 170
    :cond_2
    iget-object v0, p0, LDz;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    .line 171
    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/view/View;

    move-result-object v0

    sget v1, Lxc;->account_spinner:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 172
    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedView()Landroid/view/View;

    move-result-object v2

    .line 173
    sget v0, Lxc;->account_display_name:I

    .line 174
    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 175
    sget v0, Lxc;->account_email:I

    .line 176
    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 183
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected view : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 208
    invoke-virtual {p0}, LDz;->a()Z

    move-result v0

    .line 209
    const-string v1, "SlidingDrawerLayoutHelper"

    const-string v2, "in toggleNavigationDrawer %b"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 211
    if-nez v0, :cond_0

    .line 212
    iget-object v1, p0, LDz;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->t()V

    .line 215
    :cond_0
    iget-object v1, p0, LDz;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/view/View;

    move-result-object v1

    .line 216
    if-eqz v0, :cond_1

    .line 217
    iget-object v0, p0, LDz;->a:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->d(Landroid/view/View;)V

    .line 222
    :goto_0
    return-void

    .line 219
    :cond_1
    iget-object v0, p0, LDz;->a:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->c(Landroid/view/View;)V

    .line 220
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    goto :goto_0
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, LDz;->a:Lf;

    invoke-virtual {v0, p1}, Lf;->a(Landroid/content/res/Configuration;)V

    .line 194
    return-void
.end method

.method public a(Landroid/widget/Button;LaFO;)V
    .locals 0

    .prologue
    .line 278
    return-void
.end method

.method public a(Landroid/widget/Button;[Landroid/accounts/Account;LaqX;)V
    .locals 2

    .prologue
    .line 271
    iget-object v0, p0, LDz;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p2, p3}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(Landroid/widget/Button;[Landroid/accounts/Account;LaqX;)V

    .line 272
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 151
    iget v0, p0, LDz;->a:F

    const v1, 0x3f7d70a4    # 0.99f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 229
    const-string v0, "SlidingDrawerLayoutHelper"

    const-string v1, "in closeDrawers"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    iget-object v0, p0, LDz;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/view/View;

    move-result-object v0

    .line 235
    if-eqz v0, :cond_0

    .line 236
    iget-object v1, p0, LDz;->a:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/DrawerLayout;->d(Landroid/view/View;)V

    .line 238
    :cond_0
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 282
    const-string v0, "SlidingDrawerLayoutHelper"

    const-string v1, "lockNavigationDrawer"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    iget-object v0, p0, LDz;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/view/View;

    move-result-object v0

    .line 285
    iget-object v1, p0, LDz;->a:Landroid/support/v4/widget/DrawerLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(ILandroid/view/View;)V

    .line 286
    return-void
.end method

.method public e()V
    .locals 3

    .prologue
    .line 291
    const-string v0, "SlidingDrawerLayoutHelper"

    const-string v1, "unlockNavigationDrawer"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    iget-object v0, p0, LDz;->a:Lwm;

    invoke-interface {v0}, Lwm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 298
    :goto_0
    return-void

    .line 296
    :cond_0
    iget-object v0, p0, LDz;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/view/View;

    move-result-object v0

    .line 297
    iget-object v1, p0, LDz;->a:Landroid/support/v4/widget/DrawerLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(ILandroid/view/View;)V

    goto :goto_0
.end method

.method public g()V
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, LDz;->a:LabI;

    invoke-virtual {v0}, LabI;->c()V

    .line 251
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, LDz;->a:Lf;

    invoke-virtual {v0}, Lf;->a()V

    .line 246
    return-void
.end method

.method public j_()V
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, LDz;->a:LabI;

    invoke-virtual {v0}, LabI;->a()V

    .line 257
    return-void
.end method

.method public o_()V
    .locals 2

    .prologue
    .line 262
    iget-object v0, p0, LDz;->a:LabI;

    invoke-virtual {v0}, LabI;->d()V

    .line 263
    iget-object v0, p0, LDz;->a:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, LDz;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->c(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    iput v0, p0, LDz;->a:F

    .line 265
    iget-object v0, p0, LDz;->a:Lrm;

    invoke-virtual {v0}, Lrm;->a()LaqY;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LaqY;->b(Z)V

    .line 266
    return-void

    .line 263
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
