.class final LaCt;
.super Ljava/lang/Object;

# interfaces
.implements LaDd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LaDd",
        "<",
        "Lcom/google/android/gms/common/api/Status;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J

.field final synthetic a:LaCs;


# direct methods
.method constructor <init>(LaCs;J)V
    .locals 0

    iput-object p1, p0, LaCt;->a:LaCs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p2, p0, LaCt;->a:J

    return-void
.end method


# virtual methods
.method public synthetic a(LaDc;)V
    .locals 0

    check-cast p1, Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, p1}, LaCt;->a(Lcom/google/android/gms/common/api/Status;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/common/api/Status;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LaCt;->a:LaCs;

    iget-object v0, v0, LaCs;->a:LaCn;

    invoke-static {v0}, LaCn;->a(LaCn;)LaRf;

    move-result-object v0

    iget-wide v2, p0, LaCt;->a:J

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->b()I

    move-result v1

    invoke-virtual {v0, v2, v3, v1}, LaRf;->a(JI)V

    :cond_0
    return-void
.end method
