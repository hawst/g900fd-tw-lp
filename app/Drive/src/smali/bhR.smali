.class public LbhR;
.super Landroid/os/Handler;
.source "ProcessingThread.java"


# instance fields
.field final synthetic a:Lcom/google/bionics/scanner/unveil/nonstop/ProcessingThread;


# direct methods
.method public constructor <init>(Lcom/google/bionics/scanner/unveil/nonstop/ProcessingThread;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, LbhR;->a:Lcom/google/bionics/scanner/unveil/nonstop/ProcessingThread;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 37
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 38
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 44
    :goto_0
    return-void

    .line 42
    :cond_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;

    .line 43
    iget-object v1, p0, LbhR;->a:Lcom/google/bionics/scanner/unveil/nonstop/ProcessingThread;

    invoke-virtual {v1, v0}, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingThread;->a(Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;)V

    goto :goto_0
.end method
