.class public LaId;
.super Ljava/lang/Object;
.source "ProprietaryExtensionHandler.java"


# instance fields
.field private final a:LaGM;

.field private final a:LaIa;

.field private final a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LaGM;LaIa;Laja;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGM;",
            "LaIa;",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    iput-object p1, p0, LaId;->a:LaGM;

    .line 118
    iput-object p2, p0, LaId;->a:LaIa;

    .line 119
    iput-object p3, p0, LaId;->a:Laja;

    .line 120
    return-void
.end method

.method private a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 83
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 85
    iget-object v0, p0, LaId;->a:LaGM;

    invoke-interface {v0, p1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v2

    .line 86
    if-eqz v2, :cond_0

    invoke-interface {v2}, LaGo;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    :cond_0
    const/4 v0, 0x0

    .line 96
    :goto_0
    return-object v0

    .line 89
    :cond_1
    iget-object v0, p0, LaId;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-string v3, "android.permission.GET_ACCOUNTS"

    invoke-virtual {v0, v3}, Landroid/content/Context;->checkCallingPermission(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 91
    const-string v0, "accountName"

    iget-object v3, p1, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-virtual {v3}, LaFO;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    :cond_2
    const-string v0, "resourceId"

    invoke-interface {v2}, LaGo;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const-string v0, "htmlUri"

    invoke-interface {v2}, LaGo;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const-string v0, "_display_name"

    invoke-interface {v2}, LaGo;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 96
    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/drive/database/data/EntrySpec;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 100
    iget-object v0, p0, LaId;->a:LaIa;

    invoke-interface {v0, p1}, LaIa;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/net/Uri;

    move-result-object v1

    .line 102
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 103
    iget-object v0, p0, LaId;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-class v3, Lcom/google/android/apps/docs/app/detailpanel/DetailActivity;

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 104
    invoke-virtual {v2, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 105
    const-string v0, "openEnabled"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 106
    const-string v1, "openEnabled"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 107
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 108
    const-string v1, "android.intent.extra.INTENT"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 109
    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;LaIC;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 123
    instance-of v1, p2, LaIA;

    if-nez v1, :cond_1

    .line 140
    :cond_0
    :goto_0
    return-object v0

    .line 126
    :cond_1
    check-cast p2, LaIA;

    invoke-virtual {p2}, LaIA;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    .line 127
    if-eqz v1, :cond_0

    .line 131
    invoke-static {p1}, LaIf;->a(Ljava/lang/String;)LaIf;

    move-result-object v2

    .line 132
    if-nez v2, :cond_2

    .line 133
    const-string v1, "ProprietaryExtensionHandler"

    const-string v2, "Method is not implemented: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 137
    :cond_2
    sget-object v3, LaIe;->a:[I

    invoke-virtual {v2}, LaIf;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 138
    :pswitch_0
    invoke-direct {p0, v1}, LaId;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0

    .line 139
    :pswitch_1
    invoke-direct {p0, v1, p3}, LaId;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0

    .line 137
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
