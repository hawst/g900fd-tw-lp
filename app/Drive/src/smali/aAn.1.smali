.class public LaAn;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "LaAm;",
        ">;"
    }
.end annotation


# instance fields
.field protected a:I

.field final synthetic a:Lcom/google/android/gms/appdatasearch/SearchResults;

.field private final a:[Ljava/util/Map;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/appdatasearch/SearchResults;)V
    .locals 1

    iput-object p1, p0, LaAn;->a:Lcom/google/android/gms/appdatasearch/SearchResults;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/SearchResults;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LaAn;->a:[Ljava/util/Map;

    return-void

    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/SearchResults;->a:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [Ljava/util/Map;

    goto :goto_0
.end method


# virtual methods
.method public a()LaAm;
    .locals 3

    invoke-virtual {p0}, LaAn;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "No more results."

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, LaAm;

    iget-object v1, p0, LaAn;->a:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget v2, p0, LaAn;->a:I

    invoke-direct {v0, v1, v2, p0}, LaAm;-><init>(Lcom/google/android/gms/appdatasearch/SearchResults;ILaAn;)V

    invoke-virtual {p0}, LaAn;->a()V

    return-object v0
.end method

.method protected a()V
    .locals 1

    iget v0, p0, LaAn;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LaAn;->a:I

    return-void
.end method

.method public hasNext()Z
    .locals 2

    iget-object v0, p0, LaAn;->a:Lcom/google/android/gms/appdatasearch/SearchResults;

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/SearchResults;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, LaAn;->a:I

    iget-object v1, p0, LaAn;->a:Lcom/google/android/gms/appdatasearch/SearchResults;

    invoke-virtual {v1}, Lcom/google/android/gms/appdatasearch/SearchResults;->a()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, LaAn;->a()LaAm;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "remove not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
