.class LLc;
.super Ljava/lang/Object;
.source "SelectionCardAnimator.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:LKZ;

.field final synthetic a:Landroid/view/View;

.field final synthetic b:Landroid/view/View;


# direct methods
.method constructor <init>(LKZ;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, LLc;->a:LKZ;

    iput-object p2, p0, LLc;->a:Landroid/view/View;

    iput-object p3, p0, LLc;->b:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 118
    iget-object v0, p0, LLc;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 122
    iget-object v0, p0, LLc;->a:Landroid/view/View;

    sget v1, Lxc;->main_body:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 123
    if-nez v0, :cond_2

    .line 125
    iget-object v0, p0, LLc;->a:Landroid/view/View;

    sget v1, Lxc;->doc_entry_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 126
    if-nez v0, :cond_1

    .line 128
    iget-object v1, p0, LLc;->a:Landroid/view/View;

    .line 129
    sget v0, Lxc;->title_container:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 130
    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    :goto_0
    move v6, v0

    move-object v0, v1

    .line 142
    :goto_1
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 143
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 145
    iget-object v3, p0, LLc;->a:LKZ;

    invoke-static {v3}, LKZ;->a(LKZ;)I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    add-int v5, v1, v3

    .line 146
    iget-object v1, p0, LLc;->a:LKZ;

    invoke-static {v1}, LKZ;->a(LKZ;)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int v4, v2, v1

    .line 147
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v5, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 148
    iget-object v2, p0, LLc;->b:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 149
    iget-object v1, p0, LLc;->b:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 151
    iget-object v1, p0, LLc;->a:LKZ;

    invoke-static {v1}, LKZ;->a(LKZ;)LKp;

    move-result-object v1

    invoke-virtual {v1}, LKp;->a()Landroid/graphics/Point;

    move-result-object v1

    .line 152
    const/4 v2, 0x2

    new-array v3, v2, [I

    .line 153
    invoke-virtual {v0, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 155
    aget v0, v3, v8

    iget v2, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v2

    iget-object v2, p0, LLc;->a:LKZ;

    invoke-static {v2}, LKZ;->a(LKZ;)I

    move-result v2

    sub-int v2, v0, v2

    .line 156
    const/4 v0, 0x1

    aget v0, v3, v0

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    iget-object v1, p0, LLc;->a:LKZ;

    invoke-static {v1}, LKZ;->a(LKZ;)I

    move-result v1

    sub-int v3, v0, v1

    .line 157
    iget-object v0, p0, LLc;->b:Landroid/view/View;

    int-to-float v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setX(F)V

    .line 158
    iget-object v0, p0, LLc;->b:Landroid/view/View;

    int-to-float v1, v3

    invoke-virtual {v0, v1}, Landroid/view/View;->setY(F)V

    .line 160
    iget-object v0, p0, LLc;->a:LKZ;

    iget-object v1, p0, LLc;->a:Landroid/view/View;

    iget-object v7, p0, LLc;->b:Landroid/view/View;

    invoke-static {v0, v1, v7, v6}, LKZ;->a(LKZ;Landroid/view/View;Landroid/view/View;I)V

    .line 161
    iget-object v0, p0, LLc;->a:LKZ;

    iget-object v1, p0, LLc;->b:Landroid/view/View;

    invoke-static/range {v0 .. v6}, LKZ;->a(LKZ;Landroid/view/View;IIIII)V

    .line 163
    return v8

    .line 133
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v0

    goto :goto_0

    .line 136
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v6

    goto :goto_1

    .line 139
    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v6

    goto/16 :goto_1
.end method
