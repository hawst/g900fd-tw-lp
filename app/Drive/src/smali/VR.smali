.class public final enum LVR;
.super Ljava/lang/Enum;
.source "PsynchoWhitelistAccessor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LVR;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LVR;

.field private static final synthetic a:[LVR;

.field public static final enum b:LVR;

.field public static final enum c:LVR;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20
    new-instance v0, LVR;

    const-string v1, "PHOTO_BACKUP_ANNOUNCEMENT"

    invoke-direct {v0, v1, v2}, LVR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LVR;->a:LVR;

    .line 25
    new-instance v0, LVR;

    const-string v1, "PHOTO_BACKUP_SETTING"

    invoke-direct {v0, v1, v3}, LVR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LVR;->b:LVR;

    .line 29
    new-instance v0, LVR;

    const-string v1, "PSYNCHO_UI"

    invoke-direct {v0, v1, v4}, LVR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LVR;->c:LVR;

    .line 16
    const/4 v0, 0x3

    new-array v0, v0, [LVR;

    sget-object v1, LVR;->a:LVR;

    aput-object v1, v0, v2

    sget-object v1, LVR;->b:LVR;

    aput-object v1, v0, v3

    sget-object v1, LVR;->c:LVR;

    aput-object v1, v0, v4

    sput-object v0, LVR;->a:[LVR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LVR;
    .locals 1

    .prologue
    .line 16
    const-class v0, LVR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LVR;

    return-object v0
.end method

.method public static values()[LVR;
    .locals 1

    .prologue
    .line 16
    sget-object v0, LVR;->a:[LVR;

    invoke-virtual {v0}, [LVR;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LVR;

    return-object v0
.end method
