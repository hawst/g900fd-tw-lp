.class LzX;
.super Ljava/lang/Object;
.source "CompoundAdapterToMainAdapterVisibleEntryViewLookup.java"

# interfaces
.implements LHp;


# instance fields
.field private final a:LHp;

.field private final a:LzZ;


# direct methods
.method constructor <init>(LHp;LzZ;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, LzX;->a:LHp;

    .line 20
    iput-object p2, p0, LzX;->a:LzZ;

    .line 21
    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, LzX;->a:LzZ;

    invoke-interface {v0}, LzZ;->a()I

    move-result v0

    .line 26
    iget-object v1, p0, LzX;->a:LHp;

    add-int/2addr v0, p1

    invoke-interface {v1, v0}, LHp;->a(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 31
    const-class v0, LzX;

    invoke-static {v0}, LbiL;->a(Ljava/lang/Class;)LbiN;

    move-result-object v0

    const-string v1, "parent"

    iget-object v2, p0, LzX;->a:LHp;

    .line 32
    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;Ljava/lang/Object;)LbiN;

    move-result-object v0

    const-string v1, "headerCount"

    iget-object v2, p0, LzX;->a:LzZ;

    .line 33
    invoke-interface {v2}, LzZ;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;I)LbiN;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, LbiN;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
