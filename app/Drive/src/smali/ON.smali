.class LON;
.super LOO;
.source "PhotosPreferencesInstaller.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LOO",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LOJ;


# direct methods
.method constructor <init>(LOJ;Landroid/preference/CheckBoxPreference;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, LON;->a:LOJ;

    .line 102
    invoke-direct {p0, p1, p2, p3}, LOO;-><init>(LOJ;Landroid/preference/CheckBoxPreference;Ljava/lang/String;)V

    .line 103
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 108
    :try_start_0
    iget-object v0, p0, LON;->a:LOJ;

    iget-object v0, v0, LOJ;->a:LVN;

    iget-object v1, p0, LON;->a:LOJ;

    invoke-static {v1}, LOJ;->a(LOJ;)LaFO;

    move-result-object v1

    invoke-interface {v0, v1}, LVN;->a(LaFO;)LVO;

    move-result-object v0

    .line 109
    sget-object v1, LVO;->b:LVO;

    invoke-virtual {v1, v0}, LVO;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 111
    :goto_0
    return-object v0

    .line 110
    :catch_0
    move-exception v0

    .line 111
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 117
    if-eqz p1, :cond_0

    .line 118
    invoke-super {p0, p1}, LOO;->a(Ljava/lang/Boolean;)V

    .line 122
    :goto_0
    return-void

    .line 120
    :cond_0
    const-string v0, "PhotosPreferenceSetter"

    const-string v1, "Error fetching value of auto-add setting from server"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 100
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, LON;->a([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 100
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, LON;->a(Ljava/lang/Boolean;)V

    return-void
.end method
