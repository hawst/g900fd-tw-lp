.class public LMF;
.super Ljava/lang/Object;
.source "SwipableDocListController.java"

# interfaces
.implements LMJ;
.implements LMK;
.implements Lwn;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field a:LAP;

.field private a:LMv;

.field a:Lapn;

.field private a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;"
        }
    .end annotation
.end field

.field a:Lwm;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {}, LbmF;->c()LbmF;

    move-result-object v0

    iput-object v0, p0, LMF;->b:Ljava/util/List;

    return-void
.end method

.method static synthetic a(LMF;)LMv;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, LMF;->a:LMv;

    return-object v0
.end method

.method static synthetic a(LMF;)Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, LMF;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    return-object v0
.end method

.method private a(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;"
        }
    .end annotation

    .prologue
    .line 142
    invoke-static {p1}, LbnG;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 143
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 144
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    .line 145
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146
    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 151
    :cond_1
    invoke-static {v1}, LbnG;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/List;I)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 193
    :goto_0
    iget-object v0, p0, LMF;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p2, :cond_0

    .line 194
    const-string v0, "DocListViewPagerAdapter"

    const-string v1, "Removing path at %d"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 195
    iget-object v0, p0, LMF;->a:Ljava/util/List;

    iget-object v1, p0, LMF;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    move v0, p2

    .line 197
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 198
    const-string v1, "DocListViewPagerAdapter"

    const-string v2, "Adding path at %d"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 199
    iget-object v1, p0, LMF;->a:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 201
    :cond_1
    const-string v0, "DocListViewPagerAdapter"

    const-string v1, "Paths size %d"

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, LMF;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 202
    return-void
.end method

.method private b(I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 127
    iget-object v0, p0, LMF;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const-string v1, "Tried to update path to index out of range. %s of %s."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 128
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    iget-object v4, p0, LMF;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 127
    invoke-static {p1, v0, v1}, LbiT;->a(IILjava/lang/String;)I

    .line 129
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 130
    if-lez p1, :cond_0

    .line 131
    iget-object v0, p0, LMF;->a:Ljava/util/List;

    add-int/lit8 v2, p1, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    :cond_0
    iget-object v0, p0, LMF;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge p1, v0, :cond_1

    .line 134
    iget-object v0, p0, LMF;->a:Ljava/util/List;

    add-int/lit8 v2, p1, 0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    :cond_1
    iget-object v2, p0, LMF;->a:Lapn;

    iget-object v0, p0, LMF;->a:Ljava/util/List;

    .line 137
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-interface {v1, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    .line 136
    invoke-virtual {v2, v5, v0, v1}, Lapn;->a(ZLcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;[Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;)V

    .line 138
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, LMF;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, LMF;->a:Lwm;

    invoke-interface {v0, p0}, Lwm;->a(Lwn;)V

    .line 211
    invoke-virtual {p0}, LMF;->c()V

    .line 212
    return-void
.end method

.method public a(I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 98
    iget-object v0, p0, LMF;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 99
    const-string v0, "DocListViewPagerAdapter"

    const-string v1, "Tried to update path to index out of range. %s of %s."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget-object v3, p0, LMF;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 124
    :goto_0
    return-void

    .line 102
    :cond_0
    const-string v0, "DocListViewPagerAdapter"

    const-string v1, "Changed to %d"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 104
    invoke-direct {p0, p1}, LMF;->b(I)V

    .line 106
    iget-object v0, p0, LMF;->a:Lapn;

    new-instance v1, LMH;

    invoke-direct {v1, p0}, LMH;-><init>(LMF;)V

    invoke-virtual {v0, v1}, Lapn;->a(Lapu;)V

    .line 115
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v0

    iget-object v1, p0, LMF;->b:Ljava/util/List;

    .line 116
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Iterable;)LbmH;

    move-result-object v0

    iget-object v1, p0, LMF;->a:Ljava/util/List;

    add-int/lit8 v2, p1, 0x1

    .line 117
    invoke-interface {v1, v4, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Iterable;)LbmH;

    move-result-object v0

    invoke-virtual {v0}, LbmH;->a()LbmF;

    move-result-object v0

    .line 119
    invoke-virtual {v0}, LbmF;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, LMF;->a:Lwm;

    invoke-interface {v1}, Lwm;->a()LbmF;

    move-result-object v1

    invoke-virtual {v1, v0}, LbmF;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 120
    const-string v1, "DocListViewPagerAdapter"

    const-string v2, "Path different after change - updating"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 121
    iget-object v1, p0, LMF;->a:LAP;

    invoke-interface {v1, v0}, LAP;->a(LbmF;)V

    .line 123
    :cond_1
    iget-object v0, p0, LMF;->a:LMv;

    invoke-virtual {v0}, LMv;->b()V

    goto :goto_0
.end method

.method public a(II)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 77
    iget-object v0, p0, LMF;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p2, v0, :cond_0

    .line 78
    const-string v0, "DocListViewPagerAdapter"

    const-string v1, "Tried to update path to index out of range. %s of %s."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget-object v3, p0, LMF;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 90
    :goto_0
    return-void

    .line 81
    :cond_0
    const-string v0, "DocListViewPagerAdapter"

    const-string v1, "Changing from %d to %d"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 82
    iget-object v0, p0, LMF;->a:LMv;

    invoke-virtual {v0}, LMv;->a()V

    .line 83
    invoke-direct {p0, p2}, LMF;->b(I)V

    .line 84
    iget-object v0, p0, LMF;->a:Lapn;

    new-instance v1, LMG;

    invoke-direct {v1, p0}, LMG;-><init>(LMF;)V

    invoke-virtual {v0, v1}, Lapn;->a(Lapu;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;Lcom/google/android/apps/docs/view/DocListView;)V
    .locals 3

    .prologue
    .line 56
    sget v0, Lxc;->doc_list_snapshot:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView;

    .line 57
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LMF;->a:Ljava/util/List;

    .line 58
    new-instance v1, LMv;

    invoke-direct {v1, p2, v0}, LMv;-><init>(Lcom/google/android/apps/docs/view/DocListView;Landroid/view/TextureView;)V

    iput-object v1, p0, LMF;->a:LMv;

    .line 59
    iput-object p1, p0, LMF;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    .line 60
    iget-object v1, p0, LMF;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-virtual {v1, p2, v0, p0, p0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(Lcom/google/android/apps/docs/view/DocListView;Landroid/view/View;LMK;LMJ;)V

    .line 61
    const-string v0, "DocListViewPagerAdapter"

    const-string v1, "Init called"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 62
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, LMF;->a:Lwm;

    invoke-interface {v0, p0}, Lwm;->b(Lwn;)V

    .line 216
    return-void
.end method

.method public c()V
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 156
    iget-object v0, p0, LMF;->a:Lwm;

    invoke-interface {v0}, Lwm;->a()LbmF;

    move-result-object v0

    .line 157
    invoke-direct {p0, v0}, LMF;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    .line 159
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v4

    sub-int/2addr v1, v4

    .line 158
    invoke-interface {v0, v3, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LMF;->b:Ljava/util/List;

    .line 161
    const-string v0, "DocListViewPagerAdapter"

    const-string v1, "Path changed %s"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-static {v0, v1, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 163
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    .line 164
    add-int/lit8 v8, v7, -0x1

    .line 165
    iget-object v0, p0, LMF;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v9

    .line 166
    invoke-static {v7, v9}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 168
    if-lez v9, :cond_4

    iget-object v0, p0, LMF;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    move v1, v2

    :goto_0
    move v5, v3

    .line 170
    :goto_1
    if-ge v5, v10, :cond_1

    .line 172
    if-ge v5, v7, :cond_5

    if-ge v5, v9, :cond_5

    .line 173
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    iget-object v4, p0, LMF;->a:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    move v4, v2

    .line 175
    :goto_2
    if-ge v5, v7, :cond_6

    if-lt v5, v9, :cond_6

    move v0, v2

    .line 177
    :goto_3
    if-nez v4, :cond_0

    if-eqz v0, :cond_7

    .line 178
    :cond_0
    invoke-direct {p0, v6, v5}, LMF;->a(Ljava/util/List;I)V

    .line 183
    :cond_1
    iget-object v0, p0, LMF;->a:Ljava/util/List;

    invoke-static {v0}, Lwr;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 186
    :goto_4
    if-nez v1, :cond_2

    iget-object v0, p0, LMF;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a()I

    move-result v0

    if-ne v0, v8, :cond_2

    if-eqz v2, :cond_3

    .line 187
    :cond_2
    iget-object v0, p0, LMF;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a()I

    move-result v0

    invoke-virtual {p0, v0, v8}, LMF;->a(II)V

    .line 188
    iget-object v0, p0, LMF;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-virtual {v0, v8}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->setCurrentPage(I)V

    .line 190
    :cond_3
    return-void

    :cond_4
    move v1, v3

    .line 168
    goto :goto_0

    :cond_5
    move v4, v3

    .line 173
    goto :goto_2

    :cond_6
    move v0, v3

    .line 175
    goto :goto_3

    .line 170
    :cond_7
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_1

    :cond_8
    move v2, v3

    .line 183
    goto :goto_4
.end method

.method public d()V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, LMF;->a:Lapn;

    invoke-virtual {v0}, Lapn;->d()V

    .line 207
    return-void
.end method
