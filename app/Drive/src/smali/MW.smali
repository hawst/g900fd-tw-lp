.class public LMW;
.super LMO;
.source "DocsUploaderImpl.java"

# interfaces
.implements LNi;


# instance fields
.field final synthetic a:LMV;


# direct methods
.method constructor <init>(LMV;LMZ;LagH;LahK;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, LMW;->a:LMV;

    .line 55
    invoke-direct {p0, p1, p2, p3}, LMO;-><init>(LMN;LMZ;LagH;)V

    .line 56
    return-void
.end method

.method private a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 181
    const-string v0, "new-revision"

    .line 182
    const-string v0, "true"

    .line 184
    invoke-direct {p0, p1}, LMW;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Landroid/net/Uri;

    move-result-object v0

    .line 185
    if-nez v0, :cond_0

    .line 186
    const/4 v0, 0x0

    .line 190
    :goto_0
    return-object v0

    .line 189
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "new-revision"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 190
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 215
    const-string v0, "Failed to get upload uri"

    .line 216
    new-instance v1, LNh;

    invoke-direct {v1, v0, p1}, LNh;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 198
    const-string v0, "http://schemas.google.com/g/2005#resumable-edit-media"

    .line 200
    const/4 v0, 0x0

    .line 202
    :try_start_0
    iget-object v1, p0, LMW;->a:LMV;

    invoke-static {v1}, LMV;->a(LMV;)Laer;

    move-result-object v1

    invoke-interface {v1, p1}, Laer;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaeF;
    :try_end_0
    .catch Lbxk; {:try_start_0 .. :try_end_0} :catch_0
    .catch LbwO; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 211
    :goto_0
    const-string v1, "http://schemas.google.com/g/2005#resumable-edit-media"

    invoke-interface {v0, v1}, LaJT;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 203
    :catch_0
    move-exception v1

    .line 204
    invoke-direct {p0, v1}, LMW;->a(Ljava/lang/Exception;)V

    goto :goto_0

    .line 205
    :catch_1
    move-exception v1

    .line 206
    invoke-direct {p0, v1}, LMW;->a(Ljava/lang/Exception;)V

    goto :goto_0

    .line 207
    :catch_2
    move-exception v1

    .line 208
    invoke-direct {p0, v1}, LMW;->a(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private c()V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 136
    const-wide/16 v2, 0x0

    iput-wide v2, p0, LMW;->a:J

    .line 139
    iget-object v1, p0, LMW;->a:LMZ;

    invoke-virtual {v1}, LMZ;->a()Z

    move-result v1

    .line 140
    if-eqz v1, :cond_0

    iget-object v2, p0, LMW;->a:LMZ;

    invoke-virtual {v2}, LMZ;->b()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "image/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v0

    .line 141
    :goto_0
    iget-object v3, p0, LMW;->a:LMZ;

    invoke-virtual {v3}, LMZ;->a()LaFO;

    move-result-object v3

    .line 143
    iget-object v5, p0, LMW;->a:LMZ;

    invoke-virtual {v5}, LMZ;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v5

    .line 144
    invoke-static {v5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    iget-object v6, p0, LMW;->a:LMV;

    invoke-static {v6}, LMV;->a(LMV;)LaGM;

    move-result-object v6

    invoke-interface {v6, v5}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v6

    .line 147
    if-eqz v6, :cond_1

    move v5, v0

    .line 149
    :goto_1
    if-eqz v5, :cond_3

    .line 150
    if-nez v1, :cond_2

    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Conversion should not have been set to upload a new revision for: "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, LbiT;->b(ZLjava/lang/Object;)V

    .line 152
    invoke-direct {p0, v6}, LMW;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Landroid/net/Uri;

    move-result-object v4

    .line 153
    if-nez v4, :cond_4

    .line 154
    new-instance v0, LNh;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Upload uri not found for : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LNh;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v2, v4

    .line 140
    goto :goto_0

    :cond_1
    move v5, v4

    .line 147
    goto :goto_1

    :cond_2
    move v0, v4

    .line 150
    goto :goto_2

    .line 157
    :cond_3
    invoke-virtual {p0}, LMW;->a()Landroid/net/Uri;

    move-result-object v4

    :cond_4
    move-object v0, p0

    .line 160
    invoke-virtual/range {v0 .. v5}, LMW;->a(ZZLaFO;Landroid/net/Uri;Z)V

    .line 161
    return-void
.end method

.method private d()LaJT;
    .locals 2

    .prologue
    .line 221
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LMW;->a:J

    .line 222
    invoke-virtual {p0}, LMW;->b()LaJT;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a(LMZ;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 166
    invoke-virtual {p1, p2}, LMZ;->a(Ljava/lang/String;)V

    .line 167
    invoke-virtual {p1}, LMZ;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 169
    iget-object v1, p0, LMW;->a:LMV;

    invoke-static {v1}, LMV;->a(LMV;)LahK;

    move-result-object v1

    invoke-interface {v1, v0}, LahK;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LahF;

    move-result-object v0

    .line 170
    if-eqz v0, :cond_0

    .line 171
    invoke-interface {v0, p2}, LahF;->a(Ljava/lang/String;)V

    .line 173
    :cond_0
    return-void
.end method

.method protected b()V
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, LMW;->a:LMZ;

    invoke-virtual {v0}, LMZ;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 228
    iget-object v1, p0, LMW;->a:LMV;

    invoke-static {v1}, LMV;->a(LMV;)LahK;

    move-result-object v1

    invoke-interface {v1, v0}, LahK;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LahF;

    move-result-object v0

    .line 229
    if-eqz v0, :cond_0

    .line 230
    invoke-interface {v0}, LahF;->l()V

    .line 232
    :cond_0
    return-void
.end method

.method public c()LaJT;
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 61
    iget-object v1, p0, LMW;->a:LMV;

    invoke-static {v1}, LMV;->a(LMV;)LqK;

    move-result-object v1

    const-string v3, "upload"

    const-string v4, "uploadStarted"

    invoke-virtual {v1, v3, v4}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const/4 v1, 0x0

    .line 67
    :try_start_0
    const-string v3, "DocsUploaderImpl"

    const-string v4, "Uploading %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, LMW;->a:LMZ;

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 69
    invoke-static {}, LamS;->a()V
    :try_end_0
    .catch LQp; {:try_start_0 .. :try_end_0} :catch_1
    .catch LNh; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    :try_start_1
    new-instance v3, Ljava/io/RandomAccessFile;

    iget-object v4, p0, LMW;->a:LMZ;

    invoke-virtual {v4}, LMZ;->a()Ljava/io/File;

    move-result-object v4

    const-string v5, "r"

    invoke-direct {v3, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v3, p0, LMW;->a:Ljava/io/RandomAccessFile;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch LQp; {:try_start_1 .. :try_end_1} :catch_1
    .catch LNh; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    :try_start_2
    iget-object v3, p0, LMW;->a:LagH;

    invoke-interface {v3}, LagH;->a()V

    .line 84
    iget-object v3, p0, LMW;->a:LMZ;

    invoke-virtual {v3}, LMZ;->c()Ljava/lang/String;
    :try_end_2
    .catch LQp; {:try_start_2 .. :try_end_2} :catch_1
    .catch LNh; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v3

    if-eqz v3, :cond_2

    .line 86
    :try_start_3
    invoke-direct {p0}, LMW;->d()LaJT;

    move-result-object v1

    .line 87
    if-eqz v1, :cond_1

    move v3, v0

    .line 88
    :goto_0
    const-string v4, "DocsUploaderImpl"

    const-string v5, "Upload resuming - complete=%s bytesSent=%s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v6, v7

    const/4 v3, 0x1

    iget-wide v8, p0, LMW;->a:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v4, v5, v6}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_3
    .catch LNh; {:try_start_3 .. :try_end_3} :catch_2
    .catch LQp; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 97
    :goto_1
    if-nez v0, :cond_0

    .line 98
    :try_start_4
    const-string v0, "DocsUploaderImpl"

    const-string v2, "Beginning resumable upload"

    invoke-static {v0, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    invoke-direct {p0}, LMW;->c()V

    .line 102
    :cond_0
    invoke-virtual {p0, v1}, LMW;->a(LaJT;)LaJT;

    move-result-object v0

    .line 104
    invoke-static {}, LamS;->a()V

    .line 106
    iget-object v1, p0, LMW;->a:LagH;

    invoke-interface {v1}, LagH;->b()V

    .line 108
    iget-object v1, p0, LMW;->a:LMV;

    invoke-static {v1}, LMV;->a(LMV;)LqK;

    move-result-object v1

    const-string v2, "upload"

    const-string v3, "uploadSucceeded"

    invoke-virtual {v1, v2, v3}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch LQp; {:try_start_4 .. :try_end_4} :catch_1
    .catch LNh; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 120
    iget-object v1, p0, LMW;->a:LMZ;

    invoke-virtual {v1}, LMZ;->close()V

    .line 123
    return-object v0

    .line 73
    :catch_0
    move-exception v0

    .line 74
    :try_start_5
    new-instance v1, LNh;

    const-string v2, "Unable to upload file: "

    invoke-direct {v1, v2, v0}, LNh;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_5
    .catch LQp; {:try_start_5 .. :try_end_5} :catch_1
    .catch LNh; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 110
    :catch_1
    move-exception v0

    .line 111
    :try_start_6
    new-instance v0, Ljava/lang/InterruptedException;

    const-string v1, "Upload interrupted."

    invoke-direct {v0, v1}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 120
    :catchall_0
    move-exception v0

    iget-object v1, p0, LMW;->a:LMZ;

    invoke-virtual {v1}, LMZ;->close()V

    throw v0

    :cond_1
    move v3, v2

    .line 87
    goto :goto_0

    .line 91
    :catch_2
    move-exception v0

    .line 93
    :try_start_7
    const-string v3, "DocsUploaderImpl"

    const-string v4, "Upload resumption failed, will begin again: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v3, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_7
    .catch LQp; {:try_start_7 .. :try_end_7} :catch_1
    .catch LNh; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 112
    :catch_3
    move-exception v0

    .line 113
    :try_start_8
    iget-object v1, p0, LMW;->a:LagH;

    sget-object v2, LafT;->i:LafT;

    invoke-interface {v1, v2}, LagH;->a(LafT;)V

    .line 114
    throw v0

    .line 115
    :catch_4
    move-exception v0

    .line 116
    iget-object v1, p0, LMW;->a:LagH;

    sget-object v2, LafT;->o:LafT;

    invoke-interface {v1, v2}, LagH;->a(LafT;)V

    .line 117
    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0
.end method
