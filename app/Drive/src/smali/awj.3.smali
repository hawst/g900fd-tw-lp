.class public final Lawj;
.super Ljava/lang/Object;
.source "ParcelUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/graphics/Bitmap;

.field final synthetic a:Landroid/os/ParcelFileDescriptor;


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;Landroid/os/ParcelFileDescriptor;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lawj;->a:Landroid/graphics/Bitmap;

    iput-object p2, p0, Lawj;->a:Landroid/os/ParcelFileDescriptor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 30
    invoke-static {}, Lawt;->a()Lawt;

    move-result-object v0

    .line 31
    iget-object v1, p0, Lawj;->a:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lawj;->a:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->detachFd()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/apps/viewer/util/ParcelUtils;->a(Landroid/graphics/Bitmap;I)Z

    .line 32
    const-string v1, "ParcelUtils"

    const-string v2, "Receive bitmap native: %d ms."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lawt;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    return-void
.end method
