.class public LJF;
.super LJG;
.source "ScanHelpCard.java"


# instance fields
.field private final a:Lub;


# direct methods
.method public constructor <init>(LJc;Lub;)V
    .locals 7
    .param p2    # Lub;
        .annotation runtime Lbwm;
            value = "scanIntentProvider"
        .end annotation
    .end param

    .prologue
    .line 23
    const-string v1, "ScanHelpCard"

    sget v2, LpP;->scan_help_card:I

    sget v3, LpR;->help_card_scan_positive_button_label:I

    const/4 v4, 0x0

    const/4 v5, 0x1

    sget-object v6, LJb;->b:LJb;

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, LJc;->a(Ljava/lang/String;IIIZLJb;)LIU;

    move-result-object v0

    invoke-direct {p0, v0}, LJG;-><init>(LIU;)V

    .line 31
    iput-object p2, p0, LJF;->a:Lub;

    .line 32
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 3

    .prologue
    .line 41
    invoke-virtual {p0}, LJF;->a()Landroid/content/Context;

    move-result-object v0

    .line 42
    iget-object v1, p0, LJF;->a:Lub;

    invoke-virtual {p0}, LJF;->a()LaFO;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lub;->a(Landroid/content/Context;LaFO;)Landroid/content/Intent;

    move-result-object v1

    .line 43
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 44
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 45
    return-void
.end method

.method public a(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 36
    invoke-super {p0, p1}, LJG;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, LwN;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
