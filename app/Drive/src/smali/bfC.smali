.class public final enum LbfC;
.super Ljava/lang/Enum;
.source "GenericData.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LbfC;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LbfC;

.field private static final synthetic a:[LbfC;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 71
    new-instance v0, LbfC;

    const-string v1, "IGNORE_CASE"

    invoke-direct {v0, v1, v2}, LbfC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LbfC;->a:LbfC;

    .line 68
    const/4 v0, 0x1

    new-array v0, v0, [LbfC;

    sget-object v1, LbfC;->a:LbfC;

    aput-object v1, v0, v2

    sput-object v0, LbfC;->a:[LbfC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LbfC;
    .locals 1

    .prologue
    .line 68
    const-class v0, LbfC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LbfC;

    return-object v0
.end method

.method public static values()[LbfC;
    .locals 1

    .prologue
    .line 68
    sget-object v0, LbfC;->a:[LbfC;

    invoke-virtual {v0}, [LbfC;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LbfC;

    return-object v0
.end method
