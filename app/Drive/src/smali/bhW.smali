.class public LbhW;
.super Ljava/lang/Object;
.source "BitmapPicture.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic a:Lcom/google/bionics/scanner/unveil/util/BitmapPicture;

.field final synthetic b:I


# direct methods
.method public constructor <init>(Lcom/google/bionics/scanner/unveil/util/BitmapPicture;II)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, LbhW;->a:Lcom/google/bionics/scanner/unveil/util/BitmapPicture;

    iput p2, p0, LbhW;->a:I

    iput p3, p0, LbhW;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 53
    iget-object v1, p0, LbhW;->a:Lcom/google/bionics/scanner/unveil/util/BitmapPicture;

    monitor-enter v1

    .line 55
    :try_start_0
    iget v0, p0, LbhW;->a:I

    iget v2, p0, LbhW;->b:I

    mul-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [B

    .line 56
    iget-object v2, p0, LbhW;->a:Lcom/google/bionics/scanner/unveil/util/BitmapPicture;

    invoke-static {v2}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a(Lcom/google/bionics/scanner/unveil/util/BitmapPicture;)[B

    move-result-object v2

    if-eqz v2, :cond_0

    .line 57
    iget-object v2, p0, LbhW;->a:Lcom/google/bionics/scanner/unveil/util/BitmapPicture;

    invoke-static {v2}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a(Lcom/google/bionics/scanner/unveil/util/BitmapPicture;)[B

    move-result-object v2

    iget v3, p0, LbhW;->a:I

    iget v4, p0, LbhW;->b:I

    invoke-static {v2, v0, v3, v4}, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->convertYUV420SPToRGB565([B[BII)V

    .line 58
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 59
    iget-object v2, p0, LbhW;->a:Lcom/google/bionics/scanner/unveil/util/BitmapPicture;

    iget v3, p0, LbhW;->a:I

    iget v4, p0, LbhW;->b:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a(Lcom/google/bionics/scanner/unveil/util/BitmapPicture;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 60
    iget-object v2, p0, LbhW;->a:Lcom/google/bionics/scanner/unveil/util/BitmapPicture;

    invoke-static {v2}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a(Lcom/google/bionics/scanner/unveil/util/BitmapPicture;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 66
    :cond_0
    :try_start_1
    iget-object v0, p0, LbhW;->a:Lcom/google/bionics/scanner/unveil/util/BitmapPicture;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a(Lcom/google/bionics/scanner/unveil/util/BitmapPicture;Z)Z

    .line 67
    iget-object v0, p0, LbhW;->a:Lcom/google/bionics/scanner/unveil/util/BitmapPicture;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 69
    :goto_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 70
    return-void

    .line 63
    :catch_0
    move-exception v0

    .line 64
    :try_start_2
    invoke-static {}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v2, "Out of memory when creating bitmap picture."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 66
    :try_start_3
    iget-object v0, p0, LbhW;->a:Lcom/google/bionics/scanner/unveil/util/BitmapPicture;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a(Lcom/google/bionics/scanner/unveil/util/BitmapPicture;Z)Z

    .line 67
    iget-object v0, p0, LbhW;->a:Lcom/google/bionics/scanner/unveil/util/BitmapPicture;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    goto :goto_0

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 66
    :catchall_1
    move-exception v0

    :try_start_4
    iget-object v2, p0, LbhW;->a:Lcom/google/bionics/scanner/unveil/util/BitmapPicture;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a(Lcom/google/bionics/scanner/unveil/util/BitmapPicture;Z)Z

    .line 67
    iget-object v2, p0, LbhW;->a:Lcom/google/bionics/scanner/unveil/util/BitmapPicture;

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method
