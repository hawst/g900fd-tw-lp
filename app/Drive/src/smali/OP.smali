.class LOP;
.super LOO;
.source "PhotosPreferencesInstaller.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LOO",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LOJ;

.field private a:Ljava/lang/Exception;


# direct methods
.method constructor <init>(LOJ;Landroid/preference/CheckBoxPreference;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 70
    iput-object p1, p0, LOP;->a:LOJ;

    .line 71
    invoke-direct {p0, p1, p2, p3}, LOO;-><init>(LOJ;Landroid/preference/CheckBoxPreference;Ljava/lang/String;)V

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, LOP;->a:Ljava/lang/Exception;

    .line 72
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 78
    aget-object v0, p1, v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 79
    if-eqz v2, :cond_0

    sget-object v0, LVO;->b:LVO;

    .line 82
    :goto_0
    :try_start_0
    iget-object v3, p0, LOP;->a:LOJ;

    iget-object v3, v3, LOJ;->a:LVN;

    iget-object v4, p0, LOP;->a:LOJ;

    invoke-static {v4}, LOJ;->a(LOJ;)LaFO;

    move-result-object v4

    invoke-interface {v3, v4, v0}, LVN;->a(LaFO;LVO;)V

    .line 83
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 86
    :goto_1
    return-object v0

    .line 79
    :cond_0
    sget-object v0, LVO;->c:LVO;

    goto :goto_0

    .line 84
    :catch_0
    move-exception v0

    .line 85
    iput-object v0, p0, LOP;->a:Ljava/lang/Exception;

    .line 86
    if-nez v2, :cond_1

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_2
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 92
    invoke-super {p0, p1}, LOO;->a(Ljava/lang/Boolean;)V

    .line 93
    iget-object v0, p0, LOP;->a:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, LOP;->a:LOJ;

    invoke-static {v0}, LOJ;->a(LOJ;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 95
    sget v1, LOV;->prefs_change_error:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 97
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 68
    check-cast p1, [Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, LOP;->a([Ljava/lang/Boolean;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 68
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, LOP;->a(Ljava/lang/Boolean;)V

    return-void
.end method
