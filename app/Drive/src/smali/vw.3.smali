.class public Lvw;
.super Ljava/lang/Object;
.source "VersionCheckDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lvw;->a:Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 91
    iget-object v0, p0, Lvw;->a:Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a:LQr;

    iget-object v1, p0, Lvw;->a:Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;

    iget-object v1, v1, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "market://details?id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lvw;->a:Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;

    iget-object v3, v3, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a:Landroid/content/Context;

    .line 92
    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 91
    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 94
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 96
    :try_start_0
    iget-object v2, p0, Lvw;->a:Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a()LH;

    move-result-object v2

    invoke-virtual {v2, v0}, LH;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    :goto_0
    iget-object v0, p0, Lvw;->a:Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a()LH;

    move-result-object v0

    invoke-virtual {v0}, LH;->finish()V

    .line 104
    return-void

    .line 97
    :catch_0
    move-exception v0

    .line 98
    const-string v2, "VersionCheck"

    const-string v3, "Unable to launch upgrade link: %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 99
    iget-object v2, p0, Lvw;->a:Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;

    iget-object v2, v2, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a:Lald;

    const-string v3, "Google Docs was unable to launch the upgrade link: %1$s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v1, v4, v5

    .line 100
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 99
    invoke-interface {v2, v1, v0}, Lald;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
