.class final enum Lok;
.super Ljava/lang/Enum;
.source "GAServiceProxy.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lok;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lok;

.field private static final synthetic a:[Lok;

.field public static final enum b:Lok;

.field public static final enum c:Lok;

.field public static final enum d:Lok;

.field public static final enum e:Lok;

.field public static final enum f:Lok;

.field public static final enum g:Lok;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 30
    new-instance v0, Lok;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v3}, Lok;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lok;->a:Lok;

    .line 31
    new-instance v0, Lok;

    const-string v1, "CONNECTED_SERVICE"

    invoke-direct {v0, v1, v4}, Lok;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lok;->b:Lok;

    .line 32
    new-instance v0, Lok;

    const-string v1, "CONNECTED_LOCAL"

    invoke-direct {v0, v1, v5}, Lok;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lok;->c:Lok;

    .line 33
    new-instance v0, Lok;

    const-string v1, "BLOCKED"

    invoke-direct {v0, v1, v6}, Lok;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lok;->d:Lok;

    .line 34
    new-instance v0, Lok;

    const-string v1, "PENDING_CONNECTION"

    invoke-direct {v0, v1, v7}, Lok;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lok;->e:Lok;

    .line 35
    new-instance v0, Lok;

    const-string v1, "PENDING_DISCONNECT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lok;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lok;->f:Lok;

    .line 36
    new-instance v0, Lok;

    const-string v1, "DISCONNECTED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lok;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lok;->g:Lok;

    .line 29
    const/4 v0, 0x7

    new-array v0, v0, [Lok;

    sget-object v1, Lok;->a:Lok;

    aput-object v1, v0, v3

    sget-object v1, Lok;->b:Lok;

    aput-object v1, v0, v4

    sget-object v1, Lok;->c:Lok;

    aput-object v1, v0, v5

    sget-object v1, Lok;->d:Lok;

    aput-object v1, v0, v6

    sget-object v1, Lok;->e:Lok;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lok;->f:Lok;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lok;->g:Lok;

    aput-object v2, v0, v1

    sput-object v0, Lok;->a:[Lok;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lok;
    .locals 1

    .prologue
    .line 29
    const-class v0, Lok;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lok;

    return-object v0
.end method

.method public static values()[Lok;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lok;->a:[Lok;

    invoke-virtual {v0}, [Lok;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lok;

    return-object v0
.end method
