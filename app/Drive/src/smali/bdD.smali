.class public final LbdD;
.super Ljava/lang/Object;
.source "MediaHttpUploader.java"


# instance fields
.field private a:I

.field private a:J

.field private a:LbdE;

.field private a:LbdF;

.field private final a:LbdS;

.field private a:LbdZ;

.field private a:Lbed;

.field private a:Lbei;

.field private final a:Lbej;

.field private final a:Lbeq;

.field a:LbfP;

.field private a:Ljava/io/InputStream;

.field private a:Ljava/lang/Byte;

.field a:Ljava/lang/String;

.field private a:Z

.field private a:[B

.field private b:I

.field private b:J

.field private b:Ljava/lang/String;

.field private b:Z

.field private c:J

.field private c:Z


# direct methods
.method public constructor <init>(LbdS;Lbeq;Lbek;)V
    .locals 1

    .prologue
    .line 285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    sget-object v0, LbdE;->a:LbdE;

    iput-object v0, p0, LbdD;->a:LbdE;

    .line 181
    const-string v0, "POST"

    iput-object v0, p0, LbdD;->b:Ljava/lang/String;

    .line 184
    new-instance v0, Lbed;

    invoke-direct {v0}, Lbed;-><init>()V

    iput-object v0, p0, LbdD;->a:Lbed;

    .line 213
    const-string v0, "*"

    iput-object v0, p0, LbdD;->a:Ljava/lang/String;

    .line 226
    const/high16 v0, 0xa00000

    iput v0, p0, LbdD;->a:I

    .line 264
    sget-object v0, LbfP;->a:LbfP;

    iput-object v0, p0, LbdD;->a:LbfP;

    .line 286
    invoke-static {p1}, LbfN;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbdS;

    iput-object v0, p0, LbdD;->a:LbdS;

    .line 287
    invoke-static {p2}, LbfN;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbeq;

    iput-object v0, p0, LbdD;->a:Lbeq;

    .line 288
    if-nez p3, :cond_0

    .line 289
    invoke-virtual {p2}, Lbeq;->a()Lbej;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LbdD;->a:Lbej;

    .line 290
    return-void

    .line 289
    :cond_0
    invoke-virtual {p2, p3}, Lbeq;->a(Lbek;)Lbej;

    move-result-object v0

    goto :goto_0
.end method

.method private a()J
    .locals 2

    .prologue
    .line 495
    iget-boolean v0, p0, LbdD;->a:Z

    if-nez v0, :cond_0

    .line 496
    iget-object v0, p0, LbdD;->a:LbdS;

    invoke-virtual {v0}, LbdS;->a()J

    move-result-wide v0

    iput-wide v0, p0, LbdD;->a:J

    .line 497
    const/4 v0, 0x1

    iput-boolean v0, p0, LbdD;->a:Z

    .line 499
    :cond_0
    iget-wide v0, p0, LbdD;->a:J

    return-wide v0
.end method

.method private a(Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 694
    if-nez p1, :cond_0

    .line 695
    const-wide/16 v0, 0x0

    .line 697
    :goto_0
    return-wide v0

    :cond_0
    const/16 v0, 0x2d

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method private a(Lbei;)Lbel;
    .locals 1

    .prologue
    .line 541
    new-instance v0, Lbdk;

    invoke-direct {v0}, Lbdk;-><init>()V

    invoke-virtual {v0, p1}, Lbdk;->b(Lbei;)V

    .line 543
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lbei;->b(Z)Lbei;

    .line 545
    invoke-virtual {p1}, Lbei;->a()Lbel;

    move-result-object v0

    .line 546
    return-object v0
.end method

.method private a(LbdE;)V
    .locals 1

    .prologue
    .line 904
    iput-object p1, p0, LbdD;->a:LbdE;

    .line 905
    iget-object v0, p0, LbdD;->a:LbdF;

    if-eqz v0, :cond_0

    .line 906
    iget-object v0, p0, LbdD;->a:LbdF;

    invoke-interface {v0, p0}, LbdF;->a(LbdD;)V

    .line 908
    :cond_0
    return-void
.end method

.method private a()Z
    .locals 4

    .prologue
    .line 484
    invoke-direct {p0}, LbdD;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LbdY;)Lbel;
    .locals 4

    .prologue
    .line 346
    sget-object v0, LbdE;->d:LbdE;

    invoke-direct {p0, v0}, LbdD;->a(LbdE;)V

    .line 348
    iget-object v0, p0, LbdD;->a:LbdS;

    .line 349
    iget-object v1, p0, LbdD;->a:LbdZ;

    if-eqz v1, :cond_1

    .line 350
    new-instance v0, Lbev;

    invoke-direct {v0}, Lbev;-><init>()V

    const/4 v1, 0x2

    new-array v1, v1, [LbdZ;

    const/4 v2, 0x0

    iget-object v3, p0, LbdD;->a:LbdZ;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LbdD;->a:LbdS;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbev;->a(Ljava/util/Collection;)Lbev;

    move-result-object v0

    .line 351
    const-string v1, "uploadType"

    const-string v2, "multipart"

    invoke-virtual {p1, v1, v2}, LbdY;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    :goto_0
    iget-object v1, p0, LbdD;->a:Lbej;

    iget-object v2, p0, LbdD;->b:Ljava/lang/String;

    .line 356
    invoke-virtual {v1, v2, p1, v0}, Lbej;->a(Ljava/lang/String;LbdY;LbdZ;)Lbei;

    move-result-object v0

    .line 357
    invoke-virtual {v0}, Lbei;->a()Lbed;

    move-result-object v1

    iget-object v2, p0, LbdD;->a:Lbed;

    invoke-virtual {v1, v2}, Lbed;->putAll(Ljava/util/Map;)V

    .line 360
    invoke-direct {p0, v0}, LbdD;->b(Lbei;)Lbel;

    move-result-object v1

    .line 363
    :try_start_0
    invoke-direct {p0}, LbdD;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364
    invoke-direct {p0}, LbdD;->a()J

    move-result-wide v2

    iput-wide v2, p0, LbdD;->b:J

    .line 366
    :cond_0
    sget-object v0, LbdE;->e:LbdE;

    invoke-direct {p0, v0}, LbdD;->a(LbdE;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 373
    return-object v1

    .line 353
    :cond_1
    const-string v1, "uploadType"

    const-string v2, "media"

    invoke-virtual {p1, v1, v2}, LbdY;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 369
    :catchall_0
    move-exception v0

    .line 370
    invoke-virtual {v1}, Lbel;->b()V

    throw v0
.end method

.method private b(Lbei;)Lbel;
    .locals 1

    .prologue
    .line 558
    iget-boolean v0, p0, LbdD;->c:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lbei;->a()LbdZ;

    move-result-object v0

    instance-of v0, v0, LbdV;

    if-nez v0, :cond_0

    .line 559
    new-instance v0, LbdW;

    invoke-direct {v0}, LbdW;-><init>()V

    invoke-virtual {p1, v0}, Lbei;->a(Lbea;)Lbei;

    .line 562
    :cond_0
    invoke-direct {p0, p1}, LbdD;->a(Lbei;)Lbel;

    move-result-object v0

    .line 563
    return-object v0
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 572
    invoke-direct {p0}, LbdD;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 574
    iget v0, p0, LbdD;->a:I

    int-to-long v0, v0

    invoke-direct {p0}, LbdD;->a()J

    move-result-wide v4

    iget-wide v6, p0, LbdD;->b:J

    sub-long/2addr v4, v6

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    .line 582
    :goto_0
    invoke-direct {p0}, LbdD;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 584
    iget-object v1, p0, LbdD;->a:Ljava/io/InputStream;

    invoke-virtual {v1, v0}, Ljava/io/InputStream;->mark(I)V

    .line 586
    iget-object v1, p0, LbdD;->a:Ljava/io/InputStream;

    int-to-long v4, v0

    invoke-static {v1, v4, v5}, Lbfn;->a(Ljava/io/InputStream;J)Ljava/io/InputStream;

    move-result-object v1

    .line 587
    new-instance v3, Lbes;

    iget-object v4, p0, LbdD;->a:LbdS;

    .line 588
    invoke-virtual {v4}, LbdS;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lbes;-><init>(Ljava/lang/String;Ljava/io/InputStream;)V

    const/4 v1, 0x1

    invoke-virtual {v3, v1}, Lbes;->a(Z)Lbes;

    move-result-object v1

    int-to-long v4, v0

    .line 589
    invoke-virtual {v1, v4, v5}, Lbes;->a(J)Lbes;

    move-result-object v1

    invoke-virtual {v1, v2}, Lbes;->b(Z)Lbes;

    move-result-object v1

    .line 590
    invoke-direct {p0}, LbdD;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, LbdD;->a:Ljava/lang/String;

    .line 654
    :goto_1
    iput v0, p0, LbdD;->b:I

    .line 655
    iget-object v2, p0, LbdD;->a:Lbei;

    invoke-virtual {v2, v1}, Lbei;->a(LbdZ;)Lbei;

    .line 656
    if-nez v0, :cond_8

    .line 658
    iget-object v0, p0, LbdD;->a:Lbei;

    invoke-virtual {v0}, Lbei;->a()Lbed;

    move-result-object v0

    const-string v1, "bytes */0"

    invoke-virtual {v0, v1}, Lbed;->d(Ljava/lang/String;)Lbed;

    .line 663
    :goto_2
    return-void

    .line 577
    :cond_0
    iget v0, p0, LbdD;->a:I

    goto :goto_0

    .line 601
    :cond_1
    iget-object v1, p0, LbdD;->a:[B

    if-nez v1, :cond_5

    .line 602
    iget-object v1, p0, LbdD;->a:Ljava/lang/Byte;

    if-nez v1, :cond_4

    add-int/lit8 v1, v0, 0x1

    .line 603
    :goto_3
    add-int/lit8 v3, v0, 0x1

    new-array v3, v3, [B

    iput-object v3, p0, LbdD;->a:[B

    .line 604
    iget-object v3, p0, LbdD;->a:Ljava/lang/Byte;

    if-eqz v3, :cond_9

    .line 605
    iget-object v3, p0, LbdD;->a:[B

    iget-object v4, p0, LbdD;->a:Ljava/lang/Byte;

    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v4

    aput-byte v4, v3, v2

    move v3, v1

    move v1, v2

    .line 629
    :goto_4
    iget-object v4, p0, LbdD;->a:Ljava/io/InputStream;

    iget-object v5, p0, LbdD;->a:[B

    add-int/lit8 v6, v0, 0x1

    sub-int/2addr v6, v3

    invoke-static {v4, v5, v6, v3}, Lbfn;->a(Ljava/io/InputStream;[BII)I

    move-result v4

    .line 633
    if-ge v4, v3, :cond_7

    .line 634
    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, v1

    .line 635
    iget-object v1, p0, LbdD;->a:Ljava/lang/Byte;

    if-eqz v1, :cond_2

    .line 636
    add-int/lit8 v0, v0, 0x1

    .line 637
    const/4 v1, 0x0

    iput-object v1, p0, LbdD;->a:Ljava/lang/Byte;

    .line 640
    :cond_2
    iget-object v1, p0, LbdD;->a:Ljava/lang/String;

    const-string v3, "*"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 643
    iget-wide v4, p0, LbdD;->b:J

    int-to-long v6, v0

    add-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LbdD;->a:Ljava/lang/String;

    .line 649
    :cond_3
    :goto_5
    new-instance v1, LbdU;

    iget-object v3, p0, LbdD;->a:LbdS;

    .line 650
    invoke-virtual {v3}, LbdS;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LbdD;->a:[B

    invoke-direct {v1, v3, v4, v2, v0}, LbdU;-><init>(Ljava/lang/String;[BII)V

    .line 651
    iget-wide v2, p0, LbdD;->b:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, LbdD;->c:J

    goto :goto_1

    :cond_4
    move v1, v0

    .line 602
    goto :goto_3

    .line 616
    :cond_5
    iget-wide v4, p0, LbdD;->c:J

    iget-wide v6, p0, LbdD;->b:J

    sub-long/2addr v4, v6

    long-to-int v1, v4

    .line 619
    iget-object v3, p0, LbdD;->a:[B

    iget v4, p0, LbdD;->b:I

    sub-int/2addr v4, v1

    iget-object v5, p0, LbdD;->a:[B

    invoke-static {v3, v4, v5, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 621
    iget-object v3, p0, LbdD;->a:Ljava/lang/Byte;

    if-eqz v3, :cond_6

    .line 623
    iget-object v3, p0, LbdD;->a:[B

    iget-object v4, p0, LbdD;->a:Ljava/lang/Byte;

    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v4

    aput-byte v4, v3, v1

    .line 626
    :cond_6
    sub-int v3, v0, v1

    goto :goto_4

    .line 646
    :cond_7
    iget-object v1, p0, LbdD;->a:[B

    aget-byte v1, v1, v0

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    iput-object v1, p0, LbdD;->a:Ljava/lang/Byte;

    goto :goto_5

    .line 660
    :cond_8
    iget-object v1, p0, LbdD;->a:Lbei;

    invoke-virtual {v1}, Lbei;->a()Lbed;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bytes "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, LbdD;->b:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, LbdD;->b:J

    int-to-long v6, v0

    add-long/2addr v4, v6

    const-wide/16 v6, 0x1

    sub-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LbdD;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbed;->d(Ljava/lang/String;)Lbed;

    goto/16 :goto_2

    :cond_9
    move v3, v1

    move v1, v2

    goto/16 :goto_4
.end method

.method private c(LbdY;)Lbel;
    .locals 10

    .prologue
    .line 384
    invoke-direct {p0, p1}, LbdD;->d(LbdY;)Lbel;

    move-result-object v1

    .line 385
    invoke-virtual {v1}, Lbel;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 474
    :cond_0
    :goto_0
    return-object v1

    .line 391
    :cond_1
    :try_start_0
    new-instance v0, LbdY;

    invoke-virtual {v1}, Lbel;->a()Lbed;

    move-result-object v2

    invoke-virtual {v2}, Lbed;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, LbdY;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 393
    invoke-virtual {v1}, Lbel;->b()V

    .line 397
    iget-object v1, p0, LbdD;->a:LbdS;

    invoke-virtual {v1}, LbdS;->a()Ljava/io/InputStream;

    move-result-object v1

    iput-object v1, p0, LbdD;->a:Ljava/io/InputStream;

    .line 398
    iget-object v1, p0, LbdD;->a:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->markSupported()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-direct {p0}, LbdD;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 402
    new-instance v1, Ljava/io/BufferedInputStream;

    iget-object v2, p0, LbdD;->a:Ljava/io/InputStream;

    invoke-direct {v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v1, p0, LbdD;->a:Ljava/io/InputStream;

    .line 408
    :cond_2
    :goto_1
    iget-object v1, p0, LbdD;->a:Lbej;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lbej;->b(LbdY;LbdZ;)Lbei;

    move-result-object v1

    iput-object v1, p0, LbdD;->a:Lbei;

    .line 409
    invoke-direct {p0}, LbdD;->b()V

    .line 412
    new-instance v1, LbdG;

    iget-object v2, p0, LbdD;->a:Lbei;

    invoke-direct {v1, p0, v2}, LbdG;-><init>(LbdD;Lbei;)V

    .line 414
    invoke-direct {p0}, LbdD;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 417
    iget-object v1, p0, LbdD;->a:Lbei;

    invoke-direct {p0, v1}, LbdD;->a(Lbei;)Lbel;

    move-result-object v1

    .line 423
    :goto_2
    :try_start_1
    invoke-virtual {v1}, Lbel;->a()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 424
    invoke-direct {p0}, LbdD;->a()J

    move-result-wide v2

    iput-wide v2, p0, LbdD;->b:J

    .line 425
    iget-object v0, p0, LbdD;->a:LbdS;

    invoke-virtual {v0}, LbdS;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 426
    iget-object v0, p0, LbdD;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 428
    :cond_3
    sget-object v0, LbdE;->e:LbdE;

    invoke-direct {p0, v0}, LbdD;->a(LbdE;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 473
    :catchall_0
    move-exception v0

    .line 474
    invoke-virtual {v1}, Lbel;->b()V

    throw v0

    .line 393
    :catchall_1
    move-exception v0

    invoke-virtual {v1}, Lbel;->b()V

    throw v0

    .line 419
    :cond_4
    iget-object v1, p0, LbdD;->a:Lbei;

    invoke-direct {p0, v1}, LbdD;->b(Lbei;)Lbel;

    move-result-object v1

    goto :goto_2

    .line 433
    :cond_5
    :try_start_2
    invoke-virtual {v1}, Lbel;->a()I

    move-result v2

    const/16 v3, 0x134

    if-ne v2, v3, :cond_0

    .line 439
    invoke-virtual {v1}, Lbel;->a()Lbed;

    move-result-object v2

    invoke-virtual {v2}, Lbed;->c()Ljava/lang/String;

    move-result-object v2

    .line 440
    if-eqz v2, :cond_6

    .line 441
    new-instance v0, LbdY;

    invoke-direct {v0, v2}, LbdY;-><init>(Ljava/lang/String;)V

    .line 446
    :cond_6
    invoke-virtual {v1}, Lbel;->a()Lbed;

    move-result-object v2

    invoke-virtual {v2}, Lbed;->d()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, LbdD;->a(Ljava/lang/String;)J

    move-result-wide v4

    .line 448
    iget-wide v2, p0, LbdD;->b:J

    sub-long v6, v4, v2

    .line 449
    const-wide/16 v2, 0x0

    cmp-long v2, v6, v2

    if-ltz v2, :cond_8

    iget v2, p0, LbdD;->b:I

    int-to-long v2, v2

    cmp-long v2, v6, v2

    if-gtz v2, :cond_8

    const/4 v2, 0x1

    :goto_3
    invoke-static {v2}, LbfN;->b(Z)V

    .line 451
    iget v2, p0, LbdD;->b:I

    int-to-long v2, v2

    sub-long/2addr v2, v6

    .line 452
    invoke-direct {p0}, LbdD;->a()Z

    move-result v8

    if-eqz v8, :cond_a

    .line 453
    const-wide/16 v8, 0x0

    cmp-long v2, v2, v8

    if-lez v2, :cond_7

    .line 459
    iget-object v2, p0, LbdD;->a:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->reset()V

    .line 460
    iget-object v2, p0, LbdD;->a:Ljava/io/InputStream;

    invoke-virtual {v2, v6, v7}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v2

    .line 461
    cmp-long v2, v6, v2

    if-nez v2, :cond_9

    const/4 v2, 0x1

    :goto_4
    invoke-static {v2}, LbfN;->b(Z)V

    .line 469
    :cond_7
    :goto_5
    iput-wide v4, p0, LbdD;->b:J

    .line 471
    sget-object v2, LbdE;->d:LbdE;

    invoke-direct {p0, v2}, LbdD;->a(LbdE;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 474
    invoke-virtual {v1}, Lbel;->b()V

    goto/16 :goto_1

    .line 449
    :cond_8
    const/4 v2, 0x0

    goto :goto_3

    .line 461
    :cond_9
    const/4 v2, 0x0

    goto :goto_4

    .line 463
    :cond_a
    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-nez v2, :cond_7

    .line 467
    const/4 v2, 0x0

    :try_start_3
    iput-object v2, p0, LbdD;->a:[B
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_5
.end method

.method private d(LbdY;)Lbel;
    .locals 6

    .prologue
    .line 508
    sget-object v0, LbdE;->b:LbdE;

    invoke-direct {p0, v0}, LbdD;->a(LbdE;)V

    .line 510
    const-string v0, "uploadType"

    const-string v1, "resumable"

    invoke-virtual {p1, v0, v1}, LbdY;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 511
    iget-object v0, p0, LbdD;->a:LbdZ;

    if-nez v0, :cond_1

    new-instance v0, LbdV;

    invoke-direct {v0}, LbdV;-><init>()V

    .line 512
    :goto_0
    iget-object v1, p0, LbdD;->a:Lbej;

    iget-object v2, p0, LbdD;->b:Ljava/lang/String;

    .line 513
    invoke-virtual {v1, v2, p1, v0}, Lbej;->a(Ljava/lang/String;LbdY;LbdZ;)Lbei;

    move-result-object v0

    .line 514
    iget-object v1, p0, LbdD;->a:Lbed;

    const-string v2, "X-Upload-Content-Type"

    iget-object v3, p0, LbdD;->a:LbdS;

    invoke-virtual {v3}, LbdS;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lbed;->a(Ljava/lang/String;Ljava/lang/Object;)Lbed;

    .line 515
    invoke-direct {p0}, LbdD;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 516
    iget-object v1, p0, LbdD;->a:Lbed;

    const-string v2, "X-Upload-Content-Length"

    invoke-direct {p0}, LbdD;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lbed;->a(Ljava/lang/String;Ljava/lang/Object;)Lbed;

    .line 518
    :cond_0
    invoke-virtual {v0}, Lbei;->a()Lbed;

    move-result-object v1

    iget-object v2, p0, LbdD;->a:Lbed;

    invoke-virtual {v1, v2}, Lbed;->putAll(Ljava/util/Map;)V

    .line 519
    invoke-direct {p0, v0}, LbdD;->b(Lbei;)Lbel;

    move-result-object v1

    .line 523
    :try_start_0
    sget-object v0, LbdE;->c:LbdE;

    invoke-direct {p0, v0}, LbdD;->a(LbdE;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 530
    return-object v1

    .line 511
    :cond_1
    iget-object v0, p0, LbdD;->a:LbdZ;

    goto :goto_0

    .line 526
    :catchall_0
    move-exception v0

    .line 527
    invoke-virtual {v1}, Lbel;->b()V

    throw v0
.end method


# virtual methods
.method public a(LbdZ;)LbdD;
    .locals 0

    .prologue
    .line 707
    iput-object p1, p0, LbdD;->a:LbdZ;

    .line 708
    return-object p0
.end method

.method public a(Lbed;)LbdD;
    .locals 0

    .prologue
    .line 879
    iput-object p1, p0, LbdD;->a:Lbed;

    .line 880
    return-object p0
.end method

.method public a(Ljava/lang/String;)LbdD;
    .locals 1

    .prologue
    .line 871
    const-string v0, "POST"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "PUT"

    .line 872
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 871
    :goto_0
    invoke-static {v0}, LbfN;->a(Z)V

    .line 873
    iput-object p1, p0, LbdD;->b:Ljava/lang/String;

    .line 874
    return-object p0

    .line 872
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Z)LbdD;
    .locals 0

    .prologue
    .line 824
    iput-boolean p1, p0, LbdD;->c:Z

    .line 825
    return-object p0
.end method

.method public a(LbdY;)Lbel;
    .locals 2

    .prologue
    .line 331
    iget-object v0, p0, LbdD;->a:LbdE;

    sget-object v1, LbdE;->a:LbdE;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbfN;->a(Z)V

    .line 333
    iget-boolean v0, p0, LbdD;->b:Z

    if-eqz v0, :cond_1

    .line 334
    invoke-direct {p0, p1}, LbdD;->b(LbdY;)Lbel;

    move-result-object v0

    .line 336
    :goto_1
    return-object v0

    .line 331
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 336
    :cond_1
    invoke-direct {p0, p1}, LbdD;->c(LbdY;)Lbel;

    move-result-object v0

    goto :goto_1
.end method

.method a()V
    .locals 6

    .prologue
    .line 677
    iget-object v0, p0, LbdD;->a:Lbei;

    const-string v1, "The current request should not be null"

    invoke-static {v0, v1}, LbfN;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 680
    iget-object v0, p0, LbdD;->a:Lbei;

    new-instance v1, LbdV;

    invoke-direct {v1}, LbdV;-><init>()V

    invoke-virtual {v0, v1}, Lbei;->a(LbdZ;)Lbei;

    .line 681
    iget-object v0, p0, LbdD;->a:Lbei;

    invoke-virtual {v0}, Lbei;->a()Lbed;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bytes */"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 682
    invoke-direct {p0}, LbdD;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, LbdD;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbed;->d(Ljava/lang/String;)Lbed;

    .line 683
    return-void

    .line 682
    :cond_0
    const-string v0, "*"

    goto :goto_0
.end method
