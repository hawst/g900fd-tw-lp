.class public final Laak;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laah;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laao;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laap;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laas;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laaj;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LRO;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laau;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 44
    iput-object p1, p0, Laak;->a:LbrA;

    .line 45
    const-class v0, Laah;

    invoke-static {v0, v1}, Laak;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laak;->a:Lbsk;

    .line 48
    const-class v0, Laao;

    invoke-static {v0, v1}, Laak;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laak;->b:Lbsk;

    .line 51
    const-class v0, Laap;

    invoke-static {v0, v1}, Laak;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laak;->c:Lbsk;

    .line 54
    const-class v0, Laas;

    invoke-static {v0, v1}, Laak;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laak;->d:Lbsk;

    .line 57
    const-class v0, Laaj;

    invoke-static {v0, v1}, Laak;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laak;->e:Lbsk;

    .line 60
    const-class v0, LRO;

    invoke-static {v0, v1}, Laak;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laak;->f:Lbsk;

    .line 63
    const-class v0, Laau;

    invoke-static {v0, v1}, Laak;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, Laak;->g:Lbsk;

    .line 66
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 102
    packed-switch p1, :pswitch_data_0

    .line 146
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 104
    :pswitch_1
    new-instance v3, Laah;

    iget-object v0, p0, Laak;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laak;

    iget-object v0, v0, Laak;->e:Lbsk;

    .line 107
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Laak;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Laak;

    iget-object v1, v1, Laak;->e:Lbsk;

    .line 105
    invoke-static {v0, v1}, Laak;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laaj;

    iget-object v1, p0, Laak;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LagN;

    iget-object v1, v1, LagN;->J:Lbsk;

    .line 113
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Laak;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LagN;

    iget-object v2, v2, LagN;->J:Lbsk;

    .line 111
    invoke-static {v1, v2}, Laak;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lagl;

    iget-object v2, p0, Laak;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Laak;

    iget-object v2, v2, Laak;->g:Lbsk;

    .line 119
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, Laak;->a:LbrA;

    iget-object v4, v4, LbrA;->a:Laak;

    iget-object v4, v4, Laak;->g:Lbsk;

    .line 117
    invoke-static {v2, v4}, Laak;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Laau;

    invoke-direct {v3, v0, v1, v2}, Laah;-><init>(Laaj;Lagl;Laau;)V

    move-object v0, v3

    .line 144
    :goto_0
    return-object v0

    .line 126
    :pswitch_2
    new-instance v1, Laao;

    iget-object v0, p0, Laak;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LagN;

    iget-object v0, v0, LagN;->B:Lbsk;

    .line 129
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Laak;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LagN;

    iget-object v2, v2, LagN;->B:Lbsk;

    .line 127
    invoke-static {v0, v2}, Laak;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LagS;

    invoke-direct {v1, v0}, Laao;-><init>(LagS;)V

    move-object v0, v1

    .line 134
    goto :goto_0

    .line 136
    :pswitch_3
    new-instance v1, Laap;

    iget-object v0, p0, Laak;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LagN;

    iget-object v0, v0, LagN;->B:Lbsk;

    .line 139
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Laak;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LagN;

    iget-object v2, v2, LagN;->B:Lbsk;

    .line 137
    invoke-static {v0, v2}, Laak;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LagS;

    invoke-direct {v1, v0}, Laap;-><init>(LagS;)V

    move-object v0, v1

    .line 144
    goto :goto_0

    .line 102
    nop

    :pswitch_data_0
    .packed-switch 0x54e
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 161
    sparse-switch p2, :sswitch_data_0

    .line 199
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163
    :sswitch_0
    check-cast p1, Laav;

    .line 165
    iget-object v0, p0, Laak;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laak;

    iget-object v0, v0, Laak;->a:Lbsk;

    .line 168
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laah;

    .line 165
    invoke-virtual {p1, v0}, Laav;->provideSearchHandler(Laah;)Laas;

    move-result-object v0

    .line 192
    :goto_0
    return-object v0

    .line 172
    :sswitch_1
    check-cast p1, Laav;

    .line 174
    iget-object v0, p0, Laak;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->j:Lbsk;

    .line 177
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGg;

    .line 174
    invoke-virtual {p1, v0}, Laav;->provideCachedSearchModelLoader(LaGg;)Laaj;

    move-result-object v0

    goto :goto_0

    .line 181
    :sswitch_2
    check-cast p1, Laav;

    .line 183
    iget-object v0, p0, Laak;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LRo;

    iget-object v0, v0, LRo;->e:Lbsk;

    .line 186
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LRP;

    .line 183
    invoke-virtual {p1, v0}, Laav;->provideOnlineSearchFragmentHelper(LRP;)LRO;

    move-result-object v0

    goto :goto_0

    .line 190
    :sswitch_3
    check-cast p1, Laav;

    .line 192
    iget-object v0, p0, Laak;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Laak;

    iget-object v0, v0, Laak;->c:Lbsk;

    .line 195
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laap;

    .line 192
    invoke-virtual {p1, v0}, Laav;->provideSearchLauncher(Laap;)Laau;

    move-result-object v0

    goto :goto_0

    .line 161
    :sswitch_data_0
    .sparse-switch
        0x12b -> :sswitch_2
        0x2b2 -> :sswitch_0
        0x54f -> :sswitch_1
        0x550 -> :sswitch_3
    .end sparse-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 73
    const-class v0, Laah;

    iget-object v1, p0, Laak;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, Laak;->a(Ljava/lang/Class;Lbsk;)V

    .line 74
    const-class v0, Laao;

    iget-object v1, p0, Laak;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, Laak;->a(Ljava/lang/Class;Lbsk;)V

    .line 75
    const-class v0, Laap;

    iget-object v1, p0, Laak;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, Laak;->a(Ljava/lang/Class;Lbsk;)V

    .line 76
    const-class v0, Laas;

    iget-object v1, p0, Laak;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, Laak;->a(Ljava/lang/Class;Lbsk;)V

    .line 77
    const-class v0, Laaj;

    iget-object v1, p0, Laak;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, Laak;->a(Ljava/lang/Class;Lbsk;)V

    .line 78
    const-class v0, LRO;

    iget-object v1, p0, Laak;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, Laak;->a(Ljava/lang/Class;Lbsk;)V

    .line 79
    const-class v0, Laau;

    iget-object v1, p0, Laak;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, Laak;->a(Ljava/lang/Class;Lbsk;)V

    .line 80
    iget-object v0, p0, Laak;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x54e

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 82
    iget-object v0, p0, Laak;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x552

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 84
    iget-object v0, p0, Laak;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x551

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 86
    iget-object v0, p0, Laak;->d:Lbsk;

    const-class v1, Laav;

    const/16 v2, 0x2b2

    invoke-virtual {p0, v1, v2}, Laak;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 88
    iget-object v0, p0, Laak;->e:Lbsk;

    const-class v1, Laav;

    const/16 v2, 0x54f

    invoke-virtual {p0, v1, v2}, Laak;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 90
    iget-object v0, p0, Laak;->f:Lbsk;

    const-class v1, Laav;

    const/16 v2, 0x12b

    invoke-virtual {p0, v1, v2}, Laak;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 92
    iget-object v0, p0, Laak;->g:Lbsk;

    const-class v1, Laav;

    const/16 v2, 0x550

    invoke-virtual {p0, v1, v2}, Laak;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 94
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 153
    .line 155
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 98
    return-void
.end method
