.class public final LawG;
.super Ljava/lang/Object;
.source "ZoomView.java"

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)V
    .locals 0

    .prologue
    .line 669
    iput-object p1, p0, LawG;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/viewer/viewer/image/ZoomView;LawD;)V
    .locals 0

    .prologue
    .line 669
    invoke-direct {p0, p1}, LawG;-><init>(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 4

    .prologue
    .line 672
    iget-object v0, p0, LawG;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()F

    move-result v0

    .line 673
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v1

    mul-float/2addr v1, v0

    const/high16 v2, 0x3f000000    # 0.5f

    const/high16 v3, 0x42800000    # 64.0f

    invoke-static {v1, v2, v3}, Lawb;->a(FFF)F

    move-result v1

    .line 674
    cmpl-float v0, v1, v0

    if-eqz v0, :cond_0

    .line 675
    iget-object v0, p0, LawG;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0, v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;F)V

    .line 676
    iget-object v0, p0, LawG;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Z

    .line 677
    iget-object v0, p0, LawG;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;Z)V

    .line 679
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 684
    iget-object v0, p0, LawG;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0, v5}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;Z)Z

    .line 685
    iget-object v0, p0, LawG;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0, v4}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;Z)Z

    .line 686
    iget-object v0, p0, LawG;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;FF)V

    .line 687
    const-string v0, "ZoomView"

    const-string v1, "Scale starts zoom %s pivot (%.0f, %.0f) bounds %s, scroll (%s %s)"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LawG;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    .line 688
    invoke-virtual {v3}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v4

    iget-object v3, p0, LawG;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v3}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getPivotX()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x2

    iget-object v4, p0, LawG;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v4}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getPivotY()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, LawG;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v4}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/graphics/RectF;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p0, LawG;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    .line 689
    invoke-virtual {v4}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getScrollX()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget-object v4, p0, LawG;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-virtual {v4}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getScrollY()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 687
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 690
    return v5
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 695
    iget-object v0, p0, LawG;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0, v4}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;Z)Z

    .line 696
    iget-object v0, p0, LawG;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)V

    .line 697
    const-string v0, "ZoomView"

    const-string v1, "Scale ends zoom %.2f bounds %s, scroll (%s %s)"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LawG;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    .line 698
    invoke-virtual {v3}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v4

    iget-object v3, p0, LawG;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v3}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/graphics/RectF;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x2

    iget-object v4, p0, LawG;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-virtual {v4}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getScrollX()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, LawG;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-virtual {v4}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getScrollY()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 697
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 699
    iget-object v0, p0, LawG;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Z

    .line 700
    iget-object v0, p0, LawG;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0, v5}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;Z)V

    .line 701
    const-string v0, "ZoomView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Report Position: Finish Scale "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LawG;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v2}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Lawh;

    move-result-object v2

    invoke-virtual {v2}, Lawh;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 702
    return-void
.end method
