.class public final Liz;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final AccountsSwitcher:I = 0x7f0e0000

.field public static final AccountsSwitcher_AccountDetailsAvatarStyle:I = 0x7f0e0001

.field public static final AccountsSwitcher_AccountDetailsStyle:I = 0x7f0e0002

.field public static final AccountsSwitcher_AccountTextStyle:I = 0x7f0e000a

.field public static final AccountsSwitcher_AvatarStyle:I = 0x7f0e0003

.field public static final AccountsSwitcher_ManageAccountsFrameStyle:I = 0x7f0e0007

.field public static final AccountsSwitcher_ManageAccountsIconStyle:I = 0x7f0e0008

.field public static final AccountsSwitcher_ManageAccountsStyle:I = 0x7f0e0006

.field public static final AccountsSwitcher_ManageAccountsTextStyle:I = 0x7f0e0009

.field public static final AccountsSwitcher_RecentsOneStyle:I = 0x7f0e0004

.field public static final AccountsSwitcher_RecentsTwoStyle:I = 0x7f0e0005

.field public static final AccountsSwitcher_SelectedAccountButtonStyle:I = 0x7f0e000c

.field public static final AccountsSwitcher_SelectedAccountStyle:I = 0x7f0e000b

.field public static final AccountsSwitcher_SignInTextStyle:I = 0x7f0e000d

.field public static final ActionBar:I = 0x7f0e0037

.field public static final ActionBarMenu:I = 0x7f0e0036

.field public static final ActionBarOverflow:I = 0x7f0e0039

.field public static final ActionBarPlain:I = 0x7f0e003a

.field public static final ActionBarText:I = 0x7f0e0038

.field public static final ActionModeCloseButton:I = 0x7f0e003b

.field public static final BaseActionBar:I = 0x7f0e002c

.field public static final BaseActionBarOverflow:I = 0x7f0e002e

.field public static final BaseActionBarText:I = 0x7f0e002d

.field public static final BaseAppTheme:I = 0x7f0e002b

.field public static final BaseAutoCompleteTextView:I = 0x7f0e0034

.field public static final BaseButton:I = 0x7f0e0035

.field public static final BaseDialogTheme:I = 0x7f0e002f

.field public static final BaseDialogThemeWhenLarge:I = 0x7f0e0030

.field public static final Base_TextAppearance_AppCompat:I = 0x7f0e0199

.field public static final Base_TextAppearance_AppCompat_Body1:I = 0x7f0e01a4

.field public static final Base_TextAppearance_AppCompat_Body2:I = 0x7f0e01a3

.field public static final Base_TextAppearance_AppCompat_Button:I = 0x7f0e01a7

.field public static final Base_TextAppearance_AppCompat_Caption:I = 0x7f0e01a5

.field public static final Base_TextAppearance_AppCompat_Display1:I = 0x7f0e019d

.field public static final Base_TextAppearance_AppCompat_Display2:I = 0x7f0e019c

.field public static final Base_TextAppearance_AppCompat_Display3:I = 0x7f0e019b

.field public static final Base_TextAppearance_AppCompat_Display4:I = 0x7f0e019a

.field public static final Base_TextAppearance_AppCompat_Headline:I = 0x7f0e019e

.field public static final Base_TextAppearance_AppCompat_Inverse:I = 0x7f0e01a8

.field public static final Base_TextAppearance_AppCompat_Large:I = 0x7f0e01a9

.field public static final Base_TextAppearance_AppCompat_Large_Inverse:I = 0x7f0e01aa

.field public static final Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Large:I = 0x7f0e0186

.field public static final Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Small:I = 0x7f0e0187

.field public static final Base_TextAppearance_AppCompat_Medium:I = 0x7f0e01ab

.field public static final Base_TextAppearance_AppCompat_Medium_Inverse:I = 0x7f0e01ac

.field public static final Base_TextAppearance_AppCompat_Menu:I = 0x7f0e01a6

.field public static final Base_TextAppearance_AppCompat_SearchResult:I = 0x7f0e0188

.field public static final Base_TextAppearance_AppCompat_SearchResult_Subtitle:I = 0x7f0e018a

.field public static final Base_TextAppearance_AppCompat_SearchResult_Title:I = 0x7f0e0189

.field public static final Base_TextAppearance_AppCompat_Small:I = 0x7f0e01ad

.field public static final Base_TextAppearance_AppCompat_Small_Inverse:I = 0x7f0e01ae

.field public static final Base_TextAppearance_AppCompat_Subhead:I = 0x7f0e01a1

.field public static final Base_TextAppearance_AppCompat_Subhead_Inverse:I = 0x7f0e01a2

.field public static final Base_TextAppearance_AppCompat_Title:I = 0x7f0e019f

.field public static final Base_TextAppearance_AppCompat_Title_Inverse:I = 0x7f0e01a0

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Menu:I = 0x7f0e0171

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle:I = 0x7f0e0173

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse:I = 0x7f0e0175

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Title:I = 0x7f0e0172

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse:I = 0x7f0e0174

.field public static final Base_TextAppearance_AppCompat_Widget_ActionMode_Subtitle:I = 0x7f0e0170

.field public static final Base_TextAppearance_AppCompat_Widget_ActionMode_Title:I = 0x7f0e016f

.field public static final Base_TextAppearance_AppCompat_Widget_DropDownItem:I = 0x7f0e017c

.field public static final Base_TextAppearance_AppCompat_Widget_PopupMenu_Large:I = 0x7f0e0184

.field public static final Base_TextAppearance_AppCompat_Widget_PopupMenu_Small:I = 0x7f0e0185

.field public static final Base_TextAppearance_AppCompat_Widget_Switch:I = 0x7f0e0198

.field public static final Base_TextAppearance_Widget_AppCompat_ExpandedMenu_Item:I = 0x7f0e017d

.field public static final Base_TextAppearance_Widget_AppCompat_Toolbar_Subtitle:I = 0x7f0e0193

.field public static final Base_TextAppearance_Widget_AppCompat_Toolbar_Title:I = 0x7f0e0192

.field public static final Base_ThemeOverlay_AppCompat:I = 0x7f0e01db

.field public static final Base_ThemeOverlay_AppCompat_ActionBar:I = 0x7f0e01de

.field public static final Base_ThemeOverlay_AppCompat_Dark:I = 0x7f0e01dd

.field public static final Base_ThemeOverlay_AppCompat_Dark_ActionBar:I = 0x7f0e01df

.field public static final Base_ThemeOverlay_AppCompat_Light:I = 0x7f0e01dc

.field public static final Base_Theme_AppCompat:I = 0x7f0e01d0

.field public static final Base_Theme_AppCompat_CompactMenu:I = 0x7f0e01d3

.field public static final Base_Theme_AppCompat_Dialog:I = 0x7f0e01d5

.field public static final Base_Theme_AppCompat_DialogWhenLarge:I = 0x7f0e01d9

.field public static final Base_Theme_AppCompat_Dialog_FixedSize:I = 0x7f0e01d7

.field public static final Base_Theme_AppCompat_Light:I = 0x7f0e01d1

.field public static final Base_Theme_AppCompat_Light_DarkActionBar:I = 0x7f0e01d2

.field public static final Base_Theme_AppCompat_Light_Dialog:I = 0x7f0e01d6

.field public static final Base_Theme_AppCompat_Light_DialogWhenLarge:I = 0x7f0e01da

.field public static final Base_Theme_AppCompat_Light_Dialog_FixedSize:I = 0x7f0e01d8

.field public static final Base_V11_Theme_AppCompat:I = 0x7f0e01e0

.field public static final Base_V11_Theme_AppCompat_Dialog:I = 0x7f0e01e2

.field public static final Base_V11_Theme_AppCompat_Light:I = 0x7f0e01e1

.field public static final Base_V11_Theme_AppCompat_Light_Dialog:I = 0x7f0e01e3

.field public static final Base_V14_Theme_AppCompat:I = 0x7f0e01e4

.field public static final Base_V14_Theme_AppCompat_Dialog:I = 0x7f0e01e6

.field public static final Base_V14_Theme_AppCompat_Light:I = 0x7f0e01e5

.field public static final Base_V14_Theme_AppCompat_Light_Dialog:I = 0x7f0e01e7

.field public static final Base_V21_Theme_AppCompat:I = 0x7f0e01e8

.field public static final Base_V21_Theme_AppCompat_Dialog:I = 0x7f0e01ea

.field public static final Base_V21_Theme_AppCompat_Light:I = 0x7f0e01e9

.field public static final Base_V21_Theme_AppCompat_Light_Dialog:I = 0x7f0e01eb

.field public static final Base_V7_Theme_AppCompat:I = 0x7f0e01ce

.field public static final Base_V7_Theme_AppCompat_Dialog:I = 0x7f0e01d4

.field public static final Base_V7_Theme_AppCompat_Light:I = 0x7f0e01cf

.field public static final Base_Widget_AppCompat_ActionBar:I = 0x7f0e0160

.field public static final Base_Widget_AppCompat_ActionBar_Solid:I = 0x7f0e0162

.field public static final Base_Widget_AppCompat_ActionBar_TabBar:I = 0x7f0e0167

.field public static final Base_Widget_AppCompat_ActionBar_TabText:I = 0x7f0e016b

.field public static final Base_Widget_AppCompat_ActionBar_TabView:I = 0x7f0e0169

.field public static final Base_Widget_AppCompat_ActionButton:I = 0x7f0e0164

.field public static final Base_Widget_AppCompat_ActionButton_CloseMode:I = 0x7f0e0165

.field public static final Base_Widget_AppCompat_ActionButton_Overflow:I = 0x7f0e0166

.field public static final Base_Widget_AppCompat_ActionMode:I = 0x7f0e016e

.field public static final Base_Widget_AppCompat_ActivityChooserView:I = 0x7f0e018d

.field public static final Base_Widget_AppCompat_AutoCompleteTextView:I = 0x7f0e018b

.field public static final Base_Widget_AppCompat_CompoundButton_Switch:I = 0x7f0e0197

.field public static final Base_Widget_AppCompat_DrawerArrowToggle:I = 0x7f0e0196

.field public static final Base_Widget_AppCompat_DropDownItem_Spinner:I = 0x7f0e017a

.field public static final Base_Widget_AppCompat_EditText:I = 0x7f0e0195

.field public static final Base_Widget_AppCompat_Light_ActionBar:I = 0x7f0e0161

.field public static final Base_Widget_AppCompat_Light_ActionBar_Solid:I = 0x7f0e0163

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabBar:I = 0x7f0e0168

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabText:I = 0x7f0e016c

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabText_Inverse:I = 0x7f0e016d

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabView:I = 0x7f0e016a

.field public static final Base_Widget_AppCompat_Light_ActivityChooserView:I = 0x7f0e018e

.field public static final Base_Widget_AppCompat_Light_AutoCompleteTextView:I = 0x7f0e018c

.field public static final Base_Widget_AppCompat_Light_PopupMenu:I = 0x7f0e0183

.field public static final Base_Widget_AppCompat_Light_PopupMenu_Overflow:I = 0x7f0e0181

.field public static final Base_Widget_AppCompat_ListPopupWindow:I = 0x7f0e017f

.field public static final Base_Widget_AppCompat_ListView_DropDown:I = 0x7f0e017b

.field public static final Base_Widget_AppCompat_ListView_Menu:I = 0x7f0e017e

.field public static final Base_Widget_AppCompat_PopupMenu:I = 0x7f0e0182

.field public static final Base_Widget_AppCompat_PopupMenu_Overflow:I = 0x7f0e0180

.field public static final Base_Widget_AppCompat_PopupWindow:I = 0x7f0e018f

.field public static final Base_Widget_AppCompat_ProgressBar:I = 0x7f0e0177

.field public static final Base_Widget_AppCompat_ProgressBar_Horizontal:I = 0x7f0e0176

.field public static final Base_Widget_AppCompat_SearchView:I = 0x7f0e0194

.field public static final Base_Widget_AppCompat_Spinner:I = 0x7f0e0178

.field public static final Base_Widget_AppCompat_Spinner_DropDown_ActionBar:I = 0x7f0e0179

.field public static final Base_Widget_AppCompat_Toolbar:I = 0x7f0e0190

.field public static final Base_Widget_AppCompat_Toolbar_Button_Navigation:I = 0x7f0e0191

.field public static final BlackTitledDialog:I = 0x7f0e01f9

.field public static final Button:I = 0x7f0e00c8

.field public static final ButtonText:I = 0x7f0e00ce

.field public static final CakemixTheme:I = 0x7f0e00af

.field public static final CakemixTheme_Dialog:I = 0x7f0e00b0

.field public static final CakemixTheme_DialogWhenLarge:I = 0x7f0e00b1

.field public static final CakemixTheme_DocListActivity:I = 0x7f0e00b3

.field public static final CakemixTheme_NoTitle:I = 0x7f0e00b4

.field public static final CakemixTheme_Preferences:I = 0x7f0e00b5

.field public static final CakemixTheme_Projector:I = 0x7f0e00b6

.field public static final CakemixTheme_ProjectorDropDown:I = 0x7f0e00b2

.field public static final CakemixTheme_Punch:I = 0x7f0e00b7

.field public static final CakemixTheme_SlimTitle:I = 0x7f0e00b8

.field public static final CakemixTheme_Translucent:I = 0x7f0e00b9

.field public static final CakemixTheme_Translucent_TranslucentStatus:I = 0x7f0e00bb

.field public static final CakemixTheme_Translucent_WithTitle:I = 0x7f0e00ba

.field public static final CakemixTheme_Upload:I = 0x7f0e00bc

.field public static final CakemixTheme_WebView:I = 0x7f0e00bd

.field public static final CakemixTheme_Welcome:I = 0x7f0e00be

.field public static final CakemixWindowTitle:I = 0x7f0e004c

.field public static final CakemixWindowTitleBackground:I = 0x7f0e004b

.field public static final CakemixWindowTitleTextAppearance:I = 0x7f0e004d

.field public static final ChoiceRow:I = 0x7f0e00cc

.field public static final ChoiceText:I = 0x7f0e00c9

.field public static final ChoiceText_Item:I = 0x7f0e00ca

.field public static final ChoiceText_Title:I = 0x7f0e00cb

.field public static final CommonTheme:I = 0x7f0e01f0

.field public static final CreateBarItem:I = 0x7f0e004e

.field public static final CreateBarItemImage:I = 0x7f0e004f

.field public static final CreateBarItemText:I = 0x7f0e0050

.field public static final CreateSheet:I = 0x7f0e00da

.field public static final CreateSheetBase:I = 0x7f0e00d9

.field public static final DetailCard:I = 0x7f0e007b

.field public static final DetailCard_Header:I = 0x7f0e007c

.field public static final DetailFragment:I = 0x7f0e0043

.field public static final DetailFragmentBase:I = 0x7f0e0042

.field public static final DetailFragmentPinningSwitchText:I = 0x7f0e0089

.field public static final DetailFragmentSharingItem:I = 0x7f0e007d

.field public static final DetailFragmentSharingItemIcon:I = 0x7f0e007e

.field public static final DetailFragmentSharingItemSeparator:I = 0x7f0e0087

.field public static final DetailFragmentSharingItemSeparator_Thick:I = 0x7f0e0088

.field public static final DetailFragmentSharingItemText:I = 0x7f0e007f

.field public static final DetailFragmentSharingItemText_Bold:I = 0x7f0e0080

.field public static final DetailFragmentSharingItemText_Cards:I = 0x7f0e0084

.field public static final DetailFragmentSharingItemText_Cards_Bold:I = 0x7f0e0086

.field public static final DetailFragmentSharingItemText_Cards_Subdued:I = 0x7f0e0085

.field public static final DetailFragmentSharingItemText_Indent:I = 0x7f0e0082

.field public static final DetailFragmentSharingItemText_Indent_SectionTitle:I = 0x7f0e0083

.field public static final DetailFragmentSharingItemText_Subdued:I = 0x7f0e0081

.field public static final DetailFragment_ActionButton:I = 0x7f0e0045

.field public static final DetailFragment_Tablet:I = 0x7f0e0044

.field public static final DetailInfoCard_Row:I = 0x7f0e008a

.field public static final DetailInfoCard_Text:I = 0x7f0e008b

.field public static final DetailInfoCard_Text_Label:I = 0x7f0e008c

.field public static final DetailInfoCard_Text_Value:I = 0x7f0e008d

.field public static final DetailPanel_Title:I = 0x7f0e0046

.field public static final DialogButton:I = 0x7f0e00d1

.field public static final DialogButton_Affirmative:I = 0x7f0e00d2

.field public static final DialogTitleBlack:I = 0x7f0e01fa

.field public static final Divider:I = 0x7f0e00d3

.field public static final DocListEntryButton:I = 0x7f0e0094

.field public static final DocListEntryButton_Tablet:I = 0x7f0e00ae

.field public static final DocListEntryGroupColumnTitle:I = 0x7f0e0052

.field public static final DocListEntryGroupTitle:I = 0x7f0e0051

.field public static final DocListHorizontalRule:I = 0x7f0e0079

.field public static final DocListHorizontalRule_Tablet:I = 0x7f0e007a

.field public static final DocListView:I = 0x7f0e0078

.field public static final DragKnobNeighborBelow:I = 0x7f0e005b

.field public static final EntryButton:I = 0x7f0e0053

.field public static final EntryButton_Grid:I = 0x7f0e0055

.field public static final EntryButton_List:I = 0x7f0e0054

.field public static final EntryIcon:I = 0x7f0e0056

.field public static final EntryLabel:I = 0x7f0e0057

.field public static final EntryProgress:I = 0x7f0e0058

.field public static final EntryProgress_Grid:I = 0x7f0e005a

.field public static final EntryProgress_List:I = 0x7f0e0059

.field public static final FauxActionBar:I = 0x7f0e00d4

.field public static final FauxActionBar_TintIcon:I = 0x7f0e00d6

.field public static final FauxActionBar_Title:I = 0x7f0e00d5

.field public static final GridFolder:I = 0x7f0e00f2

.field public static final GridFooter:I = 0x7f0e00f0

.field public static final GridFooter_TwoLine:I = 0x7f0e00f1

.field public static final GridText:I = 0x7f0e00f3

.field public static final GridText_Even:I = 0x7f0e00f7

.field public static final GridText_Primary:I = 0x7f0e00f4

.field public static final GridText_Primary_Multiline:I = 0x7f0e00f5

.field public static final GridText_Secondary:I = 0x7f0e00f6

.field public static final GridTile:I = 0x7f0e00ef

.field public static final HeadlineText:I = 0x7f0e00d0

.field public static final HelpCardButton:I = 0x7f0e005c

.field public static final HelpCardButton_Primary:I = 0x7f0e005d

.field public static final HelpCardButton_Secondary:I = 0x7f0e005e

.field public static final HelpCardContent:I = 0x7f0e005f

.field public static final HelpCardContentSeparator:I = 0x7f0e0062

.field public static final HelpCardContent_Banner:I = 0x7f0e0060

.field public static final HelpCardOuterFrame:I = 0x7f0e0061

.field public static final HelpCardShadower:I = 0x7f0e0063

.field public static final HoloLikeButton:I = 0x7f0e0098

.field public static final HoloLikeButtonBar:I = 0x7f0e0096

.field public static final HoloLikeButtonBarDivider:I = 0x7f0e0097

.field public static final HomeScreen:I = 0x7f0e0047

.field public static final HomeScreen_AccountSwitcher:I = 0x7f0e0048

.field public static final HomeScreen_IconEntry:I = 0x7f0e0049

.field public static final HomeScreen_TableRow:I = 0x7f0e004a

.field public static final HorizontalPadder:I = 0x7f0e00c7

.field public static final InternalReleaseDialogButton:I = 0x7f0e0092

.field public static final Label:I = 0x7f0e01f7

.field public static final List:I = 0x7f0e00db

.field public static final ListAvatar:I = 0x7f0e00e4

.field public static final ListEntryTitle:I = 0x7f0e0064

.field public static final ListEntryTitle_DocList:I = 0x7f0e0065

.field public static final ListEntryTitle_DocList_Relative:I = 0x7f0e0066

.field public static final ListIcon:I = 0x7f0e00e1

.field public static final ListIcon_Left:I = 0x7f0e00e2

.field public static final ListIcon_Left_Round:I = 0x7f0e00e3

.field public static final ListIcon_Right:I = 0x7f0e00e5

.field public static final ListRow:I = 0x7f0e00dc

.field public static final ListRowTextLinearLayout:I = 0x7f0e00e0

.field public static final ListRow_SingleLine:I = 0x7f0e00dd

.field public static final ListRow_SingleLine_WithAvatar:I = 0x7f0e00de

.field public static final ListRow_TwoLine:I = 0x7f0e00df

.field public static final ListText:I = 0x7f0e00e6

.field public static final ListTextLinearLayout:I = 0x7f0e00ee

.field public static final ListText_Primary:I = 0x7f0e00e7

.field public static final ListText_Primary_AboveSecondary:I = 0x7f0e00ea

.field public static final ListText_Primary_Blue:I = 0x7f0e00e8

.field public static final ListText_Primary_MultiLine:I = 0x7f0e00e9

.field public static final ListText_Primary_SkipIcon:I = 0x7f0e00eb

.field public static final ListText_Secondary:I = 0x7f0e00ec

.field public static final ListText_Secondary_SkipIcon:I = 0x7f0e00ed

.field public static final LoadingDialog:I = 0x7f0e00c2

.field public static final MigrationModalScreen:I = 0x7f0e0077

.field public static final NavigationBreadcrumbItem:I = 0x7f0e00ab

.field public static final NavigationBreadcrumbItemBase:I = 0x7f0e00aa

.field public static final NavigationBreadcrumbItem_Landscape:I = 0x7f0e00ac

.field public static final NavigationBreadcrumbItem_Tablet:I = 0x7f0e00ad

.field public static final NavigationPane:I = 0x7f0e003e

.field public static final NavigationPane_ItemImage:I = 0x7f0e0040

.field public static final NavigationPane_ItemImageRight:I = 0x7f0e0041

.field public static final NavigationPane_ItemText:I = 0x7f0e003f

.field public static final NotificationBackground:I = 0x7f0e006d

.field public static final OpaqueTheme:I = 0x7f0e01ec

.field public static final OperationDialogButton:I = 0x7f0e0091

.field public static final OperationDialogText:I = 0x7f0e009a

.field public static final OperationDialogText_Tablet:I = 0x7f0e009b

.field public static final OverlayActionBarCloseButtonStyle:I = 0x7f0e01f4

.field public static final OverlayActionBarOverflowStyle:I = 0x7f0e01f2

.field public static final OverlayActionBarStyle:I = 0x7f0e01f1

.field public static final OverlayActionBarStyleLight:I = 0x7f0e01f3

.field public static final PageIndicatorStyle:I = 0x7f0e01f6

.field public static final Platform_AppCompat:I = 0x7f0e01ca

.field public static final Platform_AppCompat_Dialog:I = 0x7f0e01cc

.field public static final Platform_AppCompat_Light:I = 0x7f0e01cb

.field public static final Platform_AppCompat_Light_Dialog:I = 0x7f0e01cd

.field public static final PreLDialogButtonBar:I = 0x7f0e0031

.field public static final PreLDialogTextAppearance:I = 0x7f0e0032

.field public static final PreLDialogTitleStyle:I = 0x7f0e0033

.field public static final PreviewOpenInAnotherApp_container:I = 0x7f0e009e

.field public static final PreviewOpenInAnotherApp_image:I = 0x7f0e009d

.field public static final PreviewOpenInAnotherApp_text:I = 0x7f0e009c

.field public static final PrintTheme:I = 0x7f0e01ee

.field public static final ProgressBar:I = 0x7f0e00d7

.field public static final ProgressBarIndeterminateHorizontal:I = 0x7f0e00a8

.field public static final ProgressBarSpinner:I = 0x7f0e00a9

.field public static final ProgressBar_Horizontal:I = 0x7f0e00d8

.field public static final ProjectorActionBar:I = 0x7f0e003c

.field public static final ProjectorActionBarText:I = 0x7f0e003d

.field public static final ProjectorSeekBar:I = 0x7f0e00c3

.field public static final ProjectorSeekTime:I = 0x7f0e00c4

.field public static final PunchCentralSlideFrame:I = 0x7f0e0070

.field public static final PunchDividerRule:I = 0x7f0e0072

.field public static final RecentEventCard:I = 0x7f0e008e

.field public static final RecentEventExpandButton:I = 0x7f0e0090

.field public static final RecentEventText:I = 0x7f0e008f

.field public static final RtlOverlay_Widget_AppCompat_ActionBar_TitleItem:I = 0x7f0e01b5

.field public static final RtlOverlay_Widget_AppCompat_ActionButton_CloseMode:I = 0x7f0e01b6

.field public static final RtlOverlay_Widget_AppCompat_ActionButton_Overflow:I = 0x7f0e01b7

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem:I = 0x7f0e01b8

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem_InternalGroup:I = 0x7f0e01b9

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem_Text:I = 0x7f0e01ba

.field public static final RtlOverlay_Widget_AppCompat_SearchView_MagIcon:I = 0x7f0e01af

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown:I = 0x7f0e01b0

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Icon1:I = 0x7f0e01b2

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Icon2:I = 0x7f0e01b3

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Query:I = 0x7f0e01b1

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Text:I = 0x7f0e01b4

.field public static final Scrub:I = 0x7f0e00a3

.field public static final Scrub_Callout:I = 0x7f0e00a4

.field public static final Scrub_Text:I = 0x7f0e00a5

.field public static final Scrub_Text_Chapter:I = 0x7f0e00a6

.field public static final Scrub_Text_Page:I = 0x7f0e00a7

.field public static final SearchSuggestionDropDown:I = 0x7f0e00fa

.field public static final SearchTextView:I = 0x7f0e00f9

.field public static final SearchTheme:I = 0x7f0e00f8

.field public static final SelectionFloatingHandleSelectionText:I = 0x7f0e00a0

.field public static final SelectionFloatingHandleStyle:I = 0x7f0e009f

.field public static final SingleLine:I = 0x7f0e0093

.field public static final Snackbar:I = 0x7f0e00fe

.field public static final SnackbarText:I = 0x7f0e00ff

.field public static final SnackbarText_Action:I = 0x7f0e0100

.field public static final SpeakerNotePresence:I = 0x7f0e0095

.field public static final TextAppearanceMedium:I = 0x7f0e0099

.field public static final TextAppearancePrimary:I = 0x7f0e00c5

.field public static final TextAppearanceSecondary:I = 0x7f0e00c6

.field public static final TextAppearance_AppCompat:I = 0x7f0e0139

.field public static final TextAppearance_AppCompat_Body1:I = 0x7f0e0144

.field public static final TextAppearance_AppCompat_Body2:I = 0x7f0e0143

.field public static final TextAppearance_AppCompat_Button:I = 0x7f0e014e

.field public static final TextAppearance_AppCompat_Caption:I = 0x7f0e0145

.field public static final TextAppearance_AppCompat_Display1:I = 0x7f0e013d

.field public static final TextAppearance_AppCompat_Display2:I = 0x7f0e013c

.field public static final TextAppearance_AppCompat_Display3:I = 0x7f0e013b

.field public static final TextAppearance_AppCompat_Display4:I = 0x7f0e013a

.field public static final TextAppearance_AppCompat_Headline:I = 0x7f0e013e

.field public static final TextAppearance_AppCompat_Inverse:I = 0x7f0e0147

.field public static final TextAppearance_AppCompat_Large:I = 0x7f0e0148

.field public static final TextAppearance_AppCompat_Large_Inverse:I = 0x7f0e0149

.field public static final TextAppearance_AppCompat_Light_SearchResult_Subtitle:I = 0x7f0e0154

.field public static final TextAppearance_AppCompat_Light_SearchResult_Title:I = 0x7f0e0153

.field public static final TextAppearance_AppCompat_Light_Widget_PopupMenu_Large:I = 0x7f0e012a

.field public static final TextAppearance_AppCompat_Light_Widget_PopupMenu_Small:I = 0x7f0e012b

.field public static final TextAppearance_AppCompat_Medium:I = 0x7f0e014a

.field public static final TextAppearance_AppCompat_Medium_Inverse:I = 0x7f0e014b

.field public static final TextAppearance_AppCompat_Menu:I = 0x7f0e0146

.field public static final TextAppearance_AppCompat_SearchResult_Subtitle:I = 0x7f0e012d

.field public static final TextAppearance_AppCompat_SearchResult_Title:I = 0x7f0e012c

.field public static final TextAppearance_AppCompat_Small:I = 0x7f0e014c

.field public static final TextAppearance_AppCompat_Small_Inverse:I = 0x7f0e014d

.field public static final TextAppearance_AppCompat_Subhead:I = 0x7f0e0141

.field public static final TextAppearance_AppCompat_Subhead_Inverse:I = 0x7f0e0142

.field public static final TextAppearance_AppCompat_Title:I = 0x7f0e013f

.field public static final TextAppearance_AppCompat_Title_Inverse:I = 0x7f0e0140

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Menu:I = 0x7f0e0116

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Subtitle:I = 0x7f0e0106

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse:I = 0x7f0e0108

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Title:I = 0x7f0e0105

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse:I = 0x7f0e0107

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Subtitle:I = 0x7f0e0119

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Subtitle_Inverse:I = 0x7f0e0157

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Title:I = 0x7f0e0118

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Title_Inverse:I = 0x7f0e0156

.field public static final TextAppearance_AppCompat_Widget_DropDownItem:I = 0x7f0e011a

.field public static final TextAppearance_AppCompat_Widget_PopupMenu_Large:I = 0x7f0e0128

.field public static final TextAppearance_AppCompat_Widget_PopupMenu_Small:I = 0x7f0e0129

.field public static final TextAppearance_AppCompat_Widget_Switch:I = 0x7f0e014f

.field public static final TextAppearance_Widget_AppCompat_ExpandedMenu_Item:I = 0x7f0e0120

.field public static final TextAppearance_Widget_AppCompat_Toolbar_Subtitle:I = 0x7f0e0138

.field public static final TextAppearance_Widget_AppCompat_Toolbar_Title:I = 0x7f0e0137

.field public static final TextField:I = 0x7f0e01f8

.field public static final ThemeOverlay_AppCompat:I = 0x7f0e01c5

.field public static final ThemeOverlay_AppCompat_ActionBar:I = 0x7f0e01c8

.field public static final ThemeOverlay_AppCompat_Dark:I = 0x7f0e01c7

.field public static final ThemeOverlay_AppCompat_Dark_ActionBar:I = 0x7f0e01c9

.field public static final ThemeOverlay_AppCompat_Light:I = 0x7f0e01c6

.field public static final Theme_AppCompat:I = 0x7f0e01bb

.field public static final Theme_AppCompat_CompactMenu:I = 0x7f0e01c4

.field public static final Theme_AppCompat_Dialog:I = 0x7f0e01c2

.field public static final Theme_AppCompat_DialogWhenLarge:I = 0x7f0e01c0

.field public static final Theme_AppCompat_Light:I = 0x7f0e01bc

.field public static final Theme_AppCompat_Light_DarkActionBar:I = 0x7f0e01bd

.field public static final Theme_AppCompat_Light_Dialog:I = 0x7f0e01c3

.field public static final Theme_AppCompat_Light_DialogWhenLarge:I = 0x7f0e01c1

.field public static final Theme_AppCompat_Light_NoActionBar:I = 0x7f0e01bf

.field public static final Theme_AppCompat_NoActionBar:I = 0x7f0e01be

.field public static final Theme_IAPTheme:I = 0x7f0e0012

.field public static final Theme_MediaRouter:I = 0x7f0e0010

.field public static final Theme_MediaRouter_Light:I = 0x7f0e0011

.field public static final ThumbnailText:I = 0x7f0e00cd

.field public static final TintableIcon:I = 0x7f0e00fb

.field public static final TintableIcon_Secondary:I = 0x7f0e00fc

.field public static final TintableIcon_Tertiary:I = 0x7f0e00fd

.field public static final TitleText:I = 0x7f0e00cf

.field public static final TranslucentTheme:I = 0x7f0e01ed

.field public static final UndoHandleActionText:I = 0x7f0e00a2

.field public static final UndoHandleMessageText:I = 0x7f0e00a1

.field public static final Upload:I = 0x7f0e0068

.field public static final Upload_Button_Spinner:I = 0x7f0e0069

.field public static final Upload_Text:I = 0x7f0e006a

.field public static final Upload_Text_SectionHeader:I = 0x7f0e006c

.field public static final Upload_Text_SectionHeaderBase:I = 0x7f0e006b

.field public static final UserFeedback:I = 0x7f0e00bf

.field public static final UserFeedback_Tablet:I = 0x7f0e00c0

.field public static final VersionTheme:I = 0x7f0e01ef

.field public static final WalletFragmentDefaultButtonTextAppearance:I = 0x7f0e0015

.field public static final WalletFragmentDefaultDetailsHeaderTextAppearance:I = 0x7f0e0014

.field public static final WalletFragmentDefaultDetailsTextAppearance:I = 0x7f0e0013

.field public static final WalletFragmentDefaultStyle:I = 0x7f0e0016

.field public static final WebViewTheme:I = 0x7f0e00c1

.field public static final WelcomeContentAppearance:I = 0x7f0e01fc

.field public static final WelcomeHeadingAppearance:I = 0x7f0e01fb

.field public static final WelcomeImage:I = 0x7f0e01fd

.field public static final WelcomeImage_1:I = 0x7f0e01fe

.field public static final WelcomeImage_2:I = 0x7f0e01ff

.field public static final WelcomeImage_3:I = 0x7f0e0200

.field public static final WelcomeTextButtonStyle:I = 0x7f0e002a

.field public static final WelcomeTheme:I = 0x7f0e0029

.field public static final WelcomeThemeBase:I = 0x7f0e0028

.field public static final WhiteTitleText:I = 0x7f0e01f5

.field public static final Widget:I = 0x7f0e0201

.field public static final Widget_Action:I = 0x7f0e0202

.field public static final Widget_Action_Center:I = 0x7f0e0203

.field public static final Widget_Action_Left:I = 0x7f0e0204

.field public static final Widget_Action_Right:I = 0x7f0e0205

.field public static final Widget_AppCompat_ActionBar:I = 0x7f0e0101

.field public static final Widget_AppCompat_ActionBar_Solid:I = 0x7f0e0103

.field public static final Widget_AppCompat_ActionBar_TabBar:I = 0x7f0e010e

.field public static final Widget_AppCompat_ActionBar_TabText:I = 0x7f0e0112

.field public static final Widget_AppCompat_ActionBar_TabView:I = 0x7f0e0110

.field public static final Widget_AppCompat_ActionButton:I = 0x7f0e010b

.field public static final Widget_AppCompat_ActionButton_CloseMode:I = 0x7f0e010c

.field public static final Widget_AppCompat_ActionButton_Overflow:I = 0x7f0e010d

.field public static final Widget_AppCompat_ActionMode:I = 0x7f0e0117

.field public static final Widget_AppCompat_ActivityChooserView:I = 0x7f0e0130

.field public static final Widget_AppCompat_AutoCompleteTextView:I = 0x7f0e012e

.field public static final Widget_AppCompat_CompoundButton_Switch:I = 0x7f0e0134

.field public static final Widget_AppCompat_DrawerArrowToggle:I = 0x7f0e0113

.field public static final Widget_AppCompat_DropDownItem_Spinner:I = 0x7f0e011e

.field public static final Widget_AppCompat_EditText:I = 0x7f0e0133

.field public static final Widget_AppCompat_Light_ActionBar:I = 0x7f0e0102

.field public static final Widget_AppCompat_Light_ActionBar_Solid:I = 0x7f0e0104

.field public static final Widget_AppCompat_Light_ActionBar_Solid_Inverse:I = 0x7f0e0150

.field public static final Widget_AppCompat_Light_ActionBar_TabBar:I = 0x7f0e010f

.field public static final Widget_AppCompat_Light_ActionBar_TabBar_Inverse:I = 0x7f0e0151

.field public static final Widget_AppCompat_Light_ActionBar_TabText:I = 0x7f0e0114

.field public static final Widget_AppCompat_Light_ActionBar_TabText_Inverse:I = 0x7f0e0115

.field public static final Widget_AppCompat_Light_ActionBar_TabView:I = 0x7f0e0111

.field public static final Widget_AppCompat_Light_ActionBar_TabView_Inverse:I = 0x7f0e0152

.field public static final Widget_AppCompat_Light_ActionButton:I = 0x7f0e015a

.field public static final Widget_AppCompat_Light_ActionButton_CloseMode:I = 0x7f0e015c

.field public static final Widget_AppCompat_Light_ActionButton_Overflow:I = 0x7f0e015b

.field public static final Widget_AppCompat_Light_ActionMode_Inverse:I = 0x7f0e0155

.field public static final Widget_AppCompat_Light_ActivityChooserView:I = 0x7f0e0131

.field public static final Widget_AppCompat_Light_AutoCompleteTextView:I = 0x7f0e012f

.field public static final Widget_AppCompat_Light_DropDownItem_Spinner:I = 0x7f0e0158

.field public static final Widget_AppCompat_Light_ListPopupWindow:I = 0x7f0e015f

.field public static final Widget_AppCompat_Light_ListView_DropDown:I = 0x7f0e015e

.field public static final Widget_AppCompat_Light_PopupMenu:I = 0x7f0e0125

.field public static final Widget_AppCompat_Light_PopupMenu_Overflow:I = 0x7f0e0123

.field public static final Widget_AppCompat_Light_SearchView:I = 0x7f0e0159

.field public static final Widget_AppCompat_Light_Spinner_DropDown_ActionBar:I = 0x7f0e015d

.field public static final Widget_AppCompat_ListPopupWindow:I = 0x7f0e0121

.field public static final Widget_AppCompat_ListView_DropDown:I = 0x7f0e011f

.field public static final Widget_AppCompat_ListView_Menu:I = 0x7f0e0126

.field public static final Widget_AppCompat_PopupMenu:I = 0x7f0e0124

.field public static final Widget_AppCompat_PopupMenu_Overflow:I = 0x7f0e0122

.field public static final Widget_AppCompat_PopupWindow:I = 0x7f0e0127

.field public static final Widget_AppCompat_ProgressBar:I = 0x7f0e010a

.field public static final Widget_AppCompat_ProgressBar_Horizontal:I = 0x7f0e0109

.field public static final Widget_AppCompat_SearchView:I = 0x7f0e0132

.field public static final Widget_AppCompat_Spinner:I = 0x7f0e011b

.field public static final Widget_AppCompat_Spinner_DropDown:I = 0x7f0e011c

.field public static final Widget_AppCompat_Spinner_DropDown_ActionBar:I = 0x7f0e011d

.field public static final Widget_AppCompat_Toolbar:I = 0x7f0e0135

.field public static final Widget_AppCompat_Toolbar_Button_Navigation:I = 0x7f0e0136

.field public static final Widget_Container:I = 0x7f0e0206

.field public static final Widget_Container_BrokenTitle:I = 0x7f0e0207

.field public static final Widget_Logo:I = 0x7f0e0208

.field public static final Widget_MediaRouter_Light_MediaRouteButton:I = 0x7f0e000f

.field public static final Widget_MediaRouter_MediaRouteButton:I = 0x7f0e000e

.field public static final Widget_Text:I = 0x7f0e0209

.field public static final Widget_Text_AccountName:I = 0x7f0e020a

.field public static final Widget_Text_AppTitle:I = 0x7f0e020b

.field public static final Widget_Text_AppTitle_Only:I = 0x7f0e020c

.field public static final WrapJustWidthInLandAndViceVersa:I = 0x7f0e0067

.field public static final ds_action_bar:I = 0x7f0e0027

.field public static final ds_action_bar_compat:I = 0x7f0e0018

.field public static final ds_action_bar_compat_home_item:I = 0x7f0e001d

.field public static final ds_action_bar_compat_item:I = 0x7f0e001c

.field public static final ds_action_bar_compat_item_base:I = 0x7f0e0019

.field public static final ds_action_bar_compat_title:I = 0x7f0e001b

.field public static final ds_action_bar_compat_title_base:I = 0x7f0e001a

.field public static final ds_compat_app_theme:I = 0x7f0e0017

.field public static final ds_editor_control_icon:I = 0x7f0e0022

.field public static final ds_overflow:I = 0x7f0e0026

.field public static final ds_progress_bar_horizontal:I = 0x7f0e0024

.field public static final ds_progress_bar_spinner:I = 0x7f0e0023

.field public static final ds_rename_dialog_button:I = 0x7f0e0025

.field public static final ds_scanner_base_theme:I = 0x7f0e001e

.field public static final ds_scanner_capture_theme:I = 0x7f0e001f

.field public static final ds_scanner_editor_theme:I = 0x7f0e0020

.field public static final ds_scanner_preferences_theme:I = 0x7f0e0021

.field public static final navigation_horizontal_rule:I = 0x7f0e006f

.field public static final navigation_pane_text:I = 0x7f0e006e

.field public static final punch_status_text:I = 0x7f0e0071

.field public static final tablet_hint_description:I = 0x7f0e0076

.field public static final tablet_hint_dialog:I = 0x7f0e0073

.field public static final tablet_hint_dialog_animation:I = 0x7f0e0074

.field public static final tablet_hint_title:I = 0x7f0e0075


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8591
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
