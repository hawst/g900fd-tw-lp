.class public LacV;
.super Ljava/lang/Object;
.source "ContentExposer.java"

# interfaces
.implements LadA;


# annotations
.annotation runtime Lbxz;
.end annotation


# static fields
.field private static a:Landroid/net/Uri;


# instance fields
.field private final a:LaGM;

.field private final a:Ladf;

.field private final a:Ladi;

.field private final a:LtK;


# direct methods
.method public constructor <init>(LaGM;Ladf;Ladi;LtK;)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    iput-object p1, p0, LacV;->a:LaGM;

    .line 149
    iput-object p2, p0, LacV;->a:Ladf;

    .line 150
    iput-object p3, p0, LacV;->a:Ladi;

    .line 151
    iput-object p4, p0, LacV;->a:LtK;

    .line 152
    return-void
.end method

.method public static a(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 141
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v0, LacV;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)LadC;
    .locals 6

    .prologue
    .line 160
    const-string v0, "ContentExposer"

    const-string v1, "Open ExposedFile with path %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 163
    :try_start_0
    iget-object v0, p0, LacV;->a:Ladf;

    invoke-virtual {v0, p1}, Ladf;->a(Ljava/lang/String;)LaGp;
    :try_end_0
    .catch Lade; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 170
    iget-object v0, p0, LacV;->a:LaGM;

    invoke-interface {v0, v1}, LaGM;->b(LaGp;)LaGo;

    move-result-object v2

    .line 171
    new-instance v0, LacX;

    iget-object v3, p0, LacV;->a:Ladi;

    iget-object v4, p0, LacV;->a:LtK;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LacX;-><init>(LaGp;LaGo;Ladi;LtK;LacW;)V

    return-object v0

    .line 164
    :catch_0
    move-exception v0

    .line 165
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-virtual {v0}, Lade;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 166
    :catch_1
    move-exception v0

    .line 167
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-virtual {v0}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public a()V
    .locals 1

    .prologue
    .line 155
    const-string v0, "exposed_content"

    invoke-static {v0, p0}, Lcom/google/android/apps/docs/sync/filemanager/FileProvider;->a(Ljava/lang/String;LadA;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LacV;->a:Landroid/net/Uri;

    .line 156
    return-void
.end method
