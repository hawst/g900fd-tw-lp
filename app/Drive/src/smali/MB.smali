.class LMB;
.super Ljava/lang/Object;
.source "DocListViewSnapshotter.java"


# instance fields
.field final synthetic a:LMv;

.field private a:Landroid/view/Surface;


# direct methods
.method public constructor <init>(LMv;Landroid/view/TextureView;)V
    .locals 1

    .prologue
    .line 97
    iput-object p1, p0, LMB;->a:LMv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    new-instance v0, LMC;

    invoke-direct {v0, p0, p1}, LMC;-><init>(LMB;LMv;)V

    invoke-virtual {p2, v0}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 118
    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, LMB;->a:LMv;

    invoke-static {v0}, LMv;->a(LMv;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LMB;->a:LMv;

    invoke-static {v0}, LMv;->b(LMv;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    invoke-virtual {p0}, LMB;->b()V

    goto :goto_0
.end method

.method a(Landroid/graphics/SurfaceTexture;)V
    .locals 2

    .prologue
    .line 128
    iget-object v1, p0, LMB;->a:LMv;

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, LMv;->b(LMv;Z)Z

    .line 129
    iget-object v0, p0, LMB;->a:Landroid/view/Surface;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, LMB;->a:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 131
    const/4 v0, 0x0

    iput-object v0, p0, LMB;->a:Landroid/view/Surface;

    .line 133
    :cond_0
    if-eqz p1, :cond_1

    .line 134
    new-instance v0, Landroid/view/Surface;

    invoke-direct {v0, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, LMB;->a:Landroid/view/Surface;

    .line 136
    :cond_1
    invoke-virtual {p0}, LMB;->a()V

    .line 137
    return-void

    .line 128
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()V
    .locals 3

    .prologue
    .line 140
    iget-object v0, p0, LMB;->a:Landroid/view/Surface;

    if-eqz v0, :cond_0

    iget-object v0, p0, LMB;->a:LMv;

    invoke-static {v0}, LMv;->a(LMv;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 143
    :cond_1
    const-string v0, "DocListViewSnapshotter"

    const-string v1, "Updating snapshot"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 144
    iget-object v0, p0, LMB;->a:Landroid/view/Surface;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/Surface;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    move-result-object v0

    .line 145
    iget-object v1, p0, LMB;->a:LMv;

    invoke-static {v1}, LMv;->a(LMv;)Lcom/google/android/apps/docs/view/DocListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/view/DocListView;->draw(Landroid/graphics/Canvas;)V

    .line 146
    iget-object v1, p0, LMB;->a:Landroid/view/Surface;

    invoke-virtual {v1, v0}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method
