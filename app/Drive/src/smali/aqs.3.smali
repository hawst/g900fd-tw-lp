.class public Laqs;
.super Ljava/lang/Object;
.source "MergedListAdapter.java"

# interfaces
.implements Landroid/widget/ListAdapter;


# instance fields
.field private a:I

.field private final a:Landroid/database/DataSetObservable;

.field private final a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "Landroid/widget/ListAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Laqu;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private b:I

.field private b:Z

.field private c:Z


# direct methods
.method public constructor <init>(LbmF;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "Landroid/widget/ListAdapter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Laqs;->a:Landroid/database/DataSetObservable;

    .line 26
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Laqs;->a:Ljava/util/TreeMap;

    .line 37
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbmF;

    iput-object v0, p0, Laqs;->a:LbmF;

    .line 38
    new-instance v1, Laqv;

    const/4 v0, 0x0

    invoke-direct {v1, p0, v0}, Laqv;-><init>(Laqs;Laqt;)V

    .line 39
    invoke-virtual {p1}, LbmF;->a()Lbqv;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    .line 40
    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_0

    .line 42
    :cond_0
    invoke-direct {p0}, Laqs;->a()V

    .line 43
    return-void
.end method

.method static synthetic a(Laqs;)Landroid/database/DataSetObservable;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Laqs;->a:Landroid/database/DataSetObservable;

    return-object v0
.end method

.method private a(Ljava/util/Map$Entry;)Landroid/widget/ListAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Integer;",
            "Laqu;",
            ">;)",
            "Landroid/widget/ListAdapter;"
        }
    .end annotation

    .prologue
    .line 123
    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laqu;

    iget-object v0, v0, Laqu;->a:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method private a()V
    .locals 14

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x1

    .line 46
    .line 51
    new-instance v8, Ljava/util/TreeMap;

    invoke-direct {v8}, Ljava/util/TreeMap;-><init>()V

    .line 54
    iget-object v0, p0, Laqs;->a:LbmF;

    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v9

    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v6

    move v7, v6

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    .line 55
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v10

    .line 56
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v11

    .line 57
    if-lez v10, :cond_0

    .line 58
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    new-instance v13, Laqu;

    invoke-direct {v13, v0, v5}, Laqu;-><init>(Landroid/widget/ListAdapter;I)V

    invoke-virtual {v8, v12, v13}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    add-int/2addr v7, v10

    .line 62
    :cond_0
    add-int/2addr v5, v11

    .line 63
    if-eqz v4, :cond_1

    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v4

    if-eqz v4, :cond_1

    move v4, v2

    .line 64
    :goto_1
    if-eqz v3, :cond_2

    invoke-interface {v0}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v2

    .line 65
    :goto_2
    if-eqz v1, :cond_3

    invoke-interface {v0}, Landroid/widget/ListAdapter;->areAllItemsEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    :goto_3
    move v1, v0

    .line 66
    goto :goto_0

    :cond_1
    move v4, v6

    .line 63
    goto :goto_1

    :cond_2
    move v3, v6

    .line 64
    goto :goto_2

    :cond_3
    move v0, v6

    .line 65
    goto :goto_3

    .line 67
    :cond_4
    iput v7, p0, Laqs;->a:I

    .line 68
    iput v5, p0, Laqs;->b:I

    .line 69
    iput-boolean v4, p0, Laqs;->a:Z

    .line 70
    iput-boolean v3, p0, Laqs;->b:Z

    .line 71
    iput-boolean v1, p0, Laqs;->c:Z

    .line 72
    iput-object v8, p0, Laqs;->a:Ljava/util/TreeMap;

    .line 73
    return-void
.end method

.method static synthetic a(Laqs;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Laqs;->a()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Laqs;->a:LbmF;

    invoke-virtual {v0}, LbmF;->size()I

    move-result v0

    return v0
.end method

.method public a(I)I
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Laqs;->a:Ljava/util/TreeMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 113
    invoke-direct {p0, v0}, Laqs;->a(Ljava/util/Map$Entry;)Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v0, p1, v0

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    return v0
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Laqs;->c:Z

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Laqs;->a:I

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Laqs;->a:Ljava/util/TreeMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 88
    invoke-direct {p0, v0}, Laqs;->a(Ljava/util/Map$Entry;)Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v0, p1, v0

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Laqs;->a:Ljava/util/TreeMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 94
    invoke-direct {p0, v0}, Laqs;->a(Ljava/util/Map$Entry;)Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v0, p1, v0

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 4

    .prologue
    .line 105
    iget-object v0, p0, Laqs;->a:Ljava/util/TreeMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v1

    .line 106
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laqu;

    .line 107
    iget v2, v0, Laqu;->a:I

    .line 108
    invoke-direct {p0, v1}, Laqs;->a(Ljava/util/Map$Entry;)Landroid/widget/ListAdapter;

    move-result-object v3

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v0, p1, v0

    invoke-interface {v3, v0}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    add-int/2addr v0, v2

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Laqs;->a:Ljava/util/TreeMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 100
    invoke-direct {p0, v0}, Laqs;->a(Ljava/util/Map$Entry;)Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v0, p1, v0

    invoke-interface {v1, v0, p2, p3}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Laqs;->b:I

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 138
    iget-boolean v0, p0, Laqs;->a:Z

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Laqs;->b:Z

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Laqs;->a:Ljava/util/TreeMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 119
    invoke-direct {p0, v0}, Laqs;->a(Ljava/util/Map$Entry;)Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v0, p1, v0

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    return v0
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Laqs;->a:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    .line 78
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Laqs;->a:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    .line 83
    return-void
.end method
