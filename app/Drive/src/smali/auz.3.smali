.class Lauz;
.super Landroid/os/AsyncTask;
.source "SystemPrintDialog.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/os/CancellationSignal;

.field final synthetic a:Landroid/os/ParcelFileDescriptor;

.field final synthetic a:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

.field final synthetic a:Lauy;


# direct methods
.method constructor <init>(Lauy;Landroid/os/CancellationSignal;Landroid/os/ParcelFileDescriptor;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V
    .locals 2

    .prologue
    .line 98
    iput-object p1, p0, Lauz;->a:Lauy;

    iput-object p2, p0, Lauz;->a:Landroid/os/CancellationSignal;

    iput-object p3, p0, Lauz;->a:Landroid/os/ParcelFileDescriptor;

    iput-object p4, p0, Lauz;->a:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 102
    iget-object v0, p0, Lauz;->a:Landroid/os/CancellationSignal;

    new-instance v1, LauA;

    invoke-direct {v1, p0}, LauA;-><init>(Lauz;)V

    invoke-virtual {v0, v1}, Landroid/os/CancellationSignal;->setOnCancelListener(Landroid/os/CancellationSignal$OnCancelListener;)V

    .line 108
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4

    .prologue
    .line 112
    .line 114
    :try_start_0
    iget-object v0, p0, Lauz;->a:Lauy;

    iget-object v0, v0, Lauy;->a:Lauw;

    invoke-static {v0}, Lauw;->a(Lauw;)Lavl;

    move-result-object v0

    iget-object v1, p0, Lauz;->a:Lauy;

    invoke-static {v1}, Lauy;->a(Lauy;)Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lavl;->a(Lcom/google/android/apps/viewer/client/AuthenticatedUri;)Lavk;

    move-result-object v0

    .line 115
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v2, p0, Lauz;->a:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 116
    invoke-virtual {v0}, Lavk;->a()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 117
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 118
    iget-object v0, p0, Lauz;->a:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/print/PageRange;

    const/4 v2, 0x0

    sget-object v3, Landroid/print/PageRange;->ALL_PAGES:Landroid/print/PageRange;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteFinished([Landroid/print/PageRange;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 119
    :catch_0
    move-exception v0

    .line 120
    const-string v1, "PrintActivity"

    const-string v2, "Printing document failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 121
    iget-object v1, p0, Lauz;->a:Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteFailed(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 98
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lauz;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
