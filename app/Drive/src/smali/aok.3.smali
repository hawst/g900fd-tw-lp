.class public Laok;
.super LanC;
.source "DownloadFetcher.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LanC",
        "<",
        "Lcom/google/android/apps/docs/utils/FetchSpec;",
        "Landroid/net/Uri;",
        "LakD",
        "<",
        "Ljava/io/File;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LTl;

.field private final a:LZS;

.field private final a:Laeb;

.field private final a:Lalo;

.field private final a:Laoj;


# direct methods
.method private constructor <init>(Laoj;LTl;Lalo;Laeb;Laoo;Laop;LZS;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoj;",
            "LTl;",
            "Lalo;",
            "Laeb;",
            "Laoo",
            "<",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            "Landroid/net/Uri;",
            ">;",
            "Laop",
            "<-",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            ">;",
            "LZS;",
            ")V"
        }
    .end annotation

    .prologue
    .line 87
    invoke-direct {p0, p6, p5}, LanC;-><init>(Laop;Laoo;)V

    .line 88
    iput-object p1, p0, Laok;->a:Laoj;

    .line 89
    iput-object p2, p0, Laok;->a:LTl;

    .line 90
    iput-object p3, p0, Laok;->a:Lalo;

    .line 91
    iput-object p4, p0, Laok;->a:Laeb;

    .line 92
    invoke-static {p7}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZS;

    iput-object v0, p0, Laok;->a:LZS;

    .line 93
    return-void
.end method

.method synthetic constructor <init>(Laoj;LTl;Lalo;Laeb;Laoo;Laop;LZS;Laol;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct/range {p0 .. p7}, Laok;-><init>(Laoj;LTl;Lalo;Laeb;Laoo;Laop;LZS;)V

    return-void
.end method

.method private a(Ljava/lang/Exception;)LaoK;
    .locals 2

    .prologue
    .line 145
    new-instance v0, LaoK;

    const-string v1, "Failed to fetch thumbnail"

    invoke-direct {v0, v1, p1}, LaoK;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method private a(Landroid/net/Uri;LaFO;)Ljava/io/InputStream;
    .locals 3

    .prologue
    .line 129
    iget-object v0, p0, Laok;->a:LZS;

    invoke-interface {v0}, LZS;->b()V

    .line 131
    :try_start_0
    iget-object v0, p0, Laok;->a:Laoj;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p2, p1, v1, v2}, Laoj;->a(LaFO;Landroid/net/Uri;Ljava/lang/String;LMP;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 132
    iget-object v1, p0, Laok;->a:LTl;

    invoke-virtual {v1, p1, v0}, LTl;->a(Landroid/net/Uri;Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;
    :try_end_0
    .catch LTr; {:try_start_0 .. :try_end_0} :catch_0
    .catch LTt; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    return-object v0

    .line 133
    :catch_0
    move-exception v0

    .line 134
    invoke-direct {p0, v0}, Laok;->a(Ljava/lang/Exception;)LaoK;

    move-result-object v0

    throw v0

    .line 135
    :catch_1
    move-exception v0

    .line 136
    invoke-direct {p0, v0}, Laok;->a(Ljava/lang/Exception;)LaoK;

    move-result-object v0

    throw v0

    .line 137
    :catch_2
    move-exception v0

    .line 138
    invoke-direct {p0, v0}, Laok;->a(Ljava/lang/Exception;)LaoK;

    move-result-object v0

    throw v0

    .line 139
    :catch_3
    move-exception v0

    .line 140
    invoke-direct {p0, v0}, Laok;->a(Ljava/lang/Exception;)LaoK;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method protected a(Lcom/google/android/apps/docs/utils/FetchSpec;Landroid/net/Uri;)LakD;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            "Landroid/net/Uri;",
            ")",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    :try_start_0
    invoke-static {}, Lbrm;->a()Lbrm;

    move-result-object v2

    .line 105
    iget-object v0, p0, Laok;->a:Laeb;

    invoke-interface {v0}, Laeb;->a()LakD;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 108
    :try_start_1
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/FetchSpec;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-direct {p0, p2, v0}, Laok;->a(Landroid/net/Uri;LaFO;)Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v2, v0}, Lbrm;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    .line 109
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-virtual {v3}, LakD;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    invoke-direct {v4, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v2, v4}, Lbrm;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v1

    check-cast v1, Ljava/io/OutputStream;

    .line 110
    iget-object v4, p0, Laok;->a:Lalo;

    invoke-interface {v4, v0, v1}, Lalo;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 115
    :try_start_2
    invoke-virtual {v2}, Lbrm;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 118
    return-object v3

    .line 117
    :catchall_0
    move-exception v0

    .line 118
    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 122
    :catch_0
    move-exception v0

    .line 123
    invoke-direct {p0, v0}, Laok;->a(Ljava/lang/Exception;)LaoK;

    move-result-object v0

    throw v0

    .line 114
    :catchall_1
    move-exception v0

    .line 115
    :try_start_4
    invoke-virtual {v2}, Lbrm;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 118
    :try_start_5
    invoke-virtual {v3}, LakD;->close()V

    throw v0

    .line 117
    :catchall_2
    move-exception v0

    .line 118
    invoke-virtual {v3}, LakD;->close()V

    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32
    check-cast p1, Lcom/google/android/apps/docs/utils/FetchSpec;

    check-cast p2, Landroid/net/Uri;

    invoke-virtual {p0, p1, p2}, Laok;->a(Lcom/google/android/apps/docs/utils/FetchSpec;Landroid/net/Uri;)LakD;

    move-result-object v0

    return-object v0
.end method

.method protected a(LakD;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 150
    invoke-virtual {p1}, LakD;->close()V

    .line 151
    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 32
    check-cast p1, LakD;

    invoke-virtual {p0, p1}, Laok;->a(LakD;)V

    return-void
.end method
