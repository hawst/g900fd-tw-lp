.class public LapS;
.super Ljava/lang/Object;
.source "HtmlPreviewPage.java"

# interfaces
.implements Laqz;


# instance fields
.field private final a:LaGv;

.field private final a:LakD;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LakD",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private a:Landroid/view/ViewGroup;

.field private a:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>(LakD;LaGv;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakD",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "LaGv;",
            ")V"
        }
    .end annotation

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    invoke-static {p1}, LakD;->a(LakD;)LakD;

    move-result-object v0

    iput-object v0, p0, LapS;->a:LakD;

    .line 97
    iput-object p2, p0, LapS;->a:LaGv;

    .line 98
    return-void
.end method

.method private c()V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "setJavaScriptEnabled"
        }
    .end annotation

    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v4, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 109
    new-instance v2, LaqK;

    iget-object v3, p0, LapS;->a:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LaqK;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, LapS;->a:Landroid/webkit/WebView;

    .line 110
    iget-object v2, p0, LapS;->a:Landroid/webkit/WebView;

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 114
    iget-object v2, p0, LapS;->a:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    .line 115
    invoke-virtual {v2, v0}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 116
    invoke-static {}, LakQ;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 117
    invoke-virtual {v2, v1}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    .line 119
    :cond_0
    invoke-virtual {v2, v0}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 120
    invoke-virtual {v2, v0}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 121
    invoke-virtual {v2, v0}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 122
    invoke-virtual {v2, v1}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 123
    invoke-static {}, LakQ;->f()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 124
    invoke-virtual {v2, v0}, Landroid/webkit/WebSettings;->setAllowUniversalAccessFromFileURLs(Z)V

    .line 128
    :cond_1
    invoke-static {}, LakQ;->c()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 129
    sget-object v3, Landroid/webkit/WebSettings$LayoutAlgorithm;->TEXT_AUTOSIZING:Landroid/webkit/WebSettings$LayoutAlgorithm;

    invoke-virtual {v2, v3}, Landroid/webkit/WebSettings;->setLayoutAlgorithm(Landroid/webkit/WebSettings$LayoutAlgorithm;)V

    .line 130
    sget-object v3, LaGv;->b:LaGv;

    iget-object v4, p0, LapS;->a:LaGv;

    invoke-virtual {v3, v4}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 136
    :goto_0
    invoke-virtual {v2, v0}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 139
    iget-object v0, p0, LapS;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lajt;->a(Landroid/content/Context;)Lbuu;

    move-result-object v0

    .line 140
    const-class v1, LaqL;

    invoke-interface {v0, v1}, Lbuu;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaqL;

    .line 141
    iget-object v1, p0, LapS;->a:Landroid/webkit/WebView;

    new-instance v2, LapU;

    invoke-direct {v2, v0}, LapU;-><init>(LaqL;)V

    const-string v0, "CAKEMIX_callback"

    invoke-virtual {v1, v2, v0}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    new-instance v0, LUf;

    iget-object v1, p0, LapS;->a:Landroid/webkit/WebView;

    invoke-direct {v0, v1}, LUf;-><init>(Landroid/webkit/WebView;)V

    .line 145
    iget-object v1, p0, LapS;->a:Landroid/webkit/WebView;

    new-instance v2, LapT;

    invoke-direct {v2, p0, v0}, LapT;-><init>(LapS;LUe;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 168
    iget-object v0, p0, LapS;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, LapS;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 169
    iget-object v0, p0, LapS;->a:LakD;

    invoke-virtual {v0}, LakD;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 170
    iget-object v1, p0, LapS;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 171
    iget-object v0, p0, LapS;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->requestFocus()Z

    .line 172
    return-void

    .line 134
    :cond_2
    sget-object v0, Landroid/webkit/WebSettings$LayoutAlgorithm;->NARROW_COLUMNS:Landroid/webkit/WebSettings$LayoutAlgorithm;

    invoke-virtual {v2, v0}, Landroid/webkit/WebSettings;->setLayoutAlgorithm(Landroid/webkit/WebSettings$LayoutAlgorithm;)V

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, LapS;->a:LakD;

    invoke-virtual {v0}, LakD;->close()V

    .line 177
    return-void
.end method

.method public a(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, LapS;->a:Landroid/view/ViewGroup;

    .line 103
    invoke-direct {p0}, LapS;->c()V

    .line 104
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, LapS;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, LapS;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 182
    invoke-direct {p0}, LapS;->c()V

    .line 183
    return-void
.end method
