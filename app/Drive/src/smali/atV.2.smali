.class public LatV;
.super Ljava/lang/Object;
.source "PdfViewerActivity.java"

# interfaces
.implements Lavo;


# instance fields
.field final synthetic a:LauM;

.field final synthetic a:Lcom/google/android/apps/viewer/PdfViewerActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/viewer/PdfViewerActivity;LauM;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, LatV;->a:Lcom/google/android/apps/viewer/PdfViewerActivity;

    iput-object p2, p0, LatV;->a:LauM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILauW;)V
    .locals 4

    .prologue
    .line 132
    iget-object v0, p0, LatV;->a:Lcom/google/android/apps/viewer/PdfViewerActivity;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Lcom/google/android/apps/viewer/PdfViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 133
    iget-object v1, p0, LatV;->a:Lcom/google/android/apps/viewer/PdfViewerActivity;

    iget-object v2, p0, LatV;->a:Lcom/google/android/apps/viewer/PdfViewerActivity;

    invoke-static {v2}, Lcom/google/android/apps/viewer/PdfViewerActivity;->a(Lcom/google/android/apps/viewer/PdfViewerActivity;)LawB;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0, p2}, LawB;->a(ILandroid/view/ViewGroup;LauW;)Laww;

    move-result-object v0

    check-cast v0, LawZ;

    invoke-static {v1, v0}, Lcom/google/android/apps/viewer/PdfViewerActivity;->a(Lcom/google/android/apps/viewer/PdfViewerActivity;LawZ;)LawZ;

    .line 134
    iget-object v0, p0, LatV;->a:Lcom/google/android/apps/viewer/PdfViewerActivity;

    invoke-static {v0}, Lcom/google/android/apps/viewer/PdfViewerActivity;->a(Lcom/google/android/apps/viewer/PdfViewerActivity;)Laua;

    move-result-object v0

    iget-object v1, p0, LatV;->a:Lcom/google/android/apps/viewer/PdfViewerActivity;

    invoke-static {v1}, Lcom/google/android/apps/viewer/PdfViewerActivity;->a(Lcom/google/android/apps/viewer/PdfViewerActivity;)LawZ;

    move-result-object v1

    invoke-virtual {v0, v1}, Laua;->a(Laue;)V

    .line 135
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 124
    const-string v0, "PdfViewerActivity"

    const-string v1, "Can\'t open file %s [%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LatV;->a:LauM;

    iget-object v3, v3, LauM;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    iget-object v3, v3, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    aput-object v3, v2, v6

    aput-object p2, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    iget-object v0, p0, LatV;->a:Lcom/google/android/apps/viewer/PdfViewerActivity;

    iget-object v1, p0, LatV;->a:Lcom/google/android/apps/viewer/PdfViewerActivity;

    sget v2, Laum;->error_open:I

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, LatV;->a:LauM;

    iget-object v4, v4, LauM;->b:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/viewer/PdfViewerActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 127
    iget-object v0, p0, LatV;->a:Lcom/google/android/apps/viewer/PdfViewerActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/PdfViewerActivity;->finish()V

    .line 128
    return-void
.end method
