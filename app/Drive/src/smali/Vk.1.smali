.class LVk;
.super Ljava/lang/Object;
.source "BatchOperationQueue.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:LVg;

.field final synthetic a:Ljava/util/concurrent/Future;


# direct methods
.method constructor <init>(LVg;Ljava/util/concurrent/Future;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, LVk;->a:LVg;

    iput-object p2, p0, LVk;->a:Ljava/util/concurrent/Future;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 144
    :try_start_0
    iget-object v0, p0, LVk;->a:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LVd;

    .line 145
    if-nez v0, :cond_1

    .line 146
    const-string v1, "BatchOperationQueue"

    const-string v2, "Trying to undo an empty batch."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 152
    :cond_0
    iget-object v1, p0, LVk;->a:LVg;

    invoke-virtual {v0}, LVd;->a()LaFM;

    move-result-object v0

    invoke-static {v1, v0}, LVg;->a(LVg;LaFM;)V

    .line 158
    :goto_0
    return-void

    .line 148
    :cond_1
    invoke-virtual {v0}, LVd;->a()LbmF;

    move-result-object v1

    invoke-static {v1}, LbnG;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LVt;

    .line 149
    invoke-interface {v1}, LVt;->b()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 153
    :catch_0
    move-exception v0

    .line 154
    const-string v1, "BatchOperationQueue"

    const-string v2, "Undo operation was interrupted."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 155
    :catch_1
    move-exception v0

    .line 156
    const-string v1, "BatchOperationQueue"

    const-string v2, "An error occured while running undo."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method
