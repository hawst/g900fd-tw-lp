.class public LMt;
.super Lajq;
.source "DocumentContentStatusChangedReceiver.java"


# instance fields
.field private final a:LMq;


# direct methods
.method constructor <init>(LMq;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lajq;-><init>()V

    .line 41
    iput-object p1, p0, LMt;->a:LMq;

    .line 42
    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 28
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.docs.statesyncer.PINNED_DOCUMENT_CONTENT_STATUS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 29
    const-string v1, "PACKAGE_NAME_EXTRA"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 30
    return-object v0
.end method

.method public static a()Landroid/content/IntentFilter;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.apps.docs.statesyncer.PINNED_DOCUMENT_CONTENT_STATUS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 47
    const-string v0, "PACKAGE_NAME_EXTRA"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 48
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    :goto_0
    return-void

    .line 52
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 53
    const-string v1, "DocumentContentStatusChangedReceiver"

    const-string v2, "Received intent with action %s"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 55
    const-string v1, "com.google.android.apps.docs.statesyncer.PINNED_DOCUMENT_CONTENT_STATUS_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 56
    const-string v1, "DocumentContentStatusChangedReceiver"

    const-string v2, "Unknown action: %s"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 60
    :cond_1
    iget-object v0, p0, LMt;->a:LMq;

    invoke-virtual {v0, p1}, LMq;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
