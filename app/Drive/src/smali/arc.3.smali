.class public abstract Larc;
.super Ljava/lang/Object;
.source "ActionBarModeController.java"


# instance fields
.field private final a:I

.field private final a:Landroid/app/Activity;

.field private final a:Larj;

.field private final a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Z

.field private final b:I


# direct methods
.method protected varargs constructor <init>(Landroid/app/Activity;Larj;ZII[Ljava/lang/Integer;)V
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    iput-object p1, p0, Larc;->a:Landroid/app/Activity;

    .line 100
    iput-object p2, p0, Larc;->a:Larj;

    .line 101
    iput-boolean p3, p0, Larc;->a:Z

    .line 102
    iput p4, p0, Larc;->a:I

    .line 103
    iput p5, p0, Larc;->b:I

    .line 104
    invoke-static {p6}, LbmY;->a([Ljava/lang/Object;)LbmY;

    move-result-object v0

    iput-object v0, p0, Larc;->a:LbmY;

    .line 105
    return-void
.end method

.method private static a(Landroid/app/Activity;)Landroid/view/View;
    .locals 4

    .prologue
    .line 173
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 175
    const-string v1, "action_bar_container"

    const-string v2, "id"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 176
    if-lez v0, :cond_0

    .line 177
    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 179
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Larc;Z)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 135
    iget v0, p1, Larc;->b:I

    iget v1, p0, Larc;->b:I

    if-eq v0, v1, :cond_0

    .line 136
    iget-object v0, p0, Larc;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 137
    iget-object v1, p0, Larc;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 138
    iget v2, p0, Larc;->b:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 139
    if-eqz p2, :cond_1

    .line 140
    iget v3, p1, Larc;->b:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 142
    iget-object v3, p0, Larc;->a:Landroid/app/Activity;

    invoke-static {v3}, Larc;->a(Landroid/app/Activity;)Landroid/view/View;

    move-result-object v3

    .line 143
    new-instance v4, Lard;

    const/4 v5, 0x2

    new-array v5, v5, [Landroid/graphics/drawable/Drawable;

    aput-object v0, v5, v6

    const/4 v0, 0x1

    aput-object v2, v5, v0

    invoke-direct {v4, p0, v5, v3}, Lard;-><init>(Larc;[Landroid/graphics/drawable/Drawable;Landroid/view/View;)V

    .line 155
    invoke-virtual {v4, v6}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 156
    invoke-virtual {v1, v4}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 157
    const/16 v0, 0x12c

    invoke-virtual {v4, v0}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 163
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method protected a()Landroid/app/ActionBar;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Larc;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected a()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Larc;->a:Landroid/app/Activity;

    return-object v0
.end method

.method protected a()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Larc;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public a()Larj;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Larc;->a:Larj;

    return-object v0
.end method

.method public a()LbmY;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmY",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Larc;->a:LbmY;

    return-object v0
.end method

.method protected abstract a()V
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 199
    return-void
.end method

.method public a(Landroid/view/MenuItem;)V
    .locals 0

    .prologue
    .line 167
    return-void
.end method

.method public a(Larc;Z)V
    .locals 0

    .prologue
    .line 124
    invoke-virtual {p1}, Larc;->c()V

    .line 125
    invoke-direct {p0, p1, p2}, Larc;->b(Larc;Z)V

    .line 126
    invoke-virtual {p0}, Larc;->a()V

    .line 127
    return-void
.end method

.method public a(Laro;)V
    .locals 6

    .prologue
    .line 204
    iget-object v0, p0, Larc;->a:Landroid/app/Activity;

    sget v1, Lxi;->menu_overflow_content_description:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 205
    iget-object v0, p0, Larc;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    .line 206
    iget-object v0, p0, Larc;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v3, p0, Larc;->a:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 208
    new-instance v0, Larf;

    const/4 v5, 0x0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Larf;-><init>(Landroid/view/View;Ljava/lang/String;ILaro;Lard;)V

    .line 210
    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 211
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 196
    iget-boolean v0, p0, Larc;->a:Z

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 201
    return-void
.end method

.method public b(Landroid/view/MenuItem;)V
    .locals 3

    .prologue
    .line 116
    invoke-interface {p1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 117
    if-eqz v0, :cond_0

    .line 118
    invoke-virtual {p0}, Larc;->a()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Larc;->a:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 119
    invoke-interface {p1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v0, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 121
    :cond_0
    return-void
.end method

.method protected c()V
    .locals 0

    .prologue
    .line 132
    return-void
.end method
