.class public Lbdz;
.super Lbem;
.source "GoogleJsonResponseException.java"


# instance fields
.field private final transient a:Lbdw;


# direct methods
.method constructor <init>(Lben;Lbdw;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lbem;-><init>(Lben;)V

    .line 68
    iput-object p2, p0, Lbdz;->a:Lbdw;

    .line 69
    return-void
.end method

.method public static a(LbeO;Lbel;)Lbdz;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 93
    new-instance v4, Lben;

    .line 94
    invoke-virtual {p1}, Lbel;->a()I

    move-result v0

    invoke-virtual {p1}, Lbel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lbel;->a()Lbed;

    move-result-object v3

    invoke-direct {v4, v0, v2, v3}, Lben;-><init>(ILjava/lang/String;Lbed;)V

    .line 96
    invoke-static {p0}, LbfN;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    :try_start_0
    invoke-virtual {p1}, Lbel;->a()Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "application/json; charset=UTF-8"

    .line 101
    invoke-virtual {p1}, Lbel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbeh;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 102
    invoke-virtual {p1}, Lbel;->a()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    if-eqz v0, :cond_7

    .line 105
    :try_start_1
    invoke-virtual {p1}, Lbel;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {p0, v0}, LbeO;->a(Ljava/io/InputStream;)LbeS;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 106
    :try_start_2
    invoke-virtual {v3}, LbeS;->b()LbeX;

    move-result-object v0

    .line 108
    if-nez v0, :cond_0

    .line 109
    invoke-virtual {v3}, LbeS;->a()LbeX;

    move-result-object v0

    .line 112
    :cond_0
    if-eqz v0, :cond_9

    .line 114
    const-string v0, "error"

    invoke-virtual {v3, v0}, LbeS;->a(Ljava/lang/String;)V

    .line 115
    invoke-virtual {v3}, LbeS;->b()LbeX;

    move-result-object v0

    sget-object v2, LbeX;->d:LbeX;

    if-eq v0, v2, :cond_9

    .line 116
    const-class v0, Lbdw;

    invoke-virtual {v3, v0}, LbeS;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbdw;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 117
    :try_start_3
    invoke-virtual {v0}, Lbdw;->toPrettyString()Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    .line 124
    :goto_0
    if-nez v3, :cond_3

    .line 125
    :try_start_4
    invoke-virtual {p1}, Lbel;->a()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 138
    :cond_1
    :goto_1
    invoke-static {p1}, Lbem;->a(Lbel;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 139
    invoke-static {v0}, LbfT;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 140
    sget-object v3, LbfS;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    invoke-virtual {v4, v0}, Lben;->c(Ljava/lang/String;)Lben;

    .line 143
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lben;->a(Ljava/lang/String;)Lben;

    .line 145
    new-instance v0, Lbdz;

    invoke-direct {v0, v4, v1}, Lbdz;-><init>(Lben;Lbdw;)V

    return-object v0

    .line 126
    :cond_3
    if-nez v1, :cond_1

    .line 127
    :try_start_5
    invoke-virtual {v3}, LbeS;->a()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_1

    .line 133
    :catch_0
    move-exception v2

    .line 135
    :goto_2
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 120
    :catch_1
    move-exception v0

    move-object v2, v0

    move-object v3, v1

    move-object v0, v1

    .line 122
    :goto_3
    :try_start_6
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 124
    if-nez v3, :cond_4

    .line 125
    :try_start_7
    invoke-virtual {p1}, Lbel;->a()V

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_1

    .line 126
    :cond_4
    if-nez v0, :cond_8

    .line 127
    invoke-virtual {v3}, LbeS;->a()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_1

    .line 124
    :catchall_0
    move-exception v0

    move-object v3, v1

    move-object v2, v1

    :goto_4
    if-nez v3, :cond_6

    .line 125
    :try_start_8
    invoke-virtual {p1}, Lbel;->a()V

    .line 127
    :cond_5
    :goto_5
    throw v0

    .line 133
    :catch_2
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v2

    move-object v2, v5

    goto :goto_2

    .line 126
    :cond_6
    if-nez v2, :cond_5

    .line 127
    invoke-virtual {v3}, LbeS;->a()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    goto :goto_5

    .line 131
    :cond_7
    :try_start_9
    invoke-virtual {p1}, Lbel;->c()Ljava/lang/String;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    move-result-object v0

    goto :goto_1

    .line 133
    :catch_3
    move-exception v0

    move-object v2, v0

    move-object v0, v1

    goto :goto_2

    :catch_4
    move-exception v2

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_2

    .line 124
    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_4

    :catchall_2
    move-exception v2

    move-object v5, v2

    move-object v2, v0

    move-object v0, v5

    goto :goto_4

    .line 120
    :catch_5
    move-exception v0

    move-object v2, v0

    move-object v0, v1

    goto :goto_3

    :catch_6
    move-exception v2

    goto :goto_3

    :cond_8
    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_1

    :cond_9
    move-object v0, v1

    goto/16 :goto_0
.end method
