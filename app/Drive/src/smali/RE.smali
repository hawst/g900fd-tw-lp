.class public LRE;
.super LaGN;
.source "NavigationFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGN",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/app/Activity;

.field final synthetic a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

.field final synthetic a:Lcom/google/android/apps/docs/fragment/NavigationFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/fragment/NavigationFragment;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 501
    iput-object p1, p0, LRE;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iput-object p2, p0, LRE;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    iput-object p3, p0, LRE;->a:Landroid/app/Activity;

    invoke-direct {p0}, LaGN;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(LaGM;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 501
    invoke-virtual {p0, p1}, LRE;->a(LaGM;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(LaGM;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 504
    iget-object v0, p0, LRE;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    iget-object v1, p0, LRE;->a:Landroid/app/Activity;

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a(LaGM;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 505
    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 501
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, LRE;->a(Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 510
    iget-object v0, p0, LRE;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()LH;

    move-result-object v0

    .line 511
    if-nez v0, :cond_1

    .line 525
    :cond_0
    :goto_0
    return-void

    .line 514
    :cond_1
    const-string v0, "NavigationFragment"

    const-string v1, "new path: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 515
    iget-object v0, p0, LRE;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LRK;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 516
    iget-object v0, p0, LRE;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LRE;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LtK;

    sget-object v1, Lry;->h:Lry;

    .line 517
    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 518
    iget-object v0, p0, LRE;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(Lcom/google/android/apps/docs/fragment/NavigationFragment;)Landroid/widget/Toast;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 519
    iget-object v0, p0, LRE;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(Lcom/google/android/apps/docs/fragment/NavigationFragment;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 523
    :goto_1
    iget-object v0, p0, LRE;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(Lcom/google/android/apps/docs/fragment/NavigationFragment;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 521
    :cond_2
    iget-object v0, p0, LRE;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iget-object v1, p0, LRE;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()LH;

    move-result-object v1

    invoke-static {v1, p1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(Lcom/google/android/apps/docs/fragment/NavigationFragment;Landroid/widget/Toast;)Landroid/widget/Toast;

    goto :goto_1
.end method
