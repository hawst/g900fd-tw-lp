.class public final Lrh;
.super Ljava/lang/Object;
.source "AccountSpecificFeatureAdapter.java"

# interfaces
.implements Lrg;


# instance fields
.field private final a:LtJ;


# direct methods
.method private constructor <init>(LtJ;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtJ;

    iput-object v0, p0, Lrh;->a:LtJ;

    .line 22
    return-void
.end method

.method public static a(LtJ;)Lrh;
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lrh;

    invoke-direct {v0, p0}, Lrh;-><init>(LtJ;)V

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lrh;->a:LtJ;

    invoke-interface {v0}, LtJ;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Lrx;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lrh;->a:LtJ;

    invoke-interface {v0}, LtJ;->a()Lrx;

    move-result-object v0

    return-object v0
.end method

.method public a()LtJ;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lrh;->a:LtJ;

    return-object v0
.end method

.method public a(LtK;LQr;LpW;LaFO;)Z
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lrh;->a:LtJ;

    invoke-interface {v0, p1, p2}, LtJ;->a(LtK;LQr;)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 51
    instance-of v0, p1, Lrh;

    if-nez v0, :cond_0

    .line 52
    const/4 v0, 0x0

    .line 55
    :goto_0
    return v0

    .line 54
    :cond_0
    check-cast p1, Lrh;

    .line 55
    iget-object v0, p0, Lrh;->a:LtJ;

    iget-object v1, p1, Lrh;->a:LtJ;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 46
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, Lrh;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lrh;->a:LtJ;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AccountSpecificFeatureAdapter["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lrh;->a:LtJ;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
