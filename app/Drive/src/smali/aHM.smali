.class public final LaHM;
.super Ljava/lang/Object;
.source "DriveAppSetImpl.java"

# interfaces
.implements LaHK;


# instance fields
.field private final a:LaHP;

.field private final a:LbpT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbpT",
            "<",
            "Ljava/lang/String;",
            "LaHJ;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LbpT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbpT",
            "<",
            "Ljava/lang/String;",
            "LaHJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {}, LbnA;->a()LbnA;

    move-result-object v0

    iput-object v0, p0, LaHM;->a:LbpT;

    .line 35
    invoke-static {}, LbnA;->a()LbnA;

    move-result-object v0

    iput-object v0, p0, LaHM;->b:LbpT;

    .line 36
    new-instance v0, LaHP;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LaHP;-><init>(LaHM;LaHN;)V

    iput-object v0, p0, LaHM;->a:LaHP;

    .line 59
    return-void
.end method

.method static synthetic a(LaHM;)LbpT;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, LaHM;->a:LbpT;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 32
    invoke-static {p0}, LaHM;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(LaHM;)LbpT;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, LaHM;->b:LbpT;

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 126
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "."

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LaHJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    iget-object v0, p0, LaHM;->a:LaHP;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "LaHJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131
    invoke-static {p1}, LaHM;->b(Ljava/lang/String;)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid extension: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LbiT;->a(ZLjava/lang/Object;)V

    .line 132
    iget-object v0, p0, LaHM;->a:LbpT;

    invoke-interface {v0, p1}, LbpT;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "LaHJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    iget-object v0, p0, LaHM;->b:LbpT;

    invoke-interface {v0, p1}, LbpT;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
