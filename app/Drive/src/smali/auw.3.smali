.class public Lauw;
.super Ljava/lang/Object;
.source "SystemPrintDialog.java"

# interfaces
.implements Lauv;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final a:Lavl;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lavi;

    invoke-direct {v0}, Lavi;-><init>()V

    iget-object v0, v0, Lavi;->g:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    sput-object v0, Lauw;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lavl;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lauw;->a:Landroid/app/Activity;

    .line 45
    iput-object p2, p0, Lauw;->a:Lavl;

    .line 46
    return-void
.end method

.method static synthetic a(Lauw;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lauw;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic a(Lauw;)Lavl;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lauw;->a:Lavl;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 55
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    sget-object v0, Lauw;->a:Ljava/lang/String;

    .line 56
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    invoke-static {p1}, Lavl;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/viewer/client/AuthenticatedUri;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 62
    iget-object v0, p3, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->tokens:Lcom/google/android/apps/viewer/client/TokenSource;

    sget-object v1, Lcom/google/android/apps/viewer/client/TokenSource;->a:Lcom/google/android/apps/viewer/client/TokenSource;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Auth not supported"

    invoke-static {v0, v1}, LauV;->b(ZLjava/lang/String;)V

    .line 63
    sget-object v0, Lauw;->a:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "Only PDF currently supported."

    invoke-static {v0, v1}, LauV;->b(ZLjava/lang/String;)V

    .line 64
    iget-object v0, p3, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-static {v0}, Lavl;->a(Landroid/net/Uri;)Z

    move-result v0

    const-string v1, "Remote files not supported."

    invoke-static {v0, v1}, LauV;->b(ZLjava/lang/String;)V

    .line 65
    iget-object v0, p0, Lauw;->a:Landroid/app/Activity;

    const-string v1, "print"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/print/PrintManager;

    .line 66
    new-instance v0, Lauy;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p1

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lauy;-><init>(Lauw;Lcom/google/android/apps/viewer/client/AuthenticatedUri;Ljava/lang/String;ILaux;)V

    invoke-virtual {v6, p1, v0, v5}, Landroid/print/PrintManager;->print(Ljava/lang/String;Landroid/print/PrintDocumentAdapter;Landroid/print/PrintAttributes;)Landroid/print/PrintJob;

    .line 67
    return-void

    .line 62
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
