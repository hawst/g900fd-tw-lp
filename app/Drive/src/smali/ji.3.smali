.class Lji;
.super Landroid/view/ActionProvider;
.source "MenuItemWrapperICS.java"


# instance fields
.field final a:Ldf;

.field final synthetic a:Ljh;


# direct methods
.method public constructor <init>(Ljh;Ldf;)V
    .locals 2

    .prologue
    .line 395
    iput-object p1, p0, Lji;->a:Ljh;

    .line 396
    invoke-virtual {p2}, Ldf;->a()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/view/ActionProvider;-><init>(Landroid/content/Context;)V

    .line 397
    iput-object p2, p0, Lji;->a:Ldf;

    .line 399
    invoke-static {p1}, Ljh;->a(Ljh;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 400
    iget-object v0, p0, Lji;->a:Ldf;

    new-instance v1, Ljj;

    invoke-direct {v1, p0, p1}, Ljj;-><init>(Lji;Ljh;)V

    invoke-virtual {v0, v1}, Ldf;->a(Ldh;)V

    .line 409
    :cond_0
    return-void
.end method


# virtual methods
.method public hasSubMenu()Z
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lji;->a:Ldf;

    invoke-virtual {v0}, Ldf;->d()Z

    move-result v0

    return v0
.end method

.method public onCreateActionView()Landroid/view/View;
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lji;->a:Ljh;

    invoke-static {v0}, Ljh;->a(Ljh;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 416
    iget-object v0, p0, Lji;->a:Ljh;

    invoke-virtual {v0}, Ljh;->a()Z

    .line 418
    :cond_0
    iget-object v0, p0, Lji;->a:Ldf;

    invoke-virtual {v0}, Ldf;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onPerformDefaultAction()Z
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lji;->a:Ldf;

    invoke-virtual {v0}, Ldf;->c()Z

    move-result v0

    return v0
.end method

.method public onPrepareSubMenu(Landroid/view/SubMenu;)V
    .locals 2

    .prologue
    .line 433
    iget-object v0, p0, Lji;->a:Ldf;

    iget-object v1, p0, Lji;->a:Ljh;

    invoke-virtual {v1, p1}, Ljh;->a(Landroid/view/SubMenu;)Landroid/view/SubMenu;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldf;->a(Landroid/view/SubMenu;)V

    .line 434
    return-void
.end method
