.class public LbgD;
.super Ljava/io/OutputStream;
.source "ByteCountingOutputStream.java"


# instance fields
.field private a:I

.field private final a:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput v0, p0, LbgD;->a:I

    .line 20
    iput-object p1, p0, LbgD;->a:Ljava/io/OutputStream;

    .line 21
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, LbgD;->a:I

    return v0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, LbgD;->a:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 58
    return-void
.end method

.method public flush()V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, LbgD;->a:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 53
    return-void
.end method

.method public write(I)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, LbgD;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 35
    iget v0, p0, LbgD;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LbgD;->a:I

    .line 36
    return-void
.end method

.method public write([B)V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, LbgD;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    .line 41
    iget v0, p0, LbgD;->a:I

    array-length v1, p1

    add-int/2addr v0, v1

    iput v0, p0, LbgD;->a:I

    .line 42
    return-void
.end method

.method public write([BII)V
    .locals 1

    .prologue
    .line 46
    iget v0, p0, LbgD;->a:I

    add-int/2addr v0, p3

    iput v0, p0, LbgD;->a:I

    .line 47
    iget-object v0, p0, LbgD;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 48
    return-void
.end method
