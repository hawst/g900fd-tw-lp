.class LTJ;
.super Ljava/lang/Object;
.source "HttpIssuerBase.java"


# instance fields
.field private final a:LTN;

.field private final a:Ljava/io/IOException;

.field private final a:Lorg/apache/http/client/methods/HttpUriRequest;

.field private a:Z


# direct methods
.method public constructor <init>(Lorg/apache/http/client/methods/HttpUriRequest;LTN;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, LTJ;->a:Z

    .line 39
    iput-object p1, p0, LTJ;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 40
    iput-object p2, p0, LTJ;->a:LTN;

    .line 41
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    iput-object v0, p0, LTJ;->a:Ljava/io/IOException;

    .line 42
    return-void
.end method


# virtual methods
.method public a()LTN;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, LTJ;->a:LTN;

    return-object v0
.end method

.method public a()Ljava/io/IOException;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, LTJ;->a:Ljava/io/IOException;

    return-object v0
.end method

.method public a()Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, LTJ;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, LTJ;->a:Z

    .line 63
    return-void
.end method

.method protected finalize()V
    .locals 3

    .prologue
    .line 67
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 68
    iget-boolean v0, p0, LTJ;->a:Z

    if-nez v0, :cond_0

    .line 69
    const-string v0, "HttpIssuerBase"

    new-instance v1, Ljava/io/IOException;

    const-string v2, "HttpIsser request leak."

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LTJ;->a()Ljava/io/IOException;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    const-string v2, "HttpIssuer request NOT closed."

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 72
    :cond_0
    return-void
.end method
