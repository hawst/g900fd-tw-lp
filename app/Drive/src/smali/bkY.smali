.class final LbkY;
.super Lblh;
.source "LongAdder.java"

# interfaces
.implements LbkT;
.implements Ljava/io/Serializable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lblh;-><init>()V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 8

    .prologue
    .line 109
    iget-wide v0, p0, LbkY;->a:J

    .line 110
    iget-object v3, p0, LbkY;->a:[Lblj;

    .line 111
    if-eqz v3, :cond_1

    .line 112
    array-length v4, v3

    .line 113
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    .line 114
    aget-object v5, v3, v2

    .line 115
    if-eqz v5, :cond_0

    .line 116
    iget-wide v6, v5, Lblj;->a:J

    add-long/2addr v0, v6

    .line 113
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 119
    :cond_1
    return-wide v0
.end method

.method final a(JJ)J
    .locals 3

    .prologue
    .line 57
    add-long v0, p1, p3

    return-wide v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 89
    const-wide/16 v0, 0x1

    invoke-virtual {p0, v0, v1}, LbkY;->a(J)V

    .line 90
    return-void
.end method

.method public a(J)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    .line 76
    iget-object v2, p0, LbkY;->a:[Lblj;

    if-nez v2, :cond_0

    iget-wide v4, p0, LbkY;->a:J

    add-long v6, v4, p1

    invoke-virtual {p0, v4, v5, v6, v7}, LbkY;->a(JJ)Z

    move-result v0

    if-nez v0, :cond_2

    .line 78
    :cond_0
    sget-object v0, LbkY;->a:Lblm;

    invoke-virtual {v0}, Lblm;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbll;

    iget v3, v0, Lbll;->a:I

    .line 79
    if-eqz v2, :cond_1

    array-length v4, v2

    if-lt v4, v1, :cond_1

    add-int/lit8 v4, v4, -0x1

    and-int/2addr v3, v4

    aget-object v2, v2, v3

    if-eqz v2, :cond_1

    iget-wide v4, v2, Lblj;->a:J

    add-long v6, v4, p1

    invoke-virtual {v2, v4, v5, v6, v7}, Lblj;->a(JJ)Z

    move-result v1

    if-nez v1, :cond_2

    .line 81
    :cond_1
    invoke-virtual {p0, p1, p2, v0, v1}, LbkY;->a(JLbll;Z)V

    .line 83
    :cond_2
    return-void
.end method

.method public doubleValue()D
    .locals 2

    .prologue
    .line 203
    invoke-virtual {p0}, LbkY;->a()J

    move-result-wide v0

    long-to-double v0, v0

    return-wide v0
.end method

.method public floatValue()F
    .locals 2

    .prologue
    .line 194
    invoke-virtual {p0}, LbkY;->a()J

    move-result-wide v0

    long-to-float v0, v0

    return v0
.end method

.method public intValue()I
    .locals 2

    .prologue
    .line 185
    invoke-virtual {p0}, LbkY;->a()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public longValue()J
    .locals 2

    .prologue
    .line 176
    invoke-virtual {p0}, LbkY;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 166
    invoke-virtual {p0}, LbkY;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
