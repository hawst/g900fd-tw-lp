.class LGE;
.super LFK;
.source "RemoteOpenerSelector.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LFK",
        "<",
        "LaGv;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Laja;Laja;Laja;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "LGt;",
            ">;",
            "Laja",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;",
            "Lcom/google/android/apps/docs/doclist/documentopener/PdfExportDocumentOpener;",
            ">;>;",
            "Laja",
            "<",
            "LGF;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-static {p1, p2, p3}, LGE;->a(Lbxw;Lbxw;Lbxw;)Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v0}, LFK;-><init>(Ljava/util/Map;)V

    .line 32
    return-void
.end method

.method static a(Lbxw;Lbxw;Lbxw;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbxw",
            "<",
            "LGt;",
            ">;",
            "Lbxw",
            "<+",
            "LFR;",
            ">;",
            "Lbxw",
            "<",
            "LGF;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "LaGv;",
            "Lbxw",
            "<+",
            "LFR;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 43
    sget-object v1, LaGv;->b:LaGv;

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v1, LaGv;->i:LaGv;

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v1, LaGv;->g:LaGv;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v1, LaGv;->c:LaGv;

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v1, LaGv;->h:LaGv;

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    return-object v0
.end method
