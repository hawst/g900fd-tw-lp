.class public abstract LvC;
.super Lrd;
.source "DetailActivityBase.java"

# interfaces
.implements LCW;
.implements LCX;
.implements LQU;
.implements LRA;
.implements LaqQ;


# instance fields
.field a:LBZ;

.field a:LPp;

.field a:LVm;

.field a:LaGM;

.field a:LaGR;

.field a:LaIm;

.field a:LacF;

.field a:Lacj;

.field a:Ladf;

.field private a:Landroid/view/Menu;

.field private a:Landroid/view/View;

.field a:LsP;

.field a:LtK;

.field a:LvU;

.field a:Lwa;

.field i:Z

.field private j:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lrd;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;Z)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 146
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, LvC;->a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;ZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;ZLjava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 156
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/app/detailpanel/DetailActivityDelegate;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 160
    const-string v1, "openEnabled"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 161
    if-eqz p2, :cond_0

    .line 162
    const-string v1, "pathElement"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 164
    :cond_0
    if-eqz p4, :cond_1

    .line 165
    const-string v1, "usersToInvite"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 167
    :cond_1
    const-string v1, "entrySpec.v2"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 168
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p2}, LvC;->a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;ZLjava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p2, p3}, LvC;->a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;ZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a()Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 3

    .prologue
    .line 423
    invoke-virtual {p0}, LvC;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 424
    const-string v1, "requestCameFromExternalApp"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 427
    iget-object v1, p0, LvC;->a:LaIm;

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-interface {v1, v0}, LaIm;->a(Landroid/net/Uri;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 429
    :goto_0
    return-object v0

    :cond_0
    const-string v1, "entrySpec.v2"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    goto :goto_0
.end method

.method private a(LaGu;)V
    .locals 5

    .prologue
    .line 300
    invoke-virtual {p0}, LvC;->a()Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    move-result-object v0

    .line 301
    if-eqz v0, :cond_0

    .line 302
    invoke-virtual {p0}, LvC;->a()Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    move-result-object v0

    sget v1, Lxi;->detail_fragment_title:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 303
    invoke-interface {p1}, LaGu;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, LvC;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 302
    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a(Ljava/lang/CharSequence;)V

    .line 305
    :cond_0
    return-void
.end method

.method static synthetic a(LvC;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, LvC;->k()V

    return-void
.end method

.method static synthetic a(LvC;LaGu;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1}, LvC;->b(LaGu;)V

    return-void
.end method

.method static synthetic a(LvC;)Z
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, LvC;->c()Z

    move-result v0

    return v0
.end method

.method static synthetic a(LvC;Z)Z
    .locals 0

    .prologue
    .line 60
    iput-boolean p1, p0, LvC;->j:Z

    return p1
.end method

.method private b(LaGu;)V
    .locals 4

    .prologue
    .line 308
    if-nez p1, :cond_0

    .line 309
    invoke-virtual {p0}, LvC;->l()V

    .line 332
    :goto_0
    return-void

    .line 312
    :cond_0
    iget-object v0, p0, LvC;->a:LqK;

    const-string v1, "doclist"

    const-string v2, "showEntryDetailEvent"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    invoke-virtual {p0}, LvC;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 315
    const-string v0, "pathElement"

    .line 316
    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    .line 317
    if-eqz v0, :cond_1

    const-string v2, "requestCameFromExternalApp"

    const/4 v3, 0x0

    .line 318
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 319
    iget-object v1, p0, LvC;->a:Lwa;

    invoke-interface {v1, v0}, Lwa;->a(Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;)V

    .line 321
    :cond_1
    iget-object v0, p0, LvC;->a:LvU;

    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-interface {v0, v1}, LvU;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 322
    invoke-virtual {p0}, LvC;->a()Lcom/google/android/apps/docs/fragment/DetailFragment;

    move-result-object v0

    .line 323
    iget-boolean v1, p0, LvC;->i:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/fragment/DetailFragment;->a(Z)V

    .line 324
    invoke-direct {p0, p1}, LvC;->a(LaGu;)V

    .line 326
    iget-object v0, p0, LvC;->a:Landroid/view/View;

    new-instance v1, LvG;

    invoke-direct {v1, p0}, LvG;-><init>(LvC;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method static synthetic b(LvC;LaGu;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1}, LvC;->a(LaGu;)V

    return-void
.end method

.method private k()V
    .locals 1

    .prologue
    .line 342
    iget-boolean v0, p0, LvC;->j:Z

    if-eqz v0, :cond_0

    .line 343
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LvC;->setResult(I)V

    .line 347
    :goto_0
    invoke-virtual {p0}, LvC;->finish()V

    .line 348
    return-void

    .line 345
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LvC;->setResult(I)V

    goto :goto_0
.end method


# virtual methods
.method public abstract a()I
.end method

.method public a()LaFO;
    .locals 2

    .prologue
    .line 435
    invoke-super {p0}, Lrd;->a()LaFO;

    move-result-object v0

    .line 436
    if-nez v0, :cond_0

    .line 437
    invoke-direct {p0}, LvC;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    .line 438
    if-eqz v1, :cond_0

    .line 439
    iget-object v0, v1, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    .line 443
    :cond_0
    return-object v0
.end method

.method public abstract a()Lcom/google/android/apps/docs/entry/DetailDrawerFragment;
.end method

.method public abstract a()Lcom/google/android/apps/docs/fragment/DetailFragment;
.end method

.method public a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 173
    const-class v2, Lacr;

    if-ne p1, v2, :cond_2

    .line 174
    if-nez p2, :cond_1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 176
    iget-object v0, p0, LvC;->a:Lacj;

    invoke-virtual {v0}, Lacj;->a()Lacr;

    move-result-object p0

    .line 201
    :cond_0
    :goto_1
    return-object p0

    :cond_1
    move v0, v1

    .line 174
    goto :goto_0

    .line 178
    :cond_2
    const-class v2, Labg;

    if-ne p1, v2, :cond_4

    .line 179
    if-nez p2, :cond_3

    :goto_2
    invoke-static {v0}, LbiT;->a(Z)V

    .line 181
    invoke-virtual {p0}, LvC;->a()Lcom/google/android/apps/docs/fragment/DetailFragment;

    move-result-object p0

    goto :goto_1

    :cond_3
    move v0, v1

    .line 179
    goto :goto_2

    .line 183
    :cond_4
    const-class v2, LQT;

    if-ne p1, v2, :cond_6

    .line 184
    if-nez p2, :cond_5

    :goto_3
    invoke-static {v0}, LbiT;->a(Z)V

    .line 186
    invoke-virtual {p0}, LvC;->a()Lcom/google/android/apps/docs/fragment/DetailFragment;

    move-result-object p0

    goto :goto_1

    :cond_5
    move v0, v1

    .line 184
    goto :goto_3

    .line 188
    :cond_6
    const-class v2, LaqQ;

    if-ne p1, v2, :cond_7

    .line 189
    iget-boolean v0, p0, LvC;->i:Z

    if-nez v0, :cond_0

    .line 194
    const/4 p0, 0x0

    goto :goto_1

    .line 195
    :cond_7
    const-class v2, LQU;

    if-ne p1, v2, :cond_9

    .line 196
    if-nez p2, :cond_8

    :goto_4
    invoke-static {v0}, LbiT;->a(Z)V

    goto :goto_1

    :cond_8
    move v0, v1

    goto :goto_4

    .line 201
    :cond_9
    invoke-super {p0, p1, p2}, Lrd;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    goto :goto_1
.end method

.method public a(F)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 447
    iget-object v0, p0, LvC;->a:Landroid/view/View;

    const/high16 v1, 0x42990000    # 76.5f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    .line 448
    invoke-static {v1, v2, v2, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    .line 447
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 449
    return-void
.end method

.method public a(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V
    .locals 3

    .prologue
    .line 414
    iget-object v0, p0, LvC;->a:LaGM;

    iget-object v1, p0, LvC;->a:LtK;

    .line 415
    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    .line 414
    invoke-static {p0, v0, v1, v2}, Lala;->a(Landroid/content/Context;LaGM;LtK;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    .line 416
    if-eqz v0, :cond_0

    .line 417
    invoke-virtual {p0, v0}, LvC;->startActivity(Landroid/content/Intent;)V

    .line 419
    :cond_0
    invoke-virtual {p0}, LvC;->l()V

    .line 420
    return-void
.end method

.method public a(Landroid/view/Menu;)V
    .locals 1

    .prologue
    .line 368
    invoke-virtual {p0}, LvC;->a()Lcom/google/android/apps/docs/fragment/DetailFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/DetailFragment;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369
    invoke-virtual {p0, p1}, LvC;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 371
    :cond_0
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 363
    const/4 v0, 0x0

    return v0
.end method

.method public b_(Landroid/view/Menu;)V
    .locals 1

    .prologue
    .line 375
    invoke-virtual {p0}, LvC;->a()Lcom/google/android/apps/docs/fragment/DetailFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/DetailFragment;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 376
    invoke-virtual {p0, p1}, LvC;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 378
    :cond_0
    return-void
.end method

.method public e_()V
    .locals 0

    .prologue
    .line 453
    invoke-virtual {p0}, LvC;->f()V

    .line 454
    return-void
.end method

.method protected f()V
    .locals 1

    .prologue
    .line 400
    invoke-virtual {p0}, LvC;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 410
    :cond_0
    :goto_0
    return-void

    .line 405
    :cond_1
    invoke-static {}, LakQ;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 406
    invoke-super {p0}, Lrd;->invalidateOptionsMenu()V

    goto :goto_0

    .line 407
    :cond_2
    iget-object v0, p0, LvC;->a:Landroid/view/Menu;

    if-eqz v0, :cond_0

    .line 408
    iget-object v0, p0, LvC;->a:Landroid/view/Menu;

    invoke-virtual {p0, v0}, LvC;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    goto :goto_0
.end method

.method public l()V
    .locals 0

    .prologue
    .line 352
    invoke-direct {p0}, LvC;->k()V

    .line 353
    return-void
.end method

.method public n()V
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, LvC;->a:LvU;

    invoke-interface {v0}, LvU;->a()V

    .line 207
    iget-object v0, p0, LvC;->a:LvU;

    invoke-interface {v0}, LvU;->a()LaGu;

    move-result-object v0

    .line 208
    new-instance v1, LvD;

    invoke-direct {v1, p0, v0}, LvD;-><init>(LvC;LaGu;)V

    .line 232
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 233
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 357
    invoke-virtual {p0}, LvC;->m()V

    .line 358
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 242
    invoke-super {p0, p1}, Lrd;->onCreate(Landroid/os/Bundle;)V

    .line 245
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LvC;->setTitle(Ljava/lang/CharSequence;)V

    .line 247
    invoke-virtual {p0}, LvC;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 248
    const-string v1, "openEnabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, LvC;->i:Z

    .line 249
    const-string v1, "usersToInvite"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 250
    if-nez p1, :cond_0

    if-eqz v0, :cond_0

    .line 251
    iget-object v1, p0, LvC;->a:LacF;

    invoke-virtual {v1, v0}, LacF;->a(Ljava/lang/String;)V

    .line 254
    :cond_0
    invoke-virtual {p0}, LvC;->a()I

    move-result v0

    invoke-virtual {p0, v0}, LvC;->setContentView(I)V

    .line 256
    sget v0, Lxc;->detail_panel_container:I

    invoke-virtual {p0, v0}, LvC;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LvC;->a:Landroid/view/View;

    .line 258
    invoke-direct {p0}, LvC;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 259
    if-nez v0, :cond_1

    .line 260
    invoke-virtual {p0}, LvC;->finish()V

    .line 294
    :goto_0
    return-void

    .line 264
    :cond_1
    iget-object v1, p0, LvC;->a:LaGR;

    new-instance v2, LvE;

    invoke-direct {v2, p0, v0}, LvE;-><init>(LvC;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    invoke-virtual {v1, v2}, LaGR;->a(LaGN;)V

    .line 277
    iget-object v0, p0, LvC;->a:LvU;

    new-instance v1, LvF;

    invoke-direct {v1, p0}, LvF;-><init>(LvC;)V

    invoke-interface {v0, v1}, LvU;->a(LvV;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0}, LvC;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 383
    const/4 v0, 0x1

    .line 387
    :goto_0
    return v0

    .line 386
    :cond_0
    iput-object p1, p0, LvC;->a:Landroid/view/Menu;

    .line 387
    invoke-super {p0, p1}, Lrd;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 392
    invoke-virtual {p0}, LvC;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 393
    const/4 v0, 0x1

    .line 396
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lrd;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 336
    invoke-super {p0}, Lrd;->onResume()V

    .line 338
    iget-object v0, p0, LvC;->a:LPp;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LPp;->a(Z)V

    .line 339
    return-void
.end method
