.class public LDH;
.super Landroid/widget/BaseAdapter;
.source "SortedMergeListAdapter.java"

# interfaces
.implements LDD;


# instance fields
.field private final a:Laku;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laku",
            "<",
            "LDE",
            "<",
            "LDD;",
            ">;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LDD;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LzN;LHp;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LzN;",
            "LHp;",
            "Ljava/util/List",
            "<",
            "LBo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 53
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, LbnG;->a(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LDH;->a:Ljava/util/List;

    .line 54
    new-instance v0, LDI;

    invoke-direct {v0, p0}, LDI;-><init>(LDH;)V

    .line 55
    invoke-static {v0}, Laku;->a(Lbjv;)Laku;

    move-result-object v0

    iput-object v0, p0, LDH;->a:Laku;

    .line 63
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 64
    new-instance v2, LDG;

    iget-object v0, p0, LDH;->a:Laku;

    invoke-direct {v2, p1, v0, v1}, LDG;-><init>(LzN;Lbjv;I)V

    .line 66
    new-instance v3, LDF;

    iget-object v0, p0, LDH;->a:Laku;

    invoke-direct {v3, p2, v0, v1}, LDF;-><init>(LHp;Lbjv;I)V

    .line 70
    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBo;

    .line 71
    invoke-interface {v0, v3, v2}, LBo;->a(LHp;LzN;)LDD;

    move-result-object v0

    .line 72
    iget-object v2, p0, LDH;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 74
    :cond_0
    return-void
.end method

.method static synthetic a(LDH;)Ljava/util/List;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, LDH;->a:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a(I)LIL;
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, LDH;->a:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDE;

    invoke-virtual {v0, p1}, LDE;->a(I)LDC;

    move-result-object v0

    check-cast v0, LDD;

    .line 164
    iget-object v1, p0, LDH;->a:Laku;

    invoke-virtual {v1}, Laku;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LDE;

    invoke-virtual {v1, v0, p1}, LDE;->a(LDC;I)I

    move-result v1

    .line 165
    invoke-interface {v0, v1}, LDD;->a(I)LIL;

    move-result-object v0

    return-object v0
.end method

.method public a(I)LIy;
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, LDH;->a:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDE;

    invoke-virtual {v0, p1}, LDE;->a(I)LDC;

    move-result-object v0

    check-cast v0, LDD;

    .line 157
    iget-object v1, p0, LDH;->a:Laku;

    invoke-virtual {v1}, Laku;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LDE;

    invoke-virtual {v1, v0, p1}, LDE;->a(LDC;I)I

    move-result v1

    .line 158
    invoke-interface {v0, v1}, LDD;->a(I)LIy;

    move-result-object v0

    return-object v0
.end method

.method public a(LQX;)V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, LDH;->a:Laku;

    invoke-virtual {v0}, Laku;->a()V

    .line 79
    iget-object v0, p0, LDH;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDD;

    .line 80
    invoke-interface {v0, p1}, LDD;->a(LQX;)V

    goto :goto_0

    .line 82
    :cond_0
    return-void
.end method

.method public a(LaFX;)V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, LDH;->a:Laku;

    invoke-virtual {v0}, Laku;->a()V

    .line 87
    iget-object v0, p0, LDH;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDD;

    .line 88
    invoke-interface {v0, p1}, LDD;->a(LaFX;)V

    goto :goto_0

    .line 90
    :cond_0
    if-eqz p1, :cond_1

    .line 92
    invoke-virtual {p0}, LDH;->notifyDataSetChanged()V

    .line 97
    :goto_1
    return-void

    .line 95
    :cond_1
    invoke-virtual {p0}, LDH;->notifyDataSetInvalidated()V

    goto :goto_1
.end method

.method public b()V
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, LDH;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDD;

    .line 109
    invoke-interface {v0}, LDD;->c()V

    goto :goto_0

    .line 111
    :cond_0
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, LDH;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDD;

    .line 102
    invoke-interface {v0}, LDD;->c()V

    goto :goto_0

    .line 104
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, LDH;->a:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDE;

    invoke-virtual {v0}, LDE;->a()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, LDH;->a:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDE;

    invoke-virtual {v0, p1}, LDE;->a(I)LDC;

    move-result-object v0

    check-cast v0, LDD;

    .line 121
    iget-object v1, p0, LDH;->a:Laku;

    invoke-virtual {v1}, Laku;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LDE;

    invoke-virtual {v1, v0, p1}, LDE;->a(LDC;I)I

    move-result v1

    .line 122
    invoke-interface {v0, v1}, LDD;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, LDH;->a:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDE;

    invoke-virtual {v0, p1}, LDE;->a(I)LDC;

    move-result-object v0

    check-cast v0, LDD;

    .line 128
    iget-object v1, p0, LDH;->a:Laku;

    invoke-virtual {v1}, Laku;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LDE;

    invoke-virtual {v1, v0, p1}, LDE;->a(LDC;I)I

    move-result v1

    .line 129
    invoke-interface {v0, v1}, LDD;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, LDH;->a:Laku;

    invoke-virtual {v0}, Laku;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDE;

    invoke-virtual {v0, p1}, LDE;->a(I)LDC;

    move-result-object v0

    check-cast v0, LDD;

    .line 135
    iget-object v1, p0, LDH;->a:Laku;

    invoke-virtual {v1}, Laku;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LDE;

    invoke-virtual {v1, v0, p1}, LDE;->a(LDC;I)I

    move-result v1

    .line 136
    invoke-interface {v0, v1, p2, p3}, LDD;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, LDH;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDD;

    .line 143
    invoke-interface {v0, p1, p2, p3, p4}, LDD;->onScroll(Landroid/widget/AbsListView;III)V

    goto :goto_0

    .line 145
    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, LDH;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDD;

    .line 150
    invoke-interface {v0, p1, p2}, LDD;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    goto :goto_0

    .line 152
    :cond_0
    return-void
.end method
