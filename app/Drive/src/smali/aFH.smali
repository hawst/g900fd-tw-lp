.class public final enum LaFH;
.super Ljava/lang/Enum;
.source "FieldDefinition.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaFH;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaFH;

.field private static final synthetic a:[LaFH;

.field public static final enum b:LaFH;

.field public static final enum c:LaFH;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 48
    new-instance v0, LaFH;

    const-string v1, "CASCADE"

    const-string v2, "CASCADE"

    invoke-direct {v0, v1, v3, v2}, LaFH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LaFH;->a:LaFH;

    .line 49
    new-instance v0, LaFH;

    const-string v1, "SET_NULL"

    const-string v2, "SET NULL"

    invoke-direct {v0, v1, v4, v2}, LaFH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LaFH;->b:LaFH;

    .line 50
    new-instance v0, LaFH;

    const-string v1, "RESTRICT"

    const-string v2, "RESTRICT"

    invoke-direct {v0, v1, v5, v2}, LaFH;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LaFH;->c:LaFH;

    .line 47
    const/4 v0, 0x3

    new-array v0, v0, [LaFH;

    sget-object v1, LaFH;->a:LaFH;

    aput-object v1, v0, v3

    sget-object v1, LaFH;->b:LaFH;

    aput-object v1, v0, v4

    sget-object v1, LaFH;->c:LaFH;

    aput-object v1, v0, v5

    sput-object v0, LaFH;->a:[LaFH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 55
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    iput-object p3, p0, LaFH;->a:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaFH;
    .locals 1

    .prologue
    .line 47
    const-class v0, LaFH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaFH;

    return-object v0
.end method

.method public static values()[LaFH;
    .locals 1

    .prologue
    .line 47
    sget-object v0, LaFH;->a:[LaFH;

    invoke-virtual {v0}, [LaFH;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaFH;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, LaFH;->a:Ljava/lang/String;

    return-object v0
.end method
