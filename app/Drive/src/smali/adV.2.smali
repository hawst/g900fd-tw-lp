.class public LadV;
.super Ljava/lang/Object;
.source "PipeExposer.java"

# interfaces
.implements LadA;


# instance fields
.field private final a:LadJ;


# direct methods
.method public constructor <init>(LadJ;)V
    .locals 1

    .prologue
    .line 98
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-direct {p0, p1, v0}, LadV;-><init>(LadJ;I)V

    .line 99
    return-void
.end method

.method constructor <init>(LadJ;I)V
    .locals 3

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    const/16 v0, 0x9

    if-ge p2, v0, :cond_0

    .line 104
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Gingerbread needed and have "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_0
    iput-object p1, p0, LadV;->a:LadJ;

    .line 108
    return-void
.end method

.method static synthetic a(LadV;)LadJ;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, LadV;->a:LadJ;

    return-object v0
.end method

.method public static a()Z
    .locals 2

    .prologue
    .line 114
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-ge v0, v1, :cond_0

    .line 115
    const/4 v0, 0x0

    .line 117
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/String;)LadC;
    .locals 1

    .prologue
    .line 124
    monitor-enter p0

    :try_start_0
    new-instance v0, LadW;

    invoke-direct {v0, p0, p1}, LadW;-><init>(LadV;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
