.class public Lamj;
.super Ljava/lang/Object;
.source "OcrImageEvaluator.java"


# instance fields
.field a:LNc;

.field a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field a:LalJ;

.field a:Lalo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    return-void
.end method

.method private a(Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 71
    iget-object v1, p0, Lamj;->a:LNc;

    invoke-virtual {v1}, LNc;->a()LNb;

    move-result-object v1

    const-string v2, "image/jpeg"

    .line 72
    invoke-virtual {v1, p1, v2}, LNb;->a(Landroid/net/Uri;Ljava/lang/String;)LNb;

    move-result-object v1

    .line 73
    invoke-virtual {v1, v3}, LNb;->a(Z)LNb;

    move-result-object v1

    invoke-virtual {v1}, LNb;->a()LMZ;

    move-result-object v1

    .line 76
    :try_start_0
    invoke-virtual {v1}, LMZ;->a()LMZ;
    :try_end_0
    .catch LMS; {:try_start_0 .. :try_end_0} :catch_0
    .catch LNh; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 87
    :try_start_1
    new-instance v1, Ljava/io/FileInputStream;

    invoke-virtual {v2}, LMZ;->a()Ljava/io/File;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 92
    const/4 v0, 0x1

    :try_start_2
    invoke-static {v1, v0}, Lakq;->a(Ljava/io/InputStream;I)Lakq;

    move-result-object v0

    invoke-virtual {v0}, Lakq;->a()Landroid/graphics/Bitmap;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 94
    invoke-virtual {v2}, LMZ;->close()V

    :goto_0
    return-object v0

    .line 77
    :catch_0
    move-exception v0

    .line 78
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "ItemToUpload hasn\'t been published, so how can it be canceled?"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 79
    :catch_1
    move-exception v1

    .line 80
    const-string v2, "OcrImageEvaluator"

    invoke-static {v2, v1}, LalV;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 88
    :catch_2
    move-exception v1

    .line 89
    :try_start_3
    const-string v3, "OcrImageEvaluator"

    invoke-static {v3, v1}, LalV;->a(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 94
    invoke-virtual {v2}, LMZ;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LMZ;->close()V

    throw v0
.end method


# virtual methods
.method public a(Landroid/net/Uri;)Lamk;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 51
    new-instance v0, Lbjs;

    invoke-direct {v0}, Lbjs;-><init>()V

    .line 53
    sget-object v1, Lamk;->a:Lamk;

    .line 54
    invoke-virtual {v0}, Lbjs;->a()Lbjs;

    .line 55
    invoke-direct {p0, p1}, Lamj;->a(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 56
    invoke-virtual {v0}, Lbjs;->b()Lbjs;

    .line 57
    const-string v2, "OcrImageEvaluator"

    const-string v3, "toBitmap took %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, LalV;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 58
    if-eqz v1, :cond_0

    .line 59
    invoke-virtual {v0}, Lbjs;->c()Lbjs;

    move-result-object v2

    invoke-virtual {v2}, Lbjs;->a()Lbjs;

    .line 60
    iget-object v2, p0, Lamj;->a:LalJ;

    invoke-virtual {v2, v1}, LalJ;->a(Landroid/graphics/Bitmap;)Z

    move-result v1

    .line 61
    invoke-virtual {v0}, Lbjs;->b()Lbjs;

    .line 62
    const-string v2, "OcrImageEvaluator"

    const-string v3, "isBlurred took %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, LalV;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 63
    if-eqz v1, :cond_0

    .line 64
    sget-object v0, Lamk;->b:Lamk;

    .line 67
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lamk;->a:Lamk;

    goto :goto_0
.end method
