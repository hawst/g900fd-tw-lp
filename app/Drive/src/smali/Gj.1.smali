.class public final enum LGj;
.super Ljava/lang/Enum;
.source "ExportType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LGj;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LGj;

.field private static final synthetic a:[LGj;

.field public static final enum b:LGj;

.field public static final enum c:LGj;

.field public static final enum d:LGj;

.field public static final enum e:LGj;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 9
    new-instance v0, LGj;

    const-string v1, "PDF"

    const-string v2, "pdf"

    const-string v3, "application/pdf"

    invoke-direct {v0, v1, v4, v2, v3}, LGj;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LGj;->a:LGj;

    .line 10
    new-instance v0, LGj;

    const-string v1, "PNG"

    const-string v2, "png"

    const-string v3, "image/png"

    invoke-direct {v0, v1, v5, v2, v3}, LGj;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LGj;->b:LGj;

    .line 11
    new-instance v0, LGj;

    const-string v1, "QO_DOC"

    const-string v2, "docx"

    const-string v3, "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

    invoke-direct {v0, v1, v6, v2, v3}, LGj;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LGj;->c:LGj;

    .line 12
    new-instance v0, LGj;

    const-string v1, "QO_PPT"

    const-string v2, "pptx"

    const-string v3, "application/vnd.openxmlformats-officedocument.presentationml.presentation"

    invoke-direct {v0, v1, v7, v2, v3}, LGj;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LGj;->d:LGj;

    .line 13
    new-instance v0, LGj;

    const-string v1, "QO_XLS"

    const-string v2, "xlsx"

    const-string v3, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    invoke-direct {v0, v1, v8, v2, v3}, LGj;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LGj;->e:LGj;

    .line 8
    const/4 v0, 0x5

    new-array v0, v0, [LGj;

    sget-object v1, LGj;->a:LGj;

    aput-object v1, v0, v4

    sget-object v1, LGj;->b:LGj;

    aput-object v1, v0, v5

    sget-object v1, LGj;->c:LGj;

    aput-object v1, v0, v6

    sget-object v1, LGj;->d:LGj;

    aput-object v1, v0, v7

    sget-object v1, LGj;->e:LGj;

    aput-object v1, v0, v8

    sput-object v0, LGj;->a:[LGj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 19
    iput-object p3, p0, LGj;->a:Ljava/lang/String;

    .line 20
    iput-object p4, p0, LGj;->b:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LGj;
    .locals 1

    .prologue
    .line 8
    const-class v0, LGj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LGj;

    return-object v0
.end method

.method public static values()[LGj;
    .locals 1

    .prologue
    .line 8
    sget-object v0, LGj;->a:[LGj;

    invoke-virtual {v0}, [LGj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LGj;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, LGj;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, LGj;->b:Ljava/lang/String;

    return-object v0
.end method
