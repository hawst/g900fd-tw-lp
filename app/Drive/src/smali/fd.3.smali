.class Lfd;
.super Lfb;
.source "ViewPropertyAnimatorCompat.java"


# instance fields
.field a:Lfn;

.field e:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 334
    invoke-direct {p0}, Lfb;-><init>()V

    .line 335
    const/4 v0, 0x0

    iput-object v0, p0, Lfd;->e:Ljava/util/WeakHashMap;

    .line 508
    new-instance v0, Lfe;

    invoke-direct {v0, p0}, Lfe;-><init>(Lfd;)V

    iput-object v0, p0, Lfd;->a:Lfn;

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 434
    invoke-static {p1}, Lfj;->a(Landroid/view/View;)V

    .line 435
    return-void
.end method

.method public a(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 344
    invoke-static {p1, p2}, Lfj;->a(Landroid/view/View;F)V

    .line 345
    return-void
.end method

.method public a(Landroid/view/View;J)V
    .locals 0

    .prologue
    .line 339
    invoke-static {p1, p2, p3}, Lfj;->a(Landroid/view/View;J)V

    .line 340
    return-void
.end method

.method public a(Landroid/view/View;Landroid/view/animation/Interpolator;)V
    .locals 0

    .prologue
    .line 364
    invoke-static {p1, p2}, Lfj;->a(Landroid/view/View;Landroid/view/animation/Interpolator;)V

    .line 365
    return-void
.end method

.method public a(Landroid/view/View;Lfn;)V
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Lfd;->d:Ljava/util/WeakHashMap;

    if-nez v0, :cond_0

    .line 475
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lfd;->d:Ljava/util/WeakHashMap;

    .line 477
    :cond_0
    iget-object v0, p0, Lfd;->d:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 478
    iget-object v0, p0, Lfd;->a:Lfn;

    invoke-static {p1, v0}, Lfj;->a(Landroid/view/View;Lfn;)V

    .line 479
    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 469
    invoke-static {p1}, Lfj;->b(Landroid/view/View;)V

    .line 470
    return-void
.end method

.method public b(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 349
    invoke-static {p1, p2}, Lfj;->b(Landroid/view/View;F)V

    .line 350
    return-void
.end method

.method public c(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 354
    invoke-static {p1, p2}, Lfj;->c(Landroid/view/View;F)V

    .line 355
    return-void
.end method

.method public d(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 424
    invoke-static {p1, p2}, Lfj;->d(Landroid/view/View;F)V

    .line 425
    return-void
.end method
