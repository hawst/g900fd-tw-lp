.class public LBv;
.super Ljava/lang/Object;
.source "DocListModule.java"

# interfaces
.implements LbuC;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    return-void
.end method


# virtual methods
.method public a(Lcom/google/inject/Binder;)V
    .locals 2

    .prologue
    .line 59
    const-class v0, LsC;

    const-class v1, LQP;

    .line 60
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 59
    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Lbuv;)LbvX;

    .line 61
    const-class v0, LSv;

    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Ljava/lang/Class;)LbvX;

    .line 62
    new-instance v0, LBw;

    invoke-direct {v0, p0}, LBw;-><init>(LBv;)V

    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;LbuP;)LbvX;

    .line 64
    const-class v0, LaGP;

    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Ljava/lang/Class;)LbvX;

    .line 65
    const-class v0, LBf;

    const-class v1, LQR;

    .line 66
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 65
    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Lbuv;)LbvX;

    .line 67
    const-class v0, LCo;

    const-class v1, LQJ;

    .line 68
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 67
    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Lbuv;)LbvX;

    .line 69
    const-class v0, LAX;

    const-class v1, LQP;

    .line 70
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 69
    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Lbuv;)LbvX;

    .line 71
    const-class v0, LzQ;

    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Ljava/lang/Class;)LbvX;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, LbvX;->a()LbuT;

    move-result-object v0

    const-class v1, LzR;

    invoke-interface {v0, v1}, LbuT;->a(Ljava/lang/Class;)LbuU;

    .line 73
    const-class v0, Laqw;

    const-class v1, LQP;

    .line 74
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 73
    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Lbuv;)LbvX;

    .line 75
    const-class v0, LsI;

    const-class v1, LQJ;

    .line 76
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    .line 75
    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Lbuv;)LbvX;

    .line 77
    return-void
.end method

.method provideActionMenuHelper(Landroid/content/Context;)LCW;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 148
    const-class v0, LCW;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCW;

    return-object v0
.end method

.method provideActionMenuListener(Landroid/content/Context;)LCX;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 142
    const-class v0, LCX;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCX;

    return-object v0
.end method

.method provideActivityHelper(Landroid/content/Context;)LAQ;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 136
    const-class v0, LAQ;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LAQ;

    return-object v0
.end method

.method provideArrangementModeHelper(LzR;)LzQ;
    .locals 0
    .annotation runtime LQN;
    .end annotation

    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 259
    return-object p1
.end method

.method provideCompoundDocEntriesAdapter(LAa;)LzZ;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 100
    return-object p1
.end method

.method provideCriterionSetFactory(LvP;)LvO;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 203
    return-object p1
.end method

.method provideDocListAppConfiguration(LsD;)LsC;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 190
    invoke-virtual {p1}, LsD;->a()LsC;

    move-result-object v0

    return-object v0
.end method

.method provideDocListController(LAR;)LAP;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 130
    return-object p1
.end method

.method provideDocListCursorsContainerLoader(LAY;)LAX;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 243
    return-object p1
.end method

.method provideDocListEntrySyncState(LbiP;LBi;)LBf;
    .locals 1
    .param p1    # LbiP;
        .annotation runtime LQR;
        .end annotation
    .end param
    .annotation runtime LbuF;
    .end annotation

    .annotation runtime Lbxz;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbiP",
            "<",
            "LBf;",
            ">;",
            "LBi;",
            ")",
            "LBf;"
        }
    .end annotation

    .prologue
    .line 252
    invoke-virtual {p1, p2}, LbiP;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBf;

    return-object v0
.end method

.method provideDocListView(Landroid/content/Context;)Lcom/google/android/apps/docs/view/DocListView;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 118
    const-class v0, Lcom/google/android/apps/docs/view/DocListView;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/DocListView;

    return-object v0
.end method

.method provideDocumentCreatorIntentFactory(Lsb;)LsI;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 210
    invoke-virtual {p1}, Lsb;->a()LsI;

    move-result-object v0

    return-object v0
.end method

.method provideDocumentPinActionHelper(LCa;)LBZ;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 124
    return-object p1
.end method

.method provideDocumentPreviewActivity(Landroid/content/Context;)Lcom/google/android/apps/docs/app/DocumentPreviewActivity;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 265
    invoke-static {p1}, Lajt;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    return-object v0
.end method

.method provideEntriesFilter(LBx;)LCl;
    .locals 1
    .annotation runtime LQL;
    .end annotation

    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 197
    invoke-virtual {p1}, LBx;->a()LCl;

    move-result-object v0

    return-object v0
.end method

.method provideEntriesFilterCollection(LCp;)LCo;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 223
    invoke-virtual {p1}, LCp;->a()LCo;

    move-result-object v0

    return-object v0
.end method

.method provideEntryFilter(Landroid/content/Context;)LCr;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 184
    const-class v0, LCr;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCr;

    return-object v0
.end method

.method provideEntryOpenerListener(Landroid/content/Context;)LtE;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 172
    const-class v0, LtE;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtE;

    return-object v0
.end method

.method provideListener(Landroid/content/Context;)LFB;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 229
    const-class v0, LFB;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LFB;

    return-object v0
.end method

.method provideMoreActionsButtonController(Lul;)Laqw;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 236
    invoke-virtual {p1}, Lul;->a()Laqw;

    move-result-object v0

    return-object v0
.end method

.method provideNavigationSelectionListener(LAP;)LRJ;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 106
    return-object p1
.end method

.method provideOnCloseListener(Landroid/content/Context;)LPo;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 160
    const-class v0, LPo;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPo;

    return-object v0
.end method

.method provideOnDetailFragmentCloseListener(Landroid/content/Context;)LQU;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 166
    const-class v0, LQU;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQU;

    return-object v0
.end method

.method provideSearchSuggestionRestrictionHelper(LCQ;)LCP;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 217
    return-object p1
.end method

.method provideSharingInfoListener(Landroid/content/Context;)LRA;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 154
    const-class v0, LRA;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LRA;

    return-object v0
.end method

.method provideSingleTapListener(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;)LaqL;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 178
    return-object p1
.end method

.method provideThumbnailOpenEntryListener(Landroid/content/Context;)LaqQ;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 112
    const-class v0, LaqQ;

    invoke-static {p1, v0}, LtP;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaqQ;

    return-object v0
.end method
