.class public final LaNI;
.super Ljava/lang/Object;

# interfaces
.implements LaNL;


# instance fields
.field private a:I

.field private final a:J

.field private final a:LaNE;

.field private final a:LaNO;

.field private a:LaNR;

.field private final a:Landroid/content/Context;

.field private final a:Lcom/google/android/gms/internal/an;

.field private final a:Lcom/google/android/gms/internal/aq;

.field private final a:Lcom/google/android/gms/internal/fa;

.field private final a:Ljava/lang/Object;

.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;LaNO;LaNF;LaNE;Lcom/google/android/gms/internal/an;Lcom/google/android/gms/internal/aq;Lcom/google/android/gms/internal/fa;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LaNI;->a:Ljava/lang/Object;

    const/4 v0, -0x2

    iput v0, p0, LaNI;->a:I

    iput-object p1, p0, LaNI;->a:Landroid/content/Context;

    const-string v0, "com.google.ads.mediation.customevent.CustomEventAdapter"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p5}, LaNI;->a(LaNE;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaNI;->a:Ljava/lang/String;

    :goto_0
    iput-object p3, p0, LaNI;->a:LaNO;

    iget-wide v0, p4, LaNF;->a:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-wide v0, p4, LaNF;->a:J

    :goto_1
    iput-wide v0, p0, LaNI;->a:J

    iput-object p5, p0, LaNI;->a:LaNE;

    iput-object p6, p0, LaNI;->a:Lcom/google/android/gms/internal/an;

    iput-object p7, p0, LaNI;->a:Lcom/google/android/gms/internal/aq;

    iput-object p8, p0, LaNI;->a:Lcom/google/android/gms/internal/fa;

    return-void

    :cond_0
    iput-object p2, p0, LaNI;->a:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-wide/16 v0, 0x2710

    goto :goto_1
.end method

.method static synthetic a(LaNI;)I
    .locals 1

    iget v0, p0, LaNI;->a:I

    return v0
.end method

.method private a()LaNR;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Instantiating mediation adapter: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaNI;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaQc;->c(Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, LaNI;->a:LaNO;

    iget-object v1, p0, LaNI;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LaNO;->a(Ljava/lang/String;)LaNR;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not instantiate mediation adapter: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LaNI;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, LaQc;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(LaNI;)LaNR;
    .locals 1

    invoke-direct {p0}, LaNI;->a()LaNR;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LaNI;LaNR;)LaNR;
    .locals 0

    iput-object p1, p0, LaNI;->a:LaNR;

    return-object p1
.end method

.method static synthetic a(LaNI;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, LaNI;->a:Ljava/lang/Object;

    return-object v0
.end method

.method private a(LaNE;)Ljava/lang/String;
    .locals 3

    :try_start_0
    iget-object v0, p1, LaNE;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LaNE;->d:Ljava/lang/String;

    const/4 v1, 0x0

    const-class v2, LaNI;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-static {v0, v1, v2}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v0

    const-class v1, LayI;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.google.android.gms.ads.mediation.customevent.CustomEventAdapter"
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "Could not create custom event adapter."

    invoke-static {v0}, LaQc;->e(Ljava/lang/String;)V

    :cond_0
    const-string v0, "com.google.ads.mediation.customevent.CustomEventAdapter"

    goto :goto_0
.end method

.method private a(JJJJ)V
    .locals 3

    :goto_0
    iget v0, p0, LaNI;->a:I

    const/4 v1, -0x2

    if-eq v0, v1, :cond_0

    return-void

    :cond_0
    invoke-direct/range {p0 .. p8}, LaNI;->b(JJJJ)V

    goto :goto_0
.end method

.method private a(LaNH;)V
    .locals 7

    :try_start_0
    iget-object v0, p0, LaNI;->a:Lcom/google/android/gms/internal/fa;

    iget v0, v0, Lcom/google/android/gms/internal/fa;->c:I

    const v1, 0x3e8fa0

    if-ge v0, v1, :cond_1

    iget-object v0, p0, LaNI;->a:Lcom/google/android/gms/internal/aq;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/aq;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LaNI;->a:LaNR;

    iget-object v1, p0, LaNI;->a:Landroid/content/Context;

    invoke-static {v1}, LaLp;->a(Ljava/lang/Object;)LaLm;

    move-result-object v1

    iget-object v2, p0, LaNI;->a:Lcom/google/android/gms/internal/an;

    iget-object v3, p0, LaNI;->a:LaNE;

    iget-object v3, v3, LaNE;->e:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, p1}, LaNR;->a(LaLm;Lcom/google/android/gms/internal/an;Ljava/lang/String;LaNU;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LaNI;->a:LaNR;

    iget-object v1, p0, LaNI;->a:Landroid/content/Context;

    invoke-static {v1}, LaLp;->a(Ljava/lang/Object;)LaLm;

    move-result-object v1

    iget-object v2, p0, LaNI;->a:Lcom/google/android/gms/internal/aq;

    iget-object v3, p0, LaNI;->a:Lcom/google/android/gms/internal/an;

    iget-object v4, p0, LaNI;->a:LaNE;

    iget-object v4, v4, LaNE;->e:Ljava/lang/String;

    move-object v5, p1

    invoke-interface/range {v0 .. v5}, LaNR;->a(LaLm;Lcom/google/android/gms/internal/aq;Lcom/google/android/gms/internal/an;Ljava/lang/String;LaNU;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Could not request ad from mediation adapter."

    invoke-static {v1, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, LaNI;->a(I)V

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v0, p0, LaNI;->a:Lcom/google/android/gms/internal/aq;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/aq;->a:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LaNI;->a:LaNR;

    iget-object v1, p0, LaNI;->a:Landroid/content/Context;

    invoke-static {v1}, LaLp;->a(Ljava/lang/Object;)LaLm;

    move-result-object v1

    iget-object v2, p0, LaNI;->a:Lcom/google/android/gms/internal/an;

    iget-object v3, p0, LaNI;->a:LaNE;

    iget-object v3, v3, LaNE;->e:Ljava/lang/String;

    iget-object v4, p0, LaNI;->a:LaNE;

    iget-object v4, v4, LaNE;->a:Ljava/lang/String;

    move-object v5, p1

    invoke-interface/range {v0 .. v5}, LaNR;->a(LaLm;Lcom/google/android/gms/internal/an;Ljava/lang/String;Ljava/lang/String;LaNU;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, LaNI;->a:LaNR;

    iget-object v1, p0, LaNI;->a:Landroid/content/Context;

    invoke-static {v1}, LaLp;->a(Ljava/lang/Object;)LaLm;

    move-result-object v1

    iget-object v2, p0, LaNI;->a:Lcom/google/android/gms/internal/aq;

    iget-object v3, p0, LaNI;->a:Lcom/google/android/gms/internal/an;

    iget-object v4, p0, LaNI;->a:LaNE;

    iget-object v4, v4, LaNE;->e:Ljava/lang/String;

    iget-object v5, p0, LaNI;->a:LaNE;

    iget-object v5, v5, LaNE;->a:Ljava/lang/String;

    move-object v6, p1

    invoke-interface/range {v0 .. v6}, LaNR;->a(LaLm;Lcom/google/android/gms/internal/aq;Lcom/google/android/gms/internal/an;Ljava/lang/String;Ljava/lang/String;LaNU;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method static synthetic a(LaNI;LaNH;)V
    .locals 0

    invoke-direct {p0, p1}, LaNI;->a(LaNH;)V

    return-void
.end method

.method static synthetic b(LaNI;)LaNR;
    .locals 1

    iget-object v0, p0, LaNI;->a:LaNR;

    return-object v0
.end method

.method private b(JJJJ)V
    .locals 7

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long v2, v0, p1

    sub-long v2, p3, v2

    sub-long/2addr v0, p5

    sub-long v0, p7, v0

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-gtz v4, :cond_1

    :cond_0
    const-string v0, "Timed out waiting for adapter."

    invoke-static {v0}, LaQc;->c(Ljava/lang/String;)V

    const/4 v0, 0x3

    iput v0, p0, LaNI;->a:I

    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v4, p0, LaNI;->a:Ljava/lang/Object;

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    invoke-virtual {v4, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v0, -0x1

    iput v0, p0, LaNI;->a:I

    goto :goto_0
.end method


# virtual methods
.method public a(JJ)LaNK;
    .locals 13

    iget-object v10, p0, LaNI;->a:Ljava/lang/Object;

    monitor-enter v10

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    new-instance v11, LaNH;

    invoke-direct {v11}, LaNH;-><init>()V

    sget-object v0, LaQb;->a:Landroid/os/Handler;

    new-instance v1, LaNJ;

    invoke-direct {v1, p0, v11}, LaNJ;-><init>(LaNI;LaNH;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-wide v4, p0, LaNI;->a:J

    move-object v1, p0

    move-wide v6, p1

    move-wide/from16 v8, p3

    invoke-direct/range {v1 .. v9}, LaNI;->a(JJJJ)V

    new-instance v0, LaNK;

    iget-object v1, p0, LaNI;->a:LaNE;

    iget-object v2, p0, LaNI;->a:LaNR;

    iget-object v3, p0, LaNI;->a:Ljava/lang/String;

    iget v5, p0, LaNI;->a:I

    move-object v4, v11

    invoke-direct/range {v0 .. v5}, LaNK;-><init>(LaNE;LaNR;Ljava/lang/String;LaNH;I)V

    monitor-exit v10

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a()V
    .locals 3

    iget-object v1, p0, LaNI;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LaNI;->a:LaNR;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaNI;->a:LaNR;

    invoke-interface {v0}, LaNR;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    const/4 v0, -0x1

    :try_start_1
    iput v0, p0, LaNI;->a:I

    iget-object v0, p0, LaNI;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1

    return-void

    :catch_0
    move-exception v0

    const-string v2, "Could not destroy mediation adapter."

    invoke-static {v2, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(I)V
    .locals 2

    iget-object v1, p0, LaNI;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput p1, p0, LaNI;->a:I

    iget-object v0, p0, LaNI;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
