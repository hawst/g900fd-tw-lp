.class public final LaHC;
.super LaHp;
.source "RemoveSharedWithMeOp.java"


# instance fields
.field private final a:Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

.field private final a:Z


# direct methods
.method public constructor <init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LaHC;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 48
    return-void
.end method

.method private constructor <init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V
    .locals 1

    .prologue
    .line 52
    const-string v0, "unsubscribe"

    invoke-direct {p0, p1, p2, v0}, LaHp;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Ljava/lang/String;)V

    .line 53
    check-cast p3, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    iput-object p3, p0, LaHC;->a:Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    .line 54
    iput-boolean p4, p0, LaHC;->a:Z

    .line 55
    return-void
.end method

.method public static a(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Lorg/json/JSONObject;)LaHC;
    .locals 4

    .prologue
    .line 59
    iget-object v1, p1, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;->a:LaFO;

    .line 60
    const/4 v0, 0x0

    .line 61
    const-string v2, "folderEntrySqlId"

    invoke-virtual {p2, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 62
    const-string v0, "folderEntrySqlId"

    .line 63
    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;->a(LaFO;J)Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v0

    .line 65
    :cond_0
    const-string v1, "isUndo"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 66
    new-instance v2, LaHC;

    invoke-direct {v2, p0, p1, v0, v1}, LaHC;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    return-object v2
.end method

.method private b(LaGe;)LaHo;
    .locals 5

    .prologue
    .line 134
    new-instance v0, LaHC;

    iget-object v1, p0, LaHC;->a:LaGg;

    .line 135
    invoke-virtual {p1}, LaGe;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v2

    iget-object v3, p0, LaHC;->a:Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, LaHC;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 137
    iget-object v1, p0, LaHC;->a:Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    if-eqz v1, :cond_1

    .line 138
    iget-object v1, p0, LaHC;->a:LaGg;

    iget-object v2, p0, LaHC;->a:Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    invoke-interface {v1, v2}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFZ;

    move-result-object v1

    .line 139
    if-eqz v1, :cond_0

    .line 140
    iget-object v2, p0, LaHC;->a:LaGg;

    .line 141
    invoke-virtual {p1}, LaGe;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v3

    .line 140
    invoke-interface {v2, v3, v1}, LaGg;->a(Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;LaFZ;)LaGB;

    move-result-object v1

    .line 142
    invoke-virtual {v1}, LaGB;->e()V

    .line 148
    :cond_0
    :goto_0
    return-object v0

    .line 145
    :cond_1
    invoke-virtual {p1}, LaGe;->d()V

    goto :goto_0
.end method


# virtual methods
.method public a(LaGe;)LaHo;
    .locals 5

    .prologue
    .line 108
    iget-boolean v0, p0, LaHC;->a:Z

    if-eqz v0, :cond_0

    .line 109
    invoke-direct {p0, p1}, LaHC;->b(LaGe;)LaHo;

    move-result-object v0

    .line 129
    :goto_0
    return-object v0

    .line 111
    :cond_0
    new-instance v1, LaHC;

    iget-object v0, p0, LaHC;->a:LaGg;

    .line 112
    invoke-virtual {p1}, LaGe;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v2

    iget-object v3, p0, LaHC;->a:Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    const/4 v4, 0x1

    invoke-direct {v1, v0, v2, v3, v4}, LaHC;-><init>(LaGg;Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V

    .line 114
    iget-object v0, p0, LaHC;->a:Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    if-eqz v0, :cond_2

    .line 115
    iget-object v0, p0, LaHC;->a:LaGg;

    .line 116
    invoke-virtual {p1}, LaGe;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v2

    invoke-interface {v0, v2}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Ljava/util/Map;

    move-result-object v0

    .line 117
    iget-object v2, p0, LaHC;->a:LaGg;

    iget-object v3, p0, LaHC;->a:Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    invoke-interface {v2, v3}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFZ;

    move-result-object v2

    .line 118
    if-eqz v2, :cond_1

    .line 119
    invoke-virtual {v2}, LaFZ;->a()J

    move-result-wide v2

    .line 120
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGB;

    .line 121
    if-eqz v0, :cond_1

    .line 122
    invoke-virtual {v0}, LaGB;->f()V

    :cond_1
    :goto_1
    move-object v0, v1

    .line 129
    goto :goto_0

    .line 126
    :cond_2
    invoke-virtual {p1}, LaGe;->b()V

    goto :goto_1
.end method

.method public a()Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 153
    invoke-super {p0}, LaHp;->a()Lorg/json/JSONObject;

    move-result-object v0

    .line 154
    const-string v1, "operationName"

    const-string v2, "unsubscribe"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 155
    iget-object v1, p0, LaHC;->a:Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    if-eqz v1, :cond_0

    .line 156
    const-string v1, "folderEntrySqlId"

    iget-object v2, p0, LaHC;->a:Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 158
    :cond_0
    const-string v1, "isUndo"

    iget-boolean v2, p0, LaHC;->a:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 159
    return-object v0
.end method

.method public a(LaHy;LaHD;Lcom/google/android/gms/drive/database/data/ResourceSpec;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 72
    iget-boolean v2, p0, LaHC;->a:Z

    if-eqz v2, :cond_0

    .line 73
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 75
    :cond_0
    iget-object v2, p0, LaHC;->a:Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    if-nez v2, :cond_3

    .line 76
    invoke-virtual {p2}, LaHD;->a()LaHE;

    move-result-object v2

    const/4 v3, 0x0

    const-string v4, "*"

    invoke-interface {v2, p3, v3, v4, p1}, LaHE;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;LaHy;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 77
    iget-object v1, p0, LaHC;->a:LaGg;

    invoke-interface {v1}, LaGg;->a()V

    .line 79
    :try_start_0
    iget-object v1, p0, LaHC;->a:LaGg;

    iget-object v2, p0, LaHC;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v1, v2}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;

    move-result-object v1

    .line 80
    if-eqz v1, :cond_1

    .line 81
    invoke-virtual {v1}, LaGd;->a()LaGe;

    move-result-object v1

    invoke-virtual {v1}, LaGe;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    :cond_1
    iget-object v1, p0, LaHC;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    .line 100
    :goto_0
    return v0

    .line 84
    :catchall_0
    move-exception v0

    iget-object v1, p0, LaHC;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    throw v0

    :cond_2
    move v0, v1

    .line 88
    goto :goto_0

    .line 91
    :cond_3
    iget-object v2, p0, LaHC;->a:LaGg;

    iget-object v3, p0, LaHC;->a:Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    invoke-interface {v2, v3}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFZ;

    move-result-object v2

    .line 92
    if-eqz v2, :cond_4

    invoke-interface {v2}, LaFV;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 93
    invoke-virtual {p2}, LaHD;->a()LaHE;

    move-result-object v0

    .line 94
    invoke-interface {v2}, LaFV;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v1

    const-string v2, "*"

    .line 93
    invoke-interface {v0, p3, v1, v2, p1}, LaHE;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;LaHy;)Z

    move-result v0

    goto :goto_0

    .line 96
    :cond_4
    const-string v3, "RemoveSharedWithMeOp"

    const-string v4, "RemoveSharedWithMeOp can not be applied on the server because the collection provided is not found or is local only: EntrySpec: %s, FolderEntrySpec: %s, folder: %s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, LaHC;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    aput-object v6, v5, v1

    iget-object v1, p0, LaHC;->a:Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    aput-object v1, v5, v0

    const/4 v1, 0x2

    aput-object v2, v5, v1

    invoke-static {v3, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 164
    instance-of v1, p1, LaHC;

    if-nez v1, :cond_1

    .line 168
    :cond_0
    :goto_0
    return v0

    .line 167
    :cond_1
    check-cast p1, LaHC;

    .line 168
    invoke-virtual {p0, p1}, LaHC;->a(LaHp;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LaHC;->a:Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    iget-object v2, p1, LaHC;->a:Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    invoke-static {v1, v2}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, LaHC;->a:Z

    iget-boolean v2, p1, LaHC;->a:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 174
    iget-object v0, p0, LaHC;->a:Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    if-nez v0, :cond_0

    .line 175
    invoke-virtual {p0}, LaHC;->b()I

    move-result v0

    .line 177
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LaHC;->b()I

    move-result v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LaHC;->a:Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-boolean v3, p0, LaHC;->a:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, LbiL;->a([Ljava/lang/Object;)I

    move-result v1

    mul-int/lit8 v1, v1, 0x11

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 183
    iget-object v0, p0, LaHC;->a:Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    if-nez v0, :cond_0

    const-string v0, ""

    .line 184
    :goto_0
    iget-boolean v1, p0, LaHC;->a:Z

    if-eqz v1, :cond_1

    const-string v1, "undo, "

    .line 185
    :goto_1
    const-string v2, "%sremove%s[%s]"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v1, 0x1

    aput-object v0, v3, v1

    const/4 v0, 0x2

    invoke-virtual {p0}, LaHC;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 183
    :cond_0
    const-string v0, " from folder"

    goto :goto_0

    .line 184
    :cond_1
    const-string v1, ""

    goto :goto_1
.end method
