.class public LzR;
.super Ljava/lang/Object;
.source "ArrangementModeHelperImpl.java"

# interfaces
.implements LzQ;


# instance fields
.field private final a:LtK;


# direct methods
.method public constructor <init>(LtK;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, LzR;->a:LtK;

    .line 22
    return-void
.end method


# virtual methods
.method public a(LCl;LaFO;)LbmY;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LCl;",
            "LaFO;",
            ")",
            "LbmY",
            "<",
            "LzO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    sget-object v0, LCe;->o:LCe;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LzR;->a:LtK;

    sget-object v1, Lry;->P:Lry;

    .line 28
    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    sget-object v0, LzO;->c:LzO;

    invoke-static {v0}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v0

    .line 31
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LzO;->b:LzO;

    sget-object v1, LzO;->a:LzO;

    invoke-static {v0, v1}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmY;

    move-result-object v0

    goto :goto_0
.end method

.method public a()LzO;
    .locals 1

    .prologue
    .line 51
    sget-object v0, LzO;->b:LzO;

    return-object v0
.end method

.method public a(LpU;LzO;)LzO;
    .locals 6

    .prologue
    .line 37
    const-string v0, "docListViewArrangementMode"

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LpU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 39
    if-eqz v2, :cond_0

    .line 40
    invoke-static {}, LzO;->values()[LzO;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v0, v3, v1

    .line 41
    invoke-virtual {v0}, LzO;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    move-object p2, v0

    .line 46
    :cond_0
    return-object p2

    .line 40
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
