.class final LaTs;
.super LaTe;


# instance fields
.field private final a:LaDk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaDk",
            "<",
            "LaZi;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic a:LaTo;


# direct methods
.method public constructor <init>(LaTo;LaDk;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaDk",
            "<",
            "LaZi;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, LaTs;->a:LaTo;

    invoke-direct {p0}, LaTe;-><init>()V

    iput-object p2, p0, LaTs;->a:LaDk;

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 6

    const/4 v0, 0x0

    invoke-static {}, LaTx;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "PeopleClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bundle callback: status="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\nresolution="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\nbundle="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LaTx;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {p1, v0, p2}, LaTo;->a(ILjava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    if-nez p3, :cond_1

    :goto_0
    iget-object v2, p0, LaTs;->a:LaTo;

    new-instance v3, LaTt;

    iget-object v4, p0, LaTs;->a:LaTo;

    iget-object v5, p0, LaTs;->a:LaDk;

    invoke-direct {v3, v4, v5, v1, v0}, LaTt;-><init>(LaTo;LaDk;Lcom/google/android/gms/common/api/Status;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LaTo;->b(LaRy;)V

    return-void

    :cond_1
    const-string v0, "avatarurl"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
