.class public Lbnb;
.super LbmS;
.source "ImmutableSetMultimap.java"

# interfaces
.implements LbpT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LbmS",
        "<TK;TV;>;",
        "LbpT",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final transient a:Lbnk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbnk",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LbmL;ILjava/util/Comparator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmL",
            "<TK;",
            "LbmY",
            "<TV;>;>;I",
            "Ljava/util/Comparator",
            "<-TV;>;)V"
        }
    .end annotation

    .prologue
    .line 347
    invoke-direct {p0, p1, p2}, LbmS;-><init>(LbmL;I)V

    .line 348
    if-nez p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lbnb;->a:Lbnk;

    .line 350
    return-void

    .line 348
    :cond_0
    invoke-static {p3}, Lbnk;->a(Ljava/util/Comparator;)Lbnk;

    move-result-object v0

    goto :goto_0
.end method

.method public static a()Lbnb;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Lbnb",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 71
    sget-object v0, Lbmh;->a:Lbmh;

    return-object v0
.end method

.method public static a(Lbph;)Lbnb;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lbph",
            "<+TK;+TV;>;)",
            "Lbnb",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 306
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lbnb;->b(Lbph;Ljava/util/Comparator;)Lbnb;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lbph;Ljava/util/Comparator;)Lbnb;
    .locals 1

    .prologue
    .line 64
    invoke-static {p0, p1}, Lbnb;->b(Lbph;Ljava/util/Comparator;)Lbnb;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)Lbnb;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(TK;TV;)",
            "Lbnb",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 78
    invoke-static {}, Lbnb;->a()Lbnc;

    move-result-object v0

    .line 79
    invoke-virtual {v0, p0, p1}, Lbnc;->a(Ljava/lang/Object;Ljava/lang/Object;)Lbnc;

    .line 80
    invoke-virtual {v0}, Lbnc;->a()Lbnb;

    move-result-object v0

    return-object v0
.end method

.method public static a()Lbnc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Lbnc",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 144
    new-instance v0, Lbnc;

    invoke-direct {v0}, Lbnc;-><init>()V

    return-object v0
.end method

.method private static b(Lbph;Ljava/util/Comparator;)Lbnb;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lbph",
            "<+TK;+TV;>;",
            "Ljava/util/Comparator",
            "<-TV;>;)",
            "Lbnb",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 311
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    invoke-interface {p0}, Lbph;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 313
    invoke-static {}, Lbnb;->a()Lbnb;

    move-result-object v0

    .line 339
    :cond_0
    :goto_0
    return-object v0

    .line 316
    :cond_1
    instance-of v0, p0, Lbnb;

    if-eqz v0, :cond_2

    move-object v0, p0

    .line 319
    check-cast v0, Lbnb;

    .line 320
    invoke-virtual {v0}, Lbnb;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 325
    :cond_2
    invoke-static {}, LbmL;->a()LbmM;

    move-result-object v2

    .line 326
    const/4 v0, 0x0

    .line 328
    invoke-interface {p0}, Lbph;->b()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 329
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    .line 330
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 331
    if-nez p1, :cond_3

    invoke-static {v0}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v0

    .line 333
    :goto_2
    invoke-virtual {v0}, LbmY;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_5

    .line 334
    invoke-virtual {v2, v4, v0}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    .line 335
    invoke-virtual {v0}, LbmY;->size()I

    move-result v0

    add-int/2addr v0, v1

    :goto_3
    move v1, v0

    .line 337
    goto :goto_1

    .line 331
    :cond_3
    invoke-static {p1, v0}, Lbnk;->a(Ljava/util/Comparator;Ljava/util/Collection;)Lbnk;

    move-result-object v0

    goto :goto_2

    .line 339
    :cond_4
    new-instance v0, Lbnb;

    invoke-virtual {v2}, LbmM;->a()LbmL;

    move-result-object v2

    invoke-direct {v0, v2, v1, p1}, Lbnb;-><init>(LbmL;ILjava/util/Comparator;)V

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_3
.end method


# virtual methods
.method public a(Ljava/lang/Object;)LbmY;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "LbmY",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 363
    iget-object v0, p0, Lbnb;->a:LbmL;

    invoke-virtual {v0, p1}, LbmL;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbmY;

    .line 364
    if-eqz v0, :cond_0

    .line 369
    :goto_0
    return-object v0

    .line 366
    :cond_0
    iget-object v0, p0, Lbnb;->a:Lbnk;

    if-eqz v0, :cond_1

    .line 367
    iget-object v0, p0, Lbnb;->a:Lbnk;

    goto :goto_0

    .line 369
    :cond_1
    invoke-static {}, LbmY;->a()LbmY;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Lbmv;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lbnb;->a(Ljava/lang/Object;)LbmY;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lbnb;->a(Ljava/lang/Object;)LbmY;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lbnb;->a(Ljava/lang/Object;)LbmY;

    move-result-object v0

    return-object v0
.end method
