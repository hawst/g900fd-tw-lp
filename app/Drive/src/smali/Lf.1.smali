.class public LLf;
.super Ljava/lang/Object;
.source "SelectionDragDropRemapper.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private a:LLk;

.field private final a:Landroid/graphics/Point;

.field private a:Ljava/lang/Runnable;

.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LLk;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LLf;->a:Ljava/util/List;

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, LLf;->a:LLk;

    .line 57
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LLf;->a:Ljava/util/Set;

    .line 58
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LLf;->b:Ljava/util/Set;

    .line 59
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LLf;->c:Ljava/util/Set;

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, LLf;->a:Z

    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, LLf;->b:Z

    .line 68
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 70
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, LLf;->a:Landroid/graphics/Point;

    .line 71
    iget-object v1, p0, LLf;->a:Landroid/graphics/Point;

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    move-result v0

    invoke-static {v0}, LbiT;->a(Z)V

    .line 72
    return-void
.end method

.method private a(LLk;Landroid/view/DragEvent;)V
    .locals 3

    .prologue
    .line 207
    invoke-virtual {p1}, LLk;->a()LLm;

    move-result-object v0

    .line 208
    invoke-virtual {p1}, LLk;->a()Landroid/view/View;

    move-result-object v1

    new-instance v2, LLg;

    invoke-direct {v2, p0, p2}, LLg;-><init>(LLf;Landroid/view/DragEvent;)V

    .line 207
    invoke-virtual {v0, v1, v2}, LLm;->a(Landroid/view/View;LLl;)Z

    .line 216
    iget-object v0, p0, LLf;->c:Ljava/util/Set;

    invoke-virtual {p1}, LLk;->a()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 217
    return-void
.end method

.method private a(Landroid/view/DragEvent;)V
    .locals 9

    .prologue
    .line 163
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 164
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    .line 165
    iget-object v0, p0, LLf;->a:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/DragEvent;->getX()F

    move-result v1

    add-float/2addr v0, v1

    float-to-int v5, v0

    .line 166
    iget-object v0, p0, LLf;->a:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/DragEvent;->getY()F

    move-result v1

    add-float/2addr v0, v1

    float-to-int v6, v0

    .line 169
    const/4 v1, 0x0

    .line 170
    iget-object v0, p0, LLf;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_7

    .line 171
    iget-object v0, p0, LLf;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LLk;

    .line 172
    invoke-virtual {v0}, LLk;->a()Landroid/view/View;

    move-result-object v7

    .line 173
    invoke-virtual {v7}, Landroid/view/View;->isEnabled()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-virtual {v7, v3, v4}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    move-result v8

    if-nez v8, :cond_1

    move v0, v1

    .line 170
    :cond_0
    :goto_1
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 177
    :cond_1
    iget-object v8, p0, LLf;->a:Ljava/util/Set;

    invoke-interface {v8, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    move v0, v1

    .line 178
    goto :goto_1

    .line 180
    :cond_2
    invoke-virtual {v3, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 181
    iget-object v8, p0, LLf;->b:Ljava/util/Set;

    invoke-interface {v8, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 182
    invoke-direct {p0, v0, p1}, LLf;->b(LLk;Landroid/view/DragEvent;)V

    move v0, v1

    goto :goto_1

    .line 184
    :cond_3
    if-nez v1, :cond_6

    .line 185
    invoke-direct {p0, v0, p1}, LLf;->c(LLk;Landroid/view/DragEvent;)V

    .line 186
    const/4 v0, 0x1

    .line 187
    iget-object v1, p0, LLf;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 201
    :cond_4
    :goto_2
    return-void

    .line 193
    :cond_5
    iget-object v8, p0, LLf;->c:Ljava/util/Set;

    invoke-interface {v8, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 194
    invoke-direct {p0, v0, p1}, LLf;->a(LLk;Landroid/view/DragEvent;)V

    :cond_6
    move v0, v1

    goto :goto_1

    .line 198
    :cond_7
    if-nez v1, :cond_4

    .line 199
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, LLf;->c(LLk;Landroid/view/DragEvent;)V

    goto :goto_2
.end method

.method private b(LLk;Landroid/view/DragEvent;)V
    .locals 3

    .prologue
    .line 223
    iget-object v0, p0, LLf;->c:Ljava/util/Set;

    invoke-virtual {p1}, LLk;->a()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 224
    invoke-virtual {p1}, LLk;->a()LLm;

    move-result-object v0

    .line 225
    invoke-virtual {p1}, LLk;->a()Landroid/view/View;

    move-result-object v1

    new-instance v2, LLh;

    invoke-direct {v2, p0, p2}, LLh;-><init>(LLf;Landroid/view/DragEvent;)V

    .line 224
    invoke-virtual {v0, v1, v2}, LLm;->a(Landroid/view/View;LLl;)Z

    .line 233
    iget-object v0, p0, LLf;->c:Ljava/util/Set;

    invoke-virtual {p1}, LLk;->a()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 235
    :cond_0
    return-void
.end method

.method private c(LLk;Landroid/view/DragEvent;)V
    .locals 3

    .prologue
    .line 241
    iget-object v0, p0, LLf;->a:LLk;

    if-ne v0, p1, :cond_1

    .line 270
    :cond_0
    :goto_0
    return-void

    .line 244
    :cond_1
    iget-object v0, p0, LLf;->a:LLk;

    if-eqz v0, :cond_2

    .line 245
    iget-object v0, p0, LLf;->a:LLk;

    invoke-virtual {v0}, LLk;->a()LLm;

    move-result-object v0

    iget-object v1, p0, LLf;->a:LLk;

    .line 246
    invoke-virtual {v1}, LLk;->a()Landroid/view/View;

    move-result-object v1

    new-instance v2, LLi;

    invoke-direct {v2, p0, p2}, LLi;-><init>(LLf;Landroid/view/DragEvent;)V

    .line 245
    invoke-virtual {v0, v1, v2}, LLm;->a(Landroid/view/View;LLl;)Z

    .line 256
    :cond_2
    iput-object p1, p0, LLf;->a:LLk;

    .line 257
    iget-boolean v0, p0, LLf;->a:Z

    iput-boolean v0, p0, LLf;->b:Z

    .line 259
    iget-object v0, p0, LLf;->a:LLk;

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, LLf;->a:LLk;

    invoke-virtual {v0}, LLk;->a()LLm;

    move-result-object v0

    iget-object v1, p0, LLf;->a:LLk;

    .line 261
    invoke-virtual {v1}, LLk;->a()Landroid/view/View;

    move-result-object v1

    new-instance v2, LLj;

    invoke-direct {v2, p0, p2}, LLj;-><init>(LLf;Landroid/view/DragEvent;)V

    .line 260
    invoke-virtual {v0, v1, v2}, LLm;->a(Landroid/view/View;LLl;)Z

    goto :goto_0
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, LLf;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, LLf;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 280
    const/4 v0, 0x0

    iput-object v0, p0, LLf;->a:Ljava/lang/Runnable;

    .line 282
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;LLm;)V
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, LLf;->a:Ljava/util/List;

    new-instance v1, LLk;

    invoke-direct {v1, p0, p1, p2}, LLk;-><init>(LLf;Landroid/view/View;LLm;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    return-void
.end method

.method public a(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 131
    if-eqz p2, :cond_0

    .line 132
    iget-object v0, p0, LLf;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 136
    :goto_0
    return-void

    .line 134
    :cond_0
    iget-object v0, p0, LLf;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, LLf;->a:Ljava/lang/Runnable;

    .line 289
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 297
    iget-boolean v0, p0, LLf;->b:Z

    return v0
.end method

.method public a(Landroid/view/DragEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 88
    invoke-virtual {p1}, Landroid/view/DragEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 124
    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    :cond_0
    :goto_1
    return v0

    .line 98
    :pswitch_1
    invoke-direct {p0, p1}, LLf;->a(Landroid/view/DragEvent;)V

    .line 99
    iget-object v1, p0, LLf;->a:LLk;

    if-eqz v1, :cond_1

    .line 100
    iget-object v1, p0, LLf;->a:LLk;

    invoke-virtual {v1}, LLk;->a()LLm;

    move-result-object v1

    iget-object v2, p0, LLf;->a:LLk;

    invoke-virtual {v2}, LLk;->a()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, LLm;->onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z

    .line 102
    :cond_1
    iput-boolean v0, p0, LLf;->a:Z

    goto :goto_0

    .line 106
    :pswitch_2
    invoke-direct {p0, p1}, LLf;->a(Landroid/view/DragEvent;)V

    .line 107
    iget-object v1, p0, LLf;->a:LLk;

    if-eqz v1, :cond_0

    .line 108
    iget-object v1, p0, LLf;->a:LLk;

    invoke-virtual {v1}, LLk;->a()LLm;

    move-result-object v1

    iget-object v2, p0, LLf;->a:LLk;

    invoke-virtual {v2}, LLk;->a()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, LLm;->onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z

    goto :goto_1

    .line 117
    :pswitch_3
    invoke-virtual {p0}, LLf;->a()V

    goto :goto_0

    .line 88
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public b(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 145
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    if-eqz p2, :cond_1

    .line 147
    iget-object v0, p0, LLf;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 148
    iget-object v0, p0, LLf;->a:LLk;

    if-eqz v0, :cond_0

    iget-object v0, p0, LLf;->a:LLk;

    invoke-virtual {v0}, LLk;->a()Landroid/view/View;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 149
    iget-object v0, p0, LLf;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 150
    const/4 v0, 0x0

    iput-object v0, p0, LLf;->a:LLk;

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    iget-object v0, p0, LLf;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 155
    iget-object v0, p0, LLf;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
