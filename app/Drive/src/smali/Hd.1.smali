.class public final LHd;
.super LGX;
.source "NativeDriveAppOpenerOption.java"


# static fields
.field private static final a:Landroid/net/Uri;


# instance fields
.field private final a:Landroid/content/ComponentName;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-string v0, "content://com.google.android.apps.drive/open"

    .line 40
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LHd;->a:Landroid/net/Uri;

    .line 39
    return-void
.end method

.method private constructor <init>(LHh;Landroid/content/ComponentName;Landroid/content/Context;LGZ;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 83
    const-string v5, "NativeDriveApp"

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v6}, LGX;-><init>(LHh;Landroid/content/Context;LGZ;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 85
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    iput-object v0, p0, LHd;->a:Landroid/content/ComponentName;

    .line 86
    return-void
.end method

.method private static a(LaGo;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 70
    invoke-interface {p0}, LaGo;->i()Ljava/lang/String;

    move-result-object v0

    .line 71
    invoke-interface {p0}, LaGo;->g()Ljava/lang/String;

    move-result-object v1

    .line 72
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.apps.drive.DRIVE_OPEN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 73
    sget-object v3, LHd;->a:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 74
    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 75
    const-string v1, "resourceId"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 76
    return-object v2
.end method

.method private static a(Landroid/content/pm/ActivityInfo;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 51
    if-eqz p0, :cond_0

    iget-object v1, p0, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    if-nez v1, :cond_1

    .line 61
    :cond_0
    :goto_0
    return-object v0

    .line 54
    :cond_1
    iget-object v1, p0, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    const-string v2, "com.google.android.apps.drive.APP_ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 55
    if-eqz v1, :cond_0

    .line 56
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 57
    const-string v2, "id="

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 58
    const-string v0, "id="

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LaGo;Landroid/content/Context;LGZ;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGo;",
            "Landroid/content/Context;",
            "LGZ;",
            ")",
            "Ljava/util/List",
            "<",
            "LHd;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 97
    invoke-static {p0}, LHd;->a(LaGo;)Landroid/content/Intent;

    move-result-object v0

    .line 98
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 99
    invoke-virtual {v6, v0, v9}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 100
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v7

    .line 101
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 102
    new-instance v2, Landroid/content/ComponentName;

    iget-object v1, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v2, v1, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const/16 v0, 0x80

    :try_start_0
    invoke-virtual {v6, v2, v0}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    .line 108
    sget v1, Lxi;->open_with_native_drive_app_item_subtitle:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 109
    invoke-static {v6, v0, v1}, LHh;->a(Landroid/content/pm/PackageManager;Landroid/content/pm/ActivityInfo;Ljava/lang/String;)LHh;

    move-result-object v1

    .line 110
    invoke-static {v0}, LHd;->a(Landroid/content/pm/ActivityInfo;)Ljava/lang/String;

    move-result-object v5

    .line 111
    if-eqz v1, :cond_0

    if-eqz v5, :cond_0

    .line 112
    new-instance v0, LHd;

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, LHd;-><init>(LHh;Landroid/content/ComponentName;Landroid/content/Context;LGZ;Ljava/lang/String;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 115
    :catch_0
    move-exception v0

    .line 116
    const-string v1, "NativeDriveOpener"

    const-string v3, "Activity doesn\'t actually exist %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v9

    invoke-static {v1, v0, v3, v4}, LalV;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 119
    :cond_1
    return-object v7
.end method


# virtual methods
.method protected a(LaGo;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 90
    invoke-static {p1}, LHd;->a(LaGo;)Landroid/content/Intent;

    move-result-object v0

    .line 91
    iget-object v1, p0, LHd;->a:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 92
    return-object v0
.end method
