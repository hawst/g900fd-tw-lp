.class public final LaCc;
.super Ljava/lang/Object;


# instance fields
.field private a:I

.field a:LaCd;

.field a:Lcom/google/android/gms/cast/CastDevice;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/cast/CastDevice;LaCd;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "CastDevice parameter cannot be null"

    invoke-static {p1, v0}, LaSc;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "CastListener parameter cannot be null"

    invoke-static {p2, v0}, LaSc;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, LaCc;->a:Lcom/google/android/gms/cast/CastDevice;

    iput-object p2, p0, LaCc;->a:LaCd;

    const/4 v0, 0x0

    iput v0, p0, LaCc;->a:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/cast/CastDevice;LaCd;LaBV;)V
    .locals 0

    invoke-direct {p0, p1, p2}, LaCc;-><init>(Lcom/google/android/gms/cast/CastDevice;LaCd;)V

    return-void
.end method

.method static synthetic a(LaCc;)I
    .locals 1

    iget v0, p0, LaCc;->a:I

    return v0
.end method


# virtual methods
.method public a()LaCb;
    .locals 2

    new-instance v0, LaCb;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LaCb;-><init>(LaCc;LaBV;)V

    return-object v0
.end method
