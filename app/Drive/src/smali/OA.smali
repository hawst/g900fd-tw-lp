.class public LOA;
.super LJG;
.source "PhotoBackupSuggestHelpCard.java"


# instance fields
.field private final a:LOy;

.field private final a:LSu;

.field private final a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LSu;LOy;LJc;Laja;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LSu;",
            "LOy;",
            "LJc;",
            "Laja",
            "<",
            "Landroid/app/Activity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    const-string v1, "PhotoBackupSuggestHelpCard"

    sget v2, LOU;->photo_backup_suggest_help_card:I

    sget v3, LOV;->help_card_photo_backup_suggest_positive_button_label:I

    sget v4, LOS;->ic_check:I

    const/4 v5, 0x0

    sget-object v6, LJb;->c:LJb;

    move-object v0, p3

    invoke-virtual/range {v0 .. v6}, LJc;->a(Ljava/lang/String;IIIZLJb;)LIU;

    move-result-object v0

    invoke-direct {p0, v0}, LJG;-><init>(LIU;)V

    .line 70
    iput-object p2, p0, LOA;->a:LOy;

    .line 71
    iput-object p1, p0, LOA;->a:LSu;

    .line 72
    iput-object p4, p0, LOA;->a:Laja;

    .line 73
    return-void
.end method

.method static synthetic a(LOA;)Laja;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, LOA;->a:Laja;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 77
    invoke-super {p0, p1, p2}, LJG;->a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 79
    sget v0, LOT;->content_text:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 80
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 81
    return-object v1
.end method

.method protected a()V
    .locals 3

    .prologue
    .line 86
    iget-object v0, p0, LOA;->a:LSu;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, LOA;->a:LOy;

    invoke-virtual {v0}, LOy;->a()V

    .line 88
    invoke-virtual {p0}, LOA;->a()LaFO;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, LaFO;->b()Ljava/lang/String;

    move-result-object v0

    .line 90
    iget-object v1, p0, LOA;->a:LSu;

    .line 91
    invoke-interface {v1, v0}, LSu;->b(Ljava/lang/String;)LbsU;

    move-result-object v0

    .line 92
    new-instance v1, LOB;

    invoke-direct {v1, p0}, LOB;-><init>(LOA;)V

    .line 114
    invoke-static {}, Lanj;->a()Ljava/util/concurrent/Executor;

    move-result-object v2

    .line 113
    invoke-static {v0, v1, v2}, LbsK;->a(LbsU;LbsJ;Ljava/util/concurrent/Executor;)V

    .line 116
    :cond_0
    return-void
.end method

.method protected b()V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, LOA;->a:LOy;

    invoke-virtual {v0}, LOy;->a()V

    .line 121
    return-void
.end method
