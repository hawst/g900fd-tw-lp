.class public abstract LbmY;
.super Lbmv;
.source "ImmutableSet.java"

# interfaces
.implements Ljava/util/Set;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lbmv",
        "<TE;>;",
        "Ljava/util/Set",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 235
    const-wide v0, 0x41c6666666666666L    # 7.516192768E8

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    sput v0, LbmY;->a:I

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 391
    invoke-direct {p0}, Lbmv;-><init>()V

    return-void
.end method

.method static a(I)I
    .locals 6

    .prologue
    const/high16 v1, 0x40000000    # 2.0f

    .line 248
    sget v0, LbmY;->a:I

    if-ge p0, v0, :cond_0

    .line 250
    add-int/lit8 v0, p0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v0

    shl-int/lit8 v0, v0, 0x1

    .line 251
    :goto_0
    int-to-double v2, v0

    const-wide v4, 0x3fe6666666666666L    # 0.7

    mul-double/2addr v2, v4

    int-to-double v4, p0

    cmpg-double v1, v2, v4

    if-gez v1, :cond_1

    .line 252
    shl-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 258
    :cond_0
    if-ge p0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    const-string v2, "collection too large"

    invoke-static {v0, v2}, LbiT;->a(ZLjava/lang/Object;)V

    move v0, v1

    .line 259
    :cond_1
    return v0

    .line 258
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a()LbmY;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "LbmY",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 84
    sget-object v0, Lbmg;->a:Lbmg;

    return-object v0
.end method

.method static synthetic a(I[Ljava/lang/Object;)LbmY;
    .locals 1

    .prologue
    .line 75
    invoke-static {p0, p1}, LbmY;->b(I[Ljava/lang/Object;)LbmY;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;)LbmY;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<+TE;>;)",
            "LbmY",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 301
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_0

    invoke-static {p0}, LblV;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LbmY;->a(Ljava/util/Iterator;)LbmY;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;)LbmY;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;)",
            "LbmY",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 94
    new-instance v0, Lbqe;

    invoke-direct {v0, p0}, Lbqe;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)LbmY;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;)",
            "LbmY",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 105
    new-array v0, v2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {v2, v0}, LbmY;->b(I[Ljava/lang/Object;)LbmY;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmY;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;)",
            "LbmY",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    .line 116
    new-array v0, v2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    invoke-static {v2, v0}, LbmY;->b(I[Ljava/lang/Object;)LbmY;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmY;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;TE;)",
            "LbmY",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x4

    .line 127
    new-array v0, v2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    invoke-static {v2, v0}, LbmY;->b(I[Ljava/lang/Object;)LbmY;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmY;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;TE;TE;)",
            "LbmY",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x5

    .line 138
    new-array v0, v2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    invoke-static {v2, v0}, LbmY;->b(I[Ljava/lang/Object;)LbmY;

    move-result-object v0

    return-object v0
.end method

.method public static varargs a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LbmY;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;TE;TE;TE;TE;TE;[TE;)",
            "LbmY",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 150
    .line 151
    array-length v0, p6

    add-int/lit8 v0, v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    .line 152
    aput-object p0, v0, v3

    .line 153
    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 154
    const/4 v1, 0x2

    aput-object p2, v0, v1

    .line 155
    const/4 v1, 0x3

    aput-object p3, v0, v1

    .line 156
    const/4 v1, 0x4

    aput-object p4, v0, v1

    .line 157
    const/4 v1, 0x5

    aput-object p5, v0, v1

    .line 158
    const/4 v1, 0x6

    array-length v2, p6

    invoke-static {p6, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 159
    array-length v1, v0

    invoke-static {v1, v0}, LbmY;->b(I[Ljava/lang/Object;)LbmY;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Collection;)LbmY;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<+TE;>;)",
            "LbmY",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 357
    instance-of v0, p0, LbmY;

    if-eqz v0, :cond_0

    instance-of v0, p0, Lbnk;

    if-nez v0, :cond_0

    move-object v0, p0

    .line 360
    check-cast v0, LbmY;

    .line 361
    invoke-virtual {v0}, LbmY;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 371
    :goto_0
    return-object v0

    .line 364
    :cond_0
    instance-of v0, p0, Ljava/util/EnumSet;

    if-eqz v0, :cond_1

    .line 365
    check-cast p0, Ljava/util/EnumSet;

    invoke-static {p0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    .line 368
    invoke-static {v0}, LbmE;->a(Ljava/util/EnumSet;)LbmY;

    move-result-object v0

    goto :goto_0

    .line 371
    :cond_1
    invoke-static {p0}, LbmY;->b(Ljava/util/Collection;)LbmY;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/Iterator;)LbmY;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Iterator",
            "<+TE;>;)",
            "LbmY",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 314
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 315
    invoke-static {}, LbmY;->a()LbmY;

    move-result-object v0

    .line 321
    :goto_0
    return-object v0

    .line 317
    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 318
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 319
    invoke-static {v0}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v0

    goto :goto_0

    .line 321
    :cond_1
    new-instance v1, Lbna;

    invoke-direct {v1}, Lbna;-><init>()V

    invoke-virtual {v1, v0}, Lbna;->a(Ljava/lang/Object;)Lbna;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbna;->a(Ljava/util/Iterator;)Lbna;

    move-result-object v0

    invoke-virtual {v0}, Lbna;->a()LbmY;

    move-result-object v0

    goto :goto_0
.end method

.method public static a([Ljava/lang/Object;)LbmY;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">([TE;)",
            "LbmY",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 273
    array-length v0, p0

    packed-switch v0, :pswitch_data_0

    .line 279
    array-length v1, p0

    invoke-virtual {p0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v1, v0}, LbmY;->b(I[Ljava/lang/Object;)LbmY;

    move-result-object v0

    :goto_0
    return-object v0

    .line 275
    :pswitch_0
    invoke-static {}, LbmY;->a()LbmY;

    move-result-object v0

    goto :goto_0

    .line 277
    :pswitch_1
    const/4 v0, 0x0

    aget-object v0, p0, v0

    invoke-static {v0}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v0

    goto :goto_0

    .line 273
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a()Lbna;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "Lbna",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 513
    new-instance v0, Lbna;

    invoke-direct {v0}, Lbna;-><init>()V

    return-object v0
.end method

.method private static varargs b(I[Ljava/lang/Object;)LbmY;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(I[",
            "Ljava/lang/Object;",
            ")",
            "LbmY",
            "<TE;>;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 178
    packed-switch p0, :pswitch_data_0

    .line 188
    invoke-static {p0}, LbmY;->a(I)I

    move-result v5

    .line 189
    new-array v6, v5, [Ljava/lang/Object;

    .line 190
    add-int/lit8 v7, v5, -0x1

    move v3, v4

    move v1, v4

    move v2, v4

    .line 193
    :goto_0
    if-ge v3, p0, :cond_2

    .line 194
    aget-object v0, p1, v3

    invoke-static {v0, v3}, Lbpv;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v8

    .line 195
    invoke-virtual {v8}, Ljava/lang/Object;->hashCode()I

    move-result v9

    .line 196
    invoke-static {v9}, Lbms;->a(I)I

    move-result v0

    .line 197
    :goto_1
    and-int v10, v0, v7

    .line 198
    aget-object v11, v6, v10

    .line 199
    if-nez v11, :cond_0

    .line 201
    add-int/lit8 v0, v1, 0x1

    aput-object v8, p1, v1

    .line 202
    aput-object v8, v6, v10

    .line 203
    add-int v1, v2, v9

    .line 193
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 180
    :pswitch_0
    invoke-static {}, LbmY;->a()LbmY;

    move-result-object v0

    .line 224
    :goto_3
    return-object v0

    .line 183
    :pswitch_1
    aget-object v0, p1, v4

    .line 184
    invoke-static {v0}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v0

    goto :goto_3

    .line 205
    :cond_0
    invoke-virtual {v11, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    move v0, v1

    move v1, v2

    .line 206
    goto :goto_2

    .line 196
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 210
    :cond_2
    const/4 v0, 0x0

    invoke-static {p1, v1, p0, v0}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    .line 211
    const/4 v0, 0x1

    if-ne v1, v0, :cond_3

    .line 215
    aget-object v1, p1, v4

    .line 216
    new-instance v0, Lbqe;

    invoke-direct {v0, v1, v2}, Lbqe;-><init>(Ljava/lang/Object;I)V

    goto :goto_3

    .line 217
    :cond_3
    invoke-static {v1}, LbmY;->a(I)I

    move-result v0

    if-eq v5, v0, :cond_4

    .line 220
    invoke-static {v1, p1}, LbmY;->b(I[Ljava/lang/Object;)LbmY;

    move-result-object v0

    goto :goto_3

    .line 222
    :cond_4
    array-length v0, p1

    if-ge v1, v0, :cond_5

    invoke-static {p1, v1}, Lbpv;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    .line 224
    :cond_5
    new-instance v0, LbpL;

    invoke-direct {v0, p1, v2, v6, v7}, LbpL;-><init>([Ljava/lang/Object;I[Ljava/lang/Object;I)V

    goto :goto_3

    .line 178
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static b(Ljava/util/Collection;)LbmY;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<+TE;>;)",
            "LbmY",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 375
    invoke-interface {p0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    .line 376
    array-length v1, v0

    packed-switch v1, :pswitch_data_0

    .line 387
    array-length v1, v0

    invoke-static {v1, v0}, LbmY;->b(I[Ljava/lang/Object;)LbmY;

    move-result-object v0

    :goto_0
    return-object v0

    .line 378
    :pswitch_0
    invoke-static {}, LbmY;->a()LbmY;

    move-result-object v0

    goto :goto_0

    .line 382
    :pswitch_1
    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 383
    invoke-static {v0}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v0

    goto :goto_0

    .line 376
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public abstract a()Lbqv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lbqv",
            "<TE;>;"
        }
    .end annotation
.end method

.method b()Z
    .locals 1

    .prologue
    .line 395
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 400
    if-ne p1, p0, :cond_0

    .line 401
    const/4 v0, 0x1

    .line 407
    :goto_0
    return v0

    .line 403
    :cond_0
    instance-of v0, p1, LbmY;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LbmY;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, LbmY;

    invoke-virtual {v0}, LbmY;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LbmY;->hashCode()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 405
    const/4 v0, 0x0

    goto :goto_0

    .line 407
    :cond_1
    invoke-static {p0, p1}, LbpU;->a(Ljava/util/Set;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 412
    invoke-static {p0}, LbpU;->a(Ljava/util/Set;)I

    move-result v0

    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, LbmY;->a()Lbqv;

    move-result-object v0

    return-object v0
.end method
