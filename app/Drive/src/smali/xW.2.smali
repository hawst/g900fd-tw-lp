.class public LxW;
.super Ljava/lang/Object;
.source "CrossAppPromoFetcherImpl.java"

# interfaces
.implements LxV;


# static fields
.field static final a:J

.field static final a:LaGs;

.field static final a:Lyt;


# instance fields
.field private final a:LwH;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 39
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, LxW;->a:J

    .line 51
    sget-object v0, LalT;->a:LaGs;

    sput-object v0, LxW;->a:LaGs;

    .line 55
    new-instance v0, Lyt;

    .line 56
    invoke-static {}, Lyj;->a()Lyi;

    move-result-object v1

    invoke-direct {v0, v1}, Lyt;-><init>(Lyi;)V

    sput-object v0, LxW;->a:Lyt;

    .line 55
    return-void
.end method

.method public constructor <init>(LwH;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LwH;

    iput-object v0, p0, LxW;->a:LwH;

    .line 63
    return-void
.end method

.method private a(LaGL;)Ladj;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 67
    iget-object v1, p0, LxW;->a:LwH;

    invoke-interface {v1, p1}, LwH;->a(LaGL;)LwI;

    move-result-object v1

    .line 68
    if-nez v1, :cond_1

    .line 80
    :cond_0
    :goto_0
    return-object v0

    .line 72
    :cond_1
    invoke-interface {v1}, LwI;->a()Ljava/util/List;

    move-result-object v1

    .line 73
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 77
    invoke-static {v1}, Lbnm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladj;

    .line 79
    const-string v1, "CrossAppPromoFetcherImpl"

    const-string v2, "got archive file %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 88
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 89
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CrossAppPromo-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 90
    return-object v0

    .line 88
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ladj;
    .locals 5

    .prologue
    .line 121
    invoke-direct {p0, p1}, LxW;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 123
    const/4 v1, 0x0

    .line 124
    const/4 v2, 0x0

    .line 125
    const-string v3, "prod"

    sget-object v4, LxW;->a:LaGs;

    invoke-static {v0, v1, v3, v4, v2}, LaGL;->a(Ljava/lang/String;LaFO;Ljava/lang/String;LaGs;Z)LaGL;

    move-result-object v0

    .line 127
    invoke-direct {p0, v0}, LxW;->a(LaGL;)Ladj;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Lath;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 96
    invoke-direct {p0, p1}, LxW;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 99
    invoke-virtual {p2}, Lath;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lath;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LbmY;->a(Ljava/lang/Object;)LbmY;

    move-result-object v0

    .line 103
    :goto_0
    sget-wide v2, LxW;->a:J

    invoke-static {v0, v2, v3, p3}, LwM;->a(Ljava/util/Set;JLjava/lang/String;)LwM;

    move-result-object v4

    .line 107
    const/4 v2, 0x0

    .line 108
    :try_start_0
    iget-object v0, p0, LxW;->a:LwH;

    sget-object v3, LxW;->a:LaGs;

    sget-object v5, LxW;->a:Lyt;

    invoke-interface/range {v0 .. v5}, LwH;->a(Ljava/lang/String;LaFO;LaGs;LwL;Lyt;)LaGL;

    move-result-object v0

    .line 110
    invoke-direct {p0, v0}, LxW;->a(LaGL;)Ladj;
    :try_end_0
    .catch Lwx; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 111
    if-eqz v0, :cond_1

    move v0, v6

    .line 114
    :goto_1
    return v0

    .line 99
    :cond_0
    invoke-static {}, LbmY;->a()LbmY;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v7

    .line 111
    goto :goto_1

    .line 112
    :catch_0
    move-exception v0

    .line 113
    const-string v1, "CrossAppPromoFetcherImpl"

    const-string v2, "Error while fetching the archived story from url: %s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p2, v3, v7

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v7

    .line 114
    goto :goto_1
.end method
