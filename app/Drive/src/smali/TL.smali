.class public LTL;
.super Lorg/apache/http/entity/HttpEntityWrapper;
.source "HttpIssuerEntity.java"


# instance fields
.field private a:LTM;


# direct methods
.method public constructor <init>(Lorg/apache/http/HttpEntity;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lorg/apache/http/entity/HttpEntityWrapper;-><init>(Lorg/apache/http/HttpEntity;)V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, LTL;->a:LTM;

    .line 25
    return-void
.end method

.method private b()LTM;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, LTL;->a:LTM;

    if-nez v0, :cond_0

    .line 41
    invoke-virtual {p0}, LTL;->a()LTM;

    .line 43
    :cond_0
    iget-object v0, p0, LTL;->a:LTM;

    return-object v0
.end method


# virtual methods
.method public a()LTM;
    .locals 2

    .prologue
    .line 29
    new-instance v0, LTM;

    invoke-super {p0}, Lorg/apache/http/entity/HttpEntityWrapper;->getContent()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1}, LTM;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, LTL;->a:LTM;

    .line 30
    iget-object v0, p0, LTL;->a:LTM;

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, LTL;->a:LTM;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, LTL;->a:LTM;

    invoke-virtual {v0}, LTM;->a()Z

    move-result v0

    .line 54
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public consumeContent()V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0}, LTL;->b()LTM;

    move-result-object v1

    .line 61
    :try_start_0
    invoke-super {p0}, Lorg/apache/http/entity/HttpEntityWrapper;->consumeContent()V

    .line 62
    if-eqz v1, :cond_0

    .line 63
    invoke-virtual {v1}, LTM;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    :cond_0
    if-eqz v1, :cond_1

    .line 67
    invoke-virtual {v1}, LTM;->a()V

    .line 70
    :cond_1
    return-void

    .line 66
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 67
    invoke-virtual {v1}, LTM;->a()V

    :cond_2
    throw v0
.end method

.method public synthetic getContent()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, LTL;->a()LTM;

    move-result-object v0

    return-object v0
.end method
