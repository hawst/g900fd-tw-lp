.class Lazd;
.super Ljava/lang/Object;

# interfaces
.implements LayW;


# instance fields
.field private a:I

.field private a:J

.field private a:LazE;

.field final synthetic a:Lazc;

.field private a:Z

.field private b:Z


# direct methods
.method public constructor <init>(Lazc;)V
    .locals 3

    const/4 v2, 0x0

    iput-object p1, p0, Lazd;->a:Lazc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lazd;->a:Z

    iput v2, p0, Lazd;->a:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lazd;->a:J

    iput-boolean v2, p0, Lazd;->b:Z

    new-instance v0, Laze;

    invoke-direct {v0, p0, p1}, Laze;-><init>(Lazd;Lazc;)V

    iput-object v0, p0, Lazd;->a:LazE;

    return-void
.end method

.method private a()V
    .locals 6

    invoke-static {}, LayV;->a()LayV;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "GoogleAnalytics isn\'t initialized for the Tracker!"

    invoke-static {v0}, LaAj;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-wide v2, p0, Lazd;->a:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    iget-boolean v1, p0, Lazd;->a:Z

    if-eqz v1, :cond_2

    :cond_1
    iget-object v1, p0, Lazd;->a:Lazc;

    invoke-static {v1}, Lazc;->a(Lazc;)Lazd;

    move-result-object v1

    invoke-virtual {v0, v1}, LayV;->a(LayW;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lazd;->a:Lazc;

    invoke-static {v1}, Lazc;->a(Lazc;)Lazd;

    move-result-object v1

    invoke-virtual {v0, v1}, LayV;->b(LayW;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lazd;->a:Z

    invoke-direct {p0}, Lazd;->a()V

    return-void
.end method

.method public a()Z
    .locals 2

    iget-boolean v0, p0, Lazd;->b:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lazd;->b:Z

    return v0
.end method
