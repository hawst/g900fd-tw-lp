.class public LamX;
.super Ljava/lang/Object;
.source "ThumbnailFetchHelperImpl.java"

# interfaces
.implements LamW;


# static fields
.field private static final a:Landroid/net/Uri;


# instance fields
.field private final a:LQr;

.field private final a:LTg;

.field private final a:LaGM;

.field private final a:Laer;

.field private final a:LagZ;

.field private final a:LtK;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-string v0, "https://googledrive.com/thumb"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LamX;->a:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Laja;LTg;LQr;LtK;LaGM;LagZ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "Laer;",
            ">;",
            "LTg;",
            "LQr;",
            "LtK;",
            "LaGM;",
            "LagZ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    invoke-virtual {p1}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laer;

    iput-object v0, p0, LamX;->a:Laer;

    .line 74
    iput-object p2, p0, LamX;->a:LTg;

    .line 75
    iput-object p3, p0, LamX;->a:LQr;

    .line 76
    iput-object p4, p0, LamX;->a:LtK;

    .line 77
    iput-object p5, p0, LamX;->a:LaGM;

    .line 78
    iput-object p6, p0, LamX;->a:LagZ;

    .line 79
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;Z)Landroid/net/Uri;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 186
    invoke-direct {p0, p1}, LamX;->a(Ljava/lang/String;)Z

    move-result v3

    .line 187
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 189
    if-nez p2, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 191
    :goto_0
    if-eqz v3, :cond_3

    .line 193
    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 194
    const/16 v3, 0x3d

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 195
    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    .line 196
    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 198
    :cond_0
    if-nez v1, :cond_1

    .line 199
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p2, p3}, LamX;->a(Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 201
    :cond_1
    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 202
    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 214
    :goto_1
    return-object v0

    :cond_2
    move v1, v2

    .line 189
    goto :goto_0

    .line 206
    :cond_3
    if-nez v1, :cond_4

    .line 207
    const-string v0, "sz"

    .line 208
    invoke-direct {p0, p2, p3}, LamX;->a(Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;Z)Ljava/lang/String;

    move-result-object v1

    .line 207
    invoke-static {v4, v0, v1}, Lanl;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_1

    .line 210
    :cond_4
    const-string v0, "sz"

    invoke-static {v4, v0}, Lanl;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_1
.end method

.method private a(Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;Z)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 223
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    if-eqz p2, :cond_0

    .line 225
    const-string v0, "w%s-h%s-p"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 227
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "w%s-h%s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 129
    iget-object v1, p0, LamX;->a:LaGM;

    invoke-interface {v1, p1}, LaGM;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGo;

    move-result-object v1

    .line 130
    if-eqz v1, :cond_0

    .line 131
    invoke-interface {v1}, LaGo;->a()LaGx;

    move-result-object v2

    .line 132
    sget-object v3, LaGx;->a:LaGx;

    invoke-virtual {v3, v2}, LaGx;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 133
    iget-object v2, p0, LamX;->a:LagZ;

    invoke-interface {v2, v1}, LagZ;->e(LaGo;)V

    .line 134
    iget-object v1, p0, LamX;->a:LaGM;

    invoke-interface {v1, p1}, LaGM;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGo;

    move-result-object v1

    .line 135
    if-nez v1, :cond_1

    .line 150
    :cond_0
    :goto_0
    return v0

    .line 138
    :cond_1
    invoke-interface {v1}, LaGo;->a()LaGx;

    move-result-object v1

    .line 139
    sget-object v2, LaGx;->a:LaGx;

    invoke-virtual {v2, v1}, LaGx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 140
    const-string v1, "ThumbnailFetchHelper"

    const-string v2, "An entry thumbnail status is unknown after syncing metadata."

    invoke-static {v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 143
    :cond_2
    sget-object v0, LaGx;->b:LaGx;

    invoke-virtual {v0, v1}, LaGx;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 147
    :cond_3
    sget-object v0, LaGx;->b:LaGx;

    invoke-virtual {v0, v2}, LaGx;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 232
    iget-object v0, p0, LamX;->a:LQr;

    const-string v1, "fifeUrlPattern"

    const-string v2, "(?:(?:https?://lh[3-6].ggpht.com)|(?:https?://lh[3-6].googleusercontent.com)|(?:https?://lh[3-6].google.com)|(?:http://[1-4].bp.blogspot.com)|(?:http://bp[0-3].blogger.com)|(?:https?://www.google.com/visualsearch/lh))/.*"

    .line 233
    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 234
    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private b(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;Z)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, LamX;->a:Laer;

    invoke-interface {v0, p1}, Laer;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaeF;

    move-result-object v0

    .line 171
    invoke-interface {v0}, LaJT;->j()Ljava/lang/String;

    move-result-object v0

    .line 173
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 174
    new-instance v0, LaoW;

    invoke-direct {v0}, LaoW;-><init>()V

    throw v0

    .line 177
    :cond_0
    invoke-direct {p0, v0, p2, p3}, LamX;->a(Ljava/lang/String;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;Z)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 157
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LamX;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;Z)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;Z)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 104
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    iget-object v0, p0, LamX;->a:LtK;

    sget-object v1, Lry;->aE:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 109
    :try_start_0
    invoke-direct {p0, p1}, LamX;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    sget-object v0, LamX;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 111
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "width"

    .line 112
    invoke-virtual {p2}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "height"

    .line 113
    invoke-virtual {p2}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "crop"

    .line 114
    invoke-static {p3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 124
    :goto_0
    return-object v0

    .line 118
    :cond_0
    new-instance v0, LaoW;

    invoke-direct {v0}, LaoW;-><init>()V

    throw v0
    :try_end_0
    .catch LaGT; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :catch_0
    move-exception v0

    .line 121
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 124
    :cond_1
    invoke-direct {p0, p1, p2, p3}, LamX;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;Z)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/net/Uri;LaFO;)Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 90
    const-string v0, "ThumbnailFetchHelper"

    const-string v1, "Thumbnail uri: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    iget-object v0, p0, LamX;->a:LTg;

    invoke-virtual {v0, p1, p2}, LTg;->a(Landroid/net/Uri;LaFO;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method
