.class public LaoF;
.super LanC;
.source "ImageDecodeFetcher.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LanC",
        "<",
        "Lcom/google/android/apps/docs/utils/FetchSpec;",
        "LakD",
        "<",
        "Ljava/io/File;",
        ">;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ltp;


# direct methods
.method public constructor <init>(Ltp;LaoX;Laoo;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltp;",
            "LaoX;",
            "Laoo",
            "<",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p2, p3}, LanC;-><init>(Laop;Laoo;)V

    .line 48
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltp;

    iput-object v0, p0, LaoF;->a:Ltp;

    .line 49
    return-void
.end method

.method private a(F)I
    .locals 3

    .prologue
    .line 87
    float-to-double v0, p1

    sget-object v2, Ljava/math/RoundingMode;->UP:Ljava/math/RoundingMode;

    invoke-static {v0, v1, v2}, Lbsq;->a(DLjava/math/RoundingMode;)I

    move-result v0

    .line 88
    const/4 v1, 0x2

    invoke-static {v1, v0}, Lbst;->a(II)I

    move-result v0

    return v0
.end method

.method private a(Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;
    .locals 6

    .prologue
    .line 102
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v1

    mul-int/2addr v0, v1

    int-to-long v0, v0

    .line 103
    iget-object v2, p0, LaoF;->a:Ltp;

    invoke-virtual {v2}, Ltp;->a()J

    move-result-wide v2

    .line 104
    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 105
    long-to-double v0, v0

    long-to-double v2, v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    .line 106
    new-instance v0, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    .line 107
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v1

    int-to-double v4, v1

    div-double/2addr v4, v2

    double-to-int v1, v4

    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v4

    int-to-double v4, v4

    div-double v2, v4, v2

    double-to-int v2, v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;-><init>(II)V

    move-object p1, v0

    .line 109
    :cond_0
    return-object p1
.end method

.method private a(Ljava/io/File;)Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;
    .locals 2

    .prologue
    .line 93
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 95
    :try_start_0
    new-instance v0, Laks;

    invoke-direct {v0}, Laks;-><init>()V

    invoke-virtual {v0, v1}, Laks;->a(Ljava/io/InputStream;)Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 97
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
.end method


# virtual methods
.method public a(Lcom/google/android/apps/docs/utils/FetchSpec;LakD;)Landroid/graphics/Bitmap;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            "LakD",
            "<",
            "Ljava/io/File;",
            ">;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 55
    :try_start_0
    invoke-virtual {p2}, LakD;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 56
    invoke-direct {p0, v0}, LaoF;->a(Ljava/io/File;)Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    move-result-object v2

    .line 57
    invoke-direct {p0, v2}, LaoF;->a(Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    move-result-object v3

    .line 59
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 60
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v5, v4, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 61
    invoke-virtual {v2}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v3}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    .line 62
    invoke-virtual {v2}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v3}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v3

    int-to-float v3, v3

    div-float v3, v6, v3

    .line 63
    invoke-virtual {v2}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v6

    invoke-virtual {v2}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v2

    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 64
    invoke-static {v5, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 65
    invoke-direct {p0, v3}, LaoF;->a(F)I

    move-result v3

    iput v3, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 69
    :cond_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 71
    const/4 v5, 0x0

    :try_start_1
    invoke-static {v3, v5, v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 76
    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 82
    invoke-virtual {p2}, LakD;->close()V

    :goto_0
    return-object v0

    .line 72
    :catch_0
    move-exception v5

    .line 73
    :try_start_3
    const-string v6, "ImageDecodeFetcher"

    const-string v7, "Not enough memory, downscale to half. %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v5, v8, v9

    invoke-static {v6, v7, v8}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 74
    iget v5, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    mul-int/lit8 v5, v5, 0x2

    iput v5, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 76
    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 78
    iget v3, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    div-int v3, v2, v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    const/16 v5, 0xc8

    if-ge v3, v5, :cond_0

    .line 82
    invoke-virtual {p2}, LakD;->close()V

    move-object v0, v1

    goto :goto_0

    .line 76
    :catchall_0
    move-exception v0

    :try_start_5
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 82
    :catchall_1
    move-exception v0

    invoke-virtual {p2}, LakD;->close()V

    throw v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    check-cast p1, Lcom/google/android/apps/docs/utils/FetchSpec;

    check-cast p2, LakD;

    invoke-virtual {p0, p1, p2}, LaoF;->a(Lcom/google/android/apps/docs/utils/FetchSpec;LakD;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
