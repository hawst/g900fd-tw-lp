.class public LWs;
.super Ljava/lang/Object;
.source "DrivePreferencesInstaller.java"

# interfaces
.implements LWx;


# instance fields
.field a:LQr;

.field a:LSF;

.field a:LadL;

.field a:LajO;

.field a:Lamn;

.field a:Landroid/app/Activity;

.field private a:Landroid/preference/Preference;

.field private a:Landroid/preference/PreferenceScreen;

.field a:Latd;

.field a:LqK;

.field a:LsC;

.field a:LtK;

.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, LWs;->a:Landroid/preference/Preference;

    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 90
    const-string v0, "additional_filters"

    .line 91
    invoke-virtual {p0, v0}, LWs;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 92
    iget-object v1, p0, LWs;->a:LQr;

    const-string v2, "additionalFilters"

    invoke-interface {v1, v2, v3}, LQr;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 93
    iget-object v1, p0, LWs;->a:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 98
    :goto_0
    return-void

    .line 95
    :cond_0
    iget-object v1, p0, LWs;->a:LQr;

    const-string v2, "additionalFiltersDefault"

    .line 96
    invoke-interface {v1, v2, v3}, LQr;->a(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 95
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setDefaultValue(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 101
    iget-object v0, p0, LWs;->a:LQr;

    const-string v1, "storageUpgradeUrl"

    const-string v2, "https://accounts.google.com/AddSession?&continue=https://www.google.com/settings/storage"

    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 104
    const-string v0, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LWs;->a:LsC;

    .line 105
    invoke-interface {v0}, LsC;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LWs;->a:Z

    .line 106
    iget-boolean v0, p0, LWs;->a:Z

    if-nez v0, :cond_1

    .line 107
    const-string v0, "storage"

    .line 108
    invoke-virtual {p0, v0}, LWs;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 109
    iget-object v1, p0, LWs;->a:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 124
    :goto_1
    return-void

    .line 105
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 111
    :cond_1
    const-string v0, "storage_add"

    invoke-virtual {p0, v0}, LWs;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, LWs;->a:Landroid/preference/Preference;

    .line 112
    iget-object v0, p0, LWs;->a:Landroid/preference/Preference;

    new-instance v2, LWt;

    invoke-direct {v2, p0, v1}, LWt;-><init>(LWs;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_1
.end method

.method private e()V
    .locals 4

    .prologue
    .line 127
    iget-object v0, p0, LWs;->a:LQr;

    const-string v1, "abusePolicyUrl"

    const-string v2, "https://support.google.com/drive/answer/148505"

    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 129
    iget-object v0, p0, LWs;->a:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 130
    sget v0, LpR;->prefs_about_category_key:I

    .line 131
    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LWs;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 133
    new-instance v3, Landroid/preference/Preference;

    invoke-direct {v3, v2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 134
    sget v2, LpR;->prefs_abuse_program_policies:I

    invoke-virtual {v3, v2}, Landroid/preference/Preference;->setTitle(I)V

    .line 135
    new-instance v2, LWu;

    invoke-direct {v2, p0, v1}, LWu;-><init>(LWs;Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 146
    invoke-virtual {v0, v3}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 147
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 150
    iget-object v0, p0, LWs;->a:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 151
    sget v0, LpR;->prefs_about_category_key:I

    .line 152
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LWs;->a(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 154
    new-instance v2, Landroid/preference/Preference;

    invoke-direct {v2, v1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 155
    sget v1, LpR;->prefs_product_tour:I

    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setTitle(I)V

    .line 156
    new-instance v1, LWv;

    invoke-direct {v1, p0}, LWv;-><init>(LWs;)V

    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 165
    invoke-virtual {v0, v2}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 166
    return-void
.end method

.method private g()V
    .locals 8

    .prologue
    .line 180
    iget-object v0, p0, LWs;->a:LajO;

    iget-object v1, p0, LWs;->a:LSF;

    .line 181
    invoke-interface {v1}, LSF;->a()LaFO;

    move-result-object v1

    invoke-interface {v0, v1}, LajO;->a(LaFO;)LajN;

    move-result-object v0

    .line 183
    new-instance v1, LajW;

    invoke-direct {v1, v0}, LajW;-><init>(LajN;)V

    .line 184
    iget-object v0, p0, LWs;->a:Landroid/app/Activity;

    sget v2, LpR;->prefs_storage_add_summary:I

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 185
    invoke-virtual {v1}, LajW;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 186
    invoke-virtual {v1}, LajW;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    .line 187
    invoke-virtual {v1}, LajW;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    .line 185
    invoke-virtual {v0, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 189
    iget-object v1, p0, LWs;->a:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 190
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 76
    sget v0, LpS;->drive_preferences:I

    return v0
.end method

.method public a(Ljava/lang/CharSequence;)Landroid/preference/Preference;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, LWs;->a:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 198
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 170
    iget-boolean v0, p0, LWs;->a:Z

    if-eqz v0, :cond_0

    .line 171
    invoke-direct {p0}, LWs;->g()V

    .line 173
    :cond_0
    return-void
.end method

.method public a(LEE;)V
    .locals 0

    .prologue
    .line 194
    return-void
.end method

.method public a(Landroid/preference/PreferenceScreen;)V
    .locals 1

    .prologue
    .line 81
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    iput-object v0, p0, LWs;->a:Landroid/preference/PreferenceScreen;

    .line 83
    invoke-direct {p0}, LWs;->c()V

    .line 84
    invoke-direct {p0}, LWs;->d()V

    .line 85
    invoke-direct {p0}, LWs;->e()V

    .line 86
    invoke-direct {p0}, LWs;->f()V

    .line 87
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 177
    return-void
.end method
