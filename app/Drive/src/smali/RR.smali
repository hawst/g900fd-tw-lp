.class public final enum LRR;
.super Ljava/lang/Enum;
.source "SidebarAction.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LRR;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LRR;

.field private static final synthetic a:[LRR;

.field public static final enum b:LRR;

.field public static final enum c:LRR;

.field public static final enum d:LRR;


# instance fields
.field private final a:I

.field private final a:LRW;

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 12
    new-instance v0, LRR;

    const-string v1, "MENU_SETTINGS"

    sget v3, Lxi;->menu_settings:I

    sget v4, Lxb;->ic_settings:I

    new-instance v5, LRS;

    invoke-direct {v5}, LRS;-><init>()V

    invoke-direct/range {v0 .. v5}, LRR;-><init>(Ljava/lang/String;IIILRW;)V

    sput-object v0, LRR;->a:LRR;

    .line 20
    new-instance v3, LRR;

    const-string v4, "MENU_HELP"

    sget v6, Lxi;->menu_help:I

    sget v7, Lxb;->ic_help:I

    new-instance v8, LRT;

    invoke-direct {v8}, LRT;-><init>()V

    move v5, v9

    invoke-direct/range {v3 .. v8}, LRR;-><init>(Ljava/lang/String;IIILRW;)V

    sput-object v3, LRR;->b:LRR;

    .line 28
    new-instance v3, LRR;

    const-string v4, "MENU_SEND_FEEDBACK"

    sget v6, Lxi;->gf_feedback:I

    sget v7, Lxb;->ic_menu_send_feedback:I

    new-instance v8, LRU;

    invoke-direct {v8}, LRU;-><init>()V

    move v5, v10

    invoke-direct/range {v3 .. v8}, LRR;-><init>(Ljava/lang/String;IIILRW;)V

    sput-object v3, LRR;->c:LRR;

    .line 36
    new-instance v3, LRR;

    const-string v4, "MENU_HELP_AND_FEEDBACK"

    sget v6, Lxi;->menu_help_and_feedback:I

    sget v7, Lxb;->ic_help:I

    new-instance v8, LRV;

    invoke-direct {v8}, LRV;-><init>()V

    move v5, v11

    invoke-direct/range {v3 .. v8}, LRR;-><init>(Ljava/lang/String;IIILRW;)V

    sput-object v3, LRR;->d:LRR;

    .line 11
    const/4 v0, 0x4

    new-array v0, v0, [LRR;

    sget-object v1, LRR;->a:LRR;

    aput-object v1, v0, v2

    sget-object v1, LRR;->b:LRR;

    aput-object v1, v0, v9

    sget-object v1, LRR;->c:LRR;

    aput-object v1, v0, v10

    sget-object v1, LRR;->d:LRR;

    aput-object v1, v0, v11

    sput-object v0, LRR;->a:[LRR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIILRW;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "LRW;",
            ")V"
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 60
    iput p3, p0, LRR;->a:I

    .line 61
    iput p4, p0, LRR;->b:I

    .line 62
    iput-object p5, p0, LRR;->a:LRW;

    .line 63
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LRR;
    .locals 1

    .prologue
    .line 11
    const-class v0, LRR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LRR;

    return-object v0
.end method

.method public static values()[LRR;
    .locals 1

    .prologue
    .line 11
    sget-object v0, LRR;->a:[LRR;

    invoke-virtual {v0}, [LRR;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LRR;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, LRR;->a:I

    return v0
.end method

.method public a(LJK;)V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, LRR;->a:LRW;

    invoke-interface {v0, p1}, LRW;->a(LJK;)V

    .line 75
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, LRR;->b:I

    return v0
.end method
