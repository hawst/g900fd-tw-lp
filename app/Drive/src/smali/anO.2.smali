.class public LanO;
.super Ljava/lang/Object;
.source "ChainedFetcher.java"

# interfaces
.implements Laoo;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Laoo",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final a:Laoo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoo",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LakH",
            "<",
            "Ljava/lang/Long;",
            "*>;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LalD",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Laoo;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoo",
            "<TK;TV;>;",
            "Ljava/util/List",
            "<",
            "LalD",
            "<*>;>;",
            "Ljava/util/List",
            "<",
            "LakH",
            "<",
            "Ljava/lang/Long;",
            "*>;>;)V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoo;

    iput-object v0, p0, LanO;->a:Laoo;

    .line 31
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    iput-object v0, p0, LanO;->b:Ljava/util/List;

    .line 32
    invoke-static {p3}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    iput-object v0, p0, LanO;->a:Ljava/util/List;

    .line 33
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)LbsU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "LbsU",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, LanO;->a:Laoo;

    invoke-interface {v0, p1}, Laoo;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, LanO;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LalD;

    .line 104
    invoke-interface {v0}, LalD;->a()V

    goto :goto_0

    .line 106
    :cond_0
    return-void
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 41
    iget-object v0, p0, LanO;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LakH;

    .line 42
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, LakH;->a(Ljava/lang/Comparable;)V

    goto :goto_0

    .line 44
    :cond_0
    return-void
.end method
