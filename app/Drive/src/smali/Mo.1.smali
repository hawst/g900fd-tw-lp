.class public LMo;
.super Ljava/lang/Object;
.source "CrossAppStateRow.java"


# instance fields
.field private final a:[Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    sget-object v0, LMn;->a:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, LMo;->a:[Ljava/lang/Object;

    .line 44
    iget-object v0, p0, LMo;->a:[Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 45
    return-void
.end method


# virtual methods
.method public a(LMp;Ljava/lang/Object;)LMo;
    .locals 4

    .prologue
    .line 52
    invoke-virtual {p1}, LMp;->ordinal()I

    move-result v0

    .line 53
    iget-object v1, p0, LMo;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 54
    const-string v1, "CrossAppStateRow"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Overwriting existing column: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, LMp;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    :cond_0
    iget-object v1, p0, LMo;->a:[Ljava/lang/Object;

    aput-object p2, v1, v0

    .line 58
    return-object p0
.end method

.method public a()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, LMo;->a:[Ljava/lang/Object;

    return-object v0
.end method
