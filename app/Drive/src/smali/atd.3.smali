.class public Latd;
.super Ljava/lang/Object;
.source "WelcomeController.java"


# annotations
.annotation runtime Lbxz;
.end annotation


# static fields
.field private static final a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Lry;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lasa;

.field private final a:Lasb;

.field a:LatC;

.field private final a:Lati;

.field private final a:Latj;

.field private final a:Latl;

.field private a:Latm;

.field private final a:Lato;

.field private final a:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LpD;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LqK;

.field private final a:LtK;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 195
    invoke-static {}, LbmY;->a()LbmY;

    move-result-object v0

    sput-object v0, Latd;->a:LbmY;

    return-void
.end method

.method public constructor <init>(LtK;Lati;Latj;Laja;LQr;LqK;Lasb;Lasa;Lato;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LtK;",
            "Lati;",
            "Latj;",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LQr;",
            "LqK;",
            "Lasb;",
            "Lasa;",
            "Lato;",
            ")V"
        }
    .end annotation

    .prologue
    .line 388
    invoke-direct/range {p0 .. p9}, Latd;-><init>(LtK;Lati;Latj;Lbxw;LQr;LqK;Lasb;Lasa;Lato;)V

    .line 390
    return-void
.end method

.method constructor <init>(LtK;Lati;Latj;Lbxw;LQr;LqK;Lasb;Lasa;Lato;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LtK;",
            "Lati;",
            "Latj;",
            "Lbxw",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LQr;",
            "LqK;",
            "Lasb;",
            "Lasa;",
            "Lato;",
            ")V"
        }
    .end annotation

    .prologue
    .line 395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 358
    new-instance v0, Latl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Latl;-><init>(Latd;Late;)V

    iput-object v0, p0, Latd;->a:Latl;

    .line 370
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Latd;->a:Ljava/util/concurrent/atomic/AtomicReference;

    .line 396
    iput-object p2, p0, Latd;->a:Lati;

    .line 397
    iput-object p3, p0, Latd;->a:Latj;

    .line 398
    iput-object p1, p0, Latd;->a:LtK;

    .line 399
    iput-object p6, p0, Latd;->a:LqK;

    .line 400
    iput-object p7, p0, Latd;->a:Lasb;

    .line 401
    iput-object p8, p0, Latd;->a:Lasa;

    .line 402
    iput-object p9, p0, Latd;->a:Lato;

    .line 404
    new-instance v0, Late;

    invoke-direct {v0, p0, p4, p5}, Late;-><init>(Latd;Lbxw;LQr;)V

    invoke-interface {p5, v0}, LQr;->a(LQs;)V

    .line 409
    return-void
.end method

.method private static a()I
    .locals 1

    .prologue
    .line 703
    invoke-static {}, Laml;->a()I

    move-result v0

    return v0
.end method

.method private a()LasJ;
    .locals 3

    .prologue
    .line 724
    iget-object v0, p0, Latd;->a:Lati;

    invoke-interface {v0}, Lati;->a()LasP;

    move-result-object v0

    sget-object v1, LasO;->a:LasO;

    invoke-static {v0, v1}, LasJ;->a(LasP;LasO;)LasJ;

    move-result-object v0

    .line 725
    invoke-virtual {p0}, Latd;->a()LbmY;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Latd;->a(LasJ;LbmY;)LasJ;

    move-result-object v0

    .line 726
    iget-object v1, p0, Latd;->a:Lati;

    invoke-interface {v1}, Lati;->b()LasP;

    move-result-object v1

    .line 727
    if-nez v1, :cond_0

    .line 730
    :goto_0
    return-object v0

    :cond_0
    sget-object v2, LasO;->a:LasO;

    invoke-static {v1, v2}, LasJ;->a(LasP;LasO;)LasJ;

    move-result-object v1

    invoke-virtual {v0, v1}, LasJ;->a(LasJ;)LasJ;

    move-result-object v0

    goto :goto_0
.end method

.method private a(LasJ;LbmY;)LasJ;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LasJ;",
            "LbmY",
            "<",
            "Lry;",
            ">;)",
            "LasJ;"
        }
    .end annotation

    .prologue
    .line 630
    .line 631
    invoke-virtual {p2}, LbmY;->a()Lbqv;

    move-result-object v2

    move-object v1, p1

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lry;

    .line 632
    iget-object v3, p0, Latd;->a:LtK;

    invoke-interface {v3, v0}, LtK;->a(LtJ;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 633
    invoke-static {v0}, Lrh;->a(LtJ;)Lrh;

    move-result-object v0

    .line 634
    iget-object v3, p0, Latd;->a:Lati;

    invoke-interface {v3, v0}, Lati;->a(Lrg;)LasP;

    move-result-object v0

    .line 635
    if-eqz p1, :cond_1

    .line 637
    invoke-virtual {p1}, LasJ;->a()LasO;

    move-result-object v3

    invoke-static {v0, v3}, LasJ;->a(LasP;LasO;)LasJ;

    move-result-object v0

    invoke-virtual {v1, v0}, LasJ;->a(LasJ;)LasJ;

    move-result-object v1

    move-object v0, v1

    :goto_1
    move-object v1, v0

    .line 640
    goto :goto_0

    .line 641
    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method static synthetic a(Latd;)Latj;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Latd;->a:Latj;

    return-object v0
.end method

.method static synthetic a(Latd;)Latl;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Latd;->a:Latl;

    return-object v0
.end method

.method static synthetic a(Latd;)Latm;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Latd;->a:Latm;

    return-object v0
.end method

.method static synthetic a(Latd;)Lato;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Latd;->a:Lato;

    return-object v0
.end method

.method static synthetic a(Latd;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Latd;->a:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method

.method private declared-synchronized a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 707
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Latd;->a:Latm;

    if-eqz v0, :cond_0

    .line 708
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "WelcomePending"

    iget-object v2, p0, Latd;->a:Latm;

    .line 709
    invoke-virtual {v2}, Latm;->a()Ljava/lang/String;

    move-result-object v2

    .line 708
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 709
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 711
    :cond_0
    monitor-exit p0

    return-void

    .line 707
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Landroid/content/Context;LasJ;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 54
    invoke-static {p0, p1, p2}, Latd;->b(Landroid/content/Context;LasJ;Landroid/net/Uri;)V

    return-void
.end method

.method private a(Landroid/content/Context;Latn;)V
    .locals 1

    .prologue
    .line 560
    invoke-virtual {p0, p1}, Latd;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 561
    invoke-direct {p0, p2}, Latd;->a(Latn;)V

    .line 563
    :cond_0
    return-void
.end method

.method private a(Landroid/content/Context;Latn;LaFO;)V
    .locals 2

    .prologue
    .line 617
    iget-object v0, p0, Latd;->a:Lasb;

    invoke-static {p2}, Latn;->a(Latn;)I

    move-result v1

    invoke-interface {v0, p1, v1, p3}, Lasb;->a(Landroid/content/Context;ILaFO;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 618
    iget-object v0, p0, Latd;->a:Lasa;

    invoke-virtual {v0}, Lasa;->a()I

    move-result v0

    invoke-static {p2, v0}, Latn;->a(Latn;I)I

    .line 619
    iget-object v0, p0, Latd;->a:Lato;

    invoke-virtual {v0, p2}, Lato;->a(Latn;)V

    .line 621
    :cond_0
    return-void
.end method

.method static synthetic a(Latd;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1}, Latd;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Latn;)V
    .locals 1

    .prologue
    .line 606
    const/4 v0, 0x1

    invoke-static {p1, v0}, Latn;->a(Latn;Z)Z

    .line 607
    iget-object v0, p0, Latd;->a:Lasa;

    invoke-virtual {v0}, Lasa;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 609
    iget-object v0, p0, Latd;->a:Lasa;

    invoke-virtual {v0}, Lasa;->a()I

    move-result v0

    invoke-static {p1, v0}, Latn;->a(Latn;I)I

    .line 611
    :cond_0
    iget-object v0, p0, Latd;->a:Lato;

    invoke-virtual {v0, p1}, Lato;->a(Latn;)V

    .line 612
    return-void
.end method

.method private declared-synchronized b(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 714
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Latd;->a:Latm;

    if-nez v0, :cond_0

    .line 715
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "WelcomePending"

    const-string v2, "{}"

    .line 716
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 717
    invoke-static {v0}, Latm;->a(Ljava/lang/String;)Latm;

    move-result-object v0

    iput-object v0, p0, Latd;->a:Latm;

    .line 718
    const-string v0, "WelcomeController"

    const-string v1, "from Preferences: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Latd;->a:Latm;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 720
    :cond_0
    monitor-exit p0

    return-void

    .line 714
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static b(Landroid/content/Context;LasJ;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 696
    const-string v0, "WelcomeController"

    const-string v1, "Show story %s/%s (%d pages). "

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 697
    invoke-virtual {p1}, LasJ;->a()LasO;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    invoke-virtual {p1}, LasJ;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 696
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 698
    invoke-static {p0, p1}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->a(Landroid/content/Context;LasJ;)Landroid/content/Intent;

    move-result-object v0

    .line 699
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 700
    return-void
.end method

.method private b(Landroid/content/Context;Latn;)V
    .locals 3

    .prologue
    .line 654
    new-instance v0, Latg;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Get and show announce "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Latd;->a:Latm;

    .line 655
    invoke-static {v2}, Latm;->b(Latm;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, p2, p1}, Latg;-><init>(Latd;Ljava/lang/String;Latn;Landroid/content/Context;)V

    .line 684
    iget-object v1, p0, Latd;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 685
    invoke-virtual {v0}, LpD;->start()V

    .line 686
    return-void
.end method

.method private b(Landroid/content/Context;)Z
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 588
    const-string v0, "WelcomeController"

    const-string v1, "New welcome launcher is: %s"

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, Latd;->a:LatC;

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 589
    iget-object v0, p0, Latd;->a:LatC;

    invoke-interface {v0}, LatC;->a()Landroid/content/Intent;

    move-result-object v0

    .line 590
    const-string v1, "WelcomeController"

    const-string v2, "Intent is: %s"

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 591
    iget-object v1, p0, Latd;->a:Lati;

    invoke-interface {v1}, Lati;->b()LasP;

    move-result-object v1

    .line 592
    if-eqz v1, :cond_0

    .line 593
    sget-object v2, LasO;->a:LasO;

    invoke-static {v1, v2}, LasJ;->a(LasP;LasO;)LasJ;

    move-result-object v1

    .line 594
    invoke-virtual {v1}, LasJ;->a()I

    move-result v2

    if-lez v2, :cond_0

    .line 595
    invoke-static {p1, v1}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->a(Landroid/content/Context;LasJ;)Landroid/content/Intent;

    move-result-object v1

    .line 596
    const/4 v2, 0x2

    new-array v2, v2, [Landroid/content/Intent;

    aput-object v1, v2, v5

    aput-object v0, v2, v4

    invoke-virtual {p1, v2}, Landroid/content/Context;->startActivities([Landroid/content/Intent;)V

    .line 602
    :goto_0
    return v4

    .line 601
    :cond_0
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method a()LbmY;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmY",
            "<",
            "Lry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 742
    sget-object v0, Latd;->a:LbmY;

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 482
    iget-object v0, p0, Latd;->a:LtK;

    sget-object v1, Lry;->aQ:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 490
    :cond_0
    :goto_0
    return-void

    .line 486
    :cond_1
    iget-object v0, p0, Latd;->a:Lato;

    invoke-virtual {v0}, Lato;->a()Latn;

    move-result-object v0

    .line 487
    invoke-static {v0}, Latn;->a(Latn;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 488
    invoke-direct {p0, v0}, Latd;->a(Latn;)V

    goto :goto_0
.end method

.method public a(Landroid/app/Activity;ZLaFO;)V
    .locals 3

    .prologue
    .line 499
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 500
    if-eqz v0, :cond_1

    invoke-static {v0}, Lui;->a(Landroid/content/Intent;)Lui;

    move-result-object v1

    sget-object v2, Lui;->c:Lui;

    if-ne v1, v2, :cond_1

    .line 501
    iget-object v1, p0, Latd;->a:LqK;

    const-string v2, "/welcome/fromIntent"

    invoke-virtual {v1, v2, v0}, LqK;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 502
    invoke-virtual {p0, p1}, Latd;->a(Landroid/content/Context;)Z

    .line 506
    :cond_0
    :goto_0
    return-void

    .line 503
    :cond_1
    if-eqz p2, :cond_0

    .line 504
    invoke-virtual {p0, p1, p3}, Latd;->a(Landroid/content/Context;LaFO;)V

    goto :goto_0
.end method

.method a(Landroid/content/Context;LQr;)V
    .locals 7

    .prologue
    .line 420
    const-string v0, "checkCurrentAnnounce: "

    .line 422
    :try_start_0
    invoke-direct {p0, p1}, Latd;->b(Landroid/content/Context;)V

    .line 423
    iget-object v0, p0, Latd;->a:Latj;

    invoke-interface {v0}, Latj;->a()Ljava/lang/String;

    move-result-object v0

    .line 424
    const-string v1, "announcesCatalogURL"

    invoke-interface {p2, v1, v0}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 426
    invoke-static {v0}, Lbju;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Latd;->a:Latm;

    .line 427
    invoke-static {v1}, Latm;->a(Latm;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 430
    :cond_0
    const-string v0, "WelcomeController"

    const-string v1, "%s no new Catalog."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "checkCurrentAnnounce: "

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 479
    :cond_1
    :goto_0
    return-void

    .line 434
    :cond_2
    iget-object v1, p0, Latd;->a:Latj;

    .line 435
    invoke-interface {v1, v0}, Latj;->b(Ljava/lang/String;)Latj;

    move-result-object v1

    invoke-static {}, Latd;->a()I

    move-result v2

    invoke-interface {v1, v2}, Latj;->a(I)Lath;

    move-result-object v1

    .line 438
    invoke-virtual {v1}, Lath;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbju;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 439
    const-string v1, "WelcomeController"

    const-string v2, "%s new Catalog %s doesn\'t have an entry for version %s or is corrupted."

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "checkCurrentAnnounce: "

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    const/4 v0, 0x2

    .line 440
    invoke-static {}, Latd;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    .line 439
    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 472
    :catch_0
    move-exception v0

    .line 474
    const-string v1, "WelcomeController"

    const-string v2, "checkCurrentAnnounce:  unexpected exception, moving on."

    invoke-static {v1, v0, v2}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 475
    iget-object v1, p0, Latd;->a:LtK;

    sget-object v2, Lry;->ab:Lry;

    invoke-interface {v1, v2}, LtK;->a(LtJ;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 476
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 445
    :cond_3
    :try_start_1
    iget-object v2, p0, Latd;->a:Latm;

    invoke-static {v2}, Latm;->b(Latm;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lath;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 446
    const-string v2, "WelcomeController"

    const-string v3, "%s The previous announce (%s) is still current"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "checkCurrentAnnounce: "

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 448
    iget-object v1, p0, Latd;->a:Latm;

    invoke-static {v1, v0}, Latm;->a(Latm;Ljava/lang/String;)Ljava/lang/String;

    .line 449
    invoke-direct {p0, p1}, Latd;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 454
    :cond_4
    const-string v2, "WelcomeController"

    const-string v3, "%s There is a new current announce at : %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "checkCurrentAnnounce: "

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 455
    invoke-virtual {v1}, Lath;->a()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 457
    iget-object v2, p0, Latd;->a:Latj;

    new-instance v3, Latf;

    invoke-direct {v3, p0, v1, v0, p1}, Latf;-><init>(Latd;Lath;Ljava/lang/String;Landroid/content/Context;)V

    invoke-interface {v2, v1, v3}, Latj;->a(Lath;Latk;)Ljava/lang/Thread;

    goto/16 :goto_0

    .line 468
    :cond_5
    iget-object v2, p0, Latd;->a:Latm;

    invoke-virtual {v1}, Lath;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Latm;->b(Latm;Ljava/lang/String;)Ljava/lang/String;

    .line 469
    iget-object v1, p0, Latd;->a:Latm;

    invoke-static {v1, v0}, Latm;->a(Latm;Ljava/lang/String;)Ljava/lang/String;

    .line 470
    invoke-direct {p0, p1}, Latd;->a(Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public a(Landroid/content/Context;LaFO;)V
    .locals 4

    .prologue
    .line 517
    :try_start_0
    iget-object v0, p0, Latd;->a:Lato;

    invoke-virtual {v0}, Lato;->a()Latn;

    move-result-object v0

    .line 519
    invoke-static {v0}, Latn;->a(Latn;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 520
    iget-object v1, p0, Latd;->a:LqK;

    const-string v2, "/welcome"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LqK;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 521
    invoke-direct {p0, p1, v0}, Latd;->a(Landroid/content/Context;Latn;)V

    .line 539
    :cond_0
    :goto_0
    return-void

    .line 522
    :cond_1
    iget-object v1, p0, Latd;->a:Lasa;

    .line 523
    invoke-virtual {v1}, Lasa;->a()I

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Latd;->a:Lasa;

    .line 524
    invoke-virtual {v1}, Lasa;->a()I

    move-result v1

    invoke-static {v0}, Latn;->a(Latn;)I

    move-result v2

    if-eq v1, v2, :cond_2

    .line 525
    iget-object v1, p0, Latd;->a:LqK;

    const-string v2, "/welcome/highlights"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LqK;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 526
    invoke-direct {p0, p1, v0, p2}, Latd;->a(Landroid/content/Context;Latn;LaFO;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 535
    :catch_0
    move-exception v0

    .line 537
    const-string v1, "WelcomeController"

    const-string v2, "MaybeShowSomething: unexpected exception, moving on."

    invoke-static {v1, v0, v2}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0

    .line 528
    :cond_2
    :try_start_1
    invoke-direct {p0, p1}, Latd;->b(Landroid/content/Context;)V

    .line 529
    iget-object v1, p0, Latd;->a:Latm;

    invoke-static {v1}, Latm;->a(Latm;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Latd;->a:Latm;

    .line 530
    invoke-static {v1}, Latm;->b(Latm;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Latn;->a(Latn;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 531
    iget-object v1, p0, Latd;->a:LqK;

    const-string v2, "/welcome/announce"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LqK;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 532
    invoke-direct {p0, p1, v0}, Latd;->b(Landroid/content/Context;Latn;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 567
    iget-object v1, p0, Latd;->a:LtK;

    sget-object v2, Lry;->W:Lry;

    invoke-interface {v1, v2}, LtK;->a(LtJ;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_1

    iget-object v1, p0, Latd;->a:LatC;

    if-eqz v1, :cond_1

    .line 570
    const-string v1, "WelcomeController"

    const-string v2, "Using native warm welcome."

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 571
    invoke-direct {p0, p1}, Latd;->b(Landroid/content/Context;)Z

    move-result v0

    .line 581
    :cond_0
    :goto_0
    return v0

    .line 573
    :cond_1
    const-string v1, "WelcomeController"

    const-string v2, "Not using native warm welcome."

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 574
    invoke-direct {p0}, Latd;->a()LasJ;

    move-result-object v1

    .line 575
    invoke-virtual {v1}, LasJ;->a()I

    move-result v2

    if-lez v2, :cond_0

    .line 576
    iget-object v0, p0, Latd;->a:Lati;

    invoke-interface {v0}, Lati;->a()LasP;

    move-result-object v0

    invoke-interface {v0}, LasP;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-static {p1, v1, v0}, Latd;->b(Landroid/content/Context;LasJ;Landroid/net/Uri;)V

    .line 577
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 546
    iget-object v0, p0, Latd;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LpD;

    .line 547
    if-eqz v0, :cond_0

    .line 548
    invoke-virtual {v0}, LpD;->a()V

    .line 549
    const-string v0, "WelcomeController"

    const-string v1, "Cancel launch of Announce as user moved on already."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Latd;->a:Latl;

    invoke-virtual {v0}, Latl;->run()V

    .line 556
    return-void
.end method
