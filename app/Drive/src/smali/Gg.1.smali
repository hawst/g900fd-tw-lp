.class public LGg;
.super Lbuo;
.source "DriveDocumentOpenerModule.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lbuo;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 22
    const-class v0, LGE;

    invoke-virtual {p0, v0}, LGg;->a(Ljava/lang/Class;)LbuQ;

    .line 23
    const-class v0, LGA;

    invoke-virtual {p0, v0}, LGg;->a(Ljava/lang/Class;)LbuQ;

    .line 25
    const-class v0, LFR;

    invoke-virtual {p0, v0}, LGg;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    const-string v1, "DefaultRemote"

    invoke-static {v1}, Lbwo;->a(Ljava/lang/String;)Lbwm;

    move-result-object v1

    invoke-interface {v0, v1}, LbuQ;->a(Ljava/lang/annotation/Annotation;)LbuT;

    move-result-object v0

    const-class v1, LGv;

    .line 26
    invoke-interface {v0, v1}, LbuT;->a(Ljava/lang/Class;)LbuU;

    .line 27
    const-class v0, LFX;

    invoke-virtual {p0, v0}, LGg;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    const-class v1, LGh;

    invoke-interface {v0, v1}, LbuQ;->a(Ljava/lang/Class;)LbuU;

    .line 28
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;

    invoke-virtual {p0, v0}, LGg;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;

    invoke-interface {v0, v1}, LbuQ;->a(Ljava/lang/Class;)LbuU;

    .line 29
    const-class v0, LHm;

    invoke-virtual {p0, v0}, LGg;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    const-class v1, LHn;

    invoke-interface {v0, v1}, LbuQ;->a(Ljava/lang/Class;)LbuU;

    .line 31
    const-class v0, LGZ;

    invoke-virtual {p0, v0}, LGg;->a(Ljava/lang/Class;)LbuQ;

    move-result-object v0

    const-class v1, LHa;

    invoke-interface {v0, v1}, LbuQ;->a(Ljava/lang/Class;)LbuU;

    .line 32
    return-void
.end method

.method provideLocalGDocsAsPdfOpener(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;LGC;)Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;",
            "LGC;",
            ")",
            "Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;",
            "Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p2, p1, p1}, LGC;->a(LFR;LFR;)Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;

    move-result-object v0

    return-object v0
.end method

.method provideRemoteGDocsAsPdfOpener(LGd;Lcom/google/android/apps/docs/doclist/documentopener/PdfExportDocumentOpener;LGC;)Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;
    .locals 1
    .annotation runtime LbuF;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LGd;",
            "Lcom/google/android/apps/docs/doclist/documentopener/PdfExportDocumentOpener;",
            "LGC;",
            ")",
            "Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;",
            "Lcom/google/android/apps/docs/doclist/documentopener/PdfExportDocumentOpener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    sget-object v0, LGj;->a:LGj;

    invoke-virtual {p1, v0}, LGd;->a(LGj;)Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    move-result-object v0

    .line 42
    invoke-virtual {p3, v0, p2}, LGC;->a(LFR;LFR;)Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;

    move-result-object v0

    return-object v0
.end method
