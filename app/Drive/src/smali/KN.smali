.class public final enum LKN;
.super Ljava/lang/Enum;
.source "FloatingHandleView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LKN;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LKN;

.field private static final synthetic a:[LKN;

.field public static final enum b:LKN;

.field public static final enum c:LKN;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 49
    new-instance v0, LKN;

    const-string v1, "NOT_DRAGGING"

    invoke-direct {v0, v1, v2}, LKN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LKN;->a:LKN;

    .line 50
    new-instance v0, LKN;

    const-string v1, "STARTING"

    invoke-direct {v0, v1, v3}, LKN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LKN;->b:LKN;

    .line 51
    new-instance v0, LKN;

    const-string v1, "DRAGGING"

    invoke-direct {v0, v1, v4}, LKN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LKN;->c:LKN;

    .line 48
    const/4 v0, 0x3

    new-array v0, v0, [LKN;

    sget-object v1, LKN;->a:LKN;

    aput-object v1, v0, v2

    sget-object v1, LKN;->b:LKN;

    aput-object v1, v0, v3

    sget-object v1, LKN;->c:LKN;

    aput-object v1, v0, v4

    sput-object v0, LKN;->a:[LKN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LKN;
    .locals 1

    .prologue
    .line 48
    const-class v0, LKN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LKN;

    return-object v0
.end method

.method public static values()[LKN;
    .locals 1

    .prologue
    .line 48
    sget-object v0, LKN;->a:[LKN;

    invoke-virtual {v0}, [LKN;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LKN;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 54
    sget-object v0, LKN;->a:LKN;

    invoke-virtual {p0, v0}, LKN;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 58
    sget-object v0, LKN;->b:LKN;

    invoke-virtual {p0, v0}, LKN;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 62
    sget-object v0, LKN;->c:LKN;

    invoke-virtual {p0, v0}, LKN;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 66
    sget-object v0, LKN;->a:LKN;

    invoke-virtual {p0, v0}, LKN;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
