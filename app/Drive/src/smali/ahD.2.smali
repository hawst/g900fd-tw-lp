.class public LahD;
.super Ljava/lang/Object;
.source "EntryMetadataSyncer.java"


# instance fields
.field private final a:LaGM;

.field private final a:Laer;

.field private final a:Lagl;

.field private final a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "LagW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LaGM;Laja;Laer;Lagl;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGM;",
            "Laja",
            "<",
            "LagW;",
            ">;",
            "Laer;",
            "Lagl;",
            ")V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, LahD;->a:LaGM;

    .line 40
    iput-object p2, p0, LahD;->a:Laja;

    .line 41
    iput-object p3, p0, LahD;->a:Laer;

    .line 42
    iput-object p4, p0, LahD;->a:Lagl;

    .line 43
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 4

    .prologue
    .line 78
    :try_start_0
    const-string v0, "MetadataSyncer"

    const-string v1, "Syncing metadata: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 79
    iget-object v0, p0, LahD;->a:LaGM;

    invoke-interface {v0, p1}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    .line 80
    if-nez v0, :cond_0

    .line 97
    :goto_0
    return-void

    .line 84
    :cond_0
    iget-object v1, p0, LahD;->a:LaGM;

    iget-object v2, v0, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    invoke-interface {v1, v2}, LaGM;->a(LaFO;)LaFM;

    move-result-object v1

    .line 85
    iget-object v2, p0, LahD;->a:Laer;

    invoke-interface {v2, v0}, Laer;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaeF;

    move-result-object v0

    .line 86
    iget-object v2, p0, LahD;->a:Lagl;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v2, v1, v0, v3}, Lagl;->a(LaFM;LaJT;Ljava/lang/Boolean;)V
    :try_end_0
    .catch LbwO; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lbxk; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 87
    :catch_0
    move-exception v0

    .line 88
    const-string v1, "MetadataSyncer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed authentication"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Failed authentication"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 90
    :catch_1
    move-exception v0

    .line 91
    const-string v1, "MetadataSyncer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to parse entry metadata"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Failed to parse entry metadata"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 93
    :catch_2
    move-exception v0

    .line 94
    const-string v1, "MetadataSyncer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to parse entry metadata"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Failed to parse entry metadata"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;LahS;)Z
    .locals 1

    .prologue
    .line 51
    sget-object v0, LahS;->a:LahS;

    invoke-virtual {v0, p2}, LahS;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 52
    const/4 v0, 0x0

    .line 55
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LahD;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LagW;

    invoke-virtual {v0, p1}, LagW;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Z

    move-result v0

    goto :goto_0
.end method

.method public b(Lcom/google/android/gms/drive/database/data/EntrySpec;LahS;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 62
    const-string v0, "entrySpec"

    invoke-static {p1, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    const-string v0, "taskType"

    invoke-static {p2, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object v0, LahS;->a:LahS;

    invoke-virtual {v0, p2}, LahS;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 73
    :goto_0
    return v0

    .line 69
    :cond_0
    iget-object v0, p0, LahD;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LagW;

    invoke-virtual {v0, p1}, LagW;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 70
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 73
    goto :goto_0
.end method
