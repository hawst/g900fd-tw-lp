.class public LaGp;
.super LaGm;
.source "DocumentContent.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGm",
        "<",
        "LaEI;",
        "LaEz;",
        ">;"
    }
.end annotation


# instance fields
.field private a:J

.field private a:Ljava/io/File;

.field private a:Ljava/lang/Long;

.field a:Ljava/lang/String;

.field private a:Ljava/util/Date;

.field private a:Ljavax/crypto/SecretKey;

.field private a:Z

.field private b:Ljava/io/File;

.field private b:Ljava/lang/Long;

.field b:Ljava/lang/String;

.field private b:Z

.field private c:Ljava/lang/Long;

.field private c:Ljava/lang/String;

.field private c:Z

.field private d:Ljava/lang/Long;

.field private d:Ljava/lang/String;

.field private d:Z

.field private e:Ljava/lang/String;

.field private e:Z

.field private f:Z


# direct methods
.method private constructor <init>(LaEz;Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljavax/crypto/SecretKey;Ljava/lang/String;ZLjava/lang/Long;ZLjava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/Date;Ljava/lang/String;ZZZLjava/lang/String;Ljava/lang/String;)V
    .locals 21

    .prologue
    .line 278
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v2

    sget-object v3, LaEG;->c:LaEG;

    .line 279
    invoke-virtual {v3}, LaEG;->a()Landroid/net/Uri;

    move-result-object v3

    .line 278
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2, v3}, LaGm;-><init>(LaFm;LaFy;Landroid/net/Uri;)V

    .line 280
    invoke-static/range {p2 .. p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    if-eqz p7, :cond_0

    if-nez p9, :cond_2

    :cond_0
    const/4 v2, 0x1

    :goto_0
    const-string v3, "Dirty temporary content?"

    invoke-static {v2, v3}, LbiT;->a(ZLjava/lang/Object;)V

    .line 282
    if-nez p3, :cond_3

    const/4 v2, 0x1

    move v3, v2

    :goto_1
    if-nez p4, :cond_4

    const/4 v2, 0x1

    :goto_2
    if-eq v3, v2, :cond_5

    const/4 v2, 0x1

    :goto_3
    invoke-static {v2}, LbiT;->a(Z)V

    .line 283
    if-eqz p4, :cond_1

    if-nez p7, :cond_6

    :cond_1
    const/4 v2, 0x1

    :goto_4
    invoke-static {v2}, LbiT;->a(Z)V

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move/from16 v16, p15

    move/from16 v17, p16

    move/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    .line 285
    invoke-direct/range {v2 .. v20}, LaGp;->a(Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljavax/crypto/SecretKey;Ljava/lang/String;ZLjava/lang/Long;ZLjava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/Date;Ljava/lang/String;ZZZLjava/lang/String;Ljava/lang/String;)V

    .line 303
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, LaGp;->f:Z

    .line 304
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, LaGp;->a:J

    .line 305
    return-void

    .line 281
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 282
    :cond_3
    const/4 v2, 0x0

    move v3, v2

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    :cond_5
    const/4 v2, 0x0

    goto :goto_3

    .line 283
    :cond_6
    const/4 v2, 0x0

    goto :goto_4
.end method

.method synthetic constructor <init>(LaEz;Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljavax/crypto/SecretKey;Ljava/lang/String;ZLjava/lang/Long;ZLjava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/Date;Ljava/lang/String;ZZZLjava/lang/String;Ljava/lang/String;LaGq;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct/range {p0 .. p19}, LaGp;-><init>(LaEz;Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljavax/crypto/SecretKey;Ljava/lang/String;ZLjava/lang/Long;ZLjava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/Date;Ljava/lang/String;ZZZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static a(LaEz;Landroid/database/Cursor;)LaGp;
    .locals 7

    .prologue
    .line 745
    sget-object v0, LaEJ;->b:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 746
    invoke-static {p0, v0}, LaGp;->a(LaEz;Ljava/lang/String;)LaGr;

    move-result-object v2

    .line 748
    sget-object v0, LaEJ;->g:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 749
    sget-object v1, LaEJ;->h:LaEJ;

    .line 750
    invoke-virtual {v1}, LaEJ;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    .line 753
    if-nez v0, :cond_0

    if-nez v3, :cond_0

    const-string v0, ""

    .line 754
    :cond_0
    sget-object v1, LaEJ;->d:LaEJ;

    invoke-virtual {v1}, LaEJ;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    .line 759
    sget-object v1, LaEJ;->q:LaEJ;

    .line 760
    invoke-virtual {v1}, LaEJ;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v1

    .line 761
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 763
    sget-object v1, LaEJ;->s:LaEJ;

    invoke-virtual {v1}, LaEJ;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1, p1}, LaFr;->a(Landroid/database/Cursor;)Z

    move-result v1

    .line 764
    invoke-virtual {v2, v1}, LaGr;->a(Z)LaGr;

    .line 766
    sget-object v1, LaEJ;->c:LaEJ;

    .line 767
    invoke-virtual {v1}, LaEJ;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 766
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v6

    .line 769
    const/4 v1, 0x0

    .line 770
    if-eqz v4, :cond_1

    if-eqz v6, :cond_1

    .line 771
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    invoke-direct {v1, v6, v4}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 774
    :cond_1
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 775
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, LaGr;->b(Ljava/io/File;)LaGr;

    .line 782
    :goto_1
    sget-object v0, LaEJ;->a:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LaGr;->b(Ljava/lang/String;)LaGr;

    .line 784
    sget-object v0, LaEJ;->l:LaEJ;

    .line 785
    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    .line 786
    if-eqz v0, :cond_2

    .line 787
    invoke-virtual {v2, v0}, LaGr;->b(Ljava/lang/Long;)LaGr;

    .line 790
    :cond_2
    sget-object v0, LaEJ;->m:LaEJ;

    .line 791
    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    .line 792
    if-eqz v0, :cond_3

    .line 793
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v1}, LaGr;->a(Ljava/util/Date;)LaGr;

    .line 796
    :cond_3
    sget-object v0, LaEJ;->o:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 797
    if-eqz v0, :cond_4

    .line 798
    invoke-virtual {v2, v0}, LaGr;->a(Ljava/lang/String;)LaGr;

    .line 801
    :cond_4
    sget-object v0, LaEJ;->j:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    .line 802
    if-eqz v0, :cond_5

    .line 803
    invoke-virtual {v2, v0}, LaGr;->c(Ljava/lang/Long;)LaGr;

    .line 806
    :cond_5
    sget-object v0, LaEJ;->r:LaEJ;

    .line 807
    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    .line 808
    if-eqz v0, :cond_6

    .line 809
    invoke-virtual {v2, v0}, LaGr;->a(Ljava/lang/Long;)LaGr;

    .line 812
    :cond_6
    sget-object v0, LaEJ;->v:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    .line 813
    if-eqz v0, :cond_7

    .line 814
    invoke-virtual {v2, v0}, LaGr;->d(Ljava/lang/Long;)LaGr;

    .line 817
    :cond_7
    sget-object v0, LaEJ;->p:LaEJ;

    .line 818
    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Z

    move-result v0

    .line 817
    invoke-virtual {v2, v0}, LaGr;->b(Z)LaGr;

    .line 820
    sget-object v0, LaEJ;->e:LaEJ;

    .line 821
    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Z

    move-result v0

    .line 820
    invoke-virtual {v2, v0}, LaGr;->c(Z)LaGr;

    .line 823
    sget-object v0, LaEJ;->f:LaEJ;

    .line 824
    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Z

    move-result v0

    .line 823
    invoke-virtual {v2, v0}, LaGr;->d(Z)LaGr;

    .line 826
    sget-object v0, LaEJ;->w:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LaGr;->c(Ljava/lang/String;)LaGr;

    .line 828
    sget-object v0, LaEJ;->x:LaEJ;

    .line 829
    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 828
    invoke-virtual {v2, v0}, LaGr;->d(Ljava/lang/String;)LaGr;

    .line 831
    invoke-virtual {v2}, LaGr;->a()LaGp;

    move-result-object v0

    .line 833
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v1

    invoke-virtual {v1}, LaEI;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, LaFr;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 834
    invoke-virtual {v0, v2, v3}, LaGp;->c(J)V

    .line 836
    return-object v0

    .line 761
    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 776
    :cond_9
    if-eqz v3, :cond_a

    .line 777
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, LaGr;->a(Ljava/io/File;)LaGr;

    goto/16 :goto_1

    .line 779
    :cond_a
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v1}, LaGr;->a(Ljava/io/File;Ljavax/crypto/SecretKey;)LaGr;

    goto/16 :goto_1
.end method

.method public static a(LaEz;Ljava/lang/String;)LaGr;
    .locals 1

    .prologue
    .line 412
    new-instance v0, LaGr;

    invoke-direct {v0, p0, p1}, LaGr;-><init>(LaEz;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 872
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LaEJ;->t:LaEJ;

    invoke-virtual {v1}, LaEJ;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " < "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LaGv;Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 891
    invoke-virtual {p0}, LaGv;->b()Ljava/lang/String;

    move-result-object v0

    .line 892
    if-eqz p1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".db"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljavax/crypto/SecretKey;Ljava/lang/String;ZLjava/lang/Long;ZLjava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/Date;Ljava/lang/String;ZZZLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 329
    iput-object p1, p0, LaGp;->c:Ljava/lang/String;

    .line 330
    iput-object p2, p0, LaGp;->a:Ljava/io/File;

    .line 331
    iput-object p3, p0, LaGp;->b:Ljava/io/File;

    .line 332
    iput-object p4, p0, LaGp;->a:Ljavax/crypto/SecretKey;

    .line 333
    iput-object p5, p0, LaGp;->d:Ljava/lang/String;

    .line 334
    iput-boolean p6, p0, LaGp;->a:Z

    .line 335
    iput-object p7, p0, LaGp;->a:Ljava/lang/Long;

    .line 336
    iput-boolean p8, p0, LaGp;->e:Z

    .line 337
    iput-object p9, p0, LaGp;->d:Ljava/lang/Long;

    .line 338
    iput-object p10, p0, LaGp;->b:Ljava/lang/Long;

    .line 339
    iput-object p11, p0, LaGp;->c:Ljava/lang/Long;

    .line 340
    iput-object p12, p0, LaGp;->a:Ljava/util/Date;

    .line 341
    iput-object p13, p0, LaGp;->e:Ljava/lang/String;

    .line 342
    iput-boolean p14, p0, LaGp;->b:Z

    .line 343
    move/from16 v0, p15

    iput-boolean v0, p0, LaGp;->c:Z

    .line 344
    move/from16 v0, p16

    iput-boolean v0, p0, LaGp;->d:Z

    .line 345
    move-object/from16 v0, p17

    iput-object v0, p0, LaGp;->a:Ljava/lang/String;

    .line 346
    move-object/from16 v0, p18

    iput-object v0, p0, LaGp;->b:Ljava/lang/String;

    .line 347
    return-void
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 899
    const-string v0, ".db"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()LadY;
    .locals 2

    .prologue
    .line 566
    iget-object v0, p0, LaGp;->b:Ljava/io/File;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LadY;

    iget-object v1, p0, LaGp;->b:Ljava/io/File;

    invoke-direct {v0, v1}, LadY;-><init>(Ljava/io/File;)V

    goto :goto_0
.end method

.method public a()Ljava/io/File;
    .locals 1

    .prologue
    .line 558
    iget-object v0, p0, LaGp;->a:Ljava/io/File;

    return-object v0
.end method

.method public a()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 437
    iget-object v0, p0, LaGp;->a:Ljava/lang/Long;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, LaGp;->d:Ljava/lang/String;

    return-object v0
.end method

.method public a()Ljava/util/Date;
    .locals 1

    .prologue
    .line 503
    iget-object v0, p0, LaGp;->a:Ljava/util/Date;

    return-object v0
.end method

.method public a()Ljavax/crypto/SecretKey;
    .locals 1

    .prologue
    .line 615
    iget-object v0, p0, LaGp;->a:Ljavax/crypto/SecretKey;

    return-object v0
.end method

.method public a()V
    .locals 26

    .prologue
    .line 359
    invoke-virtual/range {p0 .. p0}, LaGp;->c()J

    move-result-wide v24

    .line 360
    const-wide/16 v4, 0x0

    cmp-long v4, v24, v4

    if-gez v4, :cond_0

    .line 400
    :goto_0
    return-void

    .line 363
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v4, v0, LaGp;->f:Z

    if-nez v4, :cond_1

    const/4 v4, 0x1

    :goto_1
    const-string v5, "Attempt to reload modified instance"

    invoke-static {v4, v5}, LbiT;->b(ZLjava/lang/Object;)V

    .line 364
    move-object/from16 v0, p0

    iget-object v4, v0, LaGp;->a:LaFm;

    check-cast v4, LaEz;

    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v5

    invoke-virtual {v5}, LaEI;->c()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 366
    invoke-static {}, LaEI;->a()LaEI;

    move-result-object v8

    invoke-virtual {v8}, LaEI;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "=?"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    .line 367
    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 364
    invoke-virtual/range {v4 .. v11}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v23

    .line 372
    :try_start_0
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_2

    .line 398
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 363
    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    .line 376
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, LaGp;->a:LaFm;

    check-cast v4, LaEz;

    move-object/from16 v0, v23

    invoke-static {v4, v0}, LaGp;->a(LaEz;Landroid/database/Cursor;)LaGp;

    move-result-object v4

    .line 377
    iget-object v5, v4, LaGp;->c:Ljava/lang/String;

    iget-object v6, v4, LaGp;->a:Ljava/io/File;

    iget-object v7, v4, LaGp;->b:Ljava/io/File;

    iget-object v8, v4, LaGp;->a:Ljavax/crypto/SecretKey;

    iget-object v9, v4, LaGp;->d:Ljava/lang/String;

    iget-boolean v10, v4, LaGp;->a:Z

    iget-object v11, v4, LaGp;->a:Ljava/lang/Long;

    iget-boolean v12, v4, LaGp;->e:Z

    iget-object v13, v4, LaGp;->d:Ljava/lang/Long;

    iget-object v14, v4, LaGp;->b:Ljava/lang/Long;

    iget-object v15, v4, LaGp;->c:Ljava/lang/Long;

    iget-object v0, v4, LaGp;->a:Ljava/util/Date;

    move-object/from16 v16, v0

    iget-object v0, v4, LaGp;->e:Ljava/lang/String;

    move-object/from16 v17, v0

    iget-boolean v0, v4, LaGp;->b:Z

    move/from16 v18, v0

    iget-boolean v0, v4, LaGp;->c:Z

    move/from16 v19, v0

    iget-boolean v0, v4, LaGp;->d:Z

    move/from16 v20, v0

    iget-object v0, v4, LaGp;->a:Ljava/lang/String;

    move-object/from16 v21, v0

    iget-object v0, v4, LaGp;->b:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v22}, LaGp;->a(Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljavax/crypto/SecretKey;Ljava/lang/String;ZLjava/lang/Long;ZLjava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/Date;Ljava/lang/String;ZZZLjava/lang/String;Ljava/lang/String;)V

    .line 396
    move-object/from16 v0, p0

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, LaGp;->c(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 398
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :catchall_0
    move-exception v4

    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    throw v4
.end method

.method a(J)V
    .locals 1

    .prologue
    .line 442
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LaGp;->a:Ljava/lang/Long;

    .line 443
    const/4 v0, 0x1

    iput-boolean v0, p0, LaGp;->f:Z

    .line 444
    return-void
.end method

.method public a(LaGp;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 447
    invoke-virtual {p0}, LaGp;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LaGp;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 448
    invoke-virtual {p1}, LaGp;->c()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, LaGp;->a(J)V

    .line 449
    iput-boolean v1, p0, LaGp;->f:Z

    .line 450
    return-void

    .line 447
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 700
    sget-object v0, LaEJ;->a:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaGp;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    sget-object v0, LaEJ;->b:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaGp;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 702
    sget-object v0, LaEJ;->g:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaGp;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    sget-object v0, LaEJ;->h:LaEJ;

    .line 704
    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaGp;->g()Ljava/lang/String;

    move-result-object v3

    .line 703
    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 705
    sget-object v0, LaEJ;->j:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaGp;->c()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 706
    sget-object v0, LaEJ;->l:LaEJ;

    .line 707
    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaGp;->d()Ljava/lang/Long;

    move-result-object v3

    .line 706
    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 708
    sget-object v0, LaEJ;->q:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v3

    iget-boolean v0, p0, LaGp;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 709
    sget-object v0, LaEJ;->s:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v3

    iget-boolean v0, p0, LaGp;->e:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 710
    sget-object v0, LaEJ;->r:LaEJ;

    .line 711
    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LaGp;->a:Ljava/lang/Long;

    .line 710
    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 712
    sget-object v0, LaEJ;->v:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LaGp;->d:Ljava/lang/Long;

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 713
    sget-object v0, LaEJ;->o:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaGp;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    sget-object v0, LaEJ;->p:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v3

    iget-boolean v0, p0, LaGp;->b:Z

    if-eqz v0, :cond_2

    move v0, v1

    .line 715
    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 714
    invoke-virtual {p1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 716
    sget-object v0, LaEJ;->e:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v3

    iget-boolean v0, p0, LaGp;->c:Z

    if-eqz v0, :cond_3

    move v0, v1

    .line 717
    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 716
    invoke-virtual {p1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 718
    sget-object v0, LaEJ;->f:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-boolean v3, p0, LaGp;->d:Z

    if-eqz v3, :cond_4

    .line 719
    :goto_4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 718
    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 720
    sget-object v0, LaEJ;->w:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaGp;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    sget-object v0, LaEJ;->x:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    .line 722
    invoke-virtual {p0}, LaGp;->e()Ljava/lang/String;

    move-result-object v1

    .line 721
    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    iget-object v0, p0, LaGp;->a:Ljava/util/Date;

    if-eqz v0, :cond_5

    .line 725
    sget-object v0, LaEJ;->m:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaGp;->a:Ljava/util/Date;

    .line 726
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 725
    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 730
    :goto_5
    iget-object v0, p0, LaGp;->a:Ljavax/crypto/SecretKey;

    if-eqz v0, :cond_6

    .line 731
    sget-object v0, LaEJ;->c:LaEJ;

    .line 732
    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaGp;->a:Ljavax/crypto/SecretKey;

    invoke-interface {v1}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v1

    .line 731
    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 733
    sget-object v0, LaEJ;->d:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LaGp;->a:Ljavax/crypto/SecretKey;

    .line 734
    invoke-interface {v1}, Ljavax/crypto/SecretKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v1

    .line 733
    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 739
    :goto_6
    return-void

    :cond_0
    move v0, v2

    .line 708
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 709
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 714
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 716
    goto/16 :goto_3

    :cond_4
    move v1, v2

    .line 718
    goto :goto_4

    .line 728
    :cond_5
    sget-object v0, LaEJ;->m:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_5

    .line 736
    :cond_6
    sget-object v0, LaEJ;->c:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 737
    sget-object v0, LaEJ;->d:LaEJ;

    invoke-virtual {v0}, LaEJ;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_6
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 545
    iput-object p1, p0, LaGp;->e:Ljava/lang/String;

    .line 546
    const/4 v0, 0x1

    iput-boolean v0, p0, LaGp;->f:Z

    .line 547
    return-void
.end method

.method public a(Ljava/util/Date;)V
    .locals 1

    .prologue
    .line 535
    iput-object p1, p0, LaGp;->a:Ljava/util/Date;

    .line 536
    const/4 v0, 0x1

    iput-boolean v0, p0, LaGp;->f:Z

    .line 537
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 429
    iget-boolean v0, p0, LaGp;->a:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    const-string v2, "Dirty temporary content?"

    invoke-static {v0, v2}, LbiT;->a(ZLjava/lang/Object;)V

    .line 431
    iput-boolean p1, p0, LaGp;->e:Z

    .line 432
    iput-boolean v1, p0, LaGp;->f:Z

    .line 433
    return-void

    .line 429
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 421
    iget-boolean v0, p0, LaGp;->a:Z

    return v0
.end method

.method public b()LadY;
    .locals 2

    .prologue
    .line 577
    iget-object v0, p0, LaGp;->a:Ljava/io/File;

    if-nez v0, :cond_0

    iget-object v0, p0, LaGp;->b:Ljava/io/File;

    move-object v1, v0

    .line 578
    :goto_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 577
    :cond_0
    iget-object v0, p0, LaGp;->a:Ljava/io/File;

    move-object v1, v0

    goto :goto_0

    .line 578
    :cond_1
    new-instance v0, LadY;

    invoke-direct {v0, v1}, LadY;-><init>(Ljava/io/File;)V

    goto :goto_1
.end method

.method public b()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, LaGp;->d:Ljava/lang/Long;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 541
    iget-object v0, p0, LaGp;->e:Ljava/lang/String;

    return-object v0
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 511
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LaGp;->b:Ljava/lang/Long;

    .line 512
    const/4 v0, 0x1

    iput-boolean v0, p0, LaGp;->f:Z

    .line 513
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 425
    iget-boolean v0, p0, LaGp;->e:Z

    return v0
.end method

.method public c()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, LaGp;->b:Ljava/lang/Long;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 553
    iget-object v0, p0, LaGp;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 622
    iget-boolean v0, p0, LaGp;->b:Z

    return v0
.end method

.method public d()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, LaGp;->c:Ljava/lang/Long;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 670
    iget-object v0, p0, LaGp;->a:Ljava/lang/String;

    return-object v0
.end method

.method public d(J)V
    .locals 1

    .prologue
    .line 519
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LaGp;->c:Ljava/lang/Long;

    .line 520
    const/4 v0, 0x1

    iput-boolean v0, p0, LaGp;->f:Z

    .line 521
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 637
    iget-boolean v0, p0, LaGp;->c:Z

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 683
    iget-object v0, p0, LaGp;->b:Ljava/lang/String;

    return-object v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 404
    invoke-super {p0}, LaGm;->e()V

    .line 405
    const/4 v0, 0x0

    iput-boolean v0, p0, LaGp;->f:Z

    .line 406
    return-void
.end method

.method public e(J)V
    .locals 3

    .prologue
    .line 596
    iget-wide v0, p0, LaGp;->a:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 597
    iput-wide p1, p0, LaGp;->a:J

    .line 599
    :cond_0
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 652
    iget-boolean v0, p0, LaGp;->d:Z

    return v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 904
    iget-object v0, p0, LaGp;->a:Ljava/io/File;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LaGp;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 879
    iget-object v0, p0, LaGp;->c:Ljava/lang/String;

    invoke-static {v0}, LaGp;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 909
    iget-object v0, p0, LaGp;->b:Ljava/io/File;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LaGp;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 841
    const-string v0, ""

    .line 842
    iget-object v1, p0, LaGp;->a:Ljavax/crypto/SecretKey;

    if-eqz v1, :cond_0

    .line 843
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ", encrypted["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaGp;->a:Ljavax/crypto/SecretKey;

    invoke-interface {v1}, Ljavax/crypto/SecretKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 845
    :cond_0
    const-string v1, "DocumentContent[type=%s, owned file path=%s, not owned file path=%s, encryptionMsg=%s, isTemporary=%s, dirty=%s, manifestSqlId=%s, serverSideLastModifiedTime=%s, md5Checksum=%s, lastModifiedTime=%s, isSnapshotted=%s, hasPendingChanges=%s, hasPendingComments=%s, documentId=%s, referencedFontFamilies=%s, sqlId=%s]"

    const/16 v2, 0x10

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 850
    invoke-virtual {p0}, LaGp;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 851
    invoke-virtual {p0}, LaGp;->a()Ljava/io/File;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 852
    invoke-virtual {p0}, LaGp;->a()LadY;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object v0, v2, v3

    const/4 v0, 0x4

    .line 854
    invoke-virtual {p0}, LaGp;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x5

    .line 855
    invoke-virtual {p0}, LaGp;->b()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x6

    .line 856
    invoke-virtual {p0}, LaGp;->b()Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x7

    .line 857
    invoke-virtual {p0}, LaGp;->a()Ljava/util/Date;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0x8

    .line 858
    invoke-virtual {p0}, LaGp;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0x9

    .line 859
    invoke-virtual {p0}, LaGp;->d()Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0xa

    .line 860
    invoke-virtual {p0}, LaGp;->c()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0xb

    .line 861
    invoke-virtual {p0}, LaGp;->d()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0xc

    .line 862
    invoke-virtual {p0}, LaGp;->e()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0xd

    .line 863
    invoke-virtual {p0}, LaGp;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0xe

    .line 864
    invoke-virtual {p0}, LaGp;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0xf

    .line 865
    invoke-virtual {p0}, LaGp;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    .line 845
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
