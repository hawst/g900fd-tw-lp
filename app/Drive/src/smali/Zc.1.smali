.class public LZc;
.super Ljava/lang/Object;
.source "PunchWebViewFragment.java"

# interfaces
.implements LGH;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V
    .locals 0

    .prologue
    .line 558
    iput-object p1, p0, LZc;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 617
    iget-object v0, p0, LZc;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->k(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LZd;

    invoke-direct {v1, p0}, LZd;-><init>(LZc;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 625
    return-void
.end method


# virtual methods
.method public a()LGI;
    .locals 2

    .prologue
    .line 582
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in getUrlInformation"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 583
    iget-object v0, p0, LZc;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()LH;

    move-result-object v0

    .line 584
    if-nez v0, :cond_0

    .line 585
    const/4 v0, 0x0

    .line 587
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LZc;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)LGI;

    move-result-object v0

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 572
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in finishActivity"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    iget-object v0, p0, LZc;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()LH;

    move-result-object v0

    .line 574
    if-nez v0, :cond_0

    .line 578
    :goto_0
    return-void

    .line 577
    :cond_0
    invoke-virtual {v0}, LH;->finish()V

    goto :goto_0
.end method

.method public a(LDL;)V
    .locals 2

    .prologue
    .line 639
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in showLoginSpinner"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 640
    iget-object v0, p0, LZc;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()LH;

    move-result-object v0

    .line 641
    if-nez v0, :cond_0

    .line 647
    :goto_0
    return-void

    .line 644
    :cond_0
    iget-object v0, p0, LZc;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)LzW;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LbiT;->b(Z)V

    .line 645
    iget-object v0, p0, LZc;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    new-instance v1, LzW;

    invoke-direct {v1, p1}, LzW;-><init>(LDL;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;LzW;)LzW;

    .line 646
    iget-object v0, p0, LZc;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)LzW;

    move-result-object v0

    invoke-virtual {v0}, LzW;->start()V

    goto :goto_0

    .line 644
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 562
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in startActivity"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    iget-object v0, p0, LZc;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()LH;

    move-result-object v0

    .line 564
    if-nez v0, :cond_0

    .line 568
    :goto_0
    return-void

    .line 567
    :cond_0
    invoke-virtual {v0, p1}, LH;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 592
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in loadUrlInWebView %s"

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 593
    iget-object v0, p0, LZc;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()LH;

    move-result-object v0

    .line 594
    if-nez v0, :cond_0

    .line 614
    :goto_0
    return-void

    .line 598
    :cond_0
    iget-object v1, p0, LZc;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    iget-object v1, v1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LaKR;

    invoke-interface {v1}, LaKR;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 599
    iget-object v0, p0, LZc;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->c(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V

    goto :goto_0

    .line 600
    :cond_1
    iget-object v1, p0, LZc;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, LakQ;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 601
    invoke-virtual {v0}, LH;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "intentExtraNamePunchWasRelaunched"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_2

    .line 605
    invoke-virtual {v0}, LH;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 607
    const-string v1, "intentExtraNamePunchWasRelaunched"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 608
    invoke-direct {p0}, LZc;->b()V

    .line 609
    invoke-virtual {p0, v0}, LZc;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 611
    :cond_2
    iget-object v0, p0, LZc;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v0, v3}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;Z)Z

    .line 612
    iget-object v0, p0, LZc;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 629
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in showError %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 630
    iget-object v0, p0, LZc;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()LH;

    move-result-object v0

    .line 631
    if-nez v0, :cond_0

    .line 635
    :goto_0
    return-void

    .line 634
    :cond_0
    iget-object v0, p0, LZc;->a:Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->c(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V

    goto :goto_0
.end method
