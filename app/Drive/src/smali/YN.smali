.class final enum LYN;
.super Ljava/lang/Enum;
.source "PunchTransitionPolicy.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LYN;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LYN;

.field private static final synthetic a:[LYN;

.field public static final enum b:LYN;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 39
    new-instance v0, LYN;

    const-string v1, "PASS_TOUCH_EVENTS_TO_WEBVIEW"

    invoke-direct {v0, v1, v2}, LYN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LYN;->a:LYN;

    new-instance v0, LYN;

    const-string v1, "INTERCEPT_EVENTS"

    invoke-direct {v0, v1, v3}, LYN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LYN;->b:LYN;

    .line 38
    const/4 v0, 0x2

    new-array v0, v0, [LYN;

    sget-object v1, LYN;->a:LYN;

    aput-object v1, v0, v2

    sget-object v1, LYN;->b:LYN;

    aput-object v1, v0, v3

    sput-object v0, LYN;->a:[LYN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LYN;
    .locals 1

    .prologue
    .line 38
    const-class v0, LYN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LYN;

    return-object v0
.end method

.method public static values()[LYN;
    .locals 1

    .prologue
    .line 38
    sget-object v0, LYN;->a:[LYN;

    invoke-virtual {v0}, [LYN;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LYN;

    return-object v0
.end method
