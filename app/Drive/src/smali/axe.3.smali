.class Laxe;
.super Ljava/lang/Object;
.source "PdfViewer.java"

# interfaces
.implements Lawf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lawf",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LawZ;


# direct methods
.method constructor <init>(LawZ;)V
    .locals 0

    .prologue
    .line 608
    iput-object p1, p0, Laxe;->a:LawZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 608
    check-cast p1, Ljava/lang/String;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Laxe;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 611
    if-eqz p2, :cond_0

    .line 612
    iget-object v0, p0, Laxe;->a:LawZ;

    invoke-static {v0}, LawZ;->a(LawZ;)Laxw;

    move-result-object v0

    invoke-virtual {v0, v3}, Laxw;->a(Lcom/google/android/apps/viewer/pdflib/Selection;)V

    .line 614
    :cond_0
    if-nez p2, :cond_1

    .line 615
    iget-object v0, p0, Laxe;->a:LawZ;

    invoke-static {v0}, LawZ;->b(LawZ;)Laxl;

    move-result-object v0

    invoke-virtual {v0}, Laxl;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 616
    iget-object v2, p0, Laxe;->a:LawZ;

    invoke-static {v2, v0}, LawZ;->a(LawZ;I)Lcom/google/android/apps/viewer/viewer/pdf/PageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->setOverlay(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 619
    :cond_1
    iget-object v0, p0, Laxe;->a:LawZ;

    invoke-static {v0}, LawZ;->b(LawZ;)Laxl;

    move-result-object v0

    invoke-virtual {v0}, Laxl;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 620
    iget-object v2, p0, Laxe;->a:LawZ;

    invoke-static {v2}, LawZ;->a(LawZ;)Laxz;

    move-result-object v2

    invoke-virtual {v2, v0, p2}, Laxz;->a(ILjava/lang/String;)V

    goto :goto_1

    .line 623
    :cond_2
    return-void
.end method
