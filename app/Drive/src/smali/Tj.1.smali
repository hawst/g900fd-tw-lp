.class public LTj;
.super Ljava/lang/Object;
.source "DefaultAuthenticatedHttpIssuer.java"

# interfaces
.implements LTh;


# annotations
.annotation runtime Lbxz;
.end annotation


# static fields
.field private static final a:LbmL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmL",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LbmL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmL",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:LbmL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmL",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LTF;

.field private final a:LTd;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 57
    invoke-static {}, LbmL;->a()LbmM;

    move-result-object v0

    const-string v1, "spreadsheets.google.com"

    const-string v2, "wise"

    .line 58
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    const-string v1, "docs.google.com"

    const-string v2, "writely"

    .line 59
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    const-string v1, "jmt0.google.com"

    const-string v2, "wise"

    .line 61
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    const-string v1, "was.sandbox.google.com"

    const-string v2, "wise"

    .line 62
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    const-string v1, "googledrive.com"

    const-string v2, "oauth2:https://www.googleapis.com/auth/drive"

    .line 63
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    const-string v1, "drive.google.com"

    const-string v2, "wise"

    .line 65
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    const-string v1, "drive.sandbox.google.com"

    const-string v2, "wise"

    .line 66
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, LbmM;->a()LbmL;

    move-result-object v0

    sput-object v0, LTj;->a:LbmL;

    .line 73
    invoke-static {}, LbmL;->a()LbmM;

    move-result-object v0

    const-string v1, "oauth2:https://www.googleapis.com/auth/drive"

    const-string v2, "OAuth %s"

    .line 74
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    invoke-virtual {v0}, LbmM;->a()LbmL;

    move-result-object v0

    sput-object v0, LTj;->b:LbmL;

    .line 81
    invoke-static {}, LbmL;->a()LbmM;

    move-result-object v0

    const-string v1, "docs.googleusercontent.com"

    const-string v2, "writely"

    .line 82
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    invoke-virtual {v0}, LbmM;->a()LbmL;

    move-result-object v0

    sput-object v0, LTj;->c:LbmL;

    .line 80
    return-void
.end method

.method public constructor <init>(LTd;LTF;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object p1, p0, LTj;->a:LTd;

    .line 96
    iput-object p2, p0, LTj;->a:LTF;

    .line 97
    return-void
.end method

.method private a(LaFO;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 212
    if-eqz p2, :cond_1

    if-eqz p1, :cond_1

    .line 213
    iget-object v0, p0, LTj;->a:LTd;

    invoke-interface {v0, p1, p2}, LTd;->a(LaFO;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 214
    if-eqz v1, :cond_1

    .line 215
    sget-object v0, LTj;->b:LbmL;

    invoke-virtual {v0, p2}, LbmL;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 216
    if-nez v0, :cond_0

    .line 217
    const-string v0, "GoogleLogin auth=%s"

    .line 219
    :cond_0
    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v2, v0, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 223
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 114
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    .line 116
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LFO;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 123
    const-string v1, "docs.google.com"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "/viewer"

    .line 124
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 126
    const-string v0, "wise"

    .line 141
    :cond_0
    :goto_0
    return-object v0

    .line 129
    :cond_1
    sget-object v0, LTj;->a:LbmL;

    invoke-virtual {v0, v2}, LbmL;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 130
    if-nez v0, :cond_0

    .line 135
    sget-object v1, LTj;->c:LbmL;

    invoke-virtual {v1}, LbmL;->c()LbmY;

    move-result-object v1

    invoke-virtual {v1}, LbmY;->a()Lbqv;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 136
    invoke-virtual {v2, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 137
    sget-object v0, LTj;->c:LbmL;

    invoke-virtual {v0, v1}, LbmL;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method private a(Lorg/apache/http/client/methods/HttpUriRequest;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    .line 110
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, LTj;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(LaFO;Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;)Lorg/apache/http/HttpResponse;
    .locals 2

    .prologue
    .line 228
    if-eqz p3, :cond_0

    if-eqz p1, :cond_0

    .line 229
    invoke-direct {p0, p1, p3}, LTj;->a(LaFO;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 230
    if-eqz v0, :cond_0

    .line 231
    const-string v1, "Authorization"

    invoke-interface {p2, v1, v0}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    :cond_0
    iget-object v0, p0, LTj;->a:LTF;

    invoke-interface {v0, p2}, LTF;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Ljava/io/Closeable;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, LTj;->a:LTF;

    invoke-interface {v0}, LTF;->a()Ljava/io/Closeable;

    move-result-object v0

    return-object v0
.end method

.method public a(Lorg/apache/http/HttpEntity;)Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, LTj;->a:LTF;

    invoke-interface {v0, p1}, LTF;->a(Lorg/apache/http/HttpEntity;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public a(LaFO;Landroid/net/Uri;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFO;",
            "Landroid/net/Uri;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 240
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 241
    const-string v1, "Authorization"

    .line 242
    invoke-direct {p0, p2}, LTj;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v2}, LTj;->a(LaFO;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 241
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    return-object v0
.end method

.method public final a(LaFO;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0, p2}, LTj;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Ljava/lang/String;

    move-result-object v0

    .line 104
    invoke-virtual {p0, p1, p2, v0}, LTj;->a(LaFO;Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(LaFO;Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;)Lorg/apache/http/HttpResponse;
    .locals 3

    .prologue
    .line 169
    iget-object v0, p0, LTj;->a:LTd;

    invoke-interface {v0}, LTd;->a()V

    .line 171
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, LTj;->b(LaFO;Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 173
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    .line 174
    if-eqz p3, :cond_0

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    const/16 v2, 0x191

    if-ne v1, v2, :cond_0

    .line 177
    iget-object v0, p0, LTj;->a:LTF;

    invoke-interface {v0}, LTF;->a()V

    .line 178
    iget-object v0, p0, LTj;->a:LTF;

    invoke-interface {v0}, LTF;->b()V

    .line 179
    iget-object v0, p0, LTj;->a:LTd;

    invoke-interface {v0, p1, p3}, LTd;->a(LaFO;Ljava/lang/String;)V

    .line 180
    invoke-direct {p0, p1, p2, p3}, LTj;->b(LaFO;Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 184
    :cond_0
    iget-object v1, p0, LTj;->a:LTd;

    invoke-interface {v1}, LTd;->b()V

    return-object v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LTj;->a:LTd;

    invoke-interface {v1}, LTd;->b()V

    throw v0
.end method

.method public a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, LTj;->a:LTF;

    invoke-interface {v0, p1}, LTF;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, LTj;->a:LTF;

    invoke-interface {v0}, LTF;->a()V

    .line 147
    return-void
.end method

.method public a(Lorg/apache/http/HttpRequest;)V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, LTj;->a:LTF;

    invoke-interface {v0, p1}, LTF;->a(Lorg/apache/http/HttpRequest;)V

    .line 196
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, LTj;->a:LTF;

    invoke-interface {v0}, LTF;->b()V

    .line 152
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, LTj;->a:LTF;

    invoke-interface {v0}, LTF;->c()V

    .line 162
    return-void
.end method
