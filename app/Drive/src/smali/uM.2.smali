.class public LuM;
.super Ljava/lang/Object;
.source "NativeAppInfo.java"


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LuM;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:LuM;

.field public static final b:LuM;

.field public static final c:LuM;

.field public static final d:LuM;

.field public static final e:LuM;


# instance fields
.field private final a:I

.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 19
    new-instance v0, LuM;

    const-string v1, "com.google.android.apps.docs"

    sget v2, Lxi;->app_installed_dialog_drive_installed_title:I

    const-string v3, "com.google.android.apps.docs.drive.tophat"

    invoke-direct {v0, v1, v2, v3}, LuM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LuM;->a:LuM;

    .line 23
    new-instance v0, LuM;

    const-string v1, "com.google.android.apps.docs.editors.docs"

    sget v2, Lxi;->app_installed_dialog_kix_editor_installed_title:I

    invoke-direct {v0, v1, v2, v4}, LuM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LuM;->b:LuM;

    .line 27
    new-instance v0, LuM;

    const-string v1, "com.google.android.apps.docs.editors.sheets"

    sget v2, Lxi;->app_installed_dialog_trix_editor_installed_title:I

    invoke-direct {v0, v1, v2, v4}, LuM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LuM;->c:LuM;

    .line 31
    new-instance v0, LuM;

    const-string v1, "com.google.android.apps.docs.editors.slides"

    sget v2, Lxi;->app_installed_dialog_punch_editor_installed_title:I

    invoke-direct {v0, v1, v2, v4}, LuM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LuM;->d:LuM;

    .line 35
    new-instance v0, LuM;

    const-string v1, "com.google.android.apps.docs.editors.drawings"

    sget v2, Lxi;->app_installed_dialog_sketchy_editor_installed_title:I

    invoke-direct {v0, v1, v2, v4}, LuM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LuM;->e:LuM;

    .line 40
    invoke-static {}, LuM;->a()Ljava/util/Map;

    move-result-object v0

    sput-object v0, LuM;->a:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, LuM;->a:Ljava/lang/String;

    .line 71
    iput p2, p0, LuM;->a:I

    .line 72
    iput-object p3, p0, LuM;->b:Ljava/lang/String;

    .line 73
    return-void
.end method

.method private static a()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LuM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    new-instance v1, LbmM;

    invoke-direct {v1}, LbmM;-><init>()V

    .line 44
    sget-object v0, LaGv;->b:LaGv;

    invoke-virtual {v0}, LaGv;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v2, LuM;->b:LuM;

    invoke-virtual {v1, v0, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    .line 45
    sget-object v0, LaGn;->d:LaGn;

    invoke-virtual {v0}, LaGn;->a()LbmY;

    move-result-object v0

    invoke-virtual {v0}, LbmY;->a()Lbqv;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 46
    sget-object v3, LuM;->b:LuM;

    invoke-virtual {v1, v0, v3}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    goto :goto_0

    .line 48
    :cond_0
    sget-object v0, LaGv;->i:LaGv;

    invoke-virtual {v0}, LaGv;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v2, LuM;->c:LuM;

    invoke-virtual {v1, v0, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    .line 49
    sget-object v0, LaGn;->e:LaGn;

    invoke-virtual {v0}, LaGn;->a()LbmY;

    move-result-object v0

    invoke-virtual {v0}, LbmY;->a()Lbqv;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 50
    sget-object v3, LuM;->c:LuM;

    invoke-virtual {v1, v0, v3}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    goto :goto_1

    .line 52
    :cond_1
    sget-object v0, LaGv;->g:LaGv;

    invoke-virtual {v0}, LaGv;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v2, LuM;->d:LuM;

    invoke-virtual {v1, v0, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    .line 53
    sget-object v0, LaGn;->f:LaGn;

    invoke-virtual {v0}, LaGn;->a()LbmY;

    move-result-object v0

    invoke-virtual {v0}, LbmY;->a()Lbqv;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 54
    sget-object v3, LuM;->d:LuM;

    invoke-virtual {v1, v0, v3}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    goto :goto_2

    .line 56
    :cond_2
    sget-object v0, LaGv;->c:LaGv;

    invoke-virtual {v0}, LaGv;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v2, LuM;->e:LuM;

    invoke-virtual {v1, v0, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    .line 57
    invoke-virtual {v1}, LbmM;->a()LbmL;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, LuM;->a:I

    return v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, LuM;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 82
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 83
    iget-object v1, p0, LuM;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LuM;->b:Ljava/lang/String;

    .line 84
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 95
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 97
    :try_start_0
    iget-object v2, p0, LuM;->a:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    const/4 v0, 0x1

    .line 100
    :goto_0
    return v0

    .line 99
    :catch_0
    move-exception v1

    goto :goto_0
.end method
