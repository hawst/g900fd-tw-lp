.class LqG;
.super Ljava/lang/Object;
.source "RocketEventTracker.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:LaFO;

.field final synthetic a:Lorg/apache/http/client/methods/HttpUriRequest;

.field final synthetic a:LqF;


# direct methods
.method constructor <init>(LqF;LaFO;Lorg/apache/http/client/methods/HttpUriRequest;)V
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, LqG;->a:LqF;

    iput-object p2, p0, LqG;->a:LaFO;

    iput-object p3, p0, LqG;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 270
    :try_start_0
    iget-object v0, p0, LqG;->a:LaFO;

    if-nez v0, :cond_1

    iget-object v0, p0, LqG;->a:LqF;

    invoke-static {v0}, LqF;->a(LqF;)LTh;

    move-result-object v0

    iget-object v1, p0, LqG;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0, v1}, LTh;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 272
    :goto_0
    const-string v1, "RocketEventTracker"

    const-string v2, "Submitting request for account %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LqG;->a:LaFO;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 273
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    .line 274
    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    const/16 v2, 0xcc

    if-eq v1, v2, :cond_0

    .line 275
    const-string v1, "RocketEventTracker"

    const-string v2, "Unexpected server response on Rocket event: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch LTr; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 286
    :cond_0
    iget-object v0, p0, LqG;->a:LqF;

    invoke-static {v0}, LqF;->a(LqF;)LTh;

    move-result-object v0

    invoke-interface {v0}, LTh;->b()V

    .line 288
    :goto_1
    return-void

    .line 270
    :cond_1
    :try_start_1
    iget-object v0, p0, LqG;->a:LqF;

    .line 271
    invoke-static {v0}, LqF;->a(LqF;)LTh;

    move-result-object v0

    iget-object v1, p0, LqG;->a:LaFO;

    iget-object v2, p0, LqG;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0, v1, v2}, LTh;->a(LaFO;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch LTr; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 277
    :catch_0
    move-exception v0

    .line 278
    :try_start_2
    const-string v1, "RocketEventTracker"

    const-string v2, "HttpIssuer client protocol error when logging Rocket event"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 286
    iget-object v0, p0, LqG;->a:LqF;

    invoke-static {v0}, LqF;->a(LqF;)LTh;

    move-result-object v0

    invoke-interface {v0}, LTh;->b()V

    goto :goto_1

    .line 279
    :catch_1
    move-exception v0

    .line 280
    :try_start_3
    const-string v1, "RocketEventTracker"

    const-string v2, "HttpIssuer IO error when logging Rocket event"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 286
    iget-object v0, p0, LqG;->a:LqF;

    invoke-static {v0}, LqF;->a(LqF;)LTh;

    move-result-object v0

    invoke-interface {v0}, LTh;->b()V

    goto :goto_1

    .line 281
    :catch_2
    move-exception v0

    .line 282
    :try_start_4
    const-string v1, "RocketEventTracker"

    const-string v2, "HttpIssuer credentials error when logging Rocket event"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 286
    iget-object v0, p0, LqG;->a:LqF;

    invoke-static {v0}, LqF;->a(LqF;)LTh;

    move-result-object v0

    invoke-interface {v0}, LTh;->b()V

    goto :goto_1

    .line 283
    :catch_3
    move-exception v0

    .line 284
    :try_start_5
    const-string v1, "RocketEventTracker"

    const-string v2, "HttpIssuer authentication error when logging Rocket event"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 286
    iget-object v0, p0, LqG;->a:LqF;

    invoke-static {v0}, LqF;->a(LqF;)LTh;

    move-result-object v0

    invoke-interface {v0}, LTh;->b()V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, LqG;->a:LqF;

    invoke-static {v1}, LqF;->a(LqF;)LTh;

    move-result-object v1

    invoke-interface {v1}, LTh;->b()V

    throw v0
.end method
