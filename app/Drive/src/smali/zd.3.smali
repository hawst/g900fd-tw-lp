.class public Lzd;
.super Ljava/lang/Object;
.source "InformationCardUpdater.java"


# instance fields
.field private final a:LaGR;

.field private final a:Landroid/content/Context;

.field private final a:Lzc;


# direct methods
.method constructor <init>(Landroid/content/Context;Lzc;LaGR;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lzd;->a:Landroid/content/Context;

    .line 39
    iput-object p2, p0, Lzd;->a:Lzc;

    .line 40
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGR;

    iput-object v0, p0, Lzd;->a:LaGR;

    .line 41
    return-void
.end method

.method private a(LaGu;)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 80
    const/4 v0, 0x0

    .line 81
    instance-of v1, p1, LaGo;

    if-eqz v1, :cond_0

    .line 82
    check-cast p1, LaGo;

    invoke-interface {p1}, LaGo;->a()Ljava/lang/Long;

    move-result-object v0

    .line 84
    :cond_0
    return-object v0
.end method

.method static synthetic a(Lzd;LaGu;)Ljava/lang/Long;
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lzd;->a(LaGu;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method private a(LaGM;LaGu;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 95
    invoke-interface {p2}, LaGu;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Lzd;->a:Landroid/content/Context;

    sget v1, Lxi;->menu_show_trash:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 108
    :cond_0
    :goto_0
    return-object v1

    .line 99
    :cond_1
    invoke-interface {p2}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-interface {p1, v0}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LbmY;

    move-result-object v0

    .line 100
    invoke-static {v0, v1}, Lbnm;->a(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 101
    if-nez v0, :cond_2

    .line 102
    invoke-interface {p2}, LaGu;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 103
    iget-object v0, p0, Lzd;->a:Landroid/content/Context;

    sget v1, Lxi;->menu_incoming:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 107
    :cond_2
    invoke-interface {p1, v0}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGu;

    move-result-object v0

    .line 108
    if-nez v0, :cond_3

    move-object v0, v1

    :goto_1
    move-object v1, v0

    goto :goto_0

    :cond_3
    invoke-interface {v0}, LaGu;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method static synthetic a(Lzd;LaGM;LaGu;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lzd;->a(LaGM;LaGu;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lzd;)Lzc;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lzd;->a:Lzc;

    return-object v0
.end method


# virtual methods
.method public a(LaGu;Z)V
    .locals 2

    .prologue
    .line 50
    if-eqz p1, :cond_0

    .line 51
    new-instance v0, Lze;

    invoke-direct {v0, p0, p1}, Lze;-><init>(Lzd;LaGu;)V

    .line 65
    if-eqz p2, :cond_1

    .line 66
    iget-object v1, p0, Lzd;->a:LaGR;

    invoke-virtual {v1, v0}, LaGR;->a(LaGN;)V

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    iget-object v1, p0, Lzd;->a:LaGR;

    invoke-virtual {v1, v0}, LaGR;->b(LaGN;)V

    goto :goto_0
.end method
