.class public LPD;
.super Ljava/lang/Object;
.source "ActivityHolder.java"


# instance fields
.field private final a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Lcom/google/api/services/appsactivity/model/Target;",
            ">;"
        }
    .end annotation
.end field

.field final a:Lcom/google/api/services/appsactivity/model/Activity;

.field private final b:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Lcom/google/api/services/appsactivity/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/api/services/appsactivity/model/Activity;)V
    .locals 5

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, LPD;->a:Lcom/google/api/services/appsactivity/model/Activity;

    .line 30
    invoke-static {}, LbmY;->a()Lbna;

    move-result-object v1

    .line 31
    invoke-static {}, LbmY;->a()Lbna;

    move-result-object v2

    .line 32
    invoke-virtual {p1}, Lcom/google/api/services/appsactivity/model/Activity;->getSingleEvents()Ljava/util/List;

    move-result-object v0

    .line 33
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/Event;

    .line 34
    invoke-virtual {v0}, Lcom/google/api/services/appsactivity/model/Event;->getTarget()Lcom/google/api/services/appsactivity/model/Target;

    move-result-object v4

    .line 35
    invoke-virtual {v0}, Lcom/google/api/services/appsactivity/model/Event;->getUser()Lcom/google/api/services/appsactivity/model/User;

    move-result-object v0

    .line 36
    if-eqz v4, :cond_1

    .line 37
    invoke-virtual {v1, v4}, Lbna;->a(Ljava/lang/Object;)Lbna;

    .line 39
    :cond_1
    if-eqz v0, :cond_0

    .line 40
    invoke-virtual {v2, v0}, Lbna;->a(Ljava/lang/Object;)Lbna;

    goto :goto_0

    .line 43
    :cond_2
    invoke-virtual {v1}, Lbna;->a()LbmY;

    move-result-object v0

    iput-object v0, p0, LPD;->a:LbmY;

    .line 44
    invoke-virtual {v2}, Lbna;->a()LbmY;

    move-result-object v0

    iput-object v0, p0, LPD;->b:LbmY;

    .line 45
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, LPD;->a:LbmY;

    invoke-virtual {v0}, LbmY;->size()I

    move-result v0

    return v0
.end method

.method public a()LPL;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, LPD;->a:Lcom/google/api/services/appsactivity/model/Activity;

    invoke-virtual {v0}, Lcom/google/api/services/appsactivity/model/Activity;->getCombinedEvent()Lcom/google/api/services/appsactivity/model/Event;

    move-result-object v0

    .line 71
    if-nez v0, :cond_0

    .line 72
    const/4 v0, 0x0

    .line 76
    :goto_0
    return-object v0

    .line 75
    :cond_0
    invoke-virtual {v0}, Lcom/google/api/services/appsactivity/model/Event;->getPrimaryEventType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LPL;->a(Ljava/lang/String;)LPL;

    move-result-object v0

    goto :goto_0
.end method

.method public a()LbmF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmF",
            "<",
            "Lcom/google/api/services/appsactivity/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, LPD;->b:LbmY;

    invoke-virtual {v0}, LbmY;->a()LbmF;

    move-result-object v0

    return-object v0
.end method

.method public a()Lcom/google/api/services/appsactivity/model/Event;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, LPD;->a:Lcom/google/api/services/appsactivity/model/Activity;

    invoke-virtual {v0}, Lcom/google/api/services/appsactivity/model/Activity;->getCombinedEvent()Lcom/google/api/services/appsactivity/model/Event;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lcom/google/api/services/appsactivity/model/Target;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, LPD;->a:LbmY;

    invoke-virtual {v0}, LbmY;->a()LbmF;

    move-result-object v0

    invoke-virtual {v0, p1}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/Target;

    return-object v0
.end method

.method public a()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 80
    iget-object v1, p0, LPD;->a:Lcom/google/api/services/appsactivity/model/Activity;

    invoke-virtual {v1}, Lcom/google/api/services/appsactivity/model/Activity;->getCombinedEvent()Lcom/google/api/services/appsactivity/model/Event;

    move-result-object v1

    .line 81
    if-nez v1, :cond_1

    .line 94
    :cond_0
    :goto_0
    return v0

    .line 85
    :cond_1
    invoke-virtual {v1}, Lcom/google/api/services/appsactivity/model/Event;->getPrimaryEventType()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LPL;->a(Ljava/lang/String;)LPL;

    move-result-object v1

    .line 86
    if-eqz v1, :cond_0

    .line 90
    iget-object v1, p0, LPD;->b:LbmY;

    invoke-virtual {v1}, LbmY;->size()I

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, LPD;->b:LbmY;

    invoke-virtual {v0}, LbmY;->size()I

    move-result v0

    return v0
.end method
