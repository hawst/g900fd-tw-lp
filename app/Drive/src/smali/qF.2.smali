.class public LqF;
.super Ljava/lang/Object;
.source "RocketEventTracker.java"


# annotations
.annotation runtime Lbxz;
.end annotation


# static fields
.field private static final a:I

.field private static final a:LbmL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmL",
            "<",
            "LaGv;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LQr;

.field private final a:LTh;

.field private final a:Ljava/util/concurrent/ExecutorService;

.field private final b:LbmL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmL",
            "<",
            "Ljava/lang/String;",
            "LqH;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LbmL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmL",
            "<",
            "Ljava/lang/String;",
            "LqH;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 171
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x18

    sget-object v1, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    .line 172
    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, LqF;->a:I

    .line 178
    invoke-static {}, LbmL;->a()LbmM;

    move-result-object v0

    sget-object v1, LaGv;->b:LaGv;

    const-string v2, "0"

    .line 179
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, LaGv;->i:LaGv;

    const-string v2, "1"

    .line 180
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, LaGv;->g:LaGv;

    const-string v2, "3"

    .line 182
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, LaGv;->c:LaGv;

    const-string v2, "4"

    .line 183
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    .line 184
    invoke-virtual {v0}, LbmM;->a()LbmL;

    move-result-object v0

    sput-object v0, LqF;->a:LbmL;

    .line 185
    return-void
.end method

.method public constructor <init>(LTh;LQr;)V
    .locals 1

    .prologue
    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 187
    invoke-static {}, LqF;->b()LbmL;

    move-result-object v0

    iput-object v0, p0, LqF;->b:LbmL;

    .line 197
    invoke-static {}, LqF;->c()LbmL;

    move-result-object v0

    iput-object v0, p0, LqF;->c:LbmL;

    .line 209
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, LqF;->a:Ljava/util/concurrent/ExecutorService;

    .line 213
    iput-object p1, p0, LqF;->a:LTh;

    .line 214
    iput-object p2, p0, LqF;->a:LQr;

    .line 215
    return-void
.end method

.method static synthetic a(LqF;)LTh;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, LqF;->a:LTh;

    return-object v0
.end method

.method static synthetic a()LbmL;
    .locals 1

    .prologue
    .line 40
    sget-object v0, LqF;->a:LbmL;

    return-object v0
.end method

.method private a(Lorg/apache/http/client/methods/HttpUriRequest;LaFO;)V
    .locals 2

    .prologue
    .line 263
    if-nez p1, :cond_0

    .line 290
    :goto_0
    return-void

    .line 266
    :cond_0
    iget-object v0, p0, LqF;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, LqG;

    invoke-direct {v1, p0, p2, p1}, LqG;-><init>(LqF;LaFO;Lorg/apache/http/client/methods/HttpUriRequest;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method private static b()LbmL;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmL",
            "<",
            "Ljava/lang/String;",
            "LqH;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189
    new-instance v0, LbmM;

    invoke-direct {v0}, LbmM;-><init>()V

    sget-object v1, LuM;->d:LuM;

    .line 190
    invoke-virtual {v1}, LuM;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LqH;->e:LqH;

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, LuM;->c:LuM;

    .line 191
    invoke-virtual {v1}, LuM;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LqH;->a:LqH;

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, LuM;->b:LuM;

    .line 192
    invoke-virtual {v1}, LuM;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LqH;->c:LqH;

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, LuM;->a:LuM;

    .line 193
    invoke-virtual {v1}, LuM;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LqH;->g:LqH;

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    .line 194
    invoke-virtual {v0}, LbmM;->a()LbmL;

    move-result-object v0

    return-object v0
.end method

.method private b(LqI;)V
    .locals 6

    .prologue
    .line 298
    iget-object v0, p0, LqF;->a:LQr;

    const-string v1, "rocketEventTrackerServerUrl"

    const-string v2, "https://docs.google.com/feeds/metadata/default?nocontent=true"

    .line 299
    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 300
    iget-object v1, p0, LqF;->a:LQr;

    const-string v2, "rocketEventTrackerServerQueryKey"

    const-string v3, "a"

    invoke-interface {v1, v2, v3}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 302
    iget-object v2, p0, LqF;->a:LQr;

    const-string v3, "rocketEventTrackerServerDocumentTypeKey"

    const-string v4, "t"

    invoke-interface {v2, v3, v4}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 304
    iget-object v3, p0, LqF;->a:LQr;

    const-string v4, "rocketEventTrackerServerSourceKey"

    const-string v5, "s"

    invoke-interface {v3, v4, v5}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 306
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 307
    invoke-static {p1}, LqI;->b(LqI;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 308
    invoke-static {p1}, LqI;->c(LqI;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 309
    invoke-static {p1}, LqI;->c(LqI;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 311
    :cond_0
    invoke-static {p1}, LqI;->d(LqI;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 312
    invoke-static {p1}, LqI;->d(LqI;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 314
    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 316
    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    new-instance v2, Ljava/net/URI;

    invoke-direct {v2, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    invoke-static {p1}, LqI;->a(LqI;)LaFO;

    move-result-object v0

    invoke-direct {p0, v1, v0}, LqF;->a(Lorg/apache/http/client/methods/HttpUriRequest;LaFO;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 320
    :goto_0
    return-void

    .line 317
    :catch_0
    move-exception v0

    .line 318
    const-string v1, "RocketEventTracker"

    const-string v2, "createRequest URI error"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method private static c()LbmL;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmL",
            "<",
            "Ljava/lang/String;",
            "LqH;",
            ">;"
        }
    .end annotation

    .prologue
    .line 199
    new-instance v0, LbmM;

    invoke-direct {v0}, LbmM;-><init>()V

    sget-object v1, LuM;->d:LuM;

    .line 200
    invoke-virtual {v1}, LuM;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LqH;->f:LqH;

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, LuM;->c:LuM;

    .line 201
    invoke-virtual {v1}, LuM;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LqH;->b:LqH;

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, LuM;->b:LuM;

    .line 202
    invoke-virtual {v1}, LuM;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LqH;->d:LqH;

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, LuM;->a:LuM;

    .line 203
    invoke-virtual {v1}, LuM;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LqH;->h:LqH;

    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    .line 204
    invoke-virtual {v0}, LbmM;->a()LbmL;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 221
    iget-object v0, p0, LqF;->a:LQr;

    const-string v1, "homescreenEventRateLimit"

    sget v2, LqF;->a:I

    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;)LqH;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, LqF;->b:LbmL;

    invoke-virtual {v0, p1}, LbmL;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqH;

    return-object v0
.end method

.method public a(LqI;)V
    .locals 5

    .prologue
    .line 256
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    const-string v0, "RocketEventTracker"

    const-string v1, "registerEvent: (%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, LqI;->a(LqI;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 258
    invoke-direct {p0, p1}, LqF;->b(LqI;)V

    .line 259
    return-void
.end method

.method public b(Ljava/lang/String;)LqH;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, LqF;->c:LbmL;

    invoke-virtual {v0, p1}, LbmL;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqH;

    return-object v0
.end method
