.class public LaeJ;
.super Ljava/lang/Object;
.source "AclGDataParserFactory.java"

# interfaces
.implements LbwT;


# instance fields
.field private final a:Lbxr;

.field private final a:LtK;


# direct methods
.method public constructor <init>(Lbxr;LtK;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, LaeJ;->a:Lbxr;

    .line 38
    iput-object p2, p0, LaeJ;->a:LtK;

    .line 39
    return-void
.end method


# virtual methods
.method public a(Lbxb;)LaeL;
    .locals 3

    .prologue
    .line 64
    instance-of v0, p1, LaeB;

    if-eqz v0, :cond_0

    .line 65
    check-cast p1, LaeB;

    .line 66
    new-instance v0, LaeR;

    iget-object v1, p0, LaeJ;->a:Lbxr;

    invoke-direct {v0, v1, p1}, LaeR;-><init>(Lbxr;LaeB;)V

    return-object v0

    .line 69
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected Entry class: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Ljava/io/InputStream;)Lbxj;
    .locals 3

    .prologue
    .line 45
    :try_start_0
    iget-object v0, p0, LaeJ;->a:Lbxr;

    invoke-interface {v0}, Lbxr;->a()Lorg/xmlpull/v1/XmlPullParser;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 49
    new-instance v1, LaeI;

    iget-object v2, p0, LaeJ;->a:LtK;

    invoke-direct {v1, p1, v0, v2}, LaeI;-><init>(Ljava/io/InputStream;Lorg/xmlpull/v1/XmlPullParser;LtK;)V

    return-object v1

    .line 46
    :catch_0
    move-exception v0

    .line 47
    new-instance v1, Lbxk;

    const-string v2, "Could not create XmlPullParser"

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(Ljava/lang/Class;Ljava/io/InputStream;)Lbxj;
    .locals 3

    .prologue
    .line 55
    const-class v0, LaeB;

    if-ne p1, v0, :cond_0

    .line 56
    invoke-virtual {p0, p2}, LaeJ;->a(Ljava/io/InputStream;)Lbxj;

    move-result-object v0

    return-object v0

    .line 58
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown entry class \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 59
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' specified."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic a(Lbxb;)Lbxs;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1}, LaeJ;->a(Lbxb;)LaeL;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/Enumeration;)Lbxs;
    .locals 2

    .prologue
    .line 75
    new-instance v0, LaeO;

    iget-object v1, p0, LaeJ;->a:Lbxr;

    invoke-direct {v0, p0, v1, p1}, LaeO;-><init>(LbwT;Lbxr;Ljava/util/Enumeration;)V

    return-object v0
.end method
