.class public final enum LQl;
.super Ljava/lang/Enum;
.source "PickEntryDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LQl;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LQl;

.field private static final a:[LQl;

.field public static final enum b:LQl;

.field private static final b:[LQl;

.field public static final enum c:LQl;

.field private static final synthetic c:[LQl;

.field public static final enum d:LQl;


# instance fields
.field private final a:I

.field private final a:LCl;

.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 117
    new-instance v0, LQl;

    const-string v1, "MY_DRIVE"

    const-string v3, "myDrive"

    sget-object v4, LCe;->q:LCe;

    sget v5, Lxb;->ic_drive_my_drive:I

    invoke-direct/range {v0 .. v5}, LQl;-><init>(Ljava/lang/String;ILjava/lang/String;LCl;I)V

    sput-object v0, LQl;->a:LQl;

    new-instance v3, LQl;

    const-string v4, "SHARED_WITH_ME"

    const-string v6, "sharedWithMe"

    sget-object v7, LCe;->o:LCe;

    sget v8, Lxb;->ic_drive_shared_with_me:I

    move v5, v9

    invoke-direct/range {v3 .. v8}, LQl;-><init>(Ljava/lang/String;ILjava/lang/String;LCl;I)V

    sput-object v3, LQl;->b:LQl;

    .line 119
    new-instance v3, LQl;

    const-string v4, "STARRED"

    const-string v6, "starred"

    sget-object v7, LCe;->b:LCe;

    sget v8, Lxb;->ic_drive_starred:I

    move v5, v10

    invoke-direct/range {v3 .. v8}, LQl;-><init>(Ljava/lang/String;ILjava/lang/String;LCl;I)V

    sput-object v3, LQl;->c:LQl;

    .line 120
    new-instance v3, LQl;

    const-string v4, "RECENT"

    const-string v6, "recent"

    sget-object v7, LCe;->p:LCe;

    sget v8, Lxb;->ic_drive_recently_opened:I

    move v5, v11

    invoke-direct/range {v3 .. v8}, LQl;-><init>(Ljava/lang/String;ILjava/lang/String;LCl;I)V

    sput-object v3, LQl;->d:LQl;

    .line 116
    const/4 v0, 0x4

    new-array v0, v0, [LQl;

    sget-object v1, LQl;->a:LQl;

    aput-object v1, v0, v2

    sget-object v1, LQl;->b:LQl;

    aput-object v1, v0, v9

    sget-object v1, LQl;->c:LQl;

    aput-object v1, v0, v10

    sget-object v1, LQl;->d:LQl;

    aput-object v1, v0, v11

    sput-object v0, LQl;->c:[LQl;

    .line 122
    invoke-static {}, LQl;->values()[LQl;

    move-result-object v0

    sput-object v0, LQl;->a:[LQl;

    .line 123
    new-array v0, v11, [LQl;

    sget-object v1, LQl;->a:LQl;

    aput-object v1, v0, v2

    sget-object v1, LQl;->b:LQl;

    aput-object v1, v0, v9

    sget-object v1, LQl;->c:LQl;

    aput-object v1, v0, v10

    sput-object v0, LQl;->b:[LQl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;LCl;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LCl;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 130
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 131
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LQl;->a:Ljava/lang/String;

    .line 132
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCl;

    iput-object v0, p0, LQl;->a:LCl;

    .line 133
    iput p5, p0, LQl;->a:I

    .line 134
    return-void
.end method

.method static synthetic a(LQl;)I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, LQl;->a:I

    return v0
.end method

.method public static synthetic a(LQl;)LCl;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, LQl;->a:LCl;

    return-object v0
.end method

.method public static synthetic a(Ljava/lang/String;)LQl;
    .locals 1

    .prologue
    .line 116
    invoke-static {p0}, LQl;->b(Ljava/lang/String;)LQl;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(LQl;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, LQl;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic a()[LQl;
    .locals 1

    .prologue
    .line 116
    sget-object v0, LQl;->b:[LQl;

    return-object v0
.end method

.method private static b(Ljava/lang/String;)LQl;
    .locals 5

    .prologue
    .line 137
    invoke-static {}, LQl;->values()[LQl;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 138
    iget-object v4, v3, LQl;->a:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 139
    return-object v3

    .line 137
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 143
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid TopCollection name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static synthetic b()[LQl;
    .locals 1

    .prologue
    .line 116
    sget-object v0, LQl;->a:[LQl;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LQl;
    .locals 1

    .prologue
    .line 116
    const-class v0, LQl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LQl;

    return-object v0
.end method

.method public static values()[LQl;
    .locals 1

    .prologue
    .line 116
    sget-object v0, LQl;->c:[LQl;

    invoke-virtual {v0}, [LQl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LQl;

    return-object v0
.end method
