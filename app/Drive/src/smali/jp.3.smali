.class Ljp;
.super Landroid/widget/BaseAdapter;
.source "MenuPopupHelper.java"


# instance fields
.field private a:I

.field private a:Ljb;

.field final synthetic a:Ljo;


# direct methods
.method public constructor <init>(Ljo;Ljb;)V
    .locals 1

    .prologue
    .line 332
    iput-object p1, p0, Ljp;->a:Ljo;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 330
    const/4 v0, -0x1

    iput v0, p0, Ljp;->a:I

    .line 333
    iput-object p2, p0, Ljp;->a:Ljb;

    .line 334
    invoke-virtual {p0}, Ljp;->a()V

    .line 335
    return-void
.end method

.method static synthetic a(Ljp;)Ljb;
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Ljp;->a:Ljb;

    return-object v0
.end method


# virtual methods
.method public a(I)Ljf;
    .locals 2

    .prologue
    .line 347
    iget-object v0, p0, Ljp;->a:Ljo;

    invoke-static {v0}, Ljo;->a(Ljo;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljp;->a:Ljb;

    invoke-virtual {v0}, Ljb;->c()Ljava/util/ArrayList;

    move-result-object v0

    .line 349
    :goto_0
    iget v1, p0, Ljp;->a:I

    if-ltz v1, :cond_0

    iget v1, p0, Ljp;->a:I

    if-lt p1, v1, :cond_0

    .line 350
    add-int/lit8 p1, p1, 0x1

    .line 352
    :cond_0
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljf;

    return-object v0

    .line 347
    :cond_1
    iget-object v0, p0, Ljp;->a:Ljb;

    invoke-virtual {v0}, Ljb;->a()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method a()V
    .locals 5

    .prologue
    .line 375
    iget-object v0, p0, Ljp;->a:Ljo;

    invoke-static {v0}, Ljo;->a(Ljo;)Ljb;

    move-result-object v0

    invoke-virtual {v0}, Ljb;->a()Ljf;

    move-result-object v2

    .line 376
    if-eqz v2, :cond_1

    .line 377
    iget-object v0, p0, Ljp;->a:Ljo;

    invoke-static {v0}, Ljo;->a(Ljo;)Ljb;

    move-result-object v0

    invoke-virtual {v0}, Ljb;->c()Ljava/util/ArrayList;

    move-result-object v3

    .line 378
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 379
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    .line 380
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljf;

    .line 381
    if-ne v0, v2, :cond_0

    .line 382
    iput v1, p0, Ljp;->a:I

    .line 388
    :goto_1
    return-void

    .line 379
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 387
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Ljp;->a:I

    goto :goto_1
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 338
    iget-object v0, p0, Ljp;->a:Ljo;

    invoke-static {v0}, Ljo;->a(Ljo;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljp;->a:Ljb;

    invoke-virtual {v0}, Ljb;->c()Ljava/util/ArrayList;

    move-result-object v0

    .line 340
    :goto_0
    iget v1, p0, Ljp;->a:I

    if-gez v1, :cond_1

    .line 341
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 343
    :goto_1
    return v0

    .line 338
    :cond_0
    iget-object v0, p0, Ljp;->a:Ljb;

    invoke-virtual {v0}, Ljb;->a()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    .line 343
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 328
    invoke-virtual {p0, p1}, Ljp;->a(I)Ljf;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 358
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 362
    if-nez p2, :cond_1

    .line 363
    iget-object v0, p0, Ljp;->a:Ljo;

    invoke-static {v0}, Ljo;->a(Ljo;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ljo;->a:I

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    :goto_0
    move-object v0, v1

    .line 366
    check-cast v0, Ljt;

    .line 367
    iget-object v2, p0, Ljp;->a:Ljo;

    iget-boolean v2, v2, Ljo;->a:Z

    if-eqz v2, :cond_0

    move-object v2, v1

    .line 368
    check-cast v2, Landroid/support/v7/internal/view/menu/ListMenuItemView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/support/v7/internal/view/menu/ListMenuItemView;->setForceShowIcon(Z)V

    .line 370
    :cond_0
    invoke-virtual {p0, p1}, Ljp;->a(I)Ljf;

    move-result-object v2

    invoke-interface {v0, v2, v4}, Ljt;->a(Ljf;I)V

    .line 371
    return-object v1

    :cond_1
    move-object v1, p2

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 392
    invoke-virtual {p0}, Ljp;->a()V

    .line 393
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 394
    return-void
.end method
