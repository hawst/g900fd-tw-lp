.class public LalY;
.super Ljava/lang/Object;
.source "MediaStoreUtilities.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 5

    .prologue
    .line 135
    invoke-static {}, LalZ;->values()[LalZ;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 136
    invoke-virtual {v3, p1}, LalZ;->a(Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 137
    invoke-static {v3}, LalZ;->a(LalZ;)Landroid/net/Uri;

    move-result-object v0

    .line 140
    :goto_1
    return-object v0

    .line 135
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 140
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/io/File;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 117
    invoke-virtual {p0, p2}, LalY;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0

    move-object v0, p1

    move-object v1, p2

    move-object v4, v3

    move-object v5, v3

    .line 119
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 120
    const-string v1, "_data"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 121
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 122
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 123
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 126
    :cond_0
    return-object v3
.end method

.method public a(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 130
    invoke-virtual {p0, p1}, LalY;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
