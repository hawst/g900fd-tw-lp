.class public final Lakr;
.super Ljava/lang/Object;
.source "BitmapUtilities.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;
    .locals 3

    .prologue
    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 78
    new-instance v2, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;-><init>(II)V

    return-object v2
.end method

.method public a(I)[Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;
    .locals 1

    .prologue
    .line 83
    new-array v0, p1, [Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0, p1}, Lakr;->a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0, p1}, Lakr;->a(I)[Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    move-result-object v0

    return-object v0
.end method
