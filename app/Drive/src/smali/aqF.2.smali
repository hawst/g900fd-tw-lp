.class LaqF;
.super Ljava/lang/Object;
.source "PreviewPageFetcher.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LbsU",
        "<",
        "Ljava/lang/Void;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:LaqA;

.field private final a:Lcom/google/android/apps/docs/utils/FetchSpec;


# direct methods
.method public constructor <init>(LaqA;Lcom/google/android/apps/docs/utils/FetchSpec;)V
    .locals 1

    .prologue
    .line 87
    iput-object p1, p0, LaqF;->a:LaqA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/utils/FetchSpec;

    iput-object v0, p0, LaqF;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    .line 89
    return-void
.end method


# virtual methods
.method public a()LbsU;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbsU",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, LaqF;->a:LaqA;

    invoke-static {v0}, LaqA;->a(LaqA;)LaGM;

    move-result-object v0

    iget-object v1, p0, LaqF;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/utils/FetchSpec;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-interface {v0, v1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v0

    .line 94
    if-eqz v0, :cond_3

    .line 95
    invoke-interface {v0}, LaGo;->a()LaGv;

    move-result-object v0

    .line 96
    sget-object v1, LaGv;->b:LaGv;

    invoke-virtual {v1, v0}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LaqF;->a:LaqA;

    .line 97
    invoke-static {v1}, LaqA;->a(LaqA;)LtK;

    move-result-object v1

    sget-object v2, Lry;->ak:Lry;

    invoke-interface {v1, v2}, LtK;->a(LtJ;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    sget-object v1, LaGv;->i:LaGv;

    .line 98
    invoke-virtual {v1, v0}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LaqF;->a:LaqA;

    .line 99
    invoke-static {v0}, LaqA;->a(LaqA;)LtK;

    move-result-object v0

    sget-object v1, Lry;->al:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 100
    :cond_1
    iget-object v0, p0, LaqF;->a:LaqA;

    invoke-static {v0}, LaqA;->a(LaqA;)Laoh;

    move-result-object v0

    iget-object v1, p0, LaqF;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    invoke-virtual {v0, v1}, Laoh;->b(Ljava/lang/Object;)LbsU;

    move-result-object v0

    .line 105
    :goto_0
    return-object v0

    .line 102
    :cond_2
    iget-object v0, p0, LaqF;->a:LaqA;

    invoke-static {v0}, LaqA;->a(LaqA;)LanP;

    move-result-object v0

    iget-object v1, p0, LaqF;->a:Lcom/google/android/apps/docs/utils/FetchSpec;

    invoke-virtual {v0, v1}, LanP;->b(Lcom/google/android/apps/docs/utils/FetchSpec;)LbsU;

    move-result-object v0

    goto :goto_0

    .line 105
    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    goto :goto_0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, LaqF;->a()LbsU;

    move-result-object v0

    return-object v0
.end method
