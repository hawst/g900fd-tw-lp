.class Leh;
.super Leg;
.source "ViewCompat.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 752
    invoke-direct {p0}, Leg;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 799
    invoke-static {p1}, Ler;->a(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method public a(III)I
    .locals 1

    .prologue
    .line 779
    invoke-static {p1, p2, p3}, Ler;->a(III)I

    move-result v0

    return v0
.end method

.method a()J
    .locals 2

    .prologue
    .line 755
    invoke-static {}, Ler;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Landroid/view/View;ILandroid/graphics/Paint;)V
    .locals 0

    .prologue
    .line 763
    invoke-static {p1, p2, p3}, Ler;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 764
    return-void
.end method

.method public a(Landroid/view/View;Landroid/graphics/Paint;)V
    .locals 1

    .prologue
    .line 773
    invoke-virtual {p0, p1}, Leh;->c(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0, p1, v0, p2}, Leh;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 775
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 776
    return-void
.end method

.method public b(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 876
    invoke-static {p1}, Ler;->b(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method public b(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 803
    invoke-static {p1, p2}, Ler;->a(Landroid/view/View;F)V

    .line 804
    return-void
.end method

.method public c(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 767
    invoke-static {p1}, Ler;->a(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public c(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 894
    invoke-static {p1}, Ler;->a(Landroid/view/View;)V

    .line 895
    return-void
.end method

.method public c(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 807
    invoke-static {p1, p2}, Ler;->b(Landroid/view/View;F)V

    .line 808
    return-void
.end method

.method public d(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 811
    invoke-static {p1, p2}, Ler;->c(Landroid/view/View;F)V

    .line 812
    return-void
.end method

.method public e(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 783
    invoke-static {p1}, Ler;->b(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public e(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 835
    invoke-static {p1, p2}, Ler;->d(Landroid/view/View;F)V

    .line 836
    return-void
.end method

.method public f(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 791
    invoke-static {p1}, Ler;->c(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public f(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 839
    invoke-static {p1, p2}, Ler;->e(Landroid/view/View;F)V

    .line 840
    return-void
.end method
