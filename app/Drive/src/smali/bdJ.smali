.class public abstract LbdJ;
.super Lbfz;
.source "AbstractGoogleClientRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lbfz;"
    }
.end annotation


# static fields
.field public static final USER_AGENT_SUFFIX:Ljava/lang/String; = "Google-API-Java-Client"


# instance fields
.field private final abstractGoogleClient:LbdH;

.field private disableGZipContent:Z

.field private downloader:LbdA;

.field private final httpContent:LbdZ;

.field private lastResponseHeaders:Lbed;

.field private lastStatusCode:I

.field private lastStatusMessage:Ljava/lang/String;

.field private requestHeaders:Lbed;

.field private final requestMethod:Ljava/lang/String;

.field private responseClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field private uploader:LbdD;

.field private final uriTemplate:Ljava/lang/String;


# direct methods
.method protected constructor <init>(LbdH;Ljava/lang/String;Ljava/lang/String;LbdZ;Ljava/lang/Class;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbdH;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LbdZ;",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 109
    invoke-direct {p0}, Lbfz;-><init>()V

    .line 75
    new-instance v0, Lbed;

    invoke-direct {v0}, Lbed;-><init>()V

    iput-object v0, p0, LbdJ;->requestHeaders:Lbed;

    .line 81
    const/4 v0, -0x1

    iput v0, p0, LbdJ;->lastStatusCode:I

    .line 110
    invoke-static {p5}, LbfN;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, LbdJ;->responseClass:Ljava/lang/Class;

    .line 111
    invoke-static {p1}, LbfN;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbdH;

    iput-object v0, p0, LbdJ;->abstractGoogleClient:LbdH;

    .line 112
    invoke-static {p2}, LbfN;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LbdJ;->requestMethod:Ljava/lang/String;

    .line 113
    invoke-static {p3}, LbfN;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LbdJ;->uriTemplate:Ljava/lang/String;

    .line 114
    iput-object p4, p0, LbdJ;->httpContent:LbdZ;

    .line 116
    invoke-virtual {p1}, LbdH;->getApplicationName()Ljava/lang/String;

    move-result-object v0

    .line 117
    if-eqz v0, :cond_0

    .line 118
    iget-object v1, p0, LbdJ;->requestHeaders:Lbed;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "Google-API-Java-Client"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbed;->l(Ljava/lang/String;)Lbed;

    .line 122
    :goto_0
    return-void

    .line 120
    :cond_0
    iget-object v0, p0, LbdJ;->requestHeaders:Lbed;

    const-string v1, "Google-API-Java-Client"

    invoke-virtual {v0, v1}, Lbed;->l(Ljava/lang/String;)Lbed;

    goto :goto_0
.end method

.method private buildHttpRequest(Z)Lbei;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 297
    iget-object v0, p0, LbdJ;->uploader:LbdD;

    if-nez v0, :cond_5

    move v0, v1

    :goto_0
    invoke-static {v0}, LbfN;->a(Z)V

    .line 298
    if-eqz p1, :cond_0

    iget-object v0, p0, LbdJ;->requestMethod:Ljava/lang/String;

    const-string v3, "GET"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    invoke-static {v2}, LbfN;->a(Z)V

    .line 299
    if-eqz p1, :cond_6

    const-string v0, "HEAD"

    .line 300
    :goto_1
    invoke-virtual {p0}, LbdJ;->getAbstractGoogleClient()LbdH;

    move-result-object v1

    .line 301
    invoke-virtual {v1}, LbdH;->getRequestFactory()Lbej;

    move-result-object v1

    invoke-virtual {p0}, LbdJ;->buildHttpRequestUrl()LbdY;

    move-result-object v2

    iget-object v3, p0, LbdJ;->httpContent:LbdZ;

    invoke-virtual {v1, v0, v2, v3}, Lbej;->a(Ljava/lang/String;LbdY;LbdZ;)Lbei;

    move-result-object v0

    .line 302
    new-instance v1, Lbdk;

    invoke-direct {v1}, Lbdk;-><init>()V

    invoke-virtual {v1, v0}, Lbdk;->b(Lbei;)V

    .line 303
    invoke-virtual {p0}, LbdJ;->getAbstractGoogleClient()LbdH;

    move-result-object v1

    invoke-virtual {v1}, LbdH;->getObjectParser()LbfL;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbei;->a(LbfL;)Lbei;

    .line 305
    iget-object v1, p0, LbdJ;->httpContent:LbdZ;

    if-nez v1, :cond_3

    iget-object v1, p0, LbdJ;->requestMethod:Ljava/lang/String;

    const-string v2, "POST"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, LbdJ;->requestMethod:Ljava/lang/String;

    const-string v2, "PUT"

    .line 306
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, LbdJ;->requestMethod:Ljava/lang/String;

    const-string v2, "PATCH"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 307
    :cond_2
    new-instance v1, LbdV;

    invoke-direct {v1}, LbdV;-><init>()V

    invoke-virtual {v0, v1}, Lbei;->a(LbdZ;)Lbei;

    .line 309
    :cond_3
    invoke-virtual {v0}, Lbei;->a()Lbed;

    move-result-object v1

    iget-object v2, p0, LbdJ;->requestHeaders:Lbed;

    invoke-virtual {v1, v2}, Lbed;->putAll(Ljava/util/Map;)V

    .line 310
    iget-boolean v1, p0, LbdJ;->disableGZipContent:Z

    if-nez v1, :cond_4

    .line 311
    new-instance v1, LbdW;

    invoke-direct {v1}, LbdW;-><init>()V

    invoke-virtual {v0, v1}, Lbei;->a(Lbea;)Lbei;

    .line 313
    :cond_4
    invoke-virtual {v0}, Lbei;->a()Lbeo;

    move-result-object v1

    .line 314
    new-instance v2, LbdK;

    invoke-direct {v2, p0, v1, v0}, LbdK;-><init>(LbdJ;Lbeo;Lbei;)V

    invoke-virtual {v0, v2}, Lbei;->a(Lbeo;)Lbei;

    .line 325
    return-object v0

    :cond_5
    move v0, v2

    .line 297
    goto/16 :goto_0

    .line 299
    :cond_6
    iget-object v0, p0, LbdJ;->requestMethod:Ljava/lang/String;

    goto :goto_1
.end method

.method private executeUnparsed(Z)Lbel;
    .locals 4

    .prologue
    .line 417
    iget-object v0, p0, LbdJ;->uploader:LbdD;

    if-nez v0, :cond_1

    .line 419
    invoke-direct {p0, p1}, LbdJ;->buildHttpRequest(Z)Lbei;

    move-result-object v0

    invoke-virtual {v0}, Lbei;->a()Lbel;

    move-result-object v0

    .line 436
    :cond_0
    invoke-virtual {v0}, Lbel;->a()Lbed;

    move-result-object v1

    iput-object v1, p0, LbdJ;->lastResponseHeaders:Lbed;

    .line 437
    invoke-virtual {v0}, Lbel;->a()I

    move-result v1

    iput v1, p0, LbdJ;->lastStatusCode:I

    .line 438
    invoke-virtual {v0}, Lbel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LbdJ;->lastStatusMessage:Ljava/lang/String;

    .line 439
    return-object v0

    .line 422
    :cond_1
    invoke-virtual {p0}, LbdJ;->buildHttpRequestUrl()LbdY;

    move-result-object v0

    .line 423
    invoke-virtual {p0}, LbdJ;->getAbstractGoogleClient()LbdH;

    move-result-object v1

    .line 424
    invoke-virtual {v1}, LbdH;->getRequestFactory()Lbej;

    move-result-object v1

    iget-object v2, p0, LbdJ;->requestMethod:Ljava/lang/String;

    iget-object v3, p0, LbdJ;->httpContent:LbdZ;

    invoke-virtual {v1, v2, v0, v3}, Lbej;->a(Ljava/lang/String;LbdY;LbdZ;)Lbei;

    move-result-object v1

    .line 425
    invoke-virtual {v1}, Lbei;->c()Z

    move-result v1

    .line 427
    iget-object v2, p0, LbdJ;->uploader:LbdD;

    iget-object v3, p0, LbdJ;->requestHeaders:Lbed;

    invoke-virtual {v2, v3}, LbdD;->a(Lbed;)LbdD;

    move-result-object v2

    iget-boolean v3, p0, LbdJ;->disableGZipContent:Z

    .line 428
    invoke-virtual {v2, v3}, LbdD;->a(Z)LbdD;

    move-result-object v2

    invoke-virtual {v2, v0}, LbdD;->a(LbdY;)Lbel;

    move-result-object v0

    .line 429
    invoke-virtual {v0}, Lbel;->a()Lbei;

    move-result-object v2

    invoke-virtual {p0}, LbdJ;->getAbstractGoogleClient()LbdH;

    move-result-object v3

    invoke-virtual {v3}, LbdH;->getObjectParser()LbfL;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbei;->a(LbfL;)Lbei;

    .line 431
    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lbel;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 432
    invoke-virtual {p0, v0}, LbdJ;->newExceptionOnError(Lbel;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public buildHttpRequest()Lbei;
    .locals 1

    .prologue
    .line 277
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LbdJ;->buildHttpRequest(Z)Lbei;

    move-result-object v0

    return-object v0
.end method

.method public buildHttpRequestUrl()LbdY;
    .locals 4

    .prologue
    .line 265
    new-instance v0, LbdY;

    iget-object v1, p0, LbdJ;->abstractGoogleClient:LbdH;

    .line 266
    invoke-virtual {v1}, LbdH;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LbdJ;->uriTemplate:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v1, v2, p0, v3}, Lbex;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LbdY;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public buildHttpRequestUsingHead()Lbei;
    .locals 1

    .prologue
    .line 292
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LbdJ;->buildHttpRequest(Z)Lbei;

    move-result-object v0

    return-object v0
.end method

.method public final checkRequiredParameter(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 606
    iget-object v0, p0, LbdJ;->abstractGoogleClient:LbdH;

    .line 607
    invoke-virtual {v0}, LbdH;->getSuppressRequiredParameterChecks()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Required parameter %s must be specified"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v1

    .line 606
    invoke-static {v0, v3, v2}, LbfN;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 609
    return-void

    :cond_1
    move v0, v1

    .line 607
    goto :goto_0
.end method

.method public execute()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 469
    invoke-virtual {p0}, LbdJ;->executeUnparsed()Lbel;

    move-result-object v0

    iget-object v1, p0, LbdJ;->responseClass:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Lbel;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public executeAndDownloadTo(Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 541
    invoke-virtual {p0}, LbdJ;->executeUnparsed()Lbel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbel;->a(Ljava/io/OutputStream;)V

    .line 542
    return-void
.end method

.method public executeAsInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 496
    invoke-virtual {p0}, LbdJ;->executeUnparsed()Lbel;

    move-result-object v0

    invoke-virtual {v0}, Lbel;->a()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public executeMedia()Lbel;
    .locals 2

    .prologue
    .line 379
    const-string v0, "alt"

    const-string v1, "media"

    invoke-virtual {p0, v0, v1}, LbdJ;->set(Ljava/lang/String;Ljava/lang/Object;)LbdJ;

    .line 380
    invoke-virtual {p0}, LbdJ;->executeUnparsed()Lbel;

    move-result-object v0

    return-object v0
.end method

.method public executeMediaAndDownloadTo(Ljava/io/OutputStream;)V
    .locals 3

    .prologue
    .line 559
    iget-object v0, p0, LbdJ;->downloader:LbdA;

    if-nez v0, :cond_0

    .line 560
    invoke-virtual {p0}, LbdJ;->executeMedia()Lbel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbel;->a(Ljava/io/OutputStream;)V

    .line 564
    :goto_0
    return-void

    .line 562
    :cond_0
    iget-object v0, p0, LbdJ;->downloader:LbdA;

    invoke-virtual {p0}, LbdJ;->buildHttpRequestUrl()LbdY;

    move-result-object v1

    iget-object v2, p0, LbdJ;->requestHeaders:Lbed;

    invoke-virtual {v0, v1, v2, p1}, LbdA;->a(LbdY;Lbed;Ljava/io/OutputStream;)V

    goto :goto_0
.end method

.method public executeMediaAsInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 523
    invoke-virtual {p0}, LbdJ;->executeMedia()Lbel;

    move-result-object v0

    invoke-virtual {v0}, Lbel;->a()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public executeUnparsed()Lbel;
    .locals 1

    .prologue
    .line 352
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LbdJ;->executeUnparsed(Z)Lbel;

    move-result-object v0

    return-object v0
.end method

.method public executeUsingHead()Lbel;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 405
    iget-object v0, p0, LbdJ;->uploader:LbdD;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbfN;->a(Z)V

    .line 406
    invoke-direct {p0, v1}, LbdJ;->executeUnparsed(Z)Lbel;

    move-result-object v0

    .line 407
    invoke-virtual {v0}, Lbel;->a()V

    .line 408
    return-object v0

    .line 405
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAbstractGoogleClient()LbdH;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, LbdJ;->abstractGoogleClient:LbdH;

    return-object v0
.end method

.method public final getDisableGZipContent()Z
    .locals 1

    .prologue
    .line 126
    iget-boolean v0, p0, LbdJ;->disableGZipContent:Z

    return v0
.end method

.method public final getHttpContent()LbdZ;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, LbdJ;->httpContent:LbdZ;

    return-object v0
.end method

.method public final getLastResponseHeaders()Lbed;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, LbdJ;->lastResponseHeaders:Lbed;

    return-object v0
.end method

.method public final getLastStatusCode()I
    .locals 1

    .prologue
    .line 207
    iget v0, p0, LbdJ;->lastStatusCode:I

    return v0
.end method

.method public final getLastStatusMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, LbdJ;->lastStatusMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final getMediaHttpDownloader()LbdA;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, LbdJ;->downloader:LbdA;

    return-object v0
.end method

.method public final getMediaHttpUploader()LbdD;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, LbdJ;->uploader:LbdD;

    return-object v0
.end method

.method public final getRequestHeaders()Lbed;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, LbdJ;->requestHeaders:Lbed;

    return-object v0
.end method

.method public final getRequestMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, LbdJ;->requestMethod:Ljava/lang/String;

    return-object v0
.end method

.method public final getResponseClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 220
    iget-object v0, p0, LbdJ;->responseClass:Ljava/lang/Class;

    return-object v0
.end method

.method public final getUriTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, LbdJ;->uriTemplate:Ljava/lang/String;

    return-object v0
.end method

.method protected final initializeMediaDownload()V
    .locals 3

    .prologue
    .line 250
    iget-object v0, p0, LbdJ;->abstractGoogleClient:LbdH;

    invoke-virtual {v0}, LbdH;->getRequestFactory()Lbej;

    move-result-object v0

    .line 251
    new-instance v1, LbdA;

    .line 252
    invoke-virtual {v0}, Lbej;->a()Lbeq;

    move-result-object v2

    invoke-virtual {v0}, Lbej;->a()Lbek;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LbdA;-><init>(Lbeq;Lbek;)V

    iput-object v1, p0, LbdJ;->downloader:LbdA;

    .line 253
    return-void
.end method

.method protected final initializeMediaUpload(LbdS;)V
    .locals 3

    .prologue
    .line 234
    iget-object v0, p0, LbdJ;->abstractGoogleClient:LbdH;

    invoke-virtual {v0}, LbdH;->getRequestFactory()Lbej;

    move-result-object v0

    .line 235
    new-instance v1, LbdD;

    .line 236
    invoke-virtual {v0}, Lbej;->a()Lbeq;

    move-result-object v2

    invoke-virtual {v0}, Lbej;->a()Lbek;

    move-result-object v0

    invoke-direct {v1, p1, v2, v0}, LbdD;-><init>(LbdS;Lbeq;Lbek;)V

    iput-object v1, p0, LbdJ;->uploader:LbdD;

    .line 237
    iget-object v0, p0, LbdJ;->uploader:LbdD;

    iget-object v1, p0, LbdJ;->requestMethod:Ljava/lang/String;

    invoke-virtual {v0, v1}, LbdD;->a(Ljava/lang/String;)LbdD;

    .line 238
    iget-object v0, p0, LbdJ;->httpContent:LbdZ;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, LbdJ;->uploader:LbdD;

    iget-object v1, p0, LbdJ;->httpContent:LbdZ;

    invoke-virtual {v0, v1}, LbdD;->a(LbdZ;)LbdD;

    .line 241
    :cond_0
    return-void
.end method

.method protected newExceptionOnError(Lbel;)Ljava/io/IOException;
    .locals 1

    .prologue
    .line 456
    new-instance v0, Lbem;

    invoke-direct {v0, p1}, Lbem;-><init>(Lbel;)V

    return-object v0
.end method

.method public final queue(Lbdm;Ljava/lang/Class;Lbdl;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lbdm;",
            "Ljava/lang/Class",
            "<TE;>;",
            "Lbdl",
            "<TT;TE;>;)V"
        }
    .end annotation

    .prologue
    .line 581
    iget-object v0, p0, LbdJ;->uploader:LbdD;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Batching media requests is not supported"

    invoke-static {v0, v1}, LbfN;->a(ZLjava/lang/Object;)V

    .line 582
    invoke-virtual {p0}, LbdJ;->buildHttpRequest()Lbei;

    move-result-object v0

    invoke-virtual {p0}, LbdJ;->getResponseClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1, v0, v1, p2, p3}, Lbdm;->a(Lbei;Ljava/lang/Class;Ljava/lang/Class;Lbdl;)Lbdm;

    .line 583
    return-void

    .line 581
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public set(Ljava/lang/String;Ljava/lang/Object;)LbdJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")",
            "LbdJ",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 592
    invoke-super {p0, p1, p2}, Lbfz;->set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;

    move-result-object v0

    check-cast v0, LbdJ;

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0, p1, p2}, LbdJ;->set(Ljava/lang/String;Ljava/lang/Object;)LbdJ;

    move-result-object v0

    return-object v0
.end method

.method public setDisableGZipContent(Z)LbdJ;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LbdJ",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 142
    iput-boolean p1, p0, LbdJ;->disableGZipContent:Z

    .line 143
    return-object p0
.end method

.method public setRequestHeaders(Lbed;)LbdJ;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbed;",
            ")",
            "LbdJ",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 192
    iput-object p1, p0, LbdJ;->requestHeaders:Lbed;

    .line 193
    return-object p0
.end method
