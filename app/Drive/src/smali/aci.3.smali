.class public Laci;
.super Ljava/lang/Object;
.source "SharingAcl.java"


# instance fields
.field private final a:Lqo;

.field private final a:Lqt;

.field private final a:Z

.field private final b:Z


# direct methods
.method public constructor <init>(Laci;Lqt;Z)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Lqs;

    invoke-direct {v0}, Lqs;-><init>()V

    .line 34
    invoke-virtual {p1}, Laci;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Lqs;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lqs;

    move-result-object v0

    .line 35
    invoke-virtual {p1}, Laci;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lqs;->a(Ljava/lang/String;)Lqs;

    move-result-object v0

    .line 36
    invoke-virtual {p1}, Laci;->a()Lqx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lqs;->a(Lqx;)Lqs;

    move-result-object v0

    .line 37
    invoke-virtual {v0, p2}, Lqs;->a(Lqt;)Lqs;

    move-result-object v0

    .line 38
    invoke-virtual {v0, p3}, Lqs;->a(Z)Lqs;

    move-result-object v0

    invoke-virtual {v0}, Lqs;->a()Lqo;

    move-result-object v0

    iput-object v0, p0, Laci;->a:Lqo;

    .line 39
    iget-object v0, p1, Laci;->a:Lqt;

    iput-object v0, p0, Laci;->a:Lqt;

    .line 40
    iget-boolean v0, p1, Laci;->a:Z

    iput-boolean v0, p0, Laci;->a:Z

    .line 41
    iget-object v0, p0, Laci;->a:Lqt;

    if-ne v0, p2, :cond_0

    iget-boolean v0, p0, Laci;->a:Z

    if-eq v0, p3, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Laci;->b:Z

    .line 43
    return-void

    .line 41
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lqo;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Lqs;

    invoke-direct {v0}, Lqs;-><init>()V

    .line 26
    invoke-virtual {v0, p1}, Lqs;->a(Lqo;)Lqs;

    move-result-object v0

    invoke-virtual {v0}, Lqs;->a()Lqo;

    move-result-object v0

    iput-object v0, p0, Laci;->a:Lqo;

    .line 27
    invoke-virtual {p1}, Lqo;->a()Lqt;

    move-result-object v0

    iput-object v0, p0, Laci;->a:Lqt;

    .line 28
    invoke-virtual {p1}, Lqo;->a()Z

    move-result v0

    iput-boolean v0, p0, Laci;->a:Z

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Laci;->b:Z

    .line 30
    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/gms/drive/database/data/ResourceSpec;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Laci;->a:Lqo;

    invoke-virtual {v0}, Lqo;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Laci;->a:Lqo;

    invoke-virtual {v0}, Lqo;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Lqo;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Laci;->a:Lqo;

    return-object v0
.end method

.method public a()Lqt;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Laci;->a:Lqo;

    invoke-virtual {v0}, Lqo;->a()Lqt;

    move-result-object v0

    return-object v0
.end method

.method public a()Lqv;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Laci;->a:Lqo;

    invoke-virtual {v0}, Lqo;->a()Lqv;

    move-result-object v0

    return-object v0
.end method

.method public a()Lqx;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Laci;->a:Lqo;

    invoke-virtual {v0}, Lqo;->a()Lqx;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Laci;->b:Z

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Laci;->a:Lqo;

    invoke-virtual {v0}, Lqo;->a()Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 79
    instance-of v1, p1, Laci;

    if-nez v1, :cond_1

    .line 87
    :cond_0
    :goto_0
    return v0

    .line 83
    :cond_1
    check-cast p1, Laci;

    .line 84
    iget-object v1, p0, Laci;->a:Lqo;

    iget-object v2, p1, Laci;->a:Lqo;

    invoke-virtual {v1, v2}, Lqo;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Laci;->a:Lqt;

    iget-object v2, p1, Laci;->a:Lqt;

    invoke-virtual {v1, v2}, Lqt;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Laci;->a:Z

    iget-boolean v2, p1, Laci;->a:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Laci;->b:Z

    iget-boolean v2, p1, Laci;->b:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 91
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Laci;->a:Lqo;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Laci;->a:Lqt;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Laci;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Laci;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
