.class public LaeG;
.super Lbxl;
.source "AccountMetadataGDataParser.java"


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Lbxl;-><init>(Ljava/io/InputStream;Lorg/xmlpull/v1/XmlPullParser;)V

    .line 77
    return-void
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;)LaGv;
    .locals 2

    .prologue
    .line 323
    const/4 v0, 0x0

    const-string v1, "kind"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 324
    invoke-static {v0}, LaGv;->a(Ljava/lang/String;)LaGv;

    move-result-object v0

    return-object v0
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;)LaHJ;
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 165
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    if-ne v9, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 166
    invoke-direct {p0, p1}, LaeG;->a(Lorg/xmlpull/v1/XmlPullParser;)Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 168
    const-string v0, "http://schemas.google.com/docs/2007"

    const-string v4, "installedAppId"

    invoke-interface {p1, v0, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 169
    const-string v0, "http://schemas.google.com/docs/2007"

    const-string v5, "installedAppName"

    invoke-interface {p1, v0, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 170
    const-string v0, "http://schemas.google.com/docs/2007"

    const-string v6, "installedAppObjectType"

    invoke-interface {p1, v0, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 171
    const-string v0, "http://schemas.google.com/docs/2007"

    const-string v7, "installedAppSupportsCreate"

    .line 172
    invoke-interface {p1, v0, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 171
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    .line 173
    new-instance v0, LaHJ;

    invoke-direct {v0, v4, v5, v6, v7}, LaHJ;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 175
    const-string v4, "http://schemas.google.com/docs/2007"

    const-string v5, "installedAppSupportsMobileBrowser"

    .line 176
    invoke-interface {p1, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 177
    if-eqz v4, :cond_1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_1

    .line 178
    const-string v4, "AccountMetadataGDataParser"

    const-string v5, "%s doesn\'t support mobile browsers; ignoring"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v2

    invoke-static {v4, v5, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v3

    .line 217
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 165
    goto :goto_0

    .line 184
    :cond_1
    :goto_2
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v4

    .line 185
    packed-switch v4, :pswitch_data_0

    goto :goto_2

    .line 221
    :pswitch_0
    new-instance v0, Lbxk;

    const-string v1, "Premature end of document before </installedApp>"

    invoke-direct {v0, v1}, Lbxk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 187
    :pswitch_1
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v4

    .line 188
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v5

    .line 189
    sget-object v6, Lbxq;->i:Ljava/lang/String;

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 190
    const-string v4, "http://schemas.google.com/docs/2007#product"

    sget-object v5, Lbxq;->h:Ljava/lang/String;

    invoke-interface {p1, v3, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 191
    sget-object v4, Lbxq;->l:Ljava/lang/String;

    invoke-interface {p1, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 192
    sget-object v5, Lbxq;->j:Ljava/lang/String;

    invoke-interface {p1, v3, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 193
    invoke-virtual {v0, v5, v4}, LaHJ;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 196
    :cond_2
    const-string v6, "http://schemas.google.com/docs/2007"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 197
    invoke-static {p1}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v5

    .line 198
    const-string v6, "installedAppPrimaryMimeType"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 199
    invoke-virtual {v0, v5}, LaHJ;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 200
    :cond_3
    const-string v6, "installedAppSecondaryMimeType"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 201
    invoke-virtual {v0, v5}, LaHJ;->b(Ljava/lang/String;)V

    goto :goto_2

    .line 202
    :cond_4
    const-string v6, "installedAppPrimaryFileExtension"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 203
    invoke-virtual {v0, v5}, LaHJ;->c(Ljava/lang/String;)V

    goto :goto_2

    .line 204
    :cond_5
    const-string v6, "installedAppSecondaryFileExtension"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 205
    invoke-virtual {v0, v5}, LaHJ;->d(Ljava/lang/String;)V

    goto :goto_2

    .line 206
    :cond_6
    const-string v6, "installedAppIcon"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 209
    const-string v6, "AccountMetadataGDataParser"

    const-string v7, "Unknown tag: %s with text %s"

    new-array v8, v9, [Ljava/lang/Object;

    aput-object v4, v8, v2

    aput-object v5, v8, v1

    invoke-static {v6, v7, v8}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_2

    .line 212
    :cond_7
    const-string v6, "AccountMetadataGDataParser"

    const-string v7, "Unknwon tag: %s in namespace %s"

    new-array v8, v9, [Ljava/lang/Object;

    aput-object v4, v8, v2

    aput-object v5, v8, v1

    invoke-static {v6, v7, v8}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_2

    .line 216
    :pswitch_2
    invoke-direct {p0, p1}, LaeG;->a(Lorg/xmlpull/v1/XmlPullParser;)Z

    move-result v4

    if-eqz v4, :cond_1

    goto/16 :goto_1

    .line 185
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;)Lqt;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 278
    const-string v0, "primaryRole"

    invoke-interface {p1, v9, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 279
    invoke-static {v0}, Lqv;->a(Ljava/lang/String;)Lqv;

    move-result-object v1

    .line 280
    sget-object v2, Lqv;->f:Lqv;

    invoke-virtual {v1, v2}, Lqv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 281
    const-class v2, Lqq;

    invoke-static {v2}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v2

    .line 283
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    .line 284
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v4

    .line 285
    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 306
    :pswitch_0
    new-instance v0, Lbxk;

    const-string v1, "Premature end of document before </additionalRoleSet>"

    invoke-direct {v0, v1}, Lbxk;-><init>(Ljava/lang/String;)V

    throw v0

    .line 287
    :pswitch_1
    const-string v3, "additionalRole"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 288
    const-string v3, "value"

    invoke-interface {p1, v9, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 289
    invoke-static {v3}, Lqq;->a(Ljava/lang/String;)Lqq;

    move-result-object v3

    .line 290
    sget-object v4, Lqq;->b:Lqq;

    if-eq v3, v4, :cond_1

    .line 291
    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 293
    :cond_1
    const-string v3, "AccountMetadataGDataParser"

    const-string v4, "Unknown additional role: %s"

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v0, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 296
    :cond_2
    const-string v3, "AccountMetadataGDataParser"

    const-string v5, "Unknown tag: %s"

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v4, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 300
    :pswitch_2
    const-string v3, "additionalRoleSet"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 301
    invoke-static {v1, v2}, Lqt;->a(Lqv;Ljava/util/Set;)Lqt;

    move-result-object v0

    .line 314
    :goto_1
    return-object v0

    .line 311
    :cond_3
    const-string v1, "AccountMetadataGDataParser"

    const-string v2, "Unknown primary role: %s"

    new-array v3, v8, [Ljava/lang/Object;

    aput-object v0, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    invoke-virtual {p0}, LaeG;->c()V

    .line 314
    sget-object v0, Lqt;->g:Lqt;

    goto :goto_1

    .line 285
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;LaeE;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 232
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    if-ne v8, v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 233
    invoke-direct {p0, p1}, LaeG;->b(Lorg/xmlpull/v1/XmlPullParser;)Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 235
    const/4 v0, 0x0

    const-string v3, "kind"

    invoke-interface {p1, v0, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 238
    :cond_0
    :goto_1
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    .line 239
    packed-switch v3, :pswitch_data_0

    goto :goto_1

    .line 267
    :pswitch_0
    new-instance v0, Lbxk;

    const-string v1, "Premature end of document before </additionalRoleInfo>"

    invoke-direct {v0, v1}, Lbxk;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v2

    .line 232
    goto :goto_0

    .line 241
    :pswitch_1
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 242
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v4

    .line 243
    const-string v5, "http://schemas.google.com/docs/2007"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 244
    const-string v4, "additionalRoleSet"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 245
    invoke-direct {p0, p1}, LaeG;->a(Lorg/xmlpull/v1/XmlPullParser;)Lqt;

    move-result-object v3

    .line 246
    sget-object v4, Lqt;->g:Lqt;

    invoke-virtual {v3, v4}, Lqt;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 247
    invoke-virtual {p2, v0, v3}, LaeE;->a(Ljava/lang/String;Lqt;)V

    goto :goto_1

    .line 249
    :cond_2
    const-string v3, "AccountMetadataGDataParser"

    const-string v4, "Unknown combined role in document kind: %s"

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v0, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 253
    :cond_3
    const-string v4, "AccountMetadataGDataParser"

    const-string v5, "Unknown tag: %s"

    new-array v6, v1, [Ljava/lang/Object;

    aput-object v3, v6, v2

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    invoke-virtual {p0}, LaeG;->c()V

    goto :goto_1

    .line 257
    :cond_4
    const-string v5, "AccountMetadataGDataParser"

    const-string v6, "Unknown tag: %s in namespace %s"

    new-array v7, v8, [Ljava/lang/Object;

    aput-object v3, v7, v2

    aput-object v4, v7, v1

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    invoke-virtual {p0}, LaeG;->c()V

    goto :goto_1

    .line 262
    :pswitch_2
    invoke-direct {p0, p1}, LaeG;->b(Lorg/xmlpull/v1/XmlPullParser;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 263
    return-void

    .line 239
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;)Z
    .locals 2

    .prologue
    .line 154
    const-string v0, "installedApp"

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "http://schemas.google.com/docs/2007"

    .line 155
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lorg/xmlpull/v1/XmlPullParser;)Z
    .locals 2

    .prologue
    .line 318
    const-string v0, "additionalRoleInfo"

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "http://schemas.google.com/docs/2007"

    .line 319
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a()LaeE;
    .locals 1

    .prologue
    .line 81
    new-instance v0, LaeE;

    invoke-direct {v0}, LaeE;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic a()Lbxb;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0}, LaeG;->a()LaeE;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lbxb;)V
    .locals 6

    .prologue
    .line 87
    invoke-virtual {p0}, LaeG;->a()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v1

    .line 89
    instance-of v0, p1, LaeE;

    if-nez v0, :cond_0

    .line 90
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected GdataAccountMetadataEntry!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p1

    .line 93
    check-cast v0, LaeE;

    .line 94
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 95
    invoke-static {}, LbqS;->a()LbqQ;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-interface {v3, v4}, LbqQ;->a([B)LbqL;

    move-result-object v3

    invoke-virtual {v3}, LbqL;->toString()Ljava/lang/String;

    move-result-object v3

    .line 96
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v4

    .line 98
    :try_start_0
    const-string v5, "http://schemas.google.com/g/2005"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 99
    const-string v3, "quotaBytesTotal"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 100
    invoke-static {v1}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    .line 101
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LaeE;->a(J)V

    .line 151
    :cond_1
    :goto_0
    return-void

    .line 102
    :cond_2
    const-string v3, "quotaBytesUsed"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 103
    invoke-static {v1}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    .line 104
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LaeE;->b(J)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 148
    :catch_0
    move-exception v0

    .line 149
    new-instance v1, Lbxk;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error parsing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lbxk;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 106
    :cond_3
    :try_start_1
    const-string v5, "http://schemas.google.com/docs/2007"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 107
    const-string v4, "quotaBytesUsedInTrash"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 108
    invoke-static {v1}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    .line 109
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LaeE;->c(J)V

    goto :goto_0

    .line 110
    :cond_4
    const-string v4, "78a01019947af21bd73cd4efde4707ce"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 111
    invoke-static {v1}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    .line 112
    invoke-static {}, LbqS;->a()LbqQ;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-interface {v2, v1}, LbqQ;->a([B)LbqL;

    move-result-object v1

    invoke-virtual {v1}, LbqL;->toString()Ljava/lang/String;

    move-result-object v1

    .line 113
    const-string v2, "9053bb14821d3c6da8319dbf5a065400"

    .line 114
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object v1, LaJS;->b:LaJS;

    .line 115
    :goto_1
    invoke-virtual {v0, v1}, LaeE;->a(LaJS;)V

    goto :goto_0

    .line 114
    :cond_5
    sget-object v1, LaJS;->a:LaJS;

    goto :goto_1

    .line 116
    :cond_6
    const-string v3, "importFormat"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string v3, "exportFormat"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 117
    :cond_7
    const/4 v3, 0x0

    const-string v4, "source"

    .line 118
    invoke-interface {v1, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 119
    const/4 v4, 0x0

    const-string v5, "target"

    .line 120
    invoke-interface {v1, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 121
    const-string v4, "importFormat"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 122
    invoke-virtual {v0, v3, v1}, LaeE;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 124
    :cond_8
    invoke-virtual {v0, v3, v1}, LaeE;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 126
    :cond_9
    const-string v3, "featureName"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 127
    invoke-static {v1}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    .line 128
    invoke-virtual {v0, v1}, LaeE;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 129
    :cond_a
    const-string v3, "maxUploadSize"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 130
    invoke-direct {p0, v1}, LaeG;->a(Lorg/xmlpull/v1/XmlPullParser;)LaGv;

    move-result-object v2

    .line 131
    invoke-static {v1}, Lbxe;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 132
    invoke-virtual {v0, v2, v4, v5}, LaeE;->a(LaGv;J)V

    goto/16 :goto_0

    .line 133
    :cond_b
    const-string v3, "largestChangestamp"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 134
    const/4 v2, 0x0

    const-string v3, "value"

    invoke-interface {v1, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 135
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, LaeE;->a(I)V

    goto/16 :goto_0

    .line 136
    :cond_c
    const-string v3, "remainingChangestamps"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 137
    const/4 v2, 0x0

    const-string v3, "value"

    invoke-interface {v1, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 138
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, LaeE;->b(I)V

    goto/16 :goto_0

    .line 139
    :cond_d
    const-string v3, "installedApp"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 140
    invoke-direct {p0, v1}, LaeG;->a(Lorg/xmlpull/v1/XmlPullParser;)LaHJ;

    move-result-object v1

    .line 141
    if-eqz v1, :cond_1

    .line 142
    invoke-virtual {v0, v1}, LaeE;->a(LaHJ;)Z

    goto/16 :goto_0

    .line 144
    :cond_e
    const-string v3, "additionalRoleInfo"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 145
    invoke-direct {p0, v1, v0}, LaeG;->a(Lorg/xmlpull/v1/XmlPullParser;LaeE;)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
