.class public final Ladw;
.super Ljava/lang/Object;
.source "FileContentInstance.java"


# static fields
.field static final synthetic a:Z


# instance fields
.field private a:I

.field private final a:LaGp;

.field private final a:LadE;

.field private final a:LadY;

.field private a:Ladz;

.field private final a:Lalo;

.field private a:LbsU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbsU",
            "<*>;"
        }
    .end annotation
.end field

.field private final a:Ljava/io/File;

.field private final b:LadY;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Ladw;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Ladw;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lalo;LadE;LaGp;)V
    .locals 2

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    const/4 v0, 0x0

    iput v0, p0, Ladw;->a:I

    .line 85
    sget-object v0, Ladz;->c:Ladz;

    iput-object v0, p0, Ladw;->a:Ladz;

    .line 93
    iput-object p2, p0, Ladw;->a:LadE;

    .line 94
    iput-object p1, p0, Ladw;->a:Lalo;

    .line 95
    iput-object p3, p0, Ladw;->a:LaGp;

    .line 97
    invoke-virtual {p3}, LaGp;->a()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Ladw;->a:Ljava/io/File;

    .line 98
    invoke-virtual {p3}, LaGp;->a()LadY;

    move-result-object v0

    iput-object v0, p0, Ladw;->a:LadY;

    .line 99
    iget-object v0, p0, Ladw;->a:Ljava/io/File;

    if-nez v0, :cond_0

    iget-object v0, p0, Ladw;->a:LadY;

    :goto_0
    iput-object v0, p0, Ladw;->b:LadY;

    .line 100
    return-void

    .line 99
    :cond_0
    new-instance v0, LadY;

    iget-object v1, p0, Ladw;->a:Ljava/io/File;

    invoke-direct {v0, v1}, LadY;-><init>(Ljava/io/File;)V

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 237
    iget-object v0, p0, Ladw;->a:LadE;

    iget-object v1, p0, Ladw;->b:LadY;

    invoke-interface {v0, v1}, LadE;->a(LadY;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a()LaGp;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Ladw;->a:LaGp;

    return-object v0
.end method

.method public declared-synchronized a()LadY;
    .locals 2

    .prologue
    .line 188
    monitor-enter p0

    :try_start_0
    sget-object v0, Ladz;->a:Ladz;

    iget-object v1, p0, Ladw;->a:Ladz;

    .line 189
    invoke-virtual {v0, v1}, Ladz;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ladz;->b:Ladz;

    iget-object v1, p0, Ladw;->a:Ladz;

    invoke-virtual {v0, v1}, Ladz;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 188
    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 190
    iget-object v0, p0, Ladw;->b:LadY;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 189
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()Ladz;
    .locals 1

    .prologue
    .line 230
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ladw;->a:Ladz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()Ljava/io/File;
    .locals 2

    .prologue
    .line 198
    monitor-enter p0

    :try_start_0
    sget-object v0, Ladz;->a:Ladz;

    iget-object v1, p0, Ladw;->a:Ladz;

    .line 199
    invoke-virtual {v0, v1}, Ladz;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ladz;->b:Ladz;

    iget-object v1, p0, Ladw;->a:Ladz;

    invoke-virtual {v0, v1}, Ladz;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 198
    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 200
    iget-object v0, p0, Ladw;->a:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 199
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 198
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Ladw;->a:Ladz;

    sget-object v1, Ladz;->d:Ladz;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 215
    iget-object v0, p0, Ladw;->a:LaGp;

    invoke-virtual {v0}, LaGp;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 214
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Ljavax/crypto/SecretKey;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Ladw;->a:LaGp;

    invoke-virtual {v0}, LaGp;->a()Ljavax/crypto/SecretKey;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized a()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 118
    monitor-enter p0

    :try_start_0
    sget-object v2, Ladz;->c:Ladz;

    iget-object v3, p0, Ladw;->a:Ladz;

    invoke-virtual {v2, v3}, Ladz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Ladz;->a:Ladz;

    iget-object v3, p0, Ladw;->a:Ladz;

    invoke-virtual {v2, v3}, Ladz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    invoke-static {v0}, LbiT;->b(Z)V

    .line 119
    sget-object v0, Ladz;->a:Ladz;

    iput-object v0, p0, Ladw;->a:Ladz;

    .line 120
    iget v0, p0, Ladw;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ladw;->a:I

    .line 121
    const-string v0, "FileContentInstance"

    const-string v1, "lock: %s lockers: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Ladw;->a:LaGp;

    invoke-virtual {v4}, LaGp;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Ladw;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    monitor-exit p0

    return-void

    .line 118
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(LbsU;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbsU",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 263
    iput-object p1, p0, Ladw;->a:LbsU;

    .line 264
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Ladw;->a:Ljava/io/File;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized b()V
    .locals 4

    .prologue
    .line 128
    monitor-enter p0

    :try_start_0
    sget-object v0, Ladz;->c:Ladz;

    iget-object v1, p0, Ladw;->a:Ladz;

    invoke-virtual {v0, v1}, Ladz;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 129
    iget-object v0, p0, Ladw;->a:Lalo;

    iget-object v1, p0, Ladw;->a:Ljava/io/File;

    .line 130
    invoke-interface {v0, v1}, Lalo;->a(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ladw;->a:LadE;

    iget-object v1, p0, Ladw;->a:Ljava/io/File;

    invoke-interface {v0, v1}, LadE;->a(Ljava/io/File;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 129
    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 131
    sget-object v0, Ladz;->b:Ladz;

    iput-object v0, p0, Ladw;->a:Ladz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    monitor-exit p0

    return-void

    .line 130
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 128
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Ladw;->b:LadY;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized c()V
    .locals 6

    .prologue
    .line 138
    monitor-enter p0

    :try_start_0
    sget-object v0, Ladx;->a:[I

    iget-object v1, p0, Ladw;->a:Ladz;

    invoke-virtual {v1}, Ladz;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 148
    sget-boolean v0, Ladw;->a:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 140
    :pswitch_0
    :try_start_1
    iget v0, p0, Ladw;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Ladw;->a:I

    if-nez v0, :cond_0

    .line 141
    sget-object v0, Ladz;->c:Ladz;

    iput-object v0, p0, Ladw;->a:Ladz;

    .line 151
    :cond_0
    :goto_0
    const-string v0, "FileContentInstance"

    const-string v1, "release: %s state: %s lockers: %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Ladw;->a:LaGp;

    invoke-virtual {v4}, LaGp;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Ladw;->a:Ladz;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, Ladw;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152
    monitor-exit p0

    return-void

    .line 145
    :pswitch_1
    :try_start_2
    sget-object v0, Ladz;->c:Ladz;

    iput-object v0, p0, Ladw;->a:Ladz;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 138
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Ladw;->a:Ljava/io/File;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized d()V
    .locals 2

    .prologue
    .line 158
    monitor-enter p0

    :try_start_0
    sget-object v0, Ladz;->b:Ladz;

    iget-object v1, p0, Ladw;->a:Ladz;

    invoke-virtual {v0, v1}, Ladz;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 159
    invoke-virtual {p0}, Ladw;->c()V

    .line 160
    invoke-virtual {p0}, Ladw;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    monitor-exit p0

    return-void

    .line 158
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 251
    iget-object v0, p0, Ladw;->a:Lalo;

    iget-object v1, p0, Ladw;->b:LadY;

    invoke-interface {v0, v1}, Lalo;->a(LadY;)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized e()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 167
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Ladw;->a:LadE;

    invoke-interface {v2, p0}, LadE;->a(Ladw;)Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v1

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " has remaining references"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LbiT;->b(ZLjava/lang/Object;)V

    .line 168
    sget-object v2, Ladz;->c:Ladz;

    iget-object v3, p0, Ladw;->a:Ladz;

    .line 169
    invoke-virtual {v2, v3}, Ladz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Ladz;->b:Ladz;

    iget-object v3, p0, Ladw;->a:Ladz;

    invoke-virtual {v2, v3}, Ladz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 168
    :cond_1
    invoke-static {v0}, LbiT;->b(Z)V

    .line 170
    const-string v0, "FileContentInstance"

    const-string v1, "delete: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Ladw;->a:LaGp;

    invoke-virtual {v4}, LaGp;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 172
    sget-object v0, Ladz;->d:Ladz;

    iput-object v0, p0, Ladw;->a:Ladz;

    .line 174
    iget-object v0, p0, Ladw;->a:Ljava/io/File;

    if-eqz v0, :cond_2

    .line 175
    iget-object v0, p0, Ladw;->a:LadE;

    iget-object v1, p0, Ladw;->a:Ljava/io/File;

    invoke-interface {v0, v1}, LadE;->a(Ljava/io/File;)Z

    .line 177
    :cond_2
    iget-object v0, p0, Ladw;->a:LadE;

    iget-object v1, p0, Ladw;->a:LaGp;

    invoke-interface {v0, v1}, LadE;->b(LaGp;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    monitor-exit p0

    return-void

    :cond_3
    move v2, v0

    .line 167
    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Ladw;->a:LbsU;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 256
    const-string v0, "FileContentInstance[%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Ladw;->a:LaGp;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
