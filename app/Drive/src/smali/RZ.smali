.class public LRZ;
.super Ljava/lang/Object;
.source "StorageDisplayImpl.java"

# interfaces
.implements LRX;


# instance fields
.field private final a:LQr;

.field private final a:LaFO;

.field private final a:LajO;

.field private final a:Landroid/content/Context;

.field private final a:Landroid/view/View;

.field private final a:Landroid/widget/TextView;

.field private final a:LsC;

.field private final b:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/view/View;LQr;Landroid/content/Context;LaFO;LsC;LajO;)V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p2, p0, LRZ;->a:LQr;

    .line 77
    iput-object p3, p0, LRZ;->a:Landroid/content/Context;

    .line 78
    iput-object p4, p0, LRZ;->a:LaFO;

    .line 79
    iput-object p5, p0, LRZ;->a:LsC;

    .line 80
    iput-object p6, p0, LRZ;->a:LajO;

    .line 82
    sget v0, Lxc;->storage_summary_additional_info:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LRZ;->a:Landroid/widget/TextView;

    .line 83
    sget v0, Lxc;->storage_summary:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LRZ;->b:Landroid/widget/TextView;

    .line 84
    sget v0, Lxc;->storage_display:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LRZ;->a:Landroid/view/View;

    .line 85
    return-void
.end method

.method static synthetic a(LRZ;)LaFO;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LRZ;->a:LaFO;

    return-object v0
.end method

.method static synthetic a(LRZ;)LajO;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LRZ;->a:LajO;

    return-object v0
.end method

.method static synthetic a(LRZ;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LRZ;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(LRZ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LRZ;->a:Landroid/view/View;

    return-object v0
.end method

.method static synthetic a(LRZ;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LRZ;->a:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic b(LRZ;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LRZ;->b:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, LRZ;->a:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 167
    :goto_0
    return-void

    .line 135
    :cond_0
    new-instance v0, LSb;

    invoke-direct {v0, p0}, LSb;-><init>(LRZ;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 166
    invoke-virtual {v0, v1}, LSb;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 105
    iget-object v0, p0, LRZ;->a:Landroid/view/View;

    if-nez v0, :cond_0

    .line 127
    :goto_0
    return-void

    .line 108
    :cond_0
    iget-object v0, p0, LRZ;->a:LQr;

    const-string v1, "storageUpgradeUrl"

    const-string v2, "https://accounts.google.com/AddSession?&continue=https://www.google.com/settings/storage"

    .line 109
    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 111
    const-string v0, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LRZ;->a:LsC;

    .line 112
    invoke-interface {v0}, LsC;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 113
    :goto_1
    if-nez v0, :cond_2

    .line 114
    iget-object v0, p0, LRZ;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 112
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 116
    :cond_2
    iget-object v0, p0, LRZ;->a:Landroid/view/View;

    new-instance v2, LSa;

    invoke-direct {v2, p0, v1}, LSa;-><init>(LRZ;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
