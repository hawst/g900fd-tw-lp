.class public LalK;
.super Landroid/util/SparseArray;
.source "ImmutableSparseArray.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/util/SparseArray",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final a:LalK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LalK",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    new-instance v0, LalK;

    invoke-direct {v0}, LalK;-><init>()V

    sput-object v0, LalK;->a:LalK;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/util/SparseArray;-><init>(I)V

    .line 94
    return-void
.end method

.method synthetic constructor <init>(LalL;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, LalK;-><init>()V

    return-void
.end method

.method private constructor <init>(Landroid/util/SparseArray;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 97
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v0

    invoke-direct {p0, v0}, Landroid/util/SparseArray;-><init>(I)V

    .line 98
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v1

    .line 99
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 100
    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    invoke-super {p0, v2, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 99
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 102
    :cond_0
    return-void
.end method

.method public static a()LalK;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "LalK",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 21
    sget-object v0, LalK;->a:LalK;

    return-object v0
.end method

.method public static a(ILjava/lang/Object;)LalK;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(ITT;)",
            "LalK",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 35
    new-instance v0, LalM;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LalM;-><init>(LalL;)V

    invoke-virtual {v0, p0, p1}, LalM;->a(ILjava/lang/Object;)LalM;

    move-result-object v0

    invoke-virtual {v0}, LalM;->a()LalK;

    move-result-object v0

    return-object v0
.end method

.method public static a(ILjava/lang/Object;ILjava/lang/Object;)LalK;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(ITT;ITT;)",
            "LalK",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 42
    new-instance v0, LalM;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LalM;-><init>(LalL;)V

    invoke-virtual {v0, p0, p1}, LalM;->a(ILjava/lang/Object;)LalM;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, LalM;->a(ILjava/lang/Object;)LalM;

    move-result-object v0

    invoke-virtual {v0}, LalM;->a()LalK;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/util/SparseArray;)LalK;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/util/SparseArray",
            "<TT;>;)",
            "LalK",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 28
    new-instance v0, LalK;

    invoke-direct {v0, p0}, LalK;-><init>(Landroid/util/SparseArray;)V

    return-object v0
.end method

.method public static a()LalM;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "LalM",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 49
    new-instance v0, LalM;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LalM;-><init>(LalL;)V

    return-object v0
.end method

.method private a(ILjava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)V"
        }
    .end annotation

    .prologue
    .line 105
    invoke-super {p0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 106
    return-void
.end method

.method static synthetic a(LalK;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, LalK;->a(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public append(ILjava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)V"
        }
    .end annotation

    .prologue
    .line 110
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 115
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public delete(I)V
    .locals 1

    .prologue
    .line 120
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public put(ILjava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)V"
        }
    .end annotation

    .prologue
    .line 125
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public remove(I)V
    .locals 1

    .prologue
    .line 130
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public removeAt(I)V
    .locals 1

    .prologue
    .line 135
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public removeAtRange(II)V
    .locals 1

    .prologue
    .line 140
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setValueAt(ILjava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)V"
        }
    .end annotation

    .prologue
    .line 145
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
