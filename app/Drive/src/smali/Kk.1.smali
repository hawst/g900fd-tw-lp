.class public LKk;
.super LKQ;
.source "ActionCardFunctionViewFactory.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private final a:I

.field private final a:LKf;

.field private final a:Landroid/view/View$OnClickListener;

.field private final a:Ljava/lang/Object;

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(LKf;LLw;III)V
    .locals 7

    .prologue
    .line 51
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, LKk;-><init>(LKf;LLw;IIILjava/lang/Object;)V

    .line 53
    return-void
.end method

.method public constructor <init>(LKf;LLw;IIILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, LKQ;-><init>(LLw;Z)V

    .line 41
    new-instance v0, LKl;

    invoke-direct {v0, p0}, LKl;-><init>(LKk;)V

    iput-object v0, p0, LKk;->a:Landroid/view/View$OnClickListener;

    .line 60
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKf;

    iput-object v0, p0, LKk;->a:LKf;

    .line 61
    iput p3, p0, LKk;->a:I

    .line 62
    iput p4, p0, LKk;->c:I

    .line 63
    iput p5, p0, LKk;->b:I

    .line 64
    iput-object p6, p0, LKk;->a:Ljava/lang/Object;

    .line 65
    return-void
.end method

.method static synthetic a(LKk;)LKf;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, LKk;->a:LKf;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;Landroid/view/View$OnTouchListener;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 104
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 107
    const-string v0, "layout_inflater"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 110
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 111
    sget v3, Lxe;->action_card_button_template:I

    invoke-virtual {v0, v3, p1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 112
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 114
    sget v0, Lxc;->menu_icon:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 115
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    invoke-virtual {p0}, LKk;->b()Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 117
    iget v3, p0, LKk;->a:I

    invoke-virtual {v0, v4, v3, v4, v4}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 118
    iget v3, p0, LKk;->c:I

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(I)V

    .line 119
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p0, LKk;->b:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 121
    invoke-static {v1}, Lamt;->a(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 122
    invoke-virtual {v0}, Landroid/widget/Button;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v5

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 124
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, LwZ;->m_icon_secondary_tint:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 125
    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 128
    :cond_0
    invoke-virtual {v2, p2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 130
    invoke-virtual {p0}, LKk;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 131
    iget-object v0, p0, LKk;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    :cond_1
    invoke-static {v2}, Lanh;->a(Landroid/view/View;)V

    .line 135
    return-object v2
.end method

.method public a(Landroid/view/ViewGroup;Landroid/widget/ListPopupWindow;)Landroid/view/View;
    .locals 4

    .prologue
    .line 69
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 72
    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 75
    sget v1, Lxe;->action_card_item_template:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 77
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v2, p0, LKk;->b:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 78
    sget v0, Lxc;->menu_title:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 79
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    iget v3, p0, LKk;->c:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 81
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 83
    sget v0, Lxc;->menu_icon:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 84
    iget v3, p0, LKk;->a:I

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 85
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 87
    invoke-virtual {p0}, LKk;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    new-instance v0, LKm;

    invoke-direct {v0, p0, p2}, LKm;-><init>(LKk;Landroid/widget/ListPopupWindow;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    :goto_0
    return-object v1

    .line 96
    :cond_0
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method public a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, LKk;->a:Ljava/lang/Object;

    return-object v0
.end method
