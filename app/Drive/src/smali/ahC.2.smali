.class LahC;
.super Ljava/lang/Object;
.source "ContentSyncSchedulerImpl.java"

# interfaces
.implements LahB;
.implements LahT;


# annotations
.annotation runtime Lbxz;
.end annotation


# instance fields
.field private final a:LaGg;

.field private final a:LaHa;

.field private final a:Lagd;

.field private final a:LahK;

.field private final a:LahO;

.field private final a:LahP;

.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "Ladi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LaGg;LaHa;Laja;LahK;LahP;Lagd;LahO;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGg;",
            "LaHa;",
            "Laja",
            "<",
            "Ladi;",
            ">;",
            "LahK;",
            "LahP;",
            "Lagd;",
            "LahO;",
            ")V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, LahC;->a:LaGg;

    .line 62
    iput-object p2, p0, LahC;->a:LaHa;

    .line 63
    iput-object p3, p0, LahC;->a:Lbxw;

    .line 64
    iput-object p4, p0, LahC;->a:LahK;

    .line 65
    iput-object p5, p0, LahC;->a:LahP;

    .line 66
    iput-object p6, p0, LahC;->a:Lagd;

    .line 67
    iput-object p7, p0, LahC;->a:LahO;

    .line 68
    return-void
.end method

.method private a(Ljava/lang/Exception;)LNh;
    .locals 4

    .prologue
    .line 224
    const-string v0, "ContentSyncSchedulerImpl"

    const-string v1, "Failed to create document file: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 225
    new-instance v0, LNh;

    const-string v1, "Failed to create local document: "

    invoke-direct {v0, v1, p1}, LNh;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method private a(LMZ;)LaGv;
    .locals 6

    .prologue
    .line 204
    invoke-virtual {p1}, LMZ;->b()Ljava/lang/String;

    move-result-object v1

    .line 205
    invoke-virtual {p1}, LMZ;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 206
    iget-object v0, p0, LahC;->a:LahP;

    invoke-virtual {p1}, LMZ;->a()LaFO;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, LahP;->a(LaFO;Ljava/lang/String;)LaGv;

    move-result-object v0

    .line 207
    if-nez v0, :cond_0

    .line 208
    const-string v2, "ContentSyncSchedulerImpl"

    const-string v3, "Conversion requested but not applicable. Mime type: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 210
    :cond_0
    if-eqz v0, :cond_1

    .line 219
    :goto_0
    return-object v0

    .line 215
    :cond_1
    const-string v0, "application/pdf"

    .line 216
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 217
    sget-object v0, LaGv;->f:LaGv;

    goto :goto_0

    .line 219
    :cond_2
    sget-object v0, LaGv;->d:LaGv;

    goto :goto_0
.end method

.method private g(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, LahC;->a:LahK;

    invoke-interface {v0, p1}, LahK;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LahF;

    move-result-object v0

    .line 130
    if-eqz v0, :cond_0

    .line 131
    invoke-interface {v0}, LahF;->g()V

    .line 133
    :cond_0
    return-void
.end method

.method private h(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, LahC;->a:LahK;

    invoke-interface {v0, p1}, LahK;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LahF;

    move-result-object v0

    .line 137
    if-eqz v0, :cond_0

    .line 138
    invoke-interface {v0}, LahF;->h()V

    .line 140
    :cond_0
    iget-object v0, p0, LahC;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladi;

    invoke-interface {v0}, Ladi;->b()V

    .line 141
    return-void
.end method


# virtual methods
.method public a(LMZ;)LaGb;
    .locals 8

    .prologue
    .line 145
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    iget-object v0, p0, LahC;->a:LaGg;

    invoke-virtual {p1}, LMZ;->a()LaFO;

    move-result-object v1

    invoke-interface {v0, v1}, LaGg;->a(LaFO;)LaFM;

    move-result-object v1

    .line 150
    invoke-virtual {p1}, LMZ;->b()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 152
    if-nez v0, :cond_0

    .line 153
    new-instance v1, LNh;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No collection specified: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LNh;-><init>(Ljava/lang/String;)V

    throw v1

    .line 155
    :cond_0
    iget-object v2, p0, LahC;->a:LaGg;

    invoke-interface {v2, v0}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFZ;

    move-result-object v2

    .line 156
    if-nez v2, :cond_1

    .line 157
    new-instance v1, LNh;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Parent collection not found: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LNh;-><init>(Ljava/lang/String;)V

    throw v1

    .line 162
    :cond_1
    :try_start_0
    iget-object v0, p0, LahC;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladi;

    invoke-virtual {p1, v0}, LMZ;->a(Ladi;)Ladj;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LQm; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 167
    :try_start_1
    iget-object v0, p0, LahC;->a:LaGg;

    invoke-interface {v0}, LaGg;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 169
    :try_start_2
    invoke-interface {v3}, Ladj;->a()LaGp;

    move-result-object v0

    .line 170
    invoke-direct {p0, p1}, LahC;->a(LMZ;)LaGv;

    move-result-object v4

    invoke-virtual {v4}, LaGv;->a()Ljava/lang/String;

    move-result-object v4

    .line 171
    iget-object v5, p0, LahC;->a:LaGg;

    .line 172
    invoke-virtual {p1}, LMZ;->a()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, LahC;->a:LaHa;

    .line 171
    invoke-interface {v5, v1, v6, v4, v7}, LaGg;->a(LaFM;Ljava/lang/String;Ljava/lang/String;LaHa;)LaGb;

    move-result-object v1

    .line 173
    invoke-virtual {v1}, LaGb;->a()LaGc;

    move-result-object v1

    .line 175
    invoke-virtual {v0}, LaGp;->c()J

    move-result-wide v4

    sget-object v6, LacY;->a:LacY;

    invoke-virtual {v1, v4, v5, v6}, LaGc;->a(JLacY;)LaGc;

    move-result-object v4

    .line 176
    invoke-interface {v3}, Ladj;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, LaGc;->a(Ljava/lang/Long;)LaGc;

    move-result-object v4

    .line 177
    invoke-virtual {p1}, LMZ;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LaGc;->e(Ljava/lang/String;)LaGe;

    .line 178
    invoke-virtual {v1}, LaGc;->b()LaGb;

    move-result-object v4

    .line 180
    if-eqz v2, :cond_2

    .line 181
    iget-object v5, p0, LahC;->a:LaGg;

    .line 183
    invoke-virtual {v1}, LaGc;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v1

    .line 182
    invoke-interface {v5, v1, v2}, LaGg;->a(Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;LaFZ;)LaGB;

    move-result-object v1

    .line 184
    invoke-virtual {v1}, LaGB;->e()V

    .line 186
    :cond_2
    invoke-virtual {p0, v4, v0}, LahC;->b(LaGo;LaGp;)V

    .line 187
    iget-object v0, p0, LahC;->a:LaGg;

    invoke-interface {v0}, LaGg;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 189
    :try_start_3
    iget-object v0, p0, LahC;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 193
    :try_start_4
    invoke-interface {v3}, Ladj;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch LQm; {:try_start_4 .. :try_end_4} :catch_1

    return-object v4

    .line 189
    :catchall_0
    move-exception v0

    :try_start_5
    iget-object v1, p0, LahC;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 193
    :catchall_1
    move-exception v0

    :try_start_6
    invoke-interface {v3}, Ladj;->close()V

    throw v0
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catch LQm; {:try_start_6 .. :try_end_6} :catch_1

    .line 195
    :catch_0
    move-exception v0

    .line 196
    invoke-direct {p0, v0}, LahC;->a(Ljava/lang/Exception;)LNh;

    move-result-object v0

    throw v0

    .line 197
    :catch_1
    move-exception v0

    .line 198
    invoke-direct {p0, v0}, LahC;->a(Ljava/lang/Exception;)LNh;

    move-result-object v0

    throw v0
.end method

.method public a(LaGo;LaGp;)V
    .locals 4

    .prologue
    .line 252
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    iget-object v0, p0, LahC;->a:LaGg;

    invoke-interface {v0}, LaGg;->a()V

    .line 257
    :try_start_0
    invoke-virtual {p2}, LaGp;->a()V

    .line 258
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, LaGp;->a(Z)V

    .line 259
    invoke-virtual {p2}, LaGp;->e()V

    .line 261
    iget-object v0, p0, LahC;->a:LahO;

    .line 262
    invoke-interface {p1}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    sget-object v2, LaHf;->a:LaHf;

    const/4 v3, 0x1

    .line 261
    invoke-interface {v0, v1, v2, v3}, LahO;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaHf;Z)V

    .line 263
    iget-object v0, p0, LahC;->a:LaGg;

    invoke-interface {v0}, LaGg;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 265
    iget-object v0, p0, LahC;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V

    .line 268
    iget-object v0, p0, LahC;->a:Lagd;

    invoke-interface {v0}, Lagd;->a()V

    .line 269
    return-void

    .line 265
    :catchall_0
    move-exception v0

    iget-object v1, p0, LahC;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    throw v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1

    .prologue
    .line 91
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    iget-object v0, p0, LahC;->a:LahK;

    invoke-interface {v0, p1}, LahK;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LahF;

    move-result-object v0

    .line 93
    if-eqz v0, :cond_0

    .line 94
    invoke-interface {v0}, LahF;->e()V

    .line 96
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaGY;)V
    .locals 1

    .prologue
    .line 100
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    iget-object v0, p0, LahC;->a:LaGg;

    invoke-interface {v0, p1, p2}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaGY;)V

    .line 105
    invoke-virtual {p2}, LaGY;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    invoke-direct {p0, p1}, LahC;->g(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 110
    :goto_0
    return-void

    .line 108
    :cond_0
    invoke-direct {p0, p1}, LahC;->h(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaGZ;)V
    .locals 1

    .prologue
    .line 115
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    iget-object v0, p0, LahC;->a:LaGg;

    invoke-interface {v0, p1, p2}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaGZ;)Z

    move-result v0

    .line 119
    if-eqz v0, :cond_0

    .line 120
    invoke-virtual {p2}, LaGZ;->a()LaGY;

    move-result-object v0

    invoke-virtual {v0}, LaGY;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121
    invoke-direct {p0, p1}, LahC;->g(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    invoke-direct {p0, p1}, LahC;->h(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Z
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, LahC;->a:LahK;

    invoke-interface {v0, p1}, LahK;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LahF;

    move-result-object v0

    .line 231
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, LahF;->b()Z

    move-result v0

    goto :goto_0
.end method

.method public b(LaGo;LaGp;)V
    .locals 4

    .prologue
    .line 273
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    iget-object v0, p0, LahC;->a:LaGg;

    invoke-interface {v0}, LaGg;->a()V

    .line 278
    :try_start_0
    invoke-virtual {p2}, LaGp;->a()V

    .line 279
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, LaGp;->a(Z)V

    .line 280
    invoke-virtual {p2}, LaGp;->e()V

    .line 282
    iget-object v0, p0, LahC;->a:LahO;

    .line 283
    invoke-interface {p1}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    sget-object v2, LaHf;->a:LaHf;

    const/4 v3, 0x0

    .line 282
    invoke-interface {v0, v1, v2, v3}, LahO;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaHf;Z)V

    .line 284
    iget-object v0, p0, LahC;->a:LaGg;

    invoke-interface {v0}, LaGg;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 286
    iget-object v0, p0, LahC;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V

    .line 289
    iget-object v0, p0, LahC;->a:Lagd;

    invoke-interface {v0}, Lagd;->a()V

    .line 290
    return-void

    .line 286
    :catchall_0
    move-exception v0

    iget-object v1, p0, LahC;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    throw v0
.end method

.method public b(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1

    .prologue
    .line 72
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    iget-object v0, p0, LahC;->a:LahK;

    invoke-interface {v0, p1}, LahK;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LahF;

    move-result-object v0

    .line 74
    if-eqz v0, :cond_0

    .line 75
    invoke-interface {v0}, LahF;->f()V

    .line 77
    :cond_0
    return-void
.end method

.method public c(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1

    .prologue
    .line 81
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    iget-object v0, p0, LahC;->a:LahK;

    invoke-interface {v0, p1}, LahK;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LahF;

    move-result-object v0

    .line 83
    if-eqz v0, :cond_0

    .line 84
    invoke-interface {v0}, LahF;->m()V

    .line 86
    :cond_0
    iget-object v0, p0, LahC;->a:LahK;

    invoke-interface {v0}, LahK;->b()V

    .line 87
    return-void
.end method

.method public d(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1

    .prologue
    .line 294
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    iget-object v0, p0, LahC;->a:LahK;

    invoke-interface {v0, p1}, LahK;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LahF;

    move-result-object v0

    .line 297
    if-eqz v0, :cond_0

    .line 298
    invoke-interface {v0}, LahF;->k()V

    .line 300
    :cond_0
    return-void
.end method

.method public e(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 3

    .prologue
    .line 236
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    iget-object v0, p0, LahC;->a:LahO;

    sget-object v1, LaHf;->b:LaHf;

    const/4 v2, 0x1

    invoke-interface {v0, p1, v1, v2}, LahO;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaHf;Z)V

    .line 239
    iget-object v0, p0, LahC;->a:Lagd;

    invoke-interface {v0}, Lagd;->a()V

    .line 240
    return-void
.end method

.method public f(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 3

    .prologue
    .line 244
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    iget-object v0, p0, LahC;->a:LahO;

    sget-object v1, LaHf;->b:LaHf;

    const/4 v2, 0x0

    invoke-interface {v0, p1, v1, v2}, LahO;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LaHf;Z)V

    .line 247
    iget-object v0, p0, LahC;->a:Lagd;

    invoke-interface {v0}, Lagd;->a()V

    .line 248
    return-void
.end method
