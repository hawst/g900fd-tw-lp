.class public LRQ;
.super Ljava/lang/Object;
.source "SharingListElementComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LacD;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 90
    if-ne p1, p2, :cond_0

    .line 91
    const/4 v0, 0x0

    .line 101
    :goto_0
    return v0

    .line 94
    :cond_0
    if-nez p1, :cond_1

    .line 95
    const-string p1, ""

    .line 97
    :cond_1
    if-nez p2, :cond_2

    .line 98
    const-string p2, ""

    .line 101
    :cond_2
    invoke-virtual {p1, p2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private a(Lqv;Lqv;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 56
    if-ne p1, p2, :cond_1

    .line 66
    :cond_0
    :goto_0
    return v0

    .line 60
    :cond_1
    sget-object v1, Lqv;->a:Lqv;

    if-ne p1, v1, :cond_2

    .line 61
    const/4 v0, -0x1

    goto :goto_0

    .line 62
    :cond_2
    sget-object v1, Lqv;->a:Lqv;

    if-ne p2, v1, :cond_0

    .line 63
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Lqx;Lqx;)I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 70
    if-ne p1, p2, :cond_1

    .line 86
    :cond_0
    :goto_0
    return v0

    .line 74
    :cond_1
    sget-object v3, Lqx;->d:Lqx;

    if-ne p1, v3, :cond_2

    move v0, v1

    .line 75
    goto :goto_0

    .line 76
    :cond_2
    sget-object v3, Lqx;->d:Lqx;

    if-ne p2, v3, :cond_3

    move v0, v2

    .line 77
    goto :goto_0

    .line 80
    :cond_3
    sget-object v3, Lqx;->c:Lqx;

    if-ne p1, v3, :cond_4

    move v0, v1

    .line 81
    goto :goto_0

    .line 82
    :cond_4
    sget-object v1, Lqx;->c:Lqx;

    if-ne p2, v1, :cond_0

    move v0, v2

    .line 83
    goto :goto_0
.end method


# virtual methods
.method public a(LacD;LacD;)I
    .locals 2

    .prologue
    .line 28
    if-ne p1, p2, :cond_1

    .line 29
    const/4 v0, 0x0

    .line 50
    :cond_0
    :goto_0
    return v0

    .line 33
    :cond_1
    invoke-virtual {p1}, LacD;->a()Laci;

    move-result-object v0

    invoke-virtual {v0}, Laci;->a()Lqv;

    move-result-object v0

    invoke-virtual {p2}, LacD;->a()Laci;

    move-result-object v1

    invoke-virtual {v1}, Laci;->a()Lqv;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LRQ;->a(Lqv;Lqv;)I

    move-result v0

    .line 34
    if-nez v0, :cond_0

    .line 39
    invoke-virtual {p1}, LacD;->a()Laci;

    move-result-object v0

    invoke-virtual {v0}, Laci;->a()Lqx;

    move-result-object v0

    invoke-virtual {p2}, LacD;->a()Laci;

    move-result-object v1

    invoke-virtual {v1}, Laci;->a()Lqx;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LRQ;->a(Lqx;Lqx;)I

    move-result v0

    .line 40
    if-nez v0, :cond_0

    .line 44
    invoke-virtual {p1}, LacD;->a()LabD;

    move-result-object v0

    invoke-interface {v0}, LabD;->b()Ljava/lang/String;

    move-result-object v0

    .line 45
    invoke-virtual {p2}, LacD;->a()LabD;

    move-result-object v1

    invoke-interface {v1}, LabD;->b()Ljava/lang/String;

    move-result-object v1

    .line 44
    invoke-direct {p0, v0, v1}, LRQ;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 46
    if-nez v0, :cond_0

    .line 51
    invoke-virtual {p1}, LacD;->a()LabD;

    move-result-object v0

    invoke-interface {v0}, LabD;->a()Ljava/lang/String;

    move-result-object v0

    .line 52
    invoke-virtual {p2}, LacD;->a()LabD;

    move-result-object v1

    invoke-interface {v1}, LabD;->a()Ljava/lang/String;

    move-result-object v1

    .line 50
    invoke-direct {p0, v0, v1}, LRQ;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 24
    check-cast p1, LacD;

    check-cast p2, LacD;

    invoke-virtual {p0, p1, p2}, LRQ;->a(LacD;LacD;)I

    move-result v0

    return v0
.end method
