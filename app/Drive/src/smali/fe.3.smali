.class Lfe;
.super Ljava/lang/Object;
.source "ViewPropertyAnimatorCompat.java"

# interfaces
.implements Lfn;


# instance fields
.field final synthetic a:Lfd;


# direct methods
.method constructor <init>(Lfd;)V
    .locals 0

    .prologue
    .line 508
    iput-object p1, p0, Lfe;->a:Lfd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 511
    iget-object v0, p0, Lfe;->a:Lfd;

    iget-object v0, v0, Lfd;->e:Ljava/util/WeakHashMap;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfe;->a:Lfd;

    iget-object v0, v0, Lfd;->e:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 512
    :goto_0
    if-eqz v0, :cond_0

    .line 513
    const/4 v0, 0x2

    invoke-static {p1, v0, v1}, Lec;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 515
    :cond_0
    iget-object v0, p0, Lfe;->a:Lfd;

    iget-object v0, v0, Lfd;->c:Ljava/util/WeakHashMap;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lfe;->a:Lfd;

    iget-object v0, v0, Lfd;->c:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 516
    :goto_1
    if-eqz v0, :cond_1

    .line 517
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 518
    iget-object v0, p0, Lfe;->a:Lfd;

    iget-object v0, v0, Lfd;->c:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 520
    :cond_1
    iget-object v0, p0, Lfe;->a:Lfd;

    iget-object v0, v0, Lfd;->d:Ljava/util/WeakHashMap;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lfe;->a:Lfd;

    iget-object v0, v0, Lfd;->d:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfn;

    .line 522
    :goto_2
    if-eqz v0, :cond_2

    .line 523
    invoke-interface {v0, p1}, Lfn;->a(Landroid/view/View;)V

    .line 525
    :cond_2
    return-void

    :cond_3
    move-object v0, v1

    .line 511
    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 515
    goto :goto_1

    :cond_5
    move-object v0, v1

    .line 520
    goto :goto_2
.end method

.method public b(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 528
    iget-object v0, p0, Lfe;->a:Lfd;

    iget-object v0, v0, Lfd;->e:Ljava/util/WeakHashMap;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfe;->a:Lfd;

    iget-object v0, v0, Lfd;->e:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 529
    :goto_0
    if-eqz v0, :cond_0

    .line 530
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1, v0, v1}, Lec;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 531
    iget-object v0, p0, Lfe;->a:Lfd;

    iget-object v0, v0, Lfd;->e:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 533
    :cond_0
    iget-object v0, p0, Lfe;->a:Lfd;

    iget-object v0, v0, Lfd;->d:Ljava/util/WeakHashMap;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lfe;->a:Lfd;

    iget-object v0, v0, Lfd;->d:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfn;

    .line 535
    :goto_1
    if-eqz v0, :cond_1

    .line 536
    invoke-interface {v0, p1}, Lfn;->b(Landroid/view/View;)V

    .line 538
    :cond_1
    iget-object v0, p0, Lfe;->a:Lfd;

    iget-object v0, v0, Lfd;->b:Ljava/util/WeakHashMap;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lfe;->a:Lfd;

    iget-object v0, v0, Lfd;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 539
    :goto_2
    if-eqz v0, :cond_2

    .line 540
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 541
    iget-object v0, p0, Lfe;->a:Lfd;

    iget-object v0, v0, Lfd;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 543
    :cond_2
    return-void

    :cond_3
    move-object v0, v1

    .line 528
    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 533
    goto :goto_1

    :cond_5
    move-object v0, v1

    .line 538
    goto :goto_2
.end method

.method public c(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 547
    iget-object v0, p0, Lfe;->a:Lfd;

    iget-object v0, v0, Lfd;->d:Ljava/util/WeakHashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfe;->a:Lfd;

    iget-object v0, v0, Lfd;->d:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfn;

    .line 549
    :goto_0
    if-eqz v0, :cond_0

    .line 550
    invoke-interface {v0, p1}, Lfn;->c(Landroid/view/View;)V

    .line 552
    :cond_0
    return-void

    .line 547
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
