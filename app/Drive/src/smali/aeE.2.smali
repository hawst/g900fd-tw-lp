.class public LaeE;
.super Lbxb;
.source "GdataAccountMetadataEntry.java"

# interfaces
.implements LaJQ;


# instance fields
.field private a:I

.field private a:J

.field private final a:LaHK;

.field private a:LaJS;

.field private final a:LbpT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbpT",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LaGv;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private b:J

.field private final b:LbpT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbpT",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:J

.field private final c:LbpT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbpT",
            "<",
            "Ljava/lang/String;",
            "Lqt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 32
    invoke-direct {p0}, Lbxb;-><init>()V

    .line 33
    iput-wide v0, p0, LaeE;->a:J

    .line 34
    iput-wide v0, p0, LaeE;->b:J

    .line 35
    iput-wide v0, p0, LaeE;->c:J

    .line 37
    const/4 v0, 0x0

    iput v0, p0, LaeE;->a:I

    .line 38
    sget-object v0, LaJS;->a:LaJS;

    iput-object v0, p0, LaeE;->a:LaJS;

    .line 43
    const/4 v0, -0x1

    iput v0, p0, LaeE;->b:I

    .line 44
    invoke-static {}, Lbmq;->a()Lbmq;

    move-result-object v0

    iput-object v0, p0, LaeE;->a:LbpT;

    .line 45
    invoke-static {}, Lbmq;->a()Lbmq;

    move-result-object v0

    iput-object v0, p0, LaeE;->b:LbpT;

    .line 46
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LaeE;->a:Ljava/util/Set;

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LaeE;->a:Ljava/util/Map;

    .line 48
    new-instance v0, LaHM;

    invoke-direct {v0}, LaHM;-><init>()V

    iput-object v0, p0, LaeE;->a:LaHK;

    .line 49
    invoke-static {}, Lbmq;->a()Lbmq;

    move-result-object v0

    iput-object v0, p0, LaeE;->c:LbpT;

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 71
    iget-wide v0, p0, LaeE;->a:J

    return-wide v0
.end method

.method public a(LaGv;)J
    .locals 5

    .prologue
    .line 187
    iget-object v0, p0, LaeE;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 188
    if-nez v0, :cond_0

    .line 189
    const-string v0, "GdataAccountMetadataEntry"

    const-string v1, "Could not find max import size for file type: %s returning 0 bytes as a result."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 191
    invoke-virtual {p1}, LaGv;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 189
    invoke-static {v0, v1, v2}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 192
    const-wide/16 v0, 0x0

    .line 195
    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method

.method public a()LaHK;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, LaeE;->a:LaHK;

    invoke-static {v0}, LaHR;->a(LaHK;)LaHR;

    move-result-object v0

    return-object v0
.end method

.method public a()LaJS;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, LaeE;->a:LaJS;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 230
    invoke-virtual {p0}, LaeE;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 204
    iget-object v0, p0, LaeE;->a:Ljava/util/Set;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 156
    iget-object v0, p0, LaeE;->a:LbpT;

    invoke-interface {v0, p1}, LbpT;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 55
    invoke-super {p0}, Lbxb;->a()V

    .line 56
    iput-wide v0, p0, LaeE;->a:J

    .line 57
    iput-wide v0, p0, LaeE;->b:J

    .line 58
    iput-wide v0, p0, LaeE;->c:J

    .line 59
    const/4 v0, 0x0

    iput v0, p0, LaeE;->a:I

    .line 60
    const/4 v0, -0x1

    iput v0, p0, LaeE;->b:I

    .line 61
    sget-object v0, LaJS;->a:LaJS;

    iput-object v0, p0, LaeE;->a:LaJS;

    .line 62
    iget-object v0, p0, LaeE;->a:LbpT;

    invoke-interface {v0}, LbpT;->a()V

    .line 63
    iget-object v0, p0, LaeE;->b:LbpT;

    invoke-interface {v0}, LbpT;->a()V

    .line 64
    iget-object v0, p0, LaeE;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 65
    iget-object v0, p0, LaeE;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 66
    iget-object v0, p0, LaeE;->a:LaHK;

    invoke-interface {v0}, LaHK;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 67
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 117
    iput p1, p0, LaeE;->a:I

    .line 118
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 75
    iput-wide p1, p0, LaeE;->a:J

    .line 76
    return-void
.end method

.method public a(LaGv;J)V
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, LaeE;->a:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    return-void
.end method

.method public a(LaJS;)V
    .locals 1

    .prologue
    .line 101
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaJS;

    iput-object v0, p0, LaeE;->a:LaJS;

    .line 102
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, LaeE;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 200
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, LaeE;->a:LbpT;

    invoke-interface {v0, p1, p2}, LbpT;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 149
    return-void
.end method

.method public a(Ljava/lang/String;Lqt;)V
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, LaeE;->c:LbpT;

    invoke-interface {v0, p1}, LbpT;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    .line 219
    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 220
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 123
    iget v0, p0, LaeE;->b:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LaHJ;)Z
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, LaeE;->a:LaHK;

    invoke-interface {v0}, LaHK;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 80
    iget-wide v0, p0, LaeE;->b:J

    return-wide v0
.end method

.method public b(Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Lqt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 224
    iget-object v0, p0, LaeE;->c:LbpT;

    invoke-interface {v0, p1}, LbpT;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    .line 225
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 139
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 140
    iput p1, p0, LaeE;->b:I

    .line 141
    return-void

    .line 139
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 84
    iput-wide p1, p0, LaeE;->b:J

    .line 85
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, LaeE;->b:LbpT;

    invoke-interface {v0, p1, p2}, LbpT;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 165
    return-void
.end method

.method public c()J
    .locals 2

    .prologue
    .line 113
    iget v0, p0, LaeE;->a:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public c(J)V
    .locals 1

    .prologue
    .line 92
    iput-wide p1, p0, LaeE;->c:J

    .line 93
    return-void
.end method

.method public d()J
    .locals 2

    .prologue
    .line 134
    invoke-virtual {p0}, LaeE;->a()Z

    move-result v0

    invoke-static {v0}, LbiT;->b(Z)V

    .line 135
    iget v0, p0, LaeE;->b:I

    int-to-long v0, v0

    return-wide v0
.end method
