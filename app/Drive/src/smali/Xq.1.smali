.class public LXq;
.super LYs;
.source "PunchCompleteSvgViewer.java"


# instance fields
.field private a:I

.field private final a:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 127
    const-string v0, "PunchCompleteSvgViewer"

    sget-object v1, LYu;->b:LYu;

    invoke-direct {p0, v0, v1}, LYs;-><init>(Ljava/lang/String;LYu;)V

    .line 45
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LXq;->a:Ljava/util/Set;

    .line 51
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LXq;->a:Ljava/util/LinkedList;

    .line 58
    const/4 v0, 0x0

    iput v0, p0, LXq;->a:I

    .line 128
    return-void
.end method

.method static synthetic a(LXq;)I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, LXq;->a:I

    return v0
.end method

.method static synthetic a(LXq;I)I
    .locals 0

    .prologue
    .line 40
    iput p1, p0, LXq;->a:I

    return p1
.end method

.method static synthetic a(LXq;)Ljava/util/LinkedList;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, LXq;->a:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic a(LXq;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, LXq;->a:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method a()LdN;
    .locals 1

    .prologue
    .line 155
    new-instance v0, LXs;

    invoke-direct {v0, p0}, LXs;-><init>(LXq;)V

    return-object v0
.end method

.method a()LeT;
    .locals 1

    .prologue
    .line 164
    new-instance v0, LXr;

    invoke-direct {v0, p0}, LXr;-><init>(LXq;)V

    return-object v0
.end method

.method a()V
    .locals 2

    .prologue
    .line 211
    iget-object v0, p0, LXq;->a:Ljava/lang/String;

    const-string v1, "in updateSvgView"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    iget-object v0, p0, LXq;->a:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_0

    .line 213
    iget-object v0, p0, LXq;->a:Ljava/lang/String;

    const-string v1, "Early exit in updateSvgView as viewPager == null"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    :goto_0
    return-void

    .line 217
    :cond_0
    iget-object v0, p0, LXq;->a:LXD;

    invoke-virtual {v0}, LXD;->d()I

    move-result v0

    .line 218
    iput v0, p0, LXq;->a:I

    .line 220
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LXq;->a(IZ)V

    .line 221
    const/4 v0, 0x1

    iput-boolean v0, p0, LXq;->a:Z

    goto :goto_0
.end method

.method public a(ILcom/google/android/apps/docs/punchwebview/PunchSvgWebView;LXP;)V
    .locals 2

    .prologue
    .line 136
    sget-object v0, LXP;->b:LXP;

    if-ne p3, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 138
    invoke-virtual {p0, p1, p2, p3}, LXq;->a(ILcom/google/android/apps/docs/punchwebview/PunchSvgWebView;LXP;)LXB;

    move-result-object v0

    .line 139
    if-eqz v0, :cond_0

    .line 140
    iget-object v1, p0, LXq;->a:LXD;

    invoke-virtual {v1, p1, v0}, LXD;->a(ILXB;)V

    .line 142
    :cond_0
    return-void

    .line 136
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, LXq;->a:LXR;

    invoke-interface {v0}, LXR;->l_()V

    .line 151
    return-void
.end method

.method a(Z)V
    .locals 2

    .prologue
    .line 246
    if-eqz p1, :cond_0

    .line 247
    const/4 v0, 0x0

    iput-boolean v0, p0, LXq;->b:Z

    .line 248
    iget-object v0, p0, LXq;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    .line 249
    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->e()V

    goto :goto_0

    .line 254
    :cond_0
    iget-boolean v0, p0, LXq;->b:Z

    if-nez v0, :cond_1

    .line 255
    iget-object v0, p0, LXq;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    .line 256
    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->f()V

    goto :goto_1

    .line 259
    :cond_1
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 263
    const/4 v0, 0x0

    return v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, LXq;->a:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_0

    .line 242
    :goto_0
    return-void

    .line 230
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LXq;->a:Landroid/support/v4/view/ViewPager;

    .line 231
    iget-object v0, p0, LXq;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    .line 232
    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a()V

    goto :goto_1

    .line 235
    :cond_1
    iget-object v0, p0, LXq;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 237
    iget-object v0, p0, LXq;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    .line 238
    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a()V

    goto :goto_2

    .line 241
    :cond_2
    iget-object v0, p0, LXq;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    goto :goto_0
.end method
