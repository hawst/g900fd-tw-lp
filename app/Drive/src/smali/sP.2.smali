.class public LsP;
.super Ljava/lang/Object;
.source "DocumentOpenerActivity.java"


# instance fields
.field private final a:LaIm;

.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Laja;LaIm;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LaIm;",
            ")V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, LsP;->a:Lbxw;

    .line 68
    iput-object p2, p0, LsP;->a:LaIm;

    .line 69
    return-void
.end method


# virtual methods
.method public a(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 72
    sget-object v0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    .line 73
    invoke-virtual {v0, p2}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LaGv;->a:LaGv;

    .line 74
    invoke-interface {p1}, LaGu;->a()LaGv;

    move-result-object v1

    invoke-virtual {v0, v1}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 76
    :goto_0
    invoke-virtual {p0, p1, p2, v0}, LsP;->a(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 74
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Z)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 80
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    .line 83
    new-instance v2, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, LsP;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-class v3, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 85
    const-string v0, "entrySpec.v2"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 86
    const-string v0, "startNewDocCentricTask"

    invoke-virtual {v2, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 96
    invoke-interface {p1}, LaGu;->a()LaGv;

    move-result-object v0

    .line 97
    sget-object v3, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    sget-object v4, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->b:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    invoke-static {v3, v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v3

    .line 99
    invoke-interface {p1}, LaGu;->h()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 100
    invoke-interface {v3, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, LsP;->a:LaIm;

    .line 101
    invoke-interface {v3, v0}, LaIm;->a(LaGv;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    const-string v0, "android.intent.extra.STREAM"

    iget-object v3, p0, LsP;->a:LaIm;

    invoke-interface {v3, v1}, LaIm;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 105
    :cond_0
    invoke-static {v2, p2}, LFU;->a(Landroid/content/Intent;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V

    .line 106
    return-object v2
.end method
