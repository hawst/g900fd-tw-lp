.class public LEw;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "SwipablePanelLayout.java"


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, LEw;->a:Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;LEu;)V
    .locals 0

    .prologue
    .line 186
    invoke-direct {p0, p1}, LEw;-><init>(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;)V

    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2

    .prologue
    .line 198
    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 199
    iget-object v1, p0, LEw;->a:Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;

    invoke-static {v1}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;)I

    move-result v1

    int-to-float v1, v1

    cmpl-float v1, v0, v1

    if-lez v1, :cond_1

    iget-object v1, p0, LEw;->a:Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;

    invoke-static {v1}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->b(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;)I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 200
    iget-object v1, p0, LEw;->a:Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;

    const/4 v0, 0x0

    cmpl-float v0, p4, v0

    if-lez v0, :cond_0

    iget-object v0, p0, LEw;->a:Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->c(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;)I

    move-result v0

    :goto_0
    invoke-static {v1, v0}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;I)V

    .line 201
    const/4 v0, 0x1

    .line 203
    :goto_1
    return v0

    .line 200
    :cond_0
    iget-object v0, p0, LEw;->a:Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->c(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;)I

    move-result v0

    neg-int v0, v0

    goto :goto_0

    .line 203
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 190
    iget-object v0, p0, LEw;->a:Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;

    invoke-static {v0, v3}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;Z)Z

    .line 191
    iget-object v0, p0, LEw;->a:Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;

    iget-object v1, p0, LEw;->a:Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->getTranslationY()F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    add-float/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;F)F

    move-result v0

    .line 192
    iget-object v1, p0, LEw;->a:Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->setTranslationY(F)V

    .line 193
    return v3
.end method
