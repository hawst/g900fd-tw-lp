.class public LzS;
.super Ljava/lang/Object;
.source "BaseEntriesFilterCollection.java"

# interfaces
.implements LCo;


# static fields
.field private static final a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "LCl;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LbmL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmL",
            "<",
            "LCn;",
            "LCl;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<+",
            "LCl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    invoke-static {}, LbmF;->c()LbmF;

    move-result-object v0

    sput-object v0, LzS;->a:LbmF;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    sget-object v0, LzS;->a:LbmF;

    invoke-direct {p0, v0}, LzS;-><init>(LbmF;)V

    .line 24
    return-void
.end method

.method public constructor <init>(LbmF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<+",
            "LCl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbmF;

    iput-object v0, p0, LzS;->b:LbmF;

    .line 28
    invoke-direct {p0, p1}, LzS;->a(LbmF;)LbmL;

    move-result-object v0

    iput-object v0, p0, LzS;->a:LbmL;

    .line 29
    return-void
.end method

.method private a(LbmF;)LbmL;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<+",
            "LCl;",
            ">;)",
            "LbmL",
            "<",
            "LCn;",
            "LCl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 43
    invoke-virtual {p1}, LbmF;->a()Lbqv;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCl;

    .line 44
    invoke-interface {v0}, LCl;->a()LCn;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 46
    :cond_0
    invoke-static {v1}, LbmL;->a(Ljava/util/Map;)LbmL;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(LCn;)LCl;
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, LzS;->a:LbmL;

    invoke-virtual {v0, p1}, LbmL;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCl;

    .line 57
    if-nez v0, :cond_0

    .line 58
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No filter found with category : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_0
    return-object v0
.end method

.method public a()LbmF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmF",
            "<",
            "LCl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, LzS;->b:LbmF;

    invoke-static {v0}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    return-object v0
.end method

.method public a(LCn;)Z
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, LzS;->a:LbmL;

    invoke-virtual {v0, p1}, LbmL;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
