.class public LHe;
.super LHf;
.source "NativeOtherAppOpenerOption.java"


# instance fields
.field private final a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;

.field private final a:Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator$UriIntentBuilder;

.field private final a:Z


# direct methods
.method constructor <init>(LHh;ZLcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator$UriIntentBuilder;Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;)V
    .locals 2

    .prologue
    .line 45
    const-string v0, "NativeNonDriveApp"

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, LHf;-><init>(LHh;Ljava/lang/String;Z)V

    .line 46
    iput-boolean p2, p0, LHe;->a:Z

    .line 47
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator$UriIntentBuilder;

    iput-object v0, p0, LHe;->a:Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator$UriIntentBuilder;

    .line 48
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;

    iput-object v0, p0, LHe;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;

    .line 49
    return-void
.end method

.method static a(Landroid/content/Context;LtK;)LbmY;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LtK;",
            ")",
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    sget-object v0, Lry;->aK:Lry;

    invoke-interface {p1, v0}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.android.packageinstaller"

    .line 82
    invoke-static {v0, v1}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmY;

    move-result-object v0

    .line 88
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LbmY;->a()LbmY;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LtK;LaGo;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Landroid/content/Context;Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LtK;",
            "LaGo;",
            "Lcom/google/android/apps/docs/app/DocumentOpenMethod;",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;",
            "Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;",
            ")",
            "Ljava/util/List",
            "<",
            "LHe;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    move-object/from16 v0, p5

    invoke-interface {v0, p1}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;->a(LaGo;)Ljava/lang/String;

    move-result-object v1

    .line 114
    move-object/from16 v0, p4

    invoke-interface {v0, p2, v1, p1}, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;->a(Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;LaGo;)LGk;

    move-result-object v3

    .line 116
    invoke-interface {v3}, LGk;->a()Ljava/util/List;

    move-result-object v4

    .line 117
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 118
    invoke-interface {v3}, LGk;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    .line 119
    invoke-virtual {p3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 121
    invoke-static {p3, p0}, LHe;->a(Landroid/content/Context;LtK;)LbmY;

    move-result-object v8

    .line 122
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v6, :cond_2

    .line 123
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    .line 124
    invoke-interface {v3, v2}, LGk;->a(I)Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator$UriIntentBuilder;

    move-result-object v9

    .line 125
    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 126
    const-string v10, ""

    invoke-static {v7, v1, v10}, LHh;->a(Landroid/content/pm/PackageManager;Landroid/content/pm/ActivityInfo;Ljava/lang/String;)LHh;

    move-result-object v10

    .line 127
    if-eqz v10, :cond_0

    .line 128
    if-nez v1, :cond_1

    const/4 v1, 0x0

    .line 129
    :goto_1
    invoke-static {v7, v1, v8}, LHe;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;LbmY;)Z

    move-result v1

    .line 130
    new-instance v11, LHe;

    move-object/from16 v0, p5

    invoke-direct {v11, v10, v1, v9, v0}, LHe;-><init>(LHh;ZLcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator$UriIntentBuilder;Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;)V

    .line 132
    invoke-interface {v5, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 128
    :cond_1
    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    goto :goto_1

    .line 135
    :cond_2
    return-object v5
.end method

.method static a(Landroid/content/pm/PackageManager;Ljava/lang/String;LbmY;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/PackageManager;",
            "Ljava/lang/String;",
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 95
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    if-nez p1, :cond_0

    move v0, v1

    .line 105
    :goto_0
    return v0

    .line 100
    :cond_0
    invoke-virtual {p2}, LbmY;->a()Lbqv;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 101
    invoke-virtual {p0, p1, v0}, Landroid/content/pm/PackageManager;->checkSignatures(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_1

    .line 102
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 105
    goto :goto_0
.end method


# virtual methods
.method public a(LFT;LaGo;Landroid/os/Bundle;)LbsU;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LFT;",
            "LaGo;",
            "Landroid/os/Bundle;",
            ")",
            "LbsU",
            "<",
            "LDL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, p3}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 55
    const-string v1, "uriIntentBuilder"

    iget-object v2, p0, LHe;->a:Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator$UriIntentBuilder;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 56
    iget-object v1, p0, LHe;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;

    invoke-interface {v1, p1, p2, v0}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;->a(LFT;LaGo;Landroid/os/Bundle;)LbsU;

    move-result-object v0

    return-object v0
.end method

.method a(ZZ)Z
    .locals 1

    .prologue
    .line 62
    if-eqz p1, :cond_0

    if-nez p2, :cond_0

    iget-boolean v0, p0, LHe;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 63
    :goto_0
    return v0

    .line 62
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 68
    iget-boolean v0, p0, LHe;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "trusted"

    .line 69
    :goto_0
    const-string v1, "%s native app[%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    invoke-super {p0}, LHf;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 68
    :cond_0
    const-string v0, "untrusted"

    goto :goto_0
.end method
