.class Lapb;
.super LbjM;
.source "ReusableThumbnailCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LbjM",
        "<",
        "Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lapa;


# direct methods
.method constructor <init>(Lapa;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lapb;->a:Lapa;

    invoke-direct {p0}, LbjM;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 59
    .line 60
    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 59
    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 56
    check-cast p1, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    invoke-virtual {p0, p1}, Lapb;->a(Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
