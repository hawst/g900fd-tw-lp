.class public final enum LaKS;
.super Ljava/lang/Enum;
.source "Connectivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaKS;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaKS;

.field private static final synthetic a:[LaKS;

.field public static final enum b:LaKS;

.field public static final enum c:LaKS;


# instance fields
.field private final a:Ljava/lang/String;

.field private final a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 14
    new-instance v0, LaKS;

    const-string v1, "DISCONNECTED"

    const-string v2, "none"

    invoke-direct {v0, v1, v4, v4, v2}, LaKS;-><init>(Ljava/lang/String;IZLjava/lang/String;)V

    sput-object v0, LaKS;->a:LaKS;

    .line 15
    new-instance v0, LaKS;

    const-string v1, "WIFI"

    const-string v2, "wifi"

    invoke-direct {v0, v1, v3, v3, v2}, LaKS;-><init>(Ljava/lang/String;IZLjava/lang/String;)V

    sput-object v0, LaKS;->b:LaKS;

    .line 16
    new-instance v0, LaKS;

    const-string v1, "MOBILE"

    const-string v2, "mobile"

    invoke-direct {v0, v1, v5, v3, v2}, LaKS;-><init>(Ljava/lang/String;IZLjava/lang/String;)V

    sput-object v0, LaKS;->c:LaKS;

    .line 13
    const/4 v0, 0x3

    new-array v0, v0, [LaKS;

    sget-object v1, LaKS;->a:LaKS;

    aput-object v1, v0, v4

    sget-object v1, LaKS;->b:LaKS;

    aput-object v1, v0, v3

    sget-object v1, LaKS;->c:LaKS;

    aput-object v1, v0, v5

    sput-object v0, LaKS;->a:[LaKS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZLjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22
    iput-object p4, p0, LaKS;->a:Ljava/lang/String;

    .line 23
    iput-boolean p3, p0, LaKS;->a:Z

    .line 24
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaKS;
    .locals 1

    .prologue
    .line 13
    const-class v0, LaKS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaKS;

    return-object v0
.end method

.method public static values()[LaKS;
    .locals 1

    .prologue
    .line 13
    sget-object v0, LaKS;->a:[LaKS;

    invoke-virtual {v0}, [LaKS;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaKS;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, LaKS;->a:Z

    return v0
.end method
