.class public final enum LCn;
.super Ljava/lang/Enum;
.source "EntriesFilterCategory.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LCn;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LCn;

.field private static final synthetic a:[LCn;

.field public static final enum b:LCn;

.field public static final enum c:LCn;

.field public static final enum d:LCn;

.field public static final enum e:LCn;

.field public static final enum f:LCn;

.field public static final enum g:LCn;

.field public static final enum h:LCn;

.field public static final enum i:LCn;

.field public static final enum j:LCn;

.field public static final enum k:LCn;

.field public static final enum l:LCn;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 12
    new-instance v0, LCn;

    const-string v1, "HOME"

    invoke-direct {v0, v1, v3}, LCn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCn;->a:LCn;

    .line 14
    new-instance v0, LCn;

    const-string v1, "ALL_DOCUMENTS"

    invoke-direct {v0, v1, v4}, LCn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCn;->b:LCn;

    .line 16
    new-instance v0, LCn;

    const-string v1, "OWNED_BY_ME"

    invoke-direct {v0, v1, v5}, LCn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCn;->c:LCn;

    .line 18
    new-instance v0, LCn;

    const-string v1, "DOCUMENT_TYPE"

    invoke-direct {v0, v1, v6}, LCn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCn;->d:LCn;

    .line 20
    new-instance v0, LCn;

    const-string v1, "RECENT"

    invoke-direct {v0, v1, v7}, LCn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCn;->e:LCn;

    .line 22
    new-instance v0, LCn;

    const-string v1, "SHARED_WITH_ME"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LCn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCn;->f:LCn;

    .line 24
    new-instance v0, LCn;

    const-string v1, "STARRED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LCn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCn;->g:LCn;

    .line 26
    new-instance v0, LCn;

    const-string v1, "UPLOADS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LCn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCn;->h:LCn;

    .line 28
    new-instance v0, LCn;

    const-string v1, "PINNED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LCn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCn;->i:LCn;

    .line 30
    new-instance v0, LCn;

    const-string v1, "TRASH"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LCn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCn;->j:LCn;

    .line 32
    new-instance v0, LCn;

    const-string v1, "INCOMING_PHOTOS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LCn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCn;->k:LCn;

    .line 33
    new-instance v0, LCn;

    const-string v1, "NONE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LCn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCn;->l:LCn;

    .line 10
    const/16 v0, 0xc

    new-array v0, v0, [LCn;

    sget-object v1, LCn;->a:LCn;

    aput-object v1, v0, v3

    sget-object v1, LCn;->b:LCn;

    aput-object v1, v0, v4

    sget-object v1, LCn;->c:LCn;

    aput-object v1, v0, v5

    sget-object v1, LCn;->d:LCn;

    aput-object v1, v0, v6

    sget-object v1, LCn;->e:LCn;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LCn;->f:LCn;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LCn;->g:LCn;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LCn;->h:LCn;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LCn;->i:LCn;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LCn;->j:LCn;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LCn;->k:LCn;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LCn;->l:LCn;

    aput-object v2, v0, v1

    sput-object v0, LCn;->a:[LCn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LCn;
    .locals 1

    .prologue
    .line 10
    const-class v0, LCn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LCn;

    return-object v0
.end method

.method public static values()[LCn;
    .locals 1

    .prologue
    .line 10
    sget-object v0, LCn;->a:[LCn;

    invoke-virtual {v0}, [LCn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LCn;

    return-object v0
.end method
