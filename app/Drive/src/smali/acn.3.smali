.class Lacn;
.super LaGN;
.source "SharingHelper.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGN",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lacj;

.field final synthetic a:Ljava/lang/String;

.field final synthetic a:Ljava/util/List;


# direct methods
.method constructor <init>(Lacj;Ljava/util/List;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lacn;->a:Lacj;

    iput-object p2, p0, Lacn;->a:Ljava/util/List;

    iput-object p3, p0, Lacn;->a:Ljava/lang/String;

    invoke-direct {p0}, LaGN;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LaGM;)Ljava/lang/Boolean;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 322
    iget-object v0, p0, Lacn;->a:Lacj;

    iget-object v0, v0, Lacj;->a:Lacq;

    invoke-static {v0}, Lacq;->a(Lacq;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-interface {p1, v0}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGu;

    move-result-object v1

    .line 323
    iget-object v0, p0, Lacn;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, LbnG;->a(I)Ljava/util/ArrayList;

    move-result-object v3

    .line 324
    iget-object v0, p0, Lacn;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqo;

    .line 325
    iget-object v5, p0, Lacn;->a:Lacj;

    iget-object v5, v5, Lacj;->a:LabQ;

    invoke-virtual {v0}, Lqo;->a()Ljava/lang/String;

    move-result-object v6

    .line 326
    invoke-virtual {v0}, Lqo;->a()Lqt;

    move-result-object v0

    iget-object v7, p0, Lacn;->a:Ljava/lang/String;

    .line 325
    invoke-virtual {v5, v1, v6, v0, v7}, LabQ;->a(LaGu;Ljava/lang/String;Lqt;Ljava/lang/String;)LbsU;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 329
    :cond_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 330
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbsU;

    .line 331
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v0, v4}, Lalv;->a(Ljava/util/concurrent/Future;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_2
    or-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    move-object v1, v0

    .line 332
    goto :goto_1

    :cond_1
    move v0, v2

    .line 331
    goto :goto_2

    .line 333
    :cond_2
    return-object v1
.end method

.method public bridge synthetic a(LaGM;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 319
    invoke-virtual {p0, p1}, Lacn;->a(LaGM;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 338
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 339
    iget-object v0, p0, Lacn;->a:Lacj;

    invoke-static {v0}, Lacj;->a(Lacj;)Landroid/content/Context;

    move-result-object v0

    sget v1, Lxi;->sharing_list_updated:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 344
    :goto_0
    iget-object v0, p0, Lacn;->a:Lacj;

    invoke-virtual {v0}, Lacj;->c()V

    .line 345
    return-void

    .line 341
    :cond_0
    iget-object v0, p0, Lacn;->a:Lacj;

    .line 342
    invoke-static {v0}, Lacj;->a(Lacj;)Landroid/content/Context;

    move-result-object v0

    sget v1, Lxi;->sharing_message_unable_to_change:I

    .line 341
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 342
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 319
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lacn;->a(Ljava/lang/Boolean;)V

    return-void
.end method
