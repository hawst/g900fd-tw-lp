.class public final LNA;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LNt;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LNr;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LNm;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LNs;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 41
    iput-object p1, p0, LNA;->a:LbrA;

    .line 42
    const-class v0, LNt;

    invoke-static {v0, v1}, LNA;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LNA;->a:Lbsk;

    .line 45
    const-class v0, LNr;

    invoke-static {v0, v1}, LNA;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LNA;->b:Lbsk;

    .line 48
    const-class v0, LNm;

    invoke-static {v0, v1}, LNA;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LNA;->c:Lbsk;

    .line 51
    const-class v0, LNs;

    invoke-static {v0, v1}, LNA;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LNA;->d:Lbsk;

    .line 54
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 146
    sparse-switch p1, :sswitch_data_0

    .line 170
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :sswitch_0
    new-instance v0, LNt;

    invoke-direct {v0}, LNt;-><init>()V

    .line 150
    iget-object v1, p0, LNA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LNA;

    .line 151
    invoke-virtual {v1, v0}, LNA;->a(LNt;)V

    .line 168
    :goto_0
    return-object v0

    .line 154
    :sswitch_1
    new-instance v0, LNr;

    invoke-direct {v0}, LNr;-><init>()V

    .line 156
    iget-object v1, p0, LNA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LNA;

    .line 157
    invoke-virtual {v1, v0}, LNA;->a(LNr;)V

    goto :goto_0

    .line 160
    :sswitch_2
    new-instance v0, LNm;

    invoke-direct {v0}, LNm;-><init>()V

    goto :goto_0

    .line 164
    :sswitch_3
    new-instance v0, LNs;

    invoke-direct {v0}, LNs;-><init>()V

    .line 166
    iget-object v1, p0, LNA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LNA;

    .line 167
    invoke-virtual {v1, v0}, LNA;->a(LNs;)V

    goto :goto_0

    .line 146
    nop

    :sswitch_data_0
    .sparse-switch
        0x3d -> :sswitch_2
        0x117 -> :sswitch_1
        0x132 -> :sswitch_3
        0x139 -> :sswitch_0
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 203
    .line 205
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 117
    const-class v0, LNt;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x22

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LNA;->a(LbuP;LbuB;)V

    .line 120
    const-class v0, LNs;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x24

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LNA;->a(LbuP;LbuB;)V

    .line 123
    const-class v0, LNr;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x25

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LNA;->a(LbuP;LbuB;)V

    .line 126
    const-class v0, LNt;

    iget-object v1, p0, LNA;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LNA;->a(Ljava/lang/Class;Lbsk;)V

    .line 127
    const-class v0, LNr;

    iget-object v1, p0, LNA;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LNA;->a(Ljava/lang/Class;Lbsk;)V

    .line 128
    const-class v0, LNm;

    iget-object v1, p0, LNA;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LNA;->a(Ljava/lang/Class;Lbsk;)V

    .line 129
    const-class v0, LNs;

    iget-object v1, p0, LNA;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LNA;->a(Ljava/lang/Class;Lbsk;)V

    .line 130
    iget-object v0, p0, LNA;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x139

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 132
    iget-object v0, p0, LNA;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x117

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 134
    iget-object v0, p0, LNA;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x3d

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 136
    iget-object v0, p0, LNA;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x132

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 138
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 177
    packed-switch p1, :pswitch_data_0

    .line 197
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 179
    :pswitch_1
    check-cast p2, LNt;

    .line 181
    iget-object v0, p0, LNA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LNA;

    .line 182
    invoke-virtual {v0, p2}, LNA;->a(LNt;)V

    .line 199
    :goto_0
    return-void

    .line 185
    :pswitch_2
    check-cast p2, LNs;

    .line 187
    iget-object v0, p0, LNA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LNA;

    .line 188
    invoke-virtual {v0, p2}, LNA;->a(LNs;)V

    goto :goto_0

    .line 191
    :pswitch_3
    check-cast p2, LNr;

    .line 193
    iget-object v0, p0, LNA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LNA;

    .line 194
    invoke-virtual {v0, p2}, LNA;->a(LNr;)V

    goto :goto_0

    .line 177
    :pswitch_data_0
    .packed-switch 0x22
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(LNr;)V
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, LNA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 94
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LNA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 92
    invoke-static {v0, v1}, LNA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p1, LNr;->a:LtK;

    .line 98
    iget-object v0, p0, LNA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->t:Lbsk;

    .line 101
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LNA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->t:Lbsk;

    .line 99
    invoke-static {v0, v1}, LNA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, LNr;->a:Laja;

    .line 105
    iget-object v0, p0, LNA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->W:Lbsk;

    .line 108
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LNA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->W:Lbsk;

    .line 106
    invoke-static {v0, v1}, LNA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, LNr;->b:Laja;

    .line 112
    return-void
.end method

.method public a(LNs;)V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, LNA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->ah:Lbsk;

    .line 69
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LNA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->ah:Lbsk;

    .line 67
    invoke-static {v0, v1}, LNA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, LNs;->b:Laja;

    .line 73
    iget-object v0, p0, LNA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lajo;

    iget-object v0, v0, Lajo;->t:Lbsk;

    .line 76
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LNA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lajo;

    iget-object v1, v1, Lajo;->t:Lbsk;

    .line 74
    invoke-static {v0, v1}, LNA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, p1, LNs;->a:Laja;

    .line 80
    iget-object v0, p0, LNA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LpG;

    iget-object v0, v0, LpG;->m:Lbsk;

    .line 83
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LNA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LpG;

    iget-object v1, v1, LpG;->m:Lbsk;

    .line 81
    invoke-static {v0, v1}, LNA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p1, LNs;->a:LtK;

    .line 87
    return-void
.end method

.method public a(LNt;)V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, LNA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTz;

    .line 61
    invoke-virtual {v0, p1}, LTz;->a(LTu;)V

    .line 62
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 142
    return-void
.end method
