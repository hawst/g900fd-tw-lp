.class final enum LBd;
.super Ljava/lang/Enum;
.source "DocListEmptyViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LBd;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LBd;

.field private static final synthetic a:[LBd;

.field public static final enum b:LBd;

.field public static final enum c:LBd;

.field public static final enum d:LBd;

.field public static final enum e:LBd;

.field public static final enum f:LBd;

.field public static final enum g:LBd;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 36
    new-instance v0, LBd;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, LBd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LBd;->a:LBd;

    new-instance v0, LBd;

    const-string v1, "SYNCING"

    invoke-direct {v0, v1, v4}, LBd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LBd;->b:LBd;

    new-instance v0, LBd;

    const-string v1, "ONE_DOCUMENT"

    invoke-direct {v0, v1, v5}, LBd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LBd;->c:LBd;

    new-instance v0, LBd;

    const-string v1, "EMPTY_LOADED"

    invoke-direct {v0, v1, v6}, LBd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LBd;->d:LBd;

    new-instance v0, LBd;

    const-string v1, "EMPTY_RECENT"

    invoke-direct {v0, v1, v7}, LBd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LBd;->e:LBd;

    new-instance v0, LBd;

    const-string v1, "EMPTY_HAS_MORE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LBd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LBd;->f:LBd;

    new-instance v0, LBd;

    const-string v1, "EMPTY_PENDING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LBd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LBd;->g:LBd;

    .line 35
    const/4 v0, 0x7

    new-array v0, v0, [LBd;

    sget-object v1, LBd;->a:LBd;

    aput-object v1, v0, v3

    sget-object v1, LBd;->b:LBd;

    aput-object v1, v0, v4

    sget-object v1, LBd;->c:LBd;

    aput-object v1, v0, v5

    sget-object v1, LBd;->d:LBd;

    aput-object v1, v0, v6

    sget-object v1, LBd;->e:LBd;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LBd;->f:LBd;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LBd;->g:LBd;

    aput-object v2, v0, v1

    sput-object v0, LBd;->a:[LBd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LBd;
    .locals 1

    .prologue
    .line 35
    const-class v0, LBd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LBd;

    return-object v0
.end method

.method public static values()[LBd;
    .locals 1

    .prologue
    .line 35
    sget-object v0, LBd;->a:[LBd;

    invoke-virtual {v0}, [LBd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LBd;

    return-object v0
.end method
