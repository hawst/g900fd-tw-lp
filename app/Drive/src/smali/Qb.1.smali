.class public final LQb;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 37
    iput-object p1, p0, LQb;->a:LbrA;

    .line 38
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 132
    .line 134
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 155
    .line 157
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 121
    const-class v0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;

    invoke-static {v0}, LbuP;->b(Ljava/lang/Class;)LbuP;

    move-result-object v0

    new-instance v1, Lbsh;

    const/16 v2, 0x18

    invoke-direct {v1, v2, p0}, Lbsh;-><init>(ILbse;)V

    invoke-virtual {p0, v0, v1}, LQb;->a(LbuP;LbuB;)V

    .line 124
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 141
    packed-switch p1, :pswitch_data_0

    .line 149
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 143
    :pswitch_0
    check-cast p2, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;

    .line 145
    iget-object v0, p0, LQb;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQb;

    .line 146
    invoke-virtual {v0, p2}, LQb;->a(Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;)V

    .line 151
    return-void

    .line 141
    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, LQb;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LtQ;

    .line 45
    invoke-virtual {v0, p1}, LtQ;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 46
    iget-object v0, p0, LQb;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LCw;

    iget-object v0, v0, LCw;->X:Lbsk;

    .line 49
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LQb;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LCw;

    iget-object v1, v1, LCw;->X:Lbsk;

    .line 47
    invoke-static {v0, v1}, LQb;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LsI;

    iput-object v0, p1, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->b:LsI;

    .line 53
    iget-object v0, p0, LQb;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LapK;

    iget-object v0, v0, LapK;->f:Lbsk;

    .line 56
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LQb;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LapK;

    iget-object v1, v1, LapK;->f:Lbsk;

    .line 54
    invoke-static {v0, v1}, LQb;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapn;

    iput-object v0, p1, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lapn;

    .line 60
    iget-object v0, p0, LQb;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->j:Lbsk;

    .line 63
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LQb;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->j:Lbsk;

    .line 61
    invoke-static {v0, v1}, LQb;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGg;

    iput-object v0, p1, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LaGg;

    .line 67
    iget-object v0, p0, LQb;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lwc;

    iget-object v0, v0, Lwc;->h:Lbsk;

    .line 70
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LQb;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lwc;

    iget-object v1, v1, Lwc;->h:Lbsk;

    .line 68
    invoke-static {v0, v1}, LQb;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwa;

    iput-object v0, p1, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lwa;

    .line 74
    iget-object v0, p0, LQb;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->f:Lbsk;

    .line 77
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LQb;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->f:Lbsk;

    .line 75
    invoke-static {v0, v1}, LQb;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGR;

    iput-object v0, p1, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LaGR;

    .line 81
    iget-object v0, p0, LQb;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->L:Lbsk;

    .line 84
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LQb;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->L:Lbsk;

    .line 82
    invoke-static {v0, v1}, LQb;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaKR;

    iput-object v0, p1, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LaKR;

    .line 88
    iget-object v0, p0, LQb;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 91
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LQb;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 89
    invoke-static {v0, v1}, LQb;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p1, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LQr;

    .line 95
    iget-object v0, p0, LQb;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LSK;

    iget-object v0, v0, LSK;->b:Lbsk;

    .line 98
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LQb;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LSK;

    iget-object v1, v1, LSK;->b:Lbsk;

    .line 96
    invoke-static {v0, v1}, LQb;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSF;

    iput-object v0, p1, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LSF;

    .line 102
    iget-object v0, p0, LQb;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LagN;

    iget-object v0, v0, LagN;->I:Lbsk;

    .line 105
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LQb;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LagN;

    iget-object v1, v1, LagN;->I:Lbsk;

    .line 103
    invoke-static {v0, v1}, LQb;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafz;

    iput-object v0, p1, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lafz;

    .line 109
    iget-object v0, p0, LQb;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lwc;

    iget-object v0, v0, Lwc;->i:Lbsk;

    .line 112
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LQb;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lwc;

    iget-object v1, v1, Lwc;->i:Lbsk;

    .line 110
    invoke-static {v0, v1}, LQb;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvL;

    iput-object v0, p1, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LvL;

    .line 116
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 128
    return-void
.end method
