.class LJO;
.super Ljava/lang/Object;
.source "DocListMenuImpl.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic a:LJJ;

.field final synthetic a:LJN;


# direct methods
.method constructor <init>(LJN;ILJJ;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, LJO;->a:LJN;

    iput p2, p0, LJO;->a:I

    iput-object p3, p0, LJO;->a:LJJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 107
    iget v0, p0, LJO;->a:I

    sget v1, Lxc;->menu_refresh_icon:I

    if-ne v0, v1, :cond_1

    .line 108
    iget-object v0, p0, LJO;->a:LJJ;

    invoke-interface {v0}, LJJ;->a()V

    .line 139
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 109
    :cond_1
    iget v0, p0, LJO;->a:I

    sget v1, Lxc;->menu_create_new_doc:I

    if-ne v0, v1, :cond_2

    .line 110
    iget-object v0, p0, LJO;->a:LJN;

    iget-object v0, v0, LJN;->a:LFF;

    invoke-interface {v0}, LFF;->a()V

    goto :goto_0

    .line 111
    :cond_2
    iget v0, p0, LJO;->a:I

    sget v1, Lxc;->menu_filter_by:I

    if-ne v0, v1, :cond_3

    .line 112
    iget-object v0, p0, LJO;->a:LJJ;

    invoke-interface {v0}, LJJ;->c()V

    goto :goto_0

    .line 113
    :cond_3
    iget v0, p0, LJO;->a:I

    sget v1, Lxc;->menu_sortings:I

    if-ne v0, v1, :cond_4

    .line 114
    iget-object v0, p0, LJO;->a:LJJ;

    invoke-interface {v0}, LJJ;->b()V

    goto :goto_0

    .line 115
    :cond_4
    iget v0, p0, LJO;->a:I

    sget v1, Lxc;->menu_settings:I

    if-ne v0, v1, :cond_5

    .line 116
    iget-object v0, p0, LJO;->a:LJN;

    iget-object v0, v0, LJN;->a:LJK;

    invoke-interface {v0}, LJK;->b()V

    goto :goto_0

    .line 117
    :cond_5
    iget v0, p0, LJO;->a:I

    sget v1, Lxc;->menu_list_mode:I

    if-ne v0, v1, :cond_6

    .line 118
    iget-object v0, p0, LJO;->a:LJJ;

    invoke-interface {v0}, LJJ;->d()V

    goto :goto_0

    .line 119
    :cond_6
    iget v0, p0, LJO;->a:I

    sget v1, Lxc;->menu_grid_mode:I

    if-ne v0, v1, :cond_7

    .line 120
    iget-object v0, p0, LJO;->a:LJJ;

    invoke-interface {v0}, LJJ;->e()V

    goto :goto_0

    .line 121
    :cond_7
    iget v0, p0, LJO;->a:I

    sget v1, Lxc;->menu_help:I

    if-ne v0, v1, :cond_8

    .line 122
    iget-object v0, p0, LJO;->a:LJN;

    iget-object v0, v0, LJN;->a:LJK;

    invoke-interface {v0}, LJK;->c()V

    goto :goto_0

    .line 123
    :cond_8
    iget v0, p0, LJO;->a:I

    sget v1, Lxc;->menu_send_feedback:I

    if-ne v0, v1, :cond_9

    .line 124
    iget-object v0, p0, LJO;->a:LJN;

    iget-object v0, v0, LJN;->a:LJK;

    invoke-interface {v0}, LJK;->d()V

    goto :goto_0

    .line 125
    :cond_9
    iget v0, p0, LJO;->a:I

    sget v1, Lxc;->menu_open_with_picker:I

    if-ne v0, v1, :cond_a

    .line 126
    iget-object v0, p0, LJO;->a:LJN;

    iget-object v0, v0, LJN;->a:LJK;

    invoke-interface {v0}, LJK;->a()V

    goto :goto_0

    .line 127
    :cond_a
    iget v0, p0, LJO;->a:I

    sget v1, Lxc;->menu_selection_start:I

    if-ne v0, v1, :cond_b

    .line 128
    iget-object v0, p0, LJO;->a:LJJ;

    invoke-interface {v0}, LJJ;->g()V

    goto/16 :goto_0

    .line 129
    :cond_b
    iget v0, p0, LJO;->a:I

    sget v1, Lxc;->menu_selection_clear:I

    if-ne v0, v1, :cond_c

    .line 130
    iget-object v0, p0, LJO;->a:LJJ;

    invoke-interface {v0}, LJJ;->f()V

    goto/16 :goto_0

    .line 131
    :cond_c
    iget v0, p0, LJO;->a:I

    sget v1, Lxc;->menu_selection_all:I

    if-ne v0, v1, :cond_d

    .line 132
    iget-object v0, p0, LJO;->a:LJJ;

    invoke-interface {v0}, LJJ;->h()V

    goto/16 :goto_0

    .line 133
    :cond_d
    iget v0, p0, LJO;->a:I

    sget v1, Lxc;->menu_add_new_office_doc:I

    if-ne v0, v1, :cond_0

    .line 134
    iget-object v0, p0, LJO;->a:LJN;

    iget-object v0, v0, LJN;->a:Lve;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, LJO;->a:LJN;

    iget-object v0, v0, LJN;->a:Lve;

    iget-object v1, p0, LJO;->a:LJN;

    iget-object v1, v1, LJN;->a:Landroid/content/Context;

    iget-object v2, p0, LJO;->a:LJN;

    iget-object v2, v2, LJN;->a:LaFO;

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lve;->a(Landroid/content/Context;LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    goto/16 :goto_0
.end method
