.class LaxL;
.super Laxy;
.source "PdfPageLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Laxy",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LaxI;

.field final a:Lcom/google/android/apps/viewer/client/Dimensions;


# direct methods
.method constructor <init>(LaxI;Lcom/google/android/apps/viewer/client/Dimensions;)V
    .locals 2

    .prologue
    .line 213
    iput-object p1, p0, LaxL;->a:LaxI;

    .line 214
    invoke-static {p1}, LaxI;->a(LaxI;)Laxz;

    move-result-object v0

    sget-object v1, LaxR;->c:LaxR;

    invoke-direct {p0, v0, v1}, Laxy;-><init>(Laxz;LaxR;)V

    .line 215
    iput-object p2, p0, LaxL;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    .line 216
    return-void
.end method


# virtual methods
.method protected a(LavE;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 220
    iget-object v0, p0, LaxL;->a:LaxI;

    invoke-static {v0}, LaxI;->a(LaxI;)Laxz;

    move-result-object v0

    iget-object v0, v0, Laxz;->a:LavU;

    iget-object v1, p0, LaxL;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    invoke-virtual {v0, v1}, LavU;->a(Lcom/google/android/apps/viewer/client/Dimensions;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 221
    if-eqz v0, :cond_0

    .line 223
    :try_start_0
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    .line 224
    const/4 v2, 0x0

    aget-object v2, v1, v2

    .line 225
    const/4 v3, 0x1

    aget-object v1, v1, v3

    .line 226
    invoke-static {v0, v2}, Lcom/google/android/apps/viewer/util/ParcelUtils;->a(Landroid/graphics/Bitmap;Landroid/os/ParcelFileDescriptor;)V

    .line 228
    iget-object v2, p0, LaxL;->a:LaxI;

    invoke-static {v2}, LaxI;->a(LaxI;)I

    move-result v2

    iget-object v3, p0, LaxL;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    invoke-interface {p1, v2, v3, v1}, LavE;->a(ILcom/google/android/apps/viewer/client/Dimensions;Landroid/os/ParcelFileDescriptor;)Z

    .line 229
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 235
    :cond_0
    :goto_0
    return-object v0

    .line 230
    :catch_0
    move-exception v0

    .line 231
    const-string v1, "PdfPageLoader"

    const-string v2, "i/o error while creating pipe"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 232
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic a(LavE;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 209
    invoke-virtual {p0, p1}, LaxL;->a(LavE;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected a(LaxH;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 239
    if-eqz p2, :cond_0

    .line 240
    iget-object v0, p0, LaxL;->a:LaxI;

    invoke-static {v0}, LaxI;->a(LaxI;)I

    move-result v0

    invoke-interface {p1, v0, p2}, LaxH;->a(ILandroid/graphics/Bitmap;)V

    .line 242
    :cond_0
    iget-object v0, p0, LaxL;->a:LaxI;

    const/4 v1, 0x0

    iput-object v1, v0, LaxI;->a:LaxL;

    .line 243
    return-void
.end method

.method protected bridge synthetic a(LaxH;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 209
    check-cast p2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2}, LaxL;->a(LaxH;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 246
    const-string v0, "RenderBitmapTask(page=%d width=%d height=%d)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LaxL;->a:LaxI;

    .line 247
    invoke-static {v3}, LaxI;->a(LaxI;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LaxL;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v3, v3, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, LaxL;->a:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v3, v3, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 246
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
