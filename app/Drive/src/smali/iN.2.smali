.class public LiN;
.super Ljava/lang/Object;
.source "SupportActionModeWrapper.java"

# interfaces
.implements Lmv;


# instance fields
.field final a:Landroid/content/Context;

.field final a:Landroid/view/ActionMode$Callback;

.field final a:LcP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LcP",
            "<",
            "Lmu;",
            "LiM;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ActionMode$Callback;)V
    .locals 1

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    iput-object p1, p0, LiN;->a:Landroid/content/Context;

    .line 145
    iput-object p2, p0, LiN;->a:Landroid/view/ActionMode$Callback;

    .line 146
    new-instance v0, LcP;

    invoke-direct {v0}, LcP;-><init>()V

    iput-object v0, p0, LiN;->a:LcP;

    .line 147
    return-void
.end method

.method private a(Lmu;)Landroid/view/ActionMode;
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, LiN;->a:LcP;

    invoke-virtual {v0, p1}, LcP;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiM;

    .line 176
    if-eqz v0, :cond_0

    .line 184
    :goto_0
    return-object v0

    .line 182
    :cond_0
    new-instance v0, LiM;

    iget-object v1, p0, LiN;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, LiM;-><init>(Landroid/content/Context;Lmu;)V

    .line 183
    iget-object v1, p0, LiN;->a:LcP;

    invoke-virtual {v1, p1, v0}, LcP;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public a(Lmu;)V
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, LiN;->a:Landroid/view/ActionMode$Callback;

    invoke-direct {p0, p1}, LiN;->a(Lmu;)Landroid/view/ActionMode;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/ActionMode$Callback;->onDestroyActionMode(Landroid/view/ActionMode;)V

    .line 171
    return-void
.end method

.method public a(Lmu;Landroid/view/Menu;)Z
    .locals 3

    .prologue
    .line 151
    iget-object v0, p0, LiN;->a:Landroid/view/ActionMode$Callback;

    invoke-direct {p0, p1}, LiN;->a(Lmu;)Landroid/view/ActionMode;

    move-result-object v1

    invoke-static {p2}, Lju;->a(Landroid/view/Menu;)Landroid/view/Menu;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/view/ActionMode$Callback;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public a(Lmu;Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 164
    iget-object v0, p0, LiN;->a:Landroid/view/ActionMode$Callback;

    invoke-direct {p0, p1}, LiN;->a(Lmu;)Landroid/view/ActionMode;

    move-result-object v1

    invoke-static {p2}, Lju;->a(Landroid/view/MenuItem;)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/view/ActionMode$Callback;->onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public b(Lmu;Landroid/view/Menu;)Z
    .locals 3

    .prologue
    .line 157
    iget-object v0, p0, LiN;->a:Landroid/view/ActionMode$Callback;

    invoke-direct {p0, p1}, LiN;->a(Lmu;)Landroid/view/ActionMode;

    move-result-object v1

    invoke-static {p2}, Lju;->a(Landroid/view/Menu;)Landroid/view/Menu;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/view/ActionMode$Callback;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method
