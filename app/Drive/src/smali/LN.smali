.class LLN;
.super Ljava/lang/Object;
.source "SelectionFunctionDoStar.java"

# interfaces
.implements LLw;


# instance fields
.field private final a:LVg;


# direct methods
.method public constructor <init>(LVg;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, LLN;->a:LVg;

    .line 23
    return-void
.end method


# virtual methods
.method public a(LaFM;LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaFM;",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, LLN;->a:LVg;

    .line 40
    invoke-virtual {p1}, LaFM;->a()LaFO;

    move-result-object v1

    invoke-virtual {v0, v1}, LVg;->a(LaFO;)LVf;

    move-result-object v1

    .line 41
    invoke-virtual {p2}, LbmF;->a()Lbqv;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    .line 42
    invoke-interface {v0}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-virtual {v1, v0}, LVf;->c(Lcom/google/android/gms/drive/database/data/EntrySpec;)LVf;

    goto :goto_0

    .line 44
    :cond_0
    iget-object v0, p0, LLN;->a:LVg;

    invoke-virtual {v1}, LVf;->a()LVd;

    move-result-object v1

    invoke-virtual {v0, v1}, LVg;->a(LVd;)V

    .line 45
    return-void
.end method

.method public a(Ljava/lang/Runnable;LaFM;LbmF;LbmF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            "LaFM;",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 51
    return-void
.end method

.method public a(LbmF;Lcom/google/android/apps/docs/doclist/SelectionItem;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LaGu;",
            ">;",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 28
    invoke-virtual {p1}, LbmF;->a()Lbqv;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGu;

    .line 29
    invoke-interface {v0}, LaGu;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 30
    const/4 v0, 0x1

    .line 33
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
