.class public final LbqS;
.super Ljava/lang/Object;
.source "Hashing.java"


# static fields
.field private static final a:I

.field private static final a:LbqQ;

.field private static final b:LbqQ;

.field private static final c:LbqQ;

.field private static final d:LbqQ;

.field private static final e:LbqQ;

.field private static final f:LbqQ;

.field private static final g:LbqQ;

.field private static final h:LbqQ;

.field private static final i:LbqQ;

.field private static final j:LbqQ;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 47
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, LbqS;->a:I

    .line 50
    sget v0, LbqS;->a:I

    invoke-static {v0}, LbqS;->a(I)LbqQ;

    move-result-object v0

    sput-object v0, LbqS;->a:LbqQ;

    .line 53
    sget v0, LbqS;->a:I

    invoke-static {v0}, LbqS;->b(I)LbqQ;

    move-result-object v0

    sput-object v0, LbqS;->b:LbqQ;

    .line 115
    new-instance v0, Lbrc;

    invoke-direct {v0, v2}, Lbrc;-><init>(I)V

    sput-object v0, LbqS;->c:LbqQ;

    .line 141
    new-instance v0, Lbra;

    invoke-direct {v0, v2}, Lbra;-><init>(I)V

    sput-object v0, LbqS;->d:LbqQ;

    .line 151
    new-instance v0, LbqX;

    const-string v1, "MD5"

    const-string v2, "Hashing.md5()"

    invoke-direct {v0, v1, v2}, LbqX;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LbqS;->e:LbqQ;

    .line 161
    new-instance v0, LbqX;

    const-string v1, "SHA-1"

    const-string v2, "Hashing.sha1()"

    invoke-direct {v0, v1, v2}, LbqX;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LbqS;->f:LbqQ;

    .line 172
    new-instance v0, LbqX;

    const-string v1, "SHA-256"

    const-string v2, "Hashing.sha256()"

    invoke-direct {v0, v1, v2}, LbqX;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LbqS;->g:LbqQ;

    .line 183
    new-instance v0, LbqX;

    const-string v1, "SHA-512"

    const-string v2, "Hashing.sha512()"

    invoke-direct {v0, v1, v2}, LbqX;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LbqS;->h:LbqQ;

    .line 199
    sget-object v0, LbqU;->a:LbqU;

    const-string v1, "Hashing.crc32()"

    invoke-static {v0, v1}, LbqS;->a(LbqU;Ljava/lang/String;)LbqQ;

    move-result-object v0

    sput-object v0, LbqS;->i:LbqQ;

    .line 215
    sget-object v0, LbqU;->b:LbqU;

    const-string v1, "Hashing.adler32()"

    invoke-static {v0, v1}, LbqS;->a(LbqU;Ljava/lang/String;)LbqQ;

    move-result-object v0

    sput-object v0, LbqS;->j:LbqQ;

    return-void
.end method

.method public static a()LbqQ;
    .locals 1

    .prologue
    .line 148
    sget-object v0, LbqS;->e:LbqQ;

    return-object v0
.end method

.method public static a(I)LbqQ;
    .locals 1

    .prologue
    .line 100
    new-instance v0, Lbrc;

    invoke-direct {v0, p0}, Lbrc;-><init>(I)V

    return-object v0
.end method

.method private static a(LbqU;Ljava/lang/String;)LbqQ;
    .locals 2

    .prologue
    .line 219
    new-instance v0, LbqI;

    invoke-static {p0}, LbqU;->a(LbqU;)I

    move-result v1

    invoke-direct {v0, p0, v1, p1}, LbqI;-><init>(Lbjv;ILjava/lang/String;)V

    return-object v0
.end method

.method public static b()LbqQ;
    .locals 1

    .prologue
    .line 180
    sget-object v0, LbqS;->h:LbqQ;

    return-object v0
.end method

.method public static b(I)LbqQ;
    .locals 1

    .prologue
    .line 126
    new-instance v0, Lbra;

    invoke-direct {v0, p0}, Lbra;-><init>(I)V

    return-object v0
.end method
