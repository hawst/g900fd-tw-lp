.class public Laib;
.super Ljava/lang/Object;
.source "FileSynchronizerModule.java"

# interfaces
.implements LbuC;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/inject/Binder;)V
    .locals 2

    .prologue
    .line 40
    const-class v0, LagG;

    const-class v1, LQR;

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    invoke-static {p1, v0}, LbvX;->a(Lcom/google/inject/Binder;Lbuv;)LbvX;

    .line 41
    return-void
.end method

.method provideFileDownloader(LahY;LbiP;)LagG;
    .locals 1
    .param p2    # LbiP;
        .annotation runtime LQR;
        .end annotation
    .end param
    .annotation runtime LbuF;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LahY;",
            "LbiP",
            "<",
            "LagG;",
            ">;)",
            "LagG;"
        }
    .end annotation

    .prologue
    .line 35
    invoke-virtual {p2, p1}, LbiP;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LagG;

    return-object v0
.end method

.method providePartialDownloadedFileStore(Laig;)Laie;
    .locals 0
    .annotation runtime LbuF;
    .end annotation

    .prologue
    .line 28
    return-object p1
.end method
