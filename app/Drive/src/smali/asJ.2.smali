.class public LasJ;
.super Ljava/lang/Object;
.source "Story.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LbiG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiG",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LasO;

.field private final a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 138
    new-instance v0, LasK;

    invoke-direct {v0}, LasK;-><init>()V

    sput-object v0, LasJ;->a:LbiG;

    return-void
.end method

.method constructor <init>(LasO;LbmF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LasO;",
            "LbmF",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    iput-object p1, p0, LasJ;->a:LasO;

    .line 151
    iput-object p2, p0, LasJ;->a:LbmF;

    .line 152
    return-void
.end method

.method public static a(Landroid/os/Bundle;)LasJ;
    .locals 3

    .prologue
    .line 202
    new-instance v0, LasJ;

    invoke-static {}, LasO;->values()[LasO;

    move-result-object v1

    const-string v2, "storyTitle"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    aget-object v1, v1, v2

    const-string v2, "storyPages"

    .line 203
    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LbmF;->a([Ljava/lang/Object;)LbmF;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LasJ;-><init>(LasO;LbmF;)V

    .line 204
    return-object v0
.end method

.method public static a(LasP;LasO;)LasJ;
    .locals 2

    .prologue
    .line 113
    new-instance v0, LasM;

    invoke-direct {v0}, LasM;-><init>()V

    .line 114
    invoke-virtual {v0, p1}, LasM;->a(LasO;)LasM;

    move-result-object v0

    .line 115
    invoke-interface {p0}, LasP;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LasM;->a(Landroid/net/Uri;)LasM;

    move-result-object v0

    .line 116
    invoke-interface {p0}, LasP;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LasM;->a(Ljava/lang/String;)LasM;

    move-result-object v0

    .line 117
    invoke-virtual {v0}, LasM;->a()LasJ;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;LasO;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 122
    sget-object v0, LasL;->a:[I

    invoke-virtual {p1}, LasO;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 131
    sget v0, Lxi;->welcome_title_announce:I

    .line 134
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 124
    :pswitch_0
    sget v0, Lxi;->welcome_title:I

    goto :goto_0

    .line 127
    :pswitch_1
    sget v0, Lxi;->welcome_title_highlights:I

    goto :goto_0

    .line 122
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    invoke-static {p0}, LasJ;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 224
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 225
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 226
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, LasJ;->a:LbmF;

    invoke-virtual {v0}, LbmF;->size()I

    move-result v0

    return v0
.end method

.method public a()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 191
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 192
    const-string v2, "storyPages"

    iget-object v0, p0, LasJ;->a:LbmF;

    iget-object v3, p0, LasJ;->a:LbmF;

    invoke-virtual {v3}, LbmF;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v0, v3}, LbmF;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 193
    const-string v0, "storyTitle"

    iget-object v2, p0, LasJ;->a:LasO;

    invoke-virtual {v2}, LasO;->ordinal()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 194
    return-object v1
.end method

.method public a(LasJ;)LasJ;
    .locals 4

    .prologue
    .line 213
    invoke-static {}, LboS;->a()Ljava/util/TreeMap;

    move-result-object v1

    .line 214
    iget-object v0, p0, LasJ;->a:LbmF;

    iget-object v2, p1, LasJ;->a:LbmF;

    .line 215
    invoke-static {v0, v2}, Lbnm;->a(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    sget-object v2, LasJ;->a:LbiG;

    .line 214
    invoke-static {v0, v2}, Lbnm;->a(Ljava/lang/Iterable;LbiG;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 216
    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 219
    :cond_0
    new-instance v0, LasJ;

    iget-object v2, p0, LasJ;->a:LasO;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v1

    invoke-direct {v0, v2, v1}, LasJ;-><init>(LasO;LbmF;)V

    return-object v0
.end method

.method public a()LasO;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, LasJ;->a:LasO;

    return-object v0
.end method

.method public a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, LasJ;->a:LbmF;

    invoke-virtual {v0, p1}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 168
    iget-object v0, p0, LasJ;->a:LbmF;

    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v0

    return-object v0
.end method
