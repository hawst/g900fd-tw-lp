.class public LaGI;
.super Landroid/app/Dialog;
.source "LoadingIndicatorDialog.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 46
    sget v0, Lxj;->LoadingDialog:I

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 47
    return-void
.end method

.method public static a(Landroid/content/Context;)LaGI;
    .locals 6

    .prologue
    const/4 v4, -0x2

    .line 21
    invoke-static {}, LamV;->a()V

    .line 22
    new-instance v1, LaGI;

    invoke-direct {v1, p0}, LaGI;-><init>(Landroid/content/Context;)V

    .line 23
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, LaGI;->setCancelable(Z)V

    .line 24
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 27
    sget v2, Lxe;->loading_indicator:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 28
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 29
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0, v2}, LaGI;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 32
    invoke-static {}, Lanj;->a()Landroid/os/Handler;

    move-result-object v2

    new-instance v3, LaGJ;

    invoke-direct {v3, v1, v0}, LaGJ;-><init>(LaGI;Landroid/view/View;)V

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 41
    invoke-virtual {v1}, LaGI;->show()V

    .line 42
    return-object v1
.end method
