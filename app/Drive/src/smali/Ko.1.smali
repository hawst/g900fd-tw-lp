.class LKo;
.super Ljava/lang/Object;
.source "AnimatedFloatingHandleShadow.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field final synthetic a:LKn;

.field private final a:Ljava/lang/Runnable;

.field private a:Z


# direct methods
.method public constructor <init>(LKn;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 39
    iput-object p1, p0, LKo;->a:LKn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    iput-object v0, p0, LKo;->a:Ljava/lang/Runnable;

    .line 41
    return-void
.end method


# virtual methods
.method public declared-synchronized onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 62
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LKo;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    monitor-exit p0

    return-void

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 50
    monitor-enter p0

    .line 51
    :try_start_0
    iget-boolean v0, p0, LKo;->a:Z

    .line 52
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    if-nez v0, :cond_0

    .line 54
    iget-object v0, p0, LKo;->a:LKn;

    invoke-static {v0}, LKn;->a(LKn;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 55
    iget-object v0, p0, LKo;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 56
    iget-object v0, p0, LKo;->a:LKn;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LKn;->a(LKn;Landroid/animation/Animator;)Landroid/animation/Animator;

    .line 58
    :cond_0
    return-void

    .line 52
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 45
    return-void
.end method
