.class final enum LKX;
.super Ljava/lang/Enum;
.source "MoveDetector.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LKX;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LKX;

.field private static final synthetic a:[LKX;

.field public static final enum b:LKX;

.field public static final enum c:LKX;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 32
    new-instance v0, LKX;

    const-string v1, "UNITIALIZED"

    invoke-direct {v0, v1, v2}, LKX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LKX;->a:LKX;

    new-instance v0, LKX;

    const-string v1, "DOWN_DETECTED"

    invoke-direct {v0, v1, v3}, LKX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LKX;->b:LKX;

    new-instance v0, LKX;

    const-string v1, "MOVE_DETECTED"

    invoke-direct {v0, v1, v4}, LKX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LKX;->c:LKX;

    .line 31
    const/4 v0, 0x3

    new-array v0, v0, [LKX;

    sget-object v1, LKX;->a:LKX;

    aput-object v1, v0, v2

    sget-object v1, LKX;->b:LKX;

    aput-object v1, v0, v3

    sget-object v1, LKX;->c:LKX;

    aput-object v1, v0, v4

    sput-object v0, LKX;->a:[LKX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LKX;
    .locals 1

    .prologue
    .line 31
    const-class v0, LKX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LKX;

    return-object v0
.end method

.method public static values()[LKX;
    .locals 1

    .prologue
    .line 31
    sget-object v0, LKX;->a:[LKX;

    invoke-virtual {v0}, [LKX;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LKX;

    return-object v0
.end method
