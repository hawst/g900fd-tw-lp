.class public final LnK;
.super Ljava/lang/Object;

# interfaces
.implements LayE;
.implements LayG;


# instance fields
.field private a:Layx;

.field private a:Lcom/google/android/gms/ads/AdView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;LayC;Landroid/os/Bundle;Landroid/os/Bundle;)Layt;
    .locals 4

    const/4 v1, 0x1

    new-instance v2, Layv;

    invoke-direct {v2}, Layv;-><init>()V

    invoke-interface {p1}, LayC;->a()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v2, v0}, Layv;->a(Ljava/util/Date;)Layv;

    :cond_0
    invoke-interface {p1}, LayC;->a()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v2, v0}, Layv;->a(I)Layv;

    :cond_1
    invoke-interface {p1}, LayC;->a()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Layv;->a(Ljava/lang/String;)Layv;

    goto :goto_0

    :cond_2
    invoke-interface {p1}, LayC;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, LaQb;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Layv;->b(Ljava/lang/String;)Layv;

    :cond_3
    const-string v0, "tagForChildDirectedTreatment"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_4

    const-string v0, "tagForChildDirectedTreatment"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Layv;->a(Z)Layv;

    :cond_4
    if-eqz p2, :cond_7

    :goto_2
    const-string v0, "gw"

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "mad_hac"

    const-string v3, "mad_hac"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "adJson"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "_ad"

    const-string v3, "adJson"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    const-string v0, "_noRefresh"

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-class v0, LnK;

    invoke-virtual {v2, v0, p2}, Layv;->a(Ljava/lang/Class;Landroid/os/Bundle;)Layv;

    invoke-virtual {v2}, Layv;->a()Layt;

    move-result-object v0

    return-object v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    :cond_7
    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    goto :goto_2
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    iget-object v0, p0, LnK;->a:Lcom/google/android/gms/ads/AdView;

    return-object v0
.end method

.method public a()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, LnK;->a:Lcom/google/android/gms/ads/AdView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LnK;->a:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->a()V

    iput-object v1, p0, LnK;->a:Lcom/google/android/gms/ads/AdView;

    :cond_0
    iget-object v0, p0, LnK;->a:Layx;

    if-eqz v0, :cond_1

    iput-object v1, p0, LnK;->a:Layx;

    :cond_1
    return-void
.end method

.method public a(Landroid/content/Context;LayF;Landroid/os/Bundle;Layw;LayC;Landroid/os/Bundle;)V
    .locals 4

    new-instance v0, Lcom/google/android/gms/ads/AdView;

    invoke-direct {v0, p1}, Lcom/google/android/gms/ads/AdView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LnK;->a:Lcom/google/android/gms/ads/AdView;

    iget-object v0, p0, LnK;->a:Lcom/google/android/gms/ads/AdView;

    new-instance v1, Layw;

    invoke-virtual {p4}, Layw;->b()I

    move-result v2

    invoke-virtual {p4}, Layw;->a()I

    move-result v3

    invoke-direct {v1, v2, v3}, Layw;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/AdView;->setAdSize(Layw;)V

    iget-object v0, p0, LnK;->a:Lcom/google/android/gms/ads/AdView;

    const-string v1, "pubid"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/AdView;->setAdUnitId(Ljava/lang/String;)V

    iget-object v0, p0, LnK;->a:Lcom/google/android/gms/ads/AdView;

    new-instance v1, LnL;

    invoke-direct {v1, p0, p2}, LnL;-><init>(LnK;LayF;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/AdView;->setAdListener(Lays;)V

    iget-object v0, p0, LnK;->a:Lcom/google/android/gms/ads/AdView;

    invoke-static {p1, p5, p6, p3}, LnK;->a(Landroid/content/Context;LayC;Landroid/os/Bundle;Landroid/os/Bundle;)Layt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/AdView;->a(Layt;)V

    return-void
.end method

.method public a(Landroid/content/Context;LayH;Landroid/os/Bundle;LayC;Landroid/os/Bundle;)V
    .locals 2

    new-instance v0, Layx;

    invoke-direct {v0, p1}, Layx;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LnK;->a:Layx;

    iget-object v0, p0, LnK;->a:Layx;

    const-string v1, "pubid"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Layx;->a(Ljava/lang/String;)V

    iget-object v0, p0, LnK;->a:Layx;

    new-instance v1, LnM;

    invoke-direct {v1, p0, p2}, LnM;-><init>(LnK;LayH;)V

    invoke-virtual {v0, v1}, Layx;->a(Lays;)V

    iget-object v0, p0, LnK;->a:Layx;

    invoke-static {p1, p4, p5, p3}, LnK;->a(Landroid/content/Context;LayC;Landroid/os/Bundle;Landroid/os/Bundle;)Layt;

    move-result-object v1

    invoke-virtual {v0, v1}, Layx;->a(Layt;)V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, LnK;->a:Lcom/google/android/gms/ads/AdView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LnK;->a:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->b()V

    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, LnK;->a:Lcom/google/android/gms/ads/AdView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LnK;->a:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->c()V

    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, LnK;->a:Layx;

    invoke-virtual {v0}, Layx;->a()V

    return-void
.end method
