.class public final LaoA;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laom;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LanR;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LanU;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laor;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaoJ;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LanZ;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LaoG;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laoh;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laoe;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laoj;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laos;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laot;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "Laoc;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LakR;",
            ">;"
        }
    .end annotation
.end field

.field public o:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LZS;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LZS;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, LaoE;

    sput-object v0, LaoA;->a:Ljava/lang/Class;

    .line 33
    const-class v0, LaoC;

    sput-object v0, LaoA;->b:Ljava/lang/Class;

    .line 34
    const-class v0, LaoD;

    sput-object v0, LaoA;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LbrA;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 56
    iput-object p1, p0, LaoA;->a:LbrA;

    .line 57
    const-class v0, Laom;

    invoke-static {v0, v1}, LaoA;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaoA;->a:Lbsk;

    .line 60
    const-class v0, LanR;

    invoke-static {v0, v1}, LaoA;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaoA;->b:Lbsk;

    .line 63
    const-class v0, LanU;

    invoke-static {v0, v1}, LaoA;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaoA;->c:Lbsk;

    .line 66
    const-class v0, Laor;

    invoke-static {v0, v1}, LaoA;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaoA;->d:Lbsk;

    .line 69
    const-class v0, LaoJ;

    invoke-static {v0, v1}, LaoA;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaoA;->e:Lbsk;

    .line 72
    const-class v0, LanZ;

    invoke-static {v0, v1}, LaoA;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaoA;->f:Lbsk;

    .line 75
    const-class v0, LaoG;

    invoke-static {v0, v1}, LaoA;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaoA;->g:Lbsk;

    .line 78
    const-class v0, Laoh;

    invoke-static {v0, v1}, LaoA;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaoA;->h:Lbsk;

    .line 81
    const-class v0, Laoe;

    invoke-static {v0, v1}, LaoA;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaoA;->i:Lbsk;

    .line 84
    const-class v0, Laoj;

    invoke-static {v0, v1}, LaoA;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaoA;->j:Lbsk;

    .line 87
    const-class v0, Laos;

    invoke-static {v0, v1}, LaoA;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaoA;->k:Lbsk;

    .line 90
    const-class v0, Laot;

    invoke-static {v0, v1}, LaoA;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaoA;->l:Lbsk;

    .line 93
    const-class v0, Laoc;

    invoke-static {v0, v1}, LaoA;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaoA;->m:Lbsk;

    .line 96
    const-class v0, LakR;

    sget-object v1, LaoA;->b:Ljava/lang/Class;

    .line 97
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    const-class v1, Lbxz;

    .line 96
    invoke-static {v0, v1}, LaoA;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaoA;->n:Lbsk;

    .line 99
    const-class v0, LZS;

    sget-object v1, LaoA;->c:Ljava/lang/Class;

    .line 100
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    const-class v1, Lbxz;

    .line 99
    invoke-static {v0, v1}, LaoA;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaoA;->o:Lbsk;

    .line 102
    const-class v0, LZS;

    sget-object v1, LaoA;->a:Ljava/lang/Class;

    .line 103
    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    const-class v1, Lbxz;

    .line 102
    invoke-static {v0, v1}, LaoA;->a(Lbuv;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LaoA;->p:Lbsk;

    .line 105
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 168
    sparse-switch p1, :sswitch_data_0

    .line 486
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :sswitch_0
    new-instance v0, Laom;

    iget-object v1, p0, LaoA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaoA;

    iget-object v1, v1, LaoA;->j:Lbsk;

    .line 173
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LaoA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaoA;

    iget-object v2, v2, LaoA;->j:Lbsk;

    .line 171
    invoke-static {v1, v2}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laoj;

    iget-object v2, p0, LaoA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LTk;

    iget-object v2, v2, LTk;->a:Lbsk;

    .line 179
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LaoA;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LTk;

    iget-object v3, v3, LTk;->a:Lbsk;

    .line 177
    invoke-static {v2, v3}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LTl;

    iget-object v3, p0, LaoA;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->K:Lbsk;

    .line 185
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LaoA;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LalC;

    iget-object v4, v4, LalC;->K:Lbsk;

    .line 183
    invoke-static {v3, v4}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lalo;

    iget-object v4, p0, LaoA;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LadM;

    iget-object v4, v4, LadM;->p:Lbsk;

    .line 191
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LaoA;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LadM;

    iget-object v5, v5, LadM;->p:Lbsk;

    .line 189
    invoke-static {v4, v5}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Laeb;

    iget-object v5, p0, LaoA;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LaoA;

    iget-object v5, v5, LaoA;->g:Lbsk;

    .line 197
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LaoA;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LaoA;

    iget-object v6, v6, LaoA;->g:Lbsk;

    .line 195
    invoke-static {v5, v6}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LaoG;

    invoke-direct/range {v0 .. v5}, Laom;-><init>(Laoj;LTl;Lalo;Laeb;LaoG;)V

    .line 484
    :goto_0
    return-object v0

    .line 204
    :sswitch_1
    new-instance v3, LanR;

    iget-object v0, p0, LaoA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 207
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaoA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 205
    invoke-static {v0, v1}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iget-object v1, p0, LaoA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaoA;

    iget-object v1, v1, LaoA;->c:Lbsk;

    .line 213
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LaoA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaoA;

    iget-object v2, v2, LaoA;->c:Lbsk;

    .line 211
    invoke-static {v1, v2}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LanU;

    iget-object v2, p0, LaoA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LtQ;

    iget-object v2, v2, LtQ;->g:Lbsk;

    .line 219
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LaoA;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LtQ;

    iget-object v4, v4, LtQ;->g:Lbsk;

    .line 217
    invoke-static {v2, v4}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ltp;

    invoke-direct {v3, v0, v1, v2}, LanR;-><init>(LQr;LanU;Ltp;)V

    move-object v0, v3

    .line 224
    goto :goto_0

    .line 226
    :sswitch_2
    new-instance v0, LanU;

    iget-object v1, p0, LaoA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 229
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LaoA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 227
    invoke-static {v1, v2}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LQr;

    iget-object v2, p0, LaoA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaoA;

    iget-object v2, v2, LaoA;->e:Lbsk;

    .line 235
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LaoA;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaoA;

    iget-object v3, v3, LaoA;->e:Lbsk;

    .line 233
    invoke-static {v2, v3}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LaoJ;

    iget-object v3, p0, LaoA;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaoA;

    iget-object v3, v3, LaoA;->a:Lbsk;

    .line 241
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LaoA;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaoA;

    iget-object v4, v4, LaoA;->a:Lbsk;

    .line 239
    invoke-static {v3, v4}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Laom;

    iget-object v4, p0, LaoA;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaoV;

    iget-object v4, v4, LaoV;->c:Lbsk;

    .line 247
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LaoA;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LaoV;

    iget-object v5, v5, LaoV;->c:Lbsk;

    .line 245
    invoke-static {v4, v5}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lapl;

    iget-object v5, p0, LaoA;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LaoA;

    iget-object v5, v5, LaoA;->g:Lbsk;

    .line 253
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LaoA;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LaoA;

    iget-object v6, v6, LaoA;->g:Lbsk;

    .line 251
    invoke-static {v5, v6}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LaoG;

    iget-object v6, p0, LaoA;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LaoA;

    iget-object v6, v6, LaoA;->n:Lbsk;

    .line 259
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LaoA;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LaoA;

    iget-object v7, v7, LaoA;->n:Lbsk;

    .line 257
    invoke-static {v6, v7}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LakR;

    iget-object v7, p0, LaoA;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LaoA;

    iget-object v7, v7, LaoA;->o:Lbsk;

    .line 265
    invoke-virtual {v7}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v7

    iget-object v8, p0, LaoA;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LaoA;

    iget-object v8, v8, LaoA;->o:Lbsk;

    .line 263
    invoke-static {v7, v8}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LZS;

    iget-object v8, p0, LaoA;->a:LbrA;

    iget-object v8, v8, LbrA;->a:LaoA;

    iget-object v8, v8, LaoA;->p:Lbsk;

    .line 271
    invoke-virtual {v8}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v8

    iget-object v9, p0, LaoA;->a:LbrA;

    iget-object v9, v9, LbrA;->a:LaoA;

    iget-object v9, v9, LaoA;->p:Lbsk;

    .line 269
    invoke-static {v8, v9}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LZS;

    invoke-direct/range {v0 .. v8}, LanU;-><init>(LQr;LaoJ;Laom;Lapl;LaoG;LakR;LZS;LZS;)V

    goto/16 :goto_0

    .line 278
    :sswitch_3
    new-instance v2, Laor;

    iget-object v0, p0, LaoA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LalC;

    iget-object v0, v0, LalC;->i:Lbsk;

    .line 281
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaoA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->i:Lbsk;

    .line 279
    invoke-static {v0, v1}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LakX;

    iget-object v1, p0, LaoA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 287
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LaoA;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LQH;

    iget-object v3, v3, LQH;->d:Lbsk;

    .line 285
    invoke-static {v1, v3}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LQr;

    invoke-direct {v2, v0, v1}, Laor;-><init>(LakX;LQr;)V

    move-object v0, v2

    .line 292
    goto/16 :goto_0

    .line 294
    :sswitch_4
    new-instance v0, LaoJ;

    iget-object v1, p0, LaoA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 297
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LaoA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaGH;

    iget-object v2, v2, LaGH;->l:Lbsk;

    .line 295
    invoke-static {v1, v2}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGM;

    iget-object v2, p0, LaoA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LadM;

    iget-object v2, v2, LadM;->m:Lbsk;

    .line 303
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LaoA;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LadM;

    iget-object v3, v3, LadM;->m:Lbsk;

    .line 301
    invoke-static {v2, v3}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ladi;

    iget-object v3, p0, LaoA;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LadM;

    iget-object v3, v3, LadM;->p:Lbsk;

    .line 309
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LaoA;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LadM;

    iget-object v4, v4, LadM;->p:Lbsk;

    .line 307
    invoke-static {v3, v4}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Laeb;

    iget-object v4, p0, LaoA;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LalC;

    iget-object v4, v4, LalC;->K:Lbsk;

    .line 315
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LaoA;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LalC;

    iget-object v5, v5, LalC;->K:Lbsk;

    .line 313
    invoke-static {v4, v5}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lalo;

    iget-object v5, p0, LaoA;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LalC;

    iget-object v5, v5, LalC;->L:Lbsk;

    .line 321
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LaoA;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LalC;

    iget-object v6, v6, LalC;->L:Lbsk;

    .line 319
    invoke-static {v5, v6}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LaKR;

    invoke-direct/range {v0 .. v5}, LaoJ;-><init>(LaGM;Ladi;Laeb;Lalo;LaKR;)V

    goto/16 :goto_0

    .line 328
    :sswitch_5
    new-instance v0, LanZ;

    iget-object v1, p0, LaoA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 331
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LaoA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 329
    invoke-static {v1, v2}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LQr;

    iget-object v2, p0, LaoA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaGH;

    iget-object v2, v2, LaGH;->l:Lbsk;

    .line 337
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LaoA;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaGH;

    iget-object v3, v3, LaGH;->l:Lbsk;

    .line 335
    invoke-static {v2, v3}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LaGM;

    iget-object v3, p0, LaoA;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->L:Lbsk;

    .line 343
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LaoA;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LalC;

    iget-object v4, v4, LalC;->L:Lbsk;

    .line 341
    invoke-static {v3, v4}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaKR;

    iget-object v4, p0, LaoA;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LadM;

    iget-object v4, v4, LadM;->m:Lbsk;

    .line 349
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LaoA;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LadM;

    iget-object v5, v5, LadM;->m:Lbsk;

    .line 347
    invoke-static {v4, v5}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ladi;

    iget-object v5, p0, LaoA;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LaoA;

    iget-object v5, v5, LaoA;->m:Lbsk;

    .line 355
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LaoA;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LaoA;

    iget-object v6, v6, LaoA;->m:Lbsk;

    .line 353
    invoke-static {v5, v6}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Laoc;

    iget-object v6, p0, LaoA;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LaoA;

    iget-object v6, v6, LaoA;->p:Lbsk;

    .line 361
    invoke-virtual {v6}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v6

    iget-object v7, p0, LaoA;->a:LbrA;

    iget-object v7, v7, LbrA;->a:LaoA;

    iget-object v7, v7, LaoA;->p:Lbsk;

    .line 359
    invoke-static {v6, v7}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LZS;

    invoke-direct/range {v0 .. v6}, LanZ;-><init>(LQr;LaGM;LaKR;Ladi;Laoc;LZS;)V

    goto/16 :goto_0

    .line 368
    :sswitch_6
    new-instance v1, LaoG;

    iget-object v0, p0, LaoA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 371
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LaoA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 369
    invoke-static {v0, v2}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    invoke-direct {v1, v0}, LaoG;-><init>(LQr;)V

    move-object v0, v1

    .line 376
    goto/16 :goto_0

    .line 378
    :sswitch_7
    new-instance v1, Laoh;

    iget-object v0, p0, LaoA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaoA;

    iget-object v0, v0, LaoA;->i:Lbsk;

    .line 381
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LaoA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaoA;

    iget-object v2, v2, LaoA;->i:Lbsk;

    .line 379
    invoke-static {v0, v2}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoe;

    invoke-direct {v1, v0}, Laoh;-><init>(Laoe;)V

    move-object v0, v1

    .line 386
    goto/16 :goto_0

    .line 388
    :sswitch_8
    new-instance v4, Laoe;

    iget-object v0, p0, LaoA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 391
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaoA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 389
    invoke-static {v0, v1}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iget-object v1, p0, LaoA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LtQ;

    iget-object v1, v1, LtQ;->A:Lbsk;

    .line 397
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LaoA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LtQ;

    iget-object v2, v2, LtQ;->A:Lbsk;

    .line 395
    invoke-static {v1, v2}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Luf;

    iget-object v2, p0, LaoA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LtQ;

    iget-object v2, v2, LtQ;->f:Lbsk;

    .line 403
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LaoA;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LtQ;

    iget-object v3, v3, LtQ;->f:Lbsk;

    .line 401
    invoke-static {v2, v3}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lvs;

    iget-object v3, p0, LaoA;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaoA;

    iget-object v3, v3, LaoA;->f:Lbsk;

    .line 409
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, LaoA;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LaoA;

    iget-object v5, v5, LaoA;->f:Lbsk;

    .line 407
    invoke-static {v3, v5}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LanZ;

    invoke-direct {v4, v0, v1, v2, v3}, Laoe;-><init>(LaGM;Luf;Lvs;LanZ;)V

    move-object v0, v4

    .line 414
    goto/16 :goto_0

    .line 416
    :sswitch_9
    new-instance v1, Laoj;

    iget-object v0, p0, LaoA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LTk;

    iget-object v0, v0, LTk;->h:Lbsk;

    .line 419
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LaoA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LTk;

    iget-object v2, v2, LTk;->h:Lbsk;

    .line 417
    invoke-static {v0, v2}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTh;

    invoke-direct {v1, v0}, Laoj;-><init>(LTh;)V

    move-object v0, v1

    .line 424
    goto/16 :goto_0

    .line 426
    :sswitch_a
    new-instance v2, Laos;

    iget-object v0, p0, LaoA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 429
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LaoA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LQH;

    iget-object v1, v1, LQH;->d:Lbsk;

    .line 427
    invoke-static {v0, v1}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iget-object v1, p0, LaoA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LalC;

    iget-object v1, v1, LalC;->S:Lbsk;

    .line 435
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, LaoA;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LalC;

    iget-object v3, v3, LalC;->S:Lbsk;

    .line 433
    invoke-static {v1, v3}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaKM;

    invoke-direct {v2, v0, v1}, Laos;-><init>(LQr;LaKM;)V

    move-object v0, v2

    .line 440
    goto/16 :goto_0

    .line 442
    :sswitch_b
    new-instance v1, Laot;

    iget-object v0, p0, LaoA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LQH;

    iget-object v0, v0, LQH;->d:Lbsk;

    .line 445
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LaoA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LQH;

    iget-object v2, v2, LQH;->d:Lbsk;

    .line 443
    invoke-static {v0, v2}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    invoke-direct {v1, v0}, Laot;-><init>(LQr;)V

    move-object v0, v1

    .line 450
    goto/16 :goto_0

    .line 452
    :sswitch_c
    new-instance v0, Laoc;

    iget-object v1, p0, LaoA;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaoA;

    iget-object v1, v1, LaoA;->a:Lbsk;

    .line 455
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LaoA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaoA;

    iget-object v2, v2, LaoA;->a:Lbsk;

    .line 453
    invoke-static {v1, v2}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laom;

    iget-object v2, p0, LaoA;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LagN;

    iget-object v2, v2, LagN;->e:Lbsk;

    .line 461
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LaoA;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LagN;

    iget-object v3, v3, LagN;->e:Lbsk;

    .line 459
    invoke-static {v2, v3}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lago;

    iget-object v3, p0, LaoA;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LaGH;

    iget-object v3, v3, LaGH;->l:Lbsk;

    .line 467
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LaoA;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaGH;

    iget-object v4, v4, LaGH;->l:Lbsk;

    .line 465
    invoke-static {v3, v4}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LaGM;

    iget-object v4, p0, LaoA;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaoA;

    iget-object v4, v4, LaoA;->n:Lbsk;

    .line 473
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LaoA;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LaoA;

    iget-object v5, v5, LaoA;->n:Lbsk;

    .line 471
    invoke-static {v4, v5}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LakR;

    iget-object v5, p0, LaoA;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LaoA;

    iget-object v5, v5, LaoA;->o:Lbsk;

    .line 479
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LaoA;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LaoA;

    iget-object v6, v6, LaoA;->o:Lbsk;

    .line 477
    invoke-static {v5, v6}, LaoA;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LZS;

    invoke-direct/range {v0 .. v5}, Laoc;-><init>(Laom;Lago;LaGM;LakR;LZS;)V

    goto/16 :goto_0

    .line 168
    nop

    :sswitch_data_0
    .sparse-switch
        0x152 -> :sswitch_9
        0x20f -> :sswitch_0
        0x210 -> :sswitch_6
        0x211 -> :sswitch_1
        0x212 -> :sswitch_2
        0x214 -> :sswitch_4
        0x218 -> :sswitch_3
        0x219 -> :sswitch_5
        0x21a -> :sswitch_c
        0x21b -> :sswitch_7
        0x21c -> :sswitch_8
        0x21d -> :sswitch_a
        0x21e -> :sswitch_b
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 501
    sparse-switch p2, :sswitch_data_0

    .line 530
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 503
    :sswitch_0
    check-cast p1, Laoq;

    .line 505
    iget-object v0, p0, LaoA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaoA;

    iget-object v0, v0, LaoA;->d:Lbsk;

    .line 508
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laor;

    .line 505
    invoke-virtual {p1, v0}, Laoq;->provideDiskCache(Laor;)LakR;

    move-result-object v0

    .line 523
    :goto_0
    return-object v0

    .line 512
    :sswitch_1
    check-cast p1, Laoq;

    .line 514
    iget-object v0, p0, LaoA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaoA;

    iget-object v0, v0, LaoA;->k:Lbsk;

    .line 517
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laos;

    .line 514
    invoke-virtual {p1, v0}, Laoq;->provideWaitingRateLimiter(Laos;)LZS;

    move-result-object v0

    goto :goto_0

    .line 521
    :sswitch_2
    check-cast p1, Laoq;

    .line 523
    iget-object v0, p0, LaoA;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaoA;

    iget-object v0, v0, LaoA;->l:Lbsk;

    .line 526
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laot;

    .line 523
    invoke-virtual {p1, v0}, Laoq;->provideFetchingRetryRateLimiter(Laot;)LZS;

    move-result-object v0

    goto :goto_0

    .line 501
    nop

    :sswitch_data_0
    .sparse-switch
        0x180 -> :sswitch_0
        0x216 -> :sswitch_1
        0x217 -> :sswitch_2
    .end sparse-switch
.end method

.method public a()V
    .locals 3

    .prologue
    .line 112
    const-class v0, Laom;

    iget-object v1, p0, LaoA;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LaoA;->a(Ljava/lang/Class;Lbsk;)V

    .line 113
    const-class v0, LanR;

    iget-object v1, p0, LaoA;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LaoA;->a(Ljava/lang/Class;Lbsk;)V

    .line 114
    const-class v0, LanU;

    iget-object v1, p0, LaoA;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LaoA;->a(Ljava/lang/Class;Lbsk;)V

    .line 115
    const-class v0, Laor;

    iget-object v1, p0, LaoA;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LaoA;->a(Ljava/lang/Class;Lbsk;)V

    .line 116
    const-class v0, LaoJ;

    iget-object v1, p0, LaoA;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LaoA;->a(Ljava/lang/Class;Lbsk;)V

    .line 117
    const-class v0, LanZ;

    iget-object v1, p0, LaoA;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, LaoA;->a(Ljava/lang/Class;Lbsk;)V

    .line 118
    const-class v0, LaoG;

    iget-object v1, p0, LaoA;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, LaoA;->a(Ljava/lang/Class;Lbsk;)V

    .line 119
    const-class v0, Laoh;

    iget-object v1, p0, LaoA;->h:Lbsk;

    invoke-virtual {p0, v0, v1}, LaoA;->a(Ljava/lang/Class;Lbsk;)V

    .line 120
    const-class v0, Laoe;

    iget-object v1, p0, LaoA;->i:Lbsk;

    invoke-virtual {p0, v0, v1}, LaoA;->a(Ljava/lang/Class;Lbsk;)V

    .line 121
    const-class v0, Laoj;

    iget-object v1, p0, LaoA;->j:Lbsk;

    invoke-virtual {p0, v0, v1}, LaoA;->a(Ljava/lang/Class;Lbsk;)V

    .line 122
    const-class v0, Laos;

    iget-object v1, p0, LaoA;->k:Lbsk;

    invoke-virtual {p0, v0, v1}, LaoA;->a(Ljava/lang/Class;Lbsk;)V

    .line 123
    const-class v0, Laot;

    iget-object v1, p0, LaoA;->l:Lbsk;

    invoke-virtual {p0, v0, v1}, LaoA;->a(Ljava/lang/Class;Lbsk;)V

    .line 124
    const-class v0, Laoc;

    iget-object v1, p0, LaoA;->m:Lbsk;

    invoke-virtual {p0, v0, v1}, LaoA;->a(Ljava/lang/Class;Lbsk;)V

    .line 125
    const-class v0, LakR;

    sget-object v1, LaoA;->b:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LaoA;->n:Lbsk;

    invoke-virtual {p0, v0, v1}, LaoA;->a(Lbuv;Lbsk;)V

    .line 126
    const-class v0, LZS;

    sget-object v1, LaoA;->c:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LaoA;->o:Lbsk;

    invoke-virtual {p0, v0, v1}, LaoA;->a(Lbuv;Lbsk;)V

    .line 127
    const-class v0, LZS;

    sget-object v1, LaoA;->a:Ljava/lang/Class;

    invoke-static {v0, v1}, Lbuv;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbuv;

    move-result-object v0

    iget-object v1, p0, LaoA;->p:Lbsk;

    invoke-virtual {p0, v0, v1}, LaoA;->a(Lbuv;Lbsk;)V

    .line 128
    iget-object v0, p0, LaoA;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x20f

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 130
    iget-object v0, p0, LaoA;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x211

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 132
    iget-object v0, p0, LaoA;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x212

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 134
    iget-object v0, p0, LaoA;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x218

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 136
    iget-object v0, p0, LaoA;->e:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x214

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 138
    iget-object v0, p0, LaoA;->f:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x219

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 140
    iget-object v0, p0, LaoA;->g:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x210

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 142
    iget-object v0, p0, LaoA;->h:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x21b

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 144
    iget-object v0, p0, LaoA;->i:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x21c

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 146
    iget-object v0, p0, LaoA;->j:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x152

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 148
    iget-object v0, p0, LaoA;->k:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x21d

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 150
    iget-object v0, p0, LaoA;->l:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x21e

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 152
    iget-object v0, p0, LaoA;->m:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x21a

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 154
    iget-object v0, p0, LaoA;->n:Lbsk;

    const-class v1, Laoq;

    const/16 v2, 0x180

    invoke-virtual {p0, v1, v2}, LaoA;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 156
    iget-object v0, p0, LaoA;->o:Lbsk;

    const-class v1, Laoq;

    const/16 v2, 0x216

    invoke-virtual {p0, v1, v2}, LaoA;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 158
    iget-object v0, p0, LaoA;->p:Lbsk;

    const-class v1, Laoq;

    const/16 v2, 0x217

    invoke-virtual {p0, v1, v2}, LaoA;->a(Ljava/lang/Class;I)Lbxw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 160
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 493
    .line 495
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 164
    return-void
.end method
