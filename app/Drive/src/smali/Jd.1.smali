.class final enum LJd;
.super Ljava/lang/Enum;
.source "BaseHelpCard.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LJd;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LJd;

.field private static final synthetic a:[LJd;

.field public static final enum b:LJd;

.field public static final enum c:LJd;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 56
    new-instance v0, LJd;

    const-string v1, "HORIZONTAL"

    invoke-direct {v0, v1, v2}, LJd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LJd;->a:LJd;

    new-instance v0, LJd;

    const-string v1, "VERTICAL"

    invoke-direct {v0, v1, v3}, LJd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LJd;->b:LJd;

    new-instance v0, LJd;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, LJd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LJd;->c:LJd;

    .line 55
    const/4 v0, 0x3

    new-array v0, v0, [LJd;

    sget-object v1, LJd;->a:LJd;

    aput-object v1, v0, v2

    sget-object v1, LJd;->b:LJd;

    aput-object v1, v0, v3

    sget-object v1, LJd;->c:LJd;

    aput-object v1, v0, v4

    sput-object v0, LJd;->a:[LJd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LJd;
    .locals 1

    .prologue
    .line 55
    const-class v0, LJd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LJd;

    return-object v0
.end method

.method public static values()[LJd;
    .locals 1

    .prologue
    .line 55
    sget-object v0, LJd;->a:[LJd;

    invoke-virtual {v0}, [LJd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LJd;

    return-object v0
.end method
