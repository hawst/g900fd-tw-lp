.class public LUs;
.super Ljava/lang/Object;
.source "Accessibility.java"


# direct methods
.method public static a(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;
    .locals 1

    .prologue
    .line 38
    const-string v0, "accessibility"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, LUs;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 73
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/View;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 121
    const/16 v0, 0x4000

    invoke-static {p0, p1, p2, v0}, LUs;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/CharSequence;I)V

    .line 123
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/View;Ljava/lang/CharSequence;I)V
    .locals 2

    .prologue
    .line 86
    invoke-static {p0}, LUs;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    :goto_0
    return-void

    .line 93
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_1

    .line 94
    const/16 p3, 0x8

    .line 99
    :cond_1
    invoke-static {p3}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    invoke-virtual {p1}, Landroid/view/View;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    .line 102
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 103
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    .line 106
    invoke-static {v0}, Lfs;->a(Landroid/view/accessibility/AccessibilityEvent;)LfZ;

    move-result-object v1

    .line 107
    invoke-virtual {v1, p1}, LfZ;->a(Landroid/view/View;)V

    .line 108
    invoke-static {p0, v0}, LUs;->a(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 59
    invoke-static {p0}, LUs;->a(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 60
    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 45
    invoke-static {p0}, LUs;->a(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    invoke-static {v0}, Lfw;->a(Landroid/view/accessibility/AccessibilityManager;)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 52
    invoke-static {p0}, LUs;->a(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    return v0
.end method
