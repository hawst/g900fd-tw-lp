.class public LTS;
.super Ljava/lang/Object;
.source "AppFinderUtils.java"


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lakk;

.field private final a:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    new-instance v0, LbmM;

    invoke-direct {v0}, LbmM;-><init>()V

    sget-object v1, LTX;->a:LTX;

    .line 37
    invoke-virtual {v1}, LTY;->b()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LTX;->a:LTX;

    .line 38
    invoke-virtual {v2}, LTX;->a()Ljava/lang/String;

    move-result-object v2

    .line 37
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, LTX;->b:LTX;

    .line 39
    invoke-virtual {v1}, LTY;->b()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LTX;->b:LTX;

    .line 40
    invoke-virtual {v2}, LTX;->a()Ljava/lang/String;

    move-result-object v2

    .line 39
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    sget-object v1, LTX;->c:LTX;

    .line 41
    invoke-virtual {v1}, LTY;->b()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LTX;->c:LTX;

    .line 42
    invoke-virtual {v2}, LTX;->a()Ljava/lang/String;

    move-result-object v2

    .line 41
    invoke-virtual {v0, v1, v2}, LbmM;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmM;

    move-result-object v0

    .line 43
    invoke-virtual {v0}, LbmM;->a()LbmL;

    move-result-object v0

    sput-object v0, LTS;->a:Ljava/util/Map;

    .line 35
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lakk;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lajg;
        .end annotation
    .end param

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, LTS;->a:Landroid/content/Context;

    .line 51
    iput-object p2, p0, LTS;->a:Lakk;

    .line 52
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/pm/ResolveInfo;
    .locals 4

    .prologue
    .line 89
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 90
    invoke-virtual {v0, p2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 92
    iget-object v1, p0, LTS;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 93
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 94
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 95
    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 96
    iget-object v3, p0, LTS;->a:Lakk;

    invoke-virtual {v3, v2}, Lakk;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 101
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/pm/ResolveInfo;
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 111
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 112
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 113
    invoke-virtual {v0, p3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 115
    iget-object v1, p0, LTS;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 116
    invoke-virtual {v1, v0, v5}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 117
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118
    const/4 v0, 0x0

    .line 126
    :goto_0
    return-object v0

    .line 121
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v4, :cond_1

    .line 122
    const-string v1, "AppFinderUtils"

    const-string v2, "Multiple activities found for package %s intent %s activities"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v5

    aput-object p2, v3, v4

    const/4 v4, 0x2

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 126
    :cond_1
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 130
    iget-object v1, p0, LTS;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 132
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.apps.docs.statesyncer.DOCUMENT_PIN_STATE_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 134
    invoke-virtual {v2, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 137
    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 138
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private b(Ljava/lang/String;)LTY;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 181
    .line 182
    sget-object v0, LaGv;->b:LaGv;

    invoke-virtual {v0}, LaGv;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LTS;->a:Lakk;

    sget-object v2, LTX;->b:LTX;

    .line 183
    invoke-virtual {v2}, LTX;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lakk;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    sget-object v0, LTX;->b:LTX;

    .line 190
    :goto_0
    if-eqz v0, :cond_1

    .line 193
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 194
    invoke-virtual {v0}, LTX;->a()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 195
    iget-object v3, p0, LTS;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 200
    :goto_1
    return-object v0

    .line 185
    :cond_0
    sget-object v0, LaGv;->i:LaGv;

    invoke-virtual {v0}, LaGv;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LTS;->a:Lakk;

    sget-object v2, LTX;->c:LTX;

    .line 186
    invoke-virtual {v2}, LTX;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lakk;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 187
    sget-object v0, LTX;->c:LTX;

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 200
    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)LTY;
    .locals 5

    .prologue
    .line 60
    invoke-static {p1}, Lbju;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 62
    const-string v0, "com.google.android.apps.docs.DRIVE_OPEN_REDIRECT"

    invoke-direct {p0, v0, p1}, LTS;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 64
    if-nez v0, :cond_1

    .line 66
    invoke-direct {p0, p1}, LTS;->b(Ljava/lang/String;)LTY;

    move-result-object v0

    .line 78
    :goto_1
    return-object v0

    .line 60
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 69
    :cond_1
    iget-object v1, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 70
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v0, Landroid/content/pm/ActivityInfo;->targetActivity:Ljava/lang/String;

    .line 72
    const-string v0, "com.google.android.apps.docs.DRIVE_CREATE_REDIRECT"

    invoke-direct {p0, v2, v0, p1}, LTS;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 74
    if-eqz v0, :cond_2

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->targetActivity:Ljava/lang/String;

    .line 77
    :goto_2
    invoke-direct {p0, v2}, LTS;->a(Ljava/lang/String;)Z

    move-result v4

    .line 78
    new-instance v1, LTY;

    invoke-direct {v1, v2, v3, v0, v4}, LTY;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object v0, v1

    goto :goto_1

    .line 74
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 143
    iget-object v2, p0, LTS;->a:Lakk;

    invoke-virtual {v2, p1}, Lakk;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 169
    :goto_0
    return-object v0

    .line 147
    :cond_0
    iget-object v2, p0, LTS;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 150
    const/16 v3, 0x88

    :try_start_0
    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 157
    iget-object v2, v0, Landroid/content/pm/PackageInfo;->providers:[Landroid/content/pm/ProviderInfo;

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 158
    iget-object v5, v4, Landroid/content/pm/ProviderInfo;->metaData:Landroid/os/Bundle;

    .line 159
    if-eqz v5, :cond_1

    .line 160
    const-string v6, "com.google.android.apps.docs.crossAppStateSyncEnabled"

    .line 161
    invoke-virtual {v5, v6, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 162
    if-eqz v5, :cond_1

    .line 163
    iget-object v0, v4, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    goto :goto_0

    .line 152
    :catch_0
    move-exception v2

    .line 153
    const-string v3, "AppFinderUtils"

    const-string v4, "Error querying providers on package %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p1, v5, v1

    invoke-static {v3, v2, v4, v5}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 157
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 169
    :cond_2
    sget-object v0, LTS;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public a()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 207
    iget-object v0, p0, LTS;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 209
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.apps.docs.statesyncer.DOCUMENT_PIN_STATE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 212
    const/16 v2, 0x220

    .line 213
    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 216
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 217
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 236
    :goto_0
    return-object v0

    .line 220
    :cond_0
    iget-object v1, p0, LTS;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 221
    new-instance v2, LbmH;

    invoke-direct {v2}, LbmH;-><init>()V

    .line 223
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 224
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 225
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 230
    invoke-virtual {p0, v0}, LTS;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 231
    if-eqz v0, :cond_1

    .line 232
    invoke-virtual {v2, v0}, LbmH;->a(Ljava/lang/Object;)LbmH;

    goto :goto_1

    .line 236
    :cond_2
    invoke-virtual {v2}, LbmH;->a()LbmF;

    move-result-object v0

    goto :goto_0
.end method
