.class public Lalp;
.super Ljava/lang/Object;
.source "FileUtilitiesImpl.java"

# interfaces
.implements Lalo;


# static fields
.field public static final a:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-string v0, "[|\\\\?*<\":>+\\[\\]/\\p{Cntrl}]"

    .line 58
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lalp;->a:Ljava/util/regex/Pattern;

    .line 57
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xff

    .line 289
    const-string v0, ""

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "\\.+"

    invoke-virtual {p0, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "\\s+"

    invoke-virtual {p0, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 290
    :cond_0
    const-string v0, "file"

    .line 305
    :cond_1
    :goto_0
    return-object v0

    .line 301
    :cond_2
    sget-object v0, Lalp;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 302
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v2, :cond_1

    .line 303
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 3

    .prologue
    .line 162
    const/16 v0, 0x4000

    new-array v0, v0, [B

    .line 164
    :goto_0
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 165
    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2, v1}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0

    .line 167
    :cond_0
    return-void
.end method

.method public static b(Ljava/io/File;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 319
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 321
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 322
    invoke-static {v4}, Lalp;->b(Ljava/io/File;)Z

    move-result v4

    .line 323
    if-nez v4, :cond_0

    .line 328
    :goto_1
    return v0

    .line 321
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 328
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v0

    goto :goto_1
.end method


# virtual methods
.method public a(LadY;)J
    .locals 6

    .prologue
    .line 245
    invoke-virtual {p1}, LadY;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 246
    invoke-virtual {p1}, LadY;->a()J

    move-result-wide v2

    .line 255
    :cond_0
    return-wide v2

    .line 248
    :cond_1
    invoke-virtual {p1}, LadY;->a()J

    move-result-wide v0

    .line 249
    invoke-virtual {p1}, LadY;->a()LbmY;

    move-result-object v2

    invoke-virtual {v2}, LbmY;->a()Lbqv;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LadY;

    .line 250
    invoke-virtual {p0, v0}, Lalp;->a(LadY;)J

    move-result-wide v0

    .line 251
    cmp-long v5, v0, v2

    if-lez v5, :cond_2

    :goto_1
    move-wide v2, v0

    .line 254
    goto :goto_0

    :cond_2
    move-wide v0, v2

    goto :goto_1
.end method

.method public a(Ljava/io/File;)J
    .locals 2

    .prologue
    .line 239
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    new-instance v0, LadY;

    invoke-direct {v0, p1}, LadY;-><init>(Ljava/io/File;)V

    invoke-virtual {p0, v0}, Lalp;->a(LadY;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Lorg/apache/http/Header;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 186
    if-nez p1, :cond_1

    .line 187
    const-string v0, "text/plain"

    .line 191
    :cond_0
    :goto_0
    return-object v0

    .line 189
    :cond_1
    invoke-interface {p1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 190
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 191
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/security/Key;Ljava/io/OutputStream;)Ljavax/crypto/CipherOutputStream;
    .locals 2

    .prologue
    .line 116
    :try_start_0
    invoke-interface {p1}, Ljava/security/Key;->getAlgorithm()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 117
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 119
    new-instance v1, Ljavax/crypto/CipherOutputStream;

    invoke-direct {v1, p2, v0}, Ljavax/crypto/CipherOutputStream;-><init>(Ljava/io/OutputStream;Ljavax/crypto/Cipher;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_2

    return-object v1

    .line 120
    :catch_0
    move-exception v0

    .line 121
    new-instance v1, LQm;

    invoke-direct {v1, v0}, LQm;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 122
    :catch_1
    move-exception v0

    .line 123
    new-instance v1, LQm;

    invoke-direct {v1, v0}, LQm;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 124
    :catch_2
    move-exception v0

    .line 125
    new-instance v1, LQm;

    invoke-direct {v1, v0}, LQm;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(LadY;Ljava/io/File;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 69
    .line 72
    :try_start_0
    invoke-virtual {p1}, LadY;->a()Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 73
    :try_start_1
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 75
    :try_start_2
    invoke-direct {p0, v3, v1}, Lalp;->b(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 77
    if-eqz v3, :cond_0

    .line 78
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 81
    :cond_0
    if-eqz v1, :cond_1

    .line 82
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 85
    :cond_1
    return-void

    .line 77
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_0
    if-eqz v2, :cond_2

    .line 78
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 81
    :cond_2
    if-eqz v1, :cond_3

    .line 82
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    :cond_3
    throw v0

    .line 77
    :catchall_1
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_0

    :catchall_2
    move-exception v0

    move-object v2, v3

    goto :goto_0
.end method

.method public a(LadY;Ljava/io/File;Ljava/security/Key;Ljava/lang/String;Lamr;)V
    .locals 6

    .prologue
    .line 269
    invoke-virtual {p1}, LadY;->a()Ljava/io/InputStream;

    move-result-object v0

    .line 270
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 272
    invoke-virtual {p1}, LadY;->b()J

    move-result-wide v2

    .line 273
    new-instance v4, Lalq;

    invoke-direct {v4, p0, p5, p4}, Lalq;-><init>(Lalp;Lamr;Ljava/lang/String;)V

    .line 279
    new-instance v5, Lail;

    invoke-direct {v5, v1, v4, v2, v3}, Lail;-><init>(Ljava/io/OutputStream;LagH;J)V

    .line 281
    :try_start_0
    invoke-virtual {p0, p3, v0, v5}, Lalp;->a(Ljava/security/Key;Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 283
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    .line 284
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 286
    return-void

    .line 283
    :catchall_0
    move-exception v1

    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    .line 284
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    throw v1
.end method

.method public a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lalp;->a(Ljava/io/InputStream;Ljava/io/OutputStream;Z)V

    .line 97
    return-void
.end method

.method public a(Ljava/io/InputStream;Ljava/io/OutputStream;Z)V
    .locals 1

    .prologue
    .line 103
    :try_start_0
    invoke-direct {p0, p1, p2}, Lalp;->b(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 106
    if-eqz p3, :cond_0

    .line 107
    invoke-virtual {p2}, Ljava/io/OutputStream;->close()V

    .line 110
    :cond_0
    return-void

    .line 105
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 106
    if-eqz p3, :cond_1

    .line 107
    invoke-virtual {p2}, Ljava/io/OutputStream;->close()V

    :cond_1
    throw v0
.end method

.method public a(Ljava/security/Key;Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 4

    .prologue
    .line 135
    :try_start_0
    invoke-interface {p1}, Ljava/security/Key;->getAlgorithm()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 136
    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 138
    const/16 v1, 0x4000

    new-array v1, v1, [B

    .line 140
    :cond_0
    :goto_0
    invoke-virtual {p2, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 141
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Ljavax/crypto/Cipher;->update([BII)[B

    move-result-object v2

    .line 142
    if-eqz v2, :cond_0

    .line 143
    invoke-virtual {p3, v2}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_0

    .line 147
    :catch_0
    move-exception v0

    .line 148
    new-instance v1, LQm;

    invoke-direct {v1, v0}, LQm;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 146
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Ljavax/crypto/Cipher;->doFinal()[B

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljavax/crypto/BadPaddingException; {:try_start_1 .. :try_end_1} :catch_4

    .line 158
    return-void

    .line 149
    :catch_1
    move-exception v0

    .line 150
    new-instance v1, LQm;

    invoke-direct {v1, v0}, LQm;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 151
    :catch_2
    move-exception v0

    .line 152
    new-instance v1, LQm;

    invoke-direct {v1, v0}, LQm;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 153
    :catch_3
    move-exception v0

    .line 154
    new-instance v1, LQm;

    invoke-direct {v1, v0}, LQm;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 155
    :catch_4
    move-exception v0

    .line 156
    new-instance v1, LQm;

    invoke-direct {v1, v0}, LQm;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(LadY;)Z
    .locals 1

    .prologue
    .line 176
    invoke-virtual {p1}, LadY;->a()Z

    move-result v0

    return v0
.end method

.method public a(Ljava/io/File;)Z
    .locals 1

    .prologue
    .line 171
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method public a(Ljava/io/InputStream;)[B
    .locals 1

    .prologue
    .line 196
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 197
    invoke-virtual {p0, p1, v0}, Lalp;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 198
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method
