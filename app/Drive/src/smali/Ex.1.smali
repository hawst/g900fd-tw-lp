.class public LEx;
.super Ljava/lang/Object;
.source "AppInstalledDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, LEx;->a:Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 68
    iget-object v0, p0, LEx;->a:Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;->a:LsP;

    iget-object v1, p0, LEx;->a:Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;

    invoke-static {v1}, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;->a(Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;)LaGu;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    invoke-virtual {v0, v1, v2}, LsP;->a(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)Landroid/content/Intent;

    move-result-object v0

    .line 69
    const-string v1, "editMode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 70
    iget-object v1, p0, LEx;->a:Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;->a()LH;

    move-result-object v1

    invoke-virtual {v1, v0}, LH;->startActivity(Landroid/content/Intent;)V

    .line 71
    return-void
.end method
