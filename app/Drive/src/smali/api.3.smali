.class public final Lapi;
.super Ljava/lang/Object;
.source "ThumbnailFetcherProvider.java"

# interfaces
.implements Lbxw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbxw",
        "<",
        "Lapd;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LQr;

.field private final a:Lapf;


# direct methods
.method public constructor <init>(Lapf;LQr;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapf;

    iput-object v0, p0, Lapi;->a:Lapf;

    .line 22
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p0, Lapi;->a:LQr;

    .line 23
    return-void
.end method


# virtual methods
.method public a()Lapd;
    .locals 6

    .prologue
    .line 27
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v0

    .line 28
    iget-object v2, p0, Lapi;->a:LQr;

    const-string v3, "maxRamForThumbnailBitmapCacheFraction"

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 29
    invoke-interface {v2, v3, v4, v5}, LQr;->a(Ljava/lang/String;D)D

    move-result-wide v2

    .line 30
    long-to-double v0, v0

    mul-double/2addr v0, v2

    double-to-long v0, v0

    .line 31
    iget-object v2, p0, Lapi;->a:Lapf;

    const-string v3, "thumbnailFetcher"

    invoke-virtual {v2, v0, v1, v3}, Lapf;->a(JLjava/lang/String;)Lapd;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lapi;->a()Lapd;

    move-result-object v0

    return-object v0
.end method
