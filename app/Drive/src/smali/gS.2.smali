.class LgS;
.super Ljava/lang/Object;
.source "MaterialProgressDrawable.java"

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;


# instance fields
.field final synthetic a:LgN;


# direct methods
.method constructor <init>(LgN;)V
    .locals 0

    .prologue
    .line 389
    iput-object p1, p0, LgS;->a:LgN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, LgS;->a:LgN;

    invoke-virtual {v0}, LgN;->invalidateSelf()V

    .line 393
    return-void
.end method

.method public scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, LgS;->a:LgN;

    invoke-virtual {v0, p2, p3, p4}, LgN;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 398
    return-void
.end method

.method public unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, LgS;->a:LgN;

    invoke-virtual {v0, p2}, LgN;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 403
    return-void
.end method
