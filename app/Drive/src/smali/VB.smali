.class public LVB;
.super Ljava/lang/Object;
.source "UndoBannerHandler.java"


# annotations
.annotation runtime Lbxz;
.end annotation


# instance fields
.field private final a:I

.field private a:LVL;

.field private final a:Landroid/view/View$OnClickListener;

.field private a:Landroid/view/View;

.field private a:Landroid/widget/LinearLayout;

.field private a:Landroid/widget/PopupWindow;

.field private a:Ljava/lang/String;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lrm;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lrm;

.field private a:Lro;

.field private final a:Lrp;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LVK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation runtime Lajg;
        .end annotation
    .end param

    .prologue
    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    new-instance v0, LVC;

    invoke-direct {v0, p0}, LVC;-><init>(LVB;)V

    iput-object v0, p0, LVB;->a:Landroid/view/View$OnClickListener;

    .line 115
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LVB;->a:Ljava/util/Set;

    .line 125
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LVB;->b:Ljava/util/Set;

    .line 135
    new-instance v0, LVD;

    invoke-direct {v0, p0}, LVD;-><init>(LVB;)V

    iput-object v0, p0, LVB;->a:Lrp;

    .line 145
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxa;->m_snackbar_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, LVB;->a:I

    .line 146
    return-void
.end method

.method private a(Landroid/view/View;Landroid/app/Activity;)I
    .locals 5

    .prologue
    .line 309
    invoke-static {p2}, LakZ;->a(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    .line 310
    invoke-virtual {p2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 311
    invoke-static {v1}, LakQ;->a(Landroid/content/res/Resources;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 312
    iget v0, v0, Landroid/graphics/Point;->x:I

    .line 321
    :goto_0
    return v0

    .line 314
    :cond_0
    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v3, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {p1, v2, v3}, Landroid/view/View;->measure(II)V

    .line 315
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 316
    sget v3, Lxa;->m_snackbar_max_width:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 317
    sget v4, Lxa;->m_snackbar_min_width:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 318
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget v2, p0, LVB;->a:I

    mul-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)Landroid/widget/LinearLayout;
    .locals 3

    .prologue
    .line 151
    const-string v0, "layout_inflater"

    .line 152
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 153
    sget v1, Lxe;->undo_banner:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 154
    sget v1, Lxc;->undo_button:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 155
    iget-object v2, p0, LVB;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    sget v1, Lxc;->undo_message:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 158
    iget-object v2, p0, LVB;->a:Ljava/lang/String;

    invoke-static {v2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    return-object v0
.end method

.method static synthetic a(LVB;)Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, LVB;->a:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic a(LVB;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, LVB;->b:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic a(LVB;)Lro;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, LVB;->a:Lro;

    return-object v0
.end method

.method static synthetic a(LVB;Lro;)Lro;
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, LVB;->a:Lro;

    return-object p1
.end method

.method static synthetic a(LVB;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, LVB;->c()V

    return-void
.end method

.method static synthetic a(LVB;Ljava/lang/String;LVL;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, LVB;->b(Ljava/lang/String;LVL;)V

    return-void
.end method

.method static synthetic a(LVB;Lrm;Z)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, LVB;->c(Lrm;Z)V

    return-void
.end method

.method static synthetic a(LVB;Z)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1}, LVB;->b(Z)V

    return-void
.end method

.method private a(Lrm;Z)V
    .locals 0

    .prologue
    .line 229
    invoke-direct {p0, p1, p2}, LVB;->b(Lrm;Z)V

    .line 230
    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, LVB;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    :goto_0
    return-void

    .line 187
    :cond_0
    iget-object v0, p0, LVB;->a:Ljava/util/Set;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbnm;->a(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrm;

    .line 188
    invoke-direct {p0, v0, p1}, LVB;->a(Lrm;Z)V

    goto :goto_0
.end method

.method private a(ZZ)V
    .locals 2

    .prologue
    .line 358
    invoke-static {}, Lanj;->a()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LVH;

    invoke-direct {v1, p0, p1, p2}, LVH;-><init>(LVB;ZZ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 370
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, LVB;->a:LVL;

    if-eqz v0, :cond_0

    .line 222
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LVB;->b(Z)V

    .line 223
    iget-object v0, p0, LVB;->a:LVL;

    invoke-interface {v0}, LVL;->a()V

    .line 224
    const/4 v0, 0x0

    iput-object v0, p0, LVB;->a:LVL;

    .line 226
    :cond_0
    return-void
.end method

.method static synthetic b(LVB;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, LVB;->b()V

    return-void
.end method

.method static synthetic b(LVB;Z)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1}, LVB;->a(Z)V

    return-void
.end method

.method private b(Ljava/lang/String;LVL;)V
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, LVB;->a:LVL;

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, LVB;->a:LVL;

    invoke-interface {v0}, LVL;->b()V

    .line 205
    const/4 v0, 0x0

    iput-object v0, p0, LVB;->a:LVL;

    .line 207
    :cond_0
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LVL;

    iput-object v0, p0, LVB;->a:LVL;

    .line 208
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LVB;->a:Ljava/lang/String;

    .line 209
    return-void
.end method

.method private b(Lrm;Z)V
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, LVB;->a:LVL;

    if-nez v0, :cond_0

    .line 256
    :goto_0
    return-void

    .line 237
    :cond_0
    iget-object v0, p0, LVB;->a:Lrm;

    if-eqz v0, :cond_1

    .line 238
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LVB;->b(Z)V

    .line 241
    :cond_1
    iput-object p1, p0, LVB;->a:Lrm;

    .line 243
    invoke-virtual {p1}, Lrm;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 244
    invoke-direct {p0, p1, p2}, LVB;->c(Lrm;Z)V

    goto :goto_0

    .line 246
    :cond_2
    new-instance v0, LVF;

    invoke-direct {v0, p0, p1, p2}, LVF;-><init>(LVB;Lrm;Z)V

    iput-object v0, p0, LVB;->a:Lro;

    .line 254
    iget-object v0, p0, LVB;->a:Lro;

    invoke-virtual {p1, v0}, Lrm;->a(Lro;)V

    goto :goto_0
.end method

.method private b(Z)V
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, LVB;->a:Lrm;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    invoke-direct {p0, p1}, LVB;->c(Z)V

    .line 328
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 344
    iget-object v0, p0, LVB;->a:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, LVB;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 346
    iput-object v2, p0, LVB;->a:Landroid/widget/PopupWindow;

    .line 352
    :goto_0
    iget-object v0, p0, LVB;->a:Lrm;

    iget-object v1, p0, LVB;->a:Lrp;

    invoke-virtual {v0, v1}, Lrm;->b(Lrp;)V

    .line 353
    iput-object v2, p0, LVB;->a:Lrm;

    .line 354
    iput-object v2, p0, LVB;->a:Landroid/view/View;

    .line 355
    return-void

    .line 348
    :cond_0
    iget-object v0, p0, LVB;->a:Lro;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LbiT;->b(Z)V

    .line 349
    iget-object v0, p0, LVB;->a:Lrm;

    iget-object v1, p0, LVB;->a:Lro;

    invoke-virtual {v0, v1}, Lrm;->b(Lro;)V

    .line 350
    iput-object v2, p0, LVB;->a:Lro;

    goto :goto_0

    .line 348
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private c(Lrm;)V
    .locals 5

    .prologue
    .line 270
    invoke-virtual {p1}, Lrm;->b()Z

    move-result v0

    invoke-static {v0}, LbiT;->a(Z)V

    .line 272
    invoke-direct {p0, p1}, LVB;->a(Landroid/content/Context;)Landroid/widget/LinearLayout;

    move-result-object v0

    iput-object v0, p0, LVB;->a:Landroid/widget/LinearLayout;

    .line 274
    iget-object v0, p0, LVB;->a:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0, p1}, LVB;->a(Landroid/view/View;Landroid/app/Activity;)I

    move-result v0

    .line 275
    invoke-virtual {p1}, Lrm;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lxa;->m_snackbar_height_single_line:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 276
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v0, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 277
    iget-object v3, p0, LVB;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 279
    new-instance v2, Landroid/widget/PopupWindow;

    iget-object v3, p0, LVB;->a:Landroid/widget/LinearLayout;

    invoke-direct {v2, v3, v0, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    iput-object v2, p0, LVB;->a:Landroid/widget/PopupWindow;

    .line 280
    iget-object v0, p0, LVB;->a:Landroid/widget/PopupWindow;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const v2, 0x106000d

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 281
    iget-object v0, p0, LVB;->a:Landroid/widget/PopupWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 282
    iget-object v0, p0, LVB;->a:Landroid/widget/PopupWindow;

    new-instance v1, LVG;

    invoke-direct {v1, p0}, LVG;-><init>(LVB;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setTouchInterceptor(Landroid/view/View$OnTouchListener;)V

    .line 294
    iget-object v0, p0, LVB;->a:Landroid/widget/PopupWindow;

    invoke-virtual {p1}, Lrm;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x53

    iget v3, p0, LVB;->a:I

    iget v4, p0, LVB;->a:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 297
    iget-object v0, p0, LVB;->a:Lrp;

    invoke-virtual {p1, v0}, Lrm;->a(Lrp;)V

    .line 299
    invoke-virtual {p1}, Lrm;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, LwY;->animate_action_button_when_snackbar_shown:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    invoke-virtual {p1}, Lrm;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 301
    sget v1, Lxc;->doclist_create_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LVB;->a:Landroid/view/View;

    .line 305
    :goto_0
    return-void

    .line 303
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LVB;->a:Landroid/view/View;

    goto :goto_0
.end method

.method private c(Lrm;Z)V
    .locals 2

    .prologue
    .line 259
    invoke-direct {p0, p1}, LVB;->c(Lrm;)V

    .line 261
    if-eqz p2, :cond_0

    .line 262
    sget v0, LwV;->snackbar_transition_in:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 263
    iget-object v1, p0, LVB;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 266
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0, p2}, LVB;->a(ZZ)V

    .line 267
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 331
    if-eqz p1, :cond_0

    .line 332
    iget-object v0, p0, LVB;->a:Lrm;

    sget v1, LwV;->snackbar_transition_out:I

    .line 333
    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 334
    new-instance v1, LVJ;

    iget-object v2, p0, LVB;->a:Landroid/widget/PopupWindow;

    invoke-direct {v1, p0, v2}, LVJ;-><init>(LVB;Landroid/widget/PopupWindow;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 335
    iget-object v1, p0, LVB;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 340
    :goto_0
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, LVB;->a(ZZ)V

    .line 341
    return-void

    .line 337
    :cond_0
    invoke-direct {p0}, LVB;->c()V

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 376
    iget-object v0, p0, LVB;->a:Lrm;

    if-eqz v0, :cond_0

    iget-object v0, p0, LVB;->a:LVL;

    if-nez v0, :cond_1

    .line 377
    :cond_0
    const/4 v0, 0x0

    .line 388
    :goto_0
    return-object v0

    .line 379
    :cond_1
    iget-object v0, p0, LVB;->a:Lrm;

    invoke-static {v0}, LakZ;->a(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    .line 380
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 381
    iget-object v2, p0, LVB;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->getLocationOnScreen([I)V

    .line 382
    iget-object v1, p0, LVB;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v1

    .line 383
    iget-object v2, p0, LVB;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v2

    .line 384
    iget v3, v0, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, v1

    div-int/lit8 v3, v3, 0x2

    .line 385
    iget v0, v0, Landroid/graphics/Point;->y:I

    iget v4, p0, LVB;->a:I

    sub-int/2addr v0, v4

    sub-int v4, v0, v2

    .line 386
    add-int/2addr v1, v3

    .line 387
    add-int/2addr v2, v4

    .line 388
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v3, v4, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0
.end method

.method public a()V
    .locals 4

    .prologue
    .line 213
    iget-object v0, p0, LVB;->a:LVL;

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, LVB;->a:LVL;

    invoke-interface {v0}, LVL;->b()V

    .line 215
    const/4 v0, 0x0

    iput-object v0, p0, LVB;->a:LVL;

    .line 216
    invoke-static {}, Lanj;->a()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LVI;

    iget-object v2, p0, LVB;->a:Landroid/widget/PopupWindow;

    invoke-direct {v1, p0, v2}, LVI;-><init>(LVB;Landroid/widget/PopupWindow;)V

    const-wide/16 v2, 0x2bc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 218
    :cond_0
    return-void
.end method

.method public a(LVK;)V
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, LVB;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 393
    return-void
.end method

.method public a(Ljava/lang/String;LVL;)V
    .locals 4

    .prologue
    .line 193
    invoke-static {}, Lanj;->a()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LVE;

    invoke-direct {v1, p0, p1, p2}, LVE;-><init>(LVB;Ljava/lang/String;LVL;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 200
    return-void
.end method

.method public a(Lrm;)V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, LVB;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 165
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LVB;->a(Z)V

    .line 166
    return-void
.end method

.method public b(LVK;)V
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, LVB;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 397
    return-void
.end method

.method public b(Lrm;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 169
    iget-object v0, p0, LVB;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    const-string v0, "UndoBannerHandler"

    const-string v1, "Removing activity that has not been added! "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 180
    :goto_0
    return-void

    .line 174
    :cond_0
    iget-object v0, p0, LVB;->a:Lrm;

    if-eqz v0, :cond_1

    .line 175
    invoke-direct {p0, v3}, LVB;->b(Z)V

    .line 178
    :cond_1
    iget-object v0, p0, LVB;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 179
    invoke-direct {p0, v3}, LVB;->a(Z)V

    goto :goto_0
.end method
