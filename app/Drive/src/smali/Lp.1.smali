.class public LLp;
.super Ljava/lang/Object;
.source "SelectionDropTargetUpdater.java"


# instance fields
.field private final a:LLr;

.field private final a:LMa;

.field private final a:LMe;

.field private final a:LalK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LalK",
            "<+",
            "LLs;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Landroid/view/View;

.field private final a:LarM;


# direct methods
.method public constructor <init>(Landroid/view/View;I)V
    .locals 3

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, LarM;

    invoke-direct {v0}, LarM;-><init>()V

    iput-object v0, p0, LLp;->a:LarM;

    .line 43
    new-instance v0, LLr;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LLr;-><init>(LLp;LLq;)V

    iput-object v0, p0, LLp;->a:LLr;

    .line 50
    iput-object p1, p0, LLp;->a:Landroid/view/View;

    .line 51
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 52
    sget v1, LwZ;->selection_active_drop_target_fg:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 53
    sget v2, LwZ;->selection_active_drop_target_bg:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 55
    new-instance v2, LMe;

    invoke-direct {v2, v1}, LMe;-><init>(I)V

    iput-object v2, p0, LLp;->a:LMe;

    .line 56
    new-instance v2, LMa;

    invoke-direct {v2, v1}, LMa;-><init>(I)V

    iput-object v2, p0, LLp;->a:LMa;

    .line 57
    new-instance v1, LMf;

    invoke-direct {v1, v0}, LMf;-><init>(I)V

    invoke-static {p2, v1}, LalK;->a(ILjava/lang/Object;)LalK;

    move-result-object v0

    iput-object v0, p0, LLp;->a:LalK;

    .line 59
    return-void
.end method

.method static synthetic a(LLp;)LMa;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, LLp;->a:LMa;

    return-object v0
.end method

.method static synthetic a(LLp;)LMe;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, LLp;->a:LMe;

    return-object v0
.end method

.method static synthetic a(LLp;)LalK;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, LLp;->a:LalK;

    return-object v0
.end method


# virtual methods
.method public a(Z)V
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, LLp;->a:LLr;

    invoke-virtual {v0, p1}, LLr;->a(Z)V

    .line 63
    iget-object v0, p0, LLp;->a:LarM;

    iget-object v1, p0, LLp;->a:Landroid/view/View;

    iget-object v2, p0, LLp;->a:LLr;

    invoke-virtual {v0, v1, v2}, LarM;->a(Landroid/view/View;LarN;)V

    .line 64
    return-void
.end method
