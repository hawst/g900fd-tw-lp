.class Lbfa;
.super LbeS;
.source "GsonParser.java"


# instance fields
.field private a:LbeX;

.field private final a:LbeY;

.field private final a:Lbuj;

.field private a:Ljava/lang/String;

.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LbeY;Lbuj;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, LbeS;-><init>()V

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbfa;->a:Ljava/util/List;

    .line 48
    iput-object p1, p0, Lbfa;->a:LbeY;

    .line 49
    iput-object p2, p0, Lbfa;->a:Lbuj;

    .line 51
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lbuj;->a(Z)V

    .line 52
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lbfa;->a:LbeX;

    sget-object v1, LbeX;->g:LbeX;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lbfa;->a:LbeX;

    sget-object v1, LbeX;->h:LbeX;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbfN;->a(Z)V

    .line 126
    return-void

    .line 124
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()B
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Lbfa;->b()V

    .line 77
    iget-object v0, p0, Lbfa;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(Ljava/lang/String;)Ljava/lang/Byte;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    return v0
.end method

.method public a()D
    .locals 2

    .prologue
    .line 113
    invoke-direct {p0}, Lbfa;->b()V

    .line 114
    iget-object v0, p0, Lbfa;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    return-wide v0
.end method

.method public a()F
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0}, Lbfa;->b()V

    .line 96
    iget-object v0, p0, Lbfa;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method public a()I
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Lbfa;->b()V

    .line 90
    iget-object v0, p0, Lbfa;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public a()J
    .locals 2

    .prologue
    .line 119
    invoke-direct {p0}, Lbfa;->b()V

    .line 120
    iget-object v0, p0, Lbfa;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public a()LbeO;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lbfa;->a:LbeY;

    return-object v0
.end method

.method public a()LbeS;
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Lbfa;->a:LbeX;

    if-eqz v0, :cond_0

    .line 216
    sget-object v0, Lbfb;->a:[I

    iget-object v1, p0, Lbfa;->a:LbeX;

    invoke-virtual {v1}, LbeX;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 231
    :cond_0
    :goto_0
    return-object p0

    .line 218
    :pswitch_0
    iget-object v0, p0, Lbfa;->a:Lbuj;

    invoke-virtual {v0}, Lbuj;->f()V

    .line 219
    const-string v0, "]"

    iput-object v0, p0, Lbfa;->a:Ljava/lang/String;

    .line 220
    sget-object v0, LbeX;->b:LbeX;

    iput-object v0, p0, Lbfa;->a:LbeX;

    goto :goto_0

    .line 223
    :pswitch_1
    iget-object v0, p0, Lbfa;->a:Lbuj;

    invoke-virtual {v0}, Lbuj;->f()V

    .line 224
    const-string v0, "}"

    iput-object v0, p0, Lbfa;->a:Ljava/lang/String;

    .line 225
    sget-object v0, LbeX;->d:LbeX;

    iput-object v0, p0, Lbfa;->a:LbeX;

    goto :goto_0

    .line 216
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a()LbeX;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 135
    iget-object v0, p0, Lbfa;->a:LbeX;

    if-eqz v0, :cond_0

    .line 136
    sget-object v0, Lbfb;->a:[I

    iget-object v1, p0, Lbfa;->a:LbeX;

    invoke-virtual {v1}, LbeX;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 153
    :cond_0
    :goto_0
    :try_start_0
    iget-object v0, p0, Lbfa;->a:Lbuj;

    invoke-virtual {v0}, Lbuj;->a()Lbul;
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 157
    :goto_1
    sget-object v1, Lbfb;->b:[I

    invoke-virtual {v0}, Lbul;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1

    .line 207
    iput-object v2, p0, Lbfa;->a:Ljava/lang/String;

    .line 208
    iput-object v2, p0, Lbfa;->a:LbeX;

    .line 210
    :goto_2
    iget-object v0, p0, Lbfa;->a:LbeX;

    return-object v0

    .line 138
    :pswitch_0
    iget-object v0, p0, Lbfa;->a:Lbuj;

    invoke-virtual {v0}, Lbuj;->a()V

    .line 139
    iget-object v0, p0, Lbfa;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 142
    :pswitch_1
    iget-object v0, p0, Lbfa;->a:Lbuj;

    invoke-virtual {v0}, Lbuj;->c()V

    .line 143
    iget-object v0, p0, Lbfa;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 154
    :catch_0
    move-exception v0

    .line 155
    sget-object v0, Lbul;->j:Lbul;

    goto :goto_1

    .line 159
    :pswitch_2
    const-string v0, "["

    iput-object v0, p0, Lbfa;->a:Ljava/lang/String;

    .line 160
    sget-object v0, LbeX;->a:LbeX;

    iput-object v0, p0, Lbfa;->a:LbeX;

    goto :goto_2

    .line 163
    :pswitch_3
    const-string v0, "]"

    iput-object v0, p0, Lbfa;->a:Ljava/lang/String;

    .line 164
    sget-object v0, LbeX;->b:LbeX;

    iput-object v0, p0, Lbfa;->a:LbeX;

    .line 165
    iget-object v0, p0, Lbfa;->a:Ljava/util/List;

    iget-object v1, p0, Lbfa;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 166
    iget-object v0, p0, Lbfa;->a:Lbuj;

    invoke-virtual {v0}, Lbuj;->b()V

    goto :goto_2

    .line 169
    :pswitch_4
    const-string v0, "{"

    iput-object v0, p0, Lbfa;->a:Ljava/lang/String;

    .line 170
    sget-object v0, LbeX;->c:LbeX;

    iput-object v0, p0, Lbfa;->a:LbeX;

    goto :goto_2

    .line 173
    :pswitch_5
    const-string v0, "}"

    iput-object v0, p0, Lbfa;->a:Ljava/lang/String;

    .line 174
    sget-object v0, LbeX;->d:LbeX;

    iput-object v0, p0, Lbfa;->a:LbeX;

    .line 175
    iget-object v0, p0, Lbfa;->a:Ljava/util/List;

    iget-object v1, p0, Lbfa;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 176
    iget-object v0, p0, Lbfa;->a:Lbuj;

    invoke-virtual {v0}, Lbuj;->d()V

    goto :goto_2

    .line 179
    :pswitch_6
    iget-object v0, p0, Lbfa;->a:Lbuj;

    invoke-virtual {v0}, Lbuj;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 180
    const-string v0, "true"

    iput-object v0, p0, Lbfa;->a:Ljava/lang/String;

    .line 181
    sget-object v0, LbeX;->i:LbeX;

    iput-object v0, p0, Lbfa;->a:LbeX;

    goto :goto_2

    .line 183
    :cond_1
    const-string v0, "false"

    iput-object v0, p0, Lbfa;->a:Ljava/lang/String;

    .line 184
    sget-object v0, LbeX;->j:LbeX;

    iput-object v0, p0, Lbfa;->a:LbeX;

    goto :goto_2

    .line 188
    :pswitch_7
    const-string v0, "null"

    iput-object v0, p0, Lbfa;->a:Ljava/lang/String;

    .line 189
    sget-object v0, LbeX;->k:LbeX;

    iput-object v0, p0, Lbfa;->a:LbeX;

    .line 190
    iget-object v0, p0, Lbfa;->a:Lbuj;

    invoke-virtual {v0}, Lbuj;->e()V

    goto/16 :goto_2

    .line 193
    :pswitch_8
    iget-object v0, p0, Lbfa;->a:Lbuj;

    invoke-virtual {v0}, Lbuj;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbfa;->a:Ljava/lang/String;

    .line 194
    sget-object v0, LbeX;->f:LbeX;

    iput-object v0, p0, Lbfa;->a:LbeX;

    goto/16 :goto_2

    .line 197
    :pswitch_9
    iget-object v0, p0, Lbfa;->a:Lbuj;

    invoke-virtual {v0}, Lbuj;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbfa;->a:Ljava/lang/String;

    .line 198
    iget-object v0, p0, Lbfa;->a:Ljava/lang/String;

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    sget-object v0, LbeX;->g:LbeX;

    :goto_3
    iput-object v0, p0, Lbfa;->a:LbeX;

    goto/16 :goto_2

    :cond_2
    sget-object v0, LbeX;->h:LbeX;

    goto :goto_3

    .line 202
    :pswitch_a
    iget-object v0, p0, Lbfa;->a:Lbuj;

    invoke-virtual {v0}, Lbuj;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbfa;->a:Ljava/lang/String;

    .line 203
    sget-object v0, LbeX;->e:LbeX;

    iput-object v0, p0, Lbfa;->a:LbeX;

    .line 204
    iget-object v0, p0, Lbfa;->a:Ljava/util/List;

    iget-object v1, p0, Lbfa;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lbfa;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 136
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 157
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lbfa;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbfa;->a:Ljava/util/List;

    iget-object v1, p0, Lbfa;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public a()Ljava/math/BigDecimal;
    .locals 2

    .prologue
    .line 107
    invoke-direct {p0}, Lbfa;->b()V

    .line 108
    new-instance v0, Ljava/math/BigDecimal;

    iget-object v1, p0, Lbfa;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public a()Ljava/math/BigInteger;
    .locals 2

    .prologue
    .line 101
    invoke-direct {p0}, Lbfa;->b()V

    .line 102
    new-instance v0, Ljava/math/BigInteger;

    iget-object v1, p0, Lbfa;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public a()S
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0}, Lbfa;->b()V

    .line 83
    iget-object v0, p0, Lbfa;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Short;->valueOf(Ljava/lang/String;)Ljava/lang/Short;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S

    move-result v0

    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lbfa;->a:Lbuj;

    invoke-virtual {v0}, Lbuj;->close()V

    .line 57
    return-void
.end method

.method public b()LbeX;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lbfa;->a:LbeX;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lbfa;->a:Ljava/lang/String;

    return-object v0
.end method
