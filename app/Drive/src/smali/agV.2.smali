.class public final LagV;
.super LagI;
.source "NextFeedMonitorProcessor.java"


# instance fields
.field private a:Ljava/lang/String;

.field private a:Z


# direct methods
.method public constructor <init>(Lagy;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0, p1}, LagI;-><init>(Lagy;)V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, LagV;->a:Ljava/lang/String;

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, LagV;->a:Z

    .line 22
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 43
    iget-boolean v0, p0, LagV;->a:Z

    const-string v1, "Must not call this method before finish()"

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    .line 44
    iget-object v0, p0, LagV;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 26
    iget-boolean v0, p0, LagV;->a:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "Already finished. Did you delegate from more than one FeedProcessor to this one?"

    invoke-static {v0, v2}, LbiT;->b(ZLjava/lang/Object;)V

    .line 28
    invoke-super {p0, p1}, LagI;->a(Ljava/lang/String;)V

    .line 32
    iput-object p1, p0, LagV;->a:Ljava/lang/String;

    .line 33
    iput-boolean v1, p0, LagV;->a:Z

    .line 34
    return-void

    .line 26
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 49
    const-string v0, "NextFeedMonitorProcessor[delegate=%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, LagV;->a()Lagy;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
