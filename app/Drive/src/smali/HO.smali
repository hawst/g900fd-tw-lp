.class public LHO;
.super LHt;
.source "IncomingPhotosGridViewBinder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LHt",
        "<",
        "LHN;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final a:LCt;

.field private final a:LDk;

.field private final a:Landroid/support/v4/app/Fragment;

.field private final a:Landroid/view/LayoutInflater;

.field private final a:LtK;

.field private final a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/Fragment;LBf;LDU;LCU;LamF;LDk;ILtK;LQX;Lapd;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;Lapc;LCt;LDg;)V
    .locals 11

    .prologue
    .line 58
    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p6

    move-object/from16 v6, p10

    move-object/from16 v7, p11

    move-object/from16 v8, p12

    move-object/from16 v9, p13

    move-object/from16 v10, p15

    invoke-direct/range {v1 .. v10}, LHt;-><init>(Landroid/content/Context;LBf;LDU;LamF;LQX;Lapd;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;Lapc;LDg;)V

    .line 60
    iput-object p2, p0, LHO;->a:Landroid/support/v4/app/Fragment;

    .line 61
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    iput-object v1, p0, LHO;->a:Landroid/view/LayoutInflater;

    .line 62
    invoke-static/range {p7 .. p7}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LDk;

    iput-object v1, p0, LHO;->a:LDk;

    .line 63
    move/from16 v0, p8

    iput v0, p0, LHO;->a:I

    .line 64
    move-object/from16 v0, p9

    iput-object v0, p0, LHO;->a:LtK;

    .line 65
    invoke-virtual/range {p5 .. p5}, LCU;->a()Z

    move-result v1

    iput-boolean v1, p0, LHO;->a:Z

    .line 66
    move-object/from16 v0, p14

    iput-object v0, p0, LHO;->a:LCt;

    .line 67
    return-void
.end method

.method private a(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 106
    instance-of v0, p1, Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;

    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LHN;

    if-nez v0, :cond_1

    .line 108
    :cond_0
    const/4 v0, 0x0

    .line 111
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(ILandroid/view/View;Landroid/view/ViewGroup;Z)LHN;
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 73
    if-nez p4, :cond_0

    move v0, v2

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 76
    invoke-direct {p0, p2}, LHO;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    check-cast p2, Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;

    .line 78
    invoke-virtual {p2}, Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHN;

    .line 79
    invoke-virtual {v0}, LHN;->a()V

    .line 102
    :goto_1
    return-object v0

    :cond_0
    move v0, v3

    .line 73
    goto :goto_0

    .line 81
    :cond_1
    iget-object v0, p0, LHO;->a:Landroid/view/LayoutInflater;

    sget v1, Lxe;->doc_grid_item_incoming_photos:I

    .line 82
    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;

    .line 84
    iget-object v4, p0, LHO;->a:Landroid/view/LayoutInflater;

    iget v5, p0, LHO;->a:I

    sget v1, Lxc;->more_actions_button_container:I

    .line 86
    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 84
    invoke-virtual {v4, v5, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 88
    new-instance v1, LHN;

    iget-object v4, p0, LHO;->a:Lapd;

    iget-object v5, p0, LHO;->a:Lapc;

    iget-object v6, p0, LHO;->a:LDk;

    invoke-direct {v1, v4, v5, v0, v6}, LHN;-><init>(Lapd;Lapc;Landroid/view/View;LDk;)V

    .line 90
    iget-object v4, p0, LHO;->a:Ljava/util/Set;

    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 91
    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;->setTag(Ljava/lang/Object;)V

    .line 92
    iget-object v4, p0, LHO;->a:LtK;

    iget-object v5, p0, LHO;->a:LCt;

    invoke-virtual {v1, v4, v5}, LHN;->a(LtK;LCt;)V

    .line 94
    iget-boolean v4, p0, LHO;->a:Z

    if-nez v4, :cond_2

    iget-object v4, p0, LHO;->a:LtK;

    sget-object v5, Lry;->S:Lry;

    .line 95
    invoke-interface {v4, v5}, LtK;->a(LtJ;)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    move v3, v2

    .line 97
    :cond_3
    if-nez v3, :cond_4

    .line 99
    iget-object v2, p0, LHO;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v2, v0}, Landroid/support/v4/app/Fragment;->a(Landroid/view/View;)V

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method public bridge synthetic a(ILandroid/view/View;Landroid/view/ViewGroup;Z)LHq;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0, p1, p2, p3, p4}, LHO;->a(ILandroid/view/View;Landroid/view/ViewGroup;Z)LHN;

    move-result-object v0

    return-object v0
.end method
