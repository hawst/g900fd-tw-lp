.class public final LawE;
.super Ljava/lang/Object;
.source "ZoomView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)V
    .locals 0

    .prologue
    .line 914
    iput-object p1, p0, LawE;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/viewer/viewer/image/ZoomView;LawD;)V
    .locals 0

    .prologue
    .line 914
    invoke-direct {p0, p1}, LawE;-><init>(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 917
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 918
    iget-object v0, p0, LawE;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0, p1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;Landroid/view/View;)Landroid/view/View;

    .line 919
    iget-object v0, p0, LawE;->a:Lcom/google/android/apps/viewer/viewer/image/ZoomView;

    invoke-static {v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/graphics/Point;

    move-result-object v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 921
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
