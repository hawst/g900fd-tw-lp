.class public final LVq;
.super Lbse;
.source "GellyInjectorStore.java"


# annotations
.annotation build Lcom/google/common/labs/inject/gelly/runtime/GellyGenerated;
.end annotation


# instance fields
.field private a:LbrA;

.field public a:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LVl;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LVA;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LVv;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LVm;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LVx;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LVg;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LVs;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lbsk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbsk",
            "<",
            "LVB;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LbrA;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 44
    invoke-direct {p0, p1}, Lbse;-><init>(LbrS;)V

    .line 45
    iput-object p1, p0, LVq;->a:LbrA;

    .line 46
    const-class v0, LVl;

    invoke-static {v0, v2}, LVq;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LVq;->a:Lbsk;

    .line 49
    const-class v0, LVA;

    invoke-static {v0, v2}, LVq;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LVq;->b:Lbsk;

    .line 52
    const-class v0, LVv;

    invoke-static {v0, v2}, LVq;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LVq;->c:Lbsk;

    .line 55
    const-class v0, LVm;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LVq;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LVq;->d:Lbsk;

    .line 58
    const-class v0, LVx;

    invoke-static {v0, v2}, LVq;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LVq;->e:Lbsk;

    .line 61
    const-class v0, LVg;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LVq;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LVq;->f:Lbsk;

    .line 64
    const-class v0, LVs;

    invoke-static {v0, v2}, LVq;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LVq;->g:Lbsk;

    .line 67
    const-class v0, LVB;

    const-class v1, Lbxz;

    invoke-static {v0, v1}, LVq;->a(Ljava/lang/Class;Ljava/lang/Class;)Lbsk;

    move-result-object v0

    iput-object v0, p0, LVq;->h:Lbsk;

    .line 70
    return-void
.end method


# virtual methods
.method protected a(I)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 109
    sparse-switch p1, :sswitch_data_0

    .line 275
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 111
    :sswitch_0
    new-instance v4, LVl;

    iget-object v0, p0, LVq;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LVq;

    iget-object v0, v0, LVq;->c:Lbsk;

    .line 114
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LVq;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LVq;

    iget-object v1, v1, LVq;->c:Lbsk;

    .line 112
    invoke-static {v0, v1}, LVq;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LVv;

    iget-object v1, p0, LVq;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LVq;

    iget-object v1, v1, LVq;->g:Lbsk;

    .line 120
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LVq;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LVq;

    iget-object v2, v2, LVq;->g:Lbsk;

    .line 118
    invoke-static {v1, v2}, LVq;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LVs;

    iget-object v2, p0, LVq;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LVq;

    iget-object v2, v2, LVq;->e:Lbsk;

    .line 126
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LVq;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LVq;

    iget-object v3, v3, LVq;->e:Lbsk;

    .line 124
    invoke-static {v2, v3}, LVq;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LVx;

    iget-object v3, p0, LVq;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LVq;

    iget-object v3, v3, LVq;->b:Lbsk;

    .line 132
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, LVq;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LVq;

    iget-object v5, v5, LVq;->b:Lbsk;

    .line 130
    invoke-static {v3, v5}, LVq;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LVA;

    invoke-direct {v4, v0, v1, v2, v3}, LVl;-><init>(LVv;LVs;LVx;LVA;)V

    move-object v0, v4

    .line 273
    :goto_0
    return-object v0

    .line 139
    :sswitch_1
    new-instance v1, LVA;

    iget-object v0, p0, LVq;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LUh;

    iget-object v0, v0, LUh;->f:Lbsk;

    .line 142
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LVq;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LUh;

    iget-object v2, v2, LUh;->f:Lbsk;

    .line 140
    invoke-static {v0, v2}, LVq;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUi;

    invoke-direct {v1, v0}, LVA;-><init>(LUi;)V

    move-object v0, v1

    .line 147
    goto :goto_0

    .line 149
    :sswitch_2
    new-instance v0, LVv;

    iget-object v1, p0, LVq;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LqD;

    iget-object v1, v1, LqD;->c:Lbsk;

    .line 152
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LVq;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LqD;

    iget-object v2, v2, LqD;->c:Lbsk;

    .line 150
    invoke-static {v1, v2}, LVq;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LqK;

    iget-object v2, p0, LVq;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LUh;

    iget-object v2, v2, LUh;->f:Lbsk;

    .line 158
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LVq;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LUh;

    iget-object v3, v3, LUh;->f:Lbsk;

    .line 156
    invoke-static {v2, v3}, LVq;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LUi;

    iget-object v3, p0, LVq;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LpG;

    iget-object v3, v3, LpG;->m:Lbsk;

    .line 164
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LVq;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LpG;

    iget-object v4, v4, LpG;->m:Lbsk;

    .line 162
    invoke-static {v3, v4}, LVq;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LtK;

    iget-object v4, p0, LVq;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaGH;

    iget-object v4, v4, LaGH;->l:Lbsk;

    .line 170
    invoke-virtual {v4}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, LVq;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LaGH;

    iget-object v5, v5, LaGH;->l:Lbsk;

    .line 168
    invoke-static {v4, v5}, LVq;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LaGM;

    iget-object v5, p0, LVq;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LTV;

    iget-object v5, v5, LTV;->a:Lbsk;

    .line 176
    invoke-virtual {v5}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, LVq;->a:LbrA;

    iget-object v6, v6, LbrA;->a:LTV;

    iget-object v6, v6, LTV;->a:Lbsk;

    .line 174
    invoke-static {v5, v6}, LVq;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LTT;

    invoke-direct/range {v0 .. v5}, LVv;-><init>(LqK;LUi;LtK;LaGM;LTT;)V

    goto/16 :goto_0

    .line 183
    :sswitch_3
    new-instance v4, LVm;

    iget-object v0, p0, LVq;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->b:Lbsk;

    .line 186
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LVq;->a:LbrA;

    iget-object v1, v1, LbrA;->a:Lc;

    iget-object v1, v1, Lc;->b:Lbsk;

    .line 184
    invoke-static {v0, v1}, LVq;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, LVq;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 192
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LVq;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaGH;

    iget-object v2, v2, LaGH;->l:Lbsk;

    .line 190
    invoke-static {v1, v2}, LVq;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaGM;

    iget-object v2, p0, LVq;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LVq;

    iget-object v2, v2, LVq;->f:Lbsk;

    .line 198
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, LVq;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LVq;

    iget-object v3, v3, LVq;->f:Lbsk;

    .line 196
    invoke-static {v2, v3}, LVq;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LVg;

    iget-object v3, p0, LVq;->a:LbrA;

    iget-object v3, v3, LbrA;->a:LVq;

    iget-object v3, v3, LVq;->h:Lbsk;

    .line 204
    invoke-virtual {v3}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v5, p0, LVq;->a:LbrA;

    iget-object v5, v5, LbrA;->a:LVq;

    iget-object v5, v5, LVq;->h:Lbsk;

    .line 202
    invoke-static {v3, v5}, LVq;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LVB;

    invoke-direct {v4, v0, v1, v2, v3}, LVm;-><init>(Landroid/content/Context;LaGM;LVg;LVB;)V

    move-object v0, v4

    .line 209
    goto/16 :goto_0

    .line 211
    :sswitch_4
    new-instance v1, LVx;

    iget-object v0, p0, LVq;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LUh;

    iget-object v0, v0, LUh;->f:Lbsk;

    .line 214
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LVq;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LUh;

    iget-object v2, v2, LUh;->f:Lbsk;

    .line 212
    invoke-static {v0, v2}, LVq;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUi;

    invoke-direct {v1, v0}, LVx;-><init>(LUi;)V

    move-object v0, v1

    .line 219
    goto/16 :goto_0

    .line 221
    :sswitch_5
    new-instance v3, LVg;

    iget-object v0, p0, LVq;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LVq;

    iget-object v0, v0, LVq;->a:Lbsk;

    .line 224
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LVq;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LVq;

    iget-object v1, v1, LVq;->a:Lbsk;

    .line 222
    invoke-static {v0, v1}, LVq;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LVl;

    iget-object v1, p0, LVq;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LVq;

    iget-object v1, v1, LVq;->h:Lbsk;

    .line 230
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LVq;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LVq;

    iget-object v2, v2, LVq;->h:Lbsk;

    .line 228
    invoke-static {v1, v2}, LVq;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LVB;

    iget-object v2, p0, LVq;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LaGH;

    iget-object v2, v2, LaGH;->l:Lbsk;

    .line 236
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LVq;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LaGH;

    iget-object v4, v4, LaGH;->l:Lbsk;

    .line 234
    invoke-static {v2, v4}, LVq;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LaGM;

    invoke-direct {v3, v0, v1, v2}, LVg;-><init>(LVl;LVB;LaGM;)V

    move-object v0, v3

    .line 241
    goto/16 :goto_0

    .line 243
    :sswitch_6
    new-instance v3, LVs;

    iget-object v0, p0, LVq;->a:LbrA;

    iget-object v0, v0, LbrA;->a:LaGH;

    iget-object v0, v0, LaGH;->l:Lbsk;

    .line 246
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LVq;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LaGH;

    iget-object v1, v1, LaGH;->l:Lbsk;

    .line 244
    invoke-static {v0, v1}, LVq;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iget-object v1, p0, LVq;->a:LbrA;

    iget-object v1, v1, LbrA;->a:LUh;

    iget-object v1, v1, LUh;->f:Lbsk;

    .line 252
    invoke-virtual {v1}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LVq;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LUh;

    iget-object v2, v2, LUh;->f:Lbsk;

    .line 250
    invoke-static {v1, v2}, LVq;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LUi;

    iget-object v2, p0, LVq;->a:LbrA;

    iget-object v2, v2, LbrA;->a:LqD;

    iget-object v2, v2, LqD;->c:Lbsk;

    .line 258
    invoke-virtual {v2}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v2

    iget-object v4, p0, LVq;->a:LbrA;

    iget-object v4, v4, LbrA;->a:LqD;

    iget-object v4, v4, LqD;->c:Lbsk;

    .line 256
    invoke-static {v2, v4}, LVq;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LqK;

    invoke-direct {v3, v0, v1, v2}, LVs;-><init>(LaGM;LUi;LqK;)V

    move-object v0, v3

    .line 263
    goto/16 :goto_0

    .line 265
    :sswitch_7
    new-instance v1, LVB;

    iget-object v0, p0, LVq;->a:LbrA;

    iget-object v0, v0, LbrA;->a:Lc;

    iget-object v0, v0, Lc;->b:Lbsk;

    .line 268
    invoke-virtual {v0}, Lbsk;->a()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, LVq;->a:LbrA;

    iget-object v2, v2, LbrA;->a:Lc;

    iget-object v2, v2, Lc;->b:Lbsk;

    .line 266
    invoke-static {v0, v2}, LVq;->a(Ljava/lang/Object;Lbxw;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LVB;-><init>(Landroid/content/Context;)V

    move-object v0, v1

    .line 273
    goto/16 :goto_0

    .line 109
    nop

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_7
        0xd9 -> :sswitch_3
        0xff -> :sswitch_5
        0x16b -> :sswitch_0
        0x16c -> :sswitch_2
        0x16d -> :sswitch_6
        0x16e -> :sswitch_4
        0x16f -> :sswitch_1
    .end sparse-switch
.end method

.method protected a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 290
    .line 292
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown provides method binding ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 77
    const-class v0, LVl;

    iget-object v1, p0, LVq;->a:Lbsk;

    invoke-virtual {p0, v0, v1}, LVq;->a(Ljava/lang/Class;Lbsk;)V

    .line 78
    const-class v0, LVA;

    iget-object v1, p0, LVq;->b:Lbsk;

    invoke-virtual {p0, v0, v1}, LVq;->a(Ljava/lang/Class;Lbsk;)V

    .line 79
    const-class v0, LVv;

    iget-object v1, p0, LVq;->c:Lbsk;

    invoke-virtual {p0, v0, v1}, LVq;->a(Ljava/lang/Class;Lbsk;)V

    .line 80
    const-class v0, LVm;

    iget-object v1, p0, LVq;->d:Lbsk;

    invoke-virtual {p0, v0, v1}, LVq;->a(Ljava/lang/Class;Lbsk;)V

    .line 81
    const-class v0, LVx;

    iget-object v1, p0, LVq;->e:Lbsk;

    invoke-virtual {p0, v0, v1}, LVq;->a(Ljava/lang/Class;Lbsk;)V

    .line 82
    const-class v0, LVg;

    iget-object v1, p0, LVq;->f:Lbsk;

    invoke-virtual {p0, v0, v1}, LVq;->a(Ljava/lang/Class;Lbsk;)V

    .line 83
    const-class v0, LVs;

    iget-object v1, p0, LVq;->g:Lbsk;

    invoke-virtual {p0, v0, v1}, LVq;->a(Ljava/lang/Class;Lbsk;)V

    .line 84
    const-class v0, LVB;

    iget-object v1, p0, LVq;->h:Lbsk;

    invoke-virtual {p0, v0, v1}, LVq;->a(Ljava/lang/Class;Lbsk;)V

    .line 85
    iget-object v0, p0, LVq;->a:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x16b

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 87
    iget-object v0, p0, LVq;->b:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x16f

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 89
    iget-object v0, p0, LVq;->c:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x16c

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 91
    iget-object v0, p0, LVq;->d:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xd9

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 93
    iget-object v0, p0, LVq;->e:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x16e

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 95
    iget-object v0, p0, LVq;->f:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0xff

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 97
    iget-object v0, p0, LVq;->g:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x16d

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 99
    iget-object v0, p0, LVq;->h:Lbsk;

    new-instance v1, LbrD;

    const/16 v2, 0x20

    invoke-direct {v1, v2, p0}, LbrD;-><init>(ILbse;)V

    invoke-virtual {v0, v1}, Lbsk;->a(Lbxw;)V

    .line 101
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 282
    .line 284
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown members injector ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 105
    return-void
.end method
