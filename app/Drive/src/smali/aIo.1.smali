.class public LaIo;
.super Ljava/lang/Object;
.source "StorageBackendUriUtilsImpl.java"

# interfaces
.implements LaIm;


# instance fields
.field private final a:LaIF;

.field private final a:LaIa;

.field private final a:LbuE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbuE",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LaIa;Laja;LaIF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaIa;",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LaIF;",
            ")V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, LaIo;->a:LaIa;

    .line 44
    iput-object p2, p0, LaIo;->a:LbuE;

    .line 45
    iput-object p3, p0, LaIo;->a:LaIF;

    .line 46
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, LaIo;->a:LaIa;

    invoke-interface {v0, p1}, LaIa;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/net/Uri;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 57
    sget-object v0, LaEG;->n:LaEG;

    invoke-virtual {v0}, LaEG;->a()Landroid/net/Uri;

    move-result-object v0

    .line 60
    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61
    iget-object v0, p0, LaIo;->a:LaIa;

    invoke-interface {v0, p1}, LaIa;->a(Landroid/net/Uri;)LaIC;

    move-result-object v0

    .line 68
    if-eqz v0, :cond_0

    instance-of v2, v0, LaIA;

    if-nez v2, :cond_2

    :cond_0
    move-object v0, v1

    .line 71
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    .line 65
    goto :goto_0

    .line 71
    :cond_2
    check-cast v0, LaIA;

    invoke-virtual {v0}, LaIA;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    goto :goto_0
.end method

.method public a(JLcom/google/android/gms/drive/database/data/EntrySpec;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    invoke-static {p1, p2, p3}, LaIF;->a(JLcom/google/android/gms/drive/database/data/EntrySpec;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(LaFM;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 2

    .prologue
    .line 135
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    invoke-virtual {p1}, LaFM;->a()J

    move-result-wide v0

    invoke-static {v0, v1, p2}, LaIF;->a(JLcom/google/android/gms/drive/database/data/EntrySpec;)Ljava/lang/String;

    move-result-object v0

    .line 140
    sget-object v1, LaEG;->m:LaEG;

    invoke-virtual {v1}, LaEG;->a()Landroid/net/Uri;

    move-result-object v1

    .line 142
    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    .line 141
    invoke-static {v1, v0}, Landroid/provider/DocumentsContract;->buildDocumentUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 143
    invoke-virtual {p0, v0}, LaIo;->a(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public a(Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 120
    iget-object v0, p0, LaIo;->a:LbuE;

    invoke-interface {v0}, LbuE;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const/16 v1, 0x43

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->revokeUriPermission(Landroid/net/Uri;I)V

    .line 125
    const-string v0, "StorageBackendUriUtils"

    const-string v1, "Revoked permission for: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 126
    return-void
.end method

.method public a(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 81
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_1

    .line 114
    :cond_0
    return-void

    .line 85
    :cond_1
    iget-object v0, p0, LaIo;->a:LbuE;

    invoke-interface {v0}, LbuE;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Landroid/content/ContentResolver;->getOutgoingPersistedUriPermissions()Ljava/util/List;

    move-result-object v0

    .line 85
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/UriPermission;

    .line 87
    sget-object v2, LaEG;->m:LaEG;

    invoke-virtual {v2}, LaEG;->a()Landroid/net/Uri;

    move-result-object v2

    .line 88
    invoke-virtual {v0}, Landroid/content/UriPermission;->getUri()Landroid/net/Uri;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 97
    :try_start_0
    invoke-static {v0}, Landroid/provider/DocumentsContract;->getDocumentId(Landroid/net/Uri;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 104
    iget-object v3, p0, LaIo;->a:LaIF;

    invoke-virtual {v3, v2}, LaIF;->a(Ljava/lang/String;)LaIC;

    move-result-object v2

    .line 105
    if-nez v2, :cond_3

    .line 107
    invoke-virtual {p0, v0}, LaIo;->a(Landroid/net/Uri;)V

    goto :goto_0

    .line 98
    :catch_0
    move-exception v2

    .line 100
    invoke-virtual {p0, v0}, LaIo;->a(Landroid/net/Uri;)V

    goto :goto_0

    .line 110
    :cond_3
    invoke-virtual {v2}, LaIC;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 111
    invoke-virtual {p0, v0}, LaIo;->a(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public a(LaGv;)Z
    .locals 1

    .prologue
    .line 158
    invoke-static {p1}, LaIs;->a(LaGv;)Z

    move-result v0

    return v0
.end method
