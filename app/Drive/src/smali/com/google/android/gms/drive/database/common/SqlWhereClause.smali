.class public Lcom/google/android/gms/drive/database/common/SqlWhereClause;
.super Ljava/lang/Object;
.source "SqlWhereClause.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/drive/database/common/SqlWhereClause;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/google/android/gms/drive/database/common/SqlWhereClause;

.field public static final b:Lcom/google/android/gms/drive/database/common/SqlWhereClause;

.field public static final c:Lcom/google/android/gms/drive/database/common/SqlWhereClause;

.field public static final d:Lcom/google/android/gms/drive/database/common/SqlWhereClause;


# instance fields
.field private final a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 127
    sget-object v0, LaES;->C:LaES;

    .line 128
    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a:Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    .line 133
    sget-object v0, LaES;->x:LaES;

    .line 134
    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->b:Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    .line 139
    new-instance v2, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    const-string v3, "1=1"

    move-object v0, v1

    nop

    nop

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v2, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->c:Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    .line 144
    new-instance v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    const-string v2, "1=0"

    nop

    nop

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->d:Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    .line 248
    new-instance v0, LaFJ;

    invoke-direct {v0}, LaFJ;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 155
    if-nez p2, :cond_0

    .line 156
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 155
    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/util/Collection;)V

    .line 157
    return-void

    .line 156
    :cond_0
    invoke-static {p2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    iput-object p1, p0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a:Ljava/lang/String;

    .line 170
    invoke-static {p2}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a:LbmF;

    .line 171
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/util/Collection;LaFJ;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/util/Collection;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/util/Collection;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/gms/drive/database/common/SqlWhereClause;"
        }
    .end annotation

    .prologue
    .line 160
    new-instance v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/util/Collection;)V

    return-object v0
.end method


# virtual methods
.method public a()LaFK;
    .locals 4

    .prologue
    .line 203
    new-instance v0, LaFK;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a:LbmF;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, LaFK;-><init>(Ljava/lang/String;Ljava/util/Collection;LaFJ;)V

    return-object v0
.end method

.method public a()LbmF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmF",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a:LbmF;

    return-object v0
.end method

.method public a(LaFL;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 1

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()LaFK;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LaFK;->a(LaFL;Lcom/google/android/gms/drive/database/common/SqlWhereClause;)LaFK;

    move-result-object v0

    invoke-virtual {v0}, LaFK;->a()Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a:LbmF;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, LbmF;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a:LbmF;

    invoke-virtual {v0}, LbmF;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 184
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Trying to get an expression of a where clause with non empty parameter list: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 187
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 238
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 216
    if-ne p1, p0, :cond_1

    .line 222
    :cond_0
    :goto_0
    return v0

    .line 218
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    if-nez v2, :cond_2

    move v0, v1

    .line 219
    goto :goto_0

    .line 221
    :cond_2
    check-cast p1, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    .line 222
    iget-object v2, p0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a:LbmF;

    iget-object v3, p1, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a:LbmF;

    invoke-virtual {v2, v3}, LbmF;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 228
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a:LbmF;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 233
    const-string v0, "SqlWhereClause[%s, %s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a:LbmF;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 243
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    iget-object v0, p0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 245
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()LbmF;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 246
    return-void
.end method
