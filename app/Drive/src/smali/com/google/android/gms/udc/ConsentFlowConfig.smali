.class public Lcom/google/android/gms/udc/ConsentFlowConfig;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:LbbF;


# instance fields
.field private final a:I

.field private a:Z

.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LbbF;

    invoke-direct {v0}, LbbF;-><init>()V

    sput-object v0, Lcom/google/android/gms/udc/ConsentFlowConfig;->CREATOR:LbbF;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0, v1, v0, v1}, Lcom/google/android/gms/udc/ConsentFlowConfig;-><init>(IZZ)V

    return-void
.end method

.method public constructor <init>(IZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/udc/ConsentFlowConfig;->a:I

    iput-boolean p2, p0, Lcom/google/android/gms/udc/ConsentFlowConfig;->a:Z

    iput-boolean p3, p0, Lcom/google/android/gms/udc/ConsentFlowConfig;->b:Z

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/udc/ConsentFlowConfig;->a:I

    return v0
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/udc/ConsentFlowConfig;->a:Z

    return v0
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/udc/ConsentFlowConfig;->b:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/udc/ConsentFlowConfig;->CREATOR:LbbF;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/udc/ConsentFlowConfig;->CREATOR:LbbF;

    invoke-static {p0, p1, p2}, LbbF;->a(Lcom/google/android/gms/udc/ConsentFlowConfig;Landroid/os/Parcel;I)V

    return-void
.end method
