.class public Lcom/google/android/gms/internal/oi$b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:LaUL;


# instance fields
.field public final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaUL;

    invoke-direct {v0}, LaUL;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/oi$b;->CREATOR:LaUL;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/oi$b;->a:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/oi$b;->a:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/oi$b;->CREATOR:LaUL;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/oi$b;->CREATOR:LaUL;

    invoke-static {p0, p1, p2}, LaUL;->a(Lcom/google/android/gms/internal/oi$b;Landroid/os/Parcel;I)V

    return-void
.end method
