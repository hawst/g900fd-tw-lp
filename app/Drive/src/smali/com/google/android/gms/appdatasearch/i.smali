.class public Lcom/google/android/gms/appdatasearch/i;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:LaAF;


# instance fields
.field public final a:I

.field public final a:Ljava/lang/String;

.field public final a:[Lcom/google/android/gms/appdatasearch/Feature;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaAF;

    invoke-direct {v0}, LaAF;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/i;->CREATOR:LaAF;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;[Lcom/google/android/gms/appdatasearch/Feature;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/i;->a:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/i;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/i;->a:[Lcom/google/android/gms/appdatasearch/Feature;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/i;->CREATOR:LaAF;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/i;->CREATOR:LaAF;

    invoke-static {p0, p1, p2}, LaAF;->a(Lcom/google/android/gms/appdatasearch/i;Landroid/os/Parcel;I)V

    return-void
.end method
