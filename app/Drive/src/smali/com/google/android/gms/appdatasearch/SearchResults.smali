.class public Lcom/google/android/gms/appdatasearch/SearchResults;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;",
        "Ljava/lang/Iterable",
        "<",
        "LaAm;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:LaAs;


# instance fields
.field public final a:I

.field public final a:Ljava/lang/String;

.field public final a:[B

.field public final a:[D

.field public final a:[I

.field public final a:[Landroid/os/Bundle;

.field public final a:[Ljava/lang/String;

.field public final b:I

.field public final b:[B

.field public final b:[I

.field public final b:[Landroid/os/Bundle;

.field public final c:[Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaAs;

    invoke-direct {v0}, LaAs;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/SearchResults;->CREATOR:LaAs;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;[I[B[Landroid/os/Bundle;[Landroid/os/Bundle;[Landroid/os/Bundle;I[I[Ljava/lang/String;[B[D)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->a:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->a:[I

    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->a:[B

    iput-object p5, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->a:[Landroid/os/Bundle;

    iput-object p6, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->b:[Landroid/os/Bundle;

    iput-object p7, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->c:[Landroid/os/Bundle;

    iput p8, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->b:I

    iput-object p9, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->b:[I

    iput-object p10, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->a:[Ljava/lang/String;

    iput-object p11, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->b:[B

    iput-object p12, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->a:[D

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->b:I

    return v0
.end method

.method public a()LaAn;
    .locals 1

    new-instance v0, LaAn;

    invoke-direct {v0, p0}, LaAn;-><init>(Lcom/google/android/gms/appdatasearch/SearchResults;)V

    return-object v0
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/SearchResults;->CREATOR:LaAs;

    const/4 v0, 0x0

    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/SearchResults;->a()LaAn;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/SearchResults;->CREATOR:LaAs;

    invoke-static {p0, p1, p2}, LaAs;->a(Lcom/google/android/gms/appdatasearch/SearchResults;Landroid/os/Parcel;I)V

    return-void
.end method
