.class public final Lcom/google/android/gms/plus/widgets/AddToCirclesButton;
.super Landroid/widget/FrameLayout;


# static fields
.field private static a:Landroid/content/Context;


# instance fields
.field private final a:LaUq;

.field private a:Landroid/view/View;

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object p1, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->b:Landroid/content/Context;

    invoke-static {p1}, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->a(Landroid/content/Context;)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, LaUq;

    iput-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->a:LaUq;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->a:LaUq;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LaLp;->a(Ljava/lang/Object;)LaLm;

    move-result-object v2

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-static {v1}, LaLp;->a(Ljava/lang/Object;)LaLm;

    move-result-object v1

    invoke-static {p2}, LaLp;->a(Ljava/lang/Object;)LaLm;

    move-result-object v3

    invoke-interface {v0, v2, v1, v3}, LaUq;->a(LaLm;LaLm;LaLm;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->a:LaUq;

    invoke-interface {v0}, LaUq;->a()LaLm;

    move-result-object v0

    invoke-static {v0}, LaLp;->a(LaLm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->a:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->addView(Landroid/view/View;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;)Landroid/util/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Landroid/util/Pair",
            "<",
            "LaUq;",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    invoke-static {p0}, LaCJ;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->a:Landroid/content/Context;

    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->a:Landroid/content/Context;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    :try_start_0
    const-string v1, "com.google.android.gms.plus.circlesbutton.AddToCirclesButtonImpl$DynamiteHost"

    invoke-virtual {v0, v1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder;

    new-instance v1, Landroid/util/Pair;

    invoke-static {v0}, LaUr;->a(Landroid/os/IBinder;)LaUq;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->a:Landroid/content/Context;

    invoke-direct {v1, v0, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :goto_1
    if-eqz v0, :cond_1

    const-string v1, "AddToCirclesButton"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "AddToCirclesButton"

    const-string v2, "Can\'t load com.google.android.gms.plus.circlesbutton.AddToCirclesButtonImpl$DynamiteHost"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    new-instance v0, Landroid/util/Pair;

    new-instance v1, LbaY;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, LbaY;-><init>(LbaX;)V

    invoke-direct {v0, v1, p0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public setAnalyticsStartView(Ljava/lang/String;I)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->a:LaUq;

    invoke-interface {v0, p1, p2}, LaUq;->a(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "AddToCirclesButton"

    const-string v1, "Add to circles button failed to setAnalyticsStartView."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setShowProgressIndicator(Z)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->a:LaUq;

    invoke-interface {v0, p1}, LaUq;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "AddToCirclesButton"

    const-string v1, "Add to circles button failed to setShowProgressIndicator."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setSize(I)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->a:LaUq;

    invoke-interface {v0, p1}, LaUq;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "AddToCirclesButton"

    const-string v1, "Add to circles button failed to setSize."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setType(I)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->a:LaUq;

    invoke-interface {v0, p1}, LaUq;->b(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "AddToCirclesButton"

    const-string v1, "Add to circles button failed to setType."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
