.class public Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:LaAH;


# instance fields
.field public final a:I

.field public final a:Ljava/lang/String;

.field public final b:I

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final c:Ljava/lang/String;

.field public final d:I

.field public final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaAH;

    invoke-direct {v0}, LaAH;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->CREATOR:LaAH;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->a:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->a:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->b:I

    iput p4, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->c:I

    iput p5, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->d:I

    iput-object p6, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->b:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->c:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->CREATOR:LaAH;

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    if-eqz v2, :cond_3

    check-cast p1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->b:I

    iget v3, p1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->b:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->c:I

    iget v3, p1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->c:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->d:I

    iget v3, p1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->d:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";labelId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";settingsDescriptionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";iconId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";defaultIntentAction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";defaultIntentData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";defaultIntentActivity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->CREATOR:LaAH;

    invoke-static {p0, p1, p2}, LaAH;->a(Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;Landroid/os/Parcel;I)V

    return-void
.end method
