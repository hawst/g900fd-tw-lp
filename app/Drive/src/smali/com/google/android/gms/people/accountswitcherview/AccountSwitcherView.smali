.class public Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;
.super Landroid/widget/FrameLayout;
.source "AccountSwitcherView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lbai;
.implements Lbak;


# instance fields
.field private a:I

.field private a:LaCV;

.field private a:LaZA;

.field private a:LaZB;

.field private a:LaZD;

.field private a:LaZM;

.field private a:LaZP;

.field private a:LaZR;

.field private a:LaZx;

.field private a:LaZy;

.field private a:LaZz;

.field private a:Landroid/view/View;

.field private a:Landroid/view/ViewGroup;

.field private a:Landroid/widget/FrameLayout;

.field private a:Landroid/widget/ListView;

.field private a:Lbao;

.field private a:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

.field private a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

.field private a:Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;

.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbao;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private b:I

.field private b:Z

.field private c:I

.field private c:Z

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 79
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 82
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 71
    iput-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->c:Z

    .line 72
    iput-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->d:Z

    .line 83
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 84
    new-array v0, v1, [I

    sget v1, LaZT;->forceFullHeight:I

    aput v1, v0, v2

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 86
    invoke-static {}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a()Z

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->e:Z

    .line 87
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 88
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(Landroid/content/Context;)V

    .line 89
    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->offsetTopAndBottom(I)V

    .line 364
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getTop()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->c:I

    .line 365
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 200
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, LaZZ;->account_switcher:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 201
    sget v0, LaZX;->sign_in:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/view/ViewGroup;

    .line 202
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    sget v0, LaZX;->account_list_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    .line 204
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    sget v0, LaZX;->selected_account_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    .line 207
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iget-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->e:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->setForceFullHeight(Z)V

    .line 208
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->setOnNavigationModeChange(Lbak;)V

    .line 209
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->setOnAccountChangeListener(Lbai;)V

    .line 210
    sget v0, LaZX;->accounts_list:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/ListView;

    .line 211
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 212
    sget v0, LaZX;->accounts_wrapper:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;

    .line 213
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:I

    .line 214
    sget v0, LaZX;->nav_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    .line 215
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setNavigationMode(I)V

    .line 216
    return-void
.end method

.method private a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 357
    invoke-virtual {p1, p2}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 358
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->b:I

    .line 360
    return-void
.end method

.method private a(Lbao;Z)V
    .locals 3

    .prologue
    .line 441
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lbao;

    .line 442
    iput-object p1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lbao;

    .line 443
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 444
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lbao;

    invoke-static {v1, v0, v2}, LaZM;->a(Ljava/util/List;Lbao;Lbao;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Ljava/util/List;

    .line 445
    if-nez p2, :cond_0

    .line 446
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lbao;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a(Lbao;)V

    .line 448
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZM;

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Ljava/util/List;

    invoke-virtual {v0, v1}, LaZM;->a(Ljava/util/List;)V

    .line 453
    :goto_0
    return-void

    .line 451
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a(Lbao;)V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/high16 v2, 0x3f800000    # 1.0f

    const v5, 0x3f4ccccd    # 0.8f

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 551
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a()I

    move-result v0

    .line 552
    packed-switch v0, :pswitch_data_0

    .line 579
    :goto_0
    return-void

    .line 554
    :pswitch_0
    if-eqz p1, :cond_0

    .line 555
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 556
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 557
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setAnimation(Landroid/view/animation/Animation;)V

    .line 558
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v5}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    invoke-direct {p0, v4, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(ZLandroid/view/animation/Interpolator;)V

    .line 562
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 563
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;->setVisibility(I)V

    goto :goto_0

    .line 560
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1

    .line 567
    :pswitch_1
    if-eqz p1, :cond_1

    .line 568
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v2, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 569
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 570
    const-wide/16 v2, 0x85

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 571
    const/4 v0, 0x1

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1, v5}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(ZLandroid/view/animation/Interpolator;)V

    .line 575
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 576
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;->setVisibility(I)V

    goto :goto_0

    .line 573
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setAnimation(Landroid/view/animation/Animation;)V

    goto :goto_2

    .line 552
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(ZLandroid/view/animation/Interpolator;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 583
    if-eqz p1, :cond_0

    move v0, v1

    move v3, v2

    .line 590
    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;

    const-string v5, "animatedHeightFraction"

    const/4 v6, 0x2

    new-array v6, v6, [F

    int-to-float v3, v3

    aput v3, v6, v2

    int-to-float v0, v0

    aput v0, v6, v1

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 592
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 593
    invoke-virtual {v0, p2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 594
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 595
    return-void

    :cond_0
    move v0, v2

    move v3, v1

    .line 588
    goto :goto_0
.end method

.method public static a()Z
    .locals 2

    .prologue
    .line 709
    sget-object v0, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    const-string v1, "L"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/view/View;

    iget-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->b:Z

    invoke-virtual {v0, v1}, Landroid/view/View;->setNestedScrollingEnabled(Z)V

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    iget-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->b:Z

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setNestedScrollingEnabled(Z)V

    .line 106
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->b:Z

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setNestedScrollingEnabled(Z)V

    .line 108
    :cond_0
    return-void
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 538
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->setNavigationMode(I)V

    .line 539
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 641
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZM;

    if-eqz v0, :cond_0

    .line 642
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZM;

    invoke-virtual {v0}, LaZM;->a()V

    .line 644
    :cond_0
    return-void
.end method

.method public a(Lbao;)V
    .locals 2

    .prologue
    .line 631
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(Lbao;Z)V

    .line 632
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZx;

    if-eqz v0, :cond_0

    .line 633
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZx;

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lbao;

    invoke-interface {v0, v1}, LaZx;->a(Lbao;)V

    .line 635
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;)V
    .locals 2

    .prologue
    .line 543
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(Z)V

    .line 544
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZA;

    if-eqz v0, :cond_0

    .line 545
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZA;

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    .line 546
    invoke-virtual {v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a()I

    move-result v1

    .line 545
    invoke-interface {v0, v1}, LaZA;->a(I)V

    .line 548
    :cond_0
    return-void
.end method

.method public getNestedScrollAxes()I
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x2

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 648
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/view/ViewGroup;

    if-ne p1, v0, :cond_1

    .line 649
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZB;

    if-eqz v0, :cond_0

    .line 650
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZB;

    invoke-interface {v0}, LaZB;->a()V

    .line 662
    :cond_0
    :goto_0
    return-void

    .line 652
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    if-ne p1, v0, :cond_0

    .line 653
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    .line 654
    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a()I

    move-result v0

    if-ne v0, v2, :cond_2

    move v0, v1

    .line 653
    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->b(I)V

    .line 657
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    .line 658
    invoke-virtual {v3}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a()I

    move-result v3

    if-ne v3, v2, :cond_3

    :goto_2
    invoke-virtual {v0, v2}, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;->setExpanded(Z)V

    .line 660
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 654
    goto :goto_1

    :cond_3
    move v2, v1

    .line 658
    goto :goto_2
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 599
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZM;

    invoke-virtual {v0, p3}, LaZM;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_1

    .line 600
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZM;

    invoke-virtual {v0, p3}, LaZM;->a(I)Lbao;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(Lbao;Z)V

    .line 602
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZx;

    if-eqz v0, :cond_0

    .line 603
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZx;

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lbao;

    invoke-interface {v0, v1}, LaZx;->a(Lbao;)V

    .line 614
    :cond_0
    :goto_0
    return-void

    .line 605
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZM;

    invoke-virtual {v0, p3}, LaZM;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 606
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZy;

    if-eqz v0, :cond_0

    .line 607
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZy;

    invoke-interface {v0}, LaZy;->a()V

    goto :goto_0

    .line 609
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZM;

    invoke-virtual {v0, p3}, LaZM;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 610
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZz;

    if-eqz v0, :cond_0

    .line 611
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZz;

    invoke-interface {v0}, LaZz;->a()V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 273
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 278
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/view/ViewGroup;

    .line 279
    :goto_0
    iget v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->b:I

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 280
    iget v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->b:I

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 282
    :cond_0
    iget v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->c:I

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getTop()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 283
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    iget v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->c:I

    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->offsetTopAndBottom(I)V

    .line 285
    :cond_1
    return-void

    .line 278
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    goto :goto_0
.end method

.method public onMeasure(II)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 253
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    move v0, v1

    .line 254
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 255
    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 256
    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 257
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v0

    .line 259
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getPaddingLeft()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    .line 260
    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getPaddingRight()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->getPaddingBottom()I

    move-result v5

    .line 259
    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 263
    iget-boolean v2, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->b:Z

    if-eqz v2, :cond_2

    .line 264
    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    .line 265
    invoke-virtual {p0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->getHeight()I

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 264
    invoke-virtual {v1, p1, v0}, Landroid/widget/FrameLayout;->measure(II)V

    .line 269
    :cond_0
    return-void

    .line 257
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    .line 258
    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->getMeasuredHeight()I

    move-result v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 263
    goto :goto_2

    .line 254
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onNestedFling(Landroid/view/View;FFZ)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 292
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/view/ViewGroup;

    .line 293
    :goto_0
    if-nez p4, :cond_1

    cmpg-float v1, p3, v2

    if-gez v1, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    if-gez v1, :cond_1

    .line 295
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    neg-int v1, v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(Landroid/view/View;I)V

    .line 296
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    neg-int v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(I)V

    .line 297
    const/4 v0, 0x1

    .line 309
    :goto_1
    return v0

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    goto :goto_0

    .line 298
    :cond_1
    if-eqz p4, :cond_3

    cmpl-float v1, p3, v2

    if-lez v1, :cond_3

    .line 301
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    neg-int v2, v2

    if-le v1, v2, :cond_2

    .line 303
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    neg-int v1, v1

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(Landroid/view/View;I)V

    .line 305
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getTop()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    neg-int v2, v2

    if-le v1, v2, :cond_3

    .line 306
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    neg-int v0, v0

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(I)V

    .line 309
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onNestedPreScroll(Landroid/view/View;II[I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 322
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/view/ViewGroup;

    .line 323
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a()I

    move-result v1

    if-ne v1, v5, :cond_2

    .line 354
    :cond_0
    :goto_1
    return-void

    .line 322
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    goto :goto_0

    .line 328
    :cond_2
    if-lez p3, :cond_6

    .line 330
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    if-lez v1, :cond_6

    .line 332
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    if-le v1, p3, :cond_3

    .line 333
    neg-int v1, p3

    .line 339
    :goto_2
    if-eqz v1, :cond_0

    .line 341
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    neg-int v4, v4

    if-ge v3, v4, :cond_4

    .line 342
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    neg-int v3, v3

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(Landroid/view/View;I)V

    .line 346
    :goto_3
    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getTop()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    neg-int v4, v4

    if-ge v3, v4, :cond_5

    .line 347
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    neg-int v0, v0

    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getTop()I

    move-result v3

    sub-int/2addr v0, v3

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(I)V

    .line 351
    :goto_4
    aput v2, p4, v2

    .line 352
    aput v1, p4, v5

    goto :goto_1

    .line 335
    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    neg-int v1, v1

    goto :goto_2

    .line 344
    :cond_4
    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(Landroid/view/View;I)V

    goto :goto_3

    .line 349
    :cond_5
    invoke-direct {p0, v1}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(I)V

    goto :goto_4

    :cond_6
    move v1, v2

    goto :goto_2
.end method

.method public onNestedScroll(Landroid/view/View;IIII)V
    .locals 3

    .prologue
    .line 375
    const/4 v0, 0x0

    .line 376
    iget-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/view/ViewGroup;

    .line 377
    :goto_0
    if-gez p5, :cond_5

    .line 378
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    if-gez v2, :cond_5

    .line 379
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    if-gt p5, v0, :cond_0

    .line 380
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result p5

    .line 386
    :cond_0
    :goto_1
    if-eqz p5, :cond_1

    .line 388
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    sub-int/2addr v0, p5

    if-lez v0, :cond_3

    .line 389
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    neg-int v0, v0

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(Landroid/view/View;I)V

    .line 393
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getTop()I

    move-result v0

    sub-int/2addr v0, p5

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    if-le v0, v2, :cond_4

    .line 394
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(I)V

    .line 399
    :cond_1
    :goto_3
    return-void

    .line 376
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    goto :goto_0

    .line 391
    :cond_3
    neg-int v0, p5

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(Landroid/view/View;I)V

    goto :goto_2

    .line 396
    :cond_4
    neg-int v0, p5

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(I)V

    goto :goto_3

    :cond_5
    move p5, v0

    goto :goto_1
.end method

.method public onStartNestedScroll(Landroid/view/View;Landroid/view/View;I)Z
    .locals 1

    .prologue
    .line 317
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->b:Z

    return v0
.end method

.method public setAccountListLayout(ILaZR;LaZP;)V
    .locals 0

    .prologue
    .line 194
    iput p1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:I

    .line 195
    iput-object p2, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZR;

    .line 196
    iput-object p3, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZP;

    .line 197
    return-void
.end method

.method public setAccountSelectedListener(LaZx;)V
    .locals 0

    .prologue
    .line 492
    iput-object p1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZx;

    .line 493
    return-void
.end method

.method public setAccounts(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbao;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 238
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setAccounts(Ljava/util/List;Lbao;)V

    .line 239
    return-void
.end method

.method public setAccounts(Ljava/util/List;Lbao;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbao;",
            ">;",
            "Lbao;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 248
    invoke-virtual {p0, p1, p2, v0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setAccounts(Ljava/util/List;Lbao;Lbao;Lbao;)V

    .line 249
    return-void
.end method

.method public setAccounts(Ljava/util/List;Lbao;Lbao;Lbao;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbao;",
            ">;",
            "Lbao;",
            "Lbao;",
            "Lbao;",
            ")V"
        }
    .end annotation

    .prologue
    .line 412
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZM;

    if-nez v0, :cond_0

    .line 413
    new-instance v0, LaZM;

    invoke-virtual {p0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:I

    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZR;

    iget-object v4, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZP;

    invoke-direct {v0, v1, v2, v3, v4}, LaZM;-><init>(Landroid/content/Context;ILaZR;LaZP;)V

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZM;

    .line 415
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZM;

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZD;

    invoke-virtual {v0, v1}, LaZM;->a(LaZD;)V

    .line 416
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZM;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 417
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZM;

    iget-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->c:Z

    invoke-virtual {v0, v1}, LaZM;->a(Z)V

    .line 418
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZM;

    iget-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->d:Z

    invoke-virtual {v0, v1}, LaZM;->b(Z)V

    .line 420
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Ljava/util/List;

    .line 422
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Ljava/util/List;

    if-nez v0, :cond_1

    .line 423
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lbao;

    .line 425
    :cond_1
    invoke-virtual {p0, p2}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setSelectedAccount(Lbao;)V

    .line 426
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZM;

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Ljava/util/List;

    invoke-virtual {v0, v1}, LaZM;->a(Ljava/util/List;)V

    .line 427
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, p3, p4}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->setRecents(Lbao;Lbao;)V

    .line 428
    return-void
.end method

.method public setAddAccountListener(LaZy;)V
    .locals 0

    .prologue
    .line 485
    iput-object p1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZy;

    .line 486
    return-void
.end method

.method public setClient(LaCV;)V
    .locals 3

    .prologue
    .line 623
    iput-object p1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaCV;

    .line 624
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaCV;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->setClient(LaCV;)V

    .line 625
    new-instance v0, LaZD;

    invoke-virtual {p0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaCV;

    invoke-direct {v0, v1, v2}, LaZD;-><init>(Landroid/content/Context;LaCV;)V

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZD;

    .line 626
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZD;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->setAvatarManager(LaZD;)V

    .line 627
    return-void
.end method

.method public setManageAccountsListener(LaZz;)V
    .locals 0

    .prologue
    .line 478
    iput-object p1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZz;

    .line 479
    return-void
.end method

.method public setNavigation(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 229
    iput-object p1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/view/View;

    .line 230
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setClipToPadding(Z)V

    .line 231
    invoke-direct {p0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->b()V

    .line 232
    return-void
.end method

.method public setNavigationMode(I)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 526
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->b(I)V

    .line 527
    invoke-direct {p0, v1}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(Z)V

    .line 528
    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    .line 529
    invoke-virtual {v3}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a()I

    move-result v3

    if-ne v3, v0, :cond_0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;->setExpanded(Z)V

    .line 531
    return-void

    :cond_0
    move v0, v1

    .line 529
    goto :goto_0
.end method

.method public setNavigationModeChangeListener(LaZA;)V
    .locals 0

    .prologue
    .line 506
    iput-object p1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZA;

    .line 507
    return-void
.end method

.method public setRecents(Lbao;Lbao;)V
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->setRecents(Lbao;Lbao;)V

    .line 472
    return-void
.end method

.method public setScrollingHeaderEnabled(Z)V
    .locals 1

    .prologue
    .line 98
    if-eqz p1, :cond_0

    invoke-static {}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->b:Z

    .line 99
    invoke-direct {p0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->b()V

    .line 100
    return-void

    .line 98
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSelectedAccount(Lbao;)V
    .locals 1

    .prologue
    .line 437
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(Lbao;Z)V

    .line 438
    return-void
.end method

.method public setSelectedAccountLayout(ILban;Lbal;)V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->setSelectedAccountLayout(ILban;Lbal;)V

    .line 176
    return-void
.end method

.method public setSignInListener(LaZB;)V
    .locals 0

    .prologue
    .line 499
    iput-object p1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:LaZB;

    .line 500
    return-void
.end method
