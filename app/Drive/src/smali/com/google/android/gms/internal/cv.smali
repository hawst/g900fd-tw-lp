.class public final Lcom/google/android/gms/internal/cv;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:LaOr;


# instance fields
.field public final a:I

.field public final a:LaOC;

.field public final a:LaOE;

.field public final a:LaOP;

.field public final a:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaOr;

    invoke-direct {v0}, LaOr;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/cv;->CREATOR:LaOr;

    return-void
.end method

.method public constructor <init>(ILandroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/cv;->a:I

    invoke-static {p2}, LaLn;->a(Landroid/os/IBinder;)LaLm;

    move-result-object v0

    invoke-static {v0}, LaLp;->a(LaLm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaOP;

    iput-object v0, p0, Lcom/google/android/gms/internal/cv;->a:LaOP;

    invoke-static {p3}, LaLn;->a(Landroid/os/IBinder;)LaLm;

    move-result-object v0

    invoke-static {v0}, LaLp;->a(LaLm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaOC;

    iput-object v0, p0, Lcom/google/android/gms/internal/cv;->a:LaOC;

    invoke-static {p4}, LaLn;->a(Landroid/os/IBinder;)LaLm;

    move-result-object v0

    invoke-static {v0}, LaLp;->a(LaLm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaOE;

    iput-object v0, p0, Lcom/google/android/gms/internal/cv;->a:LaOE;

    invoke-static {p5}, LaLn;->a(Landroid/os/IBinder;)LaLm;

    move-result-object v0

    invoke-static {v0}, LaLp;->a(LaLm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/internal/cv;->a:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(LaOE;LaOP;LaOC;Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/cv;->a:I

    iput-object p1, p0, Lcom/google/android/gms/internal/cv;->a:LaOE;

    iput-object p2, p0, Lcom/google/android/gms/internal/cv;->a:LaOP;

    iput-object p3, p0, Lcom/google/android/gms/internal/cv;->a:LaOC;

    iput-object p4, p0, Lcom/google/android/gms/internal/cv;->a:Landroid/content/Context;

    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/google/android/gms/internal/cv;
    .locals 2

    :try_start_0
    const-string v0, "com.google.android.gms.ads.internal.purchase.InAppPurchaseManagerInfo"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-class v1, Lcom/google/android/gms/internal/cv;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    const-string v1, "com.google.android.gms.ads.internal.purchase.InAppPurchaseManagerInfo"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/cv;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Intent;Lcom/google/android/gms/internal/cv;)V
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    const-string v1, "com.google.android.gms.ads.internal.purchase.InAppPurchaseManagerInfo"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "com.google.android.gms.ads.internal.purchase.InAppPurchaseManagerInfo"

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public a()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/cv;->a:LaOP;

    invoke-static {v0}, LaLp;->a(Ljava/lang/Object;)LaLm;

    move-result-object v0

    invoke-interface {v0}, LaLm;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public b()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/cv;->a:LaOC;

    invoke-static {v0}, LaLp;->a(Ljava/lang/Object;)LaLm;

    move-result-object v0

    invoke-interface {v0}, LaLm;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public c()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/cv;->a:LaOE;

    invoke-static {v0}, LaLp;->a(Ljava/lang/Object;)LaLm;

    move-result-object v0

    invoke-interface {v0}, LaLm;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public d()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/cv;->a:Landroid/content/Context;

    invoke-static {v0}, LaLp;->a(Ljava/lang/Object;)LaLm;

    move-result-object v0

    invoke-interface {v0}, LaLm;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, LaOr;->a(Lcom/google/android/gms/internal/cv;Landroid/os/Parcel;I)V

    return-void
.end method
