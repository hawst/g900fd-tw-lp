.class public Lcom/google/android/gms/search/queries/QueryCall$b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lbbx;


# instance fields
.field public a:I

.field public a:Lcom/google/android/gms/appdatasearch/QuerySpecification;

.field public a:Ljava/lang/String;

.field public a:[Ljava/lang/String;

.field public b:I

.field public b:Ljava/lang/String;

.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbbx;

    invoke-direct {v0}, Lbbx;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/queries/QueryCall$b;->CREATOR:Lbbx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/queries/QueryCall$b;->c:I

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/search/queries/QueryCall$b;->c:I

    iput-object p2, p0, Lcom/google/android/gms/search/queries/QueryCall$b;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/search/queries/QueryCall$b;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/search/queries/QueryCall$b;->a:[Ljava/lang/String;

    iput p5, p0, Lcom/google/android/gms/search/queries/QueryCall$b;->a:I

    iput p6, p0, Lcom/google/android/gms/search/queries/QueryCall$b;->b:I

    iput-object p7, p0, Lcom/google/android/gms/search/queries/QueryCall$b;->a:Lcom/google/android/gms/appdatasearch/QuerySpecification;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/search/queries/QueryCall$b;->CREATOR:Lbbx;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/search/queries/QueryCall$b;->CREATOR:Lbbx;

    invoke-static {p0, p1, p2}, Lbbx;->a(Lcom/google/android/gms/search/queries/QueryCall$b;Landroid/os/Parcel;I)V

    return-void
.end method
