.class public Lcom/google/android/gms/appdatasearch/QuerySpecification;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:LaAQ;


# instance fields
.field public final a:I

.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final a:Z

.field public final b:I

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/appdatasearch/Section;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Z

.field public final c:I

.field public final c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaAQ;

    invoke-direct {v0}, LaAQ;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->CREATOR:LaAQ;

    return-void
.end method

.method public constructor <init>(IZLjava/util/List;Ljava/util/List;ZIIZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/appdatasearch/Section;",
            ">;ZIIZ)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->a:I

    iput-boolean p2, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->a:Z

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->a:Ljava/util/List;

    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->b:Ljava/util/List;

    iput-boolean p5, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->b:Z

    iput p6, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->b:I

    iput p7, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->c:I

    iput-boolean p8, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->c:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->CREATOR:LaAQ;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->CREATOR:LaAQ;

    invoke-static {p0, p1, p2}, LaAQ;->a(Lcom/google/android/gms/appdatasearch/QuerySpecification;Landroid/os/Parcel;I)V

    return-void
.end method
