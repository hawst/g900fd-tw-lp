.class public interface abstract Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;
.super Ljava/lang/Object;

# interfaces
.implements LaDA;
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LaDA",
        "<",
        "Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# virtual methods
.method public abstract a()J
.end method

.method public abstract a()Landroid/net/Uri;
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public abstract b()Landroid/net/Uri;
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public abstract c()Landroid/net/Uri;
.end method
