.class public Lcom/google/android/gms/appdatasearch/PIMEUpdate;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:LaAL;


# instance fields
.field public final a:I

.field public final a:J

.field public final a:Landroid/accounts/Account;

.field public final a:Landroid/os/Bundle;

.field public final a:Ljava/lang/String;

.field public final a:Z

.field public final a:[B

.field public final b:I

.field public final b:J

.field public final b:Ljava/lang/String;

.field public final b:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaAL;

    invoke-direct {v0}, LaAL;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->CREATOR:LaAL;

    return-void
.end method

.method public constructor <init>(I[B[BILjava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;JJLandroid/accounts/Account;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->a:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->a:[B

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->b:[B

    iput p4, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->b:I

    iput-object p5, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->a:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->b:Ljava/lang/String;

    iput-boolean p7, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->a:Z

    iput-object p8, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->a:Landroid/os/Bundle;

    iput-wide p9, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->a:J

    iput-wide p11, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->b:J

    iput-object p13, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->a:Landroid/accounts/Account;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->CREATOR:LaAL;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->CREATOR:LaAL;

    invoke-static {p0, p1, p2}, LaAL;->a(Lcom/google/android/gms/appdatasearch/PIMEUpdate;Landroid/os/Parcel;I)V

    return-void
.end method
