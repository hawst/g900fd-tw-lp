.class public Lcom/google/android/gms/appdatasearch/aa;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:LaAr;


# instance fields
.field public final a:I

.field public final a:Ljava/lang/String;

.field public final a:[Lcom/google/android/gms/appdatasearch/DocumentId;

.field public final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaAr;

    invoke-direct {v0}, LaAr;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/aa;->CREATOR:LaAr;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;[Lcom/google/android/gms/appdatasearch/DocumentId;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/aa;->a:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/aa;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/aa;->a:[Lcom/google/android/gms/appdatasearch/DocumentId;

    iput p4, p0, Lcom/google/android/gms/appdatasearch/aa;->b:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/aa;->CREATOR:LaAr;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/aa;->CREATOR:LaAr;

    invoke-static {p0, p1, p2}, LaAr;->a(Lcom/google/android/gms/appdatasearch/aa;Landroid/os/Parcel;I)V

    return-void
.end method
