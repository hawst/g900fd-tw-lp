.class public Lcom/google/android/gms/appdatasearch/NativeApiInfo;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:LaAK;


# instance fields
.field public final a:I

.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaAK;

    invoke-direct {v0}, LaAK;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/NativeApiInfo;->CREATOR:LaAK;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/NativeApiInfo;->a:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/NativeApiInfo;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/NativeApiInfo;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/NativeApiInfo;->CREATOR:LaAK;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/NativeApiInfo;->CREATOR:LaAK;

    invoke-static {p0, p1, p2}, LaAK;->a(Lcom/google/android/gms/appdatasearch/NativeApiInfo;Landroid/os/Parcel;I)V

    return-void
.end method
