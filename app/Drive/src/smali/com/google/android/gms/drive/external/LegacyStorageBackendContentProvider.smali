.class public Lcom/google/android/gms/drive/external/LegacyStorageBackendContentProvider;
.super LalP;
.source "LegacyStorageBackendContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LalP",
        "<",
        "LaHZ;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/UriMatcher;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 158
    invoke-direct {p0}, LalP;-><init>()V

    .line 159
    return-void
.end method

.method constructor <init>(LaIu;LaIw;LaIa;LaId;)V
    .locals 2

    .prologue
    .line 173
    invoke-direct {p0}, LalP;-><init>()V

    .line 174
    new-instance v1, LaHZ;

    invoke-direct {v1}, LaHZ;-><init>()V

    .line 175
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaIu;

    iput-object v0, v1, LaHZ;->a:LaIu;

    .line 176
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaIw;

    iput-object v0, v1, LaHZ;->a:LaIw;

    .line 177
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaIa;

    iput-object v0, v1, LaHZ;->a:LaIa;

    .line 178
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaId;

    iput-object v0, v1, LaHZ;->a:LaId;

    .line 180
    invoke-virtual {p0, v1}, Lcom/google/android/gms/drive/external/LegacyStorageBackendContentProvider;->a(Ljava/lang/Object;)V

    .line 182
    invoke-direct {p0}, Lcom/google/android/gms/drive/external/LegacyStorageBackendContentProvider;->a()V

    .line 183
    return-void
.end method

.method private a(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 2

    .prologue
    .line 248
    if-nez p2, :cond_0

    .line 249
    invoke-static {}, LaHV;->a()[Ljava/lang/String;

    move-result-object p2

    .line 251
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/drive/external/LegacyStorageBackendContentProvider;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaHZ;

    iget-object v0, v0, LaHZ;->a:LaIa;

    invoke-interface {v0, p1}, LaIa;->a(Landroid/net/Uri;)LaIC;

    move-result-object v0

    .line 253
    if-nez v0, :cond_1

    .line 254
    const/4 v0, 0x0

    .line 261
    :goto_0
    return-object v0

    .line 260
    :cond_1
    const/4 v1, 0x1

    .line 261
    invoke-virtual {v0, v1, p2}, LaIC;->a(Z[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 193
    sget-object v0, LaEG;->n:LaEG;

    invoke-virtual {v0}, LaEG;->a()Landroid/net/Uri;

    move-result-object v0

    .line 194
    new-instance v1, Landroid/content/UriMatcher;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/gms/drive/external/LegacyStorageBackendContentProvider;->a:Landroid/content/UriMatcher;

    .line 195
    iget-object v1, p0, Lcom/google/android/gms/drive/external/LegacyStorageBackendContentProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    const-string v2, "*"

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 196
    return-void
.end method


# virtual methods
.method protected a(Lbuu;)LaHZ;
    .locals 1

    .prologue
    .line 163
    new-instance v0, LaHZ;

    invoke-direct {v0}, LaHZ;-><init>()V

    .line 164
    invoke-interface {p1, v0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 165
    return-object v0
.end method

.method protected bridge synthetic a(Lbuu;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/external/LegacyStorageBackendContentProvider;->a(Lbuu;)LaHZ;

    move-result-object v0

    return-object v0
.end method

.method public call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 308
    invoke-super {p0, p1, p2, p3}, LalP;->call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 309
    if-eqz v0, :cond_0

    .line 325
    :goto_0
    return-object v0

    .line 313
    :cond_0
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 315
    if-nez v0, :cond_1

    move-object v0, v2

    .line 316
    goto :goto_0

    .line 319
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/drive/external/LegacyStorageBackendContentProvider;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaHZ;

    .line 320
    iget-object v3, v1, LaHZ;->a:LaIa;

    invoke-interface {v3, v0}, LaIa;->a(Landroid/net/Uri;)LaIC;

    move-result-object v0

    .line 321
    if-nez v0, :cond_2

    move-object v0, v2

    .line 322
    goto :goto_0

    .line 325
    :cond_2
    iget-object v1, v1, LaHZ;->a:LaId;

    invoke-virtual {v1, p1, v0, p3}, LaId;->a(Ljava/lang/String;LaIC;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 288
    const-string v0, "LegacyStorageBackendContentProvider"

    const-string v1, "getType: uri=%s"

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 289
    new-array v2, v5, [Ljava/lang/String;

    const-string v0, "mime_type"

    aput-object v0, v2, v4

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/external/LegacyStorageBackendContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 292
    if-nez v1, :cond_0

    .line 302
    :goto_0
    return-object v3

    .line 296
    :cond_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 302
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 300
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 302
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/google/android/gms/drive/external/LegacyStorageBackendContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/database/DocListProvider;->a(Landroid/content/Context;)V

    .line 188
    invoke-direct {p0}, Lcom/google/android/gms/drive/external/LegacyStorageBackendContentProvider;->a()V

    .line 189
    const/4 v0, 0x1

    return v0
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 4

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/gms/drive/external/LegacyStorageBackendContentProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 219
    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 220
    new-instance v1, Ljava/io/FileNotFoundException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " type="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 223
    :cond_0
    const-string v0, "size"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 224
    if-eqz v0, :cond_1

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    move-object v1, v0

    .line 226
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/drive/external/LegacyStorageBackendContentProvider;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaHZ;

    .line 227
    iget-object v2, v0, LaHZ;->a:LaIa;

    invoke-interface {v2, p1}, LaIa;->a(Landroid/net/Uri;)LaIC;

    move-result-object v2

    .line 228
    if-nez v2, :cond_2

    .line 229
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0

    .line 224
    :cond_1
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    .line 231
    :cond_2
    invoke-virtual {v2}, LaIC;->a()LaGo;

    move-result-object v2

    .line 232
    if-nez v2, :cond_3

    .line 233
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "File not found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 235
    :cond_3
    const-string v3, "r"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 236
    iget-object v0, v0, LaHZ;->a:LaIu;

    invoke-virtual {v0, v2}, LaIu;->a(LaGo;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 238
    :goto_1
    return-object v0

    .line 237
    :cond_4
    const-string v3, "rwt"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "w"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 238
    :cond_5
    iget-object v0, v0, LaHZ;->a:LaIw;

    invoke-virtual {v0, v2, v1}, LaIw;->a(LaGo;Ljava/lang/Long;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_1

    .line 240
    :cond_6
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 271
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    iget-object v0, p0, Lcom/google/android/gms/drive/external/LegacyStorageBackendContentProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 274
    const-string v1, "LegacyStorageBackendContentProvider"

    const-string v2, "query: uri: %s, projection: %s, sortOrder: %s, uriType %s"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v6

    .line 275
    invoke-static {p2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    const/4 v4, 0x2

    aput-object p5, v3, v4

    const/4 v4, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 274
    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 277
    packed-switch v0, :pswitch_data_0

    .line 281
    const-string v0, "LegacyStorageBackendContentProvider"

    const-string v1, "Unsupported URI: %s"

    new-array v2, v7, [Ljava/lang/Object;

    aput-object p1, v2, v6

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 282
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 279
    :pswitch_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/external/LegacyStorageBackendContentProvider;->a(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 277
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x0

    return v0
.end method
