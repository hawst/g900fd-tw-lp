.class public Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Response;
.super Ljava/lang/Object;

# interfaces
.implements LaDc;
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lbbu;


# instance fields
.field public final a:I

.field public a:Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;

.field public a:Lcom/google/android/gms/common/api/Status;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbbu;

    invoke-direct {v0}, Lbbu;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Response;->CREATOR:Lbbu;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Response;->a:I

    return-void
.end method

.method public constructor <init>(ILcom/google/android/gms/common/api/Status;Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Response;->a:I

    iput-object p2, p0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    iput-object p3, p0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Response;->a:Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Response;->CREATOR:Lbbu;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Response;->CREATOR:Lbbu;

    invoke-static {p0, p1, p2}, Lbbu;->a(Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Response;Landroid/os/Parcel;I)V

    return-void
.end method
