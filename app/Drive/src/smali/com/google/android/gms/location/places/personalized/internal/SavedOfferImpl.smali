.class public Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;
.super LaVF;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:LaVJ;


# instance fields
.field public final a:I

.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaVJ;

    invoke-direct {v0}, LaVJ;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;->CREATOR:LaVJ;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, LaVF;-><init>()V

    iput p1, p0, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;->a:I

    iput-object p2, p0, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;->d:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;->CREATOR:LaVJ;

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;

    iget-object v2, p0, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LaRY;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;->CREATOR:LaVJ;

    invoke-static {p0, p1, p2}, LaVJ;->a(Lcom/google/android/gms/location/places/personalized/internal/SavedOfferImpl;Landroid/os/Parcel;I)V

    return-void
.end method
