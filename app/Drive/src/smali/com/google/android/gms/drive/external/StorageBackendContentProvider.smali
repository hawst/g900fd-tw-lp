.class public Lcom/google/android/gms/drive/external/StorageBackendContentProvider;
.super Landroid/provider/DocumentsProvider;
.source "StorageBackendContentProvider.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field private volatile a:LaIk;

.field private final a:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const/16 v0, 0x40

    .line 64
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Landroid/provider/DocumentsProvider;-><init>()V

    .line 95
    invoke-static {}, Lanj;->a()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a:Landroid/os/Handler;

    .line 96
    return-void
.end method

.method public constructor <init>(LaIs;LaIq;LaIF;LaGM;LSF;Laja;LaIh;LqK;LaId;LaIo;LvL;LtK;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaIs;",
            "LaIq;",
            "LaIF;",
            "LaGM;",
            "LSF;",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LaIh;",
            "LqK;",
            "LaId;",
            "LaIo;",
            "LvL;",
            "LtK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 111
    invoke-direct {p0}, Landroid/provider/DocumentsProvider;-><init>()V

    .line 112
    new-instance v0, LaIk;

    invoke-direct {v0}, LaIk;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a:LaIk;

    .line 113
    iget-object v1, p0, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a:LaIk;

    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaIs;

    iput-object v0, v1, LaIk;->a:LaIs;

    .line 114
    iget-object v1, p0, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a:LaIk;

    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaIq;

    iput-object v0, v1, LaIk;->a:LaIq;

    .line 115
    iget-object v1, p0, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a:LaIk;

    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaIF;

    iput-object v0, v1, LaIk;->a:LaIF;

    .line 116
    iget-object v1, p0, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a:LaIk;

    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, v1, LaIk;->a:LaGM;

    .line 117
    iget-object v1, p0, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a:LaIk;

    invoke-static {p5}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSF;

    iput-object v0, v1, LaIk;->a:LSF;

    .line 118
    iget-object v1, p0, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a:LaIk;

    invoke-static {p6}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    iput-object v0, v1, LaIk;->a:Laja;

    .line 119
    iget-object v1, p0, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a:LaIk;

    invoke-static {p7}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaIh;

    iput-object v0, v1, LaIk;->a:LaIh;

    .line 120
    iget-object v1, p0, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a:LaIk;

    invoke-static {p8}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqK;

    iput-object v0, v1, LaIk;->a:LqK;

    .line 121
    iget-object v1, p0, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a:LaIk;

    invoke-static {p9}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaId;

    iput-object v0, v1, LaIk;->a:LaId;

    .line 123
    iget-object v1, p0, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a:LaIk;

    invoke-static {p10}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaIo;

    iput-object v0, v1, LaIk;->a:LaIo;

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a:LaIk;

    iput-object p11, v0, LaIk;->a:LvL;

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a:LaIk;

    iput-object p12, v0, LaIk;->a:LtK;

    .line 126
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a:Landroid/os/Handler;

    .line 127
    return-void
.end method

.method private declared-synchronized a()LaIk;
    .locals 3

    .prologue
    .line 130
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lbtd;->a()Lbtd;

    move-result-object v0

    .line 131
    invoke-static {}, Lanj;->a()Ljava/util/concurrent/Executor;

    move-result-object v1

    new-instance v2, LaIj;

    invoke-direct {v2, p0, v0}, LaIj;-><init>(Lcom/google/android/gms/drive/external/StorageBackendContentProvider;Lbtd;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 141
    :try_start_1
    invoke-virtual {v0}, Lbtd;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaIk;
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0

    .line 142
    :catch_0
    move-exception v0

    .line 143
    :try_start_2
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "wait for the injector is interrupted"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 144
    :catch_1
    move-exception v0

    .line 145
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "wait for the injector is interrupted"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method private a(LaIC;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 3

    .prologue
    .line 297
    if-nez p1, :cond_0

    .line 298
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "document not found"

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 301
    :cond_0
    if-nez p2, :cond_1

    .line 302
    invoke-static {}, LaHV;->a()[Ljava/lang/String;

    move-result-object p2

    .line 305
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a()Landroid/net/Uri;

    move-result-object v1

    .line 306
    invoke-direct {p0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->b()LaIk;

    move-result-object v0

    .line 308
    invoke-virtual {v0}, LaIk;->a()Z

    move-result v0

    sget-object v2, LIK;->b:LIK;

    .line 307
    invoke-virtual {p1, v0, p2, v2, v1}, LaIC;->a(Z[Ljava/lang/String;LIK;Landroid/net/Uri;)LaHY;

    move-result-object v2

    .line 309
    if-eqz v2, :cond_2

    .line 311
    invoke-direct {p0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->b()LaIk;

    move-result-object v0

    iget-object v0, v0, LaIk;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 310
    invoke-interface {v2, v0, v1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 313
    :cond_2
    return-object v2
.end method

.method private a()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 173
    sget-object v0, LaEG;->m:LaEG;

    invoke-virtual {v0}, LaEG;->a()Landroid/net/Uri;

    move-result-object v0

    .line 174
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "notify"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private a(LaGo;)V
    .locals 3

    .prologue
    .line 186
    invoke-interface {p1}, LaGo;->f()Ljava/lang/String;

    move-result-object v0

    .line 187
    const-string v1, "storageReadDocumentPackageName"

    invoke-virtual {p0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const-string v1, "storageReadDocumentMimeType"

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 178
    invoke-direct {p0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->b()LaIk;

    move-result-object v0

    iget-object v0, v0, LaIk;->a:LqK;

    const-string v1, "storageBackend"

    invoke-virtual {v0, v1, p1}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 182
    invoke-direct {p0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->b()LaIk;

    move-result-object v0

    iget-object v0, v0, LaIk;->a:LqK;

    const-string v1, "storageBackend"

    invoke-virtual {v0, v1, p1, p2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    return-void
.end method

.method private b()LaIk;
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a:LaIk;

    .line 152
    if-nez v0, :cond_1

    .line 153
    monitor-enter p0

    .line 154
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a:LaIk;

    .line 156
    if-nez v0, :cond_0

    .line 157
    invoke-direct {p0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a()LaIk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a:LaIk;

    .line 158
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LbiT;->b(Z)V

    .line 160
    :cond_0
    monitor-exit p0

    .line 162
    :cond_1
    return-object v0

    .line 158
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 160
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private b(LaGo;)V
    .locals 3

    .prologue
    .line 192
    invoke-interface {p1}, LaGo;->f()Ljava/lang/String;

    move-result-object v0

    .line 193
    const-string v1, "storageWriteDocumentPackageName"

    invoke-virtual {p0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    const-string v1, "storageWriteDocumentMimeType"

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 221
    sget-object v0, LaEG;->m:LaEG;

    invoke-virtual {v0}, LaEG;->a()Landroid/net/Uri;

    move-result-object v0

    .line 222
    invoke-direct {p0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->b()LaIk;

    move-result-object v1

    iget-object v1, v1, LaIk;->a:LaIo;

    .line 223
    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/provider/DocumentsContract;->buildDocumentUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 222
    invoke-virtual {v1, v0}, LaIo;->a(Landroid/net/Uri;)V

    .line 224
    return-void
.end method


# virtual methods
.method public call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 420
    invoke-super {p0, p1, p2, p3}, Landroid/provider/DocumentsProvider;->call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 421
    if-eqz v0, :cond_0

    .line 447
    :goto_0
    return-object v0

    .line 425
    :cond_0
    sget-object v0, LaEG;->m:LaEG;

    invoke-virtual {v0}, LaEG;->a()Landroid/net/Uri;

    move-result-object v1

    .line 426
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 427
    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    move-object v0, v2

    .line 428
    goto :goto_0

    .line 431
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->b()LaIk;

    move-result-object v5

    .line 432
    iget-object v1, v5, LaIk;->a:Laja;

    invoke-virtual {v1}, Laja;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    .line 433
    invoke-static {v0}, Landroid/provider/DocumentsContract;->getDocumentId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    .line 436
    const-string v3, "android.permission.MANAGE_DOCUMENTS"

    invoke-virtual {v1, v3}, Landroid/content/Context;->checkCallingPermission(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_3

    move v3, v4

    .line 438
    :goto_1
    if-nez v3, :cond_2

    .line 439
    invoke-virtual {v1, v0, v4, p1}, Landroid/content/Context;->enforceCallingUriPermission(Landroid/net/Uri;ILjava/lang/String;)V

    .line 443
    :cond_2
    iget-object v0, v5, LaIk;->a:LaIF;

    invoke-virtual {v0, v6}, LaIF;->a(Ljava/lang/String;)LaIC;

    move-result-object v0

    .line 444
    if-nez v0, :cond_4

    move-object v0, v2

    .line 445
    goto :goto_0

    .line 436
    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    .line 447
    :cond_4
    iget-object v1, v5, LaIk;->a:LaId;

    invoke-virtual {v1, p1, v0, p3}, LaId;->a(Ljava/lang/String;LaIC;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public createDocument(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 342
    if-nez p2, :cond_0

    .line 343
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "null mimeType"

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 345
    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 346
    :cond_1
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "empty name"

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 349
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->b()LaIk;

    move-result-object v0

    .line 352
    iget-object v1, v0, LaIk;->a:LaIF;

    invoke-virtual {v1, p1}, LaIF;->a(Ljava/lang/String;)LaIC;

    move-result-object v1

    .line 353
    if-nez v1, :cond_3

    .line 354
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "Parent document not found"

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 357
    :cond_3
    const-string v2, "storageCreateDocument"

    invoke-direct {p0, v2}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a(Ljava/lang/String;)V

    .line 359
    iget-object v0, v0, LaIk;->a:LaIq;

    invoke-virtual {v1, p3, p2, v0}, LaIC;->a(Ljava/lang/String;Ljava/lang/String;LaIq;)LaIA;

    move-result-object v0

    .line 360
    if-nez v0, :cond_4

    .line 363
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "failed to create document"

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 365
    :cond_4
    invoke-static {v0}, LaIF;->a(LaIC;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 167
    const-string v0, "TAG"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    invoke-virtual {p0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/database/DocListProvider;->a(Landroid/content/Context;)V

    .line 169
    const/4 v0, 0x1

    return v0
.end method

.method public openDocument(Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/os/ParcelFileDescriptor;
    .locals 5

    .prologue
    .line 229
    invoke-direct {p0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->b()LaIk;

    move-result-object v0

    .line 230
    iget-object v1, v0, LaIk;->a:LaIF;

    invoke-virtual {v1, p1}, LaIF;->a(Ljava/lang/String;)LaIC;

    move-result-object v1

    .line 231
    if-nez v1, :cond_0

    .line 232
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->b(Ljava/lang/String;)V

    .line 233
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0

    .line 235
    :cond_0
    invoke-virtual {v1}, LaIC;->a()LaGo;

    move-result-object v1

    .line 236
    if-nez v1, :cond_1

    .line 237
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->b(Ljava/lang/String;)V

    .line 238
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "File not found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 240
    :cond_1
    invoke-interface {v1}, LaGo;->e()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 241
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->b(Ljava/lang/String;)V

    .line 242
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "File is trashed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 245
    :cond_2
    invoke-static {}, Landroid/os/StrictMode;->getThreadPolicy()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v2

    .line 246
    new-instance v3, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-direct {v3}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>()V

    invoke-virtual {v3}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v3

    invoke-static {v3}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 248
    :try_start_0
    const-string v3, "r"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 249
    invoke-direct {p0, v1}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a(LaGo;)V

    .line 250
    iget-object v0, v0, LaIk;->a:LaIs;

    const/high16 v3, 0x10000000

    iget-object v4, p0, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1, v3, v4}, LaIs;->a(LaGo;ILandroid/os/Handler;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 277
    invoke-static {v2}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    :goto_0
    return-object v0

    .line 254
    :cond_3
    :try_start_1
    invoke-interface {v1}, LaGo;->h()Z

    move-result v3

    if-nez v3, :cond_4

    .line 255
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "Error opening \'%s\' for \'%s\' access: file exists, but is read only"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 277
    :catchall_0
    move-exception v0

    invoke-static {v2}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    throw v0

    .line 259
    :cond_4
    :try_start_2
    const-string v3, "w"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 260
    invoke-direct {p0, v1}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->b(LaGo;)V

    .line 261
    iget-object v0, v0, LaIk;->a:LaIs;

    const/high16 v3, 0x24000000

    iget-object v4, p0, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1, v3, v4}, LaIs;->b(LaGo;ILandroid/os/Handler;)Landroid/os/ParcelFileDescriptor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 277
    invoke-static {v2}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    goto :goto_0

    .line 265
    :cond_5
    :try_start_3
    const-string v3, "rwt"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 266
    invoke-direct {p0, v1}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->b(LaGo;)V

    .line 267
    iget-object v0, v0, LaIk;->a:LaIs;

    const/high16 v3, 0x34000000

    iget-object v4, p0, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1, v3, v4}, LaIs;->b(LaGo;ILandroid/os/Handler;)Landroid/os/ParcelFileDescriptor;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 277
    invoke-static {v2}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    goto :goto_0

    .line 274
    :cond_6
    :try_start_4
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported mode: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public openDocumentThumbnail(Ljava/lang/String;Landroid/graphics/Point;Landroid/os/CancellationSignal;)Landroid/content/res/AssetFileDescriptor;
    .locals 5

    .prologue
    .line 371
    invoke-direct {p0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->b()LaIk;

    move-result-object v0

    .line 372
    iget-object v1, v0, LaIk;->a:LaIF;

    invoke-virtual {v1, p1}, LaIF;->a(Ljava/lang/String;)LaIC;

    move-result-object v1

    .line 373
    if-nez v1, :cond_0

    .line 374
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0

    .line 376
    :cond_0
    invoke-virtual {v1}, LaIC;->a()LaGo;

    move-result-object v1

    .line 377
    if-nez v1, :cond_1

    .line 378
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "File not found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 381
    :cond_1
    iget-object v0, v0, LaIk;->a:LaIh;

    new-instance v2, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    iget v3, p2, Landroid/graphics/Point;->x:I

    iget v4, p2, Landroid/graphics/Point;->y:I

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;-><init>(II)V

    invoke-virtual {v0, v1, v2, p3}, LaIh;->a(LaGo;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;Landroid/os/CancellationSignal;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public queryChildDocuments(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 5

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->b()LaIk;

    move-result-object v0

    iget-object v0, v0, LaIk;->a:LaIF;

    invoke-virtual {v0, p1}, LaIF;->a(Ljava/lang/String;)LaIC;

    move-result-object v0

    .line 320
    const-string v1, "StorageBackendContentProvider"

    const-string v2, "GUID: %s -> %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 321
    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a(LaIC;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public queryDocument(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 2

    .prologue
    .line 283
    if-nez p2, :cond_0

    .line 284
    invoke-static {}, LaHV;->a()[Ljava/lang/String;

    move-result-object p2

    .line 286
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->b()LaIk;

    move-result-object v0

    .line 287
    invoke-direct {p0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->b()LaIk;

    move-result-object v1

    iget-object v1, v1, LaIk;->a:LaIF;

    invoke-virtual {v1, p1}, LaIF;->a(Ljava/lang/String;)LaIC;

    move-result-object v1

    .line 289
    if-nez v1, :cond_1

    .line 290
    const/4 v0, 0x0

    .line 293
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, LaIk;->a()Z

    move-result v0

    invoke-virtual {v1, v0, p2}, LaIC;->a(Z[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method

.method public queryRecentDocuments(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 388
    if-nez p2, :cond_1

    .line 389
    invoke-static {}, LaHV;->a()[Ljava/lang/String;

    move-result-object v1

    .line 392
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->b()LaIk;

    move-result-object v4

    .line 393
    iget-object v0, v4, LaIk;->a:LaGM;

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, LaGM;->a(J)LaFM;

    move-result-object v0

    .line 394
    if-nez v0, :cond_0

    .line 395
    const/4 v0, 0x0

    .line 406
    :goto_1
    return-object v0

    .line 398
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->b()LaIk;

    move-result-object v2

    iget-object v2, v2, LaIk;->a:LvL;

    .line 399
    new-instance v3, LvN;

    invoke-direct {v3}, LvN;-><init>()V

    .line 400
    invoke-virtual {v0}, LaFM;->a()LaFO;

    move-result-object v0

    invoke-interface {v2, v0}, LvL;->a(LaFO;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v0

    invoke-virtual {v3, v0}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    move-result-object v0

    .line 401
    invoke-interface {v2}, LvL;->b()Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v3

    invoke-virtual {v0, v3}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    move-result-object v0

    .line 402
    invoke-interface {v2}, LvL;->a()Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v3

    invoke-virtual {v0, v3}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    move-result-object v0

    .line 403
    invoke-interface {v2}, LvL;->c()Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v2

    invoke-virtual {v0, v2}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    move-result-object v0

    .line 404
    invoke-virtual {v0}, LvN;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v2

    .line 407
    invoke-virtual {v4}, LaIk;->a()Z

    move-result v0

    sget-object v3, LIK;->d:LIK;

    iget-object v4, v4, LaIk;->a:LaGM;

    sget-object v5, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a:Ljava/lang/String;

    .line 406
    invoke-static/range {v0 .. v5}, LaIC;->a(Z[Ljava/lang/String;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;LIK;LaGM;Ljava/lang/String;)LaHY;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v1, p2

    goto :goto_0
.end method

.method public queryRoots([Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    .line 199
    const-string v0, "storageQueryRoots"

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a(Ljava/lang/String;)V

    .line 201
    if-nez p1, :cond_0

    .line 202
    invoke-static {}, LaIg;->a()[Ljava/lang/String;

    move-result-object p1

    .line 205
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->b()LaIk;

    move-result-object v1

    .line 206
    new-instance v2, LaIg;

    iget-object v0, v1, LaIk;->a:Laja;

    .line 207
    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v2, v0, p1}, LaIg;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    .line 208
    new-instance v3, Landroid/database/MatrixCursor;

    invoke-direct {v3, p1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 211
    iget-object v0, v1, LaIk;->a:LSF;

    invoke-interface {v0}, LSF;->a()[Landroid/accounts/Account;

    move-result-object v4

    .line 212
    array-length v5, v4

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v5, :cond_1

    aget-object v6, v4, v0

    .line 213
    iget-object v7, v1, LaIk;->a:LaGM;

    iget-object v6, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 214
    invoke-static {v6}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v6

    invoke-interface {v7, v6}, LaGM;->a(LaFO;)LaFM;

    move-result-object v6

    .line 215
    invoke-virtual {v2, v6}, LaIg;->a(LaFM;)[Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 212
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 217
    :cond_1
    return-object v3
.end method

.method public querySearchDocuments(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 4

    .prologue
    .line 327
    const-string v0, "storageSearchDocument"

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a(Ljava/lang/String;)V

    .line 328
    invoke-direct {p0}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->b()LaIk;

    move-result-object v0

    iget-object v0, v0, LaIk;->a:LaIF;

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p2}, LaIF;->a(JLjava/lang/String;)LaIH;

    move-result-object v0

    .line 329
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/drive/external/StorageBackendContentProvider;->a(LaIC;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
