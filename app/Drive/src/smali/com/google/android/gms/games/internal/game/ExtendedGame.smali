.class public interface abstract Lcom/google/android/gms/games/internal/game/ExtendedGame;
.super Ljava/lang/Object;

# interfaces
.implements LaDA;
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LaDA",
        "<",
        "Lcom/google/android/gms/games/internal/game/ExtendedGame;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# virtual methods
.method public abstract a()J
.end method

.method public abstract a()Lcom/google/android/gms/games/Game;
.end method

.method public abstract a()Lcom/google/android/gms/games/snapshot/SnapshotMetadata;
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public abstract a()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/games/internal/game/GameBadge;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a()Z
.end method

.method public abstract b()I
.end method

.method public abstract b()J
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public abstract c()J
.end method

.method public abstract c_()I
.end method
