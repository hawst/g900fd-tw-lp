.class public Lcom/google/android/gms/common/api/d;
.super Landroid/support/v4/app/Fragment;

# interfaces
.implements LaCX;
.implements LaCY;
.implements Landroid/content/DialogInterface$OnCancelListener;


# instance fields
.field private a:LaCC;

.field private a:LaCV;

.field private a:LaCY;

.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    return-void
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/api/d;->a:LaCV;

    invoke-interface {v0}, LaCV;->a()V

    iput-boolean v1, p0, Lcom/google/android/gms/common/api/d;->a:Z

    :goto_1
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/d;->a()LH;

    move-result-object v2

    invoke-static {v2}, LaCJ;->a(Landroid/content/Context;)I

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    :pswitch_1
    const/4 v2, -0x1

    if-ne p2, v2, :cond_0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/d;->a:LaCY;

    iget-object v1, p0, Lcom/google/android/gms/common/api/d;->a:LaCC;

    invoke-interface {v0, v1}, LaCY;->a(LaCC;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(LaCC;)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/gms/common/api/d;->a:LaCC;

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/d;->a:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/d;->a()LH;

    move-result-object v0

    invoke-virtual {v0}, LH;->a()LM;

    move-result-object v0

    invoke-virtual {v0}, LM;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p1}, LaCC;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    shl-int/lit8 v0, v0, 0x10

    add-int/lit8 v0, v0, 0x1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/d;->a()LH;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, LaCC;->a(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/common/api/d;->a:LaCV;

    invoke-interface {v0}, LaCV;->a()V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, LaCC;->a()I

    move-result v0

    invoke-static {v0}, LaCJ;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, LaCC;->a()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/d;->a()LH;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v0, v1, p0, v2, p0}, LaCJ;->a(ILandroid/app/Activity;Landroid/support/v4/app/Fragment;ILandroid/content/DialogInterface$OnCancelListener;)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/api/d;->a:LaCY;

    iget-object v1, p0, Lcom/google/android/gms/common/api/d;->a:LaCC;

    invoke-interface {v0, v1}, LaCY;->a(LaCC;)V

    goto :goto_0
.end method

.method public a(LaCV;LaCY;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gms/common/api/d;->a:LaCV;

    iget-object v0, p0, Lcom/google/android/gms/common/api/d;->a:LaCV;

    invoke-interface {v0, p0}, LaCV;->a(LaCX;)V

    iput-object p2, p0, Lcom/google/android/gms/common/api/d;->a:LaCY;

    iget-object v0, p0, Lcom/google/android/gms/common/api/d;->a:LaCV;

    invoke-interface {v0, p0}, LaCV;->a(LaCX;)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/d;->a:LaCV;

    invoke-interface {v0, p0}, LaCV;->a(LaCY;)V

    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/d;->a:Z

    return-void
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "resolving_error"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/d;->a:Z

    const-string v0, "connection_result_status"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "resolution_pending_intent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    new-instance v2, LaCC;

    invoke-direct {v2, v1, v0}, LaCC;-><init>(ILandroid/app/PendingIntent;)V

    iput-object v2, p0, Lcom/google/android/gms/common/api/d;->a:LaCC;

    :cond_0
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->c(Landroid/os/Bundle;)V

    const-string v0, "resolving_error"

    iget-boolean v1, p0, Lcom/google/android/gms/common/api/d;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/d;->a:LaCC;

    if-eqz v0, :cond_0

    const-string v0, "connection_result_status"

    iget-object v1, p0, Lcom/google/android/gms/common/api/d;->a:LaCC;

    invoke-virtual {v1}, LaCC;->a()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "resolution_pending_intent"

    iget-object v1, p0, Lcom/google/android/gms/common/api/d;->a:LaCC;

    invoke-virtual {v1}, LaCC;->a()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method public j_()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->j_()V

    iget-object v0, p0, Lcom/google/android/gms/common/api/d;->a:LaCV;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/d;->a:LaCV;

    invoke-interface {v0}, LaCV;->b()V

    :cond_0
    return-void
.end method

.method public k_()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->k_()V

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/d;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/d;->a:LaCV;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/d;->a:LaCV;

    invoke-interface {v0}, LaCV;->a()V

    :cond_0
    return-void
.end method

.method public l()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/d;->a:LaCV;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/d;->a:Z

    iget-object v0, p0, Lcom/google/android/gms/common/api/d;->a:LaCY;

    iget-object v1, p0, Lcom/google/android/gms/common/api/d;->a:LaCC;

    invoke-interface {v0, v1}, LaCY;->a(LaCC;)V

    return-void
.end method
