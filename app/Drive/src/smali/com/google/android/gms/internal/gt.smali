.class public final Lcom/google/android/gms/internal/gt;
.super Landroid/widget/ImageView;


# instance fields
.field private a:I

.field private a:LaRr;


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/internal/gt;->a:LaRr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/gt;->a:LaRr;

    invoke-virtual {p0}, Lcom/google/android/gms/internal/gt;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/gms/internal/gt;->getHeight()I

    move-result v2

    invoke-interface {v0, v1, v2}, LaRr;->a(II)Landroid/graphics/Path;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    iget v0, p0, Lcom/google/android/gms/internal/gt;->a:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/gms/internal/gt;->a:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    :cond_1
    return-void
.end method
