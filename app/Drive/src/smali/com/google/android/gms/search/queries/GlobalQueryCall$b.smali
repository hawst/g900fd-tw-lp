.class public Lcom/google/android/gms/search/queries/GlobalQueryCall$b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lbbv;


# instance fields
.field public a:I

.field public a:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

.field public a:Ljava/lang/String;

.field public b:I

.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbbv;

    invoke-direct {v0}, Lbbv;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$b;->CREATOR:Lbbv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/queries/GlobalQueryCall$b;->c:I

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;IILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/search/queries/GlobalQueryCall$b;->c:I

    iput-object p2, p0, Lcom/google/android/gms/search/queries/GlobalQueryCall$b;->a:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/gms/search/queries/GlobalQueryCall$b;->a:I

    iput p4, p0, Lcom/google/android/gms/search/queries/GlobalQueryCall$b;->b:I

    iput-object p5, p0, Lcom/google/android/gms/search/queries/GlobalQueryCall$b;->a:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$b;->CREATOR:Lbbv;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$b;->CREATOR:Lbbv;

    invoke-static {p0, p1, p2}, Lbbv;->a(Lcom/google/android/gms/search/queries/GlobalQueryCall$b;Landroid/os/Parcel;I)V

    return-void
.end method
