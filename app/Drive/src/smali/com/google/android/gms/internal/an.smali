.class public final Lcom/google/android/gms/internal/an;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:LaMM;


# instance fields
.field public final a:I

.field public final a:J

.field public final a:Landroid/location/Location;

.field public final a:Landroid/os/Bundle;

.field public final a:Lcom/google/android/gms/internal/bb;

.field public final a:Ljava/lang/String;

.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final a:Z

.field public final b:I

.field public final b:Landroid/os/Bundle;

.field public final b:Ljava/lang/String;

.field public final b:Z

.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaMM;

    invoke-direct {v0}, LaMM;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/an;->CREATOR:LaMM;

    return-void
.end method

.method public constructor <init>(IJLandroid/os/Bundle;ILjava/util/List;ZIZLjava/lang/String;Lcom/google/android/gms/internal/bb;Landroid/location/Location;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ",
            "Landroid/os/Bundle;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;ZIZ",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/internal/bb;",
            "Landroid/location/Location;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/an;->a:I

    iput-wide p2, p0, Lcom/google/android/gms/internal/an;->a:J

    iput-object p4, p0, Lcom/google/android/gms/internal/an;->a:Landroid/os/Bundle;

    iput p5, p0, Lcom/google/android/gms/internal/an;->b:I

    iput-object p6, p0, Lcom/google/android/gms/internal/an;->a:Ljava/util/List;

    iput-boolean p7, p0, Lcom/google/android/gms/internal/an;->a:Z

    iput p8, p0, Lcom/google/android/gms/internal/an;->c:I

    iput-boolean p9, p0, Lcom/google/android/gms/internal/an;->b:Z

    iput-object p10, p0, Lcom/google/android/gms/internal/an;->a:Ljava/lang/String;

    iput-object p11, p0, Lcom/google/android/gms/internal/an;->a:Lcom/google/android/gms/internal/bb;

    iput-object p12, p0, Lcom/google/android/gms/internal/an;->a:Landroid/location/Location;

    iput-object p13, p0, Lcom/google/android/gms/internal/an;->b:Ljava/lang/String;

    iput-object p14, p0, Lcom/google/android/gms/internal/an;->b:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, LaMM;->a(Lcom/google/android/gms/internal/an;Landroid/os/Parcel;I)V

    return-void
.end method
