.class public final Lcom/google/android/gms/drive/database/data/ResourceSpec;
.super Ljava/lang/Object;
.source "ResourceSpec.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/drive/database/data/ResourceSpec;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LaFO;

.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, LaHb;

    invoke-direct {v0}, LaHb;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/database/data/ResourceSpec;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(LaFO;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFO;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    .line 34
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public synthetic constructor <init>(LaFO;Ljava/lang/String;LaHb;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/database/data/ResourceSpec;-><init>(LaFO;Ljava/lang/String;)V

    return-void
.end method

.method public static a(LaFO;Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/ResourceSpec;
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/drive/database/data/ResourceSpec;-><init>(LaFO;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 55
    if-ne p0, p1, :cond_1

    .line 66
    :cond_0
    :goto_0
    return v0

    .line 58
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/drive/database/data/ResourceSpec;

    if-nez v2, :cond_3

    .line 61
    instance-of v2, p1, Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-nez v2, :cond_2

    :goto_1
    invoke-static {v0}, LbiT;->a(Z)V

    move v0, v1

    .line 62
    goto :goto_0

    :cond_2
    move v0, v1

    .line 61
    goto :goto_1

    .line 64
    :cond_3
    check-cast p1, Lcom/google/android/gms/drive/database/data/ResourceSpec;

    .line 65
    iget-object v2, p0, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    iget-object v3, p1, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    invoke-virtual {v2, v3}, LaFO;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 71
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 76
    const-string v0, "RemoteResourceSpec[%s, %s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    invoke-virtual {v0}, LaFO;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 88
    return-void
.end method
