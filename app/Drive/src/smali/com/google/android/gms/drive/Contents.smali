.class public Lcom/google/android/gms/drive/Contents;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/drive/Contents;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final a:Landroid/os/ParcelFileDescriptor;

.field public final a:Lcom/google/android/gms/drive/DriveId;

.field public a:Ljava/lang/String;

.field public a:Z

.field public final b:I

.field private b:Z

.field public final c:I

.field private c:Z

.field private d:Z

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaDZ;

    invoke-direct {v0}, LaDZ;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/Contents;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILandroid/os/ParcelFileDescriptor;IILcom/google/android/gms/drive/DriveId;Ljava/lang/String;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/gms/drive/Contents;->b:Z

    iput-boolean v0, p0, Lcom/google/android/gms/drive/Contents;->c:Z

    iput-boolean v0, p0, Lcom/google/android/gms/drive/Contents;->d:Z

    iput-boolean v0, p0, Lcom/google/android/gms/drive/Contents;->e:Z

    iput p1, p0, Lcom/google/android/gms/drive/Contents;->a:I

    iput-object p2, p0, Lcom/google/android/gms/drive/Contents;->a:Landroid/os/ParcelFileDescriptor;

    iput p3, p0, Lcom/google/android/gms/drive/Contents;->b:I

    iput p4, p0, Lcom/google/android/gms/drive/Contents;->c:I

    iput-object p5, p0, Lcom/google/android/gms/drive/Contents;->a:Lcom/google/android/gms/drive/DriveId;

    iput-object p6, p0, Lcom/google/android/gms/drive/Contents;->a:Ljava/lang/String;

    iput-boolean p7, p0, Lcom/google/android/gms/drive/Contents;->a:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, LaDZ;->a(Lcom/google/android/gms/drive/Contents;Landroid/os/Parcel;I)V

    return-void
.end method
