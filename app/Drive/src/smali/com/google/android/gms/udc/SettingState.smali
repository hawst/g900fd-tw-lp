.class public Lcom/google/android/gms/udc/SettingState;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:LbbG;


# instance fields
.field private final a:I

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LbbG;

    invoke-direct {v0}, LbbG;-><init>()V

    sput-object v0, Lcom/google/android/gms/udc/SettingState;->CREATOR:LbbG;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/udc/SettingState;->a:I

    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/udc/SettingState;->a:I

    iput p2, p0, Lcom/google/android/gms/udc/SettingState;->b:I

    iput p3, p0, Lcom/google/android/gms/udc/SettingState;->c:I

    iput p4, p0, Lcom/google/android/gms/udc/SettingState;->d:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/udc/SettingState;->a:I

    return v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/udc/SettingState;->b:I

    return v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/udc/SettingState;->c:I

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/udc/SettingState;->d:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/udc/SettingState;->CREATOR:LbbG;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/udc/SettingState;->CREATOR:LbbG;

    invoke-static {p0, p1, p2}, LbbG;->a(Lcom/google/android/gms/udc/SettingState;Landroid/os/Parcel;I)V

    return-void
.end method
