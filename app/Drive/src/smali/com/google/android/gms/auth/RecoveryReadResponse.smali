.class public Lcom/google/android/gms/auth/RecoveryReadResponse;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:LaAY;


# instance fields
.field public final a:I

.field public a:Ljava/lang/String;

.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/auth/Country;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaAY;

    invoke-direct {v0}, LaAY;-><init>()V

    sput-object v0, Lcom/google/android/gms/auth/RecoveryReadResponse;->CREATOR:LaAY;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/auth/RecoveryReadResponse;->a:I

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/auth/Country;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/auth/RecoveryReadResponse;->a:I

    iput-object p2, p0, Lcom/google/android/gms/auth/RecoveryReadResponse;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/auth/RecoveryReadResponse;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/auth/RecoveryReadResponse;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/auth/RecoveryReadResponse;->a:Ljava/util/List;

    iput-object p6, p0, Lcom/google/android/gms/auth/RecoveryReadResponse;->d:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/gms/auth/RecoveryReadResponse;->e:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/gms/auth/RecoveryReadResponse;->f:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, LaAY;->a(Lcom/google/android/gms/auth/RecoveryReadResponse;Landroid/os/Parcel;I)V

    return-void
.end method
