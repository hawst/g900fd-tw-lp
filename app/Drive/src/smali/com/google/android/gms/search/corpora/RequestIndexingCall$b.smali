.class public Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lbbf;


# instance fields
.field public final a:I

.field public a:J

.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbbf;

    invoke-direct {v0}, Lbbf;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;->CREATOR:Lbbf;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;->a:I

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;->a:I

    iput-object p2, p0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;->b:Ljava/lang/String;

    iput-wide p4, p0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;->a:J

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;->CREATOR:Lbbf;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;->CREATOR:Lbbf;

    invoke-static {p0, p1, p2}, Lbbf;->a(Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;Landroid/os/Parcel;I)V

    return-void
.end method
