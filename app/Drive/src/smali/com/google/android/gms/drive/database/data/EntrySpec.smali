.class public abstract Lcom/google/android/gms/drive/database/data/EntrySpec;
.super Ljava/lang/Object;
.source "EntrySpec.java"

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field public final a:LaFO;

.field private volatile a:Ljava/lang/String;


# direct methods
.method protected constructor <init>(LaFO;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:Ljava/lang/String;

    .line 25
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFO;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    .line 26
    return-void
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public final b()Ljava/lang/String;
    .locals 5

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:Ljava/lang/String;

    .line 64
    if-nez v0, :cond_0

    .line 65
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s-%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    .line 66
    invoke-virtual {v4}, LaFO;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 67
    sget-object v1, LbiB;->c:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 68
    const/16 v1, 0x8

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:Ljava/lang/String;

    .line 70
    :cond_0
    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 30
    if-ne p0, p1, :cond_1

    move v1, v0

    .line 44
    :cond_0
    :goto_0
    return v1

    .line 33
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-nez v2, :cond_3

    .line 36
    instance-of v2, p1, Lcom/google/android/gms/drive/database/data/ResourceSpec;

    if-nez v2, :cond_2

    :goto_1
    invoke-static {v0}, LbiT;->a(Z)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 39
    :cond_3
    check-cast p1, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 40
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    iget-object v1, p1, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-virtual {v0, v1}, LaFO;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 49
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 54
    const-string v0, "EntrySpec[%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
