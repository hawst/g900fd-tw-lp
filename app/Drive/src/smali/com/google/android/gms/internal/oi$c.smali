.class public Lcom/google/android/gms/internal/oi$c;
.super Ljava/lang/Object;

# interfaces
.implements LaDc;
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:LaUM;


# instance fields
.field public final a:I

.field public a:J

.field public a:Lcom/google/android/gms/common/api/Status;

.field public a:[Lcom/google/android/gms/internal/oi$a;

.field public b:J

.field public c:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaUM;

    invoke-direct {v0}, LaUM;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/oi$c;->CREATOR:LaUM;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/oi$c;->a:I

    return-void
.end method

.method public constructor <init>(ILcom/google/android/gms/common/api/Status;[Lcom/google/android/gms/internal/oi$a;JJJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/oi$c;->a:I

    iput-object p2, p0, Lcom/google/android/gms/internal/oi$c;->a:Lcom/google/android/gms/common/api/Status;

    iput-object p3, p0, Lcom/google/android/gms/internal/oi$c;->a:[Lcom/google/android/gms/internal/oi$a;

    iput-wide p4, p0, Lcom/google/android/gms/internal/oi$c;->a:J

    iput-wide p6, p0, Lcom/google/android/gms/internal/oi$c;->b:J

    iput-wide p8, p0, Lcom/google/android/gms/internal/oi$c;->c:J

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/oi$c;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/oi$c;->CREATOR:LaUM;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/oi$c;->CREATOR:LaUM;

    invoke-static {p0, p1, p2}, LaUM;->a(Lcom/google/android/gms/internal/oi$c;Landroid/os/Parcel;I)V

    return-void
.end method
