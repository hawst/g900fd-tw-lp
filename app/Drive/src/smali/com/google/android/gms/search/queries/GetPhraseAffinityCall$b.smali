.class public Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lbbt;


# instance fields
.field public final a:I

.field public a:[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;

.field public a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbbt;

    invoke-direct {v0}, Lbbt;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$b;->CREATOR:Lbbt;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$b;->a:I

    return-void
.end method

.method public constructor <init>(I[Ljava/lang/String;[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$b;->a:I

    iput-object p2, p0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$b;->a:[Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$b;->a:[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$b;->CREATOR:Lbbt;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$b;->CREATOR:Lbbt;

    invoke-static {p0, p1, p2}, Lbbt;->a(Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$b;Landroid/os/Parcel;I)V

    return-void
.end method
