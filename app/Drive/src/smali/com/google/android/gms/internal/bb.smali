.class public final Lcom/google/android/gms/internal/bb;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:LaNh;


# instance fields
.field public final a:I

.field public final a:Ljava/lang/String;

.field public final b:I

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final c:Ljava/lang/String;

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:I

.field public final k:I

.field public final l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaNh;

    invoke-direct {v0}, LaNh;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/bb;->CREATOR:LaNh;

    return-void
.end method

.method public constructor <init>(IIIIIIIIILjava/lang/String;ILjava/lang/String;IILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/bb;->a:I

    iput p2, p0, Lcom/google/android/gms/internal/bb;->b:I

    iput p3, p0, Lcom/google/android/gms/internal/bb;->c:I

    iput p4, p0, Lcom/google/android/gms/internal/bb;->d:I

    iput p5, p0, Lcom/google/android/gms/internal/bb;->e:I

    iput p6, p0, Lcom/google/android/gms/internal/bb;->f:I

    iput p7, p0, Lcom/google/android/gms/internal/bb;->g:I

    iput p8, p0, Lcom/google/android/gms/internal/bb;->h:I

    iput p9, p0, Lcom/google/android/gms/internal/bb;->i:I

    iput-object p10, p0, Lcom/google/android/gms/internal/bb;->a:Ljava/lang/String;

    iput p11, p0, Lcom/google/android/gms/internal/bb;->j:I

    iput-object p12, p0, Lcom/google/android/gms/internal/bb;->b:Ljava/lang/String;

    iput p13, p0, Lcom/google/android/gms/internal/bb;->k:I

    iput p14, p0, Lcom/google/android/gms/internal/bb;->l:I

    iput-object p15, p0, Lcom/google/android/gms/internal/bb;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LayN;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/bb;->a:I

    invoke-virtual {p1}, LayN;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/bb;->b:I

    invoke-virtual {p1}, LayN;->b()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/bb;->c:I

    invoke-virtual {p1}, LayN;->c()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/bb;->d:I

    invoke-virtual {p1}, LayN;->d()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/bb;->e:I

    invoke-virtual {p1}, LayN;->e()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/bb;->f:I

    invoke-virtual {p1}, LayN;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/bb;->g:I

    invoke-virtual {p1}, LayN;->g()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/bb;->h:I

    invoke-virtual {p1}, LayN;->h()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/bb;->i:I

    invoke-virtual {p1}, LayN;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/bb;->a:Ljava/lang/String;

    invoke-virtual {p1}, LayN;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/bb;->j:I

    invoke-virtual {p1}, LayN;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/bb;->b:Ljava/lang/String;

    invoke-virtual {p1}, LayN;->j()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/bb;->k:I

    invoke-virtual {p1}, LayN;->k()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/bb;->l:I

    invoke-virtual {p1}, LayN;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/bb;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, LaNh;->a(Lcom/google/android/gms/internal/bb;Landroid/os/Parcel;I)V

    return-void
.end method
