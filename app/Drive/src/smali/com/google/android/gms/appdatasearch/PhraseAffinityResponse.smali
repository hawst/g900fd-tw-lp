.class public Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:LaAO;


# instance fields
.field public final a:I

.field public final a:Ljava/lang/String;

.field public final a:[I

.field public final a:[Lcom/google/android/gms/appdatasearch/CorpusId;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaAO;

    invoke-direct {v0}, LaAO;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->CREATOR:LaAO;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;[Lcom/google/android/gms/appdatasearch/CorpusId;[I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->a:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->a:[Lcom/google/android/gms/appdatasearch/CorpusId;

    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->a:[I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->CREATOR:LaAO;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->CREATOR:LaAO;

    invoke-static {p0, p1, p2}, LaAO;->a(Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;Landroid/os/Parcel;I)V

    return-void
.end method
