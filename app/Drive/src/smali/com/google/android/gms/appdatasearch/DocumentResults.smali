.class public Lcom/google/android/gms/appdatasearch/DocumentResults;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:LaAC;


# instance fields
.field public final a:I

.field public final a:Landroid/os/Bundle;

.field public final a:Ljava/lang/String;

.field public final b:Landroid/os/Bundle;

.field public final c:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaAC;

    invoke-direct {v0}, LaAC;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/DocumentResults;->CREATOR:LaAC;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/DocumentResults;->a:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/DocumentResults;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/DocumentResults;->a:Landroid/os/Bundle;

    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/DocumentResults;->b:Landroid/os/Bundle;

    iput-object p5, p0, Lcom/google/android/gms/appdatasearch/DocumentResults;->c:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/DocumentResults;->CREATOR:LaAC;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/DocumentResults;->CREATOR:LaAC;

    invoke-static {p0, p1, p2}, LaAC;->a(Lcom/google/android/gms/appdatasearch/DocumentResults;Landroid/os/Parcel;I)V

    return-void
.end method
