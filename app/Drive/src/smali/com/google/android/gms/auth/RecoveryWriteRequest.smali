.class public Lcom/google/android/gms/auth/RecoveryWriteRequest;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:LaAZ;


# instance fields
.field public final a:I

.field public a:Ljava/lang/String;

.field public a:Z

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaAZ;

    invoke-direct {v0}, LaAZ;-><init>()V

    sput-object v0, Lcom/google/android/gms/auth/RecoveryWriteRequest;->CREATOR:LaAZ;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/auth/RecoveryWriteRequest;->a:I

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/auth/RecoveryWriteRequest;->a:I

    iput-object p2, p0, Lcom/google/android/gms/auth/RecoveryWriteRequest;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/auth/RecoveryWriteRequest;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/auth/RecoveryWriteRequest;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/auth/RecoveryWriteRequest;->d:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/google/android/gms/auth/RecoveryWriteRequest;->a:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, LaAZ;->a(Lcom/google/android/gms/auth/RecoveryWriteRequest;Landroid/os/Parcel;I)V

    return-void
.end method
