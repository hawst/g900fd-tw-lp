.class public interface abstract Lcom/google/android/gms/games/multiplayer/Invitation;
.super Ljava/lang/Object;

# interfaces
.implements LaDA;
.implements LaLR;
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LaDA",
        "<",
        "Lcom/google/android/gms/games/multiplayer/Invitation;",
        ">;",
        "LaLR;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# virtual methods
.method public abstract a()J
.end method

.method public abstract a()Lcom/google/android/gms/games/Game;
.end method

.method public abstract a()Lcom/google/android/gms/games/multiplayer/Participant;
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public abstract b()I
.end method

.method public abstract c()I
.end method

.method public abstract d()I
.end method
