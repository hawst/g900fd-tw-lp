.class public Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:LaAP;


# instance fields
.field public final a:I

.field public final a:[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaAP;

    invoke-direct {v0}, LaAP;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->CREATOR:LaAP;

    return-void
.end method

.method public constructor <init>(I[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->a:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->a:[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->CREATOR:LaAP;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->CREATOR:LaAP;

    invoke-static {p0, p1, p2}, LaAP;->a(Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;Landroid/os/Parcel;I)V

    return-void
.end method
