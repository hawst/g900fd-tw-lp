.class public Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:LaAN;


# instance fields
.field public final a:I

.field public final a:Landroid/os/Bundle;

.field public final a:Lcom/google/android/gms/appdatasearch/CorpusId;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, LaAN;

    invoke-direct {v0}, LaAN;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;->CREATOR:LaAN;

    return-void
.end method

.method public constructor <init>(ILcom/google/android/gms/appdatasearch/CorpusId;Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;->a:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;->a:Lcom/google/android/gms/appdatasearch/CorpusId;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;->a:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;->CREATOR:LaAN;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;->CREATOR:LaAN;

    invoke-static {p0, p1, p2}, LaAN;->a(Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;Landroid/os/Parcel;I)V

    return-void
.end method
