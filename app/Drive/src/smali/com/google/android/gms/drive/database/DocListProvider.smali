.class public final Lcom/google/android/gms/drive/database/DocListProvider;
.super LalP;
.source "DocListProvider.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LalP",
        "<",
        "LaEH;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:Ljava/lang/String;


# instance fields
.field private a:Landroid/content/UriMatcher;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0}, LalP;-><init>()V

    .line 98
    return-void
.end method

.method private a()LaEz;
    .locals 1

    .prologue
    .line 176
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/DocListProvider;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaEH;

    iget-object v0, v0, LaEH;->a:LaEz;

    return-object v0
.end method

.method public static a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    sget-object v0, Lcom/google/android/gms/drive/database/DocListProvider;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 160
    sget-object v0, Lcom/google/android/gms/drive/database/DocListProvider;->a:Ljava/lang/String;

    return-object v0

    .line 159
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 112
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 113
    const-string v0, "com.google.android.apps.docs"

    .line 115
    :try_start_0
    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lcom/google/android/gms/drive/database/DocListProvider;

    invoke-direct {v2, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v3, 0x0

    .line 116
    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getProviderInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ProviderInfo;

    move-result-object v1

    .line 117
    iget-object v0, v1, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/drive/database/DocListProvider;->a(Ljava/lang/String;)V

    .line 126
    return-void

    .line 118
    :catch_0
    move-exception v1

    .line 122
    const-string v2, "DocListProvider"

    const-string v3, "DocListProvider not found in AndroidManifest.xml. Using %s as authority"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "com.google.android.apps.docs"

    aput-object v5, v4, v6

    invoke-static {v2, v1, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method private static declared-synchronized a(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 136
    const-class v3, Lcom/google/android/gms/drive/database/DocListProvider;

    monitor-enter v3

    if-eqz p0, :cond_2

    move v2, v1

    :goto_0
    :try_start_0
    const-string v4, "initializeAuthority called with null authority value"

    invoke-static {v2, v4}, LbiT;->a(ZLjava/lang/Object;)V

    .line 140
    sget-object v2, Lcom/google/android/gms/drive/database/DocListProvider;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/gms/drive/database/DocListProvider;->a:Ljava/lang/String;

    .line 141
    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_0
    :goto_1
    const-string v2, "initializeAuthority has been called with conflicting authority: %s vs %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    const/4 v5, 0x1

    sget-object v6, Lcom/google/android/gms/drive/database/DocListProvider;->a:Ljava/lang/String;

    aput-object v6, v4, v5

    .line 140
    invoke-static {v1, v2, v4}, LbiT;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 146
    sget-object v1, Lcom/google/android/gms/drive/database/DocListProvider;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_4

    .line 156
    :cond_1
    monitor-exit v3

    return-void

    :cond_2
    move v2, v0

    .line 136
    goto :goto_0

    :cond_3
    move v1, v0

    .line 141
    goto :goto_1

    .line 151
    :cond_4
    :try_start_1
    sput-object p0, Lcom/google/android/gms/drive/database/DocListProvider;->a:Ljava/lang/String;

    .line 153
    invoke-static {}, LaEG;->values()[LaEG;

    move-result-object v1

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_1

    aget-object v4, v1, v0

    .line 154
    invoke-virtual {v4, p0}, LaEG;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 153
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 136
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method


# virtual methods
.method protected a(Lbuu;)LaEH;
    .locals 1

    .prologue
    .line 102
    new-instance v0, LaEH;

    invoke-direct {v0}, LaEH;-><init>()V

    .line 103
    invoke-interface {p1, v0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 104
    return-object v0
.end method

.method protected bridge synthetic a(Lbuu;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/database/DocListProvider;->a(Lbuu;)LaEH;

    move-result-object v0

    return-object v0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 190
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "delete command not implemented."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/gms/drive/database/DocListProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 248
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URI "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 242
    :pswitch_0
    const-string v0, "vnd.android.cursor.dir/vnd.google.doclistentry"

    .line 245
    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "vnd.android.cursor.item/vnd.google.doclistentry"

    goto :goto_0

    .line 240
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 195
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "insert command not implemented."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 181
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/DocListProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/database/DocListProvider;->a(Landroid/content/Context;)V

    .line 182
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/drive/database/DocListProvider;->a:Landroid/content/UriMatcher;

    .line 183
    iget-object v0, p0, Lcom/google/android/gms/drive/database/DocListProvider;->a:Landroid/content/UriMatcher;

    invoke-static {}, Lcom/google/android/gms/drive/database/DocListProvider;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LaEG;->a:LaEG;

    invoke-static {v2}, LaEG;->a(LaEG;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/drive/database/DocListProvider;->a:Landroid/content/UriMatcher;

    invoke-static {}, Lcom/google/android/gms/drive/database/DocListProvider;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LaEG;->a:LaEG;

    invoke-static {v3}, LaEG;->a(LaEG;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/#"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 185
    return v4
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 201
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/DocListProvider;->a()LaEz;

    move-result-object v0

    invoke-virtual {v0}, LaEz;->a()V

    .line 202
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 207
    iget-object v1, p0, Lcom/google/android/gms/drive/database/DocListProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 216
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URI "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209
    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, LaER;->a()LaER;

    move-result-object v2

    invoke-virtual {v2}, LaER;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 210
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 209
    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 218
    :pswitch_1
    const-string v1, "DocumentView"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 219
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 220
    const-string v7, "title ASC"

    .line 225
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/DocListProvider;->a()LaEz;

    move-result-object v1

    invoke-virtual {v1}, LaEz;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 229
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/DocListProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 230
    return-object v0

    :cond_0
    move-object v7, p5

    goto :goto_0

    .line 207
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 235
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "delete command not implemented."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
