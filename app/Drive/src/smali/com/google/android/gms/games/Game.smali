.class public interface abstract Lcom/google/android/gms/games/Game;
.super Ljava/lang/Object;

# interfaces
.implements LaDA;
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LaDA",
        "<",
        "Lcom/google/android/gms/games/Game;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# virtual methods
.method public abstract a()Landroid/net/Uri;
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public abstract a()Z
.end method

.method public abstract a_()I
.end method

.method public abstract b()I
.end method

.method public abstract b()Landroid/net/Uri;
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public abstract b()Z
.end method

.method public abstract c()I
.end method

.method public abstract c()Landroid/net/Uri;
.end method

.method public abstract c()Ljava/lang/String;
.end method

.method public abstract c()Z
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method public abstract d()Z
.end method

.method public abstract e()Ljava/lang/String;
.end method

.method public abstract e()Z
.end method

.method public abstract f()Ljava/lang/String;
.end method

.method public abstract f()Z
.end method

.method public abstract g()Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract g()Z
.end method

.method public abstract h()Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract i()Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract j()Ljava/lang/String;
.end method
