.class public interface abstract Lcom/google/android/gms/games/multiplayer/Participant;
.super Ljava/lang/Object;

# interfaces
.implements LaDA;
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LaDA",
        "<",
        "Lcom/google/android/gms/games/multiplayer/Participant;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# virtual methods
.method public abstract a()Landroid/net/Uri;
.end method

.method public abstract a()Lcom/google/android/gms/games/Player;
.end method

.method public abstract a()Lcom/google/android/gms/games/multiplayer/ParticipantResult;
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public abstract a()Z
.end method

.method public abstract b()I
.end method

.method public abstract b()Landroid/net/Uri;
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public abstract c()Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract d()Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract e()Ljava/lang/String;
.end method

.method public abstract e_()I
.end method
