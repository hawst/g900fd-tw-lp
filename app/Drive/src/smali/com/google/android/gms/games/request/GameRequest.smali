.class public interface abstract Lcom/google/android/gms/games/request/GameRequest;
.super Ljava/lang/Object;

# interfaces
.implements LaDA;
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LaDA",
        "<",
        "Lcom/google/android/gms/games/request/GameRequest;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# virtual methods
.method public abstract a()J
.end method

.method public abstract a()Lcom/google/android/gms/games/Game;
.end method

.method public abstract a()Lcom/google/android/gms/games/Player;
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public abstract a()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/games/Player;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a()[B
.end method

.method public abstract a_(Ljava/lang/String;)I
.end method

.method public abstract b()I
.end method

.method public abstract b()J
.end method

.method public abstract c()I
.end method
