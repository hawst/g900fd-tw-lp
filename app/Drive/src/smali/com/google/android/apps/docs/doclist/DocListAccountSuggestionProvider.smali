.class public Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;
.super Landroid/content/SearchRecentSuggestionsProvider;
.source "DocListAccountSuggestionProvider.java"


# static fields
.field private static a:Ljava/lang/String;

.field private static a:LzT;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/content/SearchRecentSuggestionsProvider;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    invoke-static {p0}, Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;->a(Landroid/content/Context;)V

    .line 85
    sget-object v0, Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;->a:Ljava/lang/String;

    return-object v0
.end method

.method private static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 30
    sget-object v0, Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 44
    :goto_0
    return-void

    .line 34
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 36
    :try_start_0
    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getProviderInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ProviderInfo;

    move-result-object v0

    .line 38
    iget-object v0, v0, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    sput-object v0, Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;->a:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    new-instance v0, LzT;

    sget-object v1, Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, LzT;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;->a:LzT;

    goto :goto_0

    .line 39
    :catch_0
    move-exception v0

    .line 40
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V
    .locals 1

    .prologue
    .line 79
    invoke-static {p0}, Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;->a(Landroid/content/Context;)V

    .line 80
    sget-object v0, Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;->a:LzT;

    invoke-virtual {v0, p1}, LzT;->a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V

    .line 81
    return-void
.end method


# virtual methods
.method public onCreate()Z
    .locals 3

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;->a(Landroid/content/Context;)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;->setupSuggestions(Ljava/lang/String;I)V

    .line 56
    sget-object v0, Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;->a:LzT;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LzT;->attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V

    .line 59
    invoke-super {p0}, Landroid/content/SearchRecentSuggestionsProvider;->onCreate()Z

    move-result v0

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 68
    array-length v0, p4

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    aget-object v0, p4, v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    sget-object v0, Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;->a:LzT;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LzT;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 71
    :goto_0
    return-object v0

    :cond_0
    invoke-super/range {p0 .. p5}, Landroid/content/SearchRecentSuggestionsProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method
