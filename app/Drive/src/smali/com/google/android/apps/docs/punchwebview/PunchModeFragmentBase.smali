.class public abstract Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;
.super Lcom/google/android/apps/docs/app/BaseFragment;
.source "PunchModeFragmentBase.java"


# instance fields
.field public a:LXx;

.field public a:LYO;

.field final a:LYR;

.field public final c:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;LYR;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseFragment;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->c:Ljava/lang/String;

    .line 45
    iput-object p2, p0, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->a:LYR;

    .line 46
    return-void
.end method


# virtual methods
.method protected a(Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->a(Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    return-void
.end method

.method protected a(Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->a()LH;

    move-result-object v0

    invoke-virtual {v0}, LH;->a()LM;

    move-result-object v0

    invoke-virtual {v0}, LM;->a()Lac;

    move-result-object v0

    .line 91
    sget v1, LpN;->main_container:I

    invoke-virtual {v0, v1, p1, p2}, Lac;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lac;

    .line 92
    if-eqz p3, :cond_0

    .line 93
    invoke-virtual {v0, p3}, Lac;->a(Ljava/lang/String;)Lac;

    .line 96
    :cond_0
    invoke-virtual {v0}, Lac;->b()I

    .line 97
    return-void
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseFragment;->a_(Landroid/os/Bundle;)V

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->a:LYO;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->a:LYR;

    invoke-interface {v0, v1}, LYO;->a(LYR;)V

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->c:Ljava/lang/String;

    const-string v1, "Created %s mode fragment."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->a:LYR;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 53
    return-void
.end method

.method public h()V
    .locals 5

    .prologue
    .line 57
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseFragment;->h()V

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->c:Ljava/lang/String;

    const-string v1, "Destroyed %s mode fragment."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->a:LYR;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 59
    return-void
.end method

.method protected abstract m()Z
.end method

.method public abstract w()V
.end method

.method protected x()V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method protected y()V
    .locals 0

    .prologue
    .line 75
    return-void
.end method

.method protected z()V
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->a()LH;

    move-result-object v0

    invoke-virtual {v0}, LH;->a()LM;

    move-result-object v0

    invoke-virtual {v0}, LM;->a()Lac;

    move-result-object v0

    .line 104
    invoke-virtual {v0, p0}, Lac;->a(Landroid/support/v4/app/Fragment;)Lac;

    .line 105
    invoke-virtual {v0}, Lac;->a()I

    .line 106
    return-void
.end method
