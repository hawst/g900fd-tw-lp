.class public Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;
.super Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;
.source "RemoveEntriesFragment.java"


# instance fields
.field private a:LaGD;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;-><init>()V

    .line 25
    return-void
.end method

.method public static a(LaGD;)Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;
    .locals 3

    .prologue
    .line 42
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 43
    const-string v1, "entrySpecs"

    invoke-static {p0}, LbnG;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 44
    new-instance v1, Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;

    invoke-direct {v1}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;-><init>()V

    .line 45
    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;->e(Landroid/os/Bundle;)V

    .line 46
    return-object v1
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;->a:LaGD;

    invoke-virtual {v0}, LaGD;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;->b()Landroid/app/Dialog;

    move-result-object v1

    .line 80
    :goto_0
    return-object v1

    .line 72
    :cond_0
    const/4 v5, 0x0

    .line 73
    sget v3, Lxi;->plus_photo_item_remove_dialog_confirm_button:I

    .line 74
    sget v2, Lxi;->plus_photo_item_remove_dialog_title:I

    .line 75
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxg;->plus_photo_item_remove_dialog_text:I

    iget-object v4, p0, Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;->a:LaGD;

    .line 76
    invoke-virtual {v4}, LaGD;->size()I

    move-result v4

    .line 75
    invoke-virtual {v0, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v4

    .line 78
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v1

    move-object v0, p0

    .line 79
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;->a(Landroid/app/Dialog;IILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;->a_(Landroid/os/Bundle;)V

    .line 53
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "entrySpecs"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 54
    invoke-static {v0}, LaGD;->a(Ljava/util/Collection;)LaGD;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;->a:LaGD;

    .line 55
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 5

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;->a()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 102
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;->a()LH;

    move-result-object v1

    .line 103
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 104
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;->c()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1}, LH;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Landroid/support/v4/app/Fragment;->a(IILandroid/content/Intent;)V

    .line 107
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 108
    invoke-virtual {v1}, LH;->finish()V

    .line 109
    return-void
.end method

.method protected t()V
    .locals 3

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;->a()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;->a(Landroid/app/Dialog;ILjava/lang/String;)V

    .line 86
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;->a()LH;

    move-result-object v0

    check-cast v0, LFu;

    invoke-interface {v0}, LFu;->f()V

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;->a()V

    .line 88
    return-void
.end method

.method protected u()V
    .locals 0

    .prologue
    .line 96
    return-void
.end method
