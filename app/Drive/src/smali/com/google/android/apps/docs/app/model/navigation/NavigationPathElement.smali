.class public Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;
.super Ljava/lang/Object;
.source "NavigationPathElement.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Landroid/os/Parcelable;

.field private final a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lwo;

    invoke-direct {v0}, Lwo;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    .line 21
    return-void
.end method


# virtual methods
.method public a()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a:Landroid/os/Parcelable;

    return-object v0
.end method

.method public a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    return-object v0
.end method

.method public a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a:Landroid/os/Parcelable;

    .line 29
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 68
    if-ne p0, p1, :cond_0

    .line 69
    const/4 v0, 0x1

    .line 74
    :goto_0
    return v0

    .line 70
    :cond_0
    instance-of v0, p1, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    if-eqz v0, :cond_1

    .line 71
    check-cast p1, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    .line 72
    iget-object v0, p1, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 74
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 85
    const-string v0, "NavigationPathElement { CriterionSet: %s, savedState=%s }"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a:Landroid/os/Parcelable;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 43
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a:Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 44
    return-void
.end method
