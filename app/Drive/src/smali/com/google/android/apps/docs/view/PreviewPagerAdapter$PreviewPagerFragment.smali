.class public Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;
.super Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;
.source "PreviewPagerAdapter.java"

# interfaces
.implements Ltl;


# instance fields
.field private a:I

.field public a:LPp;

.field private a:Landroid/widget/ProgressBar;

.field public a:LaqA;

.field private a:LaqG;

.field public a:LaqL;

.field private a:Laqz;

.field private final a:LbsJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbsJ",
            "<",
            "Laqz;",
            ">;"
        }
    .end annotation
.end field

.field private a:LbsU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbsU",
            "<",
            "Laqz;",
            ">;"
        }
    .end annotation
.end field

.field public a:LqK;

.field private a:Z

.field private b:Landroid/view/ViewGroup;

.field private b:Z

.field private c:Ljava/lang/String;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;-><init>()V

    .line 106
    new-instance v0, LaqH;

    invoke-direct {v0, p0}, LaqH;-><init>(Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LbsJ;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;)I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:I

    return v0
.end method

.method private a(Landroid/view/View;)Landroid/view/View;
    .locals 3

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LaqG;

    iget v1, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:I

    invoke-interface {v0, v1}, LaqG;->a(I)LCv;

    move-result-object v0

    .line 310
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LaqG;

    iget v2, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:I

    invoke-interface {v1, v2}, LaqG;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 311
    invoke-interface {v0}, LCv;->a()LaGv;

    move-result-object v0

    .line 312
    sget-object v1, LaGv;->b:LaGv;

    invoke-virtual {v1, v0}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 313
    sget v0, Lxb;->launcher_kix_icon:I

    sget v1, Lxi;->preview_document_available_in_docs_app:I

    sget v2, Lxi;->preview_open_in_docs_app_description:I

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a(Landroid/view/View;III)Landroid/view/View;

    move-result-object v0

    .line 327
    :goto_0
    return-object v0

    .line 318
    :cond_0
    sget-object v1, LaGv;->i:LaGv;

    invoke-virtual {v1, v0}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 319
    sget v0, Lxb;->launcher_trix_icon:I

    sget v1, Lxi;->preview_document_available_in_sheets_app:I

    sget v2, Lxi;->preview_open_in_sheets_app_description:I

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a(Landroid/view/View;III)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 327
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/view/View;III)Landroid/view/View;
    .locals 2

    .prologue
    .line 287
    sget v0, Lxc;->no_preview_open_in_another_app:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 288
    invoke-virtual {p0, p4}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 290
    sget v0, Lxc;->preview_offline_app_icon:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 291
    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 293
    sget v0, Lxc;->preview_offline_message_text:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 294
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(I)V

    .line 296
    new-instance v0, LaqJ;

    invoke-direct {v0, p0}, LaqJ;-><init>(Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 303
    return-object v1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;)LaqG;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LaqG;

    return-object v0
.end method

.method private a(Laqz;)V
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LaqG;

    invoke-interface {v0}, LaqG;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laqz;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:Laqz;

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->b:Landroid/view/ViewGroup;

    invoke-interface {p1, v0}, Laqz;->a(Landroid/view/ViewGroup;)V

    .line 263
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->u()V

    .line 267
    :goto_0
    return-void

    .line 265
    :cond_0
    invoke-interface {p1}, Laqz;->a()V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->y()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;Laqz;)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a(Laqz;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->z()V

    return-void
.end method

.method private w()V
    .locals 3

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 163
    const-string v1, "position"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 164
    const-string v2, "entrySpec.v2"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 165
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    iget-object v2, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LaqG;

    invoke-interface {v2, v1, v0}, LaqG;->a(ILcom/google/android/gms/drive/database/data/EntrySpec;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 169
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a()LH;

    move-result-object v0

    .line 170
    if-eqz v0, :cond_0

    .line 171
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    iput v1, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:I

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->e:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 177
    if-eqz v0, :cond_2

    .line 178
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:Z

    .line 179
    iput-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->d:Landroid/view/View;

    .line 184
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->e:Landroid/view/View;

    sget v2, Lxc;->page_container:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->b:Landroid/view/ViewGroup;

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->b:Landroid/view/ViewGroup;

    new-instance v2, LaqI;

    invoke-direct {v2, p0}, LaqI;-><init>(Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LaqG;

    invoke-interface {v0, v1}, LaqG;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 194
    iget-object v1, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 196
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->x()V

    goto :goto_0

    .line 181
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->e:Landroid/view/View;

    sget v2, Lxc;->no_preview_icon:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->d:Landroid/view/View;

    goto :goto_1
.end method

.method private x()V
    .locals 5

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LaqG;

    iget v1, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:I

    .line 206
    invoke-interface {v0, v1}, LaqG;->a(I)Lcom/google/android/apps/docs/utils/FetchSpec;

    move-result-object v0

    .line 207
    iget-object v1, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LaqG;

    iget v2, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:I

    invoke-interface {v1, v2}, LaqG;->a(I)LCv;

    move-result-object v1

    .line 208
    invoke-interface {v1}, LCv;->a()LaGv;

    move-result-object v2

    .line 209
    invoke-interface {v1}, LCv;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1}, LCv;->a()LaGv;

    move-result-object v4

    invoke-virtual {v4}, LaGv;->toString()Ljava/lang/String;

    move-result-object v4

    .line 208
    invoke-static {v2, v3, v4}, Lala;->a(LaGv;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->c:Ljava/lang/String;

    .line 210
    if-eqz v0, :cond_0

    .line 211
    iget-object v2, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LaqA;

    .line 212
    invoke-interface {v1}, LCv;->a()LaGv;

    move-result-object v3

    invoke-interface {v1}, LCv;->e()Ljava/lang/String;

    move-result-object v1

    .line 211
    invoke-virtual {v2, v0, v3, v1}, LaqA;->a(Lcom/google/android/apps/docs/utils/FetchSpec;LaGv;Ljava/lang/String;)LbsU;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LbsU;

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LbsU;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LbsJ;

    invoke-static {}, Lanj;->a()Ljava/util/concurrent/Executor;

    move-result-object v2

    invoke-static {v0, v1, v2}, LbsK;->a(LbsU;LbsJ;Ljava/util/concurrent/Executor;)V

    .line 217
    :goto_0
    return-void

    .line 215
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->y()V

    goto :goto_0
.end method

.method private y()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 246
    iget-boolean v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a()LH;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 247
    iput-boolean v2, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->b:Z

    .line 248
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LaqG;

    invoke-interface {v0}, LaqG;->b()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:I

    if-ne v0, v1, :cond_0

    .line 249
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->t()V

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LaqG;

    invoke-interface {v0}, LaqG;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 254
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 255
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->u()V

    .line 257
    :cond_1
    return-void
.end method

.method private z()V
    .locals 4

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LqK;

    const-string v1, "documentPreview"

    const-string v2, "openItemEvent"

    iget-object v3, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->b:Z

    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a()LH;

    move-result-object v0

    check-cast v0, LaqG;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LaqG;

    .line 147
    sget v0, Lxe;->preview_pager_fragment:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->e:Landroid/view/View;

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->e:Landroid/view/View;

    sget v1, Lxc;->loading_spinner:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:Landroid/widget/ProgressBar;

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LaqG;

    invoke-interface {v0}, LaqG;->a()I

    move-result v0

    if-nez v0, :cond_0

    .line 152
    const-string v0, "PreviewPagerAdapter"

    const-string v1, "Cannot initialize the view yet, cursor is not ready"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LaqG;

    invoke-interface {v0, p0}, LaqG;->a(Ltl;)V

    .line 158
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->e:Landroid/view/View;

    return-object v0

    .line 155
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->w()V

    goto :goto_0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->w()V

    .line 202
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:Laqz;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:Laqz;

    invoke-interface {v0}, Laqz;->a()V

    .line 225
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->e()V

    .line 226
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LbsU;

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LbsU;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LbsU;->cancel(Z)Z

    .line 234
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->h()V

    .line 235
    return-void
.end method

.method public o_()V
    .locals 2

    .prologue
    .line 275
    invoke-super {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->o_()V

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:LPp;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LPp;->a(Z)V

    .line 277
    return-void
.end method

.method public t()V
    .locals 3

    .prologue
    .line 238
    iget-boolean v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 239
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a()LH;

    move-result-object v0

    invoke-static {v0}, LUs;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a()LH;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->d:Landroid/view/View;

    sget v2, Lxi;->error_opening_document:I

    .line 240
    invoke-static {v0, v1, v2}, LUs;->a(Landroid/content/Context;Landroid/view/View;I)V

    .line 243
    :cond_0
    return-void
.end method

.method protected u()V
    .locals 2

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 271
    return-void
.end method

.method public v()V
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:Laqz;

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->a:Laqz;

    invoke-interface {v0}, Laqz;->b()V

    .line 283
    :cond_0
    return-void
.end method
