.class public Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;
.super Lcom/google/android/apps/docs/fragment/DetailFragment;
.source "LegacyDetailFragment.java"

# interfaces
.implements Laco;
.implements LvV;


# static fields
.field private static final a:I


# instance fields
.field public a:LBZ;

.field public a:LCM;

.field public a:LQU;

.field public a:LQr;

.field public a:LRA;

.field public a:LUi;

.field public a:LaGM;

.field public a:LaKR;

.field a:LabW;

.field public a:LacF;

.field public a:Lacj;

.field private a:Lacr;

.field public a:Lacs;

.field public a:Lagl;

.field public a:LajO;

.field public a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "LamY;",
            ">;"
        }
    .end annotation
.end field

.field public a:Lald;

.field public a:LamL;

.field private a:LamY;

.field private a:Landroid/graphics/Bitmap;

.field private a:Landroid/os/Handler;

.field private a:Landroid/support/v4/app/DialogFragment;

.field private a:Landroid/widget/CompoundButton;

.field private a:Landroid/widget/ImageView;

.field a:Landroid/widget/LinearLayout;

.field private a:Landroid/widget/ProgressBar;

.field private a:Landroid/widget/ScrollView;

.field a:Landroid/widget/TextView;

.field private a:Landroid/widget/ToggleButton;

.field private a:Lcom/google/android/apps/docs/view/ThumbnailView;

.field public a:LqK;

.field public a:LsC;

.field public a:LtK;

.field public a:LvU;

.field private a:Z

.field private b:I

.field public b:Lald;

.field private b:Landroid/widget/LinearLayout;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/LinearLayout;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/view/View;

.field private d:Landroid/widget/LinearLayout;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/view/View;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 122
    sget v0, Lxb;->ic_contact_list_picture:I

    sput v0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/DetailFragment;-><init>()V

    .line 171
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Z

    .line 232
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/support/v4/app/DialogFragment;

    return-void
.end method

.method private a()LaGu;
    .locals 2

    .prologue
    .line 874
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LvU;

    invoke-interface {v0}, LvU;->a()LaGu;

    move-result-object v0

    .line 875
    if-nez v0, :cond_0

    .line 876
    const-string v0, "DetailFragment"

    const-string v1, "Failed to load the Entry"

    invoke-static {v0, v1}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 877
    const/4 v0, 0x0

    .line 880
    :cond_0
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;)LaGu;
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a()LaGu;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;LamY;)LamY;
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LamY;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/os/Handler;

    return-object v0
.end method

.method private a(LacD;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;LabI;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 757
    sget v0, Lxe;->detail_fragment_sharing_entry:I

    invoke-virtual {p2, v0, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 760
    invoke-virtual {p1}, LacD;->a()Laci;

    move-result-object v0

    invoke-virtual {v0}, Laci;->a()Lqv;

    move-result-object v0

    sget-object v1, Lqv;->a:Lqv;

    if-eq v0, v1, :cond_1

    move v4, v5

    .line 762
    :goto_0
    if-eqz v4, :cond_0

    .line 763
    new-instance v0, LRw;

    invoke-direct {v0, p0, p1}, LRw;-><init>(Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;LacD;)V

    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 770
    :cond_0
    invoke-virtual {v7, v4}, Landroid/view/View;->setFocusable(Z)V

    .line 772
    sget v0, Lxc;->share_name:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 773
    sget v1, Lxc;->share_description:I

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 774
    sget v2, Lxc;->share_role:I

    invoke-virtual {v7, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 775
    sget v3, Lxc;->share_badge:I

    invoke-virtual {v7, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/QuickContactBadge;

    .line 778
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a()LH;

    move-result-object v8

    invoke-static {p1, v8}, LacH;->a(LacD;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 779
    sget v9, Lxi;->acl_badge_description:I

    new-array v10, v5, [Ljava/lang/Object;

    aput-object v8, v10, v6

    invoke-virtual {p0, v9, v10}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 780
    invoke-virtual {v3, v6}, Landroid/widget/QuickContactBadge;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 784
    invoke-virtual {v3, v5}, Landroid/widget/QuickContactBadge;->setFocusable(Z)V

    .line 785
    if-eqz v4, :cond_2

    invoke-virtual {v7}, Landroid/view/View;->getId()I

    move-result v4

    .line 786
    :goto_1
    invoke-virtual {v3, v4}, Landroid/widget/QuickContactBadge;->setNextFocusRightId(I)V

    .line 787
    invoke-virtual {v3}, Landroid/widget/QuickContactBadge;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/QuickContactBadge;->setNextFocusLeftId(I)V

    .line 788
    invoke-virtual {v3}, Landroid/widget/QuickContactBadge;->getId()I

    move-result v4

    invoke-virtual {v7, v4}, Landroid/view/View;->setNextFocusLeftId(I)V

    .line 790
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 792
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a()LH;

    move-result-object v0

    invoke-static {p1, v0}, LacH;->b(LacD;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 791
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 793
    invoke-virtual {p1}, LacD;->a()Laci;

    move-result-object v0

    invoke-virtual {v0}, Laci;->a()Lqt;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a()LH;

    move-result-object v1

    invoke-virtual {v0, v1}, Lqt;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 794
    invoke-virtual {p1}, LacD;->a()LabD;

    move-result-object v0

    invoke-direct {p0, v3, p4, v0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a(Landroid/widget/QuickContactBadge;LabI;LabD;)V

    .line 796
    return-object v7

    :cond_1
    move v4, v6

    .line 760
    goto/16 :goto_0

    .line 785
    :cond_2
    invoke-virtual {v3}, Landroid/widget/QuickContactBadge;->getId()I

    move-result v4

    goto :goto_1
.end method

.method private a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 800
    sget v0, Lxe;->detail_fragment_sharing_add_entry:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 801
    new-instance v1, LRx;

    invoke-direct {v1, p0}, LRx;-><init>(Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 811
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 813
    return-object v0
.end method

.method private a()Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;
    .locals 3

    .prologue
    .line 589
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    .line 590
    sget v1, Lxa;->detail_fragment_thumbnail_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 591
    sget v2, Lxa;->detail_fragment_thumbnail_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 592
    new-instance v2, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    invoke-direct {v2, v1, v0}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;-><init>(II)V

    return-object v2
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;)Lcom/google/android/apps/docs/view/ThumbnailView;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    return-object v0
.end method

.method private a(LaGu;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 463
    invoke-interface {p1}, LaGu;->d()Ljava/lang/String;

    move-result-object v0

    .line 464
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a()LH;

    move-result-object v1

    .line 465
    invoke-interface {p1}, LaGu;->b()Ljava/util/Date;

    move-result-object v2

    .line 466
    sget v3, Lxi;->detail_fragment_general_info_modified:I

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 467
    invoke-static {v1}, Landroid/text/format/DateFormat;->getLongDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object v0, v4, v1

    .line 466
    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 469
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;)Lrt;
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a()Lrt;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 596
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LamY;

    if-eqz v0, :cond_0

    .line 597
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LamY;

    invoke-interface {v0}, LamY;->a()V

    .line 598
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LamY;

    .line 600
    :cond_0
    return-void
.end method

.method private a(LaGu;)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 454
    invoke-interface {p1}, LaGu;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 455
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/ToggleButton;

    invoke-virtual {v3, v0}, Landroid/widget/ToggleButton;->setVisibility(I)V

    .line 456
    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 457
    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 459
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->g:Landroid/widget/TextView;

    invoke-interface {p1}, LaGu;->e()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 460
    return-void

    :cond_0
    move v0, v2

    .line 454
    goto :goto_0

    :cond_1
    move v2, v1

    .line 459
    goto :goto_1
.end method

.method private a(LaGu;Z)V
    .locals 2

    .prologue
    .line 519
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/ThumbnailView;->clearAnimation()V

    .line 521
    if-eqz p2, :cond_0

    .line 522
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/graphics/Bitmap;

    .line 524
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/ThumbnailView;->setVisibility(I)V

    .line 525
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/view/ThumbnailView;->setEntry(LaGu;)V

    .line 527
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a(LaGu;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 528
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->d(LaGu;)V

    .line 536
    :cond_1
    :goto_0
    return-void

    .line 529
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    if-nez p2, :cond_3

    .line 530
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/ThumbnailView;->setImage(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 532
    :cond_3
    if-nez p2, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LamY;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LamY;

    invoke-interface {v0}, LamY;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 533
    :cond_4
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->e(LaGu;)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 321
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 322
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 323
    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 324
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 325
    return-void
.end method

.method private a(Landroid/widget/QuickContactBadge;LabI;LabD;)V
    .locals 2

    .prologue
    .line 855
    if-eqz p3, :cond_2

    invoke-interface {p3}, LabD;->a()Ljava/lang/String;

    move-result-object v0

    .line 857
    :goto_0
    sget v1, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:I

    invoke-virtual {p1, v1}, Landroid/widget/QuickContactBadge;->setImageResource(I)V

    .line 858
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 859
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/widget/QuickContactBadge;->assignContactFromEmail(Ljava/lang/String;Z)V

    .line 861
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/widget/QuickContactBadge;->setMode(I)V

    .line 862
    if-eqz p3, :cond_1

    .line 863
    invoke-interface {p3}, LabD;->b()J

    move-result-wide v0

    invoke-virtual {p2, p1, v0, v1}, LabI;->a(Landroid/widget/ImageView;J)V

    .line 865
    :cond_1
    return-void

    .line 855
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/widget/TextView;ILjava/lang/Long;)V
    .locals 1

    .prologue
    .line 509
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a()LH;

    move-result-object v0

    invoke-static {v0, p2, p3}, Lalr;->a(Landroid/content/Context;ILjava/lang/Long;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 511
    return-void
.end method

.method private a(Landroid/widget/TextView;ILjava/util/Date;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 496
    if-eqz p3, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 497
    if-eqz p3, :cond_1

    .line 498
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 499
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a()LH;

    move-result-object v2

    invoke-static {v2}, Landroid/text/format/DateFormat;->getLongDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, p2, v0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 501
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 502
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 506
    :goto_1
    return-void

    .line 496
    :cond_0
    const/4 v0, 0x4

    goto :goto_0

    .line 504
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;LaGu;)V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->d(LaGu;)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 723
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 724
    check-cast v0, Lcom/google/android/apps/docs/fragment/CustomFocusRelativeLayout;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/docs/fragment/CustomFocusRelativeLayout;->setNextFocusRightEnabled(Z)V

    goto :goto_0

    .line 727
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/CompoundButton;

    .line 728
    invoke-virtual {v0}, Landroid/widget/CompoundButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/CompoundButton;

    move-object v1, v0

    .line 729
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 730
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    sget v2, Lxc;->share_badge:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 731
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setNextFocusDownId(I)V

    .line 732
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 734
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 735
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusDownId(I)V

    .line 741
    :goto_2
    invoke-static {p1}, Lbnm;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    sget v1, Lxc;->add_collaborator_icon:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 742
    invoke-static {p1}, Lbnm;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/fragment/CustomFocusRelativeLayout;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/docs/fragment/CustomFocusRelativeLayout;->setNextFocusLeftEnabled(Z)V

    .line 747
    :cond_1
    :goto_3
    return-void

    .line 728
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    move-object v1, v0

    goto :goto_1

    .line 737
    :cond_3
    invoke-static {p1}, Lbnm;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/fragment/CustomFocusRelativeLayout;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/docs/fragment/CustomFocusRelativeLayout;->setNextFocusDownEnabled(Z)V

    goto :goto_2

    .line 745
    :cond_4
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setNextFocusDownId(I)V

    goto :goto_3
.end method

.method private static a(Ljava/util/List;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 750
    invoke-virtual {p1}, Landroid/view/View;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 751
    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 753
    :cond_0
    return-void
.end method

.method private a(LaGu;)Z
    .locals 1

    .prologue
    .line 603
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LaKR;

    invoke-interface {v0}, LaKR;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-interface {p1}, LaGu;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 604
    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lacr;)Z
    .locals 2

    .prologue
    .line 608
    invoke-interface {p1}, Lacr;->a()Lqu;

    move-result-object v0

    .line 614
    sget-object v1, Lqu;->a:Lqu;

    if-eq v0, v1, :cond_0

    sget-object v1, Lqu;->n:Lqu;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;)Lrt;
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a()Lrt;

    move-result-object v0

    return-object v0
.end method

.method private b(LaGu;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 476
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LtK;

    sget-object v2, Lry;->af:Lry;

    .line 477
    invoke-interface {v1, v2}, LtK;->a(LtJ;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, LaGu;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    .line 478
    :goto_0
    if-eqz v1, :cond_2

    .line 479
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 480
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/CompoundButton;

    invoke-virtual {v2, v0}, Landroid/widget/CompoundButton;->setVisibility(I)V

    .line 482
    if-eqz v1, :cond_0

    .line 483
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/CompoundButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/ThumbnailView;->setNextFocusDownId(I)V

    .line 486
    :cond_0
    invoke-interface {p1}, LaGu;->f()Z

    move-result v0

    .line 487
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/CompoundButton;

    invoke-virtual {v1, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 488
    return-void

    :cond_1
    move v1, v0

    .line 477
    goto :goto_0

    .line 478
    :cond_2
    const/16 v0, 0x8

    goto :goto_1
.end method

.method private b(Z)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 392
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 450
    :cond_0
    :goto_0
    return-void

    .line 398
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a()LaGu;

    move-result-object v1

    .line 399
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Z

    if-eqz v0, :cond_4

    if-eqz v1, :cond_4

    move v0, v2

    .line 400
    :goto_1
    iget-object v5, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    invoke-virtual {v5, v0}, Lcom/google/android/apps/docs/view/ThumbnailView;->setOpenButtonVisibility(Z)V

    .line 401
    iget-object v5, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    if-eqz v0, :cond_5

    sget v0, Lxi;->thumbnail_open:I

    .line 402
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 401
    :goto_2
    invoke-virtual {v5, v0}, Lcom/google/android/apps/docs/view/ThumbnailView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 403
    if-eqz v1, :cond_0

    .line 407
    if-eqz p1, :cond_2

    .line 408
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LqK;

    const-string v5, "doclist"

    const-string v6, "showEntryDetailEvent"

    invoke-virtual {v0, v5, v6}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/ScrollView;

    const/16 v5, 0x21

    invoke-virtual {v0, v5}, Landroid/widget/ScrollView;->fullScroll(I)Z

    .line 413
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->c:Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a(LaGu;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 415
    invoke-interface {v1}, LaGu;->c()Ljava/lang/Long;

    move-result-object v5

    .line 416
    if-nez v5, :cond_6

    move-object v0, v4

    .line 418
    :goto_3
    iget-object v5, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->d:Landroid/widget/TextView;

    sget v6, Lxi;->detail_fragment_general_info_modified_by_me:I

    invoke-direct {p0, v5, v6, v0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a(Landroid/widget/TextView;ILjava/util/Date;)V

    .line 421
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->e:Landroid/widget/TextView;

    sget v5, Lxi;->detail_fragment_general_info_opened_by_me:I

    .line 422
    invoke-interface {v1}, LaGu;->c()Ljava/util/Date;

    move-result-object v6

    .line 421
    invoke-direct {p0, v0, v5, v6}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a(Landroid/widget/TextView;ILjava/util/Date;)V

    .line 425
    instance-of v0, v1, LaGo;

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 426
    check-cast v0, LaGo;

    invoke-interface {v0}, LaGo;->b()Ljava/lang/Long;

    move-result-object v4

    .line 427
    const-string v0, "DetailFragment"

    const-string v5, "Quota: %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v4, v2, v3

    invoke-static {v0, v5, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 430
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LsC;

    invoke-interface {v0}, LsC;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 431
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->f:Landroid/widget/TextView;

    sget v2, Lxi;->detail_fragment_general_info_quota_used:I

    invoke-direct {p0, v0, v2, v4}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a(Landroid/widget/TextView;ILjava/lang/Long;)V

    .line 436
    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/ImageView;

    invoke-interface {v1}, LaGu;->a()LaGv;

    move-result-object v2

    .line 437
    invoke-interface {v1}, LaGu;->f()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1}, LaGu;->d()Z

    move-result v4

    .line 436
    invoke-static {v2, v3, v4}, LaGt;->a(LaGv;Ljava/lang/String;Z)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 438
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/TextView;

    invoke-interface {v1}, LaGu;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 440
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->b(LaGu;)V

    .line 441
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->c(LaGu;)V

    .line 443
    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a(LaGu;Z)V

    .line 444
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lacj;

    invoke-interface {v1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-virtual {v0, v2}, Lacj;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 446
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a(LaGu;)V

    .line 448
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/ThumbnailView;->requestFocus()Z

    goto/16 :goto_0

    :cond_4
    move v0, v3

    .line 399
    goto/16 :goto_1

    .line 402
    :cond_5
    sget v0, Lxi;->thumbnail_preview:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 416
    :cond_6
    new-instance v0, Ljava/util/Date;

    .line 417
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct {v0, v6, v7}, Ljava/util/Date;-><init>(J)V

    goto/16 :goto_3

    .line 433
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->f:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4
.end method

.method private c(LaGu;)V
    .locals 2

    .prologue
    .line 491
    invoke-interface {p1}, LaGu;->c()Z

    move-result v0

    .line 492
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/ToggleButton;

    invoke-virtual {v1, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 493
    return-void
.end method

.method private d(LaGu;)V
    .locals 3

    .prologue
    .line 539
    if-nez p1, :cond_0

    .line 545
    :goto_0
    return-void

    .line 542
    :cond_0
    invoke-interface {p1}, LaGu;->a()LaGv;

    move-result-object v0

    invoke-interface {p1}, LaGu;->f()Ljava/lang/String;

    move-result-object v1

    .line 543
    invoke-interface {p1}, LaGu;->d()Z

    move-result v2

    .line 542
    invoke-static {v0, v1, v2}, LaGt;->b(LaGv;Ljava/lang/String;Z)I

    move-result v0

    .line 544
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/view/ThumbnailView;->setIcon(I)V

    goto :goto_0
.end method

.method private e(LaGu;)V
    .locals 4

    .prologue
    .line 548
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a()V

    .line 549
    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v1

    .line 550
    invoke-static {v1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 551
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/ThumbnailView;->a()V

    .line 552
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LamY;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LamY;

    .line 553
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LamY;

    .line 554
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a()Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    move-result-object v2

    new-instance v3, LRt;

    invoke-direct {v3, p0, p1}, LRt;-><init>(Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;LaGu;)V

    .line 553
    invoke-interface {v0, v1, v2, v3}, LamY;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;LamZ;)V

    .line 586
    return-void
.end method

.method private o()Z
    .locals 1

    .prologue
    .line 868
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a()LaGu;

    move-result-object v0

    .line 870
    if-eqz v0, :cond_0

    invoke-interface {v0}, LaGu;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private p()Z
    .locals 1

    .prologue
    .line 935
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private t()V
    .locals 14

    .prologue
    const/16 v8, 0x8

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 618
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 720
    :cond_0
    :goto_0
    return-void

    .line 621
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LvU;

    invoke-interface {v0}, LvU;->a()LaGu;

    move-result-object v4

    .line 622
    if-eqz v4, :cond_0

    .line 626
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lacj;

    invoke-virtual {v0}, Lacj;->a()Lacr;

    move-result-object v11

    .line 627
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lacr;

    .line 628
    invoke-static {v11, v0}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v6

    .line 629
    :goto_1
    iput-object v11, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lacr;

    .line 631
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->o()Z

    move-result v1

    if-nez v1, :cond_5

    move v2, v6

    .line 632
    :goto_2
    if-nez v11, :cond_6

    const/4 v1, 0x0

    .line 634
    :goto_3
    if-nez v1, :cond_13

    .line 635
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    move-object v2, v1

    move v10, v6

    .line 640
    :goto_4
    if-eqz v0, :cond_2

    .line 641
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 644
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LaKR;

    invoke-interface {v1}, LaKR;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lacj;

    .line 645
    invoke-virtual {v1}, Lacj;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    if-eqz v11, :cond_7

    :cond_3
    move v9, v6

    .line 646
    :goto_5
    if-eqz v9, :cond_a

    if-eqz v0, :cond_a

    .line 648
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a()LH;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, LH;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 649
    new-instance v3, LabI;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a()LH;

    move-result-object v0

    invoke-direct {v3, v0}, LabI;-><init>(Landroid/content/Context;)V

    .line 651
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 654
    if-eqz v11, :cond_12

    .line 655
    invoke-interface {v11}, Lacr;->a()Lqu;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 656
    invoke-interface {v11}, Lacr;->a()Lqu;

    move-result-object v0

    sget-object v5, Lqu;->a:Lqu;

    if-eq v0, v5, :cond_12

    .line 657
    invoke-interface {v11}, Lacr;->a()Lqu;

    move-result-object v0

    sget-object v5, Lqu;->n:Lqu;

    if-eq v0, v5, :cond_12

    .line 658
    invoke-direct {p0, v11}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a(Lacr;)Z

    move-result v5

    .line 659
    invoke-static {v11}, LacH;->a(Lacr;)LacD;

    move-result-object v0

    .line 660
    iget-object v13, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/LinearLayout;

    .line 661
    invoke-direct {p0, v0, v1, v13, v3}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a(LacD;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;LabI;)Landroid/view/View;

    move-result-object v0

    .line 662
    iget-object v13, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v13, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 663
    invoke-static {v12, v0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a(Ljava/util/List;Landroid/view/View;)V

    .line 666
    :goto_6
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacD;

    .line 667
    iget-object v13, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/LinearLayout;

    .line 668
    invoke-direct {p0, v0, v1, v13, v3}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a(LacD;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;LabI;)Landroid/view/View;

    move-result-object v0

    .line 669
    iget-object v13, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v13, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 670
    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :cond_4
    move v0, v7

    .line 628
    goto/16 :goto_1

    :cond_5
    move v2, v7

    .line 631
    goto/16 :goto_2

    .line 633
    :cond_6
    invoke-interface {v11}, Lacr;->a()Ljava/util/List;

    move-result-object v1

    goto/16 :goto_3

    :cond_7
    move v9, v7

    .line 645
    goto :goto_5

    .line 673
    :cond_8
    if-eqz v11, :cond_9

    .line 674
    invoke-interface {v11}, Lacr;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 675
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/LinearLayout;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 676
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 677
    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 679
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LabW;

    if-eqz v0, :cond_9

    .line 680
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LabW;

    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/LinearLayout;

    .line 681
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a()LH;

    move-result-object v3

    .line 680
    invoke-interface/range {v0 .. v5}, LabW;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/app/Activity;LaGu;Z)Ljava/util/List;

    move-result-object v0

    .line 682
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 683
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 684
    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 689
    :cond_9
    invoke-direct {p0, v12}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a(Ljava/util/List;)V

    .line 692
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lacj;

    invoke-virtual {v0}, Lacj;->a()Z

    move-result v0

    if-eqz v0, :cond_e

    if-nez v11, :cond_e

    .line 693
    :goto_9
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/ProgressBar;

    if-eqz v6, :cond_f

    move v0, v7

    :goto_a
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 695
    const/4 v0, -0x1

    .line 696
    if-eqz v9, :cond_10

    if-eqz v10, :cond_10

    .line 697
    sget v0, Lxi;->sharing_list_may_not_be_completed:I

    .line 702
    :cond_b
    :goto_b
    if-ltz v0, :cond_c

    .line 703
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 705
    :cond_c
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->b:Landroid/widget/TextView;

    if-ltz v0, :cond_11

    :goto_c
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 709
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->requestLayout()V

    .line 711
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LRA;

    if-eqz v0, :cond_d

    .line 712
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LRA;

    invoke-interface {v0}, LRA;->e_()V

    .line 715
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LacF;

    .line 716
    invoke-virtual {v0}, LacF;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 718
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lacj;

    invoke-interface {v4}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Lacj;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Z

    goto/16 :goto_0

    :cond_e
    move v6, v7

    .line 692
    goto :goto_9

    :cond_f
    move v0, v8

    .line 693
    goto :goto_a

    .line 698
    :cond_10
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LaKR;

    invoke-interface {v1}, LaKR;->a()Z

    move-result v1

    if-nez v1, :cond_b

    .line 699
    sget v0, Lxi;->sharing_list_offline:I

    goto :goto_b

    :cond_11
    move v7, v8

    .line 705
    goto :goto_c

    :cond_12
    move v5, v7

    goto/16 :goto_6

    :cond_13
    move v10, v2

    move-object v2, v1

    goto/16 :goto_4
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 247
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lacr;

    .line 248
    sget v0, Lxe;->detail_fragment:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 250
    sget v0, Lxc;->detail_fragment_scrollview:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/ScrollView;

    .line 251
    sget v0, Lxc;->doc_icon:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/ImageView;

    .line 252
    sget v0, Lxc;->thumbnail:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/ThumbnailView;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    .line 253
    sget v0, Lxc;->share_list_progress_bar:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/ProgressBar;

    .line 254
    sget v0, Lxc;->share_list_warning:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->b:Landroid/widget/TextView;

    .line 255
    sget v0, Lxc;->last_modified:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->c:Landroid/widget/TextView;

    .line 256
    sget v0, Lxc;->last_modified_by_me:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->d:Landroid/widget/TextView;

    .line 257
    sget v0, Lxc;->last_opened_by_me:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->e:Landroid/widget/TextView;

    .line 258
    sget v0, Lxc;->quota_used:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->f:Landroid/widget/TextView;

    .line 259
    sget v0, Lxc;->detail_fragment_close_button:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->d:Landroid/view/View;

    .line 260
    sget v0, Lxc;->pin:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->b:Landroid/widget/LinearLayout;

    .line 261
    sget v0, Lxc;->pin_compoundButton:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/CompoundButton;

    .line 262
    sget v0, Lxc;->share_panel:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->d:Landroid/widget/LinearLayout;

    .line 263
    sget v0, Lxc;->general_info_panel:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->c:Landroid/widget/LinearLayout;

    .line 264
    sget v0, Lxc;->trashed_item_description:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->g:Landroid/widget/TextView;

    .line 265
    sget v0, Lxc;->title:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/TextView;

    .line 266
    sget v0, Lxc;->title_container:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->e:Landroid/view/View;

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->e:Landroid/view/View;

    new-instance v2, LRp;

    invoke-direct {v2, p0}, LRp;-><init>(Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 282
    sget v0, Lxc;->star_cb:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/ToggleButton;

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/ToggleButton;

    new-instance v2, LRq;

    invoke-direct {v2, p0}, LRq;-><init>(Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 295
    sget v0, Lxc;->share_list:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/LinearLayout;

    .line 297
    sget v0, Lxc;->offline_title:I

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a(Landroid/view/View;I)V

    .line 298
    sget v0, Lxc;->sharing_title:I

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a(Landroid/view/View;I)V

    .line 299
    sget v0, Lxc;->general_info_title:I

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a(Landroid/view/View;I)V

    .line 301
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 302
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a()I

    move-result v2

    const/4 v3, -0x1

    invoke-direct {v0, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 303
    const/4 v2, 0x5

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 304
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LabW;

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LabW;

    invoke-interface {v0}, LabW;->a()V

    .line 310
    :cond_0
    return-object v1
.end method

.method public a(IZI)Landroid/view/animation/Animation;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 905
    iget v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->b:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 906
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    move v1, v0

    .line 908
    :goto_0
    const/16 v0, 0x1001

    if-ne p1, v0, :cond_1

    .line 909
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v0, v1, v2, v2, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 916
    :goto_1
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 917
    :goto_2
    return-object v0

    .line 907
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxa;->detail_fragment_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    move v1, v0

    goto :goto_0

    .line 910
    :cond_1
    const/16 v0, 0x2002

    if-ne p1, v0, :cond_2

    .line 911
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v0, v2, v1, v2, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    goto :goto_1

    .line 913
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 824
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/docs/fragment/DetailFragment;->a(IILandroid/content/Intent;)V

    .line 826
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LabW;

    if-eqz v0, :cond_0

    .line 827
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LabW;

    invoke-interface {v0, p1, p2, p3}, LabW;->a(IILandroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v0

    .line 828
    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->p()Z

    move-result v1

    if-nez v1, :cond_0

    .line 829
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a()LH;

    move-result-object v1

    invoke-virtual {v1}, LH;->a()LM;

    move-result-object v1

    .line 830
    invoke-static {}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->b()Ljava/lang/String;

    move-result-object v2

    .line 831
    new-instance v3, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;

    invoke-direct {v3}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/support/v4/app/DialogFragment;

    .line 832
    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v3, v0}, Landroid/support/v4/app/DialogFragment;->e(Landroid/os/Bundle;)V

    .line 833
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LM;Ljava/lang/String;)V

    .line 836
    :cond_0
    return-void
.end method

.method public a(LacD;)V
    .locals 3

    .prologue
    .line 844
    invoke-virtual {p1}, LacD;->a()LabD;

    move-result-object v0

    .line 845
    if-nez v0, :cond_1

    .line 852
    :cond_0
    :goto_0
    return-void

    .line 848
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LvU;

    invoke-interface {v1}, LvU;->a()LaGu;

    move-result-object v1

    .line 849
    if-eqz v1, :cond_0

    .line 850
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lacj;

    invoke-interface {v1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, Lacj;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LabD;)V

    goto :goto_0
.end method

.method public a(Lacr;)V
    .locals 0

    .prologue
    .line 982
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->t()V

    .line 983
    return-void
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;Z)V
    .locals 3

    .prologue
    .line 940
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a()LH;

    move-result-object v0

    .line 941
    new-instance v1, LRy;

    invoke-direct {v1, p0, v0}, LRy;-><init>(Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;Landroid/content/Context;)V

    .line 956
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LUi;

    invoke-interface {v0, p1, p2, v1}, LUi;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;ZLaHy;)V

    .line 957
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a()LH;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/ToggleButton;

    if-eqz p2, :cond_0

    sget v0, Lxi;->doclist_starred_state:I

    :goto_0
    invoke-static {v1, v2, v0}, LUs;->a(Landroid/content/Context;Landroid/view/View;I)V

    .line 959
    return-void

    .line 957
    :cond_0
    sget v0, Lxi;->doclist_unstarred_state:I

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 968
    iput-boolean p1, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Z

    .line 969
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    if-eqz v0, :cond_0

    .line 970
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/view/ThumbnailView;->setOpenButtonVisibility(Z)V

    .line 972
    :cond_0
    return-void
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 922
    const-string v0, "DetailFragment"

    const-string v1, "in onCreate"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 923
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/fragment/DetailFragment;->a_(Landroid/os/Bundle;)V

    .line 924
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/os/Handler;

    .line 925
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 329
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/fragment/DetailFragment;->b(Landroid/os/Bundle;)V

    .line 331
    if-eqz p1, :cond_0

    .line 332
    const-string v0, "thumbnailOpenButtonVisible"

    iget-boolean v1, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Z

    .line 333
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Z

    .line 337
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 339
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->d:Landroid/view/View;

    new-instance v1, LRr;

    invoke-direct {v1, p0}, LRr;-><init>(Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 346
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:LvU;

    invoke-interface {v0, p0}, LvU;->a(LvV;)V

    .line 348
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->b(Z)V

    .line 350
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/CompoundButton;

    new-instance v1, LRs;

    invoke-direct {v1, p0}, LRs;-><init>(Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 371
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/CompoundButton;

    instance-of v0, v0, Landroid/widget/CheckBox;

    if-eqz v0, :cond_1

    .line 372
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/widget/CompoundButton;

    sget v1, Lxb;->state_selector_background:I

    invoke-static {v0, v1}, Lano;->a(Landroid/view/View;I)V

    .line 375
    :cond_1
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 315
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/fragment/DetailFragment;->c(Landroid/os/Bundle;)V

    .line 316
    const-string v0, "thumbnailOpenButtonVisible"

    iget-boolean v1, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 318
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 929
    const-string v0, "DetailFragment"

    const-string v1, "in onDestroyView"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 930
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a()V

    .line 931
    invoke-super {p0}, Lcom/google/android/apps/docs/fragment/DetailFragment;->e()V

    .line 932
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 892
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Landroid/support/v4/app/DialogFragment;

    .line 893
    invoke-super {p0}, Lcom/google/android/apps/docs/fragment/DetailFragment;->g()V

    .line 894
    return-void
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 987
    const/4 v0, 0x1

    return v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 963
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lacj;

    invoke-virtual {v0}, Lacj;->a()Lacr;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()V
    .locals 1

    .prologue
    .line 379
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->b(Z)V

    .line 380
    return-void
.end method

.method public o_()V
    .locals 1

    .prologue
    .line 885
    invoke-super {p0}, Lcom/google/android/apps/docs/fragment/DetailFragment;->o_()V

    .line 887
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->a:Lacj;

    invoke-virtual {v0, p0}, Lacj;->a(Laco;)V

    .line 888
    return-void
.end method

.method public r()V
    .locals 1

    .prologue
    .line 384
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;->b(Z)V

    .line 385
    return-void
.end method
