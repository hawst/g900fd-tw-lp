.class public Lcom/google/android/apps/docs/drive/media/AudioController;
.super Landroid/widget/MediaController;
.source "AudioController.java"


# instance fields
.field a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 16
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {p0, v0}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;)V

    .line 13
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/drive/media/AudioController;->a:Z

    .line 17
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/google/android/apps/docs/drive/media/AudioController;->a:Z

    if-eqz v0, :cond_0

    .line 21
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/widget/MediaController;->show(I)V

    .line 25
    :goto_0
    return-void

    .line 23
    :cond_0
    invoke-super {p0}, Landroid/widget/MediaController;->hide()V

    goto :goto_0
.end method


# virtual methods
.method public hide()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/apps/docs/drive/media/AudioController;->a()V

    .line 35
    return-void
.end method

.method public setShowing(Z)V
    .locals 0

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/google/android/apps/docs/drive/media/AudioController;->a:Z

    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/docs/drive/media/AudioController;->a()V

    .line 40
    return-void
.end method

.method public show()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/apps/docs/drive/media/AudioController;->a()V

    .line 30
    return-void
.end method
