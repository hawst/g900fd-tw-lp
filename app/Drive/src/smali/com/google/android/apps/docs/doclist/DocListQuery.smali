.class public Lcom/google/android/apps/docs/doclist/DocListQuery;
.super Ljava/lang/Object;
.source "DocListQuery.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/docs/doclist/DocListQuery;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LIK;

.field private final a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    new-instance v0, LBA;

    invoke-direct {v0}, LBA;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/doclist/DocListQuery;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/DocListQuery;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    .line 90
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 91
    if-eqz v0, :cond_0

    .line 92
    invoke-static {v0}, LIK;->a(Ljava/lang/String;)LIK;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/DocListQuery;->a:LIK;

    .line 96
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 97
    if-gez v0, :cond_1

    .line 98
    iput-object v1, p0, Lcom/google/android/apps/docs/doclist/DocListQuery;->a:LbmY;

    .line 104
    :goto_1
    return-void

    .line 94
    :cond_0
    iput-object v1, p0, Lcom/google/android/apps/docs/doclist/DocListQuery;->a:LIK;

    goto :goto_0

    .line 100
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 101
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 102
    invoke-static {v1}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/DocListQuery;->a:LbmY;

    goto :goto_1
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;LBA;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/doclist/DocListQuery;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;LIK;LbmY;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;",
            "LIK;",
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/DocListQuery;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    .line 37
    iput-object p2, p0, Lcom/google/android/apps/docs/doclist/DocListQuery;->a:LIK;

    .line 38
    iput-object p3, p0, Lcom/google/android/apps/docs/doclist/DocListQuery;->a:LbmY;

    .line 39
    return-void
.end method


# virtual methods
.method public a()LIK;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/DocListQuery;->a:LIK;

    return-object v0
.end method

.method public a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/DocListQuery;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    return-object v0
.end method

.method public a()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/DocListQuery;->a:LbmY;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/DocListQuery;->a:LbmY;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, LbmY;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 108
    const-string v0, "DocListQuery[criterionSet=%s, sortingClause=%s, columns=%s]"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/DocListQuery;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/DocListQuery;->a:LIK;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/DocListQuery;->a:LbmY;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/DocListQuery;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/DocListQuery;->a:LIK;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/DocListQuery;->a:LIK;

    invoke-virtual {v0}, LIK;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/DocListQuery;->a:LbmY;

    if-nez v0, :cond_2

    const/4 v0, -0x1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/DocListQuery;->a:LbmY;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/DocListQuery;->a:LbmY;

    invoke-static {v0}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 73
    :cond_0
    return-void

    .line 68
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 69
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/DocListQuery;->a:LbmY;

    invoke-virtual {v0}, LbmY;->size()I

    move-result v0

    goto :goto_1
.end method
