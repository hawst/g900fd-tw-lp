.class public Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingRoleDialogFragment;
.super Lcom/google/android/apps/docs/tools/gelly/android/GuiceDialogFragment;
.source "LinkSharingCard.java"


# instance fields
.field public a:Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;

.field private a:Lqt;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 324
    invoke-direct {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceDialogFragment;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingRoleDialogFragment;)Lqt;
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingRoleDialogFragment;->a:Lqt;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingRoleDialogFragment;Lqt;)Lqt;
    .locals 0

    .prologue
    .line 324
    iput-object p1, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingRoleDialogFragment;->a:Lqt;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingRoleDialogFragment;Lqt;Ljava/lang/CharSequence;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 324
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingRoleDialogFragment;->a(Lqt;Ljava/lang/CharSequence;[Ljava/lang/String;)V

    return-void
.end method

.method private a(Lqt;Ljava/lang/CharSequence;[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 336
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 337
    const-string v1, "role"

    invoke-virtual {p1}, Lqt;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 338
    const-string v1, "title"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 339
    const-string v1, "roleLabels"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 340
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingRoleDialogFragment;->e(Landroid/os/Bundle;)V

    .line 341
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 346
    invoke-virtual {p0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingRoleDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "roleLabels"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 348
    invoke-static {}, Lqt;->values()[Lqt;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingRoleDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "role"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    aget-object v1, v1, v2

    .line 349
    iput-object v1, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingRoleDialogFragment;->a:Lqt;

    .line 351
    invoke-static {}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->b()LbmF;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingRoleDialogFragment;->a:Lqt;

    invoke-virtual {v2, v3}, LbmF;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 353
    invoke-virtual {p0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingRoleDialogFragment;->a()LH;

    move-result-object v3

    invoke-static {v3}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v3

    .line 354
    invoke-virtual {p0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingRoleDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "title"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, LEU;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lzm;

    invoke-direct {v4, p0}, Lzm;-><init>(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingRoleDialogFragment;)V

    .line 355
    invoke-virtual {v3, v0, v2, v4}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x104000a

    new-instance v3, Lzl;

    invoke-direct {v3, p0, v1}, Lzl;-><init>(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingRoleDialogFragment;Lqt;)V

    .line 361
    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    new-instance v2, Lzk;

    invoke-direct {v2, p0}, Lzk;-><init>(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingRoleDialogFragment;)V

    .line 368
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 375
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
