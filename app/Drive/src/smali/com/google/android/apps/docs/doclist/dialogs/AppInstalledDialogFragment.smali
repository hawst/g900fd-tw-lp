.class public Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "AppInstalledDialogFragment.java"


# instance fields
.field public a:LaGM;

.field private a:LaGu;

.field public a:LsP;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;)LaGu;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;->a:LaGu;

    return-object v0
.end method

.method public static a(LM;LuM;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 3

    .prologue
    .line 85
    const-string v0, "nativeAppInfo"

    invoke-static {p1, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    const-string v0, "entrySpec"

    invoke-static {p2, v0}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 89
    const-string v1, "installedMessageId"

    invoke-virtual {p1}, LuM;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 90
    const-string v1, "entrySpec.v2"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 91
    new-instance v1, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;-><init>()V

    .line 92
    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;->e(Landroid/os/Bundle;)V

    .line 93
    const-string v0, "AppInstalledDialogFragment"

    invoke-virtual {v1, p0, v0}, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;->a(LM;Ljava/lang/String;)V

    .line 94
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 56
    .line 57
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;->a()LH;

    move-result-object v0

    invoke-static {v0}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v0

    .line 58
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 60
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;->a:LaGu;

    invoke-interface {v1}, LaGu;->c()Ljava/lang/String;

    move-result-object v1

    .line 61
    sget v2, Lxi;->app_installed_dialog_message:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 62
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 64
    sget v1, Lxi;->app_installed_dialog_open_button:I

    new-instance v2, LEx;

    invoke-direct {v2, p0}, LEx;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 73
    const/high16 v1, 0x1040000

    new-instance v2, LEy;

    invoke-direct {v2, p0}, LEy;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 80
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a_(Landroid/os/Bundle;)V

    .line 43
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;->a()LH;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "installedMessageId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, LH;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;->c:Ljava/lang/String;

    .line 44
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "entrySpec.v2"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 46
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;->a:LaGM;

    invoke-interface {v1, v0}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGu;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;->a:LaGu;

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;->a:LaGu;

    if-nez v0, :cond_0

    .line 48
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;->a()LH;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;->c:Ljava/lang/String;

    invoke-static {v0, v1}, LZY;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 49
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;->c(Z)V

    .line 50
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/AppInstalledDialogFragment;->a()V

    .line 52
    :cond_0
    return-void
.end method
