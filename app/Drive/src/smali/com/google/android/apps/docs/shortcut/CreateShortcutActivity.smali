.class public Lcom/google/android/apps/docs/shortcut/CreateShortcutActivity;
.super LqS;
.source "CreateShortcutActivity.java"


# instance fields
.field public a:Lvq;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, LqS;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->a:Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    return-object v0
.end method

.method protected a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 4

    .prologue
    .line 62
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/docs/shortcut/CreateShortcutActivity;->a:Lvq;

    invoke-interface {v0, p0, p1}, Lvq;->a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    .line 65
    if-eqz v0, :cond_0

    .line 66
    iget-object v1, p0, Lcom/google/android/apps/docs/shortcut/CreateShortcutActivity;->a:LqK;

    const-string v2, "widget"

    const-string v3, "createShortcutFinished"

    invoke-virtual {v1, v2, v3}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/docs/shortcut/CreateShortcutActivity;->setResult(ILandroid/content/Intent;)V

    .line 70
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shortcut/CreateShortcutActivity;->finish()V

    .line 71
    return-void
.end method

.method protected a(LuW;)V
    .locals 1

    .prologue
    .line 32
    sget v0, LatO;->create_shortcut_title:I

    .line 33
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/shortcut/CreateShortcutActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LuW;->a(Ljava/lang/String;)LuW;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, LuW;->b()LuW;

    .line 35
    return-void
.end method

.method protected e()V
    .locals 3

    .prologue
    .line 75
    invoke-super {p0}, LqS;->e()V

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/docs/shortcut/CreateShortcutActivity;->a:LqK;

    const-string v1, "widget"

    const-string v2, "createShortcutCanceled"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 39
    invoke-super {p0, p1}, LqS;->onCreate(Landroid/os/Bundle;)V

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/docs/shortcut/CreateShortcutActivity;->a:LqK;

    invoke-virtual {v0}, LqK;->a()V

    .line 42
    if-nez p1, :cond_0

    .line 43
    iget-object v0, p0, Lcom/google/android/apps/docs/shortcut/CreateShortcutActivity;->a:LqK;

    const-string v1, "widget"

    const-string v2, "createShortcutStarted"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, LqS;->onDestroy()V

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/docs/shortcut/CreateShortcutActivity;->a:LqK;

    invoke-virtual {v0}, LqK;->b()V

    .line 53
    return-void
.end method
