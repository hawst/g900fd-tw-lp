.class public Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;
.super Lcom/google/android/apps/viewer/util/BaseParcelable;
.source "SelectionBoundary.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;",
            ">;"
        }
    .end annotation
.end field

.field public static final PAGE_END:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

.field public static final PAGE_START:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;


# instance fields
.field public final index:I

.field public final isRtl:Z

.field public final x:I

.field public final y:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->atIndex(I)Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->PAGE_START:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    .line 21
    const v0, 0x7fffffff

    invoke-static {v0}, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->atIndex(I)Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->PAGE_END:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    .line 56
    new-instance v0, LavL;

    invoke-direct {v0}, LavL;-><init>()V

    sput-object v0, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIIZ)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/apps/viewer/util/BaseParcelable;-><init>()V

    .line 24
    iput p1, p0, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->index:I

    .line 25
    iput p2, p0, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->x:I

    .line 26
    iput p3, p0, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->y:I

    .line 27
    iput-boolean p4, p0, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->isRtl:Z

    .line 28
    return-void
.end method

.method public static atIndex(I)Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 32
    new-instance v0, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v2, v2, v1}, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;-><init>(IIIZ)V

    return-object v0
.end method

.method public static atPoint(II)Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;
    .locals 3

    .prologue
    .line 37
    new-instance v0, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, p1, v2}, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;-><init>(IIIZ)V

    return-object v0
.end method

.method public static atPoint(Landroid/graphics/Point;)Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;
    .locals 5

    .prologue
    .line 42
    new-instance v0, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    const/4 v1, -0x1

    iget v2, p0, Landroid/graphics/Point;->x:I

    iget v3, p0, Landroid/graphics/Point;->y:I

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;-><init>(IIIZ)V

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->index:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    const-string v0, "MAX"

    .line 48
    :goto_0
    const-string v1, "@%s (%d,%d)"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    iget v3, p0, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->x:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    iget v3, p0, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->y:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 47
    :cond_0
    iget v0, p0, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->index:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 53
    const/4 v2, 0x4

    new-array v2, v2, [I

    iget v3, p0, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->index:I

    aput v3, v2, v1

    iget v3, p0, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->x:I

    aput v3, v2, v0

    const/4 v3, 0x2

    iget v4, p0, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->y:I

    aput v4, v2, v3

    const/4 v3, 0x3

    iget-boolean v4, p0, Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;->isRtl:Z

    if-eqz v4, :cond_0

    :goto_0
    aput v0, v2, v3

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 54
    return-void

    :cond_0
    move v0, v1

    .line 53
    goto :goto_0
.end method
