.class public Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;
.super Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;
.source "PunchSvgWebView.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field private static a:I

.field private static final a:Ljava/util/regex/Pattern;


# instance fields
.field private a:F

.field private a:LUe;

.field private final a:LXD;

.field private a:LXP;

.field private final a:LXQ;

.field private a:LYq;

.field private a:LYr;

.field private final a:Landroid/os/Handler;

.field private final a:Landroid/view/ViewGroup;

.field private final a:Ljava/lang/String;

.field private a:Z

.field private b:I

.field private final b:Landroid/os/Handler;

.field private b:Ljava/lang/String;

.field private b:Z

.field private c:I

.field private c:Ljava/lang/String;

.field private d:I

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    sput v0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:I

    .line 59
    const-string v0, "[\\s]*?<\\?xml(.|[\\n])*?\\?>"

    .line 60
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:Ljava/util/regex/Pattern;

    .line 59
    return-void
.end method

.method private constructor <init>(Landroid/view/ViewGroup;LXD;LXQ;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 173
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, LYh;

    invoke-direct {v1, p2}, LYh;-><init>(LXD;)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;-><init>(Landroid/content/Context;LZF;)V

    .line 70
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:Landroid/os/Handler;

    .line 75
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->c:Ljava/lang/String;

    .line 76
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->d:Ljava/lang/String;

    .line 78
    iput v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:I

    .line 81
    iput-boolean v3, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:Z

    .line 90
    iput-boolean v3, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Z

    .line 92
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Landroid/os/Handler;

    .line 129
    sget-object v0, LYq;->a:LYq;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LYq;

    .line 132
    iput v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->c:I

    .line 133
    iput v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->d:I

    .line 134
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:F

    .line 185
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:Landroid/view/ViewGroup;

    .line 186
    iput-object p2, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LXD;

    .line 187
    iput-object p3, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LXQ;

    .line 188
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SvgWebView"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:I

    add-int/lit8 v2, v1, 0x1

    sput v2, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:Ljava/lang/String;

    .line 189
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->g()V

    .line 190
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;F)F
    .locals 0

    .prologue
    .line 48
    iput p1, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:F

    return p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:I

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)LXQ;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LXQ;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;LYq;)LYq;
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LYq;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Landroid/os/Handler;

    return-object v0
.end method

.method public static a(Landroid/view/ViewGroup;LXD;LTO;LYO;LXR;LqK;Lbxw;LXQ;)Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "LXD;",
            "LTO;",
            "LYO;",
            "LXR;",
            "LqK;",
            "Lbxw",
            "<",
            "LXR;",
            ">;",
            "LXQ;",
            ")",
            "Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;"
        }
    .end annotation

    .prologue
    const/4 v6, -0x1

    .line 465
    const-string v0, "SvgWebView"

    const-string v1, "Creating PunchSvgWebView"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    new-instance v0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;

    .line 467
    invoke-virtual {p7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, p1, p7, v1}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;-><init>(Landroid/view/ViewGroup;LXD;LXQ;Ljava/lang/String;)V

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    .line 468
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(LTO;LYO;LXR;LqK;Lbxw;)V

    .line 470
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 472
    return-object v0
.end method

.method private a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 509
    invoke-static {}, LakQ;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "PunchSvgKlp.html"

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->e:Ljava/lang/String;

    .line 512
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->e:Ljava/lang/String;

    return-object v0

    .line 509
    :cond_1
    const-string v0, "PunchSvg.html"

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Ljava/lang/String;

    return-object v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 499
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "javascript/punch/webview/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 500
    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 499
    invoke-static {v0}, LamN;->a(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 501
    :catch_0
    move-exception v0

    .line 502
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 225
    iget v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:I

    if-eq v0, p1, :cond_0

    .line 226
    iput p1, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:I

    .line 227
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->g()V

    .line 229
    :cond_0
    return-void
.end method

.method private a(LTO;LYO;LXR;LqK;Lbxw;)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LTO;",
            "LYO;",
            "LXR;",
            "LqK;",
            "Lbxw",
            "<",
            "LXR;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 352
    invoke-static {}, LakQ;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353
    sget-object v0, LYq;->c:LYq;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LYq;

    .line 357
    :goto_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->setOverScrollMode(I)V

    .line 358
    new-instance v0, LUf;

    invoke-direct {v0, p0}, LUf;-><init>(Landroid/webkit/WebView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LUe;

    .line 359
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:Landroid/view/ViewGroup;

    .line 360
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-interface {p1}, LTO;->c()Ljava/lang/String;

    move-result-object v1

    .line 359
    invoke-static {p0, v0, v1, v2}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(Landroid/webkit/WebView;Landroid/content/res/Resources;Ljava/lang/String;Z)V

    .line 361
    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 362
    new-instance v0, Landroid/webkit/WebChromeClient;

    invoke-direct {v0}, Landroid/webkit/WebChromeClient;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 363
    new-instance v0, LYp;

    invoke-direct {v0, p0, v3}, LYp;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;LYh;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 365
    invoke-direct {p0, p5}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Lbxw;)V

    .line 366
    return-void

    .line 355
    :cond_0
    sget-object v0, LYq;->a:LYq;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LYq;

    goto :goto_0
.end method

.method private a(LXP;)V
    .locals 0

    .prologue
    .line 232
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LXP;

    .line 233
    return-void
.end method

.method private a(Lbxw;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbxw",
            "<",
            "LXR;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 425
    new-instance v0, LYl;

    invoke-direct {v0, p0}, LYl;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)V

    const-string v1, "SvgLoader"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 456
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->o()V

    return-void
.end method

.method private a(ILXP;)Z
    .locals 1

    .prologue
    .line 340
    iget v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:I

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LXP;

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Z

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;ILXP;)Z
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(ILXP;)Z

    move-result v0

    return v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->c:Ljava/lang/String;

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 622
    :try_start_0
    sget-object v0, LbiB;->c:Ljava/nio/charset/Charset;

    invoke-virtual {v0}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "+"

    const-string v2, "%20"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 623
    :catch_0
    move-exception v0

    .line 624
    invoke-static {v0}, Lbjz;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->j()V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 522
    sget-object v0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 523
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->d:Ljava/lang/String;

    .line 526
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LXQ;

    invoke-interface {v0}, LXQ;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 527
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->i()V

    .line 530
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LYr;

    if-eqz v0, :cond_1

    .line 531
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LYr;

    invoke-interface {v0, p0}, LYr;->a(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)V

    .line 533
    :cond_1
    return-void
.end method

.method private b()Z
    .locals 4

    .prologue
    .line 601
    iget v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:F

    float-to-double v0, v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->k()V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 605
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b(Ljava/lang/String;)V

    .line 607
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LYq;

    invoke-virtual {v0}, LYq;->ordinal()I

    move-result v0

    sget-object v1, LYq;->c:LYq;

    invoke-virtual {v1}, LYq;->ordinal()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 616
    :goto_0
    return-void

    .line 611
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 612
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->m()V

    goto :goto_0

    .line 614
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->o()V

    goto :goto_0
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 675
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LYq;

    sget-object v1, LYq;->d:LYq;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 240
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Ljava/lang/String;

    .line 241
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 486
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:Z

    if-nez v0, :cond_1

    .line 487
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:Z

    .line 488
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 489
    const-string v0, "PunchSvg.js"

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->f:Ljava/lang/String;

    .line 492
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Ljava/lang/String;

    const-string v1, "InjectedInWebView"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "javascript:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->loadUrl(Ljava/lang/String;)V

    .line 495
    :cond_1
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 545
    const-string v0, "[\\s]xlink="

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 546
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 547
    const-string v1, " xmlns:xlink="

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->d:Ljava/lang/String;

    .line 549
    const-string v0, "[\\s]href="

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 550
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 551
    const-string v1, " xlink:href="

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->d:Ljava/lang/String;

    .line 552
    return-void
.end method

.method private j()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 559
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->getHeight()I

    move-result v0

    .line 560
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Ljava/lang/String;

    const-string v2, "in buildSvgToLoad webViewHeight=%s"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 562
    invoke-static {}, LakQ;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 563
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->c:Ljava/lang/String;

    .line 567
    :goto_0
    return-void

    .line 565
    :cond_0
    const-string v1, "<div id=\'holder\' class=\'svgTable\' style=\'height: %spx\'><div class=\'svgTableCell\'>%s</div></div>"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    int-to-long v4, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->d:Ljava/lang/String;

    aput-object v0, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method private k()V
    .locals 6

    .prologue
    .line 570
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:Z

    if-eqz v0, :cond_1

    .line 571
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->getHeight()I

    move-result v0

    .line 572
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Ljava/lang/String;

    const-string v2, "in resizeHeight H=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 576
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 577
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->n()V

    .line 580
    :cond_0
    invoke-static {}, LakQ;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 581
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LUe;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "window.SvgLoader.context.resizeHeight("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ");"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, LUe;->a(Ljava/lang/String;)V

    .line 585
    :cond_1
    return-void
.end method

.method private l()V
    .locals 3

    .prologue
    .line 629
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Ljava/lang/String;

    const-string v1, "in LoadTemplate"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 631
    invoke-static {}, LakQ;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 632
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:Z

    .line 633
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "image/svg+xml"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    sget-object v0, LYq;->b:LYq;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LYq;

    .line 636
    :cond_0
    return-void
.end method

.method private m()V
    .locals 5

    .prologue
    .line 639
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Ljava/lang/String;

    const-string v1, "in forceResetZoom, wasCleaned=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 640
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Z

    if-eqz v0, :cond_0

    .line 646
    :goto_0
    return-void

    .line 644
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->n()V

    .line 645
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->o()V

    goto :goto_0
.end method

.method private n()V
    .locals 1

    .prologue
    .line 649
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->zoomOut()Z

    .line 650
    invoke-static {}, LakQ;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 652
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->canZoomOut()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 653
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->zoomOut()Z

    goto :goto_0

    .line 657
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->d()V

    .line 659
    :cond_1
    return-void
.end method

.method private o()V
    .locals 3

    .prologue
    .line 691
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Ljava/lang/String;

    const-string v1, "in loadSvg"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 692
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 693
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Ljava/lang/String;

    const-string v1, "Early exit in loadSvg"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 711
    :goto_0
    return-void

    .line 699
    :cond_0
    invoke-static {}, LakQ;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 700
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a()Ljava/lang/String;

    move-result-object v0

    .line 701
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->j()V

    .line 702
    const-string v1, "PAGECONTENT"

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 703
    invoke-static {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "image/svg+xml"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    sget-object v0, LYq;->d:LYq;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LYq;

    goto :goto_0

    .line 706
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->h()V

    .line 707
    const-string v0, "javascript:document.getElementsByTagName(\'body\')[0].innerHTML=SvgLoader.getSvg();"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->loadUrl(Ljava/lang/String;)V

    .line 709
    sget-object v0, LYq;->d:LYq;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LYq;

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 236
    iget v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:I

    return v0
.end method

.method public a(ILXP;)LXB;
    .locals 5

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Ljava/lang/String;

    const-string v1, "in createSlideContentReceiver for requestedSlideIndex=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 270
    new-instance v0, LYi;

    invoke-direct {v0, p0, p2, p1}, LYi;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;LXP;I)V

    return-object v0
.end method

.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 203
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Z

    if-eqz v0, :cond_0

    .line 218
    :goto_0
    return-void

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Ljava/lang/String;

    const-string v1, "cleaning"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Z

    .line 211
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 212
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 213
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(I)V

    .line 214
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->setVisibility(I)V

    .line 215
    const-string v0, "SvgLoader"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->removeJavascriptInterface(Ljava/lang/String;)V

    .line 216
    const-string v0, "about:blank"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->loadUrl(Ljava/lang/String;)V

    .line 217
    iput-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LYr;

    goto :goto_0
.end method

.method public a(ILXP;)V
    .locals 5

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Ljava/lang/String;

    const-string v1, "in updateSvgView wasCleaned=%s slideRequested=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 245
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Z

    if-eqz v0, :cond_0

    .line 258
    :goto_0
    return-void

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LXD;

    invoke-virtual {v0}, LXD;->c()I

    move-result v0

    .line 250
    if-nez v0, :cond_1

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Ljava/lang/String;

    const-string v1, "exit updateSvgView as slideCount == 0"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 255
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(I)V

    .line 256
    invoke-direct {p0, p2}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(LXP;)V

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LXQ;

    invoke-interface {v0, p1, p0, p2}, LXQ;->a(ILcom/google/android/apps/docs/punchwebview/PunchSvgWebView;LXP;)V

    goto :goto_0
.end method

.method public a(LYr;)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LYr;

    .line 222
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 588
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Ljava/lang/String;

    const-string v1, "in updateSvgView"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 589
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Z

    if-eqz v0, :cond_0

    .line 598
    :goto_0
    return-void

    .line 593
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 594
    :cond_1
    const-string p1, " "

    .line 597
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 555
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->d:Ljava/lang/String;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 476
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Landroid/os/Handler;

    new-instance v1, LYn;

    invoke-direct {v1, p0}, LYn;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 483
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 516
    const/4 v0, -0x2

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a(I)V

    .line 517
    const-string v0, " "

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b(Ljava/lang/String;)V

    .line 518
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->clearView()V

    .line 519
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 662
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:Landroid/os/Handler;

    new-instance v1, LYo;

    invoke-direct {v1, p0}, LYo;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 672
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 679
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Z

    if-nez v0, :cond_0

    .line 680
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->m()V

    .line 682
    :cond_0
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 685
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 686
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->e()V

    .line 688
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 139
    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->onLayout(ZIIII)V

    .line 140
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->getWidth()I

    move-result v2

    .line 141
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->getHeight()I

    move-result v3

    .line 143
    iget-object v4, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Ljava/lang/String;

    const-string v5, "in svgView onLayout, changed=%s progressState=%s viewWidth=%s viewHeight=%s lastWidth=%s lastHeight=%s wasCleaned=%s"

    const/4 v6, 0x7

    new-array v6, v6, [Ljava/lang/Object;

    .line 145
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v6, v0

    iget-object v7, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LYq;

    aput-object v7, v6, v1

    const/4 v7, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x4

    iget v8, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->c:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x5

    iget v8, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->d:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x6

    iget-boolean v8, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v6, v7

    .line 143
    invoke-static {v4, v5, v6}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 146
    iget-boolean v4, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Z

    if-eqz v4, :cond_1

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    iget v4, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->c:I

    if-ne v4, v2, :cond_2

    iget v4, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->d:I

    if-eq v4, v3, :cond_3

    :cond_2
    move v0, v1

    .line 151
    :cond_3
    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->a:LYq;

    sget-object v1, LYq;->a:LYq;

    if-ne v0, v1, :cond_4

    .line 156
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->l()V

    goto :goto_0

    .line 160
    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    iput v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->c:I

    .line 162
    iput v3, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->d:I

    .line 163
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 164
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->m()V

    goto :goto_0

    .line 166
    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->k()V

    goto :goto_0
.end method

.method public setIsZoomable(Z)V
    .locals 1

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 194
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    .line 195
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 715
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSvgWebView;->b:Ljava/lang/String;

    return-object v0
.end method
