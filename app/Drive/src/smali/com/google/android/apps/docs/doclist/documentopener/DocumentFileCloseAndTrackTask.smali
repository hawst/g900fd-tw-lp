.class public Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;
.super Lcom/google/android/apps/docs/utils/AbstractParcelableTask;
.source "DocumentFileCloseAndTrackTask.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/google/android/apps/docs/utils/ParcelableTask;

.field private final a:Ljava/lang/Object;

.field public a:LqK;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, LFQ;

    invoke-direct {v0}, LFQ;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/docs/utils/ParcelableTask;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/apps/docs/utils/AbstractParcelableTask;-><init>()V

    .line 49
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/utils/ParcelableTask;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;->a:Lcom/google/android/apps/docs/utils/ParcelableTask;

    .line 50
    iput-object p2, p0, Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;->a:Ljava/lang/Object;

    .line 51
    return-void
.end method


# virtual methods
.method protected final a(Laju;)V
    .locals 3

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;->a:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;->a:LqK;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;->a:Ljava/lang/Object;

    const-string v2, "openIntentDuration"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;->a:Lcom/google/android/apps/docs/utils/ParcelableTask;

    invoke-interface {v0, p1}, Lcom/google/android/apps/docs/utils/ParcelableTask;->b(Laju;)V

    .line 71
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 75
    const-string v0, "DocumentFileCloseAndTrackTask[%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;->a:Lcom/google/android/apps/docs/utils/ParcelableTask;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;->a:Lcom/google/android/apps/docs/utils/ParcelableTask;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 56
    return-void
.end method
