.class public Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;
.super Lrm;
.source "PickFilesToUploadActivity.java"


# instance fields
.field private a:LaFO;

.field public a:Lald;

.field private a:Lcom/google/android/gms/drive/database/data/EntrySpec;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lrm;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 61
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.docs.CreateNewDocument.UPLOAD_FILE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 65
    const-class v1, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 66
    const-string v1, "accountName"

    invoke-virtual {p1}, LaFO;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 67
    const-string v1, "collectionEntrySpec"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 69
    return-object v0
.end method

.method private a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 133
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 134
    if-nez v0, :cond_1

    .line 135
    sget v0, LpR;->upload_notification_failure_ticker:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 136
    iget-object v1, p0, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->a:Lald;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lald;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->b(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 138
    :cond_2
    new-instance v1, LaaJ;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, v2}, LaaJ;-><init>(Landroid/content/ContentResolver;)V

    .line 139
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    .line 140
    const-string v3, "mime_type"

    invoke-virtual {v1, v0, v3, v2}, LaaJ;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 142
    new-instance v2, Labb;

    invoke-direct {v2, p0}, Labb;-><init>(Landroid/content/Context;)V

    .line 143
    invoke-virtual {v2, v0, v1}, Labb;->a(Landroid/net/Uri;Ljava/lang/String;)Labb;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->a:LaFO;

    .line 144
    invoke-virtual {v0, v1}, Labb;->a(LaFO;)Labb;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0, v1}, Labb;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Labb;

    move-result-object v0

    invoke-virtual {v0}, Labb;->a()Landroid/content/Intent;

    move-result-object v0

    .line 145
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private a(Landroid/net/Uri;)Z
    .locals 6

    .prologue
    .line 171
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->b(Landroid/net/Uri;)Z

    move-result v0

    invoke-static {v0}, LbiT;->a(Z)V

    .line 173
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 174
    if-eqz v0, :cond_0

    .line 175
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 177
    :catch_0
    move-exception v0

    .line 178
    sget v1, LpR;->upload_notification_failure_ticker:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 179
    iget-object v2, p0, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->a:Lald;

    invoke-interface {v2, v1, v0}, Lald;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 180
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/content/Intent;)V
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 151
    invoke-virtual {p1}, Landroid/content/Intent;->getClipData()Landroid/content/ClipData;

    move-result-object v2

    .line 152
    if-eqz v2, :cond_1

    .line 153
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v3

    move v0, v1

    .line 155
    :goto_0
    invoke-virtual {v2}, Landroid/content/ClipData;->getItemCount()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 156
    invoke-virtual {v2, v0}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v4

    .line 157
    invoke-virtual {v4}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v5

    .line 158
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 159
    const-string v6, "PickFilesToUploadActivity"

    const-string v7, "[item %s] | [uri %s]"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v4}, Landroid/content/ClipData$Item;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v8, v1

    const/4 v4, 0x1

    aput-object v5, v8, v4

    invoke-static {v6, v7, v8}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 155
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 161
    :cond_0
    new-instance v0, Labb;

    invoke-direct {v0, p0}, Labb;-><init>(Landroid/content/Context;)V

    .line 162
    invoke-virtual {v0, v3}, Labb;->a(Ljava/util/ArrayList;)Labb;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->a:LaFO;

    .line 163
    invoke-virtual {v0, v1}, Labb;->a(LaFO;)Labb;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0, v1}, Labb;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Labb;

    move-result-object v0

    invoke-virtual {v0}, Labb;->a()Landroid/content/Intent;

    move-result-object v0

    .line 164
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->startActivity(Landroid/content/Intent;)V

    .line 168
    :goto_1
    return-void

    .line 166
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->a(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method private b(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 186
    const-string v0, "content"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 108
    invoke-super {p0, p1, p2, p3}, Lrm;->onActivityResult(IILandroid/content/Intent;)V

    .line 110
    packed-switch p1, :pswitch_data_0

    .line 127
    const-string v0, "PickFilesToUploadActivity"

    const-string v1, "Unexpected activity request code: %d"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 130
    :goto_0
    return-void

    .line 112
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    .line 114
    invoke-static {}, LakQ;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    invoke-direct {p0, p3}, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->b(Landroid/content/Intent;)V

    .line 123
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->finish()V

    goto :goto_0

    .line 117
    :cond_1
    invoke-direct {p0, p3}, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->a(Landroid/content/Intent;)V

    goto :goto_1

    .line 119
    :cond_2
    if-eqz p2, :cond_0

    .line 120
    sget v0, LpR;->gallery_select_error:I

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 110
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 74
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 77
    const-string v0, "accountName"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->a:LaFO;

    .line 78
    const-string v0, "collectionEntrySpec"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 80
    if-eqz p1, :cond_0

    .line 104
    :goto_0
    return-void

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->a:LaFO;

    if-nez v0, :cond_1

    .line 85
    const-string v0, "PickFilesToUploadActivity"

    const-string v2, "Account name is not specified in the intent."

    invoke-static {v0, v2}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->a:LaFO;

    if-eqz v0, :cond_3

    const-string v0, "com.google.android.apps.docs.CreateNewDocument.UPLOAD_FILE"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->a:LqK;

    const-string v1, "/createNewFromUpload"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 91
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 92
    const-string v1, "android.intent.category.OPENABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    const-string v1, "*/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 95
    invoke-static {}, LakQ;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 96
    const-string v1, "android.intent.extra.ALLOW_MULTIPLE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 97
    const-string v1, "PickFilesToUploadActivity"

    const-string v2, "Added Allow multiple extra"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 99
    :cond_2
    invoke-virtual {p0, v0, v4}, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 101
    :cond_3
    invoke-virtual {p0, v4}, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->setResult(I)V

    .line 102
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickFilesToUploadActivity;->finish()V

    goto :goto_0
.end method
