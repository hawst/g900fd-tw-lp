.class public Lcom/google/android/apps/docs/app/DocListActivity;
.super Lrd;
.source "DocListActivity.java"

# interfaces
.implements LAQ;
.implements LAm;
.implements LAo;
.implements LCW;
.implements LCX;
.implements LDB;
.implements LFc;
.implements LN;
.implements LRf;
.implements LtE;
.implements LvV;


# static fields
.field private static final a:[C


# instance fields
.field private a:I

.field public a:LAB;

.field public a:LAP;

.field public a:LAn;

.field private a:LBR;

.field private a:LBu;

.field public a:LCP;

.field public a:LCU;

.field private final a:LCV;

.field public a:LCo;

.field public a:LFF;

.field private final a:LJJ;

.field public a:LJK;

.field public a:LJM;

.field public a:LJR;

.field public a:LJf;

.field public a:LKZ;

.field private a:LKe;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LKe",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;"
        }
    .end annotation
.end field

.field public a:LMq;

.field public a:LMt;

.field public a:LPp;

.field public a:LPu;

.field public a:LQr;

.field public a:LRO;

.field public a:LSF;

.field public a:LZV;

.field public a:LaFO;

.field public a:LaGM;

.field public a:LaGR;

.field public a:LaKR;

.field public a:Laac;

.field private a:Laad;

.field a:LabW;

.field public a:Lacj;

.field public a:LadL;

.field public a:Lahh;

.field public a:Lahy;

.field private a:Lahz;

.field public a:LajQ;

.field public a:Lakl;

.field private a:Lakn;

.field private a:Landroid/database/ContentObserver;

.field private a:Landroid/view/Menu;

.field private a:Landroid/view/MenuInflater;

.field private a:Landroid/widget/ProgressBar;

.field private a:Landroid/widget/Toast;

.field public a:Lapn;

.field private a:Larg;

.field public a:Larh;

.field public a:Larp;

.field private a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public a:Lcom/google/android/apps/docs/fragment/DocListFragment;

.field private a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

.field private a:Ljava/lang/Runnable;

.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LvJ;",
            ">;"
        }
    .end annotation
.end field

.field public a:LpW;

.field public a:LqF;

.field private a:LqZ;

.field public a:Lrc;

.field public a:LsC;

.field public a:LtK;

.field public a:LvO;

.field public a:LvU;

.field public a:Lwa;

.field public a:Lwg;

.field public a:Lwm;

.field private b:I

.field private b:Landroid/database/ContentObserver;

.field private final b:Landroid/os/Handler;

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 303
    const/4 v0, 0x1

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x3f

    aput-char v2, v0, v1

    sput-object v0, Lcom/google/android/apps/docs/app/DocListActivity;->a:[C

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 170
    invoke-direct {p0}, Lrd;-><init>()V

    .line 448
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->b:Landroid/os/Handler;

    .line 449
    new-instance v0, LCY;

    invoke-direct {v0, p0, p0, p0}, LCY;-><init>(LCW;LCX;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LCV;

    .line 468
    iput-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Ljava/lang/Runnable;

    .line 475
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->i:Z

    .line 476
    iput-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/view/Menu;

    .line 492
    new-instance v0, LsB;

    invoke-direct {v0, p0}, LsB;-><init>(Lcom/google/android/apps/docs/app/DocListActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LJJ;

    return-void
.end method

.method private A()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 1207
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 1209
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 1211
    const/16 v1, 0x8

    const/16 v2, 0x18

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 1213
    return-void
.end method

.method private B()V
    .locals 4

    .prologue
    .line 1341
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/database/ContentObserver;

    if-nez v0, :cond_0

    .line 1342
    new-instance v0, Lsk;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->b:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lsk;-><init>(Lcom/google/android/apps/docs/app/DocListActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/database/ContentObserver;

    .line 1350
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, LaEG;->f:LaEG;

    .line 1351
    invoke-virtual {v1}, LaEG;->a()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/database/ContentObserver;

    .line 1350
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1353
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lwm;

    invoke-interface {v0}, Lwm;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1354
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->D()V

    .line 1357
    :cond_0
    return-void
.end method

.method private C()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1364
    const-string v1, "DocListActivity"

    const-string v2, "in updateWorkStatus"

    invoke-static {v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1365
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LSF;

    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LaFO;

    invoke-interface {v1, v2}, LSF;->b(LaFO;)Z

    move-result v1

    .line 1366
    sget-object v2, Laat;->b:Laat;

    iget-object v3, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LRO;

    .line 1367
    invoke-interface {v3}, LRO;->a()Laat;

    move-result-object v3

    invoke-virtual {v2, v3}, Laat;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 1368
    iget-object v3, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LaKR;

    invoke-interface {v3}, LaKR;->a()Z

    move-result v3

    .line 1369
    if-nez v3, :cond_0

    .line 1370
    iget-object v3, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LSF;

    iget-object v4, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LaFO;

    invoke-interface {v3, v4, v0}, LSF;->a(LaFO;Z)V

    .line 1372
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LSF;

    iget-object v4, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LaFO;

    invoke-interface {v3, v4}, LSF;->a(LaFO;)Z

    move-result v3

    .line 1373
    if-nez v3, :cond_1

    if-nez v1, :cond_1

    if-eqz v2, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 1375
    :cond_2
    invoke-static {v1, v3}, LapE;->a(ZZ)LapE;

    move-result-object v2

    .line 1376
    iget-object v4, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-virtual {v4, v2}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a(LapE;)V

    .line 1378
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/DocListActivity;->d(Z)V

    .line 1379
    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a(Z)V

    .line 1383
    if-eqz v3, :cond_3

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->c()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1384
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Ljava/lang/Runnable;

    if-nez v0, :cond_3

    .line 1385
    new-instance v0, Lsl;

    invoke-direct {v0, p0}, Lsl;-><init>(Lcom/google/android/apps/docs/app/DocListActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Ljava/lang/Runnable;

    .line 1396
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1399
    :cond_3
    return-void
.end method

.method private D()V
    .locals 4

    .prologue
    .line 1409
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LRO;

    invoke-interface {v0}, LRO;->a()Laat;

    move-result-object v0

    .line 1411
    sget-object v1, Lsq;->a:[I

    invoke-virtual {v0}, Laat;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1426
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown search state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1416
    :pswitch_0
    sget v0, Lxi;->search_showing_local_results_only:I

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/widget/Toast;

    .line 1418
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1429
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->C()V

    .line 1430
    return-void

    .line 1411
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private E()V
    .locals 2

    .prologue
    .line 1433
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 1434
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1436
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/database/ContentObserver;

    .line 1438
    :cond_0
    return-void
.end method

.method private F()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/16 v2, 0x10

    const/4 v1, 0x0

    .line 1442
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 1445
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 1447
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 1449
    invoke-virtual {v0, v2, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 1452
    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1453
    sget v1, Lxe;->navigation_breadcrumb:I

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 1456
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->J()V

    .line 1457
    return-void
.end method

.method private G()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 1507
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeAsUpIndicator(Landroid/graphics/drawable/Drawable;)V

    .line 1508
    return-void
.end method

.method private H()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 1512
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->a()LBu;

    move-result-object v0

    invoke-interface {v0}, LBu;->c()V

    .line 1513
    invoke-static {}, LakQ;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1514
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->G()V

    .line 1516
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/DocListActivity;->c(Z)V

    .line 1517
    return-void
.end method

.method private I()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 1537
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Ljava/util/List;

    if-nez v0, :cond_1

    .line 1557
    :cond_0
    :goto_0
    return-void

    .line 1540
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1542
    const/4 v1, 0x1

    if-le v0, v1, :cond_2

    .line 1543
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->F()V

    .line 1544
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lwm;

    invoke-interface {v0}, Lwm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1545
    const-string v0, "DocListActivity"

    const-string v1, "locking bar for search result"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1546
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->H()V

    goto :goto_0

    .line 1549
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvJ;

    invoke-virtual {v0}, LvJ;->a()Ljava/lang/String;

    move-result-object v0

    .line 1550
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lwm;

    invoke-interface {v1}, Lwm;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1551
    const-string v1, "DocListActivity"

    const-string v2, "Showing search result"

    invoke-static {v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1552
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/DocListActivity;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 1554
    :cond_3
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/DocListActivity;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private J()V
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1561
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LakQ;->a(Landroid/content/res/Resources;)Z

    move-result v0

    .line 1562
    invoke-static {p0}, Lamt;->a(Landroid/content/Context;)Z

    move-result v1

    .line 1564
    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    .line 1565
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvJ;

    invoke-virtual {v0}, LvJ;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/DocListActivity;->b(Ljava/lang/String;)V

    .line 1602
    :cond_0
    :goto_0
    return-void

    .line 1569
    :cond_1
    sget-object v0, Larj;->a:Larj;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Larg;

    invoke-interface {v1}, Larg;->a()Larj;

    move-result-object v1

    invoke-virtual {v0, v1}, Larj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1572
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    .line 1573
    sget v1, Lxc;->collection_path:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1574
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1575
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    move v4, v5

    .line 1578
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v4, v1, :cond_3

    .line 1579
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LvJ;

    .line 1580
    sget v2, Lxe;->navigation_breadcrumb_item:I

    .line 1581
    invoke-virtual {v6, v2, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 1582
    sget v3, Lxc;->breadcrumb_text:I

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 1583
    invoke-virtual {v1}, LvJ;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1585
    invoke-virtual {v1}, LvJ;->a()LbmF;

    move-result-object v1

    .line 1586
    new-instance v7, Lsm;

    invoke-direct {v7, p0, v1}, Lsm;-><init>(Lcom/google/android/apps/docs/app/DocListActivity;LbmF;)V

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1595
    sget v1, Lxc;->breadcrumb_arrow:I

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1596
    iget-object v3, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v4, v3, :cond_2

    .line 1597
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1599
    :cond_2
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1578
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 1601
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->K()V

    goto :goto_0
.end method

.method private K()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 1606
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LtK;

    sget-object v1, Lry;->h:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1607
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    .line 1610
    if-eqz v0, :cond_0

    .line 1611
    sget v1, Lxc;->collection_scroller:I

    .line 1612
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    .line 1613
    new-instance v1, Lsn;

    invoke-direct {v1, p0, v0}, Lsn;-><init>(Lcom/google/android/apps/docs/app/DocListActivity;Landroid/widget/HorizontalScrollView;)V

    .line 1620
    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/HorizontalScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1623
    :cond_0
    return-void
.end method

.method private L()V
    .locals 2

    .prologue
    .line 1886
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Laac;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Laad;

    invoke-virtual {v0, v1}, Laac;->a(Laad;)V

    .line 1887
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lakl;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lakn;

    invoke-virtual {v0, v1}, Lakl;->a(Lakn;)V

    .line 1888
    return-void
.end method

.method private M()V
    .locals 2

    .prologue
    .line 1891
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Laac;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Laad;

    invoke-virtual {v0, v1}, Laac;->b(Laad;)V

    .line 1892
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lakl;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lakn;

    invoke-virtual {v0, v1}, Lakl;->b(Lakn;)V

    .line 1893
    return-void
.end method

.method private a()LBu;
    .locals 1

    .prologue
    .line 739
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LBu;

    if-nez v0, :cond_0

    .line 740
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->x()V

    .line 743
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LBu;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocListActivity;)LBu;
    .locals 1

    .prologue
    .line 170
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->a()LBu;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;LaFO;)Landroid/content/Intent;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;",
            "LaFO;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 1809
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1810
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1811
    const-class v1, Lcom/google/android/apps/docs/app/DocListActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1813
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1814
    const-string v2, "accountName"

    invoke-virtual {p2}, LaFO;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1815
    invoke-static {v1, p1}, Lwq;->a(Landroid/os/Bundle;Ljava/util/List;)V

    .line 1816
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1818
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocListActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->b:Landroid/os/Handler;

    return-object v0
.end method

.method public static a(LaGM;Ljava/util/List;LaFO;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGM;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;",
            "LaFO;",
            ")",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;"
        }
    .end annotation

    .prologue
    .line 996
    const/4 v0, 0x0

    .line 997
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 998
    invoke-static {p1}, Lwr;->a(Ljava/util/List;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 999
    if-nez v0, :cond_0

    .line 1000
    invoke-interface {p0, p2}, LaGM;->a(LaFO;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 1004
    :cond_0
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocListActivity;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Ljava/lang/Runnable;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocListActivity;)LqK;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LqK;

    return-object v0
.end method

.method private a(LM;I)V
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 748
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 750
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lxe;->tablet_doclist_with_nav_drawer:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 752
    sget v0, Lxc;->main_container:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 753
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    .line 756
    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setFocusable(Z)V

    .line 758
    :cond_0
    invoke-virtual {v3, v7}, Landroid/view/View;->setFocusable(Z)V

    .line 759
    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/app/DocListActivity;->setContentView(Landroid/view/View;)V

    .line 761
    sget v0, Lxc;->navigation_fragment:I

    invoke-virtual {p1, v0}, LM;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    .line 766
    new-instance v0, LDz;

    iget-object v4, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iget-object v5, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lwm;

    iget-object v6, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Larh;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, LDz;-><init>(Lrm;LDB;Landroid/view/View;Lcom/google/android/apps/docs/fragment/NavigationFragment;Lwm;Larh;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LBu;

    .line 768
    sget v0, Lxc;->doc_list_fragment:I

    invoke-virtual {p1, v0}, LM;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/fragment/DocListFragment;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    .line 769
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a(I)V

    .line 772
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Integer;

    sget v1, Lxc;->menu_search:I

    .line 773
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    const/4 v1, 0x1

    sget v2, Lxc;->menu_create_new_doc:I

    .line 774
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lxc;->menu_refresh_icon:I

    .line 775
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lxc;->menu_filter_by:I

    .line 776
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget v2, Lxc;->menu_sortings:I

    .line 777
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget v2, Lxc;->menu_open_with_picker:I

    .line 778
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget v2, Lxc;->menu_add_new_office_doc:I

    .line 779
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 772
    invoke-static {v0}, LbmY;->a([Ljava/lang/Object;)LbmY;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LbmY;

    .line 781
    return-void
.end method

.method public static a(Landroid/app/Activity;LaFO;)V
    .locals 2

    .prologue
    .line 1869
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/app/DocListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1870
    invoke-static {p0, v0}, Lar;->a(Landroid/app/Activity;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1872
    invoke-static {p0, p1}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->b(Landroid/content/Context;LaFO;)Landroid/content/Intent;

    move-result-object v0

    .line 1873
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 1874
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 1883
    :goto_0
    return-void

    .line 1881
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;LaFO;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;",
            "LaFO;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1823
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/docs/app/DocListActivity;->a(Landroid/content/Context;Ljava/util/List;LaFO;)Landroid/content/Intent;

    move-result-object v0

    .line 1824
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1825
    return-void
.end method

.method private a(Landroid/view/Menu;Z)V
    .locals 6

    .prologue
    .line 1022
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LBu;

    invoke-interface {v0}, LBu;->a()Z

    move-result v0

    .line 1024
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->y()V

    .line 1028
    if-eqz v0, :cond_0

    .line 1029
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LbmY;

    sget v1, Lxc;->menu_grid_mode:I

    .line 1030
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lxc;->menu_list_mode:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmY;

    move-result-object v1

    .line 1029
    invoke-static {v0, v1}, LbpU;->a(Ljava/util/Set;Ljava/util/Set;)Lbqb;

    move-result-object v0

    invoke-static {v0}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v3

    .line 1035
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Larg;

    iget v4, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:I

    iget v5, p0, Lcom/google/android/apps/docs/app/DocListActivity;->b:I

    move-object v1, p1

    move v2, p2

    invoke-interface/range {v0 .. v5}, Larg;->a(Landroid/view/Menu;ZLbmY;II)V

    .line 1037
    return-void

    .line 1032
    :cond_0
    invoke-static {}, LbmY;->a()LbmY;

    move-result-object v3

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocListActivity;)V
    .locals 0

    .prologue
    .line 170
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->C()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocListActivity;Z)V
    .locals 0

    .prologue
    .line 170
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/DocListActivity;->c(Z)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/DocListActivity;)LqK;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LqK;

    return-object v0
.end method

.method private b(Landroid/view/Menu;Z)V
    .locals 2

    .prologue
    .line 1040
    iget v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->b:I

    :goto_0
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1041
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1040
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1044
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->C()V

    .line 1045
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/DocListActivity;)V
    .locals 0

    .prologue
    .line 170
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->D()V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1471
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 1473
    sget-object v1, Larj;->a:Larj;

    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Larg;

    invoke-interface {v2}, Larg;->a()Larj;

    move-result-object v2

    invoke-virtual {v1, v2}, Larj;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1475
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 1476
    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 1478
    const/16 v1, 0x10

    invoke-virtual {v0, v3, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 1482
    :goto_0
    return-void

    .line 1480
    :cond_0
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    goto :goto_0
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/app/DocListActivity;)LqK;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LqK;

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1521
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 1524
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 1525
    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 1528
    const/16 v1, 0x10

    invoke-virtual {v0, v2, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 1530
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 1532
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->H()V

    .line 1533
    return-void
.end method

.method private c(Z)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 886
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/view/Menu;

    if-nez v1, :cond_1

    .line 912
    :cond_0
    return-void

    .line 889
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/view/Menu;

    sget v2, Lxc;->menu_create_new_doc:I

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 890
    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/view/Menu;

    sget v3, Lxc;->menu_open_with_picker:I

    invoke-interface {v2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 891
    const/4 v3, 0x2

    new-array v3, v3, [Landroid/view/MenuItem;

    aput-object v1, v3, v0

    const/4 v1, 0x1

    aput-object v2, v3, v1

    .line 892
    if-eqz p1, :cond_3

    .line 896
    array-length v1, v3

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, v3, v0

    .line 897
    if-eqz v2, :cond_2

    .line 898
    const/16 v4, 0x9

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 896
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 905
    :cond_3
    array-length v1, v3

    :goto_1
    if-ge v0, v1, :cond_0

    aget-object v2, v3, v0

    .line 906
    if-eqz v2, :cond_4

    .line 907
    const/16 v4, 0xa

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 905
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private d(Z)V
    .locals 2

    .prologue
    .line 1403
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 1404
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1406
    :cond_0
    return-void

    .line 1404
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private f()Z
    .locals 8

    .prologue
    .line 670
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LqF;

    invoke-virtual {v0}, LqF;->a()I

    move-result v0

    .line 675
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 676
    const-string v2, "lastHomescreenLogEventPreference"

    const-wide/16 v4, 0x0

    .line 677
    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 678
    sget-object v4, LaKN;->a:LaKN;

    invoke-virtual {v4}, LaKN;->a()J

    move-result-wide v4

    .line 680
    sub-long v2, v4, v2

    int-to-long v6, v0

    cmp-long v0, v2, v6

    if-lez v0, :cond_0

    .line 681
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 682
    const-string v1, "lastHomescreenLogEventPreference"

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 683
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 685
    const/4 v0, 0x1

    .line 687
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Z
    .locals 1

    .prologue
    .line 1080
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAn;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()Z
    .locals 1

    .prologue
    .line 1860
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private t()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 626
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Larp;

    invoke-virtual {v0}, Larp;->a()Larg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Larg;

    .line 627
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "action_bar"

    const-string v2, "id"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 628
    if-eqz v0, :cond_0

    .line 629
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lxa;->action_bar_left_margin:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 630
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lxa;->action_bar_right_margin:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 631
    invoke-virtual {v0, v1, v4, v2, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 633
    :cond_0
    return-void
.end method

.method private u()V
    .locals 1

    .prologue
    .line 639
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LCU;

    invoke-virtual {v0}, LCU;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 640
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->v()V

    .line 642
    :cond_0
    return-void
.end method

.method private v()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 646
    new-instance v0, Lsw;

    invoke-direct {v0, p0}, Lsw;-><init>(Lcom/google/android/apps/docs/app/DocListActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LKe;

    .line 654
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LJR;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LKe;

    invoke-virtual {v0, v1}, LJR;->a(LKe;)V

    .line 655
    return-void
.end method

.method private w()V
    .locals 2

    .prologue
    .line 658
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 659
    new-instance v0, LqJ;

    sget-object v1, LqH;->j:LqH;

    invoke-direct {v0, v1}, LqJ;-><init>(LqH;)V

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LaFO;

    .line 660
    invoke-virtual {v0, v1}, LqJ;->a(LaFO;)LqJ;

    move-result-object v0

    .line 661
    invoke-virtual {v0}, LqJ;->a()LqI;

    move-result-object v0

    .line 662
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LqF;

    invoke-virtual {v1, v0}, LqF;->a(LqI;)V

    .line 664
    :cond_0
    return-void
.end method

.method private x()V
    .locals 3

    .prologue
    .line 721
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LBu;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 723
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->a()LM;

    move-result-object v0

    .line 724
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 725
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 727
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/app/DocListActivity;->a(LM;I)V

    .line 730
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAB;

    if-eqz v1, :cond_0

    .line 731
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAB;

    invoke-interface {v1}, LAB;->a()Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 732
    invoke-virtual {v0}, LM;->a()Lac;

    move-result-object v0

    .line 733
    sget v2, Lxc;->custom_layer:I

    invoke-virtual {v0, v2, v1}, Lac;->b(ILandroid/support/v4/app/Fragment;)Lac;

    .line 734
    invoke-virtual {v0}, Lac;->a()I

    .line 736
    :cond_0
    return-void

    .line 721
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private y()V
    .locals 2

    .prologue
    .line 1170
    sget-object v0, Larj;->a:Larj;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Larg;

    invoke-interface {v1}, Larg;->a()Larj;

    move-result-object v1

    invoke-virtual {v0, v1}, Larj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1171
    invoke-static {}, LakQ;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1172
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LBu;

    invoke-interface {v0}, LBu;->a()Z

    move-result v0

    .line 1173
    if-eqz v0, :cond_0

    .line 1174
    sget v0, Lxi;->app_name:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocListActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/DocListActivity;->b(Ljava/lang/String;)V

    .line 1184
    :goto_0
    return-void

    .line 1176
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->I()V

    goto :goto_0

    .line 1179
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->z()V

    goto :goto_0

    .line 1182
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    goto :goto_0
.end method

.method private z()V
    .locals 3

    .prologue
    .line 1187
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 1188
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->A()V

    .line 1190
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lwa;

    invoke-interface {v0}, Lwa;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    .line 1191
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LaGR;

    new-instance v2, LsA;

    invoke-direct {v2, p0, v0}, LsA;-><init>(Lcom/google/android/apps/docs/app/DocListActivity;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V

    invoke-virtual {v1, v2}, LaGR;->b(LaGN;)V

    .line 1203
    return-void
.end method


# virtual methods
.method public a()LaFO;
    .locals 2

    .prologue
    .line 1796
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1798
    if-eqz v0, :cond_0

    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1799
    const-string v1, "app_data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 1802
    :cond_0
    if-eqz v0, :cond_1

    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1803
    :goto_0
    if-eqz v0, :cond_2

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    :goto_1
    return-object v0

    .line 1802
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1803
    :cond_2
    invoke-super {p0}, Lrd;->a()LaFO;

    move-result-object v0

    goto :goto_1
.end method

.method protected a()LarB;
    .locals 3

    .prologue
    .line 851
    new-instance v0, Lsx;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LsI;

    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LqK;

    invoke-direct {v0, p0, p0, v1, v2}, Lsx;-><init>(Lcom/google/android/apps/docs/app/DocListActivity;Landroid/app/Activity;LsI;LqK;)V

    return-object v0
.end method

.method public a(LaGM;Ljava/util/List;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGM;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;",
            ">;)",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;"
        }
    .end annotation

    .prologue
    .line 990
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LaFO;

    invoke-static {p1, p2, v0}, Lcom/google/android/apps/docs/app/DocListActivity;->a(LaGM;Ljava/util/List;LaFO;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 1767
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LBR;

    if-eqz v0, :cond_0

    .line 1768
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LBR;

    invoke-virtual {v0, p1, p2}, LBR;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1769
    if-eqz v0, :cond_0

    .line 1790
    :goto_0
    return-object v0

    .line 1773
    :cond_0
    const-class v0, Lacr;

    if-ne p1, v0, :cond_2

    .line 1774
    if-nez p2, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LbiT;->a(Z)V

    .line 1775
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lacj;

    invoke-virtual {v0}, Lacj;->a()Lacr;

    move-result-object v0

    goto :goto_0

    .line 1774
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1777
    :cond_2
    const-class v0, LFE;

    if-ne p1, v0, :cond_3

    .line 1778
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lapn;

    goto :goto_0

    .line 1780
    :cond_3
    const-class v0, LaqQ;

    if-ne p1, v0, :cond_4

    .line 1781
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAP;

    goto :goto_0

    .line 1783
    :cond_4
    const-class v0, LJJ;

    if-ne p1, v0, :cond_5

    .line 1784
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LJJ;

    goto :goto_0

    .line 1785
    :cond_5
    const-class v0, Lara;

    if-ne p1, v0, :cond_6

    .line 1786
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LJJ;

    goto :goto_0

    .line 1787
    :cond_6
    const-class v0, Larg;

    if-ne p1, v0, :cond_7

    .line 1788
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Larg;

    goto :goto_0

    .line 1790
    :cond_7
    invoke-super {p0, p1, p2}, Lrd;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 1750
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->a()LM;

    move-result-object v0

    invoke-virtual {v0}, LM;->a()I

    move-result v0

    if-nez v0, :cond_0

    .line 1751
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LvU;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LvU;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 1753
    :cond_0
    return-void
.end method

.method public a(LCl;)V
    .locals 2

    .prologue
    .line 1829
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAP;

    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, LAP;->a(Ljava/lang/Iterable;)V

    .line 1830
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAn;

    if-eqz v0, :cond_0

    .line 1831
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAn;

    invoke-interface {v0}, LAn;->b()V

    .line 1833
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->a()LBu;

    move-result-object v0

    invoke-interface {v0}, LBu;->b()V

    .line 1834
    return-void
.end method

.method public a(LaGu;)V
    .locals 1

    .prologue
    .line 1761
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAP;

    invoke-interface {v0, p1}, LAP;->a(LaGu;)V

    .line 1762
    return-void
.end method

.method public a(LaGu;I)V
    .locals 2

    .prologue
    .line 1644
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lapn;

    new-instance v1, Lso;

    invoke-direct {v1, p0, p1}, Lso;-><init>(Lcom/google/android/apps/docs/app/DocListActivity;LaGu;)V

    invoke-virtual {v0, v1}, Lapn;->c(Lapu;)V

    .line 1653
    return-void
.end method

.method public a(LaGu;ILcom/google/android/apps/docs/app/DocumentOpenMethod;)V
    .locals 1

    .prologue
    .line 841
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAP;

    invoke-interface {v0, p1, p2, p3}, LAP;->a(LaGu;ILcom/google/android/apps/docs/app/DocumentOpenMethod;)V

    .line 842
    return-void
.end method

.method public a(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V
    .locals 1

    .prologue
    .line 1639
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LPp;

    invoke-interface {v0, p1, p2}, LPp;->a(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V

    .line 1640
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1657
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/docs/app/DocListActivity;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1658
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/docs/app/DocListActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1659
    return-void
.end method

.method public a(Landroid/view/Menu;)V
    .locals 1

    .prologue
    .line 1049
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LPu;

    invoke-virtual {v0, p1}, LPu;->a(Landroid/view/Menu;)V

    .line 1050
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LvJ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1486
    iput-object p1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Ljava/util/List;

    .line 1487
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->y()V

    .line 1488
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAn;

    if-eqz v0, :cond_0

    .line 1489
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAn;

    invoke-interface {v0}, LAn;->b()V

    .line 1491
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->m()V

    .line 1492
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 823
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 1059
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LPu;

    invoke-virtual {v0, p1}, LPu;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public b(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1663
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/docs/app/NewMainProxyActivity;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1664
    const/high16 v0, 0x24000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1665
    const/high16 v0, 0x10000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1669
    const-string v0, "wasTaskRoot"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1671
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/DocListActivity;->startActivity(Landroid/content/Intent;)V

    .line 1672
    return-void
.end method

.method public b(Z)V
    .locals 3

    .prologue
    .line 815
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LaGM;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LtK;

    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LaFO;

    invoke-static {v0, p0, v1, v2}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(LaGM;Lrm;LtK;LaFO;)V

    .line 817
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LaFO;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LpW;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(LaFO;LpW;)V

    .line 818
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Latd;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->d()Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LaFO;

    invoke-virtual {v0, p0, v1, v2}, Latd;->a(Landroid/app/Activity;ZLaFO;)V

    .line 819
    return-void
.end method

.method public b_(Landroid/view/Menu;)V
    .locals 3

    .prologue
    .line 1054
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LPu;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LvU;

    invoke-interface {v1}, LvU;->a()LaGu;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, LPu;->a(Landroid/view/Menu;LaGu;I)V

    .line 1055
    return-void
.end method

.method public f()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 693
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_1

    .line 718
    :cond_0
    :goto_0
    return-void

    .line 701
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 702
    if-eqz v1, :cond_0

    .line 704
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getActivityLogo(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 705
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    if-eqz v2, :cond_2

    .line 706
    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setLogo(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 713
    :catch_0
    move-exception v0

    .line 714
    sget v2, Lxb;->actionbar_drive_logo:I

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setLogo(I)V

    .line 715
    const-string v1, "DocListActivity"

    const-string v2, "Activity logo was not found."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 711
    :cond_2
    :try_start_1
    sget v0, Lxb;->actionbar_drive_logo:I

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setLogo(I)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 960
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 961
    invoke-super {p0}, Lrd;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 977
    :goto_0
    return-object v0

    .line 964
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/view/MenuInflater;

    if-nez v0, :cond_1

    .line 966
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getActionBar()Landroid/app/ActionBar;

    .line 973
    new-instance v0, Landroid/view/MenuInflater;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    sget v2, Lxj;->ActionBarMenu:I

    invoke-direct {v1, p0, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/view/MenuInflater;

    .line 977
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/view/MenuInflater;

    goto :goto_0
.end method

.method protected i()V
    .locals 1

    .prologue
    .line 1165
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LZV;

    invoke-virtual {v0}, LZV;->b()V

    .line 1166
    invoke-super {p0}, Lrd;->i()V

    .line 1167
    return-void
.end method

.method public j()V
    .locals 5

    .prologue
    .line 915
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LqK;

    const-string v1, "doclist"

    const-string v2, "requestSyncEvent"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 917
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LaKR;

    invoke-interface {v0}, LaKR;->a()Z

    move-result v0

    .line 918
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LaGM;

    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LaFO;

    invoke-interface {v1, v2}, LaGM;->a(LaFO;)LaFM;

    move-result-object v1

    .line 920
    if-eqz v0, :cond_1

    .line 921
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LSF;

    invoke-virtual {v1}, LaFM;->a()LaFO;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, LSF;->a(LaFO;Z)V

    .line 922
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->C()V

    .line 924
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lahh;

    instance-of v0, v0, LagO;

    if-eqz v0, :cond_0

    .line 925
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lahh;

    check-cast v0, LagO;

    .line 926
    invoke-virtual {v1}, LaFM;->a()LaFO;

    move-result-object v2

    invoke-virtual {v0, v2}, LagO;->a(LaFO;)V

    .line 931
    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LSF;

    .line 932
    invoke-virtual {v1}, LaFM;->a()LaFO;

    move-result-object v3

    invoke-static {v2, v3}, LajS;->a(LSF;LaFO;)Landroid/accounts/Account;

    move-result-object v2

    .line 933
    invoke-static {}, Lcom/google/android/gms/drive/database/DocListProvider;->a()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Landroid/content/SyncResult;

    invoke-direct {v4}, Landroid/content/SyncResult;-><init>()V

    .line 931
    invoke-virtual {v0, v2, v3, v4}, LagO;->a(Landroid/accounts/Account;Ljava/lang/String;Landroid/content/SyncResult;)Ljava/lang/Thread;

    .line 935
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LSF;

    invoke-interface {v0, v1}, LSF;->a(LaFM;)V

    .line 936
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lwa;

    invoke-interface {v0}, Lwa;->b()V

    .line 944
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LaGM;

    invoke-interface {v0, v1}, LaGM;->b(LaFM;)V

    .line 945
    return-void

    .line 938
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    sget-object v2, LapE;->a:LapE;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a(LapE;)V

    .line 941
    sget v0, Lxi;->error_fetch_more_retry:I

    const/4 v2, 0x0

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public k()V
    .locals 1

    .prologue
    .line 983
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LFF;

    invoke-interface {v0}, LFF;->a()V

    .line 984
    return-void
.end method

.method public l()V
    .locals 1

    .prologue
    .line 1337
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAP;

    invoke-interface {v0}, LAP;->c()V

    .line 1338
    return-void
.end method

.method public m()V
    .locals 4

    .prologue
    .line 1496
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1503
    :cond_0
    :goto_0
    return-void

    .line 1500
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvJ;

    invoke-virtual {v0}, LvJ;->a()Ljava/lang/String;

    move-result-object v0

    .line 1501
    sget v1, Lxi;->announce_path_navigation:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/docs/app/DocListActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1502
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Landroid/view/View;

    move-result-object v1

    invoke-static {p0, v1, v0}, LUs;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public n()V
    .locals 2

    .prologue
    .line 1627
    const-string v0, "DocListActivity"

    const-string v1, "in TDLA.onContentObserverNotification"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1628
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LaFO;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1629
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAP;

    invoke-interface {v0}, LAP;->a()V

    .line 1630
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->C()V

    .line 1631
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    if-eqz v0, :cond_0

    .line 1632
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()V

    .line 1634
    :cond_0
    return-void
.end method

.method public o()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1676
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1677
    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->i:Z

    .line 1700
    :goto_0
    return-void

    .line 1680
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->i:Z

    .line 1682
    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LvU;

    invoke-interface {v2}, LvU;->a()LaGu;

    move-result-object v2

    .line 1683
    if-eqz v2, :cond_1

    .line 1684
    :goto_1
    if-eqz v0, :cond_2

    .line 1685
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->b:Landroid/os/Handler;

    new-instance v1, Lsp;

    invoke-direct {v1, p0, v2}, Lsp;-><init>(Lcom/google/android/apps/docs/app/DocListActivity;LaGu;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1683
    goto :goto_1

    .line 1698
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->p()V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 1288
    packed-switch p1, :pswitch_data_0

    .line 1331
    invoke-super {p0, p1, p2, p3}, Lrd;->onActivityResult(IILandroid/content/Intent;)V

    .line 1333
    :cond_0
    :goto_0
    return-void

    .line 1290
    :pswitch_0
    const-string v0, "DocListActivity"

    const-string v1, "in onActivityResult: REQUEST_CODE_CREATE_NEW"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1291
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1294
    const-string v0, "entrySpec.v2"

    .line 1295
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 1297
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LaGR;

    new-instance v2, Lsj;

    invoke-direct {v2, p0, v0, v0}, Lsj;-><init>(Lcom/google/android/apps/docs/app/DocListActivity;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    invoke-virtual {v1, v2}, LaGR;->a(LaGN;)V

    goto :goto_0

    .line 1326
    :pswitch_1
    const-string v0, "DocListActivity"

    const-string v1, "in onActivityResult: REQUEST_CODE_DOCLIST"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1327
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->a()LBu;

    move-result-object v0

    invoke-interface {v0}, LBu;->b()V

    .line 1328
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAP;

    invoke-interface {v0, p3}, LAP;->b(Landroid/content/Intent;)V

    goto :goto_0

    .line 1288
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 1838
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1839
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->s()V

    .line 1852
    :cond_0
    :goto_0
    return-void

    .line 1843
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAP;

    invoke-interface {v0}, LAP;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1844
    invoke-super {p0}, Lrd;->onBackPressed()V

    .line 1847
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->p()V

    .line 1849
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAn;

    if-eqz v0, :cond_0

    .line 1850
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAn;

    invoke-interface {v0}, LAn;->b()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    .line 828
    invoke-super {p0, p1}, Lrd;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 829
    const-string v0, "DocListActivity"

    const-string v1, "in onConfigurationChanged"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 831
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->a()LBu;

    move-result-object v0

    invoke-interface {v0, p1}, LBu;->a(Landroid/content/res/Configuration;)V

    .line 833
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LqK;

    const-string v1, "doclist"

    const-string v2, "configChangedEvent"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 835
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 498
    invoke-super {p0, p1}, Lrd;->onCreate(Landroid/os/Bundle;)V

    .line 499
    const-string v0, "DocListActivity"

    const-string v1, "in onCreate"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->t()V

    .line 503
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->w()V

    .line 507
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LBu;

    if-nez v0, :cond_0

    .line 508
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->x()V

    .line 511
    :cond_0
    new-instance v0, LBR;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    .line 512
    invoke-virtual {v1}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Lcom/google/android/apps/docs/view/DocListView;

    move-result-object v1

    invoke-direct {v0, v1}, LBR;-><init>(Lcom/google/android/apps/docs/view/DocListView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LBR;

    .line 514
    sget v0, Lxc;->refresh_progress_bar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/widget/ProgressBar;

    .line 515
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LtK;

    sget-object v1, Lry;->am:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 517
    iput-object v2, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/widget/ProgressBar;

    .line 521
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0, v2}, LUv;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 523
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->u()V

    .line 525
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LvU;

    invoke-interface {v0, p0}, LvU;->a(LvV;)V

    .line 526
    if-eqz p1, :cond_2

    .line 527
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lwg;

    invoke-interface {v0, p1}, Lwg;->b(Landroid/os/Bundle;)V

    .line 530
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 532
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->b()Landroid/view/View;

    move-result-object v1

    .line 533
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAn;

    iget-object v5, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LaFO;

    move-object v2, p0

    move-object v3, p0

    move-object v4, p0

    invoke-interface/range {v0 .. v5}, LAn;->a(Landroid/view/View;Landroid/app/Activity;LAm;LAo;LaFO;)V

    .line 535
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAn;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a(LapC;)V

    .line 542
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LSF;

    invoke-interface {v0}, LSF;->a()[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_7

    .line 543
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->finish()V

    .line 548
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->a()LM;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a(LM;)V

    .line 550
    new-instance v0, Lsh;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->b:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lsh;-><init>(Lcom/google/android/apps/docs/app/DocListActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->b:Landroid/database/ContentObserver;

    .line 562
    new-instance v0, Lsr;

    invoke-direct {v0, p0}, Lsr;-><init>(Lcom/google/android/apps/docs/app/DocListActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lahz;

    .line 575
    new-instance v0, Lss;

    invoke-direct {v0, p0}, Lss;-><init>(Lcom/google/android/apps/docs/app/DocListActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Laad;

    .line 589
    new-instance v0, Lsu;

    invoke-direct {v0, p0}, Lsu;-><init>(Lcom/google/android/apps/docs/app/DocListActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lakn;

    .line 603
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LtK;

    sget-object v1, Lry;->m:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 604
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LMt;

    .line 605
    invoke-static {}, LMt;->a()Landroid/content/IntentFilter;

    move-result-object v1

    .line 604
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/app/DocListActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 608
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->f()V

    .line 610
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LabW;

    if-eqz v0, :cond_5

    .line 611
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LabW;

    invoke-interface {v0}, LabW;->a()V

    .line 614
    :cond_5
    if-nez p1, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->d()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 617
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->c()Landroid/view/View;

    move-result-object v0

    .line 618
    if-eqz v0, :cond_6

    .line 619
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lrc;

    invoke-virtual {v1, v0}, Lrc;->a(Landroid/view/View;)LqZ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LqZ;

    .line 620
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LqZ;

    invoke-virtual {v0}, LqZ;->a()V

    .line 623
    :cond_6
    return-void

    .line 545
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAP;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LAP;->a(Landroid/os/Bundle;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 949
    iput-object p1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/view/Menu;

    .line 950
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:I

    .line 951
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LJM;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->a()LaqY;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LJM;->a(Landroid/view/Menu;LaqY;)V

    .line 952
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->b:I

    .line 953
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LPu;

    invoke-virtual {v0, p1}, LPu;->a(Landroid/view/Menu;)V

    .line 954
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 1263
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LtK;

    sget-object v1, Lry;->m:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1264
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LMt;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocListActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1267
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LabW;

    if-eqz v0, :cond_1

    .line 1268
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LabW;

    invoke-interface {v0}, LabW;->b()V

    .line 1271
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LqZ;

    if-eqz v0, :cond_2

    .line 1272
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LqZ;

    invoke-virtual {v0}, LqZ;->b()V

    .line 1275
    :cond_2
    invoke-super {p0}, Lrd;->onDestroy()V

    .line 1276
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1897
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1898
    sparse-switch p1, :sswitch_data_0

    .line 1916
    sget-object v1, Lcom/google/android/apps/docs/app/DocListActivity;->a:[C

    const/16 v2, 0xc1

    invoke-virtual {p2, v1, v2}, Landroid/view/KeyEvent;->getMatch([CI)C

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1925
    :cond_0
    invoke-super {p0, p1, p2}, Lrd;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 1900
    :sswitch_0
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LFF;

    invoke-interface {v1}, LFF;->a()V

    goto :goto_0

    .line 1904
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LBu;

    invoke-interface {v1}, LBu;->a()V

    goto :goto_0

    .line 1908
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LJK;

    invoke-interface {v1}, LJK;->b()V

    goto :goto_0

    .line 1918
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LJK;

    invoke-interface {v1}, LJK;->e()V

    goto :goto_0

    .line 1898
    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_1
        0x2a -> :sswitch_0
        0x37 -> :sswitch_2
    .end sparse-switch

    .line 1916
    :pswitch_data_0
    .packed-switch 0x3f
        :pswitch_0
    .end packed-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 794
    invoke-super {p0, p1}, Lrd;->onNewIntent(Landroid/content/Intent;)V

    .line 795
    const-string v0, "DocListActivity"

    const-string v1, "in onNewIntent"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 797
    const-string v0, "accountName"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 798
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LaFO;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 799
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/DocListActivity;->setIntent(Landroid/content/Intent;)V

    .line 800
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAP;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, LAP;->a(Landroid/os/Bundle;Landroid/content/Intent;)V

    .line 808
    :goto_0
    return-void

    .line 803
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v0

    const v1, -0x24000001

    and-int/2addr v0, v1

    .line 802
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 805
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/DocListActivity;->startActivity(Landroid/content/Intent;)V

    .line 806
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->finish()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 1217
    const-string v0, "DocListActivity"

    const-string v1, "in onPause"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1219
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lahy;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lahz;

    invoke-interface {v0, v1}, Lahy;->a(Lahz;)Z

    .line 1220
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->b:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1221
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->E()V

    .line 1223
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->a()LM;

    move-result-object v0

    invoke-virtual {v0, p0}, LM;->b(LN;)V

    .line 1225
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 1226
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 1227
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/widget/Toast;

    .line 1230
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Latd;

    invoke-virtual {v0}, Latd;->b()V

    .line 1232
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->a()LBu;

    move-result-object v0

    invoke-interface {v0}, LBu;->g()V

    .line 1234
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LakQ;->e(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "doclistLandscapeDuration"

    .line 1237
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LqK;

    invoke-virtual {v1, p0, v0}, LqK;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1239
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LabW;

    if-eqz v0, :cond_1

    .line 1240
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LabW;

    invoke-interface {v0}, LabW;->d()V

    .line 1243
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LZV;

    invoke-virtual {v0}, LZV;->b()V

    .line 1245
    invoke-super {p0}, Lrd;->onPause()V

    .line 1246
    return-void

    .line 1234
    :cond_2
    const-string v0, "doclistPortraitDuration"

    goto :goto_0
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 785
    invoke-super {p0, p1}, Lrd;->onPostCreate(Landroid/os/Bundle;)V

    .line 786
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->a()LBu;

    move-result-object v0

    invoke-interface {v0}, LBu;->h()V

    .line 787
    if-eqz p1, :cond_0

    .line 788
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Larg;

    invoke-interface {v0, p1}, Larg;->b(Landroid/os/Bundle;)V

    .line 790
    :cond_0
    return-void
.end method

.method protected onPostResume()V
    .locals 1

    .prologue
    .line 1159
    invoke-super {p0}, Lrd;->onPostResume()V

    .line 1160
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LZV;

    invoke-virtual {v0}, LZV;->a()V

    .line 1161
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1009
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LtK;

    sget-object v1, Lry;->q:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LvU;

    .line 1010
    invoke-interface {v0}, LvU;->a()LaGu;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1011
    :cond_0
    invoke-direct {p0, p1, v3}, Lcom/google/android/apps/docs/app/DocListActivity;->a(Landroid/view/Menu;Z)V

    .line 1012
    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/docs/app/DocListActivity;->b(Landroid/view/Menu;Z)V

    .line 1018
    :goto_0
    return v3

    .line 1014
    :cond_1
    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/docs/app/DocListActivity;->a(Landroid/view/Menu;Z)V

    .line 1015
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LPu;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LvU;

    invoke-interface {v1}, LvU;->a()LaGu;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/docs/app/DocListActivity;->b:I

    invoke-virtual {v0, p1, v1, v2}, LPu;->a(Landroid/view/Menu;LaGu;I)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1085
    invoke-super {p0}, Lrd;->onResume()V

    .line 1086
    const-string v0, "DocListActivity"

    const-string v1, "in onResume"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1092
    invoke-static {p0}, Lr;->a(Landroid/app/Activity;)Z

    .line 1094
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LtK;

    sget-object v1, Lry;->m:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1095
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LMq;

    invoke-virtual {v0, p0}, LMq;->a(Landroid/content/Context;)V

    .line 1098
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->L()V

    .line 1100
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LPp;

    invoke-interface {v0, v3}, LPp;->a(Z)V

    .line 1102
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->B()V

    .line 1103
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->a()LaqY;

    move-result-object v0

    .line 1105
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->a()LBu;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LaFO;

    invoke-interface {v1, v4, v2}, LBu;->a(Landroid/widget/Button;LaFO;)V

    .line 1106
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lwm;

    .line 1107
    invoke-interface {v1}, Lwm;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()Ljava/lang/String;

    move-result-object v1

    .line 1106
    invoke-interface {v0, v1}, LaqY;->a(Ljava/lang/String;)V

    .line 1108
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->a()LBu;

    move-result-object v0

    invoke-interface {v0}, LBu;->o_()V

    .line 1109
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->a()LBu;

    move-result-object v0

    invoke-interface {v0}, LBu;->e()V

    .line 1111
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->C()V

    .line 1113
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LSF;

    invoke-interface {v0}, LSF;->a()[Landroid/accounts/Account;

    move-result-object v0

    .line 1115
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->a()LBu;

    move-result-object v1

    new-instance v2, Lsy;

    invoke-direct {v2, p0}, Lsy;-><init>(Lcom/google/android/apps/docs/app/DocListActivity;)V

    invoke-interface {v1, v4, v0, v2}, LBu;->a(Landroid/widget/Button;[Landroid/accounts/Account;LaqX;)V

    .line 1126
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, LaEG;->h:LaEG;

    .line 1127
    invoke-virtual {v1}, LaEG;->a()Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocListActivity;->b:Landroid/database/ContentObserver;

    .line 1126
    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1128
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lahy;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lahz;

    invoke-interface {v0, v1}, Lahy;->a(Lahz;)V

    .line 1130
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->a()LM;

    move-result-object v0

    invoke-virtual {v0, p0}, LM;->a(LN;)V

    .line 1132
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->i:Z

    if-eqz v0, :cond_1

    .line 1136
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->b:Landroid/os/Handler;

    new-instance v1, Lsz;

    invoke-direct {v1, p0}, Lsz;-><init>(Lcom/google/android/apps/docs/app/DocListActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1144
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->y()V

    .line 1146
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LqK;

    invoke-virtual {v0, p0}, LqK;->a(Ljava/lang/Object;)V

    .line 1147
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LadL;

    invoke-interface {v0}, LadL;->b()V

    .line 1149
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LabW;

    if-eqz v0, :cond_2

    .line 1150
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LabW;

    invoke-interface {v0}, LabW;->c()V

    .line 1153
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LJf;

    invoke-interface {v0, p0}, LJf;->a(Landroid/content/Context;)V

    .line 1154
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LAP;

    invoke-interface {v0}, LAP;->a()V

    .line 1155
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1250
    invoke-super {p0, p1}, Lrd;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1251
    const-string v0, "DocListActivity"

    const-string v1, "in onSaveInstanceState"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1253
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LvU;

    invoke-interface {v0, p1}, LvU;->b(Landroid/os/Bundle;)V

    .line 1254
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lwm;

    invoke-interface {v0}, Lwm;->a()LbmF;

    move-result-object v0

    invoke-static {p1, v0}, Lwq;->a(Landroid/os/Bundle;Ljava/util/List;)V

    .line 1255
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lwg;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()I

    move-result v1

    invoke-interface {v0, v1}, Lwg;->a(I)V

    .line 1256
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lwg;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/fragment/DocListFragment;->d()I

    move-result v1

    invoke-interface {v0, v1}, Lwg;->b(I)V

    .line 1257
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lwg;

    invoke-interface {v0, p1}, Lwg;->a(Landroid/os/Bundle;)V

    .line 1258
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Larg;

    invoke-interface {v0, p1}, Larg;->a(Landroid/os/Bundle;)V

    .line 1259
    return-void
.end method

.method public onSearchRequested()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1065
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LCP;

    invoke-interface {v0}, LCP;->a()V

    .line 1067
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LaFO;

    .line 1068
    invoke-static {v0}, LzT;->a(LaFO;)Landroid/os/Bundle;

    move-result-object v0

    .line 1070
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v2, v0, v2}, Lcom/google/android/apps/docs/app/DocListActivity;->startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V

    .line 1071
    const/4 v0, 0x1

    return v0
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 1280
    invoke-super {p0}, Lrd;->onStop()V

    .line 1281
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->a()LBu;

    move-result-object v0

    invoke-interface {v0}, LBu;->j_()V

    .line 1282
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()V

    .line 1283
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->M()V

    .line 1284
    return-void
.end method

.method public p()V
    .locals 2

    .prologue
    .line 1708
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/view/Menu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1709
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LvU;

    invoke-interface {v0}, LvU;->a()LaGu;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 1710
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LCV;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LCV;

    .line 1711
    invoke-interface {v0}, LCV;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1712
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:LCV;

    invoke-interface {v0}, LCV;->a()V

    .line 1715
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_3

    .line 1716
    invoke-super {p0}, Lrd;->invalidateOptionsMenu()V

    .line 1720
    :cond_1
    :goto_1
    return-void

    .line 1709
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1717
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/view/Menu;

    if-eqz v0, :cond_1

    .line 1718
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocListActivity;->a:Landroid/view/Menu;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocListActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    goto :goto_1
.end method

.method public q()V
    .locals 1

    .prologue
    .line 1742
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->a()LM;

    move-result-object v0

    invoke-virtual {v0}, LM;->a()V

    .line 1743
    return-void
.end method

.method public r()V
    .locals 0

    .prologue
    .line 1757
    return-void
.end method

.method public s()V
    .locals 1

    .prologue
    .line 1856
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocListActivity;->a()LBu;

    move-result-object v0

    invoke-interface {v0}, LBu;->a()V

    .line 1857
    return-void
.end method
