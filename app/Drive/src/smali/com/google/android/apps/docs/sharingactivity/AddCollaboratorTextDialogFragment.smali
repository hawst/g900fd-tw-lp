.class public Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "AddCollaboratorTextDialogFragment.java"

# interfaces
.implements Labx;
.implements Landroid/text/TextWatcher;


# static fields
.field private static final a:Lqt;

.field private static m:I


# instance fields
.field public a:LQr;

.field public a:LaGM;

.field private a:LaGu;

.field public a:LabF;

.field private a:LabI;

.field public a:Labw;

.field public a:LacF;

.field private final a:Lacb;

.field public a:Lacz;

.field public a:LajO;

.field public a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private a:Landroid/app/AlertDialog;

.field private final a:Landroid/os/Handler;

.field a:Landroid/widget/Button;

.field private a:Landroid/widget/EditText;

.field private a:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

.field private a:Landroid/widget/MultiAutoCompleteTextView;

.field private a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "LabO;",
            ">;"
        }
    .end annotation
.end field

.field private a:LpD;

.field public a:LtK;

.field private a:[Ljava/lang/String;

.field public b:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Labg;",
            ">;"
        }
    .end annotation
.end field

.field b:Landroid/widget/Button;

.field public b:LqK;

.field public c:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Lacj;",
            ">;"
        }
    .end annotation
.end field

.field private w:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    sput v0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->m:I

    .line 99
    sget-object v0, Lqt;->b:Lqt;

    sput-object v0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Lqt;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    .line 103
    new-instance v0, Lacb;

    invoke-direct {v0}, Lacb;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Lacb;

    .line 171
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/os/Handler;

    .line 179
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->w:Z

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)LabI;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LabI;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/app/AlertDialog;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/os/Handler;

    return-object v0
.end method

.method private a(Landroid/content/Context;)Landroid/view/View;
    .locals 5

    .prologue
    .line 391
    const-string v0, "layout_inflater"

    .line 392
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 393
    sget v1, Lxe;->add_collaborator:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 398
    sget v0, Lxc;->sharing_options:I

    .line 399
    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AdapterView;

    .line 401
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LbmF;

    invoke-static {p1, v1}, LabO;->a(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    .line 402
    invoke-interface {v1, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 403
    new-instance v3, Landroid/widget/ArrayAdapter;

    sget v4, Lxe;->add_collaborator_sharing_option:I

    invoke-direct {v3, p1, v4, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 405
    const v1, 0x1090009

    invoke-virtual {v3, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 407
    invoke-virtual {v0, v3}, Landroid/widget/AdapterView;->setAdapter(Landroid/widget/Adapter;)V

    .line 409
    return-object v2
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)Landroid/widget/MultiAutoCompleteTextView;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView;

    return-object v0
.end method

.method private a(LaGu;Lcom/google/android/gms/drive/database/data/ResourceSpec;)LbmF;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGu;",
            "Lcom/google/android/gms/drive/database/data/ResourceSpec;",
            ")",
            "LbmF",
            "<",
            "LabO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LajO;

    iget-object v1, p2, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    .line 230
    invoke-interface {v0, v1}, LajO;->a(LaFO;)LajN;

    move-result-object v0

    .line 231
    if-eqz p1, :cond_0

    .line 232
    invoke-virtual {v0, p1}, LajN;->a(LaGu;)Ljava/util/Set;

    move-result-object v0

    .line 234
    :goto_0
    invoke-static {v0}, LabO;->a(Ljava/util/Set;)LbmF;

    move-result-object v0

    .line 233
    invoke-static {v0}, LbnG;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 235
    sget-object v1, LabO;->d:LabO;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 236
    invoke-static {v0}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    return-object v0

    .line 232
    :cond_0
    invoke-static {}, LbmY;->a()LbmY;

    move-result-object v0

    goto :goto_0
.end method

.method private a()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 470
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 471
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 472
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 474
    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v3, v1, v0}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenEnd(Ljava/lang/CharSequence;I)I

    move-result v3

    invoke-virtual {v1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 475
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 476
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 473
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v3, v1, v0}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenEnd(Ljava/lang/CharSequence;I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 479
    :cond_1
    return-object v2
.end method

.method public static synthetic a(Ljava/util/List;[Ljava/lang/String;)Ljava/util/List;
    .locals 1

    .prologue
    .line 73
    invoke-static {p0, p1}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->b(Ljava/util/List;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;LpD;)LpD;
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LpD;

    return-object p1
.end method

.method private a(Landroid/app/AlertDialog$Builder;)V
    .locals 2

    .prologue
    .line 356
    const/high16 v0, 0x1040000

    new-instance v1, Labl;

    invoke-direct {v1, p0}, Labl;-><init>(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 368
    sget v0, Lxi;->add_collaborator_accept:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 369
    return-void
.end method

.method private a(Landroid/app/AlertDialog;)V
    .locals 2

    .prologue
    .line 372
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/Button;

    .line 373
    const/4 v0, -0x2

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->b:Landroid/widget/Button;

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/Button;

    new-instance v1, Labm;

    invoke-direct {v1, p0}, Labm;-><init>(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 382
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 383
    return-void
.end method

.method private a(Landroid/app/AlertDialog;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 413
    sget v0, Lxc;->text_view:I

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/MultiAutoCompleteTextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView;

    .line 414
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0, p0}, Landroid/widget/MultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 415
    new-instance v0, Landroid/widget/MultiAutoCompleteTextView$CommaTokenizer;

    invoke-direct {v0}, Landroid/widget/MultiAutoCompleteTextView$CommaTokenizer;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    .line 416
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-virtual {v0, v1}, Landroid/widget/MultiAutoCompleteTextView;->setTokenizer(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;)V

    .line 418
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->v()V

    .line 420
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView;

    new-instance v1, Labn;

    invoke-direct {v1, p0}, Labn;-><init>(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/MultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 432
    if-eqz p2, :cond_1

    .line 433
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0, p2}, Landroid/widget/MultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 438
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v2}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/MultiAutoCompleteTextView;->setSelection(II)V

    .line 439
    return-void

    .line 434
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LacF;

    invoke-virtual {v0}, LacF;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 435
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LacF;

    invoke-virtual {v0}, LacF;->a()LbmF;

    move-result-object v0

    .line 436
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView;

    const-string v2, ","

    invoke-static {v2, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/MultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(Landroid/widget/EditText;Landroid/app/AlertDialog;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 311
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 315
    new-instance v0, Labk;

    invoke-direct {v0, p0, p2}, Labk;-><init>(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;Landroid/app/AlertDialog;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 324
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->u()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;Landroid/app/AlertDialog;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(Landroid/app/AlertDialog;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;Landroid/app/AlertDialog;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(Landroid/app/AlertDialog;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:[Ljava/lang/String;

    return-object v0
.end method

.method public static b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 735
    sget v0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->m:I

    .line 736
    sget v1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->m:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->m:I

    .line 737
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AddCollaboratorTextDialogFragment"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/util/List;[Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LabD;",
            ">;[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "LabD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 661
    new-instance v1, Ljava/util/HashSet;

    array-length v0, p1

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 662
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    .line 663
    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 662
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 665
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 666
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LabD;

    .line 667
    invoke-interface {v0}, LabD;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 668
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 671
    :cond_2
    return-object v2
.end method

.method private c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 240
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "confirmSharingDialog"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private d()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 676
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/EditText;

    if-nez v1, :cond_1

    .line 681
    :cond_0
    :goto_0
    return-object v0

    .line 680
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 681
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    goto :goto_0
.end method

.method private t()V
    .locals 2

    .prologue
    .line 460
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 461
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 462
    return-void

    .line 460
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private u()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 489
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()Ljava/util/ArrayList;

    move-result-object v7

    .line 490
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 507
    :goto_0
    return-void

    .line 494
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Lacz;

    invoke-interface {v0}, Lacz;->a()Lacr;

    move-result-object v6

    .line 495
    invoke-static {v7}, Lalb;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 496
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 497
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Labw;

    .line 498
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LaGu;

    invoke-interface {v3}, LaGu;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LaGu;

    invoke-interface {v4}, LaGu;->a()LaGw;

    move-result-object v4

    .line 499
    invoke-interface {v6}, Lacr;->b()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v6}, Lacr;->a()Ljava/lang/String;

    move-result-object v6

    .line 497
    invoke-virtual/range {v0 .. v7}, Labw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LaGw;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0

    .line 501
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()LH;

    move-result-object v1

    .line 502
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lxg;->add_collaborators_invalid_contact_address:I

    .line 503
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    new-array v5, v8, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, ", "

    .line 504
    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    .line 502
    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 501
    invoke-static {v1, v0, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 505
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private v()V
    .locals 3

    .prologue
    .line 510
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LQr;

    const-string v1, "enableMultiTokenCollaboratorSuggestions"

    const/4 v2, 0x1

    .line 511
    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 513
    if-eqz v0, :cond_0

    .line 514
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->w()V

    .line 518
    :goto_0
    return-void

    .line 516
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->x()V

    goto :goto_0
.end method

.method private w()V
    .locals 2

    .prologue
    .line 524
    const-string v0, "AddCollaboratorTextDialogFragment"

    const-string v1, "in createAdapterAsync"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 525
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LpD;

    if-eqz v0, :cond_0

    .line 588
    :goto_0
    return-void

    .line 529
    :cond_0
    new-instance v0, Labo;

    invoke-direct {v0, p0}, Labo;-><init>(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LpD;

    .line 587
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LpD;

    invoke-virtual {v0}, LpD;->start()V

    goto :goto_0
.end method

.method private x()V
    .locals 2

    .prologue
    .line 594
    const-string v0, "AddCollaboratorTextDialogFragment"

    const-string v1, "in createAdapterAsync"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LpD;

    if-eqz v0, :cond_0

    .line 636
    :goto_0
    return-void

    .line 599
    :cond_0
    new-instance v0, Labq;

    invoke-direct {v0, p0}, Labq;-><init>(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LpD;

    .line 635
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LpD;

    invoke-virtual {v0}, LpD;->start()V

    goto :goto_0
.end method

.method private y()V
    .locals 2

    .prologue
    .line 639
    const-string v0, "AddCollaboratorTextDialogFragment"

    const-string v1, "in killCreateAdapterThread"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 640
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LpD;

    .line 641
    if-eqz v0, :cond_0

    .line 642
    invoke-virtual {v0}, LpD;->a()V

    .line 644
    :try_start_0
    invoke-virtual {v0}, LpD;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 649
    :cond_0
    :goto_0
    return-void

    .line 645
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 245
    const-string v0, "AddCollaboratorTextDialogFragment"

    const-string v1, "in onCreateDialog"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    iget-boolean v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->w:Z

    if-eqz v0, :cond_0

    .line 247
    const-string v0, "AddCollaboratorTextDialogFragment"

    const-string v1, "Early exit in onCreateView"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->b()Landroid/app/Dialog;

    move-result-object v0

    .line 306
    :goto_0
    return-object v0

    .line 250
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()LH;

    move-result-object v0

    invoke-static {v0}, LEL;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v1

    .line 251
    invoke-static {v1}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v2

    .line 253
    new-instance v0, LabI;

    invoke-direct {v0, v1}, LabI;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LabI;

    .line 255
    sget v0, Lxi;->add_collaborators:I

    invoke-virtual {v2, v0}, LEU;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 256
    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(Landroid/app/AlertDialog$Builder;)V

    .line 258
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v3

    .line 259
    invoke-virtual {v2, v3}, LEU;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 260
    sget v0, Lxi;->add_collaborators:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 262
    sget v0, Lxc;->sharing_options:I

    .line 263
    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AdapterView;

    .line 264
    iget-object v4, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Lacb;

    invoke-virtual {v4, v0}, Lacb;->a(Landroid/widget/AdapterView;)V

    .line 266
    sget v0, Lxc;->message:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/EditText;

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LtK;

    sget-object v4, Lry;->K:Lry;

    .line 268
    invoke-interface {v0, v4}, LtK;->a(LtJ;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 270
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/EditText;

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setVisibility(I)V

    .line 273
    :cond_1
    invoke-virtual {v2}, LEU;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/app/AlertDialog;

    .line 275
    sget v0, Lxc;->text_view:I

    .line 276
    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v4, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/app/AlertDialog;

    .line 275
    invoke-direct {p0, v0, v4, v1}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(Landroid/widget/EditText;Landroid/app/AlertDialog;Landroid/content/Context;)V

    .line 278
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 279
    if-eqz p1, :cond_2

    .line 280
    const-string v1, "contactAddresses"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 283
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v1

    const-string v4, "contactAddresses"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-nez v1, :cond_4

    const/4 v0, 0x0

    .line 290
    :goto_1
    new-instance v1, Labj;

    invoke-direct {v1, p0, v0, v3}, Labj;-><init>(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;Ljava/lang/String;Landroid/view/View;)V

    invoke-virtual {v2, v1}, LEU;->a(Landroid/content/DialogInterface$OnShowListener;)LEU;

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 286
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 183
    const-string v0, "AddCollaboratorTextDialogFragment"

    const-string v1, "in onCreate"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a_(Landroid/os/Bundle;)V

    .line 187
    new-instance v0, Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()LH;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/MultiAutoCompleteTextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView;

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Lacz;

    invoke-interface {v0}, Lacz;->a()Lacr;

    move-result-object v0

    .line 189
    if-eqz v0, :cond_0

    .line 190
    invoke-interface {v0}, Lacr;->b()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 191
    invoke-interface {v0}, Lacr;->a()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_2

    .line 193
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->w:Z

    .line 194
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()V

    .line 197
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()LM;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LM;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/tools/gelly/android/GuiceDialogFragment;

    .line 198
    if-eqz v0, :cond_1

    .line 199
    invoke-virtual {v0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceDialogFragment;->a()V

    .line 225
    :cond_1
    :goto_0
    return-void

    .line 205
    :cond_2
    invoke-interface {v0}, Lacr;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v1

    .line 208
    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LaGM;

    invoke-interface {v2, v1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGu;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LaGu;

    .line 209
    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LaGu;

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(LaGu;Lcom/google/android/gms/drive/database/data/ResourceSpec;)LbmF;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LbmF;

    .line 212
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 213
    invoke-interface {v0}, Lacr;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacD;

    .line 214
    invoke-virtual {v0}, LacD;->a()Laci;

    move-result-object v0

    invoke-virtual {v0}, Laci;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 217
    :cond_3
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:[Ljava/lang/String;

    .line 219
    sget-object v0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Lqt;

    invoke-static {v0}, LabO;->a(Lqt;)LabO;

    move-result-object v0

    .line 220
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LbmF;

    invoke-virtual {v1, v0}, LbmF;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 221
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LbmF;

    invoke-virtual {v1}, LbmF;->size()I

    move-result v1

    invoke-static {v0, v1}, LbiT;->a(II)I

    .line 222
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Lacb;

    invoke-virtual {v1, v0}, Lacb;->a(I)V

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Lacb;

    invoke-virtual {v0, p1}, Lacb;->a(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 443
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 447
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 328
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->c(Landroid/os/Bundle;)V

    .line 329
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Lacb;

    invoke-virtual {v0, p1}, Lacb;->b(Landroid/os/Bundle;)V

    .line 330
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 331
    const-string v1, "contactAddresses"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 347
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->g()V

    .line 348
    iget-boolean v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->w:Z

    if-eqz v0, :cond_0

    .line 353
    :goto_0
    return-void

    .line 352
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LabI;

    invoke-virtual {v0}, LabI;->a()V

    goto :goto_0
.end method

.method public h()V
    .locals 2

    .prologue
    .line 729
    const-string v0, "AddCollaboratorTextDialogFragment"

    const-string v1, "in onDestroy"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 730
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->y()V

    .line 731
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->h()V

    .line 732
    return-void
.end method

.method public j(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 692
    invoke-static {p1}, Labw;->a(Landroid/os/Bundle;)Ljava/util/List;

    move-result-object v1

    .line 693
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Lacb;

    invoke-virtual {v0}, Lacb;->a()I

    move-result v0

    .line 694
    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LbmF;

    .line 695
    invoke-virtual {v2}, LbmF;->size()I

    move-result v2

    .line 694
    invoke-static {v0, v2}, LbiT;->a(II)I

    .line 697
    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LbmF;

    .line 698
    invoke-virtual {v2, v0}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LabO;

    .line 700
    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Lacz;

    invoke-interface {v2}, Lacz;->a()Lacr;

    move-result-object v2

    .line 701
    invoke-interface {v2}, Lacr;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v3

    .line 703
    invoke-virtual {v0}, LabO;->a()Lqt;

    move-result-object v4

    .line 705
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, LbnG;->a(I)Ljava/util/ArrayList;

    move-result-object v5

    .line 706
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 707
    new-instance v7, Lqs;

    invoke-direct {v7}, Lqs;-><init>()V

    invoke-virtual {v7, v0}, Lqs;->a(Ljava/lang/String;)Lqs;

    move-result-object v0

    .line 708
    invoke-virtual {v0, v3}, Lqs;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lqs;

    move-result-object v0

    .line 709
    invoke-virtual {v0, v4}, Lqs;->a(Lqt;)Lqs;

    move-result-object v0

    sget-object v7, Lqx;->a:Lqx;

    .line 710
    invoke-virtual {v0, v7}, Lqs;->a(Lqx;)Lqs;

    move-result-object v0

    .line 711
    invoke-virtual {v0}, Lqs;->a()Lqo;

    move-result-object v0

    .line 712
    invoke-interface {v2, v0}, Lacr;->a(Lqo;)V

    .line 713
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 716
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->d()Ljava/lang/String;

    move-result-object v2

    .line 718
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->b:LqK;

    const-string v3, "sharing"

    const-string v6, "addCollaborator"

    .line 719
    invoke-virtual {v4}, Lqt;->name()Ljava/lang/String;

    move-result-object v4

    .line 720
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    int-to-long v8, v1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 718
    invoke-virtual {v0, v3, v6, v4, v1}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 722
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->b:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Labg;

    invoke-interface {v0, v5, v2}, Labg;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 724
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()V

    .line 725
    return-void
.end method

.method public k(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 687
    return-void
.end method

.method public o_()V
    .locals 1

    .prologue
    .line 336
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->o_()V

    .line 337
    iget-boolean v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->w:Z

    if-eqz v0, :cond_0

    .line 343
    :goto_0
    return-void

    .line 341
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Lacb;

    invoke-virtual {v0}, Lacb;->a()V

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LabI;

    invoke-virtual {v0}, LabI;->d()V

    goto :goto_0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 742
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LacF;

    invoke-virtual {v0}, LacF;->a()V

    .line 743
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 744
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 748
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 749
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->c:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacj;

    invoke-virtual {v0}, Lacj;->d()V

    .line 750
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 451
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->t()V

    .line 452
    return-void
.end method
