.class public Lcom/google/android/apps/docs/app/SendTextToClipboardActivity;
.super Lrm;
.source "SendTextToClipboardActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lrm;-><init>()V

    .line 37
    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 116
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 119
    invoke-virtual {p0, v5}, Lcom/google/android/apps/docs/app/SendTextToClipboardActivity;->setResult(I)V

    .line 120
    const-string v0, "android.intent.action.SEND"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 124
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 125
    const-string v1, "android.intent.extra.SUBJECT"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 126
    if-nez v1, :cond_3

    .line 127
    const-string v1, "SendTextToClipboardActivity"

    const-string v2, "No label for clipboard data [%s, %s]"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v3, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-static {v1, v2, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 128
    sget v1, Lxi;->app_name:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/app/SendTextToClipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 131
    :goto_1
    invoke-static {}, Lvn;->a()Lvn;

    move-result-object v4

    .line 132
    const-string v1, "clipboard"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/app/SendTextToClipboardActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/text/ClipboardManager;

    .line 133
    if-eqz v3, :cond_2

    .line 134
    invoke-virtual {v4, v1, v2, v3}, Lvn;->a(Landroid/text/ClipboardManager;Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :goto_2
    sget v0, Lxi;->send_to_clipboard_success:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/SendTextToClipboardActivity;->a(I)V

    .line 141
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/SendTextToClipboardActivity;->setResult(I)V

    goto :goto_0

    .line 135
    :cond_2
    if-eqz v0, :cond_0

    .line 136
    invoke-virtual {v4, v1, v2, p0, v0}, Lvn;->a(Landroid/text/ClipboardManager;Ljava/lang/String;Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_2

    :cond_3
    move-object v2, v1

    goto :goto_1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 108
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 109
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/SendTextToClipboardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 110
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/SendTextToClipboardActivity;->a(Landroid/content/Intent;)V

    .line 111
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/SendTextToClipboardActivity;->finish()V

    .line 112
    return-void
.end method
