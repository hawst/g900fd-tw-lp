.class public Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;
.super Lrm;
.source "DocumentOpenerActivityDelegate.java"

# interfaces
.implements LET;
.implements LFT;


# static fields
.field public static final a:Landroid/net/Uri;


# instance fields
.field public a:LCo;

.field private a:LDL;

.field public a:LFX;

.field public a:LUM;

.field public a:LUi;

.field public a:LaGM;

.field public a:LaGR;

.field public a:LaKR;

.field public a:LadL;

.field public a:Ladi;

.field public a:LagZ;

.field public a:LamL;

.field private a:Lcom/google/android/apps/docs/utils/ParcelableTask;

.field private a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private final a:Ljava/util/concurrent/Executor;

.field public a:LtB;

.field public a:LvL;

.field private final b:Landroid/os/Handler;

.field private i:Z

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86
    const-string v0, "content://com.google.android.apps.docs/open"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 72
    invoke-direct {p0}, Lrm;-><init>()V

    .line 130
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LDL;

    .line 140
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->b:Landroid/os/Handler;

    .line 141
    new-instance v0, LalI;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->b:Landroid/os/Handler;

    invoke-direct {v0, v1}, LalI;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;)LDL;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LDL;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;LDL;)LDL;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LDL;

    return-object p1
.end method

.method private a(LaGo;Landroid/os/Bundle;)LbsU;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGo;",
            "Landroid/os/Bundle;",
            ")",
            "LbsU",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 357
    invoke-static {}, Lbtd;->a()Lbtd;

    move-result-object v0

    .line 359
    new-instance v1, LsT;

    invoke-direct {v1, p0, p2, p1, v0}, LsT;-><init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;Landroid/os/Bundle;LaGo;Lbtd;)V

    .line 409
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 411
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;Lcom/google/android/apps/docs/utils/ParcelableTask;)Lcom/google/android/apps/docs/utils/ParcelableTask;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:Lcom/google/android/apps/docs/utils/ParcelableTask;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;)Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;)LqK;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LqK;

    return-object v0
.end method

.method private a(LFV;)V
    .locals 1

    .prologue
    .line 582
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(LFV;Ljava/lang/Throwable;)V

    .line 583
    return-void
.end method

.method private a(LaFV;)V
    .locals 3

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LUi;

    invoke-interface {p1}, LaFV;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-interface {v0, v1}, LUi;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 337
    const-string v0, "root"

    invoke-interface {p1}, LaFV;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LCo;

    sget-object v2, LCn;->a:LCn;

    .line 339
    invoke-interface {v1, v2}, LCo;->a(LCn;)LCl;

    move-result-object v1

    .line 338
    invoke-static {p0, v0, v1}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;LaFO;LCl;)Landroid/content/Intent;

    move-result-object v0

    .line 345
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->startActivity(Landroid/content/Intent;)V

    .line 346
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->finish()V

    .line 347
    return-void

    .line 342
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    const/4 v1, 0x0

    invoke-static {p0, v0, p1, v1}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;LaFO;LaFV;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method private a(LaGo;)V
    .locals 2

    .prologue
    .line 269
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LUi;

    invoke-interface {p1}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-interface {v0, v1}, LUi;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 271
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(Landroid/content/Context;LaGu;)Landroid/content/Intent;

    move-result-object v0

    .line 272
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->startActivity(Landroid/content/Intent;)V

    .line 273
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->finish()V

    .line 274
    return-void
.end method

.method private a(Landroid/content/Intent;LaGo;)V
    .locals 3

    .prologue
    .line 288
    invoke-static {p2}, Lala;->a(LaGu;)Ljava/lang/String;

    move-result-object v0

    .line 290
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, p2, v1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(LaGo;Landroid/os/Bundle;)LbsU;

    move-result-object v1

    .line 291
    new-instance v2, LsS;

    invoke-direct {v2, p0, v0, p2}, LsS;-><init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;Ljava/lang/String;LaGo;)V

    .line 330
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LbsK;->a(LbsU;LbsJ;Ljava/util/concurrent/Executor;)V

    .line 331
    return-void
.end method

.method private a(Landroid/content/Intent;LaGu;Z)V
    .locals 2

    .prologue
    .line 251
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    instance-of v0, p2, LaFV;

    if-eqz v0, :cond_0

    .line 254
    check-cast p2, LaFV;

    invoke-direct {p0, p2}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(LaFV;)V

    .line 266
    :goto_0
    return-void

    .line 256
    :cond_0
    check-cast p2, LaGo;

    .line 257
    invoke-interface {p2}, LaGo;->a()LaGn;

    move-result-object v0

    .line 258
    if-eqz p3, :cond_1

    sget-object v1, LaGn;->b:LaGn;

    invoke-virtual {v1, v0}, LaGn;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 259
    invoke-direct {p0, p2}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(LaGo;)V

    goto :goto_0

    .line 260
    :cond_1
    invoke-interface {p2}, LaGo;->a()LaGv;

    move-result-object v0

    sget-object v1, LaGv;->e:LaGv;

    if-ne v0, v1, :cond_2

    .line 261
    invoke-direct {p0, p2}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->b(LaGo;)V

    goto :goto_0

    .line 263
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(Landroid/content/Intent;LaGo;)V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->j()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;LFV;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(LFV;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;LaGo;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->c(LaGo;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->c(Landroid/content/Intent;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;Landroid/content/Intent;LaGu;Z)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(Landroid/content/Intent;LaGu;Z)V

    return-void
.end method

.method private a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1

    .prologue
    .line 484
    new-instance v0, LsW;

    invoke-direct {v0, p0, p1}, LsW;-><init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 526
    invoke-virtual {v0}, LpD;->start()V

    .line 527
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;Z)Z
    .locals 0

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->i:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;)LqK;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LqK;

    return-object v0
.end method

.method private b(LaGo;)V
    .locals 2

    .prologue
    .line 277
    const-string v0, "DocumentOpenerActivityDelegate"

    const-string v1, "Redirect to browser."

    invoke-static {v0, v1}, LalV;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LUM;

    invoke-interface {p1}, LaGo;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LUM;->a(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 279
    if-eqz v0, :cond_0

    .line 280
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->startActivity(Landroid/content/Intent;)V

    .line 284
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->finish()V

    .line 285
    return-void

    .line 282
    :cond_0
    const-string v0, "DocumentOpenerActivityDelegate"

    const-string v1, "Couldn\'t find default browser."

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private b(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 227
    const-string v0, "android.intent.action.VIEW"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 228
    const-string v0, "DocumentOpenerActivityDelegate"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid action: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->finish()V

    .line 248
    :goto_0
    return-void

    .line 233
    :cond_0
    const-string v0, "entrySpec.v2"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-nez v0, :cond_1

    .line 235
    const-string v0, "DocumentOpenerActivityDelegate"

    const-string v1, "Entry spec not provided"

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->finish()V

    goto :goto_0

    .line 240
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LaGR;

    new-instance v1, LsR;

    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-direct {v1, p0, v2, p1}, LsR;-><init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;Lcom/google/android/gms/drive/database/data/EntrySpec;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, LaGR;->a(LaGN;)V

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->k()V

    return-void
.end method

.method private c(LaGo;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 456
    if-eqz p1, :cond_2

    invoke-interface {p1}, LaGo;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LtB;

    invoke-interface {v0}, LtB;->a()LaGv;

    move-result-object v0

    .line 458
    invoke-interface {p1}, LaGo;->a()LaGv;

    move-result-object v3

    .line 459
    if-eqz v0, :cond_3

    .line 460
    invoke-virtual {v0, v3}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 461
    :goto_0
    sget-object v4, LaGv;->i:LaGv;

    invoke-virtual {v4, v3}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 462
    invoke-static {p1}, LUN;->a(LaGo;)Z

    move-result v3

    if-nez v3, :cond_4

    move v3, v1

    .line 463
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LtB;

    invoke-interface {v4}, LtB;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    move v2, v1

    .line 467
    :cond_1
    if-eqz v0, :cond_5

    if-eqz v2, :cond_5

    .line 468
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 473
    :cond_2
    :goto_2
    return-void

    :cond_3
    move v0, v2

    .line 460
    goto :goto_0

    :cond_4
    move v3, v2

    .line 462
    goto :goto_1

    .line 470
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LagZ;

    invoke-interface {v0, p1}, LagZ;->b(LaGo;)V

    goto :goto_2
.end method

.method private c(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 531
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 532
    if-eqz v0, :cond_0

    .line 533
    const-string v1, "uri"

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 535
    :cond_0
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 416
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 418
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LaGR;

    new-instance v1, LsV;

    invoke-direct {v1, p0}, LsV;-><init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;)V

    invoke-virtual {v0, v1}, LaGR;->b(LaGN;)V

    .line 448
    return-void
.end method

.method private k()V
    .locals 1

    .prologue
    .line 575
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:Lcom/google/android/apps/docs/utils/ParcelableTask;

    if-eqz v0, :cond_0

    .line 576
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:Lcom/google/android/apps/docs/utils/ParcelableTask;

    invoke-interface {v0, p0}, Lcom/google/android/apps/docs/utils/ParcelableTask;->b(Laju;)V

    .line 577
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:Lcom/google/android/apps/docs/utils/ParcelableTask;

    .line 579
    :cond_0
    return-void
.end method


# virtual methods
.method public a(LFV;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 587
    const-string v0, "DocumentOpenerActivityDelegate"

    const-string v1, "Error occured %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 588
    invoke-virtual {p1}, LFV;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 615
    :goto_0
    return-void

    .line 591
    :cond_0
    invoke-virtual {p1}, LFV;->a()I

    move-result v0

    .line 592
    sget v1, Lxi;->error_page_title:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 593
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 596
    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->b:Landroid/os/Handler;

    new-instance v3, LsZ;

    invoke-direct {v3, p0, v1, v0, p1}, LsZ;-><init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;Ljava/lang/String;Ljava/lang/String;LFV;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 539
    const-string v0, "DocumentOpenerActivityDelegate"

    const-string v1, "in startIntent"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 541
    new-instance v0, LsX;

    invoke-direct {v0, p0, p1}, LsX;-><init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;Landroid/content/Intent;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 549
    return-void
.end method

.method public a(Landroid/content/Intent;Lcom/google/android/apps/docs/utils/ParcelableTask;)V
    .locals 2

    .prologue
    .line 554
    const-string v0, "DocumentOpenerActivityDelegate"

    const-string v1, "in startIntent with Runnable"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    new-instance v0, LsY;

    invoke-direct {v0, p0, p2, p1}, LsY;-><init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;Lcom/google/android/apps/docs/utils/ParcelableTask;Landroid/content/Intent;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 572
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 168
    invoke-super {p0, p1}, Lrm;->b(Z)V

    .line 169
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->j:Z

    if-eqz v0, :cond_0

    .line 170
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->b(Landroid/content/Intent;)V

    .line 172
    :cond_0
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 351
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LDL;

    .line 352
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 353
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->b(Landroid/content/Intent;)V

    .line 354
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 147
    const-string v2, "DocumentOpenerActivityDelegate"

    const-string v3, "Opening %s"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 148
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 149
    if-nez p1, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->j:Z

    .line 150
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->j:Z

    if-nez v0, :cond_3

    .line 151
    const-string v0, "cleanupTask"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/utils/ParcelableTask;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:Lcom/google/android/apps/docs/utils/ParcelableTask;

    .line 152
    const-string v0, "IsViewerStarted"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->i:Z

    .line 153
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->i:Z

    if-nez v0, :cond_0

    .line 154
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a()LM;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a(LM;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 155
    :cond_0
    const-string v0, "entrySpec.v2"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 164
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 149
    goto :goto_0

    .line 157
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->finish()V

    goto :goto_1

    .line 160
    :cond_3
    iput-boolean v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->i:Z

    .line 161
    iput-object v6, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:Lcom/google/android/apps/docs/utils/ParcelableTask;

    .line 162
    iput-object v6, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 213
    const-string v0, "DocumentOpenerActivityDelegate"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LadL;

    invoke-interface {v0}, LadL;->b()V

    .line 215
    invoke-super {p0}, Lrm;->onDestroy()V

    .line 216
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 220
    invoke-super {p0, p1}, Lrm;->onNewIntent(Landroid/content/Intent;)V

    .line 221
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->setIntent(Landroid/content/Intent;)V

    .line 222
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->b(Landroid/content/Intent;)V

    .line 223
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 185
    const-string v0, "DocumentOpenerActivityDelegate"

    const-string v1, "onResume"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    invoke-super {p0}, Lrm;->onResume()V

    .line 188
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->i:Z

    if-eqz v0, :cond_1

    .line 189
    const-string v0, "android.intent.action.VIEW"

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:LaGR;

    new-instance v1, LsQ;

    invoke-direct {v1, p0}, LsQ;-><init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;)V

    invoke-virtual {v0, v1}, LaGR;->b(LaGN;)V

    .line 204
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->k()V

    .line 205
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->finishActivity(I)V

    .line 206
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->finish()V

    .line 209
    :cond_1
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 176
    invoke-super {p0, p1}, Lrm;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 178
    const-string v0, "IsViewerStarted"

    iget-boolean v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->i:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 179
    const-string v0, "entrySpec.v2"

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 180
    const-string v0, "cleanupTask"

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;->a:Lcom/google/android/apps/docs/utils/ParcelableTask;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 181
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 452
    const/4 v0, 0x0

    return v0
.end method
