.class public enum Lcom/google/android/apps/docs/app/DocumentOpenMethod;
.super Ljava/lang/Enum;
.source "DocumentOpenMethod.java"


# annotations
.annotation build Lcom/google/android/apps/docs/neocommon/proguard/KeepAfterProguard;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/docs/app/DocumentOpenMethod;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

.field private static final synthetic a:[Lcom/google/android/apps/docs/app/DocumentOpenMethod;

.field public static final enum b:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

.field public static final enum c:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

.field public static final enum d:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

.field public static final enum e:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

.field public static final enum f:Lcom/google/android/apps/docs/app/DocumentOpenMethod;


# instance fields
.field public final a:I

.field private final a:LacY;

.field protected final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 36
    new-instance v0, LsK;

    const-string v1, "OPEN"

    sget-object v3, LacY;->a:LacY;

    const-string v4, "android.intent.action.VIEW"

    sget v5, Lxi;->document_preparing_to_open_progress:I

    invoke-direct/range {v0 .. v5}, LsK;-><init>(Ljava/lang/String;ILacY;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    .line 47
    new-instance v3, Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    const-string v4, "OPEN_WITH"

    sget-object v6, LacY;->b:LacY;

    const-string v7, "android.intent.action.VIEW"

    sget v8, Lxi;->document_preparing_to_open_progress:I

    move v5, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;-><init>(Ljava/lang/String;ILacY;Ljava/lang/String;I)V

    sput-object v3, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->b:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    .line 48
    new-instance v3, Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    const-string v4, "GET_CONTENT"

    sget-object v6, LacY;->b:LacY;

    const-string v7, "android.intent.action.GET_CONTENT"

    sget v8, Lxi;->download_progress_dialog_message:I

    move v5, v10

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;-><init>(Ljava/lang/String;ILacY;Ljava/lang/String;I)V

    sput-object v3, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->c:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    .line 50
    new-instance v3, LsL;

    const-string v4, "SEND"

    sget-object v6, LacY;->b:LacY;

    const-string v7, "android.intent.action.SEND"

    sget v8, Lxi;->document_preparing_to_send_progress:I

    move v5, v11

    invoke-direct/range {v3 .. v8}, LsL;-><init>(Ljava/lang/String;ILacY;Ljava/lang/String;I)V

    sput-object v3, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->d:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    .line 73
    new-instance v3, LsM;

    const-string v4, "DOWNLOAD"

    sget-object v6, LacY;->b:LacY;

    const-string v7, "android.intent.action.VIEW"

    sget v8, Lxi;->download_progress_dialog_message:I

    move v5, v12

    invoke-direct/range {v3 .. v8}, LsM;-><init>(Ljava/lang/String;ILacY;Ljava/lang/String;I)V

    sput-object v3, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->e:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    .line 82
    new-instance v3, LsN;

    const-string v4, "PRINT"

    const/4 v5, 0x5

    sget-object v6, LacY;->b:LacY;

    const-string v7, "android.intent.action.VIEW"

    sget v8, Lxi;->download_progress_dialog_message:I

    invoke-direct/range {v3 .. v8}, LsN;-><init>(Ljava/lang/String;ILacY;Ljava/lang/String;I)V

    sput-object v3, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->f:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    .line 34
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    sget-object v1, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->b:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->c:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->d:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    aput-object v1, v0, v11

    sget-object v1, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->e:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->f:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a:[Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILacY;Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LacY;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 101
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 102
    iput-object p3, p0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a:LacY;

    .line 103
    iput-object p4, p0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a:Ljava/lang/String;

    .line 104
    iput p5, p0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a:I

    .line 105
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;ILacY;Ljava/lang/String;ILsK;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;-><init>(Ljava/lang/String;ILacY;Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 5

    .prologue
    .line 160
    const-string v0, "file"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 161
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 162
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    .line 165
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object v0, v1

    .line 166
    :goto_0
    if-eqz v0, :cond_0

    .line 168
    const-string v4, "../"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    goto :goto_0

    .line 171
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object p0

    .line 173
    :cond_1
    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/docs/app/DocumentOpenMethod;
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/docs/app/DocumentOpenMethod;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a:[Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    invoke-virtual {v0}, [Lcom/google/android/apps/docs/app/DocumentOpenMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    return-object v0
.end method


# virtual methods
.method public final a(LaGv;)LacY;
    .locals 1

    .prologue
    .line 131
    invoke-virtual {p1}, LaGv;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a:LacY;

    .line 134
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LacY;->a:LacY;

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 121
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 124
    invoke-virtual {v0, p3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 125
    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a(Landroid/content/Intent;Landroid/net/Uri;)V

    .line 126
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 127
    return-object v0
.end method

.method public final a(LaGu;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    .line 145
    invoke-interface {p1}, LaGu;->a()LaGv;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a(LaGv;)LacY;

    move-result-object v0

    .line 146
    invoke-virtual {v0, p1}, LacY;->a(LaGu;)Ljava/lang/String;

    move-result-object v0

    .line 148
    return-object v0
.end method

.method public a(Landroid/content/Intent;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 116
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 117
    return-void
.end method
