.class public Lcom/google/android/apps/docs/utils/ToastErrorReporter;
.super Ljava/lang/Object;
.source "ToastErrorReporter.java"

# interfaces
.implements Lald;


# instance fields
.field private final a:Landroid/content/Context;

.field private final a:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Lanf;

    invoke-direct {v0, p0}, Lanf;-><init>(Lcom/google/android/apps/docs/utils/ToastErrorReporter;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/utils/ToastErrorReporter;->a:Landroid/os/Handler;

    .line 95
    iput-object p1, p0, Lcom/google/android/apps/docs/utils/ToastErrorReporter;->a:Landroid/content/Context;

    .line 96
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/utils/ToastErrorReporter;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/docs/utils/ToastErrorReporter;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/docs/utils/ToastErrorReporter;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/docs/utils/ToastErrorReporter;->a:Landroid/os/Handler;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 126
    return-void
.end method

.method public a(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 113
    new-instance v0, Lcom/google/android/apps/docs/utils/ToastErrorReporter$ErrorRetriever;

    iget-object v1, p0, Lcom/google/android/apps/docs/utils/ToastErrorReporter;->a:Landroid/content/Context;

    sget v2, Lxi;->error_page_title:I

    .line 114
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, p2, v1}, Lcom/google/android/apps/docs/utils/ToastErrorReporter$ErrorRetriever;-><init>(Lcom/google/android/apps/docs/utils/ToastErrorReporter;Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 116
    const-string v1, "errorRetriever"

    invoke-virtual {p1, v0, v1}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/docs/utils/ToastErrorReporter;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxh;->error_page:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 119
    const-string v1, "file:///android_res/raw/"

    invoke-static {v1, v0, p1}, LamN;->a(Ljava/lang/String;Ljava/io/InputStream;Landroid/webkit/WebView;)V

    .line 120
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/docs/utils/ToastErrorReporter;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/docs/utils/ToastErrorReporter;->a:Landroid/os/Handler;

    const/4 v2, 0x0

    new-instance v3, Lang;

    const/16 v4, 0x51

    invoke-direct {v3, p1, v4}, Lang;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 103
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/docs/utils/ToastErrorReporter;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/docs/utils/ToastErrorReporter;->a:Landroid/os/Handler;

    const/4 v2, 0x0

    new-instance v3, Lang;

    const/16 v4, 0x11

    invoke-direct {v3, p1, v4}, Lang;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 109
    return-void
.end method
