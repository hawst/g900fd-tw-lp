.class public Lcom/google/android/apps/docs/doclist/dialogs/SelectNewDocTypeDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "SelectNewDocTypeDialogFragment.java"


# instance fields
.field public a:LFB;

.field public a:LFC;

.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LCN;",
            ">;"
        }
    .end annotation
.end field

.field public a:LtK;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    .line 60
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/SelectNewDocTypeDialogFragment;->a()LH;

    move-result-object v2

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/SelectNewDocTypeDialogFragment;->a:LFC;

    invoke-interface {v0}, LFC;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/SelectNewDocTypeDialogFragment;->a:Ljava/util/List;

    .line 97
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/SelectNewDocTypeDialogFragment;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCN;

    .line 99
    invoke-interface {v0, v2}, LCN;->a(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 100
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 104
    :cond_1
    invoke-static {v2}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v7

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/SelectNewDocTypeDialogFragment;->a:LFC;

    invoke-interface {v0}, LFC;->a()I

    move-result v0

    invoke-virtual {v7, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 106
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 108
    const-string v0, "layout_inflater"

    .line 109
    invoke-virtual {v2, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    .line 110
    sget v0, Lxe;->create_entry_dialog_list:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/ListView;

    .line 112
    new-instance v0, LFy;

    sget v3, Lxe;->create_entry_dialog_row:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LFy;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/SelectNewDocTypeDialogFragment;Landroid/content/Context;ILjava/util/List;Landroid/view/LayoutInflater;)V

    .line 125
    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 127
    invoke-virtual {v7, v6}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 128
    const/high16 v0, 0x1040000

    new-instance v1, LFz;

    invoke-direct {v1, p0}, LFz;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/SelectNewDocTypeDialogFragment;)V

    invoke-virtual {v7, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 134
    sget-object v0, LEN;->a:LEN;

    invoke-virtual {v7, v0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 135
    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 137
    new-instance v1, LFA;

    invoke-direct {v1, p0, v4, v0}, LFA;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/SelectNewDocTypeDialogFragment;Ljava/util/List;Landroid/app/AlertDialog;)V

    invoke-virtual {v6, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 147
    return-object v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/SelectNewDocTypeDialogFragment;->a:LFB;

    invoke-interface {v0}, LFB;->j()V

    .line 153
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 154
    return-void
.end method
