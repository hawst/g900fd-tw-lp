.class public Lcom/google/android/apps/docs/tools/gelly/android/ActivitySessionSupportFragmentImpl;
.super Landroid/support/v4/app/Fragment;
.source "ActivitySessionSupportFragmentImpl.java"

# interfaces
.implements LaiD;


# instance fields
.field private a:LaiE;

.field private final a:LajA;

.field private a:Ljava/util/UUID;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 19
    new-instance v0, LajA;

    invoke-direct {v0}, LajA;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/tools/gelly/android/ActivitySessionSupportFragmentImpl;->a:LajA;

    return-void
.end method


# virtual methods
.method public a()LajA;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/docs/tools/gelly/android/ActivitySessionSupportFragmentImpl;->a:LajA;

    return-object v0
.end method

.method public a()Ljava/util/UUID;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/docs/tools/gelly/android/ActivitySessionSupportFragmentImpl;->a:Ljava/util/UUID;

    return-object v0
.end method

.method public a(LaiE;)V
    .locals 1

    .prologue
    .line 29
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaiE;

    iput-object v0, p0, Lcom/google/android/apps/docs/tools/gelly/android/ActivitySessionSupportFragmentImpl;->a:LaiE;

    .line 30
    return-void
.end method

.method public a(Ljava/util/UUID;)V
    .locals 1

    .prologue
    .line 47
    invoke-static {p1}, Lbso;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/UUID;

    iput-object v0, p0, Lcom/google/android/apps/docs/tools/gelly/android/ActivitySessionSupportFragmentImpl;->a:Ljava/util/UUID;

    .line 48
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/docs/tools/gelly/android/ActivitySessionSupportFragmentImpl;->a:LaiE;

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/google/android/apps/docs/tools/gelly/android/ActivitySessionSupportFragmentImpl;->a:LaiE;

    invoke-interface {v0, p0}, LaiE;->a(LaiD;)V

    .line 37
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->h()V

    .line 38
    return-void
.end method
