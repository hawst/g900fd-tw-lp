.class public Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl$UriIntentBuilderImpl;
.super Ljava/lang/Object;
.source "FileOpenerIntentCreatorImpl.java"

# interfaces
.implements Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator$UriIntentBuilder;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl$UriIntentBuilderImpl;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Landroid/content/Intent;

.field private final a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 154
    new-instance v0, LGs;

    invoke-direct {v0}, LGs;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl$UriIntentBuilderImpl;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    if-nez p1, :cond_0

    .line 144
    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl$UriIntentBuilderImpl;->a:Landroid/content/Intent;

    .line 145
    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl$UriIntentBuilderImpl;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    .line 150
    :goto_0
    return-void

    .line 147
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl$UriIntentBuilderImpl;->a:Landroid/content/Intent;

    .line 148
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl$UriIntentBuilderImpl;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/net/Uri;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl$UriIntentBuilderImpl;->a:Landroid/content/Intent;

    if-nez v0, :cond_0

    .line 183
    const/4 v0, 0x0

    .line 187
    :goto_0
    return-object v0

    .line 185
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl$UriIntentBuilderImpl;->a:Landroid/content/Intent;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 186
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl$UriIntentBuilderImpl;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a(Landroid/content/Intent;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 171
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl$UriIntentBuilderImpl;->a:Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl$UriIntentBuilderImpl;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 178
    return-void
.end method
