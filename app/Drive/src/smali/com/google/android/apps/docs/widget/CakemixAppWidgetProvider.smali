.class public Lcom/google/android/apps/docs/widget/CakemixAppWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "CakemixAppWidgetProvider.java"


# instance fields
.field public a:LatR;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleted(Landroid/content/Context;[I)V
    .locals 4

    .prologue
    .line 53
    new-instance v1, LatQ;

    invoke-direct {v1, p1}, LatQ;-><init>(Landroid/content/Context;)V

    .line 54
    array-length v2, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, p2, v0

    .line 55
    invoke-virtual {v1, v3}, LatQ;->a(I)V

    .line 54
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 57
    :cond_0
    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 35
    invoke-static {p1}, LaiZ;->b(Landroid/content/Context;)LaiY;

    move-result-object v0

    .line 36
    invoke-interface {v0, p0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 38
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 39
    return-void
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 43
    new-instance v1, LatQ;

    invoke-direct {v1, p1}, LatQ;-><init>(Landroid/content/Context;)V

    .line 44
    const-string v2, "CakemixAppWidgetProvider"

    const-string v3, "onUpdate: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p3}, Lbsy;->a([I)Ljava/util/List;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 45
    array-length v2, p3

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, p3, v0

    .line 46
    invoke-virtual {v1, v3}, LatQ;->a(I)LaFO;

    move-result-object v4

    .line 47
    iget-object v5, p0, Lcom/google/android/apps/docs/widget/CakemixAppWidgetProvider;->a:LatR;

    invoke-virtual {v5, p2, v3, p1, v4}, LatR;->a(Landroid/appwidget/AppWidgetManager;ILandroid/content/Context;LaFO;)V

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 49
    :cond_0
    return-void
.end method
