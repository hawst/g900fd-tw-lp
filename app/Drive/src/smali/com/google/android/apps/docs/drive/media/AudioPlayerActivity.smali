.class public Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;
.super Lrm;
.source "AudioPlayerActivity.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field public a:LGa;

.field private a:LNB;

.field private a:LNE;

.field public a:LPp;

.field public a:LTj;

.field public a:LaGM;

.field public a:LaGR;

.field public a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "LuX;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/drive/media/AudioController;

.field private a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

.field public a:LtK;

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lrm;-><init>()V

    .line 118
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;)LNB;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:LNB;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/ResourceSpec;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 137
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 140
    const-string v1, "resourceSpec"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 141
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;)Lcom/google/android/apps/docs/drive/media/AudioController;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:Lcom/google/android/apps/docs/drive/media/AudioController;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;)Lcom/google/android/gms/drive/database/data/ResourceSpec;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:LaGR;

    new-instance v1, LNC;

    invoke-direct {v1, p0, p1}, LNC;-><init>(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;Lcom/google/android/gms/drive/database/data/ResourceSpec;)V

    invoke-virtual {v0, v1}, LaGR;->b(LaGN;)V

    .line 188
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->i:Z

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;Lcom/google/android/gms/drive/database/data/ResourceSpec;)Z
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Z

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;Z)Z
    .locals 0

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->i:Z

    return p1
.end method

.method private a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Z
    .locals 4

    .prologue
    .line 207
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:LGa;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LGa;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;LFS;)Ljava/lang/String;

    move-result-object v0

    .line 208
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 210
    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:LTj;

    iget-object v2, p1, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    .line 211
    invoke-virtual {v1, v2, v0}, LTj;->a(LaFO;Landroid/net/Uri;)Ljava/util/Map;

    move-result-object v1

    .line 212
    iget-object v2, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:LNB;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, LNB;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 213
    :catch_0
    move-exception v0

    .line 214
    const-string v1, "AudioPlayerActivity"

    const-string v2, "Failed to open file for playback "

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 215
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)V
    .locals 3

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:LNE;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/drive/database/data/ResourceSpec;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, LNE;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 201
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 191
    new-instance v0, LNB;

    invoke-direct {v0}, LNB;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:LNB;

    .line 192
    new-instance v0, Lcom/google/android/apps/docs/drive/media/AudioController;

    invoke-direct {v0, p0}, Lcom/google/android/apps/docs/drive/media/AudioController;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:Lcom/google/android/apps/docs/drive/media/AudioController;

    .line 193
    new-instance v0, LND;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LND;-><init>(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;LNC;)V

    .line 194
    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:LNB;

    invoke-virtual {v1, v0}, LNB;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 195
    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:LNB;

    invoke-virtual {v1, v0}, LNB;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 196
    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:LNB;

    invoke-virtual {v1, v0}, LNB;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 197
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:LaGM;

    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-interface {v0, v1}, LaGM;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LvC;->a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->startActivity(Landroid/content/Intent;)V

    .line 262
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 146
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 148
    sget v0, LNL;->audio_player:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->setContentView(I)V

    .line 149
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a()LaqY;

    move-result-object v0

    .line 150
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, LNK;->projector_actionbar_opacity:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 149
    invoke-interface {v0, v1}, LaqY;->b(I)V

    .line 152
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "resourceSpec"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/ResourceSpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    .line 153
    new-instance v0, LNE;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LNE;-><init>(Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;LNC;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:LNE;

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    if-nez v0, :cond_0

    .line 156
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->finish()V

    .line 159
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->f()V

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)V

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)V

    .line 162
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 231
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 232
    sget v1, LNM;->menu_audio_player:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 234
    sget v0, LNJ;->share_file:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LuX;

    invoke-virtual {v0, v1}, LuX;->a(Landroid/view/MenuItem;)V

    .line 236
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 252
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, LNJ;->open_detail_panel:I

    if-ne v0, v1, :cond_0

    .line 253
    invoke-direct {p0}, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->j()V

    .line 254
    const/4 v0, 0x1

    .line 257
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lrm;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:LtK;

    sget-object v1, Lry;->r:Lry;

    .line 242
    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    .line 243
    sget v1, LNJ;->open_detail_panel:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 244
    if-eqz v1, :cond_0

    .line 245
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 247
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 166
    invoke-super {p0}, Lrm;->onResume()V

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:LPp;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LPp;->a(Z)V

    .line 168
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 223
    invoke-super {p0}, Lrm;->onStop()V

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:Lcom/google/android/apps/docs/drive/media/AudioController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/drive/media/AudioController;->setShowing(Z)V

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:LNB;

    invoke-virtual {v0}, LNB;->stop()V

    .line 226
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/AudioPlayerActivity;->a:LNB;

    invoke-virtual {v0}, LNB;->release()V

    .line 227
    return-void
.end method
