.class public Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;
.super Lcom/google/android/apps/docs/doclist/documentopener/AbstractImmediateDocumentOpener;
.source "DownloadFileDocumentOpenerImpl.java"

# interfaces
.implements Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;


# static fields
.field private static final a:I


# instance fields
.field private final a:LFR;

.field private final a:LGb;

.field private final a:LaGM;

.field private final a:Ladb;

.field private final a:LagG;

.field private final a:LqK;

.field private final a:Lrm;

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    sget v0, Lxi;->opening_document:I

    sput v0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:I

    return-void
.end method

.method public constructor <init>(Lrm;LagG;LaGM;LqK;LFR;Ladb;LGa;)V
    .locals 9
    .param p5    # LFR;
        .annotation runtime Lbxv;
            a = "DefaultLocal"
        .end annotation
    .end param

    .prologue
    .line 209
    sget v8, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:I

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;-><init>(Lrm;LagG;LaGM;LqK;LFR;Ladb;LGb;I)V

    .line 217
    return-void
.end method

.method private constructor <init>(Lrm;LagG;LaGM;LqK;LFR;Ladb;LGb;I)V
    .locals 0
    .param p5    # LFR;
        .annotation runtime Lbxv;
            a = "DefaultLocal"
        .end annotation
    .end param

    .prologue
    .line 226
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/documentopener/AbstractImmediateDocumentOpener;-><init>()V

    .line 227
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:Lrm;

    .line 228
    iput-object p2, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:LagG;

    .line 229
    iput-object p3, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:LaGM;

    .line 230
    iput-object p4, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:LqK;

    .line 231
    iput-object p5, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:LFR;

    .line 232
    iput-object p6, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:Ladb;

    .line 233
    iput-object p7, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:LGb;

    .line 234
    iput p8, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->b:I

    .line 235
    return-void
.end method

.method public synthetic constructor <init>(Lrm;LagG;LaGM;LqK;LFR;Ladb;LGb;ILFZ;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct/range {p0 .. p8}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;-><init>(Lrm;LagG;LaGM;LqK;LFR;Ladb;LGb;I)V

    return-void
.end method

.method public static synthetic a()I
    .locals 1

    .prologue
    .line 45
    sget v0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:I

    return v0
.end method

.method private a(LFT;Lcom/google/android/gms/drive/database/data/EntrySpec;Landroid/os/Bundle;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 393
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:LaGM;

    invoke-interface {v2, p2}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v2

    .line 394
    if-nez v2, :cond_1

    .line 395
    const-string v1, "DownloadFileDocumentOpener"

    const-string v2, "OpenLocalFile failed: document no longer exists"

    invoke-static {v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    :cond_0
    :goto_0
    return v0

    .line 399
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:LFR;

    .line 400
    invoke-static {v3, p1, v2, p3}, LFY;->a(LFR;LFT;LaGo;Landroid/os/Bundle;)LDL;

    move-result-object v3

    .line 404
    if-nez v3, :cond_2

    move v0, v1

    .line 405
    goto :goto_0

    .line 408
    :cond_2
    invoke-interface {v3}, LDL;->a()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 410
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:Lrm;

    new-instance v4, LFZ;

    invoke-direct {v4, p0, v3, v2}, LFZ;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;LDL;LaGo;)V

    invoke-virtual {v1, v4}, Lrm;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 422
    :cond_3
    :goto_1
    if-ltz v1, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v2

    if-nez v2, :cond_0

    .line 423
    invoke-interface {v3, v1}, LDL;->a(I)I

    move-result v1

    goto :goto_1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;LFT;Lcom/google/android/gms/drive/database/data/EntrySpec;Landroid/os/Bundle;)I
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a(LFT;Lcom/google/android/gms/drive/database/data/EntrySpec;Landroid/os/Bundle;)I

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)LGb;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:LGb;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)LaGM;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:LaGM;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)Ladb;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:Ladb;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)LagG;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:LagG;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)LqK;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:LqK;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;)Lrm;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:Lrm;

    return-object v0
.end method


# virtual methods
.method public final a(LFT;LaGo;Landroid/os/Bundle;)LDL;
    .locals 1

    .prologue
    .line 381
    invoke-interface {p2}, LaGo;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-interface {p2}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 382
    new-instance v0, LGe;

    invoke-direct {v0, p0, p1, p2, p3}, LGe;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;LFT;LaGo;Landroid/os/Bundle;)V

    return-object v0

    .line 381
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 387
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:Lrm;

    invoke-virtual {v0}, Lrm;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->b:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(LaGo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 434
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;->a:LGb;

    invoke-interface {v0, p1}, LGb;->a(LaGo;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
