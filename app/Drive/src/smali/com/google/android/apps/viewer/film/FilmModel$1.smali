.class public final Lcom/google/android/apps/viewer/film/FilmModel$1;
.super Ljava/lang/Object;
.source "FilmModel.java"

# interfaces
.implements Lcom/google/android/apps/viewer/client/Projector$FileInfoSource;


# instance fields
.field final synthetic a:[Landroid/os/Parcelable;


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x0

    return v0
.end method

.method public getFileInfo(ILauO;)V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmModel$1;->a:[Landroid/os/Parcelable;

    aget-object v0, v0, p1

    check-cast v0, Landroid/os/Bundle;

    invoke-static {v0}, LauM;->a(Landroid/os/Bundle;)LauM;

    move-result-object v0

    .line 124
    invoke-interface {p2, v0}, LauO;->a(LauM;)V

    .line 125
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 135
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "FilmModel source cannot be parcelled."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
