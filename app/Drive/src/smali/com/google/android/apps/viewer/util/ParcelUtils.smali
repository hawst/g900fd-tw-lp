.class public Lcom/google/android/apps/viewer/util/ParcelUtils;
.super Ljava/lang/Object;
.source "ParcelUtils.java"


# direct methods
.method public static a()V
    .locals 1

    .prologue
    .line 74
    const-string v0, "parcel_utils"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 75
    return-void
.end method

.method public static a(Landroid/graphics/Bitmap;Landroid/os/ParcelFileDescriptor;)V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lawj;

    invoke-direct {v1, p0, p1}, Lawj;-><init>(Landroid/graphics/Bitmap;Landroid/os/ParcelFileDescriptor;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 34
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 35
    return-void
.end method

.method public static synthetic a(Landroid/graphics/Bitmap;I)Z
    .locals 1

    .prologue
    .line 16
    invoke-static {p0, p1}, Lcom/google/android/apps/viewer/util/ParcelUtils;->receiveBitmap(Landroid/graphics/Bitmap;I)Z

    move-result v0

    return v0
.end method

.method private static native receiveBitmap(Landroid/graphics/Bitmap;I)Z
.end method
