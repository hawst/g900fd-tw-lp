.class public Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;
.super Lrm;
.source "VideoPlayerActivity.java"

# interfaces
.implements LNZ;
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private a:I

.field public a:LOf;

.field private a:LOk;

.field public a:LOn;

.field public a:LPp;

.field public a:LaGM;

.field public a:Lahr;

.field public a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "LuX;",
            ">;"
        }
    .end annotation
.end field

.field private a:Landroid/media/MediaPlayer;

.field private a:Landroid/widget/VideoView;

.field private a:Lcom/google/android/apps/docs/drive/media/VideoController;

.field private a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

.field private a:Ljava/lang/Integer;

.field private a:Ljava/lang/String;

.field private a:Ljava/util/concurrent/Executor;

.field public a:LtK;

.field private b:Landroid/os/Handler;

.field private i:Z

.field private j:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lrm;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/ResourceSpec;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 409
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 410
    const-string v1, "resourceSpec"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 411
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;)Landroid/widget/VideoView;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Landroid/widget/VideoView;

    return-object v0
.end method

.method private a()LbsU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbsU",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    if-nez v0, :cond_1

    .line 234
    :cond_0
    const-string v0, "VideoPlayerActivity"

    const-string v1, "Use the same url"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Ljava/lang/String;

    invoke-static {v0}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    .line 237
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->b()LbsU;

    move-result-object v0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;)Lcom/google/android/apps/docs/drive/media/VideoController;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Lcom/google/android/apps/docs/drive/media/VideoController;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;)Lcom/google/android/gms/drive/database/data/ResourceSpec;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    return-object v0
.end method

.method private a(I)V
    .locals 0

    .prologue
    .line 127
    if-lez p1, :cond_0

    .line 128
    iput p1, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:I

    .line 130
    :cond_0
    return-void
.end method

.method private a(LFV;)V
    .locals 1

    .prologue
    .line 341
    new-instance v0, LOe;

    invoke-direct {v0, p0, p1}, LOe;-><init>(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;LFV;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 359
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;LFV;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a(LFV;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->c(Ljava/lang/String;)V

    return-void
.end method

.method private static a(Landroid/media/MediaPlayer$TrackInfo;)Z
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 556
    invoke-virtual {p0}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v0

    .line 558
    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;)Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->j:Z

    return v0
.end method

.method private a()[Landroid/media/MediaPlayer$TrackInfo;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 489
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getTrackInfo()[Landroid/media/MediaPlayer$TrackInfo;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 495
    :goto_0
    return-object v0

    .line 490
    :catch_0
    move-exception v0

    .line 493
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Landroid/media/MediaPlayer;

    .line 494
    const-string v1, "VideoPlayerActivity"

    const-string v2, "Error fetching track info"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 495
    new-array v0, v4, [Landroid/media/MediaPlayer$TrackInfo;

    goto :goto_0
.end method

.method private b()LbsU;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbsU",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 242
    invoke-static {}, Lbtd;->a()Lbtd;

    move-result-object v0

    .line 243
    new-instance v1, LOb;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Fetch video URL for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, LOb;-><init>(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;Ljava/lang/String;Lbtd;)V

    .line 252
    invoke-virtual {v1}, LOb;->start()V

    .line 253
    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Ljava/lang/String;

    .line 134
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 289
    if-nez p1, :cond_0

    .line 290
    sget-object v0, LFV;->h:LFV;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a(LFV;)V

    .line 308
    :goto_0
    return-void

    .line 294
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:LtK;

    sget-object v1, Lry;->aC:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 295
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->d(Ljava/lang/String;)V

    .line 298
    :cond_1
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 299
    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:LOk;

    if-eqz v1, :cond_2

    .line 300
    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:LOk;

    invoke-virtual {v1, v0}, LOk;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 301
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    .line 304
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->b(Ljava/lang/String;)V

    .line 305
    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Landroid/widget/VideoView;

    invoke-virtual {v1, v0}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Landroid/widget/VideoView;

    iget v1, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->seekTo(I)V

    .line 307
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    goto :goto_0
.end method

.method private d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 311
    if-nez p1, :cond_0

    .line 312
    const-string v0, "VideoPlayerActivity"

    const-string v1, "No video id, can\'t load subtitles."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    :goto_0
    return-void

    .line 315
    :cond_0
    new-instance v0, LOd;

    invoke-direct {v0, p0, p1}, LOd;-><init>(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 332
    invoke-virtual {v0, v1}, LOd;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 137
    sget v0, LNJ;->video_view:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/VideoView;

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Landroid/widget/VideoView;

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Landroid/widget/VideoView;

    invoke-virtual {v0, p0}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Landroid/widget/VideoView;

    invoke-virtual {v0, p0}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Landroid/widget/VideoView;

    invoke-virtual {v0, p0}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 141
    return-void
.end method

.method private f()Z
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 501
    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Landroid/media/MediaPlayer;

    if-nez v1, :cond_1

    .line 510
    :cond_0
    :goto_0
    return v0

    .line 504
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object v2

    .line 505
    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 506
    invoke-static {v4}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a(Landroid/media/MediaPlayer$TrackInfo;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 507
    const/4 v0, 0x1

    goto :goto_0

    .line 505
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private g()Z
    .locals 1

    .prologue
    .line 515
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:LaGM;

    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v1

    .line 146
    if-eqz v1, :cond_0

    .line 147
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a()LaqY;

    move-result-object v0

    invoke-interface {v0, v1}, LaqY;->a(LaGu;)V

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LuX;

    .line 149
    invoke-virtual {v0, v1}, LuX;->a(LaGu;)V

    .line 152
    :cond_0
    return-void
.end method

.method private k()V
    .locals 3

    .prologue
    .line 257
    invoke-direct {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a()LbsU;

    move-result-object v0

    .line 258
    new-instance v1, LOc;

    invoke-direct {v1, p0}, LOc;-><init>(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;)V

    .line 285
    iget-object v2, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LbsK;->a(LbsU;LbsJ;Ljava/util/concurrent/Executor;)V

    .line 286
    return-void
.end method

.method private l()V
    .locals 1

    .prologue
    .line 336
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->setResult(I)V

    .line 337
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->finish()V

    .line 338
    return-void
.end method

.method private m()V
    .locals 2

    .prologue
    .line 465
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Lcom/google/android/apps/docs/drive/media/VideoController;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->f()V

    .line 466
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LvC;->a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->startActivity(Landroid/content/Intent;)V

    .line 467
    return-void
.end method

.method private n()V
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 520
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Landroid/media/MediaPlayer;

    if-nez v0, :cond_1

    .line 521
    iput-object v6, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Ljava/lang/Integer;

    .line 552
    :cond_0
    :goto_0
    return-void

    .line 525
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 527
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->deselectTrack(I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 531
    :goto_1
    iput-object v6, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 528
    :catch_0
    move-exception v0

    .line 529
    const-string v2, "VideoPlayerActivity"

    const-string v3, "Error deselecting subtitle track %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Ljava/lang/Integer;

    aput-object v5, v4, v1

    invoke-static {v2, v0, v3, v4}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1

    .line 534
    :cond_2
    const-string v0, "VideoPlayerActivity"

    const-string v3, "Looking for subtitle tracks"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 535
    invoke-direct {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object v3

    .line 536
    array-length v4, v3

    .line 538
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    move v0, v2

    .line 539
    :goto_2
    if-ge v1, v4, :cond_5

    .line 540
    aget-object v6, v3, v1

    .line 541
    invoke-static {v6}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a(Landroid/media/MediaPlayer$TrackInfo;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 542
    if-eq v0, v2, :cond_3

    .line 543
    invoke-virtual {v6}, Landroid/media/MediaPlayer$TrackInfo;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    :cond_3
    move v0, v1

    .line 539
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 548
    :cond_5
    if-eq v0, v2, :cond_0

    .line 549
    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v0}, Landroid/media/MediaPlayer;->selectTrack(I)V

    .line 550
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Ljava/lang/Integer;

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 471
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 472
    return-void
.end method

.method public d_()V
    .locals 2

    .prologue
    .line 476
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 477
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 385
    invoke-direct {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->l()V

    .line 386
    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Lcom/google/android/apps/docs/drive/media/VideoController;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->d()V

    .line 381
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 156
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:LOn;

    invoke-virtual {v0}, LOn;->a()LOk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:LOk;

    .line 158
    sget v0, LNL;->video_player:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->setContentView(I)V

    .line 159
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a()LaqY;

    move-result-object v0

    .line 160
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, LNK;->projector_actionbar_opacity:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 159
    invoke-interface {v0, v1}, LaqY;->b(I)V

    .line 161
    iput-boolean v3, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->i:Z

    .line 162
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->b:Landroid/os/Handler;

    .line 163
    new-instance v0, LalI;

    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->b:Landroid/os/Handler;

    invoke-direct {v0, v1}, LalI;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Ljava/util/concurrent/Executor;

    .line 164
    invoke-direct {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->f()V

    .line 166
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 167
    const-string v0, "resourceSpec"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/ResourceSpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    .line 168
    iput v3, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:I

    .line 169
    if-eqz p1, :cond_1

    .line 170
    const-string v0, "videoPlayPosition"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a(I)V

    .line 171
    const-string v0, "videoPlayUrl"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->b(Ljava/lang/String;)V

    .line 175
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 176
    const-string v0, "VideoPlayerActivity"

    const-string v1, "Illegal intent to start player"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->finish()V

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:LaGM;

    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-interface {v0, v1}, LaGM;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 181
    invoke-direct {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->j()V

    .line 183
    new-instance v0, Lcom/google/android/apps/docs/drive/media/VideoController;

    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Landroid/widget/VideoView;

    invoke-direct {v0, p0, v1, p0}, Lcom/google/android/apps/docs/drive/media/VideoController;-><init>(Landroid/content/Context;Landroid/widget/VideoView;LNZ;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Lcom/google/android/apps/docs/drive/media/VideoController;

    .line 184
    sget v0, LNJ;->top_frame:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 185
    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Lcom/google/android/apps/docs/drive/media/VideoController;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 186
    new-instance v1, LOa;

    invoke-direct {v1, p0}, LOa;-><init>(Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 194
    return-void

    .line 173
    :cond_1
    invoke-virtual {v1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 416
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 417
    sget v1, LNM;->menu_video_player:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 419
    sget v0, LNJ;->share_file:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 420
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LuX;

    invoke-virtual {v0, v1}, LuX;->a(Landroid/view/MenuItem;)V

    .line 421
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:LOk;

    if-eqz v0, :cond_0

    .line 482
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:LOk;

    invoke-virtual {v0}, LOk;->a()V

    .line 484
    :cond_0
    invoke-super {p0}, Lrm;->onDestroy()V

    .line 485
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 363
    const-string v0, "VideoPlayerActivity"

    const-string v1, "Error during video playback %s"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 364
    iput-object v6, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Landroid/media/MediaPlayer;

    .line 365
    iget-boolean v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->i:Z

    if-nez v0, :cond_0

    .line 368
    const-string v0, "VideoPlayerActivity"

    const-string v1, "Retry to play the video again"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    iput-boolean v5, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->i:Z

    .line 370
    invoke-direct {p0, v6}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->b(Ljava/lang/String;)V

    .line 371
    invoke-direct {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->k()V

    .line 375
    :goto_0
    return v5

    .line 373
    :cond_0
    sget-object v0, LFV;->h:LFV;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a(LFV;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 450
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, LNJ;->open_detail_panel:I

    if-ne v1, v2, :cond_0

    .line 451
    invoke-direct {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->m()V

    .line 461
    :goto_0
    return v0

    .line 453
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, LNJ;->toggle_subtitles:I

    if-ne v1, v2, :cond_1

    .line 454
    invoke-direct {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->n()V

    .line 455
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->invalidateOptionsMenu()V

    goto :goto_0

    .line 457
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, LNJ;->share_file:I

    if-ne v0, v1, :cond_2

    .line 458
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Lcom/google/android/apps/docs/drive/media/VideoController;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->f()V

    .line 461
    :cond_2
    invoke-super {p0, p1}, Lrm;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a(I)V

    .line 221
    invoke-super {p0}, Lrm;->onPause()V

    .line 222
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 426
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:LtK;

    sget-object v1, Lry;->r:Lry;

    .line 427
    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    .line 428
    sget v1, LNJ;->open_detail_panel:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 429
    if-eqz v1, :cond_0

    .line 430
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 433
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:LtK;

    sget-object v1, Lry;->aC:Lry;

    .line 434
    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    .line 435
    sget v1, LNJ;->toggle_subtitles:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 436
    if-eqz v1, :cond_1

    .line 437
    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 438
    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 440
    invoke-direct {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, LNN;->video_hide_subtitles:I

    .line 439
    :goto_0
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 445
    :cond_1
    :goto_1
    return v2

    .line 440
    :cond_2
    sget v0, LNN;->video_show_subtitles:I

    goto :goto_0

    .line 442
    :cond_3
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 2

    .prologue
    .line 390
    const-string v0, "VideoPlayerActivity"

    const-string v1, "MediaPlayer is prepared."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->i:Z

    .line 393
    iput-object p1, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Landroid/media/MediaPlayer;

    .line 395
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 396
    const-string v0, "accessibility"

    .line 397
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 398
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 399
    invoke-direct {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->n()V

    .line 403
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Lcom/google/android/apps/docs/drive/media/VideoController;

    iget v1, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:I

    invoke-virtual {v0, v1, p1, p0}, Lcom/google/android/apps/docs/drive/media/VideoController;->a(ILandroid/media/MediaPlayer;Landroid/content/Context;)V

    .line 405
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->invalidateOptionsMenu()V

    .line 406
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 206
    invoke-super {p0}, Lrm;->onResume()V

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:LPp;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LPp;->a(Z)V

    .line 208
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 212
    invoke-super {p0, p1}, Lrm;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 213
    const-string v0, "videoPlayPosition"

    iget v1, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 214
    const-string v0, "videoPlayUrl"

    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    const-string v0, "VideoPlayerActivity"

    const-string v1, "The current position is %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 216
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 198
    invoke-super {p0}, Lrm;->onStart()V

    .line 199
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->j:Z

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Lcom/google/android/apps/docs/drive/media/VideoController;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->c()V

    .line 201
    invoke-direct {p0}, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->k()V

    .line 202
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->stopPlayback()V

    .line 227
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->j:Z

    .line 228
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoPlayerActivity;->a:Landroid/media/MediaPlayer;

    .line 229
    invoke-super {p0}, Lrm;->onStop()V

    .line 230
    return-void
.end method
