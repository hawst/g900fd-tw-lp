.class public Lcom/google/android/apps/viewer/action/PrintDialogActivity;
.super Landroid/app/Activity;
.source "PrintDialogActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 93
    return-void
.end method

.method public static a(Landroid/content/Context;LauM;I)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 61
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/viewer/action/PrintDialogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 62
    iget-object v1, p1, LauM;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    iget-object v1, v1, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    iget-object v2, p1, LauM;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 63
    const-string v1, "token"

    iget-object v2, p1, LauM;->a:Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    iget-object v2, v2, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->tokens:Lcom/google/android/apps/viewer/client/TokenSource;

    invoke-interface {v2}, Lcom/google/android/apps/viewer/client/TokenSource;->newToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 64
    const-string v1, "title"

    iget-object v2, p1, LauM;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    const-string v1, "pages"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 66
    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 31
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/action/PrintDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 34
    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 35
    const-string v2, "pages"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 36
    invoke-virtual {v0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v3

    .line 37
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    .line 38
    const-string v5, "token"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 39
    if-nez v5, :cond_0

    sget-object v0, Lcom/google/android/apps/viewer/client/TokenSource;->a:Lcom/google/android/apps/viewer/client/TokenSource;

    .line 40
    :goto_0
    new-instance v5, Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    invoke-direct {v5, v4, v0}, Lcom/google/android/apps/viewer/client/AuthenticatedUri;-><init>(Landroid/net/Uri;Lcom/google/android/apps/viewer/client/TokenSource;)V

    .line 44
    new-instance v4, Lavl;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/action/PrintDialogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {v4, v0}, Lavl;-><init>(Landroid/content/ContentResolver;)V

    .line 46
    iget-object v0, v5, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-static {v3, v0}, Lauw;->a(Ljava/lang/String;Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, LavX;->r:Z

    if-nez v0, :cond_1

    .line 47
    new-instance v0, Lauw;

    invoke-direct {v0, p0, v4}, Lauw;-><init>(Landroid/app/Activity;Lavl;)V

    .line 53
    :goto_1
    :try_start_0
    invoke-interface {v0, v1, v3, v5, v2}, Lauv;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/viewer/client/AuthenticatedUri;I)V
    :try_end_0
    .catch Lauu; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    :goto_2
    return-void

    .line 39
    :cond_0
    new-instance v0, Lcom/google/android/apps/viewer/client/TokenSource$OneTimeTokenSource;

    invoke-direct {v0, v5}, Lcom/google/android/apps/viewer/client/TokenSource$OneTimeTokenSource;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :cond_1
    new-instance v0, Laun;

    invoke-direct {v0, p0, v4}, Laun;-><init>(Landroid/app/Activity;Lavl;)V

    goto :goto_1

    .line 54
    :catch_0
    move-exception v0

    .line 55
    invoke-virtual {v0}, Lauu;->getMessage()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/action/PrintDialogActivity;->finish()V

    goto :goto_2
.end method
