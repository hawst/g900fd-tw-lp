.class public Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "PickAccountDialogFragment.java"


# instance fields
.field public a:LSF;

.field public a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "LFq;",
            ">;"
        }
    .end annotation
.end field

.field public a:Lald;

.field private a:[Landroid/accounts/Account;

.field private w:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->w:Z

    return-void
.end method

.method public static a(LM;)Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;
    .locals 2

    .prologue
    .line 74
    const-string v0, "PickAccountDialogFragment"

    .line 75
    invoke-virtual {p0, v0}, LM;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;

    .line 76
    if-nez v0, :cond_0

    .line 77
    new-instance v0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;-><init>()V

    .line 79
    :cond_0
    const-string v1, "PickAccountDialogFragment"

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a(LM;Ljava/lang/String;)V

    .line 80
    return-object v0
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LFq;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:[Landroid/accounts/Account;

    aget-object v1, v1, p1

    invoke-interface {v0, v1}, LFq;->a(Landroid/accounts/Account;)V

    .line 176
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a()V

    .line 177
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;I)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a(I)V

    return-void
.end method

.method private static a([Landroid/accounts/Account;)[Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 159
    array-length v1, p0

    new-array v2, v1, [Ljava/lang/String;

    .line 161
    array-length v3, p0

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, p0, v0

    .line 162
    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v2, v1

    .line 163
    add-int/lit8 v1, v1, 0x1

    .line 161
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 166
    :cond_0
    return-object v2
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:[Landroid/accounts/Account;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a([Landroid/accounts/Account;)[Ljava/lang/String;

    move-result-object v0

    .line 125
    new-instance v1, LFo;

    invoke-direct {v1, p0}, LFo;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;)V

    .line 136
    new-instance v2, LFp;

    invoke-direct {v2, p0}, LFp;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;)V

    .line 145
    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:LSF;

    invoke-interface {v3}, LSF;->a()LaFO;

    move-result-object v3

    .line 146
    iget-object v4, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:[Landroid/accounts/Account;

    invoke-static {v4, v3}, LSH;->a([Landroid/accounts/Account;LaFO;)I

    move-result v3

    .line 147
    const/4 v4, 0x0

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 149
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a()LH;

    move-result-object v4

    invoke-static {v4}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v4

    sget v5, Lxi;->select_account:I

    .line 150
    invoke-virtual {p0, v5}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, LEU;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const/4 v5, 0x0

    .line 151
    invoke-virtual {v4, v0, v3, v5}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v3, 0x104000a

    .line 152
    invoke-virtual {v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    .line 153
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 155
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public a(Z)Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;
    .locals 2

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->w:Z

    .line 89
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 90
    const-string v1, "withConfirmation"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 91
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->e(Landroid/os/Bundle;)V

    .line 92
    return-object p0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 97
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a_(Landroid/os/Bundle;)V

    .line 98
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 99
    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->w:Z

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:LSF;

    invoke-interface {v0}, LSF;->a()[Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:[Landroid/accounts/Account;

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:[Landroid/accounts/Account;

    array-length v0, v0

    .line 106
    if-nez v0, :cond_1

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:Lald;

    sget v2, Lxi;->google_account_needed:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lald;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LFq;

    invoke-interface {v0}, LFq;->f()V

    .line 109
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->c(Z)V

    .line 119
    :goto_1
    return-void

    .line 99
    :cond_0
    const-string v2, "withConfirmation"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 111
    :cond_1
    if-ne v0, v3, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->w:Z

    if-nez v0, :cond_2

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LFq;

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:[Landroid/accounts/Account;

    aget-object v2, v2, v1

    invoke-interface {v0, v2}, LFq;->a(Landroid/accounts/Account;)V

    .line 114
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->c(Z)V

    goto :goto_1

    .line 116
    :cond_2
    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->c(Z)V

    goto :goto_1
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LFq;

    invoke-interface {v0}, LFq;->f()V

    .line 172
    return-void
.end method
