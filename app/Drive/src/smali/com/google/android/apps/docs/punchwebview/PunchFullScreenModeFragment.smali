.class public Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;
.super Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;
.source "PunchFullScreenModeFragment.java"


# instance fields
.field private a:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 42
    const-string v0, "PunchFullScreenModeFragment"

    sget-object v1, LYR;->b:LYR;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;-><init>(Ljava/lang/String;LYR;)V

    .line 43
    return-void
.end method

.method private A()V
    .locals 4

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->a()LH;

    move-result-object v1

    .line 116
    invoke-virtual {v1}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->a:I

    .line 121
    const/4 v0, 0x0

    .line 122
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x9

    if-lt v2, v3, :cond_0

    .line 123
    const/4 v0, 0x6

    .line 126
    :cond_0
    invoke-virtual {v1, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 127
    return-void
.end method

.method public static a()Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->a:LQr;

    invoke-static {v0}, LZD;->a(LQr;)LZD;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->a:LZD;

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->a:LYO;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->a:LZD;

    invoke-virtual {v1}, LZD;->b()LYK;

    move-result-object v1

    invoke-interface {v0, v1}, LYO;->a(LYK;)V

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->a:LYO;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->a:LZD;

    .line 51
    invoke-virtual {v1}, LZD;->b()Z

    move-result v1

    .line 50
    invoke-interface {v0, v1}, LYO;->c(Z)V

    .line 53
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a_(Landroid/os/Bundle;)V

    .line 56
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->A()V

    .line 57
    return-void
.end method

.method protected m()Z
    .locals 1

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->w()V

    .line 145
    const/4 v0, 0x1

    return v0
.end method

.method public o_()V
    .locals 6

    .prologue
    .line 61
    invoke-super {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->o_()V

    .line 64
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->a()LH;

    move-result-object v0

    invoke-virtual {v0}, LH;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->a:Landroid/webkit/WebView;

    new-instance v2, LXv;

    invoke-direct {v2, p0, v0}, LXv;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;Landroid/view/Window;)V

    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v2, v4, v5}, Landroid/webkit/WebView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 77
    return-void
.end method

.method protected v()V
    .locals 6

    .prologue
    .line 82
    invoke-super {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->v()V

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->a:Landroid/webkit/WebView;

    if-nez v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->c:Ljava/lang/String;

    const-string v1, "early exit in cleanup, as it was never fully created"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :goto_0
    return-void

    .line 89
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->a()LH;

    move-result-object v0

    .line 90
    iget v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->a:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 93
    check-cast v0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->j()V

    .line 96
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->a()LH;

    move-result-object v0

    invoke-virtual {v0}, LH;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 97
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->a:Landroid/webkit/WebView;

    new-instance v2, LXw;

    invoke-direct {v2, p0, v0}, LXw;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;Landroid/view/Window;)V

    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v2, v4, v5}, Landroid/webkit/WebView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public w()V
    .locals 3

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->a:LYO;

    sget-object v1, LYR;->a:LYR;

    invoke-interface {v0, v1}, LYO;->a(LYR;)V

    .line 135
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->a()LH;

    move-result-object v0

    invoke-virtual {v0}, LH;->a()LM;

    move-result-object v0

    const-string v1, "fullScreenModeFragment"

    const/4 v2, 0x1

    .line 136
    invoke-virtual {v0, v1, v2}, LM;->a(Ljava/lang/String;I)V

    .line 137
    return-void
.end method
