.class public Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;
.super Lcom/google/android/apps/docs/tools/gelly/android/GuiceDialogFragment;
.source "LinkSharingCard.java"

# interfaces
.implements Labx;


# instance fields
.field private a:LaGu;

.field public a:Labw;

.field private a:Lacr;

.field private a:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 267
    invoke-direct {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceDialogFragment;-><init>()V

    return-void
.end method

.method private a(LaGu;Lacr;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;->a:LaGu;

    .line 280
    iput-object p2, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;->a:Lacr;

    .line 281
    iput-object p3, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;->a:Ljava/lang/Runnable;

    .line 282
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;LaGu;Lacr;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 267
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;->a(LaGu;Lacr;Ljava/lang/Runnable;)V

    return-void
.end method

.method private b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 306
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "confirmSharingDialog"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a_(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 286
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceDialogFragment;->a_(Landroid/os/Bundle;)V

    .line 287
    if-eqz p1, :cond_1

    .line 288
    invoke-virtual {p0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;->a()LH;

    move-result-object v0

    .line 289
    invoke-virtual {v0}, LH;->a()LM;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LM;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 290
    if-eqz v0, :cond_0

    .line 291
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->a()V

    .line 293
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;->a()V

    .line 296
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;->a:Labw;

    invoke-direct {p0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;->b()Ljava/lang/String;

    move-result-object v1

    .line 297
    invoke-virtual {p0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;->a:LaGu;

    .line 298
    invoke-interface {v3}, LaGu;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;->a:LaGu;

    .line 299
    invoke-interface {v4}, LaGu;->a()LaGw;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;->a:Lacr;

    .line 300
    invoke-interface {v5}, Lacr;->b()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;->a:Lacr;

    .line 301
    invoke-interface {v6}, Lacr;->a()Ljava/lang/String;

    move-result-object v6

    .line 302
    invoke-static {}, LbmF;->c()LbmF;

    move-result-object v7

    .line 296
    invoke-virtual/range {v0 .. v7}, Labw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LaGw;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 303
    return-void
.end method

.method public j(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 312
    invoke-virtual {p0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;->a()V

    .line 313
    return-void
.end method

.method public k(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 317
    invoke-virtual {p0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard$LinkSharingConfirmationDialogFragment;->a()V

    .line 318
    return-void
.end method
