.class public Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;
.super Lajq;
.source "AccountChangeReceiver.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public a:LSF;

.field public a:LZU;

.field public a:LaGM;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lajq;-><init>()V

    return-void
.end method

.method constructor <init>(LaGM;LSF;LZU;)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Lajq;-><init>()V

    .line 53
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGM;

    iput-object v0, p0, Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;->a:LaGM;

    .line 54
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSF;

    iput-object v0, p0, Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;->a:LSF;

    .line 55
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZU;

    iput-object v0, p0, Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;->a:LZU;

    .line 56
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 60
    const-string v0, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 61
    sget-object v0, Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;->a:Ljava/lang/String;

    const-string v2, "Unexpected broadcast received: %s"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object p2, v3, v1

    invoke-static {v0, v2, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 94
    :cond_0
    return-void

    .line 64
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;->a:LaGM;

    invoke-interface {v0}, LaGM;->a()Ljava/util/Set;

    move-result-object v2

    .line 65
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;->a:LSF;

    invoke-interface {v0}, LSF;->a()[Landroid/accounts/Account;

    move-result-object v4

    array-length v5, v4

    move v0, v1

    :goto_0
    if-ge v0, v5, :cond_2

    aget-object v6, v4, v0

    .line 67
    iget-object v6, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v6}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 66
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 69
    :cond_2
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 70
    invoke-interface {v4, v3}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 71
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 72
    invoke-interface {v5, v2}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 74
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFO;

    .line 75
    iget-object v3, p0, Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;->a:LaGM;

    invoke-interface {v3, v0}, LaGM;->a(LaFO;)LaFM;

    goto :goto_1

    .line 78
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;->a:LZU;

    if-eqz v0, :cond_4

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;->a:LZU;

    invoke-interface {v0, p1, v5, v4}, LZU;->a(Landroid/content/Context;Ljava/util/Set;Ljava/util/Set;)V

    .line 82
    :cond_4
    sget-object v0, Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;->a:Ljava/lang/String;

    const-string v2, "Accounts changed: %s add; %s removed"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v5, v3, v1

    aput-object v4, v3, v7

    invoke-static {v0, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 83
    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFO;

    .line 85
    iget-object v2, p0, Lcom/google/android/apps/docs/receivers/AccountChangeReceiver;->a:LaGM;

    .line 86
    invoke-interface {v2, v0}, LaGM;->a(LaFO;)LaFM;

    goto :goto_2
.end method
