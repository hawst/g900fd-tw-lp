.class public Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;
.super Landroid/widget/LinearLayout;
.source "SwipablePanelLayout.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field private final a:F

.field private final a:I

.field private a:LEv;

.field private final a:Landroid/view/GestureDetector;

.field private final a:Landroid/widget/Scroller;

.field private a:Z

.field private b:F

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->b:F

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a:Z

    .line 63
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LEw;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, LEw;-><init>(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;LEu;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a:Landroid/view/GestureDetector;

    .line 64
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a:Landroid/widget/Scroller;

    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxa;->fling_velocity:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a:I

    .line 67
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a:F

    .line 68
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->b:I

    .line 69
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->c:I

    .line 70
    return-void
.end method

.method private a(F)F
    .locals 2

    .prologue
    .line 209
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method private a(Landroid/view/MotionEvent;)F
    .locals 2

    .prologue
    .line 114
    iget v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->b:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->b:F

    .line 116
    const/4 v0, 0x0

    .line 118
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->b:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;F)F
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a(F)F

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;)I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->c:I

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;)LEv;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a:LEv;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;)Landroid/widget/Scroller;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a:Landroid/widget/Scroller;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 123
    iget-boolean v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a:Z

    if-eqz v0, :cond_0

    .line 124
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a(I)V

    .line 126
    :cond_0
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->b:F

    .line 127
    iput-boolean v1, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a:Z

    .line 128
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->getTranslationY()F

    move-result v0

    float-to-int v0, v0

    .line 152
    if-nez p1, :cond_0

    .line 156
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    .line 157
    if-ge v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a:I

    neg-int v0, v0

    :goto_0
    move p1, v0

    .line 160
    :cond_0
    if-lez p1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->getHeight()I

    move-result v0

    .line 161
    :goto_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a(II)V

    .line 162
    return-void

    .line 157
    :cond_1
    iget v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a:I

    goto :goto_0

    .line 160
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(II)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a:Landroid/widget/Scroller;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->getTranslationY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->getHeight()I

    move-result v8

    move v3, v1

    move v4, p1

    move v5, v1

    move v6, v1

    move v7, v1

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a:Landroid/widget/Scroller;

    invoke-virtual {v0, p2}, Landroid/widget/Scroller;->setFinalY(I)V

    .line 169
    new-instance v0, LEu;

    invoke-direct {v0, p0}, LEu;-><init>(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->post(Ljava/lang/Runnable;)Z

    .line 184
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;I)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a(I)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;Z)Z
    .locals 0

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;)I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->b:I

    return v0
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;)I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a:I

    return v0
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 83
    invoke-static {p1}, LdH;->a(Landroid/view/MotionEvent;)I

    move-result v1

    .line 85
    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    if-ne v1, v0, :cond_2

    .line 86
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a()V

    .line 87
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 110
    :cond_1
    :goto_0
    return v0

    .line 90
    :cond_2
    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 91
    iget-boolean v1, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a:Z

    if-nez v1, :cond_1

    .line 99
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a(Landroid/view/MotionEvent;)F

    move-result v1

    .line 100
    iget v2, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    .line 102
    iput-boolean v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a:Z

    goto :goto_0

    .line 106
    :cond_3
    if-nez v1, :cond_4

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 110
    :cond_4
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 132
    invoke-static {p1}, LdH;->a(Landroid/view/MotionEvent;)I

    move-result v0

    .line 134
    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    if-ne v0, v2, :cond_1

    .line 135
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a()V

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 139
    return v2
.end method

.method public setListener(LEv;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->a:LEv;

    .line 74
    return-void
.end method
