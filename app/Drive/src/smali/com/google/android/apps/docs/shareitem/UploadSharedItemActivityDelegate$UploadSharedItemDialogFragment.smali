.class public Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "UploadSharedItemActivityDelegate.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    return-void
.end method

.method private a()Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;
    .locals 1

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;->a()LH;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;)Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;->a()Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 119
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;->a()Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    move-result-object v3

    .line 121
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;->a()LH;

    move-result-object v0

    invoke-static {v0}, LEL;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v4

    .line 122
    const-string v0, "layout_inflater"

    .line 123
    invoke-virtual {v4, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 124
    invoke-static {v3, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v5

    .line 126
    invoke-static {v4}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v6

    .line 128
    invoke-static {v3}, Lamt;->a(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 129
    invoke-static {v3, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    .line 130
    invoke-virtual {v6, v0}, LEU;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 135
    :goto_0
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LakQ;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v6, v0}, LEU;->a(Z)V

    .line 136
    invoke-virtual {v6, v1}, LEU;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    .line 137
    invoke-virtual {v6, v5}, LEU;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 138
    sget v0, Lxb;->upload_to_drive_icon:I

    invoke-virtual {v6, v0}, LEU;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 139
    sget v0, Lxi;->upload_shared_item_confirm:I

    new-instance v1, Labc;

    invoke-direct {v1, p0}, Labc;-><init>(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;)V

    invoke-virtual {v6, v0, v1}, LEU;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 145
    const/high16 v0, 0x1040000

    new-instance v1, Labd;

    invoke-direct {v1, p0}, Labd;-><init>(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;)V

    invoke-virtual {v6, v0, v1}, LEU;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 152
    invoke-virtual {v6}, LEU;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 153
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 154
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 155
    return-object v0

    .line 132
    :cond_0
    invoke-static {v3}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, LEU;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 135
    goto :goto_1
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;->a()Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)V

    .line 161
    return-void
.end method
