.class public Lcom/google/android/apps/docs/fragment/ErrorFragment;
.super Lcom/google/android/apps/docs/app/BaseFragment;
.source "ErrorFragment.java"


# instance fields
.field public a:Lald;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 42
    sget v0, Lxe;->document_opener_activity_error:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseFragment;->b(Landroid/os/Bundle;)V

    .line 49
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/ErrorFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "errorResId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 50
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/ErrorFragment;->a()Landroid/view/View;

    move-result-object v0

    sget v2, Lxc;->error_message:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 51
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/ErrorFragment;->a:Lald;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/fragment/ErrorFragment;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v2, v0, v1, v3}, Lald;->a(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/ErrorFragment;->a()Landroid/view/View;

    move-result-object v0

    sget v1, Lxc;->retry_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 55
    return-void
.end method
