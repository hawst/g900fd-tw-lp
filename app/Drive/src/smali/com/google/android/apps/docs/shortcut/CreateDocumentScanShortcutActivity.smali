.class public Lcom/google/android/apps/docs/shortcut/CreateDocumentScanShortcutActivity;
.super LqS;
.source "CreateDocumentScanShortcutActivity.java"


# instance fields
.field public a:LacP;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, LqS;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;
    .locals 3

    .prologue
    .line 34
    const/4 v0, 0x1

    new-array v0, v0, [LaGv;

    const/4 v1, 0x0

    sget-object v2, LaGv;->a:LaGv;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->a([LaGv;)Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 2

    .prologue
    .line 39
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    iget-object v0, p0, Lcom/google/android/apps/docs/shortcut/CreateDocumentScanShortcutActivity;->a:LacP;

    invoke-virtual {v0, p1}, LacP;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    .line 41
    if-eqz v0, :cond_0

    .line 42
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/docs/shortcut/CreateDocumentScanShortcutActivity;->setResult(ILandroid/content/Intent;)V

    .line 44
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shortcut/CreateDocumentScanShortcutActivity;->finish()V

    .line 45
    return-void
.end method

.method protected a(LuW;)V
    .locals 1

    .prologue
    .line 26
    sget v0, LatO;->widget_scan_to_drive_title:I

    .line 27
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/shortcut/CreateDocumentScanShortcutActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LuW;->a(Ljava/lang/String;)LuW;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, LuW;->b()LuW;

    move-result-object v0

    .line 29
    invoke-virtual {v0}, LuW;->a()LuW;

    .line 30
    return-void
.end method
