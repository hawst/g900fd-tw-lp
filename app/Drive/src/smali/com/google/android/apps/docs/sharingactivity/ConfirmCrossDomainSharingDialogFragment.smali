.class public Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;
.super Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;
.source "ConfirmCrossDomainSharingDialogFragment.java"


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;-><init>()V

    .line 30
    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 94
    invoke-static {p1}, LacH;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 95
    const-string v1, "gmail.com"

    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->e:Ljava/lang/String;

    .line 96
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 107
    sget v0, Lxi;->dialog_confirm_sharing_message:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->b:Ljava/util/List;

    .line 108
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v1, v3

    .line 107
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 113
    :goto_0
    return-object v0

    .line 110
    :cond_0
    sget v0, Lxi;->dialog_confirm_sharing_message_multiple:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->b:Ljava/util/List;

    .line 111
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    .line 110
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private l()Z
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->c:Ljava/lang/String;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->a()LH;

    move-result-object v0

    sget v1, Lxi;->dialog_confirm_sharing:I

    invoke-virtual {v0, v1}, LH;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->c:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;->a_(Landroid/os/Bundle;)V

    .line 64
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 66
    invoke-static {v0}, Labw;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->d:Ljava/lang/String;

    .line 67
    invoke-static {v0}, Labw;->b(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->e:Ljava/lang/String;

    .line 68
    invoke-static {v0}, Labw;->a(Landroid/os/Bundle;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->a:Ljava/util/List;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->b:Ljava/util/List;

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 71
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 72
    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 76
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->c:Ljava/lang/String;

    .line 78
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->l()Z

    move-result v0

    if-nez v0, :cond_2

    .line 79
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ConfirmCrossDomainSharingDialogFragment;->i(Z)V

    .line 81
    :cond_2
    return-void
.end method
