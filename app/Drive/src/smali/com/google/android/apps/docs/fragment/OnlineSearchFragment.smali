.class public Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;
.super Lcom/google/android/apps/docs/app/BaseFragment;
.source "OnlineSearchFragment.java"


# instance fields
.field public a:LaGM;

.field public a:LaKM;

.field public a:Laas;

.field private final a:Lbtd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtd",
            "<",
            "Laay;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseFragment;-><init>()V

    .line 134
    invoke-static {}, Lbtd;->a()Lbtd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a:Lbtd;

    .line 137
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->e(Z)V

    .line 138
    return-void
.end method

.method private a()LaFO;
    .locals 2

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;)LaFO;
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a()LaFO;

    move-result-object v0

    return-object v0
.end method

.method private a()Laat;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a:Laas;

    invoke-interface {v0}, Laas;->a()Laat;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;)Laat;
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a()Laat;

    move-result-object v0

    return-object v0
.end method

.method private a()LbsU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbsU",
            "<",
            "Laay;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a:Lbtd;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;)LbsU;
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a()LbsU;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 166
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a_(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 142
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseFragment;->a_(Landroid/os/Bundle;)V

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a:LaGM;

    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a()LaFO;

    move-result-object v1

    invoke-interface {v0, v1}, LaGM;->a(LaFO;)LaFM;

    move-result-object v0

    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->b()Ljava/lang/String;

    move-result-object v1

    .line 146
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a:Laas;

    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a:LaKM;

    invoke-interface {v4}, LaKM;->a()J

    move-result-wide v4

    invoke-interface {v2, v0, v3, v4, v5}, Laas;->a(LaFM;Ljava/lang/String;J)Laay;

    move-result-object v0

    .line 147
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a:Lbtd;

    invoke-virtual {v2, v0}, Lbtd;->a(Ljava/lang/Object;)Z

    .line 149
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a()LH;

    move-result-object v0

    invoke-static {v0}, Lamt;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 150
    new-instance v0, Landroid/provider/SearchRecentSuggestions;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a()LH;

    move-result-object v2

    .line 151
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a()LH;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {v0, v2, v3, v4}, Landroid/provider/SearchRecentSuggestions;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    .line 153
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/provider/SearchRecentSuggestions;->saveRecentQuery(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    :cond_0
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a:Laas;

    invoke-interface {v0}, Laas;->a()V

    .line 176
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseFragment;->h()V

    .line 177
    return-void
.end method
