.class public Lcom/google/android/apps/docs/view/LinearLayoutListView;
.super Landroid/widget/FrameLayout;
.source "LinearLayoutListView.java"


# instance fields
.field public a:I

.field private final a:Landroid/database/DataSetObserver;

.field public a:Landroid/os/Handler;

.field public a:Landroid/view/ViewGroup;

.field public a:Landroid/widget/LinearLayout;

.field private a:Landroid/widget/ListAdapter;

.field private a:Laqj;

.field private a:Laqk;

.field public a:Laql;

.field private a:Laqr;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field public b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 305
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 306
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 312
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 313
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 319
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 243
    new-instance v0, Laqb;

    invoke-direct {v0, p0}, Laqb;-><init>(Lcom/google/android/apps/docs/view/LinearLayoutListView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/database/DataSetObserver;

    .line 268
    iput v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    .line 275
    iput v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    .line 291
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Ljava/util/Map;

    .line 294
    sget-object v0, Laql;->a:Laql;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laql;

    .line 297
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/os/Handler;

    .line 299
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Z

    .line 320
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->g()V

    .line 321
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/LinearLayoutListView;)Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/LinearLayoutListView;)Laqj;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laqj;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/LinearLayoutListView;)Laqr;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laqr;

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 467
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b()V

    .line 468
    iput v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    .line 469
    iput v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    .line 470
    return-void
.end method

.method private a(II)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 569
    if-ltz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 570
    if-gt p1, p2, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, LbiT;->a(Z)V

    .line 571
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 572
    iget v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    if-lez v0, :cond_3

    move v0, v1

    :goto_2
    invoke-static {v0}, LbiT;->b(Z)V

    .line 573
    iget v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    if-lez v0, :cond_4

    :goto_3
    invoke-static {v1}, LbiT;->b(Z)V

    .line 575
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laql;

    iget v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    iget v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    invoke-static {v0, v1, v2}, Laql;->a(Laql;II)I

    move-result v1

    .line 576
    div-int v0, p1, v1

    .line 577
    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    add-int/lit8 v3, p2, -0x1

    div-int v1, v3, v1

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 578
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    iget v4, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    invoke-direct {v3, v1, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    move v1, v0

    .line 579
    :goto_4
    if-gt v1, v2, :cond_5

    .line 580
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 581
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-nez v4, :cond_0

    .line 582
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b(I)Landroid/view/View;

    move-result-object v4

    .line 583
    invoke-virtual {v4, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 584
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 579
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_1
    move v0, v2

    .line 569
    goto :goto_0

    :cond_2
    move v0, v2

    .line 570
    goto :goto_1

    :cond_3
    move v0, v2

    .line 572
    goto :goto_2

    :cond_4
    move v1, v2

    .line 573
    goto :goto_3

    .line 587
    :cond_5
    return-void
.end method

.method private a(ILandroid/view/ViewGroup;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 696
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 698
    :goto_0
    if-nez v0, :cond_1

    .line 710
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 696
    goto :goto_0

    .line 702
    :cond_1
    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 707
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 708
    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 709
    invoke-virtual {p2}, Landroid/view/ViewGroup;->removeAllViews()V

    goto :goto_1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/LinearLayoutListView;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/LinearLayoutListView;)Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Z

    return v0
.end method

.method private b(I)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 657
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 659
    const/4 v0, 0x0

    .line 661
    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 662
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 671
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    .line 672
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 674
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    invoke-interface {v2, p1, v0, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 675
    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 676
    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 677
    new-instance v1, Laqf;

    invoke-direct {v1, p0, p1}, Laqf;-><init>(Lcom/google/android/apps/docs/view/LinearLayoutListView;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 686
    return-object v0

    .line 657
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 663
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    const/16 v3, 0x32

    if-lt v2, v3, :cond_0

    .line 667
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 668
    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_1
.end method

.method private b()V
    .locals 1

    .prologue
    .line 476
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->f()V

    .line 477
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 478
    return-void
.end method

.method private b(I)V
    .locals 5

    .prologue
    .line 608
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laqk;

    if-eqz v0, :cond_0

    .line 609
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laql;

    iget v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    iget v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    invoke-static {v0, v1, v2}, Laql;->a(Laql;II)I

    move-result v0

    .line 610
    div-int v1, p1, v0

    .line 611
    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laql;

    iget-object v3, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    invoke-static {v2, v3, v4}, Laql;->a(Laql;II)I

    move-result v2

    .line 612
    add-int/2addr v2, p1

    .line 613
    iget-object v3, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    add-int/lit8 v2, v2, -0x1

    div-int v0, v2, v0

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 614
    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 615
    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laqk;

    invoke-interface {v2, v1, v0}, Laqk;->a(II)V

    .line 617
    :cond_0
    return-void
.end method

.method private b(II)V
    .locals 7

    .prologue
    .line 625
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laql;

    iget v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    iget v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    invoke-static {v0, v1, v2}, Laql;->a(Laql;II)I

    move-result v4

    .line 626
    div-int v3, p1, v4

    .line 627
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laql;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    invoke-static {v0, v1, v2}, Laql;->a(Laql;II)I

    move-result v0

    .line 628
    add-int v5, p2, v0

    .line 629
    add-int/2addr v0, p1

    .line 630
    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v0, v0, -0x1

    div-int/2addr v0, v4

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 633
    if-le p2, p1, :cond_0

    .line 634
    div-int v0, p2, v4

    .line 635
    add-int/lit8 v0, v0, -0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v6

    move v2, v3

    .line 636
    :goto_0
    if-gt v2, v6, :cond_0

    .line 637
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 638
    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a(ILandroid/view/ViewGroup;)V

    .line 636
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 643
    :cond_0
    if-ge p2, p1, :cond_1

    .line 644
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    add-int/lit8 v2, v5, -0x1

    div-int/2addr v2, v4

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 645
    add-int/lit8 v0, v0, 0x1

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 646
    :goto_1
    if-lt v1, v2, :cond_1

    .line 647
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 648
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a(ILandroid/view/ViewGroup;)V

    .line 646
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 651
    :cond_1
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/view/LinearLayoutListView;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->c()V

    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 484
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 485
    iput-boolean v3, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Z

    .line 487
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 497
    :goto_0
    return-void

    .line 491
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->d()V

    .line 493
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laql;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getScrollX()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getScrollY()I

    move-result v2

    invoke-static {v0, v1, v2}, Laql;->a(Laql;II)I

    move-result v0

    .line 494
    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 495
    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laql;

    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    invoke-static {v1, v2, v3}, Laql;->a(Laql;II)I

    move-result v1

    .line 496
    add-int/2addr v1, v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a(II)V

    goto :goto_0
.end method

.method private d()V
    .locals 5

    .prologue
    .line 503
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 505
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->e()V

    .line 507
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    .line 508
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    iget v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    iget v3, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    invoke-direct {v2, v0, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 509
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 510
    new-instance v3, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 511
    invoke-virtual {v3, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 512
    iget-object v4, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 509
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 514
    :cond_0
    return-void
.end method

.method private e()V
    .locals 7

    .prologue
    const/4 v2, -0x1

    const/high16 v3, -0x80000000

    const/4 v1, 0x0

    .line 520
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 521
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 523
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b(I)Landroid/view/View;

    move-result-object v5

    .line 525
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 526
    if-nez v0, :cond_0

    .line 527
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 529
    :cond_0
    invoke-virtual {v5, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 533
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->getHeight()I

    move-result v2

    .line 534
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->getWidth()I

    move-result v0

    .line 536
    sget-object v4, Laqg;->a:[I

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a()Laql;

    move-result-object v6

    invoke-virtual {v6}, Laql;->ordinal()I

    move-result v6

    aget v4, v4, v6

    packed-switch v4, :pswitch_data_0

    .line 546
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unexpected orientation when determining size of filmstrip thumbnail views"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v1

    .line 521
    goto :goto_0

    .line 538
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->getPaddingLeft()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->getPaddingRight()I

    move-result v6

    add-int/2addr v4, v6

    sub-int/2addr v0, v4

    move v4, v3

    move v3, v1

    .line 549
    :goto_1
    iget-object v6, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 551
    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 552
    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 551
    invoke-virtual {v5, v0, v2}, Landroid/view/View;->measure(II)V

    .line 554
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    .line 555
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    .line 557
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 558
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 559
    return-void

    .line 542
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->getPaddingTop()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->getPaddingBottom()I

    move-result v6

    add-int/2addr v4, v6

    sub-int/2addr v2, v4

    move v4, v1

    .line 544
    goto :goto_1

    .line 536
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private f()V
    .locals 3

    .prologue
    .line 716
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 717
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 718
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 719
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a(ILandroid/view/ViewGroup;)V

    .line 716
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 722
    :cond_1
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 728
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laql;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, p0, v1}, Laql;->a(Laql;Lcom/google/android/apps/docs/view/LinearLayoutListView;Landroid/content/Context;)Laqr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laqr;

    .line 729
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laqr;

    invoke-interface {v0}, Laqr;->a()Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    .line 730
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    .line 731
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laql;

    invoke-static {v1}, Laql;->c(Laql;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 732
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 733
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->addView(Landroid/view/View;)V

    .line 734
    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 2

    .prologue
    .line 454
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 455
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 456
    :cond_0
    const/4 v0, 0x0

    .line 460
    :goto_0
    return-object v0

    .line 459
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public a()Laql;
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laql;

    return-object v0
.end method

.method public a(I)V
    .locals 5

    .prologue
    .line 426
    iget v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    if-gtz v0, :cond_1

    .line 446
    :cond_0
    :goto_0
    return-void

    .line 432
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laql;

    iget v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    iget v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    invoke-static {v0, v1, v2}, Laql;->a(Laql;II)I

    move-result v0

    .line 433
    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laql;

    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    invoke-static {v1, v2, v3}, Laql;->a(Laql;II)I

    move-result v1

    .line 434
    mul-int v2, v0, p1

    .line 435
    add-int/lit8 v3, p1, 0x1

    mul-int/2addr v0, v3

    sub-int/2addr v0, v1

    .line 438
    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laql;

    iget-object v3, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getScrollX()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getScrollY()I

    move-result v4

    invoke-static {v1, v3, v4}, Laql;->a(Laql;II)I

    move-result v1

    .line 439
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 440
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 443
    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laql;

    invoke-static {v1}, Laql;->a(Laql;)I

    move-result v1

    mul-int/2addr v1, v0

    .line 444
    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laql;

    invoke-static {v2}, Laql;->b(Laql;)I

    move-result v2

    mul-int/2addr v0, v2

    .line 445
    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laqr;

    invoke-interface {v2, v1, v0}, Laqr;->a(II)V

    goto :goto_0
.end method

.method public a(IIII)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 593
    iget-boolean v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Z

    if-eqz v0, :cond_0

    .line 605
    :goto_0
    return-void

    .line 597
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laql;

    invoke-static {v0, p1, p2}, Laql;->a(Laql;II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 598
    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laql;

    invoke-static {v1, p3, p4}, Laql;->a(Laql;II)I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 599
    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laql;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->getHeight()I

    move-result v4

    invoke-static {v2, v3, v4}, Laql;->a(Laql;II)I

    move-result v2

    .line 601
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b(II)V

    .line 602
    add-int v1, v0, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a(II)V

    .line 604
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b(I)V

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    .prologue
    .line 738
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 740
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 741
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->e()V

    .line 743
    :cond_0
    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 327
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    invoke-interface {p1}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_0
    const-string v2, "Adapters with multiple view types not supported."

    .line 328
    invoke-static {v0, v2}, LbiT;->a(ZLjava/lang/Object;)V

    .line 331
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/database/DataSetObserver;

    invoke-interface {v0, v2}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 335
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/database/DataSetObserver;

    invoke-interface {v0, v2}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 337
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a()V

    .line 338
    iput-boolean v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Z

    .line 339
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/os/Handler;

    new-instance v1, Laqc;

    invoke-direct {v1, p0}, Laqc;-><init>(Lcom/google/android/apps/docs/view/LinearLayoutListView;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 347
    return-void

    .line 329
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOnItemClickListener(Laqj;)V
    .locals 0

    .prologue
    .line 409
    iput-object p1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laqj;

    .line 410
    return-void
.end method

.method public setOnScrollListener(Laqk;)V
    .locals 0

    .prologue
    .line 419
    iput-object p1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laqk;

    .line 420
    return-void
.end method

.method public setOrientation(Laql;)V
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 360
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laql;

    invoke-virtual {v0, p1}, Laql;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 401
    :goto_0
    return-void

    .line 366
    :cond_0
    iget v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    if-eq v0, v1, :cond_1

    .line 367
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laql;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    .line 368
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getScrollX()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getScrollY()I

    move-result v2

    invoke-static {v0, v1, v2}, Laql;->a(Laql;II)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laql;

    iget v2, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:I

    iget v3, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b:I

    .line 369
    invoke-static {v1, v2, v3}, Laql;->a(Laql;II)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 374
    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->b()V

    .line 375
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->removeAllViews()V

    .line 376
    iput-object p1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Laql;

    .line 377
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->g()V

    .line 381
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Z

    .line 382
    iget-object v1, p0, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a:Landroid/os/Handler;

    new-instance v2, Laqd;

    invoke-direct {v2, p0, v0}, Laqd;-><init>(Lcom/google/android/apps/docs/view/LinearLayoutListView;F)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 371
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
