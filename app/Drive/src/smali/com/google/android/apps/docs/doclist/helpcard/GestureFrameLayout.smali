.class public Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;
.super Landroid/widget/FrameLayout;
.source "GestureFrameLayout.java"

# interfaces
.implements LaqS;


# instance fields
.field private a:LJv;

.field private a:Landroid/view/GestureDetector;

.field private a:Lcom/google/android/apps/docs/view/DocListView;

.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 39
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 59
    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;->a:Z

    .line 60
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;->requestDisallowInterceptTouchEvent(Z)V

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;->a:Lcom/google/android/apps/docs/view/DocListView;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;->a:Lcom/google/android/apps/docs/view/DocListView;

    if-nez p1, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/DocListView;->setDirectionScrollEnabled(Z)V

    .line 64
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 59
    goto :goto_0

    :cond_2
    move v1, v2

    .line 62
    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;->a(Z)V

    .line 56
    return-void
.end method

.method public a(I)Z
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x1

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 76
    const-string v1, "GestureFrameLayout"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onTouchEvent"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    invoke-static {p1}, LdH;->a(Landroid/view/MotionEvent;)I

    move-result v1

    .line 79
    if-nez v1, :cond_0

    .line 80
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;->a(Z)V

    .line 83
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;->a:Z

    if-eqz v2, :cond_2

    .line 84
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 104
    :cond_1
    :goto_0
    return v0

    .line 87
    :cond_2
    if-eq v1, v0, :cond_3

    const/4 v2, 0x3

    if-ne v1, v2, :cond_4

    .line 88
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;->a()V

    .line 91
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;->a:Landroid/view/GestureDetector;

    if-eqz v2, :cond_5

    .line 92
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;->a:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    .line 93
    if-nez v2, :cond_1

    .line 98
    :cond_5
    if-ne v0, v1, :cond_6

    .line 99
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;->a:LJv;

    if-eqz v1, :cond_6

    .line 100
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;->a:LJv;

    invoke-interface {v1, p1}, LJv;->a(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 104
    :cond_6
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setDocListView(Lcom/google/android/apps/docs/view/DocListView;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;->a:Lcom/google/android/apps/docs/view/DocListView;

    .line 72
    return-void
.end method

.method public setGestureListener(Landroid/view/GestureDetector$OnGestureListener;)V
    .locals 2

    .prologue
    .line 51
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;->a:Landroid/view/GestureDetector;

    .line 52
    return-void
.end method

.method public setOnUpListener(LJv;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/helpcard/GestureFrameLayout;->a:LJv;

    .line 68
    return-void
.end method
