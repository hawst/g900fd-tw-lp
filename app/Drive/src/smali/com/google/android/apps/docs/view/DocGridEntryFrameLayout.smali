.class public Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;
.super Landroid/widget/FrameLayout;
.source "DocGridEntryFrameLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method


# virtual methods
.method protected getContextMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 2

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocGridEntryFrameLayout;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHq;

    .line 41
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LbiT;->b(Z)V

    .line 42
    new-instance v1, Lapm;

    invoke-virtual {v0}, LHq;->a()LaGu;

    move-result-object v0

    invoke-direct {v1, v0}, Lapm;-><init>(LaGu;)V

    return-object v1

    .line 41
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
