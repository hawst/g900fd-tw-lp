.class public Lcom/google/android/apps/docs/app/NewMainProxyActivity;
.super Lajr;
.source "NewMainProxyActivity.java"

# interfaces
.implements Luh;


# instance fields
.field public a:LSF;

.field public a:LaGM;

.field public a:Lald;

.field private final a:Landroid/os/Bundle;

.field public a:Latd;

.field public a:Ljava/lang/Class;
    .annotation runtime Lbxv;
        a = "StartingActivityOnLaunch"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field public a:LqK;

.field public a:LsI;

.field public a:Lug;

.field public a:LxX;

.field private final b:Landroid/os/Handler;

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lajr;-><init>()V

    .line 101
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->b:Landroid/os/Handler;

    .line 107
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->i:Z

    .line 117
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Landroid/os/Bundle;

    return-void
.end method

.method public static a(Landroid/content/Context;LaFO;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 512
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 513
    const-string v0, "android.intent.action.MAIN"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 516
    const/high16 v0, 0x24000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 518
    const-string v2, "accountName"

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 520
    return-object v1

    .line 518
    :cond_0
    invoke-virtual {p1}, LaFO;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;LaFO;LCl;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 284
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    invoke-static {p0, p1}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;LaFO;)Landroid/content/Intent;

    move-result-object v0

    .line 288
    const-string v1, "launchingAction"

    sget-object v2, Luj;->b:Luj;

    invoke-virtual {v2}, Luj;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 289
    const-string v1, "mainFilter"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 290
    return-object v0
.end method

.method public static a(Landroid/content/Context;LaFO;LaFV;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 330
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 331
    const-class v1, Lcom/google/android/apps/docs/app/DocListActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 332
    const-string v1, "accountName"

    invoke-virtual {p1}, LaFO;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 333
    const-string v1, "collectionEntrySpec"

    invoke-interface {p2}, LaFV;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 335
    return-object v0
.end method

.method public static a(Landroid/content/Context;LaGM;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 301
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    iget-object v0, p2, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-static {p0, v0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;LaFO;)Landroid/content/Intent;

    move-result-object v0

    .line 304
    const-string v1, "launchingAction"

    sget-object v2, Luj;->c:Luj;

    invoke-virtual {v2}, Luj;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 310
    invoke-interface {p1, p2}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v1

    .line 311
    if-eqz v1, :cond_0

    .line 312
    const-string v2, "resourceId"

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 316
    :goto_0
    return-object v0

    .line 314
    :cond_0
    const-string v1, "entrySpecPayload"

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private static a(LaGM;Landroid/content/Intent;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 363
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 364
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, v0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(LaGM;Landroid/os/Bundle;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LaGM;Landroid/os/Bundle;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 4

    .prologue
    .line 377
    const-string v0, "entrySpec.v2"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 378
    if-nez v0, :cond_0

    .line 379
    const-string v1, "accountName"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v2

    .line 380
    if-eqz v2, :cond_0

    .line 381
    const-string v1, "resourceId"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 382
    const-string v1, "entrySpecPayload"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 383
    if-nez v1, :cond_1

    const/4 v1, 0x0

    .line 385
    :goto_0
    if-eqz v3, :cond_2

    .line 386
    invoke-static {v2, v3}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a(LaFO;Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    .line 387
    invoke-interface {p0, v0}, LaGM;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 393
    :cond_0
    :goto_1
    return-object v0

    .line 384
    :cond_1
    invoke-interface {p0, v2, v1}, LaGM;->a(LaFO;Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    goto :goto_0

    .line 388
    :cond_2
    if-eqz v1, :cond_0

    move-object v0, v1

    .line 389
    goto :goto_1
.end method

.method public static a(LaFO;LpW;)V
    .locals 3

    .prologue
    .line 446
    const-string v0, "FirstTimeDrive"

    .line 449
    :try_start_0
    invoke-interface {p1, p0}, LpW;->a(LaFO;)LpU;

    move-result-object v1

    .line 450
    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, LpU;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 458
    :goto_0
    return-void

    .line 453
    :cond_0
    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, LpU;->a(Ljava/lang/String;Z)V

    .line 454
    invoke-interface {p1, v1}, LpW;->a(LpU;)V
    :try_end_0
    .catch LpX; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 455
    :catch_0
    move-exception v0

    .line 456
    const-string v0, "MainProxyActivity"

    const-string v1, "Failed to save account preference."

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static a(LaGM;Lrm;LtK;LaFO;)V
    .locals 4

    .prologue
    .line 399
    invoke-virtual {p1}, Lrm;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 400
    invoke-static {v1}, Lui;->a(Landroid/content/Intent;)Lui;

    move-result-object v2

    .line 401
    const-string v0, "launchingAction"

    sget-object v3, Luj;->d:Luj;

    .line 402
    invoke-static {v1, v0, v3}, Lakt;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Luj;

    .line 404
    sget-object v3, Luj;->b:Luj;

    invoke-virtual {v0, v3}, Luj;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 405
    const-string v0, "mainFilter"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LCl;

    .line 407
    invoke-static {p1, p3, v0, v2}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;LaFO;LCl;Lui;)V

    .line 420
    :cond_0
    :goto_0
    return-void

    .line 408
    :cond_1
    sget-object v3, Luj;->c:Luj;

    invoke-virtual {v0, v3}, Luj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 409
    invoke-static {p0, v1}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(LaGM;Landroid/content/Intent;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 410
    if-eqz v0, :cond_2

    .line 411
    invoke-static {p1, p0, p2, v0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;LaGM;LtK;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    goto :goto_0

    .line 413
    :cond_2
    const/4 v0, 0x0

    invoke-static {p1, p3, v0, v2}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;LaFO;LCl;Lui;)V

    goto :goto_0

    .line 415
    :cond_3
    sget-object v0, Lui;->b:Lui;

    if-ne v2, v0, :cond_0

    .line 416
    invoke-virtual {p1}, Lrm;->a()LsI;

    move-result-object v0

    .line 417
    invoke-interface {v0, p3}, LsI;->a(LaFO;)Landroid/content/Intent;

    move-result-object v0

    .line 418
    invoke-virtual {p1, v0}, Lrm;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;LaFO;LCl;Lui;)V
    .locals 3

    .prologue
    .line 424
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/app/DocListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 425
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 426
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 427
    const-string v1, "dialogToShow"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 428
    const-string v1, "accountName"

    invoke-virtual {p1}, LaFO;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 429
    if-eqz p2, :cond_0

    .line 430
    const-string v1, "mainFilter"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 431
    const-string v1, "docListTitle"

    invoke-interface {p2}, LCl;->a()I

    move-result v2

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 433
    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 434
    return-void
.end method

.method private static a(Landroid/content/Context;LaGM;LtK;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1

    .prologue
    .line 440
    invoke-static {p0, p1, p2, p3}, Lala;->a(Landroid/content/Context;LaGM;LtK;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    .line 442
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 443
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/NewMainProxyActivity;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->e()V

    return-void
.end method

.method public static b(Landroid/content/Context;LaFO;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 531
    invoke-static {p0, p1}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;LaFO;)Landroid/content/Intent;

    move-result-object v0

    .line 534
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 541
    const-string v1, "wasTaskRoot"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 542
    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/NewMainProxyActivity;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->f()V

    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 160
    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v1

    .line 163
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 164
    if-eqz v0, :cond_0

    .line 165
    iget-object v2, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Landroid/os/Bundle;

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Landroid/os/Bundle;

    const-string v2, "launchingAction"

    sget-object v3, Luj;->d:Luj;

    .line 169
    invoke-static {v0, v2, v3}, Lakt;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Luj;

    .line 170
    iget-object v2, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Lug;

    invoke-virtual {v2, v1, v0, p0}, Lug;->a(LaFO;Luj;Luh;)V

    .line 171
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->b:Landroid/os/Handler;

    new-instance v1, LuO;

    invoke-direct {v1, p0}, LuO;-><init>(Lcom/google/android/apps/docs/app/NewMainProxyActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 269
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 340
    new-instance v0, LuP;

    invoke-direct {v0, p0}, LuP;-><init>(Lcom/google/android/apps/docs/app/NewMainProxyActivity;)V

    .line 358
    iget-object v1, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:LSF;

    const-string v2, "com.google"

    invoke-interface {v1, v2, p0, v0}, LSF;->a(Ljava/lang/String;Landroid/app/Activity;LSG;)V

    .line 359
    return-void
.end method

.method public a(LaFO;Lui;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 181
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 183
    iget-object v1, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Ljava/lang/Class;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 185
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 187
    iget-object v1, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 188
    const-string v1, "accountName"

    invoke-virtual {p1}, LaFO;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 189
    const-string v1, "triggerSync"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 192
    sget-object v1, Lui;->a:Lui;

    if-ne p2, v1, :cond_0

    const-string v1, "dialogToShow"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 193
    :cond_0
    const-string v1, "dialogToShow"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 196
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->i:Z

    if-eqz v1, :cond_2

    .line 197
    const-string v1, "appLaunch"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 200
    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->startActivity(Landroid/content/Intent;)V

    .line 201
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->f()V

    .line 202
    return-void
.end method

.method public b(LaFO;Lui;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Landroid/os/Bundle;

    const-string v1, "launchingAction"

    sget-object v4, Luj;->d:Luj;

    .line 207
    invoke-static {v0, v1, v4}, Lakt;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Luj;

    .line 209
    sget-object v1, Luj;->c:Luj;

    .line 210
    invoke-virtual {v0, v1}, Luj;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    move v1, v2

    :goto_0
    const-string v4, "Should not restore stack with OPEN_ENTRY"

    .line 209
    invoke-static {v1, v4}, LbiT;->b(ZLjava/lang/Object;)V

    .line 212
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 219
    invoke-virtual {v1}, Landroid/content/Intent;->getFlags()I

    move-result v1

    const/high16 v4, 0x100000

    and-int/2addr v1, v4

    if-eqz v1, :cond_0

    .line 220
    sget-object v0, Luj;->d:Luj;

    .line 225
    :cond_0
    sget-object v1, Luj;->b:Luj;

    invoke-virtual {v0, v1}, Luj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 226
    iget-object v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Landroid/os/Bundle;

    const-string v1, "mainFilter"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LCl;

    .line 228
    invoke-static {p0, p1, v0, p2}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;LaFO;LCl;Lui;)V

    .line 249
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->f()V

    .line 254
    if-eqz v3, :cond_2

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->b:Landroid/os/Handler;

    new-instance v1, LuN;

    invoke-direct {v1, p0}, LuN;-><init>(Lcom/google/android/apps/docs/app/NewMainProxyActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 261
    :cond_2
    return-void

    :cond_3
    move v1, v3

    .line 210
    goto :goto_0

    .line 230
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Landroid/os/Bundle;

    const-string v1, "dialogToShow"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lui;

    .line 232
    sget-object v1, Lui;->a:Lui;

    if-ne p2, v1, :cond_5

    if-eqz v0, :cond_5

    move-object p2, v0

    .line 236
    :cond_5
    sget-object v0, Lui;->b:Lui;

    if-ne p2, v0, :cond_6

    .line 237
    iget-object v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:LsI;

    .line 238
    invoke-interface {v0, p1}, LsI;->a(LaFO;)Landroid/content/Intent;

    move-result-object v0

    .line 239
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 240
    :cond_6
    sget-object v0, Lui;->c:Lui;

    if-ne p2, v0, :cond_1

    move v3, v2

    .line 241
    goto :goto_1
.end method

.method public isTaskRoot()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 175
    invoke-super {p0}, Lajr;->isTaskRoot()Z

    move-result v1

    .line 176
    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "wasTaskRoot"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 121
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, LalN;->a(Landroid/content/Intent;)V

    .line 122
    invoke-super {p0, p1}, Lajr;->onCreate(Landroid/os/Bundle;)V

    .line 123
    const-string v0, "MainProxyActivity"

    const-string v1, "in onCreate"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:LqK;

    invoke-virtual {v0}, LqK;->a()V

    .line 126
    if-nez p1, :cond_0

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:LqK;

    invoke-virtual {v0, p0}, LqK;->a(Landroid/app/Activity;)V

    .line 133
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 134
    invoke-virtual {v0}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v1

    .line 135
    if-eqz v1, :cond_1

    const-string v2, "android.intent.category.LAUNCHER"

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 136
    iput-boolean v3, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->i:Z

    .line 137
    const-string v1, "MainProxyActivity"

    const-string v2, "Launcher clicked"

    invoke-static {v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    iget-object v1, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:LqK;

    const-string v2, "/launcherClicked"

    invoke-virtual {v1, v2, v0}, LqK;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 140
    :cond_1
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 143
    iput-boolean v3, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->i:Z

    .line 146
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:LxX;

    invoke-interface {v0}, LxX;->a()V

    .line 148
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->e()V

    .line 149
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:LqK;

    invoke-virtual {v0}, LqK;->b()V

    .line 154
    invoke-super {p0}, Lajr;->onDestroy()V

    .line 155
    return-void
.end method
