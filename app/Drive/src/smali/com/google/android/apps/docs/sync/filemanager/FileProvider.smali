.class public Lcom/google/android/apps/docs/sync/filemanager/FileProvider;
.super Landroid/content/ContentProvider;
.source "FileProvider.java"


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LadA;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 186
    invoke-static {}, LboS;->a()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/docs/sync/filemanager/FileProvider;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 162
    return-void
.end method

.method private a(Landroid/net/Uri;)LadC;
    .locals 4

    .prologue
    .line 246
    invoke-static {p1}, LadH;->a(Landroid/net/Uri;)LadH;

    move-result-object v1

    .line 248
    const-class v2, Lcom/google/android/apps/docs/sync/filemanager/FileProvider;

    monitor-enter v2

    .line 249
    :try_start_0
    sget-object v0, Lcom/google/android/apps/docs/sync/filemanager/FileProvider;->a:Ljava/util/Map;

    invoke-virtual {v1}, LadH;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LadA;

    .line 250
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    if-nez v0, :cond_0

    .line 253
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No file was found for uri "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 250
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 255
    :cond_0
    invoke-virtual {v1}, LadH;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LadA;->a(Ljava/lang/String;)LadC;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized a(LadA;)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 197
    const-class v1, Lcom/google/android/apps/docs/sync/filemanager/FileProvider;

    monitor-enter v1

    :cond_0
    :try_start_0
    invoke-static {}, Lbjh;->a()Ljava/security/SecureRandom;

    move-result-object v0

    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    .line 198
    sget-object v2, Lcom/google/android/apps/docs/sync/filemanager/FileProvider;->a:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 199
    invoke-static {v0, p0}, Lcom/google/android/apps/docs/sync/filemanager/FileProvider;->a(Ljava/lang/String;LadA;)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 197
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(Ljava/lang/String;LadA;)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 206
    const-class v1, Lcom/google/android/apps/docs/sync/filemanager/FileProvider;

    monitor-enter v1

    :try_start_0
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    sget-object v0, Lcom/google/android/apps/docs/sync/filemanager/FileProvider;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 209
    sget-object v0, Lcom/google/android/apps/docs/sync/filemanager/FileProvider;->a:Ljava/util/Map;

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    sget-object v0, LaEG;->k:LaEG;

    invoke-virtual {v0}, LaEG;->a()Landroid/net/Uri;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 208
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 206
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 230
    const-class v1, Lcom/google/android/apps/docs/sync/filemanager/FileProvider;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, LadH;->a(Landroid/net/Uri;)LadH;

    move-result-object v0

    .line 231
    sget-object v2, Lcom/google/android/apps/docs/sync/filemanager/FileProvider;->a:Ljava/util/Map;

    invoke-virtual {v0}, LadH;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LadA;

    .line 232
    if-nez v0, :cond_0

    .line 233
    const-string v0, "FileProvider"

    const-string v2, "No file was found for uri %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-static {v0, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 238
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 235
    :catch_0
    move-exception v0

    .line 236
    :try_start_1
    const-string v0, "FileProvider"

    const-string v2, "No file was found for uri %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-static {v0, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 230
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 313
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 277
    sget-object v1, LaEG;->k:LaEG;

    invoke-virtual {v1}, LaEG;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 278
    const-string v0, "application/octet-stream"

    .line 289
    :goto_0
    return-object v0

    .line 280
    :cond_0
    const-class v2, Lcom/google/android/apps/docs/sync/filemanager/FileProvider;

    monitor-enter v2

    .line 283
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/sync/filemanager/FileProvider;->a(Landroid/net/Uri;)LadC;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 284
    :try_start_1
    invoke-interface {v1}, LadC;->a()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v0

    .line 288
    if-eqz v1, :cond_1

    .line 289
    :try_start_2
    invoke-interface {v1}, LadC;->close()V

    :cond_1
    monitor-exit v2

    goto :goto_0

    .line 292
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 285
    :catch_0
    move-exception v1

    move-object v1, v0

    .line 288
    :goto_1
    if-eqz v1, :cond_2

    .line 289
    :try_start_3
    invoke-interface {v1}, LadC;->close()V

    :cond_2
    monitor-exit v2

    goto :goto_0

    .line 288
    :catchall_1
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_2
    if-eqz v1, :cond_3

    .line 289
    invoke-interface {v1}, LadC;->close()V

    :cond_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 288
    :catchall_2
    move-exception v0

    goto :goto_2

    .line 285
    :catch_1
    move-exception v3

    goto :goto_1
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 318
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x1

    return v0
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 6

    .prologue
    .line 298
    const-class v1, Lcom/google/android/apps/docs/sync/filemanager/FileProvider;

    monitor-enter v1

    .line 299
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/sync/filemanager/FileProvider;->a(Landroid/net/Uri;)LadC;

    move-result-object v0

    .line 300
    const-string v2, "FileProvider"

    const-string v3, "openFile: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 302
    :try_start_1
    invoke-static {p2}, LadI;->a(Ljava/lang/String;)LbmY;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/docs/sync/filemanager/FileProvider$WrappedParcelFileDescriptor;->a(LadC;LbmY;)Lcom/google/android/apps/docs/sync/filemanager/FileProvider$WrappedParcelFileDescriptor;
    :try_end_1
    .catch LadB; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :try_start_2
    monitor-exit v1

    return-object v0

    .line 303
    :catch_0
    move-exception v0

    .line 304
    new-instance v2, Ljava/lang/SecurityException;

    invoke-direct {v2, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 308
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 305
    :catch_1
    move-exception v0

    .line 306
    :try_start_3
    new-instance v2, Ljava/io/FileNotFoundException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 325
    .line 327
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/sync/filemanager/FileProvider;->a(Landroid/net/Uri;)LadC;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 328
    if-nez p2, :cond_0

    .line 329
    const/4 v0, 0x3

    :try_start_1
    new-array p2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "_size"

    aput-object v4, p2, v0

    const/4 v0, 0x1

    const-string v4, "mime_type"

    aput-object v4, p2, v0

    const/4 v0, 0x2

    const-string v4, "_display_name"

    aput-object v4, p2, v0

    .line 332
    :cond_0
    new-instance v0, Landroid/database/MatrixCursor;

    invoke-direct {v0, p2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 333
    invoke-virtual {v0}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v4

    .line 334
    array-length v5, p2

    :goto_0
    if-ge v3, v5, :cond_7

    aget-object v6, p2, v3

    .line 336
    const-string v7, "_size"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 337
    invoke-interface {v2}, LadC;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 334
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 338
    :cond_1
    const-string v7, "_display_name"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 339
    invoke-interface {v2}, LadC;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 348
    :catch_0
    move-exception v0

    move-object v0, v2

    .line 351
    :goto_2
    if-eqz v0, :cond_2

    .line 352
    invoke-interface {v0}, LadC;->close()V

    :cond_2
    move-object v0, v1

    :cond_3
    :goto_3
    return-object v0

    .line 340
    :cond_4
    :try_start_2
    const-string v7, "mime_type"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 341
    invoke-interface {v2}, LadC;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 351
    :catchall_0
    move-exception v0

    :goto_4
    if-eqz v2, :cond_5

    .line 352
    invoke-interface {v2}, LadC;->close()V

    :cond_5
    throw v0

    .line 343
    :cond_6
    :try_start_3
    const-string v6, ""

    invoke-virtual {v4, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 351
    :cond_7
    if-eqz v2, :cond_3

    .line 352
    invoke-interface {v2}, LadC;->close()V

    goto :goto_3

    .line 351
    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_4

    .line 348
    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_2
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 359
    const/4 v0, 0x0

    return v0
.end method
