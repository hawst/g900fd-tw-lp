.class public Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;
.super Ljava/lang/Object;
.source "CriterionSetImpl.java"

# interfaces
.implements Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/lang/String;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/Criterion;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 196
    new-instance v0, LvQ;

    invoke-direct {v0}, LvQ;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/Criterion;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    .line 47
    return-void
.end method

.method private a(Z)LbmF;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LbmF",
            "<",
            "LCl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v1

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    .line 153
    instance-of v3, v0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;

    if-eqz v3, :cond_0

    .line 154
    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;

    .line 155
    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->b()Z

    move-result v3

    if-nez v3, :cond_1

    if-nez p1, :cond_0

    .line 158
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->a()LCl;

    move-result-object v0

    invoke-virtual {v1, v0}, LbmH;->a(Ljava/lang/Object;)LbmH;

    goto :goto_0

    .line 161
    :cond_2
    invoke-virtual {v1}, LbmH;->a()LbmF;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()LCl;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 172
    invoke-direct {p0, v5}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a(Z)LbmF;

    move-result-object v0

    .line 173
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v5, :cond_0

    .line 174
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "More than one main filter : %s, %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 175
    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v3, v5

    .line 174
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 177
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCl;

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()LaFO;
    .locals 3

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    .line 222
    instance-of v2, v0, Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;

    if-eqz v2, :cond_0

    .line 223
    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;->a()LaFO;

    move-result-object v0

    return-object v0

    .line 227
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Criteria without account name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(LaGM;)LaeZ;
    .locals 3

    .prologue
    .line 139
    sget-object v0, LaeZ;->a:LaeZ;

    .line 141
    iget-object v1, p0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    .line 143
    invoke-interface {v0, p1}, Lcom/google/android/apps/docs/app/model/navigation/Criterion;->a(LaGM;)LaeZ;

    move-result-object v0

    invoke-virtual {v1, v0}, LaeZ;->a(LaeZ;)LaeZ;

    move-result-object v0

    move-object v1, v0

    .line 144
    goto :goto_0

    .line 146
    :cond_0
    return-object v1
.end method

.method public a()LbmF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmF",
            "<",
            "LCl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a(Z)LbmF;

    move-result-object v0

    return-object v0
.end method

.method public a()Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 3

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    .line 97
    instance-of v2, v0, Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;

    if-eqz v2, :cond_0

    .line 98
    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 102
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LaGM;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 3

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 108
    if-nez v0, :cond_0

    sget-object v1, LCe;->q:LCe;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a()LCl;

    move-result-object v2

    invoke-virtual {v1, v2}, LCe;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 110
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a()LaFO;

    move-result-object v0

    invoke-interface {p1, v0}, LaGM;->a(LaFO;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    :try_end_0
    .catch LaGT; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 115
    :cond_0
    return-object v0

    .line 111
    :catch_0
    move-exception v0

    .line 112
    new-instance v1, Ljava/lang/AssertionError;

    const-string v2, "Failed to get root collection EntrySpec."

    invoke-direct {v1, v2, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(LvR;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LvR",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    .line 214
    invoke-interface {v0, p1}, Lcom/google/android/apps/docs/app/model/navigation/Criterion;->a(LvR;)V

    goto :goto_0

    .line 216
    :cond_0
    invoke-interface {p1}, LvR;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    .line 86
    instance-of v2, v0, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;

    if-eqz v2, :cond_0

    .line 87
    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;->a()Ljava/lang/String;

    move-result-object v0

    .line 91
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LaGM;Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    invoke-static {}, LamV;->b()V

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a:Ljava/lang/String;

    .line 75
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lwf;

    invoke-direct {v0, p1, p2}, Lwf;-><init>(LaGM;Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a(LvR;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public a()LvN;
    .locals 2

    .prologue
    .line 80
    new-instance v0, LvN;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-direct {v0, v1}, LvN;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public a()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 232
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    .line 233
    instance-of v3, v0, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;

    if-eqz v3, :cond_1

    move v0, v1

    .line 240
    :goto_0
    return v0

    .line 236
    :cond_1
    instance-of v0, v0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;

    if-eqz v0, :cond_0

    move v0, v1

    .line 237
    goto :goto_0

    .line 240
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)Z
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)Z
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    .line 62
    invoke-interface {p1, v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 63
    const/4 v0, 0x0

    .line 66
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 187
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 126
    if-ne p0, p1, :cond_1

    .line 132
    :cond_0
    :goto_0
    return v0

    .line 128
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;

    if-eqz v2, :cond_3

    .line 129
    check-cast p1, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;

    .line 130
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1, p0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 132
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 121
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-static {v2}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/Criterion;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 182
    const-string v0, "CriterionSet %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 192
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 194
    return-void
.end method
