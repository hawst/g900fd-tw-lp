.class public Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;
.super Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;
.source "GridViewSlidePickerFragment.java"


# instance fields
.field private a:I

.field private a:Landroid/widget/FrameLayout;

.field a:Landroid/widget/GridView;

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;-><init>()V

    return-void
.end method

.method public static a()Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;-><init>()V

    return-object v0
.end method

.method private t()V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/GridView;

    new-instance v1, LXe;

    invoke-direct {v1, p0}, LXe;-><init>(Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 144
    return-void
.end method

.method private u()V
    .locals 2

    .prologue
    .line 147
    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a()LH;

    move-result-object v0

    invoke-virtual {v0}, LH;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, LpO;->punch_grid_view_slide_picker_columns:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:I

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/GridView;

    iget v1, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 150
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, -0x1

    const/4 v2, 0x0

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->d:Landroid/view/View;

    if-nez v0, :cond_1

    .line 52
    sget v0, LpP;->punch_grid_view_slide_picker:I

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->d:Landroid/view/View;

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->d:Landroid/view/View;

    sget v3, LpN;->grid_view:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/GridView;

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 57
    instance-of v3, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v3, :cond_0

    .line 58
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    sget-object v3, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    sget-object v4, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v0, v3, v4}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeXY(Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/GridView;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 64
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->t()V

    .line 66
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->u()V

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    if-nez v0, :cond_3

    :goto_1
    invoke-static {v1}, LbiT;->b(Z)V

    .line 69
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a()LH;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/GridView;

    new-instance v1, LXd;

    invoke-direct {v1, p0}, LXd;-><init>(Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    return-object v0

    :cond_2
    move v0, v2

    .line 61
    goto :goto_0

    :cond_3
    move v1, v2

    .line 68
    goto :goto_1
.end method

.method protected a(I)V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/GridView;

    invoke-virtual {v0, p1}, Landroid/widget/GridView;->setSelection(I)V

    .line 110
    return-void
.end method

.method protected a(Landroid/widget/ListAdapter;)V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/GridView;

    invoke-virtual {v0, p1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 105
    return-void
.end method

.method public c(I)V
    .locals 5

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/GridView;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 122
    if-nez v0, :cond_0

    .line 123
    const-string v0, "GridViewSlidePickerFragment"

    const-string v1, "Aborting onThumbnailAvailable. Could not find view for slideIndex=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 124
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 123
    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 129
    :goto_0
    return-void

    .line 128
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:LYb;

    invoke-virtual {v1, v0, p1}, LYb;->a(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    .line 89
    invoke-super {p0}, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->e()V

    .line 90
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 114
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 116
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->u()V

    .line 117
    return-void
.end method
