.class public abstract Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;
.super Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;
.source "ContactSharingDialogFragment.java"


# static fields
.field private static final a:Lqt;


# instance fields
.field private a:LacD;

.field private a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "LabO;",
            ">;"
        }
    .end annotation
.end field

.field public b:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Lacr;",
            ">;"
        }
    .end annotation
.end field

.field public b:LqK;

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lqt;->b:Lqt;

    sput-object v0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:Lqt;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;-><init>()V

    return-void
.end method

.method private u()V
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->b:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacr;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Lacr;->a(Ljava/lang/String;)LacD;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:LacD;

    .line 149
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 4

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->u()V

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:LacD;

    if-nez v0, :cond_1

    .line 155
    const-string v0, "ContactSharingDialogFragment"

    const-string v1, "Selected item in contact sharing dialog is not defined."

    invoke-static {v0, v1}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 158
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:LacD;

    invoke-virtual {v0}, LacD;->a()Laci;

    move-result-object v1

    .line 159
    invoke-static {v1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:LbmF;

    invoke-virtual {v0, p1}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LabO;

    .line 161
    invoke-virtual {v0}, LabO;->a()Lqt;

    move-result-object v0

    .line 162
    invoke-virtual {v1}, Laci;->a()Lqt;

    move-result-object v2

    if-eq v0, v2, :cond_0

    .line 165
    new-instance v2, Laci;

    .line 167
    invoke-virtual {v1}, Laci;->b()Z

    move-result v3

    invoke-direct {v2, v1, v0, v3}, Laci;-><init>(Laci;Lqt;Z)V

    .line 168
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:LacD;

    invoke-virtual {v1, v2}, LacD;->a(Laci;)V

    .line 170
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->b:LqK;

    const-string v2, "sharing"

    const-string v3, "changeSharingPermissions"

    .line 171
    invoke-virtual {v0}, Lqt;->name()Ljava/lang/String;

    move-result-object v0

    .line 170
    invoke-virtual {v1, v2, v3, v0}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->t()V

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;LajO;LaGM;Landroid/content/Context;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 76
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 79
    invoke-static {p1, p2, p3}, LabO;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;LajO;LaGM;)LbmF;

    move-result-object v3

    .line 83
    invoke-static {p4, v3}, LabO;->a(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 85
    sget v4, Lxi;->dialog_contact_sharing:I

    new-array v1, v1, [Ljava/lang/String;

    .line 86
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 85
    invoke-static {v2, v4, v0}, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a(Landroid/os/Bundle;I[Ljava/lang/String;)V

    .line 88
    const-string v0, "shareeAccountName"

    invoke-virtual {v2, v0, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string v0, "options"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 90
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->e(Landroid/os/Bundle;)V

    .line 91
    return-void

    :cond_0
    move v0, v1

    .line 75
    goto :goto_0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 96
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a_(Landroid/os/Bundle;)V

    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "options"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 100
    invoke-static {v0}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:LbmF;

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->b:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 106
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a()V

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "shareeAccountName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->c:Ljava/lang/String;

    .line 111
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->u()V

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:LacD;

    if-nez v0, :cond_2

    .line 114
    const-string v0, "ContactSharingDialogFragment"

    const-string v1, "No contact exists in Acl list with email address \"%s\"."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->c:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 116
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a()V

    goto :goto_0

    .line 120
    :cond_2
    if-nez p1, :cond_0

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:LacD;

    invoke-virtual {v0}, LacD;->a()Laci;

    move-result-object v0

    .line 122
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    invoke-virtual {v0}, Laci;->a()Lqt;

    move-result-object v0

    .line 124
    sget-object v1, Lqt;->f:Lqt;

    if-ne v0, v1, :cond_3

    .line 125
    sget-object v0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:Lqt;

    .line 128
    :cond_3
    invoke-static {v0}, LabO;->a(Lqt;)LabO;

    move-result-object v1

    .line 129
    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:LbmF;

    invoke-virtual {v2, v1}, LbmF;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 130
    if-gez v1, :cond_4

    .line 134
    invoke-virtual {v0}, Lqt;->a()Lqv;

    move-result-object v0

    new-array v1, v4, [Lqq;

    invoke-static {v0, v1}, Lqt;->a(Lqv;[Lqq;)Lqt;

    move-result-object v0

    .line 135
    invoke-static {v0}, LabO;->a(Lqt;)LabO;

    move-result-object v0

    .line 136
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a:LbmF;

    invoke-virtual {v1, v0}, LbmF;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 139
    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->b(I)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public abstract t()V
.end method
