.class public Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "VersionCheckDialogFragment.java"


# instance fields
.field public a:LQr;

.field public a:Lald;

.field public a:Landroid/content/Context;

.field public a:Lvv;

.field public c:Ljava/lang/String;
    .annotation runtime Lbxv;
        a = "marketFlag"
    .end annotation
.end field

.field public m:I
    .annotation runtime Lbxv;
        a = "tooOldTitle"
    .end annotation
.end field

.field public n:I
    .annotation runtime Lbxv;
        a = "tooOldMessage"
    .end annotation
.end field

.field public o:I
    .annotation runtime Lbxv;
        a = "tooOldClose"
    .end annotation
.end field

.field public p:I
    .annotation runtime Lbxv;
        a = "tooOldUpgrade"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    return-void
.end method

.method public static a(LM;)V
    .locals 2

    .prologue
    .line 122
    new-instance v0, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;-><init>()V

    .line 123
    const-string v1, "VersionCheck"

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a(LM;Ljava/lang/String;)V

    .line 124
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/high16 v4, 0x20000

    .line 73
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a()LH;

    move-result-object v0

    invoke-static {v0}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v0

    .line 74
    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->m:I

    .line 75
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->n:I

    .line 76
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    .line 77
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->o:I

    new-instance v3, Lvx;

    invoke-direct {v3, p0}, Lvx;-><init>(Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;)V

    .line 78
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->p:I

    new-instance v3, Lvw;

    invoke-direct {v3, p0}, Lvw;-><init>(Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;)V

    .line 85
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 107
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 108
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 110
    return-object v0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a_(Landroid/os/Bundle;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a:Lvv;

    invoke-interface {v0}, Lvv;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a()V

    .line 69
    :cond_0
    return-void
.end method
