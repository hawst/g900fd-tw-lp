.class public abstract Lcom/google/android/apps/docs/utils/AbstractParcelableTask;
.super Ljava/lang/Object;
.source "AbstractParcelableTask.java"

# interfaces
.implements Lcom/google/android/apps/docs/utils/ParcelableTask;


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/utils/AbstractParcelableTask;->a:Z

    .line 14
    return-void
.end method


# virtual methods
.method public abstract a(Laju;)V
.end method

.method public final b(Laju;)V
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/google/android/apps/docs/utils/AbstractParcelableTask;->a:Z

    if-nez v0, :cond_0

    .line 24
    invoke-interface {p1}, Laju;->a()Lbuu;

    move-result-object v0

    .line 25
    invoke-interface {v0, p0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/utils/AbstractParcelableTask;->a:Z

    .line 28
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/utils/AbstractParcelableTask;->a(Laju;)V

    .line 29
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    return v0
.end method
