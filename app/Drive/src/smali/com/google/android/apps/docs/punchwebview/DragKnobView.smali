.class public Lcom/google/android/apps/docs/punchwebview/DragKnobView;
.super Landroid/widget/RelativeLayout;
.source "DragKnobView.java"


# instance fields
.field private a:LXb;

.field final a:Landroid/content/Context;

.field private final a:Landroid/os/Handler;

.field private a:Landroid/view/MotionEvent;

.field private a:Landroid/widget/ImageView;

.field private a:Z

.field private b:Landroid/view/MotionEvent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 51
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:Landroid/os/Handler;

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:Z

    .line 59
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:Landroid/content/Context;

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:Landroid/os/Handler;

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:Z

    .line 69
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:Landroid/content/Context;

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:Landroid/os/Handler;

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:Z

    .line 64
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:Landroid/content/Context;

    .line 65
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/DragKnobView;)LXb;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:LXb;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/DragKnobView;)Landroid/view/MotionEvent;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:Landroid/view/MotionEvent;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/DragKnobView;Z)Z
    .locals 0

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/punchwebview/DragKnobView;)Landroid/view/MotionEvent;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->b:Landroid/view/MotionEvent;

    return-object v0
.end method


# virtual methods
.method public a(LXb;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:LXb;

    .line 74
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x1

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 78
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:LXb;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:LXb;

    invoke-interface {v0}, LXb;->a()V

    .line 82
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 86
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:LXb;

    if-nez v2, :cond_0

    .line 122
    :goto_0
    return v0

    .line 90
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 91
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:Landroid/view/MotionEvent;

    .line 92
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:Z

    if-eqz v0, :cond_1

    move v0, v1

    .line 93
    goto :goto_0

    .line 96
    :cond_1
    iput-boolean v1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:Z

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:Landroid/os/Handler;

    new-instance v2, LWZ;

    invoke-direct {v2, p0}, LWZ;-><init>(Lcom/google/android/apps/docs/punchwebview/DragKnobView;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move v0, v1

    .line 106
    goto :goto_0

    .line 109
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_3

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:LXb;

    invoke-interface {v0, p1}, LXb;->a(Landroid/view/MotionEvent;)V

    move v0, v1

    .line 111
    goto :goto_0

    .line 114
    :cond_3
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->b:Landroid/view/MotionEvent;

    .line 115
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:Landroid/os/Handler;

    new-instance v2, LXa;

    invoke-direct {v2, p0}, LXa;-><init>(Lcom/google/android/apps/docs/punchwebview/DragKnobView;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public setKnobPictureContentDescription(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 132
    sget v0, LpN;->drag_knob_pic:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:Landroid/widget/ImageView;

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 136
    return-void
.end method
