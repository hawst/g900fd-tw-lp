.class public abstract Lcom/google/android/apps/docs/app/AbstractModalDialogActivity;
.super Lrm;
.source "AbstractModalDialogActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lrm;-><init>()V

    .line 21
    return-void
.end method


# virtual methods
.method protected abstract a()Lcom/google/android/apps/docs/app/AbstractModalDialogActivity$ModalDialogFragment;
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 41
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 43
    if-nez p1, :cond_0

    .line 45
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/AbstractModalDialogActivity;->setResult(I)V

    .line 47
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AbstractModalDialogActivity;->a()Lcom/google/android/apps/docs/app/AbstractModalDialogActivity$ModalDialogFragment;

    move-result-object v0

    .line 48
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AbstractModalDialogActivity;->a()LM;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/app/AbstractModalDialogActivity$ModalDialogFragment;->a(LM;Ljava/lang/String;)V

    .line 50
    :cond_0
    return-void
.end method
