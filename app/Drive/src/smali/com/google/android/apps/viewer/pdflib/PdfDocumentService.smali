.class public Lcom/google/android/apps/viewer/pdflib/PdfDocumentService;
.super Landroid/app/Service;
.source "PdfDocumentService.java"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/viewer/pdflib/PdfDocumentService;->a:Z

    .line 46
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/pdflib/PdfDocumentService;)Z
    .locals 1

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/google/android/apps/viewer/pdflib/PdfDocumentService;->a:Z

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/pdflib/PdfDocumentService;Z)Z
    .locals 0

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/google/android/apps/viewer/pdflib/PdfDocumentService;->a:Z

    return p1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 5

    .prologue
    .line 29
    invoke-static {}, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->loadLibfoxit()V

    .line 30
    const-string v0, "PdfDocumentService"

    const-string v1, "Starting %s [%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "PdfDocumentService"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    new-instance v0, LavI;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LavI;-><init>(Lcom/google/android/apps/viewer/pdflib/PdfDocumentService;LavH;)V

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 42
    const-string v0, "PdfDocumentService"

    const-string v1, "Stopping PdfDocumentService"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 44
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 5

    .prologue
    .line 36
    const-string v0, "PdfDocumentService"

    const-string v1, "Unbind %s [%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "PdfDocumentService"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
