.class public Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;
.super LyR;
.source "LinkSharingCard.java"

# interfaces
.implements Laco;


# annotations
.annotation runtime LaiC;
.end annotation

.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field private static a:I

.field private static final a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "Lqt;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Lqu;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "Lqt;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LM;

.field private a:LaGu;

.field private final a:LaKR;

.field private final a:LabQ;

.field private final a:Lach;

.field private final a:Lacj;

.field private a:Lacr;

.field private final a:Landroid/content/ClipboardManager;

.field private final a:Landroid/content/Context;

.field private final a:Landroid/view/View;

.field private final a:Landroid/widget/TextView;

.field private final a:Lcom/google/android/apps/docs/common/view/RoundImageView;

.field private final a:LvU;

.field private a:Z

.field private b:Z

.field private final c:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 58
    sget-object v0, Lqu;->a:Lqu;

    sget-object v1, Lqu;->n:Lqu;

    .line 59
    invoke-static {v0, v1}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmY;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:LbmY;

    .line 61
    sget-object v0, Lqt;->b:Lqt;

    sget-object v1, Lqt;->c:Lqt;

    sget-object v2, Lqt;->d:Lqt;

    sget-object v3, Lqt;->f:Lqt;

    invoke-static {v0, v1, v2, v3}, LbmF;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmF;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:LbmF;

    .line 64
    sget-object v0, Lqt;->b:Lqt;

    sget-object v1, Lqt;->c:Lqt;

    sget-object v2, Lqt;->d:Lqt;

    invoke-static {v0, v1, v2}, LbmF;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmF;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->b:LbmF;

    .line 67
    const/4 v0, 0x0

    sput v0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lacj;LabQ;LM;Lach;LaKR;LvU;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 94
    invoke-direct {p0}, LyR;-><init>()V

    .line 85
    iput-boolean v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Z

    .line 86
    iput-object v1, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Lacr;

    .line 87
    iput-object v1, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:LaGu;

    .line 88
    iput-boolean v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->b:Z

    .line 95
    iput-object p1, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Landroid/content/Context;

    .line 96
    iput-object p3, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:LabQ;

    .line 97
    iput-object p2, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Lacj;

    .line 98
    iput-object p4, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:LM;

    .line 99
    iput-object p5, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Lach;

    .line 100
    iput-object p6, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:LaKR;

    .line 101
    iput-object p7, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:LvU;

    .line 103
    const-string v0, "clipboard"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    iput-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Landroid/content/ClipboardManager;

    .line 104
    invoke-direct {p0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->b()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Landroid/view/View;

    .line 106
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, LwW;->share_card_link_sharing_access_levels:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 105
    invoke-static {v0}, LbmF;->a([Ljava/lang/Object;)LbmF;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->c:LbmF;

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Landroid/view/View;

    sget v1, Lxc;->image:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/common/view/RoundImageView;

    iput-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Lcom/google/android/apps/docs/common/view/RoundImageView;

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Landroid/view/View;

    sget v1, Lxc;->link_sharing_access:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Landroid/widget/TextView;

    .line 109
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)LM;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:LM;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)LaGu;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:LaGu;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)LaKR;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:LaKR;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)Lach;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Lach;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)Lacj;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Lacj;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)Lacr;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Lacr;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)Landroid/content/ClipboardManager;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Landroid/content/ClipboardManager;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Landroid/content/Context;

    return-object v0
.end method

.method private a()Landroid/view/animation/RotateAnimation;
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/high16 v4, 0x3f000000    # 0.5f

    .line 171
    new-instance v0, Landroid/view/animation/RotateAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x43af0000    # 350.0f

    move v5, v3

    move v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 174
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 175
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/RotateAnimation;->setRepeatCount(I)V

    .line 176
    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 177
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Landroid/widget/TextView;

    return-object v0
.end method

.method public static synthetic a()LbmF;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->b:LbmF;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)LbmF;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->c:LbmF;

    return-object v0
.end method

.method private a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 223
    sget v0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:I

    .line 224
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LinkSharingCard"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)LvU;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:LvU;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;Lqt;Lqt;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lqt;Lqt;)V

    return-void
.end method

.method private a(Lqt;Lqt;)V
    .locals 3

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Lacr;

    invoke-interface {v0}, Lacr;->a()Lqu;

    move-result-object v0

    invoke-virtual {v0}, Lqu;->a()Lqt;

    move-result-object v0

    invoke-virtual {p2, v0}, Lqt;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    :goto_0
    return-void

    .line 186
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->b:Z

    .line 188
    invoke-direct {p0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a()Landroid/view/animation/RotateAnimation;

    move-result-object v0

    .line 192
    iget-object v1, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Lcom/google/android/apps/docs/common/view/RoundImageView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/common/view/RoundImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 193
    iget-object v1, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:LabQ;

    iget-object v2, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:LaGu;

    .line 194
    invoke-virtual {v1, v2, p2}, LabQ;->a(LaGu;Lqt;)LbsU;

    move-result-object v1

    .line 196
    new-instance v2, Lzh;

    invoke-direct {v2, p0, v0, p2, p1}, Lzh;-><init>(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;Landroid/view/animation/RotateAnimation;Lqt;Lqt;)V

    invoke-static {v1, v2}, Lanj;->a(LbsU;LbsJ;)V

    goto :goto_0
.end method

.method private a(Lacr;)Z
    .locals 2

    .prologue
    .line 167
    sget-object v0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:LbmY;

    invoke-interface {p1}, Lacr;->a()Lqu;

    move-result-object v1

    invoke-virtual {v0, v1}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;)Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->b:Z

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;Lacr;)Z
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lacr;)Z

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;Z)Z
    .locals 0

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->b:Z

    return p1
.end method

.method private b()Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Landroid/content/Context;

    sget v1, Lxe;->detail_card_share_link_status:I

    invoke-static {v0, v1, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 113
    sget v1, Lxc;->link_sharing:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lzi;

    invoke-direct {v2, p0, v3}, Lzi;-><init>(Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;Lzh;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    return-object v0
.end method

.method public static synthetic b()LbmF;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:LbmF;

    return-object v0
.end method

.method private b(Lacr;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 134
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lacr;)Z

    move-result v1

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Landroid/view/View;

    sget v3, Lxc;->link_sharing_status:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 136
    if-eqz v1, :cond_2

    sget v1, Lxi;->share_card_link_sharing_is_on:I

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 139
    invoke-interface {p1}, Lacr;->a()Lqu;

    move-result-object v0

    invoke-virtual {v0}, Lqu;->a()Lqt;

    move-result-object v4

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Landroid/view/View;

    sget v1, Lxc;->link_sharing_role:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 141
    invoke-static {p1}, LacH;->a(Lacr;)LacD;

    move-result-object v5

    .line 142
    if-eqz v4, :cond_0

    sget-object v1, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->b:LbmF;

    invoke-virtual {v1, v4}, LbmF;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_0
    const/4 v1, 0x1

    move v3, v1

    .line 143
    :goto_1
    if-nez v3, :cond_4

    move v1, v2

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 144
    iget-object v1, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Landroid/content/Context;

    .line 145
    invoke-static {v5, v2}, LacH;->a(LacD;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 144
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    if-nez v3, :cond_1

    .line 147
    iget-object v1, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->c:LbmF;

    sget-object v2, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:LbmF;

    invoke-virtual {v2, v4}, LbmF;->indexOf(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v1, v2}, LbmF;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    :cond_1
    return-void

    .line 136
    :cond_2
    sget v1, Lxi;->share_card_link_sharing_is_off:I

    goto :goto_0

    :cond_3
    move v3, v2

    .line 142
    goto :goto_1

    .line 143
    :cond_4
    const/16 v1, 0x8

    goto :goto_2
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Landroid/view/View;

    return-object v0
.end method

.method public a(LaGu;)V
    .locals 1

    .prologue
    .line 162
    iput-object p1, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:LaGu;

    .line 163
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->b:Z

    .line 164
    return-void
.end method

.method public a(Lacr;)V
    .locals 2

    .prologue
    .line 120
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Z

    .line 121
    iput-object p1, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Lacr;

    .line 123
    if-eqz p1, :cond_0

    .line 124
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->b(Lacr;)V

    .line 125
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Z

    .line 126
    iget-object v1, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Lcom/google/android/apps/docs/common/view/RoundImageView;

    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a(Lacr;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, LwZ;->detail_fragment_avatar_background_green:I

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/common/view/RoundImageView;->setBackgroundResource(I)V

    .line 130
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->notifyDataSetChanged()V

    .line 131
    return-void

    .line 126
    :cond_1
    sget v0, LwZ;->detail_fragment_avatar_background_gray:I

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/google/android/apps/docs/detailspanel/LinkSharingCard;->a:Z

    return v0
.end method
