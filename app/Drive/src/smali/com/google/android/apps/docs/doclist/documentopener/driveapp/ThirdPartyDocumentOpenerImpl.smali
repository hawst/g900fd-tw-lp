.class public Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;
.super Ljava/lang/Object;
.source "ThirdPartyDocumentOpenerImpl.java"

# interfaces
.implements Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;


# instance fields
.field private final a:LGZ;

.field private final a:LaHL;

.field private final a:Landroid/content/Context;

.field private final a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;

.field private final a:Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

.field private final a:LqK;

.field private final a:LtK;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;LaHL;LGZ;LtK;Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;LqK;)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Landroid/content/Context;

    .line 118
    iput-object p2, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

    .line 119
    iput-object p3, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:LaHL;

    .line 120
    iput-object p6, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;

    .line 121
    iput-object p4, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:LGZ;

    .line 122
    iput-object p5, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:LtK;

    .line 123
    iput-object p7, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:LqK;

    .line 124
    return-void
.end method

.method private a(LaGo;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)LbmF;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGo;",
            "Lcom/google/android/apps/docs/app/DocumentOpenMethod;",
            ")",
            "LbmF",
            "<",
            "LHf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 130
    invoke-interface {p1}, LaGo;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 131
    :goto_0
    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:LtK;

    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

    iget-object v5, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v5}, LHe;->a(LtK;LaGo;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Landroid/content/Context;Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 140
    :cond_0
    sget-object v0, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    sget-object v1, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->b:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    .line 141
    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    .line 142
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:LtK;

    sget-object v2, Lry;->E:Lry;

    invoke-interface {v1, v2}, LtK;->a(LtJ;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 143
    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:LGZ;

    invoke-static {p1, v0, v1}, LHd;->a(LaGo;Landroid/content/Context;LGZ;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:LaHL;

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:LGZ;

    invoke-static {p1, v0, v1, v2}, LHo;->a(LaGo;Landroid/content/Context;LaHL;LGZ;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 149
    :cond_1
    invoke-static {v6}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    return-object v0

    .line 130
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LFT;LaGo;Landroid/content/Intent;)LbsU;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LFT;",
            "LaGo;",
            "Landroid/content/Intent;",
            ")",
            "LbsU",
            "<",
            "LDL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173
    invoke-interface {p2}, LaGo;->a()LaFM;

    move-result-object v0

    invoke-virtual {v0}, LaFM;->a()LaFO;

    move-result-object v0

    .line 174
    new-instance v1, LGy;

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Landroid/content/Context;

    .line 175
    invoke-virtual {v0}, LaFO;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, p1, v0, p3}, LGy;-><init>(Landroid/content/Context;LGx;Ljava/lang/String;Landroid/content/Intent;)V

    .line 176
    invoke-static {v1}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    return-object v0
.end method

.method private a(LbmF;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LHf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:LqK;

    const-string v1, "documentOpener"

    const-string v2, "documentOpeningAppsAvailable"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    invoke-static {}, Lbmr;->a()Lbmr;

    move-result-object v1

    .line 247
    invoke-virtual {p1}, LbmF;->a()Lbqv;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHf;

    .line 248
    invoke-virtual {v0}, LHf;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lbpi;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 251
    :cond_0
    invoke-interface {v1}, Lbpi;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpj;

    .line 252
    invoke-interface {v0}, Lbpj;->a()I

    move-result v1

    int-to-long v4, v1

    .line 253
    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_1

    .line 254
    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:LqK;

    const-string v6, "documentOpener"

    const-string v7, "documentOpeningAppTypeAvailable"

    .line 255
    invoke-interface {v0}, Lbpj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 254
    invoke-virtual {v3, v6, v7, v1}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:LqK;

    const-string v3, "documentOpener"

    const-string v6, "documentOpeningThirdPartyAppsCount"

    .line 258
    invoke-interface {v0}, Lbpj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 257
    invoke-virtual {v1, v3, v6, v0, v4}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_1

    .line 261
    :cond_2
    return-void
.end method

.method private a(LbmF;Z)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LHf;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 154
    new-instance v0, LHi;

    invoke-direct {v0, p0}, LHi;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;)V

    .line 160
    invoke-static {p1, v0}, Lbnm;->a(Ljava/lang/Iterable;LbiU;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lbnm;->a(Ljava/lang/Iterable;)I

    move-result v0

    .line 161
    if-le v0, v2, :cond_1

    move v1, v2

    .line 162
    :goto_0
    invoke-virtual {p1}, LbmF;->a()Lbqv;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHf;

    .line 163
    invoke-virtual {v0, p2, v1}, LHf;->a(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    :goto_1
    return v2

    :cond_1
    move v1, v3

    .line 161
    goto :goto_0

    :cond_2
    move v2, v3

    .line 168
    goto :goto_1
.end method


# virtual methods
.method public a(LFT;LaGo;Landroid/os/Bundle;)LbsU;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LFT;",
            "LaGo;",
            "Landroid/os/Bundle;",
            ")",
            "LbsU",
            "<",
            "LDL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 182
    const-string v0, "requestCameFromExternalApp"

    const/4 v1, 0x0

    .line 183
    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 184
    invoke-static {p3}, LFU;->a(Landroid/os/Bundle;)Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    move-result-object v1

    .line 186
    invoke-direct {p0, p2, v1}, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a(LaGo;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)LbmF;

    move-result-object v2

    .line 187
    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a(LbmF;)V

    .line 188
    invoke-virtual {v2}, LbmF;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 189
    invoke-interface {p2}, LaGo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 190
    invoke-interface {p2}, LaGo;->a()LaFM;

    move-result-object v1

    invoke-virtual {v1}, LaFM;->a()LaFO;

    move-result-object v1

    .line 191
    invoke-interface {p2}, LaGo;->c()Ljava/lang/String;

    move-result-object v2

    .line 192
    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Landroid/content/Context;

    .line 193
    invoke-static {v3, v0, v1, v2}, Lala;->a(Landroid/content/Context;Landroid/net/Uri;LaFO;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 194
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a(LFT;LaGo;Landroid/content/Intent;)LbsU;

    move-result-object v0

    .line 231
    :goto_0
    return-object v0

    .line 195
    :cond_0
    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a(LbmF;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;->a(LFT;LaGo;Landroid/os/Bundle;)LbsU;

    move-result-object v0

    goto :goto_0

    .line 202
    :cond_1
    invoke-static {}, Lbtd;->a()Lbtd;

    move-result-object v6

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Landroid/content/Context;

    invoke-static {v0}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v7

    .line 204
    new-instance v0, LHj;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, LHj;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;LbmF;LFT;LaGo;Landroid/os/Bundle;Lbtd;)V

    .line 214
    sget v1, Lxi;->open_with_dialog_title:I

    invoke-virtual {v7, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 215
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Landroid/content/Context;

    invoke-static {v1, v2}, LHf;->a(Landroid/content/Context;Ljava/util/List;)Landroid/widget/BaseAdapter;

    move-result-object v1

    .line 216
    const/4 v2, -0x1

    invoke-virtual {v7, v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 217
    new-instance v0, LHk;

    invoke-direct {v0, p0, v6}, LHk;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;Lbtd;)V

    invoke-virtual {v7, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 223
    const/high16 v0, 0x1040000

    new-instance v1, LHl;

    invoke-direct {v1, p0}, LHl;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;)V

    invoke-virtual {v7, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 229
    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 230
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 231
    invoke-static {v6}, Lalv;->a(LbsU;)LbsU;

    move-result-object v0

    goto :goto_0
.end method

.method public a(LaGo;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)Z
    .locals 1

    .prologue
    .line 237
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a(LaGo;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)LbmF;

    move-result-object v0

    .line 238
    invoke-virtual {v0}, LbmF;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
