.class public Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;
.super Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;
.source "PunchPresentationModeFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation


# instance fields
.field private a:Landroid/app/AlertDialog;

.field private final a:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 39
    const-string v0, "PunchPresentationModeFragment"

    sget-object v1, LYR;->d:LYR;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;-><init>(Ljava/lang/String;LYR;)V

    .line 34
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;->a:Landroid/os/Handler;

    .line 40
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;->a:Landroid/app/AlertDialog;

    return-object p1
.end method

.method public static a()Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;->a:LYO;

    sget-object v1, LYR;->c:LYR;

    invoke-interface {v0, v1}, LYO;->a(LYR;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;->a:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 77
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;->a()LH;

    move-result-object v0

    invoke-virtual {v0}, LH;->a()LM;

    move-result-object v0

    const-string v1, "presentationModeFragment"

    const/4 v2, 0x1

    .line 78
    invoke-virtual {v0, v1, v2}, LM;->a(Ljava/lang/String;I)V

    .line 79
    return-void
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 44
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->a_(Landroid/os/Bundle;)V

    .line 47
    if-eqz p1, :cond_1

    .line 65
    :cond_0
    :goto_0
    return-void

    .line 53
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;->a:LYO;

    invoke-interface {v0}, LYO;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;->a()LH;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, LH;->a()LM;

    move-result-object v0

    const-string v1, "SpeakerNotesFragmentTag"

    invoke-virtual {v0, v1}, LM;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;

    .line 56
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;->a:Landroid/os/Handler;

    new-instance v2, LXK;

    invoke-direct {v2, p0, v0}, LXK;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;->a:LYO;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LYO;->a(Z)V

    goto :goto_0
.end method

.method protected m()Z
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;->w()V

    .line 112
    const/4 v0, 0x1

    return v0
.end method

.method public w()V
    .locals 3

    .prologue
    .line 86
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;->a()LH;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    .line 87
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, LpR;->dialog_confirm_stop_presenting:I

    .line 88
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1040013

    new-instance v2, LXM;

    invoke-direct {v2, p0}, LXM;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;)V

    .line 89
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    const/4 v2, 0x0

    .line 96
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, LXL;

    invoke-direct {v1, p0}, LXL;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;)V

    .line 97
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 103
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;->a:Landroid/app/AlertDialog;

    .line 104
    return-void
.end method
