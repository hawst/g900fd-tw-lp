.class public Lcom/google/android/apps/docs/drive/photos/ConfirmPhotoSharingDialogFragment;
.super Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;
.source "ConfirmPhotoSharingDialogFragment.java"


# instance fields
.field public a:LQr;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;-><init>()V

    .line 36
    return-void
.end method

.method private a(Landroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 80
    .line 81
    invoke-static {p1}, Labw;->a(Landroid/os/Bundle;)LaGw;

    move-result-object v0

    .line 82
    sget-object v1, LaGw;->b:LaGw;

    invoke-virtual {v1, v0}, LaGw;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 52
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/photos/ConfirmPhotoSharingDialogFragment;->a()LH;

    move-result-object v0

    sget v1, LOV;->share_psyncho_warning_header:I

    new-array v2, v5, [Ljava/lang/Object;

    .line 53
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/photos/ConfirmPhotoSharingDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v3

    invoke-static {v3}, Labw;->c(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    .line 52
    invoke-virtual {v0, v1, v2}, LH;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 55
    iget-object v1, p0, Lcom/google/android/apps/docs/drive/photos/ConfirmPhotoSharingDialogFragment;->a:LQr;

    const-string v2, "helpPhotoSyncSharingConfirmationUrl"

    const-string v3, "https://support.google.com/drive?hl=%s&p=share_photos"

    invoke-interface {v1, v2, v3}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 57
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 58
    new-array v3, v5, [Ljava/lang/Object;

    aput-object v2, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 60
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/photos/ConfirmPhotoSharingDialogFragment;->a()LH;

    move-result-object v2

    sget v3, LOV;->share_psyncho_warning_learnmore:I

    invoke-virtual {v2, v3}, LH;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 61
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 62
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/photos/ConfirmPhotoSharingDialogFragment;->a()LH;

    move-result-object v4

    sget v5, LOV;->share_psyncho_warning_description:I

    invoke-virtual {v4, v5}, LH;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 63
    new-instance v4, Landroid/text/SpannableString;

    invoke-direct {v4, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 64
    new-instance v5, Landroid/text/style/URLSpan;

    invoke-direct {v5, v1}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    .line 65
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x21

    .line 64
    invoke-virtual {v4, v5, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 67
    invoke-virtual {p0, v0, v4}, Lcom/google/android/apps/docs/drive/photos/ConfirmPhotoSharingDialogFragment;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;->a_(Landroid/os/Bundle;)V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/photos/ConfirmPhotoSharingDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/drive/photos/ConfirmPhotoSharingDialogFragment;->a(Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/drive/photos/ConfirmPhotoSharingDialogFragment;->i(Z)V

    .line 77
    :cond_0
    return-void
.end method
