.class public Lcom/google/android/apps/docs/entry/DetailDrawerFragment;
.super Lcom/google/android/apps/docs/app/BaseFragment;
.source "DetailDrawerFragment.java"

# interfaces
.implements LCW;
.implements LCX;
.implements LQU;
.implements LRA;


# instance fields
.field private a:I

.field private a:LCY;

.field public a:LPo;

.field public a:LPu;

.field public a:LacF;

.field public a:Lacj;

.field private a:Landroid/support/v4/widget/DrawerLayout;

.field private a:Landroid/view/Menu;

.field private a:Lcom/google/android/apps/docs/fragment/DetailFragment;

.field private final a:Lgz;

.field public a:LtK;

.field public a:LvU;

.field private a:Z

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseFragment;-><init>()V

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Z

    .line 106
    new-instance v0, LPj;

    invoke-direct {v0, p0}, LPj;-><init>(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Lgz;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)LCY;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:LCY;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)Landroid/support/v4/widget/DrawerLayout;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Landroid/support/v4/widget/DrawerLayout;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->d:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)Lcom/google/android/apps/docs/fragment/DetailFragment;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Lcom/google/android/apps/docs/fragment/DetailFragment;

    return-object v0
.end method

.method private a(Landroid/view/Menu;Z)V
    .locals 2

    .prologue
    .line 302
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:I

    if-ge v0, v1, :cond_0

    .line 303
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 302
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 305
    :cond_0
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->u()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Z

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;Z)Z
    .locals 0

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->t()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)Z
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->m()Z

    move-result v0

    return v0
.end method

.method private m()Z
    .locals 1

    .prologue
    .line 197
    invoke-static {}, LakQ;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Lcom/google/android/apps/docs/fragment/DetailFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/DetailFragment;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 328
    iget-boolean v1, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Lcom/google/android/apps/docs/fragment/DetailFragment;

    if-nez v1, :cond_1

    .line 334
    :cond_0
    :goto_0
    return v0

    .line 332
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Lcom/google/android/apps/docs/fragment/DetailFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/fragment/DetailFragment;->a()Landroid/view/View;

    move-result-object v1

    .line 333
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Landroid/support/v4/widget/DrawerLayout;

    .line 334
    invoke-virtual {v2, v1}, Landroid/support/v4/widget/DrawerLayout;->c(Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v2, v1}, Landroid/support/v4/widget/DrawerLayout;->d(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private t()V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 342
    invoke-virtual {p0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a()LH;

    move-result-object v0

    .line 343
    if-nez v0, :cond_1

    .line 359
    :cond_0
    :goto_0
    return-void

    .line 347
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Landroid/view/Menu;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:LCY;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:LCY;

    .line 349
    invoke-virtual {v0}, LCY;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 350
    invoke-direct {p0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 351
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:LCY;

    invoke-virtual {v0}, LCY;->a()V

    .line 354
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->m()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 355
    invoke-virtual {p0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a()LH;

    move-result-object v0

    invoke-virtual {v0}, LH;->invalidateOptionsMenu()V

    goto :goto_0

    .line 356
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Landroid/view/Menu;

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Landroid/view/Menu;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a_(Landroid/view/Menu;)V

    goto :goto_0
.end method

.method private u()V
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:LCY;

    if-eqz v0, :cond_0

    .line 363
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:LCY;

    invoke-virtual {v0}, LCY;->c()V

    .line 365
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 158
    sget v0, Lxe;->detail_drawer:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->d:Landroid/view/View;

    .line 159
    invoke-static {}, LakQ;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setFitsSystemWindows(Z)V

    .line 164
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->b()LM;

    move-result-object v0

    sget v1, Lxc;->detail_fragment_drawer:I

    invoke-virtual {v0, v1}, LM;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/fragment/DetailFragment;

    iput-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Lcom/google/android/apps/docs/fragment/DetailFragment;

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Lcom/google/android/apps/docs/fragment/DetailFragment;

    if-nez v0, :cond_1

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:LtK;

    sget-object v1, Lry;->q:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/apps/docs/detailspanel/DetailListFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/detailspanel/DetailListFragment;-><init>()V

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Lcom/google/android/apps/docs/fragment/DetailFragment;

    .line 168
    invoke-virtual {p0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->b()LM;

    move-result-object v0

    invoke-virtual {v0}, LM;->a()Lac;

    move-result-object v0

    .line 169
    iget-object v1, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Lcom/google/android/apps/docs/fragment/DetailFragment;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/docs/fragment/DetailFragment;->a(Z)V

    .line 170
    sget v1, Lxc;->detail_fragment_drawer:I

    iget-object v2, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Lcom/google/android/apps/docs/fragment/DetailFragment;

    invoke-virtual {v0, v1, v2}, Lac;->a(ILandroid/support/v4/app/Fragment;)Lac;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Lcom/google/android/apps/docs/fragment/DetailFragment;

    invoke-virtual {v1, v2}, Lac;->b(Landroid/support/v4/app/Fragment;)Lac;

    .line 171
    iget-object v1, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->d:Landroid/view/View;

    new-instance v2, LPk;

    invoke-direct {v2, p0, v0}, LPk;-><init>(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;Lac;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 182
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->d:Landroid/view/View;

    sget v1, Lxc;->detail_fragment_drawer:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    iput-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Landroid/support/v4/widget/DrawerLayout;

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Landroid/support/v4/widget/DrawerLayout;

    sget v1, Lxb;->gradient_details:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/DrawerLayout;->setDrawerShadow(II)V

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Lgz;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerListener(Lgz;)V

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v3}, Landroid/support/v4/widget/DrawerLayout;->setFocusable(Z)V

    .line 187
    invoke-direct {p0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->m()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 188
    new-instance v0, LCY;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a()LH;

    move-result-object v1

    invoke-direct {v0, p0, p0, v1}, LCY;-><init>(LCW;LCX;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:LCY;

    .line 193
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->d:Landroid/view/View;

    return-object v0

    .line 166
    :cond_2
    new-instance v0, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/fragment/LegacyDetailFragment;-><init>()V

    goto :goto_0

    .line 190
    :cond_3
    invoke-virtual {p0, v4}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->f(Z)V

    goto :goto_1
.end method

.method public a()Lcom/google/android/apps/docs/fragment/DetailFragment;
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Lcom/google/android/apps/docs/fragment/DetailFragment;

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->d:Landroid/view/View;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Lcom/google/android/apps/docs/fragment/DetailFragment;

    new-instance v1, LPl;

    invoke-direct {v1, p0}, LPl;-><init>(Lcom/google/android/apps/docs/entry/DetailDrawerFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/fragment/DetailFragment;->a(Ljava/lang/Runnable;)V

    .line 249
    return-void
.end method

.method public a(Landroid/view/Menu;)V
    .locals 1

    .prologue
    .line 278
    iput-object p1, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Landroid/view/Menu;

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Lcom/google/android/apps/docs/fragment/DetailFragment;

    if-nez v0, :cond_1

    .line 287
    :cond_0
    :goto_0
    return-void

    .line 283
    :cond_1
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:I

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Lcom/google/android/apps/docs/fragment/DetailFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/DetailFragment;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:LPu;

    invoke-virtual {v0, p1}, LPu;->a(Landroid/view/Menu;)V

    goto :goto_0
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 291
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:I

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:LPu;

    invoke-virtual {v0, p1}, LPu;->a(Landroid/view/Menu;)V

    .line 293
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Landroid/support/v4/widget/DrawerLayout;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerTitle(ILjava/lang/CharSequence;)V

    .line 202
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 273
    const/4 v0, 0x0

    return v0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 148
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseFragment;->a_(Landroid/os/Bundle;)V

    .line 149
    if-eqz p1, :cond_0

    .line 150
    const-string v0, "detailPaneOpen"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Z

    .line 152
    :cond_0
    return-void
.end method

.method public a_(Landroid/view/Menu;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 297
    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a(Landroid/view/Menu;Z)V

    .line 298
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:LPu;

    iget-object v1, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:LvU;

    invoke-interface {v1}, LvU;->a()LaGu;

    move-result-object v1

    invoke-virtual {v0, p1, v1, v2}, LPu;->a(Landroid/view/Menu;LaGu;I)V

    .line 299
    return-void
.end method

.method public b_(Landroid/view/Menu;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Lcom/google/android/apps/docs/fragment/DetailFragment;

    if-nez v0, :cond_1

    .line 322
    :cond_0
    :goto_0
    return-void

    .line 313
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Lcom/google/android/apps/docs/fragment/DetailFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/DetailFragment;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 314
    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a(Landroid/view/Menu;Z)V

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:LPu;

    iget-object v1, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:LvU;

    invoke-interface {v1}, LvU;->a()LaGu;

    move-result-object v1

    invoke-virtual {v0, p1, v1, v2}, LPu;->a(Landroid/view/Menu;LaGu;I)V

    goto :goto_0

    .line 317
    :cond_2
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a(Landroid/view/Menu;Z)V

    .line 318
    iget v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:I

    :goto_1
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 319
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 318
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 260
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseFragment;->c(Landroid/os/Bundle;)V

    .line 261
    const-string v0, "detailPaneOpen"

    iget-boolean v1, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 262
    return-void
.end method

.method public e_()V
    .locals 0

    .prologue
    .line 369
    invoke-direct {p0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->t()V

    .line 370
    return-void
.end method

.method public l()V
    .locals 2

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Lcom/google/android/apps/docs/fragment/DetailFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Lcom/google/android/apps/docs/fragment/DetailFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/DetailFragment;->a()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a:Lcom/google/android/apps/docs/fragment/DetailFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/fragment/DetailFragment;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->d(Landroid/view/View;)V

    .line 269
    :cond_0
    return-void
.end method

.method public m()V
    .locals 0

    .prologue
    .line 374
    invoke-virtual {p0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->l()V

    .line 375
    return-void
.end method
