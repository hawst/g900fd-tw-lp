.class public Lcom/google/android/apps/docs/compat/lmp/DocsCentricTaskRootActivity;
.super LpH;
.source "DocsCentricTaskRootActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, LpH;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 32
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 33
    const-class v1, Lcom/google/android/apps/docs/compat/lmp/DocsCentricTaskRootActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 34
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 35
    const-string v1, "EXTRA_DELEGATE_INTENT"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 36
    const-string v1, "EXTRA_TASK_TITLE"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 37
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 38
    return-object v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 67
    invoke-super {p0, p1, p2, p3}, LpH;->onActivityResult(IILandroid/content/Intent;)V

    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/docs/compat/lmp/DocsCentricTaskRootActivity;->finish()V

    .line 69
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 43
    invoke-super {p0, p1}, LpH;->onCreate(Landroid/os/Bundle;)V

    .line 45
    if-eqz p1, :cond_0

    .line 63
    :goto_0
    return-void

    .line 49
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/compat/lmp/DocsCentricTaskRootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 50
    const-string v0, "EXTRA_DELEGATE_INTENT"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 51
    if-nez v0, :cond_1

    .line 52
    const-string v0, "DocsCentricTaskRootActivity"

    const-string v2, "Closing due to an invalid intent (no delegate specified): %s"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v0, v2, v3}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 53
    invoke-virtual {p0}, Lcom/google/android/apps/docs/compat/lmp/DocsCentricTaskRootActivity;->finish()V

    goto :goto_0

    .line 57
    :cond_1
    const-string v2, "EXTRA_TASK_TITLE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 58
    if-eqz v1, :cond_2

    .line 59
    new-instance v2, Landroid/app/ActivityManager$TaskDescription;

    invoke-direct {v2, v1}, Landroid/app/ActivityManager$TaskDescription;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/compat/lmp/DocsCentricTaskRootActivity;->setTaskDescription(Landroid/app/ActivityManager$TaskDescription;)V

    .line 62
    :cond_2
    invoke-virtual {p0, v0, v3}, Lcom/google/android/apps/docs/compat/lmp/DocsCentricTaskRootActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
