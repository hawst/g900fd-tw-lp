.class public Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;
.super Ljava/lang/Object;
.source "MimeTypeCriterion.java"

# interfaces
.implements Lcom/google/android/apps/docs/app/model/navigation/Criterion;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lwi;

    invoke-direct {v0}, Lwi;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LbmY;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LbmY;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 42
    invoke-static {p1}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;->a:LbmY;

    .line 43
    iput-boolean p2, p0, Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;->a:Z

    .line 44
    return-void

    .line 41
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(LaGM;)LaeZ;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;->a:LbmY;

    invoke-static {v0}, LaeZ;->c(LbmY;)LaeZ;

    move-result-object v0

    return-object v0
.end method

.method public a(LvR;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LvR",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;->a:LbmY;

    iget-boolean v1, p0, Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;->a:Z

    invoke-interface {p1, v0, v1}, LvR;->a(LbmY;Z)V

    .line 89
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 71
    if-ne p1, p0, :cond_1

    .line 77
    :cond_0
    :goto_0
    return v0

    .line 73
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;

    if-eqz v2, :cond_3

    .line 74
    check-cast p1, Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;

    .line 75
    iget-object v2, p0, Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;->a:LbmY;

    iget-object v3, p1, Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;->a:LbmY;

    invoke-static {v2, v3}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;->a:Z

    iget-boolean v3, p1, Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;->a:Z

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 77
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 83
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;->a:LbmY;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;->a:LbmY;

    new-array v2, v1, [Ljava/lang/String;

    invoke-virtual {v0, v2}, LbmY;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 54
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/model/navigation/MimeTypeCriterion;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 55
    return-void

    :cond_0
    move v0, v1

    .line 54
    goto :goto_0
.end method
