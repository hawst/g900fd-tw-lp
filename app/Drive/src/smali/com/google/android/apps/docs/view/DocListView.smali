.class public Lcom/google/android/apps/docs/view/DocListView;
.super Lcom/google/android/apps/docs/view/FastScrollView;
.source "DocListView.java"

# interfaces
.implements LCt;
.implements LIp;
.implements LapF;
.implements Lapt;


# instance fields
.field private a:F

.field private a:I

.field private final a:J

.field private a:LQX;

.field public a:LQr;

.field public a:LZP;
    .annotation runtime Lbxv;
        a = "wapiFeedProcessor"
    .end annotation
.end field

.field private a:LaFM;

.field private a:Lafx;

.field private final a:Lafy;

.field public a:Lajw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajw",
            "<",
            "Lwg;",
            ">;"
        }
    .end annotation
.end field

.field private a:Landroid/support/v4/app/Fragment;

.field private a:Landroid/widget/ListView;

.field private a:LapA;

.field private a:LapC;

.field private a:LapD;

.field private a:LapE;

.field private a:LapG;

.field private a:LapH;

.field private a:Lcom/google/android/apps/docs/view/StickyHeaderView;

.field private a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private a:Ljava/lang/String;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LzP;",
            "LBJ;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/widget/AbsListView$OnScrollListener;",
            ">;"
        }
    .end annotation
.end field

.field public a:LsC;

.field private a:LzO;

.field private a:Z

.field private b:F

.field private b:I

.field public b:Lajw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajw",
            "<",
            "Lafw;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private final c:F

.field public c:Lajw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajw",
            "<",
            "LBX;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field public d:Lajw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajw",
            "<",
            "LBP;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z

.field public e:Lajw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajw",
            "<",
            "LtE;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lajw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajw",
            "<",
            "LBI;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lajw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajw",
            "<",
            "LBa;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 290
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/view/FastScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 179
    new-instance v0, LapB;

    invoke-direct {v0, v1}, LapB;-><init>(Lapw;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapH;

    .line 225
    sget-object v0, LapG;->a:LapG;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapG;

    .line 226
    iput-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 228
    sget-object v0, LapE;->c:LapE;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapE;

    .line 229
    sget-object v0, Lafx;->a:Lafx;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lafx;

    .line 238
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Ljava/util/Set;

    .line 240
    const-class v0, LzP;

    .line 241
    invoke-static {v0}, LboS;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Ljava/util/Map;

    .line 243
    new-instance v0, Lapw;

    invoke-direct {v0, p0}, Lapw;-><init>(Lcom/google/android/apps/docs/view/DocListView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lafy;

    .line 266
    iput-boolean v3, p0, Lcom/google/android/apps/docs/view/DocListView;->c:Z

    .line 280
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/docs/view/DocListView;->b:I

    .line 285
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/DocListView;->d:Z

    .line 287
    iput-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapA;

    .line 292
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->a()Lbuu;

    move-result-object v0

    invoke-interface {v0, p0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 294
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/view/DocListView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)LzO;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LzO;

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LQr;

    const-string v1, "wapiSyncDelayMsecAfterDoclistScroll"

    const/16 v2, 0x1f4

    .line 297
    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    .line 296
    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:J

    .line 298
    invoke-virtual {p0, p0}, Lcom/google/android/apps/docs/view/DocListView;->setOverlayStatusListener(LIp;)V

    .line 301
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 302
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, Lcom/google/android/apps/docs/view/DocListView;->b:I

    .line 303
    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/android/apps/docs/view/DocListView;->c:F

    .line 305
    sget v0, Lxe;->doc_list_view:I

    invoke-static {p1, v0, p0}, Lcom/google/android/apps/docs/view/DocListView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 306
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/DocListView;Lafx;)Lafx;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lafx;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/DocListView;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/DocListView;)Lcom/google/android/apps/docs/view/StickyHeaderView;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/apps/docs/view/StickyHeaderView;

    return-object v0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)LzO;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 309
    .line 310
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lxk;->DocListView:[I

    invoke-virtual {v0, p2, v1, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 312
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LsC;

    invoke-interface {v0}, LsC;->a()LzO;

    move-result-object v0

    .line 314
    invoke-virtual {v0}, LzO;->a()I

    move-result v0

    .line 316
    :try_start_0
    sget v2, Lxk;->DocListView_arrangementMode:I

    .line 317
    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getInteger(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 319
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 322
    invoke-static {v0}, LzO;->a(I)LzO;

    move-result-object v0

    .line 323
    return-object v0

    .line 319
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private a(F)V
    .locals 1

    .prologue
    .line 390
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/DocListView;->c:Z

    .line 391
    iput p1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:F

    .line 392
    iget v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:F

    iput v0, p0, Lcom/google/android/apps/docs/view/DocListView;->b:F

    .line 393
    return-void
.end method

.method private a(LzO;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 786
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapG;

    sget-object v2, LapG;->c:LapG;

    invoke-virtual {v0, v2}, LapG;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 787
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/DocListView;->l()V

    .line 811
    :goto_0
    return-void

    .line 791
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 792
    invoke-static {v0}, Lamt;->a(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 793
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/DocListView;->k()V

    goto :goto_0

    .line 800
    :cond_1
    sget-object v2, LzO;->a:LzO;

    invoke-virtual {v2, p1}, LzO;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 801
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lxd;->doclist_margin_percent:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 802
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 804
    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-double v4, v0

    int-to-double v2, v2

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    div-double/2addr v2, v6

    mul-double/2addr v2, v4

    double-to-int v0, v2

    .line 807
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    invoke-virtual {v2, v0, v1, v0, v1}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 808
    iget-object v2, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->setClipToPadding(Z)V

    .line 810
    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/apps/docs/view/StickyHeaderView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/view/StickyHeaderView;->a(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private a(III)Z
    .locals 1

    .prologue
    .line 500
    add-int v0, p1, p2

    if-lt v0, p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()V
    .locals 1

    .prologue
    .line 396
    iget-boolean v0, p0, Lcom/google/android/apps/docs/view/DocListView;->c:Z

    if-eqz v0, :cond_0

    .line 397
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/DocListView;->c:Z

    .line 399
    :cond_0
    return-void
.end method

.method private k()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 815
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxa;->doclist_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v1, v0

    .line 816
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->a()Landroid/widget/ListView;

    move-result-object v2

    .line 818
    invoke-virtual {v2}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 819
    invoke-virtual {v0, v1, v3, v1, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 821
    invoke-virtual {v2, v3, v3, v3, v1}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 824
    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setClipToPadding(Z)V

    .line 826
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/apps/docs/view/StickyHeaderView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/StickyHeaderView;->a(I)V

    .line 827
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 831
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LakQ;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 838
    :goto_0
    return-void

    .line 834
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->a()Landroid/widget/ListView;

    move-result-object v0

    .line 836
    invoke-virtual {v0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 837
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 512
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->a()LBJ;

    move-result-object v0

    .line 513
    invoke-interface {v0}, LBJ;->a()I

    move-result v0

    .line 514
    return v0
.end method

.method public a()LBJ;
    .locals 8

    .prologue
    .line 469
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 470
    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Ljava/util/Map;

    sget-object v2, LzP;->a:LzP;

    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->c:Lajw;

    .line 471
    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBX;

    iget-object v3, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/support/v4/app/Fragment;

    iget-object v4, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/apps/docs/view/StickyHeaderView;

    invoke-virtual {v0, p0, v3, v4, v5}, LBX;->a(Lcom/google/android/apps/docs/view/DocListView;Landroid/support/v4/app/Fragment;Landroid/widget/ListView;Lcom/google/android/apps/docs/view/StickyHeaderView;)LBS;

    move-result-object v0

    .line 470
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 472
    iget-object v6, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Ljava/util/Map;

    sget-object v7, LzP;->b:LzP;

    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->d:Lajw;

    .line 473
    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBP;

    iget-object v2, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/support/v4/app/Fragment;

    iget-object v3, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/apps/docs/view/StickyHeaderView;

    iget v5, p0, Lcom/google/android/apps/docs/view/DocListView;->b:I

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, LBP;->a(Lcom/google/android/apps/docs/view/DocListView;Landroid/support/v4/app/Fragment;Landroid/widget/ListView;Lcom/google/android/apps/docs/view/StickyHeaderView;I)LBK;

    move-result-object v0

    .line 472
    invoke-interface {v6, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 475
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LzO;

    invoke-virtual {v1}, LzO;->a()LzP;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBJ;

    .line 476
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBJ;

    return-object v0
.end method

.method public a()LQX;
    .locals 1

    .prologue
    .line 576
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LQX;

    return-object v0
.end method

.method public a()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 526
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    return-object v0
.end method

.method public a()LapG;
    .locals 1

    .prologue
    .line 724
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapG;

    return-object v0
.end method

.method public a()Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 756
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 581
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->b:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafw;

    invoke-interface {v0}, Lafw;->a()V

    .line 582
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LQX;

    .line 583
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 708
    return-void
.end method

.method public a(LQX;)V
    .locals 4

    .prologue
    .line 567
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->a()LBJ;

    move-result-object v0

    invoke-virtual {p1}, LQX;->a()LaFX;

    move-result-object v1

    invoke-interface {v0, v1}, LBJ;->a(LaFX;)V

    .line 568
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LQX;

    .line 569
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->h()V

    .line 570
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->b:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafw;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LaFM;

    iget-object v2, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LQX;

    iget-object v3, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lafy;

    invoke-interface {v0, v1, v2, v3}, Lafw;->b(LaFM;LQX;Lafy;)V

    .line 572
    return-void
.end method

.method public a(LaGu;ILcom/google/android/apps/docs/app/DocumentOpenMethod;)V
    .locals 1

    .prologue
    .line 696
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->e:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtE;

    invoke-interface {v0, p1, p2, p3}, LtE;->a(LaGu;ILcom/google/android/apps/docs/app/DocumentOpenMethod;)V

    .line 697
    return-void
.end method

.method public a(Landroid/view/View;ILaGu;)V
    .locals 2

    .prologue
    .line 771
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapD;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LQX;

    if-eqz v0, :cond_0

    .line 772
    iput p2, p0, Lcom/google/android/apps/docs/view/DocListView;->a:I

    .line 773
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LQX;

    invoke-virtual {v0}, LQX;->a()Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a(Landroid/os/Parcelable;)V

    .line 774
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapD;

    invoke-interface {v0, p1, p2, p3}, LapD;->a(Landroid/view/View;ILaGu;)V

    .line 776
    :cond_0
    return-void
.end method

.method public a(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 1

    .prologue
    .line 882
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 883
    return-void
.end method

.method public a(Lcom/google/android/gms/drive/database/data/EntrySpec;LahR;)V
    .locals 1

    .prologue
    .line 665
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 666
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 667
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    invoke-static {p1, v0}, LDQ;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;Landroid/view/View;)LDQ;

    move-result-object v0

    .line 668
    if-eqz v0, :cond_0

    .line 669
    invoke-virtual {v0, p2}, LDQ;->a(LahR;)V

    .line 671
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LaFM;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 518
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->a()LBJ;

    move-result-object v0

    invoke-interface {v0}, LBJ;->b()I

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 750
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->b:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafw;

    invoke-interface {v0}, Lafw;->a()V

    .line 751
    invoke-super {p0}, Lcom/google/android/apps/docs/view/FastScrollView;->b()V

    .line 752
    return-void
.end method

.method public b(LQX;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 590
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 591
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LaFM;

    if-eqz v0, :cond_7

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 592
    const-string v0, "DocListView"

    const-string v3, "in fillData(docListCursor) %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Ljava/lang/String;

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 594
    invoke-virtual {p1}, LQX;->a()Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    move-result-object v3

    .line 596
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->g:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBa;

    invoke-virtual {p1}, LQX;->a()LCl;

    move-result-object v4

    invoke-virtual {v0, v4}, LBa;->a(LCl;)V

    .line 597
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->g:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBa;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v4

    invoke-virtual {v0, v4}, LBa;->a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V

    .line 599
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lcom/google/android/apps/docs/fragment/DocListFragment;

    if-eqz v0, :cond_0

    .line 600
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V

    .line 603
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LQX;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LQX;

    .line 604
    invoke-virtual {v0}, LQX;->a()Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move v2, v1

    .line 606
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LQX;

    .line 608
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->a()LBJ;

    move-result-object v4

    .line 609
    invoke-interface {v4, p1}, LBJ;->a(LQX;)V

    .line 610
    if-eqz v2, :cond_8

    .line 611
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->b:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafw;

    invoke-interface {v0}, Lafw;->a()V

    .line 612
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->b:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafw;

    iget-object v2, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LaFM;

    iget-object v5, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lafy;

    invoke-interface {v0, v2, p1, v5}, Lafw;->b(LaFM;LQX;Lafy;)V

    .line 614
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->b:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafw;

    invoke-interface {v0}, Lafw;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 615
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->f:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBI;

    invoke-virtual {v0}, LBI;->b()V

    .line 621
    :cond_3
    :goto_1
    invoke-virtual {v3}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a()Landroid/os/Parcelable;

    move-result-object v0

    .line 622
    iget-boolean v2, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Z

    if-nez v2, :cond_9

    .line 623
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwg;

    invoke-interface {v0}, Lwg;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:I

    .line 626
    invoke-interface {v4}, LBJ;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 627
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwg;

    invoke-interface {v0}, Lwg;->b()I

    move-result v0

    .line 628
    if-ltz v0, :cond_4

    .line 629
    invoke-interface {v4, v0, v1}, LBJ;->a(IZ)V

    .line 633
    :cond_4
    iget v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:I

    invoke-interface {v4, v0}, LBJ;->a(I)V

    .line 634
    iget v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:I

    if-nez v0, :cond_5

    .line 636
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    iget v2, p0, Lcom/google/android/apps/docs/view/DocListView;->a:I

    .line 637
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lxa;->doclist_group_title_height:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 636
    invoke-virtual {v0, v2, v3}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 640
    :cond_5
    iput-boolean v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Z

    .line 644
    :cond_6
    :goto_2
    const/16 v0, 0x1a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/DocListView;->setTextSize(I)V

    .line 645
    const/16 v0, 0x12c

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/DocListView;->setOverlaySizeDp(I)V

    .line 646
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->h()V

    .line 647
    return-void

    :cond_7
    move v0, v2

    .line 591
    goto/16 :goto_0

    .line 618
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->f:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBI;

    invoke-virtual {v0}, LBI;->b()V

    goto :goto_1

    .line 641
    :cond_9
    if-eqz v0, :cond_6

    .line 642
    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_2
.end method

.method public b(Landroid/view/View;ILaGu;)V
    .locals 1

    .prologue
    .line 780
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapD;

    if-eqz v0, :cond_0

    .line 781
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapD;

    invoke-interface {v0, p1, p2, p3}, LapD;->b(Landroid/view/View;ILaGu;)V

    .line 783
    :cond_0
    return-void
.end method

.method public b(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 1

    .prologue
    .line 886
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 887
    return-void
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 719
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapG;

    sget-object v1, LapG;->b:LapG;

    invoke-virtual {v0, v1}, LapG;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 766
    new-instance v0, LapB;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LapB;-><init>(Lapw;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapH;

    .line 767
    return-void
.end method

.method public c()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 894
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->a()Landroid/widget/ListView;

    move-result-object v3

    .line 895
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->a()LBJ;

    move-result-object v0

    invoke-interface {v0}, LBJ;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 896
    :goto_0
    if-eqz v0, :cond_1

    .line 897
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 895
    goto :goto_0

    .line 896
    :cond_1
    invoke-virtual {v3}, Landroid/widget/ListView;->getChildCount()I

    move-result v0

    if-eqz v0, :cond_2

    .line 897
    invoke-virtual {v3, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v3}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public d()V
    .locals 1

    .prologue
    .line 874
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->a()LBJ;

    move-result-object v0

    invoke-interface {v0}, LBJ;->b()V

    .line 875
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 464
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->a()LBJ;

    move-result-object v0

    invoke-interface {v0}, LBJ;->a()V

    .line 465
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/FastScrollView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 466
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v8, 0x0

    const/4 v1, 0x0

    .line 349
    iget-boolean v2, p0, Lcom/google/android/apps/docs/view/DocListView;->d:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapC;

    if-eqz v2, :cond_0

    .line 350
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    .line 351
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 382
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/FastScrollView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 353
    :pswitch_0
    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/view/DocListView;->a(F)V

    goto :goto_0

    .line 356
    :pswitch_1
    iget-boolean v3, p0, Lcom/google/android/apps/docs/view/DocListView;->c:Z

    if-eqz v3, :cond_0

    .line 357
    iget v3, p0, Lcom/google/android/apps/docs/view/DocListView;->a:F

    sub-float v3, v2, v3

    .line 358
    iget v4, p0, Lcom/google/android/apps/docs/view/DocListView;->b:F

    iget v5, p0, Lcom/google/android/apps/docs/view/DocListView;->a:F

    sub-float/2addr v4, v5

    .line 359
    iget v5, p0, Lcom/google/android/apps/docs/view/DocListView;->c:F

    div-float/2addr v3, v5

    .line 360
    iget v5, p0, Lcom/google/android/apps/docs/view/DocListView;->c:F

    div-float/2addr v4, v5

    .line 361
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x40400000    # 3.0f

    cmpl-float v5, v5, v6

    if-lez v5, :cond_1

    .line 362
    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/DocListView;->b:Z

    .line 363
    iget-object v5, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapC;

    iget-object v6, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    cmpl-float v7, v3, v8

    if-lez v7, :cond_2

    :goto_1
    invoke-interface {v5, v6, v0}, LapC;->a(Landroid/widget/AbsListView;Z)V

    .line 365
    :cond_1
    iput v2, p0, Lcom/google/android/apps/docs/view/DocListView;->b:F

    .line 367
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    sub-float/2addr v0, v1

    cmpl-float v0, v0, v8

    if-lez v0, :cond_0

    .line 368
    iput v2, p0, Lcom/google/android/apps/docs/view/DocListView;->a:F

    .line 369
    iget v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:F

    iput v0, p0, Lcom/google/android/apps/docs/view/DocListView;->b:F

    goto :goto_0

    :cond_2
    move v0, v1

    .line 363
    goto :goto_1

    .line 375
    :pswitch_2
    iget-boolean v0, p0, Lcom/google/android/apps/docs/view/DocListView;->c:Z

    if-eqz v0, :cond_0

    .line 376
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/DocListView;->j()V

    .line 377
    iput-boolean v1, p0, Lcom/google/android/apps/docs/view/DocListView;->b:Z

    goto :goto_0

    .line 351
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public e()V
    .locals 1

    .prologue
    .line 878
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->a()LBJ;

    move-result-object v0

    invoke-interface {v0}, LBJ;->c()V

    .line 879
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 890
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->a()LBJ;

    move-result-object v0

    invoke-interface {v0}, LBJ;->e()V

    .line 891
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 403
    invoke-super {p0}, Lcom/google/android/apps/docs/view/FastScrollView;->onAttachedToWindow()V

    .line 404
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->b:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafw;

    invoke-interface {v0}, Lafw;->a()V

    .line 409
    invoke-super {p0}, Lcom/google/android/apps/docs/view/FastScrollView;->onDetachedFromWindow()V

    .line 410
    return-void
.end method

.method protected onFinishInflate()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 423
    invoke-super {p0}, Lcom/google/android/apps/docs/view/FastScrollView;->onFinishInflate()V

    .line 424
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    .line 425
    sget v0, Lxc;->sticky_header:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/StickyHeaderView;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/apps/docs/view/StickyHeaderView;

    .line 426
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 427
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 430
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 432
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->g:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBa;

    invoke-virtual {v0, p0}, LBa;->a(LapF;)V

    .line 433
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->g:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBa;

    new-instance v1, Lapy;

    invoke-direct {v1, p0}, Lapy;-><init>(Lcom/google/android/apps/docs/view/DocListView;)V

    invoke-virtual {v0, v1}, LBa;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 445
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 446
    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 447
    new-instance v2, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v2, v5, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 449
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 451
    iget-object v2, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    invoke-virtual {v2, v1, v6, v4}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 452
    sget v1, Lxa;->doclist_bottom_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 453
    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 454
    new-instance v2, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v2, v5, v0}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 456
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 458
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v1, v6, v3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 459
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setFooterDividersEnabled(Z)V

    .line 460
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 679
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_0

    .line 680
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 681
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 683
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/docs/view/FastScrollView;->onMeasure(II)V

    .line 684
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 4

    .prologue
    .line 486
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/docs/view/FastScrollView;->onScroll(Landroid/widget/AbsListView;III)V

    .line 487
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView$OnScrollListener;

    .line 488
    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    goto :goto_0

    .line 490
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LZP;

    iget-wide v2, p0, Lcom/google/android/apps/docs/view/DocListView;->a:J

    invoke-virtual {v0, v2, v3}, LZP;->a(J)V

    .line 491
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LQX;

    if-eqz v0, :cond_1

    .line 492
    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/apps/docs/view/DocListView;->a(III)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lafx;

    sget-object v1, Lafx;->b:Lafx;

    .line 493
    invoke-virtual {v0, v1}, Lafx;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 494
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->b:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafw;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LaFM;

    iget-object v2, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LQX;

    iget-object v3, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lafy;

    invoke-interface {v0, v1, v2, v3}, Lafw;->a(LaFM;LQX;Lafy;)V

    .line 497
    :cond_1
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    .prologue
    .line 505
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/docs/view/FastScrollView;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 506
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView$OnScrollListener;

    .line 507
    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    goto :goto_0

    .line 509
    :cond_0
    return-void
.end method

.method public setAccount(LaFM;)V
    .locals 1

    .prologue
    .line 543
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 544
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LaFM;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LaFM;

    invoke-virtual {v0, p1}, LaFM;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 545
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->b:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafw;

    invoke-interface {v0}, Lafw;->a()V

    .line 547
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LaFM;

    .line 548
    return-void
.end method

.method public setArrangeModeListener(LapA;)V
    .locals 0

    .prologue
    .line 870
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapA;

    .line 871
    return-void
.end method

.method public setArrangementMode(LzO;)V
    .locals 4

    .prologue
    .line 842
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 844
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/view/DocListView;->a(LzO;)V

    .line 845
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LzO;

    invoke-virtual {v0, p1}, LzO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 846
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->a()LBJ;

    move-result-object v1

    .line 848
    const-string v0, "DocListView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Arrangement mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 849
    iget-boolean v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Z

    if-eqz v0, :cond_0

    .line 850
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->a()I

    move-result v2

    .line 851
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwg;

    invoke-interface {v0, v2}, Lwg;->a(I)V

    .line 852
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwg;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->b()I

    move-result v2

    invoke-interface {v0, v2}, Lwg;->b(I)V

    .line 853
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Z

    .line 856
    :cond_0
    invoke-interface {v1}, LBJ;->b()V

    .line 857
    invoke-interface {v1}, LBJ;->d()V

    .line 858
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LzO;

    .line 860
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapA;

    if-eqz v0, :cond_1

    .line 861
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapA;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LzO;

    invoke-interface {v0, v1}, LapA;->a(LzO;)V

    .line 863
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_2

    .line 864
    invoke-virtual {p1, p0}, LzO;->a(Landroid/view/View;)V

    .line 867
    :cond_2
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 0

    .prologue
    .line 531
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/FastScrollView;->setBackgroundResource(I)V

    .line 532
    return-void
.end method

.method public setDirectionScrollEnabled(Z)V
    .locals 0

    .prologue
    .line 386
    iput-boolean p1, p0, Lcom/google/android/apps/docs/view/DocListView;->d:Z

    .line 387
    return-void
.end method

.method public setGridViewWidth(I)V
    .locals 1

    .prologue
    .line 330
    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 331
    iput p1, p0, Lcom/google/android/apps/docs/view/DocListView;->b:I

    .line 332
    return-void

    .line 330
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setItemChecked(IZ)V
    .locals 1

    .prologue
    .line 522
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->a()LBJ;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LBJ;->a(IZ)V

    .line 523
    return-void
.end method

.method public setOnDirectionalScrollListener(LapC;)V
    .locals 0

    .prologue
    .line 480
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapC;

    .line 481
    return-void
.end method

.method public setOnEntryClickListener(LapD;)V
    .locals 0

    .prologue
    .line 562
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapD;

    .line 563
    return-void
.end method

.method public setParentFragment(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 335
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/support/v4/app/Fragment;

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/support/v4/app/Fragment;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->a(Landroid/view/View;)V

    .line 337
    instance-of v0, p1, Lcom/google/android/apps/docs/fragment/DocListFragment;

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->g:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBa;

    new-instance v1, Lapx;

    invoke-direct {v1, p0, p1}, Lapx;-><init>(Lcom/google/android/apps/docs/view/DocListView;Landroid/support/v4/app/Fragment;)V

    invoke-virtual {v0, v1}, LBa;->a(Lapv;)V

    .line 345
    :cond_0
    return-void
.end method

.method public setSelectedEntrySpec(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 0

    .prologue
    .line 744
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 745
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->i()V

    .line 746
    return-void
.end method

.method public setSelectionWithCursorPosition(I)V
    .locals 1

    .prologue
    .line 655
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocListView;->a()LBJ;

    move-result-object v0

    invoke-interface {v0, p1}, LBJ;->b(I)V

    .line 656
    return-void
.end method

.method public setSyncStatus(LapE;)V
    .locals 5

    .prologue
    .line 700
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapE;

    invoke-virtual {v0, p1}, LapE;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LaFM;

    if-eqz v0, :cond_0

    .line 701
    const-string v0, "DocListView"

    const-string v1, "syncStatus[%s]=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LaFM;

    invoke-virtual {v4}, LaFM;->a()LaFO;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 703
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapE;

    .line 704
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->g:Lajw;

    invoke-virtual {v0}, Lajw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBa;

    invoke-virtual {v0, p1}, LBa;->a(LapE;)V

    .line 705
    return-void
.end method

.method public setTagName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 415
    if-eqz p1, :cond_0

    .line 416
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:Ljava/lang/String;

    .line 417
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/view/DocListView;->setTag(Ljava/lang/Object;)V

    .line 419
    :cond_0
    return-void

    .line 414
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setViewMode(LapG;)V
    .locals 1

    .prologue
    .line 732
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapG;

    .line 733
    iput-object p1, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapG;

    .line 734
    invoke-virtual {v0, p1}, LapG;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 735
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapH;

    invoke-interface {v0, p1}, LapH;->a(LapG;)V

    .line 737
    :cond_0
    return-void
.end method

.method public setViewModeListener(LapH;)V
    .locals 1

    .prologue
    .line 761
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LapH;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LapH;

    .line 762
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 712
    const-string v1, "%s[mainFilter=%s]"

    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    .line 713
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LQX;

    if-nez v0, :cond_0

    const-string v0, ""

    .line 714
    :goto_0
    aput-object v0, v2, v3

    .line 712
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 713
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/view/DocListView;->a:LQX;

    .line 714
    invoke-virtual {v0}, LQX;->a()LCl;

    move-result-object v0

    goto :goto_0
.end method
