.class public Lcom/google/android/apps/docs/capture/DocScannerActivity;
.super LwN;
.source "DocScannerActivity.java"


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    invoke-static {}, Lcom/google/android/apps/docs/capture/DocScannerActivity;->a()Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/docs/capture/DocScannerActivity;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, LwN;-><init>()V

    return-void
.end method

.method private static a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 95
    :try_start_0
    const-string v0, "com.google.bionics.scanner.CaptureActivity"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 99
    :goto_0
    return-object v0

    .line 96
    :catch_0
    move-exception v0

    .line 99
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f()Z
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/google/android/apps/docs/capture/DocScannerActivity;->a:Ljava/lang/Class;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 50
    const-string v0, "com.google.bionics.scanner.extra.FILE_PATH"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 51
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "file://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 52
    new-instance v1, Labb;

    invoke-direct {v1, p0}, Labb;-><init>(Landroid/content/Context;)V

    const-string v2, "application/pdf"

    .line 54
    invoke-virtual {v1, v0, v2}, Labb;->a(Landroid/net/Uri;Ljava/lang/String;)Labb;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/capture/DocScannerActivity;->a:LaFO;

    .line 55
    invoke-virtual {v0, v1}, Labb;->a(LaFO;)Labb;

    move-result-object v0

    .line 56
    invoke-virtual {v0, v3}, Labb;->a(Z)Labb;

    move-result-object v0

    .line 57
    invoke-virtual {v0, v3}, Labb;->b(Z)Labb;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/capture/DocScannerActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 58
    invoke-virtual {v0, v1}, Labb;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Labb;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Labb;->a()Labb;

    move-result-object v0

    .line 60
    invoke-virtual {v0}, Labb;->a()Landroid/content/Intent;

    move-result-object v0

    .line 61
    iget-object v1, p0, Lcom/google/android/apps/docs/capture/DocScannerActivity;->a:LqK;

    const-string v2, "upload"

    const-string v3, "scanDocument"

    const-string v4, "readyForUpload"

    invoke-virtual {v1, v2, v3, v4}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    return-object v0
.end method

.method protected a(Landroid/content/Intent;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 32
    const-string v0, "android.intent.action.SEND"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 34
    const-string v0, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 35
    invoke-virtual {p0}, Lcom/google/android/apps/docs/capture/DocScannerActivity;->f()V

    .line 45
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/capture/DocScannerActivity;->a:LqK;

    const-string v1, "upload"

    const-string v2, "scanDocument"

    const-string v3, "initiated"

    invoke-virtual {v0, v1, v2, v3}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/google/android/apps/docs/capture/DocScannerActivity;->a:Ljava/lang/Class;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    .line 43
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/docs/capture/DocScannerActivity;->finish()V

    goto :goto_0
.end method
