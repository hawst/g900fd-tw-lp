.class public interface abstract Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;
.super Ljava/lang/Object;
.source "CriterionSet.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Ljava/lang/Iterable",
        "<",
        "Lcom/google/android/apps/docs/app/model/navigation/Criterion;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract a()LCl;
.end method

.method public abstract a()LaFO;
.end method

.method public abstract a(LaGM;)LaeZ;
.end method

.method public abstract a()LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmF",
            "<",
            "LCl;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a()Lcom/google/android/gms/drive/database/data/EntrySpec;
.end method

.method public abstract a(LaGM;)Lcom/google/android/gms/drive/database/data/EntrySpec;
.end method

.method public abstract a(LvR;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LvR",
            "<TT;>;)TT;"
        }
    .end annotation
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public abstract a(LaGM;Landroid/content/Context;)Ljava/lang/String;
.end method

.method public abstract a()LvN;
.end method

.method public abstract a()Z
.end method

.method public abstract a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)Z
.end method
