.class public Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "EditTitleDialogFragment.java"


# instance fields
.field public a:LFa;

.field private a:Landroid/widget/EditText;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    .line 41
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:Landroid/widget/EditText;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;
    .locals 3

    .prologue
    .line 51
    new-instance v0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;-><init>()V

    .line 56
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 57
    const-string v2, "currentDocumentTitle"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const-string v2, "dialogTitle"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->e(Landroid/os/Bundle;)V

    .line 60
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->d:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a()LH;

    move-result-object v0

    invoke-static {v0}, LEL;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v1

    .line 83
    const-string v0, "layout_inflater"

    .line 84
    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 85
    sget v2, Lxe;->edit_title_dialog_content:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 86
    sget v0, Lxc;->title_editor:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:Landroid/widget/EditText;

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 89
    invoke-static {v1}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v0

    .line 90
    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, LEU;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 91
    invoke-virtual {v0, v2}, LEU;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 92
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LEU;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 93
    const v2, 0x104000a

    new-instance v3, LEW;

    invoke-direct {v3, p0}, LEW;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;)V

    invoke-virtual {v0, v2, v3}, LEU;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 101
    const/high16 v2, 0x1040000

    new-instance v3, LEX;

    invoke-direct {v3, p0}, LEX;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;)V

    invoke-virtual {v0, v2, v3}, LEU;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 107
    sget-object v2, LEN;->a:LEN;

    invoke-virtual {v0, v2}, LEU;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 108
    invoke-virtual {v0}, LEU;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 111
    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:Landroid/widget/EditText;

    new-instance v4, LEY;

    invoke-direct {v4, p0, v2}, LEY;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;Landroid/app/AlertDialog;)V

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 120
    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:Landroid/widget/EditText;

    invoke-static {v3, v2}, LEL;->a(Landroid/widget/TextView;Landroid/app/AlertDialog;)V

    .line 122
    new-instance v3, LEZ;

    invoke-direct {v3, p0, v1, v2}, LEZ;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;Landroid/content/Context;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v3}, LEU;->a(Landroid/content/DialogInterface$OnShowListener;)LEU;

    .line 130
    return-object v2
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a_(Landroid/os/Bundle;)V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 75
    const-string v1, "currentDocumentTitle"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->c:Ljava/lang/String;

    .line 76
    const-string v1, "dialogTitle"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->d:Ljava/lang/String;

    .line 77
    return-void
.end method

.method public o_()V
    .locals 2

    .prologue
    .line 143
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->o_()V

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 145
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 135
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:LFa;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:LFa;

    invoke-interface {v0}, LFa;->k()V

    .line 139
    :cond_0
    return-void
.end method
