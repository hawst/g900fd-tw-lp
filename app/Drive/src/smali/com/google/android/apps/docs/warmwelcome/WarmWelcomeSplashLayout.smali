.class public Lcom/google/android/apps/docs/warmwelcome/WarmWelcomeSplashLayout;
.super Landroid/widget/LinearLayout;
.source "WarmWelcomeSplashLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 19
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/docs/warmwelcome/WarmWelcomeSplashLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/warmwelcome/WarmWelcomeSplashLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/warmwelcome/WarmWelcomeSplashLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method private a(II)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 68
    new-instance v1, Landroid/util/StringBuilderPrinter;

    invoke-direct {v1, v0}, Landroid/util/StringBuilderPrinter;-><init>(Ljava/lang/StringBuilder;)V

    .line 69
    if-eqz p1, :cond_0

    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/docs/warmwelcome/WarmWelcomeSplashLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/StringBuilderPrinter;->println(Ljava/lang/String;)V

    .line 72
    :cond_0
    if-eqz p2, :cond_1

    .line 73
    invoke-virtual {p0}, Lcom/google/android/apps/docs/warmwelcome/WarmWelcomeSplashLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/StringBuilderPrinter;->println(Ljava/lang/String;)V

    .line 75
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 33
    const-string v0, "layout_inflater"

    .line 34
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 35
    sget v1, LarR;->warm_welcome_splash:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 37
    if-nez p2, :cond_0

    .line 64
    :goto_0
    return-void

    .line 40
    :cond_0
    sget-object v0, LarS;->WarmWelcome:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 42
    sget v0, LarQ;->splash_image:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/warmwelcome/WarmWelcomeSplashLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 43
    sget v1, LarQ;->splash_text:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/warmwelcome/WarmWelcomeSplashLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 44
    sget v2, LarQ;->splash_blurb:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/warmwelcome/WarmWelcomeSplashLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 46
    sget v4, LarS;->WarmWelcome_heading:I

    invoke-virtual {v3, v4, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    .line 47
    if-eqz v4, :cond_1

    .line 48
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    .line 51
    :cond_1
    sget v1, LarS;->WarmWelcome_description:I

    invoke-virtual {v3, v1, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 52
    if-eqz v1, :cond_2

    .line 53
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    .line 56
    :cond_2
    sget v2, LarS;->WarmWelcome_image:I

    invoke-virtual {v3, v2, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 57
    if-eqz v2, :cond_3

    .line 58
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 61
    :cond_3
    invoke-direct {p0, v4, v1}, Lcom/google/android/apps/docs/warmwelcome/WarmWelcomeSplashLayout;->a(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/warmwelcome/WarmWelcomeSplashLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 63
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0
.end method
