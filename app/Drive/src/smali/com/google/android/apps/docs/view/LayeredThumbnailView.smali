.class public Lcom/google/android/apps/docs/view/LayeredThumbnailView;
.super Lcom/google/android/apps/docs/view/DocThumbnailView;
.source "LayeredThumbnailView.java"


# static fields
.field private static final a:Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "LapI;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 25
    new-instance v0, Laqa;

    const-class v1, Ljava/lang/Float;

    const-string v2, "thumbnail_alpha"

    invoke-direct {v0, v1, v2}, Laqa;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/docs/view/LayeredThumbnailView;->a:Landroid/util/Property;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/view/DocThumbnailView;-><init>(Landroid/content/Context;)V

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/view/DocThumbnailView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/view/DocThumbnailView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 83
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LayeredThumbnailView;->a:Ljava/util/EnumSet;

    if-nez v0, :cond_0

    .line 87
    const-class v0, LapI;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/LayeredThumbnailView;->a:Ljava/util/EnumSet;

    .line 89
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Landroid/util/Property;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Property",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    sget-object v0, Lcom/google/android/apps/docs/view/LayeredThumbnailView;->a:Landroid/util/Property;

    return-object v0
.end method

.method public a(LapI;)Z
    .locals 1

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/LayeredThumbnailView;->b()V

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LayeredThumbnailView;->a:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public onCreateDrawableState(I)[I
    .locals 3

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/LayeredThumbnailView;->b()V

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LayeredThumbnailView;->a:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->size()I

    move-result v0

    invoke-super {p0, v0}, Lcom/google/android/apps/docs/view/DocThumbnailView;->onCreateDrawableState(I)[I

    move-result-object v1

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LayeredThumbnailView;->a:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LapI;

    .line 97
    invoke-virtual {v0}, LapI;->a()[I

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/apps/docs/view/LayeredThumbnailView;->mergeDrawableStates([I[I)[I

    goto :goto_0

    .line 99
    :cond_0
    return-object v1
.end method

.method public setState(LapI;Z)V
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/LayeredThumbnailView;->b()V

    .line 105
    if-eqz p2, :cond_1

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LayeredThumbnailView;->a:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LayeredThumbnailView;->refreshDrawableState()V

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/view/LayeredThumbnailView;->a:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LayeredThumbnailView;->refreshDrawableState()V

    goto :goto_0
.end method

.method public setThumbnail(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LayeredThumbnailView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    .line 125
    invoke-virtual {v0}, Landroid/graphics/drawable/LayerDrawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 126
    sget v1, Lxc;->thumbnail:I

    invoke-virtual {v0, v1, p1}, Landroid/graphics/drawable/LayerDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 127
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/LayeredThumbnailView;->invalidate()V

    .line 128
    return-void
.end method
