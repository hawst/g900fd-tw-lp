.class public abstract Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;
.super Ljava/lang/Object;
.source "SimpleCriterion.java"

# interfaces
.implements Lcom/google/android/apps/docs/app/model/navigation/Criterion;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LaeZ;

.field private final a:Ljava/lang/String;

.field private final a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 44
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;->a:Ljava/util/Map;

    .line 45
    sget-object v0, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;->a:Ljava/util/Map;

    const-string v1, "notInTrash"

    new-instance v2, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion$1;

    const-string v3, "notInTrash"

    sget-object v4, LaeZ;->a:LaeZ;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion$1;-><init>(Ljava/lang/String;LaeZ;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;->a:Ljava/util/Map;

    const-string v1, "noCollection"

    new-instance v2, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion$2;

    const-string v3, "noCollection"

    sget-object v4, LaeZ;->b:LaeZ;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion$2;-><init>(Ljava/lang/String;LaeZ;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;->a:Ljava/util/Map;

    const-string v1, "noPlaceholder"

    new-instance v2, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion$3;

    const-string v3, "noPlaceholder"

    sget-object v4, LaeZ;->a:LaeZ;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion$3;-><init>(Ljava/lang/String;LaeZ;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;->a:Ljava/util/Map;

    const-string v1, "plusMediaRootFolder"

    new-instance v2, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion$4;

    const-string v3, "plusMediaRootFolder"

    sget-object v4, LaeZ;->c:LaeZ;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion$4;-><init>(Ljava/lang/String;LaeZ;Z)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    new-instance v0, Lwt;

    invoke-direct {v0}, Lwt;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;LaeZ;Z)V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-object p1, p0, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;->a:Ljava/lang/String;

    .line 99
    iput-object p2, p0, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;->a:LaeZ;

    .line 100
    iput-boolean p3, p0, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;->a:Z

    .line 101
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;LaeZ;ZLcom/google/android/apps/docs/app/model/navigation/SimpleCriterion$1;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;-><init>(Ljava/lang/String;LaeZ;Z)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;

    .line 82
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;

    return-object v0
.end method


# virtual methods
.method public a(LaGM;)LaeZ;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;->a:LaeZ;

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;->a:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 116
    if-ne p1, p0, :cond_0

    .line 117
    const/4 v0, 0x1

    .line 122
    :goto_0
    return v0

    .line 118
    :cond_0
    instance-of v0, p1, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;

    if-eqz v0, :cond_1

    .line 119
    check-cast p1, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 122
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 129
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 161
    const-string v0, "SimpleCriterion {kind = \"%s\"}"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/SimpleCriterion;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 140
    return-void
.end method
