.class public Lcom/google/android/apps/docs/print/LegacyPrintActivity;
.super Lrm;
.source "LegacyPrintActivity.java"


# static fields
.field private static final a:Landroid/net/Uri;


# instance fields
.field private final a:LGH;

.field private a:LGI;

.field public a:LQr;

.field public a:LTd;

.field public a:LUT;

.field private a:LWJ;

.field public a:Lald;

.field private a:Landroid/view/View;

.field private a:Landroid/webkit/WebView;

.field private a:Landroid/widget/Button;

.field private a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

.field public a:Ljava/lang/Class;
    .annotation runtime Lbxv;
        a = "DocListActivity"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/lang/String;

.field private a:LpD;

.field private final b:Landroid/os/Handler;

.field private b:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    const-string v0, "https://www.google.com/cloudprint/dialog.html?skin=holo"

    .line 73
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:Landroid/net/Uri;

    .line 72
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Lrm;-><init>()V

    .line 123
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->b:Landroid/os/Handler;

    .line 425
    new-instance v0, LWG;

    invoke-direct {v0, p0}, LWG;-><init>(Lcom/google/android/apps/docs/print/LegacyPrintActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:LGH;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/print/LegacyPrintActivity;)LGI;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:LGI;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/print/LegacyPrintActivity;LGI;)LGI;
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:LGI;

    return-object p1
.end method

.method public static a(Landroid/content/Context;LaGu;)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 337
    invoke-static {p1}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a(LaGu;)Ljava/lang/String;

    move-result-object v1

    .line 339
    if-nez v1, :cond_1

    .line 354
    :cond_0
    :goto_0
    return-object v0

    .line 342
    :cond_1
    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v2

    .line 343
    if-eqz v2, :cond_0

    .line 348
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/docs/print/LegacyPrintActivity;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 350
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 351
    const-string v1, "documentTitle"

    invoke-interface {p1}, LaGu;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 352
    const-string v1, "resourceSpec"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/print/LegacyPrintActivity;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:Landroid/webkit/WebView;

    return-object v0
.end method

.method private static a()LbmY;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 300
    const-string v0, "application/msword"

    const-string v1, "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

    const-string v2, "application/vnd.oasis.opendocument.text"

    const-string v3, "application/rtf"

    const-string v4, "application/vnd.google-apps.kix"

    const-string v5, "application/vnd.google-apps.document"

    const/16 v6, 0x18

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "application/vnd.google-ocean.goodoc"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "application/vnd.ms-excel"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-string v8, "application/vnd.oasis.opendocument.spreadsheet"

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const-string v8, "text/csv"

    aput-object v8, v6, v7

    const/4 v7, 0x5

    const-string v8, "text/tsv"

    aput-object v8, v6, v7

    const/4 v7, 0x6

    const-string v8, "application/vnd.google-apps.spreadsheet"

    aput-object v8, v6, v7

    const/4 v7, 0x7

    const-string v8, "text/plain"

    aput-object v8, v6, v7

    const/16 v7, 0x8

    const-string v8, "application/vnd.ms-powerpoint"

    aput-object v8, v6, v7

    const/16 v7, 0x9

    const-string v8, "application/vnd.openxmlformats-officedocument.presentationml.presentation"

    aput-object v8, v6, v7

    const/16 v7, 0xa

    const-string v8, "application/vnd.google-apps.presentation"

    aput-object v8, v6, v7

    const/16 v7, 0xb

    const-string v8, "application/vnd.google-apps.punch"

    aput-object v8, v6, v7

    const/16 v7, 0xc

    const-string v8, "image/bmp"

    aput-object v8, v6, v7

    const/16 v7, 0xd

    const-string v8, "image/gif"

    aput-object v8, v6, v7

    const/16 v7, 0xe

    const-string v8, "image/jpeg"

    aput-object v8, v6, v7

    const/16 v7, 0xf

    const-string v8, "image/png"

    aput-object v8, v6, v7

    const/16 v7, 0x10

    const-string v8, "image/tiff"

    aput-object v8, v6, v7

    const/16 v7, 0x11

    const-string v8, "text/html"

    aput-object v8, v6, v7

    const/16 v7, 0x12

    const-string v8, "multipart/related"

    aput-object v8, v6, v7

    const/16 v7, 0x13

    const-string v8, "application/x-eps"

    aput-object v8, v6, v7

    const/16 v7, 0x14

    const-string v8, "application/postscript"

    aput-object v8, v6, v7

    const/16 v7, 0x15

    const-string v8, "application/vnd.ms-xpsdocument"

    aput-object v8, v6, v7

    const/16 v7, 0x16

    const-string v8, "image/photoshop"

    aput-object v8, v6, v7

    const/16 v7, 0x17

    const-string v8, "application/pdf"

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LbmY;

    move-result-object v0

    .line 332
    return-object v0
.end method

.method public static a(LaGu;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 263
    invoke-interface {p0}, LaGu;->a()LaGv;

    move-result-object v0

    .line 264
    invoke-interface {p0}, LaGu;->f()Ljava/lang/String;

    move-result-object v1

    .line 266
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a(LaGv;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a(LaGv;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 271
    if-nez p1, :cond_1

    .line 290
    :cond_0
    :goto_0
    return-object v0

    .line 277
    :cond_1
    invoke-static {}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a()LbmY;

    move-result-object v1

    invoke-virtual {v1, p1}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 281
    sget-object v0, LaGv;->b:LaGv;

    if-ne p0, v0, :cond_2

    .line 282
    const-string v0, "google.kix"

    goto :goto_0

    .line 283
    :cond_2
    sget-object v0, LaGv;->i:LaGv;

    if-ne p0, v0, :cond_3

    .line 284
    const-string v0, "google.spreadsheet"

    goto :goto_0

    .line 285
    :cond_3
    sget-object v0, LaGv;->g:LaGv;

    if-ne p0, v0, :cond_4

    .line 286
    const-string v0, "google.presentation"

    goto :goto_0

    .line 287
    :cond_4
    sget-object v0, LaGv;->c:LaGv;

    if-ne p0, v0, :cond_5

    .line 288
    const-string v0, "google.drawing"

    goto :goto_0

    .line 290
    :cond_5
    const-string v0, "google.drive"

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/print/LegacyPrintActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:LQr;

    const-string v1, "printUrl"

    sget-object v2, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:Landroid/net/Uri;

    .line 194
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 193
    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 197
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 198
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 199
    const-string v2, "hl"

    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 200
    const-string v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 201
    const-string v1, "contentType"

    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 202
    const-string v1, "content"

    iget-object v2, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 204
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/print/LegacyPrintActivity;)LpD;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:LpD;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/print/LegacyPrintActivity;LpD;)LpD;
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:LpD;

    return-object p1
.end method

.method private a(Landroid/view/View;)V
    .locals 8

    .prologue
    const-wide v6, 0x3fc999999999999aL    # 0.2

    const-wide v4, 0x3fa999999999999aL    # 0.05

    .line 208
    .line 213
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 214
    invoke-static {p0}, LakZ;->a(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    .line 215
    iget v2, v0, Landroid/graphics/Point;->x:I

    .line 216
    iget v0, v0, Landroid/graphics/Point;->y:I

    .line 223
    invoke-virtual {p0}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 224
    invoke-static {v1}, LakQ;->e(Landroid/content/res/Resources;)Z

    move-result v3

    .line 225
    invoke-static {v1}, LakQ;->a(Landroid/content/res/Resources;)Z

    move-result v1

    .line 227
    if-eqz v3, :cond_1

    if-eqz v1, :cond_1

    .line 228
    int-to-double v0, v0

    const-wide v4, 0x3fc3333333333333L    # 0.15

    mul-double/2addr v0, v4

    double-to-int v1, v0

    .line 229
    int-to-double v2, v2

    mul-double/2addr v2, v6

    double-to-int v0, v2

    .line 241
    :goto_0
    invoke-static {p1, v0}, LarK;->a(Landroid/view/View;I)V

    .line 242
    invoke-static {p1, v1}, LarK;->c(Landroid/view/View;I)V

    .line 243
    invoke-static {p1, v0}, LarK;->b(Landroid/view/View;I)V

    .line 244
    invoke-static {p1, v1}, LarK;->d(Landroid/view/View;I)V

    .line 245
    :cond_0
    return-void

    .line 230
    :cond_1
    if-eqz v3, :cond_2

    if-nez v1, :cond_2

    .line 231
    int-to-double v0, v0

    mul-double/2addr v0, v4

    double-to-int v1, v0

    .line 232
    int-to-double v2, v2

    const-wide v4, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v2, v4

    double-to-int v0, v2

    goto :goto_0

    .line 233
    :cond_2
    if-nez v3, :cond_3

    if-eqz v1, :cond_3

    .line 234
    int-to-double v0, v0

    mul-double/2addr v0, v6

    double-to-int v1, v0

    .line 235
    int-to-double v2, v2

    mul-double/2addr v2, v6

    double-to-int v0, v2

    goto :goto_0

    .line 237
    :cond_3
    int-to-double v0, v0

    mul-double/2addr v0, v4

    double-to-int v1, v0

    .line 238
    int-to-double v2, v2

    mul-double/2addr v2, v4

    double-to-int v0, v2

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/print/LegacyPrintActivity;)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->f()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/print/LegacyPrintActivity;Z)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->c(Z)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 490
    const-string v0, "PrintDialogActivity"

    const-string v1, "showUrl(%s)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 492
    if-eqz p1, :cond_0

    .line 493
    iget-object v0, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:LWJ;

    invoke-virtual {v0, p1}, LWJ;->a(Ljava/lang/String;)V

    .line 495
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 2

    .prologue
    .line 254
    iget-object v1, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->b:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 255
    return-void

    .line 254
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private f()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 498
    invoke-direct {p0, v4}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->c(Z)V

    .line 499
    sget v0, Lxi;->print_error:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 500
    const-string v1, "PrintDialogActivity"

    const-string v2, "showErrorMessage - %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 501
    iget-object v1, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:Lald;

    iget-object v2, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:Landroid/webkit/WebView;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v0, v3}, Lald;->a(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 502
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 249
    invoke-super {p0, p1}, Lrm;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a(Landroid/view/View;)V

    .line 251
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v8, 0x0

    const/4 v11, 0x1

    .line 131
    const-string v0, "PrintDialogActivity"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 134
    invoke-virtual {p0}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 135
    invoke-virtual {v0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v1

    .line 136
    const-string v2, "documentTitle"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 137
    const-string v3, "resourceSpec"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/ResourceSpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    .line 139
    const-string v0, "PrintDialogActivity"

    const-string v3, "Printing %s [resourceId=%s, type=%s]"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v8

    iget-object v5, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    .line 140
    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v11

    const/4 v5, 0x2

    aput-object v1, v4, v5

    .line 139
    invoke-static {v0, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 143
    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:Ljava/lang/String;

    .line 146
    invoke-virtual {p0}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lxe;->print_dialog:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 147
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->setContentView(Landroid/view/View;)V

    .line 149
    sget v1, Lxc;->progress_block:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->b:Landroid/view/View;

    .line 153
    sget v1, Lxc;->dialog_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:Landroid/view/View;

    .line 154
    iget-object v1, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a(Landroid/view/View;)V

    .line 156
    sget v1, Lxc;->cancel_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:Landroid/widget/Button;

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:Landroid/widget/Button;

    new-instance v1, LWF;

    invoke-direct {v1, p0}, LWF;-><init>(Lcom/google/android/apps/docs/print/LegacyPrintActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    invoke-virtual {p0}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a()LM;

    move-result-object v0

    .line 166
    sget v1, Lxc;->webview_fragment:I

    .line 167
    invoke-virtual {v0, v1}, LM;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/fragment/WebViewFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/WebViewFragment;->a()Landroid/webkit/WebView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:Landroid/webkit/WebView;

    .line 169
    new-instance v0, LWJ;

    iget-object v3, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:LGH;

    iget-object v1, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    iget-object v4, v1, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    iget-object v5, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:LQr;

    iget-object v6, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:Ljava/lang/Class;

    iget-object v7, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:LUT;

    const-string v1, "webview"

    .line 175
    invoke-virtual {p0, v1, v8}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:LTd;

    iget-object v10, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->b:Landroid/os/Handler;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v10}, LWJ;-><init>(Lcom/google/android/apps/docs/print/LegacyPrintActivity;Landroid/content/Context;LGH;LaFO;LQr;Ljava/lang/Class;LUT;Landroid/content/SharedPreferences;LTd;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:LWJ;

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:LWJ;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 182
    invoke-virtual {v0, v11}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:Landroid/webkit/WebView;

    new-instance v1, LWI;

    invoke-direct {v1, p0}, LWI;-><init>(Lcom/google/android/apps/docs/print/LegacyPrintActivity;)V

    const-string v2, "AndroidPrintDialog"

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 188
    invoke-direct {p0, v11}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->c(Z)V

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->b(Ljava/lang/String;)V

    .line 190
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:LpD;

    if-eqz v0, :cond_0

    .line 483
    iget-object v0, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:LpD;

    invoke-virtual {v0}, LpD;->a()V

    .line 484
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/print/LegacyPrintActivity;->a:LpD;

    .line 486
    :cond_0
    invoke-super {p0}, Lrm;->onDestroy()V

    .line 487
    return-void
.end method
