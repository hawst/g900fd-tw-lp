.class public Lcom/google/android/apps/viewer/film/HorizontalScrollView;
.super Landroid/widget/FrameLayout;
.source "HorizontalScrollView.java"


# instance fields
.field protected a:I

.field private a:J

.field private final a:Landroid/graphics/Rect;

.field private a:Landroid/view/VelocityTracker;

.field private a:Landroid/view/View;

.field protected a:Landroid/widget/EdgeEffect;

.field protected a:LavA;

.field private a:Z

.field private b:I

.field protected b:Landroid/widget/EdgeEffect;

.field private b:Z

.field private c:I

.field private c:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field

.field private d:I

.field private d:Z

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 119
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 122
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    .line 65
    iput-boolean v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Z

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/view/View;

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Z

    .line 96
    iput-boolean v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->d:Z

    .line 109
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->g:I

    .line 123
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a()V

    .line 124
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 127
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    .line 65
    iput-boolean v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Z

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/view/View;

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Z

    .line 96
    iput-boolean v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->d:Z

    .line 109
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->g:I

    .line 128
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a()V

    .line 129
    return-void
.end method

.method private static a(III)I
    .locals 1

    .prologue
    .line 1594
    if-ge p1, p2, :cond_0

    if-gez p0, :cond_2

    .line 1595
    :cond_0
    const/4 p0, 0x0

    .line 1600
    :cond_1
    :goto_0
    return p0

    .line 1597
    :cond_2
    add-int v0, p1, p0

    if-le v0, p2, :cond_1

    .line 1598
    sub-int p0, p2, p1

    goto :goto_0
.end method

.method private a(ZII)Landroid/view/View;
    .locals 11

    .prologue
    .line 873
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getFocusables(I)Ljava/util/ArrayList;

    move-result-object v6

    .line 874
    const/4 v3, 0x0

    .line 883
    const/4 v2, 0x0

    .line 885
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    .line 886
    const/4 v0, 0x0

    move v5, v0

    :goto_0
    if-ge v5, v7, :cond_7

    .line 887
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 888
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    .line 889
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v8

    .line 891
    if-ge p2, v8, :cond_8

    if-ge v4, p3, :cond_8

    .line 897
    if-ge p2, v4, :cond_0

    if-ge v8, p3, :cond_0

    const/4 v1, 0x1

    .line 900
    :goto_1
    if-nez v3, :cond_1

    move v10, v1

    move-object v1, v0

    move v0, v10

    .line 886
    :goto_2
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move-object v3, v1

    move v2, v0

    goto :goto_0

    .line 897
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 905
    :cond_1
    if-eqz p1, :cond_2

    .line 906
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v9

    if-lt v4, v9, :cond_3

    :cond_2
    if-nez p1, :cond_4

    .line 907
    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v4

    if-le v8, v4, :cond_4

    :cond_3
    const/4 v4, 0x1

    .line 909
    :goto_3
    if-eqz v2, :cond_5

    .line 910
    if-eqz v1, :cond_8

    if-eqz v4, :cond_8

    move-object v1, v0

    move v0, v2

    .line 916
    goto :goto_2

    .line 907
    :cond_4
    const/4 v4, 0x0

    goto :goto_3

    .line 919
    :cond_5
    if-eqz v1, :cond_6

    .line 922
    const/4 v1, 0x1

    move v10, v1

    move-object v1, v0

    move v0, v10

    goto :goto_2

    .line 923
    :cond_6
    if-eqz v4, :cond_8

    move-object v1, v0

    move v0, v2

    .line 928
    goto :goto_2

    .line 935
    :cond_7
    return-object v3

    :cond_8
    move v0, v2

    move-object v1, v3

    goto :goto_2
.end method

.method private a(ZILandroid/view/View;)Landroid/view/View;
    .locals 3

    .prologue
    .line 842
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getHorizontalFadingEdgeLength()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 843
    add-int v1, p2, v0

    .line 844
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getWidth()I

    move-result v2

    add-int/2addr v2, p2

    sub-int v0, v2, v0

    .line 846
    if-eqz p3, :cond_0

    .line 847
    invoke-virtual {p3}, Landroid/view/View;->getLeft()I

    move-result v2

    if-ge v2, v0, :cond_0

    .line 848
    invoke-virtual {p3}, Landroid/view/View;->getRight()I

    move-result v2

    if-le v2, v1, :cond_0

    .line 852
    :goto_0
    return-object p3

    :cond_0
    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(ZII)Landroid/view/View;

    move-result-object p3

    goto :goto_0
.end method

.method private a()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 174
    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->setHorizontalScrollBarEnabled(Z)V

    .line 175
    invoke-virtual {p0, v1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->setFillViewport(Z)V

    .line 176
    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->setFocusable(Z)V

    .line 177
    const/high16 v0, 0x40000

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->setDescendantFocusability(I)V

    .line 178
    invoke-virtual {p0, v1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->setWillNotDraw(Z)V

    .line 179
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 180
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c:I

    .line 181
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->d:I

    .line 182
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->e:I

    .line 183
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledOverscrollDistance()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->f:I

    .line 184
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledOverflingDistance()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:I

    .line 185
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 661
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const v1, 0xff00

    and-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x8

    .line 663
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    .line 664
    iget v2, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->g:I

    if-ne v1, v2, :cond_0

    .line 668
    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 669
    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:I

    .line 670
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->g:I

    .line 671
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 672
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 675
    :cond_0
    return-void

    .line 668
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1287
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1290
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1292
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(Landroid/graphics/Rect;)I

    move-result v0

    .line 1294
    if-eqz v0, :cond_0

    .line 1295
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->scrollBy(II)V

    .line 1297
    :cond_0
    return-void
.end method

.method private a()Z
    .locals 3

    .prologue
    .line 227
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getWidth()I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(II)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 369
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 370
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v1

    .line 371
    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 372
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v3

    if-lt p2, v3, :cond_0

    .line 373
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v3

    if-ge p2, v3, :cond_0

    .line 374
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    sub-int/2addr v2, v1

    if-lt p1, v2, :cond_0

    .line 376
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c()I

    move-result v2

    sub-int v1, v2, v1

    if-ge p1, v1, :cond_0

    const/4 v0, 0x1

    .line 378
    :cond_0
    return v0
.end method

.method private a(III)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1016
    .line 1018
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getWidth()I

    move-result v0

    .line 1019
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v4

    .line 1020
    add-int v5, v4, v0

    .line 1021
    const/16 v0, 0x11

    if-ne p1, v0, :cond_2

    move v0, v1

    .line 1023
    :goto_0
    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(ZII)Landroid/view/View;

    move-result-object v3

    .line 1024
    if-nez v3, :cond_0

    move-object v3, p0

    .line 1028
    :cond_0
    if-lt p2, v4, :cond_3

    if-gt p3, v5, :cond_3

    .line 1035
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->findFocus()Landroid/view/View;

    move-result-object v0

    if-eq v3, v0, :cond_1

    invoke-virtual {v3, p1}, Landroid/view/View;->requestFocus(I)Z

    .line 1037
    :cond_1
    return v2

    :cond_2
    move v0, v2

    .line 1021
    goto :goto_0

    .line 1031
    :cond_3
    if-eqz v0, :cond_4

    sub-int v0, p2, v4

    .line 1032
    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b(I)V

    move v2, v1

    goto :goto_1

    .line 1031
    :cond_4
    sub-int v0, p3, v5

    goto :goto_2
.end method

.method private a(Landroid/graphics/Rect;Z)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1308
    invoke-virtual {p0, p1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(Landroid/graphics/Rect;)I

    move-result v2

    .line 1309
    if-eqz v2, :cond_1

    const/4 v0, 0x1

    .line 1310
    :goto_0
    if-eqz v0, :cond_0

    .line 1311
    if-eqz p2, :cond_2

    .line 1312
    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->scrollBy(II)V

    .line 1317
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v0, v1

    .line 1309
    goto :goto_0

    .line 1314
    :cond_2
    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(II)V

    goto :goto_1
.end method

.method private a(Landroid/view/View;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1104
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(Landroid/view/View;I)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private a(Landroid/view/View;I)Z
    .locals 3

    .prologue
    .line 1112
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1113
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1115
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, p2

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, p2

    .line 1116
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getWidth()I

    move-result v2

    add-int/2addr v1, v2

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/view/View;Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1485
    if-ne p0, p1, :cond_0

    .line 1490
    :goto_0
    return v1

    .line 1489
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1490
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(Landroid/view/View;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1125
    if-eqz p1, :cond_0

    .line 1126
    iget-boolean v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->d:Z

    if-eqz v0, :cond_1

    .line 1127
    invoke-virtual {p0, p1, v1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(II)V

    .line 1132
    :cond_0
    :goto_0
    return-void

    .line 1129
    :cond_1
    invoke-virtual {p0, p1, v1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->scrollBy(II)V

    goto :goto_0
.end method

.method private c()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 232
    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 233
    if-eqz v1, :cond_0

    .line 234
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1}, Landroid/view/View;->getScaleX()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 236
    :cond_0
    return v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    .line 383
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/view/VelocityTracker;

    .line 387
    :goto_0
    return-void

    .line 385
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_0
.end method

.method private c(II)V
    .locals 2

    .prologue
    .line 736
    :try_start_0
    const-class v0, Landroid/view/View;

    const-string v1, "mScrollX"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 737
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 738
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 739
    const-class v0, Landroid/view/View;

    const-string v1, "mScrollY"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 740
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 741
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 745
    return-void

    .line 742
    :catch_0
    move-exception v0

    .line 743
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private d()V
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    .line 391
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/view/VelocityTracker;

    .line 393
    :cond_0
    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 398
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/view/VelocityTracker;

    .line 400
    :cond_0
    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 166
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method protected a(Landroid/graphics/Rect;)I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1329
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 1383
    :goto_0
    return v2

    .line 1331
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getWidth()I

    move-result v3

    .line 1332
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v0

    .line 1333
    add-int v1, v0, v3

    .line 1335
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getHorizontalFadingEdgeLength()I

    move-result v4

    .line 1338
    iget v5, p1, Landroid/graphics/Rect;->left:I

    if-lez v5, :cond_1

    .line 1339
    add-int/2addr v0, v4

    .line 1343
    :cond_1
    iget v5, p1, Landroid/graphics/Rect;->right:I

    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c()I

    move-result v6

    if-ge v5, v6, :cond_2

    .line 1344
    sub-int/2addr v1, v4

    .line 1349
    :cond_2
    iget v4, p1, Landroid/graphics/Rect;->right:I

    if-le v4, v1, :cond_4

    iget v4, p1, Landroid/graphics/Rect;->left:I

    if-le v4, v0, :cond_4

    .line 1354
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v4

    if-le v4, v3, :cond_3

    .line 1356
    iget v3, p1, Landroid/graphics/Rect;->left:I

    sub-int v0, v3, v0

    add-int/2addr v0, v2

    .line 1363
    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c()I

    move-result v2

    .line 1364
    sub-int v1, v2, v1

    .line 1365
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_2
    move v2, v0

    .line 1383
    goto :goto_0

    .line 1359
    :cond_3
    iget v0, p1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    add-int/2addr v0, v2

    goto :goto_1

    .line 1367
    :cond_4
    iget v4, p1, Landroid/graphics/Rect;->left:I

    if-ge v4, v0, :cond_6

    iget v4, p1, Landroid/graphics/Rect;->right:I

    if-ge v4, v1, :cond_6

    .line 1372
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v4

    if-le v4, v3, :cond_5

    .line 1374
    iget v0, p1, Landroid/graphics/Rect;->right:I

    sub-int v0, v1, v0

    sub-int v0, v2, v0

    .line 1381
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v1

    neg-int v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_2

    .line 1377
    :cond_5
    iget v1, p1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    sub-int v0, v2, v0

    goto :goto_3

    :cond_6
    move v0, v2

    goto :goto_2
.end method

.method public a(I)V
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 1501
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 1502
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingLeft()I

    move-result v1

    sub-int v3, v0, v1

    .line 1503
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c()I

    move-result v5

    .line 1505
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:LavA;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollY()I

    move-result v2

    sub-int/2addr v5, v3

    .line 1506
    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v6

    div-int/lit8 v9, v3, 0x2

    move v3, p1

    move v5, v4

    move v7, v4

    move v8, v4

    move v10, v4

    .line 1505
    invoke-interface/range {v0 .. v10}, LavA;->a(IIIIIIIIII)V

    .line 1508
    if-lez p1, :cond_0

    const/4 v4, 0x1

    .line 1510
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->findFocus()Landroid/view/View;

    move-result-object v2

    .line 1511
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:LavA;

    .line 1512
    invoke-interface {v0}, LavA;->d()I

    move-result v0

    .line 1511
    invoke-direct {p0, v4, v0, v2}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(ZILandroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 1514
    if-nez v0, :cond_4

    move-object v1, p0

    .line 1518
    :goto_0
    if-eq v1, v2, :cond_1

    .line 1519
    if-eqz v4, :cond_3

    const/16 v0, 0x42

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->requestFocus(I)Z

    .line 1522
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->postInvalidateOnAnimation()V

    .line 1524
    :cond_2
    return-void

    .line 1519
    :cond_3
    const/16 v0, 0x11

    goto :goto_1

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(II)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1141
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 1162
    :goto_0
    return-void

    .line 1145
    :cond_0
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:J

    sub-long/2addr v0, v2

    .line 1146
    const-wide/16 v2, 0xfa

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 1147
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1148
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c()I

    move-result v1

    .line 1149
    sub-int v0, v1, v0

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1150
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v1

    .line 1151
    add-int v2, v1, p1

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    sub-int/2addr v0, v1

    .line 1153
    iget-object v2, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:LavA;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollY()I

    move-result v3

    invoke-interface {v2, v1, v3, v0, v4}, LavA;->a(IIII)V

    .line 1154
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->postInvalidateOnAnimation()V

    .line 1161
    :goto_1
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:J

    goto :goto_0

    .line 1156
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:LavA;

    invoke-interface {v0}, LavA;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1157
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:LavA;

    invoke-interface {v0}, LavA;->a()V

    .line 1159
    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->scrollBy(II)V

    goto :goto_1
.end method

.method public a(LavA;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:LavA;

    .line 171
    return-void
.end method

.method public a(I)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 951
    const/16 v0, 0x42

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    .line 952
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getWidth()I

    move-result v2

    .line 954
    if-eqz v0, :cond_2

    .line 955
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v1

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 956
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c()I

    move-result v0

    .line 957
    if-lez v0, :cond_0

    .line 958
    iget-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    if-le v1, v0, :cond_0

    .line 959
    iget-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    sub-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 968
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 970
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(III)Z

    move-result v0

    return v0

    :cond_1
    move v0, v1

    .line 951
    goto :goto_0

    .line 963
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v3

    sub-int/2addr v3, v2

    iput v3, v0, Landroid/graphics/Rect;->left:I

    .line 964
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    if-gez v0, :cond_0

    .line 965
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    iput v1, v0, Landroid/graphics/Rect;->left:I

    goto :goto_1
.end method

.method public a(Landroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/16 v0, 0x11

    const/4 v1, 0x0

    const/16 v2, 0x42

    .line 328
    iget-object v3, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->setEmpty()V

    .line 330
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a()Z

    move-result v3

    if-nez v3, :cond_3

    .line 331
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 332
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 333
    if-ne v0, p0, :cond_0

    const/4 v0, 0x0

    .line 334
    :cond_0
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v3

    invoke-virtual {v3, p0, v0, v2}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 336
    if-eqz v0, :cond_2

    if-eq v0, p0, :cond_2

    .line 337
    invoke-virtual {v0, v2}, Landroid/view/View;->requestFocus(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v1, v0

    .line 365
    :cond_1
    :goto_1
    return v1

    :cond_2
    move v0, v1

    .line 337
    goto :goto_0

    .line 343
    :cond_3
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_1

    .line 344
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    goto :goto_1

    .line 346
    :sswitch_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v1

    if-nez v1, :cond_4

    .line 347
    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c(I)Z

    move-result v1

    goto :goto_1

    .line 349
    :cond_4
    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b(I)Z

    move-result v1

    goto :goto_1

    .line 353
    :sswitch_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v0

    if-nez v0, :cond_5

    .line 354
    invoke-virtual {p0, v2}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c(I)Z

    move-result v1

    goto :goto_1

    .line 356
    :cond_5
    invoke-virtual {p0, v2}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b(I)Z

    move-result v1

    goto :goto_1

    .line 360
    :sswitch_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v3

    if-eqz v3, :cond_6

    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(I)Z

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_2

    .line 344
    :sswitch_data_0
    .sparse-switch
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0x3e -> :sswitch_2
    .end sparse-switch
.end method

.method public addView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 190
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "HorizontalScrollView can host only one direct child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 193
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 194
    return-void
.end method

.method public addView(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 199
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "HorizontalScrollView can host only one direct child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    .line 203
    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 217
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "HorizontalScrollView can host only one direct child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 220
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 221
    return-void
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 207
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 208
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "HorizontalScrollView can host only one direct child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 211
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 212
    return-void
.end method

.method protected b()I
    .locals 4

    .prologue
    .line 814
    const/4 v0, 0x0

    .line 815
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int/2addr v1, v2

    .line 814
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 816
    return v0
.end method

.method protected b()V
    .locals 1

    .prologue
    .line 748
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->isHardwareAccelerated()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 749
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 751
    :cond_0
    return-void
.end method

.method public final b(II)V
    .locals 2

    .prologue
    .line 1171
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollY()I

    move-result v1

    sub-int v1, p2, v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(II)V

    .line 1172
    return-void
.end method

.method public b(I)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 986
    const/16 v0, 0x42

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    .line 987
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getWidth()I

    move-result v2

    .line 989
    iget-object v3, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    iput v1, v3, Landroid/graphics/Rect;->left:I

    .line 990
    iget-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 992
    if-eqz v0, :cond_0

    .line 993
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c()I

    move-result v0

    .line 994
    if-lez v0, :cond_0

    .line 995
    iget-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 996
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1000
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(III)Z

    move-result v0

    return v0

    :cond_1
    move v0, v1

    .line 986
    goto :goto_0
.end method

.method public c(I)Z
    .locals 6

    .prologue
    const/16 v5, 0x42

    .line 1049
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 1050
    if-ne v0, p0, :cond_0

    const/4 v0, 0x0

    .line 1052
    :cond_0
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v1

    invoke-virtual {v1, p0, v0, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    .line 1054
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a()I

    move-result v1

    .line 1056
    if-eqz v2, :cond_2

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(Landroid/view/View;I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1057
    iget-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    invoke-virtual {v2, v1}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1058
    iget-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1059
    iget-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(Landroid/graphics/Rect;)I

    move-result v1

    .line 1060
    invoke-direct {p0, v1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b(I)V

    .line 1061
    invoke-virtual {v2, p1}, Landroid/view/View;->requestFocus(I)Z

    .line 1084
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1085
    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1091
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getDescendantFocusability()I

    move-result v0

    .line 1092
    const/high16 v1, 0x20000

    invoke-virtual {p0, v1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->setDescendantFocusability(I)V

    .line 1093
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->requestFocus()Z

    .line 1094
    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->setDescendantFocusability(I)V

    .line 1096
    :cond_1
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 1066
    :cond_2
    const/16 v2, 0x11

    if-ne p1, v2, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v2

    if-ge v2, v1, :cond_4

    .line 1067
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v1

    .line 1078
    :cond_3
    :goto_2
    if-nez v1, :cond_5

    .line 1079
    const/4 v0, 0x0

    goto :goto_1

    .line 1068
    :cond_4
    if-ne p1, v5, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getChildCount()I

    move-result v2

    if-lez v2, :cond_3

    .line 1070
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c()I

    move-result v2

    .line 1072
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    .line 1074
    sub-int v4, v2, v3

    if-ge v4, v1, :cond_3

    .line 1075
    sub-int v1, v2, v3

    goto :goto_2

    .line 1081
    :cond_5
    if-ne p1, v5, :cond_6

    :goto_3
    invoke-direct {p0, v1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b(I)V

    goto :goto_0

    :cond_6
    neg-int v1, v1

    goto :goto_3
.end method

.method protected computeHorizontalScrollOffset()I
    .locals 2

    .prologue
    .line 1200
    const/4 v0, 0x0

    invoke-super {p0}, Landroid/widget/FrameLayout;->computeHorizontalScrollOffset()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method protected computeHorizontalScrollRange()I
    .locals 4

    .prologue
    .line 1180
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getChildCount()I

    move-result v0

    .line 1181
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 1182
    if-nez v0, :cond_1

    move v0, v1

    .line 1195
    :cond_0
    :goto_0
    return v0

    .line 1186
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c()I

    move-result v0

    .line 1187
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v2

    .line 1188
    const/4 v3, 0x0

    sub-int v1, v0, v1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1189
    if-gez v2, :cond_2

    .line 1190
    sub-int/2addr v0, v2

    goto :goto_0

    .line 1191
    :cond_2
    if-le v2, v1, :cond_0

    .line 1192
    sub-int v1, v2, v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 12

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 1234
    iget-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:LavA;

    invoke-interface {v1}, LavA;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1251
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v3

    .line 1252
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollY()I

    move-result v4

    .line 1253
    iget-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:LavA;

    invoke-interface {v1}, LavA;->a()I

    move-result v11

    .line 1254
    iget-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:LavA;

    invoke-interface {v1}, LavA;->b()I

    move-result v2

    .line 1256
    if-ne v3, v11, :cond_0

    if-eq v4, v2, :cond_2

    .line 1257
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b()I

    move-result v5

    .line 1258
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getOverScrollMode()I

    move-result v1

    .line 1259
    if-eqz v1, :cond_1

    if-ne v1, v0, :cond_4

    if-lez v5, :cond_4

    :cond_1
    move v10, v0

    .line 1262
    :goto_0
    sub-int v1, v11, v3

    sub-int/2addr v2, v4

    iget v7, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:I

    move-object v0, p0

    move v8, v6

    move v9, v6

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->overScrollBy(IIIIIIIIZ)Z

    .line 1264
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollY()I

    move-result v1

    invoke-virtual {p0, v0, v1, v3, v4}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->onScrollChanged(IIII)V

    .line 1266
    if-eqz v10, :cond_2

    .line 1267
    if-gez v11, :cond_5

    if-ltz v3, :cond_5

    .line 1268
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/widget/EdgeEffect;

    iget-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:LavA;

    invoke-interface {v1}, LavA;->b()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    .line 1275
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->awakenScrollBars()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1276
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->postInvalidateOnAnimation()V

    .line 1279
    :cond_3
    return-void

    :cond_4
    move v10, v6

    .line 1259
    goto :goto_0

    .line 1269
    :cond_5
    if-le v11, v5, :cond_2

    if-gt v3, v5, :cond_2

    .line 1270
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Landroid/widget/EdgeEffect;

    iget-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:LavA;

    invoke-interface {v1}, LavA;->b()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    goto :goto_1
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 316
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 1561
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1562
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/widget/EdgeEffect;

    if-eqz v0, :cond_3

    .line 1563
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v0

    .line 1564
    iget-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1565
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 1566
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    .line 1568
    const/high16 v3, 0x43870000    # 270.0f

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->rotate(F)V

    .line 1569
    neg-int v3, v2

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingTop()I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    const/4 v4, 0x0

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1570
    iget-object v3, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/widget/EdgeEffect;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getWidth()I

    move-result v4

    invoke-virtual {v3, v2, v4}, Landroid/widget/EdgeEffect;->setSize(II)V

    .line 1571
    iget-object v2, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/widget/EdgeEffect;

    invoke-virtual {v2, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1572
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->postInvalidateOnAnimation()V

    .line 1574
    :cond_0
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1576
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1577
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 1578
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getWidth()I

    move-result v2

    .line 1579
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingTop()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    .line 1581
    const/high16 v4, 0x42b40000    # 90.0f

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->rotate(F)V

    .line 1582
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingTop()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    .line 1583
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b()I

    move-result v5

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, v2

    neg-int v0, v0

    int-to-float v0, v0

    .line 1582
    invoke-virtual {p1, v4, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1584
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Landroid/widget/EdgeEffect;

    invoke-virtual {v0, v3, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    .line 1585
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Landroid/widget/EdgeEffect;

    invoke-virtual {v0, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1586
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->postInvalidateOnAnimation()V

    .line 1588
    :cond_2
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1591
    :cond_3
    return-void
.end method

.method protected getLeftFadingEdgeStrength()F
    .locals 2

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 134
    const/4 v0, 0x0

    .line 142
    :goto_0
    return v0

    .line 137
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getHorizontalFadingEdgeLength()I

    move-result v0

    .line 138
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v1

    if-ge v1, v0, :cond_1

    .line 139
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v1

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    goto :goto_0

    .line 142
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method protected getRightFadingEdgeStrength()F
    .locals 4

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 148
    const/4 v0, 0x0

    .line 158
    :goto_0
    return v0

    .line 151
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getHorizontalFadingEdgeLength()I

    move-result v0

    .line 152
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 153
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int v1, v2, v1

    .line 154
    if-ge v1, v0, :cond_1

    .line 155
    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    goto :goto_0

    .line 158
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method protected measureChild(Landroid/view/View;II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1205
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1210
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingTop()I

    move-result v1

    .line 1211
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1210
    invoke-static {p3, v1, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getChildMeasureSpec(III)I

    move-result v0

    .line 1213
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1215
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    .line 1216
    return-void
.end method

.method protected measureChildWithMargins(Landroid/view/View;IIII)V
    .locals 3

    .prologue
    .line 1221
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1224
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v1, v2

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v1, v2

    add-int/2addr v1, p5

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 1223
    invoke-static {p4, v1, v2}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getChildMeasureSpec(III)I

    move-result v1

    .line 1226
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v2

    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1229
    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    .line 1230
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xc
    .end annotation

    .prologue
    .line 680
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 681
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 709
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 683
    :pswitch_0
    iget-boolean v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Z

    if-nez v0, :cond_0

    .line 685
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 686
    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    neg-float v0, v0

    .line 690
    :goto_1
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    .line 691
    const v1, 0x3fb33333    # 1.4f

    mul-float/2addr v0, v1

    float-to-int v1, v0

    .line 692
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b()I

    move-result v0

    .line 693
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v2

    .line 694
    add-int/2addr v1, v2

    .line 695
    if-gez v1, :cond_3

    .line 696
    const/4 v0, 0x0

    .line 700
    :cond_1
    :goto_2
    if-eq v0, v2, :cond_0

    .line 701
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollY()I

    move-result v1

    invoke-super {p0, v0, v1}, Landroid/widget/FrameLayout;->scrollTo(II)V

    .line 702
    const/4 v0, 0x1

    goto :goto_0

    .line 688
    :cond_2
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    goto :goto_1

    .line 697
    :cond_3
    if-gt v1, v0, :cond_1

    move v0, v1

    goto :goto_2

    .line 681
    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 804
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 805
    const-class v0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 806
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setScrollable(Z)V

    .line 807
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setScrollX(I)V

    .line 808
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollY()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setScrollY(I)V

    .line 809
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setMaxScrollX(I)V

    .line 810
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollY()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setMaxScrollY(I)V

    .line 811
    return-void

    .line 806
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 788
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 789
    const-class v0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 790
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b()I

    move-result v0

    .line 791
    if-lez v0, :cond_1

    .line 792
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setScrollable(Z)V

    .line 793
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v1

    if-lez v1, :cond_0

    .line 794
    const/16 v1, 0x2000

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 796
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v1

    if-ge v1, v0, :cond_1

    .line 797
    const/16 v0, 0x1000

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 800
    :cond_1
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v4, -0x1

    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 423
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 424
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Z

    if-eqz v2, :cond_0

    .line 516
    :goto_0
    return v0

    .line 428
    :cond_0
    and-int/lit16 v1, v1, 0xff

    packed-switch v1, :pswitch_data_0

    .line 516
    :cond_1
    :goto_1
    :pswitch_0
    iget-boolean v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Z

    goto :goto_0

    .line 439
    :pswitch_1
    iget v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->g:I

    .line 440
    if-eq v1, v4, :cond_1

    .line 445
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v2

    .line 446
    if-ne v2, v4, :cond_2

    .line 447
    const-string v0, "HorizontalScrollView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid pointerId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in onInterceptTouchEvent"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 452
    :cond_2
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    float-to-int v1, v1

    .line 453
    iget v2, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:I

    sub-int v2, v1, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 454
    iget v3, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c:I

    if-le v2, v3, :cond_1

    .line 455
    iput-boolean v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Z

    .line 456
    iput v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:I

    .line 457
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->d()V

    .line 458
    iget-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/view/VelocityTracker;

    invoke-virtual {v1, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 459
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_1

    .line 465
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    .line 466
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(II)Z

    move-result v2

    if-nez v2, :cond_3

    .line 467
    iput-boolean v3, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Z

    .line 468
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->e()V

    goto :goto_1

    .line 476
    :cond_3
    iput v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:I

    .line 477
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->g:I

    .line 479
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c()V

    .line 480
    iget-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/view/VelocityTracker;

    invoke-virtual {v1, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 487
    iget-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:LavA;

    invoke-interface {v1}, LavA;->a()Z

    move-result v1

    if-nez v1, :cond_4

    move v3, v0

    :cond_4
    iput-boolean v3, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Z

    goto/16 :goto_1

    .line 494
    :pswitch_3
    iput-boolean v3, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Z

    .line 495
    iput v4, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->g:I

    .line 496
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:LavA;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollY()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b()I

    move-result v4

    move v5, v3

    move v6, v3

    invoke-interface/range {v0 .. v6}, LavA;->a(IIIIII)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 497
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->postInvalidateOnAnimation()V

    goto/16 :goto_1

    .line 501
    :pswitch_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    .line 502
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:I

    .line 503
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->g:I

    goto/16 :goto_1

    .line 507
    :pswitch_5
    invoke-direct {p0, p1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(Landroid/view/MotionEvent;)V

    .line 508
    iget v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->g:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:I

    goto/16 :goto_1

    .line 428
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 1451
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 1452
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Z

    .line 1454
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/view/View;

    invoke-static {v0, p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(Landroid/view/View;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1455
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(Landroid/view/View;)V

    .line 1457
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/view/View;

    .line 1460
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->scrollTo(II)V

    .line 1461
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5

    .prologue
    .line 285
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 287
    iget-boolean v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c:Z

    if-nez v0, :cond_1

    .line 311
    :cond_0
    :goto_0
    return-void

    .line 291
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 292
    if-eqz v0, :cond_0

    .line 296
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 297
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 298
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getMeasuredWidth()I

    move-result v2

    .line 299
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    if-ge v0, v2, :cond_0

    .line 300
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 302
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingTop()I

    move-result v3

    .line 303
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingBottom()I

    move-result v4

    add-int/2addr v3, v4

    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 302
    invoke-static {p2, v3, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getChildMeasureSpec(III)I

    move-result v0

    .line 304
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    .line 305
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    .line 306
    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 308
    invoke-virtual {v1, v2, v0}, Landroid/view/View;->measure(II)V

    goto :goto_0
.end method

.method protected onOverScrolled(IIZZ)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 721
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:LavA;

    invoke-interface {v0}, LavA;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 722
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c(II)V

    .line 723
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b()V

    .line 724
    if-eqz p3, :cond_0

    .line 725
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:LavA;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollY()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b()I

    move-result v4

    move v5, v3

    move v6, v3

    invoke-interface/range {v0 .. v6}, LavA;->a(IIIIII)Z

    .line 731
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->awakenScrollBars()Z

    .line 732
    return-void

    .line 728
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->scrollTo(II)V

    goto :goto_0
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1411
    const/4 v1, 0x2

    if-ne p1, v1, :cond_2

    .line 1412
    const/16 p1, 0x42

    .line 1417
    :cond_0
    :goto_0
    if-nez p2, :cond_3

    .line 1418
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 1422
    :goto_1
    if-nez v1, :cond_4

    .line 1430
    :cond_1
    :goto_2
    return v0

    .line 1413
    :cond_2
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 1414
    const/16 p1, 0x11

    goto :goto_0

    .line 1419
    :cond_3
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v1

    invoke-virtual {v1, p0, p2, p1}, Landroid/view/FocusFinder;->findNextFocusFromRect(Landroid/view/ViewGroup;Landroid/graphics/Rect;I)Landroid/view/View;

    move-result-object v1

    goto :goto_1

    .line 1426
    :cond_4
    invoke-direct {p0, v1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1430
    invoke-virtual {v1, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    goto :goto_2
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    .prologue
    .line 1465
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 1467
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 1468
    if-eqz v0, :cond_0

    if-ne p0, v0, :cond_1

    .line 1479
    :cond_0
    :goto_0
    return-void

    .line 1471
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getWidth()I

    move-result v1

    .line 1473
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(Landroid/view/View;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1474
    iget-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1475
    iget-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1476
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(Landroid/graphics/Rect;)I

    move-result v0

    .line 1477
    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b(I)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13

    .prologue
    .line 521
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->d()V

    .line 522
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 524
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 526
    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 657
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 528
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 529
    const/4 v0, 0x0

    goto :goto_1

    .line 531
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:LavA;

    invoke-interface {v0}, LavA;->a()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Z

    .line 532
    iget-boolean v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Z

    if-eqz v0, :cond_2

    .line 533
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 534
    if-eqz v0, :cond_2

    .line 535
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 543
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:LavA;

    invoke-interface {v0}, LavA;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 544
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:LavA;

    invoke-interface {v0}, LavA;->a()V

    .line 548
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:I

    .line 549
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->g:I

    goto :goto_0

    .line 531
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 553
    :pswitch_2
    iget v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->g:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    .line 554
    const/4 v1, -0x1

    if-ne v0, v1, :cond_5

    .line 555
    const-string v0, "HorizontalScrollView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid pointerId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in onTouchEvent"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 559
    :cond_5
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    float-to-int v0, v0

    .line 560
    iget v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:I

    sub-int/2addr v1, v0

    .line 561
    iget-boolean v2, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Z

    if-nez v2, :cond_7

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c:I

    if-le v2, v3, :cond_7

    .line 562
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .line 563
    if-eqz v2, :cond_6

    .line 564
    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 566
    :cond_6
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Z

    .line 567
    if-lez v1, :cond_c

    .line 568
    iget v2, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c:I

    sub-int/2addr v1, v2

    .line 573
    :cond_7
    :goto_3
    iget-boolean v2, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Z

    if-eqz v2, :cond_0

    .line 575
    iput v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:I

    .line 577
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v11

    .line 578
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollY()I

    move-result v12

    .line 579
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b()I

    move-result v5

    .line 580
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getOverScrollMode()I

    move-result v0

    .line 581
    if-eqz v0, :cond_8

    const/4 v2, 0x1

    if-ne v0, v2, :cond_d

    if-lez v5, :cond_d

    :cond_8
    const/4 v0, 0x1

    move v10, v0

    .line 584
    :goto_4
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v3

    const/4 v4, 0x0

    const/4 v6, 0x0

    iget v7, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->f:I

    const/4 v8, 0x0

    const/4 v9, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->overScrollBy(IIIIIIIIZ)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 587
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 589
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollY()I

    move-result v2

    invoke-virtual {p0, v0, v2, v11, v12}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->onScrollChanged(IIII)V

    .line 591
    if-eqz v10, :cond_0

    .line 592
    add-int v0, v11, v1

    .line 593
    if-gez v0, :cond_e

    .line 594
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/widget/EdgeEffect;

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/EdgeEffect;->onPull(F)V

    .line 595
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v0

    if-nez v0, :cond_a

    .line 596
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 604
    :cond_a
    :goto_5
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/widget/EdgeEffect;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/widget/EdgeEffect;

    .line 605
    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 606
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->postInvalidateOnAnimation()V

    goto/16 :goto_0

    .line 570
    :cond_c
    iget v2, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c:I

    add-int/2addr v1, v2

    goto :goto_3

    .line 581
    :cond_d
    const/4 v0, 0x0

    move v10, v0

    goto :goto_4

    .line 598
    :cond_e
    if-le v0, v5, :cond_a

    .line 599
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Landroid/widget/EdgeEffect;

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/EdgeEffect;->onPull(F)V

    .line 600
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v0

    if-nez v0, :cond_a

    .line 601
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    goto :goto_5

    .line 612
    :pswitch_3
    iget-boolean v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Z

    if-eqz v0, :cond_0

    .line 613
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/view/VelocityTracker;

    .line 614
    const/16 v1, 0x3e8

    iget v2, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->e:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 615
    iget v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->g:I

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v0

    float-to-int v0, v0

    .line 617
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getChildCount()I

    move-result v1

    if-lez v1, :cond_f

    .line 618
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->d:I

    if-le v1, v2, :cond_10

    .line 619
    neg-int v0, v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(I)V

    .line 628
    :cond_f
    :goto_6
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->g:I

    .line 629
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Z

    .line 630
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->e()V

    .line 632
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/widget/EdgeEffect;

    if-eqz v0, :cond_0

    .line 633
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 634
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    goto/16 :goto_0

    .line 621
    :cond_10
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:LavA;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollY()I

    move-result v2

    const/4 v3, 0x0

    .line 622
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b()I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 621
    invoke-interface/range {v0 .. v6}, LavA;->a(IIIIII)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 623
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->postInvalidateOnAnimation()V

    goto :goto_6

    .line 639
    :pswitch_4
    iget-boolean v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 640
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:LavA;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollY()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b()I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, LavA;->a(IIIIII)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 641
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->postInvalidateOnAnimation()V

    .line 643
    :cond_11
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->g:I

    .line 644
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Z

    .line 645
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->e()V

    .line 647
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/widget/EdgeEffect;

    if-eqz v0, :cond_0

    .line 648
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 649
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    goto/16 :goto_0

    .line 654
    :pswitch_5
    invoke-direct {p0, p1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 526
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 755
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 782
    :goto_0
    return v0

    .line 758
    :cond_0
    sparse-switch p1, :sswitch_data_0

    move v0, v1

    .line 782
    goto :goto_0

    .line 760
    :sswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    .line 761
    goto :goto_0

    .line 763
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    .line 764
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 765
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v3

    if-eq v2, v3, :cond_2

    .line 766
    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b(II)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 769
    goto :goto_0

    .line 771
    :sswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 772
    goto :goto_0

    .line 774
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    .line 775
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v3

    sub-int v2, v3, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 776
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v3

    if-eq v2, v3, :cond_4

    .line 777
    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b(II)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 780
    goto :goto_0

    .line 758
    nop

    :sswitch_data_0
    .sparse-switch
        0x1000 -> :sswitch_0
        0x2000 -> :sswitch_1
    .end sparse-switch
.end method

.method public postInvalidateOnAnimation()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1606
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 1607
    invoke-static {p0}, Lec;->a(Landroid/view/View;)V

    .line 1611
    :goto_0
    return-void

    .line 1609
    :cond_0
    invoke-super {p0}, Landroid/widget/FrameLayout;->postInvalidateOnAnimation()V

    goto :goto_0
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1388
    iget-boolean v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Z

    if-nez v0, :cond_0

    .line 1389
    invoke-direct {p0, p2}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(Landroid/view/View;)V

    .line 1394
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 1395
    return-void

    .line 1392
    :cond_0
    iput-object p2, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/view/View;

    goto :goto_0
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 3

    .prologue
    .line 1437
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1438
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v2

    sub-int/2addr v1, v2

    .line 1437
    invoke-virtual {p2, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 1440
    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(Landroid/graphics/Rect;Z)Z

    move-result v0

    return v0
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 0

    .prologue
    .line 404
    if-eqz p1, :cond_0

    .line 405
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->e()V

    .line 407
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->requestDisallowInterceptTouchEvent(Z)V

    .line 408
    return-void
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 1445
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Z

    .line 1446
    invoke-super {p0}, Landroid/widget/FrameLayout;->requestLayout()V

    .line 1447
    return-void
.end method

.method public scrollTo(II)V
    .locals 4

    .prologue
    .line 1534
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 1535
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1536
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c()I

    move-result v2

    invoke-static {p1, v1, v2}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(III)I

    move-result v1

    .line 1537
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-static {p2, v2, v0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(III)I

    move-result v0

    .line 1538
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollX()I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getScrollY()I

    move-result v2

    if-eq v0, v2, :cond_1

    .line 1539
    :cond_0
    invoke-super {p0, v1, v0}, Landroid/widget/FrameLayout;->scrollTo(II)V

    .line 1542
    :cond_1
    return-void
.end method

.method public setFillViewport(Z)V
    .locals 1

    .prologue
    .line 262
    iget-boolean v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c:Z

    if-eq p1, v0, :cond_0

    .line 263
    iput-boolean p1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->c:Z

    .line 264
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->requestLayout()V

    .line 266
    :cond_0
    return-void
.end method

.method public setOverScrollMode(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1546
    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    .line 1547
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/widget/EdgeEffect;

    if-nez v0, :cond_0

    .line 1548
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1549
    new-instance v1, Landroid/widget/EdgeEffect;

    invoke-direct {v1, v0}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/widget/EdgeEffect;

    .line 1550
    new-instance v1, Landroid/widget/EdgeEffect;

    invoke-direct {v1, v0}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Landroid/widget/EdgeEffect;

    .line 1556
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setOverScrollMode(I)V

    .line 1557
    return-void

    .line 1553
    :cond_1
    iput-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a:Landroid/widget/EdgeEffect;

    .line 1554
    iput-object v1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->b:Landroid/widget/EdgeEffect;

    goto :goto_0
.end method

.method public setSmoothScrollingEnabled(Z)V
    .locals 0

    .prologue
    .line 280
    iput-boolean p1, p0, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->d:Z

    .line 281
    return-void
.end method

.method public shouldDelayChildPressedState()Z
    .locals 1

    .prologue
    .line 714
    const/4 v0, 0x1

    return v0
.end method
