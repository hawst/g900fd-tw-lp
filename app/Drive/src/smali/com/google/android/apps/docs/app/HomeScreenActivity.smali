.class public Lcom/google/android/apps/docs/app/HomeScreenActivity;
.super Lrm;
.source "HomeScreenActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public a:LCP;

.field public a:LSF;

.field private a:LaFM;

.field public a:LaGM;

.field public a:LajQ;

.field public a:Lald;

.field public a:Lamn;

.field private a:Landroid/widget/Button;

.field public a:LpW;

.field public a:LsC;

.field public a:LtK;

.field public a:LvL;

.field public b:LaGM;

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Lrm;-><init>()V

    .line 110
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->i:Z

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/HomeScreenActivity;)LaFM;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LaFM;

    return-object v0
.end method

.method private a(Landroid/content/Intent;)LaFO;
    .locals 1

    .prologue
    .line 335
    const-string v0, "accountName"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    return-object v0
.end method

.method private a(LaFO;)V
    .locals 1

    .prologue
    .line 388
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->i:Z

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(LaFO;Z)V

    .line 389
    return-void
.end method

.method private a(LaFO;Z)V
    .locals 1

    .prologue
    .line 392
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 394
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LSF;

    invoke-static {v0, p1}, LajS;->a(LSF;LaFO;)Landroid/accounts/Account;

    move-result-object v0

    .line 396
    if-eqz v0, :cond_0

    .line 397
    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(Landroid/accounts/Account;Z)V

    .line 399
    :cond_0
    return-void

    .line 392
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/accounts/Account;Z)V
    .locals 1

    .prologue
    .line 426
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 427
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->b(LaFO;Z)V

    .line 428
    return-void
.end method

.method private a(Landroid/widget/ListView;)V
    .locals 6

    .prologue
    .line 195
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 197
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()[Lua;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 199
    new-instance v0, LtW;

    sget v3, Lxe;->navigation_menu_item:I

    sget v4, Lxc;->navigation_name:I

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, LtW;-><init>(Lcom/google/android/apps/docs/app/HomeScreenActivity;Landroid/content/Context;IILjava/util/ArrayList;)V

    .line 211
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lua;

    .line 212
    iget v1, v1, Lua;->a:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 215
    :cond_0
    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 216
    new-instance v0, LtX;

    invoke-direct {v0, p0, v5}, LtX;-><init>(Lcom/google/android/apps/docs/app/HomeScreenActivity;Ljava/util/ArrayList;)V

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 224
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/HomeScreenActivity;LaFO;Z)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(LaFO;Z)V

    return-void
.end method

.method private a()[Lua;
    .locals 7

    .prologue
    .line 237
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LsC;

    invoke-interface {v0}, LsC;->a()LbmF;

    move-result-object v0

    .line 239
    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvr;

    .line 241
    new-instance v3, Lua;

    invoke-virtual {v0}, Lvr;->a()LCl;

    move-result-object v4

    invoke-virtual {v0}, Lvr;->b()I

    move-result v5

    .line 242
    invoke-virtual {v0}, Lvr;->a()I

    move-result v6

    .line 243
    invoke-virtual {v0}, Lvr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v5, v6, v0}, Lua;-><init>(LCl;IILjava/lang/String;)V

    .line 241
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 246
    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Lua;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lua;

    return-object v0
.end method

.method private b(LaFO;Z)V
    .locals 6

    .prologue
    .line 431
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 432
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LaGM;

    invoke-interface {v0, p1}, LaGM;->a(LaFO;)LaFM;

    move-result-object v1

    .line 434
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LaFM;

    if-eqz v0, :cond_1

    invoke-virtual {v1}, LaFM;->a()J

    move-result-wide v2

    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LaFM;

    invoke-virtual {v0}, LaFM;->a()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 435
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()LaqY;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Landroid/widget/Button;

    invoke-interface {v2, v3, p1}, LaqY;->a(Landroid/widget/Button;LaFO;)V

    .line 436
    iput-object v1, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LaFM;

    .line 438
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->o()V

    .line 440
    if-nez v0, :cond_0

    if-nez p2, :cond_0

    .line 441
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LSF;

    invoke-interface {v0, v1}, LSF;->a(LaFM;)V

    .line 443
    :cond_0
    return-void

    .line 434
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 480
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()V
    .locals 2

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 175
    sget v1, Lxc;->home_screen_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 177
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(Landroid/widget/ListView;)V

    .line 178
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 373
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LaFM;

    if-eqz v0, :cond_0

    .line 383
    :goto_0
    return-void

    .line 377
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LSF;

    invoke-static {v0}, LSH;->a(LSF;)Landroid/accounts/Account;

    move-result-object v0

    .line 378
    if-nez v0, :cond_1

    .line 379
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->m()V

    goto :goto_0

    .line 381
    :cond_1
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(Landroid/accounts/Account;Z)V

    goto :goto_0
.end method

.method private m()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 402
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LqK;

    const-string v1, "/addAccount"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 403
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Lald;

    sget v1, Lxi;->google_account_needed:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Lald;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 404
    new-instance v0, LtY;

    invoke-direct {v0, p0}, LtY;-><init>(Lcom/google/android/apps/docs/app/HomeScreenActivity;)V

    .line 422
    iget-object v1, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LSF;

    const-string v2, "com.google"

    invoke-interface {v1, v2, p0, v0}, LSF;->a(Ljava/lang/String;Landroid/app/Activity;LSG;)V

    .line 423
    return-void
.end method

.method private n()V
    .locals 2

    .prologue
    .line 474
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->b()LaFO;

    move-result-object v0

    .line 475
    iget-object v1, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LSF;

    invoke-interface {v1, v0}, LSF;->c(LaFO;)V

    .line 476
    return-void
.end method

.method private o()V
    .locals 4

    .prologue
    .line 497
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LSF;

    invoke-interface {v0}, LSF;->a()[Landroid/accounts/Account;

    move-result-object v0

    .line 499
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()LaqY;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Landroid/widget/Button;

    new-instance v3, LtZ;

    invoke-direct {v3, p0}, LtZ;-><init>(Lcom/google/android/apps/docs/app/HomeScreenActivity;)V

    invoke-interface {v1, v2, v0, v3}, LaqY;->a(Landroid/widget/Button;[Landroid/accounts/Account;LaqX;)V

    .line 510
    return-void
.end method


# virtual methods
.method public a()LaFO;
    .locals 1

    .prologue
    .line 538
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LaFM;

    if-eqz v0, :cond_0

    .line 539
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LaFM;

    invoke-virtual {v0}, LaFM;->a()LaFO;

    move-result-object v0

    .line 543
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(Landroid/content/Intent;)LaFO;

    move-result-object v0

    goto :goto_0
.end method

.method protected a()LarB;
    .locals 3

    .prologue
    .line 519
    new-instance v0, Lsd;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LsI;

    iget-object v2, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LqK;

    invoke-direct {v0, p0, v1, v2}, Lsd;-><init>(Landroid/app/Activity;LsI;LqK;)V

    return-object v0
.end method

.method public a(LCl;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 446
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LqK;

    const-string v1, "homeScreen"

    invoke-virtual {v0, v1, p2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 449
    new-instance v1, LvN;

    invoke-direct {v1}, LvN;-><init>()V

    .line 450
    iget-object v2, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LaFM;

    invoke-virtual {v2}, LaFM;->a()LaFO;

    move-result-object v2

    .line 451
    iget-object v3, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LvL;

    invoke-interface {v3, v2}, LvL;->a(LaFO;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v3

    invoke-virtual {v1, v3}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LvL;

    .line 452
    invoke-interface {v4, p1}, LvL;->b(LCl;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v4

    .line 451
    invoke-virtual {v3, v4}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    .line 453
    invoke-virtual {v1}, LvN;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v1

    .line 454
    new-instance v3, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    invoke-direct {v3, v1}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;-><init>(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 456
    invoke-static {p0, v0, v2}, Lcom/google/android/apps/docs/app/DocListActivity;->a(Landroid/content/Context;Ljava/util/List;LaFO;)V

    .line 457
    return-void
.end method

.method protected a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 339
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(Landroid/content/Intent;)LaFO;

    move-result-object v0

    .line 340
    if-eqz v0, :cond_0

    .line 341
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(LaFO;)V

    .line 342
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->o()V

    .line 344
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 533
    const/4 v0, 0x0

    return v0
.end method

.method protected a_()V
    .locals 1

    .prologue
    .line 228
    sget v0, Lxc;->account_switcher:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Landroid/widget/Button;

    .line 229
    return-void
.end method

.method b()LaFO;
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LaFM;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LaFM;

    invoke-virtual {v0}, LaFM;->a()LaFO;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Z)V
    .locals 3

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->l()V

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->b:LaGM;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LtK;

    .line 187
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()LaFO;

    move-result-object v2

    .line 186
    invoke-static {v0, p0, v1, v2}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(LaGM;Lrm;LtK;LaFO;)V

    .line 188
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()LaFO;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LpW;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(LaFO;LpW;)V

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LaFM;

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Latd;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->d()Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LaFM;

    invoke-virtual {v2}, LaFM;->a()LaFO;

    move-result-object v2

    invoke-virtual {v0, p0, v1, v2}, Latd;->a(Landroid/app/Activity;ZLaFO;)V

    .line 192
    :cond_0
    return-void
.end method

.method protected f()V
    .locals 3

    .prologue
    .line 347
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 348
    const-class v1, Lcom/google/android/apps/docs/app/AccountsActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 349
    const-string v1, "accountName"

    iget-object v2, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LaFM;

    invoke-virtual {v2}, LaFM;->a()LaFO;

    move-result-object v2

    invoke-virtual {v2}, LaFO;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 350
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 351
    return-void
.end method

.method protected j()V
    .locals 6

    .prologue
    .line 484
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LaFM;

    if-nez v0, :cond_0

    .line 485
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LSF;

    invoke-interface {v0}, LSF;->a()LaFO;

    move-result-object v0

    .line 486
    if-eqz v0, :cond_0

    .line 488
    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(LaFO;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 494
    :cond_0
    :goto_0
    return-void

    .line 489
    :catch_0
    move-exception v1

    .line 490
    const-string v2, "HomeScreenActivity"

    const-string v3, "Could not set account for [%s]. Ignoring this user."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v2, v1, v3, v4}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 355
    invoke-super {p0, p1, p2, p3}, Lrm;->onActivityResult(IILandroid/content/Intent;)V

    .line 356
    if-nez p1, :cond_0

    .line 357
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 358
    const-string v0, "accountName"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    .line 359
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()LaFO;

    move-result-object v1

    invoke-virtual {v0, v1}, LaFO;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 360
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->finish()V

    .line 361
    iget-object v1, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LajQ;

    const-string v2, "homeScreen"

    invoke-interface {v1, p0, v0, v2}, LajQ;->a(Landroid/content/Context;LaFO;Ljava/lang/String;)V

    .line 365
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 307
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lxc;->account_switcher:I

    if-ne v0, v1, :cond_0

    .line 308
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->f()V

    .line 311
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 144
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 146
    sget v0, Lxe;->home_screen_activity:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->setContentView(I)V

    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->f()Z

    move-result v1

    if-nez v1, :cond_0

    .line 150
    iget-object v1, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LqK;

    const-string v2, "/homeScreen"

    invoke-virtual {v1, v2, v0}, LqK;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LaGM;

    invoke-interface {v0}, LaGM;->e()V

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 156
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()LaqY;

    move-result-object v0

    invoke-interface {v0, v3, v3}, LaqY;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->j()V

    .line 160
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()LM;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a(LM;)V

    .line 162
    if-nez p1, :cond_1

    .line 163
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(Landroid/content/Intent;)V

    .line 168
    :cond_1
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    .line 293
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 294
    sget v1, Lxf;->menu_action_bar:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 295
    sget v1, Lxf;->menu_home_page_a:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 296
    sget v1, Lxf;->menu_home_page_b:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 300
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()LaqY;

    move-result-object v0

    sget v1, Lxc;->menu_search:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LaqY;->a(Landroid/view/MenuItem;Lara;)V

    .line 302
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 251
    invoke-super {p0}, Lrm;->onDestroy()V

    .line 252
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 256
    invoke-super {p0, p1}, Lrm;->onNewIntent(Landroid/content/Intent;)V

    .line 258
    const-string v0, "accountName"

    .line 259
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    .line 260
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a()LaFO;

    move-result-object v1

    invoke-virtual {v0, v1}, LaFO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 261
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->setIntent(Landroid/content/Intent;)V

    .line 262
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(Landroid/content/Intent;)V

    .line 268
    :goto_0
    return-void

    .line 265
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->startActivity(Landroid/content/Intent;)V

    .line 266
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->finish()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 315
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lxc;->menu_switch_account:I

    if-ne v1, v2, :cond_0

    .line 316
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->f()V

    .line 330
    :goto_0
    return v0

    .line 318
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lxc;->menu_send_feedback:I

    if-ne v1, v2, :cond_1

    .line 319
    new-instance v1, Lali;

    const-string v2, "android_docs"

    invoke-direct {v1, p0, v2}, Lali;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    .line 321
    invoke-virtual {v1}, Lali;->a()V

    goto :goto_0

    .line 323
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lxc;->menu_settings:I

    if-ne v1, v2, :cond_2

    .line 324
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 326
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lxc;->menu_help:I

    if-ne v1, v2, :cond_3

    .line 327
    iget-object v1, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Latd;

    invoke-virtual {v1, p0}, Latd;->a(Landroid/content/Context;)Z

    goto :goto_0

    .line 330
    :cond_3
    invoke-super {p0, p1}, Lrm;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 284
    const-string v0, "HomeScreenActivity"

    const-string v1, "in onPause"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->n()V

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LqK;

    const-string v1, "/homeScreen"

    invoke-virtual {v0, p0, v1}, LqK;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Latd;

    invoke-virtual {v0}, Latd;->b()V

    .line 288
    invoke-super {p0}, Lrm;->onPause()V

    .line 289
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 138
    invoke-super {p0, p1}, Lrm;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 139
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->i:Z

    .line 140
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 272
    const-string v0, "HomeScreenActivity"

    const-string v1, "in onResume"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    invoke-super {p0}, Lrm;->onResume()V

    .line 274
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->l()V

    .line 275
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->o()V

    .line 277
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->k()V

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LqK;

    invoke-virtual {v0, p0}, LqK;->a(Ljava/lang/Object;)V

    .line 280
    return-void
.end method

.method public onSearchRequested()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 461
    iget-object v0, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LCP;

    invoke-interface {v0}, LCP;->a()V

    .line 462
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LaFM;

    .line 463
    invoke-virtual {v1}, LaFM;->a()LaFO;

    move-result-object v1

    invoke-static {v1}, LzT;->a(LaFO;)Landroid/os/Bundle;

    move-result-object v1

    .line 462
    invoke-virtual {p0, v0, v2, v1, v2}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V

    .line 464
    const/4 v0, 0x1

    return v0
.end method
