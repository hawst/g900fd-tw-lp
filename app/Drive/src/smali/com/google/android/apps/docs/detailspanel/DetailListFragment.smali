.class public Lcom/google/android/apps/docs/detailspanel/DetailListFragment;
.super Lcom/google/android/apps/docs/fragment/DetailFragment;
.source "DetailListFragment.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public a:Lamt;

.field private a:Landroid/widget/ListView;

.field public a:LyV;

.field public a:LyW;

.field public a:LyY;

.field public a:Lza;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/DetailFragment;-><init>()V

    .line 76
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/detailspanel/DetailListFragment;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/DetailListFragment;->a:Landroid/widget/ListView;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 123
    const-string v0, "DetailFragment"

    const-string v1, "onCreateView"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    sget v0, Lxe;->detail_listview:I

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 125
    sget v0, Lxc;->detail_fragment_listview:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/docs/detailspanel/DetailListFragment;->a:Landroid/widget/ListView;

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/DetailListFragment;->a:Lamt;

    invoke-virtual {v0}, Lamt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/DetailListFragment;->a:LyY;

    sget v2, Lxc;->detail_fragment_header:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, LyY;->a(Landroid/view/View;)V

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/DetailListFragment;->a:LyW;

    iget-object v2, p0, Lcom/google/android/apps/docs/detailspanel/DetailListFragment;->a:LyY;

    invoke-virtual {v0, v2}, LyW;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/DetailListFragment;->a:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/docs/detailspanel/DetailListFragment;->a:Lza;

    invoke-interface {v2}, Lza;->a()Laqs;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/DetailListFragment;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/DetailListFragment;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/DetailListFragment;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/DetailListFragment;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setClickable(Z)V

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/DetailListFragment;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 139
    if-eqz p3, :cond_1

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/DetailListFragment;->a:LyV;

    invoke-static {v0, p3}, LyV;->a(LyV;Landroid/os/Bundle;)V

    .line 142
    const-string v0, "DetailListFragment_listPos"

    invoke-virtual {p3, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 145
    iget-object v2, p0, Lcom/google/android/apps/docs/detailspanel/DetailListFragment;->a:Landroid/widget/ListView;

    new-instance v3, LyU;

    invoke-direct {v3, p0, v0}, LyU;-><init>(Lcom/google/android/apps/docs/detailspanel/DetailListFragment;I)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    .line 153
    :cond_1
    return-object v1
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 174
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/DetailListFragment;->a:LyV;

    invoke-static {v0, p1}, LyV;->b(LyV;Landroid/os/Bundle;)V

    .line 159
    const-string v0, "DetailListFragment_listPos"

    iget-object v1, p0, Lcom/google/android/apps/docs/detailspanel/DetailListFragment;->a:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 160
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/fragment/DetailFragment;->c(Landroid/os/Bundle;)V

    .line 161
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/DetailListFragment;->a:Lza;

    invoke-interface {v0}, Lza;->a()V

    .line 200
    invoke-super {p0}, Lcom/google/android/apps/docs/fragment/DetailFragment;->h()V

    .line 201
    return-void
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 194
    const/4 v0, 0x0

    return v0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/DetailListFragment;->a:LyW;

    invoke-static {v0, p1, p2, p3, p4}, LyW;->a(LyW;Landroid/widget/AbsListView;III)V

    .line 190
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/apps/docs/detailspanel/DetailListFragment;->a:LyW;

    invoke-static {v0, p1, p2}, LyW;->a(LyW;Landroid/widget/AbsListView;I)V

    .line 184
    return-void
.end method
