.class public Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;
.super Lrm;
.source "PhoneskyApplicationInstallerActivity.java"


# instance fields
.field private a:I

.field public a:LZY;

.field private a:LaFO;

.field public a:LaKR;

.field private a:Landroid/app/ProgressDialog;

.field private a:Landroid/content/Intent;

.field private a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field public a:LtK;

.field private a:LuS;

.field private a:LuT;

.field private a:[Ljava/lang/String;

.field private b:Landroid/app/ProgressDialog;

.field private final b:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lrm;-><init>()V

    .line 187
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->b:Landroid/os/Handler;

    return-void
.end method

.method private a(LaFO;Ljava/lang/String;)Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 463
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.vending.billing.PURCHASE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 464
    const-string v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 465
    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 466
    const-string v1, "authAccount"

    invoke-virtual {p1}, LaFO;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 467
    const-string v1, "backend"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 468
    const-string v1, "document_type"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 469
    const-string v1, "full_docid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 470
    const-string v1, "backend_docid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 471
    const-string v1, "offer_type"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 473
    invoke-static {p0, v0}, LalN;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 475
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 476
    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v2, "https://play.google.com/store/apps/details?id=%1$s&rdid=%1$s&rdot=%2$d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    .line 479
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    .line 476
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 480
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 481
    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 482
    const-string v1, "use_direct_purchase"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 484
    :cond_0
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 59
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 64
    const-class v1, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 65
    const-string v1, "installPackages"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    const-string v1, "forwardingIntent"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 67
    const-string v1, "entrySpec.v2"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 69
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->b:Landroid/os/Handler;

    return-object v0
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LuS;

    sget-object v1, LuS;->a:LuS;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:[Ljava/lang/String;

    iget v1, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:I

    aget-object v0, v0, v1

    return-object v0

    .line 209
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(I)V
    .locals 0

    .prologue
    .line 290
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->setResult(I)V

    .line 291
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->finish()V

    .line 292
    return-void
.end method

.method private a(ILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 295
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->setResult(ILandroid/content/Intent;)V

    .line 296
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->finish()V

    .line 297
    return-void
.end method

.method private a(ILjava/lang/String;)V
    .locals 4

    .prologue
    .line 353
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LaKR;

    invoke-interface {v0}, LaKR;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 354
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->f()V

    .line 371
    :goto_0
    return-void

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LZY;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0, p2, v1}, LZY;->a(Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 360
    sget-object v0, LuS;->a:LuS;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LuS;

    .line 361
    iput p1, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:I

    .line 364
    invoke-static {p0}, LEL;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    const-string v1, ""

    sget v2, Lxi;->phonesky_connecting_to_play_store_message:I

    .line 365
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    .line 363
    invoke-static {v0, v1, v2, v3}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->b:Landroid/app/ProgressDialog;

    .line 368
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LaFO;

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a(LaFO;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 370
    iget-object v1, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LuS;

    invoke-virtual {v1}, LuS;->a()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;I)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a(I)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 488
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 490
    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 493
    :goto_0
    return v0

    .line 492
    :catch_0
    move-exception v0

    .line 493
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()LaFO;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 499
    invoke-static {p0}, LajS;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v2

    .line 500
    array-length v0, v2

    if-nez v0, :cond_0

    .line 501
    const/4 v0, 0x0

    .line 512
    :goto_0
    return-object v0

    .line 504
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LtK;

    sget-object v3, Lry;->ad:Lry;

    invoke-interface {v0, v3}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 505
    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 506
    iget-object v5, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v5}, LajS;->a(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 507
    iget-object v0, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    goto :goto_0

    .line 505
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 512
    :cond_2
    aget-object v0, v2, v1

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    goto :goto_0
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 310
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_1

    .line 311
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:[Ljava/lang/String;

    aget-object v0, v0, p1

    .line 312
    invoke-static {p0, v0}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 313
    add-int/lit8 p1, p1, 0x1

    .line 314
    goto :goto_0

    .line 318
    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a(ILjava/lang/String;)V

    .line 333
    :goto_1
    return-void

    .line 323
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LtK;

    sget-object v1, Lry;->ae:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:Landroid/content/Intent;

    if-eqz v0, :cond_2

    .line 325
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->j()V

    goto :goto_1

    .line 329
    :cond_2
    sget v0, Lxi;->app_installation_in_progress_toast:I

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 330
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 331
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a(I)V

    goto :goto_1
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 424
    iget-object v1, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 425
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 426
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->f()Z

    .line 424
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 429
    :cond_1
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 336
    invoke-static {p0}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v0

    const v1, 0x1080027

    .line 337
    invoke-virtual {v0, v1}, LEU;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lxi;->phonesky_no_internet_connection_alert_dialog_title:I

    .line 338
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lxi;->phonesky_no_internet_connection_alert_dialog_message:I

    .line 339
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lxi;->phonesky_no_internet_connection_alert_dialog_dismiss:I

    const/4 v2, 0x0

    .line 340
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 342
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 343
    new-instance v1, LuQ;

    invoke-direct {v1, p0}, LuQ;-><init>(Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 349
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 350
    return-void
.end method

.method private f()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 432
    iget-object v2, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 433
    invoke-static {p0, v4}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 441
    :goto_1
    return v0

    .line 432
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 439
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->l()V

    .line 440
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->m()V

    .line 441
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private j()V
    .locals 1

    .prologue
    .line 378
    sget-object v0, LuS;->b:LuS;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LuS;

    .line 384
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->k()V

    .line 385
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->f()Z

    .line 386
    return-void
.end method

.method private k()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 389
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 390
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LuT;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 394
    new-instance v0, LuT;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, LuT;-><init>(Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;LuQ;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LuT;

    .line 395
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LuT;

    iget-object v2, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LuT;

    invoke-virtual {v2}, LuT;->a()Landroid/content/IntentFilter;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 398
    invoke-static {p0}, LEL;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    const-string v2, ""

    sget v3, Lxi;->app_installation_in_progress:I

    .line 399
    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 397
    invoke-static {v0, v2, v3, v1}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:Landroid/app/ProgressDialog;

    .line 401
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 402
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:Landroid/app/ProgressDialog;

    new-instance v1, LuR;

    invoke-direct {v1, p0}, LuR;-><init>(Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 410
    :cond_0
    return-void

    .line 390
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 413
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LuT;

    if-eqz v0, :cond_0

    .line 414
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 415
    iput-object v1, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:Landroid/app/ProgressDialog;

    .line 417
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LuT;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 420
    :cond_0
    iput-object v1, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LuT;

    .line 421
    return-void
.end method

.method private m()V
    .locals 2

    .prologue
    .line 449
    sget-object v0, LuS;->c:LuS;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LuS;

    .line 451
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 452
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LuS;

    invoke-virtual {v1}, LuS;->a()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 457
    :goto_0
    return-void

    .line 454
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->setResult(I)V

    .line 455
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->finish()V

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 263
    sget-object v0, LuS;->c:LuS;

    invoke-virtual {v0}, LuS;->a()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 264
    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a(ILandroid/content/Intent;)V

    .line 287
    :goto_0
    return-void

    .line 268
    :cond_0
    sget-object v0, LuS;->a:LuS;

    invoke-virtual {v0}, LuS;->a()I

    move-result v0

    if-ne p1, v0, :cond_4

    .line 269
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 270
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 274
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->b:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_2

    .line 275
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 277
    :cond_2
    iget v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->b(I)V

    goto :goto_0

    .line 281
    :cond_3
    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a(ILandroid/content/Intent;)V

    goto :goto_0

    .line 285
    :cond_4
    const-string v0, "ApplicationInstallerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected request code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a(I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 215
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 217
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 218
    const-string v0, "installPackages"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:[Ljava/lang/String;

    .line 219
    const-string v0, "forwardingIntent"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:Landroid/content/Intent;

    .line 220
    const-string v0, "accountName"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LaFO;

    .line 221
    const-string v0, "entrySpec.v2"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LaFO;

    if-nez v0, :cond_0

    .line 223
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->b()LaFO;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LaFO;

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LaFO;

    if-nez v0, :cond_0

    .line 225
    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a(I)V

    .line 238
    :goto_0
    return-void

    .line 230
    :cond_0
    if-eqz p1, :cond_1

    .line 231
    const-string v0, "currentStage"

    .line 232
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LuS;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LuS;

    .line 233
    const-string v0, "pendingPackageIndex"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:I

    goto :goto_0

    .line 237
    :cond_1
    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->b(I)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 257
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->l()V

    .line 258
    invoke-super {p0}, Lrm;->onPause()V

    .line 259
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 249
    invoke-super {p0}, Lrm;->onResume()V

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LuS;

    sget-object v1, LuS;->b:LuS;

    if-ne v0, v1, :cond_0

    .line 251
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->j()V

    .line 253
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 242
    invoke-super {p0, p1}, Lrm;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 243
    const-string v0, "currentStage"

    iget-object v1, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:LuS;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 244
    const-string v0, "pendingPackageIndex"

    iget v1, p0, Lcom/google/android/apps/docs/app/PhoneskyApplicationInstallerActivity;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 245
    return-void
.end method
