.class public Lcom/google/android/apps/docs/receivers/TransferNotificationActionReceiver;
.super Lajq;
.source "TransferNotificationActionReceiver.java"


# instance fields
.field public a:LahK;

.field public a:Lamn;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lajq;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 34
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 35
    const-string v1, "com.google.android.apps.docs.receivers.UploadActionsReceiver.FORCE_RESUME_UPLOADS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 36
    iget-object v0, p0, Lcom/google/android/apps/docs/receivers/TransferNotificationActionReceiver;->a:LahK;

    sget-object v1, LahS;->b:LahS;

    invoke-interface {v0, v1}, LahK;->a(LahS;)V

    .line 42
    :goto_0
    return-void

    .line 37
    :cond_0
    const-string v1, "com.google.android.apps.docs.receivers.UploadActionsReceiver.FORCE_RESUME_DOWNLOADS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 38
    iget-object v0, p0, Lcom/google/android/apps/docs/receivers/TransferNotificationActionReceiver;->a:LahK;

    sget-object v1, LahS;->a:LahS;

    invoke-interface {v0, v1}, LahK;->a(LahS;)V

    goto :goto_0

    .line 40
    :cond_1
    const-string v1, "UploadActionsReceiver"

    const-string v2, "Invalid action: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method
