.class public Lcom/google/android/apps/docs/app/GetContentActivity;
.super LqS;
.source "GetContentActivity.java"

# interfaces
.implements LFT;


# instance fields
.field public a:LGi;

.field public a:LaGM;

.field public a:LaIm;

.field public a:Ladf;

.field public a:Ladu;

.field public a:Laer;

.field public a:Ljava/util/concurrent/Executor;

.field private final b:Landroid/os/Handler;

.field private final b:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 55
    invoke-direct {p0}, LqS;-><init>()V

    .line 82
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/GetContentActivity;->b:Landroid/os/Handler;

    .line 83
    new-instance v0, LalI;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/GetContentActivity;->b:Landroid/os/Handler;

    invoke-direct {v0, v1}, LalI;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/GetContentActivity;->b:Ljava/util/concurrent/Executor;

    return-void
.end method

.method private a(LDL;LaGo;)V
    .locals 1

    .prologue
    .line 128
    if-nez p1, :cond_0

    .line 132
    :goto_0
    return-void

    .line 131
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/GetContentActivity;->a()LM;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a(LM;LDL;LaGo;)V

    goto :goto_0
.end method

.method private a(LaGo;)V
    .locals 3

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/GetContentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 106
    sget-object v1, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->c:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    invoke-static {v0, v1}, LFU;->a(Landroid/content/Intent;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V

    .line 107
    iget-object v1, p0, Lcom/google/android/apps/docs/app/GetContentActivity;->a:LGi;

    invoke-interface {p1}, LaGo;->a()LaGv;

    move-result-object v2

    invoke-virtual {v1, v2}, LGi;->a(Ljava/lang/Object;)LFR;

    move-result-object v1

    .line 109
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-interface {v1, p0, p1, v0}, LFR;->a(LFT;LaGo;Landroid/os/Bundle;)LbsU;

    move-result-object v0

    .line 110
    new-instance v1, LtR;

    invoke-direct {v1, p0, p1}, LtR;-><init>(Lcom/google/android/apps/docs/app/GetContentActivity;LaGo;)V

    .line 124
    iget-object v2, p0, Lcom/google/android/apps/docs/app/GetContentActivity;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LbsK;->a(LbsU;LbsJ;Ljava/util/concurrent/Executor;)V

    .line 125
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/GetContentActivity;LDL;LaGo;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/app/GetContentActivity;->a(LDL;LaGo;)V

    return-void
.end method


# virtual methods
.method protected a()Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;
    .locals 3

    .prologue
    .line 87
    const/16 v0, 0x9

    new-array v0, v0, [LaGv;

    const/4 v1, 0x0

    sget-object v2, LaGv;->b:LaGv;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LaGv;->f:LaGv;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LaGv;->g:LaGv;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LaGv;->i:LaGv;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LaGv;->c:LaGv;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LaGv;->j:LaGv;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LaGv;->h:LaGv;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LaGv;->d:LaGv;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LaGv;->k:LaGv;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->a([LaGv;)Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    move-result-object v0

    return-object v0
.end method

.method public a(LFV;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 201
    const-string v0, "GetContentActivity"

    const-string v1, "Error occured %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/docs/app/GetContentActivity;->b:Landroid/os/Handler;

    new-instance v1, LtU;

    invoke-direct {v1, p0, p1}, LtU;-><init>(Lcom/google/android/apps/docs/app/GetContentActivity;LFV;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 220
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/docs/app/GetContentActivity;->a(Landroid/content/Intent;Lcom/google/android/apps/docs/utils/ParcelableTask;)V

    .line 137
    return-void
.end method

.method public a(Landroid/content/Intent;Lcom/google/android/apps/docs/utils/ParcelableTask;)V
    .locals 6

    .prologue
    .line 151
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/docs/app/GetContentActivity;->a:LaGM;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/GetContentActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v0

    .line 153
    if-nez v0, :cond_0

    .line 154
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/GetContentActivity;->e()V

    .line 197
    :goto_0
    return-void

    .line 157
    :cond_0
    sget-object v1, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->c:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    invoke-interface {v0}, LaGo;->a()LaGv;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a(LaGv;)LacY;

    move-result-object v0

    .line 159
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/docs/app/GetContentActivity;->a:Ladu;

    iget-object v2, p0, Lcom/google/android/apps/docs/app/GetContentActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v1, v2, v0}, Ladu;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LacY;)Landroid/net/Uri;
    :try_end_0
    .catch Ladt; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ladc; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v4

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/docs/app/GetContentActivity;->a:LaIm;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/GetContentActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v1}, LaIm;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/net/Uri;

    move-result-object v5

    .line 185
    new-instance v0, LtT;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, LtT;-><init>(Lcom/google/android/apps/docs/app/GetContentActivity;Lcom/google/android/apps/docs/utils/ParcelableTask;Landroid/content/Intent;Landroid/net/Uri;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/GetContentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 160
    :catch_0
    move-exception v0

    .line 161
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/GetContentActivity;->e()V

    goto :goto_0

    .line 163
    :catch_1
    move-exception v0

    .line 164
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/GetContentActivity;->e()V

    goto :goto_0

    .line 166
    :catch_2
    move-exception v0

    .line 167
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/GetContentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 168
    new-instance v1, LtS;

    invoke-direct {v1, p0, v0}, LtS;-><init>(Lcom/google/android/apps/docs/app/GetContentActivity;Landroid/content/Context;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/app/GetContentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 175
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/GetContentActivity;->e()V

    goto :goto_0
.end method

.method protected a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/apps/docs/app/GetContentActivity;->a:LaGM;

    invoke-interface {v0, p1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v0

    .line 225
    if-nez v0, :cond_0

    .line 226
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/GetContentActivity;->e()V

    .line 230
    :goto_0
    return-void

    .line 228
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/GetContentActivity;->a(LaGo;)V

    goto :goto_0
.end method

.method protected final a(LuW;)V
    .locals 2

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/GetContentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "mimeTypes"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 143
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 144
    invoke-virtual {p1, v0}, LuW;->a([Ljava/lang/String;)LuW;

    move-result-object v0

    invoke-virtual {v0}, LuW;->b()LuW;

    .line 146
    :cond_0
    return-void
.end method
