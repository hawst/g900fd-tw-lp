.class public Lcom/google/android/apps/docs/app/PickEntryActivity;
.super Lrd;
.source "PickEntryActivity.java"

# interfaces
.implements LCr;


# instance fields
.field private a:LBR;

.field public a:LCU;

.field private a:Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;

.field public a:LvY;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lrd;-><init>()V

    .line 49
    return-void
.end method

.method public static a(Landroid/content/Context;LaFO;)LuW;
    .locals 2

    .prologue
    .line 155
    new-instance v0, LuW;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, LuW;-><init>(Landroid/content/Context;LaFO;LuV;)V

    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity;->a:Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity;->a:Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()Lcom/google/android/apps/docs/view/DocListView;

    move-result-object v0

    .line 215
    iget-object v1, p0, Lcom/google/android/apps/docs/app/PickEntryActivity;->a:LBR;

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 216
    new-instance v1, LBR;

    invoke-direct {v1, v0}, LBR;-><init>(Lcom/google/android/apps/docs/view/DocListView;)V

    iput-object v1, p0, Lcom/google/android/apps/docs/app/PickEntryActivity;->a:LBR;

    .line 219
    :cond_0
    return-void
.end method


# virtual methods
.method protected a()Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;
    .locals 2

    .prologue
    .line 189
    new-instance v1, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;-><init>()V

    .line 190
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 191
    if-nez v0, :cond_0

    .line 192
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 194
    :cond_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->e(Landroid/os/Bundle;)V

    .line 195
    return-object v1
.end method

.method public a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 223
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity;->f()V

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity;->a:LBR;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity;->a:LBR;

    invoke-virtual {v0, p1, p2}, LBR;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 226
    if-eqz v0, :cond_0

    .line 230
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lrd;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public a(LaGv;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity;->a:Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity;->a:Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a(LaGv;Ljava/lang/String;)Z

    move-result v0

    .line 209
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()V
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity;->a:LvY;

    invoke-interface {v0}, LvY;->a()V

    .line 201
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 171
    invoke-super {p0, p1}, Lrd;->onCreate(Landroid/os/Bundle;)V

    .line 174
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity;->a()LM;

    move-result-object v0

    const-string v1, "PickEntryActivity"

    invoke-virtual {v0, v1}, LM;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity;->a:Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity;->a:Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;

    if-nez v0, :cond_0

    .line 176
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity;->a()Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity;->a:Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity;->a:Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity;->a()LM;

    move-result-object v1

    const-string v2, "PickEntryActivity"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a(LM;Ljava/lang/String;)V

    .line 180
    :cond_0
    if-nez p1, :cond_1

    .line 182
    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/app/PickEntryActivity;->setResult(I)V

    .line 185
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity;->a:LCU;

    invoke-virtual {v0, v3}, LCU;->a(Z)V

    .line 186
    return-void
.end method
