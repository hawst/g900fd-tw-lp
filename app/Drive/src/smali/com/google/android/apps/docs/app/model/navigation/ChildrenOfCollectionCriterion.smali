.class public Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;
.super Ljava/lang/Object;
.source "ChildrenOfCollectionCriterion.java"

# interfaces
.implements Lcom/google/android/apps/docs/app/model/navigation/Criterion;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/google/android/gms/drive/database/data/EntrySpec;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    new-instance v0, LvK;

    invoke-direct {v0}, LvK;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    const-class v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 69
    return-void
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;LvK;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 32
    return-void
.end method


# virtual methods
.method public a(LaGM;)LaeZ;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {p1, v0}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFV;

    move-result-object v0

    .line 106
    if-nez v0, :cond_0

    .line 107
    sget-object v0, LaeZ;->d:LaeZ;

    .line 109
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, LaeZ;->a(LaFV;)LaeZ;

    move-result-object v0

    goto :goto_0
.end method

.method public a()Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    return-object v0
.end method

.method public a(LvR;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LvR",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {p1, v0}, LvR;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 116
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 41
    if-ne p1, p0, :cond_0

    .line 42
    const/4 v0, 0x1

    .line 47
    :goto_0
    return v0

    .line 43
    :cond_0
    instance-of v0, p1, Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;

    if-eqz v0, :cond_1

    .line 44
    check-cast p1, Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v1, p1, Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 47
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 53
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 99
    const-string v0, "ChildrenOfCollectionCriterion {entrySpec=%s}"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/ChildrenOfCollectionCriterion;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 64
    return-void
.end method
