.class public Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;
.super Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;
.source "LinearLayoutListViewSlidePickerFragment.java"


# static fields
.field static final a:LalK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LalK",
            "<",
            "Laql;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Landroid/widget/FrameLayout;

.field a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

.field private d:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 34
    const/4 v0, 0x1

    sget-object v1, Laql;->a:Laql;

    const/4 v2, 0x2

    sget-object v3, Laql;->b:Laql;

    .line 35
    invoke-static {v0, v1, v2, v3}, LalK;->a(ILjava/lang/Object;ILjava/lang/Object;)LalK;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:LalK;

    .line 34
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;-><init>()V

    return-void
.end method

.method private a()Laql;
    .locals 4

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    .line 119
    sget-object v0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:LalK;

    invoke-virtual {v0, v1}, LalK;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laql;

    .line 120
    if-nez v0, :cond_0

    .line 121
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Configuration has unknown orientation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :cond_0
    return-object v0
.end method

.method public static a()Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;-><init>()V

    return-object v0
.end method

.method private t()V
    .locals 2

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a()Laql;

    move-result-object v0

    .line 104
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->setOrientation(Laql;)V

    .line 105
    return-void
.end method

.method private u()V
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

    new-instance v1, LXj;

    invoke-direct {v1, p0}, LXj;-><init>(Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->setOnScrollListener(Laqk;)V

    .line 115
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, -0x1

    const/4 v2, 0x0

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->d:Landroid/view/View;

    if-nez v0, :cond_0

    .line 51
    sget v0, LpP;->punch_list_view_slide_picker:I

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->d:Landroid/view/View;

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->d:Landroid/view/View;

    sget v3, LpN;->list_view:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/LinearLayoutListView;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 56
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->u()V

    .line 58
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->t()V

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    if-nez v0, :cond_2

    :goto_1
    invoke-static {v1}, LbiT;->b(Z)V

    .line 61
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a()LH;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

    new-instance v1, LXi;

    invoke-direct {v1, p0}, LXi;-><init>(Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->setOnItemClickListener(Laqj;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    return-object v0

    :cond_1
    move v0, v2

    .line 53
    goto :goto_0

    :cond_2
    move v1, v2

    .line 60
    goto :goto_1
.end method

.method protected a(I)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a(I)V

    .line 99
    return-void
.end method

.method protected a(Landroid/widget/ListAdapter;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 87
    return-void
.end method

.method public c(I)V
    .locals 5

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a(I)Landroid/view/View;

    move-result-object v0

    .line 131
    if-nez v0, :cond_0

    .line 132
    const-string v0, "LinearLayoutListViewSlidePickerFragment"

    const-string v1, "Aborting onThumbnailAvailable. Could not find view for slideIndex=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 133
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 132
    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 138
    :goto_0
    return-void

    .line 137
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:LYb;

    invoke-virtual {v1, v0, p1}, LYb;->a(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    .line 81
    invoke-super {p0}, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->e()V

    .line 82
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 91
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 93
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->t()V

    .line 94
    return-void
.end method
