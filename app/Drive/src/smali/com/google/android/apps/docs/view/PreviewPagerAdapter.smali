.class public Lcom/google/android/apps/docs/view/PreviewPagerAdapter;
.super LY;
.source "PreviewPagerAdapter.java"


# instance fields
.field private final a:LaqG;

.field private a:Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;


# direct methods
.method public constructor <init>(LM;LaqG;)V
    .locals 1

    .prologue
    .line 340
    invoke-direct {p0, p1}, LY;-><init>(LM;)V

    .line 342
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaqG;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter;->a:LaqG;

    .line 343
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter;->a:LaqG;

    invoke-interface {v0}, LaqG;->a()I

    move-result v0

    return v0
.end method

.method public a(I)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 347
    new-instance v0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;-><init>()V

    .line 348
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 349
    const-string v2, "position"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 350
    const-string v2, "entrySpec.v2"

    iget-object v3, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter;->a:LaqG;

    .line 351
    invoke-interface {v3, p1}, LaqG;->a(I)LCv;

    move-result-object v3

    invoke-interface {v3}, LCv;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v3

    .line 350
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 352
    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->e(Landroid/os/Bundle;)V

    .line 354
    return-object v0
.end method

.method public b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 364
    invoke-super {p0, p1, p2, p3}, LY;->b(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 366
    const/4 v0, 0x0

    .line 367
    instance-of v1, p3, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;

    if-eqz v1, :cond_1

    .line 368
    check-cast p3, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;

    .line 371
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter;->a:Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter;->a:Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter;->a:Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->v()V

    .line 373
    invoke-static {p3}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->b(Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;)V

    .line 376
    :cond_0
    iput-object p3, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter;->a:Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;

    .line 377
    iget-object v0, p0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter;->a:Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter$PreviewPagerFragment;->t()V

    .line 378
    return-void

    :cond_1
    move-object p3, v0

    goto :goto_0
.end method
