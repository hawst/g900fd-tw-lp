.class public Lcom/google/android/apps/docs/app/BaseFragment;
.super Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;
.source "BaseFragment.java"


# instance fields
.field private a:Lrt;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;-><init>()V

    .line 26
    sget-object v0, Lrt;->a:Lrt;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/BaseFragment;->a:Lrt;

    return-void
.end method


# virtual methods
.method public a()Lrt;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/docs/app/BaseFragment;->a:Lrt;

    return-object v0
.end method

.method public a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->a(Landroid/app/Activity;)V

    .line 37
    sget-object v0, Lrt;->c:Lrt;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/BaseFragment;->a:Lrt;

    .line 38
    return-void
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 30
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->a_(Landroid/os/Bundle;)V

    .line 31
    sget-object v0, Lrt;->b:Lrt;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/BaseFragment;->a:Lrt;

    .line 32
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 75
    const-string v0, "BaseIsRestart"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 76
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->c(Landroid/os/Bundle;)V

    .line 77
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->h()V

    .line 69
    sget-object v0, Lrt;->f:Lrt;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/BaseFragment;->a:Lrt;

    .line 70
    return-void
.end method

.method public j_()V
    .locals 1

    .prologue
    .line 48
    invoke-super {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->j_()V

    .line 49
    sget-object v0, Lrt;->e:Lrt;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/BaseFragment;->a:Lrt;

    .line 50
    return-void
.end method

.method public k_()V
    .locals 1

    .prologue
    .line 42
    invoke-super {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->k_()V

    .line 43
    sget-object v0, Lrt;->d:Lrt;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/BaseFragment;->a:Lrt;

    .line 44
    return-void
.end method

.method public l()Z
    .locals 4

    .prologue
    .line 56
    sget-object v0, Lrt;->a:Lrt;

    sget-object v1, Lrt;->e:Lrt;

    sget-object v2, Lrt;->f:Lrt;

    sget-object v3, Lrt;->g:Lrt;

    invoke-static {v0, v1, v2, v3}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmY;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/BaseFragment;->a:Lrt;

    .line 57
    invoke-virtual {v0, v1}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public n_()V
    .locals 1

    .prologue
    .line 62
    invoke-super {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->n_()V

    .line 63
    sget-object v0, Lrt;->g:Lrt;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/BaseFragment;->a:Lrt;

    .line 64
    return-void
.end method
