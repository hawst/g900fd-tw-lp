.class public Lcom/google/android/apps/viewer/viewer/pdf/PageView;
.super Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;
.source "PageView.java"


# instance fields
.field public final a:I

.field private a:Landroid/graphics/drawable/Drawable;

.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/google/android/apps/viewer/client/Dimensions;LawO;LavU;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;-><init>(Landroid/content/Context;)V

    .line 27
    invoke-virtual {p0, p3, p5, p4}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->a(Lcom/google/android/apps/viewer/client/Dimensions;LavU;LawO;)V

    .line 28
    invoke-virtual {p0, p2}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->setId(I)V

    .line 29
    iput p2, p0, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->a:I

    .line 30
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->setFocusableInTouchMode(Z)V

    .line 31
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->setPageText(Ljava/lang/String;)V

    .line 32
    invoke-direct {p0, p3}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->a(Lcom/google/android/apps/viewer/client/Dimensions;)V

    .line 33
    return-void
.end method

.method private a(Lcom/google/android/apps/viewer/client/Dimensions;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 36
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p1, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    iget v2, p1, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 38
    const/4 v1, 0x1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 39
    new-instance v1, Lawl;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lawl;-><init>(Landroid/content/Context;)V

    .line 40
    invoke-virtual {v1, v4}, Lawl;->a(I)I

    move-result v2

    invoke-virtual {v1, v4}, Lawl;->a(I)I

    move-result v1

    invoke-virtual {v0, v3, v2, v3, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 41
    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 42
    return-void
.end method


# virtual methods
.method public b()Z
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->a()V

    .line 62
    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->setPageText(Ljava/lang/String;)V

    .line 63
    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->setOverlay(Landroid/graphics/drawable/Drawable;)V

    .line 64
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 72
    :cond_0
    return-void
.end method

.method public setOverlay(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->a:Landroid/graphics/drawable/Drawable;

    .line 57
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->invalidate()V

    .line 58
    return-void
.end method

.method public setPageText(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->a:Ljava/lang/String;

    .line 50
    if-eqz p1, :cond_0

    .line 52
    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 53
    return-void

    .line 51
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Laum;->desc_page:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/apps/viewer/viewer/pdf/PageView;->a:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method
