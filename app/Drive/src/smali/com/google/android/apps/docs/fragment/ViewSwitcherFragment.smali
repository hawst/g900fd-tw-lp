.class public abstract Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;
.super Lcom/google/android/apps/docs/app/BaseFragment;
.source "ViewSwitcherFragment.java"


# instance fields
.field private a:I

.field private a:[I

.field public a:[Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseFragment;-><init>()V

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:I

    return-void
.end method

.method public static a(Landroid/os/Bundle;[II)V
    .locals 1

    .prologue
    .line 30
    const-string v0, "listOfIdsForEachPanel"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 31
    const-string v0, "visiblePanel"

    invoke-virtual {p0, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 32
    return-void
.end method

.method private b()Landroid/view/View;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 105
    const-string v0, "ViewSwitcherFragment"

    const-string v1, "in applyState"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    const/4 v1, 0x0

    move v2, v3

    .line 107
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:[Landroid/view/View;

    array-length v0, v0

    if-ge v2, v0, :cond_1

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:[Landroid/view/View;

    aget-object v0, v0, v2

    .line 109
    iget v4, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:I

    if-ne v2, v4, :cond_0

    .line 111
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 107
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 113
    :cond_0
    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    move-object v0, v1

    goto :goto_1

    .line 117
    :cond_1
    return-object v1
.end method

.method private j(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 63
    const-string v0, "visiblePanel"

    invoke-static {p1, v0}, Lakt;->a(Landroid/os/Bundle;Ljava/lang/String;)I

    move-result v0

    .line 64
    const-string v1, "ViewSwitcherFragment"

    const-string v2, "in loadState panelIndex=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 65
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a(I)V

    .line 66
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 95
    const-string v0, "ViewSwitcherFragment"

    const-string v1, "in initialize"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:[I

    array-length v0, v0

    new-array v0, v0, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:[Landroid/view/View;

    .line 97
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:[Landroid/view/View;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 98
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a()LH;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, LH;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 99
    invoke-static {v1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:[Landroid/view/View;

    aput-object v1, v2, v0

    .line 97
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 102
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 82
    const-string v0, "ViewSwitcherFragment"

    const-string v3, "in setVisiblePanel %d"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 83
    if-ltz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:[I

    array-length v0, v0

    if-ge p1, v0, :cond_2

    :goto_1
    invoke-static {v1}, LbiT;->a(Z)V

    .line 85
    iput p1, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:I

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:[Landroid/view/View;

    if-eqz v0, :cond_0

    .line 87
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->b()Landroid/view/View;

    move-result-object v0

    .line 88
    if-eqz v0, :cond_0

    .line 89
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 92
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 83
    goto :goto_0

    :cond_2
    move v1, v2

    .line 84
    goto :goto_1
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 51
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseFragment;->a_(Landroid/os/Bundle;)V

    .line 53
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 54
    const-string v1, "listOfIdsForEachPanel"

    .line 55
    invoke-static {v0, v1}, Lakt;->a(Landroid/os/Bundle;Ljava/lang/String;)[I

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:[I

    .line 57
    iget v1, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 58
    if-eqz p1, :cond_1

    :goto_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->j(Landroid/os/Bundle;)V

    .line 60
    :cond_0
    return-void

    :cond_1
    move-object p1, v0

    .line 58
    goto :goto_0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseFragment;->c(Landroid/os/Bundle;)V

    .line 78
    const-string v0, "visiblePanel"

    iget v1, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 79
    return-void
.end method

.method public k_()V
    .locals 0

    .prologue
    .line 70
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseFragment;->k_()V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a()V

    .line 72
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->b()Landroid/view/View;

    .line 73
    return-void
.end method
