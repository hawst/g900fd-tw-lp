.class public abstract Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;
.super Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;
.source "AbstractDeleteOperationFragment.java"


# instance fields
.field public a:LUi;

.field private m:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/app/AlertDialog;)V
    .locals 2

    .prologue
    .line 55
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;->m:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 56
    return-void
.end method

.method protected final a(Landroid/app/Dialog;IIILjava/lang/String;)V
    .locals 6

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p5

    .line 30
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;->a(Landroid/app/Dialog;IILjava/lang/String;Ljava/lang/String;)V

    .line 31
    return-void
.end method

.method protected final a(Landroid/app/Dialog;IILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 35
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;->b()Landroid/view/View;

    move-result-object v1

    .line 36
    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v2}, Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;->a(Landroid/app/Dialog;ILjava/lang/String;)V

    .line 39
    sget v0, Lxc;->new_name:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 40
    sget v0, Lxc;->item_name:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 42
    invoke-virtual {p1, p2}, Landroid/app/Dialog;->setTitle(I)V

    .line 43
    sget v2, Lxc;->first_label:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    if-nez p5, :cond_0

    .line 45
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 50
    :goto_0
    iput p3, p0, Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;->m:I

    .line 51
    return-void

    .line 47
    :cond_0
    invoke-virtual {v0, p5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
