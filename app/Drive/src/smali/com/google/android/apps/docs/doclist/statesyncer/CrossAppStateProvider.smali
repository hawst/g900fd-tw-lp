.class public Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;
.super LalP;
.source "CrossAppStateProvider.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LalP",
        "<",
        "LMm;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lajw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajw",
            "<",
            "LaFr;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:[Ljava/lang/String;

.field public static final b:Lajw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajw",
            "<",
            "LaFr;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Lajw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajw",
            "<",
            "LaFr;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Lajw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajw",
            "<",
            "LbmF",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private a:LMm;

.field private a:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 75
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "currentVersion"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a:[Ljava/lang/String;

    .line 104
    new-instance v0, LMg;

    invoke-direct {v0}, LMg;-><init>()V

    .line 105
    invoke-static {v0}, Lajw;->a(Lbxw;)Lajw;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a:Lajw;

    .line 113
    new-instance v0, LMh;

    invoke-direct {v0}, LMh;-><init>()V

    .line 114
    invoke-static {v0}, Lajw;->a(Lbxw;)Lajw;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->b:Lajw;

    .line 122
    new-instance v0, LMi;

    invoke-direct {v0}, LMi;-><init>()V

    .line 123
    invoke-static {v0}, Lajw;->a(Lbxw;)Lajw;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->c:Lajw;

    .line 132
    new-instance v0, LMj;

    invoke-direct {v0}, LMj;-><init>()V

    .line 133
    invoke-static {v0}, Lajw;->a(Lbxw;)Lajw;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->d:Lajw;

    .line 132
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, LalP;-><init>()V

    .line 167
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a:LMm;

    return-void
.end method

.method private a(J)LaGp;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 405
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-gez v1, :cond_1

    .line 414
    :cond_0
    :goto_0
    return-object v0

    .line 409
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a:LMm;

    iget-object v1, v1, LMm;->a:LaGM;

    invoke-interface {v1, p1, p2}, LaGM;->a(J)LaGp;

    move-result-object v1

    .line 410
    if-eqz v1, :cond_2

    invoke-virtual {v1}, LaGp;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 411
    invoke-virtual {v1}, LaGp;->a()Ljava/lang/Long;

    move-result-object v1

    .line 412
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a:LMm;

    iget-object v0, v0, LMm;->a:LaGM;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, LaGM;->a(J)LaGp;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 414
    goto :goto_0
.end method

.method private a()Landroid/database/Cursor;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 288
    new-instance v0, Landroid/database/MatrixCursor;

    sget-object v1, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a:[Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 290
    new-array v1, v3, [Ljava/lang/Object;

    .line 291
    const/4 v2, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 293
    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 294
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->moveToPosition(I)Z

    .line 296
    return-object v0
.end method

.method private a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Landroid/database/Cursor;
    .locals 8

    .prologue
    .line 300
    new-instance v1, Landroid/database/MatrixCursor;

    sget-object v0, LMn;->a:[Ljava/lang/String;

    invoke-direct {v1, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 307
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->b(Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 313
    if-eqz v4, :cond_2

    .line 319
    const-wide/16 v2, 0x0

    .line 320
    :cond_0
    :goto_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 321
    sget-object v0, LaES;->q:LaES;

    .line 322
    invoke-virtual {v0}, LaES;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, v4}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    .line 323
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 329
    :cond_1
    const-wide/16 v6, 0x1

    add-long/2addr v2, v6

    .line 331
    sget-object v0, LaEL;->c:LaEL;

    invoke-virtual {v0}, LaEL;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, v4}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    .line 332
    if-nez v0, :cond_3

    const-wide/16 v6, -0x1

    .line 333
    :goto_1
    invoke-direct {p0, v6, v7}, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a(J)LaGp;

    move-result-object v5

    move-object v0, p0

    .line 334
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a(Landroid/database/MatrixCursor;JLandroid/database/Cursor;LaGp;)V

    goto :goto_0

    .line 308
    :catch_0
    move-exception v0

    .line 309
    const-string v1, "CrossAppStateProvider"

    const-string v2, "Database query exception"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 310
    const/4 v1, 0x0

    .line 340
    :cond_2
    :goto_2
    return-object v1

    .line 333
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    goto :goto_1

    .line 337
    :cond_4
    const/4 v0, -0x1

    invoke-virtual {v1, v0}, Landroid/database/MatrixCursor;->moveToPosition(I)Z

    goto :goto_2
.end method

.method private a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 457
    new-instance v0, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LaES;->z:LaES;

    .line 458
    invoke-virtual {v2}, LaES;->a()LaFr;

    move-result-object v2

    invoke-virtual {v2}, LaFr;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " != 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v7}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    new-instance v1, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LaES;->y:LaES;

    .line 463
    invoke-virtual {v3}, LaES;->a()LaFr;

    move-result-object v3

    invoke-virtual {v3}, LaFr;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " != 0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v7}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    new-instance v2, Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LaES;->x:LaES;

    .line 466
    invoke-virtual {v4}, LaES;->a()LaFr;

    move-result-object v4

    invoke-virtual {v4}, LaFr;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = 0"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v7}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    sget-object v3, LaFL;->b:LaFL;

    new-array v4, v6, [Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    aput-object v1, v4, v5

    invoke-virtual {v3, v0, v4}, LaFL;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;[Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 469
    sget-object v1, LaFL;->a:LaFL;

    new-array v3, v6, [Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    aput-object v2, v3, v5

    invoke-virtual {v1, v0, v3}, LaFL;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;[Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 471
    if-nez p1, :cond_0

    .line 474
    :goto_0
    return-object v0

    .line 471
    :cond_0
    sget-object v1, LaFL;->a:LaFL;

    new-array v2, v6, [Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    aput-object p1, v2, v5

    .line 472
    invoke-virtual {v1, v0, v2}, LaFL;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;[Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    goto :goto_0
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 450
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " INNER JOIN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, LaEe;->a()LaEe;

    move-result-object v1

    invoke-virtual {v1}, LaEe;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ON ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 451
    invoke-static {}, LaEe;->a()LaEe;

    move-result-object v1

    invoke-virtual {v1}, LaEe;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LaES;->E:LaES;

    .line 452
    invoke-virtual {v1}, LaES;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 152
    const-class v0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;

    invoke-static {p0, v0}, Lams;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    .line 154
    const-string v1, "CrossAppStateProvider"

    const-string v2, "Initialized StateProvider with authority: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 155
    return-object v0
.end method

.method private a(Landroid/database/MatrixCursor;JLandroid/database/Cursor;LaGp;)V
    .locals 20

    .prologue
    .line 355
    if-eqz p5, :cond_0

    invoke-virtual/range {p5 .. p5}, LaGp;->c()Ljava/lang/String;

    move-result-object v2

    move-object v6, v2

    .line 356
    :goto_0
    if-eqz p5, :cond_1

    invoke-virtual/range {p5 .. p5}, LaGp;->f()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 357
    :goto_1
    if-eqz v3, :cond_2

    .line 358
    invoke-virtual/range {p5 .. p5}, LaGp;->a()Ljava/util/Date;

    move-result-object v2

    .line 359
    :goto_2
    if-eqz v2, :cond_3

    .line 360
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 363
    :goto_3
    :try_start_0
    invoke-static/range {p4 .. p4}, LaER;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v7

    .line 364
    if-nez v7, :cond_4

    .line 365
    const-string v2, "CrossAppStateProvider"

    const-string v3, "localOnly entry in cursor"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 401
    :goto_4
    return-void

    .line 355
    :cond_0
    const/4 v2, 0x0

    move-object v6, v2

    goto :goto_0

    .line 356
    :cond_1
    const/4 v2, 0x0

    move-object v3, v2

    goto :goto_1

    .line 358
    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    .line 360
    :cond_3
    const/4 v2, 0x0

    goto :goto_3

    .line 368
    :cond_4
    sget-object v4, LaEf;->a:LaEf;

    .line 369
    invoke-virtual {v4}, LaEf;->a()LaFr;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    .line 368
    invoke-static {v4}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v8

    .line 370
    sget-object v4, LaES;->t:LaES;

    invoke-virtual {v4}, LaES;->a()LaFr;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v9

    .line 371
    sget-object v4, LaES;->r:LaES;

    .line 372
    invoke-virtual {v4}, LaES;->a()LaFr;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v10

    .line 373
    sget-object v4, LaEL;->e:LaEL;

    invoke-virtual {v4}, LaEL;->a()LaFr;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v11

    .line 375
    sget-object v4, LaES;->y:LaES;

    invoke-virtual {v4}, LaES;->a()LaFr;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, LaFr;->a(Landroid/database/Cursor;)Z

    move-result v4

    if-eqz v4, :cond_5

    const-wide/16 v4, 0x1

    .line 376
    :goto_5
    sget-object v12, LaES;->z:LaES;

    .line 377
    invoke-virtual {v12}, LaES;->a()LaFr;

    move-result-object v12

    move-object/from16 v0, p4

    invoke-virtual {v12, v0}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    .line 378
    sget-object v14, LaES;->A:LaES;

    .line 379
    invoke-virtual {v14}, LaES;->a()LaFr;

    move-result-object v14

    move-object/from16 v0, p4

    invoke-virtual {v14, v0}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v14

    .line 385
    new-instance v16, LMo;

    invoke-direct/range {v16 .. v16}, LMo;-><init>()V

    .line 386
    sget-object v17, LMp;->a:LMp;

    .line 387
    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    invoke-virtual/range {v16 .. v18}, LMo;->a(LMp;Ljava/lang/Object;)LMo;

    move-result-object v17

    sget-object v18, LMp;->b:LMp;

    .line 388
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v7}, LMo;->a(LMp;Ljava/lang/Object;)LMo;

    move-result-object v7

    sget-object v17, LMp;->c:LMp;

    .line 389
    invoke-virtual {v8}, LaFO;->a()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v17

    invoke-virtual {v7, v0, v8}, LMo;->a(LMp;Ljava/lang/Object;)LMo;

    move-result-object v7

    sget-object v8, LMp;->d:LMp;

    .line 390
    invoke-virtual {v7, v8, v9}, LMo;->a(LMp;Ljava/lang/Object;)LMo;

    move-result-object v7

    sget-object v8, LMp;->e:LMp;

    .line 391
    invoke-virtual {v7, v8, v10}, LMo;->a(LMp;Ljava/lang/Object;)LMo;

    move-result-object v7

    sget-object v8, LMp;->f:LMp;

    .line 392
    invoke-virtual {v7, v8, v11}, LMo;->a(LMp;Ljava/lang/Object;)LMo;

    move-result-object v7

    sget-object v8, LMp;->g:LMp;

    .line 393
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v7, v8, v4}, LMo;->a(LMp;Ljava/lang/Object;)LMo;

    move-result-object v4

    sget-object v5, LMp;->h:LMp;

    .line 394
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, LMo;->a(LMp;Ljava/lang/Object;)LMo;

    move-result-object v4

    sget-object v5, LMp;->i:LMp;

    .line 395
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, LMo;->a(LMp;Ljava/lang/Object;)LMo;

    move-result-object v4

    sget-object v5, LMp;->j:LMp;

    .line 396
    invoke-virtual {v4, v5, v6}, LMo;->a(LMp;Ljava/lang/Object;)LMo;

    move-result-object v4

    sget-object v5, LMp;->k:LMp;

    .line 397
    invoke-virtual {v4, v5, v3}, LMo;->a(LMp;Ljava/lang/Object;)LMo;

    move-result-object v3

    sget-object v4, LMp;->l:LMp;

    .line 398
    invoke-virtual {v3, v4, v2}, LMo;->a(LMp;Ljava/lang/Object;)LMo;

    .line 400
    invoke-virtual/range {v16 .. v16}, LMo;->a()[Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_4

    .line 375
    :cond_5
    const-wide/16 v4, 0x0

    goto/16 :goto_5

    .line 380
    :catch_0
    move-exception v2

    .line 381
    const-string v3, "CrossAppStateProvider"

    const-string v4, "StateSyncer cursor does not contain expected columns."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v2, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_4
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 272
    const-string v0, "CrossAppStateProvider"

    const-string v1, "Provider exception"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, p1, v1, v2}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 274
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CrossAppStateProvider "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 275
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a:LMm;

    iget-object v1, v1, LMm;->a:LqK;

    invoke-virtual {v1, v0, v3}, LqK;->a(Ljava/lang/String;Z)V

    .line 277
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, LMk;

    invoke-direct {v1, p0, p1}, LMk;-><init>(Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;Ljava/lang/Exception;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 284
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 285
    return-void
.end method

.method private b(Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 420
    const/16 v0, 0xa

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    sget-object v1, LaES;->p:LaES;

    .line 421
    invoke-virtual {v1}, LaES;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    const/4 v0, 0x1

    sget-object v1, LaES;->q:LaES;

    .line 422
    invoke-virtual {v1}, LaES;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    const/4 v0, 0x2

    sget-object v1, LaEf;->a:LaEf;

    .line 423
    invoke-virtual {v1}, LaEf;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    const/4 v0, 0x3

    sget-object v1, LaES;->t:LaES;

    .line 424
    invoke-virtual {v1}, LaES;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    const/4 v0, 0x4

    sget-object v1, LaES;->r:LaES;

    .line 425
    invoke-virtual {v1}, LaES;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    const/4 v0, 0x5

    sget-object v1, LaEL;->e:LaEL;

    .line 426
    invoke-virtual {v1}, LaEL;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    const/4 v0, 0x6

    sget-object v1, LaES;->y:LaES;

    .line 427
    invoke-virtual {v1}, LaES;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    const/4 v0, 0x7

    sget-object v1, LaES;->z:LaES;

    .line 428
    invoke-virtual {v1}, LaES;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    const/16 v0, 0x8

    sget-object v1, LaES;->A:LaES;

    .line 429
    invoke-virtual {v1}, LaES;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    const/16 v0, 0x9

    sget-object v1, LaEL;->c:LaEL;

    .line 430
    invoke-virtual {v1}, LaEL;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    .line 433
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EntryView"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 434
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v4

    .line 437
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a:LMm;

    iget-object v0, v0, LMm;->a:LaEz;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    .line 438
    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 437
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 444
    :goto_0
    return-object v0

    .line 439
    :catch_0
    move-exception v0

    .line 442
    invoke-direct {p0, v8}, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v4

    .line 444
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a:LMm;

    iget-object v0, v0, LMm;->a:LaEz;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    .line 445
    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v8

    move-object v6, v8

    move-object v7, v8

    .line 444
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected a(Lbuu;)LMm;
    .locals 2

    .prologue
    .line 484
    new-instance v0, LMm;

    invoke-direct {v0}, LMm;-><init>()V

    .line 485
    invoke-interface {p1, v0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 487
    iget-object v1, v0, LMm;->a:LaEz;

    invoke-virtual {v1}, LaEz;->d()Z

    .line 488
    return-object v0
.end method

.method protected bridge synthetic a(Lbuu;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a(Lbuu;)LMm;

    move-result-object v0

    return-object v0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 192
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 202
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 171
    .line 173
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 179
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v3, -0x1

    invoke-direct {v0, v3}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a:Landroid/content/UriMatcher;

    .line 180
    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a:Landroid/content/UriMatcher;

    invoke-static {v2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v4, LMl;->a:LMl;

    invoke-static {v4}, LMl;->a(LMl;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4, v1}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 182
    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a:Landroid/content/UriMatcher;

    invoke-static {v2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v4, LMl;->b:LMl;

    invoke-static {v4}, LMl;->a(LMl;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v3, v0, v4, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 184
    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a:Landroid/content/UriMatcher;

    invoke-static {v2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v2, LMl;->c:LMl;

    invoke-static {v2}, LMl;->a(LMl;)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x3

    invoke-virtual {v3, v0, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    move v0, v1

    .line 187
    :goto_0
    return v0

    .line 174
    :catch_0
    move-exception v1

    .line 175
    const-string v2, "CrossAppStateProvider"

    const-string v3, "Cannot create provider, DocumentStateProvider not found in manifest."

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 210
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a:Landroid/content/UriMatcher;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LMm;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a:LMm;

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a:LMm;

    iget-object v0, v0, LMm;->a:LtK;

    sget-object v1, Lry;->m:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v8

    .line 263
    :goto_0
    return-object v0

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a:LMm;

    iget-object v0, v0, LMm;->a:Lakk;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lakk;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 219
    const-string v0, "CrossAppStateProvider"

    const-string v1, "Caller package not authorized"

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a:LMm;

    iget-object v0, v0, LMm;->a:LqK;

    const-string v1, "crossAppStateSync"

    const-string v2, "crossAppSyncerAccessDenied"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v8

    .line 222
    goto :goto_0

    .line 225
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a:LMm;

    iget-object v0, v0, LMm;->a:LaEz;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 253
    const-string v0, "CrossAppStateProvider"

    const-string v1, "Unknown URI %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v8

    .line 254
    goto :goto_0

    .line 230
    :pswitch_0
    if-eqz p3, :cond_4

    .line 231
    if-nez p4, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 233
    :goto_1
    invoke-static {p3, v0}, Lcom/google/android/gms/drive/database/common/SqlWhereClause;->a(Ljava/lang/String;Ljava/util/Collection;)Lcom/google/android/gms/drive/database/common/SqlWhereClause;

    move-result-object v0

    .line 236
    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a(Lcom/google/android/gms/drive/database/common/SqlWhereClause;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 232
    :cond_2
    invoke-static {p4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_1

    .line 238
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, LaEc;->a()LaEc;

    move-result-object v1

    invoke-virtual {v1}, LaEc;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " INNER JOIN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 239
    invoke-static {}, LaEe;->a()LaEe;

    move-result-object v1

    invoke-virtual {v1}, LaEe;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ON ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 240
    invoke-static {}, LaEe;->a()LaEe;

    move-result-object v1

    invoke-virtual {v1}, LaEe;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LaEd;->a:LaEd;

    .line 241
    invoke-virtual {v1}, LaEd;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 243
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a:LMm;

    iget-object v0, v0, LMm;->a:LaEz;

    sget-object v2, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->d:Lajw;

    .line 244
    invoke-virtual {v2}, Lajw;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LbmF;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v2, v3}, LbmF;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p3

    move-object v4, p4

    .line 243
    invoke-virtual/range {v0 .. v7}, LaEz;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto/16 :goto_0

    .line 251
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a()Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto/16 :goto_0

    .line 256
    :catch_0
    move-exception v0

    .line 257
    const-string v1, "CrossAppStateProvider"

    const-string v2, "Database query exception"

    new-array v3, v9, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v8

    .line 258
    goto/16 :goto_0

    .line 259
    :catch_1
    move-exception v0

    .line 260
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a:LMm;

    iget-object v1, v1, LMm;->a:LtK;

    sget-object v2, Lry;->l:Lry;

    invoke-interface {v1, v2}, LtK;->a(LtJ;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 262
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateProvider;->a(Ljava/lang/Exception;)V

    move-object v0, v8

    .line 263
    goto/16 :goto_0

    .line 265
    :cond_3
    const-string v1, "CrossAppStateProvider"

    const-string v2, "Exception caught and rethrown"

    new-array v3, v9, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 266
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_4
    move-object v0, v8

    goto/16 :goto_2

    .line 227
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 479
    const/4 v0, 0x0

    return v0
.end method
