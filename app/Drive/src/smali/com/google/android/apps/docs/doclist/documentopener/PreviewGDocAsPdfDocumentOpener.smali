.class public Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;
.super Ljava/lang/Object;
.source "PreviewGDocAsPdfDocumentOpener.java"

# interfaces
.implements LFR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D::",
        "LFR;",
        "F::",
        "LFR;",
        ">",
        "Ljava/lang/Object;",
        "LFR;"
    }
.end annotation


# instance fields
.field private final a:LFR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TD;"
        }
    .end annotation
.end field

.field private final a:LaIm;

.field private final a:Landroid/content/Context;

.field private final a:LsP;

.field private final a:Lye;

.field private final b:LFR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TF;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LFR;LFR;Landroid/content/Context;Lye;LsP;LaIm;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;TF;",
            "Landroid/content/Context;",
            "Lye;",
            "LsP;",
            "LaIm;",
            ")V"
        }
    .end annotation

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;->a:LFR;

    .line 83
    iput-object p2, p0, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;->b:LFR;

    .line 84
    iput-object p3, p0, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;->a:Landroid/content/Context;

    .line 85
    iput-object p4, p0, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;->a:Lye;

    .line 86
    iput-object p5, p0, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;->a:LsP;

    .line 87
    iput-object p6, p0, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;->a:LaIm;

    .line 88
    return-void
.end method

.method public synthetic constructor <init>(LFR;LFR;Landroid/content/Context;Lye;LsP;LaIm;LGB;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;-><init>(LFR;LFR;Landroid/content/Context;Lye;LsP;LaIm;)V

    return-void
.end method

.method private a(LFT;LaGo;)LDL;
    .locals 4

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;->a:Landroid/content/Context;

    sget v1, Lxi;->cross_app_promo_view_only_button_text:I

    .line 164
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 166
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;->a:Lye;

    .line 167
    invoke-interface {p2}, LaGo;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v3

    .line 166
    invoke-interface {v1, v2, v3}, Lye;->a(Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v1

    .line 169
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;->a:Lye;

    invoke-interface {v2, v1, v0}, Lye;->a(Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 170
    new-instance v1, LGy;

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;->a:Landroid/content/Context;

    .line 171
    invoke-interface {p2}, LaGo;->c()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, p1, v3, v0}, LGy;-><init>(Landroid/content/Context;LGx;Ljava/lang/String;Landroid/content/Intent;)V

    .line 172
    return-object v1
.end method

.method private static a(LaGo;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 176
    invoke-interface {p0}, LaGo;->c()Ljava/lang/String;

    move-result-object v0

    .line 177
    const-string v1, "file:///data/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 178
    return-object v0
.end method

.method private a(LFT;LaGo;Landroid/os/Bundle;Z)LbsU;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LFT;",
            "LaGo;",
            "Landroid/os/Bundle;",
            "Z)",
            "LbsU",
            "<",
            "LDL;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 124
    invoke-static {p3}, LFU;->a(Landroid/os/Bundle;)Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    move-result-object v1

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;->a:LaIm;

    invoke-interface {p2}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-interface {v0, v2}, LaIm;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/net/Uri;

    move-result-object v0

    .line 126
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;->a:Landroid/content/Context;

    .line 127
    invoke-static {p2}, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;->a(LaGo;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "application/pdf"

    .line 126
    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 131
    const/high16 v3, 0x10000

    .line 132
    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 133
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 135
    const/4 v0, 0x0

    .line 159
    :goto_0
    return-object v0

    .line 138
    :cond_0
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 139
    iget-object v3, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    if-eqz p4, :cond_1

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;->a:LsP;

    invoke-virtual {v0, p2, v1, v5}, LsP;->a(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Z)Landroid/content/Intent;

    move-result-object v0

    .line 144
    const-string v3, "editMode"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 148
    const-string v3, "reopenForEditIntent"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 152
    :cond_1
    invoke-static {v2, v1}, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;->a(Landroid/content/Intent;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator$UriIntentBuilder;

    move-result-object v0

    .line 155
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1, p3}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 156
    const-string v2, "uriIntentBuilder"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;->a:LFR;

    invoke-interface {v0, p1, p2, v1}, LFR;->a(LFT;LaGo;Landroid/os/Bundle;)LbsU;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(LFT;LaGo;Landroid/os/Bundle;)LbsU;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LFT;",
            "LaGo;",
            "Landroid/os/Bundle;",
            ")",
            "LbsU",
            "<",
            "LDL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    invoke-static {p3}, LFU;->a(Landroid/os/Bundle;)Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    move-result-object v0

    .line 94
    sget-object v1, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;->b:LFR;

    invoke-interface {v0, p1, p2, p3}, LFR;->a(LFT;LaGo;Landroid/os/Bundle;)LbsU;

    move-result-object v0

    .line 117
    :cond_0
    :goto_0
    return-object v0

    .line 99
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;->a:Lye;

    .line 100
    invoke-interface {p2}, LaGo;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lye;->a(Ljava/lang/String;)Z

    move-result v0

    .line 101
    const-string v1, "editMode"

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 102
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 105
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;->a(LFT;LaGo;)LDL;

    move-result-object v0

    .line 106
    invoke-static {v0}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    goto :goto_0

    .line 112
    :cond_2
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;->a(LFT;LaGo;Landroid/os/Bundle;Z)LbsU;

    move-result-object v0

    .line 113
    if-nez v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/PreviewGDocAsPdfDocumentOpener;->b:LFR;

    invoke-interface {v0, p1, p2, p3}, LFR;->a(LFT;LaGo;Landroid/os/Bundle;)LbsU;

    move-result-object v0

    goto :goto_0
.end method
