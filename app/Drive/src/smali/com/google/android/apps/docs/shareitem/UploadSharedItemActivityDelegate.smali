.class public Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;
.super Lrm;
.source "UploadSharedItemActivityDelegate.java"


# instance fields
.field public a:LCo;

.field public a:LNc;

.field public a:LQr;

.field public a:LSF;

.field public a:LUx;

.field private a:LaFO;

.field public a:LaGM;

.field public a:LaGR;

.field public a:LaIa;

.field public a:LaaA;

.field private a:LaaG;

.field private a:LaaJ;

.field public a:LaaP;

.field public a:Ladu;

.field public a:LajO;

.field public a:LalY;

.field public a:Lald;

.field private a:Landroid/app/ProgressDialog;

.field private a:Landroid/content/ServiceConnection;

.field private a:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private a:Landroid/widget/Button;

.field private a:Landroid/widget/CheckBox;

.field private a:Landroid/widget/EditText;

.field private a:Landroid/widget/ImageView;

.field private a:Landroid/widget/LinearLayout;

.field private a:Landroid/widget/Spinner;

.field private a:Landroid/widget/TextView;

.field private a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "LMZ;",
            ">;"
        }
    .end annotation
.end field

.field public a:Lcom/google/android/apps/docs/utils/BitmapUtilities;

.field private a:Ljava/lang/String;

.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LaaC;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LaFO;",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;"
        }
    .end annotation
.end field

.field public a:LpW;

.field public a:LtK;

.field private b:Landroid/widget/TextView;

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 267
    invoke-direct {p0}, Lrm;-><init>()V

    .line 268
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)LaFO;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaFO;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;LaFO;)LaFO;
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaFO;

    return-object p1
.end method

.method private a()LaaG;
    .locals 5

    .prologue
    .line 524
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaaG;

    if-nez v0, :cond_0

    .line 525
    new-instance v0, LaaR;

    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaaJ;

    .line 526
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities;

    iget-object v4, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LalY;

    invoke-direct {v0, v1, v2, v3, v4}, LaaR;-><init>(LaaJ;Landroid/content/ContentResolver;Lcom/google/android/apps/docs/utils/BitmapUtilities;LalY;)V

    .line 527
    new-instance v1, LaaK;

    iget-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LQr;

    .line 528
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LaaK;-><init>(LQr;Landroid/content/ContentResolver;)V

    .line 530
    new-instance v2, LaaG;

    invoke-direct {v2}, LaaG;-><init>()V

    .line 531
    invoke-virtual {v2, v0}, LaaG;->a(LaaR;)LaaG;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaaP;

    .line 532
    invoke-virtual {v0, v2}, LaaG;->a(LaaP;)LaaG;

    move-result-object v0

    .line 533
    invoke-virtual {v0, v1}, LaaG;->a(LaaK;)LaaG;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaaG;

    .line 535
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaaG;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)Landroid/os/AsyncTask;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/os/AsyncTask;

    return-object v0
.end method

.method private a(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 2

    .prologue
    .line 338
    sget v0, Lxe;->upload_shared_item_activity:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 339
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Landroid/view/View;)V

    .line 340
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->k()V

    .line 341
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/Button;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)LbmF;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LbmF;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;LbmF;)LbmF;
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LbmF;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;ZZ)LbmF;
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(ZZ)LbmF;

    move-result-object v0

    return-object v0
.end method

.method private a(ZZ)LbmF;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)",
            "LbmF",
            "<",
            "LMZ;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 819
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaFO;

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    const-string v2, "Account name is not set for uploading."

    invoke-static {v0, v2}, LbiT;->b(ZLjava/lang/Object;)V

    .line 821
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v3

    .line 823
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaaC;

    .line 825
    iget-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v1, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/EditText;

    if-eqz v2, :cond_4

    .line 827
    iget-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 833
    :goto_1
    iget-object v5, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaFO;

    invoke-direct {p0, v5}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(LaFO;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v5

    .line 834
    iget-object v6, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LNc;

    invoke-virtual {v6}, LNc;->a()LNb;

    move-result-object v6

    .line 835
    invoke-virtual {v6, v2}, LNb;->a(Ljava/lang/String;)LNb;

    move-result-object v2

    iget-object v6, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaFO;

    .line 836
    invoke-virtual {v2, v6}, LNb;->a(LaFO;)LNb;

    move-result-object v2

    .line 837
    invoke-virtual {v2, p1}, LNb;->a(Z)LNb;

    move-result-object v2

    iget-boolean v6, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->k:Z

    .line 838
    invoke-virtual {v2, v6}, LNb;->b(Z)LNb;

    move-result-object v2

    .line 839
    invoke-virtual {v2, v5}, LNb;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LNb;

    move-result-object v2

    .line 841
    if-eqz p2, :cond_1

    .line 842
    invoke-virtual {v2}, LNb;->a()LNb;

    .line 845
    :cond_1
    invoke-interface {v0, v2}, LaaC;->a(LNb;)LMZ;

    move-result-object v0

    invoke-virtual {v3, v0}, LbmH;->a(Ljava/lang/Object;)LbmH;

    .line 847
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 851
    :cond_2
    invoke-virtual {v3}, LbmH;->a()LbmF;

    move-result-object v0

    return-object v0

    .line 819
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 830
    :cond_4
    invoke-interface {v0}, LaaC;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method private a(LaFO;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 3

    .prologue
    .line 1258
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 1259
    if-nez v0, :cond_0

    .line 1260
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 1261
    const-string v2, "accountName"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v2

    .line 1262
    if-eqz v2, :cond_0

    invoke-virtual {v2, p1}, LaFO;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1263
    const-string v0, "collectionEntrySpec"

    .line 1264
    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 1268
    :cond_0
    if-nez v0, :cond_1

    .line 1269
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(LaFO;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 1271
    :cond_1
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;LaFO;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(LaFO;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    return-object v0
.end method

.method private a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 607
    iget-boolean v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->j:Z

    if-eqz v0, :cond_0

    sget v0, Lxi;->upload_shared_item_title_convert:I

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    sget v0, Lxi;->upload_shared_item_title:I

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 512
    if-nez p1, :cond_0

    .line 513
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaaJ;

    const-string v1, "_display_name"

    .line 514
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    .line 513
    invoke-virtual {v0, p2, v1, v2}, LaaJ;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 515
    if-nez p1, :cond_0

    .line 516
    sget v0, Lxi;->upload_untitled_file_title:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 520
    :cond_0
    return-object p1
.end method

.method private a()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LaFO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 728
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 729
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LSF;

    invoke-interface {v0}, LSF;->a()[Landroid/accounts/Account;

    move-result-object v2

    .line 730
    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 731
    iget-object v5, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LajO;

    iget-object v6, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 732
    invoke-static {v6}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v6

    invoke-interface {v5, v6}, LajO;->a(LaFO;)LajN;

    move-result-object v5

    .line 733
    invoke-direct {p0, v5}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(LajN;)Z

    move-result v6

    .line 737
    invoke-virtual {v5}, LajN;->b()Z

    move-result v5

    if-nez v5, :cond_0

    if-eqz v6, :cond_1

    .line 738
    :cond_0
    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v4}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 730
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 742
    :cond_2
    return-object v1
.end method

.method private a(Landroid/content/Intent;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "LaaC;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 430
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 432
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 433
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 434
    const-string v3, "evaluateBeforeUploading"

    .line 435
    invoke-virtual {v0, v3, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 437
    const-string v4, "android.intent.action.SEND"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 439
    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    instance-of v2, v2, Landroid/net/Uri;

    if-eqz v2, :cond_1

    .line 440
    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 441
    const-string v2, "android.intent.extra.SUBJECT"

    .line 442
    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Ljava/lang/String;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 443
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a()LaaG;

    move-result-object v4

    .line 444
    invoke-virtual {v4, v0}, LaaG;->a(Landroid/net/Uri;)LaaG;

    move-result-object v0

    invoke-virtual {v0, v2}, LaaG;->a(Ljava/lang/String;)LaaG;

    move-result-object v0

    .line 445
    invoke-virtual {v0, v3}, LaaG;->a(Z)LaaG;

    move-result-object v0

    .line 446
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LaaG;->b(Ljava/lang/String;)LaaG;

    .line 447
    invoke-virtual {v4}, LaaG;->a()LaaE;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    move-object v0, v1

    .line 482
    :goto_1
    return-object v0

    .line 448
    :cond_1
    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 449
    const-string v0, "android.intent.extra.SUBJECT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 450
    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 451
    new-instance v3, LaaH;

    invoke-direct {v3, v0, v2, p0}, LaaH;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Landroid/content/Context;)V

    .line 452
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 460
    :cond_2
    new-instance v0, LaaT;

    invoke-direct {v0, p0}, LaaT;-><init>(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 461
    const-string v0, "UploadSharedItemActivityDelegate"

    const-string v2, "Uploading single file but neither EXTRA_STREAM nor EXTRA_TEXT is specified."

    invoke-static {v0, v2}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 463
    :cond_3
    const-string v0, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 464
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 465
    if-nez v2, :cond_4

    .line 466
    const-string v0, "UploadSharedItemActivityDelegate"

    const-string v2, "Uploading multiple files but EXTRA_STREAM is null or not specified."

    invoke-static {v0, v2}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 467
    goto :goto_1

    .line 469
    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a()LaaG;

    move-result-object v4

    .line 470
    invoke-virtual {v4, v3}, LaaG;->a(Z)LaaG;

    .line 471
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 472
    const/4 v5, 0x0

    invoke-direct {p0, v5, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Ljava/lang/String;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    .line 473
    if-nez v0, :cond_5

    .line 474
    const-string v0, "UploadSharedItemActivityDelegate"

    const-string v5, "Null uri found when uploading %d files."

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v0, v5, v6}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_2

    .line 476
    :cond_5
    invoke-virtual {v4, v0}, LaaG;->a(Landroid/net/Uri;)LaaG;

    move-result-object v0

    invoke-virtual {v0, v5}, LaaG;->a(Ljava/lang/String;)LaaG;

    .line 477
    invoke-virtual {v4}, LaaG;->a()LaaE;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)Ljava/util/List;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Landroid/content/Intent;)Ljava/util/List;
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    return-object p1
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 1125
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1126
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Lald;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lald;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1127
    const-string v1, "UploadSharedItemActivityDelegate"

    invoke-static {v1, v0}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1128
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->finish()V

    .line 1129
    return-void
.end method

.method private a(ILaFO;)V
    .locals 2

    .prologue
    .line 982
    if-lez p1, :cond_0

    .line 983
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaGR;

    new-instance v1, LaaZ;

    invoke-direct {v1, p0, p2, p1}, LaaZ;-><init>(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;LaFO;I)V

    invoke-virtual {v0, v1}, LaGR;->b(LaGN;)V

    .line 1012
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 1013
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr v0, p1

    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(LaFO;I)V

    .line 1015
    :cond_1
    return-void
.end method

.method private a(LaFO;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 663
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LajO;

    invoke-interface {v0, p1}, LajO;->a(LaFO;)LajN;

    move-result-object v3

    .line 667
    iget-boolean v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->i:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, v3}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(LajN;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 668
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    :goto_1
    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 671
    invoke-virtual {v3}, LajN;->b()Z

    move-result v0

    .line 672
    if-nez v0, :cond_0

    .line 673
    iget-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/CheckBox;

    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 675
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 676
    return-void

    :cond_1
    move v0, v2

    .line 667
    goto :goto_0

    .line 668
    :cond_2
    const/16 v2, 0x8

    goto :goto_1
.end method

.method private a(LaFO;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1021
    if-lez p2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 1023
    new-instance v0, LaE;

    invoke-direct {v0, p0}, LaE;-><init>(Landroid/content/Context;)V

    sget v3, Lxb;->notification_icon:I

    .line 1024
    invoke-virtual {v0, v3}, LaE;->a(I)LaE;

    move-result-object v0

    .line 1025
    invoke-virtual {v0, v1}, LaE;->b(Z)LaE;

    move-result-object v0

    .line 1026
    invoke-virtual {v0, v2}, LaE;->a(Z)LaE;

    move-result-object v0

    const/4 v3, -0x1

    .line 1027
    invoke-virtual {v0, v3}, LaE;->b(I)LaE;

    move-result-object v0

    sget v3, Lxi;->upload_notification_failure_no_retry_title:I

    .line 1028
    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LaE;->a(Ljava/lang/CharSequence;)LaE;

    move-result-object v0

    .line 1030
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lxg;->upload_notification_prepare_upload_failures:I

    new-array v1, v1, [Ljava/lang/Object;

    .line 1031
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    .line 1030
    invoke-virtual {v3, v4, p2, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1032
    invoke-virtual {v0, v1}, LaE;->b(Ljava/lang/CharSequence;)LaE;

    .line 1033
    invoke-virtual {v0, v1}, LaE;->c(Ljava/lang/CharSequence;)LaE;

    .line 1038
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LCo;

    sget-object v4, LCn;->h:LCn;

    .line 1039
    invoke-interface {v3, v4}, LCo;->a(LCn;)LCl;

    move-result-object v3

    .line 1037
    invoke-static {v1, p1, v3}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;LaFO;LCl;)Landroid/content/Intent;

    move-result-object v1

    .line 1040
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2, v1, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 1041
    invoke-virtual {v0, v1}, LaE;->a(Landroid/app/PendingIntent;)LaE;

    .line 1043
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LUx;

    const/4 v2, 0x7

    invoke-virtual {v0}, LaE;->a()Landroid/app/Notification;

    move-result-object v0

    invoke-interface {v1, v2, v0}, LUx;->a(ILandroid/app/Notification;)V

    .line 1044
    return-void

    :cond_0
    move v0, v2

    .line 1021
    goto :goto_0
.end method

.method private a(Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 367
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 368
    new-instance v1, LaaJ;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, v2}, LaaJ;-><init>(Landroid/content/ContentResolver;)V

    iput-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaaJ;

    .line 369
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    .line 371
    const-string v1, "android.intent.action.SEND"

    const-string v2, "android.intent.action.SEND_MULTIPLE"

    invoke-static {v1, v2}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmY;

    move-result-object v1

    .line 372
    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 373
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid intent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(Ljava/lang/String;)V

    .line 427
    :goto_0
    return-void

    .line 377
    :cond_0
    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "android.intent.extra.TEXT"

    .line 379
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 380
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 381
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LQr;

    const-string v3, "maxExtraTextLength"

    const v4, 0xf4240

    .line 382
    invoke-interface {v2, v3, v4}, LQr;->a(Ljava/lang/String;I)I

    move-result v2

    if-le v1, v2, :cond_1

    .line 383
    sget v0, Lxi;->notification_extra_text_is_too_long:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(I)V

    goto :goto_0

    .line 388
    :cond_1
    new-instance v1, LaaS;

    invoke-direct {v1, p0, p1, v0}, LaaS;-><init>(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Landroid/content/Intent;Ljava/lang/String;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    .line 426
    invoke-virtual {v1, v0}, LaaS;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private a(Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 599
    iget-boolean v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->k:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    const-string v0, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600
    const-string v0, "UploadSharedItemActivityDelegate"

    const-string v1, "cleaning up file %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 601
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 602
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 604
    :cond_0
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 353
    sget v0, Lxc;->upload_textview_document_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/TextView;

    .line 354
    sget v0, Lxc;->upload_multiple_listview_document_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b:Landroid/widget/TextView;

    .line 355
    sget v0, Lxc;->upload_edittext_document_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/EditText;

    .line 356
    sget v0, Lxc;->upload_image_preview:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/ImageView;

    .line 357
    sget v0, Lxc;->upload_doclist_convert:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/CheckBox;

    .line 358
    sget v0, Lxc;->upload_spinner_account:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/Spinner;

    .line 359
    sget v0, Lxc;->upload_folder:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/Button;

    .line 360
    sget v0, Lxc;->upload_conversion_options_layout:I

    .line 361
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/LinearLayout;

    .line 363
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->f()Z

    .line 364
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->f()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;ILaFO;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(ILaFO;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;LaFO;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(LaFO;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Ljava/util/List;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Z)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->c(Z)V

    return-void
.end method

.method private a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 2

    .prologue
    .line 773
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaGR;

    new-instance v1, LaaW;

    invoke-direct {v1, p0, p1}, LaaW;-><init>(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    invoke-virtual {v0, v1}, LaGR;->a(LaGN;)V

    .line 809
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 943
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 945
    if-eq v0, v5, :cond_2

    .line 946
    if-nez v0, :cond_0

    .line 947
    const-string v1, "UploadSharedItemActivityDelegate"

    const-string v2, "Upload not scheduled. Caller: "

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 949
    :cond_0
    if-le v0, v5, :cond_1

    .line 950
    const-string v0, "UploadSharedItemActivityDelegate"

    const-string v1, "Will not return more than 1 Uris? caller: %s, documents:%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 951
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v3

    aput-object v3, v2, v6

    aput-object p1, v2, v5

    .line 950
    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 953
    :cond_1
    invoke-virtual {p0, v5}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->setResult(I)V

    .line 962
    :goto_0
    return-void

    .line 957
    :cond_2
    invoke-static {p1}, Lbnm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 958
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaIa;

    invoke-interface {v1, v0}, LaIa;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/net/Uri;

    move-result-object v0

    .line 959
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 960
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 961
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->setResult(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method private a(Ljava/util/List;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LaFO;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 747
    new-instance v1, Landroid/widget/ArrayAdapter;

    .line 748
    invoke-static {p0}, LEL;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    const v2, 0x1090008

    invoke-direct {v1, v0, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 750
    const v0, 0x1090009

    invoke-virtual {v1, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 752
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFO;

    .line 753
    invoke-virtual {v0}, LaFO;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 755
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 756
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/Spinner;

    new-instance v1, LaaV;

    invoke-direct {v1, p0, p1}, LaaV;-><init>(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 769
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/Spinner;

    invoke-virtual {v0, p2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 770
    return-void
.end method

.method private a(LMZ;)Z
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1064
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LajO;

    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaFO;

    .line 1065
    invoke-interface {v0, v1}, LajO;->a(LaFO;)LajN;

    move-result-object v6

    .line 1068
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/LinearLayout;

    .line 1069
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/CheckBox;

    .line 1070
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 1074
    :goto_0
    invoke-virtual {p1}, LMZ;->a()I

    move-result v1

    int-to-long v8, v1

    .line 1075
    const-wide/16 v2, 0x0

    .line 1079
    if-eqz v0, :cond_0

    invoke-virtual {p1}, LMZ;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "image/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1080
    :cond_0
    sget-object v0, LaGv;->d:LaGv;

    invoke-virtual {v6, v0}, LajN;->a(LaGv;)J

    move-result-wide v2

    .line 1094
    :cond_1
    cmp-long v0, v8, v2

    if-lez v0, :cond_4

    .line 1095
    invoke-virtual {p1}, LMZ;->a()Ljava/lang/String;

    move-result-object v0

    .line 1096
    invoke-static {p0, v8, v9}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 1097
    invoke-static {p0, v2, v3}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    .line 1098
    sget v3, Lxi;->file_too_large_for_upload:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1099
    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v0, v6, v5

    aput-object v1, v6, v4

    const/4 v0, 0x2

    aput-object v2, v6, v0

    .line 1100
    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1101
    sget v1, Lxi;->file_too_large_for_upload_title:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1102
    sget v2, Lxi;->file_too_large_for_upload_okbtn:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1104
    new-instance v3, Laba;

    invoke-direct {v3, p0, v0, v1, v2}, Laba;-><init>(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->runOnUiThread(Ljava/lang/Runnable;)V

    move v0, v4

    .line 1116
    :goto_1
    return v0

    .line 1070
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->j:Z

    goto :goto_0

    .line 1086
    :cond_3
    invoke-virtual {p1}, LMZ;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, LajN;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    .line 1087
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1089
    invoke-static {v0}, LaGv;->a(Ljava/lang/String;)LaGv;

    move-result-object v0

    invoke-virtual {v6, v0}, LajN;->a(LaGv;)J

    move-result-wide v0

    .line 1090
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    move-wide v2, v0

    .line 1091
    goto :goto_2

    :cond_4
    move v0, v5

    .line 1116
    goto :goto_1
.end method

.method private a(LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 486
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 488
    const-string v2, "forceShowDialog"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 505
    :cond_0
    :goto_0
    return v0

    .line 492
    :cond_1
    if-eqz p1, :cond_0

    .line 496
    if-eqz p2, :cond_0

    .line 499
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LSF;

    .line 500
    invoke-interface {v1}, LSF;->a()[Landroid/accounts/Account;

    move-result-object v1

    iget-object v2, p2, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    .line 499
    invoke-static {v1, v2}, LSH;->a([Landroid/accounts/Account;LaFO;)I

    move-result v1

    if-ltz v1, :cond_0

    .line 504
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaGM;

    invoke-interface {v1, p2}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFV;

    move-result-object v1

    .line 505
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(LajN;)Z
    .locals 2

    .prologue
    .line 679
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaaC;

    .line 680
    invoke-interface {v0}, LaaC;->b()Ljava/lang/String;

    move-result-object v0

    .line 681
    invoke-virtual {p1, v0}, LajN;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 682
    const/4 v0, 0x0

    .line 685
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->j:Z

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;LMZ;)Z
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(LMZ;)Z

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;)Z
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;)Z

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Z)Z
    .locals 0

    .prologue
    .line 109
    iput-boolean p1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->j:Z

    return p1
.end method

.method private b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 346
    sget v0, Lxe;->upload_shared_item_header:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 347
    sget v0, Lxc;->title:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 348
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 349
    return-object v1
.end method

.method static synthetic b(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private b(LaFO;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1276
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LpW;

    invoke-interface {v1, p1}, LpW;->a(LaFO;)LpU;

    move-result-object v1

    .line 1277
    const-string v2, "lastUploadCollectionEntrySpecPayload"

    const/4 v3, 0x0

    .line 1278
    invoke-interface {v1, v2, v3}, LpU;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1280
    if-eqz v2, :cond_0

    .line 1281
    iget-object v3, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaGM;

    .line 1282
    invoke-interface {v1}, LpU;->a()LaFO;

    move-result-object v1

    .line 1281
    invoke-interface {v3, v1, v2}, LaGM;->a(LaFO;Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    :try_end_0
    .catch LpX; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1287
    :cond_0
    :goto_0
    return-object v0

    .line 1284
    :catch_0
    move-exception v1

    .line 1285
    const-string v2, "UploadSharedItemActivityDelegate"

    const-string v3, "Failed to lookup the last collection to upload the file to."

    invoke-static {v2, v1, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1047
    sget v0, Lxi;->menu_my_drive:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1048
    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(LaFO;)V
    .locals 1

    .prologue
    .line 1303
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(LaFO;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 1304
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 1305
    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->j()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;LaFO;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(LaFO;)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->c(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    return-void
.end method

.method private b(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 2

    .prologue
    .line 1253
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1255
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1135
    const-string v0, "UploadSharedItemActivityDelegate"

    invoke-static {v0, p1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1136
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->finish()V

    .line 1137
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->l:Z

    return v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Z)Z
    .locals 0

    .prologue
    .line 109
    iput-boolean p1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->k:Z

    return p1
.end method

.method private c(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 3

    .prologue
    .line 1292
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LpW;

    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaFO;

    invoke-interface {v0, v1}, LpW;->a(LaFO;)LpU;

    move-result-object v0

    .line 1293
    const-string v1, "lastUploadCollectionEntrySpecPayload"

    .line 1295
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a()Ljava/lang/String;

    move-result-object v2

    .line 1293
    invoke-interface {v0, v1, v2}, LpU;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1296
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LpW;

    invoke-interface {v1, v0}, LpW;->a(LpU;)V
    :try_end_0
    .catch LpX; {:try_start_0 .. :try_end_0} :catch_0

    .line 1300
    :goto_0
    return-void

    .line 1297
    :catch_0
    move-exception v0

    .line 1298
    const-string v1, "UploadSharedItemActivityDelegate"

    const-string v2, "Failed to save the last collection to upload the file to."

    invoke-static {v1, v0, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private c(Z)V
    .locals 2

    .prologue
    .line 856
    new-instance v0, LaaX;

    invoke-direct {v0, p0, p1}, LaaX;-><init>(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Z)V

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/os/AsyncTask;

    .line 934
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 935
    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Z)Z
    .locals 0

    .prologue
    .line 109
    iput-boolean p1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->i:Z

    return p1
.end method

.method public static synthetic d(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Z)Z
    .locals 0

    .prologue
    .line 109
    iput-boolean p1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->l:Z

    return p1
.end method

.method private f()V
    .locals 1

    .prologue
    .line 589
    iget-boolean v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->j:Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->c(Z)V

    .line 590
    return-void
.end method

.method private f()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 708
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a()Ljava/util/List;

    move-result-object v1

    .line 709
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 710
    sget v1, Lxi;->no_account_support_this_upload:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(I)V

    .line 711
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->finish()V

    .line 724
    :goto_0
    return v0

    .line 715
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaFO;

    if-nez v2, :cond_1

    .line 716
    iget-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LSF;

    invoke-interface {v2}, LSF;->a()LaFO;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaFO;

    .line 718
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaFO;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 719
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFO;

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaFO;

    .line 720
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaFO;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(LaFO;)V

    .line 721
    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Ljava/util/List;I)V

    .line 723
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaFO;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(LaFO;)V

    .line 724
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private g()Z
    .locals 1

    .prologue
    .line 1309
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    return v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 593
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 594
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Landroid/net/Uri;)V

    .line 595
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->finish()V

    .line 596
    return-void
.end method

.method private k()V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 616
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 617
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 618
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 625
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->l()V

    .line 626
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_2

    .line 627
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaaC;

    .line 628
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 629
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 634
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lano;->a(Landroid/widget/EditText;Landroid/app/Dialog;)V

    .line 635
    invoke-interface {v0}, LaaC;->a()V

    .line 644
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->j:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 645
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/Button;

    new-instance v1, LaaU;

    invoke-direct {v1, p0}, LaaU;-><init>(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 656
    return-void

    .line 620
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 621
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 622
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/TextView;

    sget v1, Lxi;->upload_multiple_document_titles:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 631
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/EditText;

    invoke-interface {v0}, LaaC;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 637
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 638
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaaC;

    .line 639
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, LaaC;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 641
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method private l()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 690
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 691
    const/4 v0, 0x0

    .line 698
    :goto_0
    if-eqz v0, :cond_1

    .line 699
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 700
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 701
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 705
    :goto_1
    return-void

    .line 693
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaaC;

    .line 694
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/ImageView;

    .line 695
    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 694
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 696
    invoke-interface {v0, v1}, LaaC;->a(I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 703
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public a()LaFO;
    .locals 1

    .prologue
    .line 1249
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaFO;

    return-object v0
.end method

.method public a(LbmF;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "LMZ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 965
    invoke-static {}, Lbrm;->a()Lbrm;

    move-result-object v1

    .line 966
    invoke-virtual {p1}, LbmF;->a()Lbqv;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LMZ;

    .line 967
    invoke-virtual {v1, v0}, Lbrm;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    goto :goto_0

    .line 970
    :cond_0
    :try_start_0
    invoke-virtual {v1}, Lbrm;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 974
    :goto_1
    return-void

    .line 971
    :catch_0
    move-exception v0

    .line 972
    const-string v1, "UploadSharedItemActivityDelegate"

    const-string v2, "Error closing items to upload: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 334
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Landroid/content/Intent;)V

    .line 335
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 571
    invoke-super {p0, p1, p2, p3}, Lrm;->onActivityResult(IILandroid/content/Intent;)V

    .line 572
    if-ne p1, v3, :cond_1

    .line 573
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 574
    const-string v0, "entrySpec.v2"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 575
    const-string v1, "UploadSharedItemActivityDelegate"

    const-string v2, "Picked folder entry %s"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 578
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 586
    :cond_0
    :goto_0
    return-void

    .line 581
    :cond_1
    if-eqz p1, :cond_2

    .line 582
    const-string v0, "UploadSharedItemActivityDelegate"

    const-string v1, "Invalid request code in activity result."

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 584
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->finish()V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 1053
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1054
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Landroid/net/Uri;)V

    .line 1055
    invoke-super {p0}, Lrm;->onBackPressed()V

    .line 1056
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 272
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 274
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 275
    const-string v0, "accountName"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaFO;

    .line 277
    invoke-static {}, LboS;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/Map;

    .line 279
    if-eqz p1, :cond_1

    .line 285
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a()LM;

    move-result-object v0

    const-string v1, "UploadSharedItemDialog"

    .line 286
    invoke-virtual {v0, v1}, LM;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;

    .line 287
    if-eqz v0, :cond_1

    .line 288
    const-string v1, "docListTitle"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/lang/String;

    .line 289
    const-string v1, "accountName"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaFO;

    .line 291
    const-string v1, "AccountCollectionList"

    .line 292
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 293
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 294
    iget-object v4, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/Map;

    iget-object v5, v1, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-interface {v4, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 297
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;->a()V

    .line 300
    const-string v0, "forceShowDialog"

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 302
    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Landroid/content/Intent;)V

    .line 305
    :cond_1
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->unbindService(Landroid/content/ServiceConnection;)V

    .line 327
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/content/ServiceConnection;

    .line 329
    :cond_0
    invoke-super {p0}, Lrm;->onDestroy()V

    .line 330
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 540
    invoke-super {p0}, Lrm;->onResume()V

    .line 544
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/Spinner;

    if-eqz v0, :cond_1

    .line 545
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->f()Z

    .line 549
    :cond_0
    :goto_0
    return-void

    .line 546
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaFO;

    if-eqz v0, :cond_0

    .line 547
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaFO;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(LaFO;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 553
    invoke-super {p0, p1}, Lrm;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 555
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 556
    const-string v0, "docListTitle"

    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    :cond_0
    const-string v0, "accountName"

    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaFO;

    invoke-static {v1}, LaFO;->a(LaFO;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 561
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 562
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 563
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFO;

    iget-object v4, v1, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-virtual {v0, v4}, LaFO;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LbiT;->a(Z)V

    .line 564
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 566
    :cond_1
    const-string v0, "AccountCollectionList"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 567
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 311
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/app/ProgressDialog;

    .line 313
    :cond_0
    invoke-super {p0}, Lrm;->onStop()V

    .line 314
    return-void
.end method
