.class public Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;
.super Lcom/google/android/apps/docs/app/BaseFragment;
.source "FullscreenSwitcherFragment.java"


# instance fields
.field private a:I

.field public a:LalR;

.field private final a:Ljava/lang/Runnable;

.field private a:Z

.field private b:I

.field private b:Z

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 22
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseFragment;-><init>()V

    .line 42
    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->a:Z

    .line 43
    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->b:Z

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->c:Z

    .line 70
    new-instance v0, LRm;

    invoke-direct {v0, p0}, LRm;-><init>(Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->a:Ljava/lang/Runnable;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;)I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->b:I

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;I)I
    .locals 0

    .prologue
    .line 22
    iput p1, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->b:I

    return p1
.end method

.method public static a(ZZZI)Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;
    .locals 3

    .prologue
    .line 51
    new-instance v0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;-><init>()V

    .line 52
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 53
    const-string v2, "keyFullscreenMode"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 54
    const-string v2, "forceShow"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 55
    const-string v2, "switchWithProfile"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 56
    const-string v2, "actionBarTimeout"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 57
    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->e(Landroid/os/Bundle;)V

    .line 58
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;Z)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->b(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 4

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->a:LalR;

    invoke-interface {v0}, LalR;->a()Landroid/view/View;

    move-result-object v1

    .line 153
    if-eqz v1, :cond_0

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->a:Ljava/lang/Runnable;

    invoke-virtual {v1, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 157
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 170
    :cond_1
    :goto_0
    return-void

    .line 162
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->d:Z

    if-eqz v0, :cond_3

    iget-boolean p1, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->d:Z

    .line 164
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->a:LalR;

    if-eqz p1, :cond_4

    sget-object v0, LalQ;->b:LalQ;

    :goto_1
    invoke-static {v2, v0}, LalQ;->a(LalR;LalQ;)V

    .line 166
    iput-boolean p1, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->b:Z

    .line 167
    if-eqz p1, :cond_1

    if-eqz v1, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->d:Z

    if-nez v0, :cond_1

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->a:Ljava/lang/Runnable;

    iget v2, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->a:I

    int-to-long v2, v2

    invoke-virtual {v1, v0, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 164
    :cond_4
    sget-object v0, LalQ;->a:LalQ;

    goto :goto_1
.end method

.method private u()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 119
    invoke-static {}, LakQ;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->a:LalR;

    invoke-interface {v0}, LalR;->a()Landroid/view/View;

    move-result-object v0

    .line 121
    if-eqz v0, :cond_0

    .line 122
    new-instance v1, LRn;

    invoke-direct {v1, p0}, LRn;-><init>(Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 146
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 176
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->d:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 177
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->b:Z

    if-nez v0, :cond_1

    :goto_1
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->b(Z)V

    .line 178
    return-void

    :cond_0
    move v0, v2

    .line 176
    goto :goto_0

    :cond_1
    move v1, v2

    .line 177
    goto :goto_1
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 190
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->d:Z

    .line 191
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->b(Z)V

    .line 192
    return-void
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 96
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseFragment;->a_(Landroid/os/Bundle;)V

    .line 97
    const-string v0, "FullscreenSwitcherFragment"

    const-string v1, "onCreate savedInstanceState=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 99
    if-eqz p1, :cond_0

    .line 101
    :goto_0
    const-string v0, "keyFullscreenMode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->b:Z

    .line 102
    const-string v0, "forceShow"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->d:Z

    .line 103
    const-string v0, "switchWithProfile"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->c:Z

    .line 104
    const-string v0, "actionBarTimeout"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->a:I

    .line 105
    return-void

    .line 99
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->a()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 109
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseFragment;->b(Landroid/os/Bundle;)V

    .line 110
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->c:Z

    if-eqz v0, :cond_0

    .line 113
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->u()V

    .line 115
    :cond_0
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 63
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseFragment;->c(Landroid/os/Bundle;)V

    .line 64
    const-string v0, "keyFullscreenMode"

    iget-boolean v1, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->b:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 65
    const-string v0, "forceShow"

    iget-boolean v1, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 66
    const-string v0, "switchWithProfile"

    iget-boolean v1, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 67
    const-string v0, "actionBarTimeout"

    iget v1, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->a:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 68
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 79
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseFragment;->g()V

    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->a:Z

    .line 81
    return-void
.end method

.method protected m()Z
    .locals 1

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->a:Z

    return v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 195
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->d:Z

    return v0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 199
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->b:Z

    return v0
.end method

.method public o_()V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseFragment;->o_()V

    .line 86
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->a:Z

    .line 87
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->b:Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->b(Z)V

    .line 88
    return-void
.end method

.method public t()V
    .locals 1

    .prologue
    .line 181
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->d:Z

    .line 182
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->d:Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->b(Z)V

    .line 183
    return-void
.end method
