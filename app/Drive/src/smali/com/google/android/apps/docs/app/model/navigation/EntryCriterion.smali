.class public final Lcom/google/android/apps/docs/app/model/navigation/EntryCriterion;
.super Ljava/lang/Object;
.source "EntryCriterion.java"

# interfaces
.implements Lcom/google/android/apps/docs/app/model/navigation/Criterion;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/EntryCriterion;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/google/android/gms/drive/database/data/EntrySpec;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, LvT;

    invoke-direct {v0}, LvT;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/app/model/navigation/EntryCriterion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/EntryCriterion;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 20
    return-void
.end method


# virtual methods
.method public a(LaGM;)LaeZ;
    .locals 1

    .prologue
    .line 30
    sget-object v0, LaeZ;->d:LaeZ;

    return-object v0
.end method

.method public a(LvR;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LvR",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/EntryCriterion;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {p1, v0}, LvR;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 61
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 65
    if-ne p1, p0, :cond_0

    .line 66
    const/4 v0, 0x1

    .line 71
    :goto_0
    return v0

    .line 67
    :cond_0
    instance-of v0, p1, Lcom/google/android/apps/docs/app/model/navigation/EntryCriterion;

    if-eqz v0, :cond_1

    .line 68
    check-cast p1, Lcom/google/android/apps/docs/app/model/navigation/EntryCriterion;

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/EntryCriterion;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v1, p1, Lcom/google/android/apps/docs/app/model/navigation/EntryCriterion;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 71
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 77
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/apps/docs/app/model/navigation/EntryCriterion;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/docs/app/model/navigation/EntryCriterion;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 82
    const-string v0, "EntryCriterion {entrySpec=%s}"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/docs/app/model/navigation/EntryCriterion;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/EntryCriterion;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 41
    return-void
.end method
