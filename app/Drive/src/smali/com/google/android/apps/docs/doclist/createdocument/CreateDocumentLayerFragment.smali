.class public Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;
.super Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;
.source "CreateDocumentLayerFragment.java"

# interfaces
.implements Lari;


# instance fields
.field public a:LVB;

.field private a:LVK;

.field public a:LaFO;

.field public a:LaGR;

.field public a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "Larg;",
            ">;"
        }
    .end annotation
.end field

.field public a:Lwm;

.field private a:Z

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;-><init>()V

    .line 70
    new-instance v0, LEn;

    invoke-direct {v0, p0}, LEn;-><init>(Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a:LVK;

    return-void
.end method

.method private a()F
    .locals 4

    .prologue
    .line 147
    const/4 v0, 0x0

    .line 148
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a:LVB;

    invoke-virtual {v1}, LVB;->a()Landroid/graphics/Rect;

    move-result-object v1

    .line 149
    if-eqz v1, :cond_0

    .line 150
    const/4 v2, 0x2

    new-array v2, v2, [I

    .line 151
    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->d:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 152
    const/4 v3, 0x0

    aget v2, v2, v3

    iget v3, v1, Landroid/graphics/Rect;->right:I

    if-ge v2, v3, :cond_0

    .line 153
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a()LH;

    move-result-object v2

    invoke-static {v2}, LakZ;->a(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v2

    .line 154
    iget v2, v2, Landroid/graphics/Point;->y:I

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int v1, v2, v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    .line 158
    :cond_0
    return v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a:Lwm;

    invoke-interface {v0}, Lwm;->a()LbmF;

    move-result-object v0

    .line 106
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a:LaGR;

    new-instance v2, LEp;

    invoke-direct {v2, p0, v0}, LEp;-><init>(Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;Ljava/util/List;)V

    invoke-virtual {v1, v2}, LaGR;->b(LaGN;)V

    .line 122
    return-void
.end method

.method private a(FF)V
    .locals 3

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->d:Landroid/view/View;

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    .line 174
    invoke-static {v0, v1}, Lxm;->b(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v0

    invoke-static {v0}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    .line 175
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a()LH;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxr;->b(Landroid/content/Context;)Lxr;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->d:Landroid/view/View;

    .line 176
    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxr;->c(Landroid/content/res/Resources;)Lxr;

    move-result-object v0

    .line 177
    invoke-virtual {v0}, Lxr;->b()Landroid/animation/Animator;

    move-result-object v0

    .line 178
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 179
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;Z)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v1

    .line 137
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a()F

    move-result v2

    .line 138
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 139
    :goto_0
    if-eqz v0, :cond_1

    .line 140
    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a(FF)V

    .line 144
    :goto_1
    return-void

    .line 138
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 142
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_1
.end method

.method private b(FF)V
    .locals 3

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->d:Landroid/view/View;

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    .line 183
    invoke-static {v0, v1}, Lxm;->b(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v0

    invoke-static {v0}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v0

    .line 184
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a()LH;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxr;->c(Landroid/content/Context;)Lxr;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->d:Landroid/view/View;

    .line 185
    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lxr;->c(Landroid/content/res/Resources;)Lxr;

    move-result-object v0

    .line 186
    invoke-virtual {v0}, Lxr;->b()Landroid/animation/Animator;

    move-result-object v0

    .line 187
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 188
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v1

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v2, v0

    .line 164
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 165
    :goto_0
    if-eqz v0, :cond_1

    .line 166
    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->b(FF)V

    .line 170
    :goto_1
    return-void

    .line 164
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 168
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x2

    .line 86
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const v1, 0x800055

    invoke-direct {v0, v2, v2, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a()LH;

    move-result-object v0

    invoke-virtual {v0}, LH;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    sget v1, Lxc;->custom_layer:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->d:Landroid/view/View;

    .line 89
    sget v0, Lxe;->new_document_button:I

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 91
    new-instance v1, LEo;

    invoke-direct {v1, p0}, LEo;-><init>(Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 100
    return-object v0
.end method

.method public a(Larj;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 126
    sget-object v0, Larj;->a:Larj;

    invoke-virtual {p1, v0}, Larj;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 128
    if-eqz v0, :cond_0

    .line 129
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a(Z)V

    .line 133
    :goto_0
    return-void

    .line 131
    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->b(Z)V

    goto :goto_0
.end method

.method public g()V
    .locals 1

    .prologue
    .line 215
    invoke-super {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->o_()V

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larg;

    invoke-interface {v0, p0}, Larg;->b(Lari;)V

    .line 218
    return-void
.end method

.method public j_()V
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a:LVB;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a:LVK;

    invoke-virtual {v0, v1}, LVB;->b(LVK;)V

    .line 202
    invoke-super {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->j_()V

    .line 203
    return-void
.end method

.method public k_()V
    .locals 2

    .prologue
    .line 192
    invoke-super {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->k_()V

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a:LVB;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a:LVK;

    invoke-virtual {v0, v1}, LVB;->a(LVK;)V

    .line 195
    return-void
.end method

.method public o_()V
    .locals 1

    .prologue
    .line 207
    invoke-super {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->o_()V

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larg;

    invoke-interface {v0, p0}, Larg;->a(Lari;)V

    .line 210
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentLayerFragment;->a:Z

    .line 211
    return-void
.end method
