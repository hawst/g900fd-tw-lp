.class public Lcom/google/android/apps/docs/view/SwipeToRefreshView;
.super Landroid/support/v4/widget/SwipeRefreshLayout;
.source "SwipeToRefreshView.java"


# instance fields
.field a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

.field a:Lcom/google/android/apps/docs/view/DocListView;

.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;-><init>(Landroid/content/Context;)V

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/SwipeToRefreshView;->a:Z

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/support/v4/widget/SwipeRefreshLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/SwipeToRefreshView;->a:Z

    .line 37
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 63
    const/4 v0, 0x4

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, LwZ;->progress_dark_green:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, LwZ;->progress_blue:I

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, LwZ;->progress_yellow:I

    aput v2, v0, v1

    const/4 v1, 0x3

    sget v2, LwZ;->progress_green:I

    aput v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/SwipeToRefreshView;->setColorScheme([I)V

    .line 67
    return-void
.end method

.method private d()V
    .locals 5

    .prologue
    .line 72
    :try_start_0
    const-class v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    const-string v1, "setProgressViewEndTarget"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    .line 73
    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 78
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/SwipeToRefreshView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lxa;->doclist_group_title_height:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 79
    const/high16 v2, 0x42200000    # 40.0f

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/SwipeToRefreshView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 80
    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 82
    const/4 v2, 0x2

    :try_start_1
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-virtual {v0, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 86
    :goto_0
    return-void

    .line 83
    :catch_0
    move-exception v0

    goto :goto_0

    .line 74
    :catch_1
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/SwipeToRefreshView;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/SwipeToRefreshView;->a:Z

    .line 94
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 95
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/google/android/apps/docs/view/SwipeToRefreshView;->a:Z

    invoke-super {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 102
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 0

    .prologue
    .line 57
    invoke-super {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->onAttachedToWindow()V

    .line 58
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/SwipeToRefreshView;->c()V

    .line 59
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/SwipeToRefreshView;->d()V

    .line 60
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 46
    iget-object v1, p0, Lcom/google/android/apps/docs/view/SwipeToRefreshView;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/docs/view/SwipeToRefreshView;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 52
    :cond_0
    :goto_0
    return v0

    .line 49
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/docs/view/SwipeToRefreshView;->a:Lcom/google/android/apps/docs/view/DocListView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/docs/view/SwipeToRefreshView;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/view/DocListView;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    invoke-super {p0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setRefreshing(Z)V
    .locals 1

    .prologue
    .line 106
    if-nez p1, :cond_0

    .line 109
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/SwipeToRefreshView;->a:Z

    .line 111
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 112
    return-void
.end method

.method public setup(Lcom/google/android/apps/docs/view/DocListView;Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/apps/docs/view/SwipeToRefreshView;->a:Lcom/google/android/apps/docs/view/DocListView;

    .line 41
    iput-object p2, p0, Lcom/google/android/apps/docs/view/SwipeToRefreshView;->a:Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    .line 42
    return-void
.end method
