.class public Lcom/google/android/apps/docs/doclist/documentopener/PunchPreviewDocumentOpener;
.super LGt;
.source "PunchPreviewDocumentOpener.java"


# instance fields
.field private final a:LQr;

.field private final a:Lala;

.field private final a:Landroid/content/Context;

.field private final a:LtK;


# direct methods
.method public constructor <init>(Landroid/content/Context;LQr;LtK;Lala;Lye;Lcom/google/android/apps/docs/doclist/documentopener/ForcePreventOpener;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1, p3, p5, p6}, LGt;-><init>(Landroid/content/Context;LtK;Lye;Lcom/google/android/apps/docs/doclist/documentopener/ForcePreventOpener;)V

    .line 56
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/PunchPreviewDocumentOpener;->a:Landroid/content/Context;

    .line 57
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQr;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/PunchPreviewDocumentOpener;->a:LQr;

    .line 58
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtK;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/PunchPreviewDocumentOpener;->a:LtK;

    .line 59
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lala;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/PunchPreviewDocumentOpener;->a:Lala;

    .line 60
    return-void
.end method

.method private final a(Landroid/net/Uri$Builder;)V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/PunchPreviewDocumentOpener;->a:LQr;

    const-string v2, "punchUriParametersToAdd"

    const-string v3, "touch=1, rm=minimal"

    invoke-interface {v0, v2, v3}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 118
    const-string v2, "\\s*([a-zA-Z]\\w*)\\s*=\\s*(\\w+)\\s*"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 119
    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    .line 120
    invoke-virtual {v2, v5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    .line 121
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->matches()Z

    move-result v7

    if-nez v7, :cond_0

    .line 122
    const-string v6, "PunchPreviewDocumentOpener"

    const-string v7, "Client flag key \"%s\" contains invalid parameter: %s"

    new-array v8, v11, [Ljava/lang/Object;

    const-string v9, "punchUriParametersToAdd"

    aput-object v9, v8, v1

    aput-object v5, v8, v10

    invoke-static {v6, v7, v8}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 119
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 126
    :cond_0
    invoke-virtual {v6, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    .line 127
    invoke-virtual {v6, v11}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    .line 128
    invoke-virtual {p1, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_1

    .line 130
    :cond_1
    return-void
.end method


# virtual methods
.method protected a(LaGo;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 65
    invoke-interface {p1}, LaGo;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 66
    invoke-static {v1}, LFO;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 67
    invoke-interface {p1}, LaGo;->a()LaFM;

    move-result-object v3

    invoke-virtual {v3}, LaFM;->a()LaFO;

    move-result-object v3

    .line 68
    const-string v4, "editMode"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 71
    iget-object v5, p0, Lcom/google/android/apps/docs/doclist/documentopener/PunchPreviewDocumentOpener;->a:LtK;

    sget-object v6, Lry;->ap:Lry;

    invoke-interface {v5, v6}, LtK;->a(LtJ;)Z

    move-result v5

    if-eqz v5, :cond_1

    if-nez v4, :cond_1

    .line 72
    iget-object v4, p0, Lcom/google/android/apps/docs/doclist/documentopener/PunchPreviewDocumentOpener;->a:LQr;

    const-string v5, "punchUriSignature"

    const-string v6, "/presentation/"

    .line 73
    invoke-interface {v4, v5, v6}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 74
    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 75
    const-string v2, "PunchPreviewDocumentOpener"

    const-string v4, "Opening the Punch HiFi WebView."

    invoke-static {v2, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    invoke-virtual {v1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    .line 77
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    .line 78
    invoke-virtual {v4, v0}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 79
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_0

    .line 80
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 79
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/PunchPreviewDocumentOpener;->a:LQr;

    const-string v1, "punchUriPathToAdd"

    const-string v2, "mobilepresent"

    .line 84
    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 85
    invoke-virtual {v4, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 87
    invoke-direct {p0, v4}, Lcom/google/android/apps/docs/doclist/documentopener/PunchPreviewDocumentOpener;->a(Landroid/net/Uri$Builder;)V

    .line 89
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/PunchPreviewDocumentOpener;->a:Landroid/content/Context;

    .line 90
    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-interface {p1}, LaGo;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, LaGo;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v5

    move-object v0, p0

    .line 89
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/docs/doclist/documentopener/PunchPreviewDocumentOpener;->a(Landroid/content/Context;Landroid/net/Uri;LaFO;Ljava/lang/String;Lcom/google/android/gms/drive/database/data/ResourceSpec;)Landroid/content/Intent;

    move-result-object v0

    .line 95
    :cond_1
    return-object v0
.end method

.method public a(Landroid/content/Context;Landroid/net/Uri;LaFO;Ljava/lang/String;Lcom/google/android/gms/drive/database/data/ResourceSpec;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/PunchPreviewDocumentOpener;->a:Lala;

    sget-object v0, LaGv;->g:LaGv;

    invoke-static {p2, p3, p4, v0}, Lala;->a(Landroid/net/Uri;LaFO;Ljava/lang/String;LaGv;)Landroid/content/Intent;

    move-result-object v0

    .line 110
    const-class v1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 111
    const-string v1, "resourceSpec"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 112
    return-object v0
.end method
