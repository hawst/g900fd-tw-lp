.class public Lcom/google/android/apps/docs/app/RetriesExceededActivity;
.super Lrm;
.source "RetriesExceededActivity.java"


# instance fields
.field public a:LUx;

.field public a:LaHr;

.field private a:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lrm;-><init>()V

    return-void
.end method

.method static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 84
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.BUG_REPORT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 85
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 86
    const-class v1, Lcom/google/android/apps/docs/app/RetriesExceededActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 87
    return-object v0
.end method

.method public static a(Landroid/content/Context;LUx;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 92
    invoke-static {p0}, Lcom/google/android/apps/docs/app/RetriesExceededActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 93
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 94
    sget v1, Lxi;->operation_retry_error:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 95
    new-instance v4, Landroid/app/Notification;

    const v5, 0x1080078

    invoke-direct {v4, v5, v1, v2, v3}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 97
    invoke-static {p0, v6, v0, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 98
    iget v2, v4, Landroid/app/Notification;->flags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v4, Landroid/app/Notification;->flags:I

    .line 99
    const-string v2, ""

    invoke-virtual {v4, p0, v1, v2, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 100
    const/4 v0, 0x4

    invoke-interface {p1, v0, v4}, LUx;->a(ILandroid/app/Notification;)V

    .line 102
    return-void
.end method


# virtual methods
.method public f()V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/docs/app/RetriesExceededActivity;->a:LUx;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, LUx;->a(I)V

    .line 106
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/high16 v4, 0x20000

    .line 38
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/RetriesExceededActivity;->getIntent()Landroid/content/Intent;

    .line 42
    invoke-static {p0}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v0

    .line 43
    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lxi;->operation_retry_error:I

    .line 44
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lxi;->operation_retry_exceeded_message:I

    .line 45
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    .line 46
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lxi;->operation_retry_exceeded_retry:I

    new-instance v3, Lvi;

    invoke-direct {v3, p0}, Lvi;-><init>(Lcom/google/android/apps/docs/app/RetriesExceededActivity;)V

    .line 47
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    new-instance v3, Lvh;

    invoke-direct {v3, p0}, Lvh;-><init>(Lcom/google/android/apps/docs/app/RetriesExceededActivity;)V

    .line 56
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 66
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/RetriesExceededActivity;->a:Landroid/app/AlertDialog;

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/docs/app/RetriesExceededActivity;->a:Landroid/app/AlertDialog;

    new-instance v1, Lvj;

    invoke-direct {v1, p0}, Lvj;-><init>(Lcom/google/android/apps/docs/app/RetriesExceededActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/docs/app/RetriesExceededActivity;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/docs/app/RetriesExceededActivity;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 76
    return-void
.end method
