.class public Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;
.super Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;
.source "RemoveDialogFragment.java"


# instance fields
.field private a:LaGD;

.field private a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private b:Lcom/google/android/gms/drive/database/data/EntrySpec;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;-><init>()V

    return-void
.end method

.method public static a(LaGD;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;
    .locals 3

    .prologue
    .line 58
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 59
    const-string v1, "entrySpecs"

    invoke-static {p0}, LbnG;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 60
    const-string v1, "collectionEntrySpec"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 61
    new-instance v1, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;-><init>()V

    .line 62
    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->e(Landroid/os/Bundle;)V

    .line 63
    return-object v1
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->a:LaGD;

    invoke-virtual {v0}, LaGD;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->b()Landroid/app/Dialog;

    move-result-object v1

    .line 118
    :goto_0
    return-object v1

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->a:LaGD;

    invoke-static {v0}, Lbnm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->a:LaGM;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGu;

    move-result-object v0

    .line 87
    if-nez v0, :cond_1

    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->b()Landroid/app/Dialog;

    move-result-object v1

    goto :goto_0

    .line 96
    :cond_1
    invoke-interface {v0}, LaGu;->a()LaGw;

    move-result-object v1

    sget-object v2, LaGw;->c:LaGw;

    invoke-virtual {v1, v2}, LaGw;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 97
    const/4 v5, 0x0

    .line 98
    sget v3, Lxi;->plus_photo_item_remove_dialog_confirm_button:I

    .line 99
    sget v2, Lxi;->plus_photo_item_remove_dialog_title:I

    .line 100
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxg;->plus_photo_item_remove_dialog_text:I

    iget-object v4, p0, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->a:LaGD;

    .line 101
    invoke-virtual {v4}, LaGD;->size()I

    move-result v4

    .line 100
    invoke-virtual {v0, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 116
    :goto_1
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v1

    move-object v0, p0

    .line 117
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->a(Landroid/app/Dialog;IILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 103
    :cond_2
    sget v3, Lxi;->remove_button_confirm:I

    .line 104
    invoke-interface {v0}, LaGu;->c()Ljava/lang/String;

    move-result-object v5

    .line 105
    invoke-interface {v0}, LaGu;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 106
    sget v2, Lxi;->remove_collection:I

    .line 108
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxi;->ask_confirmation_for_folder_deletion:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v4, v0

    goto :goto_1

    .line 110
    :cond_3
    sget v2, Lxi;->remove_document:I

    .line 112
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxi;->ask_confirmation_for_document_deletion:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v4, v0

    goto :goto_1
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;->a_(Landroid/os/Bundle;)V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "entrySpecs"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 71
    invoke-static {v0}, LaGD;->a(Ljava/util/Collection;)LaGD;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->a:LaGD;

    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "collectionEntrySpec"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 73
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 4

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->a()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 141
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->a()LH;

    move-result-object v1

    .line 142
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 143
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->c()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1}, LH;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Landroid/support/v4/app/Fragment;->a(IILandroid/content/Intent;)V

    .line 146
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 147
    return-void
.end method

.method protected t()V
    .locals 4

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->a()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->a(Landroid/app/Dialog;ILjava/lang/String;)V

    .line 125
    new-instance v0, LFm;

    invoke-direct {v0, p0}, LFm;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;)V

    .line 127
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->a:LUi;

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v1, v2, v3, v0}, LUi;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;LaHy;)V

    .line 128
    return-void
.end method

.method protected u()V
    .locals 2

    .prologue
    .line 132
    invoke-static {}, LamV;->a()V

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->a:LaGM;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v1, v1, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-interface {v0, v1}, LaGM;->a(LaFO;)LaFM;

    move-result-object v0

    .line 134
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/RemoveDialogFragment;->a:LaGM;

    invoke-interface {v1, v0}, LaGM;->b(LaFM;)V

    .line 135
    return-void
.end method
