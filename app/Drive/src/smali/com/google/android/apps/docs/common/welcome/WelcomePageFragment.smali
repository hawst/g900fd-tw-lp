.class public Lcom/google/android/apps/docs/common/welcome/WelcomePageFragment;
.super Landroid/support/v4/app/Fragment;
.source "WelcomePageFragment.java"


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static a(II)Lcom/google/android/apps/docs/common/welcome/WelcomePageFragment;
    .locals 2

    .prologue
    .line 32
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 33
    const-string v1, "layoutKey"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 34
    const-string v1, "positionKey"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 35
    new-instance v1, Lcom/google/android/apps/docs/common/welcome/WelcomePageFragment;

    invoke-direct {v1}, Lcom/google/android/apps/docs/common/welcome/WelcomePageFragment;-><init>()V

    .line 36
    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/common/welcome/WelcomePageFragment;->e(Landroid/os/Bundle;)V

    .line 37
    return-object v1
.end method

.method private static a(Landroid/view/View;)LxI;
    .locals 1

    .prologue
    .line 73
    sget v0, LxA;->g:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LxI;

    return-object v0
.end method

.method static a(Landroid/view/View;F)V
    .locals 3

    .prologue
    .line 45
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 46
    sget v1, Lxy;->a:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    mul-float/2addr v1, p1

    .line 47
    sget v2, Lxy;->b:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    mul-float/2addr v0, p1

    .line 48
    invoke-static {p0}, Lcom/google/android/apps/docs/common/welcome/WelcomePageFragment;->a(Landroid/view/View;)LxI;

    move-result-object v2

    .line 49
    invoke-virtual {v2, v1}, LxI;->a(F)V

    .line 50
    invoke-virtual {v2, v0}, LxI;->b(F)V

    .line 51
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 66
    iget v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomePageFragment;->a:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 67
    iget v1, p0, Lcom/google/android/apps/docs/common/welcome/WelcomePageFragment;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    .line 68
    sget v1, LxA;->g:I

    new-instance v2, LxI;

    invoke-direct {v2, v0}, LxI;-><init>(Landroid/view/ViewGroup;)V

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->setTag(ILjava/lang/Object;)V

    .line 69
    return-object v0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 57
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/docs/common/welcome/WelcomePageFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 59
    const-string v1, "layoutKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/docs/common/welcome/WelcomePageFragment;->a:I

    .line 60
    const-string v1, "positionKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomePageFragment;->b:I

    .line 61
    return-void
.end method
