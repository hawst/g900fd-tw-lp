.class public Lcom/google/android/apps/docs/capture/OcrCameraActivity;
.super LwN;
.source "OcrCameraActivity.java"


# instance fields
.field private a:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, LwN;-><init>()V

    return-void
.end method

.method public static b(Landroid/content/Context;LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 116
    const-class v0, Lcom/google/android/apps/docs/capture/OcrCameraActivity;

    invoke-static {v0, p0, p1, p2}, Lcom/google/android/apps/docs/capture/OcrCameraActivity;->a(Ljava/lang/Class;Landroid/content/Context;LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 87
    sget v0, LpR;->camera_ocr_default_name:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/capture/OcrCameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 88
    new-array v1, v5, [Ljava/lang/Object;

    const/4 v2, 0x0

    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyyMMdd-HHmm"

    invoke-direct {v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    .line 89
    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 88
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 91
    new-instance v1, Labb;

    invoke-direct {v1, p0}, Labb;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/apps/docs/capture/OcrCameraActivity;->a:Landroid/net/Uri;

    const-string v3, "image/jpeg"

    .line 93
    invoke-virtual {v1, v2, v3}, Labb;->a(Landroid/net/Uri;Ljava/lang/String;)Labb;

    move-result-object v1

    .line 94
    invoke-virtual {v1, v0}, Labb;->a(Ljava/lang/String;)Labb;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/capture/OcrCameraActivity;->a:LaFO;

    .line 95
    invoke-virtual {v0, v1}, Labb;->a(LaFO;)Labb;

    move-result-object v0

    .line 96
    invoke-virtual {v0, v5}, Labb;->a(Z)Labb;

    move-result-object v0

    .line 97
    invoke-virtual {v0, v5}, Labb;->b(Z)Labb;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/capture/OcrCameraActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 98
    invoke-virtual {v0, v1}, Labb;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Labb;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Labb;->a()Landroid/content/Intent;

    move-result-object v0

    .line 100
    iget-object v1, p0, Lcom/google/android/apps/docs/capture/OcrCameraActivity;->a:LqK;

    const-string v2, "upload"

    const-string v3, "evaluateForOcr"

    const-string v4, "readyForUpload"

    invoke-virtual {v1, v2, v3, v4}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    return-object v0
.end method

.method protected a(Landroid/content/Intent;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 51
    if-eqz p2, :cond_1

    .line 52
    const-string v0, "keySaveImageUri"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 53
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LbiT;->a(Z)V

    .line 54
    iput-object v0, p0, Lcom/google/android/apps/docs/capture/OcrCameraActivity;->a:Landroid/net/Uri;

    .line 76
    :goto_1
    const/4 v0, 0x0

    :goto_2
    return-object v0

    .line 53
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 55
    :cond_1
    const-string v0, "android.intent.action.SEND"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 57
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    .line 58
    const-string v1, "title"

    sget v2, LpR;->ocr_image_title:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/capture/OcrCameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v1, "description"

    sget v2, LpR;->ocr_image_description:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/capture/OcrCameraActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const-string v1, "mime_type"

    const-string v2, "image/jpeg"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    invoke-virtual {p0}, Lcom/google/android/apps/docs/capture/OcrCameraActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/capture/OcrCameraActivity;->a:Landroid/net/Uri;

    .line 62
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 63
    const-string v1, "output"

    iget-object v2, p0, Lcom/google/android/apps/docs/capture/OcrCameraActivity;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 64
    iget-object v1, p0, Lcom/google/android/apps/docs/capture/OcrCameraActivity;->a:LqK;

    const-string v2, "upload"

    const-string v3, "evaluateForOcr"

    const-string v4, "initiated"

    invoke-virtual {v1, v2, v3, v4}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_2

    .line 68
    :catch_0
    move-exception v0

    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/docs/capture/OcrCameraActivity;->f()V

    goto :goto_1

    .line 70
    :catch_1
    move-exception v0

    .line 71
    invoke-virtual {p0}, Lcom/google/android/apps/docs/capture/OcrCameraActivity;->f()V

    goto :goto_1

    .line 74
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/docs/capture/OcrCameraActivity;->finish()V

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 81
    invoke-super {p0, p1}, LwN;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 82
    const-string v0, "keySaveImageUri"

    iget-object v1, p0, Lcom/google/android/apps/docs/capture/OcrCameraActivity;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 83
    return-void
.end method
