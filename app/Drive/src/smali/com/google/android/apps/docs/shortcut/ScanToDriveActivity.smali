.class public Lcom/google/android/apps/docs/shortcut/ScanToDriveActivity;
.super Lrm;
.source "ScanToDriveActivity.java"


# instance fields
.field public a:LaGM;

.field public a:LaIm;

.field public a:Lald;

.field public a:Lub;
    .annotation runtime Lbwm;
        value = "scanIntentProvider"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lrm;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 71
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/shortcut/ScanToDriveActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 72
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 77
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 79
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 80
    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 41
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 42
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shortcut/ScanToDriveActivity;->finish()V

    .line 43
    const-string v0, "ScanToDriveActivity"

    const-string v1, "in onCreate"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shortcut/ScanToDriveActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 46
    if-nez v0, :cond_0

    .line 47
    const-string v0, "ScanToDriveActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Intent.EXTRA_STREAM Uri is missing. intent="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/shortcut/ScanToDriveActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    iget-object v0, p0, Lcom/google/android/apps/docs/shortcut/ScanToDriveActivity;->a:Lald;

    sget v1, LatO;->error_internal_error_html:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/shortcut/ScanToDriveActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Lald;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 63
    :goto_0
    return-void

    .line 52
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/docs/shortcut/ScanToDriveActivity;->a:LaIm;

    invoke-interface {v1, v0}, LaIm;->a(Landroid/net/Uri;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    .line 53
    if-nez v1, :cond_1

    .line 54
    const-string v1, "ScanToDriveActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Intent.EXTRA_STREAM Uri failed to convert to EntrySpec. uri="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/docs/shortcut/ScanToDriveActivity;->a:Lald;

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shortcut/ScanToDriveActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, LatO;->error_document_not_available:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 55
    invoke-interface {v0, v1, v4}, Lald;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 60
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/shortcut/ScanToDriveActivity;->a:Lub;

    invoke-interface {v0, p0, v1}, Lub;->a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    .line 61
    const/high16 v1, 0x2000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 62
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/shortcut/ScanToDriveActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
