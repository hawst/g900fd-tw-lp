.class public Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;
.super Landroid/webkit/WebView;
.source "ScrollRestrainedWebView.java"


# instance fields
.field private a:I

.field private final a:LZF;

.field private final a:Landroid/os/Handler;

.field private a:Z

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LZF;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 58
    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 39
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a:Landroid/os/Handler;

    .line 41
    iput-boolean v1, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a:Z

    .line 43
    iput v1, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a:I

    .line 44
    iput v1, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->b:I

    .line 45
    iput v1, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->c:I

    .line 46
    iput v1, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->d:I

    .line 48
    iput v2, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->e:I

    .line 49
    iput v2, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->f:I

    .line 50
    iput v2, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->g:I

    .line 59
    iput-object p2, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a:LZF;

    .line 60
    return-void
.end method

.method private a(FI)I
    .locals 6

    .prologue
    .line 132
    iget v3, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a:I

    iget v4, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->b:I

    iget v5, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->f:I

    move-object v0, p0

    move v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a(FIIII)I

    move-result v0

    return v0
.end method

.method private a(FIIII)I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 108
    int-to-float v1, p3

    mul-float/2addr v1, p1

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    .line 109
    int-to-float v2, p4

    mul-float/2addr v2, p1

    int-to-float v3, p5

    sub-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    .line 111
    if-ge v2, v1, :cond_2

    .line 112
    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 113
    if-ne p2, v1, :cond_1

    .line 128
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 117
    goto :goto_0

    .line 120
    :cond_2
    if-ge p2, v1, :cond_3

    move v0, v1

    .line 121
    goto :goto_0

    .line 124
    :cond_3
    if-le p2, v2, :cond_0

    move v0, v2

    .line 125
    goto :goto_0
.end method

.method private a()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 63
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a:LZF;

    invoke-interface {v2}, LZF;->a()I

    move-result v3

    .line 64
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->getWidth()I

    move-result v4

    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->getHeight()I

    move-result v5

    .line 67
    iget v2, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->e:I

    if-ne v2, v3, :cond_1

    iget v2, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->f:I

    if-ne v2, v4, :cond_1

    iget v2, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->g:I

    if-ne v2, v5, :cond_1

    move v2, v0

    .line 72
    :goto_0
    if-nez v2, :cond_0

    const/4 v2, -0x1

    if-ne v3, v2, :cond_2

    .line 101
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v2, v1

    .line 67
    goto :goto_0

    .line 76
    :cond_2
    iput v3, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->e:I

    .line 77
    iput v4, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->f:I

    .line 78
    iput v5, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->g:I

    .line 82
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a:LZF;

    .line 83
    invoke-interface {v2}, LZF;->b()I

    move-result v2

    mul-int/2addr v2, v4

    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a:LZF;

    invoke-interface {v3}, LZF;->a()I

    move-result v3

    mul-int/2addr v3, v5

    if-le v2, v3, :cond_3

    .line 84
    :goto_2
    if-eqz v0, :cond_4

    .line 85
    iput v1, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->c:I

    .line 86
    iput v5, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->d:I

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a:LZF;

    .line 89
    invoke-interface {v0}, LZF;->a()I

    move-result v0

    mul-int/2addr v0, v5

    int-to-double v0, v0

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a:LZF;

    invoke-interface {v2}, LZF;->b()I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v0, v2

    double-to-int v0, v0

    .line 90
    sub-int v1, v4, v0

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a:I

    .line 91
    iget v1, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->b:I

    goto :goto_1

    :cond_3
    move v0, v1

    .line 83
    goto :goto_2

    .line 93
    :cond_4
    iput v1, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a:I

    .line 94
    iput v4, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->b:I

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a:LZF;

    .line 97
    invoke-interface {v0}, LZF;->b()I

    move-result v0

    mul-int/2addr v0, v4

    int-to-double v0, v0

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a:LZF;

    invoke-interface {v2}, LZF;->a()I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v0, v2

    double-to-int v0, v0

    .line 98
    sub-int v1, v5, v0

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->c:I

    .line 99
    iget v1, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->c:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->d:I

    goto :goto_1
.end method

.method private a(FII)V
    .locals 2

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a:Landroid/os/Handler;

    new-instance v1, LZE;

    invoke-direct {v1, p0, p1, p2, p3}, LZE;-><init>(Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;FII)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 204
    return-void
.end method

.method private b(FI)I
    .locals 6

    .prologue
    .line 136
    iget v3, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->c:I

    iget v4, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->d:I

    iget v5, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->g:I

    move-object v0, p0

    move v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a(FIIII)I

    move-result v0

    return v0
.end method


# virtual methods
.method public invalidate()V
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a:Z

    if-eqz v0, :cond_0

    .line 142
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a:Z

    .line 147
    :goto_0
    return-void

    .line 146
    :cond_0
    invoke-super {p0}, Landroid/webkit/WebView;->invalidate()V

    goto :goto_0
.end method

.method protected onScrollChanged(IIII)V
    .locals 6

    .prologue
    const/4 v2, -0x1

    .line 208
    invoke-static {}, LakQ;->c()Z

    move-result v0

    if-nez v0, :cond_3

    .line 209
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a()V

    .line 210
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->getScale()F

    move-result v3

    .line 213
    float-to-double v0, v3

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v0, v4

    if-eqz v0, :cond_4

    .line 214
    invoke-direct {p0, v3, p1}, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a(FI)I

    move-result v1

    .line 215
    invoke-direct {p0, v3, p2}, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->b(FI)I

    move-result v0

    .line 218
    :goto_0
    if-gt v1, v2, :cond_0

    if-le v0, v2, :cond_3

    .line 219
    :cond_0
    if-ne v1, v2, :cond_1

    move v1, p1

    .line 220
    :cond_1
    if-ne v0, v2, :cond_2

    move v0, p2

    .line 221
    :cond_2
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->scrollTo(II)V

    .line 222
    invoke-direct {p0, v3, v1, v0}, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a(FII)V

    .line 226
    :cond_3
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebView;->onScrollChanged(IIII)V

    .line 227
    return-void

    :cond_4
    move v0, v2

    move v1, v2

    goto :goto_0
.end method

.method protected overScrollBy(IIIIIIIIZ)Z
    .locals 10

    .prologue
    .line 159
    invoke-static {}, LakQ;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    invoke-super/range {p0 .. p9}, Landroid/webkit/WebView;->overScrollBy(IIIIIIIIZ)Z

    move-result v0

    .line 192
    :goto_0
    return v0

    .line 164
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a()V

    .line 165
    add-int v2, p3, p1

    .line 166
    add-int v0, p4, p2

    .line 167
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->getScale()F

    move-result v4

    .line 168
    const/4 v3, -0x1

    .line 169
    const/4 v1, -0x1

    .line 170
    float-to-double v6, v4

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    cmpl-double v5, v6, v8

    if-eqz v5, :cond_1

    .line 171
    invoke-direct {p0, v4, v2}, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a(FI)I

    move-result v3

    .line 172
    invoke-direct {p0, v4, v0}, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->b(FI)I

    move-result v1

    .line 175
    :cond_1
    const/4 v5, -0x1

    if-ne v3, v5, :cond_2

    const/4 v5, -0x1

    if-ne v1, v5, :cond_2

    .line 177
    invoke-super/range {p0 .. p9}, Landroid/webkit/WebView;->overScrollBy(IIIIIIIIZ)Z

    move-result v0

    goto :goto_0

    .line 188
    :cond_2
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a:Z

    .line 189
    const/4 v5, -0x1

    if-ne v3, v5, :cond_3

    .line 190
    :goto_1
    const/4 v3, -0x1

    if-ne v1, v3, :cond_4

    .line 191
    :goto_2
    invoke-direct {p0, v4, v2, v0}, Lcom/google/android/apps/docs/punchwebview/ScrollRestrainedWebView;->a(FII)V

    .line 192
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    move v2, v3

    .line 189
    goto :goto_1

    :cond_4
    move v0, v1

    .line 190
    goto :goto_2
.end method
