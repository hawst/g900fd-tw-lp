.class public Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;
.super Lcom/google/android/apps/docs/fragment/WebViewFragment;
.source "PunchWebViewFragment.java"

# interfaces
.implements LXR;
.implements LZC;


# instance fields
.field a:I

.field private a:J

.field private final a:LGH;

.field private a:LGI;

.field public a:LQr;

.field public a:LTO;

.field public a:LTd;

.field public a:LUT;

.field private a:LUe;

.field private a:LXA;

.field public a:LXD;

.field private a:LXE;

.field public a:LXq;

.field public a:LYO;

.field private a:LYP;

.field public a:LYc;

.field public a:LYs;

.field private a:LZj;

.field public a:LZm;

.field private a:LZx;

.field public a:LaKR;

.field public a:Lald;

.field public a:Lalo;

.field private final a:Landroid/webkit/WebChromeClient;

.field public a:Landroid/widget/FrameLayout;

.field private a:Landroid/widget/ProgressBar;

.field private a:Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;

.field private a:Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;

.field private a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

.field public a:Ljava/lang/Class;
    .annotation runtime Lbxv;
        a = "DocListActivity"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LXB;",
            ">;"
        }
    .end annotation
.end field

.field public a:LqK;

.field public a:LtK;

.field public a:Lye;

.field private a:LzW;

.field private a:Z

.field private b:J

.field public b:LQr;

.field private b:Landroid/view/ViewGroup;

.field private b:Landroid/webkit/WebView;

.field private b:Z

.field private c:Ljava/lang/String;

.field private h:Ljava/lang/Object;

.field private i:Ljava/lang/Object;

.field private j:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 95
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/WebViewFragment;-><init>()V

    .line 558
    new-instance v0, LZc;

    invoke-direct {v0, p0}, LZc;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LGH;

    .line 664
    new-instance v0, LZe;

    invoke-direct {v0, p0}, LZe;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebChromeClient;

    .line 733
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Z

    .line 770
    iput-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LGI;

    .line 779
    iput-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->h:Ljava/lang/Object;

    .line 782
    iput-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->i:Ljava/lang/Object;

    .line 785
    iput-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->j:Ljava/lang/Object;

    .line 791
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:I

    .line 799
    invoke-static {}, LboS;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Ljava/util/Map;

    return-void
.end method

.method private A()V
    .locals 2

    .prologue
    .line 1020
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 1021
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LYO;

    invoke-interface {v0}, LYO;->a()LYR;

    move-result-object v0

    .line 1022
    invoke-virtual {v0}, LYR;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1023
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LYc;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(LYs;)V

    .line 1028
    :cond_0
    :goto_0
    return-void

    .line 1025
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXq;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(LYs;)V

    goto :goto_0
.end method

.method private B()V
    .locals 2

    .prologue
    .line 1057
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LYP;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 1058
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LYO;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LYP;

    invoke-interface {v0, v1}, LYO;->b(LYP;)V

    .line 1059
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LYP;

    .line 1060
    return-void

    .line 1057
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private C()V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1063
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LYO;

    invoke-interface {v2}, LYO;->a()LYK;

    move-result-object v2

    invoke-virtual {v2}, LYK;->a()Z

    move-result v2

    .line 1064
    const-string v3, "PunchWebViewFragment"

    const-string v4, "passTouchEventsToWebView = %s"

    new-array v5, v0, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v3, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1065
    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LUe;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "window.punchWebViewEventListener.setInterceptClicks("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-nez v2, :cond_0

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ");"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, LUe;->a(Ljava/lang/String;)V

    .line 1067
    return-void

    :cond_0
    move v0, v1

    .line 1065
    goto :goto_0
.end method

.method private D()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1070
    invoke-static {}, LakQ;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1072
    :goto_0
    const-string v2, "PunchWebViewFragment"

    const-string v3, "fixTargetDensity = %s"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1073
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LUe;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "window.punchWebViewEventListener.setFixTargetDensity("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ");"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, LUe;->a(Ljava/lang/String;)V

    .line 1074
    return-void

    .line 1070
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQr;

    const-string v2, "punchFixTargetDensity"

    .line 1071
    invoke-interface {v0, v2, v4}, LQr;->a(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method private E()V
    .locals 4

    .prologue
    .line 1154
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in loadJavascriptBridge"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1157
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()LH;

    move-result-object v0

    .line 1158
    invoke-virtual {v0}, LH;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "javascript/punch/webview/PunchNativeMessagingBridge.js"

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 1159
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lalo;

    invoke-interface {v1, v0}, Lalo;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 1160
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1165
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LUe;

    invoke-interface {v0, v1}, LUe;->a(Ljava/lang/String;)V

    .line 1166
    return-void

    .line 1161
    :catch_0
    move-exception v0

    .line 1162
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to load Punch javascript bridge: ."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method private F()V
    .locals 3

    .prologue
    .line 1307
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()Landroid/os/Bundle;

    move-result-object v1

    .line 1308
    invoke-static {v1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1309
    const-string v0, "url"

    invoke-static {v1, v0}, Lakt;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->c:Ljava/lang/String;

    .line 1310
    const-string v0, "resourceSpec"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/ResourceSpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    .line 1311
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1312
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXD;

    const-string v2, "title"

    invoke-static {v1, v2}, Lakt;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LXD;->d(Ljava/lang/String;)V

    .line 1313
    return-void
.end method

.method private G()V
    .locals 2

    .prologue
    .line 1316
    new-instance v0, LZi;

    invoke-direct {v0, p0}, LZi;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXE;

    .line 1352
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXD;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXE;

    invoke-virtual {v0, v1}, LXD;->a(LXE;)V

    .line 1353
    return-void
.end method

.method private H()V
    .locals 1

    .prologue
    .line 1356
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 1357
    invoke-static {v0}, LZj;->a(I)LZj;

    move-result-object v0

    .line 1359
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(LZj;)V

    .line 1360
    return-void
.end method

.method private I()V
    .locals 2

    .prologue
    .line 1391
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->h:Ljava/lang/Object;

    .line 1392
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LqK;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->h:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LqK;->a(Ljava/lang/Object;)V

    .line 1393
    return-void
.end method

.method private J()V
    .locals 3

    .prologue
    .line 1396
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->h:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 1397
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LqK;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->h:Ljava/lang/Object;

    const-string v2, "webViewPunchApiLoadedDuration"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1399
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->h:Ljava/lang/Object;

    .line 1401
    :cond_0
    return-void
.end method

.method private K()V
    .locals 2

    .prologue
    .line 1404
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->h:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 1406
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LqK;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->h:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LqK;->b(Ljava/lang/Object;)V

    .line 1407
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->h:Ljava/lang/Object;

    .line 1409
    :cond_0
    return-void
.end method

.method private L()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 1413
    invoke-static {}, LakQ;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1414
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LYO;

    invoke-interface {v0}, LYO;->c()Z

    move-result v0

    .line 1415
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/webkit/WebView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1418
    :cond_0
    return-void

    .line 1415
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(LXB;)I
    .locals 3

    .prologue
    .line 1244
    iget v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:I

    .line 1245
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1246
    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)J
    .locals 2

    .prologue
    .line 95
    iget-wide v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:J

    return-wide v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)LGI;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LGI;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method public static a(Landroid/net/Uri;Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;)Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;
    .locals 3

    .prologue
    .line 163
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in createFragment"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 165
    const-string v1, "url"

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const-string v1, "resourceSpec"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 167
    const-string v1, "title"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    new-instance v1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    invoke-direct {v1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;-><init>()V

    .line 169
    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->e(Landroid/os/Bundle;)V

    .line 170
    return-object v1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Ljava/util/Map;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)LzW;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LzW;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;LzW;)LzW;
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LzW;

    return-object p1
.end method

.method private a(ILXB;)V
    .locals 4

    .prologue
    .line 1223
    invoke-direct {p0, p2}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(LXB;)I

    move-result v0

    .line 1224
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LUe;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "window.punchWebViewEventListener.requestFullSlideContent("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ");"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, LUe;->a(Ljava/lang/String;)V

    .line 1226
    return-void
.end method

.method private a(LXB;)V
    .locals 4

    .prologue
    .line 1229
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(LXB;)I

    move-result v0

    .line 1230
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LUe;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "window.punchWebViewEventListener.requestCurrentStateContent("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ");"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, LUe;->a(Ljava/lang/String;)V

    .line 1231
    return-void
.end method

.method private a(LYs;)V
    .locals 6

    .prologue
    .line 1039
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LYs;->b(Z)V

    .line 1040
    invoke-virtual {p1}, LYs;->a()V

    .line 1041
    invoke-virtual {p1}, LYs;->a()LYu;

    move-result-object v0

    invoke-virtual {v0}, LYu;->a()I

    move-result v0

    .line 1042
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;

    .line 1043
    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 1045
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/os/Handler;

    new-instance v2, LZh;

    invoke-direct {v2, p0, v0, p1}, LZh;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;Landroid/widget/FrameLayout;LYs;)V

    const-wide/16 v4, 0x64

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1054
    return-void
.end method

.method private a(LZj;)V
    .locals 3

    .prologue
    .line 1363
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LZj;

    if-eqz v0, :cond_0

    .line 1364
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LqK;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->i:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LZj;

    invoke-static {v2}, LZj;->a(LZj;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1365
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->i:Ljava/lang/Object;

    .line 1368
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LZj;

    .line 1370
    if-eqz p1, :cond_1

    .line 1371
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->i:Ljava/lang/Object;

    .line 1372
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LqK;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LqK;->a(Ljava/lang/Object;)V

    .line 1374
    :cond_1
    return-void
.end method

.method private a(LZx;)V
    .locals 3

    .prologue
    .line 1377
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LZx;

    if-eqz v0, :cond_0

    .line 1378
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LqK;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->j:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LZx;

    invoke-static {v2}, LZx;->a(LZx;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1379
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->j:Ljava/lang/Object;

    .line 1382
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LZx;

    .line 1384
    if-eqz p1, :cond_1

    .line 1385
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->j:Ljava/lang/Object;

    .line 1386
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LqK;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->j:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LqK;->a(Ljava/lang/Object;)V

    .line 1388
    :cond_1
    return-void
.end method

.method static a(Landroid/view/View;LXR;)V
    .locals 1

    .prologue
    .line 201
    new-instance v0, LZa;

    invoke-direct {v0, p1}, LZa;-><init>(LXR;)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 215
    new-instance v0, LZb;

    invoke-direct {v0, p1}, LZb;-><init>(LXR;)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 223
    return-void
.end method

.method static a(Landroid/webkit/WebView;Landroid/content/res/Resources;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 175
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in commonWebViewConfiguration"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    invoke-virtual {p0, v2}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 177
    invoke-virtual {p0, v2}, Landroid/webkit/WebView;->setHorizontalScrollBarEnabled(Z)V

    .line 178
    sget v0, LpK;->punch_grey_background:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 179
    invoke-virtual {p0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 180
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 181
    sget-object v1, Landroid/webkit/WebSettings$PluginState;->ON:Landroid/webkit/WebSettings$PluginState;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setPluginState(Landroid/webkit/WebSettings$PluginState;)V

    .line 182
    invoke-virtual {v0, p3}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 183
    invoke-virtual {v0, p3}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 184
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 185
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSupportMultipleWindows(Z)V

    .line 186
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setJavaScriptCanOpenWindowsAutomatically(Z)V

    .line 187
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    .line 188
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    .line 189
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 190
    invoke-virtual {v0, p3}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 192
    invoke-static {}, LakQ;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 193
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    .line 196
    :cond_0
    invoke-virtual {v0, p2}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 197
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->J()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;ILXB;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(ILXB;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;LXB;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(LXB;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b(Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;)V
    .locals 2

    .prologue
    .line 973
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXA;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 974
    new-instance v0, LZf;

    invoke-direct {v0, p0, p1}, LZf;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXA;

    .line 985
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXD;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXA;

    invoke-virtual {v0, v1}, LXD;->a(LXA;)V

    .line 986
    return-void

    .line 973
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Z

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;Z)Z
    .locals 0

    .prologue
    .line 95
    iput-boolean p1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)J
    .locals 2

    .prologue
    .line 95
    iget-wide v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:J

    return-wide v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/os/Handler;

    return-object v0
.end method

.method private b(LXB;)V
    .locals 4

    .prologue
    .line 1234
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(LXB;)I

    move-result v0

    .line 1235
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LUe;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "window.punchWebViewEventListener.requestPreviousContent("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ");"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, LUe;->a(Ljava/lang/String;)V

    .line 1236
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->v()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;LXB;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b(LXB;)V

    return-void
.end method

.method private b(Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;)V
    .locals 2

    .prologue
    .line 1077
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXD;

    invoke-virtual {v0}, LXD;->a()I

    move-result v0

    .line 1078
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXD;

    invoke-virtual {v1}, LXD;->b()I

    move-result v1

    .line 1079
    if-lez v1, :cond_0

    .line 1080
    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 1081
    invoke-virtual {p1, v0}, Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;->setAspect(F)V

    .line 1083
    :cond_0
    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/os/Handler;

    return-object v0
.end method

.method private c(LXB;)V
    .locals 4

    .prologue
    .line 1239
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(LXB;)I

    move-result v0

    .line 1240
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LUe;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "window.punchWebViewEventListener.requestNextContent("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ");"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, LUe;->a(Ljava/lang/String;)V

    .line 1241
    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->u()V

    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;LXB;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->c(LXB;)V

    return-void
.end method

.method public static synthetic d(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->C()V

    return-void
.end method

.method public static synthetic e(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->L()V

    return-void
.end method

.method public static synthetic f(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->A()V

    return-void
.end method

.method public static synthetic g(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic h(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic i(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic j(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic k(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/os/Handler;

    return-object v0
.end method

.method private u()V
    .locals 6

    .prologue
    .line 808
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, LFU;->a(Landroid/os/Bundle;)Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    move-result-object v3

    .line 809
    sget v0, LpR;->punch_open_failed:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(I)Ljava/lang/String;

    move-result-object v4

    .line 810
    sget v0, LpR;->punch_open_failed_expanded:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(I)Ljava/lang/String;

    move-result-object v5

    .line 811
    new-instance v0, LES;

    .line 812
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()LM;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-direct/range {v0 .. v5}, LES;-><init>(LM;Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, LES;->a()V

    .line 813
    return-void
.end method

.method private v()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 822
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lye;

    sget-object v1, LaGv;->g:LaGv;

    invoke-virtual {v1}, LaGv;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lye;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LtK;

    sget-object v1, LNn;->c:LNn;

    .line 823
    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 843
    :cond_0
    :goto_0
    return-void

    .line 827
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()LH;

    move-result-object v0

    const-string v1, "PunchWebViewPreferences"

    invoke-virtual {v0, v1, v2}, LH;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 828
    const-string v1, "punchPromoShown"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 829
    if-nez v1, :cond_0

    .line 834
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 835
    const-string v1, "punchPromoShown"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 836
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 838
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()LH;

    move-result-object v0

    sget v1, LpR;->cross_app_promo_view_only_button_text:I

    invoke-virtual {v0, v1}, LH;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 839
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lye;

    sget-object v2, LaGv;->g:LaGv;

    .line 840
    invoke-virtual {v2}, LaGv;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lye;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 841
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lye;

    invoke-interface {v2, v1, v0}, Lye;->a(Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 842
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private w()V
    .locals 4

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 852
    invoke-static {}, LakQ;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 853
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    if-nez v0, :cond_1

    .line 863
    :cond_0
    :goto_0
    return-void

    .line 858
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LYO;

    invoke-interface {v0}, LYO;->a()LYR;

    move-result-object v0

    invoke-virtual {v0}, LYR;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 860
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LYO;

    invoke-interface {v3}, LYO;->a()LYR;

    move-result-object v3

    invoke-virtual {v3}, LYR;->b()Z

    move-result v3

    if-eqz v3, :cond_3

    :goto_2
    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 858
    goto :goto_1

    :cond_3
    move v1, v2

    .line 860
    goto :goto_2
.end method

.method private x()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 965
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;

    sget v1, LpN;->editor_state_view_state:I

    .line 966
    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 968
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LYc;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LYc;->b(Z)V

    .line 969
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LYc;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LYO;

    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LqK;

    invoke-virtual {v1, v0, v2, p0, v3}, LYc;->a(Landroid/view/ViewGroup;LYO;LXR;LqK;)V

    .line 970
    return-void
.end method

.method private y()V
    .locals 2

    .prologue
    .line 989
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXA;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 990
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXD;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXA;

    invoke-virtual {v0, v1}, LXD;->b(LXA;)V

    .line 991
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXA;

    .line 992
    return-void
.end method

.method private z()V
    .locals 2

    .prologue
    .line 995
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LYP;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 996
    new-instance v0, LZg;

    invoke-direct {v0, p0}, LZg;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LYP;

    .line 1012
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LYO;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LYP;

    invoke-interface {v0, v1}, LYO;->a(LYP;)V

    .line 1013
    return-void

    .line 995
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, -0x1

    .line 868
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in onCreateView savedInstanceState=%s"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p3, v2, v10

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 869
    sget v0, LpP;->punch_web_view_fragment:I

    invoke-virtual {p1, v0, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 871
    sget v0, LpN;->loading_spinner:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/widget/ProgressBar;

    .line 872
    sget v0, LpN;->punch_detachable_slide_view:I

    .line 873
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;

    .line 875
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->I()V

    .line 880
    sget v0, LpN;->punch_web_view_container:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Landroid/view/ViewGroup;

    .line 881
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    .line 882
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    if-nez v0, :cond_6

    .line 883
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:J

    .line 884
    iput-boolean v10, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Z

    .line 886
    new-instance v0, Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, v9}, Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;

    .line 887
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    .line 888
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v8, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 890
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Landroid/webkit/WebView;

    .line 892
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    .line 893
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LTO;

    invoke-interface {v3}, LTO;->c()Ljava/lang/String;

    move-result-object v3

    .line 892
    invoke-static {v0, v2, v3, v4}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(Landroid/webkit/WebView;Landroid/content/res/Resources;Ljava/lang/String;Z)V

    .line 894
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->L()V

    .line 895
    new-instance v0, LUf;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    invoke-direct {v0, v2}, LUf;-><init>(Landroid/webkit/WebView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LUe;

    .line 897
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebChromeClient;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 898
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->G()V

    .line 899
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->F()V

    .line 900
    new-instance v0, LZk;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LGH;

    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    iget-object v3, v3, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    invoke-direct {v0, p0, v2, v3}, LZk;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;LGH;LaFO;)V

    .line 902
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v2, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 903
    new-instance v2, LGG;

    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXD;

    invoke-virtual {v4}, LXD;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LUT;

    .line 904
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()LH;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->c:Ljava/lang/String;

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 903
    invoke-interface {v5, v6, v7}, LUT;->a(Landroid/content/Context;Landroid/net/Uri;)LVa;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, LGG;-><init>(Ljava/lang/String;Ljava/lang/String;LVa;)V

    iput-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LGI;

    .line 905
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    new-instance v3, LZn;

    invoke-direct {v3, p0, v9}, LZn;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;LZa;)V

    const-string v4, "WebViewApi"

    invoke-virtual {v2, v3, v4}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 906
    invoke-static {}, LakQ;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 908
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    const-string v3, ""

    const-string v4, "text/html"

    invoke-virtual {v2, v3, v4, v9}, Landroid/webkit/WebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 911
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->E()V

    .line 912
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, LZk;->a(Ljava/lang/String;)V

    .line 914
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;)V

    .line 915
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->z()V

    .line 916
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXD;

    invoke-virtual {v0}, LXD;->a()I

    move-result v0

    if-eq v0, v8, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXD;

    invoke-virtual {v0}, LXD;->b()I

    move-result v0

    if-eq v0, v8, :cond_1

    .line 917
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b(Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;)V

    .line 920
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->D()V

    .line 921
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->C()V

    .line 927
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXq;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LYs;

    .line 928
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;

    sget v2, LpN;->editor_state_view_full_slide:I

    .line 929
    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/widget/FrameLayout;

    .line 930
    invoke-static {}, LakQ;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 931
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXq;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LYO;

    iget-object v4, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LqK;

    invoke-virtual {v0, v2, v3, p0, v4}, LXq;->a(Landroid/view/ViewGroup;LYO;LXR;LqK;)V

    .line 933
    :cond_2
    invoke-static {}, LakQ;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LtK;

    sget-object v2, Lry;->ao:Lry;

    .line 934
    invoke-interface {v0, v2}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 935
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->x()V

    .line 937
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->A()V

    .line 939
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->w()V

    .line 944
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->setWebView(Landroid/webkit/WebView;)V

    .line 946
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Landroid/view/ViewGroup;

    invoke-static {v0, p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(Landroid/view/View;LXR;)V

    .line 948
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Z

    if-nez v0, :cond_4

    .line 949
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v10}, Landroid/webkit/WebView;->setFocusable(Z)V

    .line 950
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 953
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXD;

    invoke-virtual {v0}, LXD;->a()Z

    move-result v0

    if-nez v0, :cond_5

    .line 954
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 957
    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->H()V

    .line 958
    sget-object v0, LZx;->b:LZx;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(LZx;)V

    .line 960
    return-object v1

    .line 923
    :cond_6
    const-string v0, "PunchWebViewFragment"

    const-string v2, "Reusing saved webview"

    invoke-static {v0, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 924
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->G()V

    goto :goto_0
.end method

.method public a(I)V
    .locals 5

    .prologue
    .line 1173
    const-string v0, "PunchWebViewFragment"

    const-string v1, "requestGoToSlide %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1174
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXD;

    invoke-virtual {v0}, LXD;->c()I

    move-result v0

    if-nez v0, :cond_0

    .line 1181
    :goto_0
    return-void

    .line 1178
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXD;

    invoke-virtual {v0}, LXD;->c()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, LbiT;->b(II)I

    .line 1179
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:J

    .line 1180
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LUe;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "window.PUNCH_WEBVIEW_CONTROL_API.gotoSlide("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LUe;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Landroid/webkit/WebView;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1279
    const-string v0, "PunchWebViewFragment"

    const-string v2, "in restoreWebView"

    invoke-static {v0, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1286
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1287
    const-string v0, "PunchWebViewFragment"

    const-string v1, "early exit in restoreWebView"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1304
    :goto_0
    return-void

    .line 1291
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Z

    if-eqz v0, :cond_1

    .line 1292
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;

    if-ne v0, p1, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LbiT;->b(Z)V

    .line 1294
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()Landroid/view/View;

    move-result-object v0

    sget v2, LpN;->punch_web_view_container:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1295
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1296
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1299
    sget-object v0, LZx;->b:LZx;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(LZx;)V

    .line 1301
    :cond_1
    iput-boolean v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Z

    .line 1303
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->w()V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1292
    goto :goto_1
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 817
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/fragment/WebViewFragment;->a_(Landroid/os/Bundle;)V

    .line 818
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in onCreate savedInstanceState=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 819
    return-void
.end method

.method public b()Landroid/webkit/WebView;
    .locals 2

    .prologue
    .line 1251
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Z

    if-nez v0, :cond_0

    .line 1253
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()Landroid/view/View;

    move-result-object v0

    sget v1, LpN;->punch_web_view_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1254
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1255
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1256
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Z

    .line 1257
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;->setVisibility(I)V

    .line 1272
    sget-object v0, LZx;->a:LZx;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(LZx;)V

    .line 1274
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Lcom/google/android/apps/docs/view/ConfigurableAspectWebView;

    return-object v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 1203
    const-string v0, "PunchWebViewFragment"

    const-string v1, "animateToNextState"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1204
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXD;

    invoke-virtual {v0}, LXD;->c()I

    move-result v0

    if-nez v0, :cond_0

    .line 1210
    :goto_0
    return-void

    .line 1208
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:J

    .line 1209
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LUe;

    const-string v1, "window.PUNCH_WEBVIEW_CONTROL_API.goNext();"

    invoke-interface {v0, v1}, LUe;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 1214
    const-string v0, "PunchWebViewFragment"

    const-string v1, "animateToPreviousState"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1215
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXD;

    invoke-virtual {v0}, LXD;->c()I

    move-result v0

    if-nez v0, :cond_0

    .line 1220
    :goto_0
    return-void

    .line 1218
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:J

    .line 1219
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LUe;

    const-string v1, "window.PUNCH_WEBVIEW_CONTROL_API.goPrev();"

    invoke-interface {v0, v1}, LUe;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1087
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in onDestroyView"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1088
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->K()V

    .line 1089
    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(LZj;)V

    .line 1090
    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(LZx;)V

    .line 1094
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXB;

    .line 1095
    invoke-interface {v0}, LXB;->a()V

    goto :goto_0

    .line 1097
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1098
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in onDestroyView"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1099
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXD;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXE;

    invoke-virtual {v0, v1}, LXD;->b(LXE;)V

    .line 1103
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()Landroid/view/View;

    move-result-object v0

    sget v1, LpN;->punch_web_view_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1104
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1105
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1107
    iput-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    .line 1108
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXq;

    invoke-virtual {v0}, LXq;->b()V

    .line 1109
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LYc;

    invoke-virtual {v0}, LYc;->b()V

    .line 1111
    invoke-super {p0}, Lcom/google/android/apps/docs/fragment/WebViewFragment;->e()V

    .line 1112
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 847
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in onDestroy"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 848
    invoke-super {p0}, Lcom/google/android/apps/docs/fragment/WebViewFragment;->h()V

    .line 849
    return-void
.end method

.method public l_()V
    .locals 2

    .prologue
    .line 1185
    const-string v0, "PunchWebViewFragment"

    const-string v1, "requestSkipToNextSlide"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1186
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXD;

    invoke-virtual {v0}, LXD;->d()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 1187
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXD;

    invoke-virtual {v1}, LXD;->c()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1188
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(I)V

    .line 1190
    :cond_0
    return-void
.end method

.method public m_()V
    .locals 2

    .prologue
    .line 1194
    const-string v0, "PunchWebViewFragment"

    const-string v1, "requestSkipToPreviousSlide"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1195
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LXD;

    invoke-virtual {v0}, LXD;->d()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 1196
    if-ltz v0, :cond_0

    .line 1197
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(I)V

    .line 1199
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 1116
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/fragment/WebViewFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1118
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->H()V

    .line 1119
    return-void
.end method

.method public t()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1122
    const-string v0, "PunchWebViewFragment"

    const-string v1, "in cleanup"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1123
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Landroid/webkit/WebView;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1124
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    .line 1125
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LzW;

    if-eqz v0, :cond_0

    .line 1126
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LzW;

    invoke-virtual {v0}, LzW;->a()V

    .line 1127
    iput-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LzW;

    .line 1129
    :cond_0
    iput-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LGI;

    .line 1131
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Landroid/webkit/WebView;

    if-eqz v0, :cond_1

    .line 1132
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 1133
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 1136
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->B()V

    .line 1137
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->y()V

    .line 1141
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_2

    .line 1142
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Landroid/webkit/WebView;

    const-string v1, "WebViewApi"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->removeJavascriptInterface(Ljava/lang/String;)V

    .line 1145
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a()V

    .line 1146
    return-void
.end method
