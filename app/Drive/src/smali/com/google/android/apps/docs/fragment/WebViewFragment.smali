.class public Lcom/google/android/apps/docs/fragment/WebViewFragment;
.super Lcom/google/android/apps/docs/app/BaseFragment;
.source "WebViewFragment.java"


# instance fields
.field private a:LSe;

.field public final a:Landroid/os/Handler;

.field public a:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseFragment;-><init>()V

    .line 94
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/WebViewFragment;->a:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/WebViewFragment;->a:LSe;

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/WebViewFragment;->a:LSe;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/fragment/WebViewFragment;->a(Landroid/content/Context;)Landroid/webkit/WebView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/WebViewFragment;->a:Landroid/webkit/WebView;

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/WebViewFragment;->a:Landroid/webkit/WebView;

    return-object v0
.end method

.method public a()Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/WebViewFragment;->a:Landroid/webkit/WebView;

    return-object v0
.end method

.method protected a(Landroid/content/Context;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 102
    new-instance v0, Landroid/webkit/WebView;

    invoke-direct {v0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public a()V
    .locals 6

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/WebViewFragment;->a:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/WebViewFragment;->a:Landroid/webkit/WebView;

    .line 151
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/docs/fragment/WebViewFragment;->a:Landroid/webkit/WebView;

    .line 152
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/WebViewFragment;->a:LSe;

    invoke-virtual {v1}, LSe;->a()V

    .line 155
    new-instance v1, LSd;

    invoke-direct {v1, p0, v0}, LSd;-><init>(Lcom/google/android/apps/docs/fragment/WebViewFragment;Landroid/webkit/WebView;)V

    .line 161
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/view/ViewConfiguration;->getZoomControlsTimeout()J

    move-result-wide v4

    add-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    add-long/2addr v2, v4

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/WebViewFragment;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z

    .line 164
    :cond_0
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 107
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseFragment;->a(Landroid/app/Activity;)V

    .line 108
    new-instance v0, LSe;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/WebViewFragment;->a()LH;

    move-result-object v1

    invoke-direct {v0, v1}, LSe;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/WebViewFragment;->a:LSe;

    .line 109
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/WebViewFragment;->a()V

    .line 145
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseFragment;->e()V

    .line 146
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 124
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseFragment;->g()V

    .line 126
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/WebViewFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->onPause()V

    .line 129
    :cond_0
    return-void
.end method

.method public o_()V
    .locals 2

    .prologue
    .line 134
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/WebViewFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->onResume()V

    .line 137
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseFragment;->o_()V

    .line 138
    return-void
.end method
