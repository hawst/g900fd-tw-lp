.class public Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;
.super Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;
.source "SpeakerNotesFragment.java"

# interfaces
.implements LZJ;
.implements LZM;


# instance fields
.field private a:Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;

.field private a:Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;-><init>()V

    return-void
.end method

.method public static a(II)Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 39
    new-instance v0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;-><init>()V

    .line 41
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 42
    const/4 v2, 0x2

    new-array v2, v2, [I

    aput p0, v2, v4

    const/4 v3, 0x1

    aput p1, v2, v3

    invoke-static {v1, v2, v4}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;->a(Landroid/os/Bundle;[II)V

    .line 43
    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;->e(Landroid/os/Bundle;)V

    .line 45
    return-object v0
.end method

.method private u()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;->a:Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;->a:Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;->a:Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->setListener(LZM;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;->a:Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->setListener(LZJ;)V

    .line 75
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 53
    invoke-super {p0}, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a()V

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;->a:[Landroid/view/View;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 55
    instance-of v1, v0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;

    invoke-static {v1}, LbiT;->b(Z)V

    .line 56
    check-cast v0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;->a:Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;->a:Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->setListener(LZM;)V

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;->a:[Landroid/view/View;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    .line 59
    instance-of v1, v0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;

    invoke-static {v1}, LbiT;->b(Z)V

    .line 60
    check-cast v0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;->a:Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;->a:Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->setListener(LZJ;)V

    .line 62
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;->a(I)V

    .line 85
    return-void
.end method

.method public j_()V
    .locals 0

    .prologue
    .line 66
    invoke-super {p0}, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->j_()V

    .line 67
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;->u()V

    .line 68
    return-void
.end method

.method public t()V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;->a(I)V

    .line 80
    return-void
.end method
