.class public Lcom/google/android/apps/docs/welcome/WelcomeActivity;
.super LpH;
.source "WelcomeActivity.java"

# interfaces
.implements LasI;


# instance fields
.field a:Lcom/google/android/apps/docs/welcome/RedeemVoucherController;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, LpH;-><init>()V

    .line 71
    return-void
.end method

.method public static a(Landroid/content/Context;LasJ;)Landroid/content/Intent;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 78
    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->a(Landroid/content/Context;LasJ;Landroid/content/Intent;Ljava/lang/String;LqH;Ljava/lang/String;LqH;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;LasJ;Landroid/content/Intent;Ljava/lang/String;LqH;Ljava/lang/String;LqH;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 89
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.docs.WELCOME"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 90
    const-class v1, Lcom/google/android/apps/docs/welcome/WelcomeActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 91
    const-string v1, "story"

    invoke-virtual {p1}, LasJ;->a()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 92
    const-string v1, "positiveButtonText"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    const-string v1, "positiveButtonIntent"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 94
    if-eqz p4, :cond_0

    .line 95
    const-string v1, "positiveButtonRocketTrackerEvent"

    .line 96
    invoke-virtual {p4}, LqH;->name()Ljava/lang/String;

    move-result-object v2

    .line 95
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 98
    :cond_0
    const-string v1, "closeButtonText"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 99
    if-eqz p6, :cond_1

    .line 100
    const-string v1, "closeButtonRocketTrackerEvent"

    .line 101
    invoke-virtual {p6}, LqH;->name()Ljava/lang/String;

    move-result-object v2

    .line 100
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 103
    :cond_1
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 104
    return-object v0
.end method

.method public static a(Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 108
    const-string v0, "closeButtonText"

    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 109
    return-object p0
.end method

.method static a(Landroid/content/Intent;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 122
    const-string v0, "story"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 224
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->a()LM;

    move-result-object v0

    invoke-virtual {v0, p1}, LM;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method private a()Latc;
    .locals 6

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 176
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, LakQ;->c(Landroid/content/res/Resources;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 178
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 180
    const/high16 v2, 0x44070000    # 540.0f

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v2

    float-to-double v2, v0

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    add-double/2addr v2, v4

    double-to-int v0, v2

    .line 181
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    .line 182
    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v4

    add-int/2addr v3, v4

    .line 183
    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    add-int/2addr v0, v4

    invoke-virtual {v2}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    add-int/2addr v0, v2

    .line 184
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {v2, v3, v0}, Landroid/view/Window;->setLayout(II)V

    .line 188
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x10100

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 190
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 191
    sget-object v0, Latc;->c:Latc;

    .line 195
    :goto_0
    return-object v0

    .line 194
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->setRequestedOrientation(I)V

    .line 195
    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    const/16 v1, 0x168

    if-lt v0, v1, :cond_1

    sget-object v0, Latc;->b:Latc;

    goto :goto_0

    :cond_1
    sget-object v0, Latc;->a:Latc;

    goto :goto_0
.end method

.method public static b(Landroid/content/Intent;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 113
    const-string v0, "positiveButtonText"

    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    const-string v0, "isPhotoBackupAnnouncement"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 115
    return-object p0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 155
    const-string v1, "RedeemVoucherController"

    .line 156
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;

    iput-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->a:Lcom/google/android/apps/docs/welcome/RedeemVoucherController;

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->a:Lcom/google/android/apps/docs/welcome/RedeemVoucherController;

    if-nez v0, :cond_0

    .line 158
    new-instance v0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;

    invoke-direct {v0}, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->a:Lcom/google/android/apps/docs/welcome/RedeemVoucherController;

    .line 159
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->a()LM;

    move-result-object v0

    invoke-virtual {v0}, LM;->a()Lac;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->a:Lcom/google/android/apps/docs/welcome/RedeemVoucherController;

    .line 160
    invoke-virtual {v0, v2, v1}, Lac;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lac;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Lac;->a()I

    .line 163
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)LasG;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->a:Lcom/google/android/apps/docs/welcome/RedeemVoucherController;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a(Ljava/lang/String;)LasG;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 212
    const-class v0, LFq;

    if-ne p1, v0, :cond_0

    .line 214
    const-string v0, "RedeemVoucherController.PickAccountDialogFragment"

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 215
    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->a:Lcom/google/android/apps/docs/welcome/RedeemVoucherController;

    .line 219
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, LpH;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->a:Lcom/google/android/apps/docs/welcome/RedeemVoucherController;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->a()LM;

    move-result-object v0

    sget v1, Lxc;->welcome_fragment:I

    invoke-virtual {v0, v1}, LM;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 168
    if-eqz v0, :cond_0

    .line 169
    check-cast v0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a()V

    .line 171
    :cond_0
    invoke-super {p0}, LpH;->onBackPressed()V

    .line 172
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 127
    invoke-super {p0, p1}, LpH;->onCreate(Landroid/os/Bundle;)V

    .line 128
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 129
    sget v1, Lxc;->welcome_fragment:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setId(I)V

    .line 130
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->setContentView(Landroid/view/View;)V

    .line 131
    invoke-direct {p0}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->a()Latc;

    move-result-object v0

    .line 132
    if-nez p1, :cond_0

    .line 133
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->a(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v1

    .line 134
    const-string v2, "isPhotoBackupAnnouncement"

    .line 135
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "isPhotoBackupAnnouncement"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 134
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 136
    const-string v2, "screenType"

    invoke-virtual {v0}, Latc;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string v0, "positiveButtonIntent"

    .line 138
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "positiveButtonIntent"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    .line 137
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 139
    const-string v0, "positiveButtonText"

    .line 140
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "positiveButtonText"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 139
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const-string v0, "positiveButtonRocketTrackerEvent"

    .line 142
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "positiveButtonRocketTrackerEvent"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 141
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string v0, "closeButtonText"

    .line 144
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "closeButtonText"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 143
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const-string v0, "closeButtonRocketTrackerEvent"

    .line 146
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "closeButtonRocketTrackerEvent"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 145
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    invoke-static {v1}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a(Landroid/os/Bundle;)Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    move-result-object v0

    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->a()LM;

    move-result-object v1

    invoke-virtual {v1}, LM;->a()Lac;

    move-result-object v1

    sget v2, Lxc;->welcome_fragment:I

    invoke-virtual {v1, v2, v0}, Lac;->a(ILandroid/support/v4/app/Fragment;)Lac;

    move-result-object v0

    invoke-virtual {v0}, Lac;->a()I

    .line 151
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/welcome/WelcomeActivity;->e()V

    .line 152
    return-void
.end method
