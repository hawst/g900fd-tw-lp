.class public Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;
.super Ljava/lang/Object;
.source "SearchCriterion.java"

# interfaces
.implements Lcom/google/android/apps/docs/app/model/navigation/Criterion;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Laay;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    new-instance v0, Lws;

    invoke-direct {v0}, Lws;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Laay;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laay;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;->a:Laay;

    .line 25
    return-void
.end method


# virtual methods
.method public a(LaGM;)LaeZ;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;->a:Laay;

    invoke-virtual {v0}, Laay;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaeZ;->c(Ljava/lang/String;)LaeZ;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;->a:Laay;

    invoke-virtual {v0}, Laay;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(LvR;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LvR",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;->a:Laay;

    invoke-interface {p1, v0}, LvR;->a(Laay;)V

    .line 104
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 43
    if-ne p1, p0, :cond_0

    .line 44
    const/4 v0, 0x1

    .line 49
    :goto_0
    return v0

    .line 45
    :cond_0
    instance-of v0, p1, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;

    if-eqz v0, :cond_1

    .line 46
    check-cast p1, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;->a:Laay;

    iget-object v1, p1, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;->a:Laay;

    invoke-virtual {v0, v1}, Laay;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 49
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 55
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;->a:Laay;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 92
    const-string v0, "SearchCriterion {searchTerm=%s}"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;->a:Laay;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;->a:Laay;

    invoke-virtual {v0}, Laay;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/SearchCriterion;->a:Laay;

    invoke-virtual {v0}, Laay;->a()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 67
    return-void
.end method
