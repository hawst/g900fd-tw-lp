.class public Lcom/google/android/apps/docs/view/FixedAspectRatioFrameLayout;
.super Landroid/widget/FrameLayout;
.source "FixedAspectRatioFrameLayout.java"


# instance fields
.field private a:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 15
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/docs/view/FixedAspectRatioFrameLayout;->a:F

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/docs/view/FixedAspectRatioFrameLayout;->a:F

    .line 24
    sget-object v0, Lxk;->FixedAspectRatioFrameLayout:[I

    .line 25
    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 26
    sget v1, Lxk;->FixedAspectRatioFrameLayout_aspectRatio:I

    iget v2, p0, Lcom/google/android/apps/docs/view/FixedAspectRatioFrameLayout;->a:F

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/view/FixedAspectRatioFrameLayout;->setAspectRatio(F)V

    .line 28
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 15
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/docs/view/FixedAspectRatioFrameLayout;->a:F

    .line 34
    sget-object v0, Lxk;->FixedAspectRatioFrameLayout:[I

    .line 35
    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 36
    sget v1, Lxk;->FixedAspectRatioFrameLayout_aspectRatio:I

    iget v2, p0, Lcom/google/android/apps/docs/view/FixedAspectRatioFrameLayout;->a:F

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/view/FixedAspectRatioFrameLayout;->setAspectRatio(F)V

    .line 38
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 39
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 48
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 49
    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/docs/view/FixedAspectRatioFrameLayout;->a:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 50
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 51
    invoke-super {p0, p1, v0}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 52
    return-void
.end method

.method public setAspectRatio(F)V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 43
    iput p1, p0, Lcom/google/android/apps/docs/view/FixedAspectRatioFrameLayout;->a:F

    .line 44
    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
