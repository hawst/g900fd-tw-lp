.class public Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;
.super Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;
.source "DeleteForeverDialogFragment.java"


# instance fields
.field private a:LaGu;

.field private a:Lcom/google/android/gms/drive/database/data/EntrySpec;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;
    .locals 2

    .prologue
    .line 37
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 38
    const-string v1, "entrySpec"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 39
    new-instance v1, Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;-><init>()V

    .line 40
    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;->e(Landroid/os/Bundle;)V

    .line 41
    return-object v1
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;->a:LaGu;

    if-nez v0, :cond_0

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;->b()Landroid/app/Dialog;

    move-result-object v1

    .line 64
    :goto_0
    return-object v1

    .line 60
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v1

    .line 61
    sget v4, Lxi;->trash_delete_forever_warning:I

    .line 62
    sget v2, Lxi;->trash_delete_forever_question:I

    sget v3, Lxi;->trash_delete_forever_confirm:I

    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;->a:LaGu;

    .line 63
    invoke-interface {v0}, LaGu;->c()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    .line 62
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;->a(Landroid/app/Dialog;IIILjava/lang/String;)V

    goto :goto_0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/doclist/dialogs/AbstractDeleteOperationFragment;->a_(Landroid/os/Bundle;)V

    .line 48
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "entrySpec"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;->a:LaGM;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v1}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGu;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;->a:LaGu;

    .line 50
    return-void
.end method

.method protected t()V
    .locals 3

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;->a()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;->a(Landroid/app/Dialog;ILjava/lang/String;)V

    .line 71
    new-instance v0, LFm;

    invoke-direct {v0, p0}, LFm;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;)V

    .line 72
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;->a:LUi;

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v1, v2, v0}, LUi;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;LaHy;)V

    .line 73
    return-void
.end method

.method protected u()V
    .locals 3

    .prologue
    .line 77
    invoke-static {}, LamV;->a()V

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;->a:LaGM;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v1, v1, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-interface {v0, v1}, LaGM;->a(LaFO;)LaFM;

    move-result-object v0

    .line 79
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;->a:LaGM;

    invoke-interface {v1, v0}, LaGM;->b(LaFM;)V

    .line 80
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteForeverDialogFragment;->a()LH;

    move-result-object v0

    .line 81
    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    sget v1, Lxi;->trash_delete_forever_success:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 85
    :cond_0
    return-void
.end method
