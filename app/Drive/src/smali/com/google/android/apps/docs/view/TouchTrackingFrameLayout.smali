.class public Lcom/google/android/apps/docs/view/TouchTrackingFrameLayout;
.super Landroid/widget/FrameLayout;
.source "TouchTrackingFrameLayout.java"


# instance fields
.field public a:LaqT;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 25
    invoke-static {p1}, Lajt;->a(Landroid/content/Context;)Lbuu;

    move-result-object v0

    invoke-interface {v0, p0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    invoke-static {p1}, Lajt;->a(Landroid/content/Context;)Lbuu;

    move-result-object v0

    invoke-interface {v0, p0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    invoke-static {p1}, Lajt;->a(Landroid/content/Context;)Lbuu;

    move-result-object v0

    invoke-interface {v0, p0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 36
    return-void
.end method


# virtual methods
.method public dispatchDragEvent(Landroid/view/DragEvent;)Z
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/docs/view/TouchTrackingFrameLayout;->a:LaqT;

    invoke-virtual {v0, p1}, LaqT;->a(Landroid/view/DragEvent;)V

    .line 42
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDragEvent(Landroid/view/DragEvent;)Z

    move-result v0

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/docs/view/TouchTrackingFrameLayout;->a:LaqT;

    invoke-virtual {v0, p1}, LaqT;->a(Landroid/view/MotionEvent;)V

    .line 48
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
