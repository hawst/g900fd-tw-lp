.class public Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;
.super Lcom/google/android/apps/docs/utils/AbstractParcelableTask;
.source "DocumentFileCloseTaskImpl.java"

# interfaces
.implements Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl$DocumentFileCloseTask;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ladi;

.field private final a:Lcom/google/android/apps/docs/sync/filemanager/FileSpec;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Ladh;

    invoke-direct {v0}, Ladh;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/docs/sync/filemanager/FileSpec;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/docs/utils/AbstractParcelableTask;-><init>()V

    .line 40
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;->a:Lcom/google/android/apps/docs/sync/filemanager/FileSpec;

    .line 41
    return-void
.end method


# virtual methods
.method protected a(Laju;)V
    .locals 5

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;->a:Ladi;

    iget-object v1, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;->a:Lcom/google/android/apps/docs/sync/filemanager/FileSpec;

    invoke-interface {v0, v1}, Ladi;->a(Lcom/google/android/apps/docs/sync/filemanager/FileSpec;)Ladj;

    move-result-object v0

    .line 51
    if-eqz v0, :cond_0

    .line 52
    invoke-interface {v0}, Ladj;->close()V

    .line 56
    :goto_0
    return-void

    .line 54
    :cond_0
    const-string v0, "DocumentFileCloseTaskImpl"

    const-string v1, "Failed to find existing document file: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;->a:Lcom/google/android/apps/docs/sync/filemanager/FileSpec;

    iget-object v4, v4, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 60
    instance-of v0, p1, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;

    if-nez v0, :cond_0

    .line 61
    const/4 v0, 0x0

    .line 64
    :goto_0
    return v0

    .line 63
    :cond_0
    check-cast p1, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;->a:Lcom/google/android/apps/docs/sync/filemanager/FileSpec;

    iget-object v1, p1, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;->a:Lcom/google/android/apps/docs/sync/filemanager/FileSpec;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;->a:Lcom/google/android/apps/docs/sync/filemanager/FileSpec;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 74
    const-string v0, "DocumentFileCloseTask[%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;->a:Lcom/google/android/apps/docs/sync/filemanager/FileSpec;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileCloseTaskImpl;->a:Lcom/google/android/apps/docs/sync/filemanager/FileSpec;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 46
    return-void
.end method
