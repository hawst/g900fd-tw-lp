.class public Lcom/google/android/apps/docs/view/GestureImageView;
.super Landroid/view/View;
.source "GestureImageView.java"

# interfaces
.implements LaqS;


# instance fields
.field private a:F

.field private a:I

.field private final a:LSC;

.field private final a:LSx;

.field private a:Landroid/graphics/Matrix;

.field private final a:Landroid/graphics/Paint;

.field private a:Landroid/graphics/drawable/Drawable;

.field private final a:Landroid/os/Handler;

.field private a:LapM;

.field public a:LaqL;

.field private b:F

.field private b:I

.field private c:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 233
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/docs/view/GestureImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 234
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 237
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/docs/view/GestureImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 238
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 241
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:Landroid/os/Handler;

    .line 243
    new-instance v0, LSC;

    new-instance v1, LapO;

    invoke-direct {v1, p0, v2}, LapO;-><init>(Lcom/google/android/apps/docs/view/GestureImageView;LapL;)V

    invoke-direct {v0, p1, v1}, LSC;-><init>(Landroid/content/Context;LSD;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:LSC;

    .line 244
    new-instance v0, LSx;

    new-instance v1, LapN;

    invoke-direct {v1, p0, v2}, LapN;-><init>(Lcom/google/android/apps/docs/view/GestureImageView;LapL;)V

    invoke-direct {v0, p1, v1}, LSx;-><init>(Landroid/content/Context;LSA;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:LSx;

    .line 245
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:Landroid/graphics/Paint;

    .line 247
    invoke-static {p1}, Lajt;->a(Landroid/content/Context;)Lbuu;

    move-result-object v0

    invoke-interface {v0, p0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 248
    return-void
.end method

.method private a(F)F
    .locals 2

    .prologue
    .line 213
    iget v0, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:F

    iget v1, p0, Lcom/google/android/apps/docs/view/GestureImageView;->b:F

    invoke-static {v1, p1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method public static synthetic a(Landroid/graphics/Matrix;)F
    .locals 1

    .prologue
    .line 33
    invoke-static {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->b(Landroid/graphics/Matrix;)F

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/GestureImageView;)F
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/google/android/apps/docs/view/GestureImageView;->c:F

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/GestureImageView;F)F
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/view/GestureImageView;->a(F)F

    move-result v0

    return v0
.end method

.method private a()Landroid/graphics/Matrix;
    .locals 3

    .prologue
    .line 323
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 325
    iget v1, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:I

    neg-int v1, v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/docs/view/GestureImageView;->b:I

    neg-int v2, v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 327
    iget v1, p0, Lcom/google/android/apps/docs/view/GestureImageView;->c:F

    iget v2, p0, Lcom/google/android/apps/docs/view/GestureImageView;->c:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 329
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 331
    return-object v0
.end method

.method public static synthetic a(FFF)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 33
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/docs/view/GestureImageView;->b(FFF)Landroid/graphics/Matrix;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/GestureImageView;Landroid/graphics/Matrix;)Landroid/graphics/Matrix;
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:Landroid/graphics/Matrix;

    return-object p1
.end method

.method private a(Landroid/graphics/Matrix;)Landroid/graphics/Rect;
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 405
    const/4 v0, 0x4

    new-array v0, v0, [F

    aput v1, v0, v2

    aput v1, v0, v3

    iget v1, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:I

    int-to-float v1, v1

    aput v1, v0, v4

    iget v1, p0, Lcom/google/android/apps/docs/view/GestureImageView;->b:I

    int-to-float v1, v1

    aput v1, v0, v5

    .line 407
    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 408
    new-instance v1, Landroid/graphics/Rect;

    aget v2, v0, v2

    float-to-int v2, v2

    aget v3, v0, v3

    float-to-int v3, v3

    aget v4, v0, v4

    float-to-int v4, v4

    aget v0, v0, v5

    float-to-int v0, v0

    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v1
.end method

.method private a()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/GestureImageView;LapM;)LapM;
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:LapM;

    return-object p1
.end method

.method private a(Landroid/graphics/Matrix;)V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/4 v0, 0x0

    .line 368
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Landroid/graphics/Matrix;)Landroid/graphics/Rect;

    move-result-object v2

    .line 370
    iget v1, v2, Landroid/graphics/Rect;->right:I

    iget v3, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v3

    int-to-float v1, v1

    .line 371
    iget v3, v2, Landroid/graphics/Rect;->bottom:I

    iget v4, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    .line 376
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->getWidth()I

    move-result v4

    int-to-float v4, v4

    .line 377
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->getHeight()I

    move-result v5

    int-to-float v5, v5

    .line 379
    cmpg-float v6, v1, v4

    if-gtz v6, :cond_1

    .line 380
    sub-float v1, v4, v1

    div-float/2addr v1, v7

    .line 381
    iget v4, v2, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    sub-float/2addr v1, v4

    .line 390
    :goto_0
    cmpg-float v4, v3, v5

    if-gtz v4, :cond_3

    .line 391
    sub-float v0, v5, v3

    div-float/2addr v0, v7

    .line 392
    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    sub-float/2addr v0, v2

    .line 401
    :cond_0
    :goto_1
    invoke-virtual {p1, v1, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 402
    return-void

    .line 383
    :cond_1
    iget v1, v2, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    cmpl-float v1, v1, v0

    if-lez v1, :cond_2

    .line 384
    iget v1, v2, Landroid/graphics/Rect;->left:I

    neg-int v1, v1

    int-to-float v1, v1

    goto :goto_0

    .line 385
    :cond_2
    iget v1, v2, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    cmpg-float v1, v1, v4

    if-gez v1, :cond_5

    .line 386
    iget v1, v2, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    sub-float v1, v4, v1

    goto :goto_0

    .line 394
    :cond_3
    iget v3, v2, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    cmpl-float v3, v3, v0

    if-lez v3, :cond_4

    .line 395
    iget v0, v2, Landroid/graphics/Rect;->top:I

    neg-int v0, v0

    int-to-float v0, v0

    goto :goto_1

    .line 396
    :cond_4
    iget v3, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    cmpg-float v3, v3, v5

    if-gez v3, :cond_0

    .line 397
    iget v0, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    sub-float v0, v5, v0

    goto :goto_1

    :cond_5
    move v1, v0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/GestureImageView;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->d()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/GestureImageView;Landroid/graphics/Matrix;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Landroid/graphics/Matrix;)V

    return-void
.end method

.method private a(Landroid/graphics/Matrix;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    .line 267
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_1

    .line 274
    :cond_0
    :goto_0
    return v0

    .line 271
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Landroid/graphics/Matrix;)Landroid/graphics/Rect;

    move-result-object v1

    .line 273
    iget v2, v1, Landroid/graphics/Rect;->right:I

    iget v3, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->getWidth()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    cmpg-float v2, v2, v4

    if-gtz v2, :cond_2

    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int v1, v2, v1

    .line 274
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpg-float v1, v1, v4

    if-lez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/GestureImageView;Landroid/graphics/Matrix;)Z
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Landroid/graphics/Matrix;)Z

    move-result v0

    return v0
.end method

.method public static synthetic a(Landroid/graphics/Matrix;)[F
    .locals 1

    .prologue
    .line 33
    invoke-static {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->b(Landroid/graphics/Matrix;)[F

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/graphics/Matrix;)F
    .locals 1

    .prologue
    .line 422
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Landroid/graphics/Matrix;->mapRadius(F)F

    move-result v0

    return v0
.end method

.method private static b(FFF)Landroid/graphics/Matrix;
    .locals 3

    .prologue
    .line 432
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 433
    neg-float v1, p0

    neg-float v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 434
    invoke-virtual {v0, p2, p2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 435
    invoke-virtual {v0, p0, p1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 436
    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/view/GestureImageView;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->a()Landroid/graphics/Matrix;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/graphics/Matrix;)[F
    .locals 1

    .prologue
    .line 426
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    .line 427
    invoke-virtual {p0, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 428
    return-object v0

    .line 426
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 344
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    .line 345
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    .line 346
    cmpl-float v2, v0, v3

    if-eqz v2, :cond_0

    cmpl-float v2, v1, v3

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-nez v2, :cond_1

    .line 365
    :cond_0
    :goto_0
    return-void

    .line 349
    :cond_1
    iget v2, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    iget v2, p0, Lcom/google/android/apps/docs/view/GestureImageView;->b:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 350
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/docs/view/GestureImageView;->b:F

    .line 351
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 352
    sget v2, Lxa;->projector_max_scale_factor:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:F

    .line 354
    iget v1, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:F

    iget v2, p0, Lcom/google/android/apps/docs/view/GestureImageView;->b:F

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:F

    .line 360
    iget v1, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:F

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 362
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/view/GestureImageView;->a(F)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/view/GestureImageView;->c:F

    .line 364
    iget v0, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:F

    iget v1, p0, Lcom/google/android/apps/docs/view/GestureImageView;->c:F

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:F

    goto :goto_0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:LapM;

    if-eqz v0, :cond_0

    .line 416
    iget-object v0, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:LapM;

    invoke-virtual {v0}, LapM;->a()V

    .line 417
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:LapM;

    .line 419
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 255
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:Landroid/graphics/drawable/Drawable;

    .line 256
    return-void
.end method

.method public a(I)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 446
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-nez v2, :cond_1

    .line 455
    :cond_0
    :goto_0
    return v0

    .line 450
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:Landroid/graphics/Matrix;

    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/view/GestureImageView;->a(Landroid/graphics/Matrix;)Landroid/graphics/Rect;

    move-result-object v2

    .line 452
    if-gez p1, :cond_2

    .line 453
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->getWidth()I

    move-result v3

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int v2, v3, v2

    int-to-float v2, v2

    int-to-float v3, p1

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 455
    :cond_2
    iget v2, v2, Landroid/graphics/Rect;->left:I

    neg-int v2, v2

    int-to-float v2, v2

    int-to-float v3, p1

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 306
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 307
    if-eqz v0, :cond_0

    .line 308
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:I

    .line 309
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/docs/view/GestureImageView;->b:I

    .line 310
    iget v1, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:I

    iget v2, p0, Lcom/google/android/apps/docs/view/GestureImageView;->b:I

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 311
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->c()V

    .line 317
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->a()Landroid/graphics/Matrix;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:Landroid/graphics/Matrix;

    .line 319
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->invalidate()V

    .line 320
    return-void

    .line 313
    :cond_0
    iput v3, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:I

    .line 314
    iput v3, p0, Lcom/google/android/apps/docs/view/GestureImageView;->b:I

    goto :goto_0
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 464
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 465
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->invalidate()V

    .line 469
    :goto_0
    return-void

    .line 467
    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 280
    iget-object v0, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 282
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 283
    if-eqz v0, :cond_0

    .line 284
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 285
    iget-object v1, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:Landroid/graphics/Matrix;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 286
    iget-object v1, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 287
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 288
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 297
    :goto_0
    return-void

    .line 290
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 291
    iget-object v0, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:Landroid/graphics/Paint;

    const/high16 v1, 0x42480000    # 50.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 292
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 293
    iget-object v1, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:Landroid/graphics/Paint;

    const v2, 0x106000c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 294
    sget v1, Lxi;->loading:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    .line 295
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:Landroid/graphics/Paint;

    .line 294
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->b()V

    .line 337
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:LSC;

    invoke-virtual {v0, p1}, LSC;->a(Landroid/view/MotionEvent;)Z

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:LSx;

    invoke-virtual {v0, p1}, LSx;->a(Landroid/view/MotionEvent;)Z

    .line 263
    const/4 v0, 0x1

    return v0
.end method

.method public setDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 300
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/GestureImageView;->a:Landroid/graphics/drawable/Drawable;

    .line 301
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 302
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->b()V

    .line 303
    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 476
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/GestureImageView;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eq v0, p1, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
