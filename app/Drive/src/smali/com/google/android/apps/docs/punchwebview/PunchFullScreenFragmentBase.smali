.class public abstract Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;
.super Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;
.source "PunchFullScreenFragmentBase.java"


# instance fields
.field public a:LQr;

.field public a:LXz;

.field a:LZD;

.field public a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "LZC;",
            ">;"
        }
    .end annotation
.end field

.field public a:Lald;

.field private a:Landroid/view/WindowManager;

.field public a:Landroid/webkit/WebView;

.field public a:Landroid/widget/FrameLayout;

.field public b:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "LXR;",
            ">;"
        }
    .end annotation
.end field

.field public b:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Ljava/lang/String;LYR;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;-><init>(Ljava/lang/String;LYR;)V

    .line 85
    return-void
.end method

.method private A()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a()LH;

    move-result-object v0

    .line 114
    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Landroid/view/WindowManager;

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZC;

    invoke-interface {v0}, LZC;->b()Landroid/webkit/WebView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Landroid/webkit/WebView;

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 121
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->C()V

    .line 122
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->b:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->b:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXR;

    invoke-static {v1, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a(Landroid/view/View;LXR;)V

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setFocusable(Z)V

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearView()V

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->b:Landroid/view/ViewGroup;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setFocusable(Z)V

    .line 126
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->u()V

    .line 127
    return-void
.end method

.method private B()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 226
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 227
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 228
    return-void
.end method

.method private C()V
    .locals 3

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->c:Ljava/lang/String;

    const-string v1, "in showPopup"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    new-instance v0, LXu;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a()LH;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LXu;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Landroid/widget/FrameLayout;

    .line 233
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a()Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;

    move-result-object v0

    .line 234
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->B()V

    .line 235
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->setWebView(Landroid/webkit/WebView;)V

    .line 237
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->b:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Landroid/webkit/WebView;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 238
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 242
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 243
    const/16 v1, 0x77

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 244
    const/4 v1, -0x1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 245
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 246
    or-int/lit16 v1, v1, 0x200

    .line 247
    or-int/lit16 v1, v1, 0x100

    .line 248
    const v2, -0x10001

    and-int/2addr v1, v2

    .line 249
    or-int/lit16 v1, v1, 0x400

    .line 250
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 252
    const/16 v1, 0x3eb

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 253
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a()LH;

    move-result-object v1

    invoke-virtual {v1}, LH;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    iput-object v1, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 254
    const/4 v1, 0x1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    .line 255
    const-string v1, "Punch Fullscreen Presentation"

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 257
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Landroid/widget/FrameLayout;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 258
    return-void
.end method

.method private D()V
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->c:Ljava/lang/String;

    const-string v1, "in hidePopup"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Landroid/widget/FrameLayout;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 263
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->b:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 264
    return-void
.end method

.method private a()Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 207
    new-instance v1, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a()LH;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;-><init>(Landroid/content/Context;)V

    .line 208
    const/16 v0, 0x11

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->setGravity(I)V

    .line 209
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 213
    sget v0, LpN;->loading_spinner:I

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 215
    sget v0, LpN;->punch_web_view_container:I

    .line 216
    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->b:Landroid/view/ViewGroup;

    .line 218
    return-object v1
.end method

.method private a(ILandroid/content/Context;)V
    .locals 3

    .prologue
    .line 156
    invoke-virtual {p2, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 157
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Lald;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lald;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 158
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 75
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 191
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 195
    if-eqz p1, :cond_1

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->b:Landroid/view/ViewGroup;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setSystemUiVisibility(I)V

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 198
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->b:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setSystemUiVisibility(I)V

    goto :goto_0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 95
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->a_(Landroid/os/Bundle;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->c:Ljava/lang/String;

    const-string v1, "in onCreate savedInstanceState=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 99
    if-eqz p1, :cond_1

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->c:Ljava/lang/String;

    const-string v1, "discarding reconstruction."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Landroid/widget/FrameLayout;

    if-nez v0, :cond_0

    .line 105
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->A()V

    goto :goto_0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 89
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->c(Landroid/os/Bundle;)V

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->c:Ljava/lang/String;

    const-string v1, "in onSaveInstanceState outState=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 91
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->c:Ljava/lang/String;

    const-string v1, "in onDestroy"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->v()V

    .line 186
    invoke-super {p0}, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->h()V

    .line 187
    return-void
.end method

.method public t()V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 81
    :cond_0
    return-void
.end method

.method public u()V
    .locals 4

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a()LH;

    move-result-object v0

    .line 131
    if-nez v0, :cond_1

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:LZD;

    sget-object v2, LZD;->a:LZD;

    invoke-virtual {v1, v2}, LZD;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 136
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:LXz;

    invoke-interface {v1}, LXz;->b()Ljava/lang/Boolean;

    move-result-object v1

    .line 137
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 138
    sget v1, LpR;->punch_missing_features_notification:I

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a(ILandroid/content/Context;)V

    goto :goto_0

    .line 141
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:LXz;

    invoke-interface {v1}, LXz;->a()Ljava/lang/Boolean;

    move-result-object v1

    .line 142
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:LQr;

    const-string v2, "punchEnableApproximateTransitionsNotification"

    const/4 v3, 0x0

    .line 143
    invoke-interface {v1, v2, v3}, LQr;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 145
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:LZD;

    sget-object v2, LZD;->b:LZD;

    invoke-virtual {v1, v2}, LZD;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 146
    sget v1, LpR;->punch_approximate_transitions_notification:I

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a(ILandroid/content/Context;)V

    goto :goto_0

    .line 147
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:LZD;

    sget-object v2, LZD;->c:LZD;

    invoke-virtual {v1, v2}, LZD;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:LYO;

    .line 148
    invoke-interface {v1}, LYO;->a()LYR;

    move-result-object v1

    sget-object v2, LYR;->b:LYR;

    if-eq v1, v2, :cond_0

    .line 149
    sget v1, LpR;->punch_approximate_transitions_notification:I

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a(ILandroid/content/Context;)V

    goto :goto_0
.end method

.method protected v()V
    .locals 3

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Landroid/widget/FrameLayout;

    if-nez v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->c:Ljava/lang/String;

    const-string v1, "early exit in cleanup, as it was never fully created"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    :goto_0
    return-void

    .line 166
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a()LH;

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:LQr;

    .line 169
    invoke-static {v0}, LZD;->a(LQr;)LZD;

    move-result-object v0

    .line 170
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:LYO;

    invoke-virtual {v0}, LZD;->a()LYK;

    move-result-object v2

    invoke-interface {v1, v2}, LYO;->a(LYK;)V

    .line 171
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:LYO;

    .line 172
    invoke-virtual {v0}, LZD;->a()Z

    move-result v0

    .line 171
    invoke-interface {v1, v0}, LYO;->c(Z)V

    .line 174
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->D()V

    .line 176
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a(Z)V

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZC;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Landroid/webkit/WebView;

    invoke-interface {v0, v1}, LZC;->a(Landroid/webkit/WebView;)V

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearView()V

    goto :goto_0
.end method
