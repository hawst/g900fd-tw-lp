.class public Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;
.super Landroid/widget/RelativeLayout;
.source "SelectionOverlayLayout.java"

# interfaces
.implements LKe;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/RelativeLayout;",
        "LKe",
        "<",
        "Lcom/google/android/gms/drive/database/data/EntrySpec;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LKs;

.field private a:Landroid/view/View;

.field private a:Lcom/google/android/apps/docs/doclist/selection/SelectionModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/docs/doclist/selection/SelectionModel",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 40
    invoke-static {p1}, Lajt;->a(Landroid/content/Context;)Lbuu;

    move-result-object v0

    invoke-interface {v0, p0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    invoke-static {p1}, Lajt;->a(Landroid/content/Context;)Lbuu;

    move-result-object v0

    invoke-interface {v0, p0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    invoke-static {p1}, Lajt;->a(Landroid/content/Context;)Lbuu;

    move-result-object v0

    invoke-interface {v0, p0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 51
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;)Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;->b()V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 120
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 121
    sget v1, Lxc;->selection_underlay:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 123
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;->a:LKs;

    invoke-virtual {v1, v0}, LKs;->a(Landroid/view/View;)V

    .line 124
    return-void
.end method


# virtual methods
.method public c_()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;->a:Lcom/google/android/apps/docs/doclist/selection/SelectionModel;

    invoke-interface {v0}, Lcom/google/android/apps/docs/doclist/selection/SelectionModel;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 129
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;->setVisibility(I)V

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;->a:Lcom/google/android/apps/docs/doclist/selection/SelectionModel;

    invoke-interface {v0}, Lcom/google/android/apps/docs/doclist/selection/SelectionModel;->a()LbmF;

    move-result-object v0

    invoke-virtual {v0}, LbmF;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->clearAnimation()V

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->e()V

    .line 140
    :goto_0
    return-void

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 138
    :cond_1
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 114
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;->a:LKs;

    invoke-virtual {v0}, LKs;->a()V

    .line 116
    return-void
.end method

.method public setUp(Lcom/google/android/apps/docs/doclist/selection/SelectionModel;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/doclist/selection/SelectionModel",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/selection/SelectionModel;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;->a:Lcom/google/android/apps/docs/doclist/selection/SelectionModel;

    .line 60
    sget v0, Lxc;->drag_shadow_double:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 62
    sget v1, Lxc;->selection_floating_handle:I

    .line 63
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    iput-object v1, p0, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    .line 64
    sget v1, Lxc;->floating_handle_all:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;->a:Landroid/view/View;

    .line 67
    sget v1, Lxc;->selection_floating_visual:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 69
    new-instance v2, LMb;

    invoke-direct {v2, p0}, LMb;-><init>(Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 95
    new-instance v2, LMc;

    invoke-direct {v2, p0}, LMc;-><init>(Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;->a:Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;

    invoke-virtual {v1, p0, v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->setUp(Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;Landroid/widget/ImageView;)V

    .line 102
    invoke-interface {p1, p0}, Lcom/google/android/apps/docs/doclist/selection/SelectionModel;->a(LKe;)V

    .line 104
    new-instance v0, LMd;

    invoke-direct {v0, p0}, LMd;-><init>(Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;->post(Ljava/lang/Runnable;)Z

    .line 110
    return-void
.end method
