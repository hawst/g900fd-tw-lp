.class public Lcom/google/android/apps/docs/app/SelectNewDocTypeActivity;
.super Lrm;
.source "SelectNewDocTypeActivity.java"

# interfaces
.implements LFB;


# instance fields
.field private a:LaFO;

.field protected a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field public a:Luc;

.field private final b:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lrm;-><init>()V

    .line 44
    invoke-static {}, Lanj;->a()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/SelectNewDocTypeActivity;->b:Landroid/os/Handler;

    return-void
.end method

.method public static a(Landroid/content/Context;LaFO;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 53
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/apps/docs/app/SelectNewDocTypeActivity;->a(Landroid/content/Context;LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/apps/docs/app/SelectNewDocTypeActivity;->a(Landroid/content/Context;LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 79
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.docs.CreateNewDocument.CREATE_DOCUMENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 84
    const-class v1, Lcom/google/android/apps/docs/app/SelectNewDocTypeActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 85
    const-string v1, "accountName"

    invoke-virtual {p1}, LaFO;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    const-string v1, "returnResult"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 87
    const-string v1, "collectionEntrySpec"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 89
    return-object v0
.end method

.method private k()V
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/docs/app/SelectNewDocTypeActivity;->b:Landroid/os/Handler;

    new-instance v1, Lvl;

    invoke-direct {v1, p0}, Lvl;-><init>(Lcom/google/android/apps/docs/app/SelectNewDocTypeActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 144
    return-void
.end method


# virtual methods
.method public a(LCN;)V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/apps/docs/app/SelectNewDocTypeActivity;->a:LaFO;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/SelectNewDocTypeActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {p1, p0, v0, v1}, LCN;->a(Landroid/content/Context;LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    .line 155
    const/high16 v1, 0x2000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 156
    iget-object v1, p0, Lcom/google/android/apps/docs/app/SelectNewDocTypeActivity;->a:Luc;

    invoke-interface {v1, v0}, Luc;->a(Landroid/content/Intent;)V

    .line 157
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/SelectNewDocTypeActivity;->k()V

    .line 158
    return-void
.end method

.method f()V
    .locals 3

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/docs/app/SelectNewDocTypeActivity;->a:LqK;

    const-string v1, "/createNewDoc"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 124
    new-instance v0, Lcom/google/android/apps/docs/doclist/dialogs/SelectNewDocTypeDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/doclist/dialogs/SelectNewDocTypeDialogFragment;-><init>()V

    .line 130
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/doclist/dialogs/SelectNewDocTypeDialogFragment;->e(Z)V

    .line 131
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/SelectNewDocTypeActivity;->a()LM;

    move-result-object v1

    const-string v2, "createNewEntryDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/doclist/dialogs/SelectNewDocTypeDialogFragment;->a(LM;Ljava/lang/String;)V

    .line 132
    return-void
.end method

.method public j()V
    .locals 0

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/SelectNewDocTypeActivity;->k()V

    .line 150
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 94
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 96
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/SelectNewDocTypeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 97
    const-string v0, "accountName"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/SelectNewDocTypeActivity;->a:LaFO;

    .line 98
    const-string v0, "collectionEntrySpec"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/SelectNewDocTypeActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 100
    if-eqz p1, :cond_0

    .line 114
    :goto_0
    return-void

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/SelectNewDocTypeActivity;->a:LaFO;

    if-nez v0, :cond_1

    .line 105
    const-string v0, "SelectNewDocTypeActivity"

    const-string v2, "Account name is not specified in the intent."

    invoke-static {v0, v2}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/app/SelectNewDocTypeActivity;->a:LaFO;

    if-eqz v0, :cond_2

    const-string v0, "com.google.android.apps.docs.CreateNewDocument.CREATE_DOCUMENT"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 109
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/SelectNewDocTypeActivity;->f()V

    goto :goto_0

    .line 111
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/SelectNewDocTypeActivity;->setResult(I)V

    .line 112
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/SelectNewDocTypeActivity;->finish()V

    goto :goto_0
.end method
