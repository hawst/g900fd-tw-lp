.class public Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;
.super Lrm;
.source "ModifySharingActivity.java"

# interfaces
.implements Labg;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static a:I


# instance fields
.field public a:LaGM;

.field private a:LabI;

.field private a:LacA;

.field private a:LacD;

.field private a:Laca;

.field private a:Lacr;

.field public a:Lacs;

.field public a:LajO;

.field public a:Lald;

.field private a:Landroid/os/Parcelable;

.field private a:Landroid/support/v4/app/DialogFragment;

.field public a:Landroid/view/View;

.field a:Landroid/widget/ListView;

.field private a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

.field private a:Ljava/lang/String;

.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LacD;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/util/concurrent/Executor;

.field private a:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field public a:LtK;

.field private a:[Ljava/lang/String;

.field private final a:[Lqu;

.field private b:Landroid/os/Handler;

.field b:Landroid/view/View;

.field c:Landroid/view/View;

.field d:Landroid/view/View;

.field e:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 597
    const/4 v0, 0x0

    sput v0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Lrm;-><init>()V

    .line 138
    iput-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:[Ljava/lang/String;

    .line 140
    iput-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LacA;

    .line 155
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a()[Lqu;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:[Lqu;

    .line 168
    iput-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LacD;

    .line 174
    iput-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/os/Parcelable;

    .line 182
    iput-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/support/v4/app/DialogFragment;

    .line 186
    invoke-static {v1}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/concurrent/Future;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;Laca;)Laca;
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Laca;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;Lacr;)Lacr;
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lacr;

    return-object p1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 533
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 534
    const-string v1, "resourceSpec"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 535
    const-string v1, "documentTitle"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 536
    return-object v0
.end method

.method public static a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 600
    sget v0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:I

    .line 601
    sget v1, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:I

    .line 602
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AddCollaboratorTextDialogFragment"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->e:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->e:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    .line 302
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 306
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Landroid/view/View;)V

    .line 307
    return-void

    .line 304
    :cond_0
    const-string v0, "ModifySharingActivity"

    const-string v1, "Invalid empty list message view"

    invoke-static {v0, v1}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private a(ILjava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 528
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lald;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lald;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 529
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 294
    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/view/View;

    if-ne p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 295
    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/widget/ListView;

    if-ne p1, v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/ListView;->setVisibility(I)V

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b:Landroid/view/View;

    if-ne p1, v3, :cond_2

    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 297
    return-void

    :cond_0
    move v0, v2

    .line 294
    goto :goto_0

    :cond_1
    move v0, v2

    .line 295
    goto :goto_1

    :cond_2
    move v1, v2

    .line 296
    goto :goto_2
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->m()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;I)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b(I)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;ILjava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(ILjava/lang/Throwable;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Landroid/view/View;)V

    return-void
.end method

.method private a(LacD;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 351
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Laca;

    sget-object v2, Laca;->c:Laca;

    if-ne v1, v2, :cond_2

    .line 352
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lacr;

    invoke-interface {v1}, Lacr;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 353
    sget v1, Lxi;->sharing_cannot_change:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->c(I)V

    .line 363
    :goto_0
    return v0

    .line 355
    :cond_0
    if-nez p1, :cond_1

    .line 356
    sget v1, Lxi;->sharing_cannot_change_option:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->c(I)V

    goto :goto_0

    .line 358
    :cond_1
    invoke-virtual {p1}, LacD;->a()Laci;

    move-result-object v1

    invoke-virtual {v1}, Laci;->a()Lqv;

    move-result-object v1

    sget-object v2, Lqv;->a:Lqv;

    if-ne v1, v2, :cond_2

    .line 359
    sget v1, Lxi;->sharing_cannot_change_owner:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->c(I)V

    goto :goto_0

    .line 363
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a()[Ljava/lang/String;
    .locals 5

    .prologue
    .line 494
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 495
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:[Lqu;

    array-length v1, v0

    .line 496
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:[Ljava/lang/String;

    .line 497
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 498
    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:[Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:[Lqu;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lacr;

    .line 499
    invoke-interface {v4}, Lacr;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, p0, v4}, Lqu;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 497
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 502
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:[Ljava/lang/String;

    return-object v0
.end method

.method private final a()[Lqu;
    .locals 2

    .prologue
    .line 158
    const-class v0, Lqu;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 159
    sget-object v1, Lqu;->n:Lqu;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 160
    const/4 v1, 0x0

    new-array v1, v1, [Lqu;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lqu;

    return-object v0
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 489
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lacr;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:[Lqu;

    aget-object v1, v1, p1

    invoke-interface {v0, v1}, Lacr;->a(Lqu;)V

    .line 490
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->m()V

    .line 491
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->j()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;I)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->c(I)V

    return-void
.end method

.method private c(I)V
    .locals 3

    .prologue
    .line 524
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lald;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lald;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 525
    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->k()V

    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Laca;

    sget-object v1, Laca;->c:Laca;

    if-ne v0, v1, :cond_0

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lacr;

    invoke-interface {v0}, Lacr;->a()V

    .line 246
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->j()V

    .line 247
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->m()V

    .line 249
    :cond_0
    return-void
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 606
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()V
    .locals 5

    .prologue
    .line 252
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lacr;

    invoke-interface {v0}, Lacr;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacD;

    .line 254
    invoke-virtual {v0}, LacD;->a()Laci;

    move-result-object v3

    invoke-virtual {v3}, Laci;->a()Lqx;

    move-result-object v3

    sget-object v4, Lqx;->b:Lqx;

    if-eq v3, v4, :cond_1

    .line 255
    invoke-virtual {v0}, LacD;->a()Laci;

    move-result-object v3

    invoke-virtual {v3}, Laci;->a()Lqx;

    move-result-object v3

    sget-object v4, Lqx;->a:Lqx;

    if-ne v3, v4, :cond_0

    .line 256
    :cond_1
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 259
    :cond_2
    iput-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/ArrayList;

    .line 260
    return-void
.end method

.method private k()V
    .locals 3

    .prologue
    .line 263
    sget-object v0, Laca;->a:Laca;

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Laca;

    .line 264
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->s()V

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lacs;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-interface {v0, v1}, Lacs;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LbsU;

    move-result-object v0

    .line 266
    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/concurrent/Future;

    .line 267
    new-instance v1, LabX;

    invoke-direct {v1, p0}, LabX;-><init>(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V

    .line 290
    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LbsK;->a(LbsU;LbsJ;Ljava/util/concurrent/Executor;)V

    .line 291
    return-void
.end method

.method private l()V
    .locals 6

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 312
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lacr;

    invoke-interface {v0}, Lacr;->a()Z

    move-result v3

    .line 313
    iget-object v4, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->c:Landroid/view/View;

    if-eqz v3, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 314
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->d:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Laca;

    sget-object v5, Laca;->c:Laca;

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lacr;

    .line 315
    invoke-interface {v4}, Lacr;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    if-nez v3, :cond_1

    .line 314
    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 317
    return-void

    :cond_0
    move v0, v2

    .line 313
    goto :goto_0

    :cond_1
    move v1, v2

    .line 315
    goto :goto_1
.end method

.method private m()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 320
    const-string v0, "ModifySharingActivity"

    const-string v1, "Populating adapter"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Laca;

    sget-object v1, Laca;->c:Laca;

    if-eq v0, v1, :cond_0

    .line 322
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Landroid/view/View;)V

    .line 348
    :goto_0
    return-void

    .line 326
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->r()V

    .line 328
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->l()V

    .line 329
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 330
    sget v0, Lxi;->empty_sharing_list:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(I)V

    goto :goto_0

    .line 333
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/widget/ListView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Landroid/view/View;)V

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Laca;

    sget-object v1, Laca;->c:Laca;

    if-ne v0, v1, :cond_2

    .line 335
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/ArrayList;

    invoke-static {}, LacA;->a()Ljava/util/Comparator;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 339
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lacr;

    invoke-interface {v0}, Lacr;->a()Lqu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lacr;

    .line 340
    invoke-interface {v1}, Lacr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lqu;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 341
    new-instance v0, LacA;

    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LabI;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LacA;-><init>(Landroid/content/Context;Ljava/util/List;LabI;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LacA;

    .line 346
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LacA;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 347
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0

    .line 344
    :cond_2
    new-instance v0, LacA;

    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LabI;

    const/4 v4, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LacA;-><init>(Landroid/content/Context;Ljava/util/List;LabI;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LacA;

    goto :goto_1
.end method

.method private n()V
    .locals 6

    .prologue
    .line 367
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->r()V

    .line 369
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LacD;

    if-nez v0, :cond_1

    .line 370
    const-string v0, "ModifySharingActivity"

    const-string v1, "Selected item in contact sharing dialog is not defined."

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    :cond_0
    :goto_0
    return-void

    .line 372
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 373
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LacD;

    invoke-virtual {v0}, LacD;->a()LabD;

    move-result-object v0

    invoke-interface {v0}, LabD;->a()Ljava/lang/String;

    move-result-object v5

    .line 374
    new-instance v0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity$ContactSharingDialogFragmentImpl;

    invoke-direct {v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity$ContactSharingDialogFragmentImpl;-><init>()V

    .line 375
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LajO;

    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LaGM;

    move-object v4, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity$ContactSharingDialogFragmentImpl;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;LajO;LaGM;Landroid/content/Context;Ljava/lang/String;)V

    .line 377
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a()LM;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity$ContactSharingDialogFragmentImpl;->a(LM;Ljava/lang/String;)V

    .line 378
    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/support/v4/app/DialogFragment;

    goto :goto_0
.end method

.method private o()V
    .locals 5

    .prologue
    .line 384
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->r()V

    .line 386
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:[Lqu;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 387
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lacr;

    invoke-interface {v1}, Lacr;->a()Lqu;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:[Lqu;

    aget-object v2, v2, v0

    if-ne v1, v2, :cond_1

    .line 389
    new-instance v1, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity$SharingOptionsDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity$SharingOptionsDialogFragment;-><init>()V

    .line 390
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 391
    sget v3, Lxi;->dialog_sharing_options:I

    .line 392
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a()[Ljava/lang/String;

    move-result-object v4

    .line 391
    invoke-static {v2, v3, v4}, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a(Landroid/os/Bundle;I[Ljava/lang/String;)V

    .line 393
    invoke-virtual {v1, v2}, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->e(Landroid/os/Bundle;)V

    .line 395
    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->b(I)V

    .line 396
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a()LM;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a(LM;Ljava/lang/String;)V

    .line 401
    :cond_0
    return-void

    .line 386
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private p()V
    .locals 3

    .prologue
    .line 436
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 437
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a()LM;

    move-result-object v0

    .line 438
    invoke-static {}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a()Ljava/lang/String;

    move-result-object v1

    .line 439
    new-instance v2, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;

    invoke-direct {v2}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/support/v4/app/DialogFragment;

    .line 440
    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v2, v0, v1}, Landroid/support/v4/app/DialogFragment;->a(LM;Ljava/lang/String;)V

    .line 442
    :cond_0
    return-void
.end method

.method private q()V
    .locals 3

    .prologue
    .line 462
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->s()V

    .line 463
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lacs;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lacr;

    invoke-interface {v0, v1}, Lacs;->a(Lacr;)LbsU;

    move-result-object v0

    .line 464
    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/concurrent/Future;

    .line 465
    new-instance v1, LabY;

    invoke-direct {v1, p0}, LabY;-><init>(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V

    .line 485
    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LbsK;->a(LbsU;LbsJ;Ljava/util/concurrent/Executor;)V

    .line 486
    return-void
.end method

.method private r()V
    .locals 2

    .prologue
    .line 562
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LtK;

    sget-object v1, Lry;->ab:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 571
    :cond_0
    :goto_0
    return-void

    .line 566
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 569
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lacr;

    .line 570
    invoke-interface {v1}, Lacr;->a()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    .line 569
    invoke-static {v0}, LbiT;->b(Z)V

    goto :goto_0
.end method

.method private s()V
    .locals 2

    .prologue
    .line 610
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 611
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 613
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 546
    const-class v2, Lacr;

    if-ne p1, v2, :cond_1

    .line 547
    if-nez p2, :cond_0

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 549
    iget-object p0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lacr;

    .line 557
    :goto_1
    return-object p0

    :cond_0
    move v0, v1

    .line 547
    goto :goto_0

    .line 551
    :cond_1
    const-class v2, Labg;

    if-ne p1, v2, :cond_3

    .line 552
    if-nez p2, :cond_2

    :goto_2
    invoke-static {v0}, LbiT;->a(Z)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2

    .line 557
    :cond_3
    invoke-super {p0, p1, p2}, Lrm;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    goto :goto_1
.end method

.method public a(Ljava/util/List;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lqo;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 575
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->s()V

    .line 576
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lacs;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lacr;

    invoke-interface {v0, v1}, Lacs;->a(Lacr;)LbsU;

    move-result-object v0

    .line 577
    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/concurrent/Future;

    .line 578
    new-instance v1, LabZ;

    invoke-direct {v1, p0}, LabZ;-><init>(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V

    .line 594
    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LbsK;->a(LbsU;LbsJ;Ljava/util/concurrent/Executor;)V

    .line 595
    return-void
.end method

.method protected a_()V
    .locals 1

    .prologue
    .line 218
    invoke-super {p0}, Lrm;->a_()V

    .line 219
    sget v0, Lxc;->loading_sharing_list:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/view/View;

    .line 220
    sget v0, Lxc;->empty_sharing_list:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b:Landroid/view/View;

    .line 221
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/widget/ListView;

    .line 222
    sget v0, Lxc;->save_sharing_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->c:Landroid/view/View;

    .line 223
    sget v0, Lxc;->add_collaborators_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->d:Landroid/view/View;

    .line 224
    sget v0, Lxc;->empty_list_message:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->e:Landroid/view/View;

    .line 225
    return-void
.end method

.method public onAddClicked(Landroid/view/View;)V
    .locals 0
    .annotation build Lcom/google/android/apps/docs/neocommon/proguard/KeepAfterProguard;
    .end annotation

    .prologue
    .line 432
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->p()V

    .line 433
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 190
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 191
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b:Landroid/os/Handler;

    .line 192
    new-instance v0, LalI;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b:Landroid/os/Handler;

    invoke-direct {v0, v1}, LalI;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/concurrent/Executor;

    .line 196
    sget v0, Lxe;->sharing_list:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->setContentView(I)V

    .line 198
    new-instance v0, LabI;

    invoke-direct {v0, p0}, LabI;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LabI;

    .line 199
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 200
    const-string v0, "resourceSpec"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/ResourceSpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    if-nez v0, :cond_0

    .line 202
    const-string v0, "ModifySharingActivity"

    const-string v1, "No resourceSpec for the document is specified. Exiting."

    invoke-static {v0, v1}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->finish()V

    .line 214
    :goto_0
    return-void

    .line 207
    :cond_0
    const-string v0, "documentTitle"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/lang/String;

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 209
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 212
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lacs;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-interface {v0, v1}, Lacs;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lacr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lacr;

    .line 213
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->k()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 520
    invoke-super {p0}, Lrm;->onDestroy()V

    .line 521
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 405
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacD;

    .line 406
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(LacD;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 416
    :goto_0
    return-void

    .line 409
    :cond_0
    if-nez v0, :cond_1

    .line 410
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->o()V

    goto :goto_0

    .line 412
    :cond_1
    const-string v1, "ModifySharingActivity"

    const-string v2, "Clicked: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, LacD;->a()LabD;

    move-result-object v5

    invoke-interface {v5}, LabD;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 413
    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LacD;

    .line 414
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->n()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 5

    .prologue
    .line 507
    const-string v0, "ModifySharingActivity"

    const-string v1, "onPause in state %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Laca;

    invoke-virtual {v4}, Laca;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 508
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LqK;

    const-string v1, "/sharing"

    invoke-virtual {v0, p0, v1}, LqK;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 512
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LabI;

    invoke-virtual {v0}, LabI;->a()V

    .line 513
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->s()V

    .line 514
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/support/v4/app/DialogFragment;

    .line 515
    invoke-super {p0}, Lrm;->onPause()V

    .line 516
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 446
    invoke-super {p0, p1}, Lrm;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 447
    const-string v0, "listViewState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/os/Parcelable;

    .line 448
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    .line 229
    invoke-super {p0}, Lrm;->onResume()V

    .line 230
    const-string v1, "ModifySharingActivity"

    const-string v2, "onResume in state %s"

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Laca;

    if-nez v0, :cond_1

    const-string v0, "NULL"

    :goto_0
    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LqK;

    invoke-virtual {v0, p0}, LqK;->a(Ljava/lang/Object;)V

    .line 232
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LabI;

    invoke-virtual {v0}, LabI;->d()V

    .line 234
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->f()V

    .line 235
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->r()V

    .line 236
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->m()V

    .line 237
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/os/Parcelable;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 239
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/os/Parcelable;

    .line 241
    :cond_0
    return-void

    .line 230
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Laca;

    invoke-virtual {v0}, Laca;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public onSaveClicked(Landroid/view/View;)V
    .locals 2
    .annotation build Lcom/google/android/apps/docs/neocommon/proguard/KeepAfterProguard;
    .end annotation

    .prologue
    .line 423
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 424
    sget-object v0, Laca;->b:Laca;

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Laca;

    .line 425
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Landroid/view/View;)V

    .line 426
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->q()V

    .line 427
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 452
    invoke-super {p0, p1}, Lrm;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 453
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 454
    const-string v0, "listViewState"

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 456
    :cond_0
    return-void
.end method
