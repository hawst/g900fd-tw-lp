.class public Lcom/google/android/apps/viewer/viewer/image/ZoomView;
.super Landroid/widget/FrameLayout;
.source "ZoomView.java"


# static fields
.field private static final a:I


# instance fields
.field private final a:Landroid/graphics/Point;

.field private final a:Landroid/graphics/Rect;

.field private final a:Landroid/graphics/RectF;

.field private final a:Landroid/os/Handler;

.field private final a:Landroid/view/GestureDetector;

.field private final a:Landroid/view/ScaleGestureDetector;

.field private a:Landroid/view/View;

.field private final a:Landroid/widget/OverScroller;

.field private final a:LawE;

.field private final a:LawF;

.field private final a:LawG;

.field private a:LawH;

.field private final a:Lawh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lawh",
            "<",
            "LawH;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/lang/Runnable;

.field private a:Z

.field private b:I

.field private final b:Landroid/graphics/RectF;

.field private b:Landroid/view/View;

.field private b:Z

.field private c:I

.field private c:Z

.field private d:Z

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 72
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    sput v0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 149
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 78
    new-instance v0, LawG;

    invoke-direct {v0, p0, v1}, LawG;-><init>(Lcom/google/android/apps/viewer/viewer/image/ZoomView;LawD;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawG;

    .line 81
    new-instance v0, LawF;

    invoke-direct {v0, p0, v1}, LawF;-><init>(Lcom/google/android/apps/viewer/viewer/image/ZoomView;LawD;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawF;

    .line 84
    new-instance v0, LawE;

    invoke-direct {v0, p0, v1}, LawE;-><init>(Lcom/google/android/apps/viewer/viewer/image/ZoomView;LawD;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawE;

    .line 85
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Point;

    .line 86
    iput-object v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/view/View;

    .line 94
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    .line 100
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/RectF;

    .line 106
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/graphics/RectF;

    .line 113
    iput-boolean v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Z

    .line 116
    iput-boolean v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Z

    .line 119
    iput-boolean v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->c:Z

    .line 139
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/os/Handler;

    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->d:Z

    .line 161
    new-instance v0, Landroid/widget/OverScroller;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/widget/OverScroller;

    .line 162
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawG;

    invoke-direct {v0, v1, v2}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/view/ScaleGestureDetector;

    .line 163
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawF;

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/view/GestureDetector;

    .line 164
    new-instance v0, LawH;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v3, v3, v3}, LawH;-><init>(FIIZ)V

    invoke-static {v0}, Lawg;->a(Ljava/lang/Object;)Lawh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Lawh;

    .line 150
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 153
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 78
    new-instance v0, LawG;

    invoke-direct {v0, p0, v1}, LawG;-><init>(Lcom/google/android/apps/viewer/viewer/image/ZoomView;LawD;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawG;

    .line 81
    new-instance v0, LawF;

    invoke-direct {v0, p0, v1}, LawF;-><init>(Lcom/google/android/apps/viewer/viewer/image/ZoomView;LawD;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawF;

    .line 84
    new-instance v0, LawE;

    invoke-direct {v0, p0, v1}, LawE;-><init>(Lcom/google/android/apps/viewer/viewer/image/ZoomView;LawD;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawE;

    .line 85
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Point;

    .line 86
    iput-object v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/view/View;

    .line 94
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    .line 100
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/RectF;

    .line 106
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/graphics/RectF;

    .line 113
    iput-boolean v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Z

    .line 116
    iput-boolean v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Z

    .line 119
    iput-boolean v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->c:Z

    .line 139
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/os/Handler;

    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->d:Z

    .line 161
    new-instance v0, Landroid/widget/OverScroller;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/widget/OverScroller;

    .line 162
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawG;

    invoke-direct {v0, v1, v2}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/view/ScaleGestureDetector;

    .line 163
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawF;

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/view/GestureDetector;

    .line 164
    new-instance v0, LawH;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v3, v3, v3}, LawH;-><init>(FIIZ)V

    invoke-static {v0}, Lawg;->a(Ljava/lang/Object;)Lawh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Lawh;

    .line 154
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 157
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 78
    new-instance v0, LawG;

    invoke-direct {v0, p0, v1}, LawG;-><init>(Lcom/google/android/apps/viewer/viewer/image/ZoomView;LawD;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawG;

    .line 81
    new-instance v0, LawF;

    invoke-direct {v0, p0, v1}, LawF;-><init>(Lcom/google/android/apps/viewer/viewer/image/ZoomView;LawD;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawF;

    .line 84
    new-instance v0, LawE;

    invoke-direct {v0, p0, v1}, LawE;-><init>(Lcom/google/android/apps/viewer/viewer/image/ZoomView;LawD;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawE;

    .line 85
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Point;

    .line 86
    iput-object v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/view/View;

    .line 94
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    .line 100
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/RectF;

    .line 106
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/graphics/RectF;

    .line 113
    iput-boolean v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Z

    .line 116
    iput-boolean v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Z

    .line 119
    iput-boolean v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->c:Z

    .line 139
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/os/Handler;

    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->d:Z

    .line 161
    new-instance v0, Landroid/widget/OverScroller;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/widget/OverScroller;

    .line 162
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawG;

    invoke-direct {v0, v1, v2}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/view/ScaleGestureDetector;

    .line 163
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawF;

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/view/GestureDetector;

    .line 164
    new-instance v0, LawH;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v3, v3, v3}, LawH;-><init>(FIIZ)V

    invoke-static {v0}, Lawg;->a(Ljava/lang/Object;)Lawh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Lawh;

    .line 158
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:I

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Point;

    return-object v0
.end method

.method private a()Landroid/graphics/PointF;
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 461
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 462
    invoke-direct {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()Landroid/graphics/Rect;

    move-result-object v1

    .line 463
    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    invoke-virtual {v2, v1}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 476
    :goto_0
    return-object v0

    .line 467
    :cond_0
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    if-le v2, v3, :cond_1

    .line 468
    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget v3, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v2, v5

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/PointF;->x:F

    .line 471
    :cond_1
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    if-le v2, v3, :cond_2

    .line 472
    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget v3, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v2, v5

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iget-object v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int/2addr v1, v3

    int-to-float v1, v1

    div-float v1, v2, v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 475
    :cond_2
    const-string v1, "ZoomView"

    const-string v2, "UpdateScrollPos (%s %s)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, v0, Landroid/graphics/PointF;->x:F

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, v0, Landroid/graphics/PointF;->y:F

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private a()Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 512
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    iget-object v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    float-to-int v3, v3

    iget-object v4, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 514
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getScrollX()I

    move-result v1

    neg-int v1, v1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getScrollY()I

    move-result v2

    neg-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 515
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/graphics/RectF;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/view/View;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/widget/OverScroller;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/widget/OverScroller;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Lawh;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Lawh;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 386
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    neg-float v0, v0

    float-to-int v0, v0

    .line 387
    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    neg-float v1, v1

    float-to-int v1, v1

    .line 388
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()F

    move-result v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(F)V

    .line 389
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->scrollBy(II)V

    .line 390
    return-void
.end method

.method private a(F)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 371
    iput-boolean v4, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->d:Z

    .line 372
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setScaleX(F)V

    .line 373
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setScaleY(F)V

    .line 374
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/RectF;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 375
    const-string v0, "ZoomView"

    const-string v1, "After zoom (%s): (%s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/graphics/RectF;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    return-void
.end method

.method private a(FF)V
    .locals 3

    .prologue
    .line 394
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()F

    move-result v0

    .line 395
    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getScrollX()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, p1

    div-float/2addr v2, v0

    invoke-virtual {v1, v2}, Landroid/view/View;->setPivotX(F)V

    .line 396
    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getScrollY()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, p2

    div-float/2addr v2, v0

    invoke-virtual {v1, v2}, Landroid/view/View;->setPivotY(F)V

    .line 397
    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(F)V

    .line 398
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    float-to-int v0, v0

    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    float-to-int v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->scrollBy(II)V

    .line 399
    return-void
.end method

.method private a(Landroid/graphics/PointF;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 486
    invoke-direct {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()Landroid/graphics/Rect;

    move-result-object v2

    .line 487
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 505
    :goto_0
    return-void

    .line 492
    :cond_0
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    if-le v0, v3, :cond_2

    .line 493
    iget v0, p1, Landroid/graphics/PointF;->x:F

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v0, v3

    .line 496
    :goto_1
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    if-le v3, v4, :cond_1

    .line 497
    iget v1, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v1, v2

    .line 499
    :cond_1
    const-string v2, "ZoomView"

    const-string v3, "ApplyScrollPos (%s %s) -> (%.0f %.0f)"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p1, Landroid/graphics/PointF;->x:F

    .line 500
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v4, v5

    iget v5, p1, Landroid/graphics/PointF;->y:F

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x2

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v4, v5

    .line 499
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    float-to-int v0, v0

    float-to-int v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->scrollTo(II)V

    .line 503
    invoke-direct {p0, v7}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Z)V

    .line 504
    const-string v0, "ZoomView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Report Position: Finish Apply Position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Lawh;

    invoke-virtual {v2}, Lawh;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;F)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(F)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;FF)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(FF)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;Z)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 6

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Lawh;

    new-instance v1, LawH;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()F

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getScrollX()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 231
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getScrollY()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-direct {v1, v2, v3, v4, p1}, LawH;-><init>(FIIZ)V

    .line 230
    invoke-virtual {v0, v1}, Lawh;->b(Ljava/lang/Object;)V

    .line 232
    return-void
.end method

.method private a()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 411
    invoke-direct {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()Landroid/graphics/Rect;

    move-result-object v2

    .line 413
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    invoke-virtual {v2, v0}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 449
    :cond_0
    :goto_0
    return v1

    .line 419
    :cond_1
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 421
    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    move-result v3

    sub-int/2addr v0, v3

    .line 430
    :goto_1
    iget v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:I

    sub-int/2addr v3, v0

    iput v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:I

    .line 432
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    if-ge v3, v4, :cond_5

    .line 434
    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    sub-int/2addr v2, v3

    .line 443
    :goto_2
    iget v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->c:I

    sub-int/2addr v3, v2

    iput v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->c:I

    .line 445
    if-nez v0, :cond_2

    if-eqz v2, :cond_0

    .line 446
    :cond_2
    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->scrollBy(II)V

    .line 447
    const/4 v1, 0x1

    goto :goto_0

    .line 424
    :cond_3
    iget v0, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    if-le v0, v3, :cond_4

    .line 425
    iget v0, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v3

    goto :goto_1

    .line 426
    :cond_4
    iget v0, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    if-ge v0, v3, :cond_8

    .line 427
    iget v0, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v3

    goto :goto_1

    .line 437
    :cond_5
    iget v3, v2, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    if-le v3, v4, :cond_6

    .line 438
    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    goto :goto_2

    .line 439
    :cond_6
    iget v3, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    if-ge v3, v4, :cond_7

    .line 440
    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v2, v3

    goto :goto_2

    :cond_7
    move v2, v1

    goto :goto_2

    :cond_8
    move v0, v1

    goto :goto_1
.end method

.method private a(Landroid/view/View;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 654
    :goto_0
    if-eqz p1, :cond_2

    .line 655
    if-ne p1, p0, :cond_0

    .line 656
    const/4 v0, 0x1

    .line 663
    :goto_1
    return v0

    .line 657
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-eqz v0, :cond_1

    .line 658
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object p1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 660
    goto :goto_1

    :cond_2
    move v0, v1

    .line 663
    goto :goto_1
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Z
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()Z

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/viewer/image/ZoomView;Z)Z
    .locals 0

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->c:I

    return v0
.end method

.method public static synthetic b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/RectF;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/view/View;

    return-object v0
.end method

.method private b()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 578
    const-string v0, "ZoomView"

    const-string v1, "Restoring position %s"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawH;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawH;

    iget v0, v0, LawH;->a:F

    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(F)V

    .line 583
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawH;

    iget v0, v0, LawH;->a:I

    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawH;

    iget v1, v1, LawH;->b:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->scrollTo(II)V

    .line 584
    invoke-direct {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()Z

    .line 585
    invoke-direct {p0, v5}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Z)V

    .line 586
    const-string v0, "ZoomView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Report Position: Finish Restore "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Lawh;

    invoke-virtual {v2}, Lawh;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 587
    iput-object v6, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawH;

    .line 588
    iput-object v6, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Ljava/lang/Runnable;

    .line 589
    return-void
.end method

.method private b(F)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 379
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setPivotX(F)V

    .line 380
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setPivotY(F)V

    .line 381
    invoke-direct {p0, p1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(F)V

    .line 382
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()V

    return-void
.end method

.method private b()Z
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 541
    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawH;

    if-nez v2, :cond_1

    .line 572
    :cond_0
    :goto_0
    return v0

    .line 547
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawH;

    iget v3, v3, LawH;->a:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 548
    const-string v3, "ZoomView"

    const-string v4, "Try Restore %s in %s"

    new-array v5, v7, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawH;

    aput-object v6, v5, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    iget-object v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Ljava/lang/Runnable;

    if-eqz v3, :cond_2

    .line 550
    iget-object v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 551
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Ljava/lang/Runnable;

    .line 554
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawH;

    iget v3, v3, LawH;->b:I

    if-lt v2, v3, :cond_0

    .line 557
    iget-object v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawH;

    iget v3, v3, LawH;->b:I

    iget-object v4, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    add-int/2addr v3, v4

    if-ge v2, v3, :cond_3

    .line 561
    new-instance v1, LawD;

    invoke-direct {v1, p0}, LawD;-><init>(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)V

    iput-object v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Ljava/lang/Runnable;

    .line 566
    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Ljava/lang/Runnable;

    const-wide/16 v4, 0xc8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 569
    :cond_3
    const-string v3, "ZoomView"

    const-string v4, "Restore %s in %s"

    new-array v5, v7, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawH;

    aput-object v6, v5, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 571
    invoke-direct {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b()V

    move v0, v1

    .line 572
    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Z

    return v0
.end method

.method public static synthetic b(Lcom/google/android/apps/viewer/viewer/image/ZoomView;Z)Z
    .locals 0

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Z

    return p1
.end method

.method public static synthetic c(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Z

    return v0
.end method

.method public static synthetic c(Lcom/google/android/apps/viewer/viewer/image/ZoomView;Z)Z
    .locals 0

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->c:Z

    return p1
.end method

.method public static synthetic d(Lcom/google/android/apps/viewer/viewer/image/ZoomView;)Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->e:Z

    return v0
.end method


# virtual methods
.method public a()F
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getScaleX()F

    move-result v0

    return v0
.end method

.method public a()Landroid/graphics/Point;
    .locals 2

    .prologue
    .line 650
    new-instance v0, Landroid/graphics/Point;

    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Point;

    invoke-direct {v0, v1}, Landroid/graphics/Point;-><init>(Landroid/graphics/Point;)V

    return-object v0
.end method

.method public a()Lawe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lawe",
            "<",
            "LawH;",
            ">;"
        }
    .end annotation

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Lawh;

    return-object v0
.end method

.method public a(II)V
    .locals 3

    .prologue
    .line 357
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()F

    move-result v0

    .line 358
    int-to-float v1, p1

    mul-float/2addr v1, v0

    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    .line 359
    int-to-float v2, p2

    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v0, v2

    float-to-int v0, v0

    .line 361
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->scrollTo(II)V

    .line 362
    invoke-direct {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()Z

    .line 363
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Z)V

    .line 364
    return-void
.end method

.method public addView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 243
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/view/View;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "ZoomView can\'t take a second View"

    invoke-static {v0, v1}, LauV;->a(ZLjava/lang/String;)V

    .line 245
    iput-object p1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/view/View;

    .line 246
    return-void

    .line 244
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 333
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 334
    sget-boolean v0, LavX;->e:Z

    if-eqz v0, :cond_0

    .line 335
    const-string v0, "ZoomView"

    const-string v1, "ComputeScroll at (%s %s) moving to (%s, %s)"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    .line 336
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getScrollX()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getScrollY()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/widget/OverScroller;

    invoke-virtual {v4}, Landroid/widget/OverScroller;->getCurrX()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/widget/OverScroller;

    invoke-virtual {v4}, Landroid/widget/OverScroller;->getCurrY()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 335
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->getCurrX()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/widget/OverScroller;

    invoke-virtual {v1}, Landroid/widget/OverScroller;->getCurrY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->scrollTo(II)V

    .line 341
    invoke-direct {p0, v5}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Z)V

    .line 344
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->invalidate()V

    .line 350
    :cond_1
    :goto_0
    return-void

    .line 345
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->c:Z

    if-eqz v0, :cond_1

    .line 346
    invoke-direct {p0, v4}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Z)V

    .line 347
    const-string v0, "ZoomView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Report Position: Finish Fling "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Lawh;

    invoke-virtual {v2}, Lawh;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    iput-boolean v5, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->c:Z

    goto :goto_0
.end method

.method protected measureChild(Landroid/view/View;II)V
    .locals 2

    .prologue
    .line 250
    sget v0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:I

    sget v1, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:I

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    .line 251
    return-void
.end method

.method protected measureChildWithMargins(Landroid/view/View;IIII)V
    .locals 2

    .prologue
    .line 256
    sget v0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:I

    sget v1, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:I

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    .line 257
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 236
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 237
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/view/View;

    .line 239
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    .line 261
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/RectF;

    .line 267
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 268
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/RectF;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 269
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/RectF;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 271
    const-string v0, "ZoomView"

    const-string v1, "Layout: Bounds = %s Scale = %s Scroll = %s %s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/RectF;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 272
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getScrollX()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getScrollY()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 271
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    :cond_1
    const/4 v0, 0x0

    .line 279
    if-eqz p1, :cond_3

    .line 280
    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 281
    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getPaddingLeft()I

    move-result v2

    sub-int v2, p2, v2

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getPaddingTop()I

    move-result v3

    sub-int v3, p3, v3

    .line 282
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getPaddingRight()I

    move-result v4

    sub-int v4, p4, v4

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getPaddingBottom()I

    move-result v5

    sub-int v5, p5, v5

    .line 281
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 283
    invoke-direct {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 284
    const/4 v0, 0x1

    .line 285
    const-string v1, "ZoomView"

    const-string v2, "Report Position: Layout init viewport"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    :cond_2
    const-string v1, "ZoomView"

    const-string v2, "Layout: Viewport init = %s Scale = %s Scroll = %s %s"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 288
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getScrollX()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getScrollY()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 287
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    :cond_3
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->d:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/view/View;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    if-lez v1, :cond_5

    .line 305
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->d:Z

    .line 306
    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    .line 307
    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v1

    .line 308
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()F

    move-result v2

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_5

    .line 309
    invoke-direct {p0, v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b(F)V

    .line 310
    invoke-direct {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 311
    const/4 v0, 0x1

    .line 312
    const-string v2, "ZoomView"

    const-string v3, "Report Position: Layout Fit to screen"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    :cond_4
    const-string v2, "ZoomView"

    const-string v3, "Fit to screen: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b()Z

    move-result v1

    if-nez v1, :cond_6

    invoke-direct {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 321
    const/4 v0, 0x1

    .line 322
    const-string v1, "ZoomView"

    const-string v2, "Report Position: Layout contents moved"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    :cond_6
    if-eqz v0, :cond_7

    .line 326
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Z)V

    .line 327
    const-string v0, "ZoomView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Report Position: ... (cont\'d) "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Lawh;

    invoke-virtual {v2}, Lawh;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    :cond_7
    return-void

    .line 290
    :cond_8
    invoke-direct {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()Landroid/graphics/PointF;

    move-result-object v1

    .line 291
    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getPaddingLeft()I

    move-result v3

    sub-int v3, p2, v3

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getPaddingTop()I

    move-result v4

    sub-int v4, p3, v4

    .line 292
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getPaddingRight()I

    move-result v5

    sub-int v5, p4, v5

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getPaddingBottom()I

    move-result v6

    sub-int v6, p5, v6

    .line 291
    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 293
    invoke-direct {p0, v1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Landroid/graphics/PointF;)V

    .line 294
    invoke-direct {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 295
    const/4 v0, 0x1

    .line 296
    const-string v1, "ZoomView"

    const-string v2, "Report Position: Layout change viewport"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    :cond_9
    const-string v1, "ZoomView"

    const-string v2, "Layout: Viewport changed = %s Scale = %s Scroll = %s %s"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/graphics/Rect;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 299
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getScrollX()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getScrollY()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 298
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 529
    check-cast p1, Landroid/os/Bundle;

    .line 530
    const-string v0, "s"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 531
    const-string v0, "p"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, LawH;->a(Landroid/os/Bundle;)LawH;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawH;

    .line 532
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 6

    .prologue
    .line 520
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 521
    const-string v0, "s"

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 522
    const-string v2, "p"

    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Lawh;

    invoke-virtual {v0}, Lawh;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LawH;

    invoke-virtual {v0}, LawH;->a()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 523
    const-string v0, "ZoomView"

    const-string v2, "Saving position %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Lawh;

    invoke-virtual {v5}, Lawh;->a()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    return-object v1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 593
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 594
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 611
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 612
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 614
    iget-boolean v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:Z

    if-eqz v0, :cond_0

    .line 615
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 622
    :cond_0
    :goto_1
    :pswitch_1
    return v3

    .line 597
    :pswitch_2
    iput-boolean v3, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Z

    goto :goto_0

    .line 601
    :pswitch_3
    iput v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->c:I

    iput v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->b:I

    .line 602
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->abortAnimation()V

    .line 603
    iput-boolean v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Z

    goto :goto_0

    .line 607
    :pswitch_4
    iput-boolean v1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Z

    .line 608
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawF;

    invoke-virtual {v0}, LawF;->a()V

    goto :goto_0

    .line 618
    :pswitch_5
    invoke-direct {p0, v3}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Z)V

    .line 619
    const-string v0, "ZoomView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Report Position: Finish scroll "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:Lawh;

    invoke-virtual {v2}, Lawh;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 594
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 615
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_1
        :pswitch_5
    .end packed-switch
.end method

.method public setClickListenerForDescendant(Landroid/view/View;Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 638
    invoke-direct {p0, p1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Landroid/view/View;)Z

    move-result v0

    const-string v1, "parameter must be a descendant of this view"

    invoke-static {v0, v1}, LauV;->b(ZLjava/lang/String;)V

    .line 640
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawE;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 641
    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 642
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    .line 643
    return-void
.end method

.method public setLongClickListenerForDescendant(Landroid/view/View;Landroid/view/View$OnLongClickListener;)V
    .locals 2

    .prologue
    .line 630
    invoke-direct {p0, p1}, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a(Landroid/view/View;)Z

    move-result v0

    const-string v1, "parameter must be a descendant of this view"

    invoke-static {v0, v1}, LauV;->b(ZLjava/lang/String;)V

    .line 632
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawE;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 633
    invoke-virtual {p1, p2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 634
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setLongClickable(Z)V

    .line 635
    return-void
.end method

.method public setShareScroll(ZZZZ)V
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawF;

    invoke-static {v0, p1}, LawF;->a(LawF;Z)Z

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawF;

    invoke-static {v0, p2}, LawF;->b(LawF;Z)Z

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawF;

    invoke-static {v0, p3}, LawF;->c(LawF;Z)Z

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->a:LawF;

    invoke-static {v0, p4}, LawF;->d(LawF;Z)Z

    .line 172
    return-void
.end method

.method public setStraightenVerticalScroll(Z)V
    .locals 0

    .prologue
    .line 175
    iput-boolean p1, p0, Lcom/google/android/apps/viewer/viewer/image/ZoomView;->e:Z

    .line 176
    return-void
.end method
