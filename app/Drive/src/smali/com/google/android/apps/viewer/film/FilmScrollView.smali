.class public Lcom/google/android/apps/viewer/film/FilmScrollView;
.super Lcom/google/android/apps/viewer/film/HorizontalScrollView;
.source "FilmScrollView.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# static fields
.field private static final a:Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Lcom/google/android/apps/viewer/film/FilmScrollView;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:F

.field private final a:Landroid/animation/ObjectAnimator;

.field private final a:Landroid/view/View$OnClickListener;

.field private a:Lavu;

.field private a:Lavz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lavz",
            "<",
            "Lcom/google/android/apps/viewer/film/FilmScrollView;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lawf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lawf",
            "<",
            "Lcom/google/android/apps/viewer/client/Dimensions;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lcom/google/android/apps/viewer/film/FilmView;

.field private a:Z

.field private b:F

.field private c:F

.field private final d:F

.field private e:F

.field private f:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 44
    new-instance v0, Lavv;

    const-class v1, Ljava/lang/Float;

    const-string v2, "zoom"

    invoke-direct {v0, v1, v2}, Lavv;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Landroid/util/Property;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 57
    new-instance v0, Lavw;

    invoke-direct {v0, p0}, Lavw;-><init>(Lcom/google/android/apps/viewer/film/FilmScrollView;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lawf;

    .line 89
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->d:F

    .line 105
    sget-object v0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Landroid/animation/ObjectAnimator;

    .line 388
    new-instance v0, Lavy;

    invoke-direct {v0, p0}, Lavy;-><init>(Lcom/google/android/apps/viewer/film/FilmScrollView;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Landroid/view/View$OnClickListener;

    .line 111
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->c()V

    .line 112
    return-void

    .line 105
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 115
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    new-instance v0, Lavw;

    invoke-direct {v0, p0}, Lavw;-><init>(Lcom/google/android/apps/viewer/film/FilmScrollView;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lawf;

    .line 89
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->d:F

    .line 105
    sget-object v0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Landroid/animation/ObjectAnimator;

    .line 388
    new-instance v0, Lavy;

    invoke-direct {v0, p0}, Lavy;-><init>(Lcom/google/android/apps/viewer/film/FilmScrollView;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Landroid/view/View$OnClickListener;

    .line 116
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->c()V

    .line 117
    return-void

    .line 105
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 120
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    new-instance v0, Lavw;

    invoke-direct {v0, p0}, Lavw;-><init>(Lcom/google/android/apps/viewer/film/FilmScrollView;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lawf;

    .line 89
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->d:F

    .line 105
    sget-object v0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Landroid/animation/ObjectAnimator;

    .line 388
    new-instance v0, Lavy;

    invoke-direct {v0, p0}, Lavy;-><init>(Lcom/google/android/apps/viewer/film/FilmScrollView;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Landroid/view/View$OnClickListener;

    .line 121
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->c()V

    .line 122
    return-void

    .line 105
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private a(Landroid/view/View;FF)F
    .locals 5

    .prologue
    .line 366
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getScrollX()I

    move-result v0

    .line 367
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getWidth()I

    move-result v1

    .line 368
    div-float v2, p3, p2

    .line 369
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v4

    add-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    .line 370
    int-to-float v0, v0

    mul-float/2addr v0, v2

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float/2addr v0, v1

    .line 371
    mul-float v1, v3, p3

    sub-float v0, v1, v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/film/FilmScrollView;)F
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/film/FilmScrollView;)Lcom/google/android/apps/viewer/film/FilmView;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lcom/google/android/apps/viewer/film/FilmView;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/film/FilmScrollView;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->d()V

    return-void
.end method

.method private a(IZ)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 267
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 268
    iget-object v2, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lcom/google/android/apps/viewer/film/FilmView;

    iget-object v2, v2, Lcom/google/android/apps/viewer/film/FilmView;->a:[I

    if-nez v2, :cond_0

    .line 269
    const-string v1, "FilmScrollView"

    const-string v2, "scrollToTarget called when tabs == null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    :goto_0
    return v0

    .line 273
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lcom/google/android/apps/viewer/film/FilmView;

    iget-object v2, v2, Lcom/google/android/apps/viewer/film/FilmView;->a:[I

    aget v2, v2, p1

    .line 274
    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    sub-int v1, v2, v1

    .line 275
    if-eqz p2, :cond_1

    .line 276
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->a(II)V

    .line 280
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 278
    :cond_1
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->scrollBy(II)V

    goto :goto_1
.end method

.method private b(F)V
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lcom/google/android/apps/viewer/film/FilmView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/viewer/film/FilmView;->setScaleY(F)V

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lcom/google/android/apps/viewer/film/FilmView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/viewer/film/FilmView;->setScaleX(F)V

    .line 203
    iput p1, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    .line 204
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/viewer/film/FilmScrollView;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->e()V

    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 126
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->setOverScrollMode(I)V

    .line 127
    new-instance v0, Lavz;

    invoke-direct {v0, p0}, Lavz;-><init>(Lcom/google/android/apps/viewer/film/FilmScrollView;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lavz;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->a(LavA;)V

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 130
    return-void
.end method

.method private c(F)V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x1

    .line 221
    sget-boolean v0, LavX;->j:Z

    if-eqz v0, :cond_0

    .line 250
    :goto_0
    return-void

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lcom/google/android/apps/viewer/film/FilmView;

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/film/FilmView;->requestLayout()V

    .line 227
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getScrollX()I

    move-result v0

    .line 229
    int-to-float v1, v0

    iget-object v2, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lcom/google/android/apps/viewer/film/FilmView;

    invoke-virtual {v2}, Lcom/google/android/apps/viewer/film/FilmView;->getLeft()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    mul-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lcom/google/android/apps/viewer/film/FilmView;

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/film/FilmView;->a()Landroid/widget/FrameLayout;

    move-result-object v0

    .line 238
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lcom/google/android/apps/viewer/film/FilmView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/viewer/film/FilmView;->a(Landroid/view/View;)I

    move-result v1

    .line 240
    iget v2, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    cmpl-float v2, v2, v5

    if-nez v2, :cond_3

    .line 241
    iget-object v2, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lavu;

    iget-object v2, v2, Lavu;->b:Lawh;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lawh;->b(Ljava/lang/Object;)V

    .line 242
    const-string v2, "FilmScrollView"

    const-string v3, "Adjust Freeze (FS): Frame@ (%s, %s) * %s : %s"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    .line 243
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v6

    iget v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v4, v8

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getScrollX()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v9

    .line 242
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    invoke-direct {p0, v1, v6}, Lcom/google/android/apps/viewer/film/FilmScrollView;->a(IZ)Z

    goto :goto_0

    .line 231
    :cond_1
    int-to-float v1, v0

    iget-object v2, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lcom/google/android/apps/viewer/film/FilmView;

    invoke-virtual {v2}, Lcom/google/android/apps/viewer/film/FilmView;->getRight()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    mul-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    .line 232
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lcom/google/android/apps/viewer/film/FilmView;

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/film/FilmView;->b()Landroid/widget/FrameLayout;

    move-result-object v0

    goto :goto_1

    .line 234
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 235
    iget-object v1, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lcom/google/android/apps/viewer/film/FilmView;

    int-to-float v0, v0

    iget v2, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    div-float/2addr v0, v2

    float-to-int v0, v0

    invoke-virtual {v1, v0, p1}, Lcom/google/android/apps/viewer/film/FilmView;->a(IF)Landroid/widget/FrameLayout;

    move-result-object v0

    goto :goto_1

    .line 246
    :cond_3
    const-string v0, "FilmScrollView"

    const-string v2, "Adjust Freeze (Z): Scroll: %s, Zoom: %s to View #%s"

    new-array v3, v9, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getScrollX()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    iget v4, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    .line 247
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    .line 246
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v5, v0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->a(FLjava/lang/Integer;)V

    goto/16 :goto_0
.end method

.method private d()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 329
    iput v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->e:F

    .line 330
    iput v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->c:F

    .line 331
    iput v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:F

    .line 332
    sget-boolean v0, LavX;->e:Z

    if-eqz v0, :cond_0

    .line 333
    const-string v0, "FilmScrollView"

    const-string v1, "DBG End scaling zoom: %s scroll: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getScrollX()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    :cond_0
    return-void
.end method

.method private d(F)V
    .locals 5

    .prologue
    .line 318
    iput p1, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->e:F

    .line 319
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getScrollX()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->c:F

    .line 320
    iget v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    iput v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:F

    .line 321
    sget-boolean v0, LavX;->e:Z

    if-eqz v0, :cond_0

    .line 322
    const-string v0, "FilmScrollView"

    const-string v1, "DBG Start scaling zoom: %s scroll: %s scroll(@1.0): %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 323
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getScrollX()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->c:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    .line 322
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    :cond_0
    return-void
.end method

.method private e()V
    .locals 6

    .prologue
    .line 481
    sget-boolean v0, LavX;->e:Z

    if-eqz v0, :cond_0

    .line 482
    const-string v0, "FilmScrollView"

    const-string v1, "Width @1.0: %s (ext: %s), @%s : %s (ext: %s)"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lcom/google/android/apps/viewer/film/FilmView;

    .line 483
    invoke-virtual {v4}, Lcom/google/android/apps/viewer/film/FilmView;->getWidth()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lcom/google/android/apps/viewer/film/FilmView;

    .line 484
    invoke-virtual {v4}, Lcom/google/android/apps/viewer/film/FilmView;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getWidth()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    .line 485
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lcom/google/android/apps/viewer/film/FilmView;

    .line 486
    invoke-virtual {v4}, Lcom/google/android/apps/viewer/film/FilmView;->getWidth()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lcom/google/android/apps/viewer/film/FilmView;

    .line 487
    invoke-virtual {v4}, Lcom/google/android/apps/viewer/film/FilmView;->getWidth()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    mul-float/2addr v4, v5

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getWidth()I

    move-result v5

    int-to-float v5, v5

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    .line 482
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    :cond_0
    return-void
.end method


# virtual methods
.method public a()F
    .locals 1

    .prologue
    .line 196
    iget v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    return v0
.end method

.method public a()V
    .locals 3

    .prologue
    .line 159
    iget-object v1, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lcom/google/android/apps/viewer/film/FilmView;

    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lavu;

    iget-object v0, v0, Lavu;->a:Lawh;

    invoke-virtual {v0}, Lawh;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/viewer/client/Dimensions;

    iget-object v2, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lavu;

    invoke-virtual {v2}, Lavu;->a()[Lcom/google/android/apps/viewer/client/Dimensions;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/viewer/film/FilmView;->a(Lcom/google/android/apps/viewer/client/Dimensions;[Lcom/google/android/apps/viewer/client/Dimensions;)V

    .line 160
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Z

    .line 161
    return-void
.end method

.method public a(F)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 342
    iget v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:F

    cmpl-float v0, v0, v5

    if-nez v0, :cond_0

    .line 343
    const-string v0, "FilmScrollView"

    const-string v1, "ERROR setZoom without start %s to %s"

    new-array v2, v7, [Ljava/lang/Object;

    iget v3, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    :cond_0
    iget v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_1

    .line 347
    const-string v0, "FilmScrollView"

    const-string v1, "AWK setZoom noop %s to %s"

    new-array v2, v7, [Ljava/lang/Object;

    iget v3, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    :goto_0
    return-void

    .line 351
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lcom/google/android/apps/viewer/film/FilmView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/viewer/film/FilmView;->setScaleY(F)V

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lcom/google/android/apps/viewer/film/FilmView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/viewer/film/FilmView;->setScaleX(F)V

    .line 354
    iget v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->c:F

    mul-float/2addr v0, p1

    .line 355
    iget v1, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->e:F

    cmpl-float v1, v1, v5

    if-eqz v1, :cond_2

    .line 356
    iget v1, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->e:F

    iget v2, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:F

    sub-float v2, p1, v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 360
    :goto_1
    float-to-int v0, v0

    invoke-virtual {p0, v0, v4}, Lcom/google/android/apps/viewer/film/FilmScrollView;->scrollTo(II)V

    .line 361
    iput p1, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    goto :goto_0

    .line 358
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:F

    div-float v2, p1, v2

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    goto :goto_1
.end method

.method public a(FF)V
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 292
    sget-boolean v0, LavX;->e:Z

    if-eqz v0, :cond_0

    .line 293
    const-string v0, "FilmScrollView"

    const-string v1, "zoom %s // drift %s"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 298
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->a()F

    move-result v0

    sub-float v0, p1, v0

    div-float v0, p2, v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->d(F)V

    .line 299
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Landroid/animation/ObjectAnimator;

    new-array v1, v6, [F

    iget v2, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    aput v2, v1, v4

    aput p1, v1, v5

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 300
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Landroid/animation/ObjectAnimator;

    new-instance v1, Lavx;

    invoke-direct {v1, p0}, Lavx;-><init>(Lcom/google/android/apps/viewer/film/FilmScrollView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 312
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 313
    return-void
.end method

.method public a(FLjava/lang/Integer;)V
    .locals 2

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lavu;

    iget-object v0, v0, Lavu;->b:Lawh;

    invoke-virtual {v0, p2}, Lawh;->b(Ljava/lang/Object;)V

    .line 286
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    .line 287
    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->a(FF)V

    .line 288
    return-void

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lcom/google/android/apps/viewer/film/FilmView;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/viewer/film/FilmView;->a(I)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->a()F

    move-result v1

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/viewer/film/FilmScrollView;->a(Landroid/view/View;FF)F

    move-result v0

    goto :goto_0
.end method

.method public a(I)V
    .locals 5

    .prologue
    .line 426
    invoke-super {p0, p1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->a(I)V

    .line 427
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lavz;

    invoke-virtual {v0}, Lavz;->b()F

    move-result v0

    int-to-float v1, p1

    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->f:F

    .line 428
    const-string v0, "FilmScrollView"

    const-string v1, "Fling start @%s (v: %s) from:%s to:%s; in [0, %s]"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 429
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getScrollX()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->f:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lavz;

    invoke-virtual {v4}, Lavz;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lavz;

    invoke-virtual {v4}, Lavz;->d()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 428
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 188
    iget v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    const/4 v11, 0x1

    const/4 v6, 0x0

    .line 437
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:LavA;

    invoke-interface {v0}, LavA;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 438
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getScrollX()I

    move-result v3

    .line 439
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getScrollY()I

    move-result v4

    .line 440
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:LavA;

    invoke-interface {v0}, LavA;->a()I

    move-result v12

    .line 441
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:LavA;

    invoke-interface {v0}, LavA;->b()I

    move-result v0

    .line 443
    iget-object v1, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lavz;

    invoke-virtual {v1}, Lavz;->a()F

    move-result v1

    .line 444
    const v2, 0x3f333333    # 0.7f

    cmpl-float v2, v1, v2

    if-ltz v2, :cond_0

    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v2, v1, v2

    if-gtz v2, :cond_0

    iget v2, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_0

    .line 445
    invoke-direct {p0, v1}, Lcom/google/android/apps/viewer/film/FilmScrollView;->b(F)V

    .line 446
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->e()V

    .line 449
    :cond_0
    if-ne v3, v12, :cond_1

    if-eq v4, v0, :cond_3

    .line 450
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->b()I

    move-result v5

    .line 451
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getOverScrollMode()I

    move-result v1

    .line 452
    if-eqz v1, :cond_2

    if-ne v1, v11, :cond_6

    if-lez v5, :cond_6

    :cond_2
    move v10, v11

    .line 455
    :goto_0
    sub-int v1, v12, v3

    sub-int v2, v0, v4

    iget v7, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:I

    move-object v0, p0

    move v8, v6

    move v9, v6

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/apps/viewer/film/FilmScrollView;->overScrollBy(IIIIIIIIZ)Z

    .line 456
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getScrollY()I

    move-result v1

    invoke-virtual {p0, v0, v1, v3, v4}, Lcom/google/android/apps/viewer/film/FilmScrollView;->onScrollChanged(IIII)V

    .line 458
    if-eqz v10, :cond_3

    .line 459
    if-gez v12, :cond_7

    if-ltz v3, :cond_7

    .line 460
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Landroid/widget/EdgeEffect;

    iget-object v1, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:LavA;

    invoke-interface {v1}, LavA;->b()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    .line 467
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->awakenScrollBars()Z

    move-result v0

    if-nez v0, :cond_4

    .line 468
    invoke-static {p0}, Lec;->a(Landroid/view/View;)V

    .line 473
    :cond_4
    iget v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->f:F

    cmpl-float v0, v0, v13

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:LavA;

    invoke-interface {v0}, LavA;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 474
    const-string v0, "FilmScrollView"

    const-string v1, "Fling stop @%s z:%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getScrollX()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    iget v3, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    iget v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->f:F

    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->c(F)V

    .line 476
    iput v13, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->f:F

    .line 478
    :cond_5
    return-void

    :cond_6
    move v10, v6

    .line 452
    goto :goto_0

    .line 461
    :cond_7
    if-le v12, v5, :cond_3

    if-gt v3, v5, :cond_3

    .line 462
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:Landroid/widget/EdgeEffect;

    iget-object v1, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:LavA;

    invoke-interface {v1}, LavA;->b()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    goto :goto_1
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 153
    invoke-super {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->onDetachedFromWindow()V

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lavu;

    iget-object v0, v0, Lavu;->a:Lawh;

    iget-object v1, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lawf;

    invoke-virtual {v0, v1}, Lawh;->a(Ljava/lang/Object;)V

    .line 155
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 134
    invoke-super {p0}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->onFinishInflate()V

    .line 135
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->setSmoothScrollingEnabled(Z)V

    .line 137
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/viewer/film/FilmView;

    iput-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lcom/google/android/apps/viewer/film/FilmView;

    .line 138
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 165
    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->onLayout(ZIIII)V

    .line 168
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Z

    if-eqz v0, :cond_1

    .line 169
    const-string v0, "FilmScrollView"

    const-string v1, "Reposition scroll to %s after orientation change to %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lavu;

    iget-object v3, v3, Lavu;->b:Lawh;

    .line 170
    invoke-virtual {v3}, Lawh;->a()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getWidth()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 169
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    iget v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->b:F

    cmpl-float v0, v0, v6

    if-nez v0, :cond_2

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lavu;

    iget-object v0, v0, Lavu;->b:Lawh;

    invoke-virtual {v0}, Lawh;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0, v5}, Lcom/google/android/apps/viewer/film/FilmScrollView;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 173
    const-string v0, "FilmScrollView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t apply scroll on Reposition scroll after fresh layout w:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    :cond_0
    iput-boolean v5, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Z

    .line 180
    :cond_1
    :goto_0
    return-void

    .line 177
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lavu;

    iget-object v0, v0, Lavu;->b:Lawh;

    invoke-virtual {v0}, Lawh;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {p0, v6, v0}, Lcom/google/android/apps/viewer/film/FilmScrollView;->a(FLjava/lang/Integer;)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 208
    invoke-super {p0, p1}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 209
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    .line 210
    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 211
    :cond_0
    iget v1, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->f:F

    cmpl-float v1, v1, v3

    if-nez v1, :cond_1

    .line 212
    invoke-direct {p0, v3}, Lcom/google/android/apps/viewer/film/FilmScrollView;->c(F)V

    .line 216
    :cond_1
    return v0
.end method

.method protected overScrollBy(IIIIIIIIZ)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 378
    iget-object v1, p0, Lcom/google/android/apps/viewer/film/FilmScrollView;->a:Lcom/google/android/apps/viewer/film/FilmView;

    invoke-virtual {v1}, Lcom/google/android/apps/viewer/film/FilmView;->a()I

    move-result v1

    if-le v1, v0, :cond_0

    .line 379
    invoke-super/range {p0 .. p9}, Lcom/google/android/apps/viewer/film/HorizontalScrollView;->overScrollBy(IIIIIIIIZ)Z

    move-result v0

    .line 383
    :cond_0
    return v0
.end method
