.class public Lcom/google/android/apps/docs/app/RemoveEntriesActivity;
.super Lrm;
.source "RemoveEntriesActivity.java"

# interfaces
.implements LFu;


# instance fields
.field public a:LVm;

.field public a:LaGM;

.field private a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lrm;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;LbmF;)Landroid/content/Intent;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 81
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/app/RemoveEntriesActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 82
    const-string v1, "itemKeys"

    invoke-static {p1}, LbnG;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 83
    return-object v0
.end method

.method private a(LbmF;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    .line 66
    invoke-virtual {p1}, LbmF;->a()Lbqv;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/selection/ItemKey;

    .line 67
    invoke-interface {v0}, Lcom/google/android/apps/docs/doclist/selection/ItemKey;->a()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 68
    iget-object v2, p0, Lcom/google/android/apps/docs/app/RemoveEntriesActivity;->a:LaGM;

    invoke-interface {v2, v0}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGu;

    move-result-object v0

    .line 69
    sget-object v2, LaGw;->a:LaGw;

    invoke-interface {v0}, LaGu;->a()LaGw;

    move-result-object v0

    invoke-virtual {v2, v0}, LaGw;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    const/4 v0, 0x1

    .line 73
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public f()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/docs/app/RemoveEntriesActivity;->a:LVm;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/RemoveEntriesActivity;->a:LbmF;

    invoke-virtual {v0, v1}, LVm;->a(LbmF;)V

    .line 89
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 42
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 45
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/RemoveEntriesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "itemKeys"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 46
    invoke-static {v0}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/RemoveEntriesActivity;->a:LbmF;

    .line 48
    iget-object v0, p0, Lcom/google/android/apps/docs/app/RemoveEntriesActivity;->a:LbmF;

    new-instance v1, Lvg;

    invoke-direct {v1, p0}, Lvg;-><init>(Lcom/google/android/apps/docs/app/RemoveEntriesActivity;)V

    .line 49
    invoke-static {v0, v1}, LblV;->a(Ljava/util/Collection;LbiG;)Ljava/util/Collection;

    move-result-object v0

    .line 48
    invoke-static {v0}, LaGD;->a(Ljava/util/Collection;)LaGD;

    move-result-object v0

    .line 56
    iget-object v1, p0, Lcom/google/android/apps/docs/app/RemoveEntriesActivity;->a:LbmF;

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/app/RemoveEntriesActivity;->a(LbmF;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;->a(LaGD;)Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;

    move-result-object v0

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/RemoveEntriesActivity;->a()LM;

    move-result-object v1

    const-string v2, "RemoveEntriesFragment"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/doclist/dialogs/RemoveEntriesFragment;->a(LM;Ljava/lang/String;)V

    .line 63
    :goto_0
    return-void

    .line 60
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/RemoveEntriesActivity;->f()V

    .line 61
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/RemoveEntriesActivity;->finish()V

    goto :goto_0
.end method
