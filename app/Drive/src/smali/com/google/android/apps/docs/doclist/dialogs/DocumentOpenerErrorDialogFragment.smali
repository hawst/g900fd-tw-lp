.class public Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "DocumentOpenerErrorDialogFragment.java"


# instance fields
.field public a:LET;

.field public a:LaGM;

.field public a:Ladi;

.field private a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

.field private a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private w:Z

.field private x:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    .line 175
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 42
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->b(Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    return-object v0
.end method

.method public static synthetic a(LM;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 42
    invoke-static {p0, p1}, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->b(LM;Landroid/os/Bundle;)V

    return-void
.end method

.method public static a(LM;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 257
    new-instance v0, LES;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LES;-><init>(LM;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, p5}, LES;->a(Z)LES;

    move-result-object v0

    .line 258
    invoke-virtual {v0}, LES;->a()V

    .line 259
    return-void
.end method

.method public static a(LM;Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 280
    new-instance v0, LES;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LES;-><init>(LM;Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    invoke-virtual {v0, p5}, LES;->a(Z)LES;

    move-result-object v0

    .line 282
    invoke-virtual {v0}, LES;->a()V

    .line 283
    return-void
.end method

.method public static a(LM;)Z
    .locals 1

    .prologue
    .line 296
    const-string v0, "DocumentOpenerErrorDialogFragment"

    invoke-virtual {p0, v0}, LM;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 297
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 164
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 166
    const-string v1, "errorTitle"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const-string v1, "errorHtml"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    invoke-static {v0, p0}, LFU;->a(Landroid/os/Bundle;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V

    .line 169
    return-object v0
.end method

.method private static b(LM;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 286
    const-string v0, "DocumentOpenerErrorDialogFragment"

    invoke-virtual {p0, v0}, LM;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 287
    if-eqz v0, :cond_0

    .line 288
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->a()V

    .line 290
    :cond_0
    new-instance v0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;-><init>()V

    .line 291
    invoke-virtual {v0, p1}, Landroid/support/v4/app/DialogFragment;->e(Landroid/os/Bundle;)V

    .line 292
    const-string v1, "DocumentOpenerErrorDialogFragment"

    invoke-virtual {v0, p0, v1}, Landroid/support/v4/app/DialogFragment;->a(LM;Ljava/lang/String;)V

    .line 293
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a()LH;

    move-result-object v1

    .line 103
    invoke-static {v1}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v2

    .line 104
    invoke-static {}, Laml;->b()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->c:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 107
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 108
    iget-boolean v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->w:Z

    if-eqz v0, :cond_0

    .line 109
    sget v0, Lxi;->button_retry:I

    new-instance v3, LEO;

    invoke-direct {v3, p0}, LEO;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;)V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-eqz v0, :cond_1

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:LaGM;

    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v3}, LaGM;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;

    move-result-object v0

    .line 118
    if-eqz v0, :cond_1

    .line 119
    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    invoke-interface {v0}, LaGo;->a()LaGv;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a(LaGv;)LacY;

    move-result-object v3

    .line 120
    iget-object v4, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:Ladi;

    invoke-interface {v4, v0, v3}, Ladi;->a(LaGo;LacY;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 121
    sget v0, Lxi;->open_pinned_version:I

    new-instance v3, LEP;

    invoke-direct {v3, p0, v1}, LEP;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;Landroid/app/Activity;)V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 145
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-eqz v0, :cond_3

    const/high16 v0, 0x1040000

    :goto_1
    new-instance v3, LER;

    invoke-direct {v3, p0, v1}, LER;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;Landroid/app/Activity;)V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 153
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 129
    :cond_2
    iget-boolean v3, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->x:Z

    if-eqz v3, :cond_1

    .line 130
    invoke-interface {v0}, LaGo;->a()Ljava/lang/String;

    move-result-object v0

    .line 131
    if-eqz v0, :cond_1

    .line 132
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 133
    sget v3, Lxi;->open_document_in_browser:I

    new-instance v4, LEQ;

    invoke-direct {v4, p0, v0, v1}, LEQ;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;Landroid/net/Uri;Landroid/app/Activity;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 145
    :cond_3
    const v0, 0x104000a

    goto :goto_1
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 80
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a_(Landroid/os/Bundle;)V

    .line 81
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v1

    .line 82
    const-string v0, "errorTitle"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->c:Ljava/lang/String;

    .line 83
    const-string v0, "errorHtml"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->d:Ljava/lang/String;

    .line 84
    const-string v0, "canRetry"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->w:Z

    .line 85
    const-string v0, "canBrowser"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->x:Z

    .line 86
    iget-boolean v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->w:Z

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:LET;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    :cond_0
    const-string v0, "entrySpec.v2"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-nez v0, :cond_1

    .line 91
    const-string v0, "resourceSpec"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/ResourceSpec;

    .line 92
    if-eqz v0, :cond_1

    .line 93
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:LaGM;

    invoke-interface {v2, v0}, LaGM;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 96
    :cond_1
    invoke-static {v1}, LFU;->a(Landroid/os/Bundle;)Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    .line 97
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a()LH;

    move-result-object v0

    .line 159
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 160
    return-void
.end method
