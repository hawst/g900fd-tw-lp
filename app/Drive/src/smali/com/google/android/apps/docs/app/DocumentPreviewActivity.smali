.class public Lcom/google/android/apps/docs/app/DocumentPreviewActivity;
.super Lrd;
.source "DocumentPreviewActivity.java"

# interfaces
.implements LalR;
.implements Landroid/view/View$OnKeyListener;
.implements LaqG;
.implements LaqL;
.implements LvV;


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LaGv;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LaGv;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LaGv;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:I

.field public a:LQr;

.field public a:LTT;

.field public a:LUi;

.field public a:LZV;

.field private a:LaGA;

.field public a:LaGM;

.field public a:LaGR;

.field public a:Ladi;

.field public a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "LuX;",
            ">;"
        }
    .end annotation
.end field

.field public a:LaqE;

.field private final a:Lbjv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjv",
            "<",
            "LaqA;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

.field private a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

.field private a:Lcom/google/android/apps/docs/view/TouchEventSharingViewPager;

.field private a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field public a:LqF;

.field public a:LsP;

.field public a:LtK;

.field private a:Ltf;

.field private final a:Ltj;

.field public a:Ltk;

.field private a:Ltn;

.field public a:Ltp;

.field public a:LvL;

.field public a:LvU;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 158
    sget-object v0, LaGv;->g:LaGv;

    sget-object v1, LaGv;->c:LaGv;

    invoke-static {v0, v1}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmY;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ljava/util/Set;

    .line 159
    sget-object v0, LaGv;->b:LaGv;

    sget-object v1, LaGv;->i:LaGv;

    invoke-static {v0, v1}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;)LbmY;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->b:Ljava/util/Set;

    .line 164
    sget-object v0, LaGv;->b:LaGv;

    sget-object v1, LaGv;->i:LaGv;

    sget-object v2, LaGv;->c:LaGv;

    invoke-static {v0, v1, v2}, LbmY;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LbmY;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->c:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Lrd;-><init>()V

    .line 263
    new-instance v0, Ltj;

    invoke-direct {v0, p0}, Ltj;-><init>(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ltj;

    .line 271
    new-instance v0, Ltb;

    invoke-direct {v0, p0}, Ltb;-><init>(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;)V

    .line 272
    invoke-static {v0}, Lbjw;->a(Lbjv;)Lbjv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lbjv;

    .line 999
    return-void
.end method

.method private a(LaGA;Lcom/google/android/gms/drive/database/data/EntrySpec;)I
    .locals 3

    .prologue
    .line 808
    invoke-interface {p1}, LaGA;->l()Z

    move-result v0

    .line 809
    :goto_0
    if-eqz v0, :cond_1

    .line 811
    invoke-interface {p1}, LaGA;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 812
    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 813
    invoke-interface {p1}, LaGA;->b()I

    move-result v0

    .line 819
    :goto_1
    return v0

    .line 815
    :cond_0
    invoke-interface {p1}, LaGA;->k()Z

    move-result v0

    goto :goto_0

    .line 818
    :cond_1
    const-string v0, "PreviewActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "entrySpec not found: %s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 819
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private a(I)LaGA;
    .locals 1

    .prologue
    .line 868
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a()I

    move-result v0

    invoke-static {p1, v0}, LbiT;->b(II)I

    .line 869
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LaGA;

    invoke-interface {v0, p1}, LaGA;->a(I)Z

    move-result v0

    .line 870
    invoke-static {v0}, LbiT;->b(Z)V

    .line 871
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LaGA;

    return-object v0
.end method

.method private a()LaGu;
    .locals 1

    .prologue
    .line 719
    iget v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(I)LCv;

    move-result-object v0

    .line 720
    invoke-interface {v0}, LCv;->a()LaGu;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;)LaGu;
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a()LaGu;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;LaGu;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 914
    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-interface {p1}, LaGu;->j()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;Ljava/lang/String;Lcom/google/android/apps/docs/doclist/DocListQuery;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;LaGu;Lcom/google/android/apps/docs/doclist/DocListQuery;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 910
    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-interface {p1}, LaGu;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1, p2}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;Ljava/lang/String;Lcom/google/android/apps/docs/doclist/DocListQuery;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;Ljava/lang/String;Lcom/google/android/apps/docs/doclist/DocListQuery;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 896
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 897
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 901
    const-string v1, "entrySpec.v2"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 902
    const-string v1, "kindString"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 903
    if-eqz p3, :cond_0

    .line 904
    const-string v1, "docListQuery"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 906
    :cond_0
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;)Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    return-object v0
.end method

.method private a(II)V
    .locals 2

    .prologue
    .line 823
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    .line 824
    add-int v1, p1, v0

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->d(I)V

    .line 825
    sub-int v1, p1, v0

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->d(I)V

    .line 823
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 827
    :cond_0
    return-void
.end method

.method private a(LaGu;)V
    .locals 3

    .prologue
    .line 765
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->k()V

    .line 767
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LsP;

    sget-object v1, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    invoke-virtual {v0, p1, v1}, LsP;->a(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)Landroid/content/Intent;

    move-result-object v0

    .line 768
    invoke-interface {p1}, LaGu;->a()LaGv;

    move-result-object v1

    .line 769
    sget-object v2, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->c:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 771
    const-string v1, "editMode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 773
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->startActivity(Landroid/content/Intent;)V

    .line 774
    return-void
.end method

.method private a(LaGv;)V
    .locals 1

    .prologue
    .line 479
    sget-object v0, LqH;->i:LqH;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(LqH;LaGv;)V

    .line 480
    return-void
.end method

.method private a(Landroid/os/Bundle;LaGv;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 447
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a()LM;

    move-result-object v1

    .line 448
    const-string v0, "FullscreenSwitcherFragment"

    invoke-virtual {v1, v0}, LM;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    .line 450
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    if-nez v0, :cond_1

    .line 451
    :cond_0
    sget-object v0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->b:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 453
    const/4 v0, -0x1

    invoke-static {v2, v2, v3, v0}, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->a(ZZZI)Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    .line 458
    :goto_0
    invoke-virtual {v1}, LM;->a()Lac;

    move-result-object v0

    .line 459
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    const-string v2, "FullscreenSwitcherFragment"

    invoke-virtual {v0, v1, v2}, Lac;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lac;

    .line 460
    invoke-virtual {v0}, Lac;->a()I

    .line 463
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->o()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, LakQ;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 464
    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->c(Z)V

    .line 466
    :cond_2
    return-void

    .line 455
    :cond_3
    const/16 v0, 0x1770

    invoke-static {v2, v3, v3, v0}, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->a(ZZZI)Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    goto :goto_0
.end method

.method private a(Landroid/view/Menu;ZII)V
    .locals 2

    .prologue
    .line 635
    if-eqz p2, :cond_1

    .line 636
    sget v0, Lxc;->open_file:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 637
    invoke-interface {v1, p3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 638
    invoke-interface {v1, p4}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 640
    invoke-static {p0}, Lamt;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lxi;->menu_sharing:I

    if-ne p3, v0, :cond_0

    .line 641
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LuX;

    .line 642
    invoke-virtual {v0, v1}, LuX;->a(Landroid/view/MenuItem;)V

    .line 650
    :goto_0
    return-void

    .line 644
    :cond_0
    invoke-interface {v1}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    .line 645
    invoke-interface {v0}, Landroid/view/SubMenu;->clear()V

    goto :goto_0

    .line 648
    :cond_1
    sget v0, Lxc;->open_file:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->j()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;I)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->c(I)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;II)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(II)V

    return-void
.end method

.method private a(LqH;LaGv;)V
    .locals 2

    .prologue
    .line 469
    sget-object v0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->b:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 470
    new-instance v0, LqJ;

    invoke-direct {v0, p1}, LqJ;-><init>(LqH;)V

    .line 471
    invoke-virtual {v0, p2}, LqJ;->a(LaGv;)LqJ;

    move-result-object v0

    .line 472
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->b()LaFO;

    move-result-object v1

    invoke-virtual {v0, v1}, LqJ;->a(LaFO;)LqJ;

    move-result-object v0

    .line 473
    invoke-virtual {v0}, LqJ;->a()LqI;

    move-result-object v0

    .line 474
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LqF;

    invoke-virtual {v1, v0}, LqF;->a(LqI;)V

    .line 476
    :cond_0
    return-void
.end method

.method private b()LaFO;
    .locals 1

    .prologue
    .line 725
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a()LaGu;

    move-result-object v0

    .line 726
    invoke-interface {v0}, LaGu;->a()LaFM;

    move-result-object v0

    .line 727
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, LaFM;->a()LaFO;

    move-result-object v0

    goto :goto_0
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 760
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(I)LCv;

    move-result-object v0

    .line 761
    invoke-interface {v0}, LCv;->a()LaGu;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(LaGu;)V

    .line 762
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->m()V

    return-void
.end method

.method private c(I)V
    .locals 4

    .prologue
    .line 797
    iput p1, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:I

    .line 798
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lbjv;

    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaqA;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, LaqA;->a(J)V

    .line 799
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LUi;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(I)LCv;

    move-result-object v1

    invoke-interface {v1}, LCv;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-interface {v0, v1}, LUi;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 800
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 801
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 802
    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->s()V

    return-void
.end method

.method private d(I)V
    .locals 1

    .prologue
    .line 830
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a()I

    move-result v0

    .line 831
    if-ltz p1, :cond_0

    if-ge p1, v0, :cond_0

    .line 832
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->e(I)V

    .line 834
    :cond_0
    return-void
.end method

.method private e(I)V
    .locals 2

    .prologue
    .line 847
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(I)LaGA;

    move-result-object v0

    .line 848
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/utils/FetchSpec;->a(LaGA;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)Lcom/google/android/apps/docs/utils/FetchSpec;

    move-result-object v1

    .line 849
    invoke-virtual {v1}, Lcom/google/android/apps/docs/utils/FetchSpec;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 850
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lbjv;

    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaqA;

    invoke-virtual {v0, v1}, LaqA;->a(Lcom/google/android/apps/docs/utils/FetchSpec;)LbsU;

    .line 852
    :cond_0
    return-void
.end method

.method private j()V
    .locals 1

    .prologue
    .line 266
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->finish()V

    .line 269
    :cond_0
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 483
    iget v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(I)LCv;

    move-result-object v0

    invoke-interface {v0}, LCv;->a()LaGv;

    move-result-object v0

    .line 484
    sget-object v1, LqH;->k:LqH;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(LqH;LaGv;)V

    .line 485
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 488
    invoke-static {}, LakQ;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 534
    :goto_0
    return-void

    .line 492
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 493
    new-instance v1, Ltd;

    invoke-direct {v1, p0}, Ltd;-><init>(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method

.method private m()V
    .locals 4

    .prologue
    .line 734
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    if-eqz v0, :cond_0

    .line 735
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->t()V

    .line 737
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a()LaGu;

    move-result-object v0

    .line 738
    invoke-interface {v0}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "usersToInvite"

    .line 739
    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 738
    invoke-static {p0, v0, v1, v2}, LvC;->a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;ZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->startActivity(Landroid/content/Intent;)V

    .line 740
    return-void
.end method

.method private p()V
    .locals 2

    .prologue
    .line 743
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LaGA;

    if-eqz v0, :cond_0

    .line 744
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->m()V

    .line 753
    :goto_0
    return-void

    .line 746
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ltj;

    new-instance v1, Lte;

    invoke-direct {v1, p0}, Lte;-><init>(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;)V

    invoke-virtual {v0, v1}, Ltj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private q()V
    .locals 1

    .prologue
    .line 756
    iget v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->b(I)V

    .line 757
    return-void
.end method

.method private s()V
    .locals 2

    .prologue
    .line 777
    invoke-static {p0}, Lr;->a(Landroid/app/Activity;)Z

    .line 778
    iget v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(I)LCv;

    move-result-object v0

    .line 779
    invoke-interface {v0}, LCv;->a()LaGu;

    move-result-object v1

    .line 780
    if-eqz v1, :cond_0

    .line 781
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a()LaqY;

    move-result-object v0

    invoke-interface {v0, v1}, LaqY;->a(LaGu;)V

    .line 782
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Laja;

    invoke-virtual {v0}, Laja;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LuX;

    .line 783
    invoke-virtual {v0, v1}, LuX;->a(LaGu;)V

    .line 785
    :cond_0
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 864
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LaGA;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LaGA;

    invoke-interface {v0}, LaGA;->a()I

    move-result v0

    goto :goto_0
.end method

.method public a(I)LCv;
    .locals 1

    .prologue
    .line 876
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(I)LaGA;

    move-result-object v0

    return-object v0
.end method

.method public a(LaGM;Lcom/google/android/apps/docs/doclist/DocListQuery;)LaGA;
    .locals 3

    .prologue
    .line 437
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 440
    invoke-virtual {p2}, Lcom/google/android/apps/docs/doclist/DocListQuery;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()LvN;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LvL;

    invoke-interface {v1}, LvL;->b()Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v1

    invoke-virtual {v0, v1}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    move-result-object v0

    invoke-virtual {v0}, LvN;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    .line 442
    invoke-virtual {p2}, Lcom/google/android/apps/docs/doclist/DocListQuery;->a()LIK;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/docs/doclist/DocListQuery;->a()[Ljava/lang/String;

    move-result-object v2

    .line 441
    invoke-interface {p1, v0, v1, v2}, LaGM;->a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;LIK;[Ljava/lang/String;)LaGA;

    move-result-object v0

    return-object v0
.end method

.method public a(LaGM;Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGA;
    .locals 3

    .prologue
    .line 429
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 430
    new-instance v0, LvN;

    invoke-direct {v0}, LvN;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LvL;

    .line 431
    invoke-interface {v1, p2}, LvL;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v1

    .line 430
    invoke-virtual {v0, v1}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    move-result-object v0

    .line 431
    invoke-virtual {v0}, LvN;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    .line 432
    const/4 v1, 0x0

    invoke-static {}, LaER;->a()[Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LaGM;->a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;LIK;[Ljava/lang/String;)LaGA;

    move-result-object v0

    return-object v0
.end method

.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 945
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/view/TouchEventSharingViewPager;

    return-object v0
.end method

.method public a(I)Lcom/google/android/apps/docs/utils/FetchSpec;
    .locals 2

    .prologue
    .line 857
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(I)LaGA;

    move-result-object v0

    .line 858
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/utils/FetchSpec;->a(LaGA;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)Lcom/google/android/apps/docs/utils/FetchSpec;

    move-result-object v0

    .line 859
    return-object v0
.end method

.method public a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 708
    const-class v0, LaqA;

    if-ne p1, v0, :cond_1

    .line 709
    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 711
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lbjv;

    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    .line 715
    :goto_1
    return-object v0

    .line 709
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 715
    :cond_1
    invoke-super {p0, p1, p2}, Lrd;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1
.end method

.method public a(I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 989
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(I)LCv;

    move-result-object v0

    .line 991
    if-eqz v0, :cond_0

    .line 992
    sget v1, Lxi;->document_preview_page_description:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {v0}, LCv;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 996
    :goto_0
    return-object v0

    .line 994
    :cond_0
    sget v0, Lxi;->document_preview_page_error_description:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 891
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->b(I)V

    .line 892
    return-void
.end method

.method public a(LaGA;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, -0x1

    const/4 v3, 0x0

    .line 379
    if-eqz p1, :cond_0

    invoke-interface {p1}, LaGA;->a()I

    move-result v0

    if-nez v0, :cond_1

    .line 380
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->j()V

    .line 426
    :goto_0
    return-void

    .line 384
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LaGA;

    .line 385
    if-eqz p2, :cond_2

    .line 386
    const-string v0, "position"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 387
    const-string v0, "entrySpec.v2"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 388
    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(ILcom/google/android/gms/drive/database/data/EntrySpec;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 394
    :goto_1
    if-ne v0, v1, :cond_3

    .line 395
    iput-object v3, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LaGA;

    .line 396
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->j()V

    goto :goto_0

    .line 392
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(LaGA;Lcom/google/android/gms/drive/database/data/EntrySpec;)I

    move-result v0

    goto :goto_1

    .line 400
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LvU;

    invoke-interface {v1, p0}, LvU;->a(LvV;)V

    .line 401
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->c(I)V

    .line 402
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->s()V

    .line 403
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(I)LCv;

    move-result-object v0

    invoke-interface {v0}, LCv;->a()LaGv;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(LaGv;)V

    .line 404
    new-instance v0, Lcom/google/android/apps/docs/view/PreviewPagerAdapter;

    .line 405
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a()LM;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter;-><init>(LM;LaqG;)V

    .line 407
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/view/TouchEventSharingViewPager;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/view/TouchEventSharingViewPager;->setAdapter(LdN;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 418
    iget v1, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:I

    .line 419
    const/4 v2, 0x5

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(II)V

    .line 421
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/view/TouchEventSharingViewPager;

    iget v2, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/docs/view/TouchEventSharingViewPager;->setCurrentItem(I)V

    .line 422
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/view/TouchEventSharingViewPager;

    new-instance v2, Ltm;

    invoke-direct {v2, p0, v3}, Ltm;-><init>(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;Ltb;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/docs/view/TouchEventSharingViewPager;->setOnPageChangeListener(LeT;)V

    .line 423
    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/PreviewPagerAdapter;->a()V

    .line 424
    invoke-static {p0}, Lr;->a(Landroid/app/Activity;)Z

    .line 425
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ltj;

    invoke-virtual {v0}, Ltj;->a()V

    goto :goto_0

    .line 408
    :catch_0
    move-exception v0

    .line 412
    const-string v1, "PreviewActivity"

    const-string v2, "Fragment manager error."

    invoke-static {v1, v0, v2}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 414
    iput-object v3, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LaGA;

    .line 415
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->j()V

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public a(Ltl;)V
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ltj;

    invoke-virtual {v0, p1}, Ltj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    return-void
.end method

.method public a(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 881
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(I)LCv;

    move-result-object v1

    .line 882
    if-eqz v1, :cond_0

    invoke-interface {v1}, LCv;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 883
    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LTT;

    invoke-interface {v1}, LCv;->a()LaGu;

    move-result-object v1

    invoke-interface {v2, v1}, LTT;->d(LaGu;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 886
    :cond_0
    return v0
.end method

.method public a(ILcom/google/android/gms/drive/database/data/EntrySpec;)Z
    .locals 1

    .prologue
    .line 368
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a()I

    move-result v0

    if-ge p1, v0, :cond_0

    if-eqz p2, :cond_0

    .line 369
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(I)LCv;

    move-result-object v0

    .line 370
    invoke-interface {v0}, LCv;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 371
    const/4 v0, 0x1

    .line 374
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 940
    iget v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:I

    return v0
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 950
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a()LaqY;

    move-result-object v0

    invoke-interface {v0, p1}, LaqY;->a(Z)V

    .line 951
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 919
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    if-nez v0, :cond_0

    .line 928
    :goto_0
    return-void

    .line 922
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->n()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 923
    :goto_1
    if-eqz v0, :cond_2

    .line 924
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->t()V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 922
    goto :goto_1

    .line 926
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->a(Z)V

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 935
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LaGA;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected i()V
    .locals 1

    .prologue
    .line 605
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LZV;

    invoke-virtual {v0}, LZV;->b()V

    .line 606
    invoke-super {p0}, Lrd;->i()V

    .line 607
    return-void
.end method

.method public n()V
    .locals 1

    .prologue
    .line 1029
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LvU;

    invoke-interface {v0}, LvU;->a()V

    .line 1030
    return-void
.end method

.method public o()V
    .locals 0

    .prologue
    .line 979
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->s()V

    .line 980
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 281
    invoke-super {p0, p1}, Lrd;->onCreate(Landroid/os/Bundle;)V

    .line 282
    new-instance v0, Ltf;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Ltf;-><init>(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;Ltb;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ltf;

    .line 283
    invoke-static {p0}, LUs;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 284
    invoke-static {}, LakQ;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 285
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->requestWindowFeature(I)Z

    .line 290
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LQr;

    const-string v2, "maxPreviewImageLongerSizePixels"

    const/16 v3, 0x640

    invoke-interface {v0, v2, v3}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    .line 293
    new-instance v2, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    invoke-direct {v2, v0, v0}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;-><init>(II)V

    iput-object v2, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    .line 296
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 297
    const-string v0, "entrySpec.v2"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 298
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    const-string v3, "Entry not specified"

    invoke-static {v0, v3}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    const-string v0, "kindString"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 301
    if-eqz v3, :cond_6

    move v0, v1

    :goto_1
    invoke-static {v0}, LbiT;->a(Z)V

    .line 302
    invoke-static {v3}, LaGv;->a(Ljava/lang/String;)LaGv;

    move-result-object v1

    .line 304
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ltk;

    invoke-virtual {v0}, Ltk;->a()LaGA;

    move-result-object v3

    .line 305
    if-nez v3, :cond_1

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ltk;

    invoke-virtual {v0}, Ltk;->a()V

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LaGR;

    new-instance v4, Ltc;

    invoke-direct {v4, p0, v2, p1}, Ltc;-><init>(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;Landroid/content/Intent;Landroid/os/Bundle;)V

    invoke-virtual {v0, v4}, LaGR;->a(LaGN;)V

    .line 331
    :cond_1
    sget v0, Lxe;->preview_activity:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->setContentView(I)V

    .line 333
    sget v0, Lxc;->document_pager:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/TouchEventSharingViewPager;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/view/TouchEventSharingViewPager;

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/view/TouchEventSharingViewPager;

    iget-object v4, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ltp;

    invoke-virtual {v4}, Ltp;->a()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/docs/view/TouchEventSharingViewPager;->setOffscreenPageLimit(I)V

    .line 335
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/view/TouchEventSharingViewPager;

    .line 336
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lxa;->document_preview_pager_margin:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    .line 335
    invoke-virtual {v0, v4}, Lcom/google/android/apps/docs/view/TouchEventSharingViewPager;->setPageMargin(I)V

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/view/TouchEventSharingViewPager;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/view/TouchEventSharingViewPager;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 339
    invoke-static {p0}, LUs;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 340
    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(Landroid/os/Bundle;LaGv;)V

    .line 341
    invoke-static {}, LakQ;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 342
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a()LaqY;

    move-result-object v0

    .line 343
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v4, Lxd;->projector_actionbar_opacity:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 342
    invoke-interface {v0, v1}, LaqY;->b(I)V

    .line 347
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->l()V

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ltf;

    invoke-static {v0}, Ltf;->a(Ltf;)V

    .line 349
    if-eqz v3, :cond_3

    .line 350
    invoke-virtual {p0, v3, p1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(LaGA;Landroid/os/Bundle;)V

    .line 352
    :cond_3
    if-nez p1, :cond_4

    const-string v0, "usersToInvite"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 353
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->p()V

    .line 355
    :cond_4
    return-void

    .line 287
    :cond_5
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->requestWindowFeature(I)Z

    goto/16 :goto_0

    .line 301
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 628
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 629
    sget v1, Lxf;->menu_document_preview:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 630
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 611
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LvU;

    invoke-interface {v0, p0}, LvU;->b(LvV;)V

    .line 613
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LaGA;

    if-eqz v0, :cond_0

    .line 615
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LaGA;

    invoke-interface {v0}, LaGA;->a()V

    .line 619
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lbjv;

    invoke-interface {v0}, Lbjv;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaqA;

    invoke-virtual {v0}, LaqA;->a()V

    .line 621
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ltf;

    invoke-static {v0}, Ltf;->b(Ltf;)V

    .line 623
    invoke-super {p0}, Lrd;->onDestroy()V

    .line 624
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 955
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_2

    .line 956
    sparse-switch p2, :sswitch_data_0

    move v0, v1

    .line 974
    :cond_0
    :goto_0
    return v0

    .line 959
    :sswitch_0
    sget v2, Lxc;->open_file:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 960
    if-eqz v2, :cond_1

    .line 961
    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    .line 962
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    if-eqz v1, :cond_0

    .line 963
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/fragment/FullscreenSwitcherFragment;->t()V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 967
    goto :goto_0

    :cond_2
    move v0, v1

    .line 974
    goto :goto_0

    .line 956
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x3e -> :sswitch_0
    .end sparse-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 693
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lxc;->open_detail_panel:I

    if-ne v1, v2, :cond_1

    .line 694
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->p()V

    .line 703
    :cond_0
    :goto_0
    return v0

    .line 696
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lxc;->open_file:I

    if-ne v1, v2, :cond_2

    .line 697
    invoke-interface {p1}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/SubMenu;->hasVisibleItems()Z

    move-result v1

    if-nez v1, :cond_0

    .line 698
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->q()V

    goto :goto_0

    .line 703
    :cond_2
    invoke-super {p0, p1}, Lrd;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 589
    invoke-super {p0}, Lrd;->onPause()V

    .line 591
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ltn;

    if-eqz v0, :cond_0

    .line 592
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ltj;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ltn;

    invoke-virtual {v0, v1}, Ltj;->a(Ljava/lang/Object;)V

    .line 593
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ltn;

    .line 595
    :cond_0
    return-void
.end method

.method protected onPostResume()V
    .locals 1

    .prologue
    .line 599
    invoke-super {p0}, Lrd;->onPostResume()V

    .line 600
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LZV;

    invoke-virtual {v0}, LZV;->a()V

    .line 601
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 654
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LaGA;

    if-nez v0, :cond_1

    .line 688
    :cond_0
    :goto_0
    return v1

    .line 657
    :cond_1
    iget v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(I)LCv;

    move-result-object v0

    invoke-interface {v0}, LCv;->a()LaGv;

    move-result-object v0

    .line 658
    sget-object v2, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->b:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 660
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LtK;

    sget-object v2, Lry;->x:Lry;

    .line 662
    invoke-interface {v0, v2}, LtK;->a(LtJ;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    sget v2, Lxi;->menu_edit:I

    sget v3, Lxb;->ic_menu_edit_alpha:I

    .line 660
    invoke-direct {p0, p1, v0, v2, v3}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(Landroid/view/Menu;ZII)V

    .line 682
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LtK;

    sget-object v2, Lry;->s:Lry;

    .line 683
    invoke-interface {v0, v2}, LtK;->a(LtJ;)Z

    move-result v0

    .line 684
    sget v2, Lxc;->open_detail_panel:I

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 685
    if-eqz v2, :cond_0

    if-nez v0, :cond_0

    .line 686
    sget v0, Lxc;->open_detail_panel:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_0

    .line 662
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 665
    :cond_3
    sget-object v2, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 667
    sget v0, Lxi;->thumbnail_open:I

    sget v2, Lxb;->ic_open_in_alpha:I

    invoke-direct {p0, p1, v1, v0, v2}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(Landroid/view/Menu;ZII)V

    goto :goto_2

    .line 673
    :cond_4
    invoke-static {p0}, Lamt;->a(Landroid/content/Context;)Z

    move-result v3

    .line 675
    if-eqz v3, :cond_5

    sget v0, Lxi;->menu_sharing:I

    move v2, v0

    :goto_3
    if-eqz v3, :cond_6

    sget v0, Lxb;->ic_sharing_alpha:I

    :goto_4
    invoke-direct {p0, p1, v1, v2, v0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(Landroid/view/Menu;ZII)V

    goto :goto_2

    :cond_5
    sget v0, Lxi;->menu_download:I

    move v2, v0

    goto :goto_3

    :cond_6
    sget v0, Lxb;->ic_download_alpha:I

    goto :goto_4
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 572
    invoke-super {p0}, Lrd;->onResume()V

    .line 573
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LaGA;

    if-eqz v0, :cond_2

    .line 574
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ltn;

    if-nez v0, :cond_1

    .line 575
    new-instance v0, Ltn;

    invoke-direct {v0, p0, v2}, Ltn;-><init>(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;Ltb;)V

    invoke-virtual {v0}, Ltn;->a()V

    .line 585
    :cond_0
    :goto_0
    return-void

    .line 577
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ltn;

    invoke-virtual {v0}, Ltn;->a()V

    .line 578
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ltj;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ltn;

    invoke-virtual {v0, v1}, Ltj;->a(Ljava/lang/Object;)V

    .line 579
    iput-object v2, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ltn;

    goto :goto_0

    .line 581
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ltn;

    if-nez v0, :cond_0

    .line 582
    new-instance v0, Ltn;

    invoke-direct {v0, p0, v2}, Ltn;-><init>(Lcom/google/android/apps/docs/app/DocumentPreviewActivity;Ltb;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ltn;

    .line 583
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ltj;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:Ltn;

    invoke-virtual {v0, v1}, Ltj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 838
    invoke-super {p0, p1}, Lrd;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 839
    const-string v0, "position"

    iget v1, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 840
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:LaGA;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 841
    const-string v0, "entrySpec.v2"

    iget v1, p0, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a:I

    .line 842
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->a(I)LCv;

    move-result-object v1

    invoke-interface {v1}, LCv;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    .line 841
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 844
    :cond_0
    return-void
.end method

.method public r()V
    .locals 0

    .prologue
    .line 984
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentPreviewActivity;->s()V

    .line 985
    return-void
.end method
