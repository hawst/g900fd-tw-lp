.class public abstract Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "SelectionDialogFragment.java"

# interfaces
.implements Lacd;


# instance fields
.field private final a:Lacb;

.field private a:[Ljava/lang/String;

.field private m:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    .line 28
    new-instance v0, Lacb;

    invoke-direct {v0}, Lacb;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a:Lacb;

    return-void
.end method

.method public static a(Landroid/os/Bundle;I[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 49
    const-string v0, "titleId"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 50
    const-string v0, "choiceItems"

    invoke-virtual {p0, v0, p2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 51
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a()LH;

    move-result-object v0

    invoke-static {v0}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->m:I

    .line 66
    invoke-virtual {v0, v1}, LEU;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a:[Ljava/lang/String;

    const/4 v2, 0x0

    .line 67
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a:Lacb;

    .line 69
    invoke-virtual {v2, p0}, Lacb;->a(Lacd;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    .line 68
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    .line 70
    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 71
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a:Lacb;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v1, v2}, Lacb;->a(Landroid/widget/AdapterView;)V

    .line 74
    return-object v0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 55
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a_(Landroid/os/Bundle;)V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "titleId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->m:I

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "choiceItems"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a:[Ljava/lang/String;

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a:Lacb;

    invoke-virtual {v0, p1}, Lacb;->a(Landroid/os/Bundle;)V

    .line 61
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a:Lacb;

    invoke-virtual {v0, p1}, Lacb;->a(I)V

    .line 91
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 79
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->c(Landroid/os/Bundle;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a:Lacb;

    invoke-virtual {v0, p1}, Lacb;->b(Landroid/os/Bundle;)V

    .line 81
    return-void
.end method

.method public o_()V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->o_()V

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a:Lacb;

    invoke-virtual {v0}, Lacb;->a()V

    .line 87
    return-void
.end method
