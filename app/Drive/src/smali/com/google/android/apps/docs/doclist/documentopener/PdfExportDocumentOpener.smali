.class public Lcom/google/android/apps/docs/doclist/documentopener/PdfExportDocumentOpener;
.super Ljava/lang/Object;
.source "PdfExportDocumentOpener.java"

# interfaces
.implements LFR;


# instance fields
.field private final a:Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;


# direct methods
.method public constructor <init>(LHm;LGd;)V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    sget-object v0, LGj;->a:LGj;

    sget v1, Lxi;->exporting_document:I

    .line 27
    invoke-virtual {p2, v0, v1}, LGd;->a(LGj;I)Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    move-result-object v0

    .line 26
    invoke-interface {p1, v0}, LHm;->a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;)Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/PdfExportDocumentOpener;->a:Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;

    .line 28
    return-void
.end method


# virtual methods
.method public a(LFT;LaGo;Landroid/os/Bundle;)LbsU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LFT;",
            "LaGo;",
            "Landroid/os/Bundle;",
            ")",
            "LbsU",
            "<",
            "LDL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/PdfExportDocumentOpener;->a:Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;->a(LFT;LaGo;Landroid/os/Bundle;)LbsU;

    move-result-object v0

    return-object v0
.end method
