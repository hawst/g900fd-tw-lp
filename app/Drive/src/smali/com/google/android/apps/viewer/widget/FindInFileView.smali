.class public Lcom/google/android/apps/viewer/widget/FindInFileView;
.super Landroid/widget/LinearLayout;
.source "FindInFileView.java"


# instance fields
.field private final a:Landroid/text/TextWatcher;

.field private final a:Landroid/view/View$OnClickListener;

.field private a:Landroid/view/View;

.field private final a:Landroid/widget/TextView$OnEditorActionListener;

.field private a:Landroid/widget/TextView;

.field private a:LaxV;

.field private b:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/viewer/widget/FindInFileView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 101
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 104
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    new-instance v0, LaxS;

    invoke-direct {v0, p0}, LaxS;-><init>(Lcom/google/android/apps/viewer/widget/FindInFileView;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/widget/FindInFileView;->a:Landroid/view/View$OnClickListener;

    .line 64
    new-instance v0, LaxT;

    invoke-direct {v0, p0}, LaxT;-><init>(Lcom/google/android/apps/viewer/widget/FindInFileView;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/widget/FindInFileView;->a:Landroid/text/TextWatcher;

    .line 86
    new-instance v0, LaxU;

    invoke-direct {v0, p0}, LaxU;-><init>(Lcom/google/android/apps/viewer/widget/FindInFileView;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/widget/FindInFileView;->a:Landroid/widget/TextView$OnEditorActionListener;

    .line 106
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lauk;->find_in_file:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 108
    sget v0, Lauj;->find_query_box:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/widget/FindInFileView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/viewer/widget/FindInFileView;->a:Landroid/widget/TextView;

    .line 109
    sget v0, Lauj;->find_prev_btn:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/widget/FindInFileView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/viewer/widget/FindInFileView;->a:Landroid/view/View;

    .line 110
    sget v0, Lauj;->find_next_btn:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/widget/FindInFileView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/viewer/widget/FindInFileView;->b:Landroid/view/View;

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/viewer/widget/FindInFileView;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/viewer/widget/FindInFileView;->a:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/viewer/widget/FindInFileView;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/viewer/widget/FindInFileView;->a:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/viewer/widget/FindInFileView;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/viewer/widget/FindInFileView;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/viewer/widget/FindInFileView;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/viewer/widget/FindInFileView;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/widget/FindInFileView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/viewer/widget/FindInFileView;->a:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/widget/FindInFileView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/viewer/widget/FindInFileView;->a:Landroid/widget/TextView;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/widget/FindInFileView;)LaxV;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/viewer/widget/FindInFileView;->a:LaxV;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/viewer/widget/FindInFileView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/viewer/widget/FindInFileView;->b:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public setFindInFileListener(LaxV;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/google/android/apps/viewer/widget/FindInFileView;->a:LaxV;

    .line 120
    return-void
.end method
