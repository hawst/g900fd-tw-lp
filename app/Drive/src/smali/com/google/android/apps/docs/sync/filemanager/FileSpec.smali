.class public Lcom/google/android/apps/docs/sync/filemanager/FileSpec;
.super Ljava/lang/Object;
.source "FileSpec.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/docs/sync/filemanager/FileSpec;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final a:LacY;

.field public final a:Lcom/google/android/gms/drive/database/data/EntrySpec;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, LadK;

    invoke-direct {v0}, LadK;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/database/data/EntrySpec;JLacY;)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 40
    iput-object p1, p0, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 41
    iput-wide p2, p0, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->a:J

    .line 42
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacY;

    iput-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->a:LacY;

    .line 43
    return-void

    .line 39
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 59
    instance-of v1, p1, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;

    if-nez v1, :cond_1

    .line 65
    :cond_0
    :goto_0
    return v0

    .line 62
    :cond_1
    check-cast p1, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;

    .line 63
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v2, p1, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-static {v1, v2}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->a:J

    iget-wide v4, p1, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->a:LacY;

    iget-object v2, p1, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->a:LacY;

    .line 65
    invoke-virtual {v1, v2}, LacY;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 70
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->a:LacY;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 75
    const-string v0, "FileSpec[%s %d %s]"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->a:J

    .line 76
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->a:LacY;

    aput-object v3, v1, v2

    .line 75
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 53
    iget-wide v0, p0, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->a:LacY;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 55
    return-void
.end method
