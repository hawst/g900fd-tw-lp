.class public Lcom/google/android/apps/docs/app/TestFragmentActivity;
.super Lrm;
.source "TestFragmentActivity.java"

# interfaces
.implements Lue;


# instance fields
.field private a:Landroid/content/Intent;

.field private a:Lue;

.field private i:Z

.field private j:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lrm;-><init>()V

    .line 19
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/TestFragmentActivity;->j:Z

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/apps/docs/app/TestFragmentActivity;->a:Lue;

    if-nez v0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 27
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/TestFragmentActivity;->a:Lue;

    invoke-interface {v0, p1, p2}, Lue;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 67
    iput-object p2, p0, Lcom/google/android/apps/docs/app/TestFragmentActivity;->a:Landroid/content/Intent;

    .line 68
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/TestFragmentActivity;->j:Z

    if-eqz v0, :cond_0

    .line 69
    invoke-super {p0, p1, p2, p3}, Lrm;->a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    .line 71
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/TestFragmentActivity;->i:Z

    return v0
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/android/apps/docs/app/TestFragmentActivity;->a:Landroid/content/Intent;

    .line 51
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/TestFragmentActivity;->j:Z

    if-eqz v0, :cond_0

    .line 52
    invoke-super {p0, p1}, Lrm;->startActivity(Landroid/content/Intent;)V

    .line 54
    :cond_0
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/apps/docs/app/TestFragmentActivity;->a:Landroid/content/Intent;

    .line 59
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/TestFragmentActivity;->j:Z

    if-eqz v0, :cond_0

    .line 60
    invoke-super {p0, p1, p2}, Lrm;->startActivityForResult(Landroid/content/Intent;I)V

    .line 62
    :cond_0
    return-void
.end method
