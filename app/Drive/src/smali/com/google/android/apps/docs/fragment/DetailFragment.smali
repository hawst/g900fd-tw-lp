.class public abstract Lcom/google/android/apps/docs/fragment/DetailFragment;
.super Lcom/google/android/apps/docs/app/BaseFragment;
.source "DetailFragment.java"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseFragment;-><init>()V

    .line 29
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/DetailFragment;->a:Ljava/util/List;

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DetailFragment;->a:Z

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DetailFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 41
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DetailFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lxa;->detail_fragment_width:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 42
    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public a(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DetailFragment;->a:Z

    if-eqz v0, :cond_0

    .line 34
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DetailFragment;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 36
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DetailFragment;->a:Z

    if-eqz v0, :cond_0

    .line 47
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 51
    :goto_0
    return-void

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DetailFragment;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public abstract a(Z)V
.end method

.method public e()V
    .locals 1

    .prologue
    .line 67
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseFragment;->e()V

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DetailFragment;->a:Z

    .line 70
    return-void
.end method

.method public f(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 55
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseFragment;->f(Landroid/os/Bundle;)V

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DetailFragment;->a:Z

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DetailFragment;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 60
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DetailFragment;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 63
    return-void
.end method

.method public abstract m()Z
.end method
