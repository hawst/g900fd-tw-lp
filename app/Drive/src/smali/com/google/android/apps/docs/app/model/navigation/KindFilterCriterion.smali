.class public Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;
.super Ljava/lang/Object;
.source "KindFilterCriterion.java"

# interfaces
.implements Lcom/google/android/apps/docs/app/model/navigation/Criterion;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "LaGv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lwe;

    invoke-direct {v0}, Lwe;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LbmY;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "LaGv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-virtual {p1}, LbmY;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "allowedKinds is empty"

    invoke-static {v0, v1}, LbiT;->a(ZLjava/lang/Object;)V

    .line 24
    invoke-static {p1}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;->a:LbmY;

    .line 25
    return-void

    .line 23
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(LaGM;)LaeZ;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;->a:LbmY;

    invoke-virtual {v0}, LbmY;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;->a:LbmY;

    invoke-virtual {v0}, LbmY;->size()I

    move-result v0

    if-ne v0, v1, :cond_2

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;->a:LbmY;

    invoke-static {v0}, Lbnm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGv;

    .line 68
    sget-object v1, LaGv;->a:LaGv;

    invoke-virtual {v0, v1}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 69
    sget-object v0, LaeZ;->c:LaeZ;

    .line 80
    :goto_1
    return-object v0

    .line 63
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 71
    :cond_1
    invoke-static {v0}, LaeZ;->a(LaGv;)LaeZ;

    move-result-object v0

    goto :goto_1

    .line 73
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;->a:LbmY;

    const-class v1, LaGv;

    invoke-static {v1}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-virtual {v0, v1}, LbmY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;->a:LbmY;

    sget-object v1, LaGv;->a:LaGv;

    .line 74
    invoke-virtual {v0, v1}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 78
    :cond_3
    sget-object v0, LaeZ;->a:LaeZ;

    goto :goto_1

    .line 80
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;->a:LbmY;

    invoke-static {v0}, LaeZ;->b(LbmY;)LaeZ;

    move-result-object v0

    goto :goto_1
.end method

.method public a(LvR;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LvR",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;->a:LbmY;

    invoke-interface {p1, v0}, LvR;->a(LbmY;)V

    .line 112
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x1

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 38
    if-ne p1, p0, :cond_0

    .line 39
    const/4 v0, 0x1

    .line 44
    :goto_0
    return v0

    .line 40
    :cond_0
    instance-of v0, p1, Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;

    if-eqz v0, :cond_1

    .line 41
    check-cast p1, Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;

    .line 42
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;->a:LbmY;

    iget-object v1, p1, Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;->a:LbmY;

    invoke-virtual {v0, v1}, LbmY;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 44
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 50
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;->a:LbmY;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 55
    const-class v0, Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;

    invoke-static {v0}, LbiL;->a(Ljava/lang/Class;)LbiN;

    move-result-object v0

    const-string v1, "allowedKinds"

    iget-object v2, p0, Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;->a:LbmY;

    .line 56
    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;Ljava/lang/Object;)LbiN;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, LbiN;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;->a:LbmY;

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/Collection;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 107
    return-void
.end method
