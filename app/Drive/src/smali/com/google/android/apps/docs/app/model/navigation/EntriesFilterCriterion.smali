.class public Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;
.super Ljava/lang/Object;
.source "EntriesFilterCriterion.java"

# interfaces
.implements Lcom/google/android/apps/docs/app/model/navigation/Criterion;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LCl;

.field private final a:Z

.field private final b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    new-instance v0, LvS;

    invoke-direct {v0}, LvS;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LCl;ZZ)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCl;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->a:LCl;

    .line 40
    iput-boolean p2, p0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->a:Z

    .line 41
    iput-boolean p3, p0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->b:Z

    .line 42
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCl;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->a:LCl;

    .line 91
    invoke-virtual {p1}, Landroid/os/Parcel;->createBooleanArray()[Z

    move-result-object v0

    .line 92
    const/4 v1, 0x0

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->a:Z

    .line 93
    const/4 v1, 0x1

    aget-boolean v0, v0, v1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->b:Z

    .line 94
    return-void
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;LvS;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public a()LCl;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->a:LCl;

    return-object v0
.end method

.method public a(LaGM;)LaeZ;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->a:LCl;

    invoke-interface {v0}, LCl;->a()LaeZ;

    move-result-object v0

    return-object v0
.end method

.method public a(LvR;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LvR",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->a:LCl;

    iget-boolean v1, p0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->b:Z

    invoke-interface {p1, v0, v1}, LvR;->a(LCl;Z)V

    .line 131
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->a:Z

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->b:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 58
    if-ne p1, p0, :cond_1

    .line 64
    :cond_0
    :goto_0
    return v0

    .line 60
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;

    if-eqz v2, :cond_3

    .line 61
    check-cast p1, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;

    .line 62
    iget-object v2, p0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->a:LCl;

    iget-object v3, p1, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->a:LCl;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->a:Z

    iget-boolean v3, p1, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->a:Z

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 64
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 74
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->a:Z

    .line 75
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->a:LCl;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 74
    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 117
    const-string v0, "EntriesFilterCriterion {filter=%s, isInheritable=%s, isMainFilter=%s}"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->a:LCl;

    .line 119
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->a:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-boolean v3, p0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->b:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .line 117
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->a:LCl;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 86
    const/4 v0, 0x2

    new-array v0, v0, [Z

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->a:Z

    aput-boolean v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;->b:Z

    aput-boolean v2, v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 87
    return-void
.end method
