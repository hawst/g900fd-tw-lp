.class public Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;
.super LH;
.source "WelcomeActivity.java"


# instance fields
.field private a:Landroid/support/v4/view/ViewPager;

.field private a:Landroid/view/View;

.field private a:Landroid/view/ViewGroup;

.field private a:LxH;

.field private a:LxJ;

.field private b:Landroid/view/View;

.field private b:Landroid/view/ViewGroup;

.field private c:Landroid/view/View;

.field private c:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, LH;-><init>()V

    .line 249
    return-void
.end method

.method private static a(FII)I
    .locals 8

    .prologue
    .line 269
    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    .line 270
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    .line 271
    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    .line 272
    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    .line 274
    invoke-static {p2}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    .line 275
    invoke-static {p2}, Landroid/graphics/Color;->red(I)I

    move-result v5

    .line 276
    invoke-static {p2}, Landroid/graphics/Color;->green(I)I

    move-result v6

    .line 277
    invoke-static {p2}, Landroid/graphics/Color;->blue(I)I

    move-result v7

    .line 279
    sub-int/2addr v4, v0

    int-to-float v4, v4

    mul-float/2addr v4, p0

    float-to-int v4, v4

    add-int/2addr v0, v4

    shl-int/lit8 v0, v0, 0x18

    sub-int v4, v5, v1

    int-to-float v4, v4

    mul-float/2addr v4, p0

    float-to-int v4, v4

    add-int/2addr v1, v4

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    sub-int v1, v6, v2

    int-to-float v1, v1

    mul-float/2addr v1, p0

    float-to-int v1, v1

    add-int/2addr v1, v2

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    sub-int v1, v7, v3

    int-to-float v1, v1

    mul-float/2addr v1, p0

    float-to-int v1, v1

    add-int/2addr v1, v3

    or-int/2addr v0, v1

    return v0
.end method

.method public static a(Landroid/content/Context;LxJ;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 54
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 55
    invoke-static {v0, p1}, LxJ;->a(Landroid/content/Intent;LxJ;)V

    .line 56
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->b:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;)LxJ;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:LxJ;

    return-object v0
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:LxJ;

    invoke-virtual {v0}, LxJ;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    .line 157
    :goto_0
    if-eqz v0, :cond_1

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->b(Landroid/view/View;)V

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->b:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->b(Landroid/view/View;)V

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->c:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->c(Landroid/view/View;)V

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 167
    :goto_1
    return-void

    .line 155
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->c(Landroid/view/View;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->b:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->c(Landroid/view/View;)V

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->c:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->b(Landroid/view/View;)V

    goto :goto_1
.end method

.method private a(IF)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:LxJ;

    .line 190
    invoke-virtual {v1, p1, p0}, LxJ;->a(ILandroid/content/Context;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:LxJ;

    add-int/lit8 v3, p1, 0x1

    .line 191
    invoke-virtual {v2, v3, p0}, LxJ;->a(ILandroid/content/Context;)I

    move-result v2

    .line 188
    invoke-static {p2, v1, v2}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a(FII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:LxJ;

    invoke-virtual {v0}, LxJ;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    if-ne p1, v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->c:Landroid/view/ViewGroup;

    sub-float v1, v4, p2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:Landroid/support/v4/view/ViewPager;

    sub-float v1, v4, p2

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAlpha(F)V

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:Landroid/support/v4/view/ViewPager;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:LxJ;

    invoke-virtual {v0}, LxJ;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:Landroid/support/v4/view/ViewPager;

    add-int/lit8 v2, p1, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 203
    :goto_0
    invoke-static {v1, p2}, Lcom/google/android/apps/docs/common/welcome/WelcomePageFragment;->a(Landroid/view/View;F)V

    .line 204
    if-eqz v0, :cond_1

    .line 205
    sub-float v1, p2, v4

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/common/welcome/WelcomePageFragment;->a(Landroid/view/View;F)V

    .line 207
    :cond_1
    return-void

    .line 200
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 178
    if-eqz p1, :cond_0

    invoke-static {p0}, LUs;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    .line 180
    if-eqz v0, :cond_0

    .line 181
    invoke-static {p0, p1, v0}, LUs;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 184
    :cond_0
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;I)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a(I)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;IF)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a(IF)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a(Landroid/view/View;)V

    return-void
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 170
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 172
    if-ne v1, p1, :cond_0

    sget v2, Lxz;->b:I

    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 170
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 172
    :cond_0
    sget v2, Lxz;->a:I

    goto :goto_1

    .line 175
    :cond_1
    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 219
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    :goto_0
    return-void

    .line 222
    :cond_0
    invoke-static {p1}, Lxm;->b(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;I)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->b(I)V

    return-void
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:LxJ;

    invoke-virtual {v0}, LxJ;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    .line 212
    invoke-virtual {p0}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->finish()V

    .line 216
    :goto_0
    return-void

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:LxJ;

    invoke-virtual {v1, p1, p0}, LxJ;->a(ILandroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method private c(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 226
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 230
    :goto_0
    return-void

    .line 229
    :cond_0
    invoke-static {p1}, Lxm;->a(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;I)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->c(I)V

    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 110
    invoke-virtual {p0}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:LxJ;

    invoke-virtual {v1}, LxJ;->a()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 112
    new-instance v1, LxE;

    invoke-direct {v1, p0}, LxE;-><init>(Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 129
    iget-object v1, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 130
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a(Landroid/view/View;)V

    .line 131
    new-array v1, v3, [F

    fill-array-data v1, :array_0

    invoke-static {v0, v1}, Lxm;->a(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v1

    invoke-static {v1}, Lxm;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:Landroid/support/v4/view/ViewPager;

    new-array v3, v3, [F

    fill-array-data v3, :array_1

    .line 132
    invoke-static {v2, v3}, Lxm;->a(Landroid/view/View;[F)Landroid/animation/Animator;

    move-result-object v2

    invoke-virtual {v1, v2}, Lxr;->a(Landroid/animation/Animator;)Lxr;

    move-result-object v1

    const/16 v2, 0x5dc

    .line 133
    invoke-virtual {v1, v2}, Lxr;->a(I)Lxr;

    move-result-object v1

    .line 134
    invoke-virtual {v1}, Lxr;->a()Lxr;

    move-result-object v1

    new-instance v2, LxF;

    invoke-direct {v2, p0, v0}, LxF;-><init>(Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;Landroid/view/View;)V

    .line 135
    invoke-virtual {v1, v2}, Lxr;->a(Landroid/animation/Animator$AnimatorListener;)Lxr;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    .line 143
    invoke-virtual {v0, v2, v3}, Lxr;->a(J)Landroid/animation/Animator;

    .line 144
    return-void

    .line 131
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private f()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 147
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:LxJ;

    invoke-virtual {v1}, LxJ;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 148
    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 149
    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 150
    iget-object v2, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->b:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1, v3, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 147
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 152
    :cond_0
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 61
    invoke-super {p0, p1}, LH;->onCreate(Landroid/os/Bundle;)V

    .line 62
    sget v0, LxB;->a:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->setContentView(I)V

    .line 64
    invoke-virtual {p0}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, LxJ;->a(Landroid/content/Intent;)LxJ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:LxJ;

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:LxJ;

    if-nez v0, :cond_1

    .line 66
    invoke-virtual {p0}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->finish()V

    .line 107
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    sget v0, LxA;->h:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:Landroid/view/ViewGroup;

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:LxJ;

    invoke-virtual {v1, v2, p0}, LxJ;->a(ILandroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 73
    sget v0, LxA;->a:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->c:Landroid/view/ViewGroup;

    .line 74
    sget v0, LxA;->f:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:Landroid/view/View;

    .line 75
    sget v0, LxA;->b:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->c:Landroid/view/View;

    .line 76
    sget v0, LxA;->d:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->b:Landroid/view/View;

    .line 77
    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a(I)V

    .line 79
    sget v0, LxA;->c:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->b:Landroid/view/ViewGroup;

    .line 80
    invoke-direct {p0}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->f()V

    .line 81
    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->b(I)V

    .line 83
    sget v0, LxA;->e:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:Landroid/support/v4/view/ViewPager;

    .line 84
    new-instance v0, LxH;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a()LM;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LxH;-><init>(Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;LM;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:LxH;

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:LxH;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LdN;)V

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:Landroid/support/v4/view/ViewPager;

    new-instance v1, LxG;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, LxG;-><init>(Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;LxC;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LeT;)V

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 89
    new-instance v0, LxC;

    invoke-direct {v0, p0}, LxC;-><init>(Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;)V

    .line 95
    iget-object v1, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    iget-object v1, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->c:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->b:Landroid/view/View;

    new-instance v1, LxD;

    invoke-direct {v1, p0}, LxD;-><init>(Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->a:LxJ;

    invoke-virtual {v0}, LxJ;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 105
    invoke-direct {p0}, Lcom/google/android/apps/docs/common/welcome/WelcomeActivity;->e()V

    goto/16 :goto_0
.end method
