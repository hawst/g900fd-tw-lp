.class public Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;
.super Landroid/widget/RelativeLayout;
.source "FloatingHandleView.java"

# interfaces
.implements LKW;
.implements LKe;
.implements LLo;
.implements LaqU;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/RelativeLayout;",
        "LKW;",
        "LKe",
        "<",
        "Lcom/google/android/gms/drive/database/data/EntrySpec;",
        ">;",
        "LLo;",
        "LaqU;"
    }
.end annotation


# instance fields
.field public a:LCU;

.field public a:LJR;

.field public a:LJU;

.field private a:LKN;

.field private final a:LKV;

.field private a:LKn;

.field public a:LLS;

.field public a:LLf;

.field private a:LLt;

.field private final a:Landroid/view/View$OnTouchListener;

.field private a:Landroid/widget/TextView;

.field public a:LaqT;

.field private a:Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;

.field private final a:Ljava/lang/Runnable;

.field private a:Z

.field private final b:Ljava/lang/Runnable;

.field private final c:Ljava/lang/Runnable;

.field private final d:Ljava/lang/Runnable;

.field private final e:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 171
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 174
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 70
    new-instance v0, LKV;

    invoke-direct {v0}, LKV;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKV;

    .line 72
    new-instance v0, LKD;

    invoke-direct {v0, p0}, LKD;-><init>(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:Landroid/view/View$OnTouchListener;

    .line 83
    new-instance v0, LKE;

    invoke-direct {v0, p0}, LKE;-><init>(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:Ljava/lang/Runnable;

    .line 90
    new-instance v0, LKF;

    invoke-direct {v0, p0}, LKF;-><init>(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->b:Ljava/lang/Runnable;

    .line 103
    new-instance v0, LKG;

    invoke-direct {v0, p0}, LKG;-><init>(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->c:Ljava/lang/Runnable;

    .line 133
    new-instance v0, LKI;

    invoke-direct {v0, p0}, LKI;-><init>(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->d:Ljava/lang/Runnable;

    .line 140
    new-instance v0, LKJ;

    invoke-direct {v0, p0}, LKJ;-><init>(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->e:Ljava/lang/Runnable;

    .line 153
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:Landroid/widget/TextView;

    .line 162
    sget-object v0, LKN;->a:LKN;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKN;

    .line 175
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a(Landroid/content/Context;)V

    .line 176
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 180
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 70
    new-instance v0, LKV;

    invoke-direct {v0}, LKV;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKV;

    .line 72
    new-instance v0, LKD;

    invoke-direct {v0, p0}, LKD;-><init>(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:Landroid/view/View$OnTouchListener;

    .line 83
    new-instance v0, LKE;

    invoke-direct {v0, p0}, LKE;-><init>(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:Ljava/lang/Runnable;

    .line 90
    new-instance v0, LKF;

    invoke-direct {v0, p0}, LKF;-><init>(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->b:Ljava/lang/Runnable;

    .line 103
    new-instance v0, LKG;

    invoke-direct {v0, p0}, LKG;-><init>(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->c:Ljava/lang/Runnable;

    .line 133
    new-instance v0, LKI;

    invoke-direct {v0, p0}, LKI;-><init>(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->d:Ljava/lang/Runnable;

    .line 140
    new-instance v0, LKJ;

    invoke-direct {v0, p0}, LKJ;-><init>(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->e:Ljava/lang/Runnable;

    .line 153
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:Landroid/widget/TextView;

    .line 162
    sget-object v0, LKN;->a:LKN;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKN;

    .line 181
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a(Landroid/content/Context;)V

    .line 182
    return-void
.end method

.method private a()I
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LJR;

    invoke-virtual {v0}, LJR;->a()I

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)LKN;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKN;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;LKN;)LKN;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKN;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)LKV;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKV;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)LKn;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKn;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)LLt;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LLt;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:Ljava/lang/Runnable;

    return-object v0
.end method

.method private a(Z)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 357
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->d:Ljava/lang/Runnable;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->e:Ljava/lang/Runnable;

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 185
    invoke-static {p1}, Lajt;->a(Landroid/content/Context;)Lbuu;

    move-result-object v0

    invoke-interface {v0, p0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LaqT;

    invoke-virtual {v0, p0}, LaqT;->a(LaqU;)V

    .line 187
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;Z)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->b(Z)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:Z

    return v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->b:Ljava/lang/Runnable;

    return-object v0
.end method

.method private b(Z)V
    .locals 4

    .prologue
    .line 364
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 398
    :goto_0
    return-void

    .line 368
    :cond_0
    new-instance v0, Landroid/content/ClipData$Item;

    const-string v1, "drive:///current-selection"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    .line 369
    new-instance v1, Landroid/content/ClipDescription;

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:Landroid/widget/TextView;

    .line 370
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Landroid/content/ClipDescription;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;)V

    .line 371
    new-instance v2, Landroid/content/ClipData;

    invoke-direct {v2, v1, v0}, Landroid/content/ClipData;-><init>(Landroid/content/ClipDescription;Landroid/content/ClipData$Item;)V

    .line 373
    new-instance v0, LLf;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;

    invoke-direct {v0, v1}, LLf;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LLf;

    .line 374
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LLf;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, LLf;->a(Ljava/lang/Runnable;)V

    .line 376
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKn;

    if-nez v0, :cond_1

    new-instance v0, Landroid/view/View$DragShadowBuilder;

    invoke-direct {v0, p0}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    .line 382
    :goto_1
    sget-object v1, LKN;->b:LKN;

    iput-object v1, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKN;

    .line 383
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v3, LKL;

    invoke-direct {v3, p0, v2, v0, p1}, LKL;-><init>(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Z)V

    invoke-virtual {v1, v3}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    goto :goto_0

    .line 376
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKn;

    .line 377
    invoke-virtual {v0}, LKn;->a()Landroid/view/View$DragShadowBuilder;

    move-result-object v0

    goto :goto_1
.end method

.method private c()Z
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 331
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;

    new-instance v1, LKK;

    invoke-direct {v1, p0}, LKK;-><init>(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 271
    return-void
.end method


# virtual methods
.method public a()LLf;
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LLf;

    return-object v0
.end method

.method public a(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 402
    invoke-virtual {p0, p1, p2, p0, v1}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    move-result v0

    .line 403
    if-eqz v0, :cond_1

    .line 404
    sget-object v0, LKN;->c:LKN;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKN;

    .line 405
    if-nez p3, :cond_0

    .line 406
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LLt;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LLf;

    invoke-virtual {v0, v1}, LLt;->a(LLf;)V

    .line 412
    :cond_0
    :goto_0
    return-void

    .line 409
    :cond_1
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->setVisibility(I)V

    .line 410
    sget-object v0, LKN;->a:LKN;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKN;

    goto :goto_0
.end method

.method public a(Landroid/view/DragEvent;)V
    .locals 3

    .prologue
    .line 247
    invoke-virtual {p1}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 248
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKn;

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKn;

    invoke-virtual {p1}, Landroid/view/DragEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/DragEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, LKn;->a(FF)V

    .line 252
    :cond_0
    return-void
.end method

.method public a(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKn;

    if-eqz v0, :cond_1

    .line 235
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    .line 236
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit8 v0, v0, 0x3

    if-nez v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKn;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, LKn;->a(FF)V

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKN;

    invoke-virtual {v0}, LKN;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 240
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->h()V

    .line 243
    :cond_1
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKN;

    invoke-virtual {v0}, LKN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 431
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a(Z)V

    .line 433
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 337
    const-string v0, "FloatingHandleView"

    const-string v1, "Drag started."

    invoke-static {v0, v1}, LalV;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKN;

    invoke-virtual {v0}, LKN;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 340
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->setVisibility(I)V

    .line 341
    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    :cond_0
    if-nez p1, :cond_4

    .line 342
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKn;

    if-eqz v0, :cond_2

    .line 343
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKn;

    invoke-virtual {v0}, LKn;->b()V

    .line 345
    :cond_2
    sget-object v0, LKN;->b:LKN;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKN;

    .line 346
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a(Z)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 354
    :cond_3
    :goto_0
    return-void

    .line 348
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKn;

    if-eqz v0, :cond_3

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKn;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKN;

    .line 350
    invoke-virtual {v1}, LKN;->d()Z

    move-result v1

    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a(Z)Ljava/lang/Runnable;

    move-result-object v2

    .line 349
    invoke-virtual {v0, v1, v2}, LKn;->a(ZLjava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKn;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKn;

    invoke-virtual {v0}, LKn;->a()Z

    move-result v0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:Z

    .line 218
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 439
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKN;

    invoke-virtual {v0}, LKN;->c()Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 221
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:Z

    .line 222
    return-void
.end method

.method public c_()V
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LJR;

    invoke-virtual {v0}, LJR;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 276
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->setVisibility(I)V

    .line 285
    :cond_0
    :goto_0
    return-void

    .line 279
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKn;

    if-eqz v0, :cond_0

    .line 280
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->setVisibility(I)V

    .line 281
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->e()V

    .line 282
    sget-object v0, LKN;->a:LKN;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKN;

    .line 283
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->clearAnimation()V

    goto :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LJR;

    invoke-virtual {v0}, LJR;->a()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 226
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a(Z)V

    .line 230
    :goto_0
    return-void

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LJU;

    invoke-virtual {v0, p0}, LJU;->a(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;)V

    goto :goto_0
.end method

.method public e()V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 291
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LJR;

    invoke-virtual {v0}, LJR;->a()LbmF;

    move-result-object v0

    invoke-virtual {v0}, LbmF;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->setVisibility(I)V

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 311
    :goto_0
    return-void

    .line 295
    :cond_0
    invoke-virtual {p0, v6}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->setVisibility(I)V

    .line 296
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->clearAnimation()V

    .line 298
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LJR;

    invoke-virtual {v0}, LJR;->a()I

    move-result v0

    .line 300
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lxg;->selection_floating_handle_count:I

    new-array v3, v5, [Ljava/lang/Object;

    .line 301
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    .line 300
    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 303
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lxg;->selection_floating_handle_count_content_desc:I

    new-array v4, v5, [Ljava/lang/Object;

    .line 304
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    .line 303
    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 306
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 307
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LLS;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:Landroid/view/View$OnTouchListener;

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LCU;

    .line 309
    invoke-virtual {v2}, LCU;->a()Lcom/google/android/apps/docs/doclist/SelectionItem;

    move-result-object v2

    .line 308
    invoke-virtual {v0, p0, v1, v2}, LLS;->a(Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;Landroid/view/View$OnTouchListener;Lcom/google/android/apps/docs/doclist/SelectionItem;)V

    goto :goto_0
.end method

.method public f()V
    .locals 0

    .prologue
    .line 317
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->e()V

    .line 318
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 324
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->e()V

    .line 325
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->c:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 423
    return-void
.end method

.method public setUp(Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 190
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 214
    :goto_0
    return-void

    .line 195
    :cond_0
    sget v0, Lxc;->selection_floating_handle_description:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:Landroid/widget/TextView;

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:Landroid/widget/TextView;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKV;

    invoke-virtual {v0, p0}, LKV;->a(LKW;)V

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:Landroid/view/View$OnTouchListener;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LJR;

    invoke-virtual {v0, p0}, LJR;->a(LKe;)V

    .line 203
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;

    .line 204
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->i()V

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LCU;

    invoke-virtual {v0, p0}, LCU;->a(LLo;)V

    .line 207
    new-instance v0, LLt;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LCU;

    invoke-direct {v0, v1, p1}, LLt;-><init>(LCU;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LLt;

    .line 210
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 211
    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    .line 212
    new-instance v1, LKn;

    invoke-direct {v1, p2, p0, v0}, LKn;-><init>(Landroid/widget/ImageView;Landroid/view/View;F)V

    iput-object v1, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->a:LKn;

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->b:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/selection/view/FloatingHandleView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
