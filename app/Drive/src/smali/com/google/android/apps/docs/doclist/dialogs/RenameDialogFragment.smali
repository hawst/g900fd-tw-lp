.class public Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;
.super Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;
.source "RenameDialogFragment.java"


# instance fields
.field public a:LUi;

.field private a:LaGv;

.field private a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field public a:LvU;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;-><init>()V

    .line 227
    return-void
.end method

.method private a(LaGv;)I
    .locals 2

    .prologue
    .line 91
    sget-object v0, LFw;->a:[I

    invoke-virtual {p1}, LaGv;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 108
    sget v0, Lxi;->rename_file:I

    .line 110
    :goto_0
    return v0

    .line 93
    :pswitch_0
    sget v0, Lxi;->rename_collection:I

    goto :goto_0

    .line 96
    :pswitch_1
    sget v0, Lxi;->rename_document:I

    goto :goto_0

    .line 99
    :pswitch_2
    sget v0, Lxi;->rename_spreadsheet:I

    goto :goto_0

    .line 102
    :pswitch_3
    sget v0, Lxi;->rename_presentation:I

    goto :goto_0

    .line 105
    :pswitch_4
    sget v0, Lxi;->rename_drawing:I

    goto :goto_0

    .line 91
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(LaGu;)Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;
    .locals 3

    .prologue
    .line 67
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 68
    const-string v1, "entrySpec"

    invoke-interface {p0}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 69
    const-string v1, "title"

    invoke-interface {p0}, LaGu;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v1, "kindString"

    invoke-interface {p0}, LaGu;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    new-instance v1, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;-><init>()V

    .line 72
    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->e(Landroid/os/Bundle;)V

    .line 73
    return-object v1
.end method

.method public static synthetic a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 41
    invoke-static {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 223
    invoke-virtual {p0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 118
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a()Landroid/app/AlertDialog;

    move-result-object v2

    .line 120
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->b()Landroid/view/View;

    move-result-object v0

    sget v3, Lxc;->item_name:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 121
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->b()Landroid/view/View;

    move-result-object v0

    sget v3, Lxc;->new_name:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 122
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3, v1}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a(Landroid/app/Dialog;ILjava/lang/String;)V

    .line 124
    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a:LaGv;

    invoke-direct {p0, v3}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a(LaGv;)I

    move-result v3

    .line 125
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 127
    if-eqz p1, :cond_0

    const-string v1, "newName"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 128
    :cond_0
    if-eqz v1, :cond_1

    .line 129
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 130
    invoke-static {v0, v2}, Lano;->a(Landroid/widget/EditText;Landroid/app/Dialog;)V

    .line 131
    invoke-static {v0, v2}, LEL;->a(Landroid/widget/TextView;Landroid/app/AlertDialog;)V

    .line 133
    return-object v2

    .line 128
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method protected a(Landroid/app/AlertDialog;)V
    .locals 3

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a()LH;

    move-result-object v0

    .line 148
    if-eqz v0, :cond_0

    .line 150
    invoke-virtual {p1}, Landroid/app/AlertDialog;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a:LaGv;

    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a(LaGv;)I

    move-result v2

    .line 149
    invoke-static {v0, v1, v2}, LUs;->a(Landroid/content/Context;Landroid/view/View;I)V

    .line 152
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 153
    sget v0, Lxc;->new_name:I

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 155
    new-instance v2, LFx;

    invoke-direct {v2, v1}, LFx;-><init>(Landroid/widget/Button;)V

    .line 156
    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 160
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v2, v0}, LFx;->a(Landroid/text/Editable;)V

    .line 161
    return-void
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->a_(Landroid/os/Bundle;)V

    .line 80
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "entrySpec"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 81
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->c:Ljava/lang/String;

    .line 82
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "kindString"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 83
    invoke-static {v0}, LaGv;->a(Ljava/lang/String;)LaGv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a:LaGv;

    .line 84
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 138
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->c(Landroid/os/Bundle;)V

    .line 140
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a()Landroid/app/Dialog;

    move-result-object v0

    sget v1, Lxc;->new_name:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 141
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 142
    const-string v1, "newName"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 6

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a()LH;

    move-result-object v1

    .line 187
    if-nez v1, :cond_0

    .line 220
    :goto_0
    return-void

    .line 192
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a()Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 193
    if-eqz v2, :cond_2

    .line 194
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a()LH;

    move-result-object v0

    invoke-virtual {v0}, LH;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 197
    const/4 v0, 0x0

    .line 198
    iget-object v4, p0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->d:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 199
    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "documentTitle"

    iget-object v5, p0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    const/4 v0, -0x1

    .line 202
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->c()I

    move-result v4

    invoke-virtual {v2, v4, v0, v3}, Landroid/support/v4/app/Fragment;->a(IILandroid/content/Intent;)V

    .line 209
    :cond_2
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v2, LFv;

    invoke-direct {v2, p0, v1}, LFv;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;Landroid/app/Activity;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 219
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    goto :goto_0
.end method

.method protected t()V
    .locals 4

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->b()Landroid/view/View;

    move-result-object v0

    sget v1, Lxc;->new_name:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 166
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 167
    invoke-static {v1}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 173
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 175
    invoke-virtual {v0}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 182
    :goto_0
    return-void

    .line 179
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a()Landroid/app/Dialog;

    move-result-object v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a(Landroid/app/Dialog;ILjava/lang/String;)V

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a:LUi;

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    new-instance v3, LFm;

    invoke-direct {v3, p0}, LFm;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;)V

    invoke-interface {v0, v2, v1, v3}, LUi;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;Ljava/lang/String;LaHy;)V

    .line 181
    iput-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method protected u()V
    .locals 1

    .prologue
    .line 253
    invoke-static {}, LamV;->a()V

    .line 254
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a:LvU;

    invoke-interface {v0}, LvU;->a()V

    .line 255
    return-void
.end method
