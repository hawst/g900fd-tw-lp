.class public abstract Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;
.super Lcom/google/android/apps/docs/app/BaseFragment;
.source "BaseSlidePickerFragment.java"

# interfaces
.implements LYx;


# instance fields
.field public a:LQr;

.field private a:LWN;

.field public a:LWR;

.field public a:LXz;

.field public a:LYb;

.field public a:LYv;

.field public a:LYw;

.field public a:LqK;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseFragment;-><init>()V

    .line 32
    return-void
.end method

.method private t()V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LWN;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 129
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 131
    new-instance v1, LWP;

    invoke-direct {v1, p0, v0}, LWP;-><init>(Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LWN;

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LXz;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LWN;

    invoke-interface {v0, v1}, LXz;->a(LXA;)V

    .line 169
    return-void

    .line 127
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private u()V
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LWN;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LXz;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LWN;

    invoke-interface {v0, v1}, LXz;->b(LXA;)V

    .line 174
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LWN;

    .line 175
    return-void

    .line 172
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private v()V
    .locals 4

    .prologue
    .line 178
    new-instance v0, LYb;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LXz;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LYv;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a()LH;

    move-result-object v3

    invoke-virtual {v3}, LH;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LYb;-><init>(LXz;LYv;Landroid/view/LayoutInflater;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LYb;

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LYb;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a(Landroid/widget/ListAdapter;)V

    .line 180
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a()LH;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cannot perform cleanup while the Fragment is still attached"

    invoke-static {v0, v1}, LbiT;->b(ZLjava/lang/Object;)V

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LYv;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LYv;

    invoke-interface {v0}, LYv;->b()V

    .line 117
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LYv;

    .line 119
    :cond_0
    return-void

    .line 113
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract a(I)V
.end method

.method protected abstract a(Landroid/widget/ListAdapter;)V
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 100
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseFragment;->a_(Landroid/os/Bundle;)V

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LYv;

    if-nez v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LYw;

    invoke-interface {v0, p0}, LYw;->a(LYx;)LYv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LYv;

    .line 105
    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 6

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LqK;

    const-string v1, "punch"

    const-string v2, "webViewPunchPickSlide"

    const/4 v3, 0x0

    int-to-long v4, p1

    .line 184
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 183
    invoke-virtual {v0, v1, v2, v3, v4}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LWR;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LWR;

    invoke-interface {v0, p1}, LWR;->a(I)V

    .line 189
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 66
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseFragment;->b(Landroid/os/Bundle;)V

    .line 68
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->v()V

    .line 69
    return-void
.end method

.method public h()V
    .locals 0

    .prologue
    .line 109
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseFragment;->h()V

    .line 110
    return-void
.end method

.method public j_()V
    .locals 0

    .prologue
    .line 80
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseFragment;->j_()V

    .line 82
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->u()V

    .line 83
    return-void
.end method

.method public k_()V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseFragment;->k_()V

    .line 75
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->t()V

    .line 76
    return-void
.end method
