.class public Lcom/google/android/apps/viewer/client/TokenSource$OneTimeTokenSource;
.super Ljava/lang/Object;
.source "TokenSource.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/google/android/apps/viewer/client/TokenSource;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/viewer/client/TokenSource$OneTimeTokenSource;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected token:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 129
    new-instance v0, LauT;

    invoke-direct {v0}, LauT;-><init>()V

    sput-object v0, Lcom/google/android/apps/viewer/client/TokenSource$OneTimeTokenSource;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    invoke-static {p1}, LauV;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    iput-object p1, p0, Lcom/google/android/apps/viewer/client/TokenSource$OneTimeTokenSource;->token:Ljava/lang/String;

    .line 110
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    return v0
.end method

.method public newToken()Ljava/lang/String;
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/apps/viewer/client/TokenSource$OneTimeTokenSource;->token:Ljava/lang/String;

    .line 115
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/viewer/client/TokenSource$OneTimeTokenSource;->token:Ljava/lang/String;

    .line 116
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/viewer/client/TokenSource$OneTimeTokenSource;->token:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 122
    return-void
.end method
