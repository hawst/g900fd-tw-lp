.class public Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;
.super Ljava/lang/Object;
.source "FileOpenerIntentCreatorImpl.java"

# interfaces
.implements Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;


# instance fields
.field private final a:LQr;

.field private final a:LaIm;

.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Laja;LQr;LaIm;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laja",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LQr;",
            "LaIm;",
            ")V"
        }
    .end annotation

    .prologue
    .line 249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 250
    iput-object p2, p0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;->a:LQr;

    .line 251
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;->a:Lbxw;

    .line 252
    iput-object p3, p0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;->a:LaIm;

    .line 253
    return-void
.end method

.method public static synthetic a(Landroid/content/Context;Lcom/google/android/apps/docs/app/DocumentOpenMethod;LaGo;Ljava/lang/String;Landroid/net/Uri;)LGk;
    .locals 1

    .prologue
    .line 72
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;->b(Landroid/content/Context;Lcom/google/android/apps/docs/app/DocumentOpenMethod;LaGo;Ljava/lang/String;Landroid/net/Uri;)LGk;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Landroid/content/Context;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;)LGk;
    .locals 1

    .prologue
    .line 72
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;->b(Landroid/content/Context;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;)LGk;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;LaGo;Landroid/net/Uri;)LGk;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 280
    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;->a(Ljava/lang/String;LaGo;)Ljava/lang/String;

    move-result-object v3

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    .line 284
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v0

    .line 285
    if-eqz v3, :cond_0

    .line 286
    new-array v2, v7, [LGm;

    sget-object v4, LGm;->a:LGm;

    aput-object v4, v2, v5

    sget-object v4, LGm;->c:LGm;

    aput-object v4, v2, v6

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, LbmH;->a(Ljava/lang/Iterable;)LbmH;

    .line 293
    :cond_0
    new-array v2, v7, [LGm;

    sget-object v4, LGm;->b:LGm;

    aput-object v4, v2, v5

    sget-object v4, LGm;->d:LGm;

    aput-object v4, v2, v6

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, LbmH;->a(Ljava/lang/Iterable;)LbmH;

    .line 295
    invoke-virtual {v0}, LbmH;->a()LbmF;

    move-result-object v0

    .line 297
    invoke-interface {p3}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    .line 298
    if-nez v2, :cond_2

    const/4 v6, 0x0

    .line 300
    :goto_0
    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v7

    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LGm;

    move-object v2, p1

    move-object v4, p3

    move-object v5, p4

    .line 301
    invoke-virtual/range {v0 .. v6}, LGm;->a(Landroid/content/Context;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;LaGo;Landroid/net/Uri;Landroid/net/Uri;)LGk;

    move-result-object v0

    .line 303
    invoke-interface {v0}, LGk;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 307
    :goto_1
    return-object v0

    .line 298
    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;->a:LaIm;

    .line 299
    invoke-interface {v4, v2}, LaIm;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/net/Uri;

    move-result-object v6

    goto :goto_0

    .line 307
    :cond_3
    sget-object v0, LGr;->a:LGr;

    goto :goto_1
.end method

.method private static a(LaGo;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 345
    invoke-interface {p0}, LaGo;->c()Ljava/lang/String;

    move-result-object v0

    .line 346
    const-string v1, "file:///data/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 347
    return-object v0
.end method

.method public static a(Landroid/content/Intent;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator$UriIntentBuilder;
    .locals 1

    .prologue
    .line 130
    const-string v0, "android.intent.action.VIEW"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 131
    new-instance v0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl$UriIntentBuilderImpl;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl$UriIntentBuilderImpl;-><init>(Landroid/content/Intent;Lcom/google/android/apps/docs/app/DocumentOpenMethod;)V

    return-object v0
.end method

.method public static a(LaGo;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 426
    invoke-interface {p0}, LaGo;->c()Ljava/lang/String;

    move-result-object v0

    .line 434
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 435
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 436
    const/4 v0, 0x0

    .line 438
    :goto_0
    return-object v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;LaGo;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 331
    .line 333
    invoke-static {p2}, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;->a(LaGo;)Ljava/lang/String;

    move-result-object v0

    .line 334
    if-eqz v0, :cond_0

    .line 337
    invoke-static {v0}, Lrw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 338
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;->a:LQr;

    invoke-interface {v1, v0, p1}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 341
    :cond_0
    return-object p1
.end method

.method private static a(Ljava/util/List;Landroid/net/Uri;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;",
            "Landroid/net/Uri;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 406
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 407
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 408
    iget v1, v0, Landroid/content/pm/ResolveInfo;->match:I

    .line 409
    const/high16 v4, 0x500000

    and-int/2addr v1, v4

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    .line 410
    :goto_1
    if-nez v1, :cond_1

    .line 411
    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    .line 412
    if-eqz v4, :cond_1

    .line 413
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/IntentFilter;->hasDataPath(Ljava/lang/String;)Z

    move-result v1

    .line 416
    :cond_1
    if-eqz v1, :cond_0

    .line 417
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 409
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 421
    :cond_3
    return-object v2
.end method

.method private static b(Landroid/content/Context;Lcom/google/android/apps/docs/app/DocumentOpenMethod;LaGo;Ljava/lang/String;Landroid/net/Uri;)LGk;
    .locals 4

    .prologue
    .line 377
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 379
    invoke-static {p2}, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;->a(LaGo;)Ljava/lang/String;

    move-result-object v1

    .line 380
    if-nez v1, :cond_0

    .line 381
    sget-object v0, LGr;->a:LGr;

    .line 391
    :goto_0
    return-object v0

    .line 384
    :cond_0
    invoke-static {p2}, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;->a(LaGo;)Landroid/net/Uri;

    move-result-object v1

    .line 386
    invoke-virtual {p1, p0, v1, p3, p4}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    .line 387
    const/high16 v3, 0x10000

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 388
    if-nez p3, :cond_1

    .line 389
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;->a(Ljava/util/List;Landroid/net/Uri;)Ljava/util/List;

    move-result-object v0

    .line 391
    :cond_1
    new-instance v1, LGr;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, p1, v3}, LGr;-><init>(Landroid/content/Intent;Ljava/util/List;Lcom/google/android/apps/docs/app/DocumentOpenMethod;LGl;)V

    move-object v0, v1

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;)LGk;
    .locals 4

    .prologue
    .line 317
    invoke-virtual {p1, p0, p3, p2, p4}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 318
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 319
    const/high16 v2, 0x10000

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 320
    if-nez p2, :cond_0

    .line 321
    invoke-static {v0, p3}, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;->a(Ljava/util/List;Landroid/net/Uri;)Ljava/util/List;

    move-result-object v0

    .line 323
    :cond_0
    new-instance v2, LGr;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v0, p1, v3}, LGr;-><init>(Landroid/content/Intent;Ljava/util/List;Lcom/google/android/apps/docs/app/DocumentOpenMethod;LGl;)V

    return-object v2
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;LaGo;)LGk;
    .locals 1

    .prologue
    .line 258
    invoke-static {p3}, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;->a(LaGo;)Landroid/net/Uri;

    move-result-object v0

    .line 259
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;->a(Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;LaGo;Landroid/net/Uri;)LGk;

    move-result-object v0

    .line 260
    return-object v0
.end method

.method public a(Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;LaGo;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 266
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;->a(Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;LaGo;Landroid/net/Uri;)LGk;

    move-result-object v0

    .line 268
    invoke-interface {v0}, LGk;->a()Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator$UriIntentBuilder;

    move-result-object v0

    .line 271
    invoke-interface {v0, p4}, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator$UriIntentBuilder;->a(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
