.class public Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;
.super Lrm;
.source "WidgetConfigureActivity.java"

# interfaces
.implements LFq;


# instance fields
.field private a:I

.field public a:LSF;

.field public a:Lald;

.field private final a:LatQ;

.field public a:LatR;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lrm;-><init>()V

    .line 35
    new-instance v0, LatQ;

    invoke-direct {v0, p0}, LatQ;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a:LatQ;

    .line 82
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a:I

    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->setResult(I)V

    .line 71
    const-string v0, "appWidgetId"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 74
    if-nez v0, :cond_0

    .line 75
    invoke-virtual {p0}, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->finish()V

    .line 80
    :goto_0
    return-void

    .line 77
    :cond_0
    iput v0, p0, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a:I

    .line 78
    invoke-virtual {p0}, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a()LM;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a(LM;)Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/accounts/Account;)V
    .locals 4

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a:LqK;

    const-string v1, "widget"

    const-string v2, "addWidgetInstance"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    .line 88
    iget v1, p0, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a:I

    .line 89
    iget-object v2, p0, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a:LatQ;

    invoke-virtual {v2, v1, v0}, LatQ;->a(ILaFO;)V

    .line 91
    iget-object v2, p0, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a:LatR;

    .line 92
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v3

    .line 91
    invoke-virtual {v2, v3, v1, p0, v0}, LatR;->a(Landroid/appwidget/AppWidgetManager;ILandroid/content/Context;LaFO;)V

    .line 93
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 94
    const-string v2, "appWidgetId"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 95
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->setResult(ILandroid/content/Intent;)V

    .line 96
    invoke-virtual {p0}, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->finish()V

    .line 97
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->setResult(I)V

    .line 102
    invoke-virtual {p0}, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->finish()V

    .line 103
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 39
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a(Landroid/content/Intent;)V

    .line 41
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 45
    invoke-super {p0}, Lrm;->onDestroy()V

    .line 46
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0, p1}, Lrm;->onNewIntent(Landroid/content/Intent;)V

    .line 51
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->setIntent(Landroid/content/Intent;)V

    .line 52
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a(Landroid/content/Intent;)V

    .line 53
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 57
    invoke-super {p0, p1}, Lrm;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 58
    const-string v0, "appWidgetId"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a:I

    .line 60
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 64
    invoke-super {p0, p1}, Lrm;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 65
    const-string v0, "appWidgetId"

    iget v1, p0, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 66
    return-void
.end method
