.class public Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;
.super Lcom/google/android/apps/docs/tools/gelly/android/GuiceService;
.source "ContentSyncService.java"


# instance fields
.field private a:I

.field public a:LQr;

.field private a:LZQ;

.field public a:LZS;

.field public a:LaGM;

.field public a:LaKM;

.field public a:LaKR;

.field public a:LaKU;

.field public a:Laer;

.field public a:LagG;

.field public a:LahK;

.field public a:Lamh;

.field public a:LqK;

.field public a:LtK;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceService;-><init>()V

    .line 195
    return-void
.end method

.method private a()I
    .locals 3

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LQr;

    const-string v1, "maxContentSyncThreadCount"

    const/4 v2, 0x4

    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static a(LQr;)I
    .locals 2

    .prologue
    .line 283
    const-string v0, "maxContentSyncAttemptCount"

    const/4 v1, 0x5

    invoke-interface {p0, v0, v1}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;)I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:I

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;)LZQ;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZQ;

    return-object v0
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 289
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 291
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 292
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 293
    if-eqz p2, :cond_0

    .line 294
    const-string v1, "entrySpec.v2"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 297
    :cond_0
    return-object v0
.end method

.method private declared-synchronized a(I)V
    .locals 1

    .prologue
    .line 244
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:I

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LaKU;

    invoke-virtual {v0}, LaKU;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 246
    monitor-exit p0

    return-void

    .line 244
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static synthetic a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 0

    .prologue
    .line 55
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->c(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    return-void
.end method

.method private b()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 227
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LQr;

    const-string v1, "contentSyncServiceConnectivityCheckPeriodSeconds"

    const/16 v2, 0x708

    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v0, v0

    .line 230
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v0, v4

    add-long/2addr v2, v0

    .line 231
    const-string v0, "com.google.android.apps.docs.sync.syncadapter.SYNC"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    .line 232
    const/high16 v1, 0x8000000

    .line 233
    invoke-static {p0, v6, v0, v1}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 234
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 235
    invoke-virtual {v0, v6, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 236
    return-void
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 302
    invoke-static {p0}, Lbfd;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    invoke-static {p1}, Lbfd;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    if-nez p2, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "com.google.android.apps.docs.sync.syncadapter.SYNC"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-ne v0, v3, :cond_1

    :goto_1
    invoke-static {v1}, Lbfd;->a(Z)V

    .line 305
    return-void

    :cond_0
    move v0, v2

    .line 304
    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private c()V
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LaKU;

    invoke-virtual {v0}, LaKU;->b()V

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LahK;

    invoke-interface {v0}, LahK;->a()V

    .line 251
    new-instance v0, Lagf;

    invoke-direct {v0, p0}, Lagf;-><init>(Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;)V

    invoke-virtual {v0}, Lagf;->start()V

    .line 252
    return-void
.end method

.method private static c(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1

    .prologue
    .line 309
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 311
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 312
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .prologue
    .line 146
    invoke-super {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceService;->onCreate()V

    .line 147
    new-instance v1, LZQ;

    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LQr;

    const-string v2, "contentSyncBackoffMinWait"

    const/16 v3, 0x3e8

    .line 149
    invoke-interface {v0, v2, v3}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v2, v0

    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LQr;

    const-string v4, "contentSyncBackoffWaitGrowthFactor"

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 151
    invoke-interface {v0, v4, v6, v7}, LQr;->a(Ljava/lang/String;D)D

    move-result-wide v4

    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LQr;

    const-string v6, "contentSyncBackoffMaxWait"

    const v7, 0x927c0

    .line 153
    invoke-interface {v0, v6, v7}, LQr;->a(Ljava/lang/String;I)I

    move-result v0

    int-to-long v6, v0

    invoke-direct/range {v1 .. v7}, LZQ;-><init>(JDJ)V

    iput-object v1, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LZQ;

    .line 156
    new-instance v1, Laga;

    invoke-direct {v1, p0}, Laga;-><init>(Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;)V

    .line 164
    new-instance v2, Lagb;

    invoke-direct {v2, p0}, Lagb;-><init>(Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;)V

    .line 181
    new-instance v0, LaKU;

    .line 182
    invoke-direct {p0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a()I

    move-result v3

    const-wide/32 v4, 0xea60

    invoke-direct/range {v0 .. v5}, LaKU;-><init>(LaKW;Ljava/lang/Runnable;IJ)V

    iput-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LaKU;

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LqK;

    invoke-virtual {v0}, LqK;->a()V

    .line 184
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 218
    invoke-direct {p0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->c()V

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LqK;

    invoke-virtual {v0}, LqK;->b()V

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a:LahK;

    invoke-interface {v0}, LahK;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    invoke-direct {p0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->b()V

    .line 223
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceService;->onDestroy()V

    .line 224
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 264
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceService;->onStartCommand(Landroid/content/Intent;II)I

    .line 266
    if-nez p1, :cond_0

    .line 267
    const-string v0, "ContentSyncService"

    const-string v1, "restarted with null Intent"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    invoke-direct {p0, p3}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(I)V

    .line 275
    :goto_0
    return v2

    .line 271
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 272
    const-string v1, "Action should not be null"

    invoke-static {v0, v1}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    const-string v1, "com.google.android.apps.docs.sync.syncadapter.SYNC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 274
    invoke-direct {p0, p3}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(I)V

    goto :goto_0

    .line 278
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
