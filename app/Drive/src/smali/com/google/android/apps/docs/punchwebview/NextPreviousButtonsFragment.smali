.class public Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;
.super Lcom/google/android/apps/docs/app/BaseFragment;
.source "NextPreviousButtonsFragment.java"


# instance fields
.field private a:LXA;

.field public a:LXD;

.field public a:LXR;

.field public a:LXS;

.field private final a:LXU;

.field public a:LYO;

.field public a:LqK;

.field public b:LXS;

.field private final b:LXU;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseFragment;-><init>()V

    .line 61
    new-instance v0, LXk;

    invoke-direct {v0, p0}, LXk;-><init>(Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LXU;

    .line 70
    new-instance v0, LXl;

    invoke-direct {v0, p0}, LXl;-><init>(Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->b:LXU;

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LXA;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 120
    new-instance v0, LXm;

    invoke-direct {v0, p0}, LXm;-><init>(Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LXA;

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LXD;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LXA;

    invoke-virtual {v0, v1}, LXD;->a(LXA;)V

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LXS;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->b:LXU;

    invoke-virtual {v0, v1}, LXS;->a(LXU;)V

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->b:LXS;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LXU;

    invoke-virtual {v0, v1}, LXS;->a(LXU;)V

    .line 135
    return-void

    .line 118
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private t()V
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LXA;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LXD;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LXA;

    invoke-virtual {v0, v1}, LXD;->b(LXA;)V

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LXA;

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LXS;

    invoke-virtual {v0}, LXS;->a()V

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->b:LXS;

    invoke-virtual {v0}, LXS;->a()V

    .line 144
    return-void

    .line 138
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private u()V
    .locals 2

    .prologue
    .line 150
    const-string v0, "NextPreviousButtonsFragment"

    const-string v1, "in loadCachedSvgContent"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LXD;

    invoke-virtual {v0}, LXD;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a(Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LXD;

    invoke-virtual {v0}, LXD;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->b(Ljava/lang/String;)V

    .line 153
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 159
    const-string v2, "NextPreviousButtonsFragment"

    const-string v3, "onNextPreviewChange. nextContent is null: %s"

    new-array v4, v0, [Ljava/lang/Object;

    if-nez p1, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->b:LXS;

    invoke-virtual {v0, p1}, LXS;->a(Ljava/lang/String;)V

    .line 161
    return-void

    :cond_0
    move v0, v1

    .line 159
    goto :goto_0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 81
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseFragment;->a_(Landroid/os/Bundle;)V

    .line 82
    const-string v2, "NextPreviousButtonsFragment"

    const-string v3, "in onCreate savedInstanceState=%s"

    new-array v4, v0, [Ljava/lang/Object;

    aput-object p1, v4, v1

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 85
    if-eqz p1, :cond_0

    .line 86
    const-string v0, "NextPreviousButtonsFragment"

    const-string v1, "discarding reconstruction."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    :goto_0
    return-void

    .line 92
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LXR;

    if-eqz v2, :cond_1

    :goto_1
    invoke-static {v0}, LbiT;->b(Z)V

    .line 94
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a()LH;

    move-result-object v1

    .line 95
    sget v0, LpN;->previous_state_view_container:I

    .line 96
    invoke-virtual {v1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 97
    sget v2, LpN;->next_state_view_container:I

    .line 98
    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 100
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LXS;

    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LYO;

    iget-object v4, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LXR;

    iget-object v5, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LqK;

    invoke-virtual {v2, v0, v3, v4, v5}, LXS;->a(Landroid/widget/FrameLayout;LYO;LXR;LqK;)Landroid/webkit/WebView;

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->b:LXS;

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LYO;

    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LXR;

    iget-object v4, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LqK;

    invoke-virtual {v0, v1, v2, v3, v4}, LXS;->a(Landroid/widget/FrameLayout;LYO;LXR;LqK;)Landroid/webkit/WebView;

    .line 103
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->u()V

    .line 104
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a()V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 92
    goto :goto_1
.end method

.method public b(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 167
    const-string v2, "NextPreviousButtonsFragment"

    const-string v3, "onPreviousPreviewChange. previousContent is null: %s"

    new-array v4, v0, [Ljava/lang/Object;

    if-nez p1, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LXS;

    invoke-virtual {v0, p1}, LXS;->a(Ljava/lang/String;)V

    .line 169
    return-void

    :cond_0
    move v0, v1

    .line 167
    goto :goto_0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 109
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseFragment;->h()V

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->a:LXR;

    if-nez v0, :cond_0

    .line 115
    :goto_0
    return-void

    .line 114
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;->t()V

    goto :goto_0
.end method
