.class public Lcom/google/android/apps/viewer/PdfViewerActivity;
.super LhN;
.source "PdfViewerActivity.java"

# interfaces
.implements LatU;


# instance fields
.field private a:Laua;

.field private a:Laur;

.field private a:Lavl;

.field private final a:LawB;

.field private a:LawZ;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 65
    invoke-direct {p0}, LhN;-><init>()V

    .line 66
    new-instance v0, LawB;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/PdfViewerActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    new-instance v2, LatZ;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, LatZ;-><init>(Lcom/google/android/apps/viewer/PdfViewerActivity;LatV;)V

    invoke-direct {v0, v1, v2}, LawB;-><init>(Landroid/app/FragmentManager;LawC;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:LawB;

    .line 67
    return-void
.end method

.method private a()LauM;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 106
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/PdfViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 107
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/PdfViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    .line 108
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    .line 109
    iget-object v2, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:Lavl;

    invoke-virtual {v2, v0}, Lavl;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 110
    new-instance v4, Lcom/google/android/apps/viewer/client/AuthenticatedUri;

    sget-object v3, Lcom/google/android/apps/viewer/client/TokenSource;->a:Lcom/google/android/apps/viewer/client/TokenSource;

    invoke-direct {v4, v0, v3}, Lcom/google/android/apps/viewer/client/AuthenticatedUri;-><init>(Landroid/net/Uri;Lcom/google/android/apps/viewer/client/TokenSource;)V

    .line 111
    new-instance v3, Lavi;

    invoke-direct {v3}, Lavi;-><init>()V

    .line 112
    new-instance v0, LauM;

    iget-object v3, v3, Lavi;->g:[Ljava/lang/String;

    const/4 v6, 0x0

    aget-object v3, v3, v6

    if-eqz v7, :cond_0

    const-string v6, "android.intent.extra.STREAM"

    .line 113
    invoke-virtual {v7, v6}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    :goto_0
    if-eqz v7, :cond_1

    const-string v8, "reopenForEditIntent"

    .line 114
    invoke-virtual {v7, v8}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Intent;

    :goto_1
    invoke-direct/range {v0 .. v7}, LauM;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/viewer/client/AuthenticatedUri;Lcom/google/android/apps/viewer/client/AuthenticatedUri;Landroid/net/Uri;Landroid/content/Intent;)V

    return-object v0

    :cond_0
    move-object v6, v5

    .line 113
    goto :goto_0

    :cond_1
    move-object v7, v5

    .line 114
    goto :goto_1
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/PdfViewerActivity;)Laua;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:Laua;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/PdfViewerActivity;)Laur;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:Laur;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/PdfViewerActivity;)LawB;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:LawB;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/PdfViewerActivity;)LawZ;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:LawZ;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/PdfViewerActivity;LawZ;)LawZ;
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:LawZ;

    return-object p1
.end method

.method private a(LauM;)V
    .locals 4

    .prologue
    .line 118
    invoke-static {p1}, LauV;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    new-instance v0, LatV;

    invoke-direct {v0, p0, p1}, LatV;-><init>(Lcom/google/android/apps/viewer/PdfViewerActivity;LauM;)V

    .line 138
    iget-object v1, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:Lavl;

    sget-object v2, Lavr;->a:Lavr;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3, v0}, Lavl;->a(Lavr;LauM;ILavo;)V

    .line 139
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:Laur;

    invoke-virtual {v0}, Laur;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    const-string v0, ".pdf"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x4

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:Laua;

    sget v1, Laui;->icon_pdfviewer:I

    invoke-virtual {v0, p1, v1}, Laua;->a(Ljava/lang/String;I)V

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:Laua;

    invoke-virtual {v0}, Laua;->a()V

    .line 150
    return-void
.end method


# virtual methods
.method public a()Laua;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:Laua;

    return-object v0
.end method

.method protected finalize()V
    .locals 2

    .prologue
    .line 245
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 246
    sget-boolean v0, LavX;->k:Z

    if-eqz v0, :cond_0

    .line 247
    const-string v0, "PdfViewerActivity"

    const-string v1, "Good job! No Activity leakage! Activity destroyed."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 269
    packed-switch p1, :pswitch_data_0

    .line 276
    :cond_0
    invoke-super {p0, p1, p2, p3}, LhN;->onActivityResult(IILandroid/content/Intent;)V

    .line 278
    :goto_0
    return-void

    .line 271
    :pswitch_0
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 272
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/PdfViewerActivity;->finish()V

    goto :goto_0

    .line 269
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 71
    invoke-super {p0, p1}, LhN;->onCreate(Landroid/os/Bundle;)V

    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/PdfViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lavs;->a(Landroid/content/Context;)V

    .line 73
    new-instance v0, Lavl;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/PdfViewerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, v1}, Lavl;-><init>(Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:Lavl;

    .line 74
    invoke-direct {p0}, Lcom/google/android/apps/viewer/PdfViewerActivity;->a()LauM;

    move-result-object v0

    .line 75
    new-instance v1, Laur;

    invoke-direct {v1, p0, v0}, Laur;-><init>(Landroid/app/Activity;LauM;)V

    iput-object v1, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:Laur;

    .line 76
    new-instance v1, Laua;

    iget-object v2, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:Laur;

    invoke-direct {v1, p0, v2}, Laua;-><init>(LhN;Laur;)V

    iput-object v1, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:Laua;

    .line 77
    iget-object v1, v0, LauM;->b:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/apps/viewer/PdfViewerActivity;->b(Ljava/lang/String;)V

    .line 79
    if-eqz p1, :cond_0

    .line 80
    const-string v0, "PdfViewerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Activity create/restore "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:LawB;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LawB;->a(I)Laww;

    move-result-object v0

    check-cast v0, LawZ;

    iput-object v0, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:LawZ;

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:Laua;

    iget-object v1, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:LawZ;

    invoke-virtual {v0, v1}, Laua;->a(Laue;)V

    .line 87
    :goto_0
    return-void

    .line 84
    :cond_0
    const-string v1, "PdfViewerActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Activity create/fresh "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/PdfViewerActivity;->a(LauM;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 154
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/PdfViewerActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v3, Laul;->pdfviewer:I

    invoke-virtual {v0, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 156
    sget v0, Lauj;->action_add_to_drive:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:Laur;

    invoke-virtual {v3}, Laur;->d()Z

    move-result v3

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 157
    sget v0, Lauj;->action_print:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:Laur;

    invoke-virtual {v0}, Laur;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:Laur;

    .line 158
    invoke-virtual {v0}, Laur;->e()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 157
    :goto_0
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 159
    sget v0, Lauj;->action_send:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:Laur;

    invoke-virtual {v3}, Laur;->e()Z

    move-result v3

    if-nez v3, :cond_0

    move v2, v1

    :cond_0
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 160
    sget v0, Lauj;->action_details:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:Laur;

    invoke-virtual {v2}, Laur;->e()Z

    move-result v2

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 162
    sget v0, Lauj;->action_details:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v2, LatW;

    invoke-direct {v2, p0}, LatW;-><init>(Lcom/google/android/apps/viewer/PdfViewerActivity;)V

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 172
    sget v0, Lauj;->action_edit:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:Laur;

    invoke-virtual {v2}, Laur;->f()Z

    move-result v2

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 176
    sget v0, Lauj;->action_find:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v2, LatX;

    invoke-direct {v2, p0}, LatX;-><init>(Lcom/google/android/apps/viewer/PdfViewerActivity;)V

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 226
    return v1

    :cond_1
    move v0, v2

    .line 158
    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:Lavl;

    invoke-virtual {v0}, Lavl;->a()V

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:Laua;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Laua;->a(Laue;)V

    .line 101
    const-string v0, "PdfViewerActivity"

    const-string v1, "Activity destroyed."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    invoke-super {p0}, LhN;->onDestroy()V

    .line 103
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:Laua;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-virtual {v0, v1}, Laua;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    const/4 v0, 0x1

    .line 240
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LhN;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0}, LhN;->onResume()V

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:Laua;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/viewer/PdfViewerActivity;->a:Laua;

    invoke-virtual {v0}, Laua;->a()V

    .line 95
    :cond_0
    return-void
.end method
