.class public Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;
.super Lrm;
.source "MoveEntryActivityLegacy.java"


# instance fields
.field public a:LUi;

.field public a:LaGg;

.field public a:Lald;

.field private a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private a:LuL;

.field private b:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field public b:LqK;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lrm;-><init>()V

    .line 119
    return-void
.end method

.method private a(LaFV;LaFV;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 306
    if-eqz p1, :cond_3

    .line 307
    invoke-interface {p1}, LaFV;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 308
    invoke-interface {p2}, LaFV;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 309
    sget v0, Lxi;->move_shared_to_shared:I

    .line 324
    :cond_0
    :goto_0
    return v0

    .line 311
    :cond_1
    sget v0, Lxi;->move_shared_to_unshared:I

    goto :goto_0

    .line 314
    :cond_2
    invoke-interface {p2}, LaFV;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 315
    sget v0, Lxi;->move_unshared_to_shared:I

    goto :goto_0

    .line 321
    :cond_3
    invoke-interface {p2}, LaFV;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 322
    sget v0, Lxi;->move_unshared_to_shared:I

    goto :goto_0
.end method

.method private a()LaGu;
    .locals 2

    .prologue
    .line 400
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:LaGg;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v1}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;

    move-result-object v0

    .line 401
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 427
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 428
    const-string v1, "entrySpec.v2"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 429
    return-object v0
.end method

.method private a()LuD;
    .locals 5

    .prologue
    .line 196
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a()LaGu;

    move-result-object v0

    .line 197
    if-nez v0, :cond_0

    .line 198
    sget-object v0, LuD;->f:LuD;

    .line 212
    :goto_0
    return-object v0

    .line 201
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->b:LqK;

    const-string v2, "doclist"

    const-string v3, "moveShowEvent"

    .line 203
    invoke-static {v0}, Lala;->a(LaGu;)Ljava/lang/String;

    move-result-object v4

    .line 201
    invoke-virtual {v1, v2, v3, v4}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    iget-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:LaGg;

    .line 206
    invoke-interface {v0}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    .line 205
    invoke-interface {v1, v2}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LbmY;

    move-result-object v1

    .line 208
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    const/4 v2, 0x2

    if-lt v1, v2, :cond_1

    .line 209
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a(LaGu;)V

    .line 210
    sget-object v0, LuD;->a:LuD;

    goto :goto_0

    .line 212
    :cond_1
    sget-object v0, LuD;->b:LuD;

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;)LuD;
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a()LuD;

    move-result-object v0

    return-object v0
.end method

.method private a()LuK;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 368
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a()LaGu;

    move-result-object v3

    .line 369
    if-nez v3, :cond_1

    .line 391
    :cond_0
    :goto_0
    return-object v1

    .line 373
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:LaGg;

    .line 374
    invoke-interface {v3}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    .line 373
    invoke-interface {v0, v2}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LbmY;

    move-result-object v0

    .line 376
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    const/4 v4, 0x2

    if-ge v2, v4, :cond_0

    .line 381
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 382
    iget-object v2, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:LaGg;

    iget-object v4, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v4, v4, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-interface {v2, v4}, LaGg;->a(LaFO;)LaFM;

    .line 383
    invoke-static {v0}, Lbnm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 384
    iget-object v2, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:LaGg;

    invoke-interface {v2, v0}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFZ;

    move-result-object v0

    .line 386
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:LaGg;

    iget-object v4, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v2, v4}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFZ;

    move-result-object v4

    .line 387
    if-eqz v4, :cond_0

    .line 391
    new-instance v2, LuK;

    invoke-direct {v2, v3, v0, v4, v1}, LuK;-><init>(LaGu;LaFV;LaFV;Luy;)V

    move-object v1, v2

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;)LuL;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:LuL;

    return-object v0
.end method

.method private a(LaGu;)V
    .locals 3

    .prologue
    .line 405
    invoke-static {p0}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v1

    .line 406
    sget v0, Lxi;->move:I

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 408
    invoke-interface {p1}, LaGu;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lxi;->move_multi_parent_folder:I

    .line 410
    :goto_0
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 411
    const v0, 0x104000a

    new-instance v2, LuB;

    invoke-direct {v2, p0}, LuB;-><init>(Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 417
    new-instance v0, LuC;

    invoke-direct {v0, p0}, LuC;-><init>(Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 423
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 424
    return-void

    .line 408
    :cond_0
    sget v0, Lxi;->move_multi_parent_file:I

    goto :goto_0
.end method

.method private b()LuD;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 217
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a()LaGu;

    move-result-object v1

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:LaGg;

    .line 220
    invoke-interface {v1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    .line 219
    invoke-interface {v0, v2}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LbmY;

    move-result-object v2

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:LaGg;

    iget-object v3, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v3, v3, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-interface {v0, v3}, LaGg;->a(LaFO;)LaFM;

    move-result-object v0

    .line 224
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 225
    iget-object v3, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:LaGg;

    invoke-interface {v3, v0}, LaGg;->a(LaFM;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 234
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v3, v3, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    invoke-static {p0, v3}, Lcom/google/android/apps/docs/app/PickEntryActivity;->a(Landroid/content/Context;LaFO;)LuW;

    move-result-object v3

    .line 236
    invoke-virtual {v3, v0}, LuW;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LuW;

    move-result-object v0

    const/4 v3, 0x1

    new-array v3, v3, [LaGv;

    sget-object v4, LaGv;->a:LaGv;

    aput-object v4, v3, v5

    .line 237
    invoke-static {v3}, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->a([LaGv;)Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    move-result-object v3

    invoke-virtual {v0, v3}, LuW;->a(Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;)LuW;

    move-result-object v0

    sget v3, Lxi;->move:I

    .line 238
    invoke-virtual {v0, v3}, LuW;->a(I)LuW;

    move-result-object v0

    sget v3, Lxi;->move_dialog_title:I

    .line 239
    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LuW;->a(Ljava/lang/String;)LuW;

    move-result-object v0

    .line 240
    invoke-virtual {v0}, LuW;->a()LuW;

    move-result-object v0

    .line 241
    invoke-virtual {v0}, LuW;->b()LuW;

    move-result-object v0

    .line 242
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 243
    invoke-virtual {v0}, LuW;->c()LuW;

    .line 244
    instance-of v1, v1, LaFV;

    if-eqz v1, :cond_0

    .line 245
    iget-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0, v1}, LuW;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LuW;

    .line 249
    :cond_0
    invoke-virtual {v0}, LuW;->a()Landroid/content/Intent;

    move-result-object v0

    .line 250
    invoke-virtual {p0, v0, v5}, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->startActivityForResult(Landroid/content/Intent;I)V

    .line 251
    sget-object v0, LuD;->c:LuD;

    return-object v0

    .line 229
    :cond_1
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 230
    iget-object v3, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:LaGg;

    invoke-interface {v3, v0}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFZ;

    move-result-object v0

    .line 231
    invoke-interface {v0}, LaFV;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;)LuD;
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->b()LuD;

    move-result-object v0

    return-object v0
.end method

.method private c()LuD;
    .locals 7

    .prologue
    .line 255
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a()LuK;

    move-result-object v0

    .line 256
    if-nez v0, :cond_0

    .line 257
    sget-object v0, LuD;->a:LuD;

    .line 297
    :goto_0
    return-object v0

    .line 260
    :cond_0
    iget-object v1, v0, LuK;->a:LaGu;

    .line 261
    iget-object v2, v0, LuK;->a:LaFV;

    .line 262
    iget-object v3, v0, LuK;->b:LaFV;

    .line 264
    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a(LaFV;LaFV;)I

    move-result v4

    .line 265
    if-gtz v4, :cond_1

    .line 266
    sget-object v0, LuD;->e:LuD;

    goto :goto_0

    .line 269
    :cond_1
    invoke-static {p0}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v5

    .line 270
    sget v0, Lxi;->move_confirm_dialog_title:I

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 272
    if-eqz v2, :cond_2

    invoke-interface {v2}, LaFV;->c()Ljava/lang/String;

    move-result-object v0

    .line 273
    :goto_1
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 274
    invoke-interface {v1}, LaGu;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v6

    const/4 v1, 0x1

    aput-object v0, v2, v1

    const/4 v0, 0x2

    invoke-interface {v3}, LaFV;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    .line 273
    invoke-virtual {p0, v4, v2}, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 275
    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 277
    sget v0, Lxi;->move:I

    new-instance v1, Luy;

    invoke-direct {v1, p0}, Luy;-><init>(Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;)V

    invoke-virtual {v5, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 283
    const/high16 v0, 0x1040000

    new-instance v1, Luz;

    invoke-direct {v1, p0}, Luz;-><init>(Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;)V

    invoke-virtual {v5, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 289
    new-instance v0, LuA;

    invoke-direct {v0, p0}, LuA;-><init>(Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;)V

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 295
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 297
    sget-object v0, LuD;->d:LuD;

    goto :goto_0

    .line 272
    :cond_2
    const-string v0, ""

    goto :goto_1
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;)LuD;
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->c()LuD;

    move-result-object v0

    return-object v0
.end method

.method private d()LuD;
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 330
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a()LuK;

    move-result-object v1

    .line 331
    if-nez v1, :cond_0

    .line 332
    sget-object v0, LuD;->a:LuD;

    .line 359
    :goto_0
    return-object v0

    .line 335
    :cond_0
    iget-object v0, v1, LuK;->a:LaFV;

    .line 336
    if-eqz v0, :cond_1

    .line 337
    invoke-interface {v0}, LaFV;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 338
    :goto_1
    iget-object v2, v1, LuK;->b:LaFV;

    invoke-interface {v2}, LaFV;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    .line 339
    iget-object v3, v1, LuK;->a:LaGu;

    .line 340
    iget-object v4, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:LUi;

    invoke-interface {v3}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:Lald;

    .line 341
    invoke-static {p0, v6}, LUo;->a(Landroid/content/Context;Lald;)LaHy;

    move-result-object v6

    .line 340
    invoke-interface {v4, v5, v0, v2, v6}, LUi;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;LaHy;)V

    .line 344
    if-nez v0, :cond_2

    .line 345
    sget v0, Lxi;->move_toast_no_source_folder:I

    new-array v2, v9, [Ljava/lang/Object;

    iget-object v4, v1, LuK;->a:LaGu;

    .line 346
    invoke-interface {v4}, LaGu;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v8

    iget-object v1, v1, LuK;->b:LaFV;

    .line 347
    invoke-interface {v1}, LaFV;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v7

    .line 345
    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 353
    :goto_2
    invoke-static {p0, v0, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 355
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->b:LqK;

    const-string v1, "doclist"

    const-string v2, "moveEvent"

    .line 357
    invoke-static {v3}, Lala;->a(LaGu;)Ljava/lang/String;

    move-result-object v3

    .line 355
    invoke-virtual {v0, v1, v2, v3}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    sget-object v0, LuD;->f:LuD;

    goto :goto_0

    .line 337
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 349
    :cond_2
    sget v0, Lxi;->move_toast_with_source_folder:I

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, v1, LuK;->a:LaGu;

    .line 350
    invoke-interface {v4}, LaGu;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v8

    iget-object v4, v1, LuK;->a:LaFV;

    .line 351
    invoke-interface {v4}, LaFV;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v7

    iget-object v1, v1, LuK;->b:LaFV;

    invoke-interface {v1}, LaFV;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v9

    .line 349
    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public static synthetic d(Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;)LuD;
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->d()LuD;

    move-result-object v0

    return-object v0
.end method

.method private e()LuD;
    .locals 1

    .prologue
    .line 395
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->finish()V

    .line 396
    sget-object v0, LuD;->f:LuD;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;)LuD;
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->e()LuD;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 180
    if-nez p1, :cond_1

    .line 181
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 182
    sget-object v0, LuD;->c:LuD;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:LuL;

    .line 183
    invoke-virtual {v1}, LuL;->a()LuD;

    move-result-object v1

    invoke-virtual {v0, v1}, LuD;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 182
    invoke-static {v0}, LbiT;->b(Z)V

    .line 184
    const-string v0, "entrySpec.v2"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:LuL;

    sget-object v1, LuD;->d:LuD;

    invoke-virtual {v0, v1}, LuL;->a(LuD;)V

    .line 193
    :goto_0
    return-void

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:LuL;

    sget-object v1, LuD;->f:LuD;

    invoke-virtual {v0, v1}, LuL;->a(LuD;)V

    goto :goto_0

    .line 191
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:LuL;

    invoke-virtual {v0}, LuL;->a()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 152
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 154
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "entrySpec.v2"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 156
    const/4 v0, 0x0

    .line 157
    if-eqz p1, :cond_0

    .line 158
    const-string v0, "movingState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LuD;

    .line 159
    const-string v1, "collectionEntrySpec"

    .line 160
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 162
    :cond_0
    if-nez v0, :cond_1

    .line 163
    sget-object v0, LuD;->a:LuD;

    .line 166
    :cond_1
    new-instance v1, LuL;

    invoke-direct {v1, p0, v0}, LuL;-><init>(Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;LuD;)V

    iput-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:LuL;

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:LuL;

    invoke-virtual {v0}, LuL;->a()V

    .line 168
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 172
    invoke-super {p0, p1}, Lrm;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 173
    const-string v0, "movingState"

    iget-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->a:LuL;

    invoke-virtual {v1}, LuL;->a()LuD;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 174
    const-string v0, "collectionEntrySpec"

    iget-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivityLegacy;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 175
    return-void
.end method
