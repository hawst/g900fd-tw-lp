.class public Lcom/google/android/apps/docs/doclist/documentopener/BlobFileExportDocumentOpener;
.super Ljava/lang/Object;
.source "BlobFileExportDocumentOpener.java"

# interfaces
.implements LFR;


# instance fields
.field private final a:LFR;


# direct methods
.method public constructor <init>(LHm;LGd;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    sget v0, Lxi;->exporting_document:I

    .line 28
    invoke-virtual {p2, v0}, LGd;->a(I)Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    move-result-object v0

    invoke-interface {p1, v0}, LHm;->a(Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;)Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/BlobFileExportDocumentOpener;->a:LFR;

    .line 29
    return-void
.end method


# virtual methods
.method public a(LFT;LaGo;Landroid/os/Bundle;)LbsU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LFT;",
            "LaGo;",
            "Landroid/os/Bundle;",
            ")",
            "LbsU",
            "<",
            "LDL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/BlobFileExportDocumentOpener;->a:LFR;

    invoke-interface {v0, p1, p2, p3}, LFR;->a(LFT;LaGo;Landroid/os/Bundle;)LbsU;

    move-result-object v0

    return-object v0
.end method
