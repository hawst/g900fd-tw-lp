.class public Lcom/google/android/apps/docs/welcome/RedeemVoucherFailureDialog;
.super Lcom/google/android/apps/docs/tools/gelly/android/GuiceDialogFragment;
.source "RedeemVoucherFailureDialog.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/16 v4, 0x28

    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/RedeemVoucherFailureDialog;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 41
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/RedeemVoucherFailureDialog;->a()LH;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 42
    new-instance v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/RedeemVoucherFailureDialog;->a()LH;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 43
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    invoke-virtual {v2, v4, v4, v4, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 45
    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/RedeemVoucherFailureDialog;->a()LH;

    move-result-object v0

    invoke-static {v0}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v0

    sget v2, Lxi;->welcome_offer_failed_title:I

    .line 47
    invoke-virtual {v0, v2}, LEU;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 48
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lxi;->welcome_offer_button_close:I

    new-instance v2, LasH;

    invoke-direct {v2, p0}, LasH;-><init>(Lcom/google/android/apps/docs/welcome/RedeemVoucherFailureDialog;)V

    .line 49
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/google/android/apps/docs/welcome/RedeemVoucherFailureDialog;
    .locals 2

    .prologue
    .line 32
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 33
    const-string v1, "message"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/welcome/RedeemVoucherFailureDialog;->e(Landroid/os/Bundle;)V

    .line 35
    return-object p0
.end method
