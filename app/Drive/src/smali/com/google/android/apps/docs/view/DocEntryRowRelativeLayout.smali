.class public Lcom/google/android/apps/docs/view/DocEntryRowRelativeLayout;
.super Landroid/widget/RelativeLayout;
.source "DocEntryRowRelativeLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    return-void
.end method


# virtual methods
.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 46
    const/4 v1, 0x3

    new-array v1, v1, [I

    sget v2, Lxc;->title:I

    aput v2, v1, v0

    sget v2, Lxc;->statusLabels:I

    aput v2, v1, v5

    const/4 v2, 0x2

    sget v3, Lxc;->sortLabel:I

    aput v3, v1, v2

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_1

    aget v3, v1, v0

    .line 47
    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/view/DocEntryRowRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 48
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    .line 49
    invoke-virtual {v3, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    .line 46
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 52
    :cond_1
    return v5
.end method

.method protected getContextMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 2

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/DocEntryRowRelativeLayout;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBy;

    .line 58
    new-instance v1, Lapm;

    invoke-virtual {v0}, LBy;->a()LaGu;

    move-result-object v0

    invoke-direct {v1, v0}, Lapm;-><init>(LaGu;)V

    return-object v1
.end method
