.class public Lcom/google/android/apps/docs/view/PinWarningDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "PinWarningDialogFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/PinWarningDialogFragment;->a()LH;

    move-result-object v0

    invoke-static {v0}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v0

    sget v1, Lxi;->pin_warning_external_storage_not_ready:I

    .line 22
    invoke-virtual {v0, v1}, LEU;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lxi;->pin_warning_external_storage_not_ready_ok:I

    new-instance v2, Laqy;

    invoke-direct {v2, p0}, Laqy;-><init>(Lcom/google/android/apps/docs/view/PinWarningDialogFragment;)V

    .line 23
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
