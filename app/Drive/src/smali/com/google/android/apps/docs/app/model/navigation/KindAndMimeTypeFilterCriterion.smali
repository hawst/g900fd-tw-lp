.class public Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;
.super Ljava/lang/Object;
.source "KindAndMimeTypeFilterCriterion.java"

# interfaces
.implements Lcom/google/android/apps/docs/app/model/navigation/Criterion;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "LaGv;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Z

.field private final b:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 106
    new-instance v0, Lwd;

    invoke-direct {v0}, Lwd;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/util/Set;Ljava/util/Set;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LaGv;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "allowedKinds is empty. Consider using MimeTypeFilterCriterion."

    invoke-static {v0, v3}, LbiT;->a(ZLjava/lang/Object;)V

    .line 37
    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v1

    :cond_0
    const-string v0, "allowedMimeTypes is empty. Consider using KindFilterCriterion."

    invoke-static {v2, v0}, LbiT;->a(ZLjava/lang/Object;)V

    .line 43
    sget-object v0, LaGv;->a:LaGv;

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 44
    sget-object v0, LaGv;->a:LaGv;

    invoke-interface {p1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move p3, v1

    .line 48
    :cond_1
    sget-object v0, LaGv;->a:LaGv;

    invoke-virtual {v0}, LaGv;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 49
    sget-object v0, LaGv;->a:LaGv;

    invoke-virtual {v0}, LaGv;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 53
    :goto_1
    invoke-static {p1}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;->a:LbmY;

    .line 54
    invoke-static {p2}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;->b:LbmY;

    .line 55
    iput-boolean v1, p0, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;->a:Z

    .line 56
    return-void

    :cond_2
    move v0, v2

    .line 35
    goto :goto_0

    :cond_3
    move v1, p3

    goto :goto_1
.end method


# virtual methods
.method public a(LaGM;)LaeZ;
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;->a:LbmY;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;->b:LbmY;

    invoke-static {v0, v1}, LaeZ;->a(LbmY;LbmY;)LaeZ;

    move-result-object v0

    return-object v0
.end method

.method public a(LvR;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LvR",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;->a:LbmY;

    invoke-static {v0}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;->b:LbmY;

    .line 141
    invoke-static {v1}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;->a:Z

    .line 140
    invoke-interface {p1, v0, v1, v2}, LvR;->a(LbmY;LbmY;Z)V

    .line 142
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 73
    if-ne p1, p0, :cond_1

    .line 81
    :cond_0
    :goto_0
    return v0

    .line 75
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;

    if-eqz v2, :cond_3

    .line 76
    check-cast p1, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;

    .line 77
    iget-object v2, p0, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;->a:LbmY;

    iget-object v3, p1, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;->a:LbmY;

    invoke-virtual {v2, v3}, LbmY;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;->b:LbmY;

    iget-object v3, p1, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;->b:LbmY;

    .line 78
    invoke-virtual {v2, v3}, LbmY;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;->a:Z

    iget-boolean v3, p1, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;->a:Z

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 81
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 87
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/apps/docs/app/model/navigation/EntriesFilterCriterion;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;->a:LbmY;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;->b:LbmY;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;->a:Z

    .line 88
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 87
    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 93
    const-class v0, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;

    invoke-static {v0}, LbiL;->a(Ljava/lang/Class;)LbiN;

    move-result-object v0

    const-string v1, "allowedKinds"

    iget-object v2, p0, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;->a:LbmY;

    .line 94
    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;Ljava/lang/Object;)LbiN;

    move-result-object v0

    const-string v1, "allowedMimeTypes"

    iget-object v2, p0, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;->b:LbmY;

    .line 95
    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;Ljava/lang/Object;)LbiN;

    move-result-object v0

    const-string v1, "includeFolder"

    iget-boolean v2, p0, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;->a:Z

    .line 96
    invoke-virtual {v0, v1, v2}, LbiN;->a(Ljava/lang/String;Z)LbiN;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, LbiN;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;->a:LbmY;

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/Collection;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;->b:LbmY;

    new-array v2, v1, [Ljava/lang/String;

    invoke-virtual {v0, v2}, LbmY;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 135
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/model/navigation/KindAndMimeTypeFilterCriterion;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 136
    return-void

    :cond_0
    move v0, v1

    .line 135
    goto :goto_0
.end method
