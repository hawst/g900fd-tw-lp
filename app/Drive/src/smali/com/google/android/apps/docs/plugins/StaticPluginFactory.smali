.class public Lcom/google/android/apps/docs/plugins/StaticPluginFactory;
.super LVW;
.source "StaticPluginFactory.java"


# annotations
.annotation build Lcom/google/android/apps/docs/neocommon/proguard/KeepAfterProguard;
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .annotation build Lcom/google/android/apps/docs/neocommon/proguard/KeepAfterProguard;
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1}, LVW;-><init>(Ljava/lang/String;)V

    .line 20
    return-void
.end method


# virtual methods
.method public createPlugins()Ljava/util/List;
    .locals 2
    .annotation build Lcom/google/android/apps/docs/neocommon/proguard/KeepAfterProguard;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LVU;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 26
    const-string v1, ":"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/plugins/StaticPluginFactory;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 27
    new-instance v1, LpF;

    invoke-direct {v1}, LpF;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 29
    :cond_0
    const-string v1, ":"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/plugins/StaticPluginFactory;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 30
    new-instance v1, LOI;

    invoke-direct {v1}, LOI;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32
    :cond_1
    const-string v1, ":"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/plugins/StaticPluginFactory;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 33
    new-instance v1, LatT;

    invoke-direct {v1}, LatT;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 35
    :cond_2
    const-string v1, ":"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/plugins/StaticPluginFactory;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 36
    new-instance v1, LxM;

    invoke-direct {v1}, LxM;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    :cond_3
    const-string v1, ":"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/plugins/StaticPluginFactory;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 39
    new-instance v1, LPd;

    invoke-direct {v1}, LPd;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    :cond_4
    const-string v1, ":"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/plugins/StaticPluginFactory;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 42
    new-instance v1, LNu;

    invoke-direct {v1}, LNu;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    :cond_5
    const-string v1, ":"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/plugins/StaticPluginFactory;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 45
    new-instance v1, LNG;

    invoke-direct {v1}, LNG;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    :cond_6
    const-string v1, ":"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/plugins/StaticPluginFactory;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 48
    new-instance v1, Lqm;

    invoke-direct {v1}, Lqm;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    :cond_7
    const-string v1, ":"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/plugins/StaticPluginFactory;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 51
    new-instance v1, LSq;

    invoke-direct {v1}, LSq;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    :cond_8
    invoke-static {v0}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    return-object v0
.end method

.method public createSecondaryPlugins()Ljava/util/List;
    .locals 1
    .annotation build Lcom/google/android/apps/docs/neocommon/proguard/KeepAfterProguard;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LVU;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 60
    invoke-static {v0}, LbmF;->a(Ljava/util/Collection;)LbmF;

    move-result-object v0

    return-object v0
.end method
