.class public Lcom/google/android/apps/docs/welcome/WelcomeFragment;
.super Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;
.source "WelcomeFragment.java"


# instance fields
.field private a:I

.field private a:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private a:Landroid/widget/Button;

.field public a:Lasl;

.field public a:Lass;

.field private a:LatA;

.field private a:LatB;

.field public a:Latd;

.field private a:Latz;

.field private a:Lcom/google/android/apps/docs/welcome/HighlightsViewPager;

.field public a:LqF;

.field public a:LqK;

.field private a:Z

.field private b:Landroid/widget/Button;

.field private b:Z

.field private c:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;-><init>()V

    .line 136
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:I

    .line 138
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Landroid/os/AsyncTask;

    .line 401
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:I

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;I)I
    .locals 0

    .prologue
    .line 47
    iput p1, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:I

    return p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)Landroid/os/AsyncTask;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Landroid/os/AsyncTask;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->c:Landroid/widget/Button;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)LatB;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:LatB;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)Latz;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Latz;

    return-object v0
.end method

.method public static a(Landroid/os/Bundle;)Lcom/google/android/apps/docs/welcome/WelcomeFragment;
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;-><init>()V

    .line 68
    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->e(Landroid/os/Bundle;)V

    .line 69
    return-object v0
.end method

.method private a(LasO;II)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 380
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a()LH;

    move-result-object v1

    invoke-static {v1, p1}, LasJ;->a(Landroid/content/Context;LasO;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 381
    if-le p3, v6, :cond_0

    .line 382
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lxi;->page_counter_format:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 384
    :cond_0
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;LasO;II)Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a(LasO;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/support/v4/view/ViewPager;LasJ;Latc;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 227
    invoke-virtual {p2}, LasJ;->a()I

    move-result v0

    .line 228
    iget-object v1, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Latz;

    invoke-static {v1, v0}, Latz;->a(Latz;I)V

    .line 230
    new-instance v1, LatA;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a()LH;

    move-result-object v2

    invoke-virtual {v2}, LH;->a()LM;

    move-result-object v2

    invoke-direct {v1, p0, v2, p2, p3}, LatA;-><init>(Lcom/google/android/apps/docs/welcome/WelcomeFragment;LM;LasJ;Latc;)V

    iput-object v1, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:LatA;

    .line 231
    iget-object v1, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:LatA;

    invoke-virtual {p1, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LdN;)V

    .line 232
    invoke-virtual {p1, v3}, Landroid/support/v4/view/ViewPager;->setFocusable(Z)V

    .line 233
    invoke-virtual {p1, v3}, Landroid/support/v4/view/ViewPager;->setFocusableInTouchMode(Z)V

    .line 234
    iget-object v1, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:LatA;

    invoke-virtual {v1}, LatA;->a()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 236
    new-instance v1, Latq;

    invoke-direct {v1, p0}, Latq;-><init>(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)V

    .line 264
    invoke-virtual {p1, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LeT;)V

    .line 265
    invoke-interface {v1, v3}, LeT;->a(I)V

    .line 267
    new-instance v1, Latr;

    invoke-direct {v1, p0, p1, v0}, Latr;-><init>(Lcom/google/android/apps/docs/welcome/WelcomeFragment;Landroid/support/v4/view/ViewPager;I)V

    iput-object v1, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:LatB;

    .line 274
    return-void
.end method

.method private a(Lcom/google/android/apps/docs/welcome/Page;)V
    .locals 4

    .prologue
    .line 507
    invoke-virtual {p1}, Lcom/google/android/apps/docs/welcome/Page;->c()Ljava/lang/String;

    move-result-object v1

    .line 508
    invoke-virtual {p1}, Lcom/google/android/apps/docs/welcome/Page;->b()Ljava/lang/String;

    move-result-object v0

    .line 509
    invoke-direct {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->c()Ljava/lang/String;

    move-result-object v2

    .line 513
    if-nez v1, :cond_0

    if-nez v0, :cond_0

    .line 514
    invoke-direct {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->b()Ljava/lang/String;

    move-result-object v1

    .line 515
    invoke-direct {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->d()Ljava/lang/String;

    move-result-object v0

    .line 520
    :cond_0
    invoke-static {v1}, Lbju;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 521
    invoke-direct {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->b()Ljava/lang/String;

    move-result-object v1

    .line 527
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 528
    invoke-direct {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->d()Ljava/lang/String;

    move-result-object v0

    .line 531
    :cond_2
    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->x()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;Lcom/google/android/apps/docs/welcome/Page;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->b(Lcom/google/android/apps/docs/welcome/Page;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 363
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 365
    :try_start_0
    invoke-static {p1}, LqH;->valueOf(Ljava/lang/String;)LqH;

    move-result-object v0

    .line 366
    new-instance v1, LqJ;

    invoke-direct {v1, v0}, LqJ;-><init>(LqH;)V

    invoke-virtual {v1}, LqJ;->a()LqI;

    move-result-object v0

    .line 368
    iget-object v1, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:LqF;

    invoke-virtual {v1, v0}, LqF;->a(LqI;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 372
    :goto_0
    return-void

    .line 369
    :catch_0
    move-exception v0

    .line 370
    const-string v1, "WelcomeFragment"

    const-string v2, "Invalid event enum label (%s)"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 485
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->b(Ljava/lang/String;)V

    .line 486
    invoke-direct {p0, p2}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->c(Ljava/lang/String;)V

    .line 487
    invoke-direct {p0, p3}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->d(Ljava/lang/String;)V

    .line 488
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->b:Z

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/welcome/WelcomeFragment;Z)Z
    .locals 0

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->b:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->b:Landroid/widget/Button;

    return-object v0
.end method

.method private b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 465
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "closeButtonText"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 466
    if-nez v0, :cond_0

    .line 467
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a()LH;

    move-result-object v0

    sget v1, Lxi;->welcome_button_close:I

    invoke-virtual {v0, v1}, LH;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 469
    :cond_0
    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/google/android/apps/docs/welcome/Page;)V
    .locals 2

    .prologue
    .line 543
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 544
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 547
    :cond_0
    new-instance v0, Latx;

    invoke-direct {v0, p0, p1}, Latx;-><init>(Lcom/google/android/apps/docs/welcome/WelcomeFragment;Lcom/google/android/apps/docs/welcome/Page;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Landroid/os/AsyncTask;

    .line 586
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a()LH;

    move-result-object v0

    new-instance v1, Laty;

    invoke-direct {v1, p0}, Laty;-><init>(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)V

    invoke-virtual {v0, v1}, LH;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 592
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/welcome/WelcomeFragment;Lcom/google/android/apps/docs/welcome/Page;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a(Lcom/google/android/apps/docs/welcome/Page;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 438
    if-eqz p1, :cond_0

    .line 439
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 440
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 444
    :goto_0
    return-void

    .line 442
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method private c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 473
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a()LH;

    move-result-object v0

    sget v1, Lxi;->welcome_button_continue:I

    invoke-virtual {v0, v1}, LH;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 474
    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 447
    if-eqz p1, :cond_0

    .line 448
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->c:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 449
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->c:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 453
    :goto_0
    return-void

    .line 451
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->c:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method private d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 478
    .line 479
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "positiveButtonText"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 480
    return-object v0
.end method

.method private d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 456
    if-eqz p1, :cond_0

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->b:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 458
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->b:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 462
    :goto_0
    return-void

    .line 460
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->b:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method private t()V
    .locals 0

    .prologue
    .line 277
    invoke-direct {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->u()V

    .line 278
    invoke-direct {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->v()V

    .line 279
    invoke-direct {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->w()V

    .line 280
    return-void
.end method

.method private u()V
    .locals 2

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Landroid/widget/Button;

    new-instance v1, Lats;

    invoke-direct {v1, p0}, Lats;-><init>(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 296
    return-void
.end method

.method private v()V
    .locals 2

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->c:Landroid/widget/Button;

    new-instance v1, Latt;

    invoke-direct {v1, p0}, Latt;-><init>(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 305
    return-void
.end method

.method private w()V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 308
    iget-boolean v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Z

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->b:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->b:Landroid/widget/Button;

    new-instance v1, Latu;

    invoke-direct {v1, p0}, Latu;-><init>(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 360
    :goto_0
    return-void

    .line 333
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a()Landroid/os/Bundle;

    move-result-object v1

    .line 334
    if-nez v1, :cond_1

    .line 335
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->b:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 339
    :cond_1
    const-string v0, "positiveButtonIntent"

    .line 340
    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 341
    if-nez v0, :cond_2

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->b:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 344
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->b:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 345
    iget-object v2, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->b:Landroid/widget/Button;

    new-instance v3, Latw;

    invoke-direct {v3, p0, v1, v0}, Latw;-><init>(Lcom/google/android/apps/docs/welcome/WelcomeFragment;Landroid/os/Bundle;Landroid/content/Intent;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private x()V
    .locals 1

    .prologue
    .line 375
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a()V

    .line 376
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a()LH;

    move-result-object v0

    invoke-virtual {v0}, LH;->finish()V

    .line 377
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    .line 146
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 148
    sget v0, Lxe;->welcome:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 149
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a()Landroid/os/Bundle;

    move-result-object v2

    .line 150
    const-string v0, "isPhotoBackupAnnouncement"

    .line 151
    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Z

    .line 153
    sget v0, Lxc;->welcome_pager:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/welcome/HighlightsViewPager;

    iput-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Lcom/google/android/apps/docs/welcome/HighlightsViewPager;

    .line 154
    new-instance v3, Latz;

    sget v0, Lxc;->welcome_page_indicator:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4}, Latz;-><init>(Landroid/widget/ProgressBar;Latp;)V

    iput-object v3, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Latz;

    .line 155
    sget v0, Lxc;->welcome_button_close:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Landroid/widget/Button;

    .line 156
    sget v0, Lxc;->welcome_button_positive:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->b:Landroid/widget/Button;

    .line 157
    sget v0, Lxc;->welcome_button_continue:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->c:Landroid/widget/Button;

    .line 159
    invoke-static {v2}, LasJ;->a(Landroid/os/Bundle;)LasJ;

    move-result-object v0

    .line 160
    iget-object v3, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Lcom/google/android/apps/docs/welcome/HighlightsViewPager;

    const-string v4, "screenType"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Latc;->valueOf(Ljava/lang/String;)Latc;

    move-result-object v2

    invoke-direct {p0, v3, v0, v2}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a(Landroid/support/v4/view/ViewPager;LasJ;Latc;)V

    .line 161
    invoke-direct {p0}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->t()V

    .line 163
    if-eqz p3, :cond_0

    .line 164
    const-string v0, "savedCloseButtonText"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "savedContinueButtonText"

    .line 165
    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "savedPositiveButtonText"

    .line 166
    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 164
    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    :cond_0
    return-object v1
.end method

.method a()V
    .locals 4

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Latd;

    invoke-virtual {v0}, Latd;->c()V

    .line 389
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:LqK;

    const-string v1, "warmWelcome"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exitAtPage#"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Latz;

    .line 390
    invoke-virtual {v3}, Latz;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 389
    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 219
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->c(Landroid/os/Bundle;)V

    .line 221
    const-string v0, "savedCloseButtonText"

    iget-object v1, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const-string v0, "savedContinueButtonText"

    iget-object v1, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->c:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    const-string v0, "savedPositiveButtonText"

    iget-object v1, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->b:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 395
    invoke-super {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->e()V

    .line 396
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 399
    :cond_0
    return-void
.end method

.method public o_()V
    .locals 2

    .prologue
    .line 174
    invoke-super {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->o_()V

    .line 176
    iget-boolean v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Z

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Lcom/google/android/apps/docs/welcome/HighlightsViewPager;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/welcome/HighlightsViewPager;->d()V

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Latz;

    invoke-static {v0}, Latz;->a(Latz;)V

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->c:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/WelcomeFragment;->a:Lass;

    new-instance v1, Latp;

    invoke-direct {v1, p0}, Latp;-><init>(Lcom/google/android/apps/docs/welcome/WelcomeFragment;)V

    invoke-virtual {v0, v1}, Lass;->a(Lasw;)V

    .line 215
    :cond_0
    return-void
.end method
