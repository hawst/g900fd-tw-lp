.class public Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;
.super Ljava/lang/Object;
.source "DocumentTypeFilter.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;


# instance fields
.field private final a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "LaGv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    .line 22
    invoke-static {}, LbmY;->a()LbmY;

    move-result-object v1

    const-class v2, LaGv;

    invoke-static {v2}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v2

    invoke-static {v2}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;-><init>(LbmY;LbmY;)V

    sput-object v0, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->a:Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    .line 125
    new-instance v0, LCb;

    invoke-direct {v0}, LCb;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(LbmY;LbmY;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LbmY",
            "<",
            "LaGv;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {}, LbmY;->a()Lbna;

    move-result-object v1

    .line 30
    invoke-static {p2}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->b:LbmY;

    .line 31
    invoke-virtual {p2}, LbmY;->a()Lbqv;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGv;

    .line 32
    invoke-virtual {v0}, LaGv;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 33
    invoke-virtual {v0}, LaGv;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbna;->a(Ljava/lang/Object;)Lbna;

    goto :goto_0

    .line 39
    :cond_0
    invoke-static {}, LbmY;->a()Lbna;

    move-result-object v0

    .line 43
    :goto_1
    invoke-virtual {v0, p1}, Lbna;->a(Ljava/lang/Iterable;)Lbna;

    .line 44
    invoke-virtual {v0}, Lbna;->a()LbmY;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->a:LbmY;

    .line 45
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public synthetic constructor <init>(LbmY;LbmY;LCb;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;-><init>(LbmY;LbmY;)V

    return-void
.end method

.method public static varargs a([LaGv;)Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;
    .locals 3

    .prologue
    .line 56
    new-instance v0, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    invoke-static {}, LbmY;->a()LbmY;

    move-result-object v1

    invoke-static {p0}, LbmY;->a([Ljava/lang/Object;)LbmY;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;-><init>(LbmY;LbmY;)V

    return-object v0
.end method


# virtual methods
.method public a(LvL;LvN;)LvN;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->a:LbmY;

    invoke-virtual {v0}, LbmY;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->b:LbmY;

    invoke-interface {p1, v0}, LvL;->a(LbmY;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v0

    invoke-virtual {p2, v0}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    move-result-object v0

    .line 101
    :goto_0
    return-object v0

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->b:LbmY;

    invoke-virtual {v0}, LbmY;->a()Lbqv;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGv;

    .line 95
    invoke-virtual {v0}, LaGv;->b()Z

    move-result v2

    if-nez v2, :cond_1

    .line 96
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string v2, "The kind %s does not have a corresponding MIME type. Please consider using a filter based only on Kinds."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 98
    invoke-virtual {v0}, LaGv;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    .line 97
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 101
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->a:LbmY;

    invoke-interface {p1, v0, v4}, LvL;->a(LbmY;Z)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v0

    invoke-virtual {p2, v0}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;LaGv;)Z
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->a:LbmY;

    invoke-virtual {v0, p1}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->b:LbmY;

    invoke-virtual {v0, p2}, LbmY;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 107
    if-ne p0, p1, :cond_1

    .line 115
    :cond_0
    :goto_0
    return v0

    .line 109
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    if-eqz v2, :cond_3

    .line 110
    check-cast p1, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    .line 112
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->a:LbmY;

    iget-object v3, p1, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->a:LbmY;

    invoke-virtual {v2, v3}, LbmY;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->b:LbmY;

    iget-object v3, p1, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->b:LbmY;

    .line 113
    invoke-virtual {v2, v3}, LbmY;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 115
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 82
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->a:LbmY;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->b:LbmY;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 121
    const-string v0, "DocumentTypeFilter[%s, %s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->a:LbmY;

    .line 122
    invoke-virtual {v3}, LbmY;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->b:LbmY;

    invoke-virtual {v3}, LbmY;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 121
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->a:LbmY;

    invoke-static {v0}, LbnG;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->b:LbmY;

    invoke-static {v0}, LbnG;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 151
    return-void
.end method
