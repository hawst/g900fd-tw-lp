.class public Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "AddCollaboratorLoaderDialogFragment.java"

# interfaces
.implements Lacp;


# instance fields
.field public a:LM;

.field public a:LaKR;

.field public a:Labh;

.field private a:Labi;

.field public a:Lacj;

.field public a:Lald;

.field private a:Landroid/app/ProgressDialog;

.field private a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field public a:LvU;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    .line 43
    return-void
.end method

.method public static a(LM;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 3

    .prologue
    .line 100
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    const-string v0, "AddCollaboratorLoaderDialogFragment"

    .line 104
    invoke-virtual {p0, v0}, LM;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;

    .line 105
    if-eqz v0, :cond_0

    .line 106
    invoke-virtual {p0}, LM;->a()Lac;

    move-result-object v1

    invoke-virtual {v1, v0}, Lac;->a(Landroid/support/v4/app/Fragment;)Lac;

    move-result-object v0

    invoke-virtual {v0}, Lac;->b()I

    .line 108
    :cond_0
    new-instance v0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;-><init>()V

    .line 109
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 110
    const-string v2, "contactAddresses"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 111
    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->e(Landroid/os/Bundle;)V

    .line 113
    const-string v1, "AddCollaboratorLoaderDialogFragment"

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a(LM;Ljava/lang/String;)V

    .line 114
    return-void
.end method

.method private a(Lacr;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 158
    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Lacj;

    invoke-virtual {v0}, Lacj;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:LaKR;

    invoke-interface {v0}, LaKR;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Lald;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a()LH;

    move-result-object v1

    sget v2, Lxi;->sharing_error:I

    invoke-virtual {v1, v2}, LH;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Lald;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 164
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a()V

    .line 179
    :cond_0
    :goto_1
    return-void

    .line 162
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Lald;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a()LH;

    move-result-object v1

    sget v2, Lxi;->sharing_offline:I

    invoke-virtual {v1, v2}, LH;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Lald;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 165
    :cond_2
    if-eqz p1, :cond_0

    .line 166
    sget-object v0, Labi;->d:Labi;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Labi;

    invoke-virtual {v0, v1}, Labi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:LvU;

    invoke-interface {v0}, LvU;->a()V

    .line 168
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a()V

    goto :goto_1

    .line 170
    :cond_3
    sget-object v0, Labi;->b:Labi;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Labi;

    invoke-virtual {v0, v1}, Labi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Lacj;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0, v1}, Lacj;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 172
    sget-object v0, Labi;->c:Labi;

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Labi;

    goto :goto_1

    .line 174
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a()V

    goto :goto_1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;Lacr;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a(Lacr;)V

    return-void
.end method

.method private u()V
    .locals 3

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a()LH;

    move-result-object v0

    .line 149
    sget-object v1, Labi;->b:Labi;

    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Labi;

    invoke-virtual {v1, v2}, Labi;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150
    sget v1, Lxi;->sharing_progress_loading_message:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 154
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 155
    return-void

    .line 152
    :cond_0
    sget v1, Lxi;->sharing_progress_saving_message:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 141
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a()LH;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Landroid/app/ProgressDialog;

    .line 142
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->u()V

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 118
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a_(Landroid/os/Bundle;)V

    .line 120
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 121
    const-string v1, "contactAddresses"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-nez v0, :cond_1

    .line 123
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a()V

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Lacj;

    invoke-virtual {v0, p0}, Lacj;->a(Lacp;)V

    .line 129
    if-nez p1, :cond_2

    sget-object v0, Labi;->a:Labi;

    .line 130
    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Labi;

    .line 132
    sget-object v0, Labi;->a:Labi;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Labi;

    invoke-virtual {v0, v1}, Labi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    sget-object v0, Labi;->b:Labi;

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Labi;

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Labh;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Lacj;

    invoke-virtual {v0, v1}, Labh;->a(Lacj;)V

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Lacj;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0, v1}, Lacj;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    goto :goto_0

    .line 129
    :cond_2
    const-string v0, "state"

    .line 130
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Labi;

    goto :goto_1
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 183
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->c(Landroid/os/Bundle;)V

    .line 184
    const-string v0, "state"

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Labi;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 185
    return-void
.end method

.method public j_()V
    .locals 1

    .prologue
    .line 200
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->j_()V

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Labh;

    invoke-virtual {v0}, Labh;->a()V

    .line 202
    return-void
.end method

.method public k_()V
    .locals 2

    .prologue
    .line 189
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->k_()V

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Labh;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Lacj;

    invoke-virtual {v0, p0, v1}, Labh;->a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;Lacj;)V

    .line 191
    return-void
.end method

.method public o_()V
    .locals 0

    .prologue
    .line 195
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->o_()V

    .line 196
    return-void
.end method

.method public t()V
    .locals 1

    .prologue
    .line 210
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a()LH;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Lacj;

    invoke-virtual {v0}, Lacj;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 212
    sget-object v0, Labi;->d:Labi;

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a:Labi;

    .line 213
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->u()V

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 215
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorLoaderDialogFragment;->a()V

    goto :goto_0
.end method
