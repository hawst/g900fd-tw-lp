.class public Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "CooperateStateMachineProgressFragment.java"

# interfaces
.implements Lamr;


# instance fields
.field private a:J

.field private a:LDL;

.field private a:LEz;

.field private a:LaGo;

.field private a:LpD;

.field private b:J

.field private m:I

.field private w:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 59
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    .line 53
    iput-wide v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:J

    .line 54
    iput-wide v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->b:J

    .line 60
    return-void
.end method

.method private constructor <init>(LDL;LaGo;I)V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 63
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    .line 53
    iput-wide v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:J

    .line 54
    iput-wide v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->b:J

    .line 64
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 65
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LDL;

    .line 66
    iput-object p2, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LaGo;

    .line 67
    iput p3, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->m:I

    .line 68
    return-void

    .line 64
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;)LDL;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LDL;

    return-object v0
.end method

.method public static a(LM;LDL;LaGo;)V
    .locals 2

    .prologue
    .line 193
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    const-string v0, "CooperateStateMachineProgressFragment"

    .line 198
    invoke-virtual {p0, v0}, LM;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;

    .line 199
    if-eqz v0, :cond_0

    .line 200
    invoke-virtual {p0}, LM;->a()Lac;

    move-result-object v1

    invoke-virtual {v1, v0}, Lac;->a(Landroid/support/v4/app/Fragment;)Lac;

    move-result-object v0

    invoke-virtual {v0}, Lac;->b()I

    .line 202
    :cond_0
    new-instance v0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;

    const/4 v1, 0x1

    invoke-direct {v0, p1, p2, v1}, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;-><init>(LDL;LaGo;I)V

    .line 204
    const-string v1, "CooperateStateMachineProgressFragment"

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a(LM;Ljava/lang/String;)V

    .line 205
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;Z)Z
    .locals 0

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->w:Z

    return p1
.end method

.method private t()V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LpD;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LpD;

    invoke-virtual {v0}, LpD;->a()V

    .line 187
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LpD;

    .line 189
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a()LH;

    move-result-object v0

    .line 73
    new-instance v1, LEz;

    iget v2, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->m:I

    invoke-direct {v1, v0, v2}, LEz;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LEz;

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LaGo;

    if-nez v0, :cond_0

    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a()V

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LEz;

    .line 99
    :goto_0
    return-object v0

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LaGo;

    invoke-interface {v0}, LaGo;->a()LaGv;

    move-result-object v0

    .line 89
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LaGo;

    invoke-interface {v1}, LaGo;->f()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LaGo;

    .line 90
    invoke-interface {v2}, LaGo;->d()Z

    move-result v2

    .line 89
    invoke-static {v0, v1, v2}, LaGt;->b(LaGv;Ljava/lang/String;Z)I

    move-result v0

    .line 91
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LEz;

    invoke-virtual {v1, v0}, LEz;->setIcon(I)V

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LEz;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LDL;

    invoke-interface {v1}, LDL;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LEz;->setTitle(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LEz;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LEz;->setCancelable(Z)V

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LEz;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LEz;->setCanceledOnTouchOutside(Z)V

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LDL;

    invoke-interface {v0, p0}, LDL;->a(Lamr;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LEz;

    goto :goto_0
.end method

.method public a(JJLjava/lang/String;)V
    .locals 7

    .prologue
    .line 104
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 110
    iget-wide v2, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:J

    cmp-long v1, p1, v2

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->b:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x64

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    const-string v1, "CooperateStateMachineProgressFragment"

    const-string v2, "Progress: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 114
    iput-wide p1, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:J

    .line 115
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->b:J

    .line 116
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LEz;

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, LEz;->b(JJLjava/lang/String;)V

    goto :goto_0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 121
    const-string v0, "CooperateStateMachineProgressFragment"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a_(Landroid/os/Bundle;)V

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LDL;

    if-nez v0, :cond_0

    .line 125
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a()V

    .line 158
    :goto_0
    return-void

    .line 129
    :cond_0
    new-instance v0, LAg;

    invoke-direct {v0, p0}, LAg;-><init>(Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LpD;

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LpD;

    invoke-virtual {v0}, LpD;->start()V

    goto :goto_0
.end method

.method public h()V
    .locals 2

    .prologue
    .line 171
    const-string v0, "CooperateStateMachineProgressFragment"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->t()V

    .line 173
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->h()V

    .line 174
    return-void
.end method

.method public o_()V
    .locals 1

    .prologue
    .line 162
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->o_()V

    .line 164
    iget-boolean v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->w:Z

    if-eqz v0, :cond_0

    .line 165
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a()V

    .line 167
    :cond_0
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 178
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->t()V

    .line 180
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a()LH;

    move-result-object v0

    .line 181
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 182
    return-void
.end method
