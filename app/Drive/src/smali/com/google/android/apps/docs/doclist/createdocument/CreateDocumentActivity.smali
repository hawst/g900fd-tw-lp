.class public Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;
.super Lrm;
.source "CreateDocumentActivity.java"


# instance fields
.field public a:LTT;

.field public a:LaFO;

.field private a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LAt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lrm;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 178
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 182
    const-class v1, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 183
    const-string v1, "collectionEntrySpec"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 184
    const-string v1, "accountName"

    invoke-virtual {p1}, LaFO;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 186
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;->j()V

    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 95
    sget v0, Lxc;->choice_create_background:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, LEk;

    invoke-direct {v2, p0, v0}, LEk;-><init>(Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 116
    return-void
.end method

.method private j()V
    .locals 3

    .prologue
    .line 119
    sget v0, Lxc;->choice_create_background:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 121
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, LEl;

    invoke-direct {v2, p0, v0}, LEl;-><init>(Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 146
    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 147
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 156
    invoke-virtual {p0, p2, p3}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;->setResult(ILandroid/content/Intent;)V

    .line 157
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;->finish()V

    .line 158
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 151
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;->j()V

    .line 152
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 50
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 52
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 53
    const-string v1, "collectionEntrySpec"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 55
    sget v0, Lxe;->create_document_host_activity:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;->setContentView(I)V

    .line 56
    sget v0, Lxc;->choice_create_panel:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;

    .line 57
    new-instance v1, LEh;

    invoke-direct {v1, p0}, LEh;-><init>(Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/doclist/createdocument/SwipablePanelLayout;->setListener(LEv;)V

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LAt;

    .line 66
    invoke-interface {v0}, LAt;->a()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 67
    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;->a:LTT;

    invoke-interface {v0, p0, v3}, LAt;->a(Landroid/content/Context;LTT;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 68
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 71
    :cond_0
    new-instance v3, LEi;

    invoke-direct {v3, p0, v0}, LEi;-><init>(Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;LAt;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 83
    :cond_1
    sget v0, Lxc;->choice_create_background:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 84
    new-instance v1, LEj;

    invoke-direct {v1, p0}, LEj;-><init>(Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;->f()V

    .line 92
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 162
    invoke-super {p0}, Lrm;->onPause()V

    .line 166
    const/4 v0, 0x0

    const v1, 0x10a0001

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/doclist/createdocument/CreateDocumentActivity;->overridePendingTransition(II)V

    .line 167
    return-void
.end method
