.class public Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;
.super Ljava/lang/Object;
.source "AccountCriterion.java"

# interfaces
.implements Lcom/google/android/apps/docs/app/model/navigation/Criterion;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LaFO;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    new-instance v0, LvI;

    invoke-direct {v0}, LvI;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LaFO;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFO;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;->a:LaFO;

    .line 31
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;->a:LaFO;

    .line 71
    return-void
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;LvI;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public a()LaFO;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;->a:LaFO;

    return-object v0
.end method

.method public a(LaGM;)LaeZ;
    .locals 1

    .prologue
    .line 89
    sget-object v0, LaeZ;->a:LaeZ;

    return-object v0
.end method

.method public a(LvR;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LvR",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;->a:LaFO;

    invoke-interface {p1, v0}, LvR;->a(LaFO;)V

    .line 106
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x1

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 40
    if-ne p1, p0, :cond_0

    .line 41
    const/4 v0, 0x1

    .line 46
    :goto_0
    return v0

    .line 42
    :cond_0
    instance-of v0, p1, Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;

    if-eqz v0, :cond_1

    .line 43
    check-cast p1, Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;->a:LaFO;

    iget-object v1, p1, Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;->a:LaFO;

    invoke-virtual {v0, v1}, LaFO;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 46
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 52
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;->a:LaFO;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 100
    const-string v0, "AccountCriterion {accountId=%s}"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;->a:LaFO;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/app/model/navigation/AccountCriterion;->a:LaFO;

    invoke-virtual {v0}, LaFO;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 63
    return-void
.end method
