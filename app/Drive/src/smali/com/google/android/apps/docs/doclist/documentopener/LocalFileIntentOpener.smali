.class public Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;
.super Lcom/google/android/apps/docs/doclist/documentopener/AbstractImmediateDocumentOpener;
.source "LocalFileIntentOpener.java"


# instance fields
.field private final a:Ladb;

.field private final a:Landroid/content/Context;

.field private final a:Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

.field private final a:LqK;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ladb;LqK;Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/documentopener/AbstractImmediateDocumentOpener;-><init>()V

    .line 84
    iput-object p2, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:Ladb;

    .line 85
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:Landroid/content/Context;

    .line 86
    iput-object p3, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:LqK;

    .line 87
    iput-object p4, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

    .line 88
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;)Ladb;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:Ladb;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:Landroid/content/Context;

    return-object v0
.end method

.method private a(LFT;LaGo;LacY;Landroid/os/Bundle;Lamr;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 107
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:Ladb;

    if-nez p5, :cond_0

    sget-object p5, Ladk;->a:Ladk;

    :cond_0
    invoke-interface {v0, p2, p3, p5}, Ladb;->a(LaGo;LacY;Lamr;)LbsU;

    move-result-object v0

    .line 109
    invoke-interface {v0}, LbsU;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LacU;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 120
    :try_start_1
    new-instance v4, Ljava/lang/Object;

    invoke-direct {v4}, Ljava/lang/Object;-><init>()V

    .line 122
    new-instance v5, Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;

    .line 123
    invoke-interface {v0}, LacU;->a()Lcom/google/android/apps/docs/utils/ParcelableTask;

    move-result-object v1

    invoke-direct {v5, v1, v4}, Lcom/google/android/apps/docs/doclist/documentopener/DocumentFileCloseAndTrackTask;-><init>(Lcom/google/android/apps/docs/utils/ParcelableTask;Ljava/lang/Object;)V

    .line 125
    invoke-interface {v0}, LacU;->a()Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v6

    .line 127
    invoke-static {p4}, LFU;->a(Landroid/os/Bundle;)Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    move-result-object v7

    .line 130
    const-string v1, "uriIntentBuilder"

    invoke-virtual {p4, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator$UriIntentBuilder;

    .line 133
    if-nez v1, :cond_2

    .line 134
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

    .line 135
    invoke-interface {v0}, LacU;->a()Ljava/lang/String;

    move-result-object v8

    .line 134
    invoke-interface {v1, v7, v8, p2, v6}, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;->a(Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;LaGo;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 139
    :goto_0
    if-nez v1, :cond_3

    .line 140
    sget-object v1, LFV;->c:LFV;

    const/4 v2, 0x0

    invoke-interface {p1, v1, v2}, LFT;->a(LFV;Ljava/lang/Throwable;)V

    .line 142
    const-string v1, "LocalFileIntentOpener"

    const-string v2, "No installed package can handle file \"%s\" with mime-type \"%s\""

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 143
    invoke-interface {p2}, LaGo;->c()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-interface {v0}, LacU;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 142
    invoke-static {v1, v2, v3}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 144
    invoke-interface {v0}, LacU;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 161
    invoke-interface {v0}, LacU;->close()V

    .line 164
    :cond_1
    :goto_1
    return-void

    .line 110
    :catch_0
    move-exception v0

    .line 111
    sget-object v1, LFV;->h:LFV;

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LFT;->a(LFV;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 113
    :catch_1
    move-exception v0

    .line 114
    sget-object v1, LFV;->h:LFV;

    invoke-interface {p1, v1, v0}, LFT;->a(LFV;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 137
    :cond_2
    :try_start_2
    invoke-interface {v1, v6}, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator$UriIntentBuilder;->a(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    goto :goto_0

    .line 151
    :cond_3
    iget-object v6, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:LqK;

    invoke-virtual {v6, v4}, LqK;->a(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 153
    :try_start_3
    invoke-interface {p1, v1, v5}, LFT;->a(Landroid/content/Intent;Lcom/google/android/apps/docs/utils/ParcelableTask;)V
    :try_end_3
    .catch Landroid/content/ActivityNotFoundException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v1, v2

    .line 160
    :goto_2
    if-nez v1, :cond_1

    .line 161
    invoke-interface {v0}, LacU;->close()V

    goto :goto_1

    .line 155
    :catch_2
    move-exception v1

    .line 156
    :try_start_4
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:LqK;

    invoke-virtual {v2, v4}, LqK;->b(Ljava/lang/Object;)V

    .line 157
    sget-object v2, LFV;->c:LFV;

    invoke-interface {p1, v2, v1}, LFT;->a(LFV;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v1, v3

    goto :goto_2

    .line 160
    :catchall_0
    move-exception v1

    .line 161
    invoke-interface {v0}, LacU;->close()V

    throw v1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;LFT;LaGo;LacY;Landroid/os/Bundle;Lamr;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a(LFT;LaGo;LacY;Landroid/os/Bundle;Lamr;)V

    return-void
.end method


# virtual methods
.method public a(LFT;LaGo;Landroid/os/Bundle;)LDL;
    .locals 6

    .prologue
    .line 93
    invoke-static {p3}, LFU;->a(Landroid/os/Bundle;)Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    move-result-object v0

    .line 94
    invoke-interface {p2}, LaGo;->a()LaGv;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a(LaGv;)LacY;

    move-result-object v4

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:Ladb;

    invoke-interface {v0, p2, v4}, Ladb;->a(LaGo;LacY;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, LGz;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LGz;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;LFT;LaGo;LacY;Landroid/os/Bundle;)V

    .line 97
    :goto_0
    return-object v0

    .line 95
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
