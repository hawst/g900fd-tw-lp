.class public Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;
.super Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;
.source "PunchSingleScreenModeFragment.java"


# instance fields
.field public a:LWS;

.field private a:LWT;

.field private a:LXA;

.field public a:LXD;

.field private a:Landroid/app/AlertDialog;

.field private final a:Landroid/os/Handler;

.field public a:LtK;

.field private a:Z

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 63
    const-string v0, "PunchSingleScreenModeFragment"

    sget-object v1, LYR;->a:LYR;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;-><init>(Ljava/lang/String;LYR;)V

    .line 41
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:Landroid/os/Handler;

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->b:Z

    .line 64
    return-void
.end method

.method private A()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 209
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a()LH;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    .line 210
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, LpR;->dialog_confirm_start_presenting:I

    .line 211
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1040013

    new-instance v2, LXZ;

    invoke-direct {v2, p0}, LXZ;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;)V

    .line 212
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1040009

    new-instance v2, LXY;

    invoke-direct {v2, p0}, LXY;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;)V

    .line 219
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, LXX;

    invoke-direct {v1, p0}, LXX;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;)V

    .line 226
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 232
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:Landroid/app/AlertDialog;

    .line 233
    return-void
.end method

.method private B()V
    .locals 2

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:LXA;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 258
    new-instance v0, LYa;

    invoke-direct {v0, p0}, LYa;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:LXA;

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:LXD;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:LXA;

    invoke-virtual {v0, v1}, LXD;->a(LXA;)V

    .line 268
    return-void

    .line 257
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private C()V
    .locals 2

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:LXA;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 272
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:LXD;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:LXA;

    invoke-virtual {v0, v1}, LXD;->b(LXA;)V

    .line 273
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:LXA;

    .line 274
    return-void

    .line 271
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private D()V
    .locals 3

    .prologue
    .line 289
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->n()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:LXx;

    invoke-interface {v0}, LXx;->a()Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;

    move-result-object v0

    const-string v1, "fullScreenModeFragmentTag"

    const-string v2, "fullScreenModeFragment"

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a(Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    return-void

    .line 289
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:Landroid/app/AlertDialog;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:Landroid/app/AlertDialog;

    return-object p1
.end method

.method private a()Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;
    .locals 2

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a()LH;

    move-result-object v0

    invoke-virtual {v0}, LH;->a()LM;

    move-result-object v0

    const-string v1, "fullScreenModeFragmentTag"

    .line 296
    invoke-virtual {v0, v1}, LM;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 297
    instance-of v1, v0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;)Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a()Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;

    move-result-object v0

    return-object v0
.end method

.method public static a()Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;-><init>()V

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->u()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;Z)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 243
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a()V

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:LXx;

    .line 247
    invoke-interface {v0}, LXx;->a()Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;

    move-result-object v0

    .line 248
    const-string v1, "ViewModeFragment"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a(Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;Ljava/lang/String;)V

    .line 251
    if-eqz p1, :cond_0

    .line 252
    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->w()V

    .line 254
    :cond_0
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;)Z
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->n()Z

    move-result v0

    return v0
.end method

.method private n()Z
    .locals 1

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a()Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private t()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:Z

    invoke-static {v0}, LbiT;->b(Z)V

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:LWT;

    if-nez v0, :cond_0

    .line 138
    new-instance v0, LXW;

    invoke-direct {v0, p0}, LXW;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:LWT;

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:LWS;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:LWT;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LWS;->a(LWT;Landroid/os/Handler;)V

    .line 163
    return-void
.end method

.method private u()V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:LWS;

    const-string v1, "android.hardware.display.category.PRESENTATION"

    invoke-interface {v0, v1}, LWS;->a(Ljava/lang/String;)[Landroid/view/Display;

    move-result-object v0

    .line 169
    array-length v0, v0

    if-lez v0, :cond_1

    .line 170
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->v()V

    .line 177
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:LYO;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LYO;->b(Z)V

    .line 178
    return-void

    .line 171
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    goto :goto_0
.end method

.method private v()V
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    :goto_0
    return-void

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:LYO;

    invoke-interface {v0}, LYO;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 197
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->A()V

    goto :goto_0

    .line 199
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a(Z)V

    goto :goto_0
.end method


# virtual methods
.method protected a()V
    .locals 1

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a()Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->w()V

    .line 121
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->z()V

    .line 122
    return-void
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 68
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->a_(Landroid/os/Bundle;)V

    .line 69
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->B()V

    .line 72
    invoke-static {}, LakQ;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:LtK;

    sget-object v2, Lry;->ao:Lry;

    .line 73
    invoke-interface {v0, v2}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:Z

    .line 74
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:Z

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:LWS;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a()LH;

    move-result-object v2

    invoke-interface {v0, v2}, LWS;->a(Landroid/content/Context;)V

    .line 80
    :cond_0
    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    iput-boolean v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->b:Z

    .line 83
    :cond_1
    return-void

    .line 73
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 87
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->b(Landroid/os/Bundle;)V

    .line 88
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->b:Z

    if-eqz v0, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->w()V

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->b:Z

    .line 92
    :cond_0
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 110
    invoke-super {p0}, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->g()V

    .line 111
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:Z

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:LWS;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:LWT;

    invoke-interface {v0, v1}, LWS;->a(LWT;)V

    .line 114
    :cond_0
    return-void
.end method

.method public h()V
    .locals 0

    .prologue
    .line 126
    invoke-super {p0}, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->h()V

    .line 127
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->C()V

    .line 128
    return-void
.end method

.method protected m()Z
    .locals 1

    .prologue
    .line 310
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 311
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a()Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->w()V

    .line 312
    const/4 v0, 0x1

    .line 314
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o_()V
    .locals 2

    .prologue
    .line 96
    invoke-super {p0}, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->o_()V

    .line 97
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:Z

    if-eqz v0, :cond_0

    .line 98
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->t()V

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a:Landroid/os/Handler;

    new-instance v1, LXV;

    invoke-direct {v1, p0}, LXV;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 106
    :cond_0
    return-void
.end method

.method public w()V
    .locals 1

    .prologue
    .line 281
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a()Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->w()V

    .line 286
    :goto_0
    return-void

    .line 284
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->D()V

    goto :goto_0
.end method

.method protected x()V
    .locals 1

    .prologue
    .line 320
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 321
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a()Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->a()V

    .line 323
    :cond_0
    return-void
.end method

.method protected y()V
    .locals 1

    .prologue
    .line 327
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;->a()Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenModeFragment;->t()V

    .line 330
    :cond_0
    return-void
.end method
