.class public Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;
.super Landroid/widget/LinearLayout;
.source "SpeakerNotesContent.java"


# instance fields
.field private a:LWN;

.field public a:LXz;

.field private a:LZJ;

.field final a:Landroid/content/Context;

.field private a:Landroid/view/View;

.field private a:Landroid/webkit/WebView;

.field private a:Landroid/widget/ImageView;

.field private a:Landroid/widget/TextView;

.field a:Ljava/lang/String;

.field b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 75
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Landroid/content/Context;

    .line 76
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a()V

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 87
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Landroid/content/Context;

    .line 88
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a()V

    .line 89
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;)LZJ;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:LZJ;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Landroid/content/Context;

    invoke-static {v0}, Lajt;->a(Landroid/content/Context;)Lbuu;

    move-result-object v0

    invoke-interface {v0, p0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Landroid/content/Context;

    sget v1, LpP;->speaker_note_content:I

    invoke-static {v0, v1, p0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 108
    sget v0, LpN;->speaker_notes:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Landroid/webkit/WebView;

    .line 109
    sget v0, LpN;->no_notes:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Landroid/widget/TextView;

    .line 110
    sget v0, LpN;->speaker_notes_close_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Landroid/widget/ImageView;

    .line 111
    sget v0, LpN;->speaker_notes_wrapper:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Landroid/view/View;

    .line 113
    invoke-static {}, LakQ;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->b()V

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 119
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setLightTouchEnabled(Z)V

    .line 121
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, LakQ;->c(Landroid/content/res/Resources;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 122
    invoke-static {}, LakQ;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 123
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a(Landroid/webkit/WebSettings;)V

    .line 129
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Landroid/widget/ImageView;

    new-instance v1, LZH;

    invoke-direct {v1, p0}, LZH;-><init>(Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->d()V

    .line 140
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->c()V

    .line 141
    return-void

    .line 125
    :cond_2
    sget-object v1, Landroid/webkit/WebSettings$TextSize;->LARGER:Landroid/webkit/WebSettings$TextSize;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setTextSize(Landroid/webkit/WebSettings$TextSize;)V

    goto :goto_0
.end method

.method private a(Landroid/webkit/WebSettings;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 145
    const/16 v0, 0x96

    invoke-virtual {p1, v0}, Landroid/webkit/WebSettings;->setTextZoom(I)V

    .line 146
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->c()V

    return-void
.end method

.method private b()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Landroid/webkit/WebView;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 151
    return-void
.end method

.method private c()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:LXz;

    invoke-interface {v0}, LXz;->c()I

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:LXz;

    invoke-interface {v0}, LXz;->d()I

    move-result v0

    .line 156
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:LXz;

    invoke-interface {v1, v0}, LXz;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Ljava/lang/String;

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Landroid/webkit/WebView;

    const-string v1, "about:blank"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 162
    iput-object v5, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->b:Ljava/lang/String;

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 178
    :goto_0
    return-void

    .line 168
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Landroid/webkit/WebView;

    const-string v1, "fake-url"

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Ljava/lang/String;

    const-string v3, "text/html"

    const-string v4, "utf-8"

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->b:Ljava/lang/String;

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 172
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 175
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:LWN;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 188
    new-instance v0, LZI;

    invoke-direct {v0, p0}, LZI;-><init>(Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:LWN;

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:LXz;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:LWN;

    invoke-interface {v0, v1}, LXz;->a(LXA;)V

    .line 206
    return-void

    .line 187
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:LWN;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:LXz;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:LWN;

    invoke-interface {v0, v1}, LXz;->b(LXA;)V

    .line 211
    return-void

    .line 209
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 182
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->e()V

    .line 183
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 184
    return-void
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 100
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onVisibilityChanged(Landroid/view/View;I)V

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, p2}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 102
    return-void
.end method

.method public setListener(LZJ;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesContent;->a:LZJ;

    .line 96
    return-void
.end method
