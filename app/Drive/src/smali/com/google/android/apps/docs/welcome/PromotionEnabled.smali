.class public Lcom/google/android/apps/docs/welcome/PromotionEnabled;
.super LalP;
.source "PromotionEnabled.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LalP",
        "<",
        "Lasx;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, LalP;-><init>()V

    .line 28
    return-void
.end method


# virtual methods
.method protected a(Lbuu;)Lasx;
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lasx;

    invoke-direct {v0}, Lasx;-><init>()V

    .line 33
    invoke-interface {p1, v0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 34
    return-object v0
.end method

.method protected bridge synthetic a(Lbuu;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/welcome/PromotionEnabled;->a(Lbuu;)Lasx;

    move-result-object v0

    return-object v0
.end method

.method public call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 48
    const-string v0, "available"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 50
    const-string v2, "available"

    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/PromotionEnabled;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lasx;

    iget-object v0, v0, Lasx;->a:Lasy;

    invoke-virtual {v0}, Lasy;->a()Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move-object v0, v1

    .line 54
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    return-object v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return v0
.end method
