.class public Lcom/google/android/apps/docs/welcome/RedeemVoucherController;
.super Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;
.source "RedeemVoucherController.java"

# interfaces
.implements LFq;


# instance fields
.field public a:LarX;

.field public a:LasX;

.field private a:Lasd;

.field private a:Lcom/google/android/apps/docs/welcome/RedeemVoucherProgressDialog;

.field private a:Ljava/lang/Runnable;

.field public a:LqK;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;-><init>()V

    .line 72
    return-void
.end method

.method private a()LasF;
    .locals 3

    .prologue
    .line 296
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a()LH;

    move-result-object v0

    .line 297
    invoke-virtual {v0}, LH;->a()LM;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, LM;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, LasF;

    .line 298
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Listener @"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is null "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lcom/google/android/apps/docs/welcome/RedeemVoucherProgressDialog;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lcom/google/android/apps/docs/welcome/RedeemVoucherProgressDialog;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/welcome/RedeemVoucherProgressDialog;->a()V

    .line 170
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lcom/google/android/apps/docs/welcome/RedeemVoucherProgressDialog;

    .line 172
    :cond_0
    return-void
.end method

.method private a(LaFO;)V
    .locals 3

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lasd;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lasd;->a(Ljava/lang/Boolean;)V

    .line 143
    const-string v0, "RedeemVoucherController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Redeem voucher, step 3 (granted) "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lasd;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:LqK;

    const-string v1, "welcomeOffer"

    const-string v2, "redeemVoucherGranted"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a()V

    .line 146
    invoke-direct {p0}, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a()LasF;

    move-result-object v0

    invoke-interface {v0, p1}, LasF;->a(LaFO;)V

    .line 147
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/welcome/RedeemVoucherController;LaFO;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a(LaFO;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/welcome/RedeemVoucherController;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/welcome/RedeemVoucherController;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a(ZLjava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 310
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 311
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 316
    :goto_0
    return-void

    .line 313
    :cond_0
    const-string v0, "RedeemVoucherController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Got a callback while offline, will run when online again."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    iput-object p1, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Ljava/lang/Runnable;

    goto :goto_0
.end method

.method private a(ZLjava/lang/String;)V
    .locals 3

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lasd;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lasd;->a(Ljava/lang/Boolean;)V

    .line 152
    iget-object v1, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:LqK;

    const-string v2, "welcomeOffer"

    if-eqz p1, :cond_0

    const-string v0, "redeemVoucherFailed"

    :goto_0
    invoke-virtual {v1, v2, v0}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const-string v0, "RedeemVoucherController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Redeem voucher, step 3 (declined) "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lasd;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    invoke-direct {p0}, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a()V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:LarX;

    new-instance v1, LasB;

    const-class v2, Lcom/google/android/apps/docs/welcome/RedeemVoucherFailureDialog;

    invoke-direct {v1, p0, v2, p2}, LasB;-><init>(Lcom/google/android/apps/docs/welcome/RedeemVoucherController;Ljava/lang/Class;Ljava/lang/String;)V

    const-string v2, "RedeemVoucherDialog"

    invoke-virtual {v0, v1, v2}, LarX;->a(LarY;Ljava/lang/String;)Landroid/support/v4/app/DialogFragment;

    .line 163
    invoke-direct {p0}, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a()LasF;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LasF;->a(ZLjava/lang/String;)V

    .line 164
    return-void

    .line 152
    :cond_0
    const-string v0, "redeemVoucherDeclined"

    goto :goto_0
.end method

.method private b(Landroid/accounts/Account;)V
    .locals 3

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lasd;

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v1

    invoke-virtual {v0, v1}, Lasd;->a(LaFO;)V

    .line 134
    const-string v0, "RedeemVoucherController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Redeem voucher, step 2 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lasd;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:LqK;

    const-string v1, "welcomeOffer"

    const-string v2, "redeemVoucherRedeem"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:LarX;

    const-class v1, Lcom/google/android/apps/docs/welcome/RedeemVoucherProgressDialog;

    invoke-static {v1}, LarY;->a(Ljava/lang/Class;)LarY;

    move-result-object v1

    const-string v2, "RedeemVoucherProgressDialog"

    invoke-virtual {v0, v1, v2}, LarX;->a(LarY;Ljava/lang/String;)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/welcome/RedeemVoucherProgressDialog;

    iput-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lcom/google/android/apps/docs/welcome/RedeemVoucherProgressDialog;

    .line 137
    invoke-direct {p0}, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->t()V

    .line 138
    return-void
.end method

.method private t()V
    .locals 3

    .prologue
    .line 246
    new-instance v0, LasC;

    invoke-direct {v0, p0}, LasC;-><init>(Lcom/google/android/apps/docs/welcome/RedeemVoucherController;)V

    .line 292
    iget-object v1, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:LasX;

    iget-object v2, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lasd;

    invoke-interface {v1, v2, v0}, LasX;->a(Lasd;LasY;)V

    .line 293
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)LasG;
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lasd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lasd;

    invoke-virtual {v0}, Lasd;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lasd;

    invoke-virtual {v0}, Lasd;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 100
    :cond_0
    sget-object v0, LasG;->c:LasG;

    .line 106
    :goto_0
    return-object v0

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lasd;

    invoke-virtual {v0}, Lasd;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 103
    sget-object v0, LasG;->a:LasG;

    goto :goto_0

    .line 105
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lasd;

    invoke-virtual {v0}, Lasd;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 106
    sget-object v0, LasG;->b:LasG;

    goto :goto_0

    .line 109
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Voucher is an unexpected state. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lasd;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->b(Landroid/accounts/Account;)V

    .line 235
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 178
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->a(Landroid/app/Activity;)V

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:LarX;

    const-class v1, Lcom/google/android/apps/docs/welcome/RedeemVoucherProgressDialog;

    const-string v2, "RedeemVoucherProgressDialog"

    invoke-virtual {v0, v1, v2}, LarX;->a(Ljava/lang/Class;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/welcome/RedeemVoucherProgressDialog;

    iput-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lcom/google/android/apps/docs/welcome/RedeemVoucherProgressDialog;

    .line 180
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 119
    new-instance v0, Lasd;

    invoke-direct {v0, p1}, Lasd;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lasd;

    .line 120
    iput-object p2, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->c:Ljava/lang/String;

    .line 121
    const-string v0, "RedeemVoucherController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Redeem voucher, step 1 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lasd;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:LqK;

    const-string v1, "welcomeOffer"

    const-string v2, "redeemVoucherStart"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:LarX;

    new-instance v1, LasA;

    const-class v2, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;

    invoke-direct {v1, p0, v2}, LasA;-><init>(Lcom/google/android/apps/docs/welcome/RedeemVoucherController;Ljava/lang/Class;)V

    const-string v2, "RedeemVoucherController.PickAccountDialogFragment"

    invoke-virtual {v0, v1, v2}, LarX;->a(LarY;Ljava/lang/String;)Landroid/support/v4/app/DialogFragment;

    .line 129
    return-void
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 184
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->a_(Landroid/os/Bundle;)V

    .line 185
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->e(Z)V

    .line 186
    if-eqz p1, :cond_0

    .line 187
    invoke-static {p1}, Lasd;->a(Landroid/os/Bundle;)Lasd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lasd;

    .line 188
    const-string v0, "listener"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->c:Ljava/lang/String;

    .line 190
    :cond_0
    const-string v0, "RedeemVoucherController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with offer "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lasd;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    invoke-direct {p0}, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a()V

    .line 195
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lasd;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lasd;

    invoke-virtual {v0, p1}, Lasd;->a(Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 212
    const-string v0, "listener"

    iget-object v1, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->c(Landroid/os/Bundle;)V

    .line 216
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 239
    const-string v0, "RedeemVoucherController"

    const-string v1, "No account."

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 220
    invoke-super {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->h()V

    .line 221
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Ljava/lang/Runnable;

    .line 222
    return-void
.end method

.method public n_()V
    .locals 1

    .prologue
    .line 226
    invoke-super {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->n_()V

    .line 227
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Lcom/google/android/apps/docs/welcome/RedeemVoucherProgressDialog;

    .line 228
    return-void
.end method

.method public o_()V
    .locals 2

    .prologue
    .line 199
    invoke-super {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->o_()V

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 201
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a()LH;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, LH;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 202
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/welcome/RedeemVoucherController;->a:Ljava/lang/Runnable;

    .line 204
    :cond_0
    return-void
.end method
