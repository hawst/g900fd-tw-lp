.class public Lcom/google/android/apps/docs/DocsApplication;
.super Lajp;
.source "DocsApplication.java"


# static fields
.field private static volatile b:Z


# instance fields
.field public a:LQr;

.field public a:LQt;

.field public a:LQy;

.field public a:LSF;

.field public a:LUJ;

.field private a:LVX;

.field public a:LaGU;

.field public a:LaHv;

.field public a:Laac;

.field public a:LacV;

.field public a:LafX;

.field public a:Lagd;

.field public a:Lahw;

.field public a:Lahy;

.field public a:Lamn;

.field private a:Ljava/lang/Boolean;

.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LbuC;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LqA;

.field a:Z

.field private c:Z

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 180
    invoke-direct {p0}, Lajp;-><init>()V

    .line 114
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Ljava/util/Map;

    .line 155
    iput-boolean v1, p0, Lcom/google/android/apps/docs/DocsApplication;->c:Z

    .line 157
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/DocsApplication;->d:Z

    .line 158
    iput-boolean v1, p0, Lcom/google/android/apps/docs/DocsApplication;->e:Z

    .line 166
    new-instance v0, LqB;

    invoke-direct {v0}, LqB;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:LqA;

    .line 173
    iput-boolean v1, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Z

    .line 181
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 188
    invoke-direct {p0}, Lajp;-><init>()V

    .line 114
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Ljava/util/Map;

    .line 155
    iput-boolean v1, p0, Lcom/google/android/apps/docs/DocsApplication;->c:Z

    .line 157
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/DocsApplication;->d:Z

    .line 158
    iput-boolean v1, p0, Lcom/google/android/apps/docs/DocsApplication;->e:Z

    .line 166
    new-instance v0, LqB;

    invoke-direct {v0}, LqB;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:LqA;

    .line 173
    iput-boolean v1, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Z

    .line 189
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/DocsApplication;->attachBaseContext(Landroid/content/Context;)V

    .line 190
    return-void
.end method

.method static a(Landroid/app/Application;LqA;)Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "LqA;",
            ")",
            "Ljava/util/Collection",
            "<",
            "LbuC;",
            ">;"
        }
    .end annotation

    .prologue
    .line 401
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v0

    new-instance v1, LaiO;

    invoke-direct {v1, p0}, LaiO;-><init>(Landroid/content/Context;)V

    .line 402
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, Lanm;

    invoke-direct {v1}, Lanm;-><init>()V

    .line 403
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, Lpz;

    invoke-direct {v1, p1}, Lpz;-><init>(LqA;)V

    .line 404
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, Lahi;

    invoke-direct {v1}, Lahi;-><init>()V

    .line 405
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LahA;

    invoke-direct {v1}, LahA;-><init>()V

    .line 406
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LFW;

    invoke-direct {v1}, LFW;-><init>()V

    .line 407
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, Lwz;

    invoke-direct {v1}, Lwz;-><init>()V

    .line 408
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LacE;

    invoke-direct {v1}, LacE;-><init>()V

    .line 409
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LSJ;

    invoke-direct {v1}, LSJ;-><init>()V

    .line 410
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LMX;

    invoke-direct {v1}, LMX;-><init>()V

    .line 411
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, Laeh;

    invoke-direct {v1}, Laeh;-><init>()V

    .line 412
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LTn;

    invoke-direct {v1, p0}, LTn;-><init>(Landroid/content/Context;)V

    .line 413
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LadG;

    invoke-direct {v1}, LadG;-><init>()V

    .line 414
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LaEE;

    invoke-direct {v1}, LaEE;-><init>()V

    .line 415
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LaFY;

    invoke-direct {v1}, LaFY;-><init>()V

    .line 416
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, Laib;

    invoke-direct {v1}, Laib;-><init>()V

    .line 417
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LUK;

    invoke-direct {v1}, LUK;-><init>()V

    .line 418
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, Laav;

    invoke-direct {v1}, Laav;-><init>()V

    .line 419
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LaHA;

    invoke-direct {v1}, LaHA;-><init>()V

    .line 420
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LQF;

    invoke-direct {v1}, LQF;-><init>()V

    .line 421
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LpZ;

    invoke-direct {v1}, LpZ;-><init>()V

    .line 422
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LEK;

    invoke-direct {v1}, LEK;-><init>()V

    .line 423
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LUz;

    invoke-direct {v1}, LUz;-><init>()V

    .line 424
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, Larq;

    invoke-direct {v1}, Larq;-><init>()V

    .line 425
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, Lwl;

    invoke-direct {v1}, Lwl;-><init>()V

    .line 426
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LBv;

    invoke-direct {v1}, LBv;-><init>()V

    .line 427
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LJD;

    invoke-direct {v1}, LJD;-><init>()V

    .line 428
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LRl;

    invoke-direct {v1}, LRl;-><init>()V

    .line 429
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LED;

    invoke-direct {v1}, LED;-><init>()V

    .line 430
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LIC;

    invoke-direct {v1}, LIC;-><init>()V

    .line 431
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LwA;

    invoke-direct {v1}, LwA;-><init>()V

    .line 432
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LatD;

    invoke-direct {v1}, LatD;-><init>()V

    .line 433
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LaJX;

    invoke-direct {v1}, LaJX;-><init>()V

    .line 434
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LPs;

    invoke-direct {v1}, LPs;-><init>()V

    .line 435
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LaIl;

    invoke-direct {v1}, LaIl;-><init>()V

    .line 436
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, Lpx;

    invoke-direct {v1}, Lpx;-><init>()V

    .line 437
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, Laoq;

    invoke-direct {v1}, Laoq;-><init>()V

    .line 438
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LUj;

    invoke-direct {v1}, LUj;-><init>()V

    .line 439
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, Lyp;

    invoke-direct {v1}, Lyp;-><init>()V

    .line 440
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LPJ;

    invoke-direct {v1}, LPJ;-><init>()V

    .line 441
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LaaB;

    invoke-direct {v1}, LaaB;-><init>()V

    .line 442
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, Lyg;

    invoke-direct {v1}, Lyg;-><init>()V

    .line 443
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LLR;

    invoke-direct {v1}, LLR;-><init>()V

    .line 444
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LJP;

    invoke-direct {v1}, LJP;-><init>()V

    .line 445
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    new-instance v1, LFI;

    invoke-direct {v1}, LFI;-><init>()V

    .line 446
    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Object;)LbmH;

    move-result-object v0

    .line 447
    invoke-virtual {v0}, LbmH;->a()LbmF;

    move-result-object v0

    return-object v0
.end method

.method private a()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LbuC;",
            ">;"
        }
    .end annotation

    .prologue
    .line 375
    iget-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 376
    iget-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Ljava/util/Map;

    .line 385
    :goto_0
    return-object v0

    .line 379
    :cond_0
    invoke-static {}, LboS;->a()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Ljava/util/Map;

    .line 380
    invoke-virtual {p0}, Lcom/google/android/apps/docs/DocsApplication;->a()Ljava/util/Collection;

    move-result-object v0

    .line 381
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuC;

    .line 382
    iget-object v2, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Ljava/util/Map;

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/docs/DocsApplication;->a(Ljava/util/Map;LbuC;)V

    goto :goto_1

    .line 385
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Ljava/util/Map;

    goto :goto_0
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 557
    iget-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 558
    iget-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 573
    :goto_0
    return v0

    .line 567
    :cond_0
    :try_start_0
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/DocsApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 568
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    .line 569
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 573
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    .line 570
    :catch_0
    move-exception v0

    .line 571
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Ljava/lang/Boolean;

    goto :goto_1
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 543
    if-eqz p1, :cond_0

    const-string v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized e()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 250
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/apps/docs/DocsApplication;->c:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/docs/DocsApplication;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 261
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 254
    :cond_1
    :try_start_1
    const-string v1, "DocsApplication"

    const-string v2, "Installing DEX configuration."

    invoke-static {v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    invoke-static {p0}, Lakg;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 256
    new-instance v2, LwQ;

    invoke-direct {v2}, LwQ;-><init>()V

    .line 257
    const-string v3, "eager_dex_configuration.xml"

    .line 258
    invoke-virtual {v2, p0, v3, v1}, LwQ;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LwR;

    move-result-object v2

    .line 259
    invoke-virtual {v2}, LwR;->a()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/DocsApplication;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/docs/DocsApplication;->d:Z

    .line 260
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/DocsApplication;->c:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 250
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 259
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private declared-synchronized f()V
    .locals 3

    .prologue
    .line 268
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/DocsApplication;->e()V

    .line 269
    iget-boolean v0, p0, Lcom/google/android/apps/docs/DocsApplication;->e:Z

    if-nez v0, :cond_0

    .line 270
    invoke-static {p0}, Lakg;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 271
    new-instance v1, LVX;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/DocsApplication;->a()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-direct {v1, v2, p0, v0}, LVX;-><init>(Ljava/lang/ClassLoader;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/docs/DocsApplication;->a:LVX;

    .line 272
    iget-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:LVX;

    invoke-virtual {v0, p0}, LVX;->a(Landroid/content/Context;)V

    .line 273
    const-string v0, "DocsApplication"

    const-string v1, "Plugin installer initialized."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/DocsApplication;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 276
    :cond_0
    monitor-exit p0

    return-void

    .line 268
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private g()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 330
    iget-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:LSF;

    invoke-interface {v0}, LSF;->a()[Landroid/accounts/Account;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 331
    invoke-static {}, Lcom/google/android/gms/drive/database/DocListProvider;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v5}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 332
    invoke-static {p0, v3, v5}, Lahe;->a(Landroid/content/Context;Landroid/accounts/Account;Z)V

    .line 330
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 334
    :cond_0
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:LafX;

    invoke-virtual {v0}, LafX;->a()LafU;

    move-result-object v0

    .line 338
    iget-object v1, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Lahy;

    invoke-interface {v1, v0}, Lahy;->a(Lahz;)V

    .line 339
    return-void
.end method

.method private i()V
    .locals 5

    .prologue
    .line 348
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/docs/DocsApplication;->a:LQr;

    monitor-enter v1
    :try_end_0
    .catch LQu; {:try_start_0 .. :try_end_0} :catch_0
    .catch LQz; {:try_start_0 .. :try_end_0} :catch_1

    .line 349
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:LQt;

    iget-object v2, p0, Lcom/google/android/apps/docs/DocsApplication;->a:LQr;

    invoke-interface {v0, v2}, LQt;->b(LQr;)V

    .line 350
    invoke-static {}, Laml;->a()Lrx;

    move-result-object v0

    .line 351
    iget-object v2, v0, Lrx;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 352
    iget-object v2, p0, Lcom/google/android/apps/docs/DocsApplication;->a:LQy;

    iget-object v0, v0, Lrx;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/docs/DocsApplication;->a:LQr;

    const/4 v4, 0x0

    invoke-interface {v2, v0, v3, v4}, LQy;->a(Ljava/lang/String;LQr;Z)V

    .line 354
    :cond_0
    monitor-exit v1

    .line 360
    :goto_0
    return-void

    .line 354
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch LQu; {:try_start_2 .. :try_end_2} :catch_0
    .catch LQz; {:try_start_2 .. :try_end_2} :catch_1

    .line 355
    :catch_0
    move-exception v0

    .line 356
    const-string v1, "DocsApplication"

    const-string v2, "Unable to load cached client flags, will use defaults until next sync."

    invoke-static {v1, v0, v2}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0

    .line 357
    :catch_1
    move-exception v0

    .line 358
    const-string v1, "DocsApplication"

    const-string v2, "Unable to parse override client flags, will use cached flags only."

    invoke-static {v1, v0, v2}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private j()V
    .locals 5

    .prologue
    .line 527
    :try_start_0
    const-string v0, "com.google.android.apps.docs.DocsClassLoader"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 528
    const-string v1, "insertIntoClassLoaderHierarchy"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/lang/Class;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-class v4, Landroid/content/Context;

    aput-object v4, v2, v3

    .line 529
    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 530
    const/4 v1, 0x0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 540
    :goto_0
    return-void

    .line 533
    :catch_0
    move-exception v0

    .line 535
    invoke-static {v0}, Lbjz;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 536
    :catch_1
    move-exception v0

    .line 538
    invoke-static {v0}, Lbjz;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 531
    :catch_2
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method protected a()Ljava/lang/ClassLoader;
    .locals 1

    .prologue
    .line 282
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    return-object v0
.end method

.method protected a()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "LbuC;",
            ">;"
        }
    .end annotation

    .prologue
    .line 389
    invoke-static {}, LbmF;->a()LbmH;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/DocsApplication;->a:LqA;

    .line 390
    invoke-static {p0, v1}, Lcom/google/android/apps/docs/DocsApplication;->a(Landroid/app/Application;LqA;)Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Iterable;)LbmH;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/DocsApplication;->a:LVX;

    .line 391
    invoke-virtual {v1}, LVX;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, LbmH;->a(Ljava/lang/Iterable;)LbmH;

    move-result-object v0

    .line 392
    invoke-virtual {v0}, LbmH;->a()LbmF;

    move-result-object v0

    return-object v0
.end method

.method protected a()V
    .locals 1

    .prologue
    .line 235
    invoke-virtual {p0}, Lcom/google/android/apps/docs/DocsApplication;->a()Lbuu;

    move-result-object v0

    invoke-interface {v0, p0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 236
    return-void
.end method

.method protected a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LbuC;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 371
    invoke-direct {p0}, Lcom/google/android/apps/docs/DocsApplication;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 372
    return-void
.end method

.method protected a(Ljava/util/Map;LbuC;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LbuC;",
            ">;",
            "LbuC;",
            ")V"
        }
    .end annotation

    .prologue
    .line 451
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 452
    return-void
.end method

.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 364
    invoke-super {p0, p1}, Lajp;->attachBaseContext(Landroid/content/Context;)V

    .line 365
    invoke-direct {p0}, Lcom/google/android/apps/docs/DocsApplication;->e()V

    .line 366
    invoke-static {p1}, Laml;->a(Landroid/content/Context;)V

    .line 367
    return-void
.end method

.method protected b()V
    .locals 5

    .prologue
    .line 287
    const-string v0, "DocsApplication"

    const-string v1, "in onCreateInjector, thread: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 289
    invoke-direct {p0}, Lcom/google/android/apps/docs/DocsApplication;->j()V

    .line 290
    invoke-direct {p0}, Lcom/google/android/apps/docs/DocsApplication;->f()V

    .line 292
    invoke-static {p0}, Lcom/google/android/gms/drive/database/DocListProvider;->a(Landroid/content/Context;)V

    .line 293
    return-void
.end method

.method protected c()V
    .locals 3

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Lamn;

    const/4 v1, 0x0

    const-string v2, "task_startup"

    invoke-interface {v0, v1, v2}, Lamn;->a(LaFO;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 303
    invoke-direct {p0}, Lcom/google/android/apps/docs/DocsApplication;->g()V

    .line 306
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/DocsApplication;->h()V

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Laac;

    iget-object v1, p0, Lcom/google/android/apps/docs/DocsApplication;->a:LaHv;

    invoke-virtual {v0, v1}, Laac;->a(Laad;)V

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Laac;

    iget-object v1, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Lahw;

    invoke-virtual {v0, v1}, Laac;->a(Laad;)V

    .line 312
    iget-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:LacV;

    invoke-virtual {v0}, LacV;->a()V

    .line 314
    new-instance v0, LpE;

    const-string v1, "Background initialization thread"

    invoke-direct {v0, p0, v1}, LpE;-><init>(Lcom/google/android/apps/docs/DocsApplication;Ljava/lang/String;)V

    .line 322
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 323
    return-void
.end method

.method public declared-synchronized d()V
    .locals 2

    .prologue
    .line 475
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 477
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 478
    :catch_0
    move-exception v0

    goto :goto_0

    .line 482
    :cond_0
    :try_start_2
    const-string v0, "DocsApplication"

    const-string v1, "Reseting injector"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Ljava/util/Map;

    .line 484
    invoke-super {p0}, Lajp;->d()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 485
    monitor-exit p0

    return-void

    .line 475
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 194
    invoke-static {}, Laml;->a()Lrx;

    move-result-object v0

    iget-boolean v0, v0, Lrx;->a:Z

    if-nez v0, :cond_0

    .line 195
    const/4 v0, 0x5

    invoke-static {v0}, LalV;->a(I)V

    .line 200
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/DocsApplication;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 232
    :cond_1
    :goto_0
    return-void

    .line 204
    :cond_2
    const-string v0, "DocsApplication"

    const-string v1, "in DocsApplication.onCreate; process name \'%s\'"

    new-array v2, v2, [Ljava/lang/Object;

    .line 205
    invoke-static {p0}, Lakg;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    .line 204
    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 207
    invoke-super {p0}, Lajp;->onCreate()V

    .line 209
    sget-boolean v0, Lcom/google/android/apps/docs/DocsApplication;->b:Z

    if-nez v0, :cond_1

    .line 213
    invoke-static {p0}, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a(Landroid/content/Context;)V

    .line 215
    invoke-direct {p0}, Lcom/google/android/apps/docs/DocsApplication;->f()V

    .line 217
    iget-boolean v0, p0, Lcom/google/android/apps/docs/DocsApplication;->d:Z

    if-nez v0, :cond_3

    .line 218
    const-string v0, "DocsApplication"

    const-string v1, "Injection not required. Skipped."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 222
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/docs/DocsApplication;->a()V

    .line 224
    monitor-enter p0

    .line 225
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/docs/DocsApplication;->a:Z

    .line 226
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 227
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229
    invoke-direct {p0}, Lcom/google/android/apps/docs/DocsApplication;->i()V

    .line 231
    invoke-virtual {p0}, Lcom/google/android/apps/docs/DocsApplication;->c()V

    goto :goto_0

    .line 227
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onTerminate()V
    .locals 2

    .prologue
    .line 496
    invoke-direct {p0}, Lcom/google/android/apps/docs/DocsApplication;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 497
    invoke-virtual {p0}, Lcom/google/android/apps/docs/DocsApplication;->a()Lbuu;

    move-result-object v0

    const-class v1, LTF;

    invoke-interface {v0, v1}, Lbuu;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTF;

    .line 498
    if-eqz v0, :cond_0

    .line 499
    invoke-interface {v0}, LTF;->c()V

    .line 502
    :cond_0
    invoke-super {p0}, Lajp;->onTerminate()V

    .line 503
    return-void
.end method
