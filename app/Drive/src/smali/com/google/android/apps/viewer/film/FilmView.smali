.class public Lcom/google/android/apps/viewer/film/FilmView;
.super Landroid/widget/LinearLayout;
.source "FilmView.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private a:I

.field protected a:[I

.field private a:[Lcom/google/android/apps/viewer/client/Dimensions;

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 45
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 36
    iput v0, p0, Lcom/google/android/apps/viewer/film/FilmView;->a:I

    .line 39
    iput v0, p0, Lcom/google/android/apps/viewer/film/FilmView;->b:I

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    iput v0, p0, Lcom/google/android/apps/viewer/film/FilmView;->a:I

    .line 39
    iput v0, p0, Lcom/google/android/apps/viewer/film/FilmView;->b:I

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 53
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    iput v0, p0, Lcom/google/android/apps/viewer/film/FilmView;->a:I

    .line 39
    iput v0, p0, Lcom/google/android/apps/viewer/film/FilmView;->b:I

    .line 54
    return-void
.end method

.method private a(I)I
    .locals 3

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmView;->a:[Lcom/google/android/apps/viewer/client/Dimensions;

    aget-object v0, v0, p1

    .line 111
    if-nez v0, :cond_0

    .line 112
    iget v0, p0, Lcom/google/android/apps/viewer/film/FilmView;->b:I

    .line 119
    :goto_0
    return v0

    .line 115
    :cond_0
    iget v1, v0, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    iget v2, p0, Lcom/google/android/apps/viewer/film/FilmView;->a:I

    if-gt v1, v2, :cond_1

    .line 116
    iget v1, p0, Lcom/google/android/apps/viewer/film/FilmView;->b:I

    iget v0, v0, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    .line 118
    :cond_1
    iget v1, p0, Lcom/google/android/apps/viewer/film/FilmView;->a:I

    int-to-float v1, v1

    iget v2, v0, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 119
    iget v2, p0, Lcom/google/android/apps/viewer/film/FilmView;->b:I

    iget v0, v0, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    int-to-float v0, v0

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmView;->getChildCount()I

    move-result v1

    .line 86
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 87
    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/FilmView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 88
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/film/FilmView;->a(I)I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 86
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 90
    :cond_0
    return-void
.end method

.method private a(I)V
    .locals 4

    .prologue
    .line 193
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "DBG Fling tabs: ["

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 194
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/viewer/film/FilmView;->a:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 195
    iget-object v2, p0, Lcom/google/android/apps/viewer/film/FilmView;->a:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 197
    :cond_0
    const-string v0, "] -- unscaled width = "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 198
    const-string v0, "FilmView"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    return-void
.end method

.method private b()I
    .locals 7

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmView;->getChildCount()I

    move-result v2

    .line 171
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/google/android/apps/viewer/film/FilmView;->a:[I

    .line 173
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmView;->getPaddingLeft()I

    move-result v1

    .line 176
    const/4 v0, 0x0

    move v6, v0

    move v0, v1

    move v1, v6

    :goto_0
    if-ge v1, v2, :cond_1

    .line 177
    invoke-virtual {p0, v1}, Lcom/google/android/apps/viewer/film/FilmView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 178
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 179
    div-int/lit8 v4, v3, 0x2

    add-int/lit8 v4, v4, 0x2

    .line 180
    add-int/2addr v0, v4

    .line 181
    iget-object v5, p0, Lcom/google/android/apps/viewer/film/FilmView;->a:[I

    aput v0, v5, v1

    .line 182
    add-int/2addr v0, v4

    .line 183
    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    .line 184
    add-int/lit8 v0, v0, 0x1

    .line 176
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 187
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmView;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    .line 188
    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/film/FilmView;->a(I)V

    .line 189
    return v0
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmView;->getChildCount()I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 135
    invoke-virtual {p0, p1}, Lcom/google/android/apps/viewer/film/FilmView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method protected a()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/FilmView;->a(I)Landroid/widget/FrameLayout;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(I)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 130
    invoke-virtual {p0, p1}, Lcom/google/android/apps/viewer/film/FilmView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    return-object v0
.end method

.method a(IF)Landroid/widget/FrameLayout;
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v2, -0x1

    const/4 v8, 0x0

    const/4 v1, 0x0

    .line 209
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmView;->getChildCount()I

    move-result v5

    .line 210
    if-nez v5, :cond_0

    .line 211
    const/4 v0, 0x0

    .line 238
    :goto_0
    return-object v0

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmView;->a:[I

    aget v0, v0, v1

    if-ge p1, v0, :cond_9

    move v0, v1

    :goto_1
    move v3, v4

    .line 219
    :goto_2
    if-ge v3, v5, :cond_1

    .line 220
    iget-object v6, p0, Lcom/google/android/apps/viewer/film/FilmView;->a:[I

    aget v6, v6, v3

    if-le v6, p1, :cond_8

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/viewer/film/FilmView;->a:[I

    aget v0, v0, v3

    iget-object v6, p0, Lcom/google/android/apps/viewer/film/FilmView;->a:[I

    add-int/lit8 v7, v3, -0x1

    aget v6, v6, v7

    sub-int/2addr v0, v6

    .line 223
    cmpl-float v6, p2, v8

    if-lez v6, :cond_4

    .line 224
    iget-object v6, p0, Lcom/google/android/apps/viewer/film/FilmView;->a:[I

    add-int/lit8 v7, v3, -0x1

    aget v6, v6, v7

    div-int/lit8 v0, v0, 0x4

    add-int/2addr v0, v6

    if-ge p1, v0, :cond_3

    add-int/lit8 v0, v3, -0x1

    .line 233
    :cond_1
    :goto_3
    if-ne v0, v2, :cond_2

    .line 234
    add-int/lit8 v0, v5, -0x1

    .line 237
    :cond_2
    const-string v2, "FilmView"

    const-string v3, "Closest frame to (%s) is #%s at %s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v4

    const/4 v1, 0x2

    iget-object v4, p0, Lcom/google/android/apps/viewer/film/FilmView;->a:[I

    aget v4, v4, v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v1

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/FilmView;->a(I)Landroid/widget/FrameLayout;

    move-result-object v0

    goto :goto_0

    :cond_3
    move v0, v3

    .line 224
    goto :goto_3

    .line 225
    :cond_4
    cmpg-float v6, p2, v8

    if-gez v6, :cond_6

    .line 226
    iget-object v6, p0, Lcom/google/android/apps/viewer/film/FilmView;->a:[I

    aget v6, v6, v3

    div-int/lit8 v0, v0, 0x4

    sub-int v0, v6, v0

    if-ge p1, v0, :cond_5

    add-int/lit8 v3, v3, -0x1

    :cond_5
    move v0, v3

    goto :goto_3

    .line 228
    :cond_6
    iget-object v6, p0, Lcom/google/android/apps/viewer/film/FilmView;->a:[I

    add-int/lit8 v7, v3, -0x1

    aget v6, v6, v7

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v6

    if-ge p1, v0, :cond_7

    add-int/lit8 v3, v3, -0x1

    :cond_7
    move v0, v3

    .line 230
    goto :goto_3

    .line 219
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_9
    move v0, v2

    goto :goto_1
.end method

.method protected a(Lcom/google/android/apps/viewer/client/Dimensions;[Lcom/google/android/apps/viewer/client/Dimensions;)V
    .locals 4

    .prologue
    .line 73
    iget v0, p1, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    iput v0, p0, Lcom/google/android/apps/viewer/film/FilmView;->a:I

    .line 74
    iget v0, p1, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    iput v0, p0, Lcom/google/android/apps/viewer/film/FilmView;->b:I

    .line 75
    iput-object p2, p0, Lcom/google/android/apps/viewer/film/FilmView;->a:[Lcom/google/android/apps/viewer/client/Dimensions;

    .line 76
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/FilmView;->setPivotX(F)V

    .line 77
    iget v0, p0, Lcom/google/android/apps/viewer/film/FilmView;->a:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/FilmView;->setPivotY(F)V

    .line 78
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/FilmView;->a()V

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/viewer/film/FilmView;->a:[I

    .line 80
    const-string v0, "FilmView"

    const-string v1, "reset dimensions %s, tabs = null"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    return-void
.end method

.method protected b()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/film/FilmView;->getChildCount()I

    move-result v0

    .line 151
    if-lez v0, :cond_0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/FilmView;->a(I)Landroid/widget/FrameLayout;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 59
    sget-boolean v0, LavX;->g:Z

    if-eqz v0, :cond_0

    .line 60
    const v0, -0xff0001

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/film/FilmView;->setBackgroundColor(I)V

    .line 62
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 156
    iget v0, p0, Lcom/google/android/apps/viewer/film/FilmView;->a:I

    if-lez v0, :cond_0

    .line 157
    iget v0, p0, Lcom/google/android/apps/viewer/film/FilmView;->a:I

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 159
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 160
    invoke-direct {p0}, Lcom/google/android/apps/viewer/film/FilmView;->b()I

    .line 161
    return-void
.end method
