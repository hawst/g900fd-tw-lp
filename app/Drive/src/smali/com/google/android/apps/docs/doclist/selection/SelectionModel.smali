.class public interface abstract Lcom/google/android/apps/docs/doclist/selection/SelectionModel;
.super Ljava/lang/Object;
.source "SelectionModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K::",
        "Landroid/os/Parcelable;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract a()I
.end method

.method public abstract a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;)LKb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<TK;>;)",
            "LKb;"
        }
    .end annotation
.end method

.method public abstract a()LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmF",
            "<",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<TK;>;>;"
        }
    .end annotation
.end method

.method public abstract a()V
.end method

.method public abstract a(LKe;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LKe",
            "<TK;>;)V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<TK;>;Z)V"
        }
    .end annotation
.end method

.method public abstract a(Z)V
.end method

.method public abstract a()Z
.end method

.method public abstract a(Lcom/google/android/apps/docs/doclist/selection/ItemKey;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
            "<TK;>;)Z"
        }
    .end annotation
.end method

.method public abstract b()V
.end method

.method public abstract b(LKe;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LKe",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract b()Z
.end method

.method public abstract c()V
.end method
