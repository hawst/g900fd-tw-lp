.class public Lcom/google/android/apps/docs/app/MoveEntryActivity;
.super Lrm;
.source "MoveEntryActivity.java"


# instance fields
.field public a:LDc;

.field public a:LUi;

.field public a:LaGR;

.field public a:LaGg;

.field public a:Lald;

.field private a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field public a:LtK;

.field private a:Lux;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lrm;-><init>()V

    .line 123
    return-void
.end method

.method private a()LaGu;
    .locals 3

    .prologue
    .line 487
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LbmY;

    invoke-virtual {v0}, LbmY;->a()Lbqv;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 488
    iget-object v2, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LaGg;

    invoke-interface {v2, v0}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;

    move-result-object v0

    .line 489
    if-eqz v0, :cond_0

    .line 493
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;LbmY;)Landroid/content/Intent;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 501
    invoke-static {p0, p1, v0, v0, v0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a(Landroid/content/Context;Lbmv;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;Ljava/lang/Boolean;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lbmv;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lbmv",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 514
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 516
    invoke-static {p0, p1, p2, v0, v0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a(Landroid/content/Context;Lbmv;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;Ljava/lang/Boolean;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Lbmv;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;Ljava/lang/Boolean;)Landroid/content/Intent;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lbmv",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            "Ljava/lang/Boolean;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 522
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/app/MoveEntryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 523
    const-string v1, "entrySpecs"

    .line 524
    invoke-static {p1}, LbnG;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    .line 523
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 525
    if-eqz p2, :cond_0

    .line 526
    const-string v1, "targetEntrySpec"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 528
    :cond_0
    if-eqz p3, :cond_1

    .line 529
    const-string v1, "startCollectionEntrySpec"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 531
    invoke-static {p4}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 532
    const-string v1, "canStartCollectionBeTarget"

    .line 533
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 532
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 535
    :cond_1
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/MoveEntryActivity;)LbmY;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LbmY;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/MoveEntryActivity;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    return-object v0
.end method

.method private a(Luw;LaFV;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 321
    invoke-interface {p2}, LaFV;->d()Z

    move-result v0

    .line 322
    iget-boolean v3, p1, Luw;->d:Z

    if-nez v3, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 326
    iget-object v0, p1, Luw;->b:LbmY;

    invoke-virtual {v0}, LbmY;->size()I

    move-result v0

    iget-object v3, p1, Luw;->c:LbmY;

    invoke-virtual {v3}, LbmY;->size()I

    move-result v3

    add-int v5, v0, v3

    .line 327
    iget-boolean v0, p1, Luw;->d:Z

    if-eqz v0, :cond_8

    .line 330
    iget-object v0, p1, Luw;->a:LbmY;

    invoke-virtual {v0}, LbmY;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 360
    iget-boolean v0, p1, Luw;->a:Z

    if-eqz v0, :cond_6

    .line 361
    sget v3, Lxg;->move_confirm_dialog_message_unshare_files_and_folders_in_three_or_more_folders:I

    move-object v0, v4

    .line 373
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    .line 375
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v1

    iget-object v1, p1, Luw;->a:LbmY;

    .line 376
    invoke-virtual {v1}, LbmY;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v7, v2

    const/4 v1, 0x2

    aput-object v4, v7, v1

    const/4 v1, 0x3

    aput-object v0, v7, v1

    .line 373
    invoke-virtual {v6, v3, v5, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 391
    :goto_2
    return-object v0

    :cond_1
    move v0, v1

    .line 322
    goto :goto_0

    .line 332
    :pswitch_0
    iget-boolean v0, p1, Luw;->a:Z

    if-eqz v0, :cond_2

    .line 333
    sget v0, Lxg;->move_confirm_dialog_message_unshare_files_and_folders_in_one_folder:I

    move v3, v0

    .line 341
    :goto_3
    iget-object v0, p1, Luw;->a:LbmY;

    .line 342
    invoke-static {v0}, Lbnm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFV;

    invoke-interface {v0}, LaFV;->c()Ljava/lang/String;

    move-result-object v0

    move-object v9, v4

    move-object v4, v0

    move-object v0, v9

    .line 343
    goto :goto_1

    .line 335
    :cond_2
    iget-boolean v0, p1, Luw;->b:Z

    if-eqz v0, :cond_3

    .line 336
    sget v0, Lxg;->move_confirm_dialog_message_unshare_files_in_one_folder:I

    move v3, v0

    goto :goto_3

    .line 338
    :cond_3
    iget-boolean v0, p1, Luw;->c:Z

    invoke-static {v0}, LbiT;->a(Z)V

    .line 339
    sget v0, Lxg;->move_confirm_dialog_message_unshare_folders_in_one_folder:I

    move v3, v0

    goto :goto_3

    .line 345
    :pswitch_1
    iget-boolean v0, p1, Luw;->a:Z

    if-eqz v0, :cond_4

    .line 346
    sget v0, Lxg;->move_confirm_dialog_message_unshare_files_and_folders_in_two_folders:I

    move v3, v0

    .line 354
    :goto_4
    iget-object v0, p1, Luw;->a:LbmY;

    .line 355
    invoke-static {v0, v1}, Lbnm;->a(Ljava/lang/Iterable;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFV;

    invoke-interface {v0}, LaFV;->c()Ljava/lang/String;

    move-result-object v4

    .line 356
    iget-object v0, p1, Luw;->a:LbmY;

    .line 357
    invoke-static {v0, v2}, Lbnm;->a(Ljava/lang/Iterable;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFV;

    invoke-interface {v0}, LaFV;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 348
    :cond_4
    iget-boolean v0, p1, Luw;->b:Z

    if-eqz v0, :cond_5

    .line 349
    sget v0, Lxg;->move_confirm_dialog_message_unshare_files_in_two_folders:I

    move v3, v0

    goto :goto_4

    .line 351
    :cond_5
    iget-boolean v0, p1, Luw;->c:Z

    invoke-static {v0}, LbiT;->a(Z)V

    .line 352
    sget v0, Lxg;->move_confirm_dialog_message_unshare_folders_in_two_folders:I

    move v3, v0

    goto :goto_4

    .line 363
    :cond_6
    iget-boolean v0, p1, Luw;->b:Z

    if-eqz v0, :cond_7

    .line 364
    sget v3, Lxg;->move_confirm_dialog_message_unshare_files_in_three_or_more_folders:I

    move-object v0, v4

    goto/16 :goto_1

    .line 367
    :cond_7
    iget-boolean v0, p1, Luw;->c:Z

    invoke-static {v0}, LbiT;->a(Z)V

    .line 368
    sget v3, Lxg;->move_confirm_dialog_message_unshare_folders_in_three_or_more_folders:I

    move-object v0, v4

    goto/16 :goto_1

    .line 380
    :cond_8
    iget-boolean v0, p1, Luw;->a:Z

    if-eqz v0, :cond_9

    .line 381
    sget v0, Lxg;->move_confirm_dialog_message_share_files_and_folders:I

    .line 388
    :goto_5
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {p2}, LaFV;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v1

    invoke-virtual {v3, v0, v5, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 382
    :cond_9
    iget-boolean v0, p1, Luw;->b:Z

    if-eqz v0, :cond_a

    .line 383
    sget v0, Lxg;->move_confirm_dialog_message_share_files:I

    goto :goto_5

    .line 385
    :cond_a
    iget-boolean v0, p1, Luw;->c:Z

    invoke-static {v0}, LbiT;->a(Z)V

    .line 386
    sget v0, Lxg;->move_confirm_dialog_message_share_folders:I

    goto :goto_5

    .line 330
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Luw;Z)Ljava/lang/String;
    .locals 3

    .prologue
    .line 297
    iget-boolean v0, p1, Luw;->d:Z

    if-nez v0, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 301
    iget-boolean v0, p1, Luw;->d:Z

    if-eqz v0, :cond_2

    .line 302
    sget v0, Lxi;->move_confirm_dialog_title_unshare:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 317
    :goto_1
    return-object v0

    .line 297
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 304
    :cond_2
    iget-boolean v0, p1, Luw;->a:Z

    if-eqz v0, :cond_3

    .line 305
    sget v0, Lxg;->move_confirm_dialog_title_share_files_and_folders:I

    .line 312
    :goto_2
    iget-object v1, p1, Luw;->b:LbmY;

    .line 313
    invoke-virtual {v1}, LbmY;->size()I

    move-result v1

    iget-object v2, p1, Luw;->c:LbmY;

    invoke-virtual {v2}, LbmY;->size()I

    move-result v2

    add-int/2addr v1, v2

    .line 314
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 306
    :cond_3
    iget-boolean v0, p1, Luw;->b:Z

    if-eqz v0, :cond_4

    .line 307
    sget v0, Lxg;->move_confirm_dialog_title_share_files:I

    goto :goto_2

    .line 309
    :cond_4
    iget-boolean v0, p1, Luw;->c:Z

    invoke-static {v0}, LbiT;->b(Z)V

    .line 310
    sget v0, Lxg;->move_confirm_dialog_title_share_folders:I

    goto :goto_2
.end method

.method private a()Luq;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 210
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a()LaGu;

    move-result-object v0

    .line 211
    if-nez v0, :cond_0

    .line 212
    sget-object v0, Luq;->e:Luq;

    .line 253
    :goto_0
    return-object v0

    .line 215
    :cond_0
    invoke-interface {v0}, LaGu;->a()LaFM;

    move-result-object v3

    .line 217
    invoke-virtual {v3}, LaFM;->a()LaFO;

    move-result-object v0

    .line 216
    invoke-static {p0, v0}, Lcom/google/android/apps/docs/app/PickEntryActivity;->a(Landroid/content/Context;LaFO;)LuW;

    move-result-object v0

    new-array v4, v1, [LaGv;

    sget-object v5, LaGv;->a:LaGv;

    aput-object v5, v4, v2

    .line 218
    invoke-static {v4}, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->a([LaGv;)Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    move-result-object v4

    invoke-virtual {v0, v4}, LuW;->a(Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;)LuW;

    move-result-object v0

    sget v4, Lxi;->move:I

    .line 219
    invoke-virtual {v0, v4}, LuW;->a(I)LuW;

    move-result-object v0

    sget v4, Lxi;->move_dialog_title:I

    .line 220
    invoke-virtual {p0, v4}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LuW;->a(Ljava/lang/String;)LuW;

    move-result-object v0

    .line 221
    invoke-virtual {v0}, LuW;->a()LuW;

    move-result-object v0

    .line 222
    invoke-virtual {v0}, LuW;->b()LuW;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LbmY;

    .line 223
    invoke-virtual {v0, v4}, LuW;->a(Lbmv;)LuW;

    move-result-object v4

    .line 227
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    .line 228
    const-string v0, "startCollectionEntrySpec"

    invoke-virtual {v5, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 229
    const-string v0, "startCollectionEntrySpec"

    invoke-virtual {v5, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 231
    const-string v1, "canStartCollectionBeTarget"

    .line 232
    invoke-virtual {v5, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    .line 231
    invoke-static {v1}, LbiT;->a(Z)V

    .line 233
    const-string v1, "canStartCollectionBeTarget"

    invoke-virtual {v5, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    move v6, v1

    move-object v1, v0

    move v0, v6

    .line 246
    :goto_1
    invoke-virtual {v4, v1}, LuW;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LuW;

    .line 247
    if-eqz v0, :cond_1

    .line 248
    invoke-virtual {v4}, LuW;->c()LuW;

    .line 251
    :cond_1
    invoke-virtual {v4}, LuW;->a()Landroid/content/Intent;

    move-result-object v0

    .line 252
    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 253
    sget-object v0, Luq;->b:Luq;

    goto :goto_0

    .line 236
    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LaGg;

    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LbmY;

    .line 237
    invoke-static {v0}, Lbnm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v5, v0}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LbmY;

    move-result-object v0

    .line 238
    invoke-virtual {v0}, LbmY;->size()I

    move-result v5

    if-ne v5, v1, :cond_3

    .line 239
    invoke-static {v0}, Lbnm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    move v6, v1

    move-object v1, v0

    move v0, v6

    .line 240
    goto :goto_1

    .line 242
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LaGg;

    invoke-interface {v0, v3}, LaGg;->a(LaFM;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    move-object v1, v0

    move v0, v2

    .line 243
    goto :goto_1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/MoveEntryActivity;)Luq;
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a()Luq;

    move-result-object v0

    return-object v0
.end method

.method private a(LbmY;)Luw;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;)",
            "Luw;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 395
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v3

    .line 396
    new-instance v4, Lbna;

    invoke-direct {v4}, Lbna;-><init>()V

    .line 398
    new-instance v5, Lbna;

    invoke-direct {v5}, Lbna;-><init>()V

    .line 400
    new-instance v6, Lbna;

    invoke-direct {v6}, Lbna;-><init>()V

    .line 403
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LaGg;

    invoke-interface {v0}, LaGg;->a()V

    .line 405
    :try_start_0
    invoke-virtual {p1}, LbmY;->a()Lbqv;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 406
    iget-object v8, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LaGg;

    invoke-interface {v8, v0}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;

    move-result-object v8

    .line 407
    if-eqz v8, :cond_0

    .line 410
    invoke-interface {v8}, LaGu;->j()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 411
    invoke-virtual {v6, v0}, Lbna;->a(Ljava/lang/Object;)Lbna;

    .line 415
    :goto_0
    iget-object v8, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LaGg;

    .line 416
    invoke-interface {v8, v0}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LbmY;

    move-result-object v0

    .line 417
    invoke-virtual {v0}, LbmY;->a()Lbqv;

    move-result-object v8

    :cond_1
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 418
    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 421
    iget-object v9, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LaGg;

    invoke-interface {v9, v0}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFZ;

    move-result-object v0

    .line 422
    if-eqz v0, :cond_1

    invoke-interface {v0}, LaFV;->d()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 423
    invoke-virtual {v4, v0}, Lbna;->a(Ljava/lang/Object;)Lbna;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 429
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LaGg;

    invoke-interface {v1}, LaGg;->b()V

    throw v0

    .line 413
    :cond_2
    :try_start_1
    invoke-virtual {v5, v0}, Lbna;->a(Ljava/lang/Object;)Lbna;

    goto :goto_0

    .line 427
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LaGg;

    invoke-interface {v0}, LaGg;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 429
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LaGg;

    invoke-interface {v0}, LaGg;->b()V

    .line 432
    new-instance v3, Luw;

    const/4 v0, 0x0

    invoke-direct {v3, p0, v0}, Luw;-><init>(Lcom/google/android/apps/docs/app/MoveEntryActivity;Lum;)V

    .line 433
    invoke-virtual {v4}, Lbna;->a()LbmY;

    move-result-object v0

    iput-object v0, v3, Luw;->a:LbmY;

    .line 434
    invoke-virtual {v5}, Lbna;->a()LbmY;

    move-result-object v0

    iput-object v0, v3, Luw;->b:LbmY;

    .line 435
    invoke-virtual {v6}, Lbna;->a()LbmY;

    move-result-object v0

    iput-object v0, v3, Luw;->c:LbmY;

    .line 436
    iget-object v0, v3, Luw;->b:LbmY;

    .line 437
    invoke-virtual {v0}, LbmY;->size()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, v3, Luw;->c:LbmY;

    invoke-virtual {v0}, LbmY;->size()I

    move-result v0

    if-lez v0, :cond_4

    move v0, v1

    :goto_2
    iput-boolean v0, v3, Luw;->a:Z

    .line 438
    iget-object v0, v3, Luw;->b:LbmY;

    invoke-virtual {v0}, LbmY;->size()I

    move-result v0

    if-lez v0, :cond_5

    iget-object v0, v3, Luw;->c:LbmY;

    invoke-virtual {v0}, LbmY;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_3
    iput-boolean v0, v3, Luw;->b:Z

    .line 439
    iget-object v0, v3, Luw;->b:LbmY;

    invoke-virtual {v0}, LbmY;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, v3, Luw;->c:LbmY;

    invoke-virtual {v0}, LbmY;->size()I

    move-result v0

    if-lez v0, :cond_6

    move v0, v1

    :goto_4
    iput-boolean v0, v3, Luw;->c:Z

    .line 440
    iget-object v0, v3, Luw;->a:LbmY;

    invoke-virtual {v0}, LbmY;->size()I

    move-result v0

    if-lez v0, :cond_7

    :goto_5
    iput-boolean v1, v3, Luw;->d:Z

    .line 441
    return-object v3

    :cond_4
    move v0, v2

    .line 437
    goto :goto_2

    :cond_5
    move v0, v2

    .line 438
    goto :goto_3

    :cond_6
    move v0, v2

    .line 439
    goto :goto_4

    :cond_7
    move v1, v2

    .line 440
    goto :goto_5
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/MoveEntryActivity;)Lux;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Lux;

    return-object v0
.end method

.method private b()Luq;
    .locals 4

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LaGg;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v1}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFZ;

    move-result-object v0

    .line 258
    if-nez v0, :cond_0

    .line 259
    sget-object v0, Luq;->e:Luq;

    .line 293
    :goto_0
    return-object v0

    .line 262
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LbmY;

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a(LbmY;)Luw;

    move-result-object v1

    .line 263
    invoke-interface {v0}, LaFV;->d()Z

    move-result v2

    .line 264
    iget-object v3, v1, Luw;->a:LbmY;

    invoke-virtual {v3}, LbmY;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    if-nez v2, :cond_1

    .line 265
    sget-object v0, Luq;->d:Luq;

    goto :goto_0

    .line 268
    :cond_1
    invoke-static {p0}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v3

    .line 269
    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a(Luw;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 271
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a(Luw;LaFV;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 273
    sget v0, Lxi;->move:I

    new-instance v1, Lum;

    invoke-direct {v1, p0}, Lum;-><init>(Lcom/google/android/apps/docs/app/MoveEntryActivity;)V

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 279
    const/high16 v0, 0x1040000

    new-instance v1, Lun;

    invoke-direct {v1, p0}, Lun;-><init>(Lcom/google/android/apps/docs/app/MoveEntryActivity;)V

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 285
    new-instance v0, Luo;

    invoke-direct {v0, p0}, Luo;-><init>(Lcom/google/android/apps/docs/app/MoveEntryActivity;)V

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 291
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 293
    sget-object v0, Luq;->c:Luq;

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/MoveEntryActivity;)Luq;
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->b()Luq;

    move-result-object v0

    return-object v0
.end method

.method private c()Luq;
    .locals 2

    .prologue
    .line 445
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LaGR;

    new-instance v1, Lup;

    invoke-direct {v1, p0}, Lup;-><init>(Lcom/google/android/apps/docs/app/MoveEntryActivity;)V

    invoke-virtual {v0, v1}, LaGR;->a(LaGN;)V

    .line 458
    sget-object v0, Luq;->e:Luq;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/app/MoveEntryActivity;)Luq;
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->c()Luq;

    move-result-object v0

    return-object v0
.end method

.method private d()Luq;
    .locals 1

    .prologue
    .line 479
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->finish()V

    .line 480
    sget-object v0, Luq;->e:Luq;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/docs/app/MoveEntryActivity;)Luq;
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->d()Luq;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 194
    if-nez p1, :cond_1

    .line 195
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 196
    sget-object v0, Luq;->b:Luq;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Lux;

    .line 197
    invoke-virtual {v1}, Lux;->a()Luq;

    move-result-object v1

    invoke-virtual {v0, v1}, Luq;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 196
    invoke-static {v0}, LbiT;->b(Z)V

    .line 198
    const-string v0, "entrySpec.v2"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Lux;

    sget-object v1, Luq;->c:Luq;

    invoke-virtual {v0, v1}, Lux;->a(Luq;)V

    .line 207
    :goto_0
    return-void

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Lux;

    sget-object v1, Luq;->e:Luq;

    invoke-virtual {v0, v1}, Lux;->a(Luq;)V

    goto :goto_0

    .line 205
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Lux;

    invoke-virtual {v0}, Lux;->a()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 158
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 161
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "entrySpecs"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 162
    invoke-static {v0}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LbmY;

    .line 165
    if-eqz p1, :cond_0

    .line 166
    const-string v0, "movingState"

    .line 167
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 166
    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Luq;

    .line 168
    const-string v1, "collectionEntrySpec"

    .line 169
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 180
    :goto_0
    new-instance v1, Lux;

    invoke-direct {v1, p0, v0}, Lux;-><init>(Lcom/google/android/apps/docs/app/MoveEntryActivity;Luq;)V

    iput-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Lux;

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Lux;

    invoke-virtual {v0}, Lux;->a()V

    .line 182
    return-void

    .line 172
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "targetEntrySpec"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-eqz v0, :cond_1

    .line 174
    sget-object v0, Luq;->c:Luq;

    goto :goto_0

    .line 176
    :cond_1
    sget-object v0, Luq;->a:Luq;

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 186
    invoke-super {p0, p1}, Lrm;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 187
    const-string v0, "movingState"

    iget-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Lux;

    invoke-virtual {v1}, Lux;->a()Luq;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 188
    const-string v0, "collectionEntrySpec"

    iget-object v1, p0, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 189
    return-void
.end method
