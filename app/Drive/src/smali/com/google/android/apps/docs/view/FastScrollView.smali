.class public Lcom/google/android/apps/docs/view/FastScrollView;
.super Landroid/widget/FrameLayout;
.source "FastScrollView.java"

# interfaces
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field private final a:LIl;

.field private final a:LIm;

.field private a:LIo;

.field private a:LIp;

.field private final a:Lbuu;

.field private a:Lcom/google/android/apps/docs/view/CustomListView;

.field public a:LtK;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 60
    sget-object v0, LIm;->b:LIm;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIm;

    .line 62
    sget-object v0, LIl;->b:LIl;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIl;

    .line 71
    invoke-static {p1}, Lajt;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lajt;->a(Landroid/content/Context;)Lbuu;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:Lbuu;

    .line 72
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/view/FastScrollView;->a(Landroid/content/Context;)V

    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 60
    sget-object v0, LIm;->b:LIm;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIm;

    .line 62
    sget-object v0, LIl;->b:LIl;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIl;

    .line 78
    invoke-static {p1}, Lajt;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lajt;->a(Landroid/content/Context;)Lbuu;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:Lbuu;

    .line 79
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/view/FastScrollView;->a(Landroid/content/Context;)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    sget-object v0, LIm;->b:LIm;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIm;

    .line 62
    sget-object v0, LIl;->b:LIl;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIl;

    .line 85
    invoke-static {p1}, Lajt;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lajt;->a(Landroid/content/Context;)Lbuu;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:Lbuu;

    .line 86
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/view/FastScrollView;->a(Landroid/content/Context;)V

    .line 87
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    .line 142
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 143
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/FastScrollView;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 144
    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget v2, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    .line 145
    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v0

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIo;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/FastScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    move v3, v1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, LIo;->a(IIIILandroid/content/res/Resources;)V

    .line 147
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/FastScrollView;->setWillNotDraw(Z)V

    .line 93
    invoke-virtual {p0, p0}, Lcom/google/android/apps/docs/view/FastScrollView;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 94
    return-void
.end method


# virtual methods
.method a()Lbuu;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:Lbuu;

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIo;

    invoke-virtual {v0}, LIo;->b()V

    .line 106
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 137
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIo;

    invoke-virtual {v0, p1}, LIo;->a(Landroid/graphics/Canvas;)V

    .line 139
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIo;

    invoke-virtual {v0}, LIo;->a()V

    .line 102
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIo;

    invoke-virtual {v0}, LIo;->d()V

    .line 113
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/FastScrollView;->a()V

    .line 114
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:Lcom/google/android/apps/docs/view/CustomListView;

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:Lcom/google/android/apps/docs/view/CustomListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/CustomListView;->invalidateViews()V

    .line 219
    :cond_0
    return-void
.end method

.method public onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .locals 6

    .prologue
    .line 167
    instance-of v0, p2, Lcom/google/android/apps/docs/view/CustomListView;

    if-eqz v0, :cond_2

    .line 168
    check-cast p2, Lcom/google/android/apps/docs/view/CustomListView;

    iput-object p2, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:Lcom/google/android/apps/docs/view/CustomListView;

    .line 174
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIm;

    .line 175
    iget-object v5, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIl;

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LtK;

    sget-object v1, Lry;->G:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    sget-object v4, LIm;->a:LIm;

    .line 178
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/FastScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LakQ;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, LIl;->a:LIl;

    :goto_1
    move-object v5, v0

    .line 181
    :cond_0
    new-instance v0, LIo;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:Lcom/google/android/apps/docs/view/CustomListView;

    .line 182
    invoke-virtual {v1}, Lcom/google/android/apps/docs/view/CustomListView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:Lcom/google/android/apps/docs/view/CustomListView;

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, LIo;-><init>(Landroid/content/Context;Landroid/widget/AbsListView;Landroid/view/View;LIm;LIl;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIo;

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:Lcom/google/android/apps/docs/view/CustomListView;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIo;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/CustomListView;->setFastScroller(LIo;)V

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIo;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIp;

    invoke-virtual {v0, v1}, LIo;->a(LIp;)V

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:Lcom/google/android/apps/docs/view/CustomListView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/view/CustomListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 186
    :cond_1
    return-void

    .line 169
    :cond_2
    instance-of v0, p2, Lcom/google/android/apps/docs/view/SwipeToRefreshView;

    if-eqz v0, :cond_1

    .line 170
    const v0, 0x102000a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/CustomListView;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:Lcom/google/android/apps/docs/view/CustomListView;

    goto :goto_0

    .line 178
    :cond_3
    sget-object v0, LIl;->b:LIl;

    goto :goto_1
.end method

.method public onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:Lcom/google/android/apps/docs/view/CustomListView;

    if-ne p2, v0, :cond_0

    .line 191
    iput-object v1, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:Lcom/google/android/apps/docs/view/CustomListView;

    .line 192
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/FastScrollView;->b()V

    .line 193
    iput-object v1, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIo;

    .line 195
    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIo;

    invoke-virtual {v0, p1}, LIo;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIo;

    invoke-virtual {v0, p1, p2, p3, p4}, LIo;->a(Landroid/widget/AbsListView;III)V

    .line 163
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 157
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 6

    .prologue
    .line 151
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIo;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/FastScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, LIo;->a(IIIILandroid/content/res/Resources;)V

    .line 153
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIo;

    invoke-virtual {v0, p1}, LIo;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 205
    if-eqz v0, :cond_0

    .line 206
    const/4 v0, 0x1

    .line 209
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setOverlaySizeDp(I)V
    .locals 2

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/FastScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 132
    iget-object v1, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIo;

    invoke-virtual {v1, p1, v0}, LIo;->a(ILandroid/content/res/Resources;)V

    .line 133
    return-void
.end method

.method public setOverlayStatusListener(LIp;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIp;

    .line 98
    return-void
.end method

.method public setTextSize(I)V
    .locals 4

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/FastScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 123
    iget-object v1, p0, Lcom/google/android/apps/docs/view/FastScrollView;->a:LIo;

    const/4 v2, 0x2

    int-to-float v3, p1

    .line 124
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 123
    invoke-static {v2, v3, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {v1, v0}, LIo;->b(I)V

    .line 125
    return-void
.end method
