.class public Lcom/google/android/apps/docs/view/ThumbnailView;
.super Landroid/widget/FrameLayout;
.source "ThumbnailView.java"


# instance fields
.field private a:LaGu;

.field private final a:Landroid/view/View;

.field private final a:Landroid/widget/ImageView;

.field public a:LaqQ;

.field private a:Z

.field private final b:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/ThumbnailView;->a:Z

    .line 44
    instance-of v0, p1, Laju;

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Context must implements InjectorProvider"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p1

    .line 48
    check-cast v0, Laju;

    invoke-interface {v0}, Laju;->a()Lbuu;

    move-result-object v0

    .line 49
    invoke-interface {v0, p0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 51
    sget v0, Lxe;->thumbnail_view:I

    invoke-static {p1, v0, p0}, Lcom/google/android/apps/docs/view/ThumbnailView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 53
    sget v0, Lxc;->thumbnailImage:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/ThumbnailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/ThumbnailView;->a:Landroid/widget/ImageView;

    .line 54
    sget v0, Lxc;->thumbnail_progress_bar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/ThumbnailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/ThumbnailView;->a:Landroid/view/View;

    .line 55
    sget v0, Lxc;->open_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/ThumbnailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/ThumbnailView;->b:Landroid/view/View;

    .line 57
    new-instance v0, LaqR;

    invoke-direct {v0, p0}, LaqR;-><init>(Lcom/google/android/apps/docs/view/ThumbnailView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/ThumbnailView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/ThumbnailView;)LaGu;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/docs/view/ThumbnailView;->a:LaGu;

    return-object v0
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 93
    iget-object v3, p0, Lcom/google/android/apps/docs/view/ThumbnailView;->a:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/docs/view/ThumbnailView;->a:Landroid/view/View;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 95
    return-void

    :cond_0
    move v0, v2

    .line 93
    goto :goto_0

    :cond_1
    move v2, v1

    .line 94
    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/view/ThumbnailView;->a(Z)V

    .line 99
    return-void
.end method

.method public setEntry(LaGu;)V
    .locals 2

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/apps/docs/view/ThumbnailView;->a:LaGu;

    .line 74
    iget-object v1, p0, Lcom/google/android/apps/docs/view/ThumbnailView;->b:Landroid/view/View;

    invoke-interface {p1}, LaGu;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/docs/view/ThumbnailView;->a:Z

    if-nez v0, :cond_1

    :cond_0
    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 75
    return-void

    .line 74
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setIcon(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/docs/view/ThumbnailView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/docs/view/ThumbnailView;->a:Landroid/widget/ImageView;

    const/16 v1, 0x32

    invoke-virtual {v0, v2, v2, v2, v1}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 89
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/view/ThumbnailView;->a(Z)V

    .line 90
    return-void
.end method

.method public setImage(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/docs/view/ThumbnailView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/docs/view/ThumbnailView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 81
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/view/ThumbnailView;->a(Z)V

    .line 82
    return-void
.end method

.method public setOpenButtonVisibility(Z)V
    .locals 2

    .prologue
    .line 102
    iget-object v1, p0, Lcom/google/android/apps/docs/view/ThumbnailView;->b:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 103
    iput-boolean p1, p0, Lcom/google/android/apps/docs/view/ThumbnailView;->a:Z

    .line 104
    return-void

    .line 102
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
