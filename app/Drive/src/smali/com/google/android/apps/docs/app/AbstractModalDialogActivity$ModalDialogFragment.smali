.class public abstract Lcom/google/android/apps/docs/app/AbstractModalDialogActivity$ModalDialogFragment;
.super Lcom/google/android/apps/docs/tools/gelly/android/GuiceDialogFragment;
.source "AbstractModalDialogActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 34
    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/AbstractModalDialogActivity$ModalDialogFragment;->a(Landroid/content/DialogInterface;)V

    .line 25
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AbstractModalDialogActivity$ModalDialogFragment;->a()LH;

    move-result-object v0

    .line 27
    if-eqz v0, :cond_0

    .line 28
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 30
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 31
    return-void
.end method
