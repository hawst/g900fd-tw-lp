.class public Lcom/google/android/apps/docs/app/BaseDialogFragment;
.super Lcom/google/android/apps/docs/tools/gelly/android/GuiceDialogFragment;
.source "BaseDialogFragment.java"


# instance fields
.field private a:Landroid/os/Handler;

.field protected a:LaqY;

.field public a:LaqZ;

.field public a:LqK;

.field public a:LsI;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()LarB;
    .locals 4

    .prologue
    .line 56
    new-instance v0, Lsd;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a()LH;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a:LsI;

    iget-object v3, p0, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a:LqK;

    invoke-direct {v0, v1, v2, v3}, Lsd;-><init>(Landroid/app/Activity;LsI;LqK;)V

    return-object v0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceDialogFragment;->a_(Landroid/os/Bundle;)V

    .line 43
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a()LH;

    move-result-object v0

    .line 44
    instance-of v1, v0, Lrm;

    if-eqz v1, :cond_0

    .line 45
    check-cast v0, Lrm;

    invoke-virtual {v0}, Lrm;->a()LaqY;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a:LaqY;

    .line 50
    :goto_0
    const/4 v0, 0x0

    sget v1, Lxj;->CakemixTheme_Dialog:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a(II)V

    .line 51
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a:Landroid/os/Handler;

    .line 52
    return-void

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a:LaqZ;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a()LH;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a()LarB;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LaqZ;->a(Landroid/app/Activity;LarB;)LaqY;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a:LaqY;

    goto :goto_0
.end method

.method public a_(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a:LaqY;

    invoke-interface {v0, p1}, LaqY;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceDialogFragment;->b(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a()LH;

    move-result-object v0

    invoke-static {v0}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v0

    invoke-virtual {v0}, LEU;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a:Landroid/os/Handler;

    new-instance v2, Lrr;

    invoke-direct {v2, p0, v0}, Lrr;-><init>(Lcom/google/android/apps/docs/app/BaseDialogFragment;Landroid/app/Dialog;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 80
    return-object v0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 95
    const-string v0, "BaseIsRestart"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 96
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceDialogFragment;->c(Landroid/os/Bundle;)V

    .line 97
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 89
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceDialogFragment;->e()V

    .line 90
    return-void
.end method
