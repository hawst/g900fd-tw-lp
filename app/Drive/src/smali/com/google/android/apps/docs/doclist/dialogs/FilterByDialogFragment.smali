.class public Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "FilterByDialogFragment.java"


# instance fields
.field public a:LFc;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    .line 35
    return-void
.end method

.method public static a(LM;LCl;LbmF;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LM;",
            "LCl;",
            "LbmF",
            "<+",
            "LCl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 76
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    new-instance v0, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;-><init>()V

    .line 80
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 81
    const-string v2, "currentFilter"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 82
    const-string v2, "availableFilters"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 83
    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;->e(Landroid/os/Bundle;)V

    .line 84
    const-string v1, "FilterByDialogFragment"

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;->a(LM;Ljava/lang/String;)V

    .line 85
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;->a()LH;

    move-result-object v0

    invoke-static {v0}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v2

    .line 46
    sget v0, Lxi;->menu_filter_by:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 48
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v1

    .line 50
    const-string v0, "availableFilters"

    .line 51
    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 52
    const-string v3, "currentFilter"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, LCl;

    .line 53
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 55
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 56
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LCl;

    .line 57
    invoke-interface {v1}, LCl;->a()I

    move-result v1

    .line 58
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 59
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 62
    :cond_0
    new-instance v1, LFb;

    invoke-direct {v1, p0, v0}, LFb;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/FilterByDialogFragment;Ljava/util/List;)V

    .line 69
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    .line 70
    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    .line 69
    invoke-virtual {v2, v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 71
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
