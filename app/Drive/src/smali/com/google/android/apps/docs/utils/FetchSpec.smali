.class public Lcom/google/android/apps/docs/utils/FetchSpec;
.super Ljava/lang/Object;
.source "FetchSpec.java"

# interfaces
.implements LakM;
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LakM",
        "<",
        "Ljava/lang/Long;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/docs/utils/FetchSpec;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:J

.field private final a:Laon;

.field private final a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

.field private final a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private final a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

.field private final b:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 103
    new-instance v0, Lalm;

    invoke-direct {v0}, Lalm;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/utils/FetchSpec;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/ResourceSpec;JJLcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    iget-object v3, p1, Lcom/google/android/gms/drive/database/data/EntrySpec;->a:LaFO;

    .line 44
    invoke-virtual {v0, v3}, LaFO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    .line 43
    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 45
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 46
    iput-object p2, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    .line 47
    const-wide/16 v4, 0x0

    cmp-long v0, p5, v4

    if-ltz v0, :cond_2

    :goto_1
    invoke-static {v2}, LbiT;->a(Z)V

    .line 48
    iput-wide p3, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->a:J

    .line 49
    iput-wide p5, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->b:J

    .line 50
    invoke-static {p7}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    iput-object v0, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    .line 51
    invoke-static {p1, p3, p4, p7}, Laon;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;JLcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)Laon;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->a:Laon;

    .line 52
    return-void

    :cond_1
    move v0, v1

    .line 44
    goto :goto_0

    :cond_2
    move v2, v1

    .line 47
    goto :goto_1
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/ResourceSpec;JJLcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;Lalm;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct/range {p0 .. p7}, Lcom/google/android/apps/docs/utils/FetchSpec;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/ResourceSpec;JJLcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)V

    return-void
.end method

.method public static a(LaGA;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)Lcom/google/android/apps/docs/utils/FetchSpec;
    .locals 9

    .prologue
    .line 148
    invoke-interface {p0}, LaGA;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    .line 149
    invoke-interface {p0}, LaGA;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v3

    .line 150
    invoke-interface {p0}, LaGA;->c()J

    move-result-wide v4

    .line 151
    invoke-interface {p0}, LaGA;->b()I

    move-result v0

    .line 153
    new-instance v1, Lcom/google/android/apps/docs/utils/FetchSpec;

    int-to-long v6, v0

    move-object v8, p1

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/docs/utils/FetchSpec;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/ResourceSpec;JJLcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)V

    return-object v1
.end method

.method public static a(LaGu;ILcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)Lcom/google/android/apps/docs/utils/FetchSpec;
    .locals 9

    .prologue
    .line 142
    new-instance v1, Lcom/google/android/apps/docs/utils/FetchSpec;

    invoke-interface {p0}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-interface {p0}, LaGu;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v3

    .line 143
    invoke-interface {p0}, LaGu;->b()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    int-to-long v6, p1

    move-object v8, p2

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/docs/utils/FetchSpec;-><init>(Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/ResourceSpec;JJLcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;)V

    return-object v1
.end method


# virtual methods
.method public a()Laon;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->a:Laon;

    return-object v0
.end method

.method public a()Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    return-object v0
.end method

.method public a()Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    return-object v0
.end method

.method public a()Lcom/google/android/gms/drive/database/data/ResourceSpec;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    return-object v0
.end method

.method public a()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->b:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/google/android/apps/docs/utils/FetchSpec;->a()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 82
    instance-of v1, p1, Lcom/google/android/apps/docs/utils/FetchSpec;

    if-nez v1, :cond_1

    .line 87
    :cond_0
    :goto_0
    return v0

    .line 85
    :cond_1
    check-cast p1, Lcom/google/android/apps/docs/utils/FetchSpec;

    .line 86
    iget-object v1, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    iget-object v2, p1, Lcom/google/android/apps/docs/utils/FetchSpec;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-static {v1, v2}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v2, p1, Lcom/google/android/apps/docs/utils/FetchSpec;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 87
    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->a:J

    iget-wide v4, p1, Lcom/google/android/apps/docs/utils/FetchSpec;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->b:J

    iget-wide v4, p1, Lcom/google/android/apps/docs/utils/FetchSpec;->b:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 94
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FetchSpec["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 134
    iget-wide v0, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 135
    iget-wide v0, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->a()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/docs/utils/FetchSpec;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 138
    return-void
.end method
