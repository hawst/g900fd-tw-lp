.class public Lcom/google/android/apps/docs/app/AccountsActivity;
.super Lrm;
.source "AccountsActivity.java"


# instance fields
.field public a:LSF;

.field public a:Lald;

.field private a:Landroid/widget/Button;

.field private a:Landroid/widget/ListView;

.field private a:[Landroid/accounts/Account;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lrm;-><init>()V

    return-void
.end method

.method private a(LaFO;)V
    .locals 3

    .prologue
    .line 164
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 165
    const-string v1, "accountName"

    invoke-virtual {p1}, LaFO;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 166
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/docs/app/AccountsActivity;->setResult(ILandroid/content/Intent;)V

    .line 167
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AccountsActivity;->finish()V

    .line 168
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/AccountsActivity;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/AccountsActivity;->j()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/AccountsActivity;LaFO;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/AccountsActivity;->a(LaFO;)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/AccountsActivity;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/AccountsActivity;->f()V

    return-void
.end method

.method private f()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:LSF;

    invoke-interface {v0}, LSF;->a()[Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:[Landroid/accounts/Account;

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:[Landroid/accounts/Account;

    array-length v0, v0

    new-array v4, v0, [Ljava/lang/String;

    .line 116
    iget-object v5, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:[Landroid/accounts/Account;

    array-length v6, v5

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v6, :cond_0

    aget-object v7, v5, v0

    .line 117
    add-int/lit8 v3, v2, 0x1

    iget-object v7, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v7, v4, v2

    .line 116
    add-int/lit8 v0, v0, 0x1

    move v2, v3

    goto :goto_0

    .line 120
    :cond_0
    new-instance v0, Landroid/widget/ArrayAdapter;

    sget v2, Lxe;->accounts_list_row:I

    invoke-direct {v0, p0, v2, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 121
    iget-object v2, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:Landroid/widget/ListView;

    new-instance v2, Lri;

    invoke-direct {v2, p0, v4}, Lri;-><init>(Lcom/google/android/apps/docs/app/AccountsActivity;[Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:Landroid/widget/Button;

    new-instance v2, Lrj;

    invoke-direct {v2, p0}, Lrj;-><init>(Lcom/google/android/apps/docs/app/AccountsActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AccountsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lxg;->accounts_title:I

    array-length v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    .line 138
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    array-length v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 139
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AccountsActivity;->a()LaqY;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, LaqY;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    return-void
.end method

.method private j()V
    .locals 3

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:LqK;

    const-string v1, "systemConfiguration"

    const-string v2, "addAccount"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:LSF;

    const-string v1, "com.google"

    new-instance v2, Lrk;

    invoke-direct {v2, p0}, Lrk;-><init>(Lcom/google/android/apps/docs/app/AccountsActivity;)V

    invoke-interface {v0, v1, p0, v2}, LSF;->a(Ljava/lang/String;Landroid/app/Activity;LSG;)V

    .line 161
    return-void
.end method


# virtual methods
.method protected a_()V
    .locals 1

    .prologue
    .line 77
    invoke-super {p0}, Lrm;->a_()V

    .line 79
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/AccountsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:Landroid/widget/ListView;

    .line 80
    sget v0, Lxc;->new_account_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/AccountsActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:Landroid/widget/Button;

    .line 81
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 56
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AccountsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 59
    const-string v1, "android.intent.action.PICK"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 60
    const-string v1, "AccountsActivity"

    const-string v2, "Invalid intent: %s"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 61
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AccountsActivity;->finish()V

    .line 73
    :goto_0
    return-void

    .line 65
    :cond_0
    sget v0, Lxe;->accounts_activity:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/AccountsActivity;->setContentView(I)V

    .line 67
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AccountsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxb;->home_bg_plain:I

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 68
    invoke-static {v0}, Lakq;->a(Landroid/graphics/Bitmap;)Lakq;

    move-result-object v0

    invoke-virtual {v0}, Lakq;->a()Lakq;

    move-result-object v0

    invoke-virtual {v0}, Lakq;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 69
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AccountsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 70
    invoke-virtual {v1, v3}, Landroid/graphics/drawable/BitmapDrawable;->setDither(Z)V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AccountsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/BitmapDrawable;->setTargetDensity(Landroid/util/DisplayMetrics;)V

    .line 72
    sget v0, Lxc;->root_layout:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/AccountsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, v1}, LUv;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AccountsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 92
    sget v1, Lxf;->menu_accounts_activity:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 93
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 98
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lxc;->menu_account_settings:I

    if-ne v1, v2, :cond_0

    .line 99
    iget-object v1, p0, Lcom/google/android/apps/docs/app/AccountsActivity;->a:LSF;

    invoke-interface {v1, p0}, LSF;->a(Landroid/content/Context;)V

    .line 107
    :goto_0
    return v0

    .line 101
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lxc;->menu_send_feedback:I

    if-ne v1, v2, :cond_1

    .line 102
    new-instance v1, Lali;

    const-string v2, "android_docs"

    invoke-direct {v1, p0, v2}, Lali;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    .line 104
    invoke-virtual {v1}, Lali;->a()V

    goto :goto_0

    .line 107
    :cond_1
    invoke-super {p0, p1}, Lrm;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 85
    invoke-super {p0}, Lrm;->onResume()V

    .line 86
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/AccountsActivity;->f()V

    .line 87
    return-void
.end method
