.class public Lcom/google/android/apps/viewer/client/ProjectorClientService;
.super Landroid/app/Service;
.source "ProjectorClientService.java"


# instance fields
.field private final a:Landroid/os/Messenger;

.field private a:Lcom/google/android/apps/viewer/client/Projector$CloseCallback;

.field private a:Lcom/google/android/apps/viewer/client/Projector$FileInfoSource;

.field private b:Landroid/os/Messenger;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 40
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ProjectorClientService"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 41
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 42
    new-instance v1, LauP;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, p0, v0}, LauP;-><init>(Lcom/google/android/apps/viewer/client/ProjectorClientService;Landroid/os/Looper;)V

    .line 48
    new-instance v0, Landroid/os/Messenger;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/client/ProjectorClientService;->a:Landroid/os/Messenger;

    .line 49
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/client/ProjectorClientService;)Landroid/os/Messenger;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/apps/viewer/client/ProjectorClientService;->b:Landroid/os/Messenger;

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/viewer/client/ProjectorClientService;->a:Lcom/google/android/apps/viewer/client/Projector$CloseCallback;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/viewer/client/ProjectorClientService;->a:Lcom/google/android/apps/viewer/client/Projector$CloseCallback;

    invoke-interface {v0}, Lcom/google/android/apps/viewer/client/Projector$CloseCallback;->a()V

    .line 83
    :cond_0
    return-void
.end method

.method private a(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 52
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 63
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/viewer/client/ProjectorClientService;->b(Landroid/os/Message;)V

    .line 65
    :goto_0
    return-void

    .line 57
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/viewer/client/ProjectorClientService;->c(Landroid/os/Message;)V

    goto :goto_0

    .line 60
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/apps/viewer/client/ProjectorClientService;->a()V

    goto :goto_0

    .line 52
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/client/ProjectorClientService;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/google/android/apps/viewer/client/ProjectorClientService;->a(Landroid/os/Message;)V

    return-void
.end method

.method private b(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    iput-object v0, p0, Lcom/google/android/apps/viewer/client/ProjectorClientService;->b:Landroid/os/Messenger;

    .line 70
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 71
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/client/ProjectorClientService;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 72
    const-string v0, "infos"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/viewer/client/Projector$FileInfoSource;

    iput-object v0, p0, Lcom/google/android/apps/viewer/client/ProjectorClientService;->a:Lcom/google/android/apps/viewer/client/Projector$FileInfoSource;

    .line 73
    const-string v0, "close"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/viewer/client/Projector$CloseCallback;

    iput-object v0, p0, Lcom/google/android/apps/viewer/client/ProjectorClientService;->a:Lcom/google/android/apps/viewer/client/Projector$CloseCallback;

    .line 74
    return-void
.end method

.method private c(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/viewer/client/ProjectorClientService;->a:Lcom/google/android/apps/viewer/client/Projector$FileInfoSource;

    const-string v1, "Init message not received with file source."

    invoke-static {v0, v1}, LauV;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 90
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 91
    iget-object v1, p0, Lcom/google/android/apps/viewer/client/ProjectorClientService;->a:Lcom/google/android/apps/viewer/client/Projector$FileInfoSource;

    new-instance v2, LauQ;

    invoke-direct {v2, p0, v0}, LauQ;-><init>(Lcom/google/android/apps/viewer/client/ProjectorClientService;I)V

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/viewer/client/Projector$FileInfoSource;->getFileInfo(ILauO;)V

    .line 105
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3

    .prologue
    .line 109
    const-string v0, "ProjectorClientService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Projector client service is bound "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/viewer/client/ProjectorClientService;->a:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method
