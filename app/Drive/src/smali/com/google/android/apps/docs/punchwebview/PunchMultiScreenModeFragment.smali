.class public Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;
.super Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;
.source "PunchMultiScreenModeFragment.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation


# instance fields
.field public a:LWS;

.field private a:LWT;

.field private a:LXI;

.field private a:Z

.field a:[Landroid/view/Display;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 87
    const-string v0, "PunchMultiScreenModeFragment"

    sget-object v1, LYR;->c:LYR;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;-><init>(Ljava/lang/String;LYR;)V

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:Z

    .line 88
    return-void
.end method

.method private D()V
    .locals 3

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LWT;

    if-nez v0, :cond_0

    .line 151
    new-instance v0, LXG;

    invoke-direct {v0, p0}, LXG;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LWT;

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LWS;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LWT;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LWS;->a(LWT;Landroid/os/Handler;)V

    .line 189
    return-void
.end method

.method private E()V
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LWS;

    const-string v1, "android.hardware.display.category.PRESENTATION"

    invoke-interface {v0, v1}, LWS;->a(Ljava/lang/String;)[Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:[Landroid/view/Display;

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:[Landroid/view/Display;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 197
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->A()V

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 201
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LXI;

    if-eqz v0, :cond_2

    .line 203
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LXI;

    .line 204
    invoke-virtual {v1}, LXI;->getDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getDisplayId()I

    move-result v1

    if-eq v0, v1, :cond_3

    const/4 v0, 0x1

    .line 205
    :goto_1
    if-eqz v0, :cond_2

    .line 206
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->C()V

    .line 210
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LXI;

    if-nez v0, :cond_0

    .line 211
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->B()V

    goto :goto_0

    .line 204
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private F()V
    .locals 3

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->n()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 264
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LXx;

    invoke-interface {v0}, LXx;->a()Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;

    move-result-object v0

    const-string v1, "presentationModeFragmentTag"

    const-string v2, "presentationModeFragment"

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a(Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    return-void

    .line 263
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;)LXI;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LXI;

    return-object v0
.end method

.method private a()Landroid/view/Display;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:[Landroid/view/Display;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:[Landroid/view/Display;

    array-length v0, v0

    if-lez v0, :cond_1

    :goto_1
    invoke-static {v1}, LbiT;->b(Z)V

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:[Landroid/view/Display;

    aget-object v0, v0, v2

    return-object v0

    :cond_0
    move v0, v2

    .line 284
    goto :goto_0

    :cond_1
    move v1, v2

    .line 285
    goto :goto_1
.end method

.method public static a()Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;-><init>()V

    return-object v0
.end method

.method private a()Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;
    .locals 2

    .prologue
    .line 269
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a()LH;

    move-result-object v0

    invoke-virtual {v0}, LH;->a()LM;

    move-result-object v0

    const-string v1, "presentationModeFragmentTag"

    .line 270
    invoke-virtual {v0, v1}, LM;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 271
    instance-of v1, v0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->E()V

    return-void
.end method

.method private n()Z
    .locals 1

    .prologue
    .line 276
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a()Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public A()V
    .locals 2

    .prologue
    .line 216
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a()Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;->a()V

    .line 221
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->z()V

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LXx;

    invoke-interface {v0}, LXx;->a()Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;

    move-result-object v0

    const-string v1, "ViewModeFragment"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a(Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;Ljava/lang/String;)V

    .line 226
    return-void
.end method

.method B()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 295
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LXI;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 298
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a()Landroid/view/Display;

    move-result-object v0

    .line 299
    invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I

    move-result v3

    .line 300
    iget-object v4, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->c:Ljava/lang/String;

    const-string v5, "Showing presentation on display #%s."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v4, v5, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 302
    new-instance v1, LXI;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a()LH;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, LXI;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;Landroid/content/Context;Landroid/view/Display;)V

    iput-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LXI;

    .line 303
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LXI;

    invoke-virtual {v0}, LXI;->show()V

    .line 304
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LXI;

    new-instance v1, LXH;

    invoke-direct {v1, p0}, LXH;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;)V

    invoke-virtual {v0, v1}, LXI;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 315
    return-void

    :cond_0
    move v0, v2

    .line 295
    goto :goto_0
.end method

.method public C()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LXI;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 327
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 328
    if-eqz v0, :cond_0

    .line 329
    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 333
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->b:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 340
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LXI;

    invoke-virtual {v0}, LXI;->getDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I

    move-result v0

    .line 341
    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->c:Ljava/lang/String;

    const-string v4, "Dismissing presentation on display #%s."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v3, v4, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LXI;

    invoke-virtual {v0}, LXI;->dismiss()V

    .line 343
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LXI;

    .line 344
    return-void

    :cond_1
    move v0, v2

    .line 324
    goto :goto_0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LQr;

    invoke-static {v0}, LZD;->a(LQr;)LZD;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LZD;

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LYO;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LZD;

    invoke-virtual {v1}, LZD;->a()LYK;

    move-result-object v1

    invoke-interface {v0, v1}, LYO;->a(LYK;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LYO;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LZD;

    .line 97
    invoke-virtual {v1}, LZD;->a()Z

    move-result v1

    .line 96
    invoke-interface {v0, v1}, LYO;->c(Z)V

    .line 99
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->a_(Landroid/os/Bundle;)V

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LWS;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a()LH;

    move-result-object v1

    invoke-interface {v0, v1}, LWS;->a(Landroid/content/Context;)V

    .line 101
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 129
    invoke-super {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->g()V

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LWS;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LWT;

    invoke-interface {v0, v1}, LWS;->a(LWT;)V

    .line 131
    return-void
.end method

.method protected m()Z
    .locals 1

    .prologue
    .line 233
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a()Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;->w()V

    .line 235
    const/4 v0, 0x1

    .line 237
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o_()V
    .locals 2

    .prologue
    .line 105
    invoke-super {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->o_()V

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->b:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->c:Ljava/lang/String;

    const-string v1, "early exit in onResume as fragment was not fully created"

    invoke-static {v0, v1}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->A()V

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a(Z)V

    .line 114
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->D()V

    .line 118
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->E()V

    .line 121
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:Z

    if-eqz v0, :cond_0

    .line 122
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->w()V

    .line 123
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:Z

    goto :goto_0
.end method

.method protected v()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:LXI;

    if-eqz v0, :cond_0

    .line 139
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->C()V

    .line 142
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragmentBase;->v()V

    .line 143
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:Landroid/webkit/WebView;

    .line 144
    return-void
.end method

.method public w()V
    .locals 1

    .prologue
    .line 248
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a()LH;

    move-result-object v0

    if-nez v0, :cond_0

    .line 251
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a:Z

    .line 260
    :goto_0
    return-void

    .line 255
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 256
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->a()Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationModeFragment;->w()V

    goto :goto_0

    .line 258
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchMultiScreenModeFragment;->F()V

    goto :goto_0
.end method
