.class public Lcom/google/android/apps/docs/app/detailpanel/DetailActivityDelegate;
.super LvC;
.source "DetailActivityDelegate.java"

# interfaces
.implements LPo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, LvC;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 25
    sget v0, Lxe;->detail_list_activity:I

    return v0
.end method

.method protected a()Lcom/google/android/apps/docs/entry/DetailDrawerFragment;
    .locals 2

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/detailpanel/DetailActivityDelegate;->a()LM;

    move-result-object v0

    sget v1, Lxc;->detail_drawer_fragment:I

    invoke-virtual {v0, v1}, LM;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    return-object v0
.end method

.method protected a()Lcom/google/android/apps/docs/fragment/DetailFragment;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/detailpanel/DetailActivityDelegate;->a()Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a()Lcom/google/android/apps/docs/fragment/DetailFragment;

    move-result-object v0

    return-object v0
.end method

.method public b(F)V
    .locals 0

    .prologue
    .line 47
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/detailpanel/DetailActivityDelegate;->a(F)V

    .line 48
    return-void
.end method

.method public k()V
    .locals 0

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/detailpanel/DetailActivityDelegate;->l()V

    .line 43
    return-void
.end method

.method public m()V
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/detailpanel/DetailActivityDelegate;->a()Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->l()V

    .line 53
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 19
    invoke-super {p0, p1}, LvC;->onCreate(Landroid/os/Bundle;)V

    .line 20
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/detailpanel/DetailActivityDelegate;->a()Lcom/google/android/apps/docs/entry/DetailDrawerFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/entry/DetailDrawerFragment;->a()V

    .line 21
    return-void
.end method
