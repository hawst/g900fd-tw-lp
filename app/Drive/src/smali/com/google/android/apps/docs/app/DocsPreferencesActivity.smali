.class public Lcom/google/android/apps/docs/app/DocsPreferencesActivity;
.super Lajs;
.source "DocsPreferencesActivity.java"

# interfaces
.implements Lrl;


# instance fields
.field private a:LEE;

.field public a:LEF;

.field public a:LWy;

.field public a:Lakf;

.field private a:LaqY;

.field public a:LaqZ;

.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LWx;",
            ">;"
        }
    .end annotation
.end field

.field public a:LqK;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lajs;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LEF;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, LEF;->a(Landroid/app/Activity;I)LEE;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LEE;

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LWx;

    .line 105
    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LEE;

    invoke-interface {v0, v2}, LWx;->a(LEE;)V

    goto :goto_0

    .line 107
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x1

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 57
    invoke-super {p0, p1}, Lajs;->onCreate(Landroid/os/Bundle;)V

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LqK;

    invoke-virtual {v0}, LqK;->a()V

    .line 60
    if-nez p1, :cond_0

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LqK;

    invoke-virtual {v0, p0}, LqK;->a(Landroid/app/Activity;)V

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LaqZ;

    invoke-interface {v0, p0}, LaqZ;->a(Landroid/app/Activity;)LaqY;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LaqY;

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LWy;

    invoke-virtual {v0}, LWy;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Ljava/util/List;

    .line 68
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a()V

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LWx;

    .line 71
    invoke-interface {v0}, LWx;->a()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->addPreferencesFromResource(I)V

    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-interface {v0, v2}, LWx;->a(Landroid/preference/PreferenceScreen;)V

    goto :goto_0

    .line 74
    :cond_1
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LEE;

    invoke-interface {v0, p1}, LEE;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LEE;

    invoke-interface {v0, p1}, LEE;->a(I)Landroid/app/Dialog;

    move-result-object v0

    .line 120
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lajs;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LqK;

    invoke-virtual {v0}, LqK;->b()V

    .line 136
    invoke-super {p0}, Lajs;->onDestroy()V

    .line 137
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LaqY;

    invoke-interface {v0, p1}, LaqY;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lajs;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Lakf;

    .line 93
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LWx;

    .line 96
    invoke-interface {v0}, LWx;->b()V

    goto :goto_0

    .line 99
    :cond_0
    invoke-super {p0}, Lajs;->onPause()V

    .line 100
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LEE;

    invoke-interface {v0, p1}, LEE;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LEE;

    invoke-interface {v0, p1, p2}, LEE;->a(ILandroid/app/Dialog;)V

    .line 131
    :goto_0
    return-void

    .line 128
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lajs;->onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 78
    invoke-super {p0}, Lajs;->onResume()V

    .line 80
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Lakf;

    .line 82
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LWx;

    .line 85
    invoke-interface {v0}, LWx;->a()V

    goto :goto_0

    .line 87
    :cond_0
    return-void
.end method
