.class public Lcom/google/android/apps/docs/welcome/Page;
.super Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;
.source "Page.java"

# interfaces
.implements LasF;


# instance fields
.field private a:I

.field private a:LUf;

.field private final a:Landroid/os/Handler;

.field private a:Landroid/webkit/WebView;

.field public a:LasI;

.field private a:Lasj;

.field public a:Lasl;

.field private final a:Lbtd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbtd",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;-><init>()V

    .line 137
    iput-object v0, p0, Lcom/google/android/apps/docs/welcome/Page;->c:Ljava/lang/String;

    .line 138
    iput-object v0, p0, Lcom/google/android/apps/docs/welcome/Page;->d:Ljava/lang/String;

    .line 143
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Landroid/os/Handler;

    .line 144
    invoke-static {}, Lbtd;->a()Lbtd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Lbtd;

    .line 354
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/welcome/Page;)LUf;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/Page;->a:LUf;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/welcome/Page;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/welcome/Page;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Landroid/webkit/WebView;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/welcome/Page;)Lasj;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Lasj;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/welcome/Page;)Lbtd;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Lbtd;

    return-object v0
.end method

.method public static a(Latc;ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/docs/welcome/Page;
    .locals 4

    .prologue
    .line 95
    new-instance v0, Lcom/google/android/apps/docs/welcome/Page;

    invoke-direct {v0}, Lcom/google/android/apps/docs/welcome/Page;-><init>()V

    .line 96
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 97
    const-string v2, "page-id"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 98
    const-string v2, "page-uri"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const-string v2, "screenType"

    invoke-virtual {p0}, Latc;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const-string v2, "page-text"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/welcome/Page;->e(Landroid/os/Bundle;)V

    .line 102
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/welcome/Page;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/Page;->e:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/welcome/Page;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/android/apps/docs/welcome/Page;->c:Ljava/lang/String;

    return-object p1
.end method

.method private a(Landroid/webkit/WebSettings;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x8
    .end annotation

    .prologue
    .line 393
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/webkit/WebSettings;->setBlockNetworkLoads(Z)V

    .line 394
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/welcome/Page;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/Page;->f:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/welcome/Page;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/android/apps/docs/welcome/Page;->d:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 150
    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/Page;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 151
    const-string v1, "page-uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 152
    const-string v2, "page-id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/docs/welcome/Page;->a:I

    .line 153
    const-string v2, "screenType"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/docs/welcome/Page;->e:Ljava/lang/String;

    .line 154
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "_"

    const-string v4, "-r"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/docs/welcome/Page;->f:Ljava/lang/String;

    .line 156
    new-instance v2, Landroid/webkit/WebView;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Landroid/webkit/WebView;

    .line 158
    iget-object v2, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Landroid/webkit/WebView;

    invoke-virtual {v2, v6}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 159
    iget-object v2, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Landroid/webkit/WebView;

    invoke-virtual {v2, v6}, Landroid/webkit/WebView;->setHorizontalScrollBarEnabled(Z)V

    .line 161
    iget-object v2, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Landroid/webkit/WebView;

    invoke-virtual {v2, v7}, Landroid/webkit/WebView;->setFocusable(Z)V

    .line 162
    iget-object v2, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Landroid/webkit/WebView;

    invoke-virtual {v2, v7}, Landroid/webkit/WebView;->setFocusableInTouchMode(Z)V

    .line 164
    new-instance v2, LUf;

    iget-object v3, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Landroid/webkit/WebView;

    invoke-direct {v2, v3}, LUf;-><init>(Landroid/webkit/WebView;)V

    iput-object v2, p0, Lcom/google/android/apps/docs/welcome/Page;->a:LUf;

    .line 166
    new-instance v2, Lasj;

    const-string v3, "page-text"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, p0, v0}, Lasj;-><init>(Lcom/google/android/apps/docs/welcome/Page;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Lasj;

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 169
    invoke-virtual {v0, v7}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 170
    invoke-virtual {v0, v6}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    .line 171
    invoke-virtual {v0, v6}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 172
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x8

    if-lt v2, v3, :cond_0

    .line 173
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/welcome/Page;->a(Landroid/webkit/WebSettings;)V

    .line 176
    :cond_0
    new-instance v0, Lasq;

    invoke-direct {v0, p0, v5}, Lasq;-><init>(Lcom/google/android/apps/docs/welcome/Page;Lasg;)V

    .line 178
    iget-object v2, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Landroid/webkit/WebView;

    new-instance v3, Lasg;

    invoke-direct {v3, p0, v0}, Lasg;-><init>(Lcom/google/android/apps/docs/welcome/Page;Lasq;)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 209
    iget-object v2, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Landroid/webkit/WebView;

    new-instance v3, Lasi;

    invoke-direct {v3, p0}, Lasi;-><init>(Lcom/google/android/apps/docs/welcome/Page;)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 217
    iget-object v2, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Landroid/webkit/WebView;

    new-instance v3, Lasm;

    invoke-direct {v3, p0, v5}, Lasm;-><init>(Lcom/google/android/apps/docs/welcome/Page;Lasg;)V

    const-string v4, "stringExporter"

    invoke-virtual {v2, v3, v4}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 218
    iget-object v2, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Landroid/webkit/WebView;

    new-instance v3, Lasp;

    invoke-direct {v3, p0, v5}, Lasp;-><init>(Lcom/google/android/apps/docs/welcome/Page;Lasg;)V

    const-string v4, "welcomeReader"

    invoke-virtual {v2, v3, v4}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 219
    iget-object v2, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Landroid/webkit/WebView;

    new-instance v3, Lasn;

    invoke-direct {v3, p0, v5}, Lasn;-><init>(Lcom/google/android/apps/docs/welcome/Page;Lasg;)V

    const-string v4, "welcomeOffer"

    invoke-virtual {v2, v3, v4}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 220
    iget-object v2, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Landroid/webkit/WebView;

    const-string v3, "windowLoadListener"

    invoke-virtual {v2, v0, v3}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Landroid/webkit/WebView;

    new-instance v2, Lask;

    invoke-direct {v2, p0, v5}, Lask;-><init>(Lcom/google/android/apps/docs/welcome/Page;Lasg;)V

    const-string v3, "pageLoadListener"

    invoke-virtual {v0, v2, v3}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Landroid/webkit/WebView;

    return-object v0
.end method

.method public a()LbsU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbsU",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Lbtd;

    return-object v0
.end method

.method public a(LaFO;)V
    .locals 3

    .prologue
    .line 381
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/Page;->a:LUf;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "offerClaimGranted(\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, LaFO;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LUf;->a(Ljava/lang/String;)V

    .line 382
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 229
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->a(Landroid/app/Activity;)V

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Lasl;

    iget v1, p0, Lcom/google/android/apps/docs/welcome/Page;->a:I

    invoke-virtual {p0}, Lcom/google/android/apps/docs/welcome/Page;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lasl;->a(ILjava/lang/String;)V

    .line 231
    return-void
.end method

.method public a(ZLjava/lang/String;)V
    .locals 5

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/Page;->a:LUf;

    const-string v1, "offerClaimDeclined(%b, \"%s\")"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LUf;->a(Ljava/lang/String;)V

    .line 389
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/Page;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/Page;->d:Ljava/lang/String;

    return-object v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 398
    invoke-super {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->h()V

    .line 399
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 400
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    .line 402
    :cond_0
    return-void
.end method

.method public n_()V
    .locals 2

    .prologue
    .line 235
    invoke-super {p0}, Lcom/google/android/apps/docs/tools/gelly/android/GuiceFragment;->n_()V

    .line 236
    iget-object v0, p0, Lcom/google/android/apps/docs/welcome/Page;->a:Lasl;

    iget v1, p0, Lcom/google/android/apps/docs/welcome/Page;->a:I

    invoke-virtual {v0, v1}, Lasl;->a(I)V

    .line 237
    return-void
.end method
