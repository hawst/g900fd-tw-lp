.class public Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;
.super Landroid/widget/FrameLayout;
.source "SwipableDocListLayout.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field private final a:I

.field private final a:LMD;

.field private a:LMJ;

.field private a:LMK;

.field private a:LML;

.field private final a:Landroid/graphics/drawable/GradientDrawable;

.field private final a:Landroid/view/GestureDetector;

.field private a:Landroid/view/View;

.field private final a:Landroid/widget/Scroller;

.field private a:Z

.field private final b:I

.field private b:Landroid/view/View;

.field private b:Z

.field private final c:I

.field private c:Z

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 100
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 92
    iput-boolean v6, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:Z

    .line 93
    iput-boolean v5, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->b:Z

    .line 94
    iput-boolean v5, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->c:Z

    .line 96
    sget-object v0, LML;->c:LML;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LML;

    .line 101
    new-instance v0, LMD;

    invoke-direct {v0, p1}, LMD;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LMD;

    .line 102
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LMM;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, LMM;-><init>(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;LMI;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:Landroid/view/GestureDetector;

    .line 103
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:Landroid/widget/Scroller;

    .line 104
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->LEFT_RIGHT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v2, 0x2

    new-array v2, v2, [I

    aput v6, v2, v6

    .line 105
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, LwZ;->drop_shadow_start:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v2, v5

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:Landroid/graphics/drawable/GradientDrawable;

    .line 107
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxa;->min_distance_for_fling:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:I

    .line 108
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxa;->fling_velocity:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->b:I

    .line 109
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxa;->drop_shadow_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->c:I

    .line 111
    invoke-virtual {p0, v5}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->setChildrenDrawingOrderEnabled(Z)V

    .line 112
    invoke-virtual {p0, v6}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->setWillNotDraw(Z)V

    .line 113
    invoke-static {p0}, Lec;->b(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_0

    .line 115
    invoke-static {p0, v5}, Lec;->a(Landroid/view/View;I)V

    .line 118
    :cond_0
    return-void
.end method

.method private a(F)F
    .locals 2

    .prologue
    .line 381
    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->b()I

    move-result v1

    int-to-float v1, v1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;F)F
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(F)F

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)I
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->b()I

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)LMJ;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LMJ;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)LMK;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LMK;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)LML;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LML;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;LML;)LML;
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LML;

    return-object p1
.end method

.method private a()Landroid/view/View;
    .locals 2

    .prologue
    .line 389
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LML;

    sget-object v1, LML;->b:LML;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->b:Landroid/view/View;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:Landroid/view/View;

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)Landroid/view/View;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)Landroid/widget/Scroller;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:Landroid/widget/Scroller;

    return-object v0
.end method

.method private a(ILandroid/view/View;III)V
    .locals 7

    .prologue
    .line 288
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:Z

    .line 290
    new-instance v0, LMI;

    move-object v1, p0

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, LMI;-><init>(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;IIIILandroid/view/View;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->post(Ljava/lang/Runnable;)Z

    .line 322
    return-void
.end method

.method private a(II)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 254
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LML;

    sget-object v2, LML;->c:LML;

    if-ne v0, v2, :cond_0

    move v0, v1

    .line 283
    :goto_0
    return v0

    .line 257
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a()Landroid/view/View;

    move-result-object v0

    .line 258
    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v0

    float-to-int v3, v0

    .line 260
    if-nez p1, :cond_6

    .line 263
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 264
    if-ge v3, v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->b:I

    neg-int v0, v0

    :goto_1
    move v4, v0

    .line 268
    :goto_2
    if-lez v4, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->b()I

    move-result v5

    .line 272
    :goto_3
    const/4 v0, -0x1

    if-ne p2, v0, :cond_5

    .line 273
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LML;

    sget-object v1, LML;->b:LML;

    if-ne v0, v1, :cond_3

    if-lez v4, :cond_3

    .line 274
    iget v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->d:I

    add-int/lit8 v1, v0, -0x1

    .line 282
    :goto_4
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a()Landroid/view/View;

    move-result-object v2

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(ILandroid/view/View;III)V

    .line 283
    const/4 v0, 0x1

    goto :goto_0

    .line 264
    :cond_1
    iget v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->b:I

    goto :goto_1

    :cond_2
    move v5, v1

    .line 268
    goto :goto_3

    .line 275
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LML;

    sget-object v1, LML;->a:LML;

    if-ne v0, v1, :cond_4

    if-gez v4, :cond_4

    .line 276
    iget v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->d:I

    add-int/lit8 v1, v0, 0x1

    goto :goto_4

    .line 278
    :cond_4
    iget v1, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->d:I

    goto :goto_4

    :cond_5
    move v1, p2

    goto :goto_4

    :cond_6
    move v4, p1

    goto :goto_2
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->c:Z

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;II)Z
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(II)Z

    move-result v0

    return v0
.end method

.method private b()I
    .locals 2

    .prologue
    .line 385
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->c:I

    add-int/2addr v0, v1

    return v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->d:I

    return v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)Landroid/view/View;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->b:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:Z

    return v0
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:I

    return v0
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)Landroid/view/View;
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 372
    iput-boolean v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->b:Z

    .line 373
    iput-boolean v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:Z

    .line 374
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->c:Z

    .line 375
    sget-object v0, LML;->c:LML;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LML;

    .line 376
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 377
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 378
    return-void
.end method

.method public static synthetic d(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->b:I

    return v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->d:I

    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->b:Z

    .line 131
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/view/DocListView;Landroid/view/View;LMK;LMJ;)V
    .locals 1

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:Landroid/view/View;

    .line 122
    iput-object p2, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->b:Landroid/view/View;

    .line 123
    iput-object p3, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LMK;

    .line 124
    iput-object p4, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LMJ;

    .line 125
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->setCurrentPageInternal(I)V

    .line 126
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a()V

    .line 127
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LMD;

    invoke-virtual {v0}, LMD;->a()Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->c:Z

    .line 135
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 162
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LML;

    sget-object v1, LML;->c:LML;

    if-ne v0, v1, :cond_0

    .line 170
    :goto_0
    return-void

    .line 166
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 167
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->c:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 169
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method protected getChildDrawingOrder(II)I
    .locals 2

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LML;

    sget-object v1, LML;->b:LML;

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->b:Z

    if-nez v0, :cond_1

    .line 177
    :cond_0
    add-int/lit8 v0, p1, -0x1

    sub-int p2, v0, p2

    .line 179
    :cond_1
    return p2
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 188
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    .line 191
    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    if-ne v2, v0, :cond_2

    .line 193
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LMD;

    invoke-virtual {v0}, LMD;->a()V

    move v0, v1

    .line 213
    :cond_1
    :goto_0
    return v0

    .line 198
    :cond_2
    if-eqz v2, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LMD;

    invoke-virtual {v3}, LMD;->a()Z

    move-result v3

    if-nez v3, :cond_1

    .line 200
    :cond_3
    if-eqz v2, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LMD;

    invoke-virtual {v0}, LMD;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 201
    goto :goto_0

    .line 205
    :cond_4
    if-nez v2, :cond_6

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LMD;

    invoke-virtual {v0, p1, p0}, LMD;->a(Landroid/view/MotionEvent;Landroid/view/View;)V

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 213
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LMD;

    invoke-virtual {v0}, LMD;->a()Z

    move-result v0

    goto :goto_0

    .line 210
    :cond_6
    const/4 v0, 0x2

    if-ne v2, v0, :cond_5

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LMD;

    invoke-virtual {v0, p1, p0}, LMD;->a(Landroid/view/MotionEvent;Landroid/view/View;)V

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 240
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 241
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LMD;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, LMD;->a(I)V

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:Landroid/graphics/drawable/GradientDrawable;

    iget v1, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->c:I

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setBounds(IIII)V

    .line 243
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 224
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    .line 225
    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 228
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:Z

    if-nez v1, :cond_1

    .line 229
    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(II)Z

    .line 232
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LMD;

    invoke-virtual {v1}, LMD;->a()V

    .line 234
    :cond_2
    invoke-static {p0}, Lec;->a(Landroid/view/View;)V

    .line 235
    return v0
.end method

.method public setCurrentPage(I)V
    .locals 2

    .prologue
    .line 148
    iget v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->d:I

    sub-int v0, p1, v0

    int-to-float v0, v0

    invoke-static {v0}, LML;->a(F)LML;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LML;

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LML;

    sget-object v1, LML;->a:LML;

    if-ne v0, v1, :cond_0

    .line 150
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->b()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 151
    iget v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->b:I

    neg-int v0, v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(II)Z

    .line 158
    :goto_0
    return-void

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LML;

    sget-object v1, LML;->b:LML;

    if-ne v0, v1, :cond_1

    .line 153
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 154
    iget v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->b:I

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a(II)Z

    goto :goto_0

    .line 156
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->setCurrentPageInternal(I)V

    goto :goto_0
.end method

.method public setCurrentPageInternal(I)V
    .locals 1

    .prologue
    .line 142
    iput p1, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->d:I

    .line 143
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->c()V

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->a:LMJ;

    invoke-interface {v0, p1}, LMJ;->a(I)V

    .line 145
    return-void
.end method
