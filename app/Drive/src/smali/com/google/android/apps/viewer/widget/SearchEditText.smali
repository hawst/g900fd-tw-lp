.class public Lcom/google/android/apps/viewer/widget/SearchEditText;
.super Landroid/widget/EditText;
.source "SearchEditText.java"


# instance fields
.field private final a:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 17
    new-instance v0, LaxW;

    invoke-direct {v0, p0}, LaxW;-><init>(Lcom/google/android/apps/viewer/widget/SearchEditText;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/widget/SearchEditText;->a:Ljava/lang/Runnable;

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    new-instance v0, LaxW;

    invoke-direct {v0, p0}, LaxW;-><init>(Lcom/google/android/apps/viewer/widget/SearchEditText;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/widget/SearchEditText;->a:Ljava/lang/Runnable;

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 17
    new-instance v0, LaxW;

    invoke-direct {v0, p0}, LaxW;-><init>(Lcom/google/android/apps/viewer/widget/SearchEditText;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/widget/SearchEditText;->a:Ljava/lang/Runnable;

    .line 38
    return-void
.end method


# virtual methods
.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 3

    .prologue
    .line 42
    invoke-super {p0, p1, p2, p3}, Landroid/widget/EditText;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 43
    if-eqz p1, :cond_1

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/viewer/widget/SearchEditText;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/widget/SearchEditText;->post(Ljava/lang/Runnable;)Z

    .line 52
    :cond_0
    :goto_0
    return-void

    .line 47
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/widget/SearchEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 48
    if-eqz v0, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/widget/SearchEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0
.end method
