.class public abstract Lcom/google/android/apps/docs/view/DocThumbnailView;
.super Lcom/google/android/apps/docs/view/FixedSizeImageView;
.source "DocThumbnailView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/view/FixedSizeImageView;-><init>(Landroid/content/Context;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/view/FixedSizeImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/view/FixedSizeImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    return-void
.end method


# virtual methods
.method public abstract a()Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Property",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end method

.method public a()V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/DocThumbnailView;->setThumbnail(Landroid/graphics/drawable/Drawable;)V

    .line 51
    return-void
.end method

.method public abstract a(LapI;)Z
.end method

.method public abstract setState(LapI;Z)V
.end method

.method public abstract setThumbnail(Landroid/graphics/drawable/Drawable;)V
.end method
