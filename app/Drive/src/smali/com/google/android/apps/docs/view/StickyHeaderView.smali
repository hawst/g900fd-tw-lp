.class public Lcom/google/android/apps/docs/view/StickyHeaderView;
.super Landroid/widget/LinearLayout;
.source "StickyHeaderView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements LapH;


# instance fields
.field private a:I

.field private a:LIB;

.field private a:Landroid/view/View;

.field private a:LaqN;

.field private a:Z

.field private b:I

.field private b:Z

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 156
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 142
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:I

    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:Z

    .line 153
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->d:I

    .line 157
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 160
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 142
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:I

    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:Z

    .line 153
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->d:I

    .line 161
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 165
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 142
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:I

    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:Z

    .line 153
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->d:I

    .line 166
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:LaqN;

    if-nez v0, :cond_0

    .line 235
    :goto_0
    return-void

    .line 222
    :cond_0
    iget v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->b:I

    .line 225
    const v1, 0x3dcccccd    # 0.1f

    const v2, 0x3f666666    # 0.9f

    .line 226
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/StickyHeaderView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    iget v4, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/StickyHeaderView;->getMeasuredHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    .line 230
    iget-object v2, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:LaqN;

    iget-object v3, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:Landroid/view/View;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v4, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-interface {v2, v3, v1}, LaqN;->a(Landroid/view/View;F)V

    .line 232
    iget-object v1, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 233
    iget-object v2, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 234
    iget-object v3, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:Landroid/view/View;

    const/4 v4, 0x0

    add-int/2addr v2, v0

    invoke-virtual {v3, v4, v0, v1, v2}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method

.method private a(II)V
    .locals 1

    .prologue
    .line 327
    if-nez p1, :cond_0

    iget v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:I

    if-ne p2, v0, :cond_0

    .line 328
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/StickyHeaderView;->setVisibility(I)V

    .line 333
    :goto_0
    return-void

    .line 330
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/StickyHeaderView;->setVisibility(I)V

    .line 331
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:Z

    goto :goto_0
.end method

.method private a(Landroid/widget/AbsListView;II)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 341
    if-nez p2, :cond_0

    move v0, v1

    .line 342
    :goto_0
    invoke-virtual {p1, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 343
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    if-ge v2, v0, :cond_1

    .line 344
    iget-object v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:LaqN;

    invoke-interface {v0, v1}, LaqN;->b(Landroid/view/View;)V

    .line 350
    :goto_1
    const/4 v0, 0x1

    :goto_2
    if-ge v0, p3, :cond_2

    .line 351
    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 352
    iget-object v2, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:LaqN;

    invoke-interface {v2, v1}, LaqN;->a(Landroid/view/View;)V

    .line 350
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 341
    :cond_0
    iget v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:I

    goto :goto_0

    .line 346
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:LaqN;

    invoke-interface {v0, v1}, LaqN;->a(Landroid/view/View;)V

    goto :goto_1

    .line 354
    :cond_2
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 357
    iget v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->b:I

    iget v1, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:I

    sub-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 358
    :goto_0
    if-eqz v0, :cond_1

    sget-object v0, LaqM;->b:LaqM;

    .line 360
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:LaqN;

    iget-object v2, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:Landroid/view/View;

    invoke-interface {v1, v2, v0}, LaqN;->a(Landroid/view/View;LaqM;)V

    .line 361
    return-void

    .line 357
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 358
    :cond_1
    sget-object v0, LaqM;->a:LaqM;

    goto :goto_1
.end method


# virtual methods
.method protected a(I)V
    .locals 2

    .prologue
    .line 376
    iput p1, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->d:I

    .line 377
    iget-object v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:LaqN;

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:LaqN;

    iget v1, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->d:I

    invoke-interface {v0, v1}, LaqN;->a(I)V

    .line 380
    :cond_0
    return-void
.end method

.method public a(Landroid/widget/AbsListView;)V
    .locals 3

    .prologue
    .line 243
    invoke-static {}, LamV;->a()V

    .line 244
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v0

    invoke-virtual {p1}, Landroid/widget/AbsListView;->getChildCount()I

    move-result v1

    .line 245
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getCount()I

    move-result v2

    .line 244
    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/google/android/apps/docs/view/StickyHeaderView;->onScroll(Landroid/widget/AbsListView;III)V

    .line 246
    return-void
.end method

.method public a(LapG;)V
    .locals 2

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:LaqN;

    if-eqz v0, :cond_0

    .line 371
    iget-object v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:LaqN;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:Landroid/view/View;

    invoke-interface {v0, v1, p1}, LaqN;->a(Landroid/view/View;LapG;)V

    .line 373
    :cond_0
    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 214
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/StickyHeaderView;->a()V

    .line 215
    return-void
.end method

.method public onMeasure(II)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 200
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 201
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    const/high16 v2, -0x80000000

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 203
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 206
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/StickyHeaderView;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 207
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/StickyHeaderView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 208
    invoke-virtual {v3, v1, v2}, Landroid/view/View;->measure(II)V

    .line 206
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 210
    :cond_0
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x4

    const/4 v7, 0x0

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:LaqN;

    if-nez v0, :cond_1

    .line 320
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    iget v3, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->b:I

    .line 263
    invoke-virtual {p1, v7}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 264
    if-nez v0, :cond_2

    .line 265
    invoke-virtual {p0, v5}, Lcom/google/android/apps/docs/view/StickyHeaderView;->setVisibility(I)V

    goto :goto_0

    .line 269
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:LaqN;

    invoke-interface {v2, v0}, LaqN;->a(Landroid/view/View;)LIB;

    move-result-object v4

    .line 270
    if-nez v4, :cond_3

    .line 271
    invoke-virtual {p0, v5}, Lcom/google/android/apps/docs/view/StickyHeaderView;->setVisibility(I)V

    goto :goto_0

    .line 276
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:LaqN;

    invoke-interface {v2, v0}, LaqN;->b(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_b

    move-object v2, v0

    .line 280
    :goto_1
    add-int/lit8 v0, p2, 0x1

    .line 282
    if-ge v0, p4, :cond_a

    .line 283
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 284
    if-eqz v0, :cond_a

    .line 285
    iget-object v5, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:LaqN;

    invoke-interface {v5, v0}, LaqN;->a(Landroid/view/View;)LIB;

    move-result-object v5

    .line 286
    if-eqz v5, :cond_a

    iget-object v5, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:LaqN;

    invoke-interface {v5, v0}, LaqN;->b(Landroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 292
    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:LIB;

    invoke-virtual {v4, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 293
    iput-object v4, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:LIB;

    .line 294
    iget-object v1, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:LaqN;

    iget-object v5, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/StickyHeaderView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-interface {v1, v5, v4, v6}, LaqN;->a(Landroid/view/View;LIB;Landroid/content/Context;)V

    .line 297
    :cond_4
    if-eqz v2, :cond_8

    .line 298
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v1

    .line 299
    :goto_3
    if-nez p2, :cond_5

    .line 300
    iget v2, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:I

    add-int/2addr v1, v2

    .line 303
    :cond_5
    if-eqz v0, :cond_9

    .line 304
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    .line 306
    :goto_4
    invoke-direct {p0, p2, v1}, Lcom/google/android/apps/docs/view/StickyHeaderView;->a(II)V

    .line 307
    iget-boolean v2, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->b:Z

    if-eqz v2, :cond_6

    .line 308
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/view/StickyHeaderView;->a(Landroid/widget/AbsListView;II)V

    .line 311
    :cond_6
    iget v2, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:I

    iget v4, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->c:I

    sub-int/2addr v0, v4

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->b:I

    .line 312
    iget v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->b:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->b:I

    .line 314
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/StickyHeaderView;->b()V

    .line 316
    iget v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->b:I

    if-ne v3, v0, :cond_7

    iget-boolean v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:Z

    if-eqz v0, :cond_0

    .line 317
    :cond_7
    iput-boolean v7, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:Z

    .line 318
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/StickyHeaderView;->requestLayout()V

    goto/16 :goto_0

    .line 298
    :cond_8
    const v1, -0x7fffffff

    goto :goto_3

    .line 304
    :cond_9
    const v0, 0x7fffffff

    goto :goto_4

    :cond_a
    move-object v0, v1

    goto :goto_2

    :cond_b
    move-object v2, v1

    goto :goto_1
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 365
    return-void
.end method

.method public setAdapter(LaqN;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 169
    iput-object p1, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:LaqN;

    .line 171
    iget v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->d:I

    invoke-interface {p1, v0}, LaqN;->a(I)V

    .line 173
    invoke-interface {p1}, LaqN;->a()I

    move-result v0

    neg-int v0, v0

    iput v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:I

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/StickyHeaderView;->removeView(Landroid/view/View;)V

    .line 178
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/StickyHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p1, v0}, LaqN;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:Landroid/view/View;

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:Landroid/view/View;

    invoke-interface {p1, v0}, LaqN;->a(Landroid/view/View;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->b:Z

    .line 186
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 187
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 189
    iget-object v2, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:Landroid/view/View;

    sget-object v3, LaqM;->b:LaqM;

    invoke-interface {p1, v2, v3}, LaqN;->a(Landroid/view/View;LaqM;)V

    .line 190
    iget-object v2, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:Landroid/view/View;

    invoke-virtual {v2, v0, v1}, Landroid/view/View;->measure(II)V

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->c:I

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/StickyHeaderView;->addView(Landroid/view/View;)V

    .line 195
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/StickyHeaderView;->a:LIB;

    .line 196
    return-void
.end method
