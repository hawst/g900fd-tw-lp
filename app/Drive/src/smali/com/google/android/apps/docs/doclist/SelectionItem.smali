.class public Lcom/google/android/apps/docs/doclist/SelectionItem;
.super Ljava/lang/Object;
.source "SelectionItem.java"

# interfaces
.implements Lcom/google/android/apps/docs/doclist/selection/ItemKey;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/docs/doclist/selection/ItemKey",
        "<",
        "Lcom/google/android/gms/drive/database/data/EntrySpec;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/docs/doclist/SelectionItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lcom/google/android/apps/docs/doclist/SelectionItem;

.field private a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    new-instance v0, LCT;

    invoke-direct {v0}, LCT;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/doclist/SelectionItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    const-class v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/SelectionItem;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 32
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/docs/doclist/SelectionItem;->a:Z

    .line 33
    const-class v0, Lcom/google/android/apps/docs/doclist/SelectionItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/SelectionItem;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/SelectionItem;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    .line 34
    return-void

    .line 32
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;LCT;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/doclist/SelectionItem;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/database/data/EntrySpec;ZLcom/google/android/apps/docs/doclist/SelectionItem;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p3, p0, Lcom/google/android/apps/docs/doclist/SelectionItem;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    .line 25
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/SelectionItem;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 26
    iput-boolean p2, p0, Lcom/google/android/apps/docs/doclist/SelectionItem;->a:Z

    .line 27
    return-void
.end method


# virtual methods
.method public bridge synthetic a()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/SelectionItem;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    return-object v0
.end method

.method public a()Lcom/google/android/apps/docs/doclist/SelectionItem;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/SelectionItem;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    return-object v0
.end method

.method public bridge synthetic a()Lcom/google/android/apps/docs/doclist/selection/ItemKey;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/SelectionItem;->a()Lcom/google/android/apps/docs/doclist/SelectionItem;

    move-result-object v0

    return-object v0
.end method

.method public a()Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/SelectionItem;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/apps/docs/doclist/SelectionItem;->a:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 71
    if-ne p0, p1, :cond_1

    .line 78
    :cond_0
    :goto_0
    return v0

    .line 73
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/docs/doclist/SelectionItem;

    if-eqz v2, :cond_3

    .line 74
    check-cast p1, Lcom/google/android/apps/docs/doclist/SelectionItem;

    .line 75
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/SelectionItem;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v3, p1, Lcom/google/android/apps/docs/doclist/SelectionItem;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/SelectionItem;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    iget-object v3, p1, Lcom/google/android/apps/docs/doclist/SelectionItem;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    .line 76
    invoke-static {v2, v3}, LbiL;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 78
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 66
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/SelectionItem;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/SelectionItem;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    aput-object v2, v0, v1

    invoke-static {v0}, LbiL;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 43
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/SelectionItem;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 45
    iget-boolean v0, p0, Lcom/google/android/apps/docs/doclist/SelectionItem;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/SelectionItem;->a:Lcom/google/android/apps/docs/doclist/SelectionItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 47
    return-void

    .line 45
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
