.class public Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;
.super Ljava/lang/Object;
.source "DocumentFileManagerImpl.java"

# interfaces
.implements Ladi;


# instance fields
.field private a:J

.field private final a:LMs;

.field private final a:LaGg;

.field private final a:LaKR;

.field private final a:LadE;

.field private final a:Lady;

.field private final a:LahT;

.field private final a:Lalo;

.field private final a:LamL;

.field private final a:Lamn;

.field final a:Lamw;

.field private final a:Landroid/content/Context;

.field final a:LbsW;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ladw;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/concurrent/locks/Lock;

.field private final a:LtB;

.field private final a:LtK;


# direct methods
.method public constructor <init>(LadE;Lady;LaGg;Lamn;Landroid/content/Context;LamL;Lalo;LQr;LaKR;Lamx;LahT;LMs;LtK;LbiP;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LadE;",
            "Lady;",
            "LaGg;",
            "Lamn;",
            "Landroid/content/Context;",
            "LamL;",
            "Lalo;",
            "LQr;",
            "LaKR;",
            "Lamx;",
            "LahT;",
            "LMs;",
            "LtK;",
            "LbiP",
            "<",
            "LtB;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 487
    .line 500
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v14

    .line 501
    invoke-virtual/range {p14 .. p14}, LbiP;->b()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, LtB;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    .line 487
    invoke-direct/range {v0 .. v15}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;-><init>(LadE;Lady;LaGg;Lamn;Landroid/content/Context;LamL;Lalo;LQr;LaKR;Lamx;LahT;LMs;LtK;Ljava/util/concurrent/ExecutorService;LtB;)V

    .line 502
    return-void
.end method

.method protected constructor <init>(LadE;Lady;LaGg;Lamn;Landroid/content/Context;LamL;Lalo;LQr;LaKR;Lamx;LahT;LMs;LtK;Ljava/util/concurrent/ExecutorService;LtB;)V
    .locals 5

    .prologue
    .line 520
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 521
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Ljava/util/Map;

    .line 522
    iput-object p2, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Lady;

    .line 523
    iput-object p3, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    .line 524
    iput-object p4, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Lamn;

    .line 525
    iput-object p5, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Landroid/content/Context;

    .line 526
    iput-object p6, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LamL;

    .line 527
    iput-object p7, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Lalo;

    .line 528
    iput-object p1, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LadE;

    .line 530
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:J

    .line 531
    new-instance v2, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v2}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Ljava/util/concurrent/locks/Lock;

    .line 532
    iput-object p9, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaKR;

    .line 533
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LahT;

    .line 534
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LMs;

    .line 535
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LtK;

    .line 536
    invoke-static/range {p14 .. p14}, LbsY;->a(Ljava/util/concurrent/ExecutorService;)LbsW;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LbsW;

    .line 537
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LtB;

    .line 539
    const-string v2, "contentGcRateMilliseconds"

    const/16 v3, 0x1388

    invoke-interface {p8, v2, v3}, LQr;->a(Ljava/lang/String;I)I

    move-result v2

    .line 542
    new-instance v3, Ladl;

    invoke-direct {v3, p0}, Ladl;-><init>(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)V

    .line 549
    const-string v4, "DocumentFileManager:runGc()"

    .line 550
    move-object/from16 v0, p14

    invoke-interface {p10, v3, v2, v0, v4}, Lamx;->a(Ljava/lang/Runnable;ILjava/util/concurrent/Executor;Ljava/lang/String;)Lamw;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Lamw;

    .line 551
    return-void
.end method

.method private declared-synchronized a(LaGp;)J
    .locals 8

    .prologue
    .line 1288
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(LaGp;)Ladw;

    move-result-object v2

    .line 1289
    invoke-virtual {v2}, Ladw;->a()Ladz;

    move-result-object v0

    sget-object v1, Ladz;->c:Ladz;

    if-eq v0, v1, :cond_0

    .line 1290
    const-string v0, "DocumentFileManager"

    const-string v1, "Content instance in use: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1291
    const-wide/16 v0, 0x0

    .line 1301
    :goto_0
    monitor-exit p0

    return-wide v0

    .line 1294
    :cond_0
    :try_start_1
    invoke-virtual {v2}, Ladw;->a()J

    move-result-wide v0

    .line 1296
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(LaGp;)V

    .line 1297
    invoke-virtual {v2}, Ladw;->e()V

    .line 1298
    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->c(Ladw;)V

    .line 1300
    const-string v2, "DocumentFileManager"

    const-string v3, "Deleting content instance: %s size: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p1}, LaGp;->c()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1288
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LMs;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LMs;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LaGg;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    return-object v0
.end method

.method private a(LaGp;)LaGp;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1587
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, p1

    .line 1588
    :goto_0
    if-eqz v0, :cond_0

    .line 1589
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->b(LaGp;)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object v1, v0

    .line 1595
    :cond_0
    return-object v1

    .line 1592
    :cond_1
    invoke-virtual {v0}, LaGp;->a()Ljava/lang/Long;

    move-result-object v0

    .line 1593
    if-nez v0, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v2, v4, v5}, LaGg;->a(J)LaGp;

    move-result-object v0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;LaGp;)LaGp;
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(LaGp;)LaGp;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LadE;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LadE;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Lcom/google/android/gms/drive/database/data/EntrySpec;LacY;Ladw;)Ladj;
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LacY;Ladw;)Ladj;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized a(Lcom/google/android/gms/drive/database/data/EntrySpec;LacY;Ladw;)Ladj;
    .locals 6

    .prologue
    .line 1077
    monitor-enter p0

    :try_start_0
    new-instance v0, Ladq;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Ladq;-><init>(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Lcom/google/android/gms/drive/database/data/EntrySpec;Ladw;LacY;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(LaGp;)Ladw;
    .locals 4

    .prologue
    .line 1305
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Ljava/util/Map;

    invoke-virtual {p1}, LaGp;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladw;

    .line 1306
    if-nez v0, :cond_0

    .line 1307
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Lady;

    invoke-virtual {v0, p1}, Lady;->a(LaGp;)Ladw;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1309
    :cond_0
    monitor-exit p0

    return-object v0

    .line 1305
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LahT;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LahT;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)Lalo;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Lalo;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Landroid/content/Context;

    return-object v0
.end method

.method private a(LadY;Ljava/io/File;Ljavax/crypto/SecretKey;J)LbsU;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LadY;",
            "Ljava/io/File;",
            "Ljavax/crypto/SecretKey;",
            "J)",
            "LbsU",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 847
    invoke-virtual {p1}, LadY;->b()Z

    move-result v0

    invoke-virtual {p2}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 848
    iget-object v7, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LbsW;

    new-instance v0, Ladm;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p1

    move-wide v4, p4

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Ladm;-><init>(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Ljavax/crypto/SecretKey;LadY;JLjava/io/File;)V

    invoke-interface {v7, v0}, LbsW;->a(Ljava/util/concurrent/Callable;)LbsU;

    move-result-object v0

    return-object v0

    .line 847
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized a(Ladw;Ladw;Lamr;)LbsU;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ladw;",
            "Ladw;",
            "Lamr;",
            ")",
            "LbsU",
            "<",
            "Ladw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1168
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 1169
    invoke-virtual {p1}, Ladw;->b()Z

    move-result v0

    invoke-static {v0}, LbiT;->a(Z)V

    .line 1170
    invoke-virtual {p2}, Ladw;->c()Z

    move-result v0

    invoke-static {v0}, LbiT;->a(Z)V

    .line 1172
    new-instance v0, Ladp;

    invoke-direct {v0, p0, p1, p2, p3}, Ladp;-><init>(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Ladw;Ladw;Lamr;)V

    .line 1258
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Ladw;)V

    .line 1259
    invoke-direct {p0, p2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->b(Ladw;)V

    .line 1260
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LbsW;

    invoke-interface {v1, v0}, LbsW;->a(Ljava/util/concurrent/Callable;)LbsU;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 1168
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lcom/google/android/gms/drive/database/data/EntrySpec;LacY;Ladw;Lamr;)LbsU;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            "LacY;",
            "Ladw;",
            "Lamr;",
            ")",
            "LbsU",
            "<",
            "Ladj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1087
    monitor-enter p0

    :try_start_0
    invoke-virtual {p3}, Ladw;->a()LaGp;

    move-result-object v6

    .line 1088
    invoke-virtual {v6}, LaGp;->b()LadY;

    move-result-object v0

    .line 1089
    invoke-virtual {v0}, LadY;->b()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 1092
    if-eqz v1, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LadE;

    invoke-interface {v0}, LadE;->b()Ljava/io/File;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 1097
    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    .line 1098
    invoke-virtual {v6}, LaGp;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LaGg;->a(Ljava/lang/String;)LaGr;

    move-result-object v1

    .line 1099
    invoke-virtual {v1, v0}, LaGr;->b(Ljava/io/File;)LaGr;

    move-result-object v0

    .line 1100
    invoke-virtual {v0, v6}, LaGr;->a(LaGp;)LaGr;

    move-result-object v0

    .line 1101
    invoke-virtual {v0}, LaGr;->b()LaGp;

    move-result-object v5

    .line 1102
    invoke-direct {p0, v5}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(LaGp;)Ladw;

    move-result-object v2

    .line 1104
    new-instance v0, Lado;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v6}, Lado;-><init>(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Ladw;Lcom/google/android/gms/drive/database/data/EntrySpec;LacY;LaGp;LaGp;)V

    .line 1156
    invoke-direct {p0, p3, v2, p4}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Ladw;Ladw;Lamr;)LbsU;

    move-result-object v1

    .line 1158
    invoke-static {v1, v0}, LbsK;->a(LbsU;LbiG;)LbsU;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    :goto_1
    monitor-exit p0

    return-object v0

    .line 1092
    :cond_0
    :try_start_3
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LadE;

    .line 1093
    invoke-virtual {v0}, LadY;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, LadE;->b(Ljava/lang/String;)Ljava/io/File;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 1094
    :catch_0
    move-exception v0

    .line 1095
    :try_start_4
    invoke-static {v0}, LbsK;->a(Ljava/lang/Throwable;)LbsU;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v0

    goto :goto_1

    .line 1087
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Ljava/util/Map;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;)LtK;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LtK;

    return-object v0
.end method

.method private declared-synchronized a(LaGo;Ladw;Ljava/util/Date;)V
    .locals 9

    .prologue
    .line 794
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 795
    invoke-virtual {p2}, Ladw;->b()Z

    move-result v0

    invoke-static {v0}, LbiT;->a(Z)V

    .line 796
    invoke-virtual {p2}, Ladw;->a()LaGp;

    move-result-object v0

    .line 797
    invoke-virtual {v0}, LaGp;->a()Ljavax/crypto/SecretKey;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LadE;

    invoke-interface {v1}, LadE;->a()Ljavax/crypto/SecretKey;

    move-result-object v3

    .line 800
    :goto_0
    const/4 v6, 0x0

    .line 801
    invoke-direct {p0, p2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Ladw;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 803
    :try_start_1
    invoke-virtual {p2}, Ladw;->a()LadY;

    move-result-object v1

    .line 805
    invoke-virtual {v1}, LadY;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LadE;

    invoke-interface {v2}, LadE;->a()Ljava/io/File;

    move-result-object v2

    .line 808
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Lalo;

    invoke-interface {v4, v1}, Lalo;->a(LadY;)J

    move-result-wide v4

    .line 809
    iget-object v7, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    .line 810
    invoke-virtual {v0}, LaGp;->c()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, LaGg;->a(Ljava/lang/String;)LaGr;

    move-result-object v7

    .line 811
    invoke-virtual {v7, v2, v3}, LaGr;->a(Ljava/io/File;Ljavax/crypto/SecretKey;)LaGr;

    move-result-object v7

    .line 812
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, LaGr;->b(Ljava/lang/Long;)LaGr;

    move-result-object v7

    .line 813
    invoke-virtual {v7, v0}, LaGr;->a(LaGp;)LaGr;

    move-result-object v0

    .line 814
    if-eqz p3, :cond_0

    .line 816
    invoke-virtual {v0, p3}, LaGr;->a(Ljava/util/Date;)LaGr;

    .line 818
    :cond_0
    invoke-virtual {v0}, LaGr;->b()LaGp;

    move-result-object v0

    .line 820
    iget-object v7, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Lady;

    invoke-virtual {v7, v0}, Lady;->a(LaGp;)Ladw;

    move-result-object v7

    .line 821
    invoke-direct {p0, v7}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->b(Ladw;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 823
    :try_start_2
    new-instance v8, Lads;

    .line 824
    invoke-interface {p1}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-direct {v8, p0, v0, p2, v7}, Lads;-><init>(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Lcom/google/android/gms/drive/database/data/EntrySpec;Ladw;Ladw;)V

    move-object v0, p0

    .line 826
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(LadY;Ljava/io/File;Ljavax/crypto/SecretKey;J)LbsU;

    move-result-object v0

    .line 829
    invoke-static {v0, v8}, LbsK;->a(LbsU;LbsJ;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 830
    const/4 v1, 0x1

    .line 832
    :try_start_3
    invoke-virtual {p2, v0}, Ladw;->a(LbsU;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 843
    monitor-exit p0

    return-void

    .line 798
    :cond_1
    :try_start_4
    invoke-virtual {v0}, LaGp;->a()Ljavax/crypto/SecretKey;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result-object v3

    goto :goto_0

    .line 805
    :cond_2
    :try_start_5
    iget-object v2, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LadE;

    .line 806
    invoke-virtual {v1}, LadY;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, LadE;->a(Ljava/lang/String;)Ljava/io/File;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    move-result-object v2

    goto :goto_1

    .line 834
    :catchall_0
    move-exception v0

    move v1, v6

    :goto_2
    if-nez v1, :cond_3

    .line 835
    :try_start_6
    invoke-direct {p0, v7}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->c(Ladw;)V

    :cond_3
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 839
    :catchall_1
    move-exception v0

    move v6, v1

    :goto_3
    if-nez v6, :cond_4

    .line 840
    :try_start_7
    invoke-direct {p0, p2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->c(Ladw;)V

    :cond_4
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 794
    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0

    .line 839
    :catchall_3
    move-exception v0

    goto :goto_3

    .line 834
    :catchall_4
    move-exception v0

    goto :goto_2
.end method

.method private a(LaGp;)V
    .locals 4

    .prologue
    .line 1066
    invoke-virtual {p1}, LaGp;->a()Ljava/lang/Long;

    move-result-object v0

    .line 1067
    if-eqz v0, :cond_0

    .line 1068
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, LaGg;->a(J)LaGp;

    move-result-object v0

    .line 1069
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    invoke-interface {v1, p1, v0}, LaGg;->a(LaGp;LaGp;)V

    .line 1073
    :goto_0
    return-void

    .line 1071
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    invoke-interface {v0, p1}, LaGg;->a(LaGp;)V

    goto :goto_0
.end method

.method private declared-synchronized a(Ladw;)V
    .locals 3

    .prologue
    .line 1313
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ladw;->a()LaGp;

    move-result-object v0

    invoke-virtual {v0}, LaGp;->c()J

    move-result-wide v0

    .line 1314
    invoke-virtual {p1}, Ladw;->a()V

    .line 1315
    iget-object v2, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1316
    monitor-exit p0

    return-void

    .line 1313
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;LaGo;Ladw;Ljava/util/Date;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(LaGo;Ladw;Ljava/util/Date;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Ladw;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->c(Ladw;)V

    return-void
.end method

.method private a(Ljava/io/File;)V
    .locals 6

    .prologue
    .line 1514
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 1515
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    .line 1516
    iget-object v5, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    invoke-interface {v5, v4}, LaGg;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1517
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1518
    invoke-direct {p0, v3}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Ljava/io/File;)V

    .line 1514
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1520
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LadE;

    invoke-interface {v4, v3}, LadE;->a(Ljava/io/File;)Z

    goto :goto_1

    .line 1524
    :cond_2
    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    .line 1381
    invoke-direct {p0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->d()V

    .line 1384
    if-eqz p1, :cond_2

    .line 1386
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1397
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->e()V

    .line 1398
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->b(Z)V

    .line 1400
    if-eqz p1, :cond_1

    .line 1402
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Lamn;

    invoke-interface {v0}, Lamn;->b()Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Ljava/io/File;)V

    .line 1403
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Lamn;

    invoke-interface {v0}, Lamn;->a()Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Ljava/io/File;)V

    .line 1404
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Lamn;

    invoke-interface {v0}, Lamn;->c()Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Ljava/io/File;)V

    .line 1405
    invoke-direct {p0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1408
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1411
    monitor-enter p0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1412
    :try_start_3
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LadE;

    .line 1413
    invoke-interface {v0}, LadE;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LadE;

    invoke-interface {v2}, LadE;->b()J

    move-result-wide v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:J

    .line 1414
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1421
    :goto_0
    return-void

    .line 1390
    :cond_2
    :try_start_4
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->tryLock()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1391
    const-string v0, "DocumentFileManager"

    const-string v1, "Garbage collection already in progress."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    .line 1415
    :catch_0
    move-exception v0

    .line 1418
    const-string v1, "DocumentFileManager"

    const-string v2, "Garbage collection failed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1419
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:J

    goto :goto_0

    .line 1408
    :catchall_0
    move-exception v0

    :try_start_5
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    .line 1414
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
.end method

.method private a(LaGo;LaGp;)Z
    .locals 4

    .prologue
    .line 785
    invoke-virtual {p2}, LaGp;->a()Ljava/lang/Long;

    move-result-object v0

    .line 786
    invoke-virtual {p2}, LaGp;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 787
    invoke-virtual {p2}, LaGp;->d()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p2}, LaGp;->e()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-interface {p1}, LaGo;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz v0, :cond_1

    .line 788
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized a(LaGp;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 748
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Ljava/util/Map;

    invoke-virtual {p1}, LaGp;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladw;

    .line 749
    if-eqz v0, :cond_1

    .line 750
    invoke-virtual {v0}, Ladw;->a()Ladz;

    move-result-object v3

    sget-object v4, Ladz;->c:Ladz;

    if-eq v3, v4, :cond_0

    .line 751
    invoke-virtual {v0}, Ladw;->a()Ladz;

    move-result-object v3

    sget-object v4, Ladz;->a:Ladz;

    if-ne v3, v4, :cond_3

    invoke-virtual {p1}, LaGp;->f()Z

    move-result v3

    if-nez v3, :cond_3

    .line 752
    :cond_0
    invoke-virtual {v0}, Ladw;->e()Z

    move-result v3

    if-nez v3, :cond_3

    :cond_1
    move v3, v2

    .line 753
    :goto_0
    invoke-virtual {p1}, LaGp;->a()Z

    move-result v4

    if-eqz v4, :cond_5

    if-eqz v3, :cond_5

    .line 754
    if-nez v0, :cond_2

    .line 755
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Lady;

    invoke-virtual {v0, p1}, Lady;->a(LaGp;)Ladw;

    move-result-object v0

    .line 757
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    invoke-interface {v3, p1}, LaGg;->a(LaGp;)LaGb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 758
    if-nez v3, :cond_4

    move v0, v1

    .line 776
    :goto_1
    monitor-exit p0

    return v0

    :cond_3
    move v3, v1

    .line 752
    goto :goto_0

    .line 762
    :cond_4
    :try_start_1
    invoke-direct {p0, v3, p1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(LaGo;LaGp;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-eqz v4, :cond_5

    .line 764
    :try_start_2
    invoke-interface {v3, p1}, LaGo;->a(LaGp;)LacY;

    move-result-object v4

    .line 765
    if-eqz v4, :cond_5

    sget-object v5, LacY;->a:LacY;

    invoke-virtual {v4, v5}, LacY;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 766
    const/4 v4, 0x0

    invoke-direct {p0, v3, v0, v4}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(LaGo;Ladw;Ljava/util/Date;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch LQm; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v2

    .line 767
    goto :goto_1

    .line 769
    :catch_0
    move-exception v0

    .line 770
    :try_start_3
    const-string v2, "DocumentFileManager"

    const-string v3, "Got an exception when trying to page cached file content to sdcard: "

    invoke-static {v2, v0, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    :cond_5
    :goto_2
    move v0, v1

    .line 776
    goto :goto_1

    .line 771
    :catch_1
    move-exception v0

    .line 772
    const-string v2, "DocumentFileManager"

    const-string v3, "Got an exception when trying to page cached file content to sdcard: "

    invoke-static {v2, v0, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 748
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;LaGo;LaGp;)Z
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->b(LaGo;LaGp;)Z

    move-result v0

    return v0
.end method

.method private declared-synchronized b(LaGo;Ljava/lang/String;Ljava/lang/String;LacY;Ljava/lang/String;)Ladj;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 701
    monitor-enter p0

    :try_start_0
    invoke-static {p2}, LaGp;->a(Ljava/lang/String;)Z

    move-result v3

    .line 702
    if-nez p1, :cond_0

    if-nez p5, :cond_0

    if-eqz v3, :cond_4

    :cond_0
    move v2, v0

    :goto_0
    invoke-static {v2}, LbiT;->a(Z)V

    .line 703
    if-eqz p5, :cond_1

    if-nez v3, :cond_5

    :cond_1
    move v2, v0

    :goto_1
    invoke-static {v2}, LbiT;->a(Z)V

    .line 706
    if-nez v3, :cond_6

    if-eqz p1, :cond_3

    sget-object v2, LacY;->a:LacY;

    .line 707
    invoke-virtual {v2, p4}, LacY;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, LaGo;->f()Z

    move-result v2

    if-nez v2, :cond_3

    .line 708
    :cond_2
    invoke-interface {p1}, LaGo;->b()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 709
    :cond_3
    :goto_2
    if-eqz v0, :cond_7

    iget-object v1, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LamL;

    invoke-interface {v1}, LamL;->b()Z

    move-result v1

    if-nez v1, :cond_7

    .line 710
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "External storage not ready for writing pinned file:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 701
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    move v2, v1

    .line 702
    goto :goto_0

    :cond_5
    move v2, v1

    .line 703
    goto :goto_1

    :cond_6
    move v0, v1

    .line 708
    goto :goto_2

    .line 713
    :cond_7
    if-nez p5, :cond_8

    if-nez v3, :cond_8

    .line 714
    :try_start_1
    invoke-interface {p1}, LaGo;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p5

    .line 717
    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    .line 718
    invoke-interface {v1, p2}, LaGg;->a(Ljava/lang/String;)LaGr;

    move-result-object v1

    invoke-virtual {v1, p3}, LaGr;->b(Ljava/lang/String;)LaGr;

    move-result-object v1

    .line 719
    if-eqz v0, :cond_b

    .line 720
    if-eqz v3, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LadE;

    invoke-interface {v0}, LadE;->a()Ljava/io/File;

    move-result-object v0

    .line 722
    :goto_3
    iget-object v2, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LadE;

    invoke-interface {v2}, LadE;->a()Ljavax/crypto/SecretKey;

    move-result-object v2

    .line 723
    invoke-virtual {v1, v0, v2}, LaGr;->a(Ljava/io/File;Ljavax/crypto/SecretKey;)LaGr;

    .line 730
    :goto_4
    if-eqz p1, :cond_9

    .line 731
    invoke-interface {p1}, LaGo;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LaGr;->a(Ljava/lang/String;)LaGr;

    .line 732
    invoke-interface {p1}, LaGo;->b()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v1, v0}, LaGr;->a(Ljava/util/Date;)LaGr;

    .line 734
    :cond_9
    invoke-virtual {v1}, LaGr;->b()LaGp;

    move-result-object v0

    .line 736
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Lady;

    invoke-virtual {v1, v0}, Lady;->a(LaGp;)Ladw;

    move-result-object v3

    .line 737
    invoke-direct {p0, v3}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->b(Ladw;)V

    .line 739
    if-nez p1, :cond_d

    const/4 v2, 0x0

    .line 740
    :goto_5
    new-instance v0, Ladq;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Ladq;-><init>(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Lcom/google/android/gms/drive/database/data/EntrySpec;Ladw;LacY;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0

    .line 720
    :cond_a
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LadE;

    .line 721
    invoke-interface {v0, p5}, LadE;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    goto :goto_3

    .line 725
    :cond_b
    if-eqz v3, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LadE;

    .line 726
    invoke-interface {v0}, LadE;->b()Ljava/io/File;

    move-result-object v0

    .line 728
    :goto_6
    invoke-virtual {v1, v0}, LaGr;->b(Ljava/io/File;)LaGr;

    goto :goto_4

    .line 726
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LadE;

    .line 727
    invoke-interface {v0, p5}, LadE;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    goto :goto_6

    .line 739
    :cond_d
    invoke-interface {p1}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_5
.end method

.method private declared-synchronized b(Ladw;)V
    .locals 3

    .prologue
    .line 1319
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ladw;->a()LaGp;

    move-result-object v0

    invoke-virtual {v0}, LaGp;->c()J

    move-result-wide v0

    .line 1320
    invoke-virtual {p1}, Ladw;->b()V

    .line 1321
    iget-object v2, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1322
    monitor-exit p0

    return-void

    .line 1319
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(Z)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1461
    const-wide v2, 0x7fffffffffffffffL

    .line 1464
    if-nez p1, :cond_5

    .line 1471
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LadE;

    invoke-interface {v0}, LadE;->a()J

    move-result-wide v2

    .line 1472
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LadE;

    invoke-interface {v0}, LadE;->b()J

    move-result-wide v4

    .line 1473
    cmp-long v0, v4, v2

    if-gtz v0, :cond_1

    .line 1474
    const-string v0, "DocumentFileManager"

    const-string v6, "Garbage collection skipped %s/%s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v7, v1

    const/4 v1, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v7, v1

    invoke-static {v0, v6, v7}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1511
    :cond_0
    :goto_0
    return-void

    .line 1479
    :cond_1
    sub-long v2, v4, v2

    .line 1481
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LadE;

    invoke-interface {v0}, LadE;->a()I

    move-result v0

    .line 1484
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    invoke-interface {v4}, LaGg;->a()Landroid/database/Cursor;

    move-result-object v4

    .line 1485
    if-eqz v4, :cond_0

    .line 1490
    :try_start_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-nez v5, :cond_2

    .line 1509
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1494
    :cond_2
    :try_start_1
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v5

    .line 1495
    :goto_2
    sub-int v6, v5, v0

    if-ge v1, v6, :cond_3

    .line 1496
    iget-object v6, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    invoke-interface {v6, v4}, LaGg;->a(Landroid/database/Cursor;)LaGp;

    move-result-object v6

    .line 1497
    invoke-direct {p0, v6}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(LaGp;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v6

    .line 1498
    sub-long/2addr v2, v6

    .line 1499
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-gtz v6, :cond_4

    .line 1509
    :cond_3
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1504
    :cond_4
    :try_start_2
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v6

    if-eqz v6, :cond_3

    .line 1495
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1509
    :catchall_0
    move-exception v0

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method private b(LaGo;LaGp;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 997
    invoke-virtual {p2}, LaGp;->f()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1006
    :cond_0
    :goto_0
    return v0

    .line 1004
    :cond_1
    invoke-virtual {p2}, LaGp;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1005
    invoke-virtual {p2}, LaGp;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LtB;

    invoke-interface {v1}, LtB;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1006
    :cond_2
    invoke-interface {p1}, LaGo;->b()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized b(LaGp;)Z
    .locals 4

    .prologue
    .line 1424
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Ljava/util/Map;

    invoke-virtual {p1}, LaGp;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c(Ladw;)V
    .locals 3

    .prologue
    .line 1563
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ladw;->c()V

    .line 1564
    invoke-virtual {p1}, Ladw;->a()Ladz;

    move-result-object v0

    sget-object v1, Ladz;->c:Ladz;

    if-ne v0, v1, :cond_0

    .line 1565
    invoke-virtual {p1}, Ladw;->a()LaGp;

    move-result-object v0

    invoke-virtual {v0}, LaGp;->c()J

    move-result-wide v0

    .line 1566
    iget-object v2, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1568
    :cond_0
    monitor-exit p0

    return-void

    .line 1563
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 1365
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    invoke-interface {v0}, LaGg;->a()Landroid/database/Cursor;

    move-result-object v1

    .line 1366
    if-nez v1, :cond_0

    .line 1378
    :goto_0
    return-void

    .line 1371
    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1372
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    invoke-interface {v0, v1}, LaGg;->a(Landroid/database/Cursor;)LaGp;

    move-result-object v0

    .line 1373
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(LaGp;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1376
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private declared-synchronized e()V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 1430
    monitor-enter p0

    move v3, v2

    .line 1432
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    invoke-interface {v0}, LaGg;->a()Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1433
    if-nez v0, :cond_1

    .line 1453
    :cond_0
    monitor-exit p0

    return-void

    .line 1438
    :cond_1
    :try_start_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaGp;

    .line 1439
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->b(LaGp;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1442
    const-string v5, "DocumentFileManager"

    const-string v6, "Not garbage collecting locked instance: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v0}, LaGp;->c()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v7, v8

    invoke-static {v5, v6, v7}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1430
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1447
    :cond_2
    :try_start_2
    const-string v5, "DocumentFileManager"

    const-string v6, "GC stage %s - Deleting instance: %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-virtual {v0}, LaGp;->c()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1448
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(LaGp;)Ladw;

    move-result-object v0

    invoke-virtual {v0}, Ladw;->e()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1449
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 1450
    goto :goto_1

    .line 1452
    :cond_3
    if-eqz v1, :cond_0

    .line 1456
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    .line 1457
    goto :goto_0
.end method

.method private e(LaGo;LacY;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 566
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaKR;

    invoke-interface {v1}, LaKR;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 575
    :cond_0
    :goto_0
    return v0

    .line 570
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    invoke-interface {v1, p1, p2}, LaGg;->a(LaGo;LacY;)LaGp;

    move-result-object v1

    .line 571
    if-eqz v1, :cond_2

    invoke-virtual {v1}, LaGp;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 575
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->f(LaGo;LacY;)Z

    move-result v0

    goto :goto_0
.end method

.method private f()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1527
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Lamn;

    invoke-interface {v0}, Lamn;->d()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 1528
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v5

    .line 1529
    if-eqz v5, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, LaGg;->b(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1530
    :cond_0
    invoke-static {v4}, Lalp;->b(Ljava/io/File;)Z

    .line 1531
    if-nez v5, :cond_1

    .line 1532
    const-string v5, "DocumentFileManager"

    const-string v6, "Deleted file probably placed in the metadata directory by mistake: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    .line 1533
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v1

    .line 1532
    invoke-static {v5, v6, v7}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1527
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1537
    :cond_2
    return-void
.end method

.method private f(LaGo;LacY;)Z
    .locals 4

    .prologue
    .line 579
    invoke-interface {p1, p2}, LaGo;->a(LacY;)J

    move-result-wide v0

    .line 582
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    const/4 v0, 0x0

    .line 587
    :goto_0
    if-eqz v0, :cond_1

    .line 588
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->b(LaGp;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 589
    :goto_1
    return v0

    .line 582
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    .line 583
    invoke-interface {v2, v0, v1}, LaGg;->a(J)LaGp;

    move-result-object v0

    goto :goto_0

    .line 588
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public declared-synchronized a(LaGo;)Ladj;
    .locals 6

    .prologue
    .line 681
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 683
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    .line 684
    invoke-interface {p1}, LaGo;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LaGg;->a(Ljava/lang/String;)LaGr;

    move-result-object v0

    .line 686
    invoke-interface {p1}, LaGo;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 687
    iget-object v2, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LadE;

    invoke-interface {v2, v1}, LadE;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 688
    invoke-virtual {v0, v1}, LaGr;->b(Ljava/io/File;)LaGr;

    .line 689
    invoke-virtual {v0}, LaGr;->b()LaGp;

    move-result-object v0

    .line 691
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Lady;

    invoke-virtual {v1, v0}, Lady;->a(LaGp;)Ladw;

    move-result-object v3

    .line 692
    invoke-direct {p0, v3}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->b(Ladw;)V

    .line 694
    invoke-interface {p1}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    .line 695
    new-instance v0, Ladq;

    sget-object v4, LacY;->a:LacY;

    const/4 v5, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ladq;-><init>(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Lcom/google/android/gms/drive/database/data/EntrySpec;Ladw;LacY;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 681
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(LaGo;Ljava/lang/String;Ljava/lang/String;LacY;Ljava/lang/String;)Ladj;
    .locals 1

    .prologue
    .line 665
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 667
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->b(LaGo;Ljava/lang/String;Ljava/lang/String;LacY;Ljava/lang/String;)Ladj;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 665
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/google/android/apps/docs/sync/filemanager/FileSpec;)Ladj;
    .locals 6

    .prologue
    .line 1573
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Ljava/util/Map;

    iget-wide v2, p1, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ladw;

    .line 1574
    if-nez v3, :cond_0

    .line 1575
    const/4 v0, 0x0

    .line 1577
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ladq;

    iget-object v2, p1, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v4, p1, Lcom/google/android/apps/docs/sync/filemanager/FileSpec;->a:LacY;

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ladq;-><init>(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Lcom/google/android/gms/drive/database/data/EntrySpec;Ladw;LacY;Z)V

    goto :goto_0
.end method

.method public declared-synchronized a(Ljava/lang/String;Ljava/io/File;)Ladj;
    .locals 6

    .prologue
    .line 627
    monitor-enter p0

    :try_start_0
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 629
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    .line 630
    invoke-interface {v0, p1}, LaGg;->a(Ljava/lang/String;)LaGr;

    move-result-object v0

    invoke-virtual {v0, p2}, LaGr;->a(Ljava/io/File;)LaGr;

    move-result-object v0

    .line 631
    invoke-virtual {p2}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, LaGr;->b(Ljava/lang/Long;)LaGr;

    .line 632
    invoke-virtual {v0}, LaGr;->b()LaGp;

    move-result-object v0

    .line 634
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Lady;

    invoke-virtual {v1, v0}, Lady;->a(LaGp;)Ladw;

    move-result-object v3

    .line 635
    invoke-direct {p0, v3}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Ladw;)V

    .line 637
    new-instance v0, Ladq;

    const/4 v2, 0x0

    sget-object v4, LacY;->a:LacY;

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ladq;-><init>(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Lcom/google/android/gms/drive/database/data/EntrySpec;Ladw;LacY;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 627
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)Ladj;
    .locals 6

    .prologue
    .line 673
    monitor-enter p0

    :try_start_0
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 674
    invoke-static {p2}, Lalp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 675
    const/4 v1, 0x0

    const/4 v3, 0x0

    sget-object v4, LacY;->a:LacY;

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->b(LaGo;Ljava/lang/String;Ljava/lang/String;LacY;Ljava/lang/String;)Ladj;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 673
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(LaGo;LacY;)LbsU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGo;",
            "LacY;",
            ")",
            "LbsU",
            "<",
            "Ladj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1013
    monitor-enter p0

    :try_start_0
    sget-object v0, Ladk;->a:Ladk;

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(LaGo;LacY;Lamr;)LbsU;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(LaGo;LacY;Lamr;)LbsU;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGo;",
            "LacY;",
            "Lamr;",
            ")",
            "LbsU",
            "<",
            "Ladj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1019
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(LaGo;LacY;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1020
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "Failed to find document file."

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LbsK;->a(Ljava/lang/Throwable;)LbsU;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1025
    :goto_0
    monitor-exit p0

    return-object v0

    .line 1023
    :cond_0
    :try_start_1
    invoke-interface {p1, p2}, LaGo;->a(LacY;)J

    move-result-wide v0

    .line 1024
    iget-object v2, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    invoke-interface {v2, v0, v1}, LaGg;->a(J)LaGp;

    move-result-object v0

    .line 1025
    invoke-virtual {p0, v0, p2, p3, p1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(LaGp;LacY;Lamr;LaGo;)LbsU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 1019
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(LaGp;LacY;Lamr;LaGo;)LbsU;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaGp;",
            "LacY;",
            "Lamr;",
            "LaGo;",
            ")",
            "LbsU",
            "<",
            "Ladj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1032
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1033
    if-nez p4, :cond_0

    .line 1034
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    invoke-interface {v0, p1}, LaGg;->a(LaGp;)LaGo;

    move-result-object p4

    .line 1036
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(LaGp;)Ladw;

    move-result-object v0

    .line 1037
    invoke-virtual {v0}, Ladw;->d()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1038
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "Failed to find document file."

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LbsK;->a(Ljava/lang/Throwable;)LbsU;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1060
    :goto_0
    monitor-exit p0

    return-object v0

    .line 1042
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    invoke-interface {v1, p1}, LaGg;->a(LaGp;)LaGp;

    move-result-object v1

    .line 1043
    if-nez v1, :cond_2

    .line 1045
    invoke-interface {p4}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    .line 1044
    invoke-direct {p0, v1, p2, v0, p3}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LacY;Ladw;Lamr;)LbsU;

    move-result-object v0

    goto :goto_0

    .line 1047
    :cond_2
    invoke-virtual {v1}, LaGp;->a()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1048
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ModelLoader.findTemporaryContent returned main content:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1032
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1051
    :cond_3
    :try_start_2
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(LaGp;)Ladw;

    move-result-object v2

    .line 1052
    invoke-virtual {v2}, Ladw;->d()Z

    move-result v3

    if-nez v3, :cond_4

    .line 1053
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(LaGp;)V

    .line 1055
    invoke-interface {p4}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    .line 1054
    invoke-direct {p0, v1, p2, v0, p3}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LacY;Ladw;Lamr;)LbsU;

    move-result-object v0

    goto :goto_0

    .line 1057
    :cond_4
    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Ladw;)V

    .line 1059
    invoke-interface {p4}, LaGo;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 1058
    invoke-direct {p0, v0, p2, v2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;LacY;Ladw;)Ladj;

    move-result-object v0

    .line 1060
    invoke-static {v0}, LbsK;->a(Ljava/lang/Object;)LbsU;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    goto :goto_0
.end method

.method public declared-synchronized a(Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LaFS;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ladj;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1266
    monitor-enter p0

    :try_start_0
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1267
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaFS;

    .line 1268
    iget-object v4, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    invoke-virtual {v0}, LaFS;->a()J

    move-result-wide v6

    invoke-interface {v4, v6, v7}, LaGg;->a(J)LaGp;

    move-result-object v0

    .line 1269
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(LaGp;)Ladw;

    move-result-object v0

    .line 1270
    invoke-virtual {v0}, Ladw;->d()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1271
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1266
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    move-object v0, v1

    .line 1283
    :goto_1
    monitor-exit p0

    return-object v0

    .line 1276
    :cond_1
    :try_start_1
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 1277
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ladw;

    .line 1278
    invoke-direct {p0, v3}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Ladw;)V

    .line 1279
    new-instance v0, Ladq;

    const/4 v2, 0x0

    sget-object v4, LacY;->a:LacY;

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ladq;-><init>(Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;Lcom/google/android/gms/drive/database/data/EntrySpec;Ladw;LacY;Z)V

    .line 1281
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :cond_2
    move-object v0, v6

    .line 1283
    goto :goto_1
.end method

.method public a()V
    .locals 2

    .prologue
    .line 1347
    const-string v0, "DocumentFileManager"

    const-string v1, "GC started"

    invoke-static {v0, v1}, LalV;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 1348
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Z)V

    .line 1349
    const-string v0, "DocumentFileManager"

    const-string v1, "GC completed"

    invoke-static {v0, v1}, LalV;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 1350
    return-void
.end method

.method public a(J)V
    .locals 7

    .prologue
    .line 1329
    const/4 v0, 0x0

    .line 1330
    monitor-enter p0

    .line 1331
    :try_start_0
    iget-wide v2, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:J

    sub-long/2addr v2, p1

    iput-wide v2, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:J

    .line 1332
    iget-wide v2, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 1333
    const/4 v0, 0x1

    .line 1335
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1337
    if-eqz v0, :cond_1

    .line 1338
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->b()V

    .line 1340
    :cond_1
    return-void

    .line 1335
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public declared-synchronized a(LaGo;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 612
    monitor-enter p0

    :try_start_0
    sget-object v1, LacY;->a:LacY;

    invoke-interface {p1, v1}, LaGo;->a(LacY;)J

    move-result-wide v2

    .line 613
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    invoke-interface {v1, v2, v3}, LaGg;->a(J)LaGp;

    move-result-object v1

    .line 614
    if-eqz v1, :cond_0

    invoke-virtual {v1}, LaGp;->f()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 621
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 617
    :cond_1
    :try_start_1
    invoke-interface {p1}, LaGo;->f()Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, LacY;->a:LacY;

    invoke-virtual {p0, p1, v2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(LaGo;LacY;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 620
    :cond_2
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(LaGp;)Ladw;

    move-result-object v0

    .line 621
    invoke-virtual {v0}, Ladw;->d()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 612
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(LaGo;LacY;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 555
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 556
    if-eqz p2, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 558
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->d(LaGo;LacY;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 559
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->e(LaGo;LacY;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->b(LaGo;LacY;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    .line 561
    :cond_1
    return v2

    :cond_2
    move v0, v2

    .line 556
    goto :goto_0
.end method

.method public declared-synchronized b()V
    .locals 1

    .prologue
    .line 1354
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Lamw;

    invoke-interface {v0}, Lamw;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1355
    monitor-exit p0

    return-void

    .line 1354
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(LaGo;LacY;)Z
    .locals 1

    .prologue
    .line 594
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->a(Z)V

    .line 595
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    invoke-interface {v0, p1, p2}, LaGg;->a(LaGo;LacY;)Z

    move-result v0

    return v0

    .line 594
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 1359
    const-string v0, "DocumentFileManager"

    const-string v1, "Cache cleanup started"

    invoke-static {v0, v1}, LalV;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 1360
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(Z)V

    .line 1361
    const-string v0, "DocumentFileManager"

    const-string v1, "Cache cleanup completed"

    invoke-static {v0, v1}, LalV;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 1362
    return-void
.end method

.method public c(LaGo;LacY;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1551
    invoke-interface {p1, p2}, LaGo;->a(LacY;)J

    move-result-wide v2

    .line 1552
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    invoke-interface {v1, v2, v3}, LaGg;->a(J)LaGp;

    move-result-object v1

    .line 1553
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(LaGp;)Ladw;

    move-result-object v2

    .line 1554
    invoke-virtual {v1}, LaGp;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1558
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v2}, Ladw;->a()J

    move-result-wide v2

    const-wide/32 v4, 0x200000

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public declared-synchronized d(LaGo;LacY;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 600
    monitor-enter p0

    if-eqz p2, :cond_0

    const/4 v1, 0x1

    :goto_0
    :try_start_0
    invoke-static {v1}, LbiT;->a(Z)V

    .line 601
    invoke-interface {p1, p2}, LaGo;->a(LacY;)J

    move-result-wide v2

    .line 602
    iget-object v1, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:LaGg;

    invoke-interface {v1, v2, v3}, LaGg;->a(J)LaGp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 603
    if-nez v1, :cond_1

    .line 607
    :goto_1
    monitor-exit p0

    return v0

    :cond_0
    move v1, v0

    .line 600
    goto :goto_0

    .line 606
    :cond_1
    :try_start_1
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a(LaGp;)Ladw;

    move-result-object v0

    .line 607
    invoke-virtual {v0}, Ladw;->d()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_1

    .line 600
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1541
    monitor-enter p0

    :try_start_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "DocumentFileManagerImpl[%d files]"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/docs/sync/filemanager/DocumentFileManagerImpl;->a:Ljava/util/Map;

    .line 1542
    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 1541
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
