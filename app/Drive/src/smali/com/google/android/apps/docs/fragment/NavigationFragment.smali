.class public Lcom/google/android/apps/docs/fragment/NavigationFragment;
.super Lcom/google/android/apps/docs/app/BaseFragment;
.source "NavigationFragment.java"

# interfaces
.implements LaqW;
.implements Lwn;


# instance fields
.field private a:I

.field public a:LQr;

.field public a:LRJ;

.field public a:LRK;

.field private a:LRX;

.field public a:LRh;

.field public a:LSc;

.field public a:LaFO;

.field public a:LaGM;

.field public a:LaGR;

.field public a:LabF;

.field private a:LabI;

.field public a:Lahh;

.field public a:LajO;

.field public a:Lamn;

.field private final a:Landroid/os/Handler;

.field public a:Landroid/widget/ListView;

.field a:Landroid/widget/Spinner;

.field private a:Landroid/widget/Toast;

.field private a:LaqX;

.field private a:LbmF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmF",
            "<",
            "Landroid/accounts/Account;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lqc;

.field public a:Lqd;

.field public a:LsC;

.field public a:LtK;

.field public a:Lwm;

.field public a:Lye;

.field private a:Z

.field private b:LaFO;

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseFragment;-><init>()V

    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Lqd;

    .line 191
    invoke-static {}, LbmF;->c()LbmF;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LbmF;

    .line 194
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/os/Handler;

    .line 199
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Z

    .line 203
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Ljava/util/List;

    .line 207
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:I

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/NavigationFragment;)I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:I

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/NavigationFragment;I)I
    .locals 0

    .prologue
    .line 80
    iput p1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:I

    return p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/NavigationFragment;)LRX;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LRX;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/NavigationFragment;)LaFO;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->b:LaFO;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/NavigationFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/NavigationFragment;)Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/Toast;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/NavigationFragment;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/Toast;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/NavigationFragment;)LaqX;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LaqX;

    return-object v0
.end method

.method private a()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LRN;",
            ">;"
        }
    .end annotation

    .prologue
    .line 288
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LsC;

    invoke-interface {v0}, LsC;->a()LbmF;

    move-result-object v0

    .line 291
    invoke-virtual {v0}, LbmF;->a()Lbqv;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvr;

    .line 292
    invoke-virtual {v0}, Lvr;->a()I

    move-result v3

    .line 294
    new-instance v4, LRN;

    .line 295
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()LH;

    move-result-object v5

    invoke-virtual {v0}, Lvr;->b()I

    move-result v6

    invoke-virtual {v5, v6}, LH;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lvr;->a()LCl;

    move-result-object v0

    invoke-direct {v4, v5, v0, v3}, LRN;-><init>(Ljava/lang/String;LCl;I)V

    .line 297
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 300
    :cond_0
    return-object v1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/NavigationFragment;)Ljava/util/List;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Ljava/util/List;

    return-object v0
.end method

.method private a(Ljava/util/List;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/accounts/Account;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 547
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 548
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 549
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 551
    :cond_0
    return-object v1
.end method

.method private a(LaFO;LaqX;)V
    .locals 4

    .prologue
    .line 620
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->x()V

    .line 622
    new-instance v0, LRH;

    invoke-direct {v0, p0, p2, p1}, LRH;-><init>(Lcom/google/android/apps/docs/fragment/NavigationFragment;LaqX;LaFO;)V

    .line 636
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/os/Handler;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 637
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/NavigationFragment;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->x()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/NavigationFragment;LaFO;LaqX;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(LaFO;LaqX;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/NavigationFragment;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(Ljava/util/List;)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LRN;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 373
    new-instance v0, LRK;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()LH;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LaFO;

    iget-object v4, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LaGM;

    iget-object v5, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Lahh;

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, LRK;-><init>(Landroid/content/Context;Ljava/util/List;LaFO;LaGM;Lahh;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LRK;

    .line 378
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LRK;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 379
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 380
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 383
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ListView;

    new-instance v1, LRC;

    invoke-direct {v1, p0, p1}, LRC;-><init>(Lcom/google/android/apps/docs/fragment/NavigationFragment;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 420
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->w()V

    .line 421
    return-void
.end method

.method public static a(Landroid/content/res/Resources;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 482
    invoke-static {p0}, LakQ;->b(Landroid/content/res/Resources;)Z

    move-result v3

    .line 483
    invoke-static {p0}, LakQ;->e(Landroid/content/res/Resources;)Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v0

    .line 484
    :goto_0
    if-eqz v3, :cond_1

    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 483
    goto :goto_0

    :cond_1
    move v0, v1

    .line 484
    goto :goto_1
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/fragment/NavigationFragment;)Ljava/util/List;
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private u()V
    .locals 3

    .prologue
    .line 257
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()LH;

    move-result-object v0

    invoke-virtual {v0}, LH;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lxe;->sidebar_action_top_margin:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 259
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 260
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LRh;

    invoke-virtual {v0}, LRh;->a()Ljava/util/List;

    move-result-object v0

    .line 263
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 264
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 265
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 267
    :cond_0
    return-void
.end method

.method private v()V
    .locals 4

    .prologue
    .line 273
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()LH;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LsC;

    .line 274
    invoke-interface {v1}, LsC;->a()I

    move-result v1

    const/4 v2, 0x0

    .line 273
    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->d:Landroid/view/View;

    .line 275
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LRh;

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, LRh;->a(Landroid/view/View;)V

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LSc;

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, LSc;->a(Landroid/view/View;)LRX;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LRX;

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->d:Landroid/view/View;

    const-string v2, "viewAppSpecificFooter"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 278
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->d:Landroid/view/View;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 279
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->t()V

    .line 280
    return-void
.end method

.method private w()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 451
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->clearChoices()V

    .line 453
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Lwm;

    invoke-interface {v0}, Lwm;->a()LbmF;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbnm;->a(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    .line 454
    if-nez v0, :cond_1

    .line 479
    :cond_0
    return-void

    .line 457
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()LH;

    move-result-object v1

    .line 458
    if-eqz v1, :cond_0

    .line 461
    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()LCl;

    move-result-object v0

    .line 462
    if-eqz v0, :cond_0

    .line 465
    invoke-interface {v0}, LCl;->name()Ljava/lang/String;

    move-result-object v3

    move v1, v2

    .line 467
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LRK;

    invoke-virtual {v0}, LRK;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 468
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LRK;

    invoke-virtual {v0, v1}, LRK;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 469
    const-string v4, "FILTER_NAME"

    .line 470
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 471
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 473
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 474
    const-string v0, "NavigationFragment"

    const-string v4, "checked item set to %s"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v0, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 475
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v1, v7}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 476
    iput v1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:I

    .line 467
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private x()V
    .locals 1

    .prologue
    .line 611
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()LH;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/docs/app/DocListActivity;

    if-eqz v0, :cond_0

    .line 612
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()LH;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/DocListActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/DocListActivity;->s()V

    .line 614
    :cond_0
    return-void
.end method


# virtual methods
.method public a()LabI;
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LabI;

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 216
    sget v0, Lxe;->navigation_sliding_panel:I

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 217
    sget v0, Lxc;->account_spinner:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/Spinner;

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getId()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setNextFocusRightId(I)V

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getId()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setNextFocusLeftId(I)V

    .line 221
    new-instance v0, LabI;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()LH;

    move-result-object v3

    invoke-direct {v0, v3}, LabI;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LabI;

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LtK;

    sget-object v3, Lry;->c:Lry;

    .line 223
    invoke-interface {v0, v3}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Lqd;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Z

    .line 225
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Z

    if-eqz v0, :cond_0

    .line 226
    new-instance v0, LRB;

    invoke-direct {v0, p0}, LRB;-><init>(Lcom/google/android/apps/docs/fragment/NavigationFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Lqc;

    .line 239
    sget v0, Lxc;->navigation_list_view:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 240
    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/Spinner;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 241
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/Spinner;

    .line 242
    sget v3, Lxc;->navigation_folders_top_margin:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 243
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Lqd;

    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Lqc;

    iget-object v4, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LaFO;

    .line 244
    invoke-virtual {v4}, LaFO;->b()Ljava/lang/String;

    move-result-object v4

    .line 243
    invoke-interface {v1, v0, p1, v3, v4}, Lqd;->a(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;Lqc;Ljava/lang/String;)V

    .line 246
    :cond_0
    sget v0, Lxc;->navigation_folders:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ListView;

    .line 247
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->u()V

    .line 248
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->v()V

    .line 249
    return-object v2

    :cond_1
    move v0, v1

    .line 223
    goto :goto_0
.end method

.method public a(IZI)Landroid/view/animation/Animation;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 534
    const/16 v0, 0x1001

    if-ne p1, v0, :cond_0

    .line 535
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-direct {v0, v2, v1, v2, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 542
    :goto_0
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 543
    :goto_1
    return-object v0

    .line 536
    :cond_0
    const/16 v0, 0x2002

    if-ne p1, v0, :cond_1

    .line 537
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-direct {v0, v1, v2, v2, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    goto :goto_0

    .line 539
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a()V
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LRX;

    invoke-interface {v0}, LRX;->b()V

    .line 316
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LRX;

    invoke-interface {v0}, LRX;->a()V

    .line 317
    return-void
.end method

.method public a(Landroid/widget/Button;LaFO;)V
    .locals 3

    .prologue
    .line 641
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->b:LaFO;

    invoke-virtual {p2, v0}, LaFO;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 658
    :goto_0
    return-void

    .line 644
    :cond_0
    iput-object p2, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->b:LaFO;

    .line 646
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Z

    if-eqz v0, :cond_1

    .line 647
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Lqd;

    invoke-virtual {p2}, LaFO;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lqd;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 649
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LbmF;

    new-instance v1, LRI;

    invoke-direct {v1, p0}, LRI;-><init>(Lcom/google/android/apps/docs/fragment/NavigationFragment;)V

    .line 650
    invoke-static {v0, v1}, LbnG;->a(Ljava/util/List;LbiG;)Ljava/util/List;

    move-result-object v0

    .line 656
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->b:LaFO;

    invoke-interface {v0, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_0
.end method

.method public a(Landroid/widget/Button;[Landroid/accounts/Account;LaqX;)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 562
    iput-object p3, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LaqX;

    .line 563
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Z

    if-nez v0, :cond_0

    .line 564
    invoke-static {p2}, LbmF;->a([Ljava/lang/Object;)LbmF;

    move-result-object v0

    .line 566
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(Ljava/util/List;)Ljava/util/Set;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LbmF;

    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(Ljava/util/List;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 568
    if-nez v1, :cond_0

    .line 569
    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LbmF;

    .line 571
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LbmF;

    new-instance v1, LRF;

    invoke-direct {v1, p0}, LRF;-><init>(Lcom/google/android/apps/docs/fragment/NavigationFragment;)V

    .line 572
    invoke-static {v0, v1}, LbnG;->a(Ljava/util/List;LbiG;)Ljava/util/List;

    move-result-object v3

    .line 579
    new-instance v0, LajV;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()LH;

    move-result-object v1

    sget v2, Lxe;->account_spinner:I

    iget-object v4, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LabI;

    iget-object v5, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LabF;

    move v7, v6

    invoke-direct/range {v0 .. v7}, LajV;-><init>(Landroid/content/Context;ILjava/util/List;LabI;LabF;ZZ)V

    .line 587
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 588
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/Spinner;

    new-instance v1, LRG;

    invoke-direct {v1, p0, p2, p3}, LRG;-><init>(Lcom/google/android/apps/docs/fragment/NavigationFragment;[Landroid/accounts/Account;LaqX;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 605
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LaFO;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(Landroid/widget/Button;LaFO;)V

    .line 608
    :cond_0
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 429
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Z

    invoke-static {v0}, LbiT;->b(Z)V

    .line 430
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Lqd;

    invoke-interface {v0}, Lqd;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 305
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseFragment;->b(Landroid/os/Bundle;)V

    .line 307
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Lwm;

    invoke-interface {v0, p0}, Lwm;->a(Lwn;)V

    .line 309
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(Ljava/util/List;)V

    .line 311
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()V

    .line 312
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 446
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Z

    invoke-static {v0}, LbiT;->b(Z)V

    .line 447
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Lqd;

    invoke-interface {v0}, Lqd;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 4

    .prologue
    .line 495
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()LH;

    move-result-object v0

    .line 496
    if-nez v0, :cond_0

    .line 528
    :goto_0
    return-void

    .line 499
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Lwm;

    invoke-interface {v1}, Lwm;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v1

    .line 501
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LaGR;

    new-instance v3, LRE;

    invoke-direct {v3, p0, v1, v0}, LRE;-><init>(Lcom/google/android/apps/docs/fragment/NavigationFragment;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;Landroid/app/Activity;)V

    invoke-virtual {v2, v3}, LaGR;->b(LaGN;)V

    .line 527
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->w()V

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 489
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LRK;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 490
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LRK;

    invoke-virtual {v0}, LRK;->notifyDataSetChanged()V

    .line 491
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 359
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Z

    if-eqz v0, :cond_0

    .line 360
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Lqd;

    invoke-interface {v0}, Lqd;->c()V

    .line 362
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseFragment;->h()V

    .line 363
    return-void
.end method

.method public j_()V
    .locals 1

    .prologue
    .line 342
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Z

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Lqd;

    invoke-interface {v0}, Lqd;->b()V

    .line 345
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseFragment;->j_()V

    .line 346
    return-void
.end method

.method public k_()V
    .locals 2

    .prologue
    .line 350
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseFragment;->k_()V

    .line 351
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Z

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Lqd;

    invoke-interface {v0}, Lqd;->a()V

    .line 353
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Lqd;

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LaFO;

    invoke-virtual {v1}, LaFO;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lqd;->a(Ljava/lang/String;)V

    .line 355
    :cond_0
    return-void
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 437
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Z

    return v0
.end method

.method public t()V
    .locals 2

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->d:Landroid/view/View;

    if-nez v0, :cond_1

    .line 338
    :cond_0
    :goto_0
    return-void

    .line 324
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->d:Landroid/view/View;

    sget v1, Lxc;->navigate_to_drive:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 325
    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Lye;

    const-string v1, "DRIVE_PROMO"

    invoke-interface {v0, v1}, Lye;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, LuM;->a:LuM;

    .line 331
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()LH;

    move-result-object v1

    invoke-virtual {v0, v1}, LuM;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 332
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 336
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
