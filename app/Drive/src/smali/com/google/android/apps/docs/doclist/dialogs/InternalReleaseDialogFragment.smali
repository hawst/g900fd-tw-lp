.class public Lcom/google/android/apps/docs/doclist/dialogs/InternalReleaseDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "InternalReleaseDialogFragment.java"


# instance fields
.field public a:LFi;

.field public a:Lrx;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    .line 57
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 110
    invoke-static {}, Laml;->a()Ljava/lang/String;

    move-result-object v0

    .line 111
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "acceptedAppVersion"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 112
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 113
    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 5

    .prologue
    .line 122
    invoke-static {}, Laml;->a()Lrx;

    move-result-object v0

    .line 123
    invoke-static {}, Laml;->a()Ljava/lang/String;

    move-result-object v1

    .line 124
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "acceptedAppVersion"

    const/4 v4, 0x0

    .line 125
    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 126
    iget-boolean v0, v0, Lrx;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 141
    sget v0, Lxe;->internal_release_dialog:I

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 142
    sget v0, Lxc;->internal_release_logo:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 143
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/InternalReleaseDialogFragment;->a()LH;

    move-result-object v2

    invoke-virtual {v2}, LH;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->icon:I

    .line 144
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 146
    sget v0, Lxc;->internal_release_dialog_ok:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 148
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/dialogs/InternalReleaseDialogFragment;->a:LFi;

    invoke-static {v2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    new-instance v2, LFe;

    invoke-direct {v2, p0}, LFe;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/InternalReleaseDialogFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    return-object v1
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 134
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a_(Landroid/os/Bundle;)V

    .line 135
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/doclist/dialogs/InternalReleaseDialogFragment;->a(II)V

    .line 136
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 172
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 173
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/InternalReleaseDialogFragment;->a()LH;

    move-result-object v0

    .line 174
    if-nez v0, :cond_1

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 179
    :cond_1
    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/dialogs/InternalReleaseDialogFragment;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 180
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method
