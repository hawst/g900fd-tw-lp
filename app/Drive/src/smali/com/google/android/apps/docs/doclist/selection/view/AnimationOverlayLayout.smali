.class public Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;
.super Landroid/widget/FrameLayout;
.source "AnimationOverlayLayout.java"


# instance fields
.field public a:LKp;

.field private final a:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final a:[I

.field private final b:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 41
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 27
    new-instance v0, LKr;

    invoke-direct {v0, p0}, LKr;-><init>(Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 36
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a:Ljava/util/Set;

    .line 37
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a:[I

    .line 38
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->b:[I

    .line 42
    invoke-static {p1}, Lajt;->a(Landroid/content/Context;)Lbuu;

    move-result-object v0

    invoke-interface {v0, p0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 46
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    new-instance v0, LKr;

    invoke-direct {v0, p0}, LKr;-><init>(Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 36
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a:Ljava/util/Set;

    .line 37
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a:[I

    .line 38
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->b:[I

    .line 47
    invoke-static {p1}, Lajt;->a(Landroid/content/Context;)Lbuu;

    move-result-object v0

    invoke-interface {v0, p0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    new-instance v0, LKr;

    invoke-direct {v0, p0}, LKr;-><init>(Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 36
    invoke-static {}, LbpU;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a:Ljava/util/Set;

    .line 37
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a:[I

    .line 38
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->b:[I

    .line 52
    invoke-static {p1}, Lajt;->a(Landroid/content/Context;)Lbuu;

    move-result-object v0

    invoke-interface {v0, p0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 53
    return-void
.end method


# virtual methods
.method public a()Landroid/graphics/Point;
    .locals 4

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a:[I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->getLocationOnScreen([I)V

    .line 69
    new-instance v0, Landroid/graphics/Point;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    return-object v0
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 75
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->setWillNotDraw(Z)V

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 78
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 79
    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 85
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-static {v0, v1}, LUv;->a(Landroid/view/ViewTreeObserver;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 88
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->setWillNotDraw(Z)V

    .line 90
    :cond_0
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 95
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a:[I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->getLocationOnScreen([I)V

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 98
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v2

    .line 99
    invoke-virtual {v0}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    .line 100
    iget-object v4, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->b:[I

    invoke-virtual {v0, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 101
    invoke-virtual {v0}, Landroid/view/View;->getPivotX()F

    move-result v4

    .line 102
    invoke-virtual {v0}, Landroid/view/View;->getPivotY()F

    move-result v5

    .line 103
    iget-object v6, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->b:[I

    aget v6, v6, v9

    iget-object v7, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a:[I

    aget v7, v7, v9

    sub-int/2addr v6, v7

    int-to-float v6, v6

    sub-float/2addr v6, v4

    iget-object v7, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->b:[I

    aget v7, v7, v10

    iget-object v8, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a:[I

    aget v8, v8, v10

    sub-int/2addr v7, v8

    int-to-float v7, v7

    sub-float/2addr v7, v5

    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->translate(FF)V

    .line 105
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 106
    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 108
    invoke-virtual {v0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 109
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0

    .line 111
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->invalidate()V

    .line 112
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 57
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a:LKp;

    invoke-virtual {v0, p0}, LKp;->a(Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;)V

    .line 59
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/selection/view/AnimationOverlayLayout;->a:LKp;

    invoke-virtual {v0}, LKp;->a()V

    .line 65
    return-void
.end method
