.class public Lcom/google/android/apps/docs/common/view/RoundImageView;
.super Landroid/widget/ImageView;
.source "RoundImageView.java"


# instance fields
.field private a:Landroid/graphics/Bitmap;

.field private a:Landroid/graphics/BitmapShader;

.field private a:Landroid/graphics/Canvas;

.field private a:Landroid/graphics/Paint;

.field private a:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/docs/common/view/RoundImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 64
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/docs/common/view/RoundImageView;->a:Landroid/graphics/Paint;

    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/docs/common/view/RoundImageView;->getWidth()I

    move-result v0

    .line 66
    invoke-virtual {p0}, Lcom/google/android/apps/docs/common/view/RoundImageView;->getHeight()I

    move-result v1

    .line 67
    iget-object v2, p0, Lcom/google/android/apps/docs/common/view/RoundImageView;->a:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/docs/common/view/RoundImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-ne v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/docs/common/view/RoundImageView;->a:Landroid/graphics/Bitmap;

    .line 68
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 74
    :goto_0
    return-void

    .line 71
    :cond_0
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/common/view/RoundImageView;->a:Landroid/graphics/Bitmap;

    .line 72
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/google/android/apps/docs/common/view/RoundImageView;->a:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/common/view/RoundImageView;->a:Landroid/graphics/Canvas;

    .line 73
    new-instance v0, Landroid/graphics/BitmapShader;

    iget-object v1, p0, Lcom/google/android/apps/docs/common/view/RoundImageView;->a:Landroid/graphics/Bitmap;

    sget-object v2, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v3, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/common/view/RoundImageView;->a:Landroid/graphics/BitmapShader;

    goto :goto_0
.end method

.method private a(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/android/apps/docs/common/view/RoundImageView;->a:Landroid/graphics/drawable/Drawable;

    .line 45
    invoke-virtual {p0}, Lcom/google/android/apps/docs/common/view/RoundImageView;->invalidate()V

    .line 46
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/docs/common/view/RoundImageView;->a:Landroid/graphics/Canvas;

    if-nez v0, :cond_0

    .line 101
    :goto_0
    return-void

    .line 87
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/common/view/RoundImageView;->getWidth()I

    move-result v0

    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/docs/common/view/RoundImageView;->getHeight()I

    move-result v1

    .line 89
    iget-object v2, p0, Lcom/google/android/apps/docs/common/view/RoundImageView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_1

    .line 90
    iget-object v2, p0, Lcom/google/android/apps/docs/common/view/RoundImageView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3, v3, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 91
    iget-object v2, p0, Lcom/google/android/apps/docs/common/view/RoundImageView;->a:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/google/android/apps/docs/common/view/RoundImageView;->a:Landroid/graphics/Canvas;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 93
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/docs/common/view/RoundImageView;->a:Landroid/graphics/Canvas;

    invoke-super {p0, v2}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 94
    iget-object v2, p0, Lcom/google/android/apps/docs/common/view/RoundImageView;->a:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/google/android/apps/docs/common/view/RoundImageView;->a:Landroid/graphics/BitmapShader;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 95
    invoke-virtual {p0}, Lcom/google/android/apps/docs/common/view/RoundImageView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/common/view/RoundImageView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v0, v2

    .line 96
    invoke-virtual {p0}, Lcom/google/android/apps/docs/common/view/RoundImageView;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/common/view/RoundImageView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    .line 97
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    .line 98
    invoke-virtual {p0}, Lcom/google/android/apps/docs/common/view/RoundImageView;->getPaddingLeft()I

    move-result v3

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v3

    .line 99
    invoke-virtual {p0}, Lcom/google/android/apps/docs/common/view/RoundImageView;->getPaddingTop()I

    move-result v3

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v3

    .line 100
    int-to-float v0, v0

    int-to-float v1, v1

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/apps/docs/common/view/RoundImageView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 78
    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    .line 79
    invoke-direct {p0}, Lcom/google/android/apps/docs/common/view/RoundImageView;->a()V

    .line 80
    return-void
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/common/view/RoundImageView;->a(Landroid/graphics/drawable/Drawable;)V

    .line 41
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/common/view/RoundImageView;->a(Landroid/graphics/drawable/Drawable;)V

    .line 51
    return-void
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/common/view/RoundImageView;->a(Landroid/graphics/drawable/Drawable;)V

    .line 56
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/android/apps/docs/common/view/RoundImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/common/view/RoundImageView;->a(Landroid/graphics/drawable/Drawable;)V

    .line 61
    return-void
.end method
