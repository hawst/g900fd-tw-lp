.class public final Lcom/google/android/apps/viewer/client/AuthenticatedUri;
.super Ljava/lang/Object;
.source "AuthenticatedUri.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/viewer/client/AuthenticatedUri;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final tokens:Lcom/google/android/apps/viewer/client/TokenSource;

.field public final uri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, LauK;

    invoke-direct {v0}, LauK;-><init>()V

    sput-object v0, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Lcom/google/android/apps/viewer/client/TokenSource;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-static {p1}, LauV;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    .line 20
    const-string v0, "Use NO_AUTH for unauthenticated."

    invoke-static {p2, v0}, LauV;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/viewer/client/TokenSource;

    iput-object v0, p0, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->tokens:Lcom/google/android/apps/viewer/client/TokenSource;

    .line 21
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/viewer/client/TokenSource;)V
    .locals 1

    .prologue
    .line 24
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/viewer/client/AuthenticatedUri;-><init>(Landroid/net/Uri;Lcom/google/android/apps/viewer/client/TokenSource;)V

    .line 25
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->uri:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 35
    iget-object v0, p0, Lcom/google/android/apps/viewer/client/AuthenticatedUri;->tokens:Lcom/google/android/apps/viewer/client/TokenSource;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 36
    return-void
.end method
