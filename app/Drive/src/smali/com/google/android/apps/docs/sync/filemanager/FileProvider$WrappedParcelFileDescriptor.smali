.class Lcom/google/android/apps/docs/sync/filemanager/FileProvider$WrappedParcelFileDescriptor;
.super Landroid/os/ParcelFileDescriptor;
.source "FileProvider.java"


# instance fields
.field private final a:LadC;


# direct methods
.method private constructor <init>(Landroid/os/ParcelFileDescriptor;LadC;)V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0, p1}, Landroid/os/ParcelFileDescriptor;-><init>(Landroid/os/ParcelFileDescriptor;)V

    .line 167
    iput-object p2, p0, Lcom/google/android/apps/docs/sync/filemanager/FileProvider$WrappedParcelFileDescriptor;->a:LadC;

    .line 168
    return-void
.end method

.method static a(LadC;LbmY;)Lcom/google/android/apps/docs/sync/filemanager/FileProvider$WrappedParcelFileDescriptor;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LadC;",
            "LbmY",
            "<",
            "LadI;",
            ">;)",
            "Lcom/google/android/apps/docs/sync/filemanager/FileProvider$WrappedParcelFileDescriptor;"
        }
    .end annotation

    .prologue
    .line 181
    new-instance v0, Lcom/google/android/apps/docs/sync/filemanager/FileProvider$WrappedParcelFileDescriptor;

    invoke-interface {p0, p1}, LadC;->a(LbmY;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/android/apps/docs/sync/filemanager/FileProvider$WrappedParcelFileDescriptor;-><init>(Landroid/os/ParcelFileDescriptor;LadC;)V

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 173
    :try_start_0
    invoke-super {p0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/docs/sync/filemanager/FileProvider$WrappedParcelFileDescriptor;->a:LadC;

    invoke-interface {v0}, LadC;->close()V

    .line 177
    return-void

    .line 175
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/docs/sync/filemanager/FileProvider$WrappedParcelFileDescriptor;->a:LadC;

    invoke-interface {v1}, LadC;->close()V

    throw v0
.end method
