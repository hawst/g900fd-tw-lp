.class public Lcom/google/android/apps/docs/drive/media/VideoController;
.super Landroid/widget/FrameLayout;
.source "VideoController.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private a:LNV;

.field private final a:LNZ;

.field private final a:LalR;

.field private final a:Landroid/view/View$OnClickListener;

.field private a:Landroid/view/View;

.field private a:Landroid/widget/ImageButton;

.field private a:Landroid/widget/ProgressBar;

.field private final a:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private a:Landroid/widget/TextView;

.field private a:Landroid/widget/VideoView;

.field private final a:Ljava/lang/Runnable;

.field private a:Ljava/lang/StringBuilder;

.field private a:Ljava/util/Formatter;

.field private a:Z

.field private b:Landroid/view/View;

.field private b:Landroid/widget/ProgressBar;

.field private b:Landroid/widget/TextView;

.field private final b:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/VideoView;LNZ;)V
    .locals 1

    .prologue
    .line 157
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {p0, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 115
    new-instance v0, LNQ;

    invoke-direct {v0, p0}, LNQ;-><init>(Lcom/google/android/apps/docs/drive/media/VideoController;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/view/View$OnClickListener;

    .line 122
    new-instance v0, LNR;

    invoke-direct {v0, p0}, LNR;-><init>(Lcom/google/android/apps/docs/drive/media/VideoController;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Ljava/lang/Runnable;

    .line 129
    new-instance v0, LNS;

    invoke-direct {v0, p0}, LNS;-><init>(Lcom/google/android/apps/docs/drive/media/VideoController;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->b:Ljava/lang/Runnable;

    .line 140
    new-instance v0, LNT;

    invoke-direct {v0, p0}, LNT;-><init>(Lcom/google/android/apps/docs/drive/media/VideoController;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:LalR;

    .line 225
    new-instance v0, LNU;

    invoke-direct {v0, p0}, LNU;-><init>(Lcom/google/android/apps/docs/drive/media/VideoController;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 158
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNZ;

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:LNZ;

    .line 159
    invoke-direct {p0, p2}, Lcom/google/android/apps/docs/drive/media/VideoController;->a(Landroid/widget/VideoView;)V

    .line 160
    sget-object v0, LNV;->a:LNV;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->a(LNV;)V

    .line 161
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Z

    .line 162
    return-void
.end method

.method private a()I
    .locals 1

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/media/VideoController;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lamt;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, LNL;->mtl_video_controller:I

    :goto_0
    return v0

    :cond_0
    sget v0, LNL;->video_controller:I

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/drive/media/VideoController;)LNV;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:LNV;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/drive/media/VideoController;)Landroid/view/View;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/drive/media/VideoController;)Landroid/widget/VideoView;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/widget/VideoView;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/drive/media/VideoController;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->b:Ljava/lang/Runnable;

    return-object v0
.end method

.method private a(I)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 253
    div-int/lit16 v0, p1, 0x3e8

    .line 255
    rem-int/lit8 v1, v0, 0x3c

    .line 256
    div-int/lit8 v2, v0, 0x3c

    rem-int/lit8 v2, v2, 0x3c

    .line 257
    div-int/lit16 v0, v0, 0xe10

    .line 259
    iget-object v3, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 260
    if-lez v0, :cond_0

    .line 261
    iget-object v3, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Ljava/util/Formatter;

    const-string v4, "%d:%02d:%02d"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v8

    invoke-virtual {v3, v4, v5}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 263
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Ljava/util/Formatter;

    const-string v3, "%02d:%02d"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v7

    invoke-virtual {v0, v3, v4}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(LNV;)V
    .locals 1

    .prologue
    .line 323
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Z

    .line 324
    iput-object p1, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:LNV;

    .line 325
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 187
    sget v0, LNJ;->loading_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/widget/ProgressBar;

    .line 188
    sget v0, LNJ;->information_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->b:Landroid/view/View;

    .line 189
    sget v0, LNJ;->time_current:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/widget/TextView;

    .line 190
    sget v0, LNJ;->time:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->b:Landroid/widget/TextView;

    .line 191
    sget v0, LNJ;->mediacontroller_progress:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->b:Landroid/widget/ProgressBar;

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->b:Landroid/widget/ProgressBar;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 193
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Ljava/lang/StringBuilder;

    .line 194
    new-instance v0, Ljava/util/Formatter;

    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Ljava/lang/StringBuilder;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Ljava/util/Formatter;

    .line 196
    sget v0, LNJ;->control_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/widget/ImageButton;

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->b:Landroid/widget/ProgressBar;

    check-cast v0, Landroid/widget/SeekBar;

    .line 200
    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 201
    return-void
.end method

.method private a(Landroid/widget/VideoView;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 165
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/VideoView;

    iput-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/widget/VideoView;

    .line 166
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 168
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/media/VideoController;->a()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/view/View;

    .line 169
    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/view/View;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 170
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/drive/media/VideoController;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/docs/drive/media/VideoController;->h()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/drive/media/VideoController;I)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/drive/media/VideoController;->c(I)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 290
    if-eqz p1, :cond_0

    sget-object v0, LalQ;->a:LalQ;

    .line 291
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:LalR;

    invoke-static {v1, v0}, LalQ;->a(LalR;LalQ;)V

    .line 292
    return-void

    .line 290
    :cond_0
    sget-object v0, LalQ;->b:LalQ;

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/drive/media/VideoController;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Ljava/lang/Runnable;

    return-object v0
.end method

.method private c(I)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 268
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->getDuration()I

    move-result v0

    .line 270
    if-lez p1, :cond_1

    .line 271
    :goto_0
    if-lez v0, :cond_2

    .line 272
    :goto_1
    if-lez v0, :cond_0

    .line 274
    const-wide/16 v2, 0x3e8

    int-to-long v4, p1

    mul-long/2addr v2, v4

    int-to-long v4, v0

    div-long/2addr v2, v4

    .line 275
    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->b:Landroid/widget/ProgressBar;

    long-to-int v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 278
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->getBufferPercentage()I

    move-result v1

    .line 279
    iget-object v2, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->b:Landroid/widget/ProgressBar;

    mul-int/lit8 v1, v1, 0xa

    invoke-virtual {v2, v1}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    .line 280
    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->b:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/drive/media/VideoController;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 282
    return-void

    :cond_1
    move p1, v1

    .line 270
    goto :goto_0

    :cond_2
    move v0, v1

    .line 271
    goto :goto_1
.end method

.method private g()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 212
    iget-boolean v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Z

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 222
    :goto_0
    return-void

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:LNV;

    invoke-virtual {v1}, LNV;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:LNV;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/media/VideoController;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LNV;->a(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 220
    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private h()V
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v0

    .line 286
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->c(I)V

    .line 287
    return-void
.end method


# virtual methods
.method protected a()Landroid/view/View;
    .locals 3

    .prologue
    .line 173
    .line 174
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/media/VideoController;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 175
    invoke-direct {p0}, Lcom/google/android/apps/docs/drive/media/VideoController;->a()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 176
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->a(Landroid/view/View;)V

    .line 177
    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 295
    const/16 v0, 0xbb8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->a(I)V

    .line 296
    return-void
.end method

.method public a(I)V
    .locals 4

    .prologue
    .line 299
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->a(Z)V

    .line 300
    invoke-direct {p0}, Lcom/google/android/apps/docs/drive/media/VideoController;->g()V

    .line 301
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->b:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->post(Ljava/lang/Runnable;)Z

    .line 304
    if-lez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:LNV;

    sget-object v1, LNV;->a:LNV;

    invoke-virtual {v0, v1}, LNV;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Ljava/lang/Runnable;

    int-to-long v2, p1

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/apps/docs/drive/media/VideoController;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 307
    :cond_0
    return-void
.end method

.method public a(ILandroid/media/MediaPlayer;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 357
    if-gtz p1, :cond_0

    sget-object v0, LNV;->b:LNV;

    iget-object v1, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:LNV;

    invoke-virtual {v0, v1}, LNV;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 358
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/media/VideoController;->f()V

    .line 362
    :goto_0
    return-void

    .line 360
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->b(I)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->b:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 311
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 312
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 314
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->a(Z)V

    .line 315
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/widget/VideoView;

    invoke-virtual {v0, p1}, Landroid/widget/VideoView;->seekTo(I)V

    .line 341
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/media/VideoController;->e()V

    .line 342
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 318
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Z

    .line 319
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->a(I)V

    .line 320
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 328
    sget-object v0, LNV;->c:LNV;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->a(LNV;)V

    .line 329
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->a(I)V

    .line 330
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 333
    sget-object v0, LNV;->a:LNV;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->a(LNV;)V

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    .line 335
    invoke-virtual {p0}, Lcom/google/android/apps/docs/drive/media/VideoController;->a()V

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:LNZ;

    invoke-interface {v0}, LNZ;->a()V

    .line 337
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 345
    sget-object v0, LNV;->b:LNV;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->a(LNV;)V

    .line 346
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->pause()V

    .line 347
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->a(I)V

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:LNZ;

    invoke-interface {v0}, LNZ;->d_()V

    .line 349
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/docs/drive/media/VideoController;->b:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/drive/media/VideoController;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 207
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 208
    return-void
.end method
