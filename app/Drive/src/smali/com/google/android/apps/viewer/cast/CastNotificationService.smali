.class public Lcom/google/android/apps/viewer/cast/CastNotificationService;
.super Landroid/app/Service;
.source "CastNotificationService.java"


# instance fields
.field private a:I

.field private a:Landroid/app/Notification;

.field public a:Landroid/graphics/Bitmap;

.field private a:LauC;

.field private a:LauI;

.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:Z

    .line 216
    return-void
.end method

.method private a(LaCh;Landroid/graphics/Bitmap;Z)Landroid/widget/RemoteViews;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 141
    const-string v0, "CastNotificationService"

    const-string v1, "Building notification"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/cast/CastNotificationService;->getPackageName()Ljava/lang/String;

    move-result-object v0

    sget v2, Lauk;->custom_notification:I

    invoke-direct {v1, v0, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 145
    invoke-direct {p0, v1, p3, p1}, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a(Landroid/widget/RemoteViews;ZLaCh;)V

    .line 147
    if-eqz p2, :cond_0

    .line 148
    const-string v0, "CastNotificationService"

    const-string v2, "Use low res"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    sget v0, Lauj;->iconView:I

    invoke-virtual {v1, v0, p2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 155
    :goto_0
    sget v0, Lauj;->titleView:I

    invoke-virtual {p1}, LaCh;->a()LaCj;

    move-result-object v2

    const-string v3, "com.google.android.gms.cast.metadata.TITLE"

    invoke-virtual {v2, v3}, LaCj;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 157
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/cast/CastNotificationService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Laum;->casting_to_device:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:LauC;

    invoke-virtual {v2}, LauC;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 158
    sget v2, Lauj;->subTitleView:I

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 160
    new-instance v0, LaE;

    invoke-direct {v0, p0}, LaE;-><init>(Landroid/content/Context;)V

    sget v2, Laui;->ic_stat_action_notification:I

    .line 161
    invoke-virtual {v0, v2}, LaE;->a(I)LaE;

    move-result-object v0

    .line 163
    invoke-virtual {v0, v1}, LaE;->a(Landroid/widget/RemoteViews;)LaE;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LaE;->c(Z)LaE;

    move-result-object v0

    invoke-virtual {v0, v4}, LaE;->a(Z)LaE;

    move-result-object v0

    invoke-virtual {v0}, LaE;->a()Landroid/app/Notification;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:Landroid/app/Notification;

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:Landroid/app/Notification;

    iput-object v1, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 169
    const-string v0, "notification"

    .line 170
    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/cast/CastNotificationService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 171
    iget-object v2, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:Landroid/app/Notification;

    invoke-virtual {v0, v4, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 173
    return-object v1

    .line 151
    :cond_0
    sget v0, Lauj;->iconView:I

    sget v2, Laui;->chromecast_icon:I

    invoke-virtual {v1, v0, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 152
    const-string v0, "CastNotificationService"

    const-string v2, "Use default"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/cast/CastNotificationService;LaCh;Landroid/graphics/Bitmap;Z)Landroid/widget/RemoteViews;
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a(LaCh;Landroid/graphics/Bitmap;Z)Landroid/widget/RemoteViews;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 131
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/cast/CastNotificationService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 132
    return-void
.end method

.method private a(I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 74
    iput p1, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:I

    .line 75
    const-string v0, "CastNotificationService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRemoteMediaPlayerStatusUpdated() reached with status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    iget v0, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:I

    packed-switch v0, :pswitch_data_0

    .line 106
    :goto_0
    return-void

    .line 79
    :pswitch_0
    iput-boolean v3, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:Z

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:LauC;

    invoke-virtual {v0}, LauC;->a()LaCh;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a(LaCh;)V

    goto :goto_0

    .line 83
    :pswitch_1
    iput-boolean v4, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:Z

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:LauC;

    invoke-virtual {v0}, LauC;->a()LaCh;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a(LaCh;)V

    goto :goto_0

    .line 87
    :pswitch_2
    iput-boolean v3, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:Z

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:LauC;

    invoke-virtual {v0}, LauC;->a()LaCh;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a(LaCh;)V

    goto :goto_0

    .line 91
    :pswitch_3
    iput-boolean v3, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:Z

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:LauC;

    invoke-virtual {v0}, LauC;->a()LaCh;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a(LaCh;)V

    goto :goto_0

    .line 99
    :pswitch_4
    iput-boolean v3, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:Z

    .line 100
    invoke-virtual {p0, v4}, Lcom/google/android/apps/viewer/cast/CastNotificationService;->stopForeground(Z)V

    goto :goto_0

    .line 77
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private a(LaCh;)V
    .locals 3

    .prologue
    .line 109
    const-string v0, "CastNotificationService"

    const-string v1, "Setup Notification"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    if-nez p1, :cond_0

    .line 111
    const-string v0, "CastNotificationService"

    const-string v1, "mediaInfo is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    :goto_0
    return-void

    .line 115
    :cond_0
    :try_start_0
    invoke-virtual {p1}, LaCh;->a()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "preload"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    const-string v0, "CastNotificationService"

    const-string v1, "Success in parse JSON object, but preload is true."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 119
    :catch_0
    move-exception v0

    .line 120
    const-string v0, "CastNotificationService"

    const-string v1, "Fail to parse JSON mediaInfo object."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 125
    :cond_1
    new-instance v0, LauI;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LauI;-><init>(Lcom/google/android/apps/viewer/cast/CastNotificationService;LauH;)V

    iput-object v0, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:LauI;

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:LauI;

    const/4 v1, 0x1

    new-array v1, v1, [LaCh;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, LauI;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private a(Landroid/widget/RemoteViews;ZLaCh;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 187
    invoke-virtual {p3}, LaCh;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[/]"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v2

    .line 189
    const-string v1, "video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 190
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.viewer.cast.action.TOGGLE_PLAYBACK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 191
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/cast/CastNotificationService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 192
    invoke-static {p0, v2, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 193
    sget v1, Lauj;->playPauseView:I

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 195
    if-eqz p2, :cond_1

    .line 196
    sget v0, Lauj;->playPauseView:I

    sget v1, Laui;->ic_av_stop_sm_dark:I

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 209
    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.viewer.cast.action.STOP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 210
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/cast/CastNotificationService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 211
    invoke-static {p0, v2, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 212
    sget v1, Lauj;->removeView:I

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 214
    return-void

    .line 198
    :cond_1
    sget v0, Lauj;->playPauseView:I

    sget v1, Laui;->ic_av_play_sm_dark:I

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_0

    .line 202
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.viewer.cast.action.PLAY_NEXT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 203
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/cast/CastNotificationService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 204
    invoke-static {p0, v2, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 205
    sget v1, Lauj;->playPauseView:I

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 206
    sget v0, Lauj;->playPauseView:I

    sget v1, Laui;->ic_av_play_sm_dark:I

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/cast/CastNotificationService;)Z
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:Z

    return v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 294
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:LauC;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LauC;->a(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 298
    :goto_0
    return-void

    .line 295
    :catch_0
    move-exception v0

    .line 296
    const-string v0, "CastNotificationService"

    const-string v1, "Fail to cast the next file."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 312
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:LauC;

    invoke-virtual {v0}, LauC;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 316
    :goto_0
    return-void

    .line 313
    :catch_0
    move-exception v0

    .line 314
    const-string v1, "CastNotificationService"

    const-string v2, "Fail to disconnect casting "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 320
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:LauC;

    invoke-virtual {v0}, LauC;->c()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 324
    :goto_0
    return-void

    .line 321
    :catch_0
    move-exception v0

    .line 322
    const-string v1, "CastNotificationService"

    const-string v2, "Failed to toggle the playback"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 328
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 49
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 50
    const-string v0, "CastNotificationService"

    const-string v1, "Service onCreate!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    invoke-static {}, LauC;->a()LauC;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:LauC;

    .line 57
    new-instance v0, LauH;

    invoke-direct {v0, p0}, LauH;-><init>(Lcom/google/android/apps/viewer/cast/CastNotificationService;)V

    .line 70
    iget-object v1, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:LauC;

    invoke-virtual {v1, v0}, LauC;->a(LauB;)V

    .line 71
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:LauI;

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:LauI;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LauI;->cancel(Z)Z

    .line 336
    :cond_0
    const-string v0, "CastNotificationService"

    const-string v1, "onDestroy was called"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    invoke-direct {p0}, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a()V

    .line 339
    iget-object v0, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:LauC;

    if-eqz v0, :cond_1

    .line 340
    iget-object v0, p0, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a:LauC;

    invoke-virtual {v0}, LauC;->b()V

    .line 342
    :cond_1
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4

    .prologue
    .line 267
    const-string v0, "CastNotificationService"

    const-string v1, "Service started!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    if-nez p1, :cond_0

    .line 269
    const-string v0, "CastNotificationService"

    const-string v1, "onStartCommand(): Intent is null."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    :goto_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/cast/CastNotificationService;->a(I)V

    .line 289
    const/4 v0, 0x3

    return v0

    .line 272
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 273
    const-string v1, "CastNotificationService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "action = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    const-string v1, "com.google.android.apps.viewer.cast.action.TOGGLE_PLAYBACK"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 276
    const-string v0, "CastNotificationService"

    const-string v1, "onStartCommand(): Action: ACTION_TOGGLE_PLAYBACK"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    invoke-direct {p0}, Lcom/google/android/apps/viewer/cast/CastNotificationService;->d()V

    goto :goto_0

    .line 278
    :cond_1
    const-string v1, "com.google.android.apps.viewer.cast.action.STOP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 279
    const-string v0, "CastNotificationService"

    const-string v1, "onStartCommand(): Action: ACTION_STOP"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    invoke-direct {p0}, Lcom/google/android/apps/viewer/cast/CastNotificationService;->c()V

    goto :goto_0

    .line 281
    :cond_2
    const-string v1, "com.google.android.apps.viewer.cast.action.PLAY_NEXT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 282
    const-string v0, "CastNotificationService"

    const-string v1, "onStartCommand(): Action: ACTION_PLAY_NEXT"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    invoke-direct {p0}, Lcom/google/android/apps/viewer/cast/CastNotificationService;->b()V

    goto :goto_0

    .line 285
    :cond_3
    const-string v0, "CastNotificationService"

    const-string v1, "onStartCommand(): Action: none"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
