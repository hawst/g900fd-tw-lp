.class public Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;
.super Lcom/google/android/apps/docs/app/BaseFragment;
.source "DragKnobFragment.java"

# interfaces
.implements LXb;


# instance fields
.field private a:F

.field private a:I

.field private a:J

.field public a:LWY;

.field private a:Lcom/google/android/apps/docs/punchwebview/DragKnobView;

.field private a:Z

.field private b:F

.field private b:I

.field private b:J

.field private b:Landroid/view/ViewGroup;

.field private b:Z

.field private c:F

.field private c:I

.field private c:Ljava/lang/Boolean;

.field private c:Ljava/lang/String;

.field private d:F

.field private d:Landroid/view/View;

.field private d:Ljava/lang/String;

.field private e:F

.field private e:Landroid/view/View;

.field private f:F

.field private g:F

.field private h:F

.field private i:F

.field private j:F

.field private k:F

.field private l:F

.field private m:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 54
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseFragment;-><init>()V

    .line 101
    iput-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:Z

    .line 106
    iput-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:Z

    .line 152
    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:I

    .line 158
    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:I

    .line 191
    iput v1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->c:I

    .line 192
    iput v1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->m:I

    return-void
.end method

.method private a(Landroid/view/MotionEvent;)F
    .locals 10

    .prologue
    .line 407
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b(Landroid/view/MotionEvent;)F

    move-result v0

    .line 408
    iget v1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->d:F

    sub-float v1, v0, v1

    .line 409
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    .line 410
    iget-wide v4, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:J

    sub-long v4, v2, v4

    .line 412
    float-to-double v6, v1

    const-wide/16 v8, 0x0

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    const-wide/16 v6, 0x32

    cmp-long v6, v4, v6

    if-lez v6, :cond_1

    .line 413
    :cond_0
    iput-wide v4, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:J

    .line 414
    iput v1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->g:F

    .line 415
    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->d:F

    .line 416
    iput-wide v2, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:J

    .line 419
    :cond_1
    return v0
.end method

.method private a(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 512
    invoke-static {}, LakQ;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 513
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->c(Landroid/view/View;)F

    move-result v0

    .line 515
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b(Landroid/view/View;)F

    move-result v0

    goto :goto_0
.end method

.method public static a(ZLjava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;
    .locals 2

    .prologue
    .line 83
    new-instance v0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;-><init>()V

    .line 84
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 85
    invoke-static {v1, p0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a(Landroid/os/Bundle;Z)V

    .line 86
    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->e(Landroid/os/Bundle;)V

    .line 87
    iput-object p1, v0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->c:Ljava/lang/String;

    .line 88
    iput-object p2, v0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->d:Ljava/lang/String;

    .line 89
    return-object v0
.end method

.method private a(FF)V
    .locals 1

    .prologue
    .line 346
    iput p1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->e:F

    .line 347
    iput p2, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->f:F

    .line 348
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a(Landroid/view/View;F)V

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->d:Landroid/view/View;

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a(Landroid/view/View;F)V

    .line 350
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    .line 351
    return-void
.end method

.method private static a(Landroid/os/Bundle;Z)V
    .locals 1

    .prologue
    .line 211
    const-string v0, "isBelowCovered"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 212
    return-void
.end method

.method private a(Landroid/view/View;F)V
    .locals 1

    .prologue
    .line 530
    invoke-static {}, LakQ;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 531
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->c(Landroid/view/View;F)V

    .line 535
    :goto_0
    return-void

    .line 533
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b(Landroid/view/View;F)V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->w()V

    return-void
.end method

.method private a(II)Z
    .locals 3

    .prologue
    .line 319
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v0

    if-eq v0, p2, :cond_1

    .line 320
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->d:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->d:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    invoke-virtual {v0, p1, p2, v1, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 321
    const/4 v0, 0x1

    .line 324
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/high16 v6, 0x41700000    # 15.0f

    .line 438
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 439
    const-wide/16 v4, 0xfa

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 447
    :cond_0
    :goto_0
    return v0

    .line 443
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:F

    sub-float/2addr v1, v2

    .line 444
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:F

    sub-float/2addr v2, v3

    .line 446
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v1, v1, v6

    if-gez v1, :cond_0

    .line 447
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v1, v1, v6

    if-gez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(Landroid/view/MotionEvent;)F
    .locals 1

    .prologue
    .line 507
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    goto :goto_0
.end method

.method private b(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 520
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v0, v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    int-to-float v0, v0

    goto :goto_0
.end method

.method private b(Landroid/view/View;F)V
    .locals 4

    .prologue
    .line 538
    float-to-double v0, p2

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 539
    iget-boolean v1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:Z

    if-eqz v1, :cond_0

    .line 540
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 544
    :goto_0
    return-void

    .line 542
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    goto :goto_0
.end method

.method private b(Z)V
    .locals 1

    .prologue
    .line 481
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->c:Ljava/lang/Boolean;

    .line 482
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->v()V

    .line 483
    return-void
.end method

.method private c(Landroid/view/View;)F
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 525
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v0

    goto :goto_0
.end method

.method private c(Landroid/view/View;F)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 548
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:Z

    if-eqz v0, :cond_0

    .line 549
    invoke-virtual {p1, p2}, Landroid/view/View;->setX(F)V

    .line 553
    :goto_0
    return-void

    .line 551
    :cond_0
    invoke-virtual {p1, p2}, Landroid/view/View;->setY(F)V

    goto :goto_0
.end method

.method private n()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 305
    iget-boolean v1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:Z

    if-eqz v1, :cond_2

    .line 306
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:Lcom/google/android/apps/docs/punchwebview/DragKnobView;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->getWidth()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    if-eqz v1, :cond_1

    .line 307
    :cond_0
    iget v1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:I

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a(II)Z

    move-result v0

    .line 315
    :cond_1
    :goto_0
    return v0

    .line 310
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:Lcom/google/android/apps/docs/punchwebview/DragKnobView;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->getHeight()I

    move-result v2

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    if-eqz v1, :cond_1

    .line 311
    :cond_3
    iget v1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a(II)Z

    move-result v0

    goto :goto_0
.end method

.method private u()V
    .locals 2

    .prologue
    .line 218
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a()Landroid/view/View;

    move-result-object v0

    const-string v1, "DragKnobFragment"

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 219
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lano;->a(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:Landroid/view/ViewGroup;

    .line 220
    return-void
.end method

.method private v()V
    .locals 4

    .prologue
    .line 328
    iget v1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->j:F

    .line 329
    const/4 v0, 0x0

    .line 330
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 331
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->e:Landroid/view/View;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 332
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:Lcom/google/android/apps/docs/punchwebview/DragKnobView;

    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->setKnobPictureContentDescription(Ljava/lang/CharSequence;)V

    .line 340
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:LWY;

    invoke-interface {v2}, LWY;->a()V

    .line 342
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a(FF)V

    .line 343
    return-void

    .line 334
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->e:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 335
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:Lcom/google/android/apps/docs/punchwebview/DragKnobView;

    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->setKnobPictureContentDescription(Ljava/lang/CharSequence;)V

    .line 336
    iget v2, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    .line 337
    iget v2, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:I

    int-to-float v2, v2

    add-float/2addr v0, v2

    goto :goto_0
.end method

.method private w()V
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b(Z)V

    .line 452
    return-void

    .line 451
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 362
    iput-object p2, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:Landroid/view/ViewGroup;

    .line 363
    const-string v0, "DragKnobFragment"

    const-string v1, "onCreateView container=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 364
    sget v0, LpP;->drag_knob:I

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/punchwebview/DragKnobView;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:Lcom/google/android/apps/docs/punchwebview/DragKnobView;

    .line 365
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:Lcom/google/android/apps/docs/punchwebview/DragKnobView;

    sget v1, LpN;->drag_knob_pic:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 366
    new-instance v1, LWX;

    invoke-direct {v1, p0}, LWX;-><init>(Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 374
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:Lcom/google/android/apps/docs/punchwebview/DragKnobView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->a(LXb;)V

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:Lcom/google/android/apps/docs/punchwebview/DragKnobView;

    return-object v0
.end method

.method public a()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->m:I

    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:Landroid/view/ViewGroup;

    .line 225
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getWidth()I

    move-result v3

    if-ne v0, v3, :cond_0

    iget v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->c:I

    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    if-eq v0, v3, :cond_1

    .line 226
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->d:Landroid/view/View;

    .line 229
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:Landroid/view/ViewGroup;

    if-nez v0, :cond_3

    .line 232
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->u()V

    .line 233
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:Landroid/view/ViewGroup;

    if-nez v0, :cond_3

    .line 299
    :cond_2
    :goto_0
    return-void

    .line 238
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->d:Landroid/view/View;

    if-eqz v0, :cond_6

    .line 239
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->n()Z

    move-result v0

    .line 241
    iget-boolean v1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:Z

    if-nez v1, :cond_5

    .line 242
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a()Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a(Landroid/view/View;)F

    move-result v1

    float-to-double v2, v1

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    .line 243
    int-to-float v2, v1

    iget v3, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->j:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_4

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->j:F

    iget v3, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_4

    .line 244
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->v()V

    .line 250
    :cond_4
    :goto_1
    if-eqz v0, :cond_2

    .line 251
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->t()V

    goto :goto_0

    .line 247
    :cond_5
    iget v1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->e:F

    iget v2, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->f:F

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a(FF)V

    goto :goto_1

    .line 257
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->m:I

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->c:I

    .line 260
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 261
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v3, -0x2

    if-ne v0, v3, :cond_8

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:Z

    .line 262
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, LpL;->drag_knob_width_visible_size:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:I

    .line 264
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->e:Landroid/view/View;

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->d:Landroid/view/View;

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 275
    iget-boolean v3, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:Z

    if-eqz v3, :cond_9

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    :goto_3
    const/4 v3, -0x1

    if-ne v0, v3, :cond_a

    .line 278
    :goto_4
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    :goto_5
    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:I

    .line 279
    if-eqz v1, :cond_7

    .line 282
    iget v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:I

    iget v1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:I

    .line 290
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, LpL;->drag_knob_extra_touch_zone_per_side:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 291
    neg-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->j:F

    .line 292
    iget v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->j:F

    iget v1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->k:F

    .line 293
    iget v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->j:F

    iget v1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->k:F

    add-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->l:F

    .line 295
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->n()Z

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:Lcom/google/android/apps/docs/punchwebview/DragKnobView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/punchwebview/DragKnobView;->setVisibility(I)V

    .line 297
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->v()V

    .line 298
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->t()V

    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 261
    goto :goto_2

    .line 275
    :cond_9
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_3

    :cond_a
    move v1, v2

    goto :goto_4

    .line 278
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    goto :goto_5
.end method

.method public a(Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 392
    const-string v0, "DragKnobFragment"

    const-string v1, "onStartDrag"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:Z

    .line 394
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:F

    .line 395
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:F

    .line 396
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b(Landroid/view/MotionEvent;)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->c:F

    .line 397
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a(Landroid/view/View;)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->h:F

    .line 398
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->d:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a(Landroid/view/View;)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->i:F

    .line 399
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:J

    .line 400
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->g:F

    .line 401
    iget v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->c:F

    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->d:F

    .line 402
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a:J

    .line 403
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 404
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 487
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b(Z)V

    .line 489
    :cond_0
    return-void
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 380
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseFragment;->a_(Landroid/os/Bundle;)V

    .line 381
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->c:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 382
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->c:Ljava/lang/Boolean;

    .line 383
    if-eqz p1, :cond_1

    .line 384
    :goto_0
    if-eqz p1, :cond_0

    .line 385
    const-string v0, "isBelowCovered"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->c:Ljava/lang/Boolean;

    .line 388
    :cond_0
    return-void

    .line 383
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_0
.end method

.method public b(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 424
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a(Landroid/view/MotionEvent;)F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->c:F

    sub-float v1, v0, v1

    .line 425
    iget v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->h:F

    add-float/2addr v0, v1

    .line 426
    iget v2, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->j:F

    cmpg-float v2, v0, v2

    if-gez v2, :cond_1

    .line 427
    iget v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->j:F

    .line 428
    iget v1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->h:F

    sub-float v1, v0, v1

    .line 434
    :cond_0
    :goto_0
    iget v2, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->i:F

    add-float/2addr v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a(FF)V

    .line 435
    return-void

    .line 429
    :cond_1
    iget v2, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->k:F

    cmpl-float v2, v0, v2

    if-lez v2, :cond_0

    .line 430
    iget v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->k:F

    .line 431
    iget v1, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->h:F

    sub-float v1, v0, v1

    goto :goto_0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 355
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseFragment;->c(Landroid/os/Bundle;)V

    .line 356
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a(Landroid/os/Bundle;Z)V

    .line 357
    return-void
.end method

.method public c(Landroid/view/MotionEvent;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 456
    const-string v0, "DragKnobFragment"

    const-string v3, "onStopDrag"

    invoke-static {v0, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    iput-boolean v2, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:Z

    .line 459
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 461
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->w()V

    .line 478
    :goto_0
    return-void

    .line 465
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a(Landroid/view/MotionEvent;)F

    move-result v0

    iget v3, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->c:F

    sub-float/2addr v0, v3

    .line 466
    iget v3, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->h:F

    add-float/2addr v0, v3

    .line 467
    iget v3, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->l:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_2

    move v0, v1

    .line 469
    :goto_1
    iget-wide v4, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_1

    .line 470
    iget v3, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->g:F

    iget-wide v4, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:J

    long-to-float v4, v4

    div-float/2addr v3, v4

    .line 471
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const v5, 0x3e99999a    # 0.3f

    cmpl-float v4, v4, v5

    if-lez v4, :cond_1

    .line 472
    float-to-double v4, v3

    const-wide/16 v6, 0x0

    cmpg-double v0, v4, v6

    if-gez v0, :cond_3

    :goto_2
    move v0, v1

    .line 477
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b(Z)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 467
    goto :goto_1

    :cond_3
    move v1, v2

    .line 472
    goto :goto_2
.end method

.method public e()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 500
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseFragment;->e()V

    .line 502
    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->d:Landroid/view/View;

    .line 503
    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->b:Landroid/view/ViewGroup;

    .line 504
    return-void
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 495
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public t()V
    .locals 1

    .prologue
    .line 556
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 557
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 561
    :cond_0
    return-void
.end method
