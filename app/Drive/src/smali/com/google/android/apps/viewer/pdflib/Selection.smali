.class public Lcom/google/android/apps/viewer/pdflib/Selection;
.super Lcom/google/android/apps/viewer/util/BaseParcelable;
.source "Selection.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/viewer/pdflib/Selection;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final page:I

.field public final rects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field public final start:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

.field public final stop:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

.field public final text:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    new-instance v0, LavK;

    invoke-direct {v0}, LavK;-><init>()V

    sput-object v0, Lcom/google/android/apps/viewer/pdflib/Selection;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILcom/google/android/apps/viewer/pdflib/SelectionBoundary;Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;",
            "Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/apps/viewer/util/BaseParcelable;-><init>()V

    .line 26
    iput p1, p0, Lcom/google/android/apps/viewer/pdflib/Selection;->page:I

    .line 27
    iput-object p2, p0, Lcom/google/android/apps/viewer/pdflib/Selection;->start:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    .line 28
    iput-object p3, p0, Lcom/google/android/apps/viewer/pdflib/Selection;->stop:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    .line 29
    iput-object p4, p0, Lcom/google/android/apps/viewer/pdflib/Selection;->rects:Ljava/util/List;

    .line 30
    iput-object p5, p0, Lcom/google/android/apps/viewer/pdflib/Selection;->text:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 35
    const-string v0, "Selection(page=%d, start=%s, stop=%s, %d rects)"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/apps/viewer/pdflib/Selection;->page:I

    .line 36
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/viewer/pdflib/Selection;->start:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/viewer/pdflib/Selection;->stop:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/apps/viewer/pdflib/Selection;->rects:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 35
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    iget v0, p0, Lcom/google/android/apps/viewer/pdflib/Selection;->page:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 42
    iget-object v0, p0, Lcom/google/android/apps/viewer/pdflib/Selection;->start:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 43
    iget-object v0, p0, Lcom/google/android/apps/viewer/pdflib/Selection;->stop:Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/viewer/pdflib/Selection;->rects:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/viewer/pdflib/Selection;->text:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 46
    return-void
.end method
