.class public Lcom/google/android/apps/docs/doclist/documentopener/OfflineDocumentOpener;
.super Ljava/lang/Object;
.source "OfflineDocumentOpener.java"

# interfaces
.implements LFR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D::",
        "LFR;",
        "F::",
        "LFR;",
        ">",
        "Ljava/lang/Object;",
        "LFR;"
    }
.end annotation


# instance fields
.field private final a:LaGM;

.field private final a:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<TD;>;"
        }
    .end annotation
.end field

.field private final b:Lbxw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbxw",
            "<TF;>;"
        }
    .end annotation
.end field


# virtual methods
.method public a(LFT;LaGo;Landroid/os/Bundle;)LbsU;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LFT;",
            "LaGo;",
            "Landroid/os/Bundle;",
            ")",
            "LbsU",
            "<",
            "LDL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    sget-object v0, LacY;->a:LacY;

    invoke-interface {p2, v0}, LaGo;->a(LacY;)J

    move-result-wide v0

    .line 42
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/OfflineDocumentOpener;->a:LaGM;

    invoke-interface {v2, v0, v1}, LaGM;->a(J)LaGp;

    move-result-object v0

    .line 45
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LaGp;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    const-string v0, "OfflineDocumentOpener"

    const-string v1, "Opening an offline DB file in the editor."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/OfflineDocumentOpener;->a:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LFR;

    invoke-interface {v0, p1, p2, p3}, LFR;->a(LFT;LaGo;Landroid/os/Bundle;)LbsU;

    move-result-object v0

    .line 51
    :goto_0
    return-object v0

    .line 50
    :cond_0
    const-string v0, "OfflineDocumentOpener"

    const-string v1, "Opening relating offline resource."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/OfflineDocumentOpener;->b:Lbxw;

    invoke-interface {v0}, Lbxw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LFR;

    invoke-interface {v0, p1, p2, p3}, LFR;->a(LFT;LaGo;Landroid/os/Bundle;)LbsU;

    move-result-object v0

    goto :goto_0
.end method
