.class public Lcom/google/android/apps/viewer/pdflib/PdfDocument;
.super Ljava/lang/Object;
.source "PdfDocument.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "PdfDocument"


# instance fields
.field private numPages:I

.field private final pdfDocPtr:J

.field private status:LavJ;


# direct methods
.method protected constructor <init>(J)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    sget-object v0, LavJ;->a:LavJ;

    iput-object v0, p0, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->status:LavJ;

    .line 42
    iput-wide p1, p0, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->pdfDocPtr:J

    .line 43
    return-void
.end method

.method public static native createFromBytes([BLjava/lang/String;)Lcom/google/android/apps/viewer/pdflib/PdfDocument;
.end method

.method public static native createFromFd(ILjava/lang/String;)Lcom/google/android/apps/viewer/pdflib/PdfDocument;
.end method

.method public static native createFromFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/viewer/pdflib/PdfDocument;
.end method

.method static loadLibfoxit()V
    .locals 1

    .prologue
    .line 172
    const-string v0, "foxit"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 173
    return-void
.end method

.method private native renderPageFd(IIII)Z
.end method

.method private native renderTileFd(IIIIIIII)Z
.end method


# virtual methods
.method public native applyPassword(Ljava/lang/String;)V
.end method

.method public native destroy()V
.end method

.method public native getPageDimensions(I)Lcom/google/android/apps/viewer/client/Dimensions;
.end method

.method public native getPageText(I)Ljava/lang/String;
.end method

.method public getStatus()LavJ;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->status:LavJ;

    return-object v0
.end method

.method protected initError()V
    .locals 3

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->status:LavJ;

    sget-object v1, LavJ;->c:LavJ;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Can\'t change a loaded document"

    invoke-static {v0, v1}, LauV;->a(ZLjava/lang/String;)V

    .line 58
    sget-object v0, LavJ;->d:LavJ;

    iput-object v0, p0, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->status:LavJ;

    .line 59
    const-string v0, "PdfDocument"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error with doc "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    return-void

    .line 57
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected initLoaded(I)V
    .locals 3

    .prologue
    .line 46
    sget-object v0, LavJ;->c:LavJ;

    iput-object v0, p0, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->status:LavJ;

    .line 47
    iput p1, p0, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->numPages:I

    .line 48
    const-string v0, "PdfDocument"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Created "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    return-void
.end method

.method protected initRequiresPassword()V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->status:LavJ;

    sget-object v1, LavJ;->c:LavJ;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Can\'t change a loaded document"

    invoke-static {v0, v1}, LauV;->a(ZLjava/lang/String;)V

    .line 53
    sget-object v0, LavJ;->b:LavJ;

    iput-object v0, p0, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->status:LavJ;

    .line 54
    return-void

    .line 52
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLoaded()Z
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->status:LavJ;

    sget-object v1, LavJ;->c:LavJ;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public numPages()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->numPages:I

    return v0
.end method

.method public native renderPage(ILandroid/graphics/Bitmap;)Z
.end method

.method public renderPageFd(ILcom/google/android/apps/viewer/client/Dimensions;Landroid/os/ParcelFileDescriptor;)Z
    .locals 3

    .prologue
    .line 108
    iget v0, p2, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    iget v1, p2, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    invoke-virtual {p3}, Landroid/os/ParcelFileDescriptor;->getFd()I

    move-result v2

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->renderPageFd(IIII)Z

    move-result v0

    return v0
.end method

.method public native renderTile(IIIIILandroid/graphics/Bitmap;)Z
.end method

.method public renderTileFd(IIIIILcom/google/android/apps/viewer/client/Dimensions;Landroid/os/ParcelFileDescriptor;)Z
    .locals 9

    .prologue
    .line 133
    iget v6, p6, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    iget v7, p6, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    .line 134
    invoke-virtual/range {p7 .. p7}, Landroid/os/ParcelFileDescriptor;->getFd()I

    move-result v8

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    .line 133
    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->renderTileFd(IIIIIIII)Z

    move-result v0

    return v0
.end method

.method public requiresPassword()Z
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->status:LavJ;

    sget-object v1, LavJ;->b:LavJ;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public native searchPageText(ILjava/lang/String;)Lcom/google/android/apps/viewer/pdflib/MatchRects;
.end method

.method public native selectPageText(ILcom/google/android/apps/viewer/pdflib/SelectionBoundary;Lcom/google/android/apps/viewer/pdflib/SelectionBoundary;)Lcom/google/android/apps/viewer/pdflib/Selection;
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 163
    const-string v0, "PdfDocument(%x, %d pages) [%s]"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->pdfDocPtr:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->numPages:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/viewer/pdflib/PdfDocument;->status:LavJ;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
