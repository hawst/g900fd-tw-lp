.class public Lcom/google/android/apps/docs/view/FixedSizeImageView;
.super Landroid/widget/ImageView;
.source "FixedSizeImageView.java"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/FixedSizeImageView;->a:Z

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/FixedSizeImageView;->a:Z

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/FixedSizeImageView;->a:Z

    .line 31
    return-void
.end method


# virtual methods
.method public requestLayout()V
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/google/android/apps/docs/view/FixedSizeImageView;->a:Z

    if-nez v0, :cond_0

    .line 59
    invoke-super {p0}, Landroid/widget/ImageView;->requestLayout()V

    .line 61
    :cond_0
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 35
    iget-boolean v0, p0, Lcom/google/android/apps/docs/view/FixedSizeImageView;->a:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 36
    iput-boolean v1, p0, Lcom/google/android/apps/docs/view/FixedSizeImageView;->a:Z

    .line 38
    :try_start_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    iput-boolean v2, p0, Lcom/google/android/apps/docs/view/FixedSizeImageView;->a:Z

    .line 42
    return-void

    :cond_0
    move v0, v2

    .line 35
    goto :goto_0

    .line 40
    :catchall_0
    move-exception v0

    iput-boolean v2, p0, Lcom/google/android/apps/docs/view/FixedSizeImageView;->a:Z

    throw v0
.end method

.method public setImageResource(I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 46
    iget-boolean v0, p0, Lcom/google/android/apps/docs/view/FixedSizeImageView;->a:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 47
    iput-boolean v1, p0, Lcom/google/android/apps/docs/view/FixedSizeImageView;->a:Z

    .line 49
    :try_start_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    iput-boolean v2, p0, Lcom/google/android/apps/docs/view/FixedSizeImageView;->a:Z

    .line 53
    return-void

    :cond_0
    move v0, v2

    .line 46
    goto :goto_0

    .line 51
    :catchall_0
    move-exception v0

    iput-boolean v2, p0, Lcom/google/android/apps/docs/view/FixedSizeImageView;->a:Z

    throw v0
.end method
