.class public Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;
.super Landroid/widget/RelativeLayout;
.source "PunchPresentationView.java"


# instance fields
.field private a:LWN;

.field private a:LWO;

.field public a:LXz;

.field public a:LYO;

.field public a:Laja;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laja",
            "<",
            "LXR;",
            ">;"
        }
    .end annotation
.end field

.field private a:Landroid/view/ViewGroup;

.field private a:Landroid/webkit/WebView;

.field private a:Ljava/lang/String;

.field public a:LqK;

.field private final a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 135
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 136
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Z

    .line 137
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a()V

    .line 138
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 141
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 142
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Z

    .line 143
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a()V

    .line 144
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 147
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 148
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Z

    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a()V

    .line 150
    return-void
.end method

.method private final a()V
    .locals 2

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lajt;->a(Landroid/content/Context;)Lbuu;

    move-result-object v0

    invoke-interface {v0, p0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 155
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, LpP;->central_slide_view:I

    invoke-static {v0, v1, p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 156
    sget v0, LpN;->punch_web_view_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Landroid/view/ViewGroup;

    .line 157
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Z

    if-eqz v0, :cond_0

    .line 158
    sget v0, LpN;->punch_central_slide_view_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 159
    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 162
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, LpR;->punch_no_title_slide_content_description:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Ljava/lang/String;

    .line 163
    return-void
.end method

.method private a(LYQ;)V
    .locals 0

    .prologue
    .line 239
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->c()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;LYQ;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a(LYQ;)V

    return-void
.end method

.method private b()V
    .locals 7

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 178
    iget-object v6, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Landroid/webkit/WebView;

    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LYO;

    invoke-interface {v0}, LYO;->a()LYK;

    move-result-object v0

    .line 179
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LXz;

    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LYO;

    iget-object v4, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Laja;

    iget-object v5, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LqK;

    invoke-virtual/range {v0 .. v5}, LYK;->a(Landroid/content/Context;LXz;LYO;Lbxw;LqK;)Landroid/view/View$OnTouchListener;

    move-result-object v0

    .line 178
    invoke-virtual {v6, v0}, Landroid/webkit/WebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 181
    :cond_0
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->b()V

    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LXz;

    invoke-interface {v0}, LXz;->c()I

    move-result v0

    if-nez v0, :cond_1

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 202
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LXz;

    invoke-interface {v0}, LXz;->d()I

    move-result v0

    .line 203
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LXz;

    invoke-interface {v1, v0}, LXz;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 204
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LXz;

    invoke-interface {v1, v0}, LXz;->b(I)Ljava/lang/String;

    move-result-object v1

    .line 205
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    .line 206
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Landroid/view/ViewGroup;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Ljava/lang/String;

    :goto_2
    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->sendAccessibilityEvent(I)V

    goto :goto_0

    .line 205
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 206
    goto :goto_2
.end method

.method private d()V
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LWN;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 215
    new-instance v0, LXN;

    invoke-direct {v0, p0}, LXN;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LWN;

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LXz;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LWN;

    invoke-interface {v0, v1}, LXz;->a(LXA;)V

    .line 230
    return-void

    .line 214
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LWN;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LXz;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LWN;

    invoke-interface {v0, v1}, LXz;->b(LXA;)V

    .line 235
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LWN;

    .line 236
    return-void

    .line 233
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LWO;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 243
    new-instance v0, LXO;

    invoke-direct {v0, p0}, LXO;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LWO;

    .line 254
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LYO;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LWO;

    invoke-interface {v0, v1}, LYO;->a(LYP;)V

    .line 255
    return-void

    .line 242
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LWO;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LYO;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LWO;

    invoke-interface {v0, v1}, LYO;->b(LYP;)V

    .line 260
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LWO;

    .line 261
    return-void

    .line 258
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 192
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 193
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->d()V

    .line 194
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->f()V

    .line 195
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->e()V

    .line 186
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->g()V

    .line 187
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 188
    return-void
.end method

.method public setWebView(Landroid/webkit/WebView;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Landroid/webkit/WebView;

    .line 173
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->b()V

    .line 174
    return-void
.end method
