.class public abstract Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "ConfirmSharingDialogFragment.java"


# instance fields
.field public a:Labw;

.field private a:Landroid/content/DialogInterface$OnShowListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    .line 27
    new-instance v0, Labu;

    invoke-direct {v0, p0}, Labu;-><init>(Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;->a:Landroid/content/DialogInterface$OnShowListener;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 37
    new-instance v0, Labv;

    invoke-direct {v0, p0}, Labv;-><init>(Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;)V

    .line 44
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;->a()LH;

    move-result-object v1

    invoke-static {v1}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;->a:Landroid/content/DialogInterface$OnShowListener;

    .line 45
    invoke-virtual {v1, v2}, LEU;->a(Landroid/content/DialogInterface$OnShowListener;)LEU;

    move-result-object v1

    .line 46
    invoke-virtual {v1, p1}, LEU;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 47
    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    .line 48
    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    .line 49
    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 51
    return-object v0
.end method

.method public i(Z)V
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;->a:Labw;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Labw;->a(Landroid/os/Bundle;Z)V

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;->a()V

    .line 57
    return-void
.end method
