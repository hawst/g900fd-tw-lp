.class public Lcom/google/android/apps/docs/view/FixedSizeTextView;
.super Landroid/widget/TextView;
.source "FixedSizeTextView.java"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/FixedSizeTextView;->a:Z

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/FixedSizeTextView;->a:Z

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/view/FixedSizeTextView;->a:Z

    .line 36
    return-void
.end method


# virtual methods
.method public requestLayout()V
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/google/android/apps/docs/view/FixedSizeTextView;->a:Z

    if-nez v0, :cond_0

    .line 59
    invoke-super {p0}, Landroid/widget/TextView;->requestLayout()V

    .line 61
    :cond_0
    return-void
.end method

.method public setTextAndTypefaceNoLayout(Ljava/lang/String;Landroid/graphics/Typeface;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 43
    iget-boolean v0, p0, Lcom/google/android/apps/docs/view/FixedSizeTextView;->a:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 44
    iput-boolean v1, p0, Lcom/google/android/apps/docs/view/FixedSizeTextView;->a:Z

    .line 46
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/view/FixedSizeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    if-eqz p2, :cond_0

    .line 49
    invoke-virtual {p0, p2}, Lcom/google/android/apps/docs/view/FixedSizeTextView;->setTypeface(Landroid/graphics/Typeface;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/apps/docs/view/FixedSizeTextView;->a:Z

    .line 54
    return-void

    :cond_1
    move v0, v2

    .line 43
    goto :goto_0

    .line 52
    :catchall_0
    move-exception v0

    iput-boolean v2, p0, Lcom/google/android/apps/docs/view/FixedSizeTextView;->a:Z

    throw v0
.end method
