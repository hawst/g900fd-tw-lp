.class public Lcom/google/android/apps/docs/print/KitKatPrintActivity;
.super Landroid/app/Activity;
.source "KitKatPrintActivity.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation


# instance fields
.field private final a:Landroid/print/PrintDocumentInfo;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 44
    new-instance v0, Landroid/print/PrintDocumentInfo$Builder;

    const-string v1, "Unknown document name"

    invoke-direct {v0, v1}, Landroid/print/PrintDocumentInfo$Builder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 46
    invoke-virtual {v0, v1}, Landroid/print/PrintDocumentInfo$Builder;->setContentType(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v0

    const/4 v1, -0x1

    .line 47
    invoke-virtual {v0, v1}, Landroid/print/PrintDocumentInfo$Builder;->setPageCount(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/print/PrintDocumentInfo$Builder;->build()Landroid/print/PrintDocumentInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/print/KitKatPrintActivity;->a:Landroid/print/PrintDocumentInfo;

    .line 160
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/print/KitKatPrintActivity;)Landroid/print/PrintDocumentInfo;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/docs/print/KitKatPrintActivity;->a:Landroid/print/PrintDocumentInfo;

    return-object v0
.end method

.method private a(Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/print/KitKatPrintActivity;->b(Landroid/net/Uri;)V

    .line 91
    return-void
.end method

.method private a(Landroid/net/Uri;Landroid/os/ParcelFileDescriptor;)V
    .locals 4

    .prologue
    .line 148
    const-string v0, "PrintActivity"

    const-string v1, "onWrite - writing to print framework..."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    new-instance v0, Lalp;

    invoke-direct {v0}, Lalp;-><init>()V

    .line 150
    invoke-virtual {p0}, Lcom/google/android/apps/docs/print/KitKatPrintActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 151
    invoke-virtual {v1, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 152
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 153
    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Lalo;->a(Ljava/io/InputStream;Ljava/io/OutputStream;Z)V

    .line 154
    const-string v0, "PrintActivity"

    const-string v1, "onWrite - finished writing to print framework."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/print/KitKatPrintActivity;Landroid/net/Uri;Landroid/os/ParcelFileDescriptor;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/print/KitKatPrintActivity;->a(Landroid/net/Uri;Landroid/os/ParcelFileDescriptor;)V

    return-void
.end method

.method private b(Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 95
    const-string v0, "PrintActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Showing print dialog: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    const-string v0, "print"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/print/KitKatPrintActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/print/PrintManager;

    .line 99
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    new-instance v2, LWB;

    invoke-direct {v2, p0, p1}, LWB;-><init>(Lcom/google/android/apps/docs/print/KitKatPrintActivity;Landroid/net/Uri;)V

    new-instance v3, Landroid/print/PrintAttributes$Builder;

    invoke-direct {v3}, Landroid/print/PrintAttributes$Builder;-><init>()V

    .line 142
    invoke-virtual {v3}, Landroid/print/PrintAttributes$Builder;->build()Landroid/print/PrintAttributes;

    move-result-object v3

    .line 99
    invoke-virtual {v0, v1, v2, v3}, Landroid/print/PrintManager;->print(Ljava/lang/String;Landroid/print/PrintDocumentAdapter;Landroid/print/PrintAttributes;)Landroid/print/PrintJob;

    .line 143
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    const-string v0, "PrintActivity"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    invoke-virtual {p0}, Lcom/google/android/apps/docs/print/KitKatPrintActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 56
    invoke-virtual {v0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LWE;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/print/KitKatPrintActivity;->a(Landroid/net/Uri;)V

    .line 61
    :goto_0
    return-void

    .line 59
    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/print/KitKatPrintActivity;->b(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 65
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 66
    const-string v0, "PrintActivity"

    const-string v1, "onStart"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 71
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 72
    const-string v0, "PrintActivity"

    const-string v1, "onStop"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    return-void
.end method
