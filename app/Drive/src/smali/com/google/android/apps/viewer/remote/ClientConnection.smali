.class public Lcom/google/android/apps/viewer/remote/ClientConnection;
.super Ljava/lang/Object;
.source "ClientConnection.java"

# interfaces
.implements Lcom/google/android/apps/viewer/client/Projector$CloseCallback;
.implements Lcom/google/android/apps/viewer/client/Projector$FileInfoSource;


# instance fields
.field private a:Landroid/os/Messenger;

.field private a:LauM;


# direct methods
.method private a(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/apps/viewer/remote/ClientConnection;->a:Landroid/os/Messenger;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Client service not bound"

    invoke-static {v0, v1}, LauV;->a(ZLjava/lang/String;)V

    .line 114
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/viewer/remote/ClientConnection;->a:Landroid/os/Messenger;

    invoke-virtual {v0, p1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    return-void

    .line 112
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 115
    :catch_0
    move-exception v0

    .line 116
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 122
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 123
    const/4 v1, 0x4

    iput v1, v0, Landroid/os/Message;->what:I

    .line 124
    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/remote/ClientConnection;->a(Landroid/os/Message;)V

    .line 125
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x0

    return v0
.end method

.method public getFileInfo(ILauO;)V
    .locals 3

    .prologue
    .line 89
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 90
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 91
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 93
    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/remote/ClientConnection;->a(Landroid/os/Message;)V

    .line 97
    :try_start_0
    const-string v0, "ClientConnection"

    const-string v1, "Waiting for FileInfos from the client."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    const-wide/16 v0, 0x2710

    :try_start_1
    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V

    .line 101
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/viewer/remote/ClientConnection;->a:LauM;

    .line 107
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/viewer/remote/ClientConnection;->a:LauM;

    .line 108
    invoke-interface {p2, v0}, LauO;->a(LauM;)V

    .line 109
    return-void

    .line 101
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    .line 102
    :catch_0
    move-exception v0

    .line 103
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 161
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "ClientConnection cannot be parcelled."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
