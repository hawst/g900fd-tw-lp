.class public Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;
.super Landroid/widget/LinearLayout;
.source "SpeakerNotesPresence.java"


# instance fields
.field private a:LWN;

.field public a:LXz;

.field private a:LZM;

.field final a:Landroid/content/Context;

.field private a:Landroid/widget/ImageView;

.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 57
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a:Landroid/content/Context;

    .line 58
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a()V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 69
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a:Landroid/content/Context;

    .line 70
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a()V

    .line 71
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;)LZM;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a:LZM;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a:Landroid/content/Context;

    invoke-static {v0}, Lajt;->a(Landroid/content/Context;)Lbuu;

    move-result-object v0

    invoke-interface {v0, p0}, Lbuu;->a(Ljava/lang/Object;)V

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a:Landroid/content/Context;

    sget v1, LpP;->speaker_note_presence:I

    invoke-static {v0, v1, p0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 91
    sget v0, LpN;->notes_present:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a:Landroid/widget/ImageView;

    .line 92
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a(Z)V

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a:Landroid/widget/ImageView;

    new-instance v1, LZK;

    invoke-direct {v1, p0}, LZK;-><init>(Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->c()V

    .line 105
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->b()V

    .line 106
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->b()V

    return-void
.end method

.method private a(Z)V
    .locals 5

    .prologue
    .line 151
    const-string v0, "SpeakerNotesPresence"

    const-string v1, "in setAreSpeakerNotesPresent %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 152
    iput-boolean p1, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a:Z

    .line 153
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a:Z

    if-eqz v0, :cond_0

    sget v0, LpM;->punch_speaker_notes_present_large:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 156
    iget-boolean v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a:Z

    if-eqz v0, :cond_1

    sget v0, LpR;->punch_speaker_notes_present_content_description:I

    .line 162
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 163
    return-void

    .line 153
    :cond_0
    sget v0, LpM;->punch_speaker_notes_empty:I

    goto :goto_0

    .line 156
    :cond_1
    sget v0, LpR;->punch_speaker_notes_absent:I

    goto :goto_1
.end method

.method private b()V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 109
    const-string v2, "SpeakerNotesPresence"

    const-string v3, "in update"

    invoke-static {v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a:LXz;

    invoke-interface {v2}, LXz;->c()I

    move-result v2

    if-eqz v2, :cond_0

    .line 111
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a:LXz;

    invoke-interface {v2}, LXz;->d()I

    move-result v2

    .line 112
    const-string v3, "SpeakerNotesPresence"

    const-string v4, "In SpeakerNotesPresence.update() slideIndex=%s"

    new-array v5, v0, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v3, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 113
    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a:LXz;

    invoke-interface {v3, v2}, LXz;->a(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a(Z)V

    .line 115
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 113
    goto :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a:LWN;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 125
    new-instance v0, LZL;

    invoke-direct {v0, p0}, LZL;-><init>(Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a:LWN;

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a:LXz;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a:LWN;

    invoke-interface {v0, v1}, LXz;->a(LXA;)V

    .line 143
    return-void

    .line 124
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a:LWN;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a:LXz;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a:LWN;

    invoke-interface {v0, v1}, LXz;->b(LXA;)V

    .line 148
    return-void

    .line 146
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->d()V

    .line 120
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 121
    return-void
.end method

.method public setListener(LZM;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesPresence;->a:LZM;

    .line 85
    return-void
.end method
