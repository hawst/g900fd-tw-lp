.class public abstract Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "OperationDialogFragment.java"


# instance fields
.field public a:LaGM;

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->d:Landroid/view/View;

    .line 63
    return-void
.end method


# virtual methods
.method protected a()Landroid/app/AlertDialog;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 133
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->a()LH;

    move-result-object v1

    .line 134
    const-string v0, "layout_inflater"

    .line 135
    invoke-virtual {v1, v0}, LH;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 137
    invoke-static {v1}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v1

    .line 138
    sget v2, Lxe;->operation_dialog:I

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->d:Landroid/view/View;

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->d:Landroid/view/View;

    invoke-virtual {v1, v0}, LEU;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 141
    new-instance v0, LFj;

    invoke-direct {v0, p0}, LFj;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;)V

    .line 148
    const v2, 0x104000a

    invoke-virtual {v1, v2, v3}, LEU;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 149
    const/high16 v2, 0x1040000

    invoke-virtual {v1, v2, v3}, LEU;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 150
    new-instance v2, LFk;

    invoke-direct {v2, p0, v0}, LFk;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, v2}, LEU;->a(Landroid/content/DialogInterface$OnShowListener;)LEU;

    .line 159
    invoke-virtual {v1}, LEU;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 160
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->d:Landroid/view/View;

    sget v2, Lxc;->new_name:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 163
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 164
    new-instance v2, LFl;

    invoke-direct {v2, p0, v1}, LFl;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 174
    return-object v1
.end method

.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->a()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/app/AlertDialog;)V
    .locals 0

    .prologue
    .line 183
    return-void
.end method

.method public a(Landroid/app/Dialog;ILjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/16 v2, 0x8

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 105
    if-eqz v0, :cond_1

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p2, :cond_1

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 109
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->d:Landroid/view/View;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->d:Landroid/view/View;

    sget v3, Lxc;->error_status_message:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 112
    const/4 v3, 0x2

    if-ne p2, v3, :cond_2

    .line 113
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 114
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->d:Landroid/view/View;

    sget v3, Lxc;->sync_in_progress:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v0, 0x1

    if-ne p2, v0, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 122
    if-eqz p2, :cond_0

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->d:Landroid/view/View;

    sget v1, Lxc;->item_name:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 116
    :cond_2
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_3
    move v0, v2

    .line 119
    goto :goto_2
.end method

.method protected b()Landroid/view/View;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->d:Landroid/view/View;

    return-object v0
.end method

.method public abstract t()V
.end method

.method public abstract u()V
.end method
