.class public Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;
.super Lrm;
.source "OpenUrlActivityDelegate.java"

# interfaces
.implements LFq;


# instance fields
.field public a:LSF;

.field public a:LSs;

.field public a:LTF;

.field public a:LUD;

.field public a:LUM;

.field public a:LUQ;

.field public a:LUT;

.field private a:LaFO;

.field public a:LaKR;

.field public a:Lala;

.field public a:Lald;

.field public a:Landroid/content/Context;
    .annotation runtime Lajg;
    .end annotation
.end field

.field private a:Landroid/net/Uri;

.field private a:[Landroid/accounts/Account;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Lrm;-><init>()V

    .line 119
    iput-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LaFO;

    .line 121
    iput-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Landroid/net/Uri;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;)LaFO;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LaFO;

    return-object v0
.end method

.method static a([Landroid/accounts/Account;[BLjava/lang/String;)LaFO;
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 220
    :try_start_0
    const-string v0, "MD5"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 222
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p0, v0

    .line 223
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 224
    invoke-virtual {v1}, Ljava/security/MessageDigest;->reset()V

    .line 225
    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/security/MessageDigest;->update([B)V

    .line 226
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v4

    .line 227
    invoke-static {p1, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 228
    iget-object v0, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 234
    :goto_1
    return-object v0

    .line 222
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 231
    :catch_0
    move-exception v0

    .line 232
    const-string v0, "OpenUrlActivity"

    const-string v1, "MD5 digest algorithm not found"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;)LqK;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LqK;

    return-object v0
.end method

.method private a(LUF;Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 370
    sget-object v0, LUF;->d:LUF;

    invoke-virtual {v0, p1}, LUF;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 373
    const-string v0, "OpenUrlActivity"

    const-string v1, "Exception processing entry: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 374
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Lala;

    .line 375
    invoke-virtual {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LaFO;

    const/4 v2, 0x0

    .line 374
    invoke-static {p0, v0, v1, v2}, Lala;->a(Landroid/content/Context;Landroid/net/Uri;LaFO;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Landroid/content/Intent;)V

    .line 380
    :goto_0
    return-void

    .line 377
    :cond_0
    invoke-virtual {p1}, LUF;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 378
    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private a(LUP;Lcom/google/android/gms/drive/database/data/ResourceSpec;Landroid/net/Uri;Z)V
    .locals 8

    .prologue
    .line 343
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LUD;

    invoke-virtual {v0, p2}, LUD;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LbsU;

    move-result-object v7

    .line 344
    sget v0, Lxi;->open_url_getting_entry:I

    .line 345
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 344
    invoke-static {p0, v7, v0}, Lse;->a(Landroid/app/Activity;LbsU;Ljava/lang/String;)Landroid/app/ProgressDialog;

    move-result-object v6

    .line 346
    new-instance v0, LUG;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p2

    move v5, p4

    invoke-direct/range {v0 .. v6}, LUG;-><init>(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;LUP;Landroid/net/Uri;Lcom/google/android/gms/drive/database/data/ResourceSpec;ZLandroid/app/ProgressDialog;)V

    .line 366
    invoke-static {}, Lanj;->a()Ljava/util/concurrent/Executor;

    move-result-object v1

    .line 346
    invoke-static {v7, v0, v1}, LbsK;->a(LbsU;LbsJ;Ljava/util/concurrent/Executor;)V

    .line 367
    return-void
.end method

.method private a(Landroid/app/ProgressDialog;)V
    .locals 1

    .prologue
    .line 333
    invoke-virtual {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 334
    invoke-virtual {p1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 336
    :cond_0
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 324
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->startActivityForResult(Landroid/content/Intent;I)V

    .line 325
    invoke-virtual {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxi;->opening_in_app:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 327
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 328
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->k()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;LUP;Lcom/google/android/gms/drive/database/data/ResourceSpec;Landroid/net/Uri;Z)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(LUP;Lcom/google/android/gms/drive/database/data/ResourceSpec;Landroid/net/Uri;Z)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Landroid/app/ProgressDialog;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Lald;

    invoke-interface {v0, p1, p2}, Lald;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 384
    const-string v0, "OpenUrlActivity"

    invoke-static {v0, p2, p1}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 385
    invoke-virtual {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->finish()V

    .line 386
    return-void
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 301
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    instance-of v0, p1, Ljava/lang/InterruptedException;

    if-eqz v0, :cond_1

    .line 303
    :cond_0
    const-string v0, "OpenUrlActivity"

    const-string v1, "User canceled entry lookup."

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    invoke-virtual {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->finish()V

    .line 309
    :goto_0
    return-void

    .line 306
    :cond_1
    invoke-static {p1}, LUF;->a(Ljava/lang/Throwable;)LUF;

    move-result-object v0

    .line 307
    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(LUF;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LaKR;

    invoke-interface {v0}, LaKR;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LTF;

    .line 291
    invoke-static {v0, v1}, LUN;->a(Landroid/net/Uri;LTF;)LbsU;

    move-result-object v0

    .line 296
    :goto_0
    new-instance v1, LUH;

    invoke-direct {v1, p0}, LUH;-><init>(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;)V

    .line 297
    invoke-static {}, Lanj;->a()Ljava/util/concurrent/Executor;

    move-result-object v2

    .line 296
    invoke-static {v0, v1, v2}, LbsK;->a(LbsU;LbsJ;Ljava/util/concurrent/Executor;)V

    .line 298
    return-void

    .line 294
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Landroid/net/Uri;

    invoke-static {v0}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v0

    goto :goto_0
.end method

.method private k()V
    .locals 2

    .prologue
    .line 312
    const-string v0, "OpenUrlActivity"

    const-string v1, "Redirect to browser."

    invoke-static {v0, v1}, LalV;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LUM;

    iget-object v1, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LUM;->a(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 314
    if-eqz v0, :cond_0

    .line 315
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->startActivity(Landroid/content/Intent;)V

    .line 316
    invoke-virtual {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->finish()V

    .line 321
    :goto_0
    return-void

    .line 318
    :cond_0
    const-string v0, "OpenUrlActivity"

    const-string v1, "Couldn\'t find default browser."

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    invoke-virtual {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxi;->error_internal_error_html:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private l()V
    .locals 1

    .prologue
    .line 389
    invoke-virtual {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a()LM;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a(LM;)Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;

    .line 390
    return-void
.end method


# virtual methods
.method public a(Landroid/accounts/Account;)V
    .locals 1

    .prologue
    .line 403
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LaFO;

    .line 404
    invoke-direct {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->j()V

    .line 405
    return-void
.end method

.method public f()V
    .locals 0

    .prologue
    .line 409
    invoke-virtual {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->finish()V

    .line 410
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 394
    invoke-super {p0, p1, p2, p3}, Lrm;->onActivityResult(IILandroid/content/Intent;)V

    .line 395
    if-eqz p1, :cond_0

    .line 396
    const-string v0, "OpenUrlActivity"

    const-string v1, "Invalid request code in activity result."

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->finish()V

    .line 399
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 127
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 129
    invoke-virtual {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v7

    .line 130
    const-string v0, "android.intent.action.VIEW"

    invoke-virtual {v7}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid intent: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v6}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 206
    :goto_0
    return-void

    .line 135
    :cond_0
    invoke-virtual {v7}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Landroid/net/Uri;

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Landroid/net/Uri;

    if-nez v0, :cond_1

    .line 137
    const-string v0, "URL is not specified."

    invoke-direct {p0, v0, v6}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 141
    :cond_1
    const-string v0, "accountName"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LaFO;

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LaFO;

    if-nez v0, :cond_4

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LSF;

    invoke-interface {v0}, LSF;->a()[Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:[Landroid/accounts/Account;

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:[Landroid/accounts/Account;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:[Landroid/accounts/Account;

    array-length v0, v0

    if-ge v0, v2, :cond_3

    .line 145
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lxi;->google_account_missing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v6}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 148
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:[Landroid/accounts/Account;

    array-length v0, v0

    if-ne v0, v2, :cond_4

    .line 149
    const-string v0, "OpenUrlActivity"

    const-string v1, "Automatically selecting accountId from device"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:[Landroid/accounts/Account;

    aget-object v0, v0, v3

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LaFO;

    .line 156
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LaFO;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LSs;

    if-eqz v0, :cond_5

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LSs;

    iget-object v1, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Landroid/content/Context;

    invoke-interface {v0, v1, v7}, LSs;->a(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LaFO;

    .line 161
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LaFO;

    if-nez v0, :cond_7

    .line 162
    const-string v0, "account_query_uri"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 164
    if-eqz v1, :cond_7

    .line 165
    new-array v2, v2, [Ljava/lang/String;

    const-string v0, "account_name"

    aput-object v0, v2, v3

    .line 168
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v2, v0

    .line 174
    :goto_1
    if-eqz v2, :cond_7

    .line 176
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 177
    const-string v0, "account_name"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 178
    if-ltz v0, :cond_6

    .line 179
    const-string v3, "OpenUrlActivity"

    const-string v4, "attempting to obtain accountId provided via %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-static {v3, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 180
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LaFO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 184
    :cond_6
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 192
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LaFO;

    if-nez v0, :cond_8

    .line 193
    const-string v0, "digest"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 194
    const-string v1, "salt"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 195
    if-eqz v0, :cond_8

    if-eqz v1, :cond_8

    .line 196
    const-string v2, "OpenUrlActivity"

    const-string v3, "attempting to obtain accountId provided via digest"

    invoke-static {v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iget-object v2, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:[Landroid/accounts/Account;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a([Landroid/accounts/Account;[BLjava/lang/String;)LaFO;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LaFO;

    .line 201
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LaFO;

    if-nez v0, :cond_9

    .line 202
    invoke-direct {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->l()V

    goto/16 :goto_0

    .line 169
    :catch_0
    move-exception v0

    .line 172
    const-string v2, "OpenUrlActivity"

    const-string v3, "Error accessing ContentProvider"

    invoke-static {v2, v0, v3}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    move-object v2, v6

    goto :goto_1

    .line 184
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    .line 204
    :cond_9
    invoke-direct {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->j()V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 239
    invoke-super {p0}, Lrm;->onDestroy()V

    .line 240
    return-void
.end method
