.class public Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "PickEntryDialogFragment.java"

# interfaces
.implements LapD;


# instance fields
.field private a:LQl;

.field public a:LQr;

.field public a:LSF;

.field private a:LaFO;

.field public a:LaGR;

.field public a:LaGg;

.field public a:LaKR;

.field public a:Lafz;

.field private a:Landroid/database/ContentObserver;

.field public a:Lapn;

.field private a:LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbmY",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

.field private a:Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

.field private a:Lcom/google/android/apps/docs/view/DocListView;

.field private a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private a:Ljava/lang/Runnable;

.field private final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public a:LvL;

.field public a:Lwa;

.field private b:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field public b:LsI;

.field private c:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    .line 196
    invoke-static {}, LboS;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Ljava/util/HashMap;

    .line 210
    sget-object v0, LQl;->a:LQl;

    iput-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LQl;

    return-void
.end method

.method private A()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 521
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-eqz v0, :cond_1

    .line 522
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 535
    :cond_0
    :goto_0
    return-void

    .line 523
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LQl;

    if-eqz v0, :cond_0

    .line 524
    iput-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LQl;

    .line 525
    iput-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    .line 526
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->t()V

    .line 527
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 531
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lapn;

    invoke-virtual {v0}, Lapn;->e()V

    .line 533
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lwa;

    invoke-interface {v0, v1}, Lwa;->a(Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;)V

    goto :goto_0
.end method

.method private B()V
    .locals 3

    .prologue
    .line 724
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lwa;

    new-instance v1, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;

    iget-object v2, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    invoke-direct {v1, v2}, Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;-><init>(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V

    invoke-interface {v0, v1}, Lwa;->a(Lcom/google/android/apps/docs/app/model/navigation/NavigationPathElement;)V

    .line 725
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->t()V

    .line 726
    return-void
.end method

.method private C()V
    .locals 2

    .prologue
    .line 729
    invoke-virtual {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    .line 730
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 731
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 732
    return-void

    .line 731
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private D()V
    .locals 4

    .prologue
    .line 798
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 799
    const-string v1, "entrySpec.v2"

    iget-object v2, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 800
    const-string v1, "bundle"

    invoke-virtual {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "bundle"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 801
    invoke-virtual {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()LH;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, LH;->setResult(ILandroid/content/Intent;)V

    .line 802
    return-void
.end method

.method private E()V
    .locals 4

    .prologue
    .line 813
    const-string v0, "PickEntryDialogFragment"

    const-string v1, "in updateWorkStatus"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 814
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LSF;

    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LaFO;

    invoke-interface {v0, v1}, LSF;->b(LaFO;)Z

    move-result v0

    .line 815
    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LaKR;

    invoke-interface {v1}, LaKR;->a()Z

    move-result v1

    .line 816
    if-nez v1, :cond_0

    .line 817
    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LSF;

    iget-object v2, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LaFO;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LSF;->a(LaFO;Z)V

    .line 819
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LSF;

    iget-object v2, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LaFO;

    invoke-interface {v1, v2}, LSF;->a(LaFO;)Z

    move-result v1

    .line 821
    invoke-static {v0, v1}, LapE;->a(ZZ)LapE;

    move-result-object v2

    .line 822
    iget-object v3, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/docs/view/DocListView;->setSyncStatus(LapE;)V

    .line 826
    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Ljava/lang/Runnable;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    if-eqz v0, :cond_1

    .line 827
    new-instance v0, LQk;

    invoke-direct {v0, p0}, LQk;-><init>(Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Ljava/lang/Runnable;

    .line 838
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/docs/view/DocListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 840
    :cond_1
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;)LQl;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LQl;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;LQl;)LQl;
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LQl;

    return-object p1
.end method

.method private a(LaGu;)LaFV;
    .locals 3

    .prologue
    .line 772
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LaGg;

    .line 773
    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    invoke-interface {v0, v1}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LbmY;

    move-result-object v0

    .line 775
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 776
    const/4 v0, 0x0

    .line 789
    :goto_0
    return-object v0

    .line 779
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    if-eqz v1, :cond_1

    .line 780
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    .line 781
    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 782
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFV;

    move-result-object v0

    goto :goto_0

    .line 787
    :cond_1
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 788
    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LaGg;

    invoke-interface {v1, v0}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFZ;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFV;
    .locals 1

    .prologue
    .line 805
    if-nez p1, :cond_0

    .line 806
    const/4 v0, 0x0

    .line 808
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LaGg;

    invoke-interface {v0, p1}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFZ;

    move-result-object v0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;)Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    return-object v0
.end method

.method private a()Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 794
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    invoke-interface {v0}, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Ljava/lang/Runnable;

    return-object p1
.end method

.method private a(Landroid/widget/TextView;)V
    .locals 3

    .prologue
    .line 280
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 282
    invoke-virtual {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()LH;

    move-result-object v0

    check-cast v0, Lrm;

    .line 283
    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LaGR;

    new-instance v2, LQe;

    invoke-direct {v2, p0, v0, p1}, LQe;-><init>(Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;Lrm;Landroid/widget/TextView;)V

    invoke-virtual {v1, v2}, LaGR;->b(LaGN;)V

    .line 300
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->E()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    return-void
.end method

.method private a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 3

    .prologue
    .line 618
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->b:LsI;

    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LaFO;

    sget-object v2, LaGv;->a:LaGv;

    invoke-interface {v0, v1, v2, p1}, LsI;->a(LaFO;LaGv;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;

    move-result-object v0

    .line 620
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a(Landroid/content/Intent;I)V

    .line 621
    return-void
.end method

.method private a(LaGu;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 735
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 760
    :goto_0
    return v1

    .line 739
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Ljava/util/HashMap;

    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 740
    if-eqz v0, :cond_1

    .line 741
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0

    .line 745
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LaGg;

    .line 746
    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-interface {v0, v2}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LbmY;

    move-result-object v0

    .line 747
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 748
    iget-object v3, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LaGg;

    invoke-interface {v3, v0}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFZ;

    move-result-object v0

    .line 749
    if-eqz v0, :cond_2

    .line 752
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a(LaGu;)Z

    move-result v0

    .line 753
    if-eqz v0, :cond_2

    .line 754
    const/4 v0, 0x1

    .line 759
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Ljava/util/HashMap;

    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v0

    .line 760
    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;)Lcom/google/android/gms/drive/database/data/EntrySpec;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->c:Lcom/google/android/gms/drive/database/data/EntrySpec;

    return-object v0
.end method

.method private b()Ljava/lang/String;
    .locals 4

    .prologue
    .line 510
    invoke-virtual {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "dialogTitle"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 511
    if-eqz v0, :cond_0

    .line 516
    :goto_0
    return-object v0

    .line 515
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    const/4 v1, 0x1

    new-array v1, v1, [LaGv;

    const/4 v2, 0x0

    sget-object v3, LaGv;->a:LaGv;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->a([LaGv;)Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 516
    if-eqz v0, :cond_1

    sget v0, Lxi;->pick_entry_dialog_title_location:I

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget v0, Lxi;->pick_entry_dialog_title:I

    goto :goto_1
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->D()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    return-void
.end method

.method private b(Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 656
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LQl;

    if-nez v0, :cond_0

    .line 657
    iput-object v2, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 720
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->C()V

    .line 721
    return-void

    .line 664
    :cond_0
    if-eqz p1, :cond_9

    .line 665
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LaGg;

    invoke-interface {v0, p1}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;

    move-result-object v0

    .line 666
    if-eqz v0, :cond_8

    .line 667
    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LaGg;

    invoke-interface {v1, p1}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFZ;

    move-result-object v1

    .line 668
    if-nez v1, :cond_7

    .line 669
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a(LaGu;)LaFV;

    move-result-object v1

    move-object v3, v1

    move-object v1, v0

    .line 674
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 675
    const-string v4, "disablePreselectedEntry"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 676
    const-string v5, "entrySpec.v2"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 679
    if-eqz v1, :cond_3

    iget-object v5, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    .line 680
    invoke-interface {v1}, LaGu;->g()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1}, LaGu;->a()LaGv;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->a(Ljava/lang/String;LaGv;)Z

    move-result v5

    if-eqz v5, :cond_3

    if-eqz v4, :cond_1

    .line 681
    invoke-interface {v1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 682
    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a(LaGu;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 683
    invoke-interface {v1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 690
    :goto_2
    new-instance v1, LvN;

    invoke-direct {v1}, LvN;-><init>()V

    .line 691
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LvL;

    iget-object v4, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LaFO;

    invoke-interface {v0, v4}, LvL;->a(LaFO;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v0

    invoke-virtual {v1, v0}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    .line 692
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LvL;

    invoke-interface {v0}, LvL;->a()Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v0

    invoke-virtual {v1, v0}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    .line 693
    if-eqz v3, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->c:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v3}, LaFV;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/gms/drive/database/data/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 694
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LvL;

    .line 695
    invoke-interface {v3}, LaFV;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-interface {v0, v2}, LvL;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v0

    .line 694
    invoke-virtual {v1, v0}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    .line 696
    invoke-direct {p0, v3}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a(LaGu;)LaFV;

    move-result-object v0

    .line 697
    if-eqz v0, :cond_4

    .line 698
    invoke-interface {v0}, LaFV;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    :goto_3
    iput-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 706
    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LbmY;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LbmY;

    invoke-virtual {v0}, LbmY;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 707
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LvL;

    iget-object v2, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LbmY;

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, LvL;->a(LbmY;Z)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v0

    invoke-virtual {v1, v0}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    .line 711
    :cond_2
    invoke-virtual {v1}, LvN;->a()Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    move-result-object v0

    .line 712
    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 713
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/DocListView;->setSelectedEntrySpec(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    goto/16 :goto_0

    .line 686
    :cond_3
    iput-object v2, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    goto :goto_2

    .line 698
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->c:Lcom/google/android/gms/drive/database/data/EntrySpec;

    goto :goto_3

    .line 700
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LvL;

    iget-object v3, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LQl;

    .line 701
    invoke-static {v3}, LQl;->a(LQl;)LCl;

    move-result-object v3

    invoke-interface {v0, v3}, LvL;->b(LCl;)Lcom/google/android/apps/docs/app/model/navigation/Criterion;

    move-result-object v0

    .line 702
    invoke-virtual {v1, v0}, LvN;->a(Lcom/google/android/apps/docs/app/model/navigation/Criterion;)LvN;

    .line 703
    iput-object v2, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    goto :goto_4

    .line 715
    :cond_6
    iput-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    .line 716
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->B()V

    goto/16 :goto_0

    :cond_7
    move-object v3, v1

    move-object v1, v0

    goto/16 :goto_1

    :cond_8
    move-object v1, v0

    move-object v3, v2

    goto/16 :goto_1

    :cond_9
    move-object v1, v2

    move-object v3, v2

    goto/16 :goto_1
.end method

.method private c()Ljava/lang/String;
    .locals 4

    .prologue
    .line 583
    invoke-virtual {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 584
    const-string v1, "selectButtonText"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 585
    if-lez v1, :cond_1

    .line 586
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 594
    :cond_0
    :goto_0
    return-object v0

    .line 588
    :cond_1
    const-string v1, "selectButtonText"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 589
    if-nez v0, :cond_0

    .line 593
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    const/4 v1, 0x1

    new-array v1, v1, [LaGv;

    const/4 v2, 0x0

    sget-object v3, LaGv;->a:LaGv;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->a([LaGv;)Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 594
    if-eqz v0, :cond_2

    sget v0, Lxi;->dialog_select_folder:I

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    sget v0, Lxi;->dialog_select:I

    goto :goto_1
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->A()V

    return-void
.end method

.method private l()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 358
    invoke-virtual {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "showNewFolder"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_1

    .line 367
    :cond_0
    :goto_0
    return v0

    .line 362
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "showTopCollections"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LQl;

    if-nez v2, :cond_2

    move v2, v1

    .line 364
    :goto_1
    if-nez v2, :cond_0

    move v0, v1

    .line 367
    goto :goto_0

    :cond_2
    move v2, v0

    .line 362
    goto :goto_1
.end method

.method private t()V
    .locals 0

    .prologue
    .line 273
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->u()V

    .line 274
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->x()V

    .line 275
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->y()V

    .line 276
    return-void
.end method

.method private u()V
    .locals 6

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 306
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v1

    .line 308
    invoke-virtual {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "showTopCollections"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 310
    const/4 v0, 0x1

    .line 312
    if-eqz v1, :cond_0

    .line 313
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->v()V

    move v2, v0

    .line 325
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->d:Landroid/view/View;

    sget v1, Lxc;->icon_new:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 326
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v3

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 328
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->d:Landroid/view/View;

    sget v1, Lxc;->action:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 329
    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->d:Landroid/view/View;

    sget v5, Lxc;->title:I

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 330
    if-eqz v2, :cond_4

    .line 331
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 332
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a(Landroid/widget/TextView;)V

    .line 338
    :goto_2
    return-void

    .line 314
    :cond_0
    if-eqz v2, :cond_2

    .line 315
    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LQl;

    if-eqz v1, :cond_1

    .line 316
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->v()V

    move v2, v0

    goto :goto_0

    .line 318
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->w()V

    move v2, v3

    .line 319
    goto :goto_0

    .line 322
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->w()V

    move v2, v0

    goto :goto_0

    :cond_3
    move v0, v4

    .line 326
    goto :goto_1

    .line 334
    :cond_4
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 335
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 336
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method private v()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 341
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->d:Landroid/view/View;

    sget v1, Lxc;->up_affordance:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 342
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 343
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->d:Landroid/view/View;

    sget v1, Lxc;->text_box:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 344
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 345
    return-void
.end method

.method private w()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->d:Landroid/view/View;

    sget v1, Lxc;->up_affordance:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 349
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 351
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->d:Landroid/view/View;

    sget v1, Lxc;->text_box:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 352
    invoke-virtual {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()LH;

    move-result-object v1

    invoke-virtual {v1}, LH;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 353
    sget v2, Lxa;->m_entry_margin:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 354
    invoke-virtual {v0, v1, v3, v1, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 355
    return-void
.end method

.method private x()V
    .locals 2

    .prologue
    .line 377
    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LQl;

    if-nez v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/view/DocListView;->setVisibility(I)V

    .line 378
    return-void

    .line 377
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private y()V
    .locals 2

    .prologue
    .line 386
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->e:Landroid/view/View;

    sget v1, Lxc;->top_collections_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 387
    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LQl;

    if-eqz v1, :cond_0

    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 388
    return-void

    .line 387
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private z()V
    .locals 4

    .prologue
    .line 506
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocListView;->a()Landroid/widget/ListView;

    move-result-object v0

    const-wide/16 v2, 0x1388

    invoke-static {v0, v2, v3}, LajJ;->a(Landroid/view/View;J)V

    .line 507
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 411
    invoke-virtual {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()LH;

    move-result-object v0

    invoke-static {v0}, LEL;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v1

    .line 412
    invoke-static {v1}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v8

    .line 413
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LakQ;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v7

    :goto_0
    invoke-virtual {v8, v0}, LEU;->a(Z)V

    .line 415
    const-string v0, "layout_inflater"

    .line 416
    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 417
    sget v1, Lxe;->pick_entry_dialog_header:I

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->d:Landroid/view/View;

    .line 419
    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->d:Landroid/view/View;

    sget v3, Lxc;->action:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 420
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 422
    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->d:Landroid/view/View;

    sget v3, Lxc;->icon_new:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 423
    invoke-virtual {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "showNewFolder"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 424
    new-instance v3, LQf;

    invoke-direct {v3, p0}, LQf;-><init>(Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 434
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->d:Landroid/view/View;

    invoke-virtual {v8, v1}, LEU;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 435
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->c()Ljava/lang/String;

    move-result-object v1

    new-instance v3, LQg;

    invoke-direct {v3, p0}, LQg;-><init>(Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;)V

    invoke-virtual {v8, v1, v3}, LEU;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 441
    const/high16 v1, 0x1040000

    invoke-virtual {v8, v1, v5}, LEU;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 442
    sget v1, Lxe;->file_picker:I

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->e:Landroid/view/View;

    .line 444
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    new-array v1, v7, [LaGv;

    sget-object v3, LaGv;->a:LaGv;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->a([LaGv;)Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 445
    if-eqz v0, :cond_2

    .line 446
    invoke-static {}, LQl;->a()[LQl;

    move-result-object v5

    .line 447
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->e:Landroid/view/View;

    sget v1, Lxc;->top_collections_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/ListView;

    .line 448
    new-instance v0, LQh;

    .line 449
    invoke-virtual {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()LH;

    move-result-object v2

    sget v3, Lxe;->navigation_menu_item:I

    sget v4, Lxc;->navigation_name:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LQh;-><init>(Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;Landroid/content/Context;II[LQl;)V

    .line 462
    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 463
    new-instance v0, LQi;

    invoke-direct {v0, p0}, LQi;-><init>(Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;)V

    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 475
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->d:Landroid/view/View;

    sget v1, Lxc;->up_affordance:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 476
    new-instance v1, LQj;

    invoke-direct {v1, p0}, LQj;-><init>(Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 483
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->e:Landroid/view/View;

    sget v1, Lxc;->doc_list_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/DocListView;

    iput-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    .line 484
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/view/DocListView;->setParentFragment(Landroid/support/v4/app/Fragment;)V

    .line 485
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/view/DocListView;->setOnEntryClickListener(LapD;)V

    .line 486
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    sget-object v1, LapG;->c:LapG;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/DocListView;->setViewMode(LapG;)V

    .line 487
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->z()V

    .line 489
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lapn;

    sget-object v1, LIK;->b:LIK;

    invoke-virtual {v0, v1}, Lapn;->b(LIK;)V

    .line 490
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lapn;

    sget-object v1, LzO;->a:LzO;

    invoke-virtual {v0, v1}, Lapn;->b(LzO;)V

    .line 491
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lapn;

    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()Lan;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v7}, Lapn;->a(Lapt;Lan;Z)V

    .line 493
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->e:Landroid/view/View;

    invoke-virtual {v8, v0}, LEU;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 495
    invoke-virtual {v8}, LEU;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 496
    return-object v0

    :cond_0
    move v0, v2

    .line 413
    goto/16 :goto_0

    .line 431
    :cond_1
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 446
    :cond_2
    invoke-static {}, LQl;->b()[LQl;

    move-result-object v5

    goto/16 :goto_2
.end method

.method public a()Lcom/google/android/apps/docs/view/DocListView;
    .locals 1

    .prologue
    .line 847
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    return-object v0
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 625
    if-nez p1, :cond_2

    .line 626
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 627
    const-string v0, "entrySpec.v2"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 628
    if-eqz v0, :cond_1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 633
    :cond_0
    :goto_1
    return-void

    .line 628
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->c:Lcom/google/android/gms/drive/database/data/EntrySpec;

    goto :goto_0

    .line 631
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a(IILandroid/content/Intent;)V

    goto :goto_1
.end method

.method public a(Landroid/view/View;ILaGu;)V
    .locals 2

    .prologue
    .line 599
    if-nez p3, :cond_1

    .line 610
    :cond_0
    :goto_0
    return-void

    .line 603
    :cond_1
    invoke-interface {p3}, LaGu;->a()LaGv;

    move-result-object v0

    .line 604
    invoke-interface {p3}, LaGu;->g()Ljava/lang/String;

    move-result-object v1

    .line 605
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a(LaGv;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609
    invoke-interface {p3}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    goto :goto_0
.end method

.method public a(LaGv;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 843
    sget-object v0, LaGv;->a:LaGv;

    invoke-virtual {v0, p1}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    invoke-virtual {v0, p2, p1}, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;->a(Ljava/lang/String;LaGv;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 215
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a_(Landroid/os/Bundle;)V

    .line 217
    invoke-virtual {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v2

    .line 218
    if-eqz p1, :cond_1

    move-object v1, p1

    .line 219
    :goto_0
    const-string v0, "entrySpec.v2"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 220
    const-string v0, "accountName"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LaFO;

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LaGg;

    iget-object v3, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LaFO;

    invoke-interface {v0, v3}, LaGg;->a(LaFO;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->c:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 223
    invoke-virtual {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "mimeTypes"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 224
    if-eqz v0, :cond_0

    .line 225
    invoke-static {v0}, LbmY;->a([Ljava/lang/Object;)LbmY;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LbmY;

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 229
    const-string v0, "disabledAncestors"

    .line 230
    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 231
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 232
    iget-object v3, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Ljava/util/HashMap;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    move-object v1, v2

    .line 218
    goto :goto_0

    .line 235
    :cond_2
    if-eqz p1, :cond_3

    .line 236
    const-string v0, "listCriteria"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    iput-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    .line 237
    const-string v0, "parentEntrySpec"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 238
    const-string v0, "topCollection"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 239
    if-eqz v0, :cond_4

    invoke-static {v0}, LQl;->a(Ljava/lang/String;)LQl;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LQl;

    .line 242
    :cond_3
    const-string v0, "documentTypeFilter"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    iput-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/doclist/DocumentTypeFilter;

    .line 244
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 245
    new-instance v1, LQc;

    invoke-direct {v1, p0, v0, v0}, LQc;-><init>(Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;Landroid/os/Handler;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Landroid/database/ContentObserver;

    .line 256
    return-void

    .line 239
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public b(Landroid/view/View;ILaGu;)V
    .locals 0

    .prologue
    .line 615
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 392
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->c(Landroid/os/Bundle;)V

    .line 394
    const-string v0, "entrySpec.v2"

    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 395
    const-string v0, "parentEntrySpec"

    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 396
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 397
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 398
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 399
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 402
    :cond_1
    const-string v0, "disabledAncestors"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 403
    const-string v0, "listCriteria"

    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 404
    const-string v1, "topCollection"

    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LQl;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LQl;

    .line 405
    invoke-static {v0}, LQl;->a(LQl;)Ljava/lang/String;

    move-result-object v0

    .line 404
    :goto_1
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    return-void

    .line 405
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public g()V
    .locals 2

    .prologue
    .line 565
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lapn;

    invoke-virtual {v0}, Lapn;->b()V

    .line 566
    invoke-virtual {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()LH;

    move-result-object v0

    .line 567
    if-eqz v0, :cond_0

    .line 568
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 569
    if-eqz v0, :cond_0

    .line 570
    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 573
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->g()V

    .line 574
    return-void
.end method

.method public j_()V
    .locals 1

    .prologue
    .line 578
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lapn;

    invoke-virtual {v0}, Lapn;->c()V

    .line 579
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->j_()V

    .line 580
    return-void
.end method

.method public o_()V
    .locals 4

    .prologue
    .line 539
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->o_()V

    .line 541
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:LQl;

    if-nez v0, :cond_1

    .line 542
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->t()V

    .line 550
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->C()V

    .line 552
    invoke-virtual {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()LH;

    move-result-object v0

    .line 553
    if-eqz v0, :cond_0

    .line 554
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 555
    if-eqz v0, :cond_0

    .line 556
    sget-object v1, LaEG;->h:LaEG;

    invoke-virtual {v1}, LaEG;->a()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 560
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lapn;

    invoke-virtual {v0}, Lapn;->a()V

    .line 561
    return-void

    .line 543
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;

    if-eqz v0, :cond_2

    .line 544
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    iget-object v1, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/DocListView;->setSelectedEntrySpec(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 545
    invoke-direct {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->B()V

    goto :goto_0

    .line 547
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->b(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    goto :goto_0
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 260
    invoke-virtual {p0}, Lcom/google/android/apps/docs/entrypicker/PickEntryDialogFragment;->a()LH;

    move-result-object v0

    .line 262
    if-eqz v0, :cond_0

    .line 263
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 266
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 267
    return-void
.end method
