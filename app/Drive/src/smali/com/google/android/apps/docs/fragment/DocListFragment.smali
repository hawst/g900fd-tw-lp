.class public Lcom/google/android/apps/docs/fragment/DocListFragment;
.super Lcom/google/android/apps/docs/app/BaseFragment;
.source "DocListFragment.java"

# interfaces
.implements LapD;


# instance fields
.field private a:I

.field public a:LCM;

.field public a:LCU;

.field public a:LJR;

.field public a:LMF;

.field public a:LPu;

.field public a:LaGM;

.field public a:Lahy;

.field private a:Lahz;

.field public a:Lamt;

.field public a:Lapn;

.field public a:Laqw;

.field public a:LbiP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbiP",
            "<",
            "LBt;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/view/DocListView;

.field private a:Lcom/google/android/apps/docs/view/SwipeToRefreshView;

.field public a:LtK;

.field private a:Z

.field private c:Ljava/lang/String;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseFragment;-><init>()V

    .line 140
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Z

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/DocListFragment;)Lcom/google/android/apps/docs/view/DocListView;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/DocListFragment;)Lcom/google/android/apps/docs/view/SwipeToRefreshView;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/SwipeToRefreshView;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocListView;->a()I

    move-result v0

    return v0
.end method

.method public a()LQX;
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocListView;->a()LQX;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 145
    const-string v0, "DocListFragment"

    const-string v1, "in DLF.onCreateView"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    sget v0, Lxe;->doc_list_container:I

    .line 148
    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 149
    sget v1, Lxc;->doc_list_body:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 150
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LbiP;

    invoke-virtual {v2}, LbiP;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 151
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LbiP;

    invoke-virtual {v2}, LbiP;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LBt;

    invoke-interface {v2}, LBt;->a()Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->f:Landroid/view/View;

    .line 152
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->f:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 153
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->f:Landroid/view/View;

    invoke-virtual {v1, v2, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 157
    :cond_0
    sget v2, Lxc;->doc_list_view:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/docs/view/DocListView;

    iput-object v2, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    .line 158
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/docs/view/DocListView;->setOnEntryClickListener(LapD;)V

    .line 159
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/docs/view/DocListView;->setTagName(Ljava/lang/String;)V

    .line 160
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v2, v5}, Lcom/google/android/apps/docs/view/DocListView;->setVisibility(I)V

    .line 161
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/docs/view/DocListView;->setParentFragment(Landroid/support/v4/app/Fragment;)V

    .line 163
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lamt;

    invoke-virtual {v2}, Lamt;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LtK;

    sget-object v3, Lry;->aH:Lry;

    .line 164
    invoke-interface {v2, v3}, LtK;->a(LtJ;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v2, v4

    :goto_0
    iput-boolean v2, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Z

    .line 165
    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lapn;

    iget-object v6, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Lan;

    move-result-object v7

    iget-boolean v2, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Z

    if-nez v2, :cond_5

    move v2, v4

    :goto_1
    invoke-virtual {v3, v6, v7, v2}, Lapn;->a(Lapt;Lan;Z)V

    .line 166
    const/4 v2, 0x0

    .line 167
    iget-boolean v3, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Z

    if-eqz v3, :cond_1

    .line 168
    sget v2, Lxc;->swipable_doc_list_stub:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    .line 170
    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v2

    sget v3, Lxc;->swipable_doc_list:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;

    .line 171
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 172
    invoke-virtual {v2, v5}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->setFocusable(Z)V

    .line 173
    invoke-virtual {v2, v1}, Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;->addView(Landroid/view/View;)V

    .line 174
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LMF;

    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v1, v2, v3}, LMF;->a(Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;Lcom/google/android/apps/docs/view/DocListView;)V

    .line 177
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LCU;

    invoke-virtual {v1}, LCU;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 178
    sget v1, Lxc;->selection_floating_overlay_layout:I

    .line 179
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;

    .line 180
    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LJR;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/docs/doclist/selection/view/SelectionOverlayLayout;->setUp(Lcom/google/android/apps/docs/doclist/selection/SelectionModel;)V

    .line 183
    :cond_2
    sget v1, Lxc;->account_info_banner:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->d:Landroid/view/View;

    .line 185
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LtK;

    sget-object v3, Lry;->am:Lry;

    invoke-interface {v1, v3}, LtK;->a(LtJ;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 186
    const-string v1, "DocListFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "pull to refresh enabled: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    sget v1, Lxc;->list_view_refresh_frame:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/docs/view/SwipeToRefreshView;

    iput-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/SwipeToRefreshView;

    .line 190
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    new-instance v3, LRb;

    invoke-direct {v3, p0}, LRb;-><init>(Lcom/google/android/apps/docs/fragment/DocListFragment;)V

    invoke-virtual {v1, v3}, Lcom/google/android/apps/docs/view/DocListView;->setArrangeModeListener(LapA;)V

    .line 201
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/SwipeToRefreshView;

    if-eqz v1, :cond_3

    .line 202
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/SwipeToRefreshView;

    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v1, v3, v2}, Lcom/google/android/apps/docs/view/SwipeToRefreshView;->setup(Lcom/google/android/apps/docs/view/DocListView;Lcom/google/android/apps/docs/doclist/swipenavigation/SwipableDocListLayout;)V

    .line 203
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/SwipeToRefreshView;

    new-instance v2, LRc;

    invoke-direct {v2, p0}, LRc;-><init>(Lcom/google/android/apps/docs/fragment/DocListFragment;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/docs/view/SwipeToRefreshView;->setOnRefreshListener(LhE;)V

    .line 217
    :cond_3
    const-string v1, "DocListFragment"

    const-string v2, "Is Quantum %b"

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lamt;

    invoke-virtual {v4}, Lamt;->a()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 218
    sget v1, Lxc;->floating_create_bar:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->e:Landroid/view/View;

    .line 219
    return-object v0

    :cond_4
    move v2, v5

    .line 164
    goto/16 :goto_0

    :cond_5
    move v2, v5

    .line 165
    goto/16 :goto_1
.end method

.method public a(IZI)Landroid/view/animation/Animation;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 444
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Landroid/content/res/Resources;

    move-result-object v1

    .line 448
    invoke-static {v1}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lxa;->navigation_panel_narrow_width:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 450
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v1, v0

    .line 451
    const/16 v0, 0x2002

    if-ne p1, v0, :cond_2

    .line 452
    iget v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:I

    if-ne v0, v4, :cond_1

    .line 453
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    neg-float v1, v1

    invoke-direct {v0, v1, v3, v3, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 467
    :goto_1
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 468
    :goto_2
    return-object v0

    .line 448
    :cond_0
    sget v0, Lxa;->navigation_panel_width:I

    goto :goto_0

    .line 455
    :cond_1
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    neg-float v1, v2

    invoke-direct {v0, v1, v3, v3, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    goto :goto_1

    .line 457
    :cond_2
    const/16 v0, 0x1001

    if-ne p1, v0, :cond_4

    .line 458
    iget v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:I

    if-ne v0, v4, :cond_3

    .line 459
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    neg-float v1, v1

    invoke-direct {v0, v3, v1, v3, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    goto :goto_1

    .line 461
    :cond_3
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v0, v2, v3, v3, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    goto :goto_1

    .line 464
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public a()Lcom/google/android/apps/docs/view/DocListView;
    .locals 1

    .prologue
    .line 510
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 489
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocListView;->d()V

    .line 490
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/view/DocListView;->setGridViewWidth(I)V

    .line 486
    return-void
.end method

.method a(Landroid/view/ContextMenu;LCI;)V
    .locals 4

    .prologue
    .line 288
    invoke-virtual {p2}, LCI;->a()Ljava/util/Set;

    move-result-object v1

    .line 290
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Landroid/view/ContextMenu;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 291
    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 292
    invoke-interface {v2}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 293
    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 290
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 295
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;ILaGu;)V
    .locals 2

    .prologue
    .line 232
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 233
    sget v1, Lxc;->more_actions_button:I

    if-ne v0, v1, :cond_1

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Laqw;

    invoke-interface {v0, p1, p3}, Laqw;->a(Landroid/view/View;LaGu;)V

    .line 239
    :cond_0
    :goto_0
    return-void

    .line 235
    :cond_1
    sget v1, Lxc;->doc_entry_root:I

    if-eq v0, v1, :cond_2

    sget v1, Lxc;->show_preview_button:I

    if-eq v0, v1, :cond_2

    sget v1, Lxc;->open_button:I

    if-ne v0, v1, :cond_0

    .line 237
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    sget-object v1, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    invoke-virtual {v0, p3, p2, v1}, Lcom/google/android/apps/docs/view/DocListView;->a(LaGu;ILcom/google/android/apps/docs/app/DocumentOpenMethod;)V

    goto :goto_0
.end method

.method public a(LapC;)V
    .locals 1

    .prologue
    .line 498
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/view/DocListView;->setOnDirectionalScrollListener(LapC;)V

    .line 499
    return-void
.end method

.method public a(LapE;)V
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/view/DocListView;->setSyncStatus(LapE;)V

    .line 340
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V
    .locals 2

    .prologue
    .line 515
    invoke-static {}, LamV;->a()V

    .line 516
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LbiP;

    invoke-virtual {v0}, LbiP;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 517
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LbiP;

    invoke-virtual {v0}, LbiP;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBt;

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->f:Landroid/view/View;

    invoke-interface {v0, v1, p1}, LBt;->a(Landroid/view/View;Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;)V

    .line 519
    :cond_0
    return-void
.end method

.method public a(LzO;)V
    .locals 2

    .prologue
    .line 477
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lapn;

    invoke-virtual {v0, p1}, Lapn;->a(LzO;)V

    .line 478
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()LH;

    move-result-object v0

    .line 479
    instance-of v1, v0, Lcom/google/android/apps/docs/app/DocListActivity;

    if-eqz v1, :cond_0

    .line 480
    check-cast v0, Lcom/google/android/apps/docs/app/DocListActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/DocListActivity;->p()V

    .line 482
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 343
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/SwipeToRefreshView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LtK;

    sget-object v1, Lry;->am:Lry;

    invoke-interface {v0, v1}, LtK;->a(LtJ;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 345
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lamt;

    invoke-virtual {v0}, Lamt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 346
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/SwipeToRefreshView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/view/SwipeToRefreshView;->setRefreshing(Z)V

    .line 349
    :cond_1
    return-void
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 376
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseFragment;->a_(Landroid/os/Bundle;)V

    .line 377
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 378
    new-instance v1, LRd;

    invoke-direct {v1, p0, v0}, LRd;-><init>(Lcom/google/android/apps/docs/fragment/DocListFragment;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lahz;

    .line 394
    return-void
.end method

.method public b()Landroid/view/View;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->e:Landroid/view/View;

    return-object v0
.end method

.method public b(Landroid/view/View;ILaGu;)V
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Laqw;

    invoke-interface {v0, p1, p3}, Laqw;->a(Landroid/view/View;LaGu;)V

    .line 244
    return-void
.end method

.method public b(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 299
    invoke-interface {p1}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    .line 304
    instance-of v3, v0, Lapm;

    if-nez v3, :cond_0

    move v0, v1

    .line 321
    :goto_0
    return v0

    .line 307
    :cond_0
    check-cast v0, Lapm;

    .line 308
    invoke-virtual {v0}, Lapm;->a()LaGu;

    move-result-object v0

    .line 309
    if-nez v0, :cond_1

    move v0, v1

    .line 310
    goto :goto_0

    .line 312
    :cond_1
    const-string v3, "DocListFragment"

    const-string v4, "in onContextItemSelected for %s"

    new-array v5, v2, [Ljava/lang/Object;

    aput-object p0, v5, v1

    invoke-static {v3, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 313
    const-string v3, "DocListFragment"

    const-string v4, "selectedEntrySpec = %s"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-interface {v0}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v3, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 315
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()LH;

    move-result-object v3

    instance-of v3, v3, Lcom/google/android/apps/docs/app/DocListActivity;

    if-eqz v3, :cond_2

    .line 316
    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LPu;

    invoke-virtual {v3, p1, v0}, LPu;->a(Landroid/view/MenuItem;LaGu;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 317
    goto :goto_0

    :cond_2
    move v0, v1

    .line 321
    goto :goto_0
.end method

.method public c()Landroid/view/View;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->d:Landroid/view/View;

    return-object v0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 473
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseFragment;->c(Landroid/os/Bundle;)V

    .line 474
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocListView;->b()I

    move-result v0

    return v0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 412
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/SwipeToRefreshView;

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/SwipeToRefreshView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/SwipeToRefreshView;->a()V

    .line 415
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocListView;->b()V

    .line 416
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lapn;

    invoke-virtual {v0}, Lapn;->b()V

    .line 417
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Z

    if-eqz v0, :cond_1

    .line 418
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LMF;

    invoke-virtual {v0}, LMF;->b()V

    .line 420
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lahy;

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lahz;

    invoke-interface {v0, v1}, Lahy;->a(Lahz;)Z

    .line 421
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseFragment;->g()V

    .line 422
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 432
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()V

    .line 433
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocListView;->e()V

    .line 434
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseFragment;->h()V

    .line 435
    return-void
.end method

.method public j_()V
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lapn;

    invoke-virtual {v0}, Lapn;->c()V

    .line 427
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseFragment;->j_()V

    .line 428
    return-void
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/SwipeToRefreshView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/SwipeToRefreshView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/SwipeToRefreshView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocListView;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o_()V
    .locals 2

    .prologue
    .line 398
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseFragment;->o_()V

    .line 399
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lahy;

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lahz;

    invoke-interface {v0, v1}, Lahy;->a(Lahz;)V

    .line 400
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Z

    if-eqz v0, :cond_0

    .line 401
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LMF;

    invoke-virtual {v0}, LMF;->a()V

    .line 403
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/SwipeToRefreshView;

    if-eqz v0, :cond_1

    .line 404
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/SwipeToRefreshView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/SwipeToRefreshView;->b()V

    .line 406
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lapn;

    invoke-virtual {v0}, Lapn;->a()V

    .line 407
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocListView;->g()V

    .line 408
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 260
    instance-of v0, p3, Lapm;

    if-nez v0, :cond_0

    .line 279
    :goto_0
    return-void

    .line 263
    :cond_0
    check-cast p3, Lapm;

    .line 264
    invoke-virtual {p3}, Lapm;->a()LaGu;

    move-result-object v0

    .line 265
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()LH;

    move-result-object v1

    invoke-virtual {v1}, LH;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    .line 266
    sget v2, Lxf;->menu_doclist_context:I

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 270
    const-string v1, "DocListFragment"

    const-string v2, "update menu for Entry: %s"

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 271
    if-eqz v0, :cond_1

    .line 272
    invoke-interface {v0}, LaGu;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    .line 273
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LCM;

    invoke-virtual {v1, v0, v5}, LCM;->a(LaGu;Z)LCI;

    move-result-object v0

    .line 274
    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a(Landroid/view/ContextMenu;LCI;)V

    goto :goto_0

    .line 276
    :cond_1
    invoke-interface {p1}, Landroid/view/ContextMenu;->clear()V

    .line 277
    sget v0, Lxi;->error_document_not_available:I

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->add(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public t()V
    .locals 1

    .prologue
    .line 506
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocListView;->f()V

    .line 507
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 358
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseFragment;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
