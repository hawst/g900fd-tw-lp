.class public Lcom/google/android/apps/docs/shareitem/ActivityFinishingErrorDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "ActivityFinishingErrorDialogFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    return-void
.end method

.method public static a(LM;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 28
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    invoke-static {p3}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 32
    const-string v1, "message"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    const-string v1, "title"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    const-string v1, "okbtntxt"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    new-instance v1, Lcom/google/android/apps/docs/shareitem/ActivityFinishingErrorDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/docs/shareitem/ActivityFinishingErrorDialogFragment;-><init>()V

    .line 36
    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/shareitem/ActivityFinishingErrorDialogFragment;->e(Landroid/os/Bundle;)V

    .line 37
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, Lcom/google/android/apps/docs/shareitem/ActivityFinishingErrorDialogFragment;->a(LM;Ljava/lang/String;)V

    .line 38
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/ActivityFinishingErrorDialogFragment;->a()LH;

    move-result-object v0

    .line 43
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/ActivityFinishingErrorDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v1

    .line 44
    const-string v2, "title"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 45
    const-string v3, "message"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 46
    const-string v4, "okbtntxt"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 47
    invoke-static {v0}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v0

    .line 48
    invoke-virtual {v0, v2}, LEU;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 49
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v2, Laaz;

    invoke-direct {v2, p0}, Laaz;-><init>(Lcom/google/android/apps/docs/shareitem/ActivityFinishingErrorDialogFragment;)V

    .line 50
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 57
    return-object v0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 63
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/ActivityFinishingErrorDialogFragment;->a()LH;

    move-result-object v0

    .line 64
    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {v0}, LH;->finish()V

    .line 67
    :cond_0
    return-void
.end method
