.class public Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateChangedEventReceiver;
.super Lajq;
.source "CrossAppStateChangedEventReceiver.java"


# instance fields
.field public a:LMq;

.field public a:Lakk;

.field public a:LqK;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lajq;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateChangedEventReceiver;->a:Lakk;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lakk;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    const-string v0, "CrossAppStateChangedEventReceiver"

    const-string v1, "Caller package not authorized"

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateChangedEventReceiver;->a:LqK;

    const-string v1, "crossAppStateSync"

    const-string v2, "crossAppSyncerAccessDenied"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    :goto_0
    return-void

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateChangedEventReceiver;->a:LMq;

    invoke-static {v0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 49
    const-string v1, "CrossAppStateChangedEventReceiver"

    const-string v2, "Received intent with action %s"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 51
    const-string v1, "com.google.android.apps.docs.statesyncer.DOCUMENT_PIN_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 52
    const-string v1, "CrossAppStateChangedEventReceiver"

    const-string v2, "Unknown action: %s"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 56
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/statesyncer/CrossAppStateChangedEventReceiver;->a:LMq;

    invoke-virtual {v0, p1}, LMq;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
