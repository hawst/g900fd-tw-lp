.class public Lcom/google/android/apps/docs/app/CreateNewDocActivity;
.super LrX;
.source "CreateNewDocActivity.java"


# instance fields
.field public a:LSF;

.field public a:LTT;

.field public a:LaGR;

.field private a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field public a:LtK;

.field public a:Luc;

.field public a:Lye;

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, LrX;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;LaGv;LaFO;Lcom/google/android/gms/drive/database/data/EntrySpec;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 78
    invoke-static {p0}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 82
    const-class v1, Lcom/google/android/apps/docs/app/CreateNewDocActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 83
    const-string v1, "accountName"

    invoke-virtual {p2}, LaFO;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 84
    const-string v1, "returnResult"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 85
    const-string v1, "kindOfDocumentToCreate"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 86
    const-string v1, "collectionEntrySpec"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 88
    return-object v0
.end method

.method private g()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 162
    iget-object v2, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:LaGv;

    invoke-virtual {v2}, LaGv;->b()Ljava/lang/String;

    move-result-object v2

    .line 163
    invoke-static {v2}, Lbju;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 200
    :cond_0
    :goto_0
    return v0

    .line 167
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:LTT;

    iget-object v4, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:LaFO;

    invoke-interface {v3, v4, v2}, LTT;->a(LaFO;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 168
    if-eqz v3, :cond_2

    .line 169
    new-instance v0, LrW;

    invoke-direct {v0, p0, v3}, LrW;-><init>(Lcom/google/android/apps/docs/app/CreateNewDocActivity;Landroid/content/Intent;)V

    .line 186
    iget-object v2, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:LaGR;

    invoke-virtual {v2, v0}, LaGR;->a(LaGN;)V

    move v0, v1

    .line 187
    goto :goto_0

    .line 190
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:LtK;

    sget-object v4, Lry;->j:Lry;

    invoke-interface {v3, v4}, LtK;->a(LtJ;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 191
    iget-object v3, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:Lye;

    invoke-interface {v3, v2}, Lye;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 192
    if-eqz v2, :cond_0

    .line 193
    const-string v3, "CreateNewDocActivity"

    const-string v4, "Requesting installation for %s"

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v2, v5, v0

    invoke-static {v3, v4, v5}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:Luc;

    invoke-interface {v0, v2}, Luc;->a(Landroid/content/Intent;)V

    .line 195
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->finish()V

    move v0, v1

    .line 196
    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/google/android/gms/drive/database/data/ResourceSpec;
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:LaGM;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 134
    invoke-interface {v0, v1}, LaGM;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Ljava/lang/Exception;)Z
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x1

    return v0
.end method

.method public f()V
    .locals 2

    .prologue
    .line 144
    sget-object v0, LaGv;->a:LaGv;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:LaGv;

    invoke-virtual {v0, v1}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    :goto_0
    return-void

    .line 149
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->j()V

    goto :goto_0
.end method

.method protected f()Z
    .locals 1

    .prologue
    .line 139
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->i:Z

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 107
    invoke-super {p0, p1, p2, p3}, LrX;->onActivityResult(IILandroid/content/Intent;)V

    .line 109
    packed-switch p1, :pswitch_data_0

    .line 119
    const-string v0, "CreateNewDocActivity"

    const-string v1, "Unexpected activity request code: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 122
    :goto_0
    return-void

    .line 111
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:LaGM;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:LaFO;

    invoke-interface {v0, v1}, LaGM;->a(LaFO;)LaFM;

    move-result-object v0

    .line 112
    iget-object v1, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:LSF;

    const-string v2, "com.google.android.apps.docs"

    invoke-static {v1, v0, v2}, LajS;->a(LSF;LaFM;Ljava/lang/String;)V

    .line 114
    invoke-virtual {p0, p2, p3}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->setResult(ILandroid/content/Intent;)V

    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->finish()V

    goto :goto_0

    .line 109
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 93
    invoke-super {p0, p1}, LrX;->onCreate(Landroid/os/Bundle;)V

    .line 95
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 97
    const-string v0, "collectionEntrySpec"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 98
    const-string v0, "returnResult"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->i:Z

    .line 100
    if-nez p1, :cond_0

    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->f()V

    .line 103
    :cond_0
    return-void
.end method
