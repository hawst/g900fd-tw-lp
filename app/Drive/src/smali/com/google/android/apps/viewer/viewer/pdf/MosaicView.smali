.class public Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;
.super Landroid/view/ViewGroup;
.source "MosaicView.java"


# static fields
.field private static final a:I

.field public static final a:Landroid/graphics/Paint;

.field private static final b:I

.field private static final b:Landroid/graphics/Paint;


# instance fields
.field private a:F

.field private a:Landroid/graphics/Bitmap;

.field private final a:Landroid/graphics/Rect;

.field private final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Laxx;",
            ">;"
        }
    .end annotation
.end field

.field private a:LavU;

.field private a:LawO;

.field private a:Lawn;

.field private final b:Landroid/graphics/Rect;

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 44
    const/16 v0, 0x1000

    invoke-static {}, Lawl;->a()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    sput v0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:I

    .line 47
    sget v0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:I

    div-int/lit8 v0, v0, 0x2

    sput v0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->b:I

    .line 49
    new-instance v0, LawK;

    invoke-direct {v0}, LawK;-><init>()V

    sput-object v0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->b:Landroid/graphics/Paint;

    .line 54
    new-instance v0, LawL;

    invoke-direct {v0}, LawL;-><init>()V

    sput-object v0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 63
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Rect;

    .line 77
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/util/SparseArray;

    .line 85
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->b:Landroid/graphics/Rect;

    .line 100
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->setWillNotDraw(Z)V

    .line 89
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Rect;

    .line 77
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/util/SparseArray;

    .line 85
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->b:Landroid/graphics/Rect;

    .line 100
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->setWillNotDraw(Z)V

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Rect;

    .line 77
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/util/SparseArray;

    .line 85
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->b:Landroid/graphics/Rect;

    .line 100
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->setWillNotDraw(Z)V

    .line 97
    return-void
.end method

.method private a(II)I
    .locals 2

    .prologue
    .line 469
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    mul-int/2addr v0, p2

    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    div-int/2addr v0, v1

    invoke-static {p1, p2, v0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(III)I

    move-result v0

    return v0
.end method

.method private static a(III)I
    .locals 1

    .prologue
    .line 477
    invoke-static {p0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/util/SparseArray;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;)LawO;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:LawO;

    return-object v0
.end method

.method private a(I)Laxx;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxx;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;I)Laxx;
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(I)Laxx;

    move-result-object v0

    return-object v0
.end method

.method private a(I)Lcom/google/android/apps/viewer/client/Dimensions;
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    mul-int/2addr v0, p1

    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/2addr v0, v1

    .line 142
    new-instance v1, Lcom/google/android/apps/viewer/client/Dimensions;

    invoke-direct {v1, p1, v0}, Lcom/google/android/apps/viewer/client/Dimensions;-><init>(II)V

    return-object v1
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 473
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MosaicView"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Lawr;)V
    .locals 3

    .prologue
    .line 202
    new-instance v0, Laxx;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Laxx;-><init>(Landroid/content/Context;Lawr;)V

    .line 203
    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/util/SparseArray;

    invoke-virtual {p1}, Lawr;->a()I

    move-result v2

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 204
    invoke-virtual {p0, v0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->addView(Landroid/view/View;)V

    .line 205
    return-void
.end method

.method private a(Lcom/google/android/apps/viewer/client/Dimensions;F)V
    .locals 7

    .prologue
    .line 182
    new-instance v0, Lawn;

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:LavU;

    invoke-direct {v0, v1, p1, v2}, Lawn;-><init>(ILcom/google/android/apps/viewer/client/Dimensions;LavU;)V

    .line 183
    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Lawn;

    if-eqz v1, :cond_0

    .line 184
    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Rect;

    invoke-static {v1}, Lawk;->a(Landroid/graphics/Rect;)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0}, Lawn;->a()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 185
    iget v2, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:F

    cmpl-float v2, p2, v2

    if-lez v2, :cond_0

    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v2, v1, v2

    if-gez v2, :cond_0

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Rect;

    invoke-static {v0}, Lawk;->a(Landroid/graphics/Rect;)I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Lawn;

    invoke-virtual {v2}, Lawn;->a()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 187
    invoke-direct {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Zoom at %.0f, tile base area would drop to %.2f px^2, current tiling is probably good enough (%.2f)."

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 188
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x2

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v4, v1

    .line 187
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    :goto_0
    return-void

    .line 192
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->b()V

    .line 193
    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Lawn;

    .line 194
    iput p2, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:F

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;Lawr;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(Lawr;)V

    return-void
.end method

.method private a(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 260
    sget-boolean v1, LavX;->o:Z

    if-eqz v1, :cond_1

    .line 263
    :cond_0
    :goto_0
    return v0

    :cond_1
    sget v1, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:I

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(II)I

    move-result v1

    if-le p1, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Landroid/graphics/Rect;Laws;)Z
    .locals 2

    .prologue
    const/4 v1, 0x5

    .line 304
    sget-boolean v0, LavX;->f:Z

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Rect;->inset(II)V

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Lawn;

    new-instance v1, LawN;

    invoke-direct {v1, p0, p2}, LawN;-><init>(Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;Laws;)V

    invoke-virtual {v0, p1, v1}, Lawn;->a(Landroid/graphics/Rect;Laws;)Z

    move-result v0

    return v0
.end method

.method private c()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 487
    new-instance v3, Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tiles ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move v1, v2

    .line 488
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 489
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxx;

    .line 490
    const-string v4, "%d: %s, "

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v6, 0x1

    invoke-virtual {v0}, Laxx;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 488
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 492
    :cond_0
    const-string v0, "MosaicView"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Lawn;

    if-eqz v0, :cond_1

    .line 494
    const-string v0, "MosaicView"

    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Lawn;

    invoke-virtual {v1}, Lawn;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:LavU;

    iget-object v3, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v3}, LavU;->a(Landroid/graphics/Bitmap;)V

    .line 158
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Bitmap;

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Lawn;

    if-eqz v0, :cond_0

    .line 160
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->b()V

    .line 167
    :goto_0
    return-void

    .line 162
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Has Children with no TileBoard, e.g. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 163
    invoke-virtual {p0, v2}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 162
    invoke-static {v0, v3}, LauV;->a(ZLjava/lang/String;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Has TileViews with no TileBoard, e.g. "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/util/SparseArray;

    .line 165
    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 164
    invoke-static {v1, v0}, LauV;->a(ZLjava/lang/String;)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 162
    goto :goto_1

    :cond_2
    move v1, v2

    .line 164
    goto :goto_2
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    sget-boolean v0, LavX;->n:Z

    if-nez v0, :cond_0

    .line 252
    iput p1, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->c:I

    .line 253
    iget v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->c:I

    sget v1, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->b:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(II)I

    move-result v0

    .line 254
    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(I)Lcom/google/android/apps/viewer/client/Dimensions;

    move-result-object v0

    .line 255
    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:LawO;

    invoke-interface {v1, v0}, LawO;->a(Lcom/google/android/apps/viewer/client/Dimensions;)V

    .line 257
    :cond_0
    return-void
.end method

.method public a(ILandroid/graphics/Rect;F)V
    .locals 3

    .prologue
    .line 220
    iput p1, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->c:I

    .line 222
    invoke-direct {p0, p1}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(I)Z

    move-result v1

    .line 224
    sget-boolean v0, LavX;->n:Z

    if-nez v0, :cond_1

    .line 225
    if-eqz v1, :cond_4

    iget v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->c:I

    sget v2, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->b:I

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(II)I

    move-result v0

    .line 227
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-eq v2, v0, :cond_1

    .line 228
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(I)Lcom/google/android/apps/viewer/client/Dimensions;

    move-result-object v0

    .line 229
    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:LawO;

    invoke-interface {v2, v0}, LawO;->a(Lcom/google/android/apps/viewer/client/Dimensions;)V

    .line 233
    :cond_1
    if-eqz v1, :cond_5

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Lawn;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Lawn;

    invoke-virtual {v0, p1}, Lawn;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 235
    :cond_2
    iget v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->c:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(I)Lcom/google/android/apps/viewer/client/Dimensions;

    move-result-object v0

    .line 236
    invoke-direct {p0, v0, p3}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(Lcom/google/android/apps/viewer/client/Dimensions;F)V

    .line 237
    invoke-virtual {p0, p2, p3}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(Landroid/graphics/Rect;F)V

    .line 242
    :cond_3
    :goto_1
    return-void

    .line 225
    :cond_4
    iget v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->c:I

    sget v2, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:I

    .line 226
    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(II)I

    move-result v0

    goto :goto_0

    .line 240
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->b()V

    goto :goto_1
.end method

.method public a(Landroid/graphics/Canvas;Landroid/graphics/Point;Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    .line 446
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    sget-boolean v0, LavX;->p:Z

    if-nez v0, :cond_0

    .line 447
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 448
    iget v1, p2, Landroid/graphics/Point;->x:I

    iget v2, p2, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 449
    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->c:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 450
    invoke-static {v0, v1}, Lawk;->a(Landroid/graphics/Rect;F)Landroid/graphics/Rect;

    .line 451
    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v0, p3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 453
    :cond_0
    return-void
.end method

.method public a(Landroid/graphics/Rect;F)V
    .locals 6

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Lawn;

    if-nez v0, :cond_1

    .line 301
    :cond_0
    :goto_0
    return-void

    .line 279
    :cond_1
    iget v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:F

    cmpl-float v0, v0, p2

    if-eqz v0, :cond_2

    .line 280
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iget v1, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:F

    div-float/2addr v1, p2

    invoke-static {v0, v1}, Lawk;->a(Landroid/graphics/Rect;F)Landroid/graphics/Rect;

    move-result-object p1

    .line 283
    :cond_2
    iget v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->c:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(I)Lcom/google/android/apps/viewer/client/Dimensions;

    move-result-object v0

    .line 284
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->getTop()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:F

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 285
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->getLeft()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:F

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 286
    new-instance v3, Landroid/graphics/Rect;

    iget v4, v0, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    add-int/2addr v4, v2

    iget v5, v0, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    add-int/2addr v5, v1

    invoke-direct {v3, v2, v1, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 288
    invoke-virtual {v3, p1}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 289
    neg-int v2, v2

    neg-int v1, v1

    invoke-virtual {v3, v2, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 290
    new-instance v1, LawM;

    invoke-direct {v1, p0, v0}, LawM;-><init>(Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;Lcom/google/android/apps/viewer/client/Dimensions;)V

    invoke-direct {p0, v3, v1}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(Landroid/graphics/Rect;Laws;)Z

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/viewer/client/Dimensions;LavU;LawO;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Rect;

    iget v1, p1, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    iget v2, p1, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 112
    iput-object p2, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:LavU;

    .line 113
    iput-object p3, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:LawO;

    .line 114
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->removeAllViews()V

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Lawn;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Lawn;

    invoke-virtual {v0}, Lawn;->a()V

    .line 175
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Lawn;

    .line 176
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:F

    .line 178
    :cond_0
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 434
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 435
    sget-boolean v0, LavX;->f:Z

    if-eqz v0, :cond_0

    .line 436
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 437
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->c:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 438
    invoke-virtual {p1, v0, v0}, Landroid/graphics/Canvas;->scale(FF)V

    .line 439
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->b:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 440
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 442
    :cond_0
    return-void
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 3

    .prologue
    .line 457
    move-object v0, p2

    check-cast v0, Laxx;

    .line 458
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 459
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Lawn;

    iget-object v2, v2, Lawn;->b:Lcom/google/android/apps/viewer/client/Dimensions;

    iget v2, v2, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 460
    invoke-virtual {p1, v1, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 461
    invoke-virtual {v0}, Laxx;->a()Landroid/graphics/Point;

    move-result-object v0

    .line 462
    iget v1, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 463
    invoke-virtual {p2, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 464
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 465
    const/4 v0, 0x1

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 392
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Lawn;

    if-eqz v0, :cond_0

    sget-boolean v0, LavX;->p:Z

    if-eqz v0, :cond_4

    .line 393
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    .line 394
    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    .line 395
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 396
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 397
    invoke-virtual {p1, v2, v2}, Landroid/graphics/Canvas;->scale(FF)V

    .line 398
    iget-object v2, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v0, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 399
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 423
    :cond_1
    :goto_0
    sget-boolean v0, LavX;->f:Z

    if-eqz v0, :cond_2

    .line 424
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->getWidth()I

    move-result v6

    .line 425
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->getHeight()I

    move-result v7

    .line 426
    invoke-direct {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a()Ljava/lang/String;

    move-result-object v0

    div-int/lit8 v2, v6, 0x2

    int-to-float v2, v2

    div-int/lit8 v3, v7, 0x2

    add-int/lit8 v3, v3, -0xa

    int-to-float v3, v3

    sget-object v4, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 427
    int-to-float v3, v6

    int-to-float v4, v7

    sget-object v5, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 428
    int-to-float v2, v7

    int-to-float v3, v6

    sget-object v5, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 430
    :cond_2
    return-void

    .line 401
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Rect;

    sget-object v2, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 403
    :cond_4
    sget-boolean v0, LavX;->f:Z

    if-eqz v0, :cond_1

    .line 404
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v5

    .line 405
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 406
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    move v3, v4

    move v2, v4

    .line 408
    :goto_1
    if-ge v3, v5, :cond_5

    .line 409
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxx;

    .line 410
    iget-object v8, v0, Laxx;->a:Lawr;

    invoke-virtual {v8}, Lawr;->a()Landroid/graphics/Rect;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 411
    invoke-virtual {v0}, Laxx;->a()Z

    move-result v8

    if-nez v8, :cond_6

    .line 412
    iget-object v0, v0, Laxx;->a:Lawr;

    invoke-virtual {v0}, Lawr;->a()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 413
    add-int/lit8 v0, v2, 0x1

    .line 408
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_1

    .line 416
    :cond_5
    invoke-static {v6}, Lawk;->a(Landroid/graphics/Rect;)I

    move-result v0

    int-to-float v0, v0

    invoke-static {v7}, Lawk;->a(Landroid/graphics/Rect;)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v0, v3

    .line 417
    int-to-float v3, v2

    int-to-float v8, v5

    div-float/2addr v3, v8

    .line 418
    invoke-direct {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Empty tiles : %.2f %.2f "

    new-array v10, v12, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v10, v4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    invoke-direct {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a()Ljava/lang/String;

    move-result-object v0

    const-string v3, "Empty tiles : %s/%d, all: %s/%d"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v6, v8, v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v8, v11

    aput-object v7, v8, v12

    const/4 v2, 0x3

    .line 420
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v8, v2

    .line 419
    invoke-static {v3, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_6
    move v0, v2

    goto :goto_2
.end method

.method protected onLayout(ZIIII)V
    .locals 8

    .prologue
    .line 378
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v2

    .line 379
    if-eqz v2, :cond_0

    .line 380
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->c:I

    int-to-float v1, v1

    div-float v3, v0, v1

    .line 381
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 382
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxx;

    .line 383
    iget-object v4, v0, Laxx;->a:Lawr;

    invoke-virtual {v4}, Lawr;->a()Landroid/graphics/Rect;

    move-result-object v4

    invoke-static {v4, v3}, Lawk;->a(Landroid/graphics/Rect;F)Landroid/graphics/Rect;

    move-result-object v4

    .line 384
    iget v5, v4, Landroid/graphics/Rect;->left:I

    iget v6, v4, Landroid/graphics/Rect;->top:I

    iget v7, v4, Landroid/graphics/Rect;->right:I

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v5, v6, v7, v4}, Laxx;->layout(IIII)V

    .line 381
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 387
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    .line 364
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->setMeasuredDimension(II)V

    .line 365
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v2

    .line 366
    if-eqz v2, :cond_0

    .line 367
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->c:I

    int-to-float v1, v1

    div-float v3, v0, v1

    .line 368
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 369
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxx;

    .line 370
    iget-object v4, v0, Laxx;->a:Lawr;

    invoke-virtual {v4}, Lawr;->a()Lcom/google/android/apps/viewer/client/Dimensions;

    move-result-object v4

    .line 371
    iget v5, v4, Lcom/google/android/apps/viewer/client/Dimensions;->width:I

    int-to-float v5, v5

    mul-float/2addr v5, v3

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v5, v6

    iget v4, v4, Lcom/google/android/apps/viewer/client/Dimensions;->height:I

    int-to-float v4, v4

    mul-float/2addr v4, v3

    float-to-double v6, v4

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v4, v6

    invoke-virtual {v0, v5, v4}, Laxx;->measure(II)V

    .line 368
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 374
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 134
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v0, LavX;->f:Z

    if-eqz v0, :cond_0

    .line 135
    invoke-direct {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->c()V

    .line 137
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setPageBitmap(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 146
    const-string v0, "Use removePageBitmap() instead."

    invoke-static {p1, v0}, LauV;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 147
    iput-object p1, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Bitmap;

    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->invalidate()V

    .line 149
    return-void
.end method

.method public setTileBitmap(Lawr;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 351
    const-string v0, "Use removePageBitmap() instead."

    invoke-static {p2, v0}, LauV;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Lawn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Lawn;

    invoke-virtual {v0, p1, p2}, Lawn;->a(Lawr;Landroid/graphics/Bitmap;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353
    invoke-virtual {p1}, Lawr;->a()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a(I)Laxx;

    move-result-object v0

    .line 354
    if-eqz v0, :cond_1

    .line 355
    invoke-virtual {v0, p1, p2}, Laxx;->a(Lawr;Landroid/graphics/Bitmap;)V

    .line 360
    :cond_0
    :goto_0
    return-void

    .line 357
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No tile for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 482
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bg: %s /t: %s"

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    aput-object v0, v3, v4

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Lawn;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/viewer/viewer/pdf/MosaicView;->a:Lawn;

    .line 483
    invoke-virtual {v0}, Lawn;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    aput-object v0, v3, v4

    .line 482
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "x"

    goto :goto_0

    .line 483
    :cond_1
    const-string v0, "no tiles"

    goto :goto_1
.end method
