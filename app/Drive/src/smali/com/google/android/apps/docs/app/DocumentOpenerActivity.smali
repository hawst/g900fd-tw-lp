.class public Lcom/google/android/apps/docs/app/DocumentOpenerActivity;
.super LpH;
.source "DocumentOpenerActivity.java"


# instance fields
.field public a:LSF;

.field public a:LTT;

.field public a:LaGR;

.field public a:LsF;

.field public a:LtK;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, LpH;-><init>()V

    .line 60
    return-void
.end method

.method private a(Landroid/content/Intent;)Lcom/google/android/gms/drive/database/data/ResourceSpec;
    .locals 5

    .prologue
    .line 243
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 244
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    .line 245
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    .line 246
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, LzT;->a(Landroid/os/Bundle;)LaFO;

    move-result-object v0

    .line 247
    if-nez v0, :cond_0

    .line 248
    const-string v0, "accountName"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v0

    .line 254
    :goto_0
    if-eqz v0, :cond_1

    .line 255
    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a(LaFO;Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    .line 259
    :goto_1
    return-object v0

    .line 250
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LqK;

    const-string v3, "search"

    const-string v4, "searchSuggestionOpen"

    invoke-virtual {v2, v3, v4}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 259
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;Landroid/content/Intent;)Lcom/google/android/gms/drive/database/data/ResourceSpec;
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Landroid/content/Intent;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    return-object v0
.end method

.method private a(I)V
    .locals 6

    .prologue
    .line 263
    new-instance v0, LES;

    .line 265
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a()LM;

    move-result-object v1

    const/4 v2, 0x0

    check-cast v2, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 267
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    invoke-static {v3}, LFU;->a(Landroid/os/Bundle;)Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    move-result-object v3

    sget v4, Lxi;->error_page_title:I

    .line 268
    invoke-virtual {p0, v4}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 269
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LES;-><init>(LM;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    invoke-virtual {v0}, LES;->a()V

    .line 272
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 150
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 151
    const-class v0, Lcom/google/android/apps/docs/app/DocListActivity;

    invoke-virtual {v1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 152
    const/high16 v0, 0x2000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 153
    const-string v0, "app_data"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 154
    if-nez v0, :cond_0

    .line 155
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 156
    const-string v2, "app_data"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 158
    :cond_0
    const-string v2, "accountName"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 159
    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LSF;

    invoke-interface {v2}, LSF;->a()Landroid/accounts/Account;

    move-result-object v2

    .line 160
    const-string v3, "accountName"

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v2

    invoke-virtual {v2}, LaFO;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    :cond_1
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->startActivity(Landroid/content/Intent;)V

    .line 164
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->finish()V

    .line 165
    return-void
.end method

.method private a(Landroid/content/Intent;LaGu;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 203
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    const-string v0, "android.intent.action.VIEW"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LTT;

    invoke-interface {v0, p2}, LTT;->d(LaGu;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LTT;

    .line 209
    invoke-interface {v0, p2, p1}, LTT;->a(LaGu;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 211
    if-eqz v0, :cond_0

    .line 214
    invoke-virtual {p0, v0, v3}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 240
    :goto_0
    return-void

    .line 217
    :cond_0
    sget v0, Lxi;->error_no_viewer_available:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(I)V

    goto :goto_0

    .line 228
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 229
    const-class v1, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 230
    const-string v1, "entrySpec.v2"

    invoke-interface {p2}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 231
    const-string v1, "preferOpenInProjector"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 233
    const-string v1, "startNewDocCentricTask"

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 234
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LsF;

    .line 235
    invoke-interface {p2}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-interface {p2}, LaGu;->c()Ljava/lang/String;

    move-result-object v3

    .line 234
    invoke-interface {v1, v2, v3, v0}, LsF;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;Ljava/lang/String;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 238
    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->startActivity(Landroid/content/Intent;)V

    .line 239
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->finish()V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;I)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(I)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;Landroid/content/Intent;LaGu;Z)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Landroid/content/Intent;LaGu;Z)V

    return-void
.end method

.method private b(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 168
    const-string v0, "android.intent.action.SEARCH"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Landroid/content/Intent;)V

    .line 200
    :goto_0
    return-void

    .line 173
    :cond_0
    const-string v0, "entrySpec.v2"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 176
    if-nez v0, :cond_1

    const/4 v1, 0x1

    .line 178
    :goto_1
    new-instance v2, LsO;

    invoke-direct {v2, p0, v0, p1, v1}, LsO;-><init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;Lcom/google/android/gms/drive/database/data/EntrySpec;Landroid/content/Intent;Z)V

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LaGR;

    invoke-virtual {v0, v2}, LaGR;->a(LaGN;)V

    goto :goto_0

    .line 176
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 140
    if-nez p1, :cond_0

    .line 141
    invoke-virtual {p0, p2, p3}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->setResult(ILandroid/content/Intent;)V

    .line 146
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->finish()V

    .line 147
    return-void

    .line 143
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->setResult(I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 129
    invoke-super {p0, p1}, LpH;->onCreate(Landroid/os/Bundle;)V

    .line 133
    if-nez p1, :cond_0

    .line 134
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->b(Landroid/content/Intent;)V

    .line 136
    :cond_0
    return-void
.end method
