.class public Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;
.super Lrm;
.source "WebViewOpenActivity.java"

# interfaces
.implements LET;


# instance fields
.field private a:LDL;

.field private a:LGG;

.field private final a:LGH;

.field private a:LGJ;

.field public a:LQr;

.field public a:LTO;

.field public a:LTd;

.field public a:LUT;

.field public a:LaGM;

.field public a:Lald;

.field private final a:Landroid/webkit/WebChromeClient;

.field private a:Landroid/webkit/WebSettings;

.field private a:Landroid/webkit/WebView;

.field public a:Ljava/lang/Class;
    .annotation runtime Lbxv;
        a = "DocListActivity"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/lang/String;

.field public a:LtK;

.field private final b:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 80
    invoke-direct {p0}, Lrm;-><init>()V

    .line 143
    new-instance v0, LGP;

    invoke-direct {v0, p0}, LGP;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LGH;

    .line 186
    new-instance v0, LGQ;

    invoke-direct {v0, p0}, LGQ;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebChromeClient;

    .line 256
    iput-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LDL;

    .line 259
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->b:Landroid/os/Handler;

    .line 260
    iput-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LGJ;

    .line 537
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;LDL;)LDL;
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LDL;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;)LGG;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LGG;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebView;

    return-object v0
.end method

.method private a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/ResourceSpec;
    .locals 4

    .prologue
    .line 434
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a()LaFO;

    move-result-object v0

    .line 435
    if-nez v0, :cond_0

    .line 436
    const-string v0, "WebViewOpenActivity"

    const-string v1, "accountId null trying to construct ResourceSpec for resourceId: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 438
    const/4 v0, 0x0

    .line 440
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0, p1}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a(LaFO;Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 318
    const-string v0, "accountName"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LaFO;->a(Ljava/lang/String;)LaFO;

    move-result-object v4

    .line 320
    new-instance v0, LGR;

    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LGH;

    iget-object v5, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LQr;

    iget-object v6, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Ljava/lang/Class;

    iget-object v7, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LUT;

    const-string v1, "webview"

    .line 321
    invoke-virtual {p0, v1, v11}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LTd;

    iget-object v10, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->b:Landroid/os/Handler;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v10}, LGR;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;Landroid/content/Context;LGH;LaFO;LQr;Ljava/lang/Class;LUT;Landroid/content/SharedPreferences;LTd;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LGJ;

    .line 329
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LGJ;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 330
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebChromeClient;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 332
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 333
    if-nez v0, :cond_0

    .line 334
    const-string v0, "WebViewOpenActivity"

    const-string v1, "URL to load is not specified."

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->finish()V

    .line 370
    :goto_0
    return-void

    .line 338
    :cond_0
    const-string v1, "docListTitle"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 340
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LUT;

    invoke-interface {v2, p0, v0}, LUT;->a(Landroid/content/Context;Landroid/net/Uri;)LVa;

    move-result-object v0

    .line 341
    invoke-virtual {v0}, LVa;->a()Landroid/net/Uri;

    move-result-object v2

    .line 343
    sget-object v3, LaGv;->g:LaGv;

    invoke-virtual {v0}, LVa;->a()LaGv;

    move-result-object v4

    invoke-virtual {v3, v4}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 347
    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v3, v11}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 348
    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v3, v11}, Landroid/webkit/WebView;->setVerticalScrollbarOverlay(Z)V

    .line 367
    :cond_1
    :goto_1
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 368
    new-instance v3, LGG;

    invoke-direct {v3, v2, v1, v0}, LGG;-><init>(Ljava/lang/String;Ljava/lang/String;LVa;)V

    iput-object v3, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LGG;

    .line 369
    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 349
    :cond_2
    sget-object v3, LaGv;->d:LaGv;

    invoke-virtual {v0}, LVa;->a()LaGv;

    move-result-object v4

    invoke-virtual {v3, v4}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 352
    new-instance v3, Landroid/view/GestureDetector;

    new-instance v4, LGS;

    invoke-direct {v4, p0}, LGS;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;)V

    invoke-direct {v3, p0, v4}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 359
    iget-object v4, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebView;

    new-instance v5, LGT;

    invoke-direct {v5, p0, v3}, LGT;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;Landroid/view/GestureDetector;)V

    invoke-virtual {v4, v5}, Landroid/webkit/WebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->c(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 444
    const-string v0, "WebViewOpenActivity"

    const-string v1, "Start URL %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 446
    const-string v0, "present"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 447
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LakQ;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 450
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LTO;

    invoke-interface {v1}, LTO;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 458
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LGJ;

    invoke-virtual {v0, p1}, LGJ;->a(Ljava/lang/String;)V

    .line 459
    return-void

    .line 452
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    goto :goto_0

    .line 455
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 491
    sget v0, Lxi;->error_page_title:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 492
    sget v0, Lxi;->error_opening_document_for_html:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 493
    new-array v1, v5, [Ljava/lang/Object;

    aput-object p1, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 494
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 495
    const-string v1, "entrySpec.v2"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/database/data/EntrySpec;

    .line 497
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, LFU;->a(Landroid/os/Bundle;)Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    move-result-object v2

    .line 498
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a()LM;

    .line 499
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 500
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a()LM;

    move-result-object v0

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a(LM;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 511
    :goto_0
    return-void

    .line 507
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 508
    sget v1, Lxi;->error_opening_document:I

    invoke-static {v0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 509
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->finish()V

    goto :goto_0
.end method


# virtual methods
.method public a()LaFO;
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LGJ;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LGJ;

    invoke-virtual {v0}, LGJ;->a()LaFO;

    move-result-object v0

    goto :goto_0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 515
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a(Landroid/content/Intent;)V

    .line 516
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 266
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->requestWindowFeature(I)Z

    .line 268
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 270
    sget v0, Lxe;->web_view_open:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->setContentView(I)V

    .line 272
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a()LM;

    move-result-object v0

    .line 273
    sget v1, Lxc;->webview:I

    invoke-virtual {v0, v1}, LM;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/fragment/WebViewFragment;

    .line 274
    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/WebViewFragment;->a()Landroid/webkit/WebView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebView;

    .line 277
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/view/Window;->setFeatureInt(II)V

    .line 279
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    sget-object v1, Landroid/webkit/WebSettings$PluginState;->ON:Landroid/webkit/WebSettings$PluginState;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setPluginState(Landroid/webkit/WebSettings$PluginState;)V

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 286
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    .line 289
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSupportMultipleWindows(Z)V

    .line 291
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setLightTouchEnabled(Z)V

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptCanOpenWindowsAutomatically(Z)V

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 294
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebView;

    new-instance v1, LGW;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, LGW;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;LGP;)V

    const-string v2, "mkxNativeWrapperApi"

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 305
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "appcache"

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 306
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v1, v0}, Landroid/webkit/WebSettings;->setAppCachePath(Ljava/lang/String;)V

    .line 307
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    const-wide/32 v2, 0x400000

    invoke-virtual {v0, v2, v3}, Landroid/webkit/WebSettings;->setAppCacheMaxSize(J)V

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v4}, Landroid/webkit/WebView;->setClipToPadding(Z)V

    .line 311
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LTO;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v1}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LTO;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Ljava/lang/String;

    .line 312
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 313
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a(Landroid/content/Intent;)V

    .line 314
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 463
    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    .line 464
    new-instance v0, LAd;

    invoke-direct {v0, p0, v1}, LAd;-><init>(Landroid/content/Context;I)V

    .line 466
    invoke-virtual {v0, v1}, LAd;->setCancelable(Z)V

    .line 469
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 395
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 396
    sget v2, Lxf;->menu_webview:I

    invoke-virtual {v0, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 398
    sget v0, Lxc;->menu_sharing:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 399
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LakQ;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LGG;

    .line 400
    invoke-virtual {v0}, LGG;->a()LVa;

    move-result-object v0

    invoke-virtual {v0}, LVa;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 401
    :goto_0
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 403
    return v1

    .line 400
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 374
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LGG;

    .line 375
    invoke-super {p0}, Lrm;->onDestroy()V

    .line 376
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 408
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lxc;->menu_refresh:I

    if-ne v1, v2, :cond_1

    .line 410
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LGJ;

    invoke-virtual {v1}, LGJ;->a()V

    .line 411
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LGG;

    invoke-virtual {v2}, LGG;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 428
    :cond_0
    :goto_0
    return v0

    .line 413
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lxc;->menu_sharing:I

    if-ne v1, v2, :cond_2

    .line 414
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LGG;

    invoke-virtual {v1}, LGG;->a()LVa;

    move-result-object v1

    invoke-virtual {v1}, LVa;->a()Ljava/lang/String;

    move-result-object v1

    .line 415
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v1

    .line 416
    if-eqz v1, :cond_0

    .line 418
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LGG;

    invoke-virtual {v3}, LGG;->a()Ljava/lang/String;

    move-result-object v3

    .line 417
    invoke-static {v2, v1, v3}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 419
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 422
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lxc;->menu_send_feedback:I

    if-ne v1, v2, :cond_3

    .line 423
    new-instance v1, Lali;

    const-string v2, "android_docs"

    invoke-direct {v1, p0, v2}, Lali;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    .line 425
    invoke-virtual {v1}, Lali;->a()V

    goto :goto_0

    .line 428
    :cond_3
    invoke-super {p0, p1}, Lrm;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 387
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LqK;

    const-string v1, "/webOpen"

    invoke-virtual {v0, p0, v1}, LqK;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 388
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 389
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->stopSync()V

    .line 390
    invoke-super {p0}, Lrm;->onPause()V

    .line 391
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 1

    .prologue
    .line 474
    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LDL;

    if-eqz v0, :cond_0

    .line 475
    check-cast p2, LAd;

    .line 477
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LDL;

    invoke-interface {v0}, LDL;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LAd;->a(Ljava/lang/String;)V

    .line 478
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LDL;

    invoke-virtual {p2, v0}, LAd;->a(LDL;)V

    .line 479
    invoke-virtual {p2}, LAd;->a()V

    .line 480
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LDL;

    .line 482
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 380
    invoke-super {p0}, Lrm;->onResume()V

    .line 381
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LqK;

    invoke-virtual {v0, p0}, LqK;->a(Ljava/lang/Object;)V

    .line 382
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->startSync()V

    .line 383
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 486
    invoke-super {p0}, Lrm;->onStop()V

    .line 487
    return-void
.end method
