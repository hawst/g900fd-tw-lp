.class public Lcom/google/android/apps/docs/app/ErrorNotificationActivity;
.super Lrm;
.source "ErrorNotificationActivity.java"


# instance fields
.field private a:I

.field public a:LaEz;

.field public a:Lall;

.field private a:Landroid/app/AlertDialog;

.field private a:Ljava/lang/String;

.field public a:LtK;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lrm;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 155
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.BUG_REPORT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 156
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 157
    const-class v1, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 158
    const-string v1, "dumpDatabase"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 159
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Throwable;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 143
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.BUG_REPORT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 144
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 145
    const-class v1, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 146
    const-string v1, "notification_message"

    sget v2, Lxi;->ouch_msg_unhandled_exception:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 148
    invoke-static {p1}, LalV;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    .line 149
    const-string v2, "stack_trace"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 150
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/ErrorNotificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 166
    invoke-static {}, Laml;->a()Lrx;

    move-result-object v0

    sget-object v1, Lrx;->c:Lrx;

    invoke-virtual {v0, v1}, Lrx;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    new-instance v0, LtI;

    invoke-direct {v0, p0}, LtI;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 179
    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/high16 v7, 0x20000

    const/4 v3, 0x0

    .line 58
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 60
    sget v0, Lxi;->ouch_title_sawwrie:I

    .line 61
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 62
    const-string v2, "notification_message"

    const/4 v4, -0x1

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:I

    .line 63
    const-string v2, "stack_trace"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:Ljava/lang/String;

    .line 65
    const-string v2, "dumpDatabase"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:LtK;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:LtK;

    sget-object v2, Lry;->aS:Lry;

    .line 66
    invoke-interface {v1, v2}, LtK;->a(LtJ;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    move v2, v1

    .line 67
    :goto_0
    if-eqz v2, :cond_3

    .line 69
    sget v0, Lxi;->ouch_authorize_database_dump:I

    iput v0, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:I

    .line 70
    sget v0, Lxi;->gf_feedback:I

    move v1, v0

    .line 74
    :goto_1
    :try_start_0
    const-string v0, "ErrorNotificationActivity"

    iget v4, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 82
    const-string v0, "ErrorNotificationActivity"

    iget-object v4, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:Ljava/lang/String;

    invoke-static {v0, v4}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:Lall;

    if-nez v0, :cond_1

    .line 88
    const-string v0, "ErrorNotificationActivity"

    const-string v4, "This should never happen: feedbackReporter not initialized by guice"

    invoke-static {v0, v4}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    new-instance v0, Lalk;

    invoke-direct {v0}, Lalk;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:Lall;

    .line 92
    :cond_1
    invoke-static {p0}, LEL;->a(Landroid/content/Context;)LEU;

    move-result-object v0

    .line 93
    const v4, 0x1080027

    .line 94
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 95
    invoke-virtual {v4, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget v4, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:I

    .line 96
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 97
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v3, Lxi;->ouch_button_close:I

    new-instance v4, LtG;

    invoke-direct {v4, p0}, LtG;-><init>(Lcom/google/android/apps/docs/app/ErrorNotificationActivity;)V

    .line 98
    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v3, Lxi;->ouch_button_report:I

    new-instance v4, LtF;

    invoke-direct {v4, p0, v2}, LtF;-><init>(Lcom/google/android/apps/docs/app/ErrorNotificationActivity;Z)V

    .line 104
    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 123
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:Landroid/app/AlertDialog;

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:Landroid/app/AlertDialog;

    new-instance v1, LtH;

    invoke-direct {v1, p0}, LtH;-><init>(Lcom/google/android/apps/docs/app/ErrorNotificationActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v7, v7}, Landroid/view/Window;->setFlags(II)V

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 134
    return-void

    :cond_2
    move v2, v3

    .line 66
    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    const-string v4, "ErrorNotificationActivity"

    const-string v5, "There was a problem with the error message in our intent, defaulting to ouch_msg_unhandled_exception."

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v4, v0, v5, v6}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 78
    sget v0, Lxi;->ouch_msg_unhandled_exception:I

    iput v0, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:I

    goto :goto_2

    :cond_3
    move v1, v0

    goto/16 :goto_1
.end method
