.class public Lcom/google/android/apps/docs/utils/RawPixelData;
.super Ljava/lang/Object;
.source "RawPixelData.java"


# instance fields
.field private a:J

.field private final a:LamE;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-string v0, "docsimageutils"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 37
    return-void
.end method

.method public constructor <init>(LamE;)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    iput-object p1, p0, Lcom/google/android/apps/docs/utils/RawPixelData;->a:LamE;

    .line 42
    invoke-static {}, Lcom/google/android/apps/docs/utils/RawPixelData;->createNativeBuffer()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/docs/utils/RawPixelData;->a:J

    .line 43
    return-void
.end method

.method public static a(Landroid/graphics/Bitmap;LamE;)Lcom/google/android/apps/docs/utils/RawPixelData;
    .locals 1

    .prologue
    .line 58
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    new-instance v0, Lcom/google/android/apps/docs/utils/RawPixelData;

    invoke-direct {v0, p1}, Lcom/google/android/apps/docs/utils/RawPixelData;-><init>(LamE;)V

    .line 60
    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/utils/RawPixelData;->a(Landroid/graphics/Bitmap;)Z

    .line 61
    return-object v0
.end method

.method private static native capacity(J)I
.end method

.method private static native copyFrom(JLandroid/graphics/Bitmap;)Z
.end method

.method private static native copyTo(JLandroid/graphics/Bitmap;)Z
.end method

.method private static native createNativeBuffer()J
.end method

.method private static native delete(J)V
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 92
    iget-wide v0, p0, Lcom/google/android/apps/docs/utils/RawPixelData;->a:J

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/utils/RawPixelData;->capacity(J)I

    move-result v0

    return v0
.end method

.method public a()LamE;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/docs/utils/RawPixelData;->a:LamE;

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 84
    iget-wide v0, p0, Lcom/google/android/apps/docs/utils/RawPixelData;->a:J

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/utils/RawPixelData;->delete(J)V

    .line 85
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/docs/utils/RawPixelData;->a:J

    .line 86
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)Z
    .locals 4

    .prologue
    .line 68
    iget-wide v0, p0, Lcom/google/android/apps/docs/utils/RawPixelData;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 69
    iget-wide v0, p0, Lcom/google/android/apps/docs/utils/RawPixelData;->a:J

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/utils/RawPixelData;->copyFrom(JLandroid/graphics/Bitmap;)Z

    move-result v0

    return v0

    .line 68
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/graphics/Bitmap;)Z
    .locals 4

    .prologue
    .line 76
    iget-wide v0, p0, Lcom/google/android/apps/docs/utils/RawPixelData;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 77
    iget-wide v0, p0, Lcom/google/android/apps/docs/utils/RawPixelData;->a:J

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/utils/RawPixelData;->copyTo(JLandroid/graphics/Bitmap;)Z

    move-result v0

    return v0

    .line 76
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/docs/utils/RawPixelData;->a()V

    .line 98
    return-void
.end method
