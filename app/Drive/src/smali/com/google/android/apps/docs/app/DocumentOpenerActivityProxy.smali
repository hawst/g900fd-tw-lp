.class public Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;
.super LpH;
.source "DocumentOpenerActivityProxy.java"


# instance fields
.field public a:LTZ;

.field public a:LUD;

.field public a:Lakk;

.field public a:Lald;

.field public a:LsF;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, LpH;-><init>()V

    return-void
.end method

.method private a(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 119
    invoke-static {p1}, Lala;->a(LaGu;)Ljava/lang/String;

    move-result-object v0

    .line 120
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;->a:LqK;

    const-string v2, "documentOpener"

    const-string v3, "crossAppOpenDocument"

    invoke-virtual {v1, v2, v3, v0}, LqK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p3}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 126
    const-class v1, Lcom/google/android/apps/docs/app/DocumentOpenerActivityDelegate;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 127
    const-string v1, "entrySpec.v2"

    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 129
    sget-object v1, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->a:Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    invoke-virtual {v1, p2}, Lcom/google/android/apps/docs/app/DocumentOpenMethod;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, LaGv;->a:LaGv;

    .line 130
    invoke-interface {p1}, LaGu;->a()LaGv;

    move-result-object v2

    invoke-virtual {v1, v2}, LaGv;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 131
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;->a:LsF;

    .line 132
    invoke-interface {p1}, LaGu;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-interface {p1}, LaGu;->c()Ljava/lang/String;

    move-result-object v3

    .line 131
    invoke-interface {v1, v2, v3, v0}, LsF;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;Ljava/lang/String;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 135
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;->startActivity(Landroid/content/Intent;)V

    .line 136
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;->finish()V

    .line 137
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;->a(LaGu;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 59
    invoke-super {p0, p1}, LpH;->onCreate(Landroid/os/Bundle;)V

    .line 61
    if-eqz p1, :cond_0

    .line 116
    :goto_0
    return-void

    .line 65
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    .line 66
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;->a:Lakk;

    .line 67
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lakk;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 70
    :cond_1
    const-string v0, "DocumentOpenerActivityProxy"

    const-string v1, "Finishing: unauthorized invocation"

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;->finish()V

    goto :goto_0

    .line 75
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;->a:LTZ;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, LTZ;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v1

    .line 77
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 78
    invoke-static {v0}, LFU;->a(Landroid/os/Bundle;)Lcom/google/android/apps/docs/app/DocumentOpenMethod;

    move-result-object v2

    .line 80
    const-string v0, "resourceSpec"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/ResourceSpec;

    .line 81
    iget-object v3, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;->a:LUD;

    invoke-virtual {v3, v0}, LUD;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LbsU;

    move-result-object v0

    .line 82
    sget v3, Lxi;->open_url_getting_entry:I

    .line 83
    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 82
    invoke-static {p0, v0, v3}, Lse;->a(Landroid/app/Activity;LbsU;Ljava/lang/String;)Landroid/app/ProgressDialog;

    move-result-object v3

    .line 85
    new-instance v4, Lta;

    invoke-direct {v4, p0, v3, v2, v1}, Lta;-><init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivityProxy;Landroid/app/ProgressDialog;Lcom/google/android/apps/docs/app/DocumentOpenMethod;Landroid/content/Intent;)V

    .line 115
    invoke-static {}, Lanj;->a()Ljava/util/concurrent/Executor;

    move-result-object v1

    .line 85
    invoke-static {v0, v4, v1}, LbsK;->a(LbsU;LbsJ;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
