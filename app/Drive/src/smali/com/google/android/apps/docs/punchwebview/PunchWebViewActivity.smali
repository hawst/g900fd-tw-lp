.class public Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;
.super Lrm;
.source "PunchWebViewActivity.java"

# interfaces
.implements LWY;
.implements LZG;
.implements LZm;


# static fields
.field private static final a:[I


# instance fields
.field public a:LQr;

.field public a:LTT;

.field private a:LXA;

.field public a:LXC;

.field public a:LXJ;

.field public a:LXt;

.field public a:LXx;

.field public a:LYO;

.field private a:LYP;

.field public a:LaGM;

.field public a:LaGR;

.field private a:Landroid/support/v4/app/Fragment;

.field private a:Landroid/view/View;

.field private a:Landroid/view/ViewGroup;

.field private a:Landroid/webkit/WebChromeClient$CustomViewCallback;

.field private a:Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;

.field private a:Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;

.field private a:Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;

.field private a:Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;

.field private a:Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;

.field private a:Ljava/lang/String;

.field public a:LsP;

.field public a:Lye;

.field private final b:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0xc8
        0x1f4
        0x3e8
        0xfa0
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Lrm;-><init>()V

    .line 91
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->b:Landroid/os/Handler;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->b:Landroid/os/Handler;

    return-object v0
.end method

.method private a()Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;
    .locals 2

    .prologue
    .line 624
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()LM;

    move-result-object v0

    const-string v1, "ViewModeFragment"

    .line 625
    invoke-virtual {v0, v1}, LM;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;

    return-object v0
.end method

.method private a()Lcom/google/android/gms/drive/database/data/ResourceSpec;
    .locals 2

    .prologue
    .line 224
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "resourceSpec"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/ResourceSpec;

    return-object v0
.end method

.method private a(Lac;)V
    .locals 2

    .prologue
    .line 558
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;

    if-nez v0, :cond_0

    .line 559
    sget v0, LpN;->speaker_notes_presence_panel:I

    sget v1, LpN;->speaker_notes_content_panel:I

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;->a(II)Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;

    .line 563
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;

    const-string v1, "SpeakerNotesFragmentTag"

    invoke-virtual {p1, v0, v1}, Lac;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lac;

    .line 564
    return-void
.end method

.method private a(Lac;Lcom/google/android/gms/drive/database/data/ResourceSpec;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 598
    const-string v0, "PunchWebViewActivity"

    const-string v1, "in addModelFragment"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/support/v4/app/Fragment;

    if-nez v0, :cond_1

    .line 601
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()LM;

    move-result-object v0

    const-string v1, "webView"

    invoke-virtual {v0, v1}, LM;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/support/v4/app/Fragment;

    .line 602
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    .line 611
    :goto_0
    return-void

    .line 606
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LXC;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Ljava/lang/String;

    .line 607
    invoke-interface {v0, p3, p2, v1}, LXC;->a(Landroid/net/Uri;Lcom/google/android/gms/drive/database/data/ResourceSpec;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/support/v4/app/Fragment;

    .line 610
    :cond_1
    sget v0, LpN;->web_view_container:I

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/support/v4/app/Fragment;

    const-string v2, "webView"

    invoke-virtual {p1, v0, v1, v2}, Lac;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lac;

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 164
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 165
    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/view/ViewGroup;

    .line 170
    :goto_0
    return-void

    .line 167
    :cond_0
    const-string v0, "PunchWebViewActivity"

    const-string v1, "Failed to set rootView as received:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 168
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/view/ViewGroup;

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->m()V

    return-void
.end method

.method private a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 494
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()LM;

    move-result-object v0

    invoke-virtual {v0}, LM;->a()Lac;

    move-result-object v0

    .line 497
    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a(Lac;Lcom/google/android/gms/drive/database/data/ResourceSpec;Landroid/net/Uri;)V

    .line 500
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->d(Lac;)V

    .line 503
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->b(Lac;)V

    .line 508
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->e(Lac;)V

    .line 509
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->f(Lac;)V

    .line 510
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a(Lac;)V

    .line 511
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->c(Lac;)V

    .line 513
    invoke-virtual {v0}, Lac;->b()I

    .line 514
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()LM;

    move-result-object v0

    invoke-virtual {v0}, LM;->a()Z

    .line 515
    return-void
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 472
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->setContentView(I)V

    .line 474
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 475
    const-string v1, "resourceSpec"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/ResourceSpec;

    .line 477
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 479
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Landroid/net/Uri;)V

    .line 481
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->p()V

    .line 483
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()V

    .line 484
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->j()V

    .line 485
    return-void
.end method

.method private b(Lac;)V
    .locals 3

    .prologue
    .line 568
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LYO;

    invoke-interface {v0}, LYO;->a()LYR;

    move-result-object v0

    sget-object v1, LYR;->d:LYR;

    if-eq v0, v1, :cond_0

    .line 578
    :goto_0
    return-void

    .line 572
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;

    if-nez v0, :cond_1

    .line 573
    new-instance v0, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;

    .line 576
    :cond_1
    sget v0, LpN;->main_container:I

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;

    const-string v2, "NextPreviousButtonsFragmentTag"

    invoke-virtual {p1, v0, v1, v2}, Lac;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lac;

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->n()V

    return-void
.end method

.method private c(Lac;)V
    .locals 3

    .prologue
    .line 581
    const-string v0, "PunchWebViewActivity"

    const-string v1, "in addDragKnobFragment"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    sget v0, LpN;->drag_knob_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 594
    :goto_0
    return-void

    .line 586
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;

    if-nez v0, :cond_1

    .line 587
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LXt;

    invoke-virtual {v0}, LXt;->b()Z

    move-result v0

    .line 588
    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    sget v1, LpR;->punch_drag_knob_button_content_description_close_slide_picker:I

    .line 589
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, LpR;->punch_drag_knob_button_content_description_open_slide_picker:I

    .line 590
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 588
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a(ZLjava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;

    .line 593
    :cond_1
    sget v0, LpN;->drag_knob_container:I

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;

    const-string v2, "dragKnob"

    invoke-virtual {p1, v0, v1, v2}, Lac;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lac;

    goto :goto_0

    .line 588
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->k()V

    return-void
.end method

.method private d(Lac;)V
    .locals 3

    .prologue
    .line 614
    const-string v0, "PunchWebViewActivity"

    const-string v1, "in addViewModeFragment"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 615
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;

    move-result-object v0

    .line 616
    if-nez v0, :cond_0

    .line 617
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LXx;

    invoke-interface {v0}, LXx;->a()Lcom/google/android/apps/docs/punchwebview/PunchSingleScreenModeFragment;

    move-result-object v0

    .line 618
    sget v1, LpN;->main_container:I

    const-string v2, "ViewModeFragment"

    invoke-virtual {p1, v1, v0, v2}, Lac;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lac;

    .line 621
    :cond_0
    return-void
.end method

.method public static synthetic d(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->l()V

    return-void
.end method

.method private e(Lac;)V
    .locals 3

    .prologue
    .line 629
    sget v0, LpN;->grid_slide_picker_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 631
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;

    if-nez v0, :cond_0

    .line 632
    invoke-static {}, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a()Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;

    .line 635
    :cond_0
    sget v0, LpN;->grid_slide_picker_container:I

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;

    const-string v2, "slidePicker"

    invoke-virtual {p1, v0, v1, v2}, Lac;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lac;

    .line 638
    :cond_1
    return-void
.end method

.method public static synthetic e(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->r()V

    return-void
.end method

.method private f(Lac;)V
    .locals 3

    .prologue
    .line 641
    sget v0, LpN;->list_slide_picker_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 643
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;

    if-nez v0, :cond_0

    .line 644
    invoke-static {}, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a()Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;

    .line 647
    :cond_0
    sget v0, LpN;->list_slide_picker_container:I

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;

    const-string v2, "slidePicker"

    invoke-virtual {p1, v0, v1, v2}, Lac;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lac;

    .line 650
    :cond_1
    return-void
.end method

.method public static synthetic f(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->t()V

    return-void
.end method

.method private f()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 210
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LQr;

    const-string v3, "enablePunchWebViewEditButton"

    invoke-interface {v2, v3, v0}, LQr;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 219
    :cond_0
    :goto_0
    return v0

    .line 213
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LaGM;

    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v3

    invoke-interface {v2, v3}, LaGM;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGu;

    move-result-object v2

    .line 214
    if-eqz v2, :cond_3

    .line 215
    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LTT;

    invoke-interface {v3, v2}, LTT;->d(LaGu;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lye;

    .line 216
    invoke-interface {v2}, LaGu;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Lye;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 218
    :cond_3
    const-string v1, "PunchWebViewActivity"

    const-string v2, "Unable to find the entry. Not showing the edit button."

    invoke-static {v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static synthetic g(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->s()V

    return-void
.end method

.method private g()Z
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic h(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->y()V

    return-void
.end method

.method private h()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 394
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 395
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->o()V

    .line 411
    :cond_0
    :goto_0
    return v0

    .line 401
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LYO;

    invoke-interface {v1}, LYO;->a()LYR;

    move-result-object v1

    sget-object v2, LYR;->b:LYR;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LYO;

    invoke-interface {v1}, LYO;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 402
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, LakQ;->a(Landroid/content/res/Resources;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;

    if-eqz v1, :cond_2

    .line 403
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a(Z)V

    goto :goto_0

    .line 407
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->m()Z

    move-result v1

    if-nez v1, :cond_0

    .line 411
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 228
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    .line 229
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LaGM;

    invoke-interface {v1, v0}, LaGM;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 230
    if-eqz v0, :cond_0

    .line 231
    invoke-static {p0, v0, v2}, Lcom/google/android/apps/docs/app/detailpanel/DetailActivityDelegate;->a(Landroid/content/Context;Lcom/google/android/gms/drive/database/data/EntrySpec;Z)Landroid/content/Intent;

    move-result-object v0

    .line 232
    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 234
    :cond_0
    return-void
.end method

.method private l()V
    .locals 3

    .prologue
    .line 237
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    .line 238
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LaGM;

    invoke-interface {v1, v0}, LaGM;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v0

    .line 240
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LaGR;

    new-instance v2, LYU;

    invoke-direct {v2, p0, v0}, LYU;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    invoke-virtual {v1, v2}, LaGR;->a(LaGN;)V

    .line 248
    return-void
.end method

.method private m()V
    .locals 3

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LqK;

    const-string v1, "punch"

    const-string v2, "webViewPunchFullscreenOption"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->w()V

    .line 274
    return-void
.end method

.method private n()V
    .locals 5

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LqK;

    const-string v1, "punch"

    const-string v2, "helpEvent"

    invoke-virtual {v0, v1, v2}, LqK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LQr;

    const-string v1, "helpPunchUrlTemplate"

    const-string v2, "http://support.google.com/docs/?hl=%s&p=android_presentations"

    invoke-interface {v0, v1, v2}, LQr;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 281
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 282
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 283
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 284
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 285
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->startActivity(Landroid/content/Intent;)V

    .line 286
    return-void
.end method

.method private o()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 325
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 327
    iput-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/view/View;

    .line 330
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/webkit/WebChromeClient$CustomViewCallback;

    if-eqz v0, :cond_1

    .line 331
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/webkit/WebChromeClient$CustomViewCallback;

    invoke-interface {v0}, Landroid/webkit/WebChromeClient$CustomViewCallback;->onCustomViewHidden()V

    .line 332
    iput-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/webkit/WebChromeClient$CustomViewCallback;

    .line 335
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;

    move-result-object v0

    .line 336
    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->y()V

    .line 337
    return-void
.end method

.method private p()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 342
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 344
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 346
    const/16 v1, 0x8

    const/16 v2, 0x18

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 349
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->r()V

    .line 350
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->q()V

    .line 351
    return-void
.end method

.method private q()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 445
    invoke-static {}, LakQ;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 446
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()LaqY;

    move-result-object v0

    sget v1, LpM;->activity_icon_punch:I

    invoke-interface {v0, v1}, LaqY;->a(I)V

    .line 448
    :cond_0
    return-void
.end method

.method private r()V
    .locals 5

    .prologue
    .line 451
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LXJ;

    invoke-interface {v0}, LXJ;->c()I

    move-result v0

    .line 453
    if-lez v0, :cond_0

    .line 454
    sget v1, LpR;->punch_which_slide_is_displayed:I

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LXJ;

    .line 455
    invoke-interface {v4}, LXJ;->d()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    .line 454
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 455
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    .line 454
    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 460
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()LaqY;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, LaqY;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    return-void

    .line 457
    :cond_0
    sget v0, LpR;->loading:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private s()V
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LYO;

    invoke-interface {v0}, LYO;->a()LYR;

    move-result-object v0

    .line 465
    invoke-virtual {v0}, LYR;->a()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->b(I)V

    .line 466
    return-void
.end method

.method private t()V
    .locals 2

    .prologue
    .line 522
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()LM;

    move-result-object v0

    invoke-virtual {v0}, LM;->a()Lac;

    move-result-object v0

    .line 524
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/support/v4/app/Fragment;

    if-eqz v1, :cond_0

    .line 525
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1}, Lac;->a(Landroid/support/v4/app/Fragment;)Lac;

    .line 528
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;

    if-eqz v1, :cond_1

    .line 529
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;

    invoke-virtual {v0, v1}, Lac;->a(Landroid/support/v4/app/Fragment;)Lac;

    .line 532
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;

    if-eqz v1, :cond_2

    .line 533
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/SpeakerNotesFragment;

    invoke-virtual {v0, v1}, Lac;->a(Landroid/support/v4/app/Fragment;)Lac;

    .line 536
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;

    if-eqz v1, :cond_3

    .line 537
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;

    invoke-virtual {v0, v1}, Lac;->a(Landroid/support/v4/app/Fragment;)Lac;

    .line 538
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/NextPreviousButtonsFragment;

    .line 541
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;

    if-eqz v1, :cond_4

    .line 542
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;

    invoke-virtual {v0, v1}, Lac;->a(Landroid/support/v4/app/Fragment;)Lac;

    .line 545
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;

    if-eqz v1, :cond_5

    .line 546
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;

    invoke-virtual {v0, v1}, Lac;->a(Landroid/support/v4/app/Fragment;)Lac;

    .line 553
    :cond_5
    invoke-virtual {v0}, Lac;->b()I

    .line 554
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()LM;

    move-result-object v0

    invoke-virtual {v0}, LM;->a()Z

    .line 555
    return-void
.end method

.method private u()V
    .locals 2

    .prologue
    .line 670
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LXA;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 671
    new-instance v0, LYV;

    invoke-direct {v0, p0}, LYV;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LXA;

    .line 683
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LXJ;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LXA;

    invoke-interface {v0, v1}, LXJ;->a(LXA;)V

    .line 684
    return-void

    .line 670
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private v()V
    .locals 2

    .prologue
    .line 687
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LXA;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 688
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LXJ;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LXA;

    invoke-interface {v0, v1}, LXJ;->b(LXA;)V

    .line 689
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LXA;

    .line 690
    return-void

    .line 687
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private w()V
    .locals 2

    .prologue
    .line 693
    const-string v0, "PunchWebViewActivity"

    const-string v1, "in registerUiListener"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 694
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LYP;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 695
    new-instance v0, LYW;

    invoke-direct {v0, p0}, LYW;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LYP;

    .line 718
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LYO;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LYP;

    invoke-interface {v0, v1}, LYO;->a(LYP;)V

    .line 719
    return-void

    .line 694
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private x()V
    .locals 2

    .prologue
    .line 722
    const-string v0, "PunchWebViewActivity"

    const-string v1, "in unregisterUiListener"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 723
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LYP;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LbiT;->b(Z)V

    .line 724
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LYO;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LYP;

    invoke-interface {v0, v1}, LYO;->b(LYP;)V

    .line 725
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LYP;

    .line 726
    return-void

    .line 723
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private y()V
    .locals 1

    .prologue
    .line 744
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->t()V

    .line 745
    return-void
.end method

.method private z()V
    .locals 8

    .prologue
    .line 748
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->y()V

    .line 753
    sget-object v1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, v1, v0

    .line 754
    iget-object v4, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->b:Landroid/os/Handler;

    new-instance v5, LYY;

    invoke-direct {v5, p0}, LYY;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V

    int-to-long v6, v3

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 753
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 762
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 423
    const-class v0, LZC;

    if-ne p1, v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/support/v4/app/Fragment;

    .line 432
    :goto_0
    return-object v0

    .line 427
    :cond_0
    const-class v0, LXR;

    if-ne p1, v0, :cond_1

    .line 429
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/support/v4/app/Fragment;

    goto :goto_0

    .line 432
    :cond_1
    invoke-super {p0, p1, p2}, Lrm;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 739
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->m()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 740
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LYO;

    invoke-interface {v1, v0}, LYO;->d(Z)V

    .line 741
    return-void

    .line 739
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 730
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LXJ;

    invoke-interface {v0, p1}, LXJ;->a(I)V

    .line 732
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LXt;

    invoke-virtual {v0}, LXt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;

    if-eqz v0, :cond_0

    .line 733
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/punchwebview/DragKnobFragment;->a(Z)V

    .line 735
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V
    .locals 2

    .prologue
    .line 294
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/view/ViewGroup;

    if-nez v0, :cond_2

    .line 296
    :cond_0
    if-eqz p2, :cond_1

    .line 297
    invoke-interface {p2}, Landroid/webkit/WebChromeClient$CustomViewCallback;->onCustomViewHidden()V

    .line 309
    :cond_1
    :goto_0
    return-void

    .line 303
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;

    move-result-object v0

    .line 304
    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/PunchModeFragmentBase;->x()V

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/view/ViewGroup;

    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v1}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    invoke-virtual {v0, p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 307
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/view/View;

    .line 308
    iput-object p2, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/webkit/WebChromeClient$CustomViewCallback;

    goto :goto_0
.end method

.method public f()V
    .locals 0

    .prologue
    .line 313
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->o()V

    .line 314
    return-void
.end method

.method public j()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 765
    sget v0, LpN;->slide_picker_open_spacer:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 766
    if-eqz v0, :cond_0

    .line 767
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LYO;

    invoke-interface {v1}, LYO;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 768
    const-string v1, "PunchWebViewActivity"

    const-string v2, "set spacerView visible %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 769
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 776
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->z()V

    .line 777
    return-void

    .line 771
    :cond_1
    const-string v1, "PunchWebViewActivity"

    const-string v2, "set spacerView Gone %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 772
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 252
    if-nez p1, :cond_1

    .line 253
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    .line 254
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LaGM;

    invoke-interface {v1, v0}, LaGM;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGu;

    move-result-object v0

    .line 255
    if-nez v0, :cond_0

    .line 257
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->finish()V

    .line 267
    :goto_0
    return-void

    .line 262
    :cond_0
    invoke-interface {v0}, LaGu;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Ljava/lang/String;

    .line 263
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->r()V

    goto :goto_0

    .line 265
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lrm;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 416
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 417
    invoke-super {p0}, Lrm;->onBackPressed()V

    .line 419
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 382
    const-string v0, "PunchWebViewActivity"

    const-string v1, "in onConfigurationChanged"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->t()V

    .line 385
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->s()V

    .line 390
    invoke-super {p0, p1}, Lrm;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 391
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 129
    invoke-super {p0, p1}, Lrm;->onCreate(Landroid/os/Bundle;)V

    .line 130
    const-string v0, "PunchWebViewActivity"

    const-string v1, "in onCreate savedInstanceState=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LQr;

    .line 133
    invoke-static {v0}, LZD;->a(LQr;)LZD;

    move-result-object v0

    .line 134
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LYO;

    invoke-virtual {v0}, LZD;->a()LYK;

    move-result-object v2

    invoke-interface {v1, v2}, LYO;->a(LYK;)V

    .line 135
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LYO;

    .line 136
    invoke-virtual {v0}, LZD;->a()Z

    move-result v0

    .line 135
    invoke-interface {v1, v0}, LYO;->c(Z)V

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 139
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "docListTitle"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Ljava/lang/String;

    .line 142
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->s()V

    .line 143
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 175
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, LpQ;->menu_punch_webview:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 177
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    sget v0, LpN;->menu_edit_icon:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 181
    :cond_0
    new-instance v1, LYT;

    invoke-direct {v1, p0}, LYT;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V

    .line 201
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 202
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 203
    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 201
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 206
    :cond_1
    return v3
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 362
    const-string v0, "PunchWebViewActivity"

    const-string v1, "in onDestroy"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->o()V

    .line 365
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    .line 366
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LXC;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Landroid/support/v4/app/Fragment;

    invoke-interface {v0, v1}, LXC;->a(Landroid/support/v4/app/Fragment;)V

    .line 369
    :cond_0
    invoke-super {p0}, Lrm;->onDestroy()V

    .line 371
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;

    if-eqz v0, :cond_1

    .line 372
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a()V

    .line 375
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;

    if-eqz v0, :cond_2

    .line 376
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a()V

    .line 378
    :cond_2
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 355
    const-string v0, "PunchWebViewActivity"

    const-string v1, "in onPause"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->o()V

    .line 357
    invoke-super {p0}, Lrm;->onPause()V

    .line 358
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 438
    invoke-super {p0, p1}, Lrm;->onPostCreate(Landroid/os/Bundle;)V

    .line 440
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->q()V

    .line 441
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 318
    invoke-super {p0}, Lrm;->onResume()V

    .line 319
    const-string v0, "PunchWebViewActivity"

    const-string v1, "in onResume"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->j()V

    .line 322
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 654
    invoke-super {p0}, Lrm;->onStart()V

    .line 655
    const-string v0, "PunchWebViewActivity"

    const-string v1, "in onStart"

    invoke-static {v0, v1}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 657
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->u()V

    .line 658
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->w()V

    .line 659
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 663
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->v()V

    .line 664
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->x()V

    .line 666
    invoke-super {p0}, Lrm;->onStop()V

    .line 667
    return-void
.end method

.method public setContentView(I)V
    .locals 1

    .prologue
    .line 147
    invoke-super {p0, p1}, Lrm;->setContentView(I)V

    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a(Landroid/view/View;)V

    .line 149
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 153
    invoke-super {p0, p1}, Lrm;->setContentView(Landroid/view/View;)V

    .line 154
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a(Landroid/view/View;)V

    .line 155
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 159
    invoke-super {p0, p1, p2}, Lrm;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 160
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a(Landroid/view/View;)V

    .line 161
    return-void
.end method
