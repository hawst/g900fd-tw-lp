.class public Lcom/google/android/apps/viewer/pdflib/MatchRects;
.super Ljava/util/AbstractList;
.source "MatchRects.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractList",
        "<",
        "Ljava/util/List",
        "<",
        "Landroid/graphics/Rect;",
        ">;>;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/viewer/pdflib/MatchRects;",
            ">;"
        }
    .end annotation
.end field

.field public static final NO_MATCHES:Lcom/google/android/apps/viewer/pdflib/MatchRects;


# instance fields
.field private final matchToRect:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final rects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/apps/viewer/pdflib/MatchRects;

    .line 26
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/viewer/pdflib/MatchRects;-><init>(Ljava/util/List;Ljava/util/List;)V

    sput-object v0, Lcom/google/android/apps/viewer/pdflib/MatchRects;->NO_MATCHES:Lcom/google/android/apps/viewer/pdflib/MatchRects;

    .line 119
    new-instance v0, LavD;

    invoke-direct {v0}, LavD;-><init>()V

    sput-object v0, Lcom/google/android/apps/viewer/pdflib/MatchRects;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 29
    invoke-static {p1}, LauV;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/viewer/pdflib/MatchRects;->rects:Ljava/util/List;

    .line 30
    invoke-static {p2}, LauV;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/viewer/pdflib/MatchRects;->matchToRect:Ljava/util/List;

    .line 31
    return-void
.end method

.method public static synthetic access$000(Lcom/google/android/apps/viewer/pdflib/MatchRects;)Ljava/util/List;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/apps/viewer/pdflib/MatchRects;->rects:Ljava/util/List;

    return-object v0
.end method

.method public static synthetic access$100(Lcom/google/android/apps/viewer/pdflib/MatchRects;)Ljava/util/List;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/apps/viewer/pdflib/MatchRects;->matchToRect:Ljava/util/List;

    return-object v0
.end method

.method private matchToFirstRect(I)I
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/viewer/pdflib/MatchRects;->matchToRect:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/viewer/pdflib/MatchRects;->matchToRect:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/viewer/pdflib/MatchRects;->rects:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    return v0
.end method

.method public flatten()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/viewer/pdflib/MatchRects;->rects:Ljava/util/List;

    return-object v0
.end method

.method public flattenExcludingMatch(I)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/viewer/pdflib/MatchRects;->matchToRect:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 72
    :cond_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v0

    .line 74
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->matchToFirstRect(I)I

    move-result v0

    .line 75
    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->matchToFirstRect(I)I

    move-result v1

    .line 76
    new-instance v2, LavB;

    invoke-direct {v2, p0, v0, v1}, LavB;-><init>(Lcom/google/android/apps/viewer/pdflib/MatchRects;II)V

    return-object v2
.end method

.method public flattenToFirstRectPerMatch()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    new-instance v0, LavC;

    invoke-direct {v0, p0}, LavC;-><init>(Lcom/google/android/apps/viewer/pdflib/MatchRects;)V

    return-object v0
.end method

.method public bridge synthetic get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->get(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public get(I)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/viewer/pdflib/MatchRects;->matchToRect:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 36
    :cond_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v0

    .line 38
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->matchToFirstRect(I)I

    move-result v1

    .line 39
    add-int/lit8 v0, p1, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->matchToFirstRect(I)I

    move-result v2

    .line 40
    if-ge v1, v2, :cond_2

    const/4 v0, 0x1

    :goto_0
    const-string v3, "Every match should have > 0 rects"

    invoke-static {v0, v3}, LauV;->a(ZLjava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/viewer/pdflib/MatchRects;->rects:Ljava/util/List;

    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 40
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFirstRect(I)Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 46
    iget-object v1, p0, Lcom/google/android/apps/viewer/pdflib/MatchRects;->rects:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/viewer/pdflib/MatchRects;->matchToRect:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/viewer/pdflib/MatchRects;->matchToRect:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/viewer/pdflib/MatchRects;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " matches"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/apps/viewer/pdflib/MatchRects;->rects:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/viewer/pdflib/MatchRects;->matchToRect:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 117
    return-void
.end method
