.class public Lcom/google/bionics/scanner/ui/ControlPanelLayout;
.super Landroid/widget/RelativeLayout;
.source "ControlPanelLayout.java"


# instance fields
.field private final a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    const/4 v0, 0x1

    const/high16 v1, 0x42980000    # 76.0f

    .line 26
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 25
    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/bionics/scanner/ui/ControlPanelLayout;->a:I

    .line 27
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 12

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    const/4 v1, 0x0

    const/high16 v10, -0x80000000

    .line 31
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 32
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 36
    const/4 v0, 0x2

    .line 37
    invoke-virtual {p0}, Lcom/google/bionics/scanner/ui/ControlPanelLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v4, :cond_1

    const/4 v0, 0x1

    move v7, v0

    .line 39
    :goto_0
    if-eqz v7, :cond_2

    .line 40
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    move v4, v3

    move v5, v2

    move v6, v0

    move v0, v2

    .line 51
    :goto_1
    if-lez v2, :cond_0

    if-lez v3, :cond_0

    if-ne v6, v10, :cond_0

    .line 54
    int-to-double v2, v5

    int-to-double v4, v4

    const-wide/high16 v8, 0x4008000000000000L    # 3.0

    div-double/2addr v4, v8

    const-wide/high16 v8, 0x4010000000000000L    # 4.0

    mul-double/2addr v4, v8

    sub-double/2addr v2, v4

    double-to-int v1, v2

    .line 58
    :cond_0
    if-ne v6, v10, :cond_5

    if-le v1, v0, :cond_5

    .line 62
    :goto_2
    if-eqz v7, :cond_3

    .line 63
    invoke-static {v0, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 72
    :goto_3
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 73
    return-void

    :cond_1
    move v7, v1

    .line 37
    goto :goto_0

    .line 45
    :cond_2
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    move v4, v2

    move v5, v3

    move v6, v0

    move v0, v3

    .line 48
    goto :goto_1

    .line 66
    :cond_3
    if-ne v6, v10, :cond_4

    iget v1, p0, Lcom/google/bionics/scanner/ui/ControlPanelLayout;->a:I

    if-ge v0, v1, :cond_4

    .line 67
    iget v0, p0, Lcom/google/bionics/scanner/ui/ControlPanelLayout;->a:I

    .line 69
    :cond_4
    invoke-static {v0, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_2
.end method
