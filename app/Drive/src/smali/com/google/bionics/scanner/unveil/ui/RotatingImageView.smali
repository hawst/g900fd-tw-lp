.class public Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;
.super Landroid/widget/ImageView;
.source "RotatingImageView.java"


# instance fields
.field private a:I

.field private a:J

.field private a:Z

.field private b:I

.field private b:J

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 21
    iput v0, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->a:I

    .line 22
    iput v0, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->b:I

    .line 23
    iput v0, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->c:I

    .line 25
    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->a:Z

    .line 27
    iput-wide v2, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->a:J

    .line 28
    iput-wide v2, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->b:J

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    iput v0, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->a:I

    .line 22
    iput v0, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->b:I

    .line 23
    iput v0, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->c:I

    .line 25
    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->a:Z

    .line 27
    iput-wide v2, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->a:J

    .line 28
    iput-wide v2, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->b:J

    .line 36
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    .line 71
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 73
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 74
    iget v2, v0, Landroid/graphics/Rect;->right:I

    iget v3, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    .line 75
    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v0

    .line 77
    if-eqz v2, :cond_0

    if-nez v3, :cond_1

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    iget v0, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->a:I

    iget v4, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->c:I

    if-eq v0, v4, :cond_2

    .line 80
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v4

    .line 81
    iget-wide v6, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->b:J

    cmp-long v0, v4, v6

    if-gez v0, :cond_7

    .line 84
    iget-wide v6, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->a:J

    sub-long/2addr v4, v6

    long-to-int v0, v4

    .line 85
    iget v4, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->b:I

    iget-boolean v5, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->a:Z

    if-eqz v5, :cond_5

    :goto_1
    mul-int/lit16 v0, v0, 0x168

    div-int/lit16 v0, v0, 0x3e8

    add-int/2addr v0, v4

    .line 87
    if-ltz v0, :cond_6

    rem-int/lit16 v0, v0, 0x168

    .line 88
    :goto_2
    iput v0, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->a:I

    .line 89
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->invalidate()V

    .line 95
    :cond_2
    :goto_3
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->getPaddingLeft()I

    move-result v0

    .line 96
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->getPaddingTop()I

    move-result v4

    .line 97
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->getPaddingRight()I

    move-result v5

    .line 98
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->getPaddingBottom()I

    move-result v6

    .line 99
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->getWidth()I

    move-result v7

    sub-int/2addr v7, v0

    sub-int v5, v7, v5

    .line 100
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->getHeight()I

    move-result v7

    sub-int/2addr v7, v4

    sub-int v6, v7, v6

    .line 102
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getSaveCount()I

    move-result v7

    .line 105
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->getScaleType()Landroid/widget/ImageView$ScaleType;

    move-result-object v8

    sget-object v9, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    if-ne v8, v9, :cond_4

    if-lt v5, v2, :cond_3

    if-ge v6, v3, :cond_4

    .line 107
    :cond_3
    int-to-float v8, v5

    int-to-float v9, v2

    div-float/2addr v8, v9

    int-to-float v9, v6

    int-to-float v10, v3

    div-float/2addr v9, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v8

    .line 108
    int-to-float v9, v5

    div-float/2addr v9, v11

    int-to-float v10, v6

    div-float/2addr v10, v11

    invoke-virtual {p1, v8, v8, v9, v10}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 110
    :cond_4
    div-int/lit8 v5, v5, 0x2

    add-int/2addr v0, v5

    int-to-float v0, v0

    div-int/lit8 v5, v6, 0x2

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-virtual {p1, v0, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 111
    iget v0, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->a:I

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    .line 112
    neg-int v0, v2

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    neg-int v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 113
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 114
    invoke-virtual {p1, v7}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto/16 :goto_0

    .line 85
    :cond_5
    neg-int v0, v0

    goto :goto_1

    .line 87
    :cond_6
    rem-int/lit16 v0, v0, 0x168

    add-int/lit16 v0, v0, 0x168

    goto :goto_2

    .line 91
    :cond_7
    iget v0, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->c:I

    iput v0, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->a:I

    goto :goto_3
.end method

.method public setOrientation(I)V
    .locals 4

    .prologue
    .line 46
    if-ltz p1, :cond_0

    rem-int/lit16 v0, p1, 0x168

    .line 47
    :goto_0
    iget v1, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->c:I

    if-ne v0, v1, :cond_1

    .line 67
    :goto_1
    return-void

    .line 46
    :cond_0
    rem-int/lit16 v0, p1, 0x168

    add-int/lit16 v0, v0, 0x168

    goto :goto_0

    .line 49
    :cond_1
    iput v0, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->c:I

    .line 50
    iget v0, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->a:I

    iput v0, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->b:I

    .line 51
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->a:J

    .line 53
    iget v0, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->c:I

    iget v1, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->a:I

    sub-int/2addr v0, v1

    .line 54
    if-ltz v0, :cond_2

    .line 57
    :goto_2
    const/16 v1, 0xb4

    if-le v0, v1, :cond_3

    add-int/lit16 v0, v0, -0x168

    move v1, v0

    .line 59
    :goto_3
    if-ltz v1, :cond_4

    const/4 v0, 0x1

    :goto_4
    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->a:Z

    .line 60
    iget-wide v2, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->a:J

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    div-int/lit16 v0, v0, 0x168

    int-to-long v0, v0

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->b:J

    .line 66
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->invalidate()V

    goto :goto_1

    .line 54
    :cond_2
    add-int/lit16 v0, v0, 0x168

    goto :goto_2

    :cond_3
    move v1, v0

    .line 57
    goto :goto_3

    .line 59
    :cond_4
    const/4 v0, 0x0

    goto :goto_4
.end method
