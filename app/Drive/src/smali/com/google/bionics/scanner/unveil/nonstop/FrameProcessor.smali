.class public abstract Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;
.super Ljava/lang/Object;
.source "FrameProcessor.java"


# static fields
.field public static final DUTY_CYCLE_ALL:I = 0x1

.field public static final DUTY_CYCLE_NONE:I = 0x7fffffff


# instance fields
.field private a:I

.field private a:J

.field private a:Landroid/graphics/Matrix;

.field private final a:LbhP;

.field private final a:Lcom/google/bionics/scanner/unveil/util/Logger;

.field private a:Lcom/google/bionics/scanner/unveil/util/Size;

.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private b:I

.field private b:Lcom/google/bionics/scanner/unveil/util/Size;

.field private b:Z

.field private c:I

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    .line 41
    new-instance v0, LbhP;

    invoke-direct {v0, p0}, LbhP;-><init>(Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;)V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a:LbhP;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a:Ljava/util/ArrayList;

    .line 68
    iput v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->b:I

    .line 69
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->c:I

    .line 74
    iput-boolean v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->d:Z

    .line 82
    return-void
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;)Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->d:Z

    return v0
.end method


# virtual methods
.method protected a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 322
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 323
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "<anonymous>"

    goto :goto_0
.end method

.method protected final declared-synchronized a()V
    .locals 3

    .prologue
    .line 202
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Left over queued runnables from last time!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 204
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 206
    :cond_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207
    monitor-exit p0

    return-void

    .line 202
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 316
    invoke-virtual {p0, p1}, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->onDrawDebug(Landroid/graphics/Canvas;)V

    .line 317
    return-void
.end method

.method public abstract a(Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;)V
.end method

.method protected a(Lcom/google/bionics/scanner/unveil/util/Size;)V
    .locals 0

    .prologue
    .line 275
    return-void
.end method

.method protected final a(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Runnable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 210
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a:Ljava/util/ArrayList;

    monitor-enter v1

    .line 211
    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a:Ljava/util/ArrayList;

    invoke-interface {p1, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 213
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 215
    :cond_0
    monitor-exit v1

    .line 216
    return-void

    .line 215
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected a(Z)V
    .locals 0

    .prologue
    .line 352
    iput-boolean p1, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->c:Z

    .line 353
    return-void
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 330
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final declared-synchronized b()V
    .locals 1

    .prologue
    .line 259
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 260
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 261
    monitor-exit p0

    return-void

    .line 259
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final declared-synchronized b(Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;)V
    .locals 4

    .prologue
    .line 237
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->c:I

    if-nez v0, :cond_1

    .line 238
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->b:Z

    .line 240
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->d:Z

    if-eqz v0, :cond_0

    .line 241
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    .line 242
    invoke-virtual {p0, p1}, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a(Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;)V

    .line 243
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    .line 244
    iget-boolean v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->b:Z

    if-nez v2, :cond_0

    .line 245
    iget-object v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a:LbhP;

    invoke-static {v2, v0, v1}, LbhP;->a(LbhP;J)V

    .line 249
    :cond_0
    invoke-virtual {p1}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->getTimestamp()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a:J

    .line 252
    :cond_1
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->c:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->b:I

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 253
    monitor-exit p0

    return-void

    .line 237
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final declared-synchronized c()V
    .locals 1

    .prologue
    .line 268
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a:Z

    .line 269
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->onShutdown()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 270
    monitor-exit p0

    return-void

    .line 268
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected d()V
    .locals 0

    .prologue
    .line 283
    return-void
.end method

.method protected e()V
    .locals 0

    .prologue
    .line 303
    return-void
.end method

.method public getDebugText()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 348
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    return-object v0
.end method

.method public getTimePerFrame()F
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a:LbhP;

    invoke-virtual {v0}, LbhP;->a()F

    move-result v0

    return v0
.end method

.method public getTimeStats()Ljava/lang/String;
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a:LbhP;

    invoke-virtual {v0}, LbhP;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getViewSize()Lcom/google/bionics/scanner/unveil/util/Size;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->b:Lcom/google/bionics/scanner/unveil/util/Size;

    return-object v0
.end method

.method public final declared-synchronized init(Lcom/google/bionics/scanner/unveil/util/Size;Lcom/google/bionics/scanner/unveil/util/Size;ILandroid/graphics/Matrix;)V
    .locals 3

    .prologue
    .line 136
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a:Z

    .line 138
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    .line 139
    iput-object p2, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->b:Lcom/google/bionics/scanner/unveil/util/Size;

    .line 140
    iput p3, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a:I

    .line 142
    if-eqz p4, :cond_0

    .line 143
    iput-object p4, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a:Landroid/graphics/Matrix;

    .line 149
    :goto_0
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a:J

    .line 151
    invoke-virtual {p0, p1}, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a(Lcom/google/bionics/scanner/unveil/util/Size;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    monitor-exit p0

    return-void

    .line 145
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Null frameToCanvas Matrix, debug drawing may not line up!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 146
    invoke-static {p1, p2, p3}, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->getTransformationMatrix(Lcom/google/bionics/scanner/unveil/util/Size;Lcom/google/bionics/scanner/unveil/util/Size;I)Landroid/graphics/Matrix;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a:Landroid/graphics/Matrix;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 136
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isDebugActive()Z
    .locals 1

    .prologue
    .line 356
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->c:Z

    return v0
.end method

.method public onDrawDebug(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 319
    return-void
.end method

.method public onShutdown()V
    .locals 0

    .prologue
    .line 308
    return-void
.end method

.method public setDutyCycle(I)V
    .locals 0

    .prologue
    .line 155
    iput p1, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->b:I

    .line 156
    return-void
.end method

.method public declared-synchronized setProcessingEnabled(Z)V
    .locals 1

    .prologue
    .line 188
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    monitor-exit p0

    return-void

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
