.class public Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;
.super Ljava/lang/Object;
.source "PreviewLooper.java"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;
.implements Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$BufferSink;


# instance fields
.field private a:I

.field private final a:J

.field private final a:Landroid/graphics/Matrix;

.field private final a:Landroid/graphics/Paint;

.field private final a:Landroid/os/Handler;

.field private final a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

.field private final a:Lcom/google/bionics/scanner/unveil/util/Logger;

.field private final a:Lcom/google/bionics/scanner/unveil/util/Size;

.field private final a:Lcom/google/bionics/scanner/unveil/util/Stopwatch;

.field private final a:Ljava/lang/Thread;

.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private final a:[Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;

.field private b:I

.field private volatile b:Z

.field private c:I

.field private volatile c:Z

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/google/bionics/scanner/unveil/camera/CameraManager;IFI)V
    .locals 6

    .prologue
    .line 133
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;-><init>(Lcom/google/bionics/scanner/unveil/camera/CameraManager;IFILandroid/graphics/Matrix;)V

    .line 134
    return-void
.end method

.method public constructor <init>(Lcom/google/bionics/scanner/unveil/camera/CameraManager;IFILandroid/graphics/Matrix;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    .line 69
    iput-boolean v5, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->d:Z

    .line 78
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->c:I

    .line 83
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Landroid/graphics/Paint;

    .line 84
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Ljava/util/Vector;

    .line 94
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    .line 95
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Ljava/lang/Thread;

    .line 97
    iput p2, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->b:I

    .line 98
    iput-object p5, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Landroid/graphics/Matrix;

    .line 100
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/Stopwatch;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Lcom/google/bionics/scanner/unveil/util/Stopwatch;

    .line 102
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Z

    .line 103
    iput-boolean v5, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->b:Z

    .line 105
    iput-boolean v5, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->d:Z

    .line 106
    const/high16 v0, 0x447a0000    # 1000.0f

    div-float/2addr v0, p3

    float-to-long v0, v0

    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:J

    .line 108
    invoke-virtual {p1}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getPreviewSize()Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    .line 110
    new-array v0, p4, [Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:[Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;

    .line 112
    iput p2, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->b:I

    .line 114
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Landroid/os/Handler;

    .line 116
    const/4 v1, 0x0

    .line 117
    add-int/lit8 v0, p4, -0x1

    :goto_0
    if-lez v0, :cond_0

    .line 121
    rsub-int/lit8 v2, v0, 0x5

    const/4 v3, 0x5

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 122
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ProcessingThread "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 123
    new-instance v2, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingThread;

    invoke-direct {v2, v4, v3, v1}, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingThread;-><init>(Ljava/lang/String;ILcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;)V

    .line 124
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:[Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;

    aput-object v2, v1, v0

    .line 117
    add-int/lit8 v0, v0, -0x1

    move-object v1, v2

    goto :goto_0

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:[Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;

    new-instance v2, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;

    invoke-direct {v2, v1}, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;-><init>(Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;)V

    aput-object v2, v0, v5

    .line 127
    return-void
.end method

.method private declared-synchronized a()V
    .locals 4

    .prologue
    .line 163
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->d:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 186
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 166
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->d:Z

    .line 168
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Lcom/google/bionics/scanner/unveil/util/Stopwatch;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->getElapsedMilliseconds()J

    move-result-wide v0

    .line 169
    iget-wide v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:J

    sub-long v0, v2, v0

    .line 171
    const-wide/16 v2, 0x5

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 174
    iget-object v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Landroid/os/Handler;

    new-instance v3, LbhQ;

    invoke-direct {v3, p0}, LbhQ;-><init>(Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;)V

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 163
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 183
    :cond_2
    :try_start_2
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->b()V

    .line 184
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->d:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a()V

    return-void
.end method

.method private a(Lcom/google/bionics/scanner/unveil/util/Size;Landroid/graphics/Matrix;)V
    .locals 4

    .prologue
    .line 325
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->f()V

    .line 327
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->getAllProcessors()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;

    .line 328
    iget-object v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    iget v3, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->b:I

    invoke-virtual {v0, v2, p1, v3, p2}, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->init(Lcom/google/bionics/scanner/unveil/util/Size;Lcom/google/bionics/scanner/unveil/util/Size;ILandroid/graphics/Matrix;)V

    goto :goto_0

    .line 330
    :cond_0
    return-void
.end method

.method private a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 237
    iget-boolean v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->c:Z

    if-nez v1, :cond_0

    .line 246
    :goto_0
    return v0

    .line 241
    :cond_0
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->requestOneFrame(Landroid/hardware/Camera$PreviewCallback;)V

    .line 242
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->f()V

    .line 243
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->c()V

    .line 244
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->e()V

    .line 245
    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->b:Z

    .line 246
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;Z)Z
    .locals 0

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->d:Z

    return p1
.end method

.method private b()V
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-virtual {v0, p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->requestOneFrame(Landroid/hardware/Camera$PreviewCallback;)V

    .line 193
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Lcom/google/bionics/scanner/unveil/util/Stopwatch;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->reset()V

    .line 194
    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    .line 255
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 256
    iget-object v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:[Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 257
    invoke-virtual {v4, v1}, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;->a(Ljava/util/Collection;)V

    .line 256
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 260
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 261
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_1

    .line 263
    :cond_1
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 333
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->f()V

    .line 334
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->getAllProcessors()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;

    .line 335
    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a()V

    goto :goto_0

    .line 337
    :cond_0
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 340
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->f()V

    .line 341
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->getAllProcessors()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;

    .line 342
    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->b()V

    goto :goto_0

    .line 344
    :cond_0
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 369
    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:[Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 370
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:[Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;->blockUntilReadyForFrame()V

    .line 369
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 372
    :cond_0
    return-void
.end method

.method public static supportNonstopFrameProcessing(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 486
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    .line 487
    const/16 v1, 0xc

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addPreviewProcessor(Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;I)V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:[Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;

    aget-object v1, v0, p2

    monitor-enter v1

    .line 138
    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:[Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;->addProcessor(Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;)V

    .line 139
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Ljava/util/List;

    .line 140
    monitor-exit v1

    .line 141
    return-void

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public changeMode(Z)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 375
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->getAllProcessors()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 376
    iget v4, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->c:I

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v4

    iput v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->c:I

    .line 378
    add-int/lit8 v0, v2, 0x2

    .line 379
    iget v4, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->c:I

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v4, v0

    rem-int v0, v4, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->c:I

    .line 381
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->getAllProcessors()Ljava/util/List;

    move-result-object v6

    .line 382
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->c:I

    if-ne v0, v2, :cond_2

    move v2, v1

    :goto_1
    move v4, v3

    .line 383
    :goto_2
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_4

    .line 384
    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;

    .line 385
    if-nez v2, :cond_0

    iget v5, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->c:I

    if-ne v4, v5, :cond_3

    :cond_0
    move v5, v1

    .line 386
    :goto_3
    invoke-virtual {v0, v5}, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a(Z)V

    .line 383
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    .line 376
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    :cond_2
    move v2, v3

    .line 382
    goto :goto_1

    :cond_3
    move v5, v3

    .line 385
    goto :goto_3

    .line 389
    :cond_4
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->c:I

    if-ltz v0, :cond_5

    :goto_4
    return v1

    :cond_5
    move v1, v3

    goto :goto_4
.end method

.method public drawDebug(Landroid/graphics/Canvas;)V
    .locals 14

    .prologue
    .line 393
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->c:I

    if-gez v0, :cond_1

    .line 475
    :cond_0
    return-void

    .line 397
    :cond_1
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->getAllProcessors()Ljava/util/List;

    move-result-object v8

    .line 398
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    .line 401
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 402
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->c:I

    if-eq v0, v2, :cond_2

    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->c:I

    if-ne v1, v0, :cond_4

    :cond_2
    const/4 v0, 0x1

    .line 403
    :goto_1
    if-eqz v0, :cond_3

    .line 404
    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;

    .line 405
    monitor-enter v0

    .line 406
    :try_start_0
    invoke-virtual {v0, p1}, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->a(Landroid/graphics/Canvas;)V

    .line 407
    monitor-exit v0

    .line 401
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 402
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 407
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 411
    :cond_5
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->c:I

    if-ne v0, v2, :cond_7

    const/4 v0, 0x1

    move v1, v0

    .line 415
    :goto_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 416
    const/4 v0, 0x0

    move v3, v2

    move v2, v0

    :goto_3
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 418
    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;

    .line 420
    if-nez v1, :cond_6

    iget v4, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->c:I

    if-ne v2, v4, :cond_8

    :cond_6
    const/4 v4, 0x1

    move v6, v4

    .line 423
    :goto_4
    if-eqz v6, :cond_9

    .line 424
    monitor-enter v0

    .line 425
    :try_start_1
    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->getDebugText()Ljava/util/Vector;

    move-result-object v4

    .line 426
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 431
    :goto_5
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v9

    .line 435
    if-eqz v1, :cond_a

    const/4 v5, 0x2

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v7

    invoke-static {v5, v7}, Ljava/lang/Math;->min(II)I

    move-result v5

    move v7, v5

    .line 440
    :goto_6
    if-eqz v6, :cond_c

    const/4 v5, 0x1

    :goto_7
    add-int/2addr v5, v7

    mul-int/lit8 v5, v5, 0x14

    add-int/lit8 v5, v5, 0x1c

    .line 443
    sub-int/2addr v3, v5

    .line 445
    iget-object v10, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Landroid/graphics/Paint;

    const/high16 v11, -0x1000000

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setColor(I)V

    .line 446
    iget-object v10, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Landroid/graphics/Paint;

    const/16 v11, 0x64

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 449
    new-instance v10, Landroid/graphics/Rect;

    const/4 v11, 0x0

    add-int/lit8 v9, v9, 0x0

    add-int/2addr v5, v3

    invoke-direct {v10, v11, v3, v9, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v5, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v10, v5}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 452
    iget-object v5, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Landroid/graphics/Paint;

    const/16 v9, 0xff

    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 454
    iget-object v5, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Landroid/graphics/Paint;

    const/4 v9, 0x1

    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 455
    iget-object v9, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Landroid/graphics/Paint;

    if-eqz v6, :cond_d

    const v5, -0xff0001

    :goto_8
    invoke-virtual {v9, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 456
    iget-object v5, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Landroid/graphics/Paint;

    const/high16 v9, 0x41a00000    # 20.0f

    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 457
    add-int/lit8 v5, v3, 0x18

    .line 458
    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->b()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    int-to-float v11, v5

    iget-object v12, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v9, v10, v11, v12}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 460
    if-eqz v6, :cond_f

    .line 462
    iget-object v6, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Landroid/graphics/Paint;

    const/4 v9, -0x1

    invoke-virtual {v6, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 463
    iget-object v6, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Landroid/graphics/Paint;

    const/high16 v9, 0x41800000    # 16.0f

    invoke-virtual {v6, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 465
    add-int/lit8 v5, v5, 0x14

    .line 466
    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->getTimeStats()Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x0

    int-to-float v9, v5

    iget-object v10, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v6, v9, v10}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 467
    const/4 v0, 0x0

    move v13, v0

    move v0, v5

    move v5, v13

    :goto_9
    if-ge v5, v7, :cond_e

    .line 468
    add-int/lit8 v6, v0, 0x14

    .line 469
    invoke-virtual {v4, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v9, 0x0

    int-to-float v10, v6

    iget-object v11, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v9, v10, v11}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 467
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move v0, v6

    goto :goto_9

    .line 411
    :cond_7
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_2

    .line 420
    :cond_8
    const/4 v4, 0x0

    move v6, v4

    goto/16 :goto_4

    .line 426
    :catchall_1
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1

    .line 428
    :cond_9
    iget-object v4, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Ljava/util/Vector;

    goto/16 :goto_5

    .line 435
    :cond_a
    if-eqz v6, :cond_b

    .line 436
    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v5

    move v7, v5

    goto/16 :goto_6

    :cond_b
    const/4 v5, 0x0

    move v7, v5

    goto/16 :goto_6

    .line 440
    :cond_c
    const/4 v5, 0x0

    goto/16 :goto_7

    .line 455
    :cond_d
    const v5, -0xffff01

    goto :goto_8

    .line 472
    :cond_e
    add-int/lit8 v0, v0, 0x4

    .line 416
    :cond_f
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_3
.end method

.method public getAllProcessors()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Ljava/util/List;

    if-nez v0, :cond_0

    .line 148
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Ljava/util/List;

    .line 150
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:[Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 151
    iget-object v4, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Ljava/util/List;

    invoke-virtual {v3}, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;->getProcessors()Ljava/util/List;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 150
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Ljava/util/List;

    return-object v0
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 478
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->b:Z

    return v0
.end method

.method public declared-synchronized onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 8

    .prologue
    .line 198
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->b:Z

    if-nez v0, :cond_1

    .line 199
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Dropping frame due to pause event."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 200
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->requestOneFrame(Landroid/hardware/Camera$PreviewCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 205
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 211
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->c()V

    .line 213
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:I

    .line 216
    new-instance v0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;

    iget-object v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    iget v3, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:I

    .line 220
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget v6, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->b:I

    move-object v1, p1

    move-object v7, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;-><init>([BLcom/google/bionics/scanner/unveil/util/Size;IJILcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$BufferSink;)V

    .line 225
    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->addReference()V

    .line 230
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:[Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1, v0}, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;->b(Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;)V

    .line 233
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 198
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized pauseLoop()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 301
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "pauseLoop"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 302
    iget-boolean v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->b:Z

    if-nez v1, :cond_1

    .line 303
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Pausing a PreviewLooper that was already paused."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 318
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 307
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:[Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 308
    invoke-virtual {v3}, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;->b()V

    .line 307
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 311
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->c:Z

    .line 315
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    .line 316
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 301
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public returnBuffer([B)V
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-virtual {v0, p1}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->addPreviewBuffer([B)V

    .line 252
    return-void
.end method

.method public declared-synchronized shutdown()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 351
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "shutdown"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 353
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->pauseLoop()V

    .line 355
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:[Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 356
    invoke-virtual {v3}, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;->c()V

    .line 355
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 359
    :cond_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->getAllProcessors()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;

    .line 360
    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 351
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 362
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized startLoop(Lcom/google/bionics/scanner/unveil/util/Size;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 269
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "startLoop with size %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 270
    iget-boolean v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->b:Z

    if-eqz v1, :cond_0

    .line 271
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Starting a PreviewLooper that was already started, just resetting preview callback."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 273
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->requestOneFrame(Landroid/hardware/Camera$PreviewCallback;)V

    .line 274
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295
    :goto_0
    monitor-exit p0

    return-void

    .line 277
    :cond_0
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->b:Z

    .line 278
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->c:Z

    .line 280
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Lcom/google/bionics/scanner/unveil/util/Stopwatch;

    invoke-virtual {v1}, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->start()V

    .line 281
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->d:Z

    .line 283
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:I

    .line 285
    iget-boolean v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Z

    if-eqz v1, :cond_1

    .line 286
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Landroid/graphics/Matrix;

    invoke-direct {p0, p1, v1}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a(Lcom/google/bionics/scanner/unveil/util/Size;Landroid/graphics/Matrix;)V

    .line 288
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:Z

    .line 290
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->a:[Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 291
    invoke-virtual {v3}, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;->a()V

    .line 290
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 293
    :cond_2
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->d()V

    .line 294
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 269
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
