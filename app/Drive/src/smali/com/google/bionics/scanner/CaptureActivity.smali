.class public Lcom/google/bionics/scanner/CaptureActivity;
.super Lbgg;
.source "CaptureActivity.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lbgi;
.implements Lbhi;
.implements Lbhj;
.implements Lcom/google/bionics/scanner/unveil/camera/CameraManager$FocusCallback;
.implements Lcom/google/bionics/scanner/unveil/camera/CameraManager$Listener;
.implements Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureCallback;
.implements Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$CameraLayoutHandler;


# static fields
.field private static final a:Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;

.field private static final a:Lcom/google/bionics/scanner/unveil/util/Logger;


# instance fields
.field private a:I

.field private a:J

.field private a:Landroid/content/Intent;

.field private a:Landroid/content/SharedPreferences;

.field private a:Landroid/widget/FrameLayout;

.field private a:Landroid/widget/ProgressBar;

.field private final a:Lbgn;

.field private a:LbhC;

.field private a:Lbhf;

.field private a:Lcom/google/bionics/scanner/rectifier/QuadsProcessor;

.field private a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

.field private a:Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;

.field private a:Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;

.field private a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;

.field private a:Lcom/google/bionics/scanner/unveil/ui/DebugView;

.field private a:Lcom/google/bionics/scanner/unveil/ui/PreviewOverlay;

.field private a:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

.field private a:Lcom/google/bionics/scanner/unveil/ui/Viewport;

.field private a:Lcom/google/bionics/scanner/unveil/util/Size;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 62
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    const-class v1, Lcom/google/bionics/scanner/CaptureActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    .line 74
    new-instance v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;

    const/16 v1, 0xcc0

    const/16 v2, 0x990

    const/16 v3, 0x5f

    const/16 v4, 0x64

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;-><init>(IIII)V

    sput-object v0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0}, Lbgg;-><init>()V

    .line 115
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:I

    .line 118
    new-instance v0, Lbgn;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lbgn;-><init>(Lcom/google/bionics/scanner/CaptureActivity;Lbgk;)V

    iput-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbgn;

    .line 673
    return-void
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/CaptureActivity;)Lbgn;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbgn;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/CaptureActivity;)Lcom/google/bionics/scanner/unveil/camera/CameraManager;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/CaptureActivity;)Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/CaptureActivity;)Lcom/google/bionics/scanner/unveil/ui/DebugView;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/ui/DebugView;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/CaptureActivity;)Lcom/google/bionics/scanner/unveil/ui/Viewport;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/ui/Viewport;

    return-object v0
.end method

.method public static synthetic a()Lcom/google/bionics/scanner/unveil/util/Logger;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-object v0
.end method

.method private a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 548
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getExpectedFlashMode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/CaptureActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/bionics/scanner/CaptureActivity;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(LbgY;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 469
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/bionics/scanner/EditorActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 470
    if-eqz p1, :cond_0

    .line 471
    iget v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:I

    sparse-switch v1, :sswitch_data_0

    .line 497
    sget-object v1, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Unknown result code encountered while trying to launch editor"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 500
    :cond_0
    :goto_0
    invoke-virtual {p0, v0, v7}, Lcom/google/bionics/scanner/CaptureActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 501
    return-void

    .line 474
    :sswitch_0
    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbhf;

    invoke-virtual {v1}, Lbhf;->a()LbgX;

    move-result-object v1

    invoke-virtual {v1, p1}, LbgX;->b(LbgY;)V

    .line 475
    const-string v1, "ACTION_UPDATE_ADDED_PAGE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 476
    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/content/Intent;

    if-eqz v1, :cond_0

    .line 477
    const-string v1, "SAVED_INSTANCE_FILE_NAME"

    iget-object v2, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/content/Intent;

    const-string v3, "SAVED_INSTANCE_FILE_NAME"

    .line 478
    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 477
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 483
    :sswitch_1
    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/content/Intent;

    const-string v2, "com.google.bionics.scanner.extra.DOCUMENT_PAGE_INDEX"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 484
    const-string v2, "com.google.bionics.scanner.extra.DOCUMENT_PAGE_INDEX"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 485
    iget-object v2, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/content/Intent;

    if-eqz v2, :cond_1

    .line 486
    const-string v2, "SAVED_INSTANCE_FILE_NAME"

    iget-object v3, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/content/Intent;

    const-string v4, "SAVED_INSTANCE_FILE_NAME"

    .line 487
    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 486
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 489
    :cond_1
    sget-object v2, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v3, "Replacing page %d in document and relaunching editor..."

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 490
    if-ltz v1, :cond_2

    .line 491
    iget-object v2, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbhf;

    invoke-virtual {v2}, Lbhf;->a()LbgX;

    move-result-object v2

    invoke-virtual {v2, v1, p1}, LbgX;->b(ILbgY;)V

    .line 493
    :cond_2
    const-string v1, "ACTION_UPDATE_REPLACED_PAGE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 471
    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0xb -> :sswitch_0
        0x15 -> :sswitch_1
    .end sparse-switch
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/CaptureActivity;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/bionics/scanner/CaptureActivity;->j()V

    return-void
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/CaptureActivity;Lcom/google/bionics/scanner/unveil/util/Picture;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/google/bionics/scanner/CaptureActivity;->a(Lcom/google/bionics/scanner/unveil/util/Picture;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/CaptureActivity;Z)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/google/bionics/scanner/CaptureActivity;->b(Z)V

    return-void
.end method

.method private a(Lcom/google/bionics/scanner/unveil/util/Picture;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 440
    invoke-static {p0}, Lcom/google/bionics/scanner/unveil/ui/Viewport;->computeNaturalOrientation(Landroid/content/Context;)I

    move-result v0

    .line 441
    invoke-virtual {p1}, Lcom/google/bionics/scanner/unveil/util/Picture;->getOrientation()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/bionics/scanner/CaptureActivity;->a()I

    move-result v3

    add-int/2addr v2, v3

    .line 442
    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    const/16 v0, 0x5a

    :goto_0
    add-int/2addr v0, v2

    .line 443
    invoke-static {v0}, Lbhy;->a(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/bionics/scanner/unveil/util/Picture;->setOrientation(I)V

    .line 444
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 445
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:J

    .line 446
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbhf;

    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbhf;

    invoke-virtual {v1}, Lbhf;->a()LbgX;

    move-result-object v1

    invoke-virtual {v1}, LbgX;->a()Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    move-result-object v1

    invoke-virtual {v0, p1, v1, p0}, Lbhf;->a(Lcom/google/bionics/scanner/unveil/util/Picture;Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;Lbhi;)V

    .line 447
    return-void

    :cond_0
    move v0, v1

    .line 442
    goto :goto_0
.end method

.method private a()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 353
    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbhf;

    invoke-virtual {v1}, Lbhf;->a()LbgX;

    move-result-object v1

    invoke-virtual {v1}, LbgX;->a()I

    move-result v1

    .line 354
    iget v2, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:I

    const/16 v3, 0xb

    if-ne v2, v3, :cond_0

    if-gtz v1, :cond_1

    :cond_0
    iget v2, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:I

    const/16 v3, 0x15

    if-ne v2, v3, :cond_2

    if-le v1, v0, :cond_2

    .line 357
    :cond_1
    :goto_0
    return v0

    .line 354
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 368
    if-eqz p1, :cond_0

    const-string v1, "com.google.bionics.scanner.extra.BACK_BUTTON"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    return v0
.end method

.method private b()I
    .locals 3

    .prologue
    .line 518
    invoke-virtual {p0}, Lcom/google/bionics/scanner/CaptureActivity;->getRequestedOrientation()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 526
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unhandled orientation: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/bionics/scanner/CaptureActivity;->getRequestedOrientation()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 520
    :pswitch_0
    const/4 v0, 0x0

    .line 523
    :goto_0
    return v0

    :pswitch_1
    const/16 v0, 0x5a

    goto :goto_0

    .line 518
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static synthetic b(Lcom/google/bionics/scanner/CaptureActivity;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/bionics/scanner/CaptureActivity;->m()V

    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 539
    if-eqz p1, :cond_0

    .line 540
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_shutter_button_lit:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 544
    :goto_0
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->postInvalidate()V

    .line 545
    return-void

    .line 542
    :cond_0
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_shutter_button_lit:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 377
    iget v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:I

    const/16 v1, 0xb

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:I

    const/16 v1, 0x15

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic c(Lcom/google/bionics/scanner/CaptureActivity;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/bionics/scanner/CaptureActivity;->k()V

    return-void
.end method

.method public static synthetic d(Lcom/google/bionics/scanner/CaptureActivity;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/bionics/scanner/CaptureActivity;->n()V

    return-void
.end method

.method public static synthetic e(Lcom/google/bionics/scanner/CaptureActivity;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/bionics/scanner/CaptureActivity;->l()V

    return-void
.end method

.method public static synthetic f(Lcom/google/bionics/scanner/CaptureActivity;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/bionics/scanner/CaptureActivity;->o()V

    return-void
.end method

.method private g()V
    .locals 1

    .prologue
    .line 178
    sget v0, Lcom/google/bionics/scanner/docscanner/R$string;->ds_title_activity_capture:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/CaptureActivity;->setTitle(I)V

    .line 179
    return-void
.end method

.method public static synthetic g(Lcom/google/bionics/scanner/CaptureActivity;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/bionics/scanner/CaptureActivity;->p()V

    return-void
.end method

.method private h()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 382
    sget-object v0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "initializing preview looper"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 384
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;

    if-nez v0, :cond_0

    .line 385
    new-instance v0, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;

    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    const/high16 v2, 0x40800000    # 4.0f

    const/4 v3, 0x2

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;-><init>(Lcom/google/bionics/scanner/unveil/camera/CameraManager;IFI)V

    iput-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;

    .line 386
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/ui/DebugView;

    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;

    invoke-virtual {v0, v1}, Lcom/google/bionics/scanner/unveil/ui/DebugView;->setCallback(Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;)V

    .line 392
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;

    new-instance v1, Lbgk;

    invoke-direct {v1, p0}, Lbgk;-><init>(Lcom/google/bionics/scanner/CaptureActivity;)V

    invoke-virtual {v0, v1, v4}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->addPreviewProcessor(Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;I)V

    .line 411
    invoke-static {p0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->supportNonstopFrameProcessing(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 412
    new-instance v0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;

    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-direct {v0, v1}, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;-><init>(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)V

    iput-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;

    .line 413
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;

    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;

    invoke-virtual {v0, v1, v5}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->addPreviewProcessor(Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;I)V

    .line 415
    new-instance v0, LbhC;

    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/ui/PreviewOverlay;

    .line 416
    invoke-direct {p0}, Lcom/google/bionics/scanner/CaptureActivity;->b()I

    move-result v2

    invoke-direct {v0, v1, v2}, LbhC;-><init>(Landroid/view/View;I)V

    iput-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:LbhC;

    .line 417
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/ui/PreviewOverlay;

    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:LbhC;

    invoke-virtual {v0, v1}, Lcom/google/bionics/scanner/unveil/ui/PreviewOverlay;->addRenderer(Lcom/google/bionics/scanner/unveil/ui/PreviewOverlay$OverlayRenderer;)V

    .line 418
    new-instance v0, Lcom/google/bionics/scanner/rectifier/QuadsProcessor;

    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:LbhC;

    invoke-direct {v0, v1}, Lcom/google/bionics/scanner/rectifier/QuadsProcessor;-><init>(Lcom/google/bionics/scanner/rectifier/QuadsProcessor$Listener;)V

    iput-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/rectifier/QuadsProcessor;

    .line 419
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/rectifier/QuadsProcessor;

    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/content/SharedPreferences;

    sget v2, Lcom/google/bionics/scanner/docscanner/R$string;->ds_display_real_time_quads_key:I

    .line 420
    invoke-virtual {p0, v2}, Lcom/google/bionics/scanner/CaptureActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/bionics/scanner/docscanner/R$string;->ds_display_real_time_quads_default:I

    .line 421
    invoke-virtual {p0, v3}, Lcom/google/bionics/scanner/CaptureActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 420
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 419
    invoke-virtual {v0, v1}, Lcom/google/bionics/scanner/rectifier/QuadsProcessor;->setProcessingEnabled(Z)V

    .line 422
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;

    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/rectifier/QuadsProcessor;

    invoke-virtual {v0, v1, v5}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->addPreviewProcessor(Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;I)V

    .line 428
    :cond_0
    :goto_0
    sget-object v0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Starting preview frame processing."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 429
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;

    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    invoke-virtual {v0, v1}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->startLoop(Lcom/google/bionics/scanner/unveil/util/Size;)V

    .line 430
    return-void

    .line 424
    :cond_1
    sget-object v0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Nonstop frame processing is not enabled."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 535
    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->isFlashSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->setVisibility(I)V

    .line 536
    return-void

    .line 535
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 553
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    .line 554
    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getExpectedFlashMode()Ljava/lang/String;

    move-result-object v0

    const-string v1, "torch"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 555
    :goto_0
    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    if-eqz v0, :cond_1

    const-string v0, "torch"

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->setFlashMode(Ljava/lang/String;)V

    .line 557
    invoke-direct {p0}, Lcom/google/bionics/scanner/CaptureActivity;->k()V

    .line 558
    return-void

    .line 554
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 555
    :cond_1
    const-string v0, "off"

    goto :goto_1
.end method

.method private k()V
    .locals 5

    .prologue
    .line 565
    sget-object v0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Flash mode is now %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-virtual {v4}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getExpectedFlashMode()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 566
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getExpectedFlashMode()Ljava/lang/String;

    move-result-object v0

    const-string v1, "torch"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 568
    iget-object v2, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    .line 569
    invoke-virtual {p0}, Lcom/google/bionics/scanner/CaptureActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    if-eqz v1, :cond_0

    sget v0, Lcom/google/bionics/scanner/docscanner/R$drawable;->ds_flash_off_holo_light:I

    :goto_0
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 568
    invoke-virtual {v2, v0}, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 571
    iget-object v2, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    .line 572
    invoke-virtual {p0}, Lcom/google/bionics/scanner/CaptureActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    if-eqz v1, :cond_1

    sget v0, Lcom/google/bionics/scanner/docscanner/R$string;->ds_disable_torch:I

    :goto_1
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 571
    invoke-virtual {v2, v0}, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 573
    return-void

    .line 569
    :cond_0
    sget v0, Lcom/google/bionics/scanner/docscanner/R$drawable;->ds_flash_on_holo_light:I

    goto :goto_0

    .line 572
    :cond_1
    sget v0, Lcom/google/bionics/scanner/docscanner/R$string;->ds_enable_torch:I

    goto :goto_1
.end method

.method private l()V
    .locals 3

    .prologue
    .line 576
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;

    if-eqz v0, :cond_0

    .line 577
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;

    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    invoke-virtual {v0, v1}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->startLoop(Lcom/google/bionics/scanner/unveil/util/Size;)V

    .line 581
    :goto_0
    return-void

    .line 579
    :cond_0
    sget-object v0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Trying to start PreviewLooper before it has been initialized"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private m()V
    .locals 3

    .prologue
    .line 584
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;

    if-eqz v0, :cond_0

    .line 585
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->pauseLoop()V

    .line 589
    :goto_0
    return-void

    .line 587
    :cond_0
    sget-object v0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Tried to stop null PreviewLooper"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private n()V
    .locals 1

    .prologue
    .line 592
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-virtual {v0, p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->focus(Lcom/google/bionics/scanner/unveil/camera/CameraManager$FocusCallback;)V

    .line 593
    return-void
.end method

.method private o()V
    .locals 1

    .prologue
    .line 596
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->isLastFrameBlurred()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 597
    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-virtual {v0, p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->focus(Lcom/google/bionics/scanner/unveil/camera/CameraManager$FocusCallback;)V

    .line 601
    :goto_0
    return-void

    .line 599
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/CaptureActivity;->onFocus(Z)V

    goto :goto_0
.end method

.method private p()V
    .locals 3

    .prologue
    .line 625
    sget-object v0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "take Picture"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 626
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->takePicture(Lcom/google/bionics/scanner/unveil/camera/CameraManager$ShutterCallback;Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureCallback;)V

    .line 627
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 508
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbhf;

    invoke-virtual {v0}, Lbhf;->c()V

    .line 509
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/CaptureActivity;->setResult(I)V

    .line 510
    invoke-virtual {p0}, Lcom/google/bionics/scanner/CaptureActivity;->finish()V

    .line 511
    return-void
.end method

.method public a(Lbhk;LbgY;)V
    .locals 8

    .prologue
    .line 451
    sget-object v0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Rectifying and storing images finished with status: %s, total time: %d ms"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 452
    invoke-virtual {p1}, Lbhk;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:J

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    .line 451
    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 453
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 454
    sget-object v0, Lbhk;->a:Lbhk;

    if-ne p1, v0, :cond_0

    .line 455
    invoke-direct {p0, p2}, Lcom/google/bionics/scanner/CaptureActivity;->a(LbgY;)V

    .line 459
    :goto_0
    return-void

    .line 457
    :cond_0
    invoke-static {p0, p1, p0}, Lbhf;->a(Landroid/content/Context;Lbhk;Lbhj;)V

    goto :goto_0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 1158
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/CaptureActivity;->setResult(I)V

    .line 1159
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbhf;

    if-eqz v0, :cond_0

    .line 1160
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbhf;

    invoke-virtual {v0}, Lbhf;->c()V

    .line 1162
    :cond_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/CaptureActivity;->finish()V

    .line 1163
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 255
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 256
    iput p2, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:I

    .line 257
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/content/Intent;

    .line 258
    sparse-switch p2, :sswitch_data_0

    .line 281
    :cond_0
    :goto_0
    return-void

    .line 263
    :sswitch_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/CaptureActivity;->a()V

    goto :goto_0

    .line 267
    :sswitch_1
    const-string v0, "com.google.bionics.scanner.extra.NUM_PAGES"

    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbhf;

    invoke-virtual {v1}, Lbhf;->a()LbgX;

    move-result-object v1

    invoke-virtual {v1}, LbgX;->a()I

    move-result v1

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 268
    const-string v0, "com.google.bionics.scanner.extra.IMAGE_BYTES"

    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbhf;

    .line 269
    invoke-virtual {v1}, Lbhf;->a()LbgX;

    move-result-object v1

    invoke-virtual {v1}, LbgX;->a()J

    move-result-wide v2

    .line 268
    invoke-virtual {p3, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 270
    const/4 v0, -0x1

    invoke-virtual {p0, v0, p3}, Lcom/google/bionics/scanner/CaptureActivity;->setResult(ILandroid/content/Intent;)V

    .line 271
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbhf;

    invoke-virtual {v0}, Lbhf;->c()V

    .line 272
    invoke-virtual {p0}, Lcom/google/bionics/scanner/CaptureActivity;->finish()V

    goto :goto_0

    .line 277
    :sswitch_2
    iput-object p3, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/content/Intent;

    goto :goto_0

    .line 258
    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_1
        0x0 -> :sswitch_0
        0xb -> :sswitch_2
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method public onCameraAcquisitionError()V
    .locals 0

    .prologue
    .line 640
    return-void
.end method

.method public onCameraConnected()V
    .locals 1

    .prologue
    .line 631
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->requestLayout()V

    .line 632
    return-void
.end method

.method public onCameraFlashControlError()V
    .locals 3

    .prologue
    .line 649
    sget-object v0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Failed to apply camera flash setting."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 650
    return-void
.end method

.method public onCameraLayoutComplete()V
    .locals 0

    .prologue
    .line 636
    return-void
.end method

.method public onCameraLayoutFinished(Lcom/google/bionics/scanner/unveil/util/Size;Landroid/graphics/Matrix;)V
    .locals 3

    .prologue
    .line 285
    sget-object v0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "onCameraLayoutFinished"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 286
    iput-object p1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    .line 287
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->startPreview()V

    .line 288
    invoke-direct {p0}, Lcom/google/bionics/scanner/CaptureActivity;->h()V

    .line 289
    return-void
.end method

.method public onCameraPreviewSizeChanged()V
    .locals 0

    .prologue
    .line 644
    invoke-direct {p0}, Lcom/google/bionics/scanner/CaptureActivity;->i()V

    .line 645
    return-void
.end method

.method public onCameraQualityError()V
    .locals 3

    .prologue
    .line 654
    sget-object v0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Failed to apply camera quality settings."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 655
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 124
    sget-object v0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "onCreate"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 125
    sget v0, Lcom/google/bionics/scanner/docscanner/R$style;->ds_scanner_capture_theme:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/CaptureActivity;->setTheme(I)V

    .line 126
    invoke-super {p0, p1}, Lbgg;->onCreate(Landroid/os/Bundle;)V

    .line 127
    invoke-direct {p0}, Lcom/google/bionics/scanner/CaptureActivity;->g()V

    .line 128
    sget v0, Lcom/google/bionics/scanner/docscanner/R$layout;->ds_capture_activity:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/CaptureActivity;->setContentView(I)V

    .line 130
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_camera_layout:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;

    iput-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;

    .line 131
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_camera_preview:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    iput-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    .line 132
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-virtual {v0, p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->registerListener(Lcom/google/bionics/scanner/unveil/camera/CameraManager$Listener;)V

    .line 133
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    sget-object v1, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;

    invoke-virtual {v0, v1}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->initializeSnapshotQuality(Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;)V

    .line 135
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_camera_layout:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;

    iput-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;

    .line 136
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;

    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-virtual {v0, v1}, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->setCameraManager(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)V

    .line 138
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;

    invoke-direct {p0}, Lcom/google/bionics/scanner/CaptureActivity;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->setRotation(I)V

    .line 139
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;

    sget-object v1, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$Alignment;->TOP:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$Alignment;

    invoke-virtual {v0, v1}, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->setAlignment(Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$Alignment;)V

    .line 141
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_preview_overlay:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/ui/PreviewOverlay;

    iput-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/ui/PreviewOverlay;

    .line 143
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_flash_button:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    iput-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    .line 144
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_shutter_button_layout:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/widget/FrameLayout;

    .line 146
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbgn;

    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/widget/FrameLayout;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lbgn;->a(Landroid/view/View;I)V

    .line 147
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbgn;

    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Lbgn;->a(Landroid/view/View;I)V

    .line 149
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_nonstop_debug_view:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/ui/DebugView;

    iput-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/ui/DebugView;

    .line 151
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/content/SharedPreferences;

    .line 152
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 154
    new-instance v0, Lcom/google/bionics/scanner/unveil/ui/Viewport;

    invoke-direct {v0, v3}, Lcom/google/bionics/scanner/unveil/ui/Viewport;-><init>(I)V

    iput-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/ui/Viewport;

    .line 156
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_progess_bar:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/CaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/widget/ProgressBar;

    .line 160
    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/content/SharedPreferences;

    invoke-static {p0, v0}, Lbhf;->a(Landroid/content/Context;Landroid/content/SharedPreferences;)Lbhf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbhf;

    .line 163
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;

    invoke-virtual {v0, p0}, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->setCameraLayoutHandler(Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$CameraLayoutHandler;)V

    .line 165
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbgn;

    invoke-virtual {v0}, Lbgn;->a()V
    :try_end_0
    .catch Lbhl; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    :goto_0
    return-void

    .line 166
    :catch_0
    move-exception v0

    .line 167
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbgn;

    invoke-virtual {v0}, Lbgn;->b()V

    .line 168
    sget-object v0, Lbhk;->b:Lbhk;

    invoke-static {p0, v0, p0}, Lbhf;->a(Landroid/content/Context;Lbhk;Lbhj;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 246
    sget-object v0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "onDestroy"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 247
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->shutdown()V

    .line 250
    :cond_0
    invoke-super {p0}, Lbgg;->onDestroy()V

    .line 251
    return-void
.end method

.method public onFocus(Z)V
    .locals 1

    .prologue
    .line 659
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbgn;

    invoke-static {v0, p1}, Lbgn;->a(Lbgn;Z)V

    .line 660
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    .line 293
    sget-object v0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "onKeyDown"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 294
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 295
    invoke-super {p0, p1, p2}, Lbgg;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 313
    :goto_0
    return v0

    .line 298
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 313
    :pswitch_0
    invoke-super {p0, p1, p2}, Lbgg;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 298
    nop

    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
    .end packed-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    .line 318
    sget-object v0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onKeyUp "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 319
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    invoke-super {p0, p1, p2}, Lbgg;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 343
    :goto_0
    return v0

    .line 323
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 343
    invoke-super {p0, p1, p2}, Lbgg;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 325
    :pswitch_0
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/google/bionics/scanner/CaptureActivity;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 326
    invoke-direct {p0}, Lcom/google/bionics/scanner/CaptureActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 328
    invoke-virtual {p0, p0}, Lcom/google/bionics/scanner/CaptureActivity;->a(Lbgi;)V

    .line 341
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 330
    :cond_1
    invoke-virtual {p0}, Lcom/google/bionics/scanner/CaptureActivity;->a()V

    goto :goto_1

    .line 332
    :cond_2
    invoke-direct {p0}, Lcom/google/bionics/scanner/CaptureActivity;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 335
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/bionics/scanner/CaptureActivity;->a(LbgY;)V

    goto :goto_1

    .line 339
    :cond_3
    invoke-virtual {p0}, Lcom/google/bionics/scanner/CaptureActivity;->a()V

    goto :goto_1

    .line 323
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 198
    sget-object v0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "onPause"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 199
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbgn;

    invoke-virtual {v0}, Lbgn;->e()V

    .line 200
    invoke-virtual {p0}, Lcom/google/bionics/scanner/CaptureActivity;->e()V

    .line 201
    invoke-super {p0}, Lbgg;->onPause()V

    .line 202
    return-void
.end method

.method public onPictureTaken(Lcom/google/bionics/scanner/unveil/util/Picture;)V
    .locals 3

    .prologue
    .line 664
    sget-object v0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Snapshot frame received!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 665
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbgn;

    invoke-virtual {v0, p1}, Lbgn;->a(Lcom/google/bionics/scanner/unveil/util/Picture;)V

    .line 666
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 223
    invoke-super {p0, p1}, Lbgg;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 226
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/content/Intent;

    .line 227
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/content/Intent;

    const-string v1, "com.google.bionics.scanner.extra.DOCUMENT_PAGE_INDEX"

    const-string v2, "SAVED_INSTANCE_PAGE_INDEX"

    const/4 v3, -0x1

    .line 228
    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 227
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 229
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/content/Intent;

    const-string v1, "com.google.bionics.scanner.extra.BACK_BUTTON"

    const-string v2, "SAVED_INSTANCE_BACK_BUTTON"

    const/4 v3, 0x0

    .line 230
    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 229
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 231
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/content/Intent;

    const-string v1, "SAVED_INSTANCE_FILE_NAME"

    const-string v2, "SAVED_INSTANCE_FILE_NAME"

    .line 232
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 231
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 233
    const-string v0, "SAVED_INSTANCE_LAST_RESULT_CODE"

    const/4 v1, 0x6

    .line 234
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:I

    .line 235
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 190
    sget-object v0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "onResume"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 191
    invoke-super {p0}, Lbgg;->onResume()V

    .line 192
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    aput-object v1, v0, v3

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/CaptureActivity;->a([Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;)V

    .line 193
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbgn;

    invoke-virtual {v0}, Lbgn;->d()V

    .line 194
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 209
    invoke-super {p0, p1}, Lbgg;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 210
    const-string v0, "SAVED_INSTANCE_LAST_RESULT_CODE"

    iget v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 211
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 212
    const-string v0, "SAVED_INSTANCE_BACK_BUTTON"

    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/content/Intent;

    const-string v2, "com.google.bionics.scanner.extra.BACK_BUTTON"

    const/4 v3, 0x0

    .line 213
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 212
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 214
    const-string v0, "SAVED_INSTANCE_PAGE_INDEX"

    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/content/Intent;

    const-string v2, "com.google.bionics.scanner.extra.DOCUMENT_PAGE_INDEX"

    const/4 v3, -0x1

    .line 215
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 214
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 216
    const-string v0, "SAVED_INSTANCE_FILE_NAME"

    iget-object v1, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Landroid/content/Intent;

    const-string v2, "SAVED_INSTANCE_FILE_NAME"

    .line 217
    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 216
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    :cond_0
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 605
    sget v0, Lcom/google/bionics/scanner/docscanner/R$string;->ds_display_real_time_quads_key:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/CaptureActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 606
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/rectifier/QuadsProcessor;

    if-eqz v0, :cond_0

    .line 608
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/rectifier/QuadsProcessor;

    sget v1, Lcom/google/bionics/scanner/docscanner/R$string;->ds_display_real_time_quads_default:I

    .line 609
    invoke-virtual {p0, v1}, Lcom/google/bionics/scanner/CaptureActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 608
    invoke-interface {p1, p2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/bionics/scanner/rectifier/QuadsProcessor;->setProcessingEnabled(Z)V

    .line 617
    :cond_0
    :goto_0
    return-void

    .line 611
    :cond_1
    sget v0, Lcom/google/bionics/scanner/docscanner/R$string;->ds_image_enhancement_method_key:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/CaptureActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbhf;

    invoke-virtual {v0}, Lbhf;->a()LbgX;

    move-result-object v0

    sget v1, Lcom/google/bionics/scanner/docscanner/R$string;->ds_image_enhancement_method_default:I

    .line 615
    invoke-virtual {p0, v1}, Lcom/google/bionics/scanner/CaptureActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 614
    invoke-interface {p1, p2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 613
    invoke-static {v1}, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->valueOf(Ljava/lang/String;)Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    move-result-object v1

    invoke-virtual {v0, v1}, LbgX;->a(Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;)V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 3

    .prologue
    .line 183
    sget-object v0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "onStart"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 184
    invoke-super {p0}, Lbgg;->onStart()V

    .line 185
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbgn;

    invoke-virtual {v0}, Lbgn;->c()V

    .line 186
    return-void
.end method

.method protected onStop()V
    .locals 3

    .prologue
    .line 239
    sget-object v0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "onStop"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 240
    iget-object v0, p0, Lcom/google/bionics/scanner/CaptureActivity;->a:Lbgn;

    invoke-virtual {v0}, Lbgn;->f()V

    .line 241
    invoke-super {p0}, Lbgg;->onStop()V

    .line 242
    return-void
.end method
