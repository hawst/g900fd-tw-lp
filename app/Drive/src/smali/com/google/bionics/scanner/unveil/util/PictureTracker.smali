.class public Lcom/google/bionics/scanner/unveil/util/PictureTracker;
.super Ljava/lang/Object;
.source "PictureTracker.java"


# static fields
.field private static final a:Lcom/google/bionics/scanner/unveil/util/Logger;

.field private static a:Lcom/google/bionics/scanner/unveil/util/PictureTracker;


# instance fields
.field a:Ljava/lang/Thread;

.field public a:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<",
            "Lcom/google/bionics/scanner/unveil/util/Picture;",
            ">;"
        }
    .end annotation
.end field

.field public final a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/ref/PhantomReference",
            "<",
            "Lcom/google/bionics/scanner/unveil/util/Picture;",
            ">;>;"
        }
    .end annotation
.end field

.field public volatile a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 22
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    const-class v1, Lcom/google/bionics/scanner/unveil/util/PictureTracker;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Ljava/lang/ref/ReferenceQueue;

    .line 32
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Ljava/util/Collection;

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Z

    .line 47
    return-void
.end method

.method public static synthetic a()Lcom/google/bionics/scanner/unveil/util/Logger;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-object v0
.end method

.method private a(Lcom/google/bionics/scanner/unveil/util/Picture;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 89
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    invoke-virtual {p1}, Lcom/google/bionics/scanner/unveil/util/Picture;->isRecycled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 92
    const-string v1, " already recycled"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 102
    :goto_0
    return-object v0

    .line 95
    :cond_0
    invoke-virtual {p1}, Lcom/google/bionics/scanner/unveil/util/Picture;->getSize()Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v1

    .line 96
    iget v2, v1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 97
    const-string v2, "x"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    iget v1, v1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 99
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    invoke-virtual {p1}, Lcom/google/bionics/scanner/unveil/util/Picture;->getByteSize()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 101
    const-string v1, " bytes"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/unveil/util/PictureTracker;Lcom/google/bionics/scanner/unveil/util/Picture;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a(Lcom/google/bionics/scanner/unveil/util/Picture;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance()Lcom/google/bionics/scanner/unveil/util/PictureTracker;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Lcom/google/bionics/scanner/unveil/util/PictureTracker;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/PictureTracker;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/PictureTracker;-><init>()V

    sput-object v0, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Lcom/google/bionics/scanner/unveil/util/PictureTracker;

    .line 53
    :cond_0
    sget-object v0, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Lcom/google/bionics/scanner/unveil/util/PictureTracker;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized exitWhenFinished()V
    .locals 2

    .prologue
    .line 79
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Z

    .line 80
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 81
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Ljava/lang/Thread;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 82
    :try_start_1
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 83
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 85
    :cond_0
    monitor-exit p0

    return-void

    .line 83
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 79
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized track(Lcom/google/bionics/scanner/unveil/util/Picture;Lcom/google/bionics/scanner/unveil/util/Picture;)V
    .locals 5

    .prologue
    .line 63
    monitor-enter p0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 75
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 66
    :cond_1
    :try_start_0
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Z

    if-eqz v0, :cond_2

    .line 67
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No picture can be added once exitWhenFinished() is called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 69
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Ljava/lang/Thread;

    if-nez v0, :cond_3

    .line 70
    new-instance v0, Lbia;

    invoke-direct {v0, p0}, Lbia;-><init>(Lcom/google/bionics/scanner/unveil/util/PictureTracker;)V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Ljava/lang/Thread;

    .line 71
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 73
    :cond_3
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Ljava/util/Collection;

    new-instance v1, Lbib;

    iget-object v2, p0, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v1, p0, p1, v2, p2}, Lbib;-><init>(Lcom/google/bionics/scanner/unveil/util/PictureTracker;Lcom/google/bionics/scanner/unveil/util/Picture;Ljava/lang/ref/ReferenceQueue;Lcom/google/bionics/scanner/unveil/util/Picture;)V

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 74
    sget-object v0, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Tracked: %s, total %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-direct {p0, p2}, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a(Lcom/google/bionics/scanner/unveil/util/Picture;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/bionics/scanner/unveil/util/PictureTracker;->a:Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
