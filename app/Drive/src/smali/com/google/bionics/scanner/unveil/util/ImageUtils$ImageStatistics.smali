.class public Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;
.super Ljava/lang/Object;
.source "ImageUtils.java"


# instance fields
.field final synthetic a:Lcom/google/bionics/scanner/unveil/util/ImageUtils;

.field public final averageContrast:F

.field public final averageintensity:F

.field public final contrastStdDev:F

.field public final intensityStdDev:F

.field public final maxContrast:F

.field public final maxintensity:F

.field public final minContrast:F

.field public final minIntensity:F


# direct methods
.method protected constructor <init>(Lcom/google/bionics/scanner/unveil/util/ImageUtils;[F)V
    .locals 2

    .prologue
    .line 455
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;->a:Lcom/google/bionics/scanner/unveil/util/ImageUtils;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 456
    const/4 v0, 0x0

    .line 457
    const/4 v1, 0x1

    aget v0, p2, v0

    iput v0, p0, Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;->minIntensity:F

    .line 458
    const/4 v0, 0x2

    aget v1, p2, v1

    iput v1, p0, Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;->averageintensity:F

    .line 459
    const/4 v1, 0x3

    aget v0, p2, v0

    iput v0, p0, Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;->maxintensity:F

    .line 460
    const/4 v0, 0x4

    aget v1, p2, v1

    iput v1, p0, Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;->intensityStdDev:F

    .line 462
    const/4 v1, 0x5

    aget v0, p2, v0

    iput v0, p0, Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;->minContrast:F

    .line 463
    const/4 v0, 0x6

    aget v1, p2, v1

    iput v1, p0, Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;->averageContrast:F

    .line 464
    const/4 v1, 0x7

    aget v0, p2, v0

    iput v0, p0, Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;->maxContrast:F

    .line 465
    aget v0, p2, v1

    iput v0, p0, Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;->contrastStdDev:F

    .line 466
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 470
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Int: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;->minIntensity:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;->averageintensity:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;->maxintensity:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;->intensityStdDev:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  Grad: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;->minContrast:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;->averageContrast:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;->maxContrast:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;->contrastStdDev:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
