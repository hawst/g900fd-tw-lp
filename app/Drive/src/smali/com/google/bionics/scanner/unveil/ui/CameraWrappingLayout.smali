.class public Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;
.super Landroid/widget/FrameLayout;
.source "CameraWrappingLayout.java"


# instance fields
.field private a:I

.field private a:Landroid/graphics/Matrix;

.field private a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

.field a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$Alignment;

.field private a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$CameraLayoutHandler;

.field private a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$ScaleType;

.field private final a:Lcom/google/bionics/scanner/unveil/util/Logger;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 24
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    .line 55
    sget-object v0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$Alignment;->TOP:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$Alignment;

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$Alignment;

    .line 57
    sget-object v0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$ScaleType;->FIT:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$ScaleType;

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$ScaleType;

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    .line 55
    sget-object v0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$Alignment;->TOP:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$Alignment;

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$Alignment;

    .line 57
    sget-object v0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$ScaleType;->FIT:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$ScaleType;

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$ScaleType;

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    .line 55
    sget-object v0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$Alignment;->TOP:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$Alignment;

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$Alignment;

    .line 57
    sget-object v0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$ScaleType;->FIT:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$ScaleType;

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$ScaleType;

    .line 76
    return-void
.end method

.method static a(Lcom/google/bionics/scanner/unveil/util/Size;Lcom/google/bionics/scanner/unveil/util/Size;ILcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$ScaleType;)Lcom/google/bionics/scanner/unveil/util/Size;
    .locals 3

    .prologue
    .line 108
    sget-object v0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$ScaleType;->FILL:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$ScaleType;

    if-ne p3, v0, :cond_0

    .line 128
    :goto_0
    return-object p0

    .line 112
    :cond_0
    const/16 v0, 0x5a

    if-eq p2, v0, :cond_1

    const/16 v0, 0x10e

    if-ne p2, v0, :cond_2

    :cond_1
    iget v0, p1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    int-to-float v0, v0

    iget v1, p1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 116
    :goto_1
    iget v1, p0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 120
    cmpl-float v1, v0, v1

    if-lez v1, :cond_3

    .line 121
    iget v1, p0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    .line 122
    iget v2, p0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    int-to-float v2, v2

    div-float v0, v2, v0

    float-to-int v0, v0

    .line 128
    :goto_2
    new-instance p0, Lcom/google/bionics/scanner/unveil/util/Size;

    invoke-direct {p0, v1, v0}, Lcom/google/bionics/scanner/unveil/util/Size;-><init>(II)V

    goto :goto_0

    .line 112
    :cond_2
    iget v0, p1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    int-to-float v0, v0

    iget v1, p1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_1

    .line 124
    :cond_3
    iget v1, p0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v1, v0

    .line 125
    iget v0, p0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    goto :goto_2
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 133
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 134
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-virtual {v0, p1}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 135
    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 10

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "onLayout: %b, %d, %d, %d, %d"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 142
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->isPreviewing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Already previewing or no layout needed, but recursing anyway."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 147
    :cond_0
    new-instance v1, Lcom/google/bionics/scanner/unveil/util/Size;

    sub-int v0, p4, p2

    sub-int v2, p5, p3

    invoke-direct {v1, v0, v2}, Lcom/google/bionics/scanner/unveil/util/Size;-><init>(II)V

    .line 149
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Full view size: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 152
    iget-object v2, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    monitor-enter v2

    .line 155
    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-virtual {v0, v1}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->setFullScreenDisplaySize(Lcom/google/bionics/scanner/unveil/util/Size;)V

    .line 156
    iget v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:I

    const/16 v3, 0x5a

    if-eq v0, v3, :cond_1

    iget v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:I

    const/16 v3, 0x10e

    if-ne v0, v3, :cond_3

    .line 157
    :cond_1
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    iget v3, v1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    iget v4, v1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    invoke-virtual {v0, v3, v4}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getOptimalPreviewSize(II)Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v0

    .line 161
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    if-nez v0, :cond_4

    .line 164
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Preview size was null!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 165
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 212
    :cond_2
    :goto_1
    return-void

    .line 159
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    iget v3, v1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    iget v4, v1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    invoke-virtual {v0, v3, v4}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getOptimalPreviewSize(II)Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v0

    goto :goto_0

    .line 161
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 169
    :cond_4
    iget v2, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:I

    iget-object v3, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$ScaleType;

    invoke-static {v1, v0, v2, v3}, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a(Lcom/google/bionics/scanner/unveil/util/Size;Lcom/google/bionics/scanner/unveil/util/Size;ILcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$ScaleType;)Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v2

    .line 170
    iget v3, v1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    iget v4, v2, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    .line 171
    iget v1, v1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    iget v4, v2, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    sub-int/2addr v1, v4

    div-int/lit8 v1, v1, 0x2

    .line 173
    iget v4, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:I

    invoke-static {v0, v2, v4}, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->getTransformationMatrix(Lcom/google/bionics/scanner/unveil/util/Size;Lcom/google/bionics/scanner/unveil/util/Size;I)Landroid/graphics/Matrix;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Landroid/graphics/Matrix;

    .line 174
    sget-object v0, LbhS;->a:[I

    iget-object v4, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$Alignment;

    invoke-virtual {v4}, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$Alignment;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    .line 182
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v4, "Error: unsupported alignment %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$Alignment;

    invoke-virtual {v7}, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$Alignment;->name()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v0, v4, v5}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 185
    :pswitch_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Landroid/graphics/Matrix;

    int-to-float v4, v3

    int-to-float v5, v1

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 189
    :goto_2
    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->getChildCount()I

    move-result v4

    if-ge v0, v4, :cond_5

    .line 190
    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 191
    sget-object v5, LbhS;->a:[I

    iget-object v6, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$Alignment;

    invoke-virtual {v6}, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$Alignment;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_1

    .line 200
    iget-object v5, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v6, "Error: unsupported alignment %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$Alignment;

    invoke-virtual {v9}, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$Alignment;->name()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 203
    :pswitch_1
    iget v5, v2, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    add-int/2addr v5, v3

    iget v6, v2, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    add-int/2addr v6, v1

    invoke-virtual {v4, v3, v1, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 189
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 176
    :pswitch_2
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Landroid/graphics/Matrix;

    int-to-float v4, v3

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_2

    .line 179
    :pswitch_3
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Landroid/graphics/Matrix;

    int-to-float v4, v3

    mul-int/lit8 v5, v1, 0x2

    int-to-float v5, v5

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_2

    .line 193
    :pswitch_4
    const/4 v5, 0x0

    iget v6, v2, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    add-int/2addr v6, v3

    iget v7, v2, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    invoke-virtual {v4, v3, v5, v6, v7}, Landroid/view/View;->layout(IIII)V

    goto :goto_4

    .line 196
    :pswitch_5
    mul-int/lit8 v5, v1, 0x2

    iget v6, v2, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    add-int/2addr v6, v3

    mul-int/lit8 v7, v1, 0x2

    iget v8, v2, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    add-int/2addr v7, v8

    invoke-virtual {v4, v3, v5, v6, v7}, Landroid/view/View;->layout(IIII)V

    goto :goto_4

    .line 209
    :cond_5
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$CameraLayoutHandler;

    if-eqz v0, :cond_2

    .line 210
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$CameraLayoutHandler;

    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Landroid/graphics/Matrix;

    invoke-interface {v0, v2, v1}, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$CameraLayoutHandler;->onCameraLayoutFinished(Lcom/google/bionics/scanner/unveil/util/Size;Landroid/graphics/Matrix;)V

    goto/16 :goto_1

    .line 174
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch

    .line 191
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_1
    .end packed-switch
.end method

.method public setAlignment(Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$Alignment;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$Alignment;

    .line 100
    return-void
.end method

.method public setCameraLayoutHandler(Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$CameraLayoutHandler;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$CameraLayoutHandler;

    .line 92
    return-void
.end method

.method public setCameraManager(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    .line 88
    return-void
.end method

.method public setRotation(I)V
    .locals 0

    .prologue
    .line 95
    iput p1, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:I

    .line 96
    return-void
.end method

.method public setScaleType(Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$ScaleType;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout;->a:Lcom/google/bionics/scanner/unveil/ui/CameraWrappingLayout$ScaleType;

    .line 104
    return-void
.end method
