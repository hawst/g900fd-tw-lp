.class public Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;
.super Ljava/lang/Object;
.source "TimestampedFrame.java"


# instance fields
.field private a:J

.field private b:J

.field private c:J

.field private d:J

.field private final e:J

.field private final f:J


# direct methods
.method public constructor <init>(J)V
    .locals 3

    .prologue
    const-wide/16 v0, -0x1

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-wide p1, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->f:J

    .line 72
    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->b:J

    .line 73
    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->a:J

    .line 74
    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->c:J

    .line 75
    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->d:J

    .line 76
    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->e:J

    .line 77
    return-void
.end method

.method private a()J
    .locals 4

    .prologue
    .line 129
    iget-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->b:J

    iget-wide v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->f:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method private b()J
    .locals 4

    .prologue
    .line 137
    iget-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->c:J

    iget-wide v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->f:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method private c()J
    .locals 4

    .prologue
    .line 141
    iget-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->d:J

    iget-wide v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->f:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method private d()J
    .locals 4

    .prologue
    .line 145
    iget-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->e:J

    iget-wide v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->f:J

    sub-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public getDebugText()Ljava/util/Vector;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v4, -0x1

    .line 149
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 150
    iget-wide v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->d:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 151
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "up: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->c()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 154
    :cond_0
    iget-wide v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->e:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 155
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "s+r: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->d()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 157
    :cond_1
    return-object v0
.end method

.method public getEncodeFinishedTime()J
    .locals 4

    .prologue
    .line 133
    iget-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->a:J

    iget-wide v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->f:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public getQuerySentMillis()J
    .locals 2

    .prologue
    .line 96
    iget-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->d:J

    return-wide v0
.end method

.method public getTimestamp()J
    .locals 2

    .prologue
    .line 176
    iget-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->f:J

    return-wide v0
.end method

.method public getTimingString()Ljava/lang/String;
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 161
    const-string v0, "%4d\t%4d\t%4d"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    .line 162
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->getEncodeFinishedTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v7

    const/4 v2, 0x2

    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 161
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 164
    iget-wide v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->d:J

    cmp-long v1, v2, v8

    if-eqz v1, :cond_0

    .line 165
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\t%4d"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 168
    :cond_0
    iget-wide v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->e:J

    cmp-long v1, v2, v8

    if-eqz v1, :cond_1

    .line 169
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\t%4d"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->d()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 172
    :cond_1
    return-object v0
.end method

.method public onCreatedCachedJpeg()V
    .locals 2

    .prologue
    .line 124
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->a:J

    .line 125
    return-void
.end method

.method public onPickedForQuery()V
    .locals 4

    .prologue
    .line 100
    iget-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->b:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 101
    invoke-static {}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "onPickedForQuery called multiple times."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    :goto_0
    return-void

    .line 103
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->b:J

    goto :goto_0
.end method

.method public onQuerySent()V
    .locals 4

    .prologue
    .line 108
    iget-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->d:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 109
    invoke-static {}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "querySent called multiple times."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 113
    :goto_0
    return-void

    .line 111
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->d:J

    goto :goto_0
.end method

.method public onQueryStartSending()V
    .locals 4

    .prologue
    .line 116
    iget-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->c:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 117
    invoke-static {}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a()Lcom/google/bionics/scanner/unveil/util/Logger;

    move-result-object v0

    const-string v1, "queryStart called multiple times."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 121
    :goto_0
    return-void

    .line 119
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;->c:J

    goto :goto_0
.end method
