.class public Lcom/google/bionics/scanner/unveil/util/Stopwatch;
.super Ljava/lang/Object;
.source "Stopwatch.java"


# instance fields
.field private a:J

.field private final a:Ljava/lang/String;

.field private a:Z

.field private b:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->a:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->a:Ljava/lang/String;

    .line 25
    return-void
.end method


# virtual methods
.method public getElapsedMilliseconds()J
    .locals 6

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->a:Z

    if-eqz v0, :cond_0

    .line 63
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 64
    iget-wide v2, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->b:J

    iget-wide v4, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->a:J

    sub-long v4, v0, v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->b:J

    .line 65
    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->a:J

    .line 67
    :cond_0
    iget-wide v0, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->b:J

    return-wide v0
.end method

.method public getElapsedSeconds()F
    .locals 2

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->getElapsedMilliseconds()J

    move-result-wide v0

    long-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->a:Z

    return v0
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 53
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->a:J

    .line 54
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->b:J

    .line 55
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->a:Z

    if-eqz v0, :cond_0

    .line 34
    :goto_0
    return-void

    .line 32
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->a:J

    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->a:Z

    goto :goto_0
.end method

.method public startFromZero()V
    .locals 2

    .prologue
    .line 37
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->b:J

    .line 38
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->a:J

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->a:Z

    .line 40
    return-void
.end method

.method public stop()V
    .locals 6

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->a:Z

    if-nez v0, :cond_0

    .line 50
    :goto_0
    return-void

    .line 47
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 48
    iget-wide v2, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->b:J

    iget-wide v4, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->a:J

    sub-long/2addr v0, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->b:J

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->a:Z

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 80
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
