.class public Lcom/google/bionics/scanner/unveil/util/JpegPicture;
.super Lcom/google/bionics/scanner/unveil/util/Picture;
.source "JpegPicture.java"


# static fields
.field private static final a:Lcom/google/bionics/scanner/unveil/util/Logger;


# instance fields
.field private a:Landroid/graphics/drawable/BitmapDrawable;

.field private a:Lcom/google/bionics/scanner/unveil/util/Size;

.field private a:[B

.field private b:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>()V

    sput-object v0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0, p2}, Lcom/google/bionics/scanner/unveil/util/Picture;-><init>(I)V

    .line 23
    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:Landroid/graphics/drawable/BitmapDrawable;

    .line 24
    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    .line 28
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:[B

    .line 29
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0, p2, p3}, Lcom/google/bionics/scanner/unveil/util/Picture;-><init>(II)V

    .line 23
    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:Landroid/graphics/drawable/BitmapDrawable;

    .line 24
    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    .line 33
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:[B

    .line 34
    return-void
.end method


# virtual methods
.method public declared-synchronized getByteSize()I
    .locals 1

    .prologue
    .line 153
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a()V

    .line 154
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:[B

    array-length v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 153
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getCroppedPicture()Lcom/google/bionics/scanner/unveil/util/Picture;
    .locals 4

    .prologue
    .line 56
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a()V

    .line 58
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->getCropArea()Landroid/graphics/Rect;

    move-result-object v0

    .line 59
    if-eqz v0, :cond_2

    .line 60
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->getSize()Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v1

    .line 61
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget v3, v1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    if-ne v2, v3, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    iget v1, v1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    if-eq v2, v1, :cond_2

    .line 62
    :cond_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->peekBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->cropBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 63
    if-nez v1, :cond_1

    const/4 v0, 0x0

    .line 66
    :goto_0
    monitor-exit p0

    return-object v0

    .line 63
    :cond_1
    :try_start_1
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;

    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->getOrientation()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;-><init>(Landroid/graphics/Bitmap;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move-object v0, p0

    .line 66
    goto :goto_0
.end method

.method public declared-synchronized getDrawable()Landroid/graphics/drawable/BitmapDrawable;
    .locals 2

    .prologue
    .line 95
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a()V

    .line 96
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:Landroid/graphics/drawable/BitmapDrawable;

    if-nez v0, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->peekBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 98
    if-eqz v0, :cond_0

    .line 99
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:Landroid/graphics/drawable/BitmapDrawable;

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:Landroid/graphics/drawable/BitmapDrawable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getJpegData()[B
    .locals 1

    .prologue
    .line 38
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a()V

    .line 39
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 38
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSize()Lcom/google/bionics/scanner/unveil/util/Size;
    .locals 4

    .prologue
    .line 44
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a()V

    .line 45
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 47
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 48
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:[B

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:[B

    array-length v3, v3

    invoke-static {v1, v2, v3, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 49
    new-instance v1, Lcom/google/bionics/scanner/unveil/util/Size;

    iget v2, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-direct {v1, v2, v0}, Lcom/google/bionics/scanner/unveil/util/Size;-><init>(II)V

    iput-object v1, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:Lcom/google/bionics/scanner/unveil/util/Size;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 44
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getYuvData()[B
    .locals 4

    .prologue
    .line 71
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a()V

    .line 72
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->b:[B

    if-nez v0, :cond_1

    .line 75
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->getSize()Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v0

    .line 78
    iget v1, v0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    iget v2, v0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    mul-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x2

    new-array v1, v1, [B

    .line 80
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 82
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->peekBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 83
    if-eqz v3, :cond_0

    .line 84
    invoke-virtual {v3, v2}, Landroid/graphics/Bitmap;->copyPixelsToBuffer(Ljava/nio/Buffer;)V

    .line 87
    :cond_0
    iget v2, v0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    iget v3, v0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    invoke-static {v2, v3}, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->getYUVByteSize(II)I

    move-result v2

    new-array v2, v2, [B

    iput-object v2, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->b:[B

    .line 88
    iget-object v2, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->b:[B

    iget v3, v0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    iget v0, v0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    invoke-static {v1, v2, v3, v0}, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->convertRGB565ToYUV420SP([B[BII)V

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->b:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 71
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized peekBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 107
    monitor-enter p0

    :try_start_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->peekBitmap(Landroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized peekBitmap(Landroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 112
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a()V

    .line 113
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_1

    .line 114
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 148
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 117
    :cond_1
    :try_start_1
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 122
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 123
    iput-object p1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 125
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:[B

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:[B

    array-length v3, v3

    invoke-static {v1, v2, v3, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 127
    if-nez v1, :cond_2

    .line 128
    sget-object v0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Critical failure when decoding JPEG"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 129
    const/4 v0, 0x0

    goto :goto_0

    .line 133
    :cond_2
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    if-eq v0, p1, :cond_3

    .line 135
    const/4 v0, 0x0

    invoke-virtual {v1, p1, v0}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 136
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 141
    :goto_1
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    if-nez v1, :cond_0

    .line 142
    new-instance v1, Lcom/google/bionics/scanner/unveil/util/Size;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Size;-><init>(II)V

    iput-object v1, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:Lcom/google/bionics/scanner/unveil/util/Size;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 112
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public declared-synchronized recycle()V
    .locals 3

    .prologue
    .line 159
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    sget-object v0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Requested recycling, but jpeg picture is already recycled."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    :goto_0
    monitor-exit p0

    return-void

    .line 163
    :cond_0
    :try_start_1
    invoke-super {p0}, Lcom/google/bionics/scanner/unveil/util/Picture;->recycle()V

    .line 164
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_1

    .line 165
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 167
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:[B

    .line 168
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->b:[B

    .line 169
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:Landroid/graphics/drawable/BitmapDrawable;

    .line 170
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/util/JpegPicture;->a:Lcom/google/bionics/scanner/unveil/util/Size;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 159
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
