.class public Lcom/google/bionics/scanner/unveil/camera/CameraManager;
.super Landroid/view/SurfaceView;
.source "CameraManager.java"

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;
.implements Landroid/view/SurfaceHolder$Callback;
.implements LbhO;


# static fields
.field public static final FLASH_MODE_UNSUPPORTED:Ljava/lang/String; = "unsupported"

.field public static final FOCUS_MODE_CONTINUOUS_PICTURE:Ljava/lang/String; = "continuous-picture"

.field public static final RATIO_SLOP:F = 0.1f

.field private static final a:Lcom/google/bionics/scanner/unveil/util/Logger;

.field private static a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/bionics/scanner/unveil/util/Size;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private volatile a:I

.field private a:LbhL;

.field private a:Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;

.field private a:Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;

.field private a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

.field private a:Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

.field private a:Lcom/google/bionics/scanner/unveil/util/Size;

.field private a:Ljava/lang/Boolean;

.field private final a:Ljava/lang/Object;

.field private volatile a:Ljava/lang/String;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/scanner/unveil/camera/CameraManager$FocusCallback;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/bionics/scanner/unveil/camera/CameraManager$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/concurrent/Executor;

.field private volatile a:Z

.field private volatile b:I

.field private b:Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;

.field private volatile b:Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

.field private b:Lcom/google/bionics/scanner/unveil/util/Size;

.field private b:Ljava/lang/Boolean;

.field private b:Ljava/lang/String;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/scanner/unveil/camera/CameraManager$ShutterCallback;",
            ">;"
        }
    .end annotation
.end field

.field private volatile b:Z

.field private volatile c:I

.field private c:Ljava/lang/Boolean;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureCallback;",
            ">;"
        }
    .end annotation
.end field

.field private volatile c:Z

.field private volatile d:I

.field private volatile d:Z

.field private e:I

.field private volatile e:Z

.field private f:I

.field private f:Z

.field private g:I

.field private g:Z

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>()V

    sput-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 227
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 63
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/Set;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/List;

    .line 71
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;->NORMAL_QUALITY:Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;

    .line 84
    iput v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:I

    .line 88
    iput-object v2, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/lang/String;

    .line 89
    const-string v0, "off"

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Ljava/lang/String;

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Ljava/util/List;

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Ljava/util/List;

    .line 94
    iput-boolean v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Z

    .line 95
    iput-object v2, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    .line 96
    iput-boolean v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->d:Z

    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->e:Z

    .line 105
    iput-boolean v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->f:Z

    .line 110
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/concurrent/Executor;

    .line 114
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/lang/Object;

    .line 134
    iput-object v2, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Lcom/google/bionics/scanner/unveil/util/Size;

    .line 228
    invoke-direct {p0, p1}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Landroid/content/Context;)V

    .line 229
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 232
    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/Set;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/List;

    .line 71
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;->NORMAL_QUALITY:Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;

    .line 84
    iput v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:I

    .line 88
    iput-object v2, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/lang/String;

    .line 89
    const-string v0, "off"

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Ljava/lang/String;

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Ljava/util/List;

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Ljava/util/List;

    .line 94
    iput-boolean v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Z

    .line 95
    iput-object v2, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    .line 96
    iput-boolean v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->d:Z

    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->e:Z

    .line 105
    iput-boolean v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->f:Z

    .line 110
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/concurrent/Executor;

    .line 114
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/lang/Object;

    .line 134
    iput-object v2, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Lcom/google/bionics/scanner/unveil/util/Size;

    .line 233
    invoke-direct {p0, p1}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Landroid/content/Context;)V

    .line 234
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 237
    invoke-direct {p0, p1, p2, p3}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/Set;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/List;

    .line 71
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;->NORMAL_QUALITY:Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;

    .line 84
    iput v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:I

    .line 88
    iput-object v2, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/lang/String;

    .line 89
    const-string v0, "off"

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Ljava/lang/String;

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Ljava/util/List;

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Ljava/util/List;

    .line 94
    iput-boolean v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Z

    .line 95
    iput-object v2, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    .line 96
    iput-boolean v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->d:Z

    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->e:Z

    .line 105
    iput-boolean v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->f:Z

    .line 110
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/concurrent/Executor;

    .line 114
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/lang/Object;

    .line 134
    iput-object v2, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Lcom/google/bionics/scanner/unveil/util/Size;

    .line 238
    invoke-direct {p0, p1}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Landroid/content/Context;)V

    .line 239
    return-void
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/unveil/camera/CameraManager;I)I
    .locals 0

    .prologue
    .line 48
    iput p1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:I

    return p1
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)LbhL;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:LbhL;

    return-object v0
.end method

.method private declared-synchronized a()Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;
    .locals 3

    .prologue
    .line 1179
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-eqz v0, :cond_0

    .line 1180
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    if-eqz v0, :cond_1

    .line 1183
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "getParameters returning deferred value set while taking a picture!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1184
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    .line 1190
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1186
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    invoke-interface {v0}, Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;->getParameters()Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1179
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a()Lcom/google/bionics/scanner/unveil/util/Size;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/16 v6, 0x280

    const/16 v5, 0x1e0

    .line 1279
    const-string v0, "Galaxy Nexus"

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1282
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Size;

    const/16 v1, 0x3c0

    const/16 v2, 0x2d0

    invoke-direct {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Size;-><init>(II)V

    .line 1283
    sget-object v1, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Special case: using %s for preview size on Galaxy Nexus"

    new-array v3, v8, [Ljava/lang/Object;

    aput-object v0, v3, v7

    invoke-virtual {v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1327
    :goto_0
    return-object v0

    .line 1289
    :cond_0
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/HashMap;

    if-nez v0, :cond_1

    .line 1290
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/HashMap;

    .line 1295
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/HashMap;

    const-string v1, "SPH-M900"

    new-instance v2, Lcom/google/bionics/scanner/unveil/util/Size;

    invoke-direct {v2, v6, v5}, Lcom/google/bionics/scanner/unveil/util/Size;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1300
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/HashMap;

    const-string v1, "DROIDX"

    new-instance v2, Lcom/google/bionics/scanner/unveil/util/Size;

    const/16 v3, 0x320

    const/16 v4, 0x1c0

    invoke-direct {v2, v3, v4}, Lcom/google/bionics/scanner/unveil/util/Size;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1304
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/HashMap;

    const-string v1, "XT720"

    new-instance v2, Lcom/google/bionics/scanner/unveil/util/Size;

    const/16 v3, 0x350

    invoke-direct {v2, v3, v5}, Lcom/google/bionics/scanner/unveil/util/Size;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1309
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/HashMap;

    const-string v1, "Nexus S"

    new-instance v2, Lcom/google/bionics/scanner/unveil/util/Size;

    invoke-direct {v2, v6, v5}, Lcom/google/bionics/scanner/unveil/util/Size;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1311
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/HashMap;

    const-string v1, "Droid"

    new-instance v2, Lcom/google/bionics/scanner/unveil/util/Size;

    invoke-direct {v2, v6, v5}, Lcom/google/bionics/scanner/unveil/util/Size;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1317
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/HashMap;

    const-string v1, "SGH-T999"

    new-instance v2, Lcom/google/bionics/scanner/unveil/util/Size;

    invoke-direct {v2, v6, v5}, Lcom/google/bionics/scanner/unveil/util/Size;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1320
    :cond_1
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/HashMap;

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1321
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/HashMap;

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/util/Size;

    .line 1322
    sget-object v1, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Special case: using %s for preview size on %s."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v7

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v4, v3, v8

    invoke-virtual {v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1327
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(Ljava/util/List;IIZI)Lcom/google/bionics/scanner/unveil/util/Size;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/scanner/unveil/util/Size;",
            ">;IIZI)",
            "Lcom/google/bionics/scanner/unveil/util/Size;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1431
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/util/Logger;->shouldShowVerbose()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1433
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "findOptimalSize([%s], %d, %d, %b, %d)"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0}, Lcom/google/bionics/scanner/unveil/util/Size;->sizeListToString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    .line 1434
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v10

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    const/4 v3, 0x3

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 1433
    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1437
    :cond_0
    int-to-float v0, p1

    int-to-float v1, p2

    div-float v4, v0, v1

    .line 1438
    const v0, 0x3dcccccd    # 0.1f

    mul-float v5, v4, v0

    .line 1439
    if-eqz p3, :cond_1

    .line 1440
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Target ratio is %f, allowed slop of %f"

    new-array v2, v6, [Ljava/lang/Object;

    .line 1441
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v10

    .line 1440
    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1444
    :cond_1
    mul-int v6, p1, p2

    .line 1446
    const v2, 0x7fffffff

    .line 1447
    const/4 v1, 0x0

    .line 1448
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/util/Size;

    .line 1449
    if-lez p4, :cond_3

    iget v3, v0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    iget v8, v0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    mul-int/2addr v3, v8

    if-gt v3, p4, :cond_2

    .line 1452
    :cond_3
    iget v3, v0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    int-to-float v3, v3

    iget v8, v0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    int-to-float v8, v8

    div-float/2addr v3, v8

    .line 1453
    sub-float v3, v4, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 1456
    if-eqz p3, :cond_4

    cmpg-float v3, v3, v5

    if-gtz v3, :cond_8

    .line 1457
    :cond_4
    iget v3, v0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    iget v8, v0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    mul-int/2addr v3, v8

    sub-int/2addr v3, v6

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 1458
    if-ge v3, v2, :cond_8

    move v1, v3

    :goto_1
    move v2, v1

    move-object v1, v0

    .line 1463
    goto :goto_0

    .line 1465
    :cond_5
    if-nez v1, :cond_6

    if-eqz p3, :cond_6

    .line 1466
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Couldn\'t find size that meets aspect ratio requirement, trying without."

    new-array v2, v9, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1467
    invoke-static {p0, p1, p2, v9, p4}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Ljava/util/List;IIZI)Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v1

    .line 1473
    :goto_2
    return-object v1

    .line 1468
    :cond_6
    if-nez v1, :cond_7

    .line 1469
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "No optimal size!"

    new-array v3, v9, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1472
    :cond_7
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Optimal size is %s"

    new-array v3, v10, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/google/bionics/scanner/unveil/util/Size;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v9

    invoke-virtual {v0, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    :cond_8
    move-object v0, v1

    move v1, v2

    goto :goto_1
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/unveil/camera/CameraManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/lang/String;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)Ljava/util/List;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Ljava/util/List;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 256
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 257
    return-void
.end method

.method private a(II)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 826
    invoke-direct {p0, v3}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Z)V

    .line 829
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b(II)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 839
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->setPictureQuality(Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;)V

    .line 840
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->f()V

    .line 843
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    .line 845
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->d()V

    .line 848
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->setFlashMode(Ljava/lang/String;)V

    .line 849
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->g()V

    .line 850
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b()V

    .line 851
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$Listener;

    .line 852
    invoke-interface {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager$Listener;->onCameraPreviewSizeChanged()V

    goto :goto_0

    .line 830
    :catch_0
    move-exception v0

    .line 833
    sget-object v1, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Failed to set optimal preview size."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 835
    invoke-direct {p0, v4}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Z)V

    .line 863
    :goto_1
    return-void

    .line 855
    :cond_0
    invoke-direct {p0, v4}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Z)V

    .line 857
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Starting preview!"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 858
    iput-boolean v3, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->d:Z

    .line 859
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    invoke-interface {v0}, Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;->startPreview()V

    .line 861
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c()V

    .line 862
    iput v4, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->f:I

    goto :goto_1
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 242
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 243
    invoke-interface {v0}, Landroid/view/SurfaceHolder;->setSizeFromLayout()V

    .line 244
    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 246
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a()V

    .line 248
    if-eqz p1, :cond_0

    .line 249
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(I)Z

    .line 252
    :cond_0
    new-instance v0, LbhL;

    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/concurrent/Executor;

    invoke-direct {v0, v1, p0}, LbhL;-><init>(Ljava/util/concurrent/Executor;LbhO;)V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:LbhL;

    .line 253
    return-void
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->h()V

    return-void
.end method

.method private declared-synchronized a(Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;)V
    .locals 1

    .prologue
    .line 1203
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    .line 1204
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    .line 1206
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->f:Z

    if-nez v0, :cond_0

    .line 1207
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1209
    :cond_0
    monitor-exit p0

    return-void

    .line 1203
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 1213
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->f:Z

    .line 1215
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->f:Z

    if-nez v0, :cond_0

    .line 1217
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1219
    :cond_0
    monitor-exit p0

    return-void

    .line 1213
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 1264
    iget v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(I)Z
    .locals 3

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/bionics/scanner/unveil/ui/Viewport;->computeNaturalOrientation(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->d:I

    .line 302
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 303
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 304
    iget v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:I

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:I

    if-eq v1, p1, :cond_1

    .line 305
    :cond_0
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    iput v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:I

    .line 306
    iput p1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:I

    .line 307
    const/4 v0, 0x1

    .line 309
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Z

    return v0
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/unveil/camera/CameraManager;Z)Z
    .locals 0

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)Ljava/util/List;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/List;

    return-object v0
.end method

.method private b()V
    .locals 6

    .prologue
    .line 659
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-nez v0, :cond_1

    .line 672
    :cond_0
    :goto_0
    return-void

    .line 663
    :cond_1
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 664
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a()Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    move-result-object v0

    .line 665
    const-string v1, "focus-mode"

    const-string v2, "auto"

    invoke-interface {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 668
    :catch_0
    move-exception v0

    .line 669
    sget-object v1, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Unable to set focus mode to: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "auto"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private b(II)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1367
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a()Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    move-result-object v2

    .line 1369
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-eqz v0, :cond_0

    if-nez v2, :cond_1

    .line 1389
    :cond_0
    :goto_0
    return-void

    .line 1373
    :cond_1
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Lcom/google/bionics/scanner/unveil/util/Size;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Lcom/google/bionics/scanner/unveil/util/Size;

    move-object v1, v0

    .line 1377
    :goto_1
    if-nez v1, :cond_3

    .line 1378
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Could not find a suitable preview size."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1375
    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getOptimalPreviewSize(II)Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 1381
    :cond_3
    sget-object v3, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v4, "Attempting to set optimal preview size %dx%d %s"

    const/4 v0, 0x3

    new-array v5, v0, [Ljava/lang/Object;

    iget v0, v1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v6

    iget v0, v1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v7

    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Lcom/google/bionics/scanner/unveil/util/Size;

    if-nez v0, :cond_4

    const-string v0, ""

    :goto_2
    aput-object v0, v5, v8

    invoke-virtual {v3, v4, v5}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1383
    iget v0, v1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    iget v1, v1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    invoke-interface {v2, v0, v1}, Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;->setPreviewSize(II)V

    .line 1385
    invoke-direct {p0, v2}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;)V

    .line 1387
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getPreviewSize()Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v0

    .line 1388
    sget-object v1, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Set preview size to %dx%d"

    new-array v3, v8, [Ljava/lang/Object;

    iget v4, v0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    iget v0, v0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v7

    invoke-virtual {v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1381
    :cond_4
    const-string v0, "based on forced preview size"

    goto :goto_2
.end method

.method public static synthetic b(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->e()V

    return-void
.end method

.method public static synthetic b(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Z

    return v0
.end method

.method public static synthetic b(Lcom/google/bionics/scanner/unveil/camera/CameraManager;Z)Z
    .locals 0

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Z

    return p1
.end method

.method private c()V
    .locals 2

    .prologue
    .line 866
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a()Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    move-result-object v0

    .line 868
    invoke-interface {v0}, Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;->isSmoothZoomSupported()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->h:Z

    .line 870
    iget-boolean v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->h:Z

    if-eqz v1, :cond_0

    .line 871
    invoke-interface {v0}, Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;->getMaxZoom()I

    move-result v0

    iput v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->g:I

    .line 873
    :cond_0
    return-void
.end method

.method private c(II)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1392
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a()Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    move-result-object v1

    .line 1394
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 1419
    :cond_0
    :goto_0
    return-void

    .line 1399
    :cond_1
    :try_start_0
    invoke-interface {v1}, Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v0

    .line 1400
    if-nez v0, :cond_2

    .line 1401
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "Failed to find picture sizes in camera parameters."

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1411
    :catch_0
    move-exception v0

    .line 1412
    sget-object v2, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v3, "No suitable picture size available, forcing %dx%d"

    new-array v4, v9, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1413
    invoke-interface {v1, p1, p2}, Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;->setPictureSize(II)V

    .line 1415
    :goto_1
    invoke-direct {p0, v1}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;)V

    .line 1417
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getPictureSize()Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v0

    .line 1418
    sget-object v1, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Set picture size to %dx%d"

    new-array v3, v9, [Ljava/lang/Object;

    iget v4, v0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    iget v0, v0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v8

    invoke-virtual {v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1404
    :cond_2
    const/4 v2, 0x0

    const/4 v3, 0x0

    :try_start_1
    invoke-static {v0, p1, p2, v2, v3}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Ljava/util/List;IIZI)Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v0

    .line 1405
    if-nez v0, :cond_3

    .line 1406
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "Could not find a suitable picture size."

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1409
    :cond_3
    sget-object v2, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v3, "Attempting to set optimal picture size %dx%d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, v0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget v6, v0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1410
    iget v2, v0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    iget v0, v0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    invoke-interface {v1, v2, v0}, Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;->setPictureSize(II)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private d()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 884
    iput v4, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->e:I

    .line 886
    iget v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:I

    packed-switch v0, :pswitch_data_0

    .line 900
    :goto_0
    iget v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->d:I

    if-ne v0, v2, :cond_0

    .line 901
    iget v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->e:I

    add-int/lit8 v0, v0, 0x5a

    iput v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->e:I

    .line 902
    iget v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->e:I

    const/16 v1, 0x168

    if-ne v0, v1, :cond_0

    .line 903
    iput v4, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->e:I

    .line 906
    :cond_0
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Rotating camera %d degrees"

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 907
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    iget v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->e:I

    invoke-interface {v0, v1}, Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;->setDisplayOrientation(I)V

    .line 908
    return-void

    .line 888
    :pswitch_0
    iput v4, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->e:I

    goto :goto_0

    .line 891
    :pswitch_1
    const/16 v0, 0x10e

    iput v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->e:I

    goto :goto_0

    .line 894
    :pswitch_2
    const/16 v0, 0xb4

    iput v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->e:I

    goto :goto_0

    .line 897
    :pswitch_3
    const/16 v0, 0x5a

    iput v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->e:I

    goto :goto_0

    .line 886
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private declared-synchronized e()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 948
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:I

    if-ne v0, v1, :cond_0

    .line 949
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Deferring camera release until after focus"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 950
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 954
    :goto_0
    monitor-exit p0

    return-void

    .line 953
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->forceReleaseCamera()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 948
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private f()V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 957
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 979
    :cond_0
    :goto_0
    return-void

    .line 961
    :cond_1
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a()Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    move-result-object v0

    const-string v2, "flash-mode-values"

    invoke-interface {v0, v2}, Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 963
    if-eqz v0, :cond_0

    .line 969
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 970
    array-length v6, v5

    move v4, v3

    move v0, v3

    move v2, v3

    :goto_1
    if-ge v4, v6, :cond_4

    aget-object v7, v5, v4

    .line 971
    const-string v8, "torch"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    move v2, v1

    .line 970
    :cond_2
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 973
    :cond_3
    const-string v8, "off"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    move v0, v1

    .line 974
    goto :goto_2

    .line 978
    :cond_4
    if-eqz v0, :cond_5

    if-eqz v2, :cond_5

    :goto_3
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/lang/Boolean;

    goto :goto_0

    :cond_5
    move v1, v3

    goto :goto_3
.end method

.method private g()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 987
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1005
    :cond_0
    return-void

    .line 991
    :cond_1
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a()Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    move-result-object v0

    const-string v1, "focus-mode-values"

    invoke-interface {v0, v1}, Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 993
    if-eqz v0, :cond_0

    .line 997
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 998
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 999
    const-string v4, "auto"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1000
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Ljava/lang/Boolean;

    .line 998
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1001
    :cond_3
    const-string v4, "continuous-picture"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1002
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Ljava/lang/Boolean;

    goto :goto_1
.end method

.method public static getCameraToDisplayRotation(Landroid/content/Context;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1550
    const-string v0, "window"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 1551
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v2

    .line 1552
    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 1553
    invoke-static {v1, v0}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 1556
    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 1573
    :goto_0
    sget-object v3, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v4, "getDisplayRotation() %d, %d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v1, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-virtual {v3, v4, v5}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1574
    return v0

    .line 1558
    :pswitch_0
    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/lit8 v0, v0, 0x0

    rem-int/lit16 v0, v0, 0x168

    goto :goto_0

    .line 1563
    :pswitch_1
    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/lit16 v0, v0, 0x10e

    rem-int/lit16 v0, v0, 0x168

    goto :goto_0

    .line 1566
    :pswitch_2
    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/lit16 v0, v0, 0xb4

    rem-int/lit16 v0, v0, 0x168

    goto :goto_0

    .line 1569
    :pswitch_3
    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/lit8 v0, v0, 0x5a

    rem-int/lit16 v0, v0, 0x168

    goto :goto_0

    .line 1556
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private declared-synchronized h()V
    .locals 3

    .prologue
    .line 1222
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-nez v0, :cond_1

    .line 1223
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "No camera yet to set parameters on."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1261
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1227
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    if-eqz v0, :cond_0

    .line 1231
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->f:Z

    if-eqz v0, :cond_2

    .line 1233
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "maybeSetPendingCameraParameters() directly called while cache is active."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1239
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    .line 1241
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1242
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Taking picture, aborting setParameters."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1222
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1246
    :cond_3
    :try_start_2
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->isFocusing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1247
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Focusing, aborting setParameters."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1251
    :cond_4
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Setting camera parameters."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1252
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    invoke-interface {v0, v1}, Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;->setParameters(Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;)V

    .line 1253
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    .line 1260
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a()Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method a(Landroid/graphics/Rect;II)Landroid/graphics/Rect;
    .locals 11

    .prologue
    .line 1051
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-nez v0, :cond_0

    .line 1052
    const/4 v0, 0x0

    .line 1126
    :goto_0
    return-object v0

    .line 1055
    :cond_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getPreviewSize()Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v1

    .line 1056
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getPictureSize()Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v3

    .line 1058
    const/4 v0, 0x0

    .line 1059
    if-eqz p1, :cond_1

    .line 1063
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 1064
    iget v2, v1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    int-to-float v2, v2

    int-to-float v4, p2

    div-float/2addr v2, v4

    .line 1065
    iget v4, v1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    int-to-float v4, v4

    int-to-float v5, p3

    div-float/2addr v4, v5

    .line 1066
    iget v5, v0, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    mul-float/2addr v5, v2

    float-to-int v5, v5

    iput v5, v0, Landroid/graphics/Rect;->left:I

    .line 1067
    iget v5, v0, Landroid/graphics/Rect;->right:I

    int-to-float v5, v5

    mul-float/2addr v2, v5

    float-to-int v2, v2

    iput v2, v0, Landroid/graphics/Rect;->right:I

    .line 1068
    iget v2, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    mul-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, v0, Landroid/graphics/Rect;->top:I

    .line 1069
    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    mul-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 1074
    :cond_1
    iget v2, v3, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    .line 1075
    iget v3, v3, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    .line 1076
    iget v4, v1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    .line 1077
    iget v5, v1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    .line 1079
    sget-object v1, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v6, "pictureMajorAxis: %d pictureMinorAxis %d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v1, v6, v7}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1080
    sget-object v1, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v6, "previewMajorAxis: %d previewMinorAxis: %d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v1, v6, v7}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1082
    int-to-float v1, v2

    int-to-float v6, v4

    div-float/2addr v1, v6

    int-to-float v6, v3

    int-to-float v7, v5

    div-float/2addr v6, v7

    invoke-static {v1, v6}, Ljava/lang/Math;->min(FF)F

    move-result v6

    .line 1085
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, v6, v1

    if-lez v1, :cond_3

    .line 1088
    sget-object v1, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v7, "scale up factor: %f"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v1, v7, v8}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1090
    if-nez p1, :cond_2

    .line 1091
    int-to-float v0, v4

    mul-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 1092
    int-to-float v0, v5

    mul-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1118
    :goto_1
    sget-object v4, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v5, "newMajorAxis: %d newMinorAxis: %d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1120
    sub-int/2addr v2, v1

    div-int/lit8 v2, v2, 0x2

    .line 1121
    sub-int/2addr v3, v0

    div-int/lit8 v3, v3, 0x2

    .line 1122
    add-int/2addr v1, v2

    .line 1123
    add-int v4, v3, v0

    .line 1124
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v5, "left: %d top: %d right: %d bottom: %d"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v0, v5, v6}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1126
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v2, v3, v1, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    goto/16 :goto_0

    .line 1095
    :cond_2
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v6

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 1096
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_1

    .line 1101
    :cond_3
    int-to-float v1, v2

    int-to-float v6, v3

    div-float/2addr v1, v6

    .line 1104
    if-nez p1, :cond_4

    .line 1105
    int-to-float v0, v4

    int-to-float v4, v5

    div-float/2addr v0, v4

    .line 1106
    div-float/2addr v0, v1

    .line 1112
    :goto_2
    sget-object v1, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v4, "fix aspect ratio factor: %f"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v1, v4, v5}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1115
    int-to-float v1, v3

    div-float v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    move v1, v2

    goto/16 :goto_1

    .line 1108
    :cond_4
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v4, v0

    .line 1109
    div-float/2addr v0, v1

    goto :goto_2
.end method

.method public declared-synchronized acquireCamera()V
    .locals 4

    .prologue
    .line 712
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "acquireCamera"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 714
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->g:Z

    if-nez v0, :cond_1

    .line 715
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "CameraManager is requesting a camera to manage."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 716
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->g:Z

    .line 719
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 720
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:LbhL;

    const-class v1, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LbhL;->a(Ljava/lang/String;Ljava/util/Map;Landroid/content/res/Resources;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 734
    :goto_0
    monitor-exit p0

    return-void

    .line 722
    :cond_0
    :try_start_1
    new-instance v0, LbhK;

    invoke-direct {v0, p0}, LbhK;-><init>(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)V

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 712
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 729
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-nez v0, :cond_2

    .line 730
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "CameraManager is already waiting for a pending camera request."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 732
    :cond_2
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "CameraManager already has a camera, ignoring call to acquireCamera."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public addPreviewBuffer([B)V
    .locals 4

    .prologue
    .line 1156
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1157
    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;

    if-eqz v0, :cond_0

    .line 1158
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;

    invoke-virtual {v0, p1}, Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;->addPreviewBuffer([B)V

    .line 1162
    :goto_0
    monitor-exit v1

    .line 1163
    return-void

    .line 1160
    :cond_0
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Tried to give buffer to null PreviewFetcher"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1162
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clearCachedCameraParams()V
    .locals 1

    .prologue
    .line 340
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    .line 341
    return-void
.end method

.method public enableContinuousFocus(Z)V
    .locals 6

    .prologue
    .line 675
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-nez v0, :cond_1

    .line 694
    :cond_0
    :goto_0
    return-void

    .line 679
    :cond_1
    if-nez p1, :cond_2

    .line 680
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b()V

    goto :goto_0

    .line 684
    :cond_2
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 685
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a()Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    move-result-object v0

    .line 686
    const-string v1, "focus-mode"

    const-string v2, "continuous-picture"

    .line 687
    invoke-interface {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 690
    :catch_0
    move-exception v0

    .line 691
    sget-object v1, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Unable to set focus mode to: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "continuous-picture"

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public focus(Lcom/google/bionics/scanner/unveil/camera/CameraManager$FocusCallback;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 536
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-nez v0, :cond_0

    .line 600
    :goto_0
    return-void

    .line 540
    :cond_0
    monitor-enter p0

    .line 541
    :try_start_0
    iget v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 542
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Focus just finished and callbacks are being invoked, ignoring focus request!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 544
    monitor-exit p0

    goto :goto_0

    .line 560
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 546
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 547
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "A picture is being taken now, ignoring focus request!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 548
    monitor-exit p0

    goto :goto_0

    .line 553
    :cond_2
    if-eqz p1, :cond_3

    .line 554
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 556
    :cond_3
    iget v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:I

    if-ne v0, v2, :cond_4

    .line 557
    monitor-exit p0

    goto :goto_0

    .line 559
    :cond_4
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:I

    .line 560
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 562
    new-instance v0, LbhJ;

    invoke-direct {v0, p0}, LbhJ;-><init>(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)V

    .line 590
    :try_start_2
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    invoke-interface {v1, v0}, Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 591
    :catch_0
    move-exception v0

    .line 592
    monitor-enter p0

    .line 593
    const/4 v0, 0x0

    :try_start_3
    iput v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:I

    .line 594
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$FocusCallback;

    .line 595
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/google/bionics/scanner/unveil/camera/CameraManager$FocusCallback;->onFocus(Z)V

    goto :goto_1

    .line 598
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 597
    :cond_5
    :try_start_4
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 598
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0
.end method

.method public declared-synchronized forceReleaseCamera()V
    .locals 3

    .prologue
    .line 923
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-eqz v0, :cond_2

    .line 924
    iget v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 925
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Attempting cancelAutoFocus call."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 926
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    invoke-interface {v0}, Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;->cancelAutoFocus()V

    .line 928
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->requestOneFrame(Landroid/hardware/Camera$PreviewCallback;)V

    .line 929
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->d:Z

    if-eqz v0, :cond_1

    .line 930
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    invoke-interface {v0}, Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;->stopPreview()V

    .line 933
    :cond_1
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:LbhL;

    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    invoke-virtual {v0, v1}, LbhL;->a(Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;)V

    .line 934
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    .line 936
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    .line 939
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Z

    .line 940
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->d:Z

    .line 941
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Z

    .line 942
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Z

    .line 943
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/lang/String;

    .line 944
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 945
    monitor-exit p0

    return-void

    .line 923
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getCameraRotation()I
    .locals 1

    .prologue
    .line 915
    iget v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->e:I

    return v0
.end method

.method public getExecutor()Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public getExpectedFlashMode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 701
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 702
    :cond_0
    const-string v0, "unsupported"

    .line 704
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public getOptimalPreviewSize(II)Lcom/google/bionics/scanner/unveil/util/Size;
    .locals 3

    .prologue
    .line 1331
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->acquireCamera()V

    .line 1333
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a()Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    move-result-object v1

    .line 1335
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-eqz v0, :cond_0

    if-nez v1, :cond_2

    .line 1336
    :cond_0
    const/4 v0, 0x0

    .line 1356
    :cond_1
    :goto_0
    return-object v0

    .line 1342
    :cond_2
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v2, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;

    if-ne v0, v2, :cond_3

    .line 1343
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a()Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v0

    .line 1344
    if-nez v0, :cond_1

    .line 1351
    :cond_3
    invoke-interface {v1}, Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v0

    .line 1352
    if-nez v0, :cond_4

    .line 1353
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Failed to find preview sizes in camera parameters."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1356
    :cond_4
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, p1, p2, v1, v2}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Ljava/util/List;IIZI)Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v0

    goto :goto_0
.end method

.method public getPictureQuality()Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;
    .locals 1

    .prologue
    .line 628
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;

    return-object v0
.end method

.method public getPictureSize()Lcom/google/bionics/scanner/unveil/util/Size;
    .locals 1

    .prologue
    .line 1028
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a()Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    move-result-object v0

    .line 1029
    if-nez v0, :cond_0

    .line 1030
    const/4 v0, 0x0

    .line 1032
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;->getPictureSize()Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v0

    goto :goto_0
.end method

.method public getPreviewSize()Lcom/google/bionics/scanner/unveil/util/Size;
    .locals 1

    .prologue
    .line 1016
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Lcom/google/bionics/scanner/unveil/util/Size;

    if-eqz v0, :cond_0

    .line 1017
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Lcom/google/bionics/scanner/unveil/util/Size;

    .line 1024
    :goto_0
    return-object v0

    .line 1020
    :cond_0
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a()Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    move-result-object v0

    .line 1021
    if-nez v0, :cond_1

    .line 1022
    const/4 v0, 0x0

    goto :goto_0

    .line 1024
    :cond_1
    invoke-interface {v0}, Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;->getPreviewSize()Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v0

    goto :goto_0
.end method

.method public getStateName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1477
    iget v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:I

    packed-switch v0, :pswitch_data_0

    .line 1494
    const-string v0, "unknown"

    :goto_0
    return-object v0

    .line 1479
    :pswitch_0
    const-string v0, "FOCUSED"

    goto :goto_0

    .line 1482
    :pswitch_1
    const-string v0, "FOCUSING"

    goto :goto_0

    .line 1485
    :pswitch_2
    const-string v0, "IDLE"

    goto :goto_0

    .line 1488
    :pswitch_3
    const-string v0, "SNAPPED"

    goto :goto_0

    .line 1491
    :pswitch_4
    const-string v0, "SNAPPING"

    goto :goto_0

    .line 1477
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public initializeSnapshotQuality(Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;)V
    .locals 0

    .prologue
    .line 270
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;

    .line 271
    return-void
.end method

.method public declared-synchronized isCameraConnected()Z
    .locals 1

    .prologue
    .line 1536
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isContinuousFocusSupported()Z
    .locals 1

    .prologue
    .line 1012
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public isFlashSupported()Z
    .locals 1

    .prologue
    .line 708
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public isFocusSupported()Z
    .locals 1

    .prologue
    .line 1008
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public isFocusing()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1268
    iget v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPreviewing()Z
    .locals 1

    .prologue
    .line 737
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->d:Z

    return v0
.end method

.method public declared-synchronized onCameraAcquired(Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;)V
    .locals 3

    .prologue
    .line 1512
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "onCameraAcquired"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1513
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->g:Z

    .line 1514
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    .line 1516
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$Listener;

    .line 1517
    invoke-interface {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager$Listener;->onCameraConnected()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1512
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1520
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->h()V

    .line 1522
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Z

    if-eqz v0, :cond_1

    .line 1523
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Executing pending start preview.."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1524
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->startPreview()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1526
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized onCameraAcquisitionError()V
    .locals 2

    .prologue
    .line 1500
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->g:Z

    .line 1505
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$Listener;

    .line 1506
    invoke-interface {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager$Listener;->onCameraAcquisitionError()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1500
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1508
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 281
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 282
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->d:Z

    if-eqz v0, :cond_0

    .line 284
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->stopPreview()V

    .line 285
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->d()V

    .line 286
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->startPreview()V

    .line 289
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 382
    invoke-super/range {p0 .. p5}, Landroid/view/SurfaceView;->onLayout(ZIIII)V

    .line 384
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "onLayout: %b, %d, %d, %d, %d"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 385
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$Listener;

    .line 386
    invoke-interface {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager$Listener;->onCameraLayoutComplete()V

    goto :goto_0

    .line 388
    :cond_0
    return-void
.end method

.method public declared-synchronized onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 3

    .prologue
    .line 481
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 483
    if-nez p2, :cond_1

    .line 500
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 487
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 488
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/bionics/scanner/unveil/util/PictureFactory;->createJpeg([BI)Lcom/google/bionics/scanner/unveil/util/Picture;

    move-result-object v1

    .line 489
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureCallback;

    .line 490
    invoke-interface {v0, v1}, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureCallback;->onPictureTaken(Lcom/google/bionics/scanner/unveil/util/Picture;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 481
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 492
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 495
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:I

    .line 497
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Z

    if-eqz v0, :cond_0

    .line 498
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->startPreview()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 5

    .prologue
    .line 392
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->onWindowVisibilityChanged(I)V

    .line 393
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "onWindowVisibilityChanged: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 394
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->e:Z

    if-eqz v0, :cond_0

    .line 395
    if-nez p1, :cond_1

    .line 396
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->acquireCamera()V

    .line 401
    :cond_0
    :goto_0
    return-void

    .line 398
    :cond_1
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->e()V

    goto :goto_0
.end method

.method public registerListener(Lcom/google/bionics/scanner/unveil/camera/CameraManager$Listener;)V
    .locals 1

    .prologue
    .line 1166
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1167
    return-void
.end method

.method public requestOneFrame(Landroid/hardware/Camera$PreviewCallback;)V
    .locals 4

    .prologue
    .line 1130
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-nez v0, :cond_0

    .line 1131
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "No camera, can\'t comply with frame request."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1152
    :goto_0
    return-void

    .line 1135
    :cond_0
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1136
    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;

    iget-object v0, v0, Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;->a:Landroid/hardware/Camera$PreviewCallback;

    if-eq v0, p1, :cond_4

    .line 1138
    :cond_1
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;

    if-eqz v0, :cond_2

    .line 1139
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;->c()V

    .line 1140
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;

    .line 1143
    :cond_2
    if-nez p1, :cond_3

    .line 1144
    monitor-exit v1

    goto :goto_0

    .line 1150
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1147
    :cond_3
    :try_start_1
    new-instance v0, Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;

    iget-object v2, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getPreviewSize()Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v3

    invoke-direct {v0, v2, p1, v3}, Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;-><init>(Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;Landroid/hardware/Camera$PreviewCallback;Lcom/google/bionics/scanner/unveil/util/Size;)V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;

    .line 1148
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;->a()V

    .line 1150
    :cond_4
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1151
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;->b()V

    goto :goto_0
.end method

.method public setAcquireCameraOnVisibilityChange(Z)V
    .locals 0

    .prologue
    .line 412
    iput-boolean p1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->e:Z

    .line 413
    return-void
.end method

.method public setCameraProxy(Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;)V
    .locals 2

    .prologue
    .line 327
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    .line 329
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    .line 331
    const/16 v0, 0x280

    const/16 v1, 0x1e0

    :try_start_0
    invoke-direct {p0, v0, v1}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 334
    :goto_0
    return-void

    .line 332
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setFlashMode(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 632
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    const-string v0, "off"

    .line 633
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "torch"

    .line 634
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 656
    :cond_0
    :goto_0
    return-void

    .line 637
    :cond_1
    iget v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:I

    if-ne v0, v3, :cond_2

    .line 638
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Deferring flash setting until focus complete."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 639
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/lang/String;

    goto :goto_0

    .line 643
    :cond_2
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 644
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Ljava/lang/String;

    .line 645
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a()Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    move-result-object v0

    .line 646
    const-string v1, "flash-mode"

    invoke-interface {v0, v1, p1}, Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 649
    :catch_0
    move-exception v0

    .line 650
    sget-object v1, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Unable to set flash mode to: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 651
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$Listener;

    .line 652
    invoke-interface {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager$Listener;->onCameraFlashControlError()V

    goto :goto_1
.end method

.method public setForcedPreviewSize(Lcom/google/bionics/scanner/unveil/util/Size;)V
    .locals 0

    .prologue
    .line 1532
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Lcom/google/bionics/scanner/unveil/util/Size;

    .line 1533
    return-void
.end method

.method public declared-synchronized setFullScreenDisplaySize(Lcom/google/bionics/scanner/unveil/util/Size;)V
    .locals 1

    .prologue
    .line 319
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Size;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 320
    monitor-exit p0

    return-void

    .line 319
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setPictureQuality(Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;)V
    .locals 5

    .prologue
    .line 603
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-nez v0, :cond_0

    .line 625
    :goto_0
    return-void

    .line 607
    :cond_0
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a()Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;

    move-result-object v0

    .line 612
    const-string v1, "jpeg-quality"

    iget v2, p1, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;->jpegQuality:I

    invoke-interface {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;->set(Ljava/lang/String;I)V

    .line 615
    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;)V

    .line 616
    iget v0, p1, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;->desiredWidth:I

    iget v1, p1, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;->desiredHeight:I

    invoke-direct {p0, v0, v1}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c(II)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 624
    :cond_1
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;

    goto :goto_0

    .line 617
    :catch_0
    move-exception v0

    .line 618
    sget-object v1, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Unable to set quality to: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 619
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$Listener;

    .line 620
    invoke-interface {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager$Listener;->onCameraQualityError()V

    goto :goto_1
.end method

.method public declared-synchronized setZoomLevel(I)V
    .locals 5

    .prologue
    .line 503
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->isPreviewing()Z

    move-result v0

    if-nez v0, :cond_2

    .line 504
    :cond_0
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Too early to zoom!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 523
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 508
    :cond_2
    :try_start_1
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->h:Z

    if-nez v0, :cond_3

    .line 509
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Zooming not supported!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 503
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 513
    :cond_3
    :try_start_2
    iget v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->g:I

    if-le p1, v0, :cond_4

    .line 514
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Zoom is too great! %d requested, max is %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->g:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 518
    :cond_4
    iget v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->f:I

    if-eq p1, v0, :cond_1

    .line 519
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Setting zoom level to %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 520
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    invoke-interface {v0, p1}, Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;->startSmoothZoom(I)V

    .line 521
    iput p1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->f:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized startPreview()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 741
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Starting preview!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 743
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->acquireCamera()V

    .line 744
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-nez v0, :cond_2

    .line 745
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->g:Z

    if-eqz v0, :cond_1

    .line 746
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Deferring startPreview due to acquisitionPending."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 747
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 802
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 749
    :cond_1
    :try_start_1
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Failed to acquire camera, can\'t start preview"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 741
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 754
    :cond_2
    :try_start_2
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 755
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Deferring startPreview due to picture taking."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 756
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Z

    goto :goto_0

    .line 758
    :cond_3
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->isFocusing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 759
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Deferring startPreview due to focusing."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 760
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Z

    goto :goto_0

    .line 764
    :cond_4
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->d:Z

    if-eqz v0, :cond_5

    .line 765
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Already previewing."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 766
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Z

    goto :goto_0

    .line 772
    :cond_5
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    if-nez v0, :cond_7

    .line 773
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getWidth()I

    move-result v1

    .line 774
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getHeight()I

    move-result v0

    .line 775
    sget-object v2, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v3, "Full display size is null, falling back to CameraManager view size of %dx%d."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 776
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 775
    invoke-virtual {v2, v3, v4}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 783
    :goto_1
    if-eqz v1, :cond_6

    if-nez v0, :cond_8

    .line 784
    :cond_6
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Too early to preview, no device size or CameraManager View size known yet."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 785
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Z

    goto :goto_0

    .line 778
    :cond_7
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    iget v1, v0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    .line 779
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    iget v0, v0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    .line 780
    sget-object v2, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v3, "Using full display size of %dx%d to pick preview size."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 790
    :cond_8
    :try_start_3
    iget v2, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:I

    if-ne v2, v7, :cond_9

    .line 791
    invoke-direct {p0, v0, v1}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(II)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 795
    :catch_0
    move-exception v0

    .line 796
    :try_start_4
    sget-object v1, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Unable to acquire/configure camera hardware."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 797
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->e()V

    .line 798
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$Listener;

    .line 799
    invoke-interface {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager$Listener;->onCameraAcquisitionError()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 793
    :cond_9
    :try_start_5
    invoke-direct {p0, v1, v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(II)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0
.end method

.method public declared-synchronized stopPreview()V
    .locals 3

    .prologue
    .line 805
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-nez v0, :cond_0

    .line 806
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Can\'t stop preview on a null camera."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 816
    :goto_0
    monitor-exit p0

    return-void

    .line 809
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    invoke-interface {v0}, Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;->stopPreview()V

    .line 810
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Z

    .line 811
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->d:Z

    .line 812
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Z

    .line 813
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Z

    .line 814
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/lang/String;

    .line 815
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 805
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 356
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "surfaceChanged: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p3, p4}, Lcom/google/bionics/scanner/unveil/util/Size;->dimensionsAsString(II)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 357
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 358
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->acquireCamera()V

    .line 360
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-nez v0, :cond_0

    .line 361
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Failed to acquire camera before setting preview display"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 377
    :goto_0
    return-void

    .line 366
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 367
    :catch_0
    move-exception v0

    .line 368
    sget-object v1, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Failed to set preview display"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 375
    :cond_1
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->e()V

    goto :goto_0
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0

    .prologue
    .line 346
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0

    .prologue
    .line 350
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->e()V

    .line 351
    return-void
.end method

.method public takePicture(Lcom/google/bionics/scanner/unveil/camera/CameraManager$ShutterCallback;Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureCallback;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x3

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 428
    invoke-virtual {p0, v4}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->requestOneFrame(Landroid/hardware/Camera$PreviewCallback;)V

    .line 430
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    if-nez v0, :cond_0

    .line 474
    :goto_0
    return-void

    .line 434
    :cond_0
    monitor-enter p0

    .line 435
    :try_start_0
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 436
    monitor-exit p0

    goto :goto_0

    .line 454
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 440
    :cond_1
    if-eqz p1, :cond_2

    .line 441
    :try_start_1
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 443
    :cond_2
    if-eqz p2, :cond_3

    .line 444
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 446
    :cond_3
    iget v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:I

    if-ne v0, v2, :cond_4

    .line 447
    monitor-exit p0

    goto :goto_0

    .line 449
    :cond_4
    iget v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:I

    if-eq v0, v1, :cond_5

    iget v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    .line 450
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Z

    .line 451
    monitor-exit p0

    goto :goto_0

    .line 453
    :cond_6
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:I

    .line 454
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 456
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Before taking picture via hardware camera."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->time(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 458
    iput-boolean v3, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->d:Z

    .line 459
    new-instance v0, LbhI;

    invoke-direct {v0, p0}, LbhI;-><init>(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)V

    .line 472
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    invoke-interface {v1, v0, v4, p0}, Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;->takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V

    .line 473
    sget-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "After taking picture via hardware camera."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->time(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public transformToPictureCoordinates(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 1045
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->getHeight()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a(Landroid/graphics/Rect;II)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public unregisterListener(Lcom/google/bionics/scanner/unveil/camera/CameraManager$Listener;)V
    .locals 1

    .prologue
    .line 1171
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1172
    return-void
.end method
