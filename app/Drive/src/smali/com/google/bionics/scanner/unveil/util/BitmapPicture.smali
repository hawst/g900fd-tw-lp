.class public Lcom/google/bionics/scanner/unveil/util/BitmapPicture;
.super Lcom/google/bionics/scanner/unveil/util/Picture;
.source "BitmapPicture.java"


# static fields
.field private static final a:Lcom/google/bionics/scanner/unveil/util/Logger;


# instance fields
.field private a:Landroid/graphics/Bitmap;

.field private a:Landroid/graphics/drawable/BitmapDrawable;

.field private final a:Lcom/google/bionics/scanner/unveil/util/Size;

.field private volatile a:Z

.field private a:[B

.field private b:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>()V

    sput-object v0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;I)V
    .locals 1

    .prologue
    .line 77
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;-><init>(Landroid/graphics/Bitmap;II)V

    .line 78
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;II)V
    .locals 5

    .prologue
    .line 81
    invoke-direct {p0, p2, p3}, Lcom/google/bionics/scanner/unveil/util/Picture;-><init>(II)V

    .line 83
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    if-eq v0, v1, :cond_0

    .line 84
    sget-object v0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Bitmap was %s instead of the required RGB_565!"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    :cond_0
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Size;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Size;-><init>(II)V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    .line 89
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Landroid/graphics/Bitmap;

    .line 90
    return-void
.end method

.method public constructor <init>([BIII)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0, p4}, Lcom/google/bionics/scanner/unveil/util/Picture;-><init>(I)V

    .line 42
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Size;

    invoke-direct {v0, p2, p3}, Lcom/google/bionics/scanner/unveil/util/Size;-><init>(II)V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    .line 46
    invoke-virtual {p1}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:[B

    .line 50
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, LbhW;

    invoke-direct {v1, p0, p2, p3}, LbhW;-><init>(Lcom/google/bionics/scanner/unveil/util/BitmapPicture;II)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 72
    const-string v1, "BitmapPicture processing thread to convert YUV420 to RGB565."

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 73
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 74
    return-void
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/unveil/util/BitmapPicture;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/unveil/util/BitmapPicture;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 14
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method public static synthetic a()Lcom/google/bionics/scanner/unveil/util/Logger;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/unveil/util/BitmapPicture;Z)Z
    .locals 0

    .prologue
    .line 14
    iput-boolean p1, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Z

    return p1
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/unveil/util/BitmapPicture;)[B
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:[B

    return-object v0
.end method


# virtual methods
.method public getByteSize()I
    .locals 2

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a()V

    .line 168
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    iget v0, v0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    iget v1, v1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    mul-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x4

    return v0
.end method

.method public declared-synchronized getCroppedPicture()Lcom/google/bionics/scanner/unveil/util/Picture;
    .locals 5

    .prologue
    .line 130
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->getCropArea()Landroid/graphics/Rect;

    move-result-object v0

    .line 131
    if-eqz v0, :cond_1

    .line 132
    sget-object v1, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Getting cropped picture!"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 134
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->getSize()Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v1

    .line 135
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget v3, v1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    if-ne v2, v3, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    iget v1, v1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    if-eq v2, v1, :cond_1

    .line 137
    :cond_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->peekBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v3, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 136
    invoke-static {v1, v2, v3, v4, v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 138
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;

    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->getOrientation()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;-><init>(Landroid/graphics/Bitmap;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDrawable()Landroid/graphics/drawable/BitmapDrawable;
    .locals 2

    .prologue
    .line 121
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a()V

    .line 122
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Landroid/graphics/drawable/BitmapDrawable;

    if-nez v0, :cond_0

    .line 123
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->peekBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Landroid/graphics/drawable/BitmapDrawable;

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Landroid/graphics/drawable/BitmapDrawable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getJpegData()[B
    .locals 1

    .prologue
    .line 116
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSize()Lcom/google/bionics/scanner/unveil/util/Size;
    .locals 1

    .prologue
    .line 94
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a()V

    .line 95
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Lcom/google/bionics/scanner/unveil/util/Size;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getYuvData()[B
    .locals 4

    .prologue
    .line 100
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a()V

    .line 101
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:[B

    if-nez v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    iget v0, v0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    iget v1, v1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    invoke-static {v0, v1}, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->getYUVByteSize(II)I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:[B

    .line 105
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->peekBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 106
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    iget v1, v1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    iget-object v2, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    iget v2, v2, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    mul-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x2

    new-array v1, v1, [B

    .line 107
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 108
    invoke-virtual {v0, v2}, Landroid/graphics/Bitmap;->copyPixelsToBuffer(Ljava/nio/Buffer;)V

    .line 109
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:[B

    iget-object v2, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    iget v2, v2, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    iget-object v3, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    iget v3, v3, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    invoke-static {v1, v0, v2, v3}, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->convertRGB565ToYUV420SP([B[BII)V

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized peekBitmap()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 147
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a()V

    .line 148
    :goto_0
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 150
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 151
    :catch_0
    move-exception v0

    .line 152
    :try_start_2
    sget-object v1, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Exception!"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 147
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 155
    :cond_0
    :try_start_3
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Landroid/graphics/Bitmap;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized peekBitmap(Landroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 162
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->peekBitmap()Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized recycle()V
    .locals 3

    .prologue
    .line 173
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    sget-object v0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Requested recycling, but bitmap picture is already recycled."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    :goto_0
    monitor-exit p0

    return-void

    .line 177
    :cond_0
    :try_start_1
    invoke-super {p0}, Lcom/google/bionics/scanner/unveil/util/Picture;->recycle()V

    .line 178
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 179
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 181
    :cond_1
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_2

    .line 182
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 184
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:[B

    .line 185
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Landroid/graphics/Bitmap;

    .line 186
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->a:Landroid/graphics/drawable/BitmapDrawable;

    .line 187
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;->b:[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 173
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
