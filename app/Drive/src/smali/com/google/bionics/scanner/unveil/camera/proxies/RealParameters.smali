.class public Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;
.super Ljava/lang/Object;
.source "RealParameters.java"

# interfaces
.implements Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;


# instance fields
.field private final a:Landroid/hardware/Camera$Parameters;


# direct methods
.method public constructor <init>(Landroid/hardware/Camera$Parameters;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a:Landroid/hardware/Camera$Parameters;

    .line 25
    return-void
.end method

.method private a(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/scanner/unveil/util/Size;",
            ">;"
        }
    .end annotation

    .prologue
    .line 142
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 143
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 144
    new-instance v3, Lcom/google/bionics/scanner/unveil/util/Size;

    iget v4, v0, Landroid/hardware/Camera$Size;->width:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-direct {v3, v4, v0}, Lcom/google/bionics/scanner/unveil/util/Size;-><init>(II)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 146
    :cond_0
    return-object v1
.end method


# virtual methods
.method public flatten()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->flatten()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera$Parameters;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getActualParameters()Landroid/hardware/Camera$Parameters;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a:Landroid/hardware/Camera$Parameters;

    return-object v0
.end method

.method public getInt(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera$Parameters;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getMaxZoom()I
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getMaxZoom()I

    move-result v0

    return v0
.end method

.method public getPictureFormat()I
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getPictureFormat()I

    move-result v0

    return v0
.end method

.method public getPictureSize()Lcom/google/bionics/scanner/unveil/util/Size;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Size;

    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getPictureSize()Landroid/hardware/Camera$Size;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/bionics/scanner/unveil/util/Size;-><init>(Landroid/hardware/Camera$Size;)V

    return-object v0
.end method

.method public getPreviewFormat()I
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getPreviewFormat()I

    move-result v0

    return v0
.end method

.method public getPreviewFrameRate()I
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getPreviewFrameRate()I

    move-result v0

    return v0
.end method

.method public getPreviewSize()Lcom/google/bionics/scanner/unveil/util/Size;
    .locals 2

    .prologue
    .line 73
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Size;

    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/bionics/scanner/unveil/util/Size;-><init>(Landroid/hardware/Camera$Size;)V

    return-object v0
.end method

.method public getSupportedPictureSizes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/scanner/unveil/util/Size;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSupportedPreviewSizes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/scanner/unveil/util/Size;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public isSmoothZoomSupported()Z
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->isSmoothZoomSupported()Z

    move-result v0

    return v0
.end method

.method public remove(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera$Parameters;->remove(Ljava/lang/String;)V

    .line 89
    return-void
.end method

.method public set(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, p1, p2}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    .line 94
    return-void
.end method

.method public set(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, p1, p2}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    return-void
.end method

.method public setPictureFormat(I)V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera$Parameters;->setPictureFormat(I)V

    .line 104
    return-void
.end method

.method public setPictureSize(II)V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, p1, p2}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    .line 109
    return-void
.end method

.method public setPreviewFormat(I)V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera$Parameters;->setPreviewFormat(I)V

    .line 114
    return-void
.end method

.method public setPreviewFrameRate(I)V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera$Parameters;->setPreviewFrameRate(I)V

    .line 119
    return-void
.end method

.method public setPreviewSize(II)V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, p1, p2}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 124
    return-void
.end method

.method public unflatten(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;->a:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera$Parameters;->unflatten(Ljava/lang/String;)V

    .line 139
    return-void
.end method
