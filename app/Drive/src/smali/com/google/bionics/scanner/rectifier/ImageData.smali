.class public Lcom/google/bionics/scanner/rectifier/ImageData;
.super Ljava/lang/Object;
.source "ImageData.java"


# instance fields
.field public final data:[B

.field public final enhancementMethod:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

.field public final format:I

.field public final height:I

.field public final width:I


# direct methods
.method public constructor <init>([BIII)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/bionics/scanner/rectifier/ImageData;->data:[B

    .line 30
    iput p2, p0, Lcom/google/bionics/scanner/rectifier/ImageData;->format:I

    .line 31
    iput p3, p0, Lcom/google/bionics/scanner/rectifier/ImageData;->width:I

    .line 32
    iput p4, p0, Lcom/google/bionics/scanner/rectifier/ImageData;->height:I

    .line 33
    sget-object v0, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->NONE:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/ImageData;->enhancementMethod:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    .line 34
    return-void
.end method

.method public constructor <init>([BIIILcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/google/bionics/scanner/rectifier/ImageData;->data:[B

    .line 49
    iput p2, p0, Lcom/google/bionics/scanner/rectifier/ImageData;->format:I

    .line 50
    iput p3, p0, Lcom/google/bionics/scanner/rectifier/ImageData;->width:I

    .line 51
    iput p4, p0, Lcom/google/bionics/scanner/rectifier/ImageData;->height:I

    .line 52
    iput-object p5, p0, Lcom/google/bionics/scanner/rectifier/ImageData;->enhancementMethod:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    .line 53
    return-void
.end method
