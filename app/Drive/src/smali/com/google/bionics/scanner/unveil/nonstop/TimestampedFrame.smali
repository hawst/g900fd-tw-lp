.class public Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;
.super Ljava/lang/Object;
.source "TimestampedFrame.java"


# static fields
.field public static final NOT_SET:I = -0x1

.field private static final a:Lcom/google/bionics/scanner/unveil/util/Logger;


# instance fields
.field private a:I

.field private final a:J

.field private a:Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;

.field private final a:Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$BufferSink;

.field private final a:Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;

.field private a:Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;

.field private final a:Lcom/google/bionics/scanner/unveil/util/Size;

.field private a:Ljava/lang/Boolean;

.field private a:[B

.field private a:[I

.field private final b:I

.field private b:Ljava/lang/Boolean;

.field private b:[I

.field private final c:I

.field private final d:I

.field private e:I

.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 21
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "TimestampedFrame"

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-void
.end method

.method public constructor <init>([BLcom/google/bionics/scanner/unveil/util/Size;IJILcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$BufferSink;)V
    .locals 2

    .prologue
    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 190
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:[B

    .line 192
    iput p3, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->b:I

    .line 193
    iput-wide p4, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:J

    .line 195
    iput-object p2, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    .line 196
    iget v0, p2, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    iput v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->c:I

    .line 197
    iget v0, p2, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    iput v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->d:I

    .line 198
    iput p6, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->f:I

    .line 200
    iput-object p7, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$BufferSink;

    .line 202
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:I

    .line 203
    new-instance v0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;

    invoke-direct {v0, p4, p5}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;-><init>(J)V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;

    .line 204
    return-void
.end method

.method static synthetic a()Lcom/google/bionics/scanner/unveil/util/Logger;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-object v0
.end method

.method private a()V
    .locals 6

    .prologue
    .line 258
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->getRawData()[B

    move-result-object v0

    .line 259
    iget v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->c:I

    iget v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->d:I

    iget-wide v4, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:J

    invoke-static {v0, v1, v2, v4, v5}, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->downsample([BIIJ)Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;

    .line 260
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->addReference()V

    .line 261
    return-void
.end method

.method private declared-synchronized a()[B
    .locals 2

    .prologue
    .line 301
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->getRawData()[B

    move-result-object v0

    .line 303
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:[B

    .line 306
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;

    if-eqz v1, :cond_0

    .line 307
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;

    invoke-virtual {v1}, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->removeReference()V

    .line 308
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 310
    :cond_0
    monitor-exit p0

    return-object v0

    .line 301
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b()V
    .locals 4

    .prologue
    .line 388
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->b:[I

    if-nez v0, :cond_0

    .line 389
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->getDownsampledImage()Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->getBytes()[B

    move-result-object v0

    .line 390
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->getDownsampledWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->getDownsampledHeight()I

    move-result v2

    .line 389
    invoke-static {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->getBucketDistributionNative([BII)[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->b:[I

    .line 392
    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:[I

    if-nez v0, :cond_1

    .line 393
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->getDownsampledImage()Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->getBytes()[B

    move-result-object v0

    .line 394
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->getDownsampledWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->getDownsampledHeight()I

    move-result v2

    const/4 v3, 0x0

    .line 393
    invoke-static {v0, v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->computeSignatureNative([BII[I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:[I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 396
    :cond_1
    monitor-exit p0

    return-void

    .line 388
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static diffSignature([I[I)I
    .locals 1

    .prologue
    .line 408
    invoke-static {p0, p1}, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->diffSignatureNative([I[I)I

    move-result v0

    return v0
.end method


# virtual methods
.method protected declared-synchronized a()Z
    .locals 1

    .prologue
    .line 281
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized a()[I
    .locals 1

    .prologue
    .line 412
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->b()V

    .line 413
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->b:[I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 412
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized addReference()V
    .locals 1

    .prologue
    .line 350
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 351
    monitor-exit p0

    return-void

    .line 350
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized diffSignature([I)I
    .locals 1

    .prologue
    .line 404
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->getSignature()[I

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->diffSignature([I[I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getDownsampleFactor()I
    .locals 2

    .prologue
    .line 274
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->c:I

    iget v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->d:I

    invoke-static {v0, v1}, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->getDownsampleFactor(II)I

    move-result v0

    return v0
.end method

.method public getDownsampledHeight()I
    .locals 3

    .prologue
    .line 269
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->d:I

    iget v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->c:I

    iget v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->d:I

    .line 270
    invoke-static {v1, v2}, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->getDownsampleFactor(II)I

    move-result v1

    .line 269
    invoke-static {v0, v1}, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->getDownsampledHeight(II)I

    move-result v0

    return v0
.end method

.method public declared-synchronized getDownsampledImage()Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;
    .locals 3

    .prologue
    .line 244
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;

    if-nez v0, :cond_0

    .line 245
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Frame data for frame "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is no longer available."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 244
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 250
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;

    if-nez v0, :cond_1

    .line 251
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a()V

    .line 254
    :cond_1
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public getDownsampledWidth()I
    .locals 3

    .prologue
    .line 264
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->c:I

    iget v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->c:I

    iget v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->d:I

    .line 265
    invoke-static {v1, v2}, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->getDownsampleFactor(II)I

    move-result v1

    .line 264
    invoke-static {v0, v1}, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->getDownsampledWidth(II)I

    move-result v0

    return v0
.end method

.method public getFrameNum()I
    .locals 1

    .prologue
    .line 215
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->b:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 231
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->d:I

    return v0
.end method

.method public declared-synchronized getImageStatistics()Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;
    .locals 3

    .prologue
    .line 314
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;

    if-nez v0, :cond_0

    .line 315
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->c:I

    iget v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->d:I

    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->getRawData()[B

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->computeImageStatistics(II[B)Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;

    .line 317
    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 314
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getMetadata()Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$Metadata;

    return-object v0
.end method

.method public declared-synchronized getOpticalFlowDelta()I
    .locals 1

    .prologue
    .line 207
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getRawData()[B
    .locals 3

    .prologue
    .line 289
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 290
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Frame data for frame "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is no longer available."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 289
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 292
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public getRotationAngle()I
    .locals 1

    .prologue
    .line 235
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->f:I

    return v0
.end method

.method public declared-synchronized getSignature()[I
    .locals 1

    .prologue
    .line 399
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->b()V

    .line 400
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:[I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 399
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getSize()Lcom/google/bionics/scanner/unveil/util/Size;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    return-object v0
.end method

.method public getTimestamp()J
    .locals 2

    .prologue
    .line 219
    iget-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:J

    return-wide v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 227
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->c:I

    return v0
.end method

.method public declared-synchronized isBlurred()Z
    .locals 3

    .prologue
    .line 329
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 330
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->getRawData()[B

    move-result-object v0

    iget v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->c:I

    iget v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->d:I

    invoke-static {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->isBlurredNative([BII)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:Ljava/lang/Boolean;

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 329
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public recommendedQuality()I
    .locals 2

    .prologue
    .line 321
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->c:I

    iget v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->d:I

    invoke-static {v0, v1}, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->getJpegQualityRecommendation(II)I

    move-result v0

    return v0
.end method

.method public declared-synchronized removeReference()V
    .locals 2

    .prologue
    .line 358
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:I

    .line 360
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:I

    if-nez v0, :cond_1

    .line 362
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a()[B

    move-result-object v0

    .line 366
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$BufferSink;

    if-eqz v1, :cond_0

    .line 367
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$BufferSink;

    invoke-interface {v1, v0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame$BufferSink;->returnBuffer([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 372
    :cond_0
    monitor-exit p0

    return-void

    .line 369
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a:I

    if-gez v0, :cond_0

    .line 370
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Negative reference count."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 358
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setOpticalFlowDelta(I)V
    .locals 1

    .prologue
    .line 211
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212
    monitor-exit p0

    return-void

    .line 211
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setTakenWhileFocusing(Z)V
    .locals 1

    .prologue
    .line 336
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->b:Ljava/lang/Boolean;

    .line 337
    return-void
.end method

.method public takenWhileFocusing()Z
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 341
    const/4 v0, 0x0

    .line 343
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method
