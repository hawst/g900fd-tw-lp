.class public final Lcom/google/bionics/scanner/docscanner/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final abc_ab_share_pack_holo_dark:I = 0x7f020000

.field public static final abc_ab_share_pack_holo_light:I = 0x7f020001

.field public static final abc_btn_check_material:I = 0x7f020002

.field public static final abc_btn_check_to_on_mtrl_000:I = 0x7f020003

.field public static final abc_btn_check_to_on_mtrl_015:I = 0x7f020004

.field public static final abc_btn_radio_material:I = 0x7f020005

.field public static final abc_btn_radio_to_on_mtrl_000:I = 0x7f020006

.field public static final abc_btn_radio_to_on_mtrl_015:I = 0x7f020007

.field public static final abc_btn_switch_to_on_mtrl_00001:I = 0x7f020008

.field public static final abc_btn_switch_to_on_mtrl_00012:I = 0x7f020009

.field public static final abc_cab_background_internal_bg:I = 0x7f02000a

.field public static final abc_cab_background_top_material:I = 0x7f02000b

.field public static final abc_cab_background_top_mtrl_alpha:I = 0x7f02000c

.field public static final abc_edit_text_material:I = 0x7f02000d

.field public static final abc_ic_ab_back_mtrl_am_alpha:I = 0x7f02000e

.field public static final abc_ic_clear_mtrl_alpha:I = 0x7f02000f

.field public static final abc_ic_commit_search_api_mtrl_alpha:I = 0x7f020010

.field public static final abc_ic_go_search_api_mtrl_alpha:I = 0x7f020011

.field public static final abc_ic_menu_copy_mtrl_am_alpha:I = 0x7f020012

.field public static final abc_ic_menu_cut_mtrl_alpha:I = 0x7f020013

.field public static final abc_ic_menu_moreoverflow_mtrl_alpha:I = 0x7f020014

.field public static final abc_ic_menu_paste_mtrl_am_alpha:I = 0x7f020015

.field public static final abc_ic_menu_selectall_mtrl_alpha:I = 0x7f020016

.field public static final abc_ic_menu_share_mtrl_alpha:I = 0x7f020017

.field public static final abc_ic_search_api_mtrl_alpha:I = 0x7f020018

.field public static final abc_ic_voice_search_api_mtrl_alpha:I = 0x7f020019

.field public static final abc_item_background_holo_dark:I = 0x7f02001a

.field public static final abc_item_background_holo_light:I = 0x7f02001b

.field public static final abc_list_divider_mtrl_alpha:I = 0x7f02001c

.field public static final abc_list_focused_holo:I = 0x7f02001d

.field public static final abc_list_longpressed_holo:I = 0x7f02001e

.field public static final abc_list_pressed_holo_dark:I = 0x7f02001f

.field public static final abc_list_pressed_holo_light:I = 0x7f020020

.field public static final abc_list_selector_background_transition_holo_dark:I = 0x7f020021

.field public static final abc_list_selector_background_transition_holo_light:I = 0x7f020022

.field public static final abc_list_selector_disabled_holo_dark:I = 0x7f020023

.field public static final abc_list_selector_disabled_holo_light:I = 0x7f020024

.field public static final abc_list_selector_holo_dark:I = 0x7f020025

.field public static final abc_list_selector_holo_light:I = 0x7f020026

.field public static final abc_menu_hardkey_panel_mtrl_mult:I = 0x7f020027

.field public static final abc_popup_background_mtrl_mult:I = 0x7f020028

.field public static final abc_spinner_mtrl_am_alpha:I = 0x7f020029

.field public static final abc_switch_thumb_material:I = 0x7f02002a

.field public static final abc_switch_track_mtrl_alpha:I = 0x7f02002b

.field public static final abc_tab_indicator_material:I = 0x7f02002c

.field public static final abc_tab_indicator_mtrl_alpha:I = 0x7f02002d

.field public static final abc_textfield_activated_mtrl_alpha:I = 0x7f02002e

.field public static final abc_textfield_default_mtrl_alpha:I = 0x7f02002f

.field public static final abc_textfield_search_activated_mtrl_alpha:I = 0x7f020030

.field public static final abc_textfield_search_default_mtrl_alpha:I = 0x7f020031

.field public static final abc_textfield_search_material:I = 0x7f020032

.field public static final account_background:I = 0x7f020033

.field public static final account_switcher_blue:I = 0x7f020034

.field public static final action_bar_background:I = 0x7f020035

.field public static final action_bar_overflow_white:I = 0x7f020036

.field public static final action_bar_shadow:I = 0x7f020037

.field public static final action_icon_state_selector_background:I = 0x7f02026d

.field public static final action_new:I = 0x7f020038

.field public static final actionbar_drive_logo:I = 0x7f020039

.field public static final active_state_selector_background:I = 0x7f02026c

.field public static final activity_icon_punch:I = 0x7f02003a

.field public static final avatar_placeholder:I = 0x7f02003b

.field public static final back_arrow_white:I = 0x7f02003c

.field public static final back_grey:I = 0x7f02003d

.field public static final back_white:I = 0x7f02003e

.field public static final bg_footer_view_item:I = 0x7f02003f

.field public static final bg_menu_item:I = 0x7f020040

.field public static final bg_vidcontrol_middle:I = 0x7f020041

.field public static final black_gradient:I = 0x7f020042

.field public static final breadcrumb_state_selector:I = 0x7f020043

.field public static final bt_plus_document_highlighted:I = 0x7f020044

.field public static final bt_plus_document_normal:I = 0x7f020045

.field public static final button_background:I = 0x7f020046

.field public static final chromecast_icon:I = 0x7f020047

.field public static final common_signin_btn_icon_dark:I = 0x7f020048

.field public static final common_signin_btn_icon_disabled_dark:I = 0x7f020049

.field public static final common_signin_btn_icon_disabled_focus_dark:I = 0x7f02004a

.field public static final common_signin_btn_icon_disabled_focus_light:I = 0x7f02004b

.field public static final common_signin_btn_icon_disabled_light:I = 0x7f02004c

.field public static final common_signin_btn_icon_focus_dark:I = 0x7f02004d

.field public static final common_signin_btn_icon_focus_light:I = 0x7f02004e

.field public static final common_signin_btn_icon_light:I = 0x7f02004f

.field public static final common_signin_btn_icon_normal_dark:I = 0x7f020050

.field public static final common_signin_btn_icon_normal_light:I = 0x7f020051

.field public static final common_signin_btn_icon_pressed_dark:I = 0x7f020052

.field public static final common_signin_btn_icon_pressed_light:I = 0x7f020053

.field public static final common_signin_btn_text_dark:I = 0x7f020054

.field public static final common_signin_btn_text_disabled_dark:I = 0x7f020055

.field public static final common_signin_btn_text_disabled_focus_dark:I = 0x7f020056

.field public static final common_signin_btn_text_disabled_focus_light:I = 0x7f020057

.field public static final common_signin_btn_text_disabled_light:I = 0x7f020058

.field public static final common_signin_btn_text_focus_dark:I = 0x7f020059

.field public static final common_signin_btn_text_focus_light:I = 0x7f02005a

.field public static final common_signin_btn_text_light:I = 0x7f02005b

.field public static final common_signin_btn_text_normal_dark:I = 0x7f02005c

.field public static final common_signin_btn_text_normal_light:I = 0x7f02005d

.field public static final common_signin_btn_text_pressed_dark:I = 0x7f02005e

.field public static final common_signin_btn_text_pressed_light:I = 0x7f02005f

.field public static final constrained_ripple_background:I = 0x7f020060

.field public static final contact_android:I = 0x7f020061

.field public static final create_bar_shadow:I = 0x7f020062

.field public static final create_button_background:I = 0x7f020063

.field public static final create_button_background_focused:I = 0x7f020064

.field public static final create_button_background_pressed:I = 0x7f020065

.field public static final create_sheet_background:I = 0x7f020066

.field public static final dark_pill_background:I = 0x7f020067

.field public static final details_triangle:I = 0x7f020068

.field public static final divider_holo_light:I = 0x7f020069

.field public static final doc_grid_card_bottom_background:I = 0x7f02006a

.field public static final doc_grid_item_drawable:I = 0x7f02006b

.field public static final doc_grid_thumbnail_gradient:I = 0x7f02006c

.field public static final doc_icon_state_selector_background:I = 0x7f02006d

.field public static final doc_list_drop_to_current_folder_highlight:I = 0x7f02006e

.field public static final doclist_background_with_drop_shadow:I = 0x7f02006f

.field public static final doclist_drop_shadow:I = 0x7f020070

.field public static final doclist_recent_empty_view_logo:I = 0x7f020071

.field public static final doclist_state_selector_background:I = 0x7f02026f

.field public static final doclist_sticky_header_background_with_drop_shadow:I = 0x7f020072

.field public static final drag_knob_horizontal:I = 0x7f020073

.field public static final drag_knob_vertical:I = 0x7f020074

.field public static final drawer_shadow:I = 0x7f020075

.field public static final drive_logo_greyscale:I = 0x7f020076

.field public static final drive_logo_white:I = 0x7f020077

.field public static final drive_welcome_help_card_banner:I = 0x7f020078

.field public static final ds_actionbar_close_button:I = 0x7f020079

.field public static final ds_actionbar_compat_item:I = 0x7f02007a

.field public static final ds_actionbar_compat_item_focused:I = 0x7f02007b

.field public static final ds_actionbar_compat_item_pressed:I = 0x7f02007c

.field public static final ds_actionbar_shadow:I = 0x7f02007d

.field public static final ds_back_arrow_white:I = 0x7f02007e

.field public static final ds_checkmark_holo_light:I = 0x7f02007f

.field public static final ds_control_pressed:I = 0x7f020080

.field public static final ds_flash_off_holo_light:I = 0x7f020081

.field public static final ds_flash_on_holo_light:I = 0x7f020082

.field public static final ds_ic_editor_action_bar:I = 0x7f020083

.field public static final ds_ic_launcher:I = 0x7f020084

.field public static final ds_ic_menu_crop:I = 0x7f020085

.field public static final ds_ic_menu_delete:I = 0x7f020086

.field public static final ds_ic_menu_edit:I = 0x7f020087

.field public static final ds_overflow:I = 0x7f020088

.field public static final ds_plussign_holo_light:I = 0x7f020089

.field public static final ds_quad_corner_handle:I = 0x7f02008a

.field public static final ds_quad_corner_handle_focused:I = 0x7f02008b

.field public static final ds_rename_pencil:I = 0x7f02008c

.field public static final ds_retake_shutter_holo:I = 0x7f02008d

.field public static final ds_shutter_holo:I = 0x7f02008e

.field public static final ds_shutter_pressed_holo:I = 0x7f02008f

.field public static final ds_state_selector_background:I = 0x7f020090

.field public static final ds_thumb_horizontal_normal:I = 0x7f020091

.field public static final ds_thumb_vertical_normal:I = 0x7f020092

.field public static final editors_nav_icon_drive_active:I = 0x7f020093

.field public static final editors_nav_icon_drive_inactive:I = 0x7f020094

.field public static final editors_nav_icon_open_app:I = 0x7f020095

.field public static final expander_close_dark:I = 0x7f020096

.field public static final expander_group:I = 0x7f020097

.field public static final expander_open_dark:I = 0x7f020098

.field public static final floating_snackbar_background:I = 0x7f020099

.field public static final gradient_details:I = 0x7f02009a

.field public static final gradient_menu:I = 0x7f02009b

.field public static final hack_empty_actionbar_icon:I = 0x7f02009c

.field public static final help_card_background:I = 0x7f02009d

.field public static final home_bg_plain:I = 0x7f02009e

.field public static final home_page_background_proxy:I = 0x7f02009f

.field public static final home_page_button:I = 0x7f0200a0

.field public static final ic_acl_add:I = 0x7f0200a1

.field public static final ic_action_back:I = 0x7f0200a2

.field public static final ic_action_back_gray:I = 0x7f0200a3

.field public static final ic_action_details:I = 0x7f0200a4

.field public static final ic_action_info_alpha:I = 0x7f0200a5

.field public static final ic_action_menu_gray:I = 0x7f0200a6

.field public static final ic_activity_alpha:I = 0x7f0200a7

.field public static final ic_activity_subitempointer:I = 0x7f0200a8

.field public static final ic_add:I = 0x7f0200a9

.field public static final ic_add_people:I = 0x7f0200aa

.field public static final ic_addpeople:I = 0x7f0200ab

.field public static final ic_addpeople_alpha:I = 0x7f0200ac

.field public static final ic_addpeople_grey:I = 0x7f0200ad

.field public static final ic_addpeople_white:I = 0x7f0200ae

.field public static final ic_addtodrive:I = 0x7f0200af

.field public static final ic_addtodrive_grey:I = 0x7f0200b0

.field public static final ic_addtodrive_white:I = 0x7f0200b1

.field public static final ic_av_close_sm_dark:I = 0x7f0200b2

.field public static final ic_av_pause_sm_dark:I = 0x7f0200b3

.field public static final ic_av_play_sm_dark:I = 0x7f0200b4

.field public static final ic_av_stop_sm_dark:I = 0x7f0200b5

.field public static final ic_back:I = 0x7f0200b6

.field public static final ic_back_arrow_alpha:I = 0x7f0200b7

.field public static final ic_back_grey:I = 0x7f0200b8

.field public static final ic_back_white:I = 0x7f0200b9

.field public static final ic_btn_round_more:I = 0x7f0200ba

.field public static final ic_btn_round_plus:I = 0x7f0200bb

.field public static final ic_camera_scan:I = 0x7f0200bc

.field public static final ic_cancel_alpha:I = 0x7f0200bd

.field public static final ic_check:I = 0x7f0200be

.field public static final ic_chevron_down:I = 0x7f0200bf

.field public static final ic_chevron_down_grey:I = 0x7f0200c0

.field public static final ic_chevron_left_grey:I = 0x7f0200c1

.field public static final ic_chevron_right:I = 0x7f0200c2

.field public static final ic_chevron_right_grey:I = 0x7f0200c3

.field public static final ic_chevron_right_white:I = 0x7f0200c4

.field public static final ic_chevron_up:I = 0x7f0200c5

.field public static final ic_chevron_up_grey:I = 0x7f0200c6

.field public static final ic_chromcaston:I = 0x7f0200c7

.field public static final ic_chromcaston_grey:I = 0x7f0200c8

.field public static final ic_chromcaston_white:I = 0x7f0200c9

.field public static final ic_chromcastready:I = 0x7f0200ca

.field public static final ic_chromcastready_grey:I = 0x7f0200cb

.field public static final ic_chromcastready_white:I = 0x7f0200cc

.field public static final ic_clear:I = 0x7f0200cd

.field public static final ic_clear_grey:I = 0x7f0200ce

.field public static final ic_clear_white:I = 0x7f0200cf

.field public static final ic_close:I = 0x7f0200d0

.field public static final ic_close_black:I = 0x7f0200d1

.field public static final ic_close_selectable:I = 0x7f0200d2

.field public static final ic_close_white:I = 0x7f0200d3

.field public static final ic_closedcaption:I = 0x7f0200d4

.field public static final ic_contact_list_picture:I = 0x7f0200d5

.field public static final ic_create_bar_upload:I = 0x7f0200d6

.field public static final ic_create_new:I = 0x7f0200d7

.field public static final ic_create_new_from_camera:I = 0x7f0200d8

.field public static final ic_createpanel_doc:I = 0x7f0200d9

.field public static final ic_createpanel_folder:I = 0x7f0200da

.field public static final ic_createpanel_scan:I = 0x7f0200db

.field public static final ic_createpanel_sheet:I = 0x7f0200dc

.field public static final ic_createpanel_slide:I = 0x7f0200dd

.field public static final ic_createpanel_upload:I = 0x7f0200de

.field public static final ic_cross:I = 0x7f0200df

.field public static final ic_cross_light:I = 0x7f0200e0

.field public static final ic_delete_alpha:I = 0x7f0200e1

.field public static final ic_delete_black:I = 0x7f0200e2

.field public static final ic_details_overflow_alpha:I = 0x7f0200e3

.field public static final ic_docos_gray:I = 0x7f0200e4

.field public static final ic_download:I = 0x7f0200e5

.field public static final ic_download_alpha:I = 0x7f0200e6

.field public static final ic_download_grey:I = 0x7f0200e7

.field public static final ic_download_white:I = 0x7f0200e8

.field public static final ic_drawer:I = 0x7f0200e9

.field public static final ic_drawer_light:I = 0x7f0200ea

.field public static final ic_drive_all_items:I = 0x7f0200eb

.field public static final ic_drive_app_web_launcher:I = 0x7f0200ec

.field public static final ic_drive_my_drive:I = 0x7f0200ed

.field public static final ic_drive_offline:I = 0x7f0200ee

.field public static final ic_drive_owned_by_me:I = 0x7f0200ef

.field public static final ic_drive_recently_opened:I = 0x7f0200f0

.field public static final ic_drive_shared_with_me:I = 0x7f0200f1

.field public static final ic_drive_starred:I = 0x7f0200f2

.field public static final ic_drive_trash:I = 0x7f0200f3

.field public static final ic_drive_upload:I = 0x7f0200f4

.field public static final ic_edit:I = 0x7f0200f5

.field public static final ic_edit_grey:I = 0x7f0200f6

.field public static final ic_edit_white:I = 0x7f0200f7

.field public static final ic_empty_drive:I = 0x7f0200f8

.field public static final ic_empty_incoming:I = 0x7f0200f9

.field public static final ic_empty_photos:I = 0x7f0200fa

.field public static final ic_empty_pin:I = 0x7f0200fb

.field public static final ic_empty_recent:I = 0x7f0200fc

.field public static final ic_empty_star:I = 0x7f0200fd

.field public static final ic_empty_upload:I = 0x7f0200fe

.field public static final ic_filmstrip:I = 0x7f0200ff

.field public static final ic_filmstrip_grey:I = 0x7f020100

.field public static final ic_filmstrip_white:I = 0x7f020101

.field public static final ic_find:I = 0x7f020102

.field public static final ic_find_grey:I = 0x7f020103

.field public static final ic_find_white:I = 0x7f020104

.field public static final ic_full_screen:I = 0x7f020105

.field public static final ic_getlink:I = 0x7f020106

.field public static final ic_getlink_grey:I = 0x7f020107

.field public static final ic_getlink_white:I = 0x7f020108

.field public static final ic_gif_overlay:I = 0x7f020109

.field public static final ic_grid_toggle:I = 0x7f02010a

.field public static final ic_help:I = 0x7f02010b

.field public static final ic_history_alpha:I = 0x7f02010c

.field public static final ic_link_alpha:I = 0x7f02010d

.field public static final ic_link_sharing_off:I = 0x7f02010e

.field public static final ic_link_sharing_on:I = 0x7f02010f

.field public static final ic_list_toggle:I = 0x7f020110

.field public static final ic_menu_add_collaborator:I = 0x7f020111

.field public static final ic_menu_add_collaborator_inverse:I = 0x7f020112

.field public static final ic_menu_alpha:I = 0x7f020113

.field public static final ic_menu_chevron_right_alpha:I = 0x7f020114

.field public static final ic_menu_clear_alpha:I = 0x7f020115

.field public static final ic_menu_create_new:I = 0x7f020116

.field public static final ic_menu_edit_alpha:I = 0x7f020117

.field public static final ic_menu_filter:I = 0x7f020118

.field public static final ic_menu_information:I = 0x7f020119

.field public static final ic_menu_information_alpha:I = 0x7f02011a

.field public static final ic_menu_information_inverse:I = 0x7f02011b

.field public static final ic_menu_move:I = 0x7f02011c

.field public static final ic_menu_move_inverse:I = 0x7f02011d

.field public static final ic_menu_offline:I = 0x7f02011e

.field public static final ic_menu_offline_inverse:I = 0x7f02011f

.field public static final ic_menu_overflow:I = 0x7f020120

.field public static final ic_menu_overflow_alpha:I = 0x7f020121

.field public static final ic_menu_print:I = 0x7f020122

.field public static final ic_menu_rename:I = 0x7f020123

.field public static final ic_menu_search:I = 0x7f020124

.field public static final ic_menu_search_alpha:I = 0x7f020125

.field public static final ic_menu_send_feedback:I = 0x7f020126

.field public static final ic_menu_send_link:I = 0x7f020127

.field public static final ic_menu_upload:I = 0x7f020128

.field public static final ic_more:I = 0x7f020129

.field public static final ic_move_alpha:I = 0x7f02012a

.field public static final ic_new_folder:I = 0x7f02012b

.field public static final ic_new_folder_alpha:I = 0x7f02012c

.field public static final ic_next_alpha:I = 0x7f02012d

.field public static final ic_no_thumbnail:I = 0x7f02012e

.field public static final ic_notification_paused:I = 0x7f02012f

.field public static final ic_offline_notification:I = 0x7f020130

.field public static final ic_offline_small_alpha:I = 0x7f020131

.field public static final ic_open_in_alpha:I = 0x7f02026e

.field public static final ic_open_in_black:I = 0x7f020132

.field public static final ic_owner_black:I = 0x7f020133

.field public static final ic_page_indicator:I = 0x7f020134

.field public static final ic_page_indicator_enabled:I = 0x7f020135

.field public static final ic_pause:I = 0x7f020136

.field public static final ic_pause_alpha:I = 0x7f020137

.field public static final ic_person_black:I = 0x7f020138

.field public static final ic_photos_color:I = 0x7f020139

.field public static final ic_photos_color_big:I = 0x7f02013a

.field public static final ic_pin_dark:I = 0x7f02013b

.field public static final ic_play_alpha:I = 0x7f02013c

.field public static final ic_plusone_medium_off_client:I = 0x7f02013d

.field public static final ic_plusone_small_off_client:I = 0x7f02013e

.field public static final ic_plusone_standard_off_client:I = 0x7f02013f

.field public static final ic_plusone_tall_off_client:I = 0x7f020140

.field public static final ic_preview_normal:I = 0x7f020141

.field public static final ic_print_alpha:I = 0x7f020142

.field public static final ic_proj_no_thumbnail:I = 0x7f020143

.field public static final ic_recent_black:I = 0x7f020144

.field public static final ic_refresh_alpha:I = 0x7f020145

.field public static final ic_rename_alpha:I = 0x7f020146

.field public static final ic_retry:I = 0x7f020147

.field public static final ic_retry_alpha:I = 0x7f020148

.field public static final ic_rtl_back:I = 0x7f020149

.field public static final ic_rtl_back_grey:I = 0x7f02014a

.field public static final ic_rtl_back_white:I = 0x7f02014b

.field public static final ic_scandoc_widget:I = 0x7f02014c

.field public static final ic_send_attachment:I = 0x7f02014d

.field public static final ic_send_attachment_grey:I = 0x7f02014e

.field public static final ic_send_attachment_white:I = 0x7f02014f

.field public static final ic_send_file_alpha:I = 0x7f020150

.field public static final ic_sendfile:I = 0x7f020151

.field public static final ic_sendfile_grey:I = 0x7f020152

.field public static final ic_sendfile_white:I = 0x7f020153

.field public static final ic_settings:I = 0x7f020154

.field public static final ic_share:I = 0x7f020155

.field public static final ic_share_grey:I = 0x7f020156

.field public static final ic_share_white:I = 0x7f020157

.field public static final ic_shared_small_alpha:I = 0x7f020158

.field public static final ic_sharing_alpha:I = 0x7f020159

.field public static final ic_shortcut_arrow:I = 0x7f02015a

.field public static final ic_shortcut_bg:I = 0x7f02015b

.field public static final ic_shortcut_widget:I = 0x7f02015c

.field public static final ic_star:I = 0x7f02015d

.field public static final ic_star_black:I = 0x7f02015e

.field public static final ic_star_detail_fragment_selectable:I = 0x7f02015f

.field public static final ic_star_inset:I = 0x7f020160

.field public static final ic_star_off:I = 0x7f020161

.field public static final ic_star_on:I = 0x7f020162

.field public static final ic_star_selectable:I = 0x7f020163

.field public static final ic_starred_alpha:I = 0x7f020164

.field public static final ic_starred_small_alpha:I = 0x7f020165

.field public static final ic_stat_action_notification:I = 0x7f020166

.field public static final ic_storage_usage:I = 0x7f020167

.field public static final ic_sync_video_background:I = 0x7f020168

.field public static final ic_thumbnail_frame_btm:I = 0x7f020169

.field public static final ic_toast_undo:I = 0x7f02016a

.field public static final ic_type_audio:I = 0x7f02016b

.field public static final ic_type_audio_alpha:I = 0x7f02016c

.field public static final ic_type_chart:I = 0x7f02016d

.field public static final ic_type_chart_alpha:I = 0x7f02016e

.field public static final ic_type_chart_black:I = 0x7f02016f

.field public static final ic_type_chart_black_big:I = 0x7f020170

.field public static final ic_type_chart_color:I = 0x7f020171

.field public static final ic_type_doc:I = 0x7f020172

.field public static final ic_type_doc_alpha:I = 0x7f020173

.field public static final ic_type_doc_black:I = 0x7f020174

.field public static final ic_type_doc_black_big:I = 0x7f020175

.field public static final ic_type_doc_color:I = 0x7f020176

.field public static final ic_type_drawing:I = 0x7f020177

.field public static final ic_type_drawing_alpha:I = 0x7f020178

.field public static final ic_type_drawing_black:I = 0x7f020179

.field public static final ic_type_drawing_black_big:I = 0x7f02017a

.field public static final ic_type_drawing_color:I = 0x7f02017b

.field public static final ic_type_earth:I = 0x7f02017c

.field public static final ic_type_earth_alpha:I = 0x7f02017d

.field public static final ic_type_excel:I = 0x7f02017e

.field public static final ic_type_excel_alpha:I = 0x7f02017f

.field public static final ic_type_excel_black:I = 0x7f020180

.field public static final ic_type_excel_black_big:I = 0x7f020181

.field public static final ic_type_file:I = 0x7f020182

.field public static final ic_type_file_alpha:I = 0x7f020183

.field public static final ic_type_file_black:I = 0x7f020184

.field public static final ic_type_file_black_big:I = 0x7f020185

.field public static final ic_type_folder:I = 0x7f020186

.field public static final ic_type_folder_alpha:I = 0x7f020187

.field public static final ic_type_folder_black:I = 0x7f020188

.field public static final ic_type_folder_black_big:I = 0x7f020189

.field public static final ic_type_folder_shared:I = 0x7f02018a

.field public static final ic_type_folder_shared_black:I = 0x7f02018b

.field public static final ic_type_folder_shared_black_big:I = 0x7f02018c

.field public static final ic_type_form:I = 0x7f02018d

.field public static final ic_type_form_alpha:I = 0x7f02018e

.field public static final ic_type_form_black:I = 0x7f02018f

.field public static final ic_type_form_black_big:I = 0x7f020190

.field public static final ic_type_form_color:I = 0x7f020191

.field public static final ic_type_fusion:I = 0x7f020192

.field public static final ic_type_fusion_alpha:I = 0x7f020193

.field public static final ic_type_fusion_black:I = 0x7f020194

.field public static final ic_type_fusion_black_big:I = 0x7f020195

.field public static final ic_type_fusion_color:I = 0x7f020196

.field public static final ic_type_fusion_table_black:I = 0x7f020197

.field public static final ic_type_fusion_table_color:I = 0x7f020198

.field public static final ic_type_image:I = 0x7f020199

.field public static final ic_type_image_alpha:I = 0x7f02019a

.field public static final ic_type_image_black:I = 0x7f02019b

.field public static final ic_type_image_black_big:I = 0x7f02019c

.field public static final ic_type_image_color:I = 0x7f02019d

.field public static final ic_type_keep:I = 0x7f02019e

.field public static final ic_type_note:I = 0x7f02019f

.field public static final ic_type_pdf:I = 0x7f0201a0

.field public static final ic_type_pdf_alpha:I = 0x7f0201a1

.field public static final ic_type_pdf_black:I = 0x7f0201a2

.field public static final ic_type_pdf_black_big:I = 0x7f0201a3

.field public static final ic_type_powerpoint:I = 0x7f0201a4

.field public static final ic_type_powerpoint_alpha:I = 0x7f0201a5

.field public static final ic_type_powerpoint_black:I = 0x7f0201a6

.field public static final ic_type_powerpoint_black_big:I = 0x7f0201a7

.field public static final ic_type_presentation:I = 0x7f0201a8

.field public static final ic_type_presentation_alpha:I = 0x7f0201a9

.field public static final ic_type_presentation_black:I = 0x7f0201aa

.field public static final ic_type_presentation_black_big:I = 0x7f0201ab

.field public static final ic_type_presentation_color:I = 0x7f0201ac

.field public static final ic_type_script:I = 0x7f0201ad

.field public static final ic_type_script_alpha:I = 0x7f0201ae

.field public static final ic_type_script_black_big:I = 0x7f0201af

.field public static final ic_type_sharedfolder_alpha:I = 0x7f0201b0

.field public static final ic_type_sharedfolder_black_big:I = 0x7f0201b1

.field public static final ic_type_sheet:I = 0x7f0201b2

.field public static final ic_type_sheet_alpha:I = 0x7f0201b3

.field public static final ic_type_sheet_black:I = 0x7f0201b4

.field public static final ic_type_sheet_black_big:I = 0x7f0201b5

.field public static final ic_type_sheet_color:I = 0x7f0201b6

.field public static final ic_type_site:I = 0x7f0201b7

.field public static final ic_type_site_alpha:I = 0x7f0201b8

.field public static final ic_type_site_black:I = 0x7f0201b9

.field public static final ic_type_site_black_big:I = 0x7f0201ba

.field public static final ic_type_slide_black:I = 0x7f0201bb

.field public static final ic_type_slide_color:I = 0x7f0201bc

.field public static final ic_type_text:I = 0x7f0201bd

.field public static final ic_type_text_alpha:I = 0x7f0201be

.field public static final ic_type_video:I = 0x7f0201bf

.field public static final ic_type_video_alpha:I = 0x7f0201c0

.field public static final ic_type_video_black:I = 0x7f0201c1

.field public static final ic_type_video_black_big:I = 0x7f0201c2

.field public static final ic_type_video_color:I = 0x7f0201c3

.field public static final ic_type_word:I = 0x7f0201c4

.field public static final ic_type_word_alpha:I = 0x7f0201c5

.field public static final ic_type_word_black:I = 0x7f0201c6

.field public static final ic_type_word_black_big:I = 0x7f0201c7

.field public static final ic_type_zip:I = 0x7f0201c8

.field public static final ic_type_zip_alpha:I = 0x7f0201c9

.field public static final ic_unstarred_alpha:I = 0x7f0201ca

.field public static final ic_upaffordance:I = 0x7f0201cb

.field public static final ic_update:I = 0x7f0201cc

.field public static final ic_update_active:I = 0x7f0201cd

.field public static final ic_upload_notification:I = 0x7f0201ce

.field public static final ic_upload_notification_error:I = 0x7f0201cf

.field public static final ic_upload_warning:I = 0x7f0201d0

.field public static final ic_upload_warning_white:I = 0x7f0201d1

.field public static final ic_vidcontrol_pause:I = 0x7f020271

.field public static final ic_vidcontrol_play:I = 0x7f020270

.field public static final ic_vidcontrol_reload:I = 0x7f020272

.field public static final ic_video_overlay:I = 0x7f0201d2

.field public static final ic_video_overlay_big:I = 0x7f0201d3

.field public static final ic_volume_up_white:I = 0x7f0201d4

.field public static final icon_frame:I = 0x7f0201d5

.field public static final icon_pdfviewer:I = 0x7f0201d6

.field public static final icon_sharing_grey:I = 0x7f0201d7

.field public static final image2_a_02:I = 0x7f0201d8

.field public static final image2_b_02:I = 0x7f0201d9

.field public static final image2_c_03:I = 0x7f0201da

.field public static final image3_a_03:I = 0x7f0201db

.field public static final image3_a_07:I = 0x7f0201dc

.field public static final image3_b_03:I = 0x7f0201dd

.field public static final image3_b_07:I = 0x7f0201de

.field public static final image3_c_03:I = 0x7f0201df

.field public static final image4_a_03:I = 0x7f0201e0

.field public static final image4_b_02:I = 0x7f0201e1

.field public static final image4_c_03:I = 0x7f0201e2

.field public static final image4_c_07:I = 0x7f0201e3

.field public static final image4_c_11:I = 0x7f0201e4

.field public static final incoming_card_background:I = 0x7f0201e5

.field public static final incoming_card_state_selector_background:I = 0x7f0201e6

.field public static final large_item_background_focus:I = 0x7f0201e7

.field public static final large_item_background_pressed:I = 0x7f0201e8

.field public static final launcher_copy_link:I = 0x7f0201e9

.field public static final launcher_drive_icon:I = 0x7f0201ea

.field public static final launcher_drive_icon_internal_release:I = 0x7f0201eb

.field public static final launcher_kix_icon:I = 0x7f0201ec

.field public static final launcher_pdfviewer:I = 0x7f0201ed

.field public static final launcher_punch_icon:I = 0x7f0201ee

.field public static final launcher_trix_icon:I = 0x7f0201ef

.field public static final lock:I = 0x7f0201f0

.field public static final manage_accounts_icon:I = 0x7f0201f1

.field public static final menu_submenu_background:I = 0x7f0201f2

.field public static final mr_ic_audio_vol:I = 0x7f0201f3

.field public static final mr_ic_media_route_connecting_holo_dark:I = 0x7f0201f4

.field public static final mr_ic_media_route_connecting_holo_light:I = 0x7f0201f5

.field public static final mr_ic_media_route_disabled_holo_dark:I = 0x7f0201f6

.field public static final mr_ic_media_route_disabled_holo_light:I = 0x7f0201f7

.field public static final mr_ic_media_route_holo_dark:I = 0x7f0201f8

.field public static final mr_ic_media_route_holo_light:I = 0x7f0201f9

.field public static final mr_ic_media_route_off_holo_dark:I = 0x7f0201fa

.field public static final mr_ic_media_route_off_holo_light:I = 0x7f0201fb

.field public static final mr_ic_media_route_on_0_holo_dark:I = 0x7f0201fc

.field public static final mr_ic_media_route_on_0_holo_light:I = 0x7f0201fd

.field public static final mr_ic_media_route_on_1_holo_dark:I = 0x7f0201fe

.field public static final mr_ic_media_route_on_1_holo_light:I = 0x7f0201ff

.field public static final mr_ic_media_route_on_2_holo_dark:I = 0x7f020200

.field public static final mr_ic_media_route_on_2_holo_light:I = 0x7f020201

.field public static final mr_ic_media_route_on_holo_dark:I = 0x7f020202

.field public static final mr_ic_media_route_on_holo_light:I = 0x7f020203

.field public static final mtl_progress:I = 0x7f020204

.field public static final mtl_progress_bg:I = 0x7f020205

.field public static final mtl_progress_primary:I = 0x7f020206

.field public static final mtl_progress_secondary:I = 0x7f020207

.field public static final multipleaccounts_formenu:I = 0x7f020208

.field public static final navigation_shadow_bottom:I = 0x7f020209

.field public static final next:I = 0x7f02020a

.field public static final notification_icon:I = 0x7f02020b

.field public static final notification_icon_bg:I = 0x7f02020c

.field public static final notify_panel_notification_icon_bg:I = 0x7f02020d

.field public static final open_document_button_background:I = 0x7f02020e

.field public static final open_grey:I = 0x7f02020f

.field public static final paging_progress:I = 0x7f020210

.field public static final photo_backup_help_card_header:I = 0x7f020211

.field public static final pin_checkbox:I = 0x7f020212

.field public static final pin_checked:I = 0x7f020213

.field public static final pin_unchecked:I = 0x7f020214

.field public static final powered_by_google_dark:I = 0x7f020215

.field public static final powered_by_google_light:I = 0x7f020216

.field public static final previous:I = 0x7f020217

.field public static final progressbar_solid_holo:I = 0x7f020218

.field public static final projector_progress:I = 0x7f020219

.field public static final projector_progress_thumb:I = 0x7f02021a

.field public static final punch_frame_thumbnail_background:I = 0x7f02021b

.field public static final punch_frame_webview:I = 0x7f02021c

.field public static final punch_frame_webview_background:I = 0x7f02021d

.field public static final punch_speaker_notes_empty:I = 0x7f02021e

.field public static final punch_speaker_notes_present:I = 0x7f02021f

.field public static final punch_speaker_notes_present_large:I = 0x7f020220

.field public static final qp_new_document_button:I = 0x7f020221

.field public static final ripple_background:I = 0x7f020222

.field public static final scan_help_card_banner:I = 0x7f020223

.field public static final scrollbar_handle_accelerated_anim2:I = 0x7f020224

.field public static final scrubber_bottom_bar_bg_light:I = 0x7f020225

.field public static final scrubber_callout_arrow:I = 0x7f020226

.field public static final scrubber_callout_light:I = 0x7f020227

.field public static final scrubber_focused_light:I = 0x7f020228

.field public static final scrubber_knob_day:I = 0x7f020229

.field public static final scrubber_last_position_light:I = 0x7f02022a

.field public static final scrubber_normal_light:I = 0x7f02022b

.field public static final scrubber_pressed_light:I = 0x7f02022c

.field public static final scrubber_track_light:I = 0x7f02022d

.field public static final scrubber_track_off_light:I = 0x7f02022e

.field public static final scrubber_track_on_light:I = 0x7f02022f

.field public static final search_bar_background:I = 0x7f020230

.field public static final search_shadow:I = 0x7f020231

.field public static final select_clear:I = 0x7f020232

.field public static final select_deselect_overlay:I = 0x7f020233

.field public static final select_download:I = 0x7f020234

.field public static final select_drop_new_folder:I = 0x7f020235

.field public static final select_drop_this_folder:I = 0x7f020236

.field public static final select_grid_selected:I = 0x7f020237

.field public static final select_grid_selected_folder:I = 0x7f020238

.field public static final select_grid_unselected:I = 0x7f020239

.field public static final select_grid_unselected_folder:I = 0x7f02023a

.field public static final select_list_selected:I = 0x7f02023b

.field public static final select_move_to:I = 0x7f02023c

.field public static final select_move_to_current_folder:I = 0x7f02023d

.field public static final select_overflow:I = 0x7f02023e

.field public static final select_pin:I = 0x7f02023f

.field public static final select_print:I = 0x7f020240

.field public static final select_remove:I = 0x7f020241

.field public static final select_rename:I = 0x7f020242

.field public static final select_share:I = 0x7f020243

.field public static final select_star:I = 0x7f020244

.field public static final select_unstar:I = 0x7f020245

.field public static final selection_drop_zone_bg:I = 0x7f020246

.field public static final selection_drop_zone_highlight_bg:I = 0x7f020247

.field public static final selection_floating_bg_visual:I = 0x7f020248

.field public static final selection_floating_handle_bg:I = 0x7f020249

.field public static final shadow:I = 0x7f02024a

.field public static final snackbar_background:I = 0x7f02024b

.field public static final state_selector_background:I = 0x7f02026b

.field public static final state_selector_background_no_selection:I = 0x7f02024c

.field public static final text_alert:I = 0x7f02024d

.field public static final text_select_handle_left:I = 0x7f02024e

.field public static final text_select_handle_right:I = 0x7f02024f

.field public static final textfield_default_mtrl_alpha:I = 0x7f020250

.field public static final thumbnail_background:I = 0x7f020251

.field public static final title_background_proxy:I = 0x7f020252

.field public static final title_bg_plain:I = 0x7f020253

.field public static final top_bar_search:I = 0x7f020254

.field public static final transparent_active_state_selector_background:I = 0x7f020255

.field public static final transparent_rectangle:I = 0x7f020256

.field public static final transparent_state_selector_background:I = 0x7f020257

.field public static final undo_button_background_pressed:I = 0x7f020258

.field public static final undo_state_selector_background:I = 0x7f020259

.field public static final upload_to_drive_icon:I = 0x7f02025a

.field public static final widget_action_normal:I = 0x7f02025b

.field public static final widget_app_logo:I = 0x7f02025c

.field public static final widget_background:I = 0x7f02025d

.field public static final widget_center_background:I = 0x7f02025e

.field public static final widget_center_background_focus:I = 0x7f02025f

.field public static final widget_center_background_pressed:I = 0x7f020260

.field public static final widget_left_background:I = 0x7f020261

.field public static final widget_left_background_focus:I = 0x7f020262

.field public static final widget_left_background_pressed:I = 0x7f020263

.field public static final widget_newdoc:I = 0x7f020264

.field public static final widget_newdocfromcamera:I = 0x7f020265

.field public static final widget_preview:I = 0x7f020266

.field public static final widget_right_background:I = 0x7f020267

.field public static final widget_right_background_focus:I = 0x7f020268

.field public static final widget_right_background_pressed:I = 0x7f020269

.field public static final widget_upload:I = 0x7f02026a


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3016
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
