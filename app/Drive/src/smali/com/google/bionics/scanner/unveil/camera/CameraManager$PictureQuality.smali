.class public Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;
.super Ljava/lang/Object;
.source "CameraManager.java"


# static fields
.field public static final HIGH_QUALITY:Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;

.field public static final NORMAL_QUALITY:Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;

.field public static final PRIME_QUALITY:Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;


# instance fields
.field public final desiredHeight:I

.field public final desiredWidth:I

.field public final jpegQuality:I

.field public final pixelCount:I

.field public final recompressJpegQuality:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0x1000

    const/16 v4, 0x64

    const/16 v3, 0x5f

    .line 141
    new-instance v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;

    const/16 v1, 0x200

    const/16 v2, 0x180

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;-><init>(IIII)V

    sput-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;->NORMAL_QUALITY:Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;

    .line 142
    new-instance v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;

    const/16 v1, 0x640

    const/16 v2, 0x4b0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;-><init>(IIII)V

    sput-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;->HIGH_QUALITY:Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;

    .line 144
    new-instance v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;

    invoke-direct {v0, v5, v5, v3, v4}, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;-><init>(IIII)V

    sput-object v0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;->PRIME_QUALITY:Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;

    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 1

    .prologue
    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    iput p1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;->desiredWidth:I

    .line 158
    iput p2, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;->desiredHeight:I

    .line 159
    mul-int v0, p1, p2

    iput v0, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;->pixelCount:I

    .line 160
    iput p3, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;->jpegQuality:I

    .line 161
    iput p4, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;->recompressJpegQuality:I

    .line 162
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 166
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[desiredWidth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;->desiredWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "desiredHeight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;->desiredHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "jpegQuality="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;->jpegQuality:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "recompressJpegQuality="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/bionics/scanner/unveil/camera/CameraManager$PictureQuality;->recompressJpegQuality:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
