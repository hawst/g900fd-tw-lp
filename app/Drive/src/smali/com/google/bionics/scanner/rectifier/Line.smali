.class public Lcom/google/bionics/scanner/rectifier/Line;
.super Ljava/lang/Object;
.source "Line.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public transient inlier:Landroid/graphics/PointF;

.field public transient unitVector:Landroid/graphics/PointF;


# direct methods
.method public constructor <init>(FFFF)V
    .locals 6

    .prologue
    .line 35
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/bionics/scanner/rectifier/Line;-><init>(FFFFZ)V

    .line 36
    return-void
.end method

.method public constructor <init>(FFFFZ)V
    .locals 4

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    .line 52
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, p3, p4}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    .line 53
    if-eqz p5, :cond_0

    .line 54
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    invoke-virtual {v0}, Landroid/graphics/PointF;->length()F

    move-result v0

    .line 55
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    div-float/2addr v2, v0

    iget-object v3, p0, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    div-float v0, v3, v0

    invoke-virtual {v1, v2, v0}, Landroid/graphics/PointF;->set(FF)V

    .line 57
    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/google/bionics/scanner/rectifier/Line;)V
    .locals 3

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Landroid/graphics/PointF;

    iget-object v1, p1, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p1, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    .line 66
    new-instance v0, Landroid/graphics/PointF;

    iget-object v1, p1, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p1, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    .line 67
    return-void
.end method


# virtual methods
.method public applyScale(F)V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    iget v1, v0, Landroid/graphics/PointF;->x:F

    mul-float/2addr v1, p1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 85
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    iget v1, v0, Landroid/graphics/PointF;->y:F

    mul-float/2addr v1, p1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 86
    return-void
.end method

.method public copy()Lcom/google/bionics/scanner/rectifier/Line;
    .locals 1

    .prologue
    .line 75
    new-instance v0, Lcom/google/bionics/scanner/rectifier/Line;

    invoke-direct {v0, p0}, Lcom/google/bionics/scanner/rectifier/Line;-><init>(Lcom/google/bionics/scanner/rectifier/Line;)V

    return-object v0
.end method

.method public distPoint(Landroid/graphics/PointF;)F
    .locals 4

    .prologue
    .line 95
    new-instance v0, Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget v2, p1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget v3, p1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 97
    iget v1, v0, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    mul-float/2addr v1, v2

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    neg-float v2, v2

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 134
    if-ne p0, p1, :cond_1

    .line 144
    :cond_0
    :goto_0
    return v0

    .line 137
    :cond_1
    instance-of v2, p1, Lcom/google/bionics/scanner/rectifier/Line;

    if-nez v2, :cond_2

    move v0, v1

    .line 138
    goto :goto_0

    .line 140
    :cond_2
    check-cast p1, Lcom/google/bionics/scanner/rectifier/Line;

    .line 143
    iget-object v2, p1, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    iget-object v3, p0, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/PointF;->equals(FF)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p1, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    iget-object v3, p0, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    .line 144
    invoke-virtual {v2, v3, v4}, Landroid/graphics/PointF;->equals(FF)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 149
    .line 151
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 152
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 153
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 154
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 155
    return v0
.end method

.method public intersect(Lcom/google/bionics/scanner/rectifier/Line;Landroid/graphics/PointF;)Z
    .locals 5

    .prologue
    .line 111
    iget-object v0, p1, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    mul-float/2addr v0, v1

    iget-object v1, p1, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    .line 113
    iget-object v1, p1, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget-object v3, p1, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    iget-object v2, p1, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget-object v3, p0, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    iget-object v4, p1, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    .line 116
    const/4 v2, 0x0

    cmpl-float v2, v0, v2

    if-eqz v2, :cond_1

    .line 117
    if-eqz p2, :cond_0

    .line 118
    div-float v0, v1, v0

    .line 119
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    mul-float/2addr v2, v0

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget-object v3, p0, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    invoke-virtual {p2, v1, v0}, Landroid/graphics/PointF;->set(FF)V

    .line 121
    :cond_0
    const/4 v0, 0x1

    .line 123
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 128
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "[inlier(%f, %f), unitVector(%f, %f)]"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    .line 129
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    .line 128
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
