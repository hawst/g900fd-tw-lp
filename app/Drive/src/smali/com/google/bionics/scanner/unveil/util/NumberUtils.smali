.class public abstract Lcom/google/bionics/scanner/unveil/util/NumberUtils;
.super Ljava/lang/Object;
.source "NumberUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final format(FI)Ljava/lang/String;
    .locals 4

    .prologue
    .line 87
    if-nez p1, :cond_0

    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    float-to-int v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 100
    :goto_0
    return-object v0

    .line 91
    :cond_0
    const/16 v1, 0xa

    .line 92
    const/4 v0, 0x1

    :goto_1
    if-ge v0, p1, :cond_1

    .line 93
    mul-int/lit8 v1, v1, 0xa

    .line 92
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 99
    :cond_1
    int-to-float v0, v1

    mul-float/2addr v0, p0

    float-to-int v0, v0

    .line 100
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    div-int v3, v0, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    rem-int/2addr v0, v1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static final getMeanIndex([I)F
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 70
    move v1, v0

    move v2, v0

    .line 72
    :goto_0
    array-length v3, p0

    if-ge v0, v3, :cond_0

    .line 73
    aget v3, p0, v0

    mul-int/2addr v3, v0

    add-int/2addr v1, v3

    .line 74
    aget v3, p0, v0

    add-int/2addr v2, v3

    .line 72
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    :cond_0
    int-to-float v0, v1

    int-to-float v1, v2

    div-float/2addr v0, v1

    return v0
.end method

.method public static final getMedianIndex([I)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 43
    .line 44
    array-length v3, p0

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget v4, p0, v1

    .line 45
    add-int/2addr v2, v4

    .line 44
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 48
    :cond_0
    div-int/lit8 v2, v2, 0x2

    move v1, v0

    .line 50
    :goto_1
    array-length v3, p0

    if-ge v0, v3, :cond_2

    .line 51
    aget v3, p0, v0

    add-int/2addr v1, v3

    .line 52
    if-lt v1, v2, :cond_1

    .line 58
    :goto_2
    return v0

    .line 50
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 58
    :cond_2
    const/4 v0, -0x1

    goto :goto_2
.end method

.method public static final getNormalizedStdDev([I)F
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 20
    .line 21
    const-wide/16 v2, 0x0

    .line 22
    array-length v4, p0

    move v1, v0

    :goto_0
    if-ge v0, v4, :cond_0

    aget v5, p0, v0

    .line 23
    add-int/2addr v1, v5

    .line 24
    int-to-long v6, v5

    int-to-long v8, v5

    mul-long/2addr v6, v8

    add-long/2addr v2, v6

    .line 22
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 28
    :cond_0
    int-to-float v0, v1

    array-length v4, p0

    int-to-float v4, v4

    div-float/2addr v0, v4

    .line 29
    long-to-float v2, v2

    int-to-float v3, v1

    mul-float/2addr v0, v3

    sub-float v0, v2, v0

    array-length v2, p0

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 32
    mul-int/2addr v1, v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v0

    return v0
.end method

.method public static normalizeDegrees(F)F
    .locals 3

    .prologue
    const/high16 v2, 0x43b40000    # 360.0f

    .line 107
    div-float v0, p0, v2

    float-to-int v0, v0

    .line 109
    mul-int/lit16 v0, v0, 0x168

    int-to-float v0, v0

    sub-float v0, p0, v0

    .line 111
    const/high16 v1, 0x43340000    # 180.0f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_1

    .line 112
    sub-float/2addr v0, v2

    .line 117
    :cond_0
    :goto_0
    return v0

    .line 113
    :cond_1
    const/high16 v1, -0x3ccc0000    # -180.0f

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_0

    .line 114
    add-float/2addr v0, v2

    goto :goto_0
.end method

.method public static radiansToNormalizedDegrees(F)F
    .locals 2

    .prologue
    .line 121
    float-to-double v0, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    double-to-float v0, v0

    invoke-static {v0}, Lcom/google/bionics/scanner/unveil/util/NumberUtils;->normalizeDegrees(F)F

    move-result v0

    return v0
.end method
