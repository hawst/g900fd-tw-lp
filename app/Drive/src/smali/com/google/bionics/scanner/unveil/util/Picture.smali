.class public abstract Lcom/google/bionics/scanner/unveil/util/Picture;
.super Ljava/lang/Object;
.source "Picture.java"


# static fields
.field public static final SOURCE_CAMERA:I = 0x1

.field public static final SOURCE_SHARED:I = 0x2

.field public static final SOURCE_UNKNOWN:I = -0x1


# instance fields
.field protected final a:I

.field private final a:J

.field private a:Landroid/graphics/Rect;

.field private volatile a:Z

.field private b:I


# direct methods
.method protected constructor <init>(I)V
    .locals 1

    .prologue
    .line 48
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lcom/google/bionics/scanner/unveil/util/Picture;-><init>(II)V

    .line 49
    return-void
.end method

.method public constructor <init>(II)V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/util/Picture;->a:Z

    .line 42
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/util/Picture;->a:J

    .line 43
    iput p1, p0, Lcom/google/bionics/scanner/unveil/util/Picture;->b:I

    .line 44
    iput p2, p0, Lcom/google/bionics/scanner/unveil/util/Picture;->a:I

    .line 45
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 4

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/util/Picture;->a:Z

    if-eqz v0, :cond_0

    .line 176
    :try_start_0
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    :catch_0
    move-exception v0

    .line 178
    new-instance v1, Lcom/google/bionics/scanner/unveil/util/Logger;

    invoke-direct {v1}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>()V

    const-string v2, "Exception!"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 181
    :cond_0
    return-void
.end method

.method public abstract getByteSize()I
.end method

.method public final getCopyOfBitmap()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 128
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/Picture;->peekBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 130
    if-eqz v2, :cond_1

    .line 131
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    .line 133
    :goto_0
    if-nez v2, :cond_0

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {v2, v1, v0}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public final getCopyOfBitmap(Landroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 143
    invoke-virtual {p0, p1}, Lcom/google/bionics/scanner/unveil/util/Picture;->peekBitmap(Landroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 145
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public getCropArea()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/Picture;->a:Landroid/graphics/Rect;

    return-object v0
.end method

.method public abstract getCroppedPicture()Lcom/google/bionics/scanner/unveil/util/Picture;
.end method

.method public abstract getDrawable()Landroid/graphics/drawable/BitmapDrawable;
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/google/bionics/scanner/unveil/util/Picture;->a:J

    return-wide v0
.end method

.method public abstract getJpegData()[B
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/google/bionics/scanner/unveil/util/Picture;->b:I

    return v0
.end method

.method public abstract getSize()Lcom/google/bionics/scanner/unveil/util/Size;
.end method

.method public final getSource()I
    .locals 1

    .prologue
    .line 184
    iget v0, p0, Lcom/google/bionics/scanner/unveil/util/Picture;->a:I

    return v0
.end method

.method public abstract getYuvData()[B
.end method

.method public final isRecycled()Z
    .locals 1

    .prologue
    .line 170
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/util/Picture;->a:Z

    return v0
.end method

.method public abstract peekBitmap()Landroid/graphics/Bitmap;
.end method

.method public abstract peekBitmap(Landroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
.end method

.method public recycle()V
    .locals 1

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/Picture;->a()V

    .line 166
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/util/Picture;->a:Z

    .line 167
    return-void
.end method

.method public setCropArea(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/util/Picture;->a:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 57
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Crop area already set!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_0
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/util/Picture;->a:Landroid/graphics/Rect;

    .line 61
    return-void
.end method

.method public setOrientation(I)V
    .locals 0

    .prologue
    .line 68
    iput p1, p0, Lcom/google/bionics/scanner/unveil/util/Picture;->b:I

    .line 69
    return-void
.end method
