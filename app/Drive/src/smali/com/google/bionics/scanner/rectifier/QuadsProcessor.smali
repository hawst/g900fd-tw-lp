.class public Lcom/google/bionics/scanner/rectifier/QuadsProcessor;
.super Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;
.source "QuadsProcessor.java"


# instance fields
.field private a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

.field private final a:Lcom/google/bionics/scanner/rectifier/QuadsProcessor$Listener;

.field private final a:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/bionics/scanner/rectifier/QuadsProcessor$Listener;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;-><init>()V

    .line 38
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/QuadsProcessor;->a:Ljava/util/Vector;

    .line 45
    iput-object p1, p0, Lcom/google/bionics/scanner/rectifier/QuadsProcessor;->a:Lcom/google/bionics/scanner/rectifier/QuadsProcessor$Listener;

    .line 46
    return-void
.end method


# virtual methods
.method protected declared-synchronized a(Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;)V
    .locals 5

    .prologue
    .line 59
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->getWidth()I

    move-result v0

    .line 60
    invoke-virtual {p1}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->getHeight()I

    move-result v1

    .line 61
    invoke-virtual {p1}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->getRawData()[B

    move-result-object v2

    .line 63
    new-instance v3, Lcom/google/bionics/scanner/rectifier/ImageData;

    const/16 v4, 0x11

    invoke-direct {v3, v2, v4, v0, v1}, Lcom/google/bionics/scanner/rectifier/ImageData;-><init>([BIII)V

    .line 64
    invoke-static {v3}, Lcom/google/bionics/scanner/rectifier/QuadDetector;->extractQuads(Lcom/google/bionics/scanner/rectifier/ImageData;)Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    move-result-object v2

    iput-object v2, p0, Lcom/google/bionics/scanner/rectifier/QuadsProcessor;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    .line 66
    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/QuadsProcessor;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    if-eqz v2, :cond_0

    .line 67
    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/QuadsProcessor;->a:Lcom/google/bionics/scanner/rectifier/QuadsProcessor$Listener;

    if-eqz v2, :cond_0

    .line 68
    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/QuadsProcessor;->a:Lcom/google/bionics/scanner/rectifier/QuadsProcessor$Listener;

    iget-object v3, p0, Lcom/google/bionics/scanner/rectifier/QuadsProcessor;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    invoke-interface {v2, v3, v0, v1}, Lcom/google/bionics/scanner/rectifier/QuadsProcessor$Listener;->onQuadsChanged(LbhH;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    :cond_0
    monitor-exit p0

    return-void

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDebugText()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/QuadsProcessor;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 86
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/QuadsProcessor;->a:Ljava/util/Vector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onDrawDebug(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 95
    monitor-enter p0

    const/high16 v0, 0x43960000    # 300.0f

    .line 97
    :try_start_0
    new-instance v1, Lcom/google/bionics/scanner/unveil/ui/BorderedText;

    const/high16 v2, 0x41a00000    # 20.0f

    invoke-direct {v1, v2}, Lcom/google/bionics/scanner/unveil/ui/BorderedText;-><init>(F)V

    .line 98
    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/QuadsProcessor;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    if-eqz v2, :cond_0

    .line 99
    const/high16 v2, 0x41a00000    # 20.0f

    iget-object v3, p0, Lcom/google/bionics/scanner/rectifier/QuadsProcessor;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    invoke-virtual {v3}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->getNumCandidatesString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, p1, v2, v0, v3}, Lcom/google/bionics/scanner/unveil/ui/BorderedText;->drawText(Landroid/graphics/Canvas;FFLjava/lang/String;)V

    .line 100
    invoke-virtual {v1}, Lcom/google/bionics/scanner/unveil/ui/BorderedText;->getTextSize()F

    move-result v2

    add-float/2addr v0, v2

    .line 101
    const/high16 v2, 0x41a00000    # 20.0f

    iget-object v3, p0, Lcom/google/bionics/scanner/rectifier/QuadsProcessor;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    const-string v4, "%.0f"

    invoke-virtual {v3, v4}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->getDefaultQuadString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, p1, v2, v0, v3}, Lcom/google/bionics/scanner/unveil/ui/BorderedText;->drawText(Landroid/graphics/Canvas;FFLjava/lang/String;)V

    .line 102
    invoke-virtual {v1}, Lcom/google/bionics/scanner/unveil/ui/BorderedText;->getTextSize()F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    add-float/2addr v0, v1

    .line 105
    :cond_0
    monitor-exit p0

    return-void

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onShutdown()V
    .locals 0

    .prologue
    .line 76
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized setProcessingEnabled(Z)V
    .locals 1

    .prologue
    .line 50
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;->setProcessingEnabled(Z)V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/QuadsProcessor;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    .line 52
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/QuadsProcessor;->a:Lcom/google/bionics/scanner/rectifier/QuadsProcessor$Listener;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/QuadsProcessor;->a:Lcom/google/bionics/scanner/rectifier/QuadsProcessor$Listener;

    invoke-interface {v0, p1}, Lcom/google/bionics/scanner/rectifier/QuadsProcessor$Listener;->onProcessingEnabled(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55
    :cond_0
    monitor-exit p0

    return-void

    .line 50
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
