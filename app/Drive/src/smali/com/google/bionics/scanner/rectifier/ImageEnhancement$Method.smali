.class public final enum Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;
.super Ljava/lang/Enum;
.source "ImageEnhancement.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum AUTO:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

.field public static final enum NONE:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

.field public static final enum OPTIMIZE_FOR_BW:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

.field public static final enum OPTIMIZE_FOR_COLOR:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

.field public static final enum OPTIMIZE_FOR_COLOR_DRAWING:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

.field private static final synthetic a:[Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->NONE:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    .line 15
    new-instance v0, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    const-string v1, "AUTO"

    invoke-direct {v0, v1, v3}, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->AUTO:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    .line 17
    new-instance v0, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    const-string v1, "OPTIMIZE_FOR_COLOR"

    invoke-direct {v0, v1, v4}, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->OPTIMIZE_FOR_COLOR:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    .line 19
    new-instance v0, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    const-string v1, "OPTIMIZE_FOR_BW"

    invoke-direct {v0, v1, v5}, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->OPTIMIZE_FOR_BW:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    .line 21
    new-instance v0, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    const-string v1, "OPTIMIZE_FOR_COLOR_DRAWING"

    invoke-direct {v0, v1, v6}, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->OPTIMIZE_FOR_COLOR_DRAWING:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    .line 11
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    sget-object v1, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->NONE:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->AUTO:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->OPTIMIZE_FOR_COLOR:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->OPTIMIZE_FOR_BW:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->OPTIMIZE_FOR_COLOR_DRAWING:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->a:[Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    return-object v0
.end method

.method public static values()[Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->a:[Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    invoke-virtual {v0}, [Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    return-object v0
.end method
