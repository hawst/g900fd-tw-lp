.class public Lcom/google/bionics/scanner/unveil/nonstop/ProcessingThread;
.super Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;
.source "ProcessingThread.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:I

.field private a:Landroid/os/Handler;

.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p3}, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;-><init>(Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;)V

    .line 25
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingThread;->a:Ljava/lang/String;

    .line 26
    iput p2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingThread;->a:I

    .line 27
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 59
    invoke-super {p0}, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;->a()V

    .line 61
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingThread;->a:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 62
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 64
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingThread;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 66
    iget v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingThread;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 67
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 69
    :goto_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingThread;->a:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 70
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingThread;->d()V

    goto :goto_0

    .line 73
    :cond_0
    return-void
.end method

.method protected b(Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;)V
    .locals 2

    .prologue
    .line 85
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingThread;->a(Z)V

    .line 86
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 87
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 89
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingThread;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 90
    return-void
.end method

.method protected c()V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingThread;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingThread;->a:Landroid/os/Handler;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 80
    :cond_0
    invoke-super {p0}, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingChain;->c()V

    .line 81
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 31
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 33
    new-instance v0, LbhR;

    invoke-direct {v0, p0}, LbhR;-><init>(Lcom/google/bionics/scanner/unveil/nonstop/ProcessingThread;)V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingThread;->a:Landroid/os/Handler;

    .line 49
    invoke-virtual {p0, v1}, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingThread;->a(Z)V

    .line 51
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingThread;->a:Landroid/os/Handler;

    .line 54
    invoke-virtual {p0, v1}, Lcom/google/bionics/scanner/unveil/nonstop/ProcessingThread;->a(Z)V

    .line 55
    return-void
.end method
