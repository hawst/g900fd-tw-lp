.class public Lcom/google/bionics/scanner/rectifier/QuadDetector;
.super Ljava/lang/Object;
.source "QuadDetector.java"


# static fields
.field public static final TEST_QUAD_HEIGHT:I = 0x1e0

.field public static final TEST_QUAD_WIDTH:I = 0x280

.field private static final a:[F

.field private static final b:[F

.field private static final c:[F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 11
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/bionics/scanner/rectifier/QuadDetector;->a:[F

    .line 12
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/bionics/scanner/rectifier/QuadDetector;->b:[F

    .line 13
    new-array v0, v1, [F

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/bionics/scanner/rectifier/QuadDetector;->c:[F

    .line 16
    const-string v0, "rectifier"

    invoke-static {v0}, Lcom/google/bionics/scanner/unveil/util/ResourceUtils;->loadNativeLibrary(Ljava/lang/String;)V

    .line 17
    return-void

    .line 11
    nop

    :array_0
    .array-data 4
        0x42480000    # 50.0f
        0x42480000    # 50.0f
        0x44160000    # 600.0f
        0x42960000    # 75.0f
        0x44110000    # 580.0f
        0x43e10000    # 450.0f
        0x42c80000    # 100.0f
        0x43c80000    # 400.0f
    .end array-data

    .line 12
    :array_1
    .array-data 4
        0x428c0000    # 70.0f
        0x42a00000    # 80.0f
        0x44138000    # 590.0f
        0x42480000    # 50.0f
        0x440c0000    # 560.0f
        0x43c80000    # 400.0f
        0x42f00000    # 120.0f
        0x43d70000    # 430.0f
    .end array-data

    .line 13
    :array_2
    .array-data 4
        0x43200000    # 160.0f
        0x42dc0000    # 110.0f
        0x43ff0000    # 510.0f
        0x43020000    # 130.0f
        0x43f00000    # 480.0f
        0x43af0000    # 350.0f
        0x43520000    # 210.0f
        0x43be0000    # 380.0f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native extractQuads(Lcom/google/bionics/scanner/rectifier/ImageData;)Lcom/google/bionics/scanner/rectifier/Quadrilateral;
.end method

.method public static native extractQuadsInFile(Ljava/lang/String;I)Lcom/google/bionics/scanner/rectifier/Quadrilateral;
.end method

.method public static getMockQuads(Lcom/google/bionics/scanner/rectifier/ImageData;)Lcom/google/bionics/scanner/rectifier/Quadrilateral;
    .locals 4

    .prologue
    .line 27
    new-instance v0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    sget-object v1, Lcom/google/bionics/scanner/rectifier/QuadDetector;->a:[F

    const/16 v2, 0x280

    const/16 v3, 0x1e0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;-><init>([FII)V

    .line 28
    sget-object v1, Lcom/google/bionics/scanner/rectifier/QuadDetector;->b:[F

    invoke-virtual {v0, v1}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->addQuadrilateral([F)V

    .line 29
    sget-object v1, Lcom/google/bionics/scanner/rectifier/QuadDetector;->c:[F

    invoke-virtual {v0, v1}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->addQuadrilateral([F)V

    .line 31
    iget v1, p0, Lcom/google/bionics/scanner/rectifier/ImageData;->height:I

    int-to-float v1, v1

    const/high16 v2, 0x43f00000    # 480.0f

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->applyScale(F)V

    .line 32
    return-object v0
.end method
