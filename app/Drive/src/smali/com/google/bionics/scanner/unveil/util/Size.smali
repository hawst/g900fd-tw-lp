.class public Lcom/google/bionics/scanner/unveil/util/Size;
.super Ljava/lang/Object;
.source "Size.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/bionics/scanner/unveil/util/Size;",
        ">;"
    }
.end annotation


# static fields
.field public static final serialVersionUID:J = 0x6ab7b04cf27fbe29L


# instance fields
.field public final height:I

.field public final width:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput p1, p0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    .line 32
    iput p2, p0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    .line 42
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/hardware/Camera$Size;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iget v0, p1, Landroid/hardware/Camera$Size;->width:I

    iput v0, p0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    .line 37
    iget v0, p1, Landroid/hardware/Camera$Size;->height:I

    iput v0, p0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    .line 38
    return-void
.end method

.method public static final dimensionsAsString(II)Ljava/lang/String;
    .locals 2

    .prologue
    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getRotatedSize(Lcom/google/bionics/scanner/unveil/util/Size;I)Lcom/google/bionics/scanner/unveil/util/Size;
    .locals 3

    .prologue
    .line 52
    rem-int/lit16 v0, p1, 0xb4

    if-eqz v0, :cond_0

    .line 54
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Size;

    iget v1, p0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    iget v2, p0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    invoke-direct {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Size;-><init>(II)V

    move-object p0, v0

    .line 56
    :cond_0
    return-object p0
.end method

.method public static parseFromString(Ljava/lang/String;)Lcom/google/bionics/scanner/unveil/util/Size;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 77
    :cond_0
    :goto_0
    return-object v0

    .line 64
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 67
    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 68
    array-length v2, v1

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 70
    const/4 v2, 0x0

    :try_start_0
    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 71
    const/4 v3, 0x1

    aget-object v1, v1, v3

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 72
    new-instance v1, Lcom/google/bionics/scanner/unveil/util/Size;

    invoke-direct {v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Size;-><init>(II)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    .line 73
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static sizeListToString(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/scanner/unveil/util/Size;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 96
    const-string v0, ""

    .line 97
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 98
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/util/Size;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/util/Size;->toString()Ljava/lang/String;

    move-result-object v1

    .line 99
    const/4 v0, 0x1

    move v3, v0

    move-object v0, v1

    move v1, v3

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 100
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/util/Size;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/util/Size;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 99
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v2

    goto :goto_0

    .line 103
    :cond_0
    return-object v0
.end method

.method public static sizeStringToList(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/scanner/unveil/util/Size;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 83
    if-eqz p0, :cond_1

    .line 84
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 85
    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 86
    invoke-static {v4}, Lcom/google/bionics/scanner/unveil/util/Size;->parseFromString(Ljava/lang/String;)Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v4

    .line 87
    if-eqz v4, :cond_0

    .line 88
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 92
    :cond_1
    return-object v1
.end method


# virtual methods
.method public final aspectRatio()F
    .locals 2

    .prologue
    .line 107
    iget v0, p0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public compareTo(Lcom/google/bionics/scanner/unveil/util/Size;)I
    .locals 3

    .prologue
    .line 112
    iget v0, p0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    iget v1, p0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    mul-int/2addr v0, v1

    iget v1, p1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    iget v2, p1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    mul-int/2addr v1, v2

    sub-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 21
    check-cast p1, Lcom/google/bionics/scanner/unveil/util/Size;

    invoke-virtual {p0, p1}, Lcom/google/bionics/scanner/unveil/util/Size;->compareTo(Lcom/google/bionics/scanner/unveil/util/Size;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 117
    if-nez p1, :cond_1

    .line 126
    :cond_0
    :goto_0
    return v0

    .line 121
    :cond_1
    instance-of v1, p1, Lcom/google/bionics/scanner/unveil/util/Size;

    if-eqz v1, :cond_0

    .line 125
    check-cast p1, Lcom/google/bionics/scanner/unveil/util/Size;

    .line 126
    iget v1, p0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    iget v2, p1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    iget v2, p1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 131
    iget v0, p0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    mul-int/lit16 v0, v0, 0x7fc9

    iget v1, p0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 136
    iget v0, p0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    iget v1, p0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    invoke-static {v0, v1}, Lcom/google/bionics/scanner/unveil/util/Size;->dimensionsAsString(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
