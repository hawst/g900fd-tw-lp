.class public Lcom/google/bionics/scanner/EditorActivity;
.super Lbgf;
.source "EditorActivity.java"

# interfaces
.implements LbgU;
.implements Lbgi;
.implements Lbhi;
.implements Lbhn;


# static fields
.field private static final a:Lcom/google/bionics/scanner/unveil/util/Logger;


# instance fields
.field private a:Landroid/app/AlertDialog;

.field private a:Landroid/content/SharedPreferences;

.field private a:Landroid/widget/ProgressBar;

.field private a:Landroid/widget/ViewSwitcher;

.field private a:LbgZ;

.field private a:Lbgx;

.field private a:Lbhf;

.field private a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

.field private a:Lcom/google/bionics/scanner/ui/DocumentEditorView;

.field private a:Lcom/google/bionics/scanner/ui/QuadEditorView;

.field private a:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

.field private a:Ljava/lang/String;

.field private b:Lbgx;

.field private b:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

.field private c:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 63
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    const-class v1, Lcom/google/bionics/scanner/EditorActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Lbgf;-><init>()V

    .line 91
    sget-object v0, Lbgx;->b:Lbgx;

    iput-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbgx;

    .line 494
    return-void
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/EditorActivity;)Lbgx;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbgx;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/EditorActivity;Lbgx;)Lbgx;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/bionics/scanner/EditorActivity;->b:Lbgx;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/EditorActivity;)Lbhf;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbhf;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/EditorActivity;)Lcom/google/bionics/scanner/ui/DocumentEditorView;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/ui/DocumentEditorView;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/EditorActivity;)Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->c:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    return-object v0
.end method

.method private a()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 722
    .line 724
    :try_start_0
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 729
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->labelRes:I

    invoke-virtual {p0, v1}, Lcom/google/bionics/scanner/EditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 730
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%s v%s by Google"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    const/4 v1, 0x1

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    aput-object v0, v4, v1

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 725
    :catch_0
    move-exception v0

    .line 726
    sget-object v1, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Failed to retrieve app package info, falling back on default producer"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 727
    const-string v0, "Doc Scanner by Google"

    goto :goto_0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 189
    const-string v0, "DIALOG_STATE_DELETE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 190
    invoke-direct {p0}, Lcom/google/bionics/scanner/EditorActivity;->h()V

    .line 192
    :cond_0
    return-void
.end method

.method private a(LbgY;)V
    .locals 2

    .prologue
    .line 540
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/ui/QuadEditorView;

    invoke-virtual {v0, p1}, Lcom/google/bionics/scanner/ui/QuadEditorView;->setPage(LbgY;)V

    .line 541
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    if-nez v0, :cond_0

    .line 542
    new-instance v0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    invoke-virtual {p1}, LbgY;->a()Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;-><init>(Lcom/google/bionics/scanner/rectifier/Quadrilateral;)V

    iput-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    .line 544
    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/ui/QuadEditorView;

    iget-object v1, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    invoke-virtual {v0, v1}, Lcom/google/bionics/scanner/ui/QuadEditorView;->setQuadrilateral(Lcom/google/bionics/scanner/rectifier/Quadrilateral;)V

    .line 545
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/ui/QuadEditorView;

    invoke-direct {p0}, Lcom/google/bionics/scanner/EditorActivity;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/bionics/scanner/ui/QuadEditorView;->a(I)V

    .line 546
    return-void
.end method

.method private a(Lbgx;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 460
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->b:Lbgx;

    if-ne v0, p1, :cond_1

    .line 488
    :cond_0
    :goto_0
    return-void

    .line 463
    :cond_1
    sget-object v0, Lbgx;->a:Lbgx;

    if-ne p1, v0, :cond_3

    .line 464
    sget-object v0, Lbgx;->a:Lbgx;

    iput-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->b:Lbgx;

    .line 466
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbhf;

    invoke-virtual {v0}, Lbhf;->a()LbgX;

    move-result-object v0

    iget-object v1, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/ui/DocumentEditorView;

    invoke-virtual {v1}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a()I

    move-result v1

    invoke-virtual {v0, v1}, LbgX;->a(I)LbgY;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/bionics/scanner/EditorActivity;->a(LbgY;)V

    .line 467
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/widget/ViewSwitcher;

    sget v1, Lcom/google/bionics/scanner/docscanner/R$anim;->ds_shrink_fade_out_center:I

    invoke-virtual {v0, p0, v1}, Landroid/widget/ViewSwitcher;->setOutAnimation(Landroid/content/Context;I)V

    .line 468
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/widget/ViewSwitcher;

    sget v1, Lcom/google/bionics/scanner/docscanner/R$anim;->ds_grow_fade_in_center:I

    invoke-virtual {v0, p0, v1}, Landroid/widget/ViewSwitcher;->setInAnimation(Landroid/content/Context;I)V

    .line 469
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/ui/QuadEditorView;

    if-eq v0, v1, :cond_2

    .line 470
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->showNext()V

    .line 472
    :cond_2
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->setVisibility(I)V

    .line 473
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->a()Lbhm;

    move-result-object v0

    invoke-virtual {v0, v2}, Lbhm;->a(Z)V

    .line 474
    sget v0, Lcom/google/bionics/scanner/docscanner/R$string;->ds_menu_crop:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/EditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v2}, Lcom/google/bionics/scanner/EditorActivity;->a(Ljava/lang/String;Z)V

    .line 475
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->c:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    sget v1, Lcom/google/bionics/scanner/docscanner/R$string;->ds_edit_control_confirm_crop:I

    invoke-static {v0, v1}, Lbgz;->a(Landroid/view/View;I)V

    goto :goto_0

    .line 476
    :cond_3
    sget-object v0, Lbgx;->b:Lbgx;

    if-ne p1, v0, :cond_0

    .line 477
    sget-object v0, Lbgx;->b:Lbgx;

    iput-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->b:Lbgx;

    .line 478
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/widget/ViewSwitcher;

    sget v1, Lcom/google/bionics/scanner/docscanner/R$anim;->ds_shrink_fade_out_center:I

    invoke-virtual {v0, p0, v1}, Landroid/widget/ViewSwitcher;->setOutAnimation(Landroid/content/Context;I)V

    .line 479
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/widget/ViewSwitcher;

    sget v1, Lcom/google/bionics/scanner/docscanner/R$anim;->ds_grow_fade_in_center:I

    invoke-virtual {v0, p0, v1}, Landroid/widget/ViewSwitcher;->setInAnimation(Landroid/content/Context;I)V

    .line 480
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/ui/DocumentEditorView;

    if-eq v0, v1, :cond_4

    .line 481
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->showPrevious()V

    .line 483
    :cond_4
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    invoke-virtual {v0, v2}, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->setVisibility(I)V

    .line 484
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->a()Lbhm;

    move-result-object v0

    invoke-virtual {v0, v3}, Lbhm;->a(Z)V

    .line 485
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v3}, Lcom/google/bionics/scanner/EditorActivity;->a(Ljava/lang/String;Z)V

    .line 486
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->c:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    sget v1, Lcom/google/bionics/scanner/docscanner/R$string;->ds_edit_control_finish_document:I

    invoke-static {v0, v1}, Lbgz;->a(Landroid/view/View;I)V

    goto/16 :goto_0
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/EditorActivity;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/bionics/scanner/EditorActivity;->i()V

    return-void
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/EditorActivity;LbgY;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/google/bionics/scanner/EditorActivity;->a(LbgY;)V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 789
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 790
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    .line 791
    if-eqz v1, :cond_0

    .line 792
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->title:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 793
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 794
    invoke-virtual {v1, p2}, Landroid/view/View;->setClickable(Z)V

    .line 795
    invoke-virtual {v1, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 798
    :cond_0
    return-void
.end method

.method private a(ZI)V
    .locals 3

    .prologue
    .line 630
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 633
    const-string v1, "com.google.bionics.scanner.extra.DOCUMENT_PAGE_INDEX"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 634
    const-string v1, "com.google.bionics.scanner.extra.BACK_BUTTON"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 635
    const-string v1, "SAVED_INSTANCE_FILE_NAME"

    iget-object v2, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 636
    const/16 v1, 0x15

    invoke-virtual {p0, v1, v0}, Lcom/google/bionics/scanner/EditorActivity;->setResult(ILandroid/content/Intent;)V

    .line 637
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->finish()V

    .line 638
    return-void
.end method

.method private b()I
    .locals 5

    .prologue
    const/16 v1, 0x5a

    const/4 v4, 0x2

    const/4 v0, 0x0

    .line 559
    invoke-static {p0}, Lcom/google/bionics/scanner/unveil/ui/Viewport;->computeNaturalOrientation(Landroid/content/Context;)I

    move-result v2

    .line 560
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getRotation()I

    move-result v3

    .line 562
    packed-switch v3, :pswitch_data_0

    .line 579
    :cond_0
    :goto_0
    return v0

    .line 564
    :pswitch_0
    if-eq v2, v4, :cond_0

    move v0, v1

    goto :goto_0

    .line 567
    :pswitch_1
    if-ne v2, v4, :cond_0

    const/16 v0, 0x10e

    goto :goto_0

    .line 573
    :pswitch_2
    if-eq v2, v4, :cond_0

    move v0, v1

    goto :goto_0

    .line 576
    :pswitch_3
    if-ne v2, v4, :cond_1

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_1
    const/16 v1, 0xb4

    goto :goto_1

    .line 562
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static synthetic b(Lcom/google/bionics/scanner/EditorActivity;)Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->b:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    return-object v0
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    const-string v0, "DIALOG_STATE_DELETE"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 198
    :cond_0
    return-void
.end method

.method private b(LbgY;)V
    .locals 2

    .prologue
    .line 686
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 687
    invoke-virtual {p1}, LbgY;->a()Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    move-result-object v0

    .line 688
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->a()Lbhm;

    move-result-object v1

    invoke-virtual {v1, v0}, Lbhm;->a(Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;)V

    .line 689
    iget-object v1, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbhf;

    invoke-virtual {v1, p1, v0, p0}, Lbhf;->a(LbgY;Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;Lbhi;)V

    .line 690
    return-void
.end method

.method public static synthetic b(Lcom/google/bionics/scanner/EditorActivity;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/bionics/scanner/EditorActivity;->k()V

    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 601
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 605
    const-string v1, "com.google.bionics.scanner.extra.BACK_BUTTON"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 606
    const-string v1, "SAVED_INSTANCE_FILE_NAME"

    iget-object v2, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 607
    const/16 v1, 0xb

    invoke-virtual {p0, v1, v0}, Lcom/google/bionics/scanner/EditorActivity;->setResult(ILandroid/content/Intent;)V

    .line 608
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->finish()V

    .line 609
    return-void
.end method

.method private g()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/16 v2, 0x10

    .line 161
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 164
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 166
    invoke-virtual {v0, v2, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 169
    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    .line 170
    sget v1, Lcom/google/bionics/scanner/docscanner/R$layout;->ds_actionbar_title:I

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 171
    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/bionics/scanner/docscanner/R$string;->ds_click_to_rename:I

    invoke-static {v1, v2}, Lbgz;->a(Landroid/view/View;I)V

    .line 172
    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    new-instance v2, Lbgr;

    invoke-direct {v2, p0}, Lbgr;-><init>(Lcom/google/bionics/scanner/EditorActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    iget-object v1, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/bionics/scanner/EditorActivity;->b(Ljava/lang/String;)V

    .line 181
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 182
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_1

    .line 183
    sget v1, Lcom/google/bionics/scanner/docscanner/R$drawable;->ds_actionbar_close_button:I

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setLogo(I)V

    .line 184
    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeAsUpIndicator(I)V

    .line 186
    :cond_1
    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    .line 202
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 203
    sget v1, Lcom/google/bionics/scanner/docscanner/R$string;->ds_dialog_msg_delete_page:I

    .line 204
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/bionics/scanner/docscanner/R$string;->ds_dialog_ok_button_text:I

    new-instance v3, Lbgs;

    invoke-direct {v3, p0}, Lbgs;-><init>(Lcom/google/bionics/scanner/EditorActivity;)V

    .line 205
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/bionics/scanner/docscanner/R$string;->ds_dialog_cancel_button_text:I

    const/4 v3, 0x0

    .line 212
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 213
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/app/AlertDialog;

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 216
    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/bionics/scanner/ui/RenameDialogFragment;->a(Ljava/lang/String;)Lcom/google/bionics/scanner/ui/RenameDialogFragment;

    move-result-object v0

    .line 220
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->a()LM;

    move-result-object v1

    const-string v2, "Dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/ui/RenameDialogFragment;->a(LM;Ljava/lang/String;)V

    .line 221
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    const/high16 v1, 0x1000000

    .line 229
    sget v0, Lcom/google/bionics/scanner/docscanner/R$style;->ds_scanner_editor_theme:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/EditorActivity;->setTheme(I)V

    .line 230
    sget v0, Lcom/google/bionics/scanner/docscanner/R$string;->ds_title_activity_editor:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/EditorActivity;->setTitle(I)V

    .line 231
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 233
    return-void
.end method

.method private k()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 438
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/ui/DocumentEditorView;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a()I

    move-result v0

    .line 439
    sget-object v1, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "deleting page #%d..."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 440
    iget-object v1, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbhf;

    invoke-virtual {v1}, Lbhf;->a()LbgX;

    move-result-object v1

    invoke-virtual {v1, v0}, LbgX;->a(I)V

    .line 441
    iget-object v1, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbhf;

    invoke-virtual {v1}, Lbhf;->a()LbgX;

    move-result-object v1

    invoke-virtual {v1}, LbgX;->b()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 442
    add-int/lit8 v0, v0, -0x1

    .line 444
    :cond_0
    iget-object v1, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/ui/DocumentEditorView;

    invoke-virtual {v1, v0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->setActivePageIndex(I)V

    .line 445
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbhf;

    invoke-virtual {v0}, Lbhf;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 448
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->c:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    invoke-virtual {v0, v6}, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->setVisibility(I)V

    .line 449
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->b:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    invoke-virtual {v0, v6}, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->setVisibility(I)V

    .line 450
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->a()Lbhm;

    move-result-object v0

    invoke-virtual {v0, v5}, Lbhm;->b(Z)V

    .line 452
    :cond_1
    return-void
.end method

.method private l()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/16 v7, 0x8

    .line 737
    sget-object v0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Starting to create PDF file..."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 738
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 739
    new-instance v2, Lbha;

    iget-object v3, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbhf;

    invoke-direct {p0}, Lcom/google/bionics/scanner/EditorActivity;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lbha;-><init>(Lbhf;Ljava/lang/String;)V

    .line 740
    iget-object v3, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/content/SharedPreferences;

    sget v4, Lcom/google/bionics/scanner/docscanner/R$string;->ds_pdf_paper_size_key:I

    .line 741
    invoke-virtual {p0, v4}, Lcom/google/bionics/scanner/EditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 742
    invoke-static {p0}, LbgB;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 740
    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 743
    iget-object v4, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/content/SharedPreferences;

    sget v5, Lcom/google/bionics/scanner/docscanner/R$string;->ds_pdf_paper_orientation_key:I

    .line 744
    invoke-virtual {p0, v5}, Lcom/google/bionics/scanner/EditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v6, Lcom/google/bionics/scanner/docscanner/R$string;->ds_pdf_paper_orientation_default:I

    .line 745
    invoke-virtual {p0, v6}, Lcom/google/bionics/scanner/EditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 743
    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 749
    :try_start_0
    invoke-static {v3}, LbgM;->a(Ljava/lang/String;)LbgM;

    move-result-object v3

    .line 750
    invoke-static {v4}, Lbhc;->valueOf(Ljava/lang/String;)Lbhc;

    move-result-object v4

    iget-object v5, p0, Lcom/google/bionics/scanner/EditorActivity;->a:LbgZ;

    iget-object v6, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Ljava/lang/String;

    .line 751
    invoke-virtual {v5, v6}, LbgZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 748
    invoke-virtual {v2, v3, v4, v5}, Lbha;->a(LbgM;Lbhc;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 752
    sget-object v3, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v4, "Creation of PDF file...DONE: %d ms"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v0, v8, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 753
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 754
    const-string v1, "com.google.bionics.scanner.extra.FILE_PATH"

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 755
    const-string v1, "com.google.bionics.scanner.extra.FILE_SIZE"

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 756
    const-string v1, "com.google.bionics.scanner.extra.FILE_TYPE"

    const-string v2, "pdf"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 757
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/bionics/scanner/EditorActivity;->setResult(ILandroid/content/Intent;)V

    .line 758
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->finish()V
    :try_end_0
    .catch Lbhl; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 768
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 770
    :goto_0
    return-void

    .line 759
    :catch_0
    move-exception v0

    .line 760
    :try_start_1
    sget-object v0, Lbhk;->b:Lbhk;

    new-instance v1, Lbgv;

    invoke-direct {v1, p0}, Lbgv;-><init>(Lcom/google/bionics/scanner/EditorActivity;)V

    invoke-static {p0, v0, v1}, Lbhf;->a(Landroid/content/Context;Lbhk;Lbhj;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 768
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 646
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/EditorActivity;->setResult(I)V

    .line 647
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->finish()V

    .line 648
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 775
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->a()Lbhm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbhf;

    invoke-virtual {v1}, Lbhf;->a()LbgX;

    move-result-object v1

    invoke-virtual {v1, p1}, LbgX;->a(I)LbgY;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbhm;->a(LbgY;)V

    .line 776
    return-void
.end method

.method public a(Lbhk;LbgY;)V
    .locals 5

    .prologue
    .line 694
    sget-object v0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Rectifying and storing images finished with status: "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lbhk;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 695
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 696
    sget-object v0, Lbhk;->a:Lbhk;

    if-ne p1, v0, :cond_2

    .line 697
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->b:Lbgx;

    sget-object v1, Lbgx;->a:Lbgx;

    if-ne v0, v1, :cond_1

    .line 698
    sget-object v0, Lbgx;->b:Lbgx;

    invoke-direct {p0, v0}, Lcom/google/bionics/scanner/EditorActivity;->a(Lbgx;)V

    .line 710
    :cond_0
    :goto_0
    return-void

    .line 699
    :cond_1
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->b:Lbgx;

    sget-object v1, Lbgx;->b:Lbgx;

    if-ne v0, v1, :cond_0

    .line 700
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/ui/DocumentEditorView;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->invalidate()V

    goto :goto_0

    .line 703
    :cond_2
    sget-object v0, Lbhk;->b:Lbhk;

    new-instance v1, Lbgu;

    invoke-direct {v1, p0}, Lbgu;-><init>(Lcom/google/bionics/scanner/EditorActivity;)V

    invoke-static {p0, v0, v1}, Lbhf;->a(Landroid/content/Context;Lbhk;Lbhj;)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 783
    iput-object p1, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Ljava/lang/String;

    .line 784
    invoke-direct {p0, p1, v2}, Lcom/google/bionics/scanner/EditorActivity;->a(Ljava/lang/String;Z)V

    .line 785
    sget-object v0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "setScanFileName called. Title: %s"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 786
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 714
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->b:Lbgx;

    sget-object v1, Lbgx;->c:Lbgx;

    if-ne v0, v1, :cond_0

    .line 717
    invoke-direct {p0}, Lcom/google/bionics/scanner/EditorActivity;->l()V

    .line 719
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 100
    sget-object v0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "onCreate"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 101
    sget v0, Lcom/google/bionics/scanner/docscanner/R$style;->ds_scanner_editor_theme:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/EditorActivity;->setTheme(I)V

    .line 102
    invoke-super {p0, p1}, Lbgf;->onCreate(Landroid/os/Bundle;)V

    .line 103
    invoke-direct {p0}, Lcom/google/bionics/scanner/EditorActivity;->j()V

    .line 104
    sget v0, Lcom/google/bionics/scanner/docscanner/R$layout;->ds_editor_activity:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/EditorActivity;->setContentView(I)V

    .line 105
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->view_switcher:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/EditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewSwitcher;

    iput-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/widget/ViewSwitcher;

    .line 106
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0, v3}, Landroid/widget/ViewSwitcher;->setAnimateFirstView(Z)V

    .line 108
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_document_editor:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/EditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/ui/DocumentEditorView;

    iput-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/ui/DocumentEditorView;

    .line 109
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_quad_editor:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/EditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/ui/QuadEditorView;

    iput-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/ui/QuadEditorView;

    .line 110
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/ui/QuadEditorView;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/ui/QuadEditorView;->a()V

    .line 112
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_add_button:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/EditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    iput-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    .line 113
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    sget v1, Lcom/google/bionics/scanner/docscanner/R$string;->ds_edit_control_add_page:I

    invoke-static {v0, v1}, Lbgz;->a(Landroid/view/View;I)V

    .line 114
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_retake_button:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/EditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    iput-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->b:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    .line 115
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->b:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    sget v1, Lcom/google/bionics/scanner/docscanner/R$string;->ds_edit_control_retake_page:I

    invoke-static {v0, v1}, Lbgz;->a(Landroid/view/View;I)V

    .line 116
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_finish_button:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/EditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    iput-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->c:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    .line 117
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->c:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    sget v1, Lcom/google/bionics/scanner/docscanner/R$string;->ds_edit_control_finish_document:I

    invoke-static {v0, v1}, Lbgz;->a(Landroid/view/View;I)V

    .line 119
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/content/SharedPreferences;

    .line 121
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_progess_bar:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/EditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/widget/ProgressBar;

    .line 124
    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/content/SharedPreferences;

    invoke-static {p0, v0}, Lbhf;->a(Landroid/content/Context;Landroid/content/SharedPreferences;)Lbhf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbhf;

    .line 125
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbhf;

    invoke-virtual {v0, p0}, Lbhf;->a(LbgU;)V

    .line 126
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/ui/DocumentEditorView;

    invoke-virtual {v0, p0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a(Lbhn;)V

    .line 127
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/ui/DocumentEditorView;

    iget-object v1, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbhf;

    invoke-virtual {v1}, Lbhf;->a()LbgX;

    move-result-object v1

    invoke-virtual {v1}, LbgX;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->setActivePageIndex(I)V

    .line 128
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbhf;

    invoke-virtual {v0}, Lbhf;->a()LbgZ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:LbgZ;

    .line 129
    if-eqz p1, :cond_1

    .line 130
    const-string v0, "SAVED_INSTANCE_FILE_NAME"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Ljava/lang/String;

    .line 131
    sget-object v0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "onCreate: savedInstanceState != null: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 132
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:LbgZ;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LbgZ;->d(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/EditorActivity;->b(Ljava/lang/String;)V

    .line 135
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/bionics/scanner/EditorActivity;->a(Landroid/os/Bundle;)V

    .line 157
    :goto_0
    return-void

    .line 137
    :cond_1
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "SAVED_INSTANCE_FILE_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 139
    sget-object v1, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "handleIntent: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 140
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 141
    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/EditorActivity;->b(Ljava/lang/String;)V

    .line 145
    :goto_1
    sget-object v0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "onCreate: savedInstanceState == null: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lbhl; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 147
    :catch_0
    move-exception v0

    .line 148
    sget-object v0, Lbhk;->b:Lbhk;

    new-instance v1, Lbgq;

    invoke-direct {v1, p0}, Lbgq;-><init>(Lcom/google/bionics/scanner/EditorActivity;)V

    invoke-static {p0, v0, v1}, Lbhf;->a(Landroid/content/Context;Lbhk;Lbhj;)V

    goto :goto_0

    .line 143
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:LbgZ;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LbgZ;->d(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/EditorActivity;->b(Ljava/lang/String;)V
    :try_end_1
    .catch Lbhl; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/ui/DocumentEditorView;

    invoke-virtual {v0, p0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a(Lbhn;)V

    .line 238
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 239
    sget v1, Lcom/google/bionics/scanner/docscanner/R$menu;->ds_menu_editor:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 242
    invoke-super {p0, p1}, Lbgf;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 243
    new-instance v1, Lbgy;

    invoke-direct {v1, p0}, Lbgy;-><init>(Lcom/google/bionics/scanner/EditorActivity;)V

    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbgy;->a(Landroid/content/Intent;)V

    .line 244
    iget-object v1, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbgx;

    invoke-direct {p0, v1}, Lcom/google/bionics/scanner/EditorActivity;->a(Lbgx;)V

    .line 245
    return v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 402
    sget-object v0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "onDestroy"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 403
    invoke-super {p0}, Lbgf;->onDestroy()V

    .line 404
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbhf;

    if-eqz v0, :cond_0

    .line 405
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbhf;

    invoke-virtual {v0}, Lbhf;->a()V

    .line 407
    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/ui/QuadEditorView;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/ui/QuadEditorView;->b()V

    .line 408
    return-void
.end method

.method public onEditAddClicked(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 588
    sget-object v0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "onEditAddClicked"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 589
    invoke-direct {p0, v3}, Lcom/google/bionics/scanner/EditorActivity;->b(Z)V

    .line 590
    return-void
.end method

.method public onEditDoneClicked(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 656
    sget-object v0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "onEditDoneClicked"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 657
    sget-object v0, Lbgw;->a:[I

    iget-object v1, p0, Lcom/google/bionics/scanner/EditorActivity;->b:Lbgx;

    invoke-virtual {v1}, Lbgx;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 677
    :goto_0
    return-void

    .line 661
    :pswitch_0
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbhf;

    invoke-virtual {v0}, Lbhf;->a()LbgX;

    move-result-object v0

    iget-object v1, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/ui/DocumentEditorView;

    invoke-virtual {v1}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a()I

    move-result v1

    invoke-virtual {v0, v1}, LbgX;->a(I)LbgY;

    move-result-object v0

    .line 662
    iget-object v1, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    invoke-virtual {v0, v1}, LbgY;->a(Lcom/google/bionics/scanner/rectifier/Quadrilateral;)LbgY;

    .line 663
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    .line 664
    invoke-direct {p0, v0}, Lcom/google/bionics/scanner/EditorActivity;->b(LbgY;)V

    goto :goto_0

    .line 669
    :pswitch_1
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 670
    sget-object v0, Lbgx;->c:Lbgx;

    iput-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->b:Lbgx;

    .line 671
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbhf;

    invoke-virtual {v0, p0}, Lbhf;->a(LbgU;)V

    goto :goto_0

    .line 657
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 412
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-eqz v1, :cond_0

    .line 413
    invoke-super {p0, p1, p2}, Lbgf;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 431
    :goto_0
    return v0

    .line 415
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 431
    invoke-super {p0, p1, p2}, Lbgf;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 417
    :pswitch_0
    iget-object v1, p0, Lcom/google/bionics/scanner/EditorActivity;->b:Lbgx;

    sget-object v2, Lbgx;->b:Lbgx;

    if-ne v1, v2, :cond_2

    .line 419
    iget-object v1, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbhf;

    invoke-virtual {v1}, Lbhf;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 420
    iget-object v1, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbhf;

    invoke-virtual {v1}, Lbhf;->a()LbgX;

    move-result-object v1

    invoke-virtual {v1}, LbgX;->b()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/bionics/scanner/EditorActivity;->a(ZI)V

    goto :goto_0

    .line 422
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/bionics/scanner/EditorActivity;->b(Z)V

    goto :goto_0

    .line 426
    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    .line 427
    sget-object v1, Lbgx;->b:Lbgx;

    invoke-direct {p0, v1}, Lcom/google/bionics/scanner/EditorActivity;->a(Lbgx;)V

    goto :goto_0

    .line 415
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 270
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 271
    sget v2, Lcom/google/bionics/scanner/docscanner/R$id;->ds_menu_settings:I

    if-ne v1, v2, :cond_0

    .line 272
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/bionics/scanner/PreferencesActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 273
    invoke-virtual {p0, v1}, Lcom/google/bionics/scanner/EditorActivity;->startActivity(Landroid/content/Intent;)V

    .line 328
    :goto_0
    return v0

    .line 277
    :cond_0
    sget v2, Lcom/google/bionics/scanner/docscanner/R$id;->ds_menu_rename_scan:I

    if-ne v1, v2, :cond_1

    .line 278
    invoke-direct {p0}, Lcom/google/bionics/scanner/EditorActivity;->i()V

    goto :goto_0

    .line 283
    :cond_1
    const v2, 0x102002c

    if-ne v1, v2, :cond_2

    .line 284
    iget-object v2, p0, Lcom/google/bionics/scanner/EditorActivity;->b:Lbgx;

    sget-object v3, Lbgx;->b:Lbgx;

    if-ne v2, v3, :cond_4

    .line 285
    iget-object v2, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbhf;

    invoke-virtual {v2}, Lbhf;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 287
    invoke-virtual {p0, p0}, Lcom/google/bionics/scanner/EditorActivity;->a(Lbgi;)V

    .line 299
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbhf;

    invoke-virtual {v2}, Lbhf;->a()Z

    move-result v2

    if-nez v2, :cond_5

    .line 302
    const/4 v0, 0x0

    goto :goto_0

    .line 290
    :cond_3
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->a()V

    goto :goto_1

    .line 292
    :cond_4
    iget-object v2, p0, Lcom/google/bionics/scanner/EditorActivity;->b:Lbgx;

    sget-object v3, Lbgx;->a:Lbgx;

    if-ne v2, v3, :cond_2

    .line 295
    sget-object v2, Lbgx;->b:Lbgx;

    invoke-direct {p0, v2}, Lcom/google/bionics/scanner/EditorActivity;->a(Lbgx;)V

    goto :goto_1

    .line 305
    :cond_5
    sget v2, Lcom/google/bionics/scanner/docscanner/R$id;->ds_menu_edit_page:I

    if-ne v1, v2, :cond_6

    iget-object v2, p0, Lcom/google/bionics/scanner/EditorActivity;->b:Lbgx;

    sget-object v3, Lbgx;->b:Lbgx;

    if-ne v2, v3, :cond_6

    .line 307
    sget-object v1, Lbgx;->a:Lbgx;

    invoke-direct {p0, v1}, Lcom/google/bionics/scanner/EditorActivity;->a(Lbgx;)V

    goto :goto_0

    .line 311
    :cond_6
    iget-object v2, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbhf;

    invoke-virtual {v2}, Lbhf;->a()LbgX;

    move-result-object v2

    iget-object v3, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/ui/DocumentEditorView;

    invoke-virtual {v3}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a()I

    move-result v3

    invoke-virtual {v2, v3}, LbgX;->a(I)LbgY;

    move-result-object v2

    .line 312
    sget v3, Lcom/google/bionics/scanner/docscanner/R$id;->ds_menu_delete_page:I

    if-ne v1, v3, :cond_7

    .line 313
    invoke-direct {p0}, Lcom/google/bionics/scanner/EditorActivity;->h()V

    goto :goto_0

    .line 315
    :cond_7
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_menu_enhance_none:I

    if-ne v1, v0, :cond_9

    .line 316
    sget-object v0, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->NONE:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    invoke-virtual {v2, v0}, LbgY;->a(Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;)LbgY;

    .line 317
    invoke-direct {p0, v2}, Lcom/google/bionics/scanner/EditorActivity;->b(LbgY;)V

    .line 328
    :cond_8
    :goto_2
    invoke-super {p0, p1}, Lbgf;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 318
    :cond_9
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_menu_enhance_color:I

    if-ne v1, v0, :cond_a

    .line 319
    sget-object v0, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->OPTIMIZE_FOR_COLOR:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    invoke-virtual {v2, v0}, LbgY;->a(Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;)LbgY;

    .line 320
    invoke-direct {p0, v2}, Lcom/google/bionics/scanner/EditorActivity;->b(LbgY;)V

    goto :goto_2

    .line 321
    :cond_a
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_menu_enhance_bw:I

    if-ne v1, v0, :cond_b

    .line 322
    sget-object v0, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->OPTIMIZE_FOR_BW:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    invoke-virtual {v2, v0}, LbgY;->a(Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;)LbgY;

    .line 323
    invoke-direct {p0, v2}, Lcom/google/bionics/scanner/EditorActivity;->b(LbgY;)V

    goto :goto_2

    .line 324
    :cond_b
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->ds_menu_enhance_color_drawing:I

    if-ne v1, v0, :cond_8

    .line 325
    sget-object v0, Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;->OPTIMIZE_FOR_COLOR_DRAWING:Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    invoke-virtual {v2, v0}, LbgY;->a(Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;)LbgY;

    .line 326
    invoke-direct {p0, v2}, Lcom/google/bionics/scanner/EditorActivity;->b(LbgY;)V

    goto :goto_2
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 352
    sget-object v0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "onPause"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 353
    invoke-super {p0}, Lbgf;->onPause()V

    .line 354
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->e()V

    .line 355
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 250
    invoke-super {p0, p1}, Lbgf;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 253
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->b:Lbgx;

    sget-object v1, Lbgx;->b:Lbgx;

    if-ne v0, v1, :cond_2

    .line 254
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->a()Lbhm;

    move-result-object v0

    invoke-virtual {v0, v3}, Lbhm;->a(Z)V

    .line 255
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbhf;

    invoke-virtual {v0}, Lbhf;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 256
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->a()Lbhm;

    move-result-object v0

    invoke-virtual {v0, v3}, Lbhm;->b(Z)V

    .line 257
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->a()Lbhm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbhf;

    invoke-virtual {v1}, Lbhf;->a()LbgX;

    move-result-object v1

    iget-object v2, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/ui/DocumentEditorView;

    .line 258
    invoke-virtual {v2}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a()I

    move-result v2

    invoke-virtual {v1, v2}, LbgX;->a(I)LbgY;

    move-result-object v1

    invoke-virtual {v1}, LbgY;->a()Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;

    move-result-object v1

    .line 257
    invoke-virtual {v0, v1}, Lbhm;->a(Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;)V

    .line 265
    :cond_0
    :goto_0
    return v3

    .line 260
    :cond_1
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->a()Lbhm;

    move-result-object v0

    invoke-virtual {v0, v2}, Lbhm;->b(Z)V

    goto :goto_0

    .line 262
    :cond_2
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->b:Lbgx;

    sget-object v1, Lbgx;->a:Lbgx;

    if-ne v0, v1, :cond_0

    .line 263
    invoke-virtual {p0}, Lcom/google/bionics/scanner/EditorActivity;->a()Lbhm;

    move-result-object v0

    invoke-virtual {v0, v2}, Lbhm;->a(Z)V

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 385
    const-string v0, "SAVED_INSTANCE_EDIT_STATE"

    .line 386
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 387
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lbgx;->b:Lbgx;

    .line 388
    invoke-virtual {v1}, Lbgx;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lbgx;->a:Lbgx;

    .line 389
    invoke-virtual {v1}, Lbgx;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 392
    :cond_0
    invoke-static {v0}, Lbgx;->valueOf(Ljava/lang/String;)Lbgx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbgx;

    .line 394
    :cond_1
    const-string v0, "SAVED_INSTANCE_QUAD_COPY_STATE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    iput-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    .line 395
    const-string v0, "SAVED_INSTANCE_FILE_NAME"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Ljava/lang/String;

    .line 396
    sget-object v0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "onrestoreInstanceState: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 397
    invoke-super {p0, p1}, Lbgf;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 398
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 333
    sget-object v0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "onResume"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 334
    invoke-super {p0}, Lbgf;->onResume()V

    .line 335
    invoke-direct {p0}, Lcom/google/bionics/scanner/EditorActivity;->g()V

    .line 336
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->c:Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;

    new-instance v1, Lbgt;

    invoke-direct {v1, p0}, Lbgt;-><init>(Lcom/google/bionics/scanner/EditorActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/bionics/scanner/unveil/ui/RotatingImageView;->post(Ljava/lang/Runnable;)Z

    .line 348
    return-void
.end method

.method public onRetakeClicked(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 617
    sget-object v0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "onRetakeClicked"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 618
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/ui/DocumentEditorView;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a()I

    move-result v0

    invoke-direct {p0, v3, v0}, Lcom/google/bionics/scanner/EditorActivity;->a(ZI)V

    .line 619
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 368
    invoke-super {p0, p1}, Lbgf;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 371
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->b:Lbgx;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->b:Lbgx;

    invoke-virtual {v0}, Lbgx;->name()Ljava/lang/String;

    move-result-object v0

    .line 372
    :goto_0
    const-string v1, "SAVED_INSTANCE_EDIT_STATE"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    if-eqz v0, :cond_0

    .line 374
    const-string v0, "SAVED_INSTANCE_QUAD_COPY_STATE"

    iget-object v1, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 376
    :cond_0
    sget-object v0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "onSaveInstanceState: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 377
    const-string v0, "SAVED_INSTANCE_FILE_NAME"

    iget-object v1, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    invoke-direct {p0, p1}, Lcom/google/bionics/scanner/EditorActivity;->b(Landroid/os/Bundle;)V

    .line 379
    return-void

    .line 371
    :cond_1
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lbgx;

    invoke-virtual {v0}, Lbgx;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public onStop()V
    .locals 3

    .prologue
    .line 359
    sget-object v0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "onStop"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 360
    invoke-super {p0}, Lbgf;->onStop()V

    .line 361
    iget-object v0, p0, Lcom/google/bionics/scanner/EditorActivity;->a:Lcom/google/bionics/scanner/ui/DocumentEditorView;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a()V

    .line 362
    return-void
.end method
