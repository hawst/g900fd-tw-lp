.class public Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;
.super Ljava/lang/Object;
.source "DownsampledImage.java"


# static fields
.field public static final TARGET_PIXELS:I = 0x6450

.field private static final a:Lcom/google/bionics/scanner/unveil/util/Logger;

.field private static final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[B>;"
        }
    .end annotation
.end field


# instance fields
.field private a:I

.field private final a:J

.field private final a:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>()V

    sput-object v0, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->a:Ljava/util/ArrayList;

    return-void
.end method

.method private constructor <init>([BJ)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->a:[B

    .line 38
    iput-wide p2, p0, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->a:J

    .line 39
    return-void
.end method

.method private static a([B)V
    .locals 3

    .prologue
    .line 92
    sget-object v1, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->a:Ljava/util/ArrayList;

    monitor-enter v1

    .line 93
    :try_start_0
    sget-object v0, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x2

    if-ge v0, v2, :cond_0

    .line 94
    sget-object v0, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    :cond_0
    monitor-exit v1

    .line 97
    return-void

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static a(II)[B
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 70
    .line 71
    invoke-static {p0, p1}, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->getDownsampleFactor(II)I

    move-result v0

    .line 72
    invoke-static {p0, v0}, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->getDownsampledWidth(II)I

    move-result v2

    .line 73
    invoke-static {p1, v0}, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->getDownsampledHeight(II)I

    move-result v0

    mul-int/2addr v2, v0

    .line 76
    sget-object v3, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->a:Ljava/util/ArrayList;

    monitor-enter v3

    move-object v0, v1

    .line 77
    :cond_0
    :goto_0
    :try_start_0
    sget-object v4, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    if-nez v0, :cond_1

    .line 78
    sget-object v0, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->a:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 79
    array-length v4, v0

    if-eq v4, v2, :cond_0

    move-object v0, v1

    .line 80
    goto :goto_0

    .line 83
    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    if-nez v0, :cond_2

    .line 86
    new-array v0, v2, [B

    .line 88
    :cond_2
    return-object v0

    .line 83
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static downsample([BIIJ)Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;
    .locals 3

    .prologue
    .line 122
    invoke-static {p1, p2}, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->a(II)[B

    move-result-object v0

    .line 124
    invoke-static {p1, p2}, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->getDownsampleFactor(II)I

    move-result v1

    .line 125
    invoke-static {p1, p2, p0, v1, v0}, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->downsampleImageNative(II[BI[B)V

    .line 127
    new-instance v1, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;

    invoke-direct {v1, v0, p3, p4}, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;-><init>([BJ)V

    return-object v1
.end method

.method public static getDownsampleFactor(II)I
    .locals 3

    .prologue
    .line 108
    const/4 v0, 0x1

    .line 109
    :goto_0
    invoke-static {p1, v0}, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->getDownsampledHeight(II)I

    move-result v1

    .line 110
    invoke-static {p0, v0}, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->getDownsampledWidth(II)I

    move-result v2

    mul-int/2addr v1, v2

    const/16 v2, 0x6450

    if-le v1, v2, :cond_0

    .line 111
    mul-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 113
    :cond_0
    return v0
.end method

.method public static getDownsampledHeight(II)I
    .locals 1

    .prologue
    .line 104
    add-int v0, p0, p1

    add-int/lit8 v0, v0, -0x1

    div-int/2addr v0, p1

    return v0
.end method

.method public static getDownsampledWidth(II)I
    .locals 1

    .prologue
    .line 100
    add-int v0, p0, p1

    add-int/lit8 v0, v0, -0x1

    div-int/2addr v0, p1

    return v0
.end method


# virtual methods
.method public declared-synchronized addReference()V
    .locals 1

    .prologue
    .line 50
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    monitor-exit p0

    return-void

    .line 50
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected finalize()V
    .locals 3

    .prologue
    .line 64
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->a:I

    if-eqz v0, :cond_0

    .line 65
    sget-object v0, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "DownsampledImage garbage collected with a non-zero reference count."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    :cond_0
    return-void
.end method

.method public getBytes()[B
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->a:[B

    return-object v0
.end method

.method public getTimestamp()J
    .locals 2

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->a:J

    return-wide v0
.end method

.method public declared-synchronized removeReference()V
    .locals 2

    .prologue
    .line 54
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->a:I

    .line 55
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->a:I

    if-nez v0, :cond_1

    .line 56
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 60
    :cond_0
    monitor-exit p0

    return-void

    .line 57
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/DownsampledImage;->a:I

    if-gez v0, :cond_0

    .line 58
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Negative reference count."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
