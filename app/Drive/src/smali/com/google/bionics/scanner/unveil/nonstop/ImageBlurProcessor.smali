.class public Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;
.super Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;
.source "ImageBlurProcessor.java"

# interfaces
.implements Lcom/google/bionics/scanner/unveil/camera/CameraManager$FocusCallback;


# instance fields
.field private a:I

.field private a:J

.field private final a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

.field private final a:Lcom/google/bionics/scanner/unveil/util/Logger;

.field private final a:Lcom/google/bionics/scanner/unveil/util/Stopwatch;

.field private final a:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private a:[I

.field private b:I

.field private b:J

.field private final b:Lcom/google/bionics/scanner/unveil/util/Stopwatch;

.field private b:Z

.field private b:[I

.field private c:J

.field private volatile c:Z

.field private d:J

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/google/bionics/scanner/unveil/camera/CameraManager;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 132
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/FrameProcessor;-><init>()V

    .line 22
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    .line 92
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/Stopwatch;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Lcom/google/bionics/scanner/unveil/util/Stopwatch;

    .line 94
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Stopwatch;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/Stopwatch;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:Lcom/google/bionics/scanner/unveil/util/Stopwatch;

    .line 98
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Ljava/util/Vector;

    .line 101
    iput-boolean v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Z

    .line 105
    iput-boolean v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->c:Z

    .line 106
    iput v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:I

    .line 108
    iput v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:I

    .line 109
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:J

    .line 115
    iput-boolean v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->d:Z

    .line 133
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    .line 134
    return-void
.end method

.method private a()J
    .locals 4

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:[I

    if-nez v0, :cond_0

    .line 336
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Hopefully this is a test!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 337
    const-wide/16 v0, 0x1f40

    .line 368
    :goto_0
    return-wide v0

    .line 340
    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:[I

    array-length v1, v0

    .line 343
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:[I

    invoke-static {v0}, Lcom/google/bionics/scanner/unveil/util/NumberUtils;->getMeanIndex([I)F

    move-result v0

    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v2, v0

    .line 345
    int-to-float v0, v1

    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, v0, v3

    .line 346
    sub-float v0, v2, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 349
    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 350
    const v2, 0x3f99999a    # 1.2f

    mul-float/2addr v0, v2

    .line 354
    :cond_1
    int-to-float v1, v1

    sub-float/2addr v1, v3

    .line 358
    const/high16 v2, 0x3fc00000    # 1.5f

    mul-float/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 366
    div-float/2addr v0, v1

    float-to-double v0, v0

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 368
    const/high16 v1, 0x45fa0000    # 8000.0f

    const v2, 0x45f6e000    # 7900.0f

    mul-float/2addr v0, v2

    sub-float v0, v1, v0

    float-to-long v0, v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 294
    return-void
.end method

.method private a(Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;)Z
    .locals 8

    .prologue
    const/16 v6, 0x8

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 221
    iget-object v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-virtual {v2}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->isFocusSupported()Z

    move-result v2

    if-nez v2, :cond_0

    .line 222
    const-string v1, "Focus not supported!"

    invoke-direct {p0, v1}, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a(Ljava/lang/String;)V

    .line 287
    :goto_0
    return v0

    .line 226
    :cond_0
    iget-boolean v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Z

    if-nez v2, :cond_1

    .line 227
    const-string v0, "Requesting first focus!"

    invoke-direct {p0, v0}, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a(Ljava/lang/String;)V

    move v0, v1

    .line 228
    goto :goto_0

    .line 233
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->c(Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;)V

    .line 234
    iget v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:I

    const/16 v3, 0x1e

    if-le v2, v3, :cond_2

    .line 235
    const-wide/16 v2, -0x96

    iput-wide v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->d:J

    .line 239
    :cond_2
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->c:J

    .line 241
    iget-object v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Lcom/google/bionics/scanner/unveil/util/Stopwatch;

    invoke-virtual {v2}, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->getElapsedMilliseconds()J

    move-result-wide v2

    .line 242
    iget-wide v4, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->d:J

    cmp-long v4, v2, v4

    if-gez v4, :cond_3

    .line 243
    const-string v1, "Not enough time has gone by, not focusing."

    invoke-direct {p0, v1}, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 247
    :cond_3
    iget v4, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:I

    if-lt v4, v6, :cond_4

    .line 251
    iput v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:I

    .line 256
    :cond_4
    iget v4, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:I

    const/4 v5, 0x3

    if-lt v4, v5, :cond_5

    .line 257
    const-string v1, "Focused too many times on this frame!"

    invoke-direct {p0, v1}, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 263
    :cond_5
    iget v4, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:I

    if-gt v4, v6, :cond_6

    iget-boolean v4, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:Z

    if-eqz v4, :cond_8

    .line 264
    :cond_6
    invoke-direct {p0, p1}, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->d(Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;)V

    .line 266
    iget-boolean v4, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:Z

    if-nez v4, :cond_7

    .line 269
    invoke-virtual {p1}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->getSignature()[I

    move-result-object v4

    iput-object v4, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:[I

    .line 274
    :cond_7
    iget-object v4, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:Lcom/google/bionics/scanner/unveil/util/Stopwatch;

    invoke-virtual {v4}, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->getElapsedMilliseconds()J

    move-result-wide v4

    const-wide/16 v6, 0x96

    cmp-long v4, v4, v6

    if-lez v4, :cond_8

    .line 275
    const-string v0, "Focusing based on image difference."

    invoke-direct {p0, v0}, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a(Ljava/lang/String;)V

    move v0, v1

    .line 276
    goto :goto_0

    .line 281
    :cond_8
    iget-wide v4, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->c:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_9

    .line 282
    const-string v0, "Focusing based on exposure."

    invoke-direct {p0, v0}, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a(Ljava/lang/String;)V

    move v0, v1

    .line 283
    goto :goto_0

    .line 286
    :cond_9
    const-string v1, "No need to focus."

    invoke-direct {p0, v1}, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private c(Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;)V
    .locals 4

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:[I

    if-nez v0, :cond_0

    .line 298
    const/16 v0, 0x64

    iput v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:I

    .line 309
    :goto_0
    return-void

    .line 302
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    .line 306
    iget-object v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:[I

    invoke-virtual {p1, v2}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->diffSignature([I)I

    move-result v2

    iput v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:I

    .line 308
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:J

    goto :goto_0
.end method

.method private d(Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;)V
    .locals 4

    .prologue
    .line 313
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    .line 315
    invoke-virtual {p1}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->isBlurred()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:Z

    .line 316
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:J

    .line 319
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:Z

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:Lcom/google/bionics/scanner/unveil/util/Stopwatch;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->start()V

    .line 325
    :goto_0
    return-void

    .line 322
    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:Lcom/google/bionics/scanner/unveil/util/Stopwatch;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->stop()V

    .line 323
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:Lcom/google/bionics/scanner/unveil/util/Stopwatch;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->reset()V

    goto :goto_0
.end method


# virtual methods
.method protected declared-synchronized a(Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;)V
    .locals 2

    .prologue
    .line 174
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->d:Z

    .line 177
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->isFocusing()Z

    move-result v0

    .line 178
    invoke-virtual {p1, v0}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->setTakenWhileFocusing(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 179
    if-eqz v0, :cond_1

    .line 217
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 183
    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->c:Z

    if-eqz v0, :cond_3

    .line 184
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Lcom/google/bionics/scanner/unveil/util/Stopwatch;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->reset()V

    .line 185
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Lcom/google/bionics/scanner/unveil/util/Stopwatch;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->start()V

    .line 188
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:Lcom/google/bionics/scanner/unveil/util/Stopwatch;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->stop()V

    .line 189
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:Lcom/google/bionics/scanner/unveil/util/Stopwatch;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->reset()V

    .line 191
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:[I

    if-nez v0, :cond_2

    .line 192
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:I

    .line 193
    invoke-virtual {p1}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->getSignature()[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:[I

    .line 196
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->c:Z

    .line 200
    :cond_3
    invoke-virtual {p1}, Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;->a()[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:[I

    .line 202
    invoke-direct {p0, p1}, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a(Lcom/google/bionics/scanner/unveil/nonstop/TimestampedFrame;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:I

    const/16 v1, 0x8

    if-gt v0, v1, :cond_5

    .line 204
    iget v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:I

    .line 209
    :goto_1
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    monitor-enter v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 211
    :try_start_2
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->isFocusing()Z

    move-result v0

    if-nez v0, :cond_4

    .line 212
    const-string v0, "Requesting focus."

    invoke-direct {p0, v0}, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a(Ljava/lang/String;)V

    .line 213
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->f()V

    .line 215
    :cond_4
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 174
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 206
    :cond_5
    const/4 v0, 0x0

    :try_start_4
    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:[I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1
.end method

.method protected d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 138
    iput-boolean v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Z

    .line 139
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:Z

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:[I

    .line 141
    iput v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:I

    .line 142
    const-wide/16 v0, -0xfa0

    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->d:J

    .line 143
    iput v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:I

    .line 144
    return-void
.end method

.method protected f()V
    .locals 7

    .prologue
    const/high16 v6, 0x45fa0000    # 8000.0f

    .line 147
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Z

    .line 149
    const-wide/16 v0, -0xfa0

    iget-wide v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->d:J

    iget-object v4, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Lcom/google/bionics/scanner/unveil/util/Stopwatch;

    .line 150
    invoke-virtual {v4}, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->getElapsedMilliseconds()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 149
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->d:J

    .line 151
    iget-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->d:J

    long-to-float v0, v0

    add-float/2addr v0, v6

    const/high16 v1, 0x3fc00000    # 1.5f

    mul-float/2addr v0, v1

    sub-float/2addr v0, v6

    float-to-long v0, v0

    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->d:J

    .line 154
    const-wide/16 v0, 0x1f40

    iget-wide v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->d:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->d:J

    .line 156
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Lcom/google/bionics/scanner/unveil/camera/CameraManager;

    invoke-virtual {v0, p0}, Lcom/google/bionics/scanner/unveil/camera/CameraManager;->focus(Lcom/google/bionics/scanner/unveil/camera/CameraManager$FocusCallback;)V

    .line 157
    return-void
.end method

.method public declared-synchronized getDebugText()Ljava/util/Vector;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 373
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->d:Z

    if-eqz v1, :cond_2

    .line 374
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->clear()V

    .line 375
    iget-object v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Ljava/util/Vector;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:Z

    if-eqz v1, :cond_0

    const-string v1, "blurred"

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ": "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "ms"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 377
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Ljava/util/Vector;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "lastDiffPercent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms, num same: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 380
    const-string v1, ""

    .line 381
    iget-object v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:[I

    array-length v3, v2

    :goto_1
    if-ge v0, v3, :cond_1

    aget v4, v2, v0

    .line 382
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    div-int/lit8 v4, v4, 0x64

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 381
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 375
    :cond_0
    const-string v1, "focused"

    goto :goto_0

    .line 384
    :cond_1
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Ljava/util/Vector;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "buckets: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 385
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Ljava/util/Vector;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "buckets median: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:[I

    invoke-static {v2}, Lcom/google/bionics/scanner/unveil/util/NumberUtils;->getMedianIndex([I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  mean: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:[I

    .line 386
    invoke-static {v2}, Lcom/google/bionics/scanner/unveil/util/NumberUtils;->getMeanIndex([I)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  stdDev: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:[I

    .line 387
    invoke-static {v2}, Lcom/google/bionics/scanner/unveil/util/NumberUtils;->getNormalizedStdDev([I)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 385
    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 389
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Ljava/util/Vector;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Since last focus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Lcom/google/bionics/scanner/unveil/util/Stopwatch;

    invoke-virtual {v2}, Lcom/google/bionics/scanner/unveil/util/Stopwatch;->getElapsedMilliseconds()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 390
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Ljava/util/Vector;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exposure delay: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->c:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 392
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Ljava/util/Vector;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Focus fatigue: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->d:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 394
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->d:Z

    .line 396
    :cond_2
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->a:Ljava/util/Vector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 373
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isLastFrameBlurred()Z
    .locals 1

    .prologue
    .line 169
    iget-boolean v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->b:Z

    return v0
.end method

.method public onFocus(Z)V
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/bionics/scanner/unveil/nonstop/ImageBlurProcessor;->c:Z

    .line 162
    return-void
.end method
