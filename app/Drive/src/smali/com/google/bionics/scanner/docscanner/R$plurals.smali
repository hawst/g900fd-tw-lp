.class public final Lcom/google/bionics/scanner/docscanner/R$plurals;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final accounts_title:I = 0x7f110000

.field public static final add_collaborators_invalid_contact_address:I = 0x7f110053

.field public static final move_confirm_dialog_message_share_files:I = 0x7f110001

.field public static final move_confirm_dialog_message_share_files_and_folders:I = 0x7f110002

.field public static final move_confirm_dialog_message_share_folders:I = 0x7f110003

.field public static final move_confirm_dialog_message_unshare_files_and_folders_in_one_folder:I = 0x7f110004

.field public static final move_confirm_dialog_message_unshare_files_and_folders_in_three_or_more_folders:I = 0x7f110005

.field public static final move_confirm_dialog_message_unshare_files_and_folders_in_two_folders:I = 0x7f110006

.field public static final move_confirm_dialog_message_unshare_files_in_one_folder:I = 0x7f110007

.field public static final move_confirm_dialog_message_unshare_files_in_three_or_more_folders:I = 0x7f110008

.field public static final move_confirm_dialog_message_unshare_files_in_two_folders:I = 0x7f110009

.field public static final move_confirm_dialog_message_unshare_folders_in_one_folder:I = 0x7f11000a

.field public static final move_confirm_dialog_message_unshare_folders_in_three_or_more_folders:I = 0x7f11000b

.field public static final move_confirm_dialog_message_unshare_folders_in_two_folders:I = 0x7f11000c

.field public static final move_confirm_dialog_title_share_files:I = 0x7f11000d

.field public static final move_confirm_dialog_title_share_files_and_folders:I = 0x7f11000e

.field public static final move_confirm_dialog_title_share_folders:I = 0x7f11000f

.field public static final pin_notification_sync_completed:I = 0x7f110010

.field public static final pin_notification_sync_completed_all:I = 0x7f110011

.field public static final pin_notification_sync_progress:I = 0x7f110012

.field public static final pin_notification_sync_queued_files:I = 0x7f110013

.field public static final pin_notification_waiting_title:I = 0x7f110014

.field public static final plus_photo_item_remove_dialog_text:I = 0x7f110015

.field public static final recent_activity_type_commented_many_users:I = 0x7f110016

.field public static final recent_activity_type_commented_one_user:I = 0x7f110017

.field public static final recent_activity_type_commented_this_file:I = 0x7f110018

.field public static final recent_activity_type_created_extra_many_users:I = 0x7f110019

.field public static final recent_activity_type_created_extra_one_user:I = 0x7f11001a

.field public static final recent_activity_type_created_many_users:I = 0x7f11001b

.field public static final recent_activity_type_created_one_user:I = 0x7f11001c

.field public static final recent_activity_type_created_this_file:I = 0x7f11001d

.field public static final recent_activity_type_created_this_folder:I = 0x7f11001e

.field public static final recent_activity_type_edited_many_users:I = 0x7f11001f

.field public static final recent_activity_type_edited_one_user:I = 0x7f110020

.field public static final recent_activity_type_edited_this_file:I = 0x7f110021

.field public static final recent_activity_type_empty_trash:I = 0x7f110022

.field public static final recent_activity_type_link_many_users:I = 0x7f110023

.field public static final recent_activity_type_link_one_user:I = 0x7f110024

.field public static final recent_activity_type_link_this_file:I = 0x7f110025

.field public static final recent_activity_type_link_this_folder:I = 0x7f110026

.field public static final recent_activity_type_moved_extra_many_users:I = 0x7f110027

.field public static final recent_activity_type_moved_extra_one_user:I = 0x7f110028

.field public static final recent_activity_type_moved_many_users:I = 0x7f110029

.field public static final recent_activity_type_moved_one_user:I = 0x7f11002a

.field public static final recent_activity_type_moved_this_file:I = 0x7f11002b

.field public static final recent_activity_type_moved_this_file_extra:I = 0x7f11002c

.field public static final recent_activity_type_moved_this_folder:I = 0x7f11002d

.field public static final recent_activity_type_moved_this_folder_extra:I = 0x7f11002e

.field public static final recent_activity_type_renamed_many_users:I = 0x7f11002f

.field public static final recent_activity_type_renamed_one_user:I = 0x7f110030

.field public static final recent_activity_type_renamed_this_file:I = 0x7f110031

.field public static final recent_activity_type_renamed_this_folder:I = 0x7f110032

.field public static final recent_activity_type_shared_extra_many_users:I = 0x7f110033

.field public static final recent_activity_type_shared_extra_one_user:I = 0x7f110034

.field public static final recent_activity_type_shared_many_users:I = 0x7f110035

.field public static final recent_activity_type_shared_one_user:I = 0x7f110036

.field public static final recent_activity_type_shared_this_file:I = 0x7f110037

.field public static final recent_activity_type_shared_this_file_extra:I = 0x7f110038

.field public static final recent_activity_type_shared_this_folder:I = 0x7f110039

.field public static final recent_activity_type_shared_this_folder_extra:I = 0x7f11003a

.field public static final recent_activity_type_trashed_many_users:I = 0x7f11003b

.field public static final recent_activity_type_trashed_one_user:I = 0x7f11003c

.field public static final recent_activity_type_trashed_this_file:I = 0x7f11003d

.field public static final recent_activity_type_trashed_this_folder:I = 0x7f11003e

.field public static final recent_activity_type_unshared_many_users:I = 0x7f11003f

.field public static final recent_activity_type_unshared_one_user:I = 0x7f110040

.field public static final recent_activity_type_unshared_this_file:I = 0x7f110041

.field public static final recent_activity_type_unshared_this_folder:I = 0x7f110042

.field public static final recent_activity_type_uploaded_many_users:I = 0x7f110043

.field public static final recent_activity_type_uploaded_one_user:I = 0x7f110044

.field public static final recent_activity_type_uploaded_this_file:I = 0x7f110045

.field public static final recent_activity_type_uploaded_this_folder:I = 0x7f110046

.field public static final recent_activity_users_more:I = 0x7f110047

.field public static final selection_floating_handle_count:I = 0x7f110048

.field public static final selection_floating_handle_count_content_desc:I = 0x7f110049

.field public static final transfer_notification_waiting_resume:I = 0x7f11004a

.field public static final transfer_notification_waiting_ticker:I = 0x7f11004b

.field public static final upload_notification_prepare_upload_failures:I = 0x7f11004c

.field public static final upload_notification_sync_completed_successfully:I = 0x7f11004d

.field public static final upload_notification_sync_completed_with_failures:I = 0x7f11004e

.field public static final upload_notification_sync_progress:I = 0x7f11004f

.field public static final upload_notification_waiting_title:I = 0x7f110050

.field public static final upload_spinner_message:I = 0x7f110051

.field public static final upload_toast_message:I = 0x7f110052


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4607
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
