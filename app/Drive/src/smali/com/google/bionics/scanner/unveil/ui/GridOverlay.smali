.class public Lcom/google/bionics/scanner/unveil/ui/GridOverlay;
.super Landroid/view/View;
.source "GridOverlay.java"


# instance fields
.field private final a:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 21
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/GridOverlay;->a:Landroid/graphics/Paint;

    .line 25
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/ui/GridOverlay;->a()V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/GridOverlay;->a:Landroid/graphics/Paint;

    .line 30
    invoke-direct {p0}, Lcom/google/bionics/scanner/unveil/ui/GridOverlay;->a()V

    .line 31
    return-void
.end method

.method private a(II)I
    .locals 1

    .prologue
    .line 86
    if-lez p1, :cond_0

    if-gtz p2, :cond_2

    .line 87
    :cond_0
    const/4 v0, 0x1

    .line 101
    :cond_1
    return v0

    .line 90
    :cond_2
    if-ge p1, p2, :cond_3

    move v0, p2

    .line 96
    :goto_0
    if-eqz p1, :cond_1

    .line 98
    rem-int p2, v0, p1

    move v0, p1

    move p1, p2

    .line 100
    goto :goto_0

    :cond_3
    move v0, p1

    move p1, p2

    goto :goto_0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/GridOverlay;->a:Landroid/graphics/Paint;

    const/16 v1, 0x41

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 35
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    const/4 v1, 0x4

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 39
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 41
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/ui/GridOverlay;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/ui/GridOverlay;->getHeight()I

    move-result v3

    invoke-direct {p0, v0, v3}, Lcom/google/bionics/scanner/unveil/ui/GridOverlay;->a(II)I

    move-result v0

    .line 43
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/ui/GridOverlay;->getHeight()I

    move-result v3

    div-int/2addr v3, v0

    .line 44
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/ui/GridOverlay;->getWidth()I

    move-result v4

    div-int v0, v4, v0

    .line 47
    if-le v3, v5, :cond_0

    if-gt v0, v5, :cond_1

    .line 48
    :cond_0
    mul-int/lit8 v3, v3, 0x2

    .line 49
    mul-int/lit8 v0, v0, 0x2

    .line 54
    :cond_1
    if-le v3, v1, :cond_4

    .line 56
    const/high16 v0, 0x40800000    # 4.0f

    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/ui/GridOverlay;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v0, v3

    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/ui/GridOverlay;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 60
    :goto_0
    add-int/lit8 v8, v1, -0x1

    .line 61
    add-int/lit8 v9, v0, -0x1

    .line 63
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/ui/GridOverlay;->getHeight()I

    move-result v0

    add-int/lit8 v1, v8, 0x1

    div-int v10, v0, v1

    .line 64
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/ui/GridOverlay;->getWidth()I

    move-result v0

    add-int/lit8 v1, v9, 0x1

    div-int v11, v0, v1

    move v7, v6

    .line 66
    :goto_1
    if-gt v7, v9, :cond_2

    .line 67
    mul-int v0, v7, v11

    int-to-float v1, v0

    mul-int v0, v7, v11

    int-to-float v3, v0

    .line 71
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/ui/GridOverlay;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/bionics/scanner/unveil/ui/GridOverlay;->a:Landroid/graphics/Paint;

    move-object v0, p1

    .line 67
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 66
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    :cond_2
    move v0, v6

    .line 75
    :goto_2
    if-gt v0, v8, :cond_3

    .line 76
    mul-int v1, v0, v10

    int-to-float v3, v1

    .line 79
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/ui/GridOverlay;->getWidth()I

    move-result v1

    int-to-float v4, v1

    mul-int v1, v0, v10

    int-to-float v5, v1

    iget-object v6, p0, Lcom/google/bionics/scanner/unveil/ui/GridOverlay;->a:Landroid/graphics/Paint;

    move-object v1, p1

    .line 76
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 75
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 83
    :cond_3
    return-void

    :cond_4
    move v1, v3

    goto :goto_0
.end method
