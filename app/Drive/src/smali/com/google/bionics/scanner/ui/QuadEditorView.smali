.class public Lcom/google/bionics/scanner/ui/QuadEditorView;
.super Landroid/view/View;
.source "QuadEditorView.java"


# static fields
.field private static final a:Lcom/google/bionics/scanner/unveil/util/Logger;


# instance fields
.field private a:I

.field private a:Landroid/graphics/Bitmap;

.field private a:Landroid/graphics/Canvas;

.field private final a:Landroid/graphics/Matrix;

.field private final a:Landroid/graphics/Paint;

.field private final a:Landroid/graphics/PointF;

.field private final a:Landroid/graphics/Rect;

.field private a:LbhB;

.field private final a:Lcom/google/bionics/scanner/rectifier/Quadrilateral$HapticFeedback;

.field private a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

.field private a:Lcom/google/bionics/scanner/unveil/util/Picture;

.field private final a:[F

.field private final a:[Z

.field private b:I

.field private b:Landroid/graphics/Bitmap;

.field private final b:Landroid/graphics/Matrix;

.field private final b:Landroid/graphics/Paint;

.field private final b:Landroid/graphics/PointF;

.field private final b:Landroid/graphics/Rect;

.field private final b:[F

.field private c:I

.field private c:Landroid/graphics/Bitmap;

.field private final c:Landroid/graphics/Matrix;

.field private final c:Landroid/graphics/Paint;

.field private final c:[F

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>()V

    sput-object v0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v1, 0x2

    const/4 v2, -0x1

    .line 119
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Matrix;

    .line 55
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:Landroid/graphics/Matrix;

    .line 57
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:Landroid/graphics/Matrix;

    .line 59
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:[F

    .line 62
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Rect;

    .line 64
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:Landroid/graphics/Rect;

    .line 66
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/PointF;

    .line 69
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:[F

    .line 71
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:Landroid/graphics/PointF;

    .line 73
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:[F

    .line 97
    const/16 v0, 0x28

    iput v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:I

    .line 99
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:I

    .line 105
    iput v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->h:I

    .line 106
    iput v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->i:I

    .line 107
    const/4 v0, 0x4

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:[Z

    .line 116
    sget-object v0, LbhB;->c:LbhB;

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:LbhB;

    .line 120
    new-instance v0, Lbhz;

    invoke-direct {v0, p0}, Lbhz;-><init>(Lcom/google/bionics/scanner/ui/QuadEditorView;)V

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral$HapticFeedback;

    .line 129
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Paint;

    .line 130
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 131
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Paint;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 132
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 133
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 134
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Paint;

    const v1, 0x3c78d8

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 135
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Paint;

    const/16 v1, 0xb2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 137
    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:Landroid/graphics/Paint;

    .line 138
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 140
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:Landroid/graphics/Paint;

    .line 141
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 142
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 144
    invoke-direct {p0}, Lcom/google/bionics/scanner/ui/QuadEditorView;->d()V

    .line 145
    return-void
.end method

.method public static a(Landroid/graphics/Matrix;)F
    .locals 3

    .prologue
    .line 640
    const/16 v0, 0x9

    new-array v1, v0, [F

    .line 641
    invoke-virtual {p0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 645
    const/4 v0, 0x1

    aget v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 646
    const/4 v2, 0x0

    cmpl-float v2, v0, v2

    if-lez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    aget v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    goto :goto_0
.end method

.method private a([F[Landroid/graphics/PointF;I)F
    .locals 3

    .prologue
    .line 340
    aget-object v0, p2, p3

    iget v0, v0, Landroid/graphics/PointF;->x:F

    const/4 v1, 0x0

    aget v1, p1, v1

    sub-float/2addr v0, v1

    .line 341
    aget-object v1, p2, p3

    iget v1, v1, Landroid/graphics/PointF;->y:F

    const/4 v2, 0x1

    aget v2, p1, v2

    sub-float/2addr v1, v2

    .line 342
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method private a([FLbhH;Z)I
    .locals 7

    .prologue
    const/4 v2, -0x1

    .line 317
    invoke-interface {p2}, LbhH;->getVertices()[Landroid/graphics/PointF;

    move-result-object v5

    .line 318
    const v1, 0x7f7fffff    # Float.MAX_VALUE

    .line 320
    const/4 v0, 0x0

    move v3, v1

    move v1, v2

    :goto_0
    array-length v4, v5

    if-ge v0, v4, :cond_2

    .line 321
    if-eqz p3, :cond_1

    invoke-direct {p0, p1, v5, v0}, Lcom/google/bionics/scanner/ui/QuadEditorView;->b([F[Landroid/graphics/PointF;I)F

    move-result v4

    .line 323
    :goto_1
    cmpg-float v6, v4, v3

    if-gez v6, :cond_0

    move v1, v0

    move v3, v4

    .line 320
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 322
    :cond_1
    invoke-direct {p0, p1, v5, v0}, Lcom/google/bionics/scanner/ui/QuadEditorView;->a([F[Landroid/graphics/PointF;I)F

    move-result v4

    goto :goto_1

    .line 328
    :cond_2
    iget v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:I

    int-to-float v0, v0

    cmpg-float v0, v3, v0

    if-gez v0, :cond_3

    :goto_2
    return v1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method private a(Landroid/graphics/Bitmap;Landroid/view/View;I)Landroid/graphics/Matrix;
    .locals 4

    .prologue
    const/16 v3, 0x10e

    const/16 v2, 0x5a

    const/4 v1, 0x0

    .line 586
    if-eq p3, v2, :cond_0

    if-ne p3, v3, :cond_2

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->d:I

    .line 587
    if-eq p3, v2, :cond_1

    if-ne p3, v3, :cond_3

    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    :goto_1
    iput v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->e:I

    .line 590
    sparse-switch p3, :sswitch_data_0

    .line 609
    :goto_2
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 610
    int-to-float v0, p3

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 613
    iget v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->f:I

    int-to-float v0, v0

    iget v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->g:I

    int-to-float v2, v2

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 618
    iget v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->e:I

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v2

    mul-int/2addr v0, v2

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v2

    iget v3, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->d:I

    mul-int/2addr v2, v3

    if-le v0, v2, :cond_4

    .line 620
    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->e:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 625
    :goto_3
    invoke-virtual {v1, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 628
    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->d:I

    int-to-float v3, v3

    mul-float/2addr v0, v3

    sub-float v0, v2, v0

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 630
    return-object v1

    .line 586
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    goto :goto_0

    .line 587
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    goto :goto_1

    .line 592
    :sswitch_0
    iput v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->f:I

    .line 593
    iput v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->g:I

    goto :goto_2

    .line 596
    :sswitch_1
    iget v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->d:I

    iput v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->f:I

    .line 597
    iput v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->g:I

    goto :goto_2

    .line 600
    :sswitch_2
    iget v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->d:I

    iput v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->f:I

    .line 601
    iget v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->e:I

    iput v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->g:I

    goto :goto_2

    .line 604
    :sswitch_3
    iput v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->f:I

    .line 605
    iget v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->e:I

    iput v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->g:I

    goto :goto_2

    .line 623
    :cond_4
    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->d:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    goto :goto_3

    .line 590
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
        0x10e -> :sswitch_3
    .end sparse-switch
.end method

.method private a(Landroid/graphics/Canvas;LbhH;)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 368
    invoke-interface {p2}, LbhH;->getVertices()[Landroid/graphics/PointF;

    move-result-object v8

    move v6, v7

    .line 369
    :goto_0
    array-length v0, v8

    if-ge v6, v0, :cond_3

    .line 370
    add-int/lit8 v0, v6, 0x1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    move v0, v7

    .line 371
    :goto_1
    aget-object v2, v8, v6

    .line 372
    aget-object v0, v8, v0

    .line 373
    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 375
    iget v1, v2, Landroid/graphics/PointF;->x:F

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget v3, v0, Landroid/graphics/PointF;->x:F

    iget v4, v0, Landroid/graphics/PointF;->y:F

    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:[Z

    aget-boolean v0, v0, v6

    if-eqz v0, :cond_2

    iget-object v5, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:Landroid/graphics/Paint;

    :goto_2
    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 369
    :cond_0
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 370
    :cond_1
    add-int/lit8 v0, v6, 0x1

    goto :goto_1

    .line 375
    :cond_2
    iget-object v5, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Paint;

    goto :goto_2

    .line 379
    :cond_3
    return-void
.end method

.method private a(Landroid/graphics/Canvas;[F)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 444
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Matrix;

    invoke-virtual {v0, p2}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 446
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/PointF;

    aget v2, p2, v1

    iget v3, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    const/4 v3, 0x1

    aget v3, p2, v3

    iget v4, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 448
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    float-to-int v2, v2

    iget-object v3, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    float-to-int v3, v3

    iget-object v4, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    iget v5, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:I

    int-to-float v5, v5

    add-float/2addr v4, v5

    float-to-int v4, v4

    iget-object v5, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    iget v6, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:I

    int-to-float v6, v6

    add-float/2addr v5, v6

    float-to-int v5, v5

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 451
    aget v0, p2, v1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    iget v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:I

    sub-int/2addr v0, v2

    .line 453
    :goto_0
    iget-object v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:Landroid/graphics/Rect;

    iget v3, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:I

    add-int/2addr v3, v0

    iget v4, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:I

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 456
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 458
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:Landroid/graphics/Rect;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 459
    return-void

    :cond_0
    move v0, v1

    .line 451
    goto :goto_0
.end method

.method private a([FILcom/google/bionics/scanner/rectifier/Quadrilateral;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 264
    new-instance v0, Landroid/graphics/PointF;

    aget v1, p1, v5

    aget v2, p1, v6

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 265
    sget-object v1, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Looking for closest line with index %d to point [%f, %f]"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    aget v4, p1, v5

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x2

    aget v5, p1, v6

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 266
    packed-switch p2, :pswitch_data_0

    .line 282
    :goto_0
    return-void

    .line 268
    :pswitch_0
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral$HapticFeedback;

    invoke-virtual {p3, v0, v1}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->selectTopLine(Landroid/graphics/PointF;Lcom/google/bionics/scanner/rectifier/Quadrilateral$HapticFeedback;)Lcom/google/bionics/scanner/rectifier/Line;

    goto :goto_0

    .line 271
    :pswitch_1
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral$HapticFeedback;

    invoke-virtual {p3, v0, v1}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->selectRightLine(Landroid/graphics/PointF;Lcom/google/bionics/scanner/rectifier/Quadrilateral$HapticFeedback;)Lcom/google/bionics/scanner/rectifier/Line;

    goto :goto_0

    .line 274
    :pswitch_2
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral$HapticFeedback;

    invoke-virtual {p3, v0, v1}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->selectBottomLine(Landroid/graphics/PointF;Lcom/google/bionics/scanner/rectifier/Quadrilateral$HapticFeedback;)Lcom/google/bionics/scanner/rectifier/Line;

    goto :goto_0

    .line 277
    :pswitch_3
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral$HapticFeedback;

    invoke-virtual {p3, v0, v1}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->selectLeftLine(Landroid/graphics/PointF;Lcom/google/bionics/scanner/rectifier/Quadrilateral$HapticFeedback;)Lcom/google/bionics/scanner/rectifier/Line;

    goto :goto_0

    .line 266
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b([F[Landroid/graphics/PointF;I)F
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    .line 354
    aget-object v2, p2, p3

    .line 355
    add-int/lit8 v0, p3, 0x1

    array-length v3, p2

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    aget-object v0, p2, v0

    .line 356
    iget v3, v2, Landroid/graphics/PointF;->x:F

    iget v4, v0, Landroid/graphics/PointF;->x:F

    iget v5, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v4, v5

    div-float/2addr v4, v6

    add-float/2addr v3, v4

    aget v1, p1, v1

    sub-float v1, v3, v1

    .line 357
    iget v3, v2, Landroid/graphics/PointF;->y:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v2

    div-float/2addr v0, v6

    add-float/2addr v0, v3

    const/4 v2, 0x1

    aget v2, p1, v2

    sub-float/2addr v0, v2

    .line 358
    mul-float/2addr v1, v1

    mul-float/2addr v0, v0

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0

    .line 355
    :cond_0
    add-int/lit8 v0, p3, 0x1

    goto :goto_0
.end method

.method private b(Landroid/graphics/Canvas;LbhH;)V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v1, 0x0

    const/high16 v10, 0x40000000    # 2.0f

    const/high16 v9, 0x3f800000    # 1.0f

    .line 388
    invoke-interface {p2}, LbhH;->getVertices()[Landroid/graphics/PointF;

    move-result-object v3

    move v2, v1

    .line 391
    :goto_0
    if-ge v2, v11, :cond_1

    .line 392
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 395
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    neg-int v4, v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    iget-object v5, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    neg-int v5, v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 400
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Matrix;

    invoke-static {v0}, Lcom/google/bionics/scanner/ui/QuadEditorView;->a(Landroid/graphics/Matrix;)F

    move-result v0

    .line 402
    iget-object v4, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:Landroid/graphics/Matrix;

    div-float v5, v9, v0

    div-float v0, v9, v0

    invoke-virtual {v4, v5, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 404
    aget-object v4, v3, v2

    .line 405
    add-int/lit8 v0, v2, 0x1

    if-ne v0, v11, :cond_0

    move v0, v1

    :goto_1
    aget-object v0, v3, v0

    .line 408
    iget-object v5, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:Landroid/graphics/Matrix;

    iget v6, v4, Landroid/graphics/PointF;->x:F

    iget v7, v0, Landroid/graphics/PointF;->x:F

    iget v8, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v7, v8

    div-float/2addr v7, v10

    add-float/2addr v6, v7

    iget v7, v4, Landroid/graphics/PointF;->y:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget v4, v4, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v4

    div-float/2addr v0, v10

    add-float/2addr v0, v7

    invoke-virtual {v5, v6, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 410
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:Landroid/graphics/Matrix;

    const/4 v5, 0x0

    invoke-virtual {p1, v0, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 391
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 405
    :cond_0
    add-int/lit8 v0, v2, 0x1

    goto :goto_1

    .line 412
    :cond_1
    return-void
.end method

.method private b([FILcom/google/bionics/scanner/rectifier/Quadrilateral;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 301
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:[F

    aget v1, p1, v3

    iget-object v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    aput v1, v0, v3

    .line 302
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:[F

    aget v1, p1, v4

    iget-object v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v2

    aput v1, v0, v4

    .line 303
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:[F

    invoke-virtual {p3, v0, p2}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->cornerUpdate([FI)V

    .line 304
    return-void
.end method

.method private c(Landroid/graphics/Canvas;LbhH;)V
    .locals 8

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    .line 415
    invoke-interface {p2}, LbhH;->getVertices()[Landroid/graphics/PointF;

    move-result-object v2

    .line 417
    const/4 v0, 0x0

    :goto_0
    array-length v1, v2

    if-ge v0, v1, :cond_1

    .line 418
    aget-object v3, v2, v0

    .line 420
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 422
    iget v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->h:I

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:Landroid/graphics/Bitmap;

    .line 425
    :goto_1
    iget-object v4, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    neg-int v5, v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    neg-int v6, v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 427
    iget-object v4, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Matrix;

    invoke-static {v4}, Lcom/google/bionics/scanner/ui/QuadEditorView;->a(Landroid/graphics/Matrix;)F

    move-result v4

    .line 429
    iget-object v5, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:Landroid/graphics/Matrix;

    div-float v6, v7, v4

    div-float v4, v7, v4

    invoke-virtual {v5, v6, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 430
    iget-object v4, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:Landroid/graphics/Matrix;

    iget v5, v3, Landroid/graphics/PointF;->x:F

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v5, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 431
    iget-object v3, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:Landroid/graphics/Matrix;

    const/4 v4, 0x0

    invoke-virtual {p1, v1, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 417
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 422
    :cond_0
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Bitmap;

    goto :goto_1

    .line 433
    :cond_1
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:[Z

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([ZZ)V

    .line 289
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 569
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/unveil/util/Picture;->peekBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iget v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:I

    invoke-direct {p0, v0, p0, v1}, Lcom/google/bionics/scanner/ui/QuadEditorView;->a(Landroid/graphics/Bitmap;Landroid/view/View;I)Landroid/graphics/Matrix;

    move-result-object v0

    .line 570
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 571
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 572
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 573
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 148
    .line 149
    invoke-virtual {p0}, Lcom/google/bionics/scanner/ui/QuadEditorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/bionics/scanner/docscanner/R$drawable;->ds_quad_corner_handle:I

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Bitmap;

    .line 151
    invoke-virtual {p0}, Lcom/google/bionics/scanner/ui/QuadEditorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/bionics/scanner/docscanner/R$drawable;->ds_quad_corner_handle_focused:I

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:Landroid/graphics/Bitmap;

    .line 152
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 530
    iput p1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:I

    .line 531
    iget v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:I

    if-gez v0, :cond_0

    .line 532
    iget v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:I

    add-int/lit16 v0, v0, 0x168

    iput v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:I

    .line 536
    :goto_0
    invoke-direct {p0}, Lcom/google/bionics/scanner/ui/QuadEditorView;->e()V

    .line 537
    invoke-virtual {p0}, Lcom/google/bionics/scanner/ui/QuadEditorView;->invalidate()V

    .line 538
    return-void

    .line 534
    :cond_0
    iget v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:I

    rem-int/lit16 v0, v0, 0x168

    iput v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:I

    goto :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 498
    invoke-virtual {p0}, Lcom/google/bionics/scanner/ui/QuadEditorView;->c()V

    .line 499
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 502
    iput-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Canvas;

    .line 503
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 505
    iput-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:Landroid/graphics/Bitmap;

    .line 507
    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 508
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 509
    iput-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Bitmap;

    .line 510
    iput-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:Landroid/graphics/Bitmap;

    .line 511
    iput-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    .line 512
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 513
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 159
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:LbhB;

    sget-object v1, LbhB;->c:LbhB;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Canvas;

    .line 161
    :goto_0
    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 162
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    if-eqz v1, :cond_0

    .line 163
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    invoke-virtual {v1}, Lcom/google/bionics/scanner/unveil/util/Picture;->peekBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1, v2, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 165
    :cond_0
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    if-eqz v1, :cond_1

    .line 167
    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    .line 168
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 169
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    invoke-direct {p0, v0, v1}, Lcom/google/bionics/scanner/ui/QuadEditorView;->a(Landroid/graphics/Canvas;LbhH;)V

    .line 170
    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    .line 172
    :cond_1
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:LbhB;

    sget-object v2, LbhB;->c:LbhB;

    if-eq v1, v2, :cond_5

    .line 174
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:LbhB;

    sget-object v2, LbhB;->a:LbhB;

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:[F

    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/google/bionics/scanner/ui/QuadEditorView;->a(Landroid/graphics/Canvas;[F)V

    .line 176
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v3, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 185
    :cond_2
    :goto_2
    return-void

    :cond_3
    move-object v0, p1

    .line 159
    goto :goto_0

    .line 174
    :cond_4
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:[F

    goto :goto_1

    .line 177
    :cond_5
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    if-eqz v1, :cond_2

    .line 179
    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    .line 180
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 181
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    invoke-direct {p0, v0, v1}, Lcom/google/bionics/scanner/ui/QuadEditorView;->b(Landroid/graphics/Canvas;LbhH;)V

    .line 182
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    invoke-direct {p0, v0, v1}, Lcom/google/bionics/scanner/ui/QuadEditorView;->c(Landroid/graphics/Canvas;LbhH;)V

    .line 183
    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    goto :goto_2
.end method

.method public onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 517
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 518
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    if-eqz v0, :cond_0

    .line 519
    invoke-direct {p0}, Lcom/google/bionics/scanner/ui/QuadEditorView;->e()V

    .line 521
    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 3

    .prologue
    .line 482
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 483
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 485
    :cond_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:Landroid/graphics/Bitmap;

    .line 486
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->c:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Landroid/graphics/Canvas;

    .line 487
    if-lt p1, p2, :cond_1

    move v0, p2

    .line 488
    :goto_0
    const v1, 0x3e99999a    # 0.3f

    int-to-float v2, v0

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:I

    .line 489
    const v1, 0x3d4ccccd    # 0.05f

    int-to-float v0, v0

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:I

    .line 490
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 491
    return-void

    :cond_1
    move v0, p1

    .line 487
    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 192
    iget-object v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    if-nez v2, :cond_0

    move v0, v1

    .line 253
    :goto_0
    return v0

    .line 196
    :cond_0
    iget-object v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:[F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    aput v3, v2, v0

    .line 197
    iget-object v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:[F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    aput v3, v2, v1

    .line 200
    iget-object v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:[F

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 202
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 204
    :pswitch_0
    iget-object v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:[F

    iget-object v3, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    invoke-direct {p0, v2, v3, v1}, Lcom/google/bionics/scanner/ui/QuadEditorView;->a([FLbhH;Z)I

    move-result v2

    iput v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->i:I

    .line 205
    iget v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->i:I

    if-ltz v2, :cond_2

    .line 206
    sget-object v0, LbhB;->b:LbhB;

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:LbhB;

    .line 207
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:[Z

    iget v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->i:I

    aput-boolean v1, v0, v2

    .line 223
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/google/bionics/scanner/ui/QuadEditorView;->invalidate()V

    :goto_2
    move v0, v1

    .line 253
    goto :goto_0

    .line 210
    :cond_2
    iget-object v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:[F

    iget-object v3, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    invoke-direct {p0, v2, v3, v0}, Lcom/google/bionics/scanner/ui/QuadEditorView;->a([FLbhH;Z)I

    move-result v2

    iput v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->h:I

    .line 211
    iget v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->h:I

    if-ltz v2, :cond_1

    .line 212
    sget-object v2, LbhB;->a:LbhB;

    iput-object v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:LbhB;

    .line 213
    iget-object v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    invoke-virtual {v2}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->getVertices()[Landroid/graphics/PointF;

    move-result-object v2

    iget v3, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->h:I

    aget-object v2, v2, v3

    .line 214
    iget-object v3, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:Landroid/graphics/PointF;

    iget-object v4, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:[F

    aget v0, v4, v0

    iget v4, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v4

    iput v0, v3, Landroid/graphics/PointF;->x:F

    .line 215
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:Landroid/graphics/PointF;

    iget-object v3, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:[F

    aget v3, v3, v1

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float v2, v3, v2

    iput v2, v0, Landroid/graphics/PointF;->y:F

    .line 216
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:[F

    iget v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->h:I

    iget-object v3, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    invoke-direct {p0, v0, v2, v3}, Lcom/google/bionics/scanner/ui/QuadEditorView;->b([FILcom/google/bionics/scanner/rectifier/Quadrilateral;)V

    .line 217
    iget v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->h:I

    .line 218
    invoke-static {v0}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->getNeighborLineIndexes(I)Landroid/util/Pair;

    move-result-object v2

    .line 219
    iget-object v3, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:[Z

    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput-boolean v1, v3, v0

    .line 220
    iget-object v3, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:[Z

    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput-boolean v1, v3, v0

    goto :goto_1

    .line 227
    :pswitch_1
    sget-object v0, LbhA;->a:[I

    iget-object v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:LbhB;

    invoke-virtual {v2}, LbhB;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_1

    goto :goto_2

    .line 229
    :pswitch_2
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:[F

    iget v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->i:I

    iget-object v3, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    invoke-direct {p0, v0, v2, v3}, Lcom/google/bionics/scanner/ui/QuadEditorView;->a([FILcom/google/bionics/scanner/rectifier/Quadrilateral;)V

    .line 230
    invoke-virtual {p0}, Lcom/google/bionics/scanner/ui/QuadEditorView;->invalidate()V

    goto :goto_2

    .line 233
    :pswitch_3
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->b:[F

    iget v2, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->h:I

    iget-object v3, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    invoke-direct {p0, v0, v2, v3}, Lcom/google/bionics/scanner/ui/QuadEditorView;->b([FILcom/google/bionics/scanner/rectifier/Quadrilateral;)V

    .line 234
    invoke-virtual {p0}, Lcom/google/bionics/scanner/ui/QuadEditorView;->invalidate()V

    goto/16 :goto_2

    .line 243
    :pswitch_4
    sget-object v0, LbhB;->c:LbhB;

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:LbhB;

    .line 244
    invoke-direct {p0}, Lcom/google/bionics/scanner/ui/QuadEditorView;->d()V

    .line 245
    iput v4, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->h:I

    .line 246
    iput v4, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->i:I

    .line 247
    invoke-virtual {p0}, Lcom/google/bionics/scanner/ui/QuadEditorView;->invalidate()V

    goto/16 :goto_2

    .line 202
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_4
    .end packed-switch

    .line 227
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setPage(LbgY;)V
    .locals 1

    .prologue
    .line 467
    invoke-virtual {p1}, LbgY;->a()Lcom/google/bionics/scanner/unveil/util/Picture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/unveil/util/Picture;

    .line 468
    return-void
.end method

.method public setQuadrilateral(Lcom/google/bionics/scanner/rectifier/Quadrilateral;)V
    .locals 0

    .prologue
    .line 477
    iput-object p1, p0, Lcom/google/bionics/scanner/ui/QuadEditorView;->a:Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    .line 478
    return-void
.end method
