.class public Lcom/google/bionics/scanner/unveil/util/ImageUtils;
.super Ljava/lang/Object;
.source "ImageUtils.java"


# static fields
.field private static final a:Lcom/google/bionics/scanner/unveil/util/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>()V

    sput-object v0, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    .line 547
    const-string v0, "docscanner_image"

    invoke-static {v0}, Lcom/google/bionics/scanner/unveil/util/ResourceUtils;->loadNativeLibrary(Ljava/lang/String;)V

    .line 548
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 444
    return-void
.end method

.method protected static a(Landroid/graphics/Rect;Lcom/google/bionics/scanner/unveil/util/Size;Lcom/google/bionics/scanner/unveil/util/Size;)Landroid/graphics/Rect;
    .locals 12

    .prologue
    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    .line 288
    iget v0, p2, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    int-to-double v0, v0

    iget v2, p1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    .line 290
    iget v2, p2, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    int-to-double v2, v2

    iget v4, p1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    int-to-double v4, v4

    div-double/2addr v2, v4

    .line 293
    sub-double v4, v6, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    sub-double/2addr v6, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    cmpg-double v4, v4, v6

    if-gez v4, :cond_0

    .line 297
    :goto_0
    iget v2, p1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    int-to-double v2, v2

    mul-double/2addr v2, v0

    iget v4, p2, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    int-to-double v4, v4

    sub-double/2addr v2, v4

    div-double/2addr v2, v8

    .line 298
    iget v4, p1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    int-to-double v4, v4

    mul-double/2addr v4, v0

    iget v6, p2, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    int-to-double v6, v6

    sub-double/2addr v4, v6

    div-double/2addr v4, v8

    .line 301
    new-instance v6, Landroid/graphics/Rect;

    iget v7, p0, Landroid/graphics/Rect;->left:I

    int-to-double v8, v7

    mul-double/2addr v8, v0

    sub-double/2addr v8, v2

    double-to-int v7, v8

    iget v8, p0, Landroid/graphics/Rect;->top:I

    int-to-double v8, v8

    mul-double/2addr v8, v0

    sub-double/2addr v8, v4

    double-to-int v8, v8

    iget v9, p0, Landroid/graphics/Rect;->right:I

    int-to-double v10, v9

    mul-double/2addr v10, v0

    sub-double v2, v10, v2

    double-to-int v2, v2

    iget v3, p0, Landroid/graphics/Rect;->bottom:I

    int-to-double v10, v3

    mul-double/2addr v0, v10

    sub-double/2addr v0, v4

    double-to-int v0, v0

    invoke-direct {v6, v7, v8, v2, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 307
    return-object v6

    :cond_0
    move-wide v0, v2

    .line 293
    goto :goto_0
.end method

.method public static adjustCropForUnexpectedPictureSize(Landroid/graphics/Rect;Lcom/google/bionics/scanner/unveil/util/Size;Lcom/google/bionics/scanner/unveil/util/Size;)Landroid/graphics/Rect;
    .locals 9

    .prologue
    const/4 v4, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 228
    if-nez p0, :cond_0

    .line 229
    const/4 v0, 0x0

    .line 251
    :goto_0
    return-object v0

    .line 232
    :cond_0
    sget-object v0, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Original crop area: left=%d, top=%d, width=%d, height=%d"

    new-array v2, v4, [Ljava/lang/Object;

    iget v3, p0, Landroid/graphics/Rect;->left:I

    .line 233
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    iget v3, p0, Landroid/graphics/Rect;->top:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    .line 232
    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 235
    sget-object v0, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Expected picture size: %s"

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 236
    sget-object v0, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Actual picture size: %s"

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p2, v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 238
    invoke-static {p0, p1, p2}, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->a(Landroid/graphics/Rect;Lcom/google/bionics/scanner/unveil/util/Size;Lcom/google/bionics/scanner/unveil/util/Size;)Landroid/graphics/Rect;

    move-result-object v0

    .line 241
    invoke-static {p2, v0}, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->makeRectSafe(Lcom/google/bionics/scanner/unveil/util/Size;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    .line 242
    if-nez v0, :cond_1

    .line 243
    sget-object v0, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Could not make adjustedCropArea safe."

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 244
    const/4 v0, 0x0

    goto :goto_0

    .line 247
    :cond_1
    sget-object v1, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Final crop area: left=%d, top=%d, width=%d, height=%d"

    new-array v3, v4, [Ljava/lang/Object;

    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 248
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    iget v4, v0, Landroid/graphics/Rect;->top:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    .line 249
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    .line 247
    invoke-virtual {v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public static compressBitmap(Landroid/graphics/Bitmap;I)[B
    .locals 2

    .prologue
    .line 75
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 76
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {p0, v1, p1, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 78
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 79
    invoke-static {v0}, Lcom/google/bionics/scanner/unveil/util/ResourceUtils;->closeStream(Ljava/io/Closeable;)V

    .line 80
    return-object v1
.end method

.method public static native computeEdgeBitmap(II[B[B)V
.end method

.method public static computeImageStatistics(II[B)Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;
    .locals 3

    .prologue
    .line 485
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;

    new-instance v1, Lcom/google/bionics/scanner/unveil/util/ImageUtils;

    invoke-direct {v1}, Lcom/google/bionics/scanner/unveil/util/ImageUtils;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p0, p1, p2}, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->computeImageStatisticsNative(II[B)[F

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/ImageUtils$ImageStatistics;-><init>(Lcom/google/bionics/scanner/unveil/util/ImageUtils;[F)V

    return-object v0
.end method

.method private static native computeImageStatisticsNative(II[B)[F
.end method

.method public static native computeSignatureNative([BII[I)[I
.end method

.method public static native convertARGB8888ToYUV420SP([I[BII)V
.end method

.method public static native convertRGB565ToYUV420SP([B[BII)V
.end method

.method public static native convertYUV420SPToARGB8888([B[IIIZ)V
.end method

.method public static native convertYUV420SPToRGB565([B[BII)V
.end method

.method public static cropBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 60
    if-nez p1, :cond_0

    .line 70
    :goto_0
    return-object p0

    .line 64
    :cond_0
    if-nez p0, :cond_1

    .line 65
    sget-object v0, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Unable to decode camera image for processing."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 66
    const/4 p0, 0x0

    goto :goto_0

    .line 70
    :cond_1
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    .line 71
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v3

    .line 70
    invoke-static {p0, v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object p0

    goto :goto_0
.end method

.method public static native diffSignatureNative([I[I)I
.end method

.method public static native downsampleImageNative(II[BI[B)V
.end method

.method public static generateUndistortTransformer(Lcom/google/bionics/scanner/unveil/util/Size;Lcom/google/bionics/scanner/unveil/util/Size;)Landroid/graphics/Matrix;
    .locals 6

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 563
    iget v0, p0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    int-to-float v0, v0

    iget v2, p1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 564
    iget v2, p0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    int-to-float v2, v2

    iget v3, p1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 566
    cmpg-float v3, v0, v2

    if-gez v3, :cond_0

    .line 567
    div-float v0, v2, v0

    .line 574
    :goto_0
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 575
    iget v3, p1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget v4, p1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {v2, v1, v0, v3, v4}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 576
    return-object v2

    .line 570
    :cond_0
    div-float/2addr v0, v2

    move v5, v1

    move v1, v0

    move v0, v5

    .line 571
    goto :goto_0
.end method

.method public static native getBucketDistributionNative([BII)[I
.end method

.method public static getJpegQualityRecommendation(II)I
    .locals 10

    .prologue
    const/16 v1, 0x5f

    const/16 v0, 0x3c

    .line 31
    .line 33
    int-to-double v2, p0

    int-to-double v4, p1

    mul-double/2addr v2, v4

    const-wide v4, 0x412e848000000000L    # 1000000.0

    div-double/2addr v2, v4

    .line 34
    const-wide v4, 0x40acc20000000000L    # 3681.0

    const-wide v6, 0x40eb0ac000000000L    # 55382.0

    mul-double/2addr v6, v2

    add-double/2addr v4, v6

    .line 35
    const-wide v6, 0x40e5f8ffff2e48e9L    # 44999.9999

    cmpl-double v6, v4, v6

    if-ltz v6, :cond_1

    .line 51
    :cond_0
    :goto_0
    return v0

    .line 41
    :cond_1
    const-wide v6, 0x409f400000000000L    # 2000.0

    mul-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    .line 42
    const-wide v6, 0x40e5f90000000000L    # 45000.0

    sub-double v4, v6, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    const-wide v6, 0x402745ef1fddebd9L    # 11.63659

    mul-double/2addr v4, v6

    const-wide v6, -0x3fd79a415f45e0b5L    # -12.19872

    const-wide v8, -0x4014b168222848f4L    # -0.8533439

    mul-double/2addr v2, v8

    add-double/2addr v2, v6

    add-double/2addr v2, v4

    double-to-int v2, v2

    .line 46
    if-lt v2, v0, :cond_0

    .line 48
    if-le v2, v1, :cond_2

    move v0, v1

    .line 49
    goto :goto_0

    :cond_2
    move v0, v2

    .line 51
    goto :goto_0
.end method

.method public static getJpegQualityRecommendation(Lcom/google/bionics/scanner/unveil/util/Size;)I
    .locals 2

    .prologue
    .line 56
    iget v0, p0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    iget v1, p0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    invoke-static {v0, v1}, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->getJpegQualityRecommendation(II)I

    move-result v0

    return v0
.end method

.method public static getTransformationMatrix(Lcom/google/bionics/scanner/unveil/util/Size;Lcom/google/bionics/scanner/unveil/util/Size;I)Landroid/graphics/Matrix;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    .line 117
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    rem-int/lit8 v2, v2, 0x5a

    if-eqz v2, :cond_0

    .line 118
    sget-object v2, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v3, "Angle that is not multiple of 90! %d"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {v2, v3, v4}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 121
    :cond_0
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 123
    if-eqz p2, :cond_1

    .line 125
    iget v2, p0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    neg-int v2, v2

    int-to-float v2, v2

    div-float/2addr v2, v6

    iget v4, p0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    neg-int v4, v4

    int-to-float v4, v4

    div-float/2addr v4, v6

    invoke-virtual {v3, v2, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 128
    int-to-float v2, p2

    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 133
    :cond_1
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x5a

    rem-int/lit16 v2, v2, 0xb4

    if-nez v2, :cond_5

    move v2, v0

    .line 134
    :goto_0
    if-eqz v2, :cond_6

    iget v0, p0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    move v1, v0

    .line 135
    :goto_1
    if-eqz v2, :cond_7

    iget v0, p0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    .line 136
    :goto_2
    iget v2, p1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    if-ne v1, v2, :cond_2

    iget v2, p1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    if-eq v0, v2, :cond_3

    .line 137
    :cond_2
    iget v2, p1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    int-to-float v2, v2

    int-to-float v1, v1

    div-float v1, v2, v1

    .line 138
    iget v2, p1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    int-to-float v2, v2

    int-to-float v0, v0

    div-float v0, v2, v0

    .line 139
    invoke-virtual {v3, v1, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 142
    :cond_3
    if-eqz p2, :cond_4

    .line 144
    iget v0, p1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    int-to-float v0, v0

    div-float/2addr v0, v6

    iget v1, p1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    int-to-float v1, v1

    div-float/2addr v1, v6

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 147
    :cond_4
    return-object v3

    :cond_5
    move v2, v1

    .line 133
    goto :goto_0

    .line 134
    :cond_6
    iget v0, p0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    move v1, v0

    goto :goto_1

    .line 135
    :cond_7
    iget v0, p0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    goto :goto_2
.end method

.method public static getYUVByteSize(II)I
    .locals 3

    .prologue
    .line 360
    mul-int v0, p0, p1

    .line 364
    add-int/lit8 v1, p0, 0x1

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v2, p1, 0x1

    div-int/lit8 v2, v2, 0x2

    mul-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x2

    .line 366
    add-int/2addr v0, v1

    return v0
.end method

.method public static native isBlurredNative([BII)Z
.end method

.method public static makeRectSafe(Lcom/google/bionics/scanner/unveil/util/Size;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 311
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 315
    iget v2, v1, Landroid/graphics/Rect;->left:I

    if-gez v2, :cond_0

    .line 316
    iget v2, v1, Landroid/graphics/Rect;->right:I

    iget v3, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 317
    iput v6, v1, Landroid/graphics/Rect;->left:I

    .line 319
    :cond_0
    iget v2, v1, Landroid/graphics/Rect;->top:I

    if-gez v2, :cond_1

    .line 320
    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    iget v3, v1, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 321
    iput v6, v1, Landroid/graphics/Rect;->top:I

    .line 323
    :cond_1
    iget v2, v1, Landroid/graphics/Rect;->right:I

    iget v3, p0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    if-le v2, v3, :cond_2

    .line 324
    iget v2, v1, Landroid/graphics/Rect;->right:I

    iget v3, p0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    sub-int/2addr v2, v3

    int-to-double v2, v2

    .line 325
    iget v4, v1, Landroid/graphics/Rect;->right:I

    int-to-double v4, v4

    sub-double/2addr v4, v2

    double-to-int v4, v4

    iput v4, v1, Landroid/graphics/Rect;->right:I

    .line 326
    iget v4, v1, Landroid/graphics/Rect;->left:I

    int-to-double v4, v4

    add-double/2addr v2, v4

    double-to-int v2, v2

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 328
    :cond_2
    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    if-le v2, v3, :cond_3

    .line 329
    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    sub-int/2addr v2, v3

    int-to-double v2, v2

    .line 330
    iget v4, v1, Landroid/graphics/Rect;->bottom:I

    int-to-double v4, v4

    sub-double/2addr v4, v2

    double-to-int v4, v4

    iput v4, v1, Landroid/graphics/Rect;->bottom:I

    .line 331
    iget v4, v1, Landroid/graphics/Rect;->top:I

    int-to-double v4, v4

    add-double/2addr v2, v4

    double-to-int v2, v2

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 335
    :cond_3
    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget v3, v1, Landroid/graphics/Rect;->right:I

    if-lt v2, v3, :cond_4

    .line 336
    sget-object v1, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Crop area has nonpositive width"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 351
    :goto_0
    return-object v0

    .line 339
    :cond_4
    iget v2, v1, Landroid/graphics/Rect;->top:I

    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    if-lt v2, v3, :cond_5

    .line 340
    sget-object v1, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Crop area has nonpositive height"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 346
    :cond_5
    iget v0, v1, Landroid/graphics/Rect;->left:I

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 347
    iget v0, v1, Landroid/graphics/Rect;->top:I

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 348
    iget v0, p0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    iget v2, v1, Landroid/graphics/Rect;->right:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 349
    iget v0, p0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    move-object v0, v1

    .line 351
    goto :goto_0
.end method

.method public static native mirrorX(II[B)V
.end method

.method public static rotateImage(IILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 152
    const/high16 v2, 0x3f800000    # 1.0f

    .line 153
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 154
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 156
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    mul-int/2addr v3, v4

    if-le v3, p1, :cond_0

    .line 157
    int-to-float v1, v1

    int-to-float v2, v0

    div-float/2addr v1, v2

    .line 158
    int-to-float v2, p1

    mul-float/2addr v1, v2

    invoke-static {v1}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v1

    float-to-int v1, v1

    .line 160
    int-to-float v2, v1

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 161
    int-to-float v0, v0

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 163
    :cond_0
    sget-object v3, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v4, "scaleFactor %f"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 165
    sget-object v3, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v4, "outputMajorAxis: %d outputMinorAxis: %d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    .line 166
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    .line 165
    invoke-virtual {v3, v4, v5}, Lcom/google/bionics/scanner/unveil/util/Logger;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 170
    if-nez p0, :cond_1

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-ne v1, v3, :cond_1

    .line 171
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-eq v0, v3, :cond_3

    .line 172
    :cond_1
    const/16 v3, 0x5a

    if-eq p0, v3, :cond_2

    const/16 v3, 0x10e

    if-ne p0, v3, :cond_4

    .line 173
    :cond_2
    sget-object v3, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 182
    :goto_0
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 183
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    neg-int v3, v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    .line 184
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    neg-int v4, v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    .line 183
    invoke-virtual {v1, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 185
    int-to-float v3, p0

    invoke-virtual {v1, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 186
    invoke-virtual {v1, v2, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 187
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 189
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 190
    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 191
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 192
    invoke-virtual {v3, p2, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    move-object p2, v0

    .line 194
    :cond_3
    return-object p2

    .line 176
    :cond_4
    sget-object v3, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v0, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public static native rotateYuvFrame(III[B[B)V
.end method
