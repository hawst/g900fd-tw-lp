.class public Lcom/google/bionics/scanner/unveil/ui/DebugView;
.super Landroid/view/View;
.source "DebugView.java"


# instance fields
.field private a:Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;

.field private final a:Lcom/google/bionics/scanner/unveil/util/Logger;

.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/bionics/scanner/unveil/ui/DebugView$RenderCallback;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/DebugView;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/DebugView;->a:Ljava/util/ArrayList;

    .line 36
    return-void
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 59
    new-instance v0, LbhT;

    invoke-direct {v0, p0, p1}, LbhT;-><init>(Lcom/google/bionics/scanner/unveil/ui/DebugView;Z)V

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/unveil/ui/DebugView;->post(Ljava/lang/Runnable;)Z

    .line 64
    return-void
.end method


# virtual methods
.method public addCallback(Lcom/google/bionics/scanner/unveil/ui/DebugView$RenderCallback;)V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/DebugView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/bionics/scanner/unveil/ui/DebugView;->a(Z)V

    .line 41
    return-void
.end method

.method public cycleDebugMode(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 67
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/DebugView;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v3, "Toggling debug rendering."

    new-array v4, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 69
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/ui/DebugView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 70
    :goto_0
    iget-object v3, p0, Lcom/google/bionics/scanner/unveil/ui/DebugView;->a:Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/bionics/scanner/unveil/ui/DebugView;->a:Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;

    .line 71
    invoke-virtual {v3, p1}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->changeMode(Z)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/google/bionics/scanner/unveil/ui/DebugView;->a:Ljava/util/ArrayList;

    .line 72
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_2

    :cond_1
    move v2, v1

    .line 74
    :cond_2
    if-eq v0, v2, :cond_3

    .line 75
    invoke-direct {p0, v2}, Lcom/google/bionics/scanner/unveil/ui/DebugView;->a(Z)V

    .line 76
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/ui/DebugView;->requestLayout()V

    .line 78
    :cond_3
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/ui/DebugView;->postInvalidate()V

    .line 79
    return-void

    :cond_4
    move v0, v2

    .line 69
    goto :goto_0
.end method

.method public declared-synchronized onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 45
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/DebugView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/ui/DebugView$RenderCallback;

    .line 46
    invoke-interface {v0, p1}, Lcom/google/bionics/scanner/unveil/ui/DebugView$RenderCallback;->draw(Landroid/graphics/Canvas;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 49
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/DebugView;->a:Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;

    if-eqz v0, :cond_1

    .line 50
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/DebugView;->a:Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;

    invoke-virtual {v0, p1}, Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;->drawDebug(Landroid/graphics/Canvas;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 52
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized setCallback(Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;)V
    .locals 1

    .prologue
    .line 55
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/ui/DebugView;->a:Lcom/google/bionics/scanner/unveil/nonstop/PreviewLooper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    monitor-exit p0

    return-void

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
