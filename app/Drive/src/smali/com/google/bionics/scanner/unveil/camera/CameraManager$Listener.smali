.class public interface abstract Lcom/google/bionics/scanner/unveil/camera/CameraManager$Listener;
.super Ljava/lang/Object;
.source "CameraManager.java"


# virtual methods
.method public abstract onCameraAcquisitionError()V
.end method

.method public abstract onCameraConnected()V
.end method

.method public abstract onCameraFlashControlError()V
.end method

.method public abstract onCameraLayoutComplete()V
.end method

.method public abstract onCameraPreviewSizeChanged()V
.end method

.method public abstract onCameraQualityError()V
.end method
