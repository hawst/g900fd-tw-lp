.class public Lcom/google/bionics/scanner/rectifier/Quadrilateral$VertexSet;
.super Ljava/lang/Object;
.source "Quadrilateral.java"


# instance fields
.field private final a:[Landroid/graphics/PointF;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 555
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 556
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral$VertexSet;->a:[Landroid/graphics/PointF;

    return-void
.end method

.method static synthetic a(Lcom/google/bionics/scanner/rectifier/Quadrilateral$VertexSet;)[Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral$VertexSet;->a:[Landroid/graphics/PointF;

    return-object v0
.end method


# virtual methods
.method public getPoint(I)Landroid/graphics/PointF;
    .locals 2

    .prologue
    .line 565
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral$VertexSet;->a:[Landroid/graphics/PointF;

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    .line 566
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral$VertexSet;->a:[Landroid/graphics/PointF;

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    aput-object v1, v0, p1

    .line 568
    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral$VertexSet;->a:[Landroid/graphics/PointF;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getPoints()[Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 577
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral$VertexSet;->a:[Landroid/graphics/PointF;

    return-object v0
.end method
