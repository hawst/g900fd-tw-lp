.class public abstract Lcom/google/bionics/scanner/unveil/util/HoneycombAsyncTask;
.super Ljava/lang/Object;
.source "HoneycombAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Result:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TResult;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation
.end method

.method public execute(Ljava/util/concurrent/Executor;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 36
    new-instance v0, LbhX;

    invoke-direct {v0, p0, p2}, LbhX;-><init>(Lcom/google/bionics/scanner/unveil/util/HoneycombAsyncTask;Landroid/os/Handler;)V

    invoke-interface {p1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 48
    return-void
.end method
