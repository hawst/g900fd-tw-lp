.class public Lcom/google/bionics/scanner/PreferencesActivity;
.super Landroid/preference/PreferenceActivity;
.source "PreferencesActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/google/bionics/scanner/PreferencesActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 50
    sget v1, Lcom/google/bionics/scanner/docscanner/R$string;->ds_menu_settings:I

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 52
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 53
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_0

    .line 54
    sget v1, Lcom/google/bionics/scanner/docscanner/R$drawable;->ds_actionbar_close_button:I

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setLogo(I)V

    .line 55
    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeAsUpIndicator(I)V

    .line 57
    :cond_0
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 25
    sget v0, Lcom/google/bionics/scanner/docscanner/R$style;->ds_scanner_preferences_theme:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/PreferencesActivity;->setTheme(I)V

    .line 26
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    sget v0, Lcom/google/bionics/scanner/docscanner/R$xml;->ds_preferences:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/PreferencesActivity;->addPreferencesFromResource(I)V

    .line 30
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 31
    sget v1, Lcom/google/bionics/scanner/docscanner/R$string;->ds_pdf_paper_size_key:I

    invoke-virtual {p0, v1}, Lcom/google/bionics/scanner/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 34
    sget v0, Lcom/google/bionics/scanner/docscanner/R$string;->ds_pdf_paper_size_key:I

    .line 35
    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/PreferencesActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    .line 36
    invoke-static {p0}, LbgB;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 43
    :cond_0
    invoke-direct {p0}, Lcom/google/bionics/scanner/PreferencesActivity;->a()V

    .line 44
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 61
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 62
    invoke-virtual {p0}, Lcom/google/bionics/scanner/PreferencesActivity;->finish()V

    .line 63
    const/4 v0, 0x1

    .line 65
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
