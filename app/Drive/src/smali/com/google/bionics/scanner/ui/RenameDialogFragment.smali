.class public Lcom/google/bionics/scanner/ui/RenameDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "RenameDialogFragment.java"


# static fields
.field private static final a:Lcom/google/bionics/scanner/unveil/util/Logger;


# instance fields
.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 22
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    const-class v1, Lcom/google/bionics/scanner/ui/RenameDialogFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/bionics/scanner/ui/RenameDialogFragment;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/bionics/scanner/ui/RenameDialogFragment;
    .locals 2

    .prologue
    .line 36
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 37
    const-string v1, "pdfFileName"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    new-instance v1, Lcom/google/bionics/scanner/ui/RenameDialogFragment;

    invoke-direct {v1}, Lcom/google/bionics/scanner/ui/RenameDialogFragment;-><init>()V

    .line 39
    invoke-virtual {v1, v0}, Lcom/google/bionics/scanner/ui/RenameDialogFragment;->e(Landroid/os/Bundle;)V

    .line 40
    return-object v1
.end method

.method public static synthetic a()Lcom/google/bionics/scanner/unveil/util/Logger;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/bionics/scanner/ui/RenameDialogFragment;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/ui/RenameDialogFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/RenameDialogFragment;->c:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/bionics/scanner/ui/RenameDialogFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Lcom/google/bionics/scanner/ui/RenameDialogFragment;->c:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 55
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v2

    .line 56
    sget v0, Lcom/google/bionics/scanner/docscanner/R$layout;->ds_rename_dialog:I

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->setContentView(I)V

    .line 57
    sget v0, Lcom/google/bionics/scanner/docscanner/R$string;->ds_rename_scan:I

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->setTitle(I)V

    .line 59
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->new_name:I

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 60
    if-eqz p1, :cond_0

    const-string v1, "newName"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 61
    :goto_0
    if-eqz v1, :cond_1

    .line 62
    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 63
    invoke-static {v0, v2}, LbgB;->a(Landroid/widget/EditText;Landroid/app/Dialog;)V

    .line 65
    sget v1, Lcom/google/bionics/scanner/docscanner/R$id;->btn_ok:I

    invoke-virtual {v2, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 66
    new-instance v3, LbhD;

    invoke-direct {v3, p0, v0}, LbhD;-><init>(Lcom/google/bionics/scanner/ui/RenameDialogFragment;Landroid/widget/EditText;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    sget v0, Lcom/google/bionics/scanner/docscanner/R$id;->btn_cancel:I

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 82
    new-instance v1, LbhE;

    invoke-direct {v1, p0}, LbhE;-><init>(Lcom/google/bionics/scanner/ui/RenameDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    return-object v2

    .line 60
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 61
    :cond_1
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/RenameDialogFragment;->c:Ljava/lang/String;

    goto :goto_1
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->a_(Landroid/os/Bundle;)V

    .line 47
    invoke-virtual {p0}, Lcom/google/bionics/scanner/ui/RenameDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "pdfFileName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/RenameDialogFragment;->c:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 94
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->c(Landroid/os/Bundle;)V

    .line 96
    invoke-virtual {p0}, Lcom/google/bionics/scanner/ui/RenameDialogFragment;->a()Landroid/app/Dialog;

    move-result-object v0

    sget v1, Lcom/google/bionics/scanner/docscanner/R$id;->new_name:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 97
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 98
    const-string v1, "newName"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    return-void
.end method
