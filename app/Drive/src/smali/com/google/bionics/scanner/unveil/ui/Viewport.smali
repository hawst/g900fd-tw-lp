.class public Lcom/google/bionics/scanner/unveil/ui/Viewport;
.super Ljava/lang/Object;
.source "Viewport.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/bionics/scanner/unveil/ui/Viewport;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Lcom/google/bionics/scanner/unveil/util/Logger;


# instance fields
.field private a:I

.field private a:Landroid/graphics/Rect;

.field private a:Lcom/google/bionics/scanner/unveil/util/Size;

.field private b:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>()V

    sput-object v0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    .line 29
    new-instance v0, LbhV;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LbhV;-><init>(LbhU;)V

    sput-object v0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:I

    .line 57
    iput p1, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:I

    .line 58
    return-void
.end method

.method static a(Landroid/graphics/Rect;Lcom/google/bionics/scanner/unveil/util/Size;)Landroid/graphics/Rect;
    .locals 3

    .prologue
    .line 357
    if-eqz p1, :cond_0

    if-nez p0, :cond_1

    .line 358
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 360
    :cond_1
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p0}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 361
    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v2

    if-le v1, v2, :cond_3

    .line 363
    invoke-virtual {p0}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    iget v2, p1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    div-int/lit8 v2, v2, 0x2

    if-le v1, v2, :cond_2

    .line 365
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 380
    :goto_0
    return-object v0

    .line 368
    :cond_2
    iget v1, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    .line 372
    :cond_3
    invoke-virtual {p0}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    iget v2, p1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    div-int/lit8 v2, v2, 0x2

    if-ge v1, v2, :cond_4

    .line 374
    iget v1, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    goto :goto_0

    .line 377
    :cond_4
    iget v1, v0, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    goto :goto_0
.end method

.method public static computeNaturalOrientation(Landroid/content/Context;)I
    .locals 9

    .prologue
    const/4 v7, 0x3

    const/4 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x2

    .line 84
    .line 85
    const-string v0, "window"

    .line 86
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Landroid/view/Display;->getOrientation()I

    move-result v4

    .line 91
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v3, v0, Landroid/content/res/Configuration;->orientation:I

    .line 92
    if-eq v3, v2, :cond_0

    if-eq v3, v1, :cond_0

    .line 94
    sget-object v0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v5, "Current orientation is neither landscape nor portrait! Fallback as portrait"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-virtual {v0, v5, v6}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 102
    :goto_0
    sget-object v5, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v6, "Current orientation %s, screen rotated %d counter-clockwisely ==> natural orientation %s"

    new-array v7, v7, [Ljava/lang/Object;

    if-ne v3, v2, :cond_4

    const-string v3, "landscape"

    :goto_1
    aput-object v3, v7, v8

    mul-int/lit8 v3, v4, 0x5a

    .line 105
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v1

    if-ne v0, v2, :cond_5

    const-string v1, "landscape."

    :goto_2
    aput-object v1, v7, v2

    .line 102
    invoke-virtual {v5, v6, v7}, Lcom/google/bionics/scanner/unveil/util/Logger;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 107
    return v0

    .line 96
    :cond_0
    if-eq v4, v1, :cond_1

    if-ne v4, v7, :cond_3

    .line 97
    :cond_1
    if-ne v3, v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v3

    .line 100
    goto :goto_0

    .line 102
    :cond_4
    const-string v3, "portrait"

    goto :goto_1

    .line 105
    :cond_5
    const-string v1, "portrait."

    goto :goto_2
.end method


# virtual methods
.method public computeImageRotationDegree(Landroid/content/Context;Lcom/google/bionics/scanner/unveil/util/Picture;)I
    .locals 2

    .prologue
    .line 312
    invoke-virtual {p0, p1, p2}, Lcom/google/bionics/scanner/unveil/ui/Viewport;->computeRotationToLandscape(Landroid/content/Context;Lcom/google/bionics/scanner/unveil/util/Picture;)I

    move-result v0

    invoke-virtual {p2}, Lcom/google/bionics/scanner/unveil/util/Picture;->getOrientation()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public computeRotationToLandscape(Landroid/content/Context;Lcom/google/bionics/scanner/unveil/util/Picture;)I
    .locals 4

    .prologue
    .line 324
    invoke-virtual {p0, p1}, Lcom/google/bionics/scanner/unveil/ui/Viewport;->getNaturalOrientation(Landroid/content/Context;)I

    move-result v1

    .line 325
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 326
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 327
    invoke-virtual {v0}, Landroid/view/Display;->getOrientation()I

    move-result v0

    .line 330
    packed-switch v0, :pswitch_data_0

    .line 341
    const/4 v0, 0x0

    .line 344
    :goto_0
    invoke-virtual {p2}, Lcom/google/bionics/scanner/unveil/util/Picture;->getSource()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 346
    add-int/lit8 v0, v0, 0x5a

    .line 349
    :cond_0
    return v0

    .line 332
    :pswitch_0
    const/16 v0, -0xb4

    .line 333
    goto :goto_0

    .line 335
    :pswitch_1
    const/16 v0, -0x10e

    .line 336
    goto :goto_0

    .line 338
    :pswitch_2
    const/16 v0, -0x5a

    .line 339
    goto :goto_0

    .line 330
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 404
    const/4 v0, 0x0

    return v0
.end method

.method public deviceRotationToCameraRotation(I)I
    .locals 2

    .prologue
    .line 154
    iget v0, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 155
    add-int/lit8 v0, p1, 0x5a

    rem-int/lit16 p1, v0, 0x168

    .line 157
    :cond_0
    return p1
.end method

.method public getLatestBarcodeQueryCrop()Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 179
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    return-object v0
.end method

.method public getLatestBarcodeQueryRotation()I
    .locals 1

    .prologue
    .line 171
    iget v0, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->b:I

    return v0
.end method

.method public getNaturalOrientation(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:I

    return v0
.end method

.method public getRotatedBarcodeQueryCropSize(I)Lcom/google/bionics/scanner/unveil/util/Size;
    .locals 3

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    .line 143
    const/4 v0, 0x0

    .line 145
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Size;

    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Landroid/graphics/Rect;

    .line 146
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    iget-object v2, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Size;-><init>(II)V

    .line 145
    invoke-static {v0, p1}, Lcom/google/bionics/scanner/unveil/util/Size;->getRotatedSize(Lcom/google/bionics/scanner/unveil/util/Size;I)Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v0

    goto :goto_0
.end method

.method public getRotatedPreviewSize(I)Lcom/google/bionics/scanner/unveil/util/Size;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    if-nez v0, :cond_0

    .line 126
    const/4 v0, 0x0

    .line 133
    :goto_0
    return-object v0

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Landroid/graphics/Rect;

    if-eqz v0, :cond_1

    .line 130
    invoke-virtual {p0, p1}, Lcom/google/bionics/scanner/unveil/ui/Viewport;->getRotatedBarcodeQueryCropSize(I)Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v0

    goto :goto_0

    .line 133
    :cond_1
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    invoke-static {v0, p1}, Lcom/google/bionics/scanner/unveil/util/Size;->getRotatedSize(Lcom/google/bionics/scanner/unveil/util/Size;I)Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v0

    goto :goto_0
.end method

.method public isBarcodeBoxGood(Landroid/graphics/Rect;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 294
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    if-nez v1, :cond_0

    .line 295
    sget-object v1, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Cannot determine barcode box goodness without a preview size."

    new-array v3, v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 308
    :goto_0
    return v0

    .line 299
    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    invoke-static {p1, v0}, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a(Landroid/graphics/Rect;Lcom/google/bionics/scanner/unveil/util/Size;)Landroid/graphics/Rect;

    move-result-object v0

    .line 302
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    .line 303
    iget v2, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v1

    iput v2, v0, Landroid/graphics/Rect;->left:I

    .line 304
    iget v2, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v1

    iput v2, v0, Landroid/graphics/Rect;->right:I

    .line 305
    iget v2, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v1

    iput v2, v0, Landroid/graphics/Rect;->top:I

    .line 306
    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 308
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    iget v1, v1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    iget v2, v2, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    goto :goto_0
.end method

.method public rotateAndScaleBarcodeBox(Landroid/graphics/Rect;ILcom/google/bionics/scanner/unveil/util/Size;)Landroid/graphics/Rect;
    .locals 4

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    if-nez v0, :cond_0

    .line 259
    sget-object v0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Failed to rotate and scale bounding box %s because we don\'t have a preview size"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 278
    :goto_0
    return-object p1

    .line 266
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/bionics/scanner/unveil/ui/Viewport;->rotateBarcodeBox(Landroid/graphics/Rect;I)Landroid/graphics/Rect;

    move-result-object p1

    .line 271
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Landroid/graphics/Rect;

    if-eqz v0, :cond_1

    .line 272
    invoke-virtual {p0, p2}, Lcom/google/bionics/scanner/unveil/ui/Viewport;->getRotatedBarcodeQueryCropSize(I)Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v0

    iget v0, v0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    .line 273
    :goto_1
    iget v1, p3, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    .line 274
    iget v1, p1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    mul-float/2addr v1, v0

    float-to-int v1, v1

    iput v1, p1, Landroid/graphics/Rect;->left:I

    .line 275
    iget v1, p1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    mul-float/2addr v1, v0

    float-to-int v1, v1

    iput v1, p1, Landroid/graphics/Rect;->top:I

    .line 276
    iget v1, p1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    mul-float/2addr v1, v0

    float-to-int v1, v1

    iput v1, p1, Landroid/graphics/Rect;->right:I

    .line 277
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    .line 272
    :cond_1
    invoke-virtual {p0, p2}, Lcom/google/bionics/scanner/unveil/ui/Viewport;->getRotatedPreviewSize(I)Lcom/google/bionics/scanner/unveil/util/Size;

    move-result-object v0

    iget v0, v0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    goto :goto_1
.end method

.method public rotateBarcodeBox(Landroid/graphics/Rect;I)Landroid/graphics/Rect;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 192
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    if-nez v0, :cond_0

    .line 193
    sget-object v0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Tried to rotate a box without setting the preview size. Bailing out on rotation."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 239
    :goto_0
    return-object p1

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    iget v1, v0, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    .line 200
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    iget v0, v0, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    .line 202
    iget-object v3, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Landroid/graphics/Rect;

    if-eqz v3, :cond_1

    .line 203
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Landroid/graphics/Rect;

    iget v3, v0, Landroid/graphics/Rect;->top:I

    .line 204
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 205
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    .line 206
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 209
    :goto_1
    iget v4, p1, Landroid/graphics/Rect;->top:I

    sub-int v7, v4, v3

    .line 210
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    sub-int v6, v4, v3

    .line 211
    iget v4, p1, Landroid/graphics/Rect;->left:I

    sub-int v5, v4, v2

    .line 212
    iget v4, p1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v4, v2

    .line 214
    sparse-switch p2, :sswitch_data_0

    move v0, v4

    move v1, v5

    move v2, v6

    move v3, v7

    .line 239
    :goto_2
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1, v1, v3, v0, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0

    .line 217
    :sswitch_0
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v4

    add-int v4, v0, v3

    .line 218
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    add-int v3, v4, v0

    .line 219
    iget v0, p1, Landroid/graphics/Rect;->right:I

    sub-int v0, v1, v0

    add-int v1, v0, v2

    .line 220
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    add-int/2addr v0, v1

    move v2, v3

    move v3, v4

    .line 221
    goto :goto_2

    .line 225
    :sswitch_1
    iget v1, p1, Landroid/graphics/Rect;->left:I

    sub-int v4, v1, v2

    .line 226
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    add-int v2, v4, v1

    .line 227
    iget v1, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    add-int/2addr v0, v3

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    sub-int v1, v0, v1

    .line 228
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    add-int/2addr v0, v1

    move v3, v4

    .line 229
    goto :goto_2

    .line 233
    :sswitch_2
    iget v0, p1, Landroid/graphics/Rect;->left:I

    sub-int v0, v1, v0

    add-int/2addr v2, v0

    .line 234
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    sub-int v4, v2, v0

    .line 235
    iget v0, p1, Landroid/graphics/Rect;->top:I

    sub-int v1, v0, v3

    .line 236
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    add-int/2addr v0, v1

    move v3, v4

    goto :goto_2

    :cond_1
    move v3, v2

    goto :goto_1

    .line 214
    nop

    :sswitch_data_0
    .sparse-switch
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_0
        0x10e -> :sswitch_2
    .end sparse-switch
.end method

.method public setLatestBarcodeQueryCrop(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 175
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Landroid/graphics/Rect;

    .line 176
    return-void

    .line 175
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public setLatestBarcodeQueryRotation(I)V
    .locals 0

    .prologue
    .line 167
    iput p1, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->b:I

    .line 168
    return-void
.end method

.method public setPreviewSize(Lcom/google/bionics/scanner/unveil/util/Size;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    .line 112
    return-void
.end method

.method public updateNaturalOrientation(I)V
    .locals 0

    .prologue
    .line 77
    iput p1, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:I

    .line 78
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 409
    iget v0, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 410
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Landroid/graphics/Rect;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 411
    iget v0, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 412
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/Viewport;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 413
    return-void
.end method
