.class public Lcom/google/bionics/scanner/ui/DocumentEditorView;
.super Landroid/view/View;
.source "DocumentEditorView.java"


# instance fields
.field private a:F

.field private a:I

.field private a:J

.field private final a:Landroid/graphics/Matrix;

.field private final a:Landroid/graphics/Paint;

.field private final a:Landroid/graphics/Rect;

.field private a:Lbhn;

.field private a:Lbho;

.field private final a:Ljava/lang/String;

.field private b:F

.field private b:I

.field private b:J

.field private final b:Landroid/graphics/Matrix;

.field private final b:Landroid/graphics/Paint;

.field private c:F

.field private c:I

.field private final c:Landroid/graphics/Paint;

.field private d:F

.field private d:I

.field private final d:Landroid/graphics/Paint;

.field private e:F

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 131
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 65
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Landroid/graphics/Paint;

    .line 66
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->b:Landroid/graphics/Paint;

    .line 67
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->c:Landroid/graphics/Paint;

    .line 68
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->d:Landroid/graphics/Paint;

    .line 70
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Landroid/graphics/Matrix;

    .line 71
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->b:Landroid/graphics/Matrix;

    .line 75
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:I

    .line 80
    iput v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->b:F

    .line 96
    iput-wide v6, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:J

    .line 98
    iput-wide v6, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->b:J

    .line 111
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Landroid/graphics/Rect;

    .line 128
    sget-object v0, Lbho;->c:Lbho;

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Lbho;

    .line 132
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 134
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->b:Landroid/graphics/Paint;

    const v1, -0xa0a0a1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 136
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->c:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 137
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 138
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 139
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 141
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 142
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 143
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->d:Landroid/graphics/Paint;

    const/high16 v1, 0x42000000    # 32.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 144
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/bionics/scanner/docscanner/R$string;->ds_document_editor_msg_no_pages:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Ljava/lang/String;

    .line 145
    return-void
.end method

.method private a()LbgX;
    .locals 1

    .prologue
    .line 406
    invoke-static {}, Lbhf;->a()Lbhf;

    move-result-object v0

    invoke-virtual {v0}, Lbhf;->a()LbgX;

    move-result-object v0

    return-object v0
.end method

.method private a(F)V
    .locals 4

    .prologue
    .line 319
    float-to-int v0, p1

    iput v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->c:I

    .line 320
    iget v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->c:I

    iput v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->b:I

    .line 321
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:J

    .line 322
    iget-wide v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:J

    const-wide/16 v2, 0x64

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->b:J

    .line 323
    return-void
.end method

.method private a(Landroid/graphics/Canvas;IF)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 192
    invoke-direct {p0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a()LbgX;

    move-result-object v0

    .line 193
    invoke-virtual {v0, p2}, LbgX;->a(I)LbgY;

    move-result-object v1

    .line 194
    invoke-virtual {v0, v1}, LbgX;->a(LbgY;)Lcom/google/bionics/scanner/unveil/util/Picture;

    move-result-object v2

    .line 196
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 197
    const/4 v0, 0x0

    invoke-virtual {p1, p3, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 198
    invoke-virtual {v1}, LbgY;->a()I

    move-result v0

    int-to-float v0, v0

    .line 199
    iget v3, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:I

    if-ne p2, v3, :cond_0

    .line 200
    iget v3, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->b:F

    add-float/2addr v0, v3

    .line 202
    :cond_0
    iget-object v3, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Landroid/graphics/Matrix;

    iget v4, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->g:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    iget v5, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->h:I

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v3, v0, v4, v5}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 203
    iget-object v3, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Landroid/graphics/Matrix;

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 206
    invoke-virtual {v1}, LbgY;->a()F

    move-result v1

    iget v3, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->g:I

    iget v4, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->h:I

    invoke-static {v1, v0, v3, v4}, Lbhy;->a(FFII)Landroid/graphics/Rect;

    move-result-object v0

    .line 225
    invoke-virtual {v2}, Lcom/google/bionics/scanner/unveil/util/Picture;->peekBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 226
    if-eqz v1, :cond_1

    .line 227
    new-instance v2, Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-direct {v2, v6, v6, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 228
    iget-object v3, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 234
    :goto_0
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 235
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 236
    return-void

    .line 230
    :cond_1
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:I

    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Lbhn;

    .line 160
    return-void
.end method

.method public a(Lbhn;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Lbhn;

    .line 155
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 240
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 241
    invoke-direct {p0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a()LbgX;

    move-result-object v0

    invoke-virtual {v0}, LbgX;->a()I

    move-result v1

    .line 245
    if-lez v1, :cond_3

    .line 246
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 247
    iget v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->e:I

    iget v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->g:I

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    const/high16 v2, 0x41200000    # 10.0f

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 249
    iget v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:I

    add-int/lit8 v0, v0, -0x1

    .line 250
    :goto_0
    iget v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:I

    add-int/lit8 v2, v2, 0x1

    if-gt v0, v2, :cond_1

    .line 251
    if-ltz v0, :cond_0

    if-ge v0, v1, :cond_0

    iget v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:I

    if-eq v0, v2, :cond_0

    .line 252
    iget v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->c:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->d:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:I

    sub-int v3, v0, v3

    iget v4, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->i:I

    mul-int/2addr v3, v4

    int-to-float v3, v3

    add-float/2addr v2, v3

    .line 254
    invoke-direct {p0, p1, v0, v2}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a(Landroid/graphics/Canvas;IF)V

    .line 250
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 259
    :cond_1
    iget v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->c:I

    int-to-float v0, v0

    iget v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->d:F

    add-float/2addr v0, v2

    .line 264
    iput v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->e:F

    .line 265
    iget v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:I

    iput v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->d:I

    .line 268
    iget v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->d:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->e:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x3e800000    # 0.25f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    .line 269
    iget v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->d:F

    cmpg-float v2, v2, v6

    if-gez v2, :cond_5

    iget v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:I

    add-int/lit8 v2, v2, 0x1

    if-ge v2, v1, :cond_5

    .line 271
    iget v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->d:F

    iget v3, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->i:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iput v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->e:F

    .line 272
    iget v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->d:I

    .line 280
    :cond_2
    :goto_1
    iget v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:I

    invoke-direct {p0, p1, v2, v0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a(Landroid/graphics/Canvas;IF)V

    .line 281
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 282
    invoke-virtual {p0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/bionics/scanner/docscanner/R$string;->ds_document_editor_view_content_description:I

    new-array v3, v8, [Ljava/lang/Object;

    iget v4, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:I

    add-int/lit8 v4, v4, 0x1

    .line 283
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 282
    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 286
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 287
    iget v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->e:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->f:I

    iget v3, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->f:I

    iget v4, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->h:I

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x5

    int-to-float v2, v2

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 289
    if-lez v1, :cond_6

    const-string v0, "%d/%d"

    new-array v1, v8, [Ljava/lang/Object;

    iget v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:I

    add-int/lit8 v2, v2, 0x1

    .line 290
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-direct {p0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a()LbgX;

    move-result-object v2

    invoke-virtual {v2}, LbgX;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 292
    :goto_2
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v5, v2, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 293
    iget-object v1, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 294
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 297
    iget v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->c:I

    if-eqz v0, :cond_4

    .line 298
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    .line 299
    iget-wide v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->b:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_7

    .line 302
    iget-wide v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    .line 303
    iget v1, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->b:I

    mul-int/2addr v0, v1

    neg-int v0, v0

    div-int/lit8 v0, v0, 0x64

    .line 304
    iget v1, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->b:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->c:I

    .line 308
    :goto_3
    invoke-virtual {p0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->invalidate()V

    .line 310
    :cond_4
    return-void

    .line 273
    :cond_5
    iget v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->d:F

    cmpl-float v2, v2, v6

    if-ltz v2, :cond_2

    iget v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:I

    if-lez v2, :cond_2

    .line 275
    iget v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->d:F

    iget v3, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->i:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iput v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->e:F

    .line 276
    iget v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->d:I

    goto/16 :goto_1

    .line 290
    :cond_6
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Ljava/lang/String;

    goto :goto_2

    .line 306
    :cond_7
    iput v5, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->c:I

    goto :goto_3
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    .prologue
    const v2, 0x3f666666    # 0.9f

    .line 327
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 332
    invoke-virtual {p0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->e:I

    .line 333
    invoke-virtual {p0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->f:I

    .line 336
    iget v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->e:I

    iget v1, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->f:I

    if-le v0, v1, :cond_0

    .line 341
    iget v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->f:I

    int-to-float v0, v0

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->h:I

    .line 342
    iget v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->h:I

    iget v1, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->e:I

    iget v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->f:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    const v2, 0x3e4ccccd    # 0.2f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->g:I

    .line 343
    iget v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->g:I

    iget v1, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->e:I

    iget v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->g:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    const v2, 0x3dcccccd    # 0.1f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->i:I

    .line 350
    :goto_0
    return-void

    .line 346
    :cond_0
    iget v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->e:I

    int-to-float v0, v0

    const v1, 0x3f59999a    # 0.85f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->g:I

    .line 347
    iget v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->f:I

    int-to-float v0, v0

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->h:I

    .line 348
    iget v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->e:I

    int-to-float v0, v0

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->i:I

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 354
    invoke-direct {p0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a()LbgX;

    move-result-object v2

    invoke-virtual {v2}, LbgX;->a()I

    move-result v2

    if-nez v2, :cond_0

    .line 401
    :goto_0
    return v0

    .line 357
    :cond_0
    invoke-direct {p0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a()LbgX;

    move-result-object v2

    iget v3, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:I

    invoke-virtual {v2, v3}, LbgX;->a(I)LbgY;

    move-result-object v2

    .line 359
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    and-int/lit16 v3, v3, 0xff

    packed-switch v3, :pswitch_data_0

    :cond_1
    :goto_1
    :pswitch_0
    move v0, v1

    .line 401
    goto :goto_0

    .line 361
    :pswitch_1
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Lbho;

    sget-object v2, Lbho;->c:Lbho;

    if-ne v0, v2, :cond_1

    .line 362
    sget-object v0, Lbho;->b:Lbho;

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Lbho;

    .line 363
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->c:F

    goto :goto_1

    .line 368
    :pswitch_2
    sget-object v2, Lbho;->a:Lbho;

    iput-object v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Lbho;

    .line 370
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    .line 369
    invoke-static {v2, v0, v3, v4}, Lbhy;->a(FFFF)F

    move-result v0

    iput v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:F

    .line 371
    iput v5, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->b:F

    goto :goto_1

    .line 376
    :pswitch_3
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Lbho;

    sget-object v3, Lbho;->a:Lbho;

    if-ne v0, v3, :cond_3

    .line 377
    iget v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->b:F

    invoke-virtual {v2}, LbgY;->a()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v0, v3

    invoke-static {v0}, Lbhy;->a(F)I

    move-result v0

    invoke-virtual {v2, v0}, LbgY;->a(I)LbgY;

    .line 378
    iput v5, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->b:F

    .line 379
    invoke-virtual {p0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->invalidate()V

    .line 387
    :cond_2
    :goto_2
    sget-object v0, Lbho;->c:Lbho;

    iput-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Lbho;

    goto :goto_1

    .line 380
    :cond_3
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Lbho;

    sget-object v2, Lbho;->b:Lbho;

    if-ne v0, v2, :cond_2

    .line 383
    iget v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->e:F

    invoke-direct {p0, v0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a(F)V

    .line 384
    iput v5, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->d:F

    .line 385
    iget v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->d:I

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->setActivePageIndex(I)V

    goto :goto_2

    .line 391
    :pswitch_4
    iget-object v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Lbho;

    sget-object v3, Lbho;->b:Lbho;

    if-ne v2, v3, :cond_5

    .line 392
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->c:F

    sub-float/2addr v0, v2

    iput v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->d:F

    .line 398
    :cond_4
    :goto_3
    invoke-virtual {p0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->invalidate()V

    goto :goto_1

    .line 393
    :cond_5
    iget-object v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Lbho;

    sget-object v3, Lbho;->a:Lbho;

    if-ne v2, v3, :cond_4

    .line 395
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    .line 394
    invoke-static {v2, v0, v3, v4}, Lbhy;->a(FFFF)F

    move-result v0

    iget v2, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:F

    sub-float/2addr v0, v2

    iput v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->b:F

    goto :goto_3

    .line 359
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setActivePageIndex(I)V
    .locals 2

    .prologue
    .line 168
    iput p1, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:I

    .line 169
    invoke-virtual {p0}, Lcom/google/bionics/scanner/ui/DocumentEditorView;->invalidate()V

    .line 170
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Lbhn;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:Lbhn;

    iget v1, p0, Lcom/google/bionics/scanner/ui/DocumentEditorView;->a:I

    invoke-interface {v0, v1}, Lbhn;->a(I)V

    .line 173
    :cond_0
    return-void
.end method
