.class public Lcom/google/bionics/scanner/unveil/ui/PreviewOverlay;
.super Landroid/view/View;
.source "PreviewOverlay.java"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/scanner/unveil/ui/PreviewOverlay$OverlayRenderer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/PreviewOverlay;->a:Ljava/util/List;

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/PreviewOverlay;->a:Ljava/util/List;

    .line 38
    return-void
.end method


# virtual methods
.method public addRenderer(Lcom/google/bionics/scanner/unveil/ui/PreviewOverlay$OverlayRenderer;)V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/PreviewOverlay;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/ui/PreviewOverlay;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/unveil/ui/PreviewOverlay$OverlayRenderer;

    .line 53
    invoke-interface {v0, p1}, Lcom/google/bionics/scanner/unveil/ui/PreviewOverlay$OverlayRenderer;->render(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 55
    :cond_0
    return-void
.end method
