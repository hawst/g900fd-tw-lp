.class public Lcom/google/bionics/scanner/unveil/util/PictureFactory;
.super Ljava/lang/Object;
.source "PictureFactory.java"


# static fields
.field public static final BYTES_PER_PIXEL:I = 0x4

.field private static final a:Lcom/google/bionics/scanner/unveil/util/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>()V

    sput-object v0, Lcom/google/bionics/scanner/unveil/util/PictureFactory;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    return-void
.end method

.method private static a(ILandroid/graphics/BitmapFactory$Options;)V
    .locals 4

    .prologue
    .line 111
    iget v2, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 112
    iget v1, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 113
    const/4 v0, 0x1

    .line 114
    :goto_0
    mul-int v3, v2, v1

    mul-int/lit8 v3, v3, 0x4

    if-le v3, p0, :cond_0

    .line 115
    mul-int/lit8 v0, v0, 0x2

    .line 116
    div-int/lit8 v2, v2, 0x2

    .line 117
    div-int/lit8 v1, v1, 0x2

    goto :goto_0

    .line 119
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 120
    iput v0, p1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 121
    return-void
.end method

.method private static a(Ljava/io/InputStream;)V
    .locals 4

    .prologue
    .line 128
    :try_start_0
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    :goto_0
    return-void

    .line 129
    :catch_0
    move-exception v0

    .line 130
    sget-object v1, Lcom/google/bionics/scanner/unveil/util/PictureFactory;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Failed to close stream."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static create(Lcom/google/bionics/scanner/unveil/util/Picture;)Lcom/google/bionics/scanner/unveil/util/Picture;
    .locals 1

    .prologue
    .line 199
    new-instance v0, LbhZ;

    invoke-direct {v0, p0}, LbhZ;-><init>(Lcom/google/bionics/scanner/unveil/util/Picture;)V

    return-object v0
.end method

.method public static createBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;II)Lcom/google/bionics/scanner/unveil/util/Picture;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 77
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 78
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 80
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 83
    invoke-static {v1, v3, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 86
    invoke-static {v1}, Lcom/google/bionics/scanner/unveil/util/PictureFactory;->a(Ljava/io/InputStream;)V

    .line 88
    invoke-static {p2, v0}, Lcom/google/bionics/scanner/unveil/util/PictureFactory;->a(ILandroid/graphics/BitmapFactory$Options;)V

    .line 93
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 95
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    invoke-static {v2, v3, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 96
    invoke-static {v1}, Lcom/google/bionics/scanner/unveil/util/PictureFactory;->a(Ljava/io/InputStream;)V

    .line 98
    if-nez v0, :cond_0

    .line 99
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/PictureFactory$ImageDecodingException;

    const-string v1, "BitmapFactory failed to decode stream"

    invoke-direct {v0, v1}, Lcom/google/bionics/scanner/unveil/util/PictureFactory$ImageDecodingException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 102
    :cond_0
    new-instance v1, LbhZ;

    new-instance v2, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;

    invoke-direct {v2, v0, p3}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;-><init>(Landroid/graphics/Bitmap;I)V

    invoke-direct {v1, v2}, LbhZ;-><init>(Lcom/google/bionics/scanner/unveil/util/Picture;)V

    return-object v1
.end method

.method public static createBitmap(Landroid/graphics/Bitmap;I)Lcom/google/bionics/scanner/unveil/util/Picture;
    .locals 2

    .prologue
    .line 45
    new-instance v0, LbhZ;

    new-instance v1, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;

    invoke-direct {v1, p0, p1}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;-><init>(Landroid/graphics/Bitmap;I)V

    invoke-direct {v0, v1}, LbhZ;-><init>(Lcom/google/bionics/scanner/unveil/util/Picture;)V

    return-object v0
.end method

.method public static createBitmap([BIII)Lcom/google/bionics/scanner/unveil/util/Picture;
    .locals 2

    .prologue
    .line 41
    new-instance v0, LbhZ;

    new-instance v1, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/bionics/scanner/unveil/util/BitmapPicture;-><init>([BIII)V

    invoke-direct {v0, v1}, LbhZ;-><init>(Lcom/google/bionics/scanner/unveil/util/Picture;)V

    return-object v0
.end method

.method public static createBitmapWithDownsampling([BI)Lcom/google/bionics/scanner/unveil/util/Picture;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 142
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 143
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 146
    array-length v1, p0

    invoke-static {p0, v2, v1, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 148
    invoke-static {p1, v0}, Lcom/google/bionics/scanner/unveil/util/PictureFactory;->a(ILandroid/graphics/BitmapFactory$Options;)V

    .line 150
    array-length v1, p0

    invoke-static {p0, v2, v1, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 152
    if-nez v0, :cond_0

    .line 153
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/PictureFactory$ImageDecodingException;

    const-string v1, "Failed to downsample image"

    invoke-direct {v0, v1}, Lcom/google/bionics/scanner/unveil/util/PictureFactory$ImageDecodingException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 156
    :cond_0
    invoke-static {v0, v2}, Lcom/google/bionics/scanner/unveil/util/PictureFactory;->createBitmap(Landroid/graphics/Bitmap;I)Lcom/google/bionics/scanner/unveil/util/Picture;

    move-result-object v0

    return-object v0
.end method

.method public static createDownsampledBitmap([BI)Lcom/google/bionics/scanner/unveil/util/Picture;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 182
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 183
    iput p1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 185
    array-length v1, p0

    invoke-static {p0, v2, v1, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 187
    if-nez v0, :cond_0

    .line 188
    sget-object v0, Lcom/google/bionics/scanner/unveil/util/PictureFactory;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v1, "Failed to downsample JPEG data!"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/bionics/scanner/unveil/util/Logger;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 189
    const/4 v0, 0x0

    .line 192
    :goto_0
    return-object v0

    :cond_0
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/google/bionics/scanner/unveil/util/PictureFactory;->createBitmap(Landroid/graphics/Bitmap;I)Lcom/google/bionics/scanner/unveil/util/Picture;

    move-result-object v0

    goto :goto_0
.end method

.method public static createJpeg(Lcom/google/bionics/scanner/unveil/util/Picture;)Lcom/google/bionics/scanner/unveil/util/Picture;
    .locals 5

    .prologue
    .line 57
    new-instance v0, LbhZ;

    new-instance v1, Lcom/google/bionics/scanner/unveil/util/JpegPicture;

    .line 58
    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/Picture;->getJpegData()[B

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/Picture;->getOrientation()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/bionics/scanner/unveil/util/Picture;->getSource()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/bionics/scanner/unveil/util/JpegPicture;-><init>([BII)V

    invoke-direct {v0, v1}, LbhZ;-><init>(Lcom/google/bionics/scanner/unveil/util/Picture;)V

    return-object v0
.end method

.method public static createJpeg([BI)Lcom/google/bionics/scanner/unveil/util/Picture;
    .locals 2

    .prologue
    .line 49
    new-instance v0, LbhZ;

    new-instance v1, Lcom/google/bionics/scanner/unveil/util/JpegPicture;

    invoke-direct {v1, p0, p1}, Lcom/google/bionics/scanner/unveil/util/JpegPicture;-><init>([BI)V

    invoke-direct {v0, v1}, LbhZ;-><init>(Lcom/google/bionics/scanner/unveil/util/Picture;)V

    return-object v0
.end method

.method public static createJpeg([BII)Lcom/google/bionics/scanner/unveil/util/Picture;
    .locals 2

    .prologue
    .line 53
    new-instance v0, LbhZ;

    new-instance v1, Lcom/google/bionics/scanner/unveil/util/JpegPicture;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/bionics/scanner/unveil/util/JpegPicture;-><init>([BII)V

    invoke-direct {v0, v1}, LbhZ;-><init>(Lcom/google/bionics/scanner/unveil/util/Picture;)V

    return-object v0
.end method

.method public static loadBitmap(Ljava/io/File;I)Lcom/google/bionics/scanner/unveil/util/Picture;
    .locals 2

    .prologue
    .line 167
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 168
    iput p1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 170
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 171
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/bionics/scanner/unveil/util/PictureFactory;->createBitmap(Landroid/graphics/Bitmap;I)Lcom/google/bionics/scanner/unveil/util/Picture;

    move-result-object v0

    return-object v0
.end method
