.class public Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;
.super Ljava/lang/Object;
.source "PreviewFetcher.java"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;


# instance fields
.field protected final a:Landroid/hardware/Camera$PreviewCallback;

.field protected final a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

.field private final a:Lcom/google/bionics/scanner/unveil/util/Size;

.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[B>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;Landroid/hardware/Camera$PreviewCallback;Lcom/google/bionics/scanner/unveil/util/Size;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;->a:Ljava/util/ArrayList;

    .line 28
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    .line 29
    iput-object p2, p0, Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;->a:Landroid/hardware/Camera$PreviewCallback;

    .line 30
    iput-object p3, p0, Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    .line 31
    return-void
.end method


# virtual methods
.method protected a(Lcom/google/bionics/scanner/unveil/util/Size;)I
    .locals 2

    .prologue
    .line 70
    iget v0, p1, Lcom/google/bionics/scanner/unveil/util/Size;->width:I

    iget v1, p1, Lcom/google/bionics/scanner/unveil/util/Size;->height:I

    invoke-static {v0, v1}, Lcom/google/bionics/scanner/unveil/util/ImageUtils;->getYUVByteSize(II)I

    move-result v0

    return v0
.end method

.method protected a()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;->a:Ljava/util/ArrayList;

    .line 38
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    invoke-interface {v0, p0}, Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    .line 39
    return-void
.end method

.method public addPreviewBuffer([B)V
    .locals 2

    .prologue
    .line 79
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;->a:Ljava/util/ArrayList;

    monitor-enter v1

    .line 80
    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    monitor-exit v1

    .line 82
    return-void

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected b()V
    .locals 3

    .prologue
    .line 46
    .line 48
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;->a:Ljava/util/ArrayList;

    monitor-enter v1

    .line 50
    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;->a:Lcom/google/bionics/scanner/unveil/util/Size;

    invoke-virtual {p0, v0}, Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;->a(Lcom/google/bionics/scanner/unveil/util/Size;)I

    move-result v0

    new-array v0, v0, [B

    .line 55
    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    invoke-interface {v1, v0}, Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;->addCallbackBuffer([B)V

    .line 58
    return-void

    .line 53
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;->a:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    goto :goto_0

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    iput-object v1, p0, Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;->a:Ljava/util/ArrayList;

    .line 66
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;->a:Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;

    invoke-interface {v0, v1}, Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 67
    return-void
.end method

.method public onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/PreviewFetcher;->a:Landroid/hardware/Camera$PreviewCallback;

    invoke-interface {v0, p1, p2}, Landroid/hardware/Camera$PreviewCallback;->onPreviewFrame([BLandroid/hardware/Camera;)V

    .line 87
    return-void
.end method
