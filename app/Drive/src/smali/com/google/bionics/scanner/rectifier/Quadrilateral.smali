.class public Lcom/google/bionics/scanner/rectifier/Quadrilateral;
.super Ljava/lang/Object;
.source "Quadrilateral.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements LbhH;
.implements Ljava/io/Serializable;


# static fields
.field public static final BOTTOM_LINE_INDEX:I = 0x2

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/bionics/scanner/rectifier/Quadrilateral;",
            ">;"
        }
    .end annotation
.end field

.field public static final LEFT_LINE_INDEX:I = 0x3

.field public static final LOWER_LEFT_VERTEX_INDEX:I = 0x3

.field public static final LOWER_RIGHT_VERTEX_INDEX:I = 0x2

.field public static final NUM_VERTICES:I = 0x4

.field public static final RIGHT_LINE_INDEX:I = 0x1

.field public static final TOP_LINE_INDEX:I = 0x0

.field public static final UPPER_LEFT_VERTEX_INDEX:I = 0x0

.field public static final UPPER_RIGHT_VERTEX_INDEX:I = 0x1

.field private static final a:Lcom/google/bionics/scanner/unveil/util/Logger;


# instance fields
.field private a:F

.field private a:I

.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/bionics/scanner/rectifier/Line;",
            ">;"
        }
    .end annotation
.end field

.field private final a:[I

.field private final a:[Lcom/google/bionics/scanner/rectifier/Line;

.field private b:F

.field private b:I

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/bionics/scanner/rectifier/Line;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/bionics/scanner/rectifier/Line;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/bionics/scanner/rectifier/Line;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/google/bionics/scanner/unveil/util/Logger;

    invoke-direct {v0}, Lcom/google/bionics/scanner/unveil/util/Logger;-><init>()V

    sput-object v0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    .line 69
    const-string v0, "rectifier"

    invoke-static {v0}, Lcom/google/bionics/scanner/unveil/util/ResourceUtils;->loadNativeLibrary(Ljava/lang/String;)V

    .line 685
    new-instance v0, LbgR;

    invoke-direct {v0}, LbgR;-><init>()V

    sput-object v0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x4

    .line 657
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-array v1, v4, [Lcom/google/bionics/scanner/rectifier/Line;

    iput-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    .line 63
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:F

    .line 66
    new-array v1, v4, [I

    iput-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    .line 658
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:I

    .line 659
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:I

    .line 660
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v1

    iput v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:F

    .line 661
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v1

    iput v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:F

    .line 662
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:Ljava/util/ArrayList;

    move v1, v0

    .line 663
    :goto_0
    if-ge v1, v4, :cond_0

    .line 664
    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a(Landroid/os/Parcel;)Lcom/google/bionics/scanner/rectifier/Line;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 663
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 666
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:Ljava/util/ArrayList;

    move v1, v0

    .line 667
    :goto_1
    if-ge v1, v4, :cond_1

    .line 668
    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a(Landroid/os/Parcel;)Lcom/google/bionics/scanner/rectifier/Line;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 667
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 670
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->c:Ljava/util/ArrayList;

    move v1, v0

    .line 671
    :goto_2
    if-ge v1, v4, :cond_2

    .line 672
    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->c:Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a(Landroid/os/Parcel;)Lcom/google/bionics/scanner/rectifier/Line;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 671
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 674
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->d:Ljava/util/ArrayList;

    move v1, v0

    .line 675
    :goto_3
    if-ge v1, v4, :cond_3

    .line 676
    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->d:Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a(Landroid/os/Parcel;)Lcom/google/bionics/scanner/rectifier/Line;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 675
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 678
    :cond_3
    :goto_4
    if-ge v0, v4, :cond_4

    .line 679
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    invoke-direct {p0, p1}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a(Landroid/os/Parcel;)Lcom/google/bionics/scanner/rectifier/Line;

    move-result-object v2

    aput-object v2, v1, v0

    .line 678
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 681
    :cond_4
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readIntArray([I)V

    .line 682
    return-void
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;LbgR;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/bionics/scanner/rectifier/Quadrilateral;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-array v0, v3, [Lcom/google/bionics/scanner/rectifier/Line;

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    .line 63
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:F

    .line 66
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    .line 129
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:Ljava/util/ArrayList;

    .line 130
    iget-object v0, p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/rectifier/Line;

    .line 131
    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/rectifier/Line;->copy()Lcom/google/bionics/scanner/rectifier/Line;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 133
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:Ljava/util/ArrayList;

    .line 134
    iget-object v0, p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/rectifier/Line;

    .line 135
    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/rectifier/Line;->copy()Lcom/google/bionics/scanner/rectifier/Line;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 137
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->c:Ljava/util/ArrayList;

    .line 138
    iget-object v0, p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/rectifier/Line;

    .line 139
    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/rectifier/Line;->copy()Lcom/google/bionics/scanner/rectifier/Line;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 141
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->d:Ljava/util/ArrayList;

    .line 142
    iget-object v0, p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/rectifier/Line;

    .line 143
    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/rectifier/Line;->copy()Lcom/google/bionics/scanner/rectifier/Line;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 145
    :cond_3
    iget v0, p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:F

    iput v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:F

    .line 146
    iget v0, p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:I

    iget v1, p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:I

    invoke-direct {p0, v0, v1}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a(II)V

    .line 148
    const/4 v0, 0x0

    :goto_4
    if-ge v0, v3, :cond_4

    .line 149
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    iget-object v2, p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/bionics/scanner/rectifier/Line;->copy()Lcom/google/bionics/scanner/rectifier/Line;

    move-result-object v2

    aput-object v2, v1, v0

    .line 150
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    iget-object v2, p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    aget v2, v2, v0

    aput v2, v1, v0

    .line 148
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 152
    :cond_4
    return-void
.end method

.method public constructor <init>([FII)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x1

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-array v0, v2, [Lcom/google/bionics/scanner/rectifier/Line;

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    .line 63
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:F

    .line 66
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    .line 117
    invoke-direct {p0, v1, v1, v1, v1}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a(IIII)V

    .line 118
    invoke-virtual {p0, p1}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->addQuadrilateral([F)V

    .line 119
    invoke-direct {p0, p2, p3}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a(II)V

    .line 120
    invoke-virtual {p0}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->reset()V

    .line 121
    return-void
.end method

.method public constructor <init>([Lcom/google/bionics/scanner/rectifier/Line;[Lcom/google/bionics/scanner/rectifier/Line;[Lcom/google/bionics/scanner/rectifier/Line;[Lcom/google/bionics/scanner/rectifier/Line;II)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-array v0, v1, [Lcom/google/bionics/scanner/rectifier/Line;

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    .line 63
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:F

    .line 66
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:Ljava/util/ArrayList;

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:Ljava/util/ArrayList;

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->c:Ljava/util/ArrayList;

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->d:Ljava/util/ArrayList;

    .line 104
    invoke-direct {p0, p5, p6}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a(II)V

    .line 105
    invoke-virtual {p0}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->reset()V

    .line 106
    return-void
.end method

.method private a(Landroid/graphics/PointF;Ljava/util/List;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/PointF;",
            "Ljava/util/List",
            "<",
            "Lcom/google/bionics/scanner/rectifier/Line;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 490
    const v2, 0x7f7fffff    # Float.MAX_VALUE

    .line 491
    const/4 v1, -0x1

    .line 492
    const/4 v0, 0x0

    move v3, v2

    move v2, v1

    move v1, v0

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 493
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/rectifier/Line;

    invoke-virtual {v0, p1}, Lcom/google/bionics/scanner/rectifier/Line;->distPoint(Landroid/graphics/PointF;)F

    move-result v0

    .line 494
    cmpg-float v4, v0, v3

    if-gez v4, :cond_0

    iget v4, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:F

    cmpg-float v4, v0, v4

    if-gez v4, :cond_0

    move v2, v1

    move v3, v0

    .line 492
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 499
    :cond_1
    return v2
.end method

.method private a(Landroid/os/Parcel;)Lcom/google/bionics/scanner/rectifier/Line;
    .locals 5

    .prologue
    .line 624
    new-instance v0, Lcom/google/bionics/scanner/rectifier/Line;

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/bionics/scanner/rectifier/Line;-><init>(FFFF)V

    return-object v0
.end method

.method private a(Lcom/google/bionics/scanner/rectifier/Line;Lcom/google/bionics/scanner/rectifier/Line;Lcom/google/bionics/scanner/rectifier/Line;Lcom/google/bionics/scanner/rectifier/Line;)Lcom/google/bionics/scanner/rectifier/Quadrilateral$VertexSet;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 514
    new-instance v1, Lcom/google/bionics/scanner/rectifier/Quadrilateral$VertexSet;

    invoke-direct {v1}, Lcom/google/bionics/scanner/rectifier/Quadrilateral$VertexSet;-><init>()V

    .line 517
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/bionics/scanner/rectifier/Quadrilateral$VertexSet;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v2

    invoke-virtual {p4, p1, v2}, Lcom/google/bionics/scanner/rectifier/Line;->intersect(Lcom/google/bionics/scanner/rectifier/Line;Landroid/graphics/PointF;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 530
    :cond_0
    :goto_0
    return-object v0

    .line 520
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/bionics/scanner/rectifier/Quadrilateral$VertexSet;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v2

    invoke-virtual {p1, p2, v2}, Lcom/google/bionics/scanner/rectifier/Line;->intersect(Lcom/google/bionics/scanner/rectifier/Line;Landroid/graphics/PointF;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 523
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/bionics/scanner/rectifier/Quadrilateral$VertexSet;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v2

    invoke-virtual {p2, p3, v2}, Lcom/google/bionics/scanner/rectifier/Line;->intersect(Lcom/google/bionics/scanner/rectifier/Line;Landroid/graphics/PointF;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 526
    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/bionics/scanner/rectifier/Quadrilateral$VertexSet;->getPoint(I)Landroid/graphics/PointF;

    move-result-object v2

    invoke-virtual {p3, p4, v2}, Lcom/google/bionics/scanner/rectifier/Line;->intersect(Lcom/google/bionics/scanner/rectifier/Line;Landroid/graphics/PointF;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 530
    goto :goto_0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 189
    iget v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:I

    iget v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:I

    if-le v0, v1, :cond_0

    iget v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:I

    .line 190
    :goto_0
    int-to-float v0, v0

    iget v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:F

    .line 191
    return-void

    .line 189
    :cond_0
    iget v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:I

    goto :goto_0
.end method

.method private a(II)V
    .locals 0

    .prologue
    .line 179
    iput p1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:I

    .line 180
    iput p2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:I

    .line 181
    invoke-direct {p0}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a()V

    .line 182
    return-void
.end method

.method private a(IIII)V
    .locals 1

    .prologue
    .line 262
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:Ljava/util/ArrayList;

    .line 263
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:Ljava/util/ArrayList;

    .line 264
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->c:Ljava/util/ArrayList;

    .line 265
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->d:Ljava/util/ArrayList;

    .line 266
    return-void
.end method

.method private a(Landroid/os/Parcel;Lcom/google/bionics/scanner/rectifier/Line;)V
    .locals 1

    .prologue
    .line 609
    iget-object v0, p2, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 610
    iget-object v0, p2, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 611
    iget-object v0, p2, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 612
    iget-object v0, p2, Lcom/google/bionics/scanner/rectifier/Line;->unitVector:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 613
    return-void
.end method

.method private b()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 281
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    aget v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/rectifier/Line;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/rectifier/Line;->copy()Lcom/google/bionics/scanner/rectifier/Line;

    move-result-object v0

    aput-object v0, v1, v3

    .line 282
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    aget v2, v2, v4

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/rectifier/Line;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/rectifier/Line;->copy()Lcom/google/bionics/scanner/rectifier/Line;

    move-result-object v0

    aput-object v0, v1, v4

    .line 283
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->c:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    aget v2, v2, v5

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/rectifier/Line;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/rectifier/Line;->copy()Lcom/google/bionics/scanner/rectifier/Line;

    move-result-object v0

    aput-object v0, v1, v5

    .line 284
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->d:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    aget v2, v2, v6

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/rectifier/Line;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/rectifier/Line;->copy()Lcom/google/bionics/scanner/rectifier/Line;

    move-result-object v0

    aput-object v0, v1, v6

    .line 285
    return-void
.end method

.method public static getNeighborLineIndexes(I)Landroid/util/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 324
    if-nez p0, :cond_0

    const/4 v0, 0x3

    .line 327
    :goto_0
    new-instance v1, Landroid/util/Pair;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1

    .line 324
    :cond_0
    add-int/lit8 v0, p0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public addQuadrilateral([F)V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v5, 0x1

    .line 243
    iget-object v6, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/bionics/scanner/rectifier/Line;

    aget v1, p1, v8

    aget v2, p1, v5

    aget v3, p1, v9

    aget v4, p1, v8

    sub-float/2addr v3, v4

    aget v4, p1, v10

    aget v7, p1, v5

    sub-float/2addr v4, v7

    invoke-direct/range {v0 .. v5}, Lcom/google/bionics/scanner/rectifier/Line;-><init>(FFFFZ)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 245
    iget-object v6, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/bionics/scanner/rectifier/Line;

    aget v1, p1, v9

    aget v2, p1, v10

    aget v3, p1, v11

    aget v4, p1, v9

    sub-float/2addr v3, v4

    const/4 v4, 0x5

    aget v4, p1, v4

    aget v7, p1, v10

    sub-float/2addr v4, v7

    invoke-direct/range {v0 .. v5}, Lcom/google/bionics/scanner/rectifier/Line;-><init>(FFFFZ)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 247
    iget-object v6, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->c:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/bionics/scanner/rectifier/Line;

    aget v1, p1, v11

    const/4 v2, 0x5

    aget v2, p1, v2

    const/4 v3, 0x6

    aget v3, p1, v3

    aget v4, p1, v11

    sub-float/2addr v3, v4

    const/4 v4, 0x7

    aget v4, p1, v4

    const/4 v7, 0x5

    aget v7, p1, v7

    sub-float/2addr v4, v7

    invoke-direct/range {v0 .. v5}, Lcom/google/bionics/scanner/rectifier/Line;-><init>(FFFFZ)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 249
    iget-object v6, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->d:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/bionics/scanner/rectifier/Line;

    const/4 v1, 0x6

    aget v1, p1, v1

    const/4 v2, 0x7

    aget v2, p1, v2

    aget v3, p1, v8

    const/4 v4, 0x6

    aget v4, p1, v4

    sub-float/2addr v3, v4

    aget v4, p1, v5

    const/4 v7, 0x7

    aget v7, p1, v7

    sub-float/2addr v4, v7

    invoke-direct/range {v0 .. v5}, Lcom/google/bionics/scanner/rectifier/Line;-><init>(FFFFZ)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 251
    return-void
.end method

.method public applyScale(F)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 351
    sget-object v0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:Lcom/google/bionics/scanner/unveil/util/Logger;

    const-string v2, "Applying scal factor %f to quad..."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {v0, v2, v3}, Lcom/google/bionics/scanner/unveil/util/Logger;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 352
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/rectifier/Line;

    .line 353
    invoke-virtual {v0, p1}, Lcom/google/bionics/scanner/rectifier/Line;->applyScale(F)V

    goto :goto_0

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/rectifier/Line;

    .line 356
    invoke-virtual {v0, p1}, Lcom/google/bionics/scanner/rectifier/Line;->applyScale(F)V

    goto :goto_1

    .line 358
    :cond_1
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/rectifier/Line;

    .line 359
    invoke-virtual {v0, p1}, Lcom/google/bionics/scanner/rectifier/Line;->applyScale(F)V

    goto :goto_2

    .line 361
    :cond_2
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/rectifier/Line;

    .line 362
    invoke-virtual {v0, p1}, Lcom/google/bionics/scanner/rectifier/Line;->applyScale(F)V

    goto :goto_3

    .line 364
    :cond_3
    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    array-length v3, v2

    move v0, v1

    :goto_4
    if-ge v0, v3, :cond_4

    aget-object v1, v2, v0

    .line 365
    invoke-virtual {v1, p1}, Lcom/google/bionics/scanner/rectifier/Line;->applyScale(F)V

    .line 364
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 367
    :cond_4
    iget v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:I

    .line 368
    iget v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:I

    .line 369
    invoke-direct {p0}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a()V

    .line 370
    return-void
.end method

.method public cornerUpdate([FI)V
    .locals 13

    .prologue
    const/4 v9, 0x0

    const/4 v5, 0x1

    .line 294
    invoke-virtual {p0}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->getVertices()[Landroid/graphics/PointF;

    move-result-object v11

    .line 295
    invoke-static {p2}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->getNeighborLineIndexes(I)Landroid/util/Pair;

    move-result-object v12

    .line 301
    iget-object v0, v12, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 302
    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    move v10, v9

    .line 305
    :goto_0
    new-instance v0, Lcom/google/bionics/scanner/rectifier/Line;

    aget v1, p1, v9

    aget v2, p1, v5

    aget-object v3, v11, v4

    iget v3, v3, Landroid/graphics/PointF;->x:F

    aget v6, p1, v9

    sub-float/2addr v3, v6

    aget-object v4, v11, v4

    iget v4, v4, Landroid/graphics/PointF;->y:F

    aget v6, p1, v5

    sub-float/2addr v4, v6

    invoke-direct/range {v0 .. v5}, Lcom/google/bionics/scanner/rectifier/Line;-><init>(FFFFZ)V

    .line 307
    new-instance v6, Lcom/google/bionics/scanner/rectifier/Line;

    aget v7, p1, v9

    aget v8, p1, v5

    aget-object v1, v11, v10

    iget v1, v1, Landroid/graphics/PointF;->x:F

    aget v2, p1, v9

    sub-float v9, v1, v2

    aget-object v1, v11, v10

    iget v1, v1, Landroid/graphics/PointF;->y:F

    aget v2, p1, v5

    sub-float v10, v1, v2

    move v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/google/bionics/scanner/rectifier/Line;-><init>(FFFFZ)V

    .line 309
    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    iget-object v1, v12, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aput-object v0, v2, v1

    .line 310
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    iget-object v0, v12, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput-object v6, v1, v0

    .line 311
    return-void

    .line 302
    :cond_0
    add-int/lit8 v0, p2, 0x1

    move v10, v0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 599
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 720
    if-ne p0, p1, :cond_1

    .line 739
    :cond_0
    :goto_0
    return v0

    .line 725
    :cond_1
    instance-of v2, p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    if-nez v2, :cond_2

    move v0, v1

    .line 726
    goto :goto_0

    .line 729
    :cond_2
    check-cast p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;

    .line 730
    iget v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:I

    iget v3, p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:I

    iget v3, p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:F

    iget v3, p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:F

    iget v3, p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    iget-object v3, p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    .line 734
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    iget-object v3, p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    .line 735
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:Ljava/util/ArrayList;

    iget-object v3, p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:Ljava/util/ArrayList;

    .line 736
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:Ljava/util/ArrayList;

    iget-object v3, p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:Ljava/util/ArrayList;

    .line 737
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->c:Ljava/util/ArrayList;

    iget-object v3, p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->c:Ljava/util/ArrayList;

    .line 738
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->d:Ljava/util/ArrayList;

    iget-object v3, p1, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->d:Ljava/util/ArrayList;

    .line 739
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public getDefaultQuadString(Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 212
    invoke-virtual {p0}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->getVertexSet()Lcom/google/bionics/scanner/rectifier/Quadrilateral$VertexSet;

    move-result-object v0

    .line 213
    if-nez v0, :cond_0

    .line 214
    const-string v0, "no quadrilateral"

    .line 221
    :goto_0
    return-object v0

    .line 216
    :cond_0
    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 217
    invoke-virtual {v0}, Lcom/google/bionics/scanner/rectifier/Quadrilateral$VertexSet;->getPoints()[Landroid/graphics/PointF;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    .line 218
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    iget v8, v5, Landroid/graphics/PointF;->x:F

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v7, v1

    const/4 v8, 0x1

    iget v5, v5, Landroid/graphics/PointF;->y:F

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 217
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 220
    :cond_1
    const/16 v0, 0x5d

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 221
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getNumCandidatesString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 200
    const-string v0, "Top[%d] Right[%d] Bottom[%d] Left[%d]"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:Ljava/util/ArrayList;

    .line 201
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->d:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 200
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRefHeight()I
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:I

    return v0
.end method

.method public getRefWidth()I
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:I

    return v0
.end method

.method public getVertexSet()Lcom/google/bionics/scanner/rectifier/Quadrilateral$VertexSet;
    .locals 5

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a(Lcom/google/bionics/scanner/rectifier/Line;Lcom/google/bionics/scanner/rectifier/Line;Lcom/google/bionics/scanner/rectifier/Line;Lcom/google/bionics/scanner/rectifier/Line;)Lcom/google/bionics/scanner/rectifier/Quadrilateral$VertexSet;

    move-result-object v0

    return-object v0
.end method

.method public getVertices()[Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 342
    invoke-virtual {p0}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->getVertexSet()Lcom/google/bionics/scanner/rectifier/Quadrilateral$VertexSet;

    move-result-object v0

    invoke-static {v0}, Lcom/google/bionics/scanner/rectifier/Quadrilateral$VertexSet;->a(Lcom/google/bionics/scanner/rectifier/Quadrilateral$VertexSet;)[Landroid/graphics/PointF;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 701
    .line 704
    iget v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 705
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:I

    add-int/2addr v0, v1

    .line 706
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 707
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 708
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 709
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([I)I

    move-result v1

    add-int/2addr v0, v1

    .line 710
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 711
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 712
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 713
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 714
    return v0
.end method

.method public native rectifyDownsampledJpeg(Ljava/lang/String;ILcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;)Lcom/google/bionics/scanner/rectifier/ImageData;
.end method

.method public native rectifyJpeg(Ljava/lang/String;Ljava/lang/String;Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;I)Lcom/google/bionics/scanner/rectifier/ImageEnhancement$Method;
.end method

.method public reset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 272
    invoke-direct {p0}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a()V

    .line 273
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    array-length v1, v1

    invoke-static {v0, v2, v1, v2}, Ljava/util/Arrays;->fill([IIII)V

    .line 274
    invoke-direct {p0}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b()V

    .line 275
    return-void
.end method

.method public selectBottomLine(Landroid/graphics/PointF;Lcom/google/bionics/scanner/rectifier/Quadrilateral$HapticFeedback;)Lcom/google/bionics/scanner/rectifier/Line;
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 435
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->c:Ljava/util/ArrayList;

    invoke-direct {p0, p1, v0}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a(Landroid/graphics/PointF;Ljava/util/List;)I

    move-result v0

    .line 436
    if-ltz v0, :cond_2

    .line 437
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    aget v1, v1, v2

    if-eq v0, v1, :cond_1

    .line 439
    if-eqz p2, :cond_0

    .line 440
    invoke-interface {p2}, Lcom/google/bionics/scanner/rectifier/Quadrilateral$HapticFeedback;->provideFeedback()V

    .line 442
    :cond_0
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    aput v0, v1, v2

    .line 443
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/rectifier/Line;

    .line 444
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/rectifier/Line;->copy()Lcom/google/bionics/scanner/rectifier/Line;

    move-result-object v0

    aput-object v0, v1, v2

    .line 451
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    aget-object v0, v0, v2

    return-object v0

    .line 448
    :cond_2
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    const/4 v1, -0x1

    aput v1, v0, v2

    .line 449
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    invoke-virtual {v0, p1}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    goto :goto_0
.end method

.method public selectLeftLine(Landroid/graphics/PointF;Lcom/google/bionics/scanner/rectifier/Quadrilateral$HapticFeedback;)Lcom/google/bionics/scanner/rectifier/Line;
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 462
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->d:Ljava/util/ArrayList;

    invoke-direct {p0, p1, v0}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a(Landroid/graphics/PointF;Ljava/util/List;)I

    move-result v0

    .line 463
    if-ltz v0, :cond_2

    .line 464
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    aget v1, v1, v2

    if-eq v0, v1, :cond_1

    .line 466
    if-eqz p2, :cond_0

    .line 467
    invoke-interface {p2}, Lcom/google/bionics/scanner/rectifier/Quadrilateral$HapticFeedback;->provideFeedback()V

    .line 469
    :cond_0
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    aput v0, v1, v2

    .line 470
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/rectifier/Line;

    .line 471
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/rectifier/Line;->copy()Lcom/google/bionics/scanner/rectifier/Line;

    move-result-object v0

    aput-object v0, v1, v2

    .line 478
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    aget-object v0, v0, v2

    return-object v0

    .line 475
    :cond_2
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    const/4 v1, -0x1

    aput v1, v0, v2

    .line 476
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    invoke-virtual {v0, p1}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    goto :goto_0
.end method

.method public selectRightLine(Landroid/graphics/PointF;Lcom/google/bionics/scanner/rectifier/Quadrilateral$HapticFeedback;)Lcom/google/bionics/scanner/rectifier/Line;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 408
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:Ljava/util/ArrayList;

    invoke-direct {p0, p1, v0}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a(Landroid/graphics/PointF;Ljava/util/List;)I

    move-result v0

    .line 409
    if-ltz v0, :cond_2

    .line 410
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    aget v1, v1, v2

    if-eq v0, v1, :cond_1

    .line 412
    if-eqz p2, :cond_0

    .line 413
    invoke-interface {p2}, Lcom/google/bionics/scanner/rectifier/Quadrilateral$HapticFeedback;->provideFeedback()V

    .line 415
    :cond_0
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    aput v0, v1, v2

    .line 416
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/rectifier/Line;

    .line 417
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/rectifier/Line;->copy()Lcom/google/bionics/scanner/rectifier/Line;

    move-result-object v0

    aput-object v0, v1, v2

    .line 424
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    aget-object v0, v0, v2

    return-object v0

    .line 421
    :cond_2
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    const/4 v1, -0x1

    aput v1, v0, v2

    .line 422
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    invoke-virtual {v0, p1}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    goto :goto_0
.end method

.method public selectTopLine(Landroid/graphics/PointF;Lcom/google/bionics/scanner/rectifier/Quadrilateral$HapticFeedback;)Lcom/google/bionics/scanner/rectifier/Line;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 381
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:Ljava/util/ArrayList;

    invoke-direct {p0, p1, v0}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a(Landroid/graphics/PointF;Ljava/util/List;)I

    move-result v0

    .line 382
    if-ltz v0, :cond_2

    .line 383
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    aget v1, v1, v2

    if-eq v0, v1, :cond_1

    .line 385
    if-eqz p2, :cond_0

    .line 386
    invoke-interface {p2}, Lcom/google/bionics/scanner/rectifier/Quadrilateral$HapticFeedback;->provideFeedback()V

    .line 388
    :cond_0
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    aput v0, v1, v2

    .line 389
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/rectifier/Line;

    .line 390
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    invoke-virtual {v0}, Lcom/google/bionics/scanner/rectifier/Line;->copy()Lcom/google/bionics/scanner/rectifier/Line;

    move-result-object v0

    aput-object v0, v1, v2

    .line 397
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    aget-object v0, v0, v2

    return-object v0

    .line 394
    :cond_2
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    const/4 v1, -0x1

    aput v1, v0, v2

    .line 395
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/google/bionics/scanner/rectifier/Line;->inlier:Landroid/graphics/PointF;

    invoke-virtual {v0, p1}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    goto :goto_0
.end method

.method public setLineDistThresholdPercentage(F)V
    .locals 0

    .prologue
    .line 232
    iput p1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:F

    .line 233
    invoke-direct {p0}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a()V

    .line 234
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 629
    iget v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 630
    iget v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 631
    iget v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 632
    iget v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 633
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/rectifier/Line;

    .line 634
    invoke-direct {p0, p1, v0}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a(Landroid/os/Parcel;Lcom/google/bionics/scanner/rectifier/Line;)V

    goto :goto_0

    .line 636
    :cond_0
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/rectifier/Line;

    .line 637
    invoke-direct {p0, p1, v0}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a(Landroid/os/Parcel;Lcom/google/bionics/scanner/rectifier/Line;)V

    goto :goto_1

    .line 639
    :cond_1
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/rectifier/Line;

    .line 640
    invoke-direct {p0, p1, v0}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a(Landroid/os/Parcel;Lcom/google/bionics/scanner/rectifier/Line;)V

    goto :goto_2

    .line 642
    :cond_2
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/bionics/scanner/rectifier/Line;

    .line 643
    invoke-direct {p0, p1, v0}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a(Landroid/os/Parcel;Lcom/google/bionics/scanner/rectifier/Line;)V

    goto :goto_3

    .line 646
    :cond_3
    const/4 v0, 0x0

    :goto_4
    const/4 v1, 0x4

    if-ge v0, v1, :cond_4

    .line 647
    iget-object v1, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[Lcom/google/bionics/scanner/rectifier/Line;

    aget-object v1, v1, v0

    invoke-direct {p0, p1, v1}, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a(Landroid/os/Parcel;Lcom/google/bionics/scanner/rectifier/Line;)V

    .line 646
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 649
    :cond_4
    iget-object v0, p0, Lcom/google/bionics/scanner/rectifier/Quadrilateral;->a:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 650
    return-void
.end method
