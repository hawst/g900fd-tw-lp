.class public Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;
.super Ljava/lang/Object;
.source "RealCamera.java"

# interfaces
.implements Lcom/google/bionics/scanner/unveil/camera/proxies/CameraProxy;


# instance fields
.field protected a:Landroid/hardware/Camera;

.field private a:Ljava/lang/reflect/Method;

.field private b:Ljava/lang/reflect/Method;


# direct methods
.method protected constructor <init>(Landroid/hardware/Camera;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->a:Ljava/lang/reflect/Method;

    .line 30
    iput-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->b:Ljava/lang/reflect/Method;

    .line 58
    iput-object p1, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->a:Landroid/hardware/Camera;

    .line 59
    return-void
.end method

.method public static open(Landroid/os/Handler;Ljava/util/Map;Landroid/content/res/Resources;)Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/res/Resources;",
            ")",
            "Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;"
        }
    .end annotation

    .prologue
    .line 40
    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;

    move-result-object v0

    .line 42
    if-nez v0, :cond_0

    .line 43
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_1

    .line 46
    const/4 v0, 0x0

    invoke-static {v0}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v0

    .line 53
    :cond_0
    new-instance v1, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;

    invoke-direct {v1, v0}, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;-><init>(Landroid/hardware/Camera;)V

    move-object v0, v1

    :goto_0
    return-object v0

    .line 48
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addCallbackBuffer([B)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 139
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->a:Ljava/lang/reflect/Method;

    if-nez v1, :cond_0

    .line 140
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->a:Landroid/hardware/Camera;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v1

    .line 141
    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 142
    invoke-virtual {v3}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "addCallbackBuffer"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 143
    iput-object v3, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->a:Ljava/lang/reflect/Method;

    .line 150
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->a:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->a:Landroid/hardware/Camera;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    .line 158
    :goto_1
    return-void

    .line 141
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 151
    :catch_0
    move-exception v0

    .line 152
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 153
    :catch_1
    move-exception v0

    .line 154
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    .line 155
    :catch_2
    move-exception v0

    .line 156
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_1
.end method

.method public autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->a:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    .line 89
    return-void
.end method

.method public cancelAutoFocus()V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->a:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->cancelAutoFocus()V

    .line 163
    return-void
.end method

.method public getParameters()Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;
    .locals 2

    .prologue
    .line 78
    new-instance v0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;

    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->a:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/bionics/scanner/unveil/camera/proxies/RealParameters;-><init>(Landroid/hardware/Camera$Parameters;)V

    return-object v0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->a:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 64
    return-void
.end method

.method public setDisplayOrientation(I)V
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->a:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    .line 168
    return-void
.end method

.method public setOneShotPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->a:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setOneShotPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 105
    return-void
.end method

.method public setParameters(Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;)V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->a:Landroid/hardware/Camera;

    invoke-interface {p1}, Lcom/google/bionics/scanner/unveil/camera/proxies/ParametersProxy;->getActualParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 84
    return-void
.end method

.method public setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->a:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 110
    return-void
.end method

.method public setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 115
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->b:Ljava/lang/reflect/Method;

    if-nez v1, :cond_0

    .line 116
    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->a:Landroid/hardware/Camera;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v1

    .line 117
    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 118
    invoke-virtual {v3}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "setPreviewCallbackWithBuffer"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 119
    iput-object v3, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->b:Ljava/lang/reflect/Method;

    .line 126
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->b:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->a:Landroid/hardware/Camera;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    .line 134
    :goto_1
    return-void

    .line 117
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 127
    :catch_0
    move-exception v0

    .line 128
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 129
    :catch_1
    move-exception v0

    .line 130
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    .line 131
    :catch_2
    move-exception v0

    .line 132
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_1
.end method

.method public setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->a:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    .line 94
    return-void
.end method

.method public setZoomChangeListener(Landroid/hardware/Camera$OnZoomChangeListener;)V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->a:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->setZoomChangeListener(Landroid/hardware/Camera$OnZoomChangeListener;)V

    .line 178
    return-void
.end method

.method public startPreview()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->a:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->startPreview()V

    .line 69
    return-void
.end method

.method public startSmoothZoom(I)V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->a:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->startSmoothZoom(I)V

    .line 173
    return-void
.end method

.method public stopPreview()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->a:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 74
    return-void
.end method

.method public takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/bionics/scanner/unveil/camera/proxies/RealCamera;->a:Landroid/hardware/Camera;

    invoke-virtual {v0, p1, p2, p3}, Landroid/hardware/Camera;->takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V

    .line 100
    return-void
.end method
