.class public final Lcom/google/api/services/drive/model/About$AdditionalRoleInfo;
.super LbeN;
.source "About.java"


# instance fields
.field private roleSets:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/drive/model/About$AdditionalRoleInfo$RoleSets;",
            ">;"
        }
    .end annotation
.end field

.field private type:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 675
    const-class v0, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo$RoleSets;

    invoke-static {v0}, Lbfs;->a(Ljava/lang/Class;)Ljava/lang/Object;

    .line 676
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 663
    invoke-direct {p0}, LbeN;-><init>()V

    .line 732
    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/drive/model/About$AdditionalRoleInfo;
    .locals 1

    .prologue
    .line 726
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/About$AdditionalRoleInfo;
    .locals 1

    .prologue
    .line 721
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 707
    iget-object v0, p0, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo;->type:Ljava/lang/String;

    return-object v0
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/drive/model/About$AdditionalRoleInfo$RoleSets;",
            ">;"
        }
    .end annotation

    .prologue
    .line 690
    iget-object v0, p0, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo;->roleSets:Ljava/util/List;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 663
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo;->a()Lcom/google/api/services/drive/model/About$AdditionalRoleInfo;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 663
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo;->a()Lcom/google/api/services/drive/model/About$AdditionalRoleInfo;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 663
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo;->a()Lcom/google/api/services/drive/model/About$AdditionalRoleInfo;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 663
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/About$AdditionalRoleInfo;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 663
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/About$AdditionalRoleInfo;

    move-result-object v0

    return-object v0
.end method
