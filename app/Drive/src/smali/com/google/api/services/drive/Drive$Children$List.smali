.class public Lcom/google/api/services/drive/Drive$Children$List;
.super Lcom/google/api/services/drive/DriveRequest;
.source "Drive.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/api/services/drive/DriveRequest",
        "<",
        "Lcom/google/api/services/drive/model/ChildList;",
        ">;"
    }
.end annotation


# static fields
.field private static final REST_PATH:Ljava/lang/String; = "files/{folderId}/children"


# instance fields
.field private final SECONDARY_SORT_BY_PATTERN:Ljava/util/regex/Pattern;

.field private folderId:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private maxResults:Ljava/lang/Integer;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private pageToken:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private q:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private reverseSort:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private secondarySortBy:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private sortBy:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/api/services/drive/Drive$Children;


# virtual methods
.method public a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Children$List;
    .locals 1

    .prologue
    .line 2492
    invoke-super {p0, p1}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Children$List;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Children$List;
    .locals 1

    .prologue
    .line 2656
    invoke-super {p0, p1, p2}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Children$List;

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 2440
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Children$List;->a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Children$List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 2440
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Children$List;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Children$List;

    move-result-object v0

    return-object v0
.end method

.method public buildHttpRequestUsingHead()Lbei;
    .locals 1

    .prologue
    .line 2472
    invoke-super {p0}, Lcom/google/api/services/drive/DriveRequest;->buildHttpRequestUsingHead()Lbei;

    move-result-object v0

    return-object v0
.end method

.method public executeUsingHead()Lbel;
    .locals 1

    .prologue
    .line 2467
    invoke-super {p0}, Lcom/google/api/services/drive/DriveRequest;->executeUsingHead()Lbel;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdJ;
    .locals 1

    .prologue
    .line 2440
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Children$List;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Children$List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdP;
    .locals 1

    .prologue
    .line 2440
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Children$List;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Children$List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 2440
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Children$List;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Children$List;

    move-result-object v0

    return-object v0
.end method
