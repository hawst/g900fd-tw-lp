.class public final Lcom/google/api/services/drive/model/File$ImageMediaMetadata;
.super LbeN;
.source "File.java"


# instance fields
.field private aperture:Ljava/lang/Float;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private cameraMake:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private cameraModel:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private colorSpace:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private date:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private exposureBias:Ljava/lang/Float;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private exposureMode:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private exposureTime:Ljava/lang/Float;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private flashUsed:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private focalLength:Ljava/lang/Float;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private height:Ljava/lang/Integer;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private isoSpeed:Ljava/lang/Integer;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private lens:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private location:Lcom/google/api/services/drive/model/File$ImageMediaMetadata$Location;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private maxApertureValue:Ljava/lang/Float;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private meteringMode:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private rotation:Ljava/lang/Integer;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private sensor:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private subjectDistance:Ljava/lang/Integer;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private whiteBalance:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private width:Ljava/lang/Integer;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1752
    invoke-direct {p0}, LbeN;-><init>()V

    .line 2271
    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/drive/model/File$ImageMediaMetadata;
    .locals 1

    .prologue
    .line 2265
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/File$ImageMediaMetadata;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/File$ImageMediaMetadata;
    .locals 1

    .prologue
    .line 2260
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/File$ImageMediaMetadata;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 1752
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/File$ImageMediaMetadata;->a()Lcom/google/api/services/drive/model/File$ImageMediaMetadata;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 1752
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/File$ImageMediaMetadata;->a()Lcom/google/api/services/drive/model/File$ImageMediaMetadata;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1752
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/File$ImageMediaMetadata;->a()Lcom/google/api/services/drive/model/File$ImageMediaMetadata;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 1752
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/File$ImageMediaMetadata;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/File$ImageMediaMetadata;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 1752
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/File$ImageMediaMetadata;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/File$ImageMediaMetadata;

    move-result-object v0

    return-object v0
.end method
