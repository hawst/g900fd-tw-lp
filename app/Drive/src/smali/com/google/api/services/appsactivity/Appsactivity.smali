.class public Lcom/google/api/services/appsactivity/Appsactivity;
.super LbdN;
.source "Appsactivity.java"


# static fields
.field public static final DEFAULT_BASE_URL:Ljava/lang/String; = "https://www.googleapis.com/appsactivity/v1beta1/"

.field public static final DEFAULT_ROOT_URL:Ljava/lang/String; = "https://www.googleapis.com/"

.field public static final DEFAULT_SERVICE_PATH:Ljava/lang/String; = "appsactivity/v1beta1/"


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 44
    sget-object v0, Lbdj;->a:Ljava/lang/Integer;

    .line 45
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v1, :cond_0

    sget-object v0, Lbdj;->b:Ljava/lang/Integer;

    .line 46
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v3, 0xf

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "You are currently running with version %s of google-api-client. You need at least version 1.15 of google-api-client to run version 1.20.0-SNAPSHOT of the Google Apps Activity API library."

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v4, Lbdj;->a:Ljava/lang/String;

    aput-object v4, v1, v2

    .line 44
    invoke-static {v0, v3, v1}, LbfN;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 50
    return-void

    :cond_0
    move v0, v2

    .line 46
    goto :goto_0
.end method

.method public constructor <init>(Lbeq;LbeO;Lbek;)V
    .locals 1

    .prologue
    .line 102
    new-instance v0, Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/api/services/appsactivity/Appsactivity$Builder;-><init>(Lbeq;LbeO;Lbek;)V

    invoke-direct {p0, v0}, Lcom/google/api/services/appsactivity/Appsactivity;-><init>(Lcom/google/api/services/appsactivity/Appsactivity$Builder;)V

    .line 103
    return-void
.end method

.method constructor <init>(Lcom/google/api/services/appsactivity/Appsactivity$Builder;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0, p1}, LbdN;-><init>(LbdO;)V

    .line 110
    return-void
.end method


# virtual methods
.method public activities()Lcom/google/api/services/appsactivity/Appsactivity$Activities;
    .locals 1

    .prologue
    .line 129
    new-instance v0, Lcom/google/api/services/appsactivity/Appsactivity$Activities;

    invoke-direct {v0, p0}, Lcom/google/api/services/appsactivity/Appsactivity$Activities;-><init>(Lcom/google/api/services/appsactivity/Appsactivity;)V

    return-object v0
.end method

.method protected initialize(LbdJ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbdJ",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 114
    invoke-super {p0, p1}, LbdN;->initialize(LbdJ;)V

    .line 115
    return-void
.end method
