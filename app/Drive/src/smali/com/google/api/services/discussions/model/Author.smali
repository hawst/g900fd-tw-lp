.class public final Lcom/google/api/services/discussions/model/Author;
.super LbeN;
.source "Author.java"


# instance fields
.field private displayName:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private id:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private image:Lcom/google/api/services/discussions/model/Author$Image;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private isAuthenticatedUser:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private kind:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private url:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, LbeN;-><init>()V

    .line 189
    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/discussions/model/Author;
    .locals 1

    .prologue
    .line 183
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/model/Author;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/model/Author;
    .locals 1

    .prologue
    .line 178
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/model/Author;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/discussions/model/Author;->a()Lcom/google/api/services/discussions/model/Author;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/discussions/model/Author;->a()Lcom/google/api/services/discussions/model/Author;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/discussions/model/Author;->a()Lcom/google/api/services/discussions/model/Author;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/discussions/model/Author;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/model/Author;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/discussions/model/Author;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/model/Author;

    move-result-object v0

    return-object v0
.end method
