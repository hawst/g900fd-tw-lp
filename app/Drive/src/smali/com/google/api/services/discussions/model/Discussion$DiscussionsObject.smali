.class public final Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;
.super LbeN;
.source "Discussion.java"


# instance fields
.field private anchorId:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private clientId:Ljava/lang/String;
    .annotation runtime LbfE;
        a = "client_id"
    .end annotation
.end field

.field private content:Lcom/google/api/services/discussions/model/MimedcontentJson;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private context:Lcom/google/api/services/discussions/model/MimedquoteJson;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private deleted:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private dirty:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private id:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private objectType:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private originalContent:Lcom/google/api/services/discussions/model/MimedcontentJson;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private replies:Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject$Replies;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 282
    invoke-direct {p0}, LbeN;-><init>()V

    .line 558
    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;
    .locals 1

    .prologue
    .line 552
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;
    .locals 1

    .prologue
    .line 547
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 282
    invoke-virtual {p0}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->a()Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 282
    invoke-virtual {p0}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->a()Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 282
    invoke-virtual {p0}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->a()Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 282
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 282
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    move-result-object v0

    return-object v0
.end method
