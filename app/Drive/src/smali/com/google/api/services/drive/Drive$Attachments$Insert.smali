.class public Lcom/google/api/services/drive/Drive$Attachments$Insert;
.super Lcom/google/api/services/drive/DriveRequest;
.source "Drive.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/api/services/drive/DriveRequest",
        "<",
        "Lcom/google/api/services/drive/model/Attachment;",
        ">;"
    }
.end annotation


# static fields
.field private static final REST_PATH:Ljava/lang/String; = "attachments"


# instance fields
.field final synthetic this$1:Lcom/google/api/services/drive/Drive$Attachments;


# virtual methods
.method public a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Attachments$Insert;
    .locals 1

    .prologue
    .line 1096
    invoke-super {p0, p1}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Attachments$Insert;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Attachments$Insert;
    .locals 1

    .prologue
    .line 1116
    invoke-super {p0, p1, p2}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Attachments$Insert;

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 1058
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Attachments$Insert;->a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Attachments$Insert;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 1058
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Attachments$Insert;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Attachments$Insert;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdJ;
    .locals 1

    .prologue
    .line 1058
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Attachments$Insert;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Attachments$Insert;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdP;
    .locals 1

    .prologue
    .line 1058
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Attachments$Insert;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Attachments$Insert;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 1058
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Attachments$Insert;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Attachments$Insert;

    move-result-object v0

    return-object v0
.end method
