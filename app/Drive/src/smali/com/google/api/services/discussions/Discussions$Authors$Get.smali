.class public Lcom/google/api/services/discussions/Discussions$Authors$Get;
.super Lcom/google/api/services/discussions/DiscussionsRequest;
.source "Discussions.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/api/services/discussions/DiscussionsRequest",
        "<",
        "Lcom/google/api/services/discussions/model/Author;",
        ">;"
    }
.end annotation


# static fields
.field private static final REST_PATH:Ljava/lang/String; = "authors/me"


# instance fields
.field final synthetic this$1:Lcom/google/api/services/discussions/Discussions$Authors;


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/Discussions$Authors$Get;
    .locals 1

    .prologue
    .line 220
    invoke-super {p0, p1, p2}, Lcom/google/api/services/discussions/DiscussionsRequest;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/DiscussionsRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/Discussions$Authors$Get;

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/DiscussionsRequest;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/discussions/Discussions$Authors$Get;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/Discussions$Authors$Get;

    move-result-object v0

    return-object v0
.end method

.method public buildHttpRequestUsingHead()Lbei;
    .locals 1

    .prologue
    .line 180
    invoke-super {p0}, Lcom/google/api/services/discussions/DiscussionsRequest;->buildHttpRequestUsingHead()Lbei;

    move-result-object v0

    return-object v0
.end method

.method public executeUsingHead()Lbel;
    .locals 1

    .prologue
    .line 175
    invoke-super {p0}, Lcom/google/api/services/discussions/DiscussionsRequest;->executeUsingHead()Lbel;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdJ;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/discussions/Discussions$Authors$Get;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/Discussions$Authors$Get;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdP;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/discussions/Discussions$Authors$Get;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/Discussions$Authors$Get;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/discussions/Discussions$Authors$Get;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/Discussions$Authors$Get;

    move-result-object v0

    return-object v0
.end method
