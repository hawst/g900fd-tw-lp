.class public final Lcom/google/api/services/discussions/model/SyncRequest;
.super LbeN;
.source "SyncRequest.java"


# instance fields
.field private items:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/discussions/model/Discussion;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/google/api/services/discussions/model/Discussion;

    invoke-static {v0}, Lbfs;->a(Ljava/lang/Class;)Ljava/lang/Object;

    .line 43
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, LbeN;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/discussions/model/SyncRequest;
    .locals 1

    .prologue
    .line 69
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/model/SyncRequest;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/model/SyncRequest;
    .locals 1

    .prologue
    .line 64
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/model/SyncRequest;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/discussions/model/SyncRequest;->a()Lcom/google/api/services/discussions/model/SyncRequest;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/discussions/model/SyncRequest;->a()Lcom/google/api/services/discussions/model/SyncRequest;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/discussions/model/SyncRequest;->a()Lcom/google/api/services/discussions/model/SyncRequest;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/discussions/model/SyncRequest;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/model/SyncRequest;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/discussions/model/SyncRequest;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/model/SyncRequest;

    move-result-object v0

    return-object v0
.end method
