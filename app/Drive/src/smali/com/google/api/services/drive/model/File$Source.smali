.class public final Lcom/google/api/services/drive/model/File$Source;
.super LbeN;
.source "File.java"


# instance fields
.field private clientServiceId:Ljava/lang/String;
    .annotation runtime LbfE;
        a = "client_service_id"
    .end annotation
.end field

.field private value:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2539
    invoke-direct {p0}, LbeN;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/drive/model/File$Source;
    .locals 1

    .prologue
    .line 2596
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/File$Source;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/File$Source;
    .locals 1

    .prologue
    .line 2591
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/File$Source;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 2539
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/File$Source;->a()Lcom/google/api/services/drive/model/File$Source;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 2539
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/File$Source;->a()Lcom/google/api/services/drive/model/File$Source;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2539
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/File$Source;->a()Lcom/google/api/services/drive/model/File$Source;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 2539
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/File$Source;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/File$Source;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 2539
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/File$Source;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/File$Source;

    move-result-object v0

    return-object v0
.end method
