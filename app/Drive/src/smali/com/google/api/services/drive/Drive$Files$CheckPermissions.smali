.class public Lcom/google/api/services/drive/Drive$Files$CheckPermissions;
.super Lcom/google/api/services/drive/DriveRequest;
.source "Drive.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/api/services/drive/DriveRequest",
        "<",
        "Lcom/google/api/services/drive/model/CheckPermissionsResponse;",
        ">;"
    }
.end annotation


# static fields
.field private static final REST_PATH:Ljava/lang/String; = "files/checkPermissions"


# instance fields
.field final synthetic this$1:Lcom/google/api/services/drive/Drive$Files;


# virtual methods
.method public a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$CheckPermissions;
    .locals 1

    .prologue
    .line 3716
    invoke-super {p0, p1}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Files$CheckPermissions;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Files$CheckPermissions;
    .locals 1

    .prologue
    .line 3736
    invoke-super {p0, p1, p2}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Files$CheckPermissions;

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 3677
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Files$CheckPermissions;->a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$CheckPermissions;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 3677
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Files$CheckPermissions;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Files$CheckPermissions;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdJ;
    .locals 1

    .prologue
    .line 3677
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Files$CheckPermissions;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Files$CheckPermissions;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdP;
    .locals 1

    .prologue
    .line 3677
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Files$CheckPermissions;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Files$CheckPermissions;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 3677
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Files$CheckPermissions;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Files$CheckPermissions;

    move-result-object v0

    return-object v0
.end method
