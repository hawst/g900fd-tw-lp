.class public final Lcom/google/api/services/appsactivity/model/Target;
.super LbeN;
.source "Target.java"


# instance fields
.field private id:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private mimeType:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private name:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, LbeN;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/Target;->clone()Lcom/google/api/services/appsactivity/model/Target;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/Target;->clone()Lcom/google/api/services/appsactivity/model/Target;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/api/services/appsactivity/model/Target;
    .locals 1

    .prologue
    .line 112
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/Target;

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/Target;->clone()Lcom/google/api/services/appsactivity/model/Target;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Target;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Target;->mimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Target;->name:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/model/Target;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/Target;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/model/Target;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/Target;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/Target;
    .locals 1

    .prologue
    .line 107
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/Target;

    return-object v0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/api/services/appsactivity/model/Target;
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Target;->id:Ljava/lang/String;

    .line 68
    return-object p0
.end method

.method public setMimeType(Ljava/lang/String;)Lcom/google/api/services/appsactivity/model/Target;
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Target;->mimeType:Ljava/lang/String;

    .line 85
    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/api/services/appsactivity/model/Target;
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Target;->name:Ljava/lang/String;

    .line 102
    return-object p0
.end method
