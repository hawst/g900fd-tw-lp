.class public final Lcom/google/api/services/drive/model/File$Labels;
.super LbeN;
.source "File.java"


# instance fields
.field private hidden:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private restricted:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private starred:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private trashed:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private viewed:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2402
    invoke-direct {p0}, LbeN;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/drive/model/File$Labels;
    .locals 1

    .prologue
    .line 2531
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/File$Labels;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/File$Labels;
    .locals 1

    .prologue
    .line 2526
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/File$Labels;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 2402
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/File$Labels;->a()Lcom/google/api/services/drive/model/File$Labels;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 2402
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/File$Labels;->a()Lcom/google/api/services/drive/model/File$Labels;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2402
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/File$Labels;->a()Lcom/google/api/services/drive/model/File$Labels;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 2402
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/File$Labels;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/File$Labels;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 2402
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/File$Labels;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/File$Labels;

    move-result-object v0

    return-object v0
.end method
