.class public final Lcom/google/api/services/drive/model/Channel;
.super LbeN;
.source "Channel.java"


# instance fields
.field private address:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private expiration:Ljava/lang/Long;
    .annotation runtime LbeW;
    .end annotation

    .annotation runtime LbfE;
    .end annotation
.end field

.field private id:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private kind:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private params:Ljava/util/Map;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private payload:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private resourceId:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private resourceUri:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private token:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private type:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, LbeN;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/drive/model/Channel;
    .locals 1

    .prologue
    .line 291
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/Channel;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/Channel;
    .locals 1

    .prologue
    .line 286
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/Channel;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/Channel;->a()Lcom/google/api/services/drive/model/Channel;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/Channel;->a()Lcom/google/api/services/drive/model/Channel;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/Channel;->a()Lcom/google/api/services/drive/model/Channel;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/Channel;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/Channel;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/Channel;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/Channel;

    move-result-object v0

    return-object v0
.end method
