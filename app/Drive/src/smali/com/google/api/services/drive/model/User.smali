.class public final Lcom/google/api/services/drive/model/User;
.super LbeN;
.source "User.java"


# instance fields
.field private displayName:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private domain:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private domainSharingSettings:Lcom/google/api/services/drive/model/User$DomainSharingSettings;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private emailAddress:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private isAuthenticatedUser:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private kind:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private permissionId:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private picture:Lcom/google/api/services/drive/model/User$Picture;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, LbeN;-><init>()V

    .line 350
    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/drive/model/User;
    .locals 1

    .prologue
    .line 231
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/User;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/User;
    .locals 1

    .prologue
    .line 226
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/User;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/User;->a()Lcom/google/api/services/drive/model/User;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/User;->a()Lcom/google/api/services/drive/model/User;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/User;->a()Lcom/google/api/services/drive/model/User;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/User;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/User;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/User;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/User;

    move-result-object v0

    return-object v0
.end method
