.class public final Lcom/google/api/services/drive/model/App;
.super LbeN;
.source "App.java"


# instance fields
.field private appDataQuotaBytesUsed:Ljava/lang/Long;
    .annotation runtime LbeW;
    .end annotation

    .annotation runtime LbfE;
    .end annotation
.end field

.field private authorized:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private chromeExtensionIds:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private createInFolderTemplate:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private createUrl:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private hasAppData:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private hasDriveWideScope:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private icons:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/drive/model/App$Icons;",
            ">;"
        }
    .end annotation
.end field

.field private id:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private installed:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private kind:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private longDescription:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private name:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private objectType:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private openUrlTemplate:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private origins:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private primaryFileExtensions:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private primaryMimeTypes:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private productId:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private productUrl:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private rankingInfo:Lcom/google/api/services/drive/model/App$RankingInfo;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private removable:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private requiresAuthorizationBeforeOpenWith:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private secondaryFileExtensions:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private secondaryMimeTypes:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private shortDescription:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private supportsCreate:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private supportsImport:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private supportsMultiOpen:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private supportsOfflineCreate:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private type:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private useByDefault:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    const-class v0, Lcom/google/api/services/drive/model/App$Icons;

    invoke-static {v0}, Lbfs;->a(Ljava/lang/Class;)Ljava/lang/Object;

    .line 96
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, LbeN;-><init>()V

    .line 933
    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/drive/model/App;
    .locals 1

    .prologue
    .line 832
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/App;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/App;
    .locals 1

    .prologue
    .line 827
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/App;

    return-object v0
.end method

.method public a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 728
    iget-object v0, p0, Lcom/google/api/services/drive/model/App;->supportsCreate:Ljava/lang/Boolean;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lcom/google/api/services/drive/model/App;->id:Ljava/lang/String;

    return-object v0
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 556
    iget-object v0, p0, Lcom/google/api/services/drive/model/App;->primaryFileExtensions:Ljava/util/List;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 484
    iget-object v0, p0, Lcom/google/api/services/drive/model/App;->name:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 573
    iget-object v0, p0, Lcom/google/api/services/drive/model/App;->primaryMimeTypes:Ljava/util/List;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 502
    iget-object v0, p0, Lcom/google/api/services/drive/model/App;->objectType:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 677
    iget-object v0, p0, Lcom/google/api/services/drive/model/App;->secondaryFileExtensions:Ljava/util/List;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/App;->a()Lcom/google/api/services/drive/model/App;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/App;->a()Lcom/google/api/services/drive/model/App;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/App;->a()Lcom/google/api/services/drive/model/App;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 607
    iget-object v0, p0, Lcom/google/api/services/drive/model/App;->productUrl:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 694
    iget-object v0, p0, Lcom/google/api/services/drive/model/App;->secondaryMimeTypes:Ljava/util/List;

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/App;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/App;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/App;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/App;

    move-result-object v0

    return-object v0
.end method
