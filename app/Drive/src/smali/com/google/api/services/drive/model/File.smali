.class public final Lcom/google/api/services/drive/model/File;
.super LbeN;
.source "File.java"


# instance fields
.field private alternateLink:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private appDataContents:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private authorizedAppIds:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private copyable:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private createdDate:Lbfx;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private creatorAppId:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private defaultOpenWithLink:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private descendantOfRoot:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private description:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private downloadUrl:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private driveSource:Lcom/google/api/services/drive/model/File$DriveSource;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private editable:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private embedLink:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private etag:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private explicitlyTrashed:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private exportLinks:Ljava/util/Map;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private fileExtension:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private fileSize:Ljava/lang/Long;
    .annotation runtime LbeW;
    .end annotation

    .annotation runtime LbfE;
    .end annotation
.end field

.field private flaggedForAbuse:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private folderColor:Ljava/lang/Long;
    .annotation runtime LbeW;
    .end annotation

    .annotation runtime LbfE;
    .end annotation
.end field

.field private folderFeatures:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private gplusMedia:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private hasChildFolders:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private hasThumbnail:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private headRevisionId:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private iconLink:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private id:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private imageMediaMetadata:Lcom/google/api/services/drive/model/File$ImageMediaMetadata;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private indexableText:Lcom/google/api/services/drive/model/File$IndexableText;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private kind:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private labels:Lcom/google/api/services/drive/model/File$Labels;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private lastModifyingUser:Lcom/google/api/services/drive/model/User;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private lastModifyingUserName:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private lastViewedByMeDate:Lbfx;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private localId:Lcom/google/api/services/drive/model/FileLocalId;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private markedViewedByMeDate:Lbfx;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private md5Checksum:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private mimeType:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private modifiedByMeDate:Lbfx;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private modifiedDate:Lbfx;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private openWithLinks:Ljava/util/Map;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private originalFilename:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private ownerNames:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private owners:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/drive/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private parents:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/drive/model/ParentReference;",
            ">;"
        }
    .end annotation
.end field

.field private permissions:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/drive/model/Permission;",
            ">;"
        }
    .end annotation
.end field

.field private primarySyncParentId:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private properties:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/drive/model/Property;",
            ">;"
        }
    .end annotation
.end field

.field private quotaBytesUsed:Ljava/lang/Long;
    .annotation runtime LbeW;
    .end annotation

    .annotation runtime LbfE;
    .end annotation
.end field

.field private selfLink:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private shared:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private sharedWithMeDate:Lbfx;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private sharingUser:Lcom/google/api/services/drive/model/User;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private source:Lcom/google/api/services/drive/model/File$Source;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private sources:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private subscribed:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private thumbnail:Lcom/google/api/services/drive/model/File$Thumbnail;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private thumbnailLink:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private title:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private userPermission:Lcom/google/api/services/drive/model/Permission;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private version:Ljava/lang/Long;
    .annotation runtime LbeW;
    .end annotation

    .annotation runtime LbfE;
    .end annotation
.end field

.field private videoMediaMetadata:Lcom/google/api/services/drive/model/File$VideoMediaMetadata;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private webContentLink:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private webViewLink:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private writersCanShare:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, LbeN;-><init>()V

    .line 2698
    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/drive/model/File;
    .locals 1

    .prologue
    .line 1680
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/File;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/File;
    .locals 1

    .prologue
    .line 1675
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/File;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/File;->a()Lcom/google/api/services/drive/model/File;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/File;->a()Lcom/google/api/services/drive/model/File;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/File;->a()Lcom/google/api/services/drive/model/File;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/File;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/File;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/File;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/File;

    move-result-object v0

    return-object v0
.end method
