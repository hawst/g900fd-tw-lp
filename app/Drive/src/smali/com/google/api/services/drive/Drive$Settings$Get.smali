.class public Lcom/google/api/services/drive/Drive$Settings$Get;
.super Lcom/google/api/services/drive/DriveRequest;
.source "Drive.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/api/services/drive/DriveRequest",
        "<",
        "Lcom/google/api/services/drive/model/Setting;",
        ">;"
    }
.end annotation


# static fields
.field private static final REST_PATH:Ljava/lang/String; = "settings/{keyname}"


# instance fields
.field private keyname:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private namespace:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/api/services/drive/Drive$Settings;


# direct methods
.method protected constructor <init>(Lcom/google/api/services/drive/Drive$Settings;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 12527
    iput-object p1, p0, Lcom/google/api/services/drive/Drive$Settings$Get;->this$1:Lcom/google/api/services/drive/Drive$Settings;

    .line 12528
    iget-object v1, p1, Lcom/google/api/services/drive/Drive$Settings;->this$0:Lcom/google/api/services/drive/Drive;

    const-string v2, "GET"

    const-string v3, "settings/{keyname}"

    const/4 v4, 0x0

    const-class v5, Lcom/google/api/services/drive/model/Setting;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/api/services/drive/DriveRequest;-><init>(Lcom/google/api/services/drive/Drive;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)V

    .line 12529
    const-string v0, "Required parameter keyname must be specified."

    invoke-static {p2, v0}, LbfN;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/services/drive/Drive$Settings$Get;->keyname:Ljava/lang/String;

    .line 12530
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Settings$Get;
    .locals 1

    .prologue
    .line 12559
    invoke-super {p0, p1}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Settings$Get;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Settings$Get;
    .locals 1

    .prologue
    .line 12611
    invoke-super {p0, p1, p2}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Settings$Get;

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 12510
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Settings$Get;->a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Settings$Get;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 12510
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Settings$Get;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Settings$Get;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Settings$Get;
    .locals 0

    .prologue
    .line 12605
    iput-object p1, p0, Lcom/google/api/services/drive/Drive$Settings$Get;->namespace:Ljava/lang/String;

    .line 12606
    return-object p0
.end method

.method public buildHttpRequestUsingHead()Lbei;
    .locals 1

    .prologue
    .line 12539
    invoke-super {p0}, Lcom/google/api/services/drive/DriveRequest;->buildHttpRequestUsingHead()Lbei;

    move-result-object v0

    return-object v0
.end method

.method public executeUsingHead()Lbel;
    .locals 1

    .prologue
    .line 12534
    invoke-super {p0}, Lcom/google/api/services/drive/DriveRequest;->executeUsingHead()Lbel;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdJ;
    .locals 1

    .prologue
    .line 12510
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Settings$Get;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Settings$Get;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdP;
    .locals 1

    .prologue
    .line 12510
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Settings$Get;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Settings$Get;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 12510
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Settings$Get;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Settings$Get;

    move-result-object v0

    return-object v0
.end method
