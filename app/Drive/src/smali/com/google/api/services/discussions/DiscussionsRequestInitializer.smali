.class public Lcom/google/api/services/discussions/DiscussionsRequestInitializer;
.super LbdQ;
.source "DiscussionsRequestInitializer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, LbdQ;-><init>()V

    .line 84
    return-void
.end method


# virtual methods
.method protected a(Lcom/google/api/services/discussions/DiscussionsRequest;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/services/discussions/DiscussionsRequest",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 118
    return-void
.end method

.method public final initializeJsonRequest(LbdP;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbdP",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 103
    invoke-super {p0, p1}, LbdQ;->initializeJsonRequest(LbdP;)V

    .line 104
    check-cast p1, Lcom/google/api/services/discussions/DiscussionsRequest;

    invoke-virtual {p0, p1}, Lcom/google/api/services/discussions/DiscussionsRequestInitializer;->a(Lcom/google/api/services/discussions/DiscussionsRequest;)V

    .line 105
    return-void
.end method
