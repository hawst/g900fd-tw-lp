.class public final Lcom/google/api/services/appsactivity/Appsactivity$Builder;
.super LbdO;
.source "Appsactivity.java"


# direct methods
.method public constructor <init>(Lbeq;LbeO;Lbek;)V
    .locals 7

    .prologue
    .line 408
    const-string v3, "https://www.googleapis.com/"

    const-string v4, "appsactivity/v1beta1/"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, LbdO;-><init>(Lbeq;LbeO;Ljava/lang/String;Ljava/lang/String;Lbek;Z)V

    .line 415
    return-void
.end method


# virtual methods
.method public bridge synthetic build()LbdH;
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/Appsactivity$Builder;->build()Lcom/google/api/services/appsactivity/Appsactivity;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic build()LbdN;
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/Appsactivity$Builder;->build()Lcom/google/api/services/appsactivity/Appsactivity;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/api/services/appsactivity/Appsactivity;
    .locals 1

    .prologue
    .line 420
    new-instance v0, Lcom/google/api/services/appsactivity/Appsactivity;

    invoke-direct {v0, p0}, Lcom/google/api/services/appsactivity/Appsactivity;-><init>(Lcom/google/api/services/appsactivity/Appsactivity$Builder;)V

    return-object v0
.end method

.method public bridge synthetic setApplicationName(Ljava/lang/String;)LbdI;
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/Appsactivity$Builder;->setApplicationName(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic setApplicationName(Ljava/lang/String;)LbdO;
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/Appsactivity$Builder;->setApplicationName(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setApplicationName(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Builder;
    .locals 1

    .prologue
    .line 440
    invoke-super {p0, p1}, LbdO;->setApplicationName(Ljava/lang/String;)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    return-object v0
.end method

.method public setAppsactivityRequestInitializer(Lcom/google/api/services/appsactivity/AppsactivityRequestInitializer;)Lcom/google/api/services/appsactivity/Appsactivity$Builder;
    .locals 1

    .prologue
    .line 465
    invoke-super {p0, p1}, LbdO;->setGoogleClientRequestInitializer(LbdM;)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    return-object v0
.end method

.method public bridge synthetic setGoogleClientRequestInitializer(LbdM;)LbdI;
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/Appsactivity$Builder;->setGoogleClientRequestInitializer(LbdM;)Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic setGoogleClientRequestInitializer(LbdM;)LbdO;
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/Appsactivity$Builder;->setGoogleClientRequestInitializer(LbdM;)Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setGoogleClientRequestInitializer(LbdM;)Lcom/google/api/services/appsactivity/Appsactivity$Builder;
    .locals 1

    .prologue
    .line 471
    invoke-super {p0, p1}, LbdO;->setGoogleClientRequestInitializer(LbdM;)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    return-object v0
.end method

.method public bridge synthetic setHttpRequestInitializer(Lbek;)LbdI;
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/Appsactivity$Builder;->setHttpRequestInitializer(Lbek;)Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic setHttpRequestInitializer(Lbek;)LbdO;
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/Appsactivity$Builder;->setHttpRequestInitializer(Lbek;)Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setHttpRequestInitializer(Lbek;)Lcom/google/api/services/appsactivity/Appsactivity$Builder;
    .locals 1

    .prologue
    .line 435
    invoke-super {p0, p1}, LbdO;->setHttpRequestInitializer(Lbek;)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    return-object v0
.end method

.method public bridge synthetic setRootUrl(Ljava/lang/String;)LbdI;
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/Appsactivity$Builder;->setRootUrl(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic setRootUrl(Ljava/lang/String;)LbdO;
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/Appsactivity$Builder;->setRootUrl(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setRootUrl(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Builder;
    .locals 1

    .prologue
    .line 425
    invoke-super {p0, p1}, LbdO;->setRootUrl(Ljava/lang/String;)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    return-object v0
.end method

.method public bridge synthetic setServicePath(Ljava/lang/String;)LbdI;
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/Appsactivity$Builder;->setServicePath(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic setServicePath(Ljava/lang/String;)LbdO;
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/Appsactivity$Builder;->setServicePath(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setServicePath(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Builder;
    .locals 1

    .prologue
    .line 430
    invoke-super {p0, p1}, LbdO;->setServicePath(Ljava/lang/String;)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    return-object v0
.end method

.method public bridge synthetic setSuppressAllChecks(Z)LbdI;
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/Appsactivity$Builder;->setSuppressAllChecks(Z)Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic setSuppressAllChecks(Z)LbdO;
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/Appsactivity$Builder;->setSuppressAllChecks(Z)Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setSuppressAllChecks(Z)Lcom/google/api/services/appsactivity/Appsactivity$Builder;
    .locals 1

    .prologue
    .line 455
    invoke-super {p0, p1}, LbdO;->setSuppressAllChecks(Z)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    return-object v0
.end method

.method public bridge synthetic setSuppressPatternChecks(Z)LbdI;
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/Appsactivity$Builder;->setSuppressPatternChecks(Z)Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic setSuppressPatternChecks(Z)LbdO;
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/Appsactivity$Builder;->setSuppressPatternChecks(Z)Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setSuppressPatternChecks(Z)Lcom/google/api/services/appsactivity/Appsactivity$Builder;
    .locals 1

    .prologue
    .line 445
    invoke-super {p0, p1}, LbdO;->setSuppressPatternChecks(Z)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    return-object v0
.end method

.method public bridge synthetic setSuppressRequiredParameterChecks(Z)LbdI;
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/Appsactivity$Builder;->setSuppressRequiredParameterChecks(Z)Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic setSuppressRequiredParameterChecks(Z)LbdO;
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/Appsactivity$Builder;->setSuppressRequiredParameterChecks(Z)Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setSuppressRequiredParameterChecks(Z)Lcom/google/api/services/appsactivity/Appsactivity$Builder;
    .locals 1

    .prologue
    .line 450
    invoke-super {p0, p1}, LbdO;->setSuppressRequiredParameterChecks(Z)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/Appsactivity$Builder;

    return-object v0
.end method
