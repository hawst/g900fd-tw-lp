.class public final Lcom/google/api/services/drive/model/About$ExportFormats;
.super LbeN;
.source "About.java"


# instance fields
.field private source:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private targets:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 798
    invoke-direct {p0}, LbeN;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/drive/model/About$ExportFormats;
    .locals 1

    .prologue
    .line 855
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/About$ExportFormats;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/About$ExportFormats;
    .locals 1

    .prologue
    .line 850
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/About$ExportFormats;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 798
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/About$ExportFormats;->a()Lcom/google/api/services/drive/model/About$ExportFormats;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 798
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/About$ExportFormats;->a()Lcom/google/api/services/drive/model/About$ExportFormats;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 798
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/About$ExportFormats;->a()Lcom/google/api/services/drive/model/About$ExportFormats;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 798
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/About$ExportFormats;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/About$ExportFormats;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 798
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/About$ExportFormats;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/About$ExportFormats;

    move-result-object v0

    return-object v0
.end method
