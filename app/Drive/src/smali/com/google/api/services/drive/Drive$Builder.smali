.class public final Lcom/google/api/services/drive/Drive$Builder;
.super LbdO;
.source "Drive.java"


# direct methods
.method public constructor <init>(Lbeq;LbeO;Lbek;)V
    .locals 7

    .prologue
    .line 13303
    const-string v3, "https://www.googleapis.com/"

    const-string v4, "drive/v2internal/"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, LbdO;-><init>(Lbeq;LbeO;Ljava/lang/String;Ljava/lang/String;Lbek;Z)V

    .line 13310
    return-void
.end method


# virtual methods
.method public a(LbdM;)Lcom/google/api/services/drive/Drive$Builder;
    .locals 1

    .prologue
    .line 13366
    invoke-super {p0, p1}, LbdO;->setGoogleClientRequestInitializer(LbdM;)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Builder;

    return-object v0
.end method

.method public a(Lbek;)Lcom/google/api/services/drive/Drive$Builder;
    .locals 1

    .prologue
    .line 13330
    invoke-super {p0, p1}, LbdO;->setHttpRequestInitializer(Lbek;)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Builder;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Builder;
    .locals 1

    .prologue
    .line 13320
    invoke-super {p0, p1}, LbdO;->setRootUrl(Ljava/lang/String;)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Builder;

    return-object v0
.end method

.method public a(Z)Lcom/google/api/services/drive/Drive$Builder;
    .locals 1

    .prologue
    .line 13340
    invoke-super {p0, p1}, LbdO;->setSuppressPatternChecks(Z)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Builder;

    return-object v0
.end method

.method public a()Lcom/google/api/services/drive/Drive;
    .locals 1

    .prologue
    .line 13315
    new-instance v0, Lcom/google/api/services/drive/Drive;

    invoke-direct {v0, p0}, Lcom/google/api/services/drive/Drive;-><init>(Lcom/google/api/services/drive/Drive$Builder;)V

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Builder;
    .locals 1

    .prologue
    .line 13325
    invoke-super {p0, p1}, LbdO;->setServicePath(Ljava/lang/String;)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Builder;

    return-object v0
.end method

.method public b(Z)Lcom/google/api/services/drive/Drive$Builder;
    .locals 1

    .prologue
    .line 13345
    invoke-super {p0, p1}, LbdO;->setSuppressRequiredParameterChecks(Z)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Builder;

    return-object v0
.end method

.method public synthetic build()LbdH;
    .locals 1

    .prologue
    .line 13277
    invoke-virtual {p0}, Lcom/google/api/services/drive/Drive$Builder;->a()Lcom/google/api/services/drive/Drive;

    move-result-object v0

    return-object v0
.end method

.method public synthetic build()LbdN;
    .locals 1

    .prologue
    .line 13277
    invoke-virtual {p0}, Lcom/google/api/services/drive/Drive$Builder;->a()Lcom/google/api/services/drive/Drive;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Builder;
    .locals 1

    .prologue
    .line 13335
    invoke-super {p0, p1}, LbdO;->setApplicationName(Ljava/lang/String;)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Builder;

    return-object v0
.end method

.method public c(Z)Lcom/google/api/services/drive/Drive$Builder;
    .locals 1

    .prologue
    .line 13350
    invoke-super {p0, p1}, LbdO;->setSuppressAllChecks(Z)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Builder;

    return-object v0
.end method

.method public synthetic setApplicationName(Ljava/lang/String;)LbdI;
    .locals 1

    .prologue
    .line 13277
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Builder;->c(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setApplicationName(Ljava/lang/String;)LbdO;
    .locals 1

    .prologue
    .line 13277
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Builder;->c(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setGoogleClientRequestInitializer(LbdM;)LbdI;
    .locals 1

    .prologue
    .line 13277
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Builder;->a(LbdM;)Lcom/google/api/services/drive/Drive$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setGoogleClientRequestInitializer(LbdM;)LbdO;
    .locals 1

    .prologue
    .line 13277
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Builder;->a(LbdM;)Lcom/google/api/services/drive/Drive$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setHttpRequestInitializer(Lbek;)LbdI;
    .locals 1

    .prologue
    .line 13277
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Builder;->a(Lbek;)Lcom/google/api/services/drive/Drive$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setHttpRequestInitializer(Lbek;)LbdO;
    .locals 1

    .prologue
    .line 13277
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Builder;->a(Lbek;)Lcom/google/api/services/drive/Drive$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setRootUrl(Ljava/lang/String;)LbdI;
    .locals 1

    .prologue
    .line 13277
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Builder;->a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setRootUrl(Ljava/lang/String;)LbdO;
    .locals 1

    .prologue
    .line 13277
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Builder;->a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setServicePath(Ljava/lang/String;)LbdI;
    .locals 1

    .prologue
    .line 13277
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Builder;->b(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setServicePath(Ljava/lang/String;)LbdO;
    .locals 1

    .prologue
    .line 13277
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Builder;->b(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setSuppressAllChecks(Z)LbdI;
    .locals 1

    .prologue
    .line 13277
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Builder;->c(Z)Lcom/google/api/services/drive/Drive$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setSuppressAllChecks(Z)LbdO;
    .locals 1

    .prologue
    .line 13277
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Builder;->c(Z)Lcom/google/api/services/drive/Drive$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setSuppressPatternChecks(Z)LbdI;
    .locals 1

    .prologue
    .line 13277
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Builder;->a(Z)Lcom/google/api/services/drive/Drive$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setSuppressPatternChecks(Z)LbdO;
    .locals 1

    .prologue
    .line 13277
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Builder;->a(Z)Lcom/google/api/services/drive/Drive$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setSuppressRequiredParameterChecks(Z)LbdI;
    .locals 1

    .prologue
    .line 13277
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Builder;->b(Z)Lcom/google/api/services/drive/Drive$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setSuppressRequiredParameterChecks(Z)LbdO;
    .locals 1

    .prologue
    .line 13277
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Builder;->b(Z)Lcom/google/api/services/drive/Drive$Builder;

    move-result-object v0

    return-object v0
.end method
