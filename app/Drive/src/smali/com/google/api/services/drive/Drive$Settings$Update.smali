.class public Lcom/google/api/services/drive/Drive$Settings$Update;
.super Lcom/google/api/services/drive/DriveRequest;
.source "Drive.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/api/services/drive/DriveRequest",
        "<",
        "Lcom/google/api/services/drive/model/Setting;",
        ">;"
    }
.end annotation


# static fields
.field private static final REST_PATH:Ljava/lang/String; = "settings/{keyname}"


# instance fields
.field private keyname:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private namespace:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/api/services/drive/Drive$Settings;


# virtual methods
.method public a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Settings$Update;
    .locals 1

    .prologue
    .line 12981
    invoke-super {p0, p1}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Settings$Update;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Settings$Update;
    .locals 1

    .prologue
    .line 13033
    invoke-super {p0, p1, p2}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Settings$Update;

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 12941
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Settings$Update;->a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Settings$Update;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 12941
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Settings$Update;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Settings$Update;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdJ;
    .locals 1

    .prologue
    .line 12941
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Settings$Update;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Settings$Update;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdP;
    .locals 1

    .prologue
    .line 12941
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Settings$Update;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Settings$Update;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 12941
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Settings$Update;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Settings$Update;

    move-result-object v0

    return-object v0
.end method
