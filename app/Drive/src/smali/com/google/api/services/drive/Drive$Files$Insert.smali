.class public Lcom/google/api/services/drive/Drive$Files$Insert;
.super Lcom/google/api/services/drive/DriveRequest;
.source "Drive.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/api/services/drive/DriveRequest",
        "<",
        "Lcom/google/api/services/drive/model/File;",
        ">;"
    }
.end annotation


# static fields
.field private static final REST_PATH:Ljava/lang/String; = "files"


# instance fields
.field private convert:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private ocr:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private ocrLanguage:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private pinned:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/api/services/drive/Drive$Files;

.field private timedTextLanguage:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private timedTextTrackName:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private useContentAsIndexableText:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private visibility:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field


# virtual methods
.method public a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$Insert;
    .locals 1

    .prologue
    .line 5584
    invoke-super {p0, p1}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Files$Insert;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Files$Insert;
    .locals 1

    .prologue
    .line 5841
    invoke-super {p0, p1, p2}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Files$Insert;

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 5521
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Files$Insert;->a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$Insert;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 5521
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Files$Insert;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Files$Insert;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdJ;
    .locals 1

    .prologue
    .line 5521
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Files$Insert;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Files$Insert;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdP;
    .locals 1

    .prologue
    .line 5521
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Files$Insert;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Files$Insert;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 5521
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Files$Insert;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Files$Insert;

    move-result-object v0

    return-object v0
.end method
