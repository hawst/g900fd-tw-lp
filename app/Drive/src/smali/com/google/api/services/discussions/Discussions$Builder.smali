.class public final Lcom/google/api/services/discussions/Discussions$Builder;
.super LbdO;
.source "Discussions.java"


# virtual methods
.method public a(LbdM;)Lcom/google/api/services/discussions/Discussions$Builder;
    .locals 1

    .prologue
    .line 2044
    invoke-super {p0, p1}, LbdO;->setGoogleClientRequestInitializer(LbdM;)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/Discussions$Builder;

    return-object v0
.end method

.method public a(Lbek;)Lcom/google/api/services/discussions/Discussions$Builder;
    .locals 1

    .prologue
    .line 2008
    invoke-super {p0, p1}, LbdO;->setHttpRequestInitializer(Lbek;)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/Discussions$Builder;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/google/api/services/discussions/Discussions$Builder;
    .locals 1

    .prologue
    .line 1998
    invoke-super {p0, p1}, LbdO;->setRootUrl(Ljava/lang/String;)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/Discussions$Builder;

    return-object v0
.end method

.method public a(Z)Lcom/google/api/services/discussions/Discussions$Builder;
    .locals 1

    .prologue
    .line 2018
    invoke-super {p0, p1}, LbdO;->setSuppressPatternChecks(Z)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/Discussions$Builder;

    return-object v0
.end method

.method public a()Lcom/google/api/services/discussions/Discussions;
    .locals 1

    .prologue
    .line 1993
    new-instance v0, Lcom/google/api/services/discussions/Discussions;

    invoke-direct {v0, p0}, Lcom/google/api/services/discussions/Discussions;-><init>(Lcom/google/api/services/discussions/Discussions$Builder;)V

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/google/api/services/discussions/Discussions$Builder;
    .locals 1

    .prologue
    .line 2003
    invoke-super {p0, p1}, LbdO;->setServicePath(Ljava/lang/String;)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/Discussions$Builder;

    return-object v0
.end method

.method public b(Z)Lcom/google/api/services/discussions/Discussions$Builder;
    .locals 1

    .prologue
    .line 2023
    invoke-super {p0, p1}, LbdO;->setSuppressRequiredParameterChecks(Z)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/Discussions$Builder;

    return-object v0
.end method

.method public synthetic build()LbdH;
    .locals 1

    .prologue
    .line 1955
    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$Builder;->a()Lcom/google/api/services/discussions/Discussions;

    move-result-object v0

    return-object v0
.end method

.method public synthetic build()LbdN;
    .locals 1

    .prologue
    .line 1955
    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$Builder;->a()Lcom/google/api/services/discussions/Discussions;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/google/api/services/discussions/Discussions$Builder;
    .locals 1

    .prologue
    .line 2013
    invoke-super {p0, p1}, LbdO;->setApplicationName(Ljava/lang/String;)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/Discussions$Builder;

    return-object v0
.end method

.method public c(Z)Lcom/google/api/services/discussions/Discussions$Builder;
    .locals 1

    .prologue
    .line 2028
    invoke-super {p0, p1}, LbdO;->setSuppressAllChecks(Z)LbdO;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/Discussions$Builder;

    return-object v0
.end method

.method public synthetic setApplicationName(Ljava/lang/String;)LbdI;
    .locals 1

    .prologue
    .line 1955
    invoke-virtual {p0, p1}, Lcom/google/api/services/discussions/Discussions$Builder;->c(Ljava/lang/String;)Lcom/google/api/services/discussions/Discussions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setApplicationName(Ljava/lang/String;)LbdO;
    .locals 1

    .prologue
    .line 1955
    invoke-virtual {p0, p1}, Lcom/google/api/services/discussions/Discussions$Builder;->c(Ljava/lang/String;)Lcom/google/api/services/discussions/Discussions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setGoogleClientRequestInitializer(LbdM;)LbdI;
    .locals 1

    .prologue
    .line 1955
    invoke-virtual {p0, p1}, Lcom/google/api/services/discussions/Discussions$Builder;->a(LbdM;)Lcom/google/api/services/discussions/Discussions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setGoogleClientRequestInitializer(LbdM;)LbdO;
    .locals 1

    .prologue
    .line 1955
    invoke-virtual {p0, p1}, Lcom/google/api/services/discussions/Discussions$Builder;->a(LbdM;)Lcom/google/api/services/discussions/Discussions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setHttpRequestInitializer(Lbek;)LbdI;
    .locals 1

    .prologue
    .line 1955
    invoke-virtual {p0, p1}, Lcom/google/api/services/discussions/Discussions$Builder;->a(Lbek;)Lcom/google/api/services/discussions/Discussions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setHttpRequestInitializer(Lbek;)LbdO;
    .locals 1

    .prologue
    .line 1955
    invoke-virtual {p0, p1}, Lcom/google/api/services/discussions/Discussions$Builder;->a(Lbek;)Lcom/google/api/services/discussions/Discussions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setRootUrl(Ljava/lang/String;)LbdI;
    .locals 1

    .prologue
    .line 1955
    invoke-virtual {p0, p1}, Lcom/google/api/services/discussions/Discussions$Builder;->a(Ljava/lang/String;)Lcom/google/api/services/discussions/Discussions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setRootUrl(Ljava/lang/String;)LbdO;
    .locals 1

    .prologue
    .line 1955
    invoke-virtual {p0, p1}, Lcom/google/api/services/discussions/Discussions$Builder;->a(Ljava/lang/String;)Lcom/google/api/services/discussions/Discussions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setServicePath(Ljava/lang/String;)LbdI;
    .locals 1

    .prologue
    .line 1955
    invoke-virtual {p0, p1}, Lcom/google/api/services/discussions/Discussions$Builder;->b(Ljava/lang/String;)Lcom/google/api/services/discussions/Discussions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setServicePath(Ljava/lang/String;)LbdO;
    .locals 1

    .prologue
    .line 1955
    invoke-virtual {p0, p1}, Lcom/google/api/services/discussions/Discussions$Builder;->b(Ljava/lang/String;)Lcom/google/api/services/discussions/Discussions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setSuppressAllChecks(Z)LbdI;
    .locals 1

    .prologue
    .line 1955
    invoke-virtual {p0, p1}, Lcom/google/api/services/discussions/Discussions$Builder;->c(Z)Lcom/google/api/services/discussions/Discussions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setSuppressAllChecks(Z)LbdO;
    .locals 1

    .prologue
    .line 1955
    invoke-virtual {p0, p1}, Lcom/google/api/services/discussions/Discussions$Builder;->c(Z)Lcom/google/api/services/discussions/Discussions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setSuppressPatternChecks(Z)LbdI;
    .locals 1

    .prologue
    .line 1955
    invoke-virtual {p0, p1}, Lcom/google/api/services/discussions/Discussions$Builder;->a(Z)Lcom/google/api/services/discussions/Discussions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setSuppressPatternChecks(Z)LbdO;
    .locals 1

    .prologue
    .line 1955
    invoke-virtual {p0, p1}, Lcom/google/api/services/discussions/Discussions$Builder;->a(Z)Lcom/google/api/services/discussions/Discussions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setSuppressRequiredParameterChecks(Z)LbdI;
    .locals 1

    .prologue
    .line 1955
    invoke-virtual {p0, p1}, Lcom/google/api/services/discussions/Discussions$Builder;->b(Z)Lcom/google/api/services/discussions/Discussions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setSuppressRequiredParameterChecks(Z)LbdO;
    .locals 1

    .prologue
    .line 1955
    invoke-virtual {p0, p1}, Lcom/google/api/services/discussions/Discussions$Builder;->b(Z)Lcom/google/api/services/discussions/Discussions$Builder;

    move-result-object v0

    return-object v0
.end method
