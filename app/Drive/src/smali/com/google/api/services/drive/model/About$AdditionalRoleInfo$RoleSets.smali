.class public final Lcom/google/api/services/drive/model/About$AdditionalRoleInfo$RoleSets;
.super LbeN;
.source "About.java"


# instance fields
.field private additionalRoles:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private primaryRole:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 732
    invoke-direct {p0}, LbeN;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/drive/model/About$AdditionalRoleInfo$RoleSets;
    .locals 1

    .prologue
    .line 789
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo$RoleSets;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/About$AdditionalRoleInfo$RoleSets;
    .locals 1

    .prologue
    .line 784
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo$RoleSets;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 770
    iget-object v0, p0, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo$RoleSets;->primaryRole:Ljava/lang/String;

    return-object v0
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 753
    iget-object v0, p0, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo$RoleSets;->additionalRoles:Ljava/util/List;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 732
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo$RoleSets;->a()Lcom/google/api/services/drive/model/About$AdditionalRoleInfo$RoleSets;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 732
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo$RoleSets;->a()Lcom/google/api/services/drive/model/About$AdditionalRoleInfo$RoleSets;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 732
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo$RoleSets;->a()Lcom/google/api/services/drive/model/About$AdditionalRoleInfo$RoleSets;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 732
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo$RoleSets;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/About$AdditionalRoleInfo$RoleSets;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 732
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo$RoleSets;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/About$AdditionalRoleInfo$RoleSets;

    move-result-object v0

    return-object v0
.end method
