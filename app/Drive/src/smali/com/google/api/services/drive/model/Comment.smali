.class public final Lcom/google/api/services/drive/model/Comment;
.super LbeN;
.source "Comment.java"


# instance fields
.field private anchor:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private author:Lcom/google/api/services/drive/model/User;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private commentId:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private content:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private context:Lcom/google/api/services/drive/model/Comment$Context;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private createdDate:Lbfx;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private deleted:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private fileId:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private fileTitle:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private htmlContent:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private kind:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private modifiedDate:Lbfx;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private replies:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/drive/model/CommentReply;",
            ">;"
        }
    .end annotation
.end field

.field private selfLink:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private status:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, LbeN;-><init>()V

    .line 420
    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/drive/model/Comment;
    .locals 1

    .prologue
    .line 414
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/Comment;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/Comment;
    .locals 1

    .prologue
    .line 409
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/Comment;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/Comment;->a()Lcom/google/api/services/drive/model/Comment;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/Comment;->a()Lcom/google/api/services/drive/model/Comment;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/Comment;->a()Lcom/google/api/services/drive/model/Comment;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/Comment;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/Comment;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/Comment;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/Comment;

    move-result-object v0

    return-object v0
.end method
