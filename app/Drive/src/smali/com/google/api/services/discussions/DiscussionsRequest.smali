.class public abstract Lcom/google/api/services/discussions/DiscussionsRequest;
.super LbdP;
.source "DiscussionsRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LbdP",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private alt:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private fields:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private key:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private oauthToken:Ljava/lang/String;
    .annotation runtime LbfE;
        a = "oauth_token"
    .end annotation
.end field

.field private prettyPrint:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private quotaUser:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private userIp:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field


# virtual methods
.method public final a()Lcom/google/api/services/discussions/Discussions;
    .locals 1

    .prologue
    .line 189
    invoke-super {p0}, LbdP;->getAbstractGoogleClient()LbdN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/Discussions;

    return-object v0
.end method

.method public a(Lbed;)Lcom/google/api/services/discussions/DiscussionsRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbed;",
            ")",
            "Lcom/google/api/services/discussions/DiscussionsRequest",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 199
    invoke-super {p0, p1}, LbdP;->setRequestHeaders(Lbed;)LbdP;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/DiscussionsRequest;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/DiscussionsRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")",
            "Lcom/google/api/services/discussions/DiscussionsRequest",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 204
    invoke-super {p0, p1, p2}, LbdP;->set(Ljava/lang/String;Ljava/lang/Object;)LbdP;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/DiscussionsRequest;

    return-object v0
.end method

.method public a(Z)Lcom/google/api/services/discussions/DiscussionsRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/api/services/discussions/DiscussionsRequest",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 194
    invoke-super {p0, p1}, LbdP;->setDisableGZipContent(Z)LbdP;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/DiscussionsRequest;

    return-object v0
.end method

.method public synthetic getAbstractGoogleClient()LbdH;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/google/api/services/discussions/DiscussionsRequest;->a()Lcom/google/api/services/discussions/Discussions;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getAbstractGoogleClient()LbdN;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/google/api/services/discussions/DiscussionsRequest;->a()Lcom/google/api/services/discussions/Discussions;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdJ;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/discussions/DiscussionsRequest;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/DiscussionsRequest;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdP;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/discussions/DiscussionsRequest;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/DiscussionsRequest;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/discussions/DiscussionsRequest;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/DiscussionsRequest;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setDisableGZipContent(Z)LbdJ;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/api/services/discussions/DiscussionsRequest;->a(Z)Lcom/google/api/services/discussions/DiscussionsRequest;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setDisableGZipContent(Z)LbdP;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/api/services/discussions/DiscussionsRequest;->a(Z)Lcom/google/api/services/discussions/DiscussionsRequest;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setRequestHeaders(Lbed;)LbdJ;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/api/services/discussions/DiscussionsRequest;->a(Lbed;)Lcom/google/api/services/discussions/DiscussionsRequest;

    move-result-object v0

    return-object v0
.end method

.method public synthetic setRequestHeaders(Lbed;)LbdP;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/api/services/discussions/DiscussionsRequest;->a(Lbed;)Lcom/google/api/services/discussions/DiscussionsRequest;

    move-result-object v0

    return-object v0
.end method
