.class public final Lcom/google/api/services/appsactivity/model/Event;
.super LbeN;
.source "Event.java"


# instance fields
.field private additionalEventTypes:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private eventTimeMillis:Ljava/math/BigInteger;
    .annotation runtime LbeW;
    .end annotation

    .annotation runtime LbfE;
    .end annotation
.end field

.field private fromUserDeletion:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private move:Lcom/google/api/services/appsactivity/model/Move;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private permissionChanges:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/appsactivity/model/PermissionChange;",
            ">;"
        }
    .end annotation
.end field

.field private primaryEventType:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private rename:Lcom/google/api/services/appsactivity/model/Rename;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private target:Lcom/google/api/services/appsactivity/model/Target;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private timestamp:Ljava/math/BigInteger;
    .annotation runtime LbeW;
    .end annotation

    .annotation runtime LbfE;
    .end annotation
.end field

.field private user:Lcom/google/api/services/appsactivity/model/User;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, LbeN;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/Event;->clone()Lcom/google/api/services/appsactivity/model/Event;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/Event;->clone()Lcom/google/api/services/appsactivity/model/Event;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/api/services/appsactivity/model/Event;
    .locals 1

    .prologue
    .line 289
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/Event;

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/Event;->clone()Lcom/google/api/services/appsactivity/model/Event;

    move-result-object v0

    return-object v0
.end method

.method public getAdditionalEventTypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Event;->additionalEventTypes:Ljava/util/List;

    return-object v0
.end method

.method public getEventTimeMillis()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Event;->eventTimeMillis:Ljava/math/BigInteger;

    return-object v0
.end method

.method public getFromUserDeletion()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Event;->fromUserDeletion:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getMove()Lcom/google/api/services/appsactivity/model/Move;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Event;->move:Lcom/google/api/services/appsactivity/model/Move;

    return-object v0
.end method

.method public getPermissionChanges()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/appsactivity/model/PermissionChange;",
            ">;"
        }
    .end annotation

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Event;->permissionChanges:Ljava/util/List;

    return-object v0
.end method

.method public getPrimaryEventType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Event;->primaryEventType:Ljava/lang/String;

    return-object v0
.end method

.method public getRename()Lcom/google/api/services/appsactivity/model/Rename;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Event;->rename:Lcom/google/api/services/appsactivity/model/Rename;

    return-object v0
.end method

.method public getTarget()Lcom/google/api/services/appsactivity/model/Target;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Event;->target:Lcom/google/api/services/appsactivity/model/Target;

    return-object v0
.end method

.method public getTimestamp()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Event;->timestamp:Ljava/math/BigInteger;

    return-object v0
.end method

.method public getUser()Lcom/google/api/services/appsactivity/model/User;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Event;->user:Lcom/google/api/services/appsactivity/model/User;

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/model/Event;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/Event;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/model/Event;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/Event;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/Event;
    .locals 1

    .prologue
    .line 284
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/Event;

    return-object v0
.end method

.method public setAdditionalEventTypes(Ljava/util/List;)Lcom/google/api/services/appsactivity/model/Event;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/api/services/appsactivity/model/Event;"
        }
    .end annotation

    .prologue
    .line 123
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Event;->additionalEventTypes:Ljava/util/List;

    .line 124
    return-object p0
.end method

.method public setEventTimeMillis(Ljava/math/BigInteger;)Lcom/google/api/services/appsactivity/model/Event;
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Event;->eventTimeMillis:Ljava/math/BigInteger;

    .line 141
    return-object p0
.end method

.method public setFromUserDeletion(Ljava/lang/Boolean;)Lcom/google/api/services/appsactivity/model/Event;
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Event;->fromUserDeletion:Ljava/lang/Boolean;

    .line 158
    return-object p0
.end method

.method public setMove(Lcom/google/api/services/appsactivity/model/Move;)Lcom/google/api/services/appsactivity/model/Event;
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Event;->move:Lcom/google/api/services/appsactivity/model/Move;

    .line 175
    return-object p0
.end method

.method public setPermissionChanges(Ljava/util/List;)Lcom/google/api/services/appsactivity/model/Event;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/appsactivity/model/PermissionChange;",
            ">;)",
            "Lcom/google/api/services/appsactivity/model/Event;"
        }
    .end annotation

    .prologue
    .line 193
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Event;->permissionChanges:Ljava/util/List;

    .line 194
    return-object p0
.end method

.method public setPrimaryEventType(Ljava/lang/String;)Lcom/google/api/services/appsactivity/model/Event;
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Event;->primaryEventType:Ljava/lang/String;

    .line 211
    return-object p0
.end method

.method public setRename(Lcom/google/api/services/appsactivity/model/Rename;)Lcom/google/api/services/appsactivity/model/Event;
    .locals 0

    .prologue
    .line 227
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Event;->rename:Lcom/google/api/services/appsactivity/model/Rename;

    .line 228
    return-object p0
.end method

.method public setTarget(Lcom/google/api/services/appsactivity/model/Target;)Lcom/google/api/services/appsactivity/model/Event;
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Event;->target:Lcom/google/api/services/appsactivity/model/Target;

    .line 245
    return-object p0
.end method

.method public setTimestamp(Ljava/math/BigInteger;)Lcom/google/api/services/appsactivity/model/Event;
    .locals 0

    .prologue
    .line 261
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Event;->timestamp:Ljava/math/BigInteger;

    .line 262
    return-object p0
.end method

.method public setUser(Lcom/google/api/services/appsactivity/model/User;)Lcom/google/api/services/appsactivity/model/Event;
    .locals 0

    .prologue
    .line 278
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Event;->user:Lcom/google/api/services/appsactivity/model/User;

    .line 279
    return-object p0
.end method
