.class public final Lcom/google/api/services/drive/model/App$RankingInfo;
.super LbeN;
.source "App.java"


# instance fields
.field private absoluteScore:Ljava/lang/Double;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private fileExtensionScores:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/drive/model/App$RankingInfo$FileExtensionScores;",
            ">;"
        }
    .end annotation
.end field

.field private mimeTypeScores:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/drive/model/App$RankingInfo$MimeTypeScores;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 950
    const-class v0, Lcom/google/api/services/drive/model/App$RankingInfo$FileExtensionScores;

    invoke-static {v0}, Lbfs;->a(Ljava/lang/Class;)Ljava/lang/Object;

    .line 962
    const-class v0, Lcom/google/api/services/drive/model/App$RankingInfo$MimeTypeScores;

    invoke-static {v0}, Lbfs;->a(Ljava/lang/Class;)Ljava/lang/Object;

    .line 963
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 933
    invoke-direct {p0}, LbeN;-><init>()V

    .line 1087
    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/drive/model/App$RankingInfo;
    .locals 1

    .prologue
    .line 1017
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/App$RankingInfo;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/App$RankingInfo;
    .locals 1

    .prologue
    .line 1012
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/App$RankingInfo;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 933
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/App$RankingInfo;->a()Lcom/google/api/services/drive/model/App$RankingInfo;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 933
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/App$RankingInfo;->a()Lcom/google/api/services/drive/model/App$RankingInfo;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 933
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/App$RankingInfo;->a()Lcom/google/api/services/drive/model/App$RankingInfo;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 933
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/App$RankingInfo;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/App$RankingInfo;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 933
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/App$RankingInfo;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/App$RankingInfo;

    move-result-object v0

    return-object v0
.end method
