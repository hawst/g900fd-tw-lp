.class public final Lcom/google/api/services/appsactivity/model/Activity;
.super LbeN;
.source "Activity.java"


# instance fields
.field private combinedEvent:Lcom/google/api/services/appsactivity/model/Event;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private singleEvents:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/appsactivity/model/Event;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, LbeN;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/Activity;->clone()Lcom/google/api/services/appsactivity/model/Activity;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/Activity;->clone()Lcom/google/api/services/appsactivity/model/Activity;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/api/services/appsactivity/model/Activity;
    .locals 1

    .prologue
    .line 89
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/Activity;

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/Activity;->clone()Lcom/google/api/services/appsactivity/model/Activity;

    move-result-object v0

    return-object v0
.end method

.method public getCombinedEvent()Lcom/google/api/services/appsactivity/model/Event;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Activity;->combinedEvent:Lcom/google/api/services/appsactivity/model/Event;

    return-object v0
.end method

.method public getSingleEvents()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/appsactivity/model/Event;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Activity;->singleEvents:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/model/Activity;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/Activity;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/model/Activity;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/Activity;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/Activity;
    .locals 1

    .prologue
    .line 84
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/Activity;

    return-object v0
.end method

.method public setCombinedEvent(Lcom/google/api/services/appsactivity/model/Event;)Lcom/google/api/services/appsactivity/model/Activity;
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Activity;->combinedEvent:Lcom/google/api/services/appsactivity/model/Event;

    .line 62
    return-object p0
.end method

.method public setSingleEvents(Ljava/util/List;)Lcom/google/api/services/appsactivity/model/Activity;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/appsactivity/model/Event;",
            ">;)",
            "Lcom/google/api/services/appsactivity/model/Activity;"
        }
    .end annotation

    .prologue
    .line 78
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Activity;->singleEvents:Ljava/util/List;

    .line 79
    return-object p0
.end method
