.class public Lcom/google/api/services/drive/Drive$Apps$List;
.super Lcom/google/api/services/drive/DriveRequest;
.source "Drive.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/api/services/drive/DriveRequest",
        "<",
        "Lcom/google/api/services/drive/model/AppList;",
        ">;"
    }
.end annotation


# static fields
.field private static final REST_PATH:Ljava/lang/String; = "apps"


# instance fields
.field private appFilterExtensions:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private appFilterMimeTypes:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private appQueryScope:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private languageCode:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/api/services/drive/Drive$Apps;


# direct methods
.method protected constructor <init>(Lcom/google/api/services/drive/Drive$Apps;)V
    .locals 6

    .prologue
    .line 675
    iput-object p1, p0, Lcom/google/api/services/drive/Drive$Apps$List;->this$1:Lcom/google/api/services/drive/Drive$Apps;

    .line 676
    iget-object v1, p1, Lcom/google/api/services/drive/Drive$Apps;->this$0:Lcom/google/api/services/drive/Drive;

    const-string v2, "GET"

    const-string v3, "apps"

    const/4 v4, 0x0

    const-class v5, Lcom/google/api/services/drive/model/AppList;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/api/services/drive/DriveRequest;-><init>(Lcom/google/api/services/drive/Drive;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)V

    .line 677
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Apps$List;
    .locals 1

    .prologue
    .line 706
    invoke-super {p0, p1}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Apps$List;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Apps$List;
    .locals 1

    .prologue
    .line 821
    invoke-super {p0, p1, p2}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Apps$List;

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 659
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Apps$List;->a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Apps$List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 659
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Apps$List;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Apps$List;

    move-result-object v0

    return-object v0
.end method

.method public buildHttpRequestUsingHead()Lbei;
    .locals 1

    .prologue
    .line 686
    invoke-super {p0}, Lcom/google/api/services/drive/DriveRequest;->buildHttpRequestUsingHead()Lbei;

    move-result-object v0

    return-object v0
.end method

.method public executeUsingHead()Lbel;
    .locals 1

    .prologue
    .line 681
    invoke-super {p0}, Lcom/google/api/services/drive/DriveRequest;->executeUsingHead()Lbel;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdJ;
    .locals 1

    .prologue
    .line 659
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Apps$List;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Apps$List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdP;
    .locals 1

    .prologue
    .line 659
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Apps$List;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Apps$List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 659
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Apps$List;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Apps$List;

    move-result-object v0

    return-object v0
.end method
