.class public final Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;
.super LbeN;
.source "ListActivitiesResponse.java"


# instance fields
.field private activities:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/appsactivity/model/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private nextPageToken:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/google/api/services/appsactivity/model/Activity;

    invoke-static {v0}, Lbfs;->a(Ljava/lang/Class;)Ljava/lang/Object;

    .line 45
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, LbeN;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;->clone()Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;->clone()Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;
    .locals 1

    .prologue
    .line 95
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;->clone()Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;

    move-result-object v0

    return-object v0
.end method

.method public getActivities()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/appsactivity/model/Activity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;->activities:Ljava/util/List;

    return-object v0
.end method

.method public getNextPageToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;->nextPageToken:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;
    .locals 1

    .prologue
    .line 90
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;

    return-object v0
.end method

.method public setActivities(Ljava/util/List;)Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/appsactivity/model/Activity;",
            ">;)",
            "Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;"
        }
    .end annotation

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;->activities:Ljava/util/List;

    .line 68
    return-object p0
.end method

.method public setNextPageToken(Ljava/lang/String;)Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;->nextPageToken:Ljava/lang/String;

    .line 85
    return-object p0
.end method
