.class public Lcom/google/api/services/drive/Drive$Settings$Insert;
.super Lcom/google/api/services/drive/DriveRequest;
.source "Drive.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/api/services/drive/DriveRequest",
        "<",
        "Lcom/google/api/services/drive/model/Setting;",
        ">;"
    }
.end annotation


# static fields
.field private static final REST_PATH:Ljava/lang/String; = "settings"


# instance fields
.field private namespace:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/api/services/drive/Drive$Settings;


# direct methods
.method protected constructor <init>(Lcom/google/api/services/drive/Drive$Settings;Lcom/google/api/services/drive/model/Setting;)V
    .locals 6

    .prologue
    .line 12648
    iput-object p1, p0, Lcom/google/api/services/drive/Drive$Settings$Insert;->this$1:Lcom/google/api/services/drive/Drive$Settings;

    .line 12649
    iget-object v1, p1, Lcom/google/api/services/drive/Drive$Settings;->this$0:Lcom/google/api/services/drive/Drive;

    const-string v2, "POST"

    const-string v3, "settings"

    const-class v5, Lcom/google/api/services/drive/model/Setting;

    move-object v0, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/api/services/drive/DriveRequest;-><init>(Lcom/google/api/services/drive/Drive;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)V

    .line 12650
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Settings$Insert;
    .locals 1

    .prologue
    .line 12669
    invoke-super {p0, p1}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Settings$Insert;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Settings$Insert;
    .locals 1

    .prologue
    .line 12705
    invoke-super {p0, p1, p2}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Settings$Insert;

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 12631
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Settings$Insert;->a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Settings$Insert;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 12631
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Settings$Insert;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Settings$Insert;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Settings$Insert;
    .locals 0

    .prologue
    .line 12699
    iput-object p1, p0, Lcom/google/api/services/drive/Drive$Settings$Insert;->namespace:Ljava/lang/String;

    .line 12700
    return-object p0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdJ;
    .locals 1

    .prologue
    .line 12631
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Settings$Insert;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Settings$Insert;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdP;
    .locals 1

    .prologue
    .line 12631
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Settings$Insert;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Settings$Insert;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 12631
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Settings$Insert;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Settings$Insert;

    move-result-object v0

    return-object v0
.end method
