.class public final Lcom/google/api/services/drive/model/App$Icons;
.super LbeN;
.source "App.java"


# instance fields
.field private category:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private iconUrl:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private size:Ljava/lang/Integer;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 838
    invoke-direct {p0}, LbeN;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/drive/model/App$Icons;
    .locals 1

    .prologue
    .line 925
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/App$Icons;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/App$Icons;
    .locals 1

    .prologue
    .line 920
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/App$Icons;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 838
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/App$Icons;->a()Lcom/google/api/services/drive/model/App$Icons;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 838
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/App$Icons;->a()Lcom/google/api/services/drive/model/App$Icons;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 838
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/App$Icons;->a()Lcom/google/api/services/drive/model/App$Icons;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 838
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/App$Icons;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/App$Icons;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 838
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/App$Icons;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/App$Icons;

    move-result-object v0

    return-object v0
.end method
