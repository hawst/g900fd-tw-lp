.class public Lcom/google/api/services/discussions/DiscussionsScopes;
.super Ljava/lang/Object;
.source "DiscussionsScopes.java"


# static fields
.field public static final DISCUSSIONS:Ljava/lang/String; = "https://www.googleapis.com/auth/discussions"

.field public static final DISCUSSIONS_READONLY:Ljava/lang/String; = "https://www.googleapis.com/auth/discussions.readonly"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    return-void
.end method
