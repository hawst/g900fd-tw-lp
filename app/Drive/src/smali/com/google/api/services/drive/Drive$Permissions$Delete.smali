.class public Lcom/google/api/services/drive/Drive$Permissions$Delete;
.super Lcom/google/api/services/drive/DriveRequest;
.source "Drive.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/api/services/drive/DriveRequest",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final REST_PATH:Ljava/lang/String; = "files/{fileId}/permissions/{permissionId}"


# instance fields
.field private fileId:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private permissionId:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/api/services/drive/Drive$Permissions;


# direct methods
.method protected constructor <init>(Lcom/google/api/services/drive/Drive$Permissions;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 8765
    iput-object p1, p0, Lcom/google/api/services/drive/Drive$Permissions$Delete;->this$1:Lcom/google/api/services/drive/Drive$Permissions;

    .line 8766
    iget-object v1, p1, Lcom/google/api/services/drive/Drive$Permissions;->this$0:Lcom/google/api/services/drive/Drive;

    const-string v2, "DELETE"

    const-string v3, "files/{fileId}/permissions/{permissionId}"

    const/4 v4, 0x0

    const-class v5, Ljava/lang/Void;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/api/services/drive/DriveRequest;-><init>(Lcom/google/api/services/drive/Drive;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)V

    .line 8767
    const-string v0, "Required parameter fileId must be specified."

    invoke-static {p2, v0}, LbfN;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/services/drive/Drive$Permissions$Delete;->fileId:Ljava/lang/String;

    .line 8768
    const-string v0, "Required parameter permissionId must be specified."

    invoke-static {p3, v0}, LbfN;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/services/drive/Drive$Permissions$Delete;->permissionId:Ljava/lang/String;

    .line 8769
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Permissions$Delete;
    .locals 1

    .prologue
    .line 8788
    invoke-super {p0, p1}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Permissions$Delete;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Permissions$Delete;
    .locals 1

    .prologue
    .line 8840
    invoke-super {p0, p1, p2}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Permissions$Delete;

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 8747
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Permissions$Delete;->a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Permissions$Delete;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 8747
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Permissions$Delete;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Permissions$Delete;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdJ;
    .locals 1

    .prologue
    .line 8747
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Permissions$Delete;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Permissions$Delete;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdP;
    .locals 1

    .prologue
    .line 8747
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Permissions$Delete;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Permissions$Delete;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 8747
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Permissions$Delete;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Permissions$Delete;

    move-result-object v0

    return-object v0
.end method
