.class public Lcom/google/api/services/drive/Drive$Properties$Get;
.super Lcom/google/api/services/drive/DriveRequest;
.source "Drive.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/api/services/drive/DriveRequest",
        "<",
        "Lcom/google/api/services/drive/model/Property;",
        ">;"
    }
.end annotation


# static fields
.field private static final REST_PATH:Ljava/lang/String; = "files/{fileId}/properties/{propertyKey}"


# instance fields
.field private fileId:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private propertyKey:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/api/services/drive/Drive$Properties;

.field private visibility:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field


# virtual methods
.method public a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Properties$Get;
    .locals 1

    .prologue
    .line 9890
    invoke-super {p0, p1}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Properties$Get;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Properties$Get;
    .locals 1

    .prologue
    .line 9958
    invoke-super {p0, p1, p2}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Properties$Get;

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 9839
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Properties$Get;->a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Properties$Get;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 9839
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Properties$Get;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Properties$Get;

    move-result-object v0

    return-object v0
.end method

.method public buildHttpRequestUsingHead()Lbei;
    .locals 1

    .prologue
    .line 9870
    invoke-super {p0}, Lcom/google/api/services/drive/DriveRequest;->buildHttpRequestUsingHead()Lbei;

    move-result-object v0

    return-object v0
.end method

.method public executeUsingHead()Lbel;
    .locals 1

    .prologue
    .line 9865
    invoke-super {p0}, Lcom/google/api/services/drive/DriveRequest;->executeUsingHead()Lbel;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdJ;
    .locals 1

    .prologue
    .line 9839
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Properties$Get;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Properties$Get;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdP;
    .locals 1

    .prologue
    .line 9839
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Properties$Get;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Properties$Get;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 9839
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Properties$Get;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Properties$Get;

    move-result-object v0

    return-object v0
.end method
