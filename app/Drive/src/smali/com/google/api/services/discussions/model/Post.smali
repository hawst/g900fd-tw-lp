.class public final Lcom/google/api/services/discussions/model/Post;
.super LbeN;
.source "Post.java"


# instance fields
.field private action:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private actor:Lcom/google/api/services/discussions/model/Author;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private clientId:Ljava/lang/String;
    .annotation runtime LbfE;
        a = "client_id"
    .end annotation
.end field

.field private deleted:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private dirty:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private id:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private kind:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private object__:Lcom/google/api/services/discussions/model/Post$DiscussionsObject;
    .annotation runtime LbfE;
        a = "object"
    .end annotation
.end field

.field private published:Lbfx;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private target:Lcom/google/api/services/discussions/model/Post$Target;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private updated:Lbfx;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private verb:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, LbeN;-><init>()V

    .line 501
    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/discussions/model/Post;
    .locals 1

    .prologue
    .line 336
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/model/Post;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/model/Post;
    .locals 1

    .prologue
    .line 331
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/model/Post;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/discussions/model/Post;->a()Lcom/google/api/services/discussions/model/Post;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/discussions/model/Post;->a()Lcom/google/api/services/discussions/model/Post;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/discussions/model/Post;->a()Lcom/google/api/services/discussions/model/Post;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/discussions/model/Post;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/model/Post;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/discussions/model/Post;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/model/Post;

    move-result-object v0

    return-object v0
.end method
