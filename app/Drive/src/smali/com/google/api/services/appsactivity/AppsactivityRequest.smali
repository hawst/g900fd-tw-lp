.class public abstract Lcom/google/api/services/appsactivity/AppsactivityRequest;
.super LbdP;
.source "AppsactivityRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LbdP",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private alt:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private fields:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private key:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private oauthToken:Ljava/lang/String;
    .annotation runtime LbfE;
        a = "oauth_token"
    .end annotation
.end field

.field private prettyPrint:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private quotaUser:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private userIp:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/api/services/appsactivity/Appsactivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/services/appsactivity/Appsactivity;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct/range {p0 .. p5}, LbdP;-><init>(LbdN;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)V

    .line 45
    return-void
.end method


# virtual methods
.method public bridge synthetic getAbstractGoogleClient()LbdH;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/AppsactivityRequest;->getAbstractGoogleClient()Lcom/google/api/services/appsactivity/Appsactivity;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getAbstractGoogleClient()LbdN;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/AppsactivityRequest;->getAbstractGoogleClient()Lcom/google/api/services/appsactivity/Appsactivity;

    move-result-object v0

    return-object v0
.end method

.method public final getAbstractGoogleClient()Lcom/google/api/services/appsactivity/Appsactivity;
    .locals 1

    .prologue
    .line 189
    invoke-super {p0}, LbdP;->getAbstractGoogleClient()LbdN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/Appsactivity;

    return-object v0
.end method

.method public getAlt()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/api/services/appsactivity/AppsactivityRequest;->alt:Ljava/lang/String;

    return-object v0
.end method

.method public getFields()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/api/services/appsactivity/AppsactivityRequest;->fields:Ljava/lang/String;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/api/services/appsactivity/AppsactivityRequest;->key:Ljava/lang/String;

    return-object v0
.end method

.method public getOauthToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/api/services/appsactivity/AppsactivityRequest;->oauthToken:Ljava/lang/String;

    return-object v0
.end method

.method public getPrettyPrint()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/api/services/appsactivity/AppsactivityRequest;->prettyPrint:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getQuotaUser()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/api/services/appsactivity/AppsactivityRequest;->quotaUser:Ljava/lang/String;

    return-object v0
.end method

.method public getUserIp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/api/services/appsactivity/AppsactivityRequest;->userIp:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdJ;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/AppsactivityRequest;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/AppsactivityRequest;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdP;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/AppsactivityRequest;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/AppsactivityRequest;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/AppsactivityRequest;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/AppsactivityRequest;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/AppsactivityRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")",
            "Lcom/google/api/services/appsactivity/AppsactivityRequest",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 204
    invoke-super {p0, p1, p2}, LbdP;->set(Ljava/lang/String;Ljava/lang/Object;)LbdP;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/AppsactivityRequest;

    return-object v0
.end method

.method public setAlt(Ljava/lang/String;)Lcom/google/api/services/appsactivity/AppsactivityRequest;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/api/services/appsactivity/AppsactivityRequest",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/api/services/appsactivity/AppsactivityRequest;->alt:Ljava/lang/String;

    .line 61
    return-object p0
.end method

.method public bridge synthetic setDisableGZipContent(Z)LbdJ;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/AppsactivityRequest;->setDisableGZipContent(Z)Lcom/google/api/services/appsactivity/AppsactivityRequest;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic setDisableGZipContent(Z)LbdP;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/AppsactivityRequest;->setDisableGZipContent(Z)Lcom/google/api/services/appsactivity/AppsactivityRequest;

    move-result-object v0

    return-object v0
.end method

.method public setDisableGZipContent(Z)Lcom/google/api/services/appsactivity/AppsactivityRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/api/services/appsactivity/AppsactivityRequest",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 194
    invoke-super {p0, p1}, LbdP;->setDisableGZipContent(Z)LbdP;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/AppsactivityRequest;

    return-object v0
.end method

.method public setFields(Ljava/lang/String;)Lcom/google/api/services/appsactivity/AppsactivityRequest;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/api/services/appsactivity/AppsactivityRequest",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/api/services/appsactivity/AppsactivityRequest;->fields:Ljava/lang/String;

    .line 78
    return-object p0
.end method

.method public setKey(Ljava/lang/String;)Lcom/google/api/services/appsactivity/AppsactivityRequest;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/api/services/appsactivity/AppsactivityRequest",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 101
    iput-object p1, p0, Lcom/google/api/services/appsactivity/AppsactivityRequest;->key:Ljava/lang/String;

    .line 102
    return-object p0
.end method

.method public setOauthToken(Ljava/lang/String;)Lcom/google/api/services/appsactivity/AppsactivityRequest;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/api/services/appsactivity/AppsactivityRequest",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 118
    iput-object p1, p0, Lcom/google/api/services/appsactivity/AppsactivityRequest;->oauthToken:Ljava/lang/String;

    .line 119
    return-object p0
.end method

.method public setPrettyPrint(Ljava/lang/Boolean;)Lcom/google/api/services/appsactivity/AppsactivityRequest;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/google/api/services/appsactivity/AppsactivityRequest",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 135
    iput-object p1, p0, Lcom/google/api/services/appsactivity/AppsactivityRequest;->prettyPrint:Ljava/lang/Boolean;

    .line 136
    return-object p0
.end method

.method public setQuotaUser(Ljava/lang/String;)Lcom/google/api/services/appsactivity/AppsactivityRequest;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/api/services/appsactivity/AppsactivityRequest",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 159
    iput-object p1, p0, Lcom/google/api/services/appsactivity/AppsactivityRequest;->quotaUser:Ljava/lang/String;

    .line 160
    return-object p0
.end method

.method public bridge synthetic setRequestHeaders(Lbed;)LbdJ;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/AppsactivityRequest;->setRequestHeaders(Lbed;)Lcom/google/api/services/appsactivity/AppsactivityRequest;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic setRequestHeaders(Lbed;)LbdP;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/AppsactivityRequest;->setRequestHeaders(Lbed;)Lcom/google/api/services/appsactivity/AppsactivityRequest;

    move-result-object v0

    return-object v0
.end method

.method public setRequestHeaders(Lbed;)Lcom/google/api/services/appsactivity/AppsactivityRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbed;",
            ")",
            "Lcom/google/api/services/appsactivity/AppsactivityRequest",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 199
    invoke-super {p0, p1}, LbdP;->setRequestHeaders(Lbed;)LbdP;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/AppsactivityRequest;

    return-object v0
.end method

.method public setUserIp(Ljava/lang/String;)Lcom/google/api/services/appsactivity/AppsactivityRequest;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/api/services/appsactivity/AppsactivityRequest",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 183
    iput-object p1, p0, Lcom/google/api/services/appsactivity/AppsactivityRequest;->userIp:Ljava/lang/String;

    .line 184
    return-object p0
.end method
