.class public final Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions$IncreaseDomainVisibilityInfo;
.super LbeN;
.source "CheckPermissionsResponse.java"


# instance fields
.field private domainDisplayName:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private domainName:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 369
    invoke-direct {p0}, LbeN;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions$IncreaseDomainVisibilityInfo;
    .locals 1

    .prologue
    .line 435
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions$IncreaseDomainVisibilityInfo;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions$IncreaseDomainVisibilityInfo;
    .locals 1

    .prologue
    .line 430
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions$IncreaseDomainVisibilityInfo;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 369
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions$IncreaseDomainVisibilityInfo;->a()Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions$IncreaseDomainVisibilityInfo;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 369
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions$IncreaseDomainVisibilityInfo;->a()Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions$IncreaseDomainVisibilityInfo;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 369
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions$IncreaseDomainVisibilityInfo;->a()Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions$IncreaseDomainVisibilityInfo;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 369
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions$IncreaseDomainVisibilityInfo;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions$IncreaseDomainVisibilityInfo;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 369
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions$IncreaseDomainVisibilityInfo;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions$IncreaseDomainVisibilityInfo;

    move-result-object v0

    return-object v0
.end method
