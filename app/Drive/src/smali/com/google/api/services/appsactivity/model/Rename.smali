.class public final Lcom/google/api/services/appsactivity/model/Rename;
.super LbeN;
.source "Rename.java"


# instance fields
.field private newTitle:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private oldTitle:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, LbeN;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/Rename;->clone()Lcom/google/api/services/appsactivity/model/Rename;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/Rename;->clone()Lcom/google/api/services/appsactivity/model/Rename;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/api/services/appsactivity/model/Rename;
    .locals 1

    .prologue
    .line 88
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/Rename;

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/Rename;->clone()Lcom/google/api/services/appsactivity/model/Rename;

    move-result-object v0

    return-object v0
.end method

.method public getNewTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Rename;->newTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getOldTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Rename;->oldTitle:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/model/Rename;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/Rename;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/model/Rename;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/Rename;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/Rename;
    .locals 1

    .prologue
    .line 83
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/Rename;

    return-object v0
.end method

.method public setNewTitle(Ljava/lang/String;)Lcom/google/api/services/appsactivity/model/Rename;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Rename;->newTitle:Ljava/lang/String;

    .line 61
    return-object p0
.end method

.method public setOldTitle(Ljava/lang/String;)Lcom/google/api/services/appsactivity/model/Rename;
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Rename;->oldTitle:Ljava/lang/String;

    .line 78
    return-object p0
.end method
