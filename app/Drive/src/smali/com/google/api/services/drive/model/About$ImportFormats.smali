.class public final Lcom/google/api/services/drive/model/About$ImportFormats;
.super LbeN;
.source "About.java"


# instance fields
.field private source:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private targets:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 928
    invoke-direct {p0}, LbeN;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/drive/model/About$ImportFormats;
    .locals 1

    .prologue
    .line 985
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/About$ImportFormats;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/About$ImportFormats;
    .locals 1

    .prologue
    .line 980
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/About$ImportFormats;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 949
    iget-object v0, p0, Lcom/google/api/services/drive/model/About$ImportFormats;->source:Ljava/lang/String;

    return-object v0
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 966
    iget-object v0, p0, Lcom/google/api/services/drive/model/About$ImportFormats;->targets:Ljava/util/List;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 928
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/About$ImportFormats;->a()Lcom/google/api/services/drive/model/About$ImportFormats;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 928
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/About$ImportFormats;->a()Lcom/google/api/services/drive/model/About$ImportFormats;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 928
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/About$ImportFormats;->a()Lcom/google/api/services/drive/model/About$ImportFormats;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 928
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/About$ImportFormats;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/About$ImportFormats;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 928
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/About$ImportFormats;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/About$ImportFormats;

    move-result-object v0

    return-object v0
.end method
