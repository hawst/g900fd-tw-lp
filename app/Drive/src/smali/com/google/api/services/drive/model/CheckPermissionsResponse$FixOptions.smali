.class public final Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions;
.super LbeN;
.source "CheckPermissionsResponse.java"


# instance fields
.field private addCollaboratorsInfo:Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions$AddCollaboratorsInfo;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private allowedRoles:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private fixableFileIds:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private fixableRecipientEmailAddresses:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private fixesEverything:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private increaseDomainVisibilityInfo:Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions$IncreaseDomainVisibilityInfo;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private optionType:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 123
    invoke-direct {p0}, LbeN;-><init>()V

    .line 369
    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions;
    .locals 1

    .prologue
    .line 315
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions;
    .locals 1

    .prologue
    .line 310
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions;->a()Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions;->a()Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions;->a()Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/CheckPermissionsResponse$FixOptions;

    move-result-object v0

    return-object v0
.end method
