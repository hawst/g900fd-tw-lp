.class public Lcom/google/api/services/drive/Drive$About$Get;
.super Lcom/google/api/services/drive/DriveRequest;
.source "Drive.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/api/services/drive/DriveRequest",
        "<",
        "Lcom/google/api/services/drive/model/About;",
        ">;"
    }
.end annotation


# static fields
.field private static final REST_PATH:Ljava/lang/String; = "about"


# instance fields
.field private includeSubscribed:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private maxChangeIdCount:Ljava/lang/Long;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private startChangeId:Ljava/lang/Long;
    .annotation runtime LbfE;
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/api/services/drive/Drive$About;


# direct methods
.method protected constructor <init>(Lcom/google/api/services/drive/Drive$About;)V
    .locals 6

    .prologue
    .line 169
    iput-object p1, p0, Lcom/google/api/services/drive/Drive$About$Get;->this$1:Lcom/google/api/services/drive/Drive$About;

    .line 170
    iget-object v1, p1, Lcom/google/api/services/drive/Drive$About;->this$0:Lcom/google/api/services/drive/Drive;

    const-string v2, "GET"

    const-string v3, "about"

    const/4 v4, 0x0

    const-class v5, Lcom/google/api/services/drive/model/About;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/api/services/drive/DriveRequest;-><init>(Lcom/google/api/services/drive/Drive;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)V

    .line 171
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Long;)Lcom/google/api/services/drive/Drive$About$Get;
    .locals 0

    .prologue
    .line 287
    iput-object p1, p0, Lcom/google/api/services/drive/Drive$About$Get;->maxChangeIdCount:Ljava/lang/Long;

    .line 288
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$About$Get;
    .locals 1

    .prologue
    .line 200
    invoke-super {p0, p1}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$About$Get;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$About$Get;
    .locals 1

    .prologue
    .line 309
    invoke-super {p0, p1, p2}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$About$Get;

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$About$Get;->a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$About$Get;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$About$Get;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$About$Get;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Long;)Lcom/google/api/services/drive/Drive$About$Get;
    .locals 0

    .prologue
    .line 303
    iput-object p1, p0, Lcom/google/api/services/drive/Drive$About$Get;->startChangeId:Ljava/lang/Long;

    .line 304
    return-object p0
.end method

.method public buildHttpRequestUsingHead()Lbei;
    .locals 1

    .prologue
    .line 180
    invoke-super {p0}, Lcom/google/api/services/drive/DriveRequest;->buildHttpRequestUsingHead()Lbei;

    move-result-object v0

    return-object v0
.end method

.method public executeUsingHead()Lbel;
    .locals 1

    .prologue
    .line 175
    invoke-super {p0}, Lcom/google/api/services/drive/DriveRequest;->executeUsingHead()Lbel;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdJ;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$About$Get;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$About$Get;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdP;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$About$Get;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$About$Get;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$About$Get;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$About$Get;

    move-result-object v0

    return-object v0
.end method
