.class public final Lcom/google/api/services/drive/model/About$MaxUploadSizes;
.super LbeN;
.source "About.java"


# instance fields
.field private size:Ljava/lang/Long;
    .annotation runtime LbeW;
    .end annotation

    .annotation runtime LbfE;
    .end annotation
.end field

.field private type:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 993
    invoke-direct {p0}, LbeN;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/drive/model/About$MaxUploadSizes;
    .locals 1

    .prologue
    .line 1050
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/About$MaxUploadSizes;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/About$MaxUploadSizes;
    .locals 1

    .prologue
    .line 1045
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/About$MaxUploadSizes;

    return-object v0
.end method

.method public a()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 1014
    iget-object v0, p0, Lcom/google/api/services/drive/model/About$MaxUploadSizes;->size:Ljava/lang/Long;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1031
    iget-object v0, p0, Lcom/google/api/services/drive/model/About$MaxUploadSizes;->type:Ljava/lang/String;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 993
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/About$MaxUploadSizes;->a()Lcom/google/api/services/drive/model/About$MaxUploadSizes;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 993
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/About$MaxUploadSizes;->a()Lcom/google/api/services/drive/model/About$MaxUploadSizes;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 993
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/About$MaxUploadSizes;->a()Lcom/google/api/services/drive/model/About$MaxUploadSizes;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 993
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/About$MaxUploadSizes;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/About$MaxUploadSizes;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 993
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/About$MaxUploadSizes;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/About$MaxUploadSizes;

    move-result-object v0

    return-object v0
.end method
