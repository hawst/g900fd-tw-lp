.class public Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;
.super Lcom/google/api/services/appsactivity/AppsactivityRequest;
.source "Appsactivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/api/services/appsactivity/AppsactivityRequest",
        "<",
        "Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;",
        ">;"
    }
.end annotation


# static fields
.field private static final REST_PATH:Ljava/lang/String; = "activities"


# instance fields
.field private driveAncestorId:Ljava/lang/String;
    .annotation runtime LbfE;
        a = "drive.ancestorId"
    .end annotation
.end field

.field private driveFileId:Ljava/lang/String;
    .annotation runtime LbfE;
        a = "drive.fileId"
    .end annotation
.end field

.field private groupingStrategy:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private pageSize:Ljava/lang/Integer;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private pageToken:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private source:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/api/services/appsactivity/Appsactivity$Activities;

.field private userId:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/google/api/services/appsactivity/Appsactivity$Activities;)V
    .locals 6

    .prologue
    .line 176
    iput-object p1, p0, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->this$1:Lcom/google/api/services/appsactivity/Appsactivity$Activities;

    .line 177
    iget-object v1, p1, Lcom/google/api/services/appsactivity/Appsactivity$Activities;->this$0:Lcom/google/api/services/appsactivity/Appsactivity;

    const-string v2, "GET"

    const-string v3, "activities"

    const/4 v4, 0x0

    const-class v5, Lcom/google/api/services/appsactivity/model/ListActivitiesResponse;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/api/services/appsactivity/AppsactivityRequest;-><init>(Lcom/google/api/services/appsactivity/Appsactivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)V

    .line 178
    return-void
.end method


# virtual methods
.method public buildHttpRequestUsingHead()Lbei;
    .locals 1

    .prologue
    .line 187
    invoke-super {p0}, Lcom/google/api/services/appsactivity/AppsactivityRequest;->buildHttpRequestUsingHead()Lbei;

    move-result-object v0

    return-object v0
.end method

.method public executeUsingHead()Lbel;
    .locals 1

    .prologue
    .line 182
    invoke-super {p0}, Lcom/google/api/services/appsactivity/AppsactivityRequest;->executeUsingHead()Lbel;

    move-result-object v0

    return-object v0
.end method

.method public getDriveAncestorId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->driveAncestorId:Ljava/lang/String;

    return-object v0
.end method

.method public getDriveFileId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->driveFileId:Ljava/lang/String;

    return-object v0
.end method

.method public getGroupingStrategy()Ljava/lang/String;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->groupingStrategy:Ljava/lang/String;

    return-object v0
.end method

.method public getPageSize()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->pageSize:Ljava/lang/Integer;

    return-object v0
.end method

.method public getPageToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->pageToken:Ljava/lang/String;

    return-object v0
.end method

.method public getSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->source:Ljava/lang/String;

    return-object v0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->userId:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdJ;
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdP;
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;
    .locals 1

    .prologue
    .line 367
    invoke-super {p0, p1, p2}, Lcom/google/api/services/appsactivity/AppsactivityRequest;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/AppsactivityRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/AppsactivityRequest;
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    move-result-object v0

    return-object v0
.end method

.method public setAlt(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;
    .locals 1

    .prologue
    .line 192
    invoke-super {p0, p1}, Lcom/google/api/services/appsactivity/AppsactivityRequest;->setAlt(Ljava/lang/String;)Lcom/google/api/services/appsactivity/AppsactivityRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    return-object v0
.end method

.method public bridge synthetic setAlt(Ljava/lang/String;)Lcom/google/api/services/appsactivity/AppsactivityRequest;
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->setAlt(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    move-result-object v0

    return-object v0
.end method

.method public setDriveAncestorId(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->driveAncestorId:Ljava/lang/String;

    .line 238
    return-object p0
.end method

.method public setDriveFileId(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;
    .locals 0

    .prologue
    .line 253
    iput-object p1, p0, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->driveFileId:Ljava/lang/String;

    .line 254
    return-object p0
.end method

.method public setFields(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;
    .locals 1

    .prologue
    .line 197
    invoke-super {p0, p1}, Lcom/google/api/services/appsactivity/AppsactivityRequest;->setFields(Ljava/lang/String;)Lcom/google/api/services/appsactivity/AppsactivityRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    return-object v0
.end method

.method public bridge synthetic setFields(Ljava/lang/String;)Lcom/google/api/services/appsactivity/AppsactivityRequest;
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->setFields(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    move-result-object v0

    return-object v0
.end method

.method public setGroupingStrategy(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;
    .locals 0

    .prologue
    .line 276
    iput-object p1, p0, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->groupingStrategy:Ljava/lang/String;

    .line 277
    return-object p0
.end method

.method public setKey(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;
    .locals 1

    .prologue
    .line 202
    invoke-super {p0, p1}, Lcom/google/api/services/appsactivity/AppsactivityRequest;->setKey(Ljava/lang/String;)Lcom/google/api/services/appsactivity/AppsactivityRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    return-object v0
.end method

.method public bridge synthetic setKey(Ljava/lang/String;)Lcom/google/api/services/appsactivity/AppsactivityRequest;
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->setKey(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    move-result-object v0

    return-object v0
.end method

.method public setOauthToken(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;
    .locals 1

    .prologue
    .line 207
    invoke-super {p0, p1}, Lcom/google/api/services/appsactivity/AppsactivityRequest;->setOauthToken(Ljava/lang/String;)Lcom/google/api/services/appsactivity/AppsactivityRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    return-object v0
.end method

.method public bridge synthetic setOauthToken(Ljava/lang/String;)Lcom/google/api/services/appsactivity/AppsactivityRequest;
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->setOauthToken(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    move-result-object v0

    return-object v0
.end method

.method public setPageSize(Ljava/lang/Integer;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->pageSize:Ljava/lang/Integer;

    .line 300
    return-object p0
.end method

.method public setPageToken(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->pageToken:Ljava/lang/String;

    .line 316
    return-object p0
.end method

.method public setPrettyPrint(Ljava/lang/Boolean;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;
    .locals 1

    .prologue
    .line 212
    invoke-super {p0, p1}, Lcom/google/api/services/appsactivity/AppsactivityRequest;->setPrettyPrint(Ljava/lang/Boolean;)Lcom/google/api/services/appsactivity/AppsactivityRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    return-object v0
.end method

.method public bridge synthetic setPrettyPrint(Ljava/lang/Boolean;)Lcom/google/api/services/appsactivity/AppsactivityRequest;
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->setPrettyPrint(Ljava/lang/Boolean;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    move-result-object v0

    return-object v0
.end method

.method public setQuotaUser(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;
    .locals 1

    .prologue
    .line 217
    invoke-super {p0, p1}, Lcom/google/api/services/appsactivity/AppsactivityRequest;->setQuotaUser(Ljava/lang/String;)Lcom/google/api/services/appsactivity/AppsactivityRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    return-object v0
.end method

.method public bridge synthetic setQuotaUser(Ljava/lang/String;)Lcom/google/api/services/appsactivity/AppsactivityRequest;
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->setQuotaUser(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    move-result-object v0

    return-object v0
.end method

.method public setSource(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;
    .locals 0

    .prologue
    .line 338
    iput-object p1, p0, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->source:Ljava/lang/String;

    .line 339
    return-object p0
.end method

.method public setUserId(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;
    .locals 0

    .prologue
    .line 361
    iput-object p1, p0, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->userId:Ljava/lang/String;

    .line 362
    return-object p0
.end method

.method public setUserIp(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;
    .locals 1

    .prologue
    .line 222
    invoke-super {p0, p1}, Lcom/google/api/services/appsactivity/AppsactivityRequest;->setUserIp(Ljava/lang/String;)Lcom/google/api/services/appsactivity/AppsactivityRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    return-object v0
.end method

.method public bridge synthetic setUserIp(Ljava/lang/String;)Lcom/google/api/services/appsactivity/AppsactivityRequest;
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;->setUserIp(Ljava/lang/String;)Lcom/google/api/services/appsactivity/Appsactivity$Activities$List;

    move-result-object v0

    return-object v0
.end method
