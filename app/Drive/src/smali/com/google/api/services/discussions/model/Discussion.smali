.class public final Lcom/google/api/services/discussions/model/Discussion;
.super LbeN;
.source "Discussion.java"


# instance fields
.field private actor:Lcom/google/api/services/discussions/model/Author;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private dirty:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private id:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private kind:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private labels:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private object__:Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;
    .annotation runtime LbfE;
        a = "object"
    .end annotation
.end field

.field private published:Lbfx;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private target:Lcom/google/api/services/discussions/model/Discussion$Target;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private updated:Lbfx;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private verb:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, LbeN;-><init>()V

    .line 597
    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/discussions/model/Discussion;
    .locals 1

    .prologue
    .line 276
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/model/Discussion;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/model/Discussion;
    .locals 1

    .prologue
    .line 271
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/model/Discussion;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/discussions/model/Discussion;->a()Lcom/google/api/services/discussions/model/Discussion;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/discussions/model/Discussion;->a()Lcom/google/api/services/discussions/model/Discussion;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/discussions/model/Discussion;->a()Lcom/google/api/services/discussions/model/Discussion;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/discussions/model/Discussion;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/model/Discussion;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/discussions/model/Discussion;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/model/Discussion;

    move-result-object v0

    return-object v0
.end method
