.class public final Lcom/google/api/services/drive/model/About;
.super LbeN;
.source "About.java"


# instance fields
.field private additionalRoleInfo:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/drive/model/About$AdditionalRoleInfo;",
            ">;"
        }
    .end annotation
.end field

.field private domain:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private domainSharingPolicy:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private etag:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private exportFormats:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/drive/model/About$ExportFormats;",
            ">;"
        }
    .end annotation
.end field

.field private features:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/drive/model/About$Features;",
            ">;"
        }
    .end annotation
.end field

.field private importFormats:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/drive/model/About$ImportFormats;",
            ">;"
        }
    .end annotation
.end field

.field private isCurrentAppInstalled:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private kind:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private languageCode:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private largestChangeId:Ljava/lang/Long;
    .annotation runtime LbeW;
    .end annotation

    .annotation runtime LbfE;
    .end annotation
.end field

.field private maxUploadSizes:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/drive/model/About$MaxUploadSizes;",
            ">;"
        }
    .end annotation
.end field

.field private name:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private permissionId:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private quotaBytesByService:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/drive/model/About$QuotaBytesByService;",
            ">;"
        }
    .end annotation
.end field

.field private quotaBytesTotal:Ljava/lang/Long;
    .annotation runtime LbeW;
    .end annotation

    .annotation runtime LbfE;
    .end annotation
.end field

.field private quotaBytesUsed:Ljava/lang/Long;
    .annotation runtime LbeW;
    .end annotation

    .annotation runtime LbfE;
    .end annotation
.end field

.field private quotaBytesUsedAggregate:Ljava/lang/Long;
    .annotation runtime LbeW;
    .end annotation

    .annotation runtime LbfE;
    .end annotation
.end field

.field private quotaBytesUsedInTrash:Ljava/lang/Long;
    .annotation runtime LbeW;
    .end annotation

    .annotation runtime LbfE;
    .end annotation
.end field

.field private quotaType:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private remainingChangeIds:Ljava/lang/Long;
    .annotation runtime LbeW;
    .end annotation

    .annotation runtime LbfE;
    .end annotation
.end field

.field private rootFolderId:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private selfLink:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private user:Lcom/google/api/services/drive/model/User;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/google/api/services/drive/model/About$AdditionalRoleInfo;

    invoke-static {v0}, Lbfs;->a(Ljava/lang/Class;)Ljava/lang/Object;

    .line 77
    const-class v0, Lcom/google/api/services/drive/model/About$ExportFormats;

    invoke-static {v0}, Lbfs;->a(Ljava/lang/Class;)Ljava/lang/Object;

    .line 90
    const-class v0, Lcom/google/api/services/drive/model/About$Features;

    invoke-static {v0}, Lbfs;->a(Ljava/lang/Class;)Ljava/lang/Object;

    .line 103
    const-class v0, Lcom/google/api/services/drive/model/About$ImportFormats;

    invoke-static {v0}, Lbfs;->a(Ljava/lang/Class;)Ljava/lang/Object;

    .line 145
    const-class v0, Lcom/google/api/services/drive/model/About$MaxUploadSizes;

    invoke-static {v0}, Lbfs;->a(Ljava/lang/Class;)Ljava/lang/Object;

    .line 172
    const-class v0, Lcom/google/api/services/drive/model/About$QuotaBytesByService;

    invoke-static {v0}, Lbfs;->a(Ljava/lang/Class;)Ljava/lang/Object;

    .line 173
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, LbeN;-><init>()V

    .line 1058
    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/drive/model/About;
    .locals 1

    .prologue
    .line 657
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/About;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/About;
    .locals 1

    .prologue
    .line 652
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/About;

    return-object v0
.end method

.method public a()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/google/api/services/drive/model/About;->largestChangeId:Ljava/lang/Long;

    return-object v0
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/drive/model/About$AdditionalRoleInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/api/services/drive/model/About;->additionalRoleInfo:Ljava/util/List;

    return-object v0
.end method

.method public b()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 502
    iget-object v0, p0, Lcom/google/api/services/drive/model/About;->quotaBytesTotal:Ljava/lang/Long;

    return-object v0
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/drive/model/About$Features;",
            ">;"
        }
    .end annotation

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/api/services/drive/model/About;->features:Ljava/util/List;

    return-object v0
.end method

.method public c()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 519
    iget-object v0, p0, Lcom/google/api/services/drive/model/About;->quotaBytesUsed:Ljava/lang/Long;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/drive/model/About$ImportFormats;",
            ">;"
        }
    .end annotation

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/api/services/drive/model/About;->importFormats:Ljava/util/List;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/About;->a()Lcom/google/api/services/drive/model/About;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/About;->a()Lcom/google/api/services/drive/model/About;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/About;->a()Lcom/google/api/services/drive/model/About;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 587
    iget-object v0, p0, Lcom/google/api/services/drive/model/About;->remainingChangeIds:Ljava/lang/Long;

    return-object v0
.end method

.method public d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/drive/model/About$MaxUploadSizes;",
            ">;"
        }
    .end annotation

    .prologue
    .line 434
    iget-object v0, p0, Lcom/google/api/services/drive/model/About;->maxUploadSizes:Ljava/util/List;

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/About;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/About;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/About;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/About;

    move-result-object v0

    return-object v0
.end method
