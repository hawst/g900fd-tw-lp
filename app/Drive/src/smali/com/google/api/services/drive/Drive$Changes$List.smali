.class public Lcom/google/api/services/drive/Drive$Changes$List;
.super Lcom/google/api/services/drive/DriveRequest;
.source "Drive.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/api/services/drive/DriveRequest",
        "<",
        "Lcom/google/api/services/drive/model/ChangeList;",
        ">;"
    }
.end annotation


# static fields
.field private static final REST_PATH:Ljava/lang/String; = "changes"


# instance fields
.field private allProperties:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private appDataFilter:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private fileScopeAppIds:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private includeDeleted:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private includeSubscribed:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private maxResults:Ljava/lang/Integer;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private pageToken:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private sources:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private startChangeId:Ljava/lang/Long;
    .annotation runtime LbfE;
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/api/services/drive/Drive$Changes;


# virtual methods
.method public a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Changes$List;
    .locals 1

    .prologue
    .line 1353
    invoke-super {p0, p1}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Changes$List;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Changes$List;
    .locals 1

    .prologue
    .line 1627
    invoke-super {p0, p1, p2}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Changes$List;

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 1306
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Changes$List;->a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Changes$List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 1306
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Changes$List;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Changes$List;

    move-result-object v0

    return-object v0
.end method

.method public buildHttpRequestUsingHead()Lbei;
    .locals 1

    .prologue
    .line 1333
    invoke-super {p0}, Lcom/google/api/services/drive/DriveRequest;->buildHttpRequestUsingHead()Lbei;

    move-result-object v0

    return-object v0
.end method

.method public executeUsingHead()Lbel;
    .locals 1

    .prologue
    .line 1328
    invoke-super {p0}, Lcom/google/api/services/drive/DriveRequest;->executeUsingHead()Lbel;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdJ;
    .locals 1

    .prologue
    .line 1306
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Changes$List;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Changes$List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdP;
    .locals 1

    .prologue
    .line 1306
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Changes$List;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Changes$List;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 1306
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Changes$List;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Changes$List;

    move-result-object v0

    return-object v0
.end method
