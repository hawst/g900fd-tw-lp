.class public Lcom/google/api/services/discussions/Discussions$Posts$Insert;
.super Lcom/google/api/services/discussions/DiscussionsRequest;
.source "Discussions.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/api/services/discussions/DiscussionsRequest",
        "<",
        "Lcom/google/api/services/discussions/model/Post;",
        ">;"
    }
.end annotation


# static fields
.field private static final REST_PATH:Ljava/lang/String; = "targets/{targetId}/discussions/{discussionId}"


# instance fields
.field private discussionId:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private targetId:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/api/services/discussions/Discussions$Posts;


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/Discussions$Posts$Insert;
    .locals 1

    .prologue
    .line 1327
    invoke-super {p0, p1, p2}, Lcom/google/api/services/discussions/DiscussionsRequest;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/DiscussionsRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/Discussions$Posts$Insert;

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/DiscussionsRequest;
    .locals 1

    .prologue
    .line 1232
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/discussions/Discussions$Posts$Insert;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/Discussions$Posts$Insert;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdJ;
    .locals 1

    .prologue
    .line 1232
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/discussions/Discussions$Posts$Insert;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/Discussions$Posts$Insert;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdP;
    .locals 1

    .prologue
    .line 1232
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/discussions/Discussions$Posts$Insert;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/Discussions$Posts$Insert;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 1232
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/discussions/Discussions$Posts$Insert;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/Discussions$Posts$Insert;

    move-result-object v0

    return-object v0
.end method
