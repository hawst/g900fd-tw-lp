.class public final Lcom/google/api/services/drive/model/ArchiveExtractionJob;
.super LbeN;
.source "ArchiveExtractionJob.java"


# instance fields
.field private destinationLocationId:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private etag:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private id:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private kind:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private sourceFileId:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private status:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, LbeN;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/drive/model/ArchiveExtractionJob;
    .locals 1

    .prologue
    .line 186
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/ArchiveExtractionJob;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/ArchiveExtractionJob;
    .locals 1

    .prologue
    .line 181
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/ArchiveExtractionJob;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/ArchiveExtractionJob;->a()Lcom/google/api/services/drive/model/ArchiveExtractionJob;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/ArchiveExtractionJob;->a()Lcom/google/api/services/drive/model/ArchiveExtractionJob;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/ArchiveExtractionJob;->a()Lcom/google/api/services/drive/model/ArchiveExtractionJob;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/ArchiveExtractionJob;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/ArchiveExtractionJob;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/ArchiveExtractionJob;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/ArchiveExtractionJob;

    move-result-object v0

    return-object v0
.end method
