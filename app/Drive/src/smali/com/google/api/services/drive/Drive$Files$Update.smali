.class public Lcom/google/api/services/drive/Drive$Files$Update;
.super Lcom/google/api/services/drive/DriveRequest;
.source "Drive.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/api/services/drive/DriveRequest",
        "<",
        "Lcom/google/api/services/drive/model/File;",
        ">;"
    }
.end annotation


# static fields
.field private static final REST_PATH:Ljava/lang/String; = "files/{fileId}"


# instance fields
.field private addParents:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private baseRevision:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private convert:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private fileId:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private modifiedDateBehavior:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private newRevision:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private ocr:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private ocrLanguage:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private pinned:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private removeParents:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private setModifiedDate:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/api/services/drive/Drive$Files;

.field private timedTextLanguage:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private timedTextTrackName:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private updateViewedDate:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private useContentAsIndexableText:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field


# virtual methods
.method public a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$Update;
    .locals 1

    .prologue
    .line 7443
    invoke-super {p0, p1}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Files$Update;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Files$Update;
    .locals 1

    .prologue
    .line 7904
    invoke-super {p0, p1, p2}, Lcom/google/api/services/drive/DriveRequest;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/Drive$Files$Update;

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 7361
    invoke-virtual {p0, p1}, Lcom/google/api/services/drive/Drive$Files$Update;->a(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$Update;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/DriveRequest;
    .locals 1

    .prologue
    .line 7361
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Files$Update;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Files$Update;

    move-result-object v0

    return-object v0
.end method

.method public executeMedia()Lbel;
    .locals 1

    .prologue
    .line 7423
    invoke-super {p0}, Lcom/google/api/services/drive/DriveRequest;->executeMedia()Lbel;

    move-result-object v0

    return-object v0
.end method

.method public executeMediaAndDownloadTo(Ljava/io/OutputStream;)V
    .locals 0

    .prologue
    .line 7413
    invoke-super {p0, p1}, Lcom/google/api/services/drive/DriveRequest;->executeMediaAndDownloadTo(Ljava/io/OutputStream;)V

    .line 7414
    return-void
.end method

.method public executeMediaAsInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 7418
    invoke-super {p0}, Lcom/google/api/services/drive/DriveRequest;->executeMediaAsInputStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdJ;
    .locals 1

    .prologue
    .line 7361
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Files$Update;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Files$Update;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbdP;
    .locals 1

    .prologue
    .line 7361
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Files$Update;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Files$Update;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 7361
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/Drive$Files$Update;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/Drive$Files$Update;

    move-result-object v0

    return-object v0
.end method
