.class public Lcom/google/api/services/appsactivity/AppsactivityRequestInitializer;
.super LbdQ;
.source "AppsactivityRequestInitializer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, LbdQ;-><init>()V

    .line 84
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0, p1}, LbdQ;-><init>(Ljava/lang/String;)V

    .line 91
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, LbdQ;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    return-void
.end method


# virtual methods
.method protected initializeAppsactivityRequest(Lcom/google/api/services/appsactivity/AppsactivityRequest;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/services/appsactivity/AppsactivityRequest",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 118
    return-void
.end method

.method public final initializeJsonRequest(LbdP;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbdP",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 103
    invoke-super {p0, p1}, LbdQ;->initializeJsonRequest(LbdP;)V

    .line 104
    check-cast p1, Lcom/google/api/services/appsactivity/AppsactivityRequest;

    invoke-virtual {p0, p1}, Lcom/google/api/services/appsactivity/AppsactivityRequestInitializer;->initializeAppsactivityRequest(Lcom/google/api/services/appsactivity/AppsactivityRequest;)V

    .line 105
    return-void
.end method
