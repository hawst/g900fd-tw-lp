.class public final Lcom/google/api/services/appsactivity/model/Parent;
.super LbeN;
.source "Parent.java"


# instance fields
.field private id:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private isRoot:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private title:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, LbeN;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/Parent;->clone()Lcom/google/api/services/appsactivity/model/Parent;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/Parent;->clone()Lcom/google/api/services/appsactivity/model/Parent;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/api/services/appsactivity/model/Parent;
    .locals 1

    .prologue
    .line 113
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/Parent;

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/Parent;->clone()Lcom/google/api/services/appsactivity/model/Parent;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Parent;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getIsRoot()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Parent;->isRoot:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Parent;->title:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/model/Parent;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/Parent;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/model/Parent;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/Parent;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/Parent;
    .locals 1

    .prologue
    .line 108
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/Parent;

    return-object v0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/api/services/appsactivity/model/Parent;
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Parent;->id:Ljava/lang/String;

    .line 69
    return-object p0
.end method

.method public setIsRoot(Ljava/lang/Boolean;)Lcom/google/api/services/appsactivity/model/Parent;
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Parent;->isRoot:Ljava/lang/Boolean;

    .line 86
    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/api/services/appsactivity/model/Parent;
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Parent;->title:Ljava/lang/String;

    .line 103
    return-object p0
.end method
