.class public final Lcom/google/api/services/discussions/model/Discussion$Target;
.super LbeN;
.source "Discussion.java"


# instance fields
.field private id:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private title:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 597
    invoke-direct {p0}, LbeN;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/discussions/model/Discussion$Target;
    .locals 1

    .prologue
    .line 654
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/model/Discussion$Target;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/model/Discussion$Target;
    .locals 1

    .prologue
    .line 649
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/model/Discussion$Target;

    return-object v0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 597
    invoke-virtual {p0}, Lcom/google/api/services/discussions/model/Discussion$Target;->a()Lcom/google/api/services/discussions/model/Discussion$Target;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 597
    invoke-virtual {p0}, Lcom/google/api/services/discussions/model/Discussion$Target;->a()Lcom/google/api/services/discussions/model/Discussion$Target;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 597
    invoke-virtual {p0}, Lcom/google/api/services/discussions/model/Discussion$Target;->a()Lcom/google/api/services/discussions/model/Discussion$Target;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 597
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/discussions/model/Discussion$Target;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/model/Discussion$Target;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 597
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/discussions/model/Discussion$Target;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/discussions/model/Discussion$Target;

    move-result-object v0

    return-object v0
.end method
