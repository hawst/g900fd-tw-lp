.class public final Lcom/google/api/services/appsactivity/model/Move;
.super LbeN;
.source "Move.java"


# instance fields
.field private addedParents:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/appsactivity/model/Parent;",
            ">;"
        }
    .end annotation
.end field

.field private removedParents:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/appsactivity/model/Parent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, LbeN;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/Move;->clone()Lcom/google/api/services/appsactivity/model/Move;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/Move;->clone()Lcom/google/api/services/appsactivity/model/Move;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/api/services/appsactivity/model/Move;
    .locals 1

    .prologue
    .line 88
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/Move;

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/Move;->clone()Lcom/google/api/services/appsactivity/model/Move;

    move-result-object v0

    return-object v0
.end method

.method public getAddedParents()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/appsactivity/model/Parent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Move;->addedParents:Ljava/util/List;

    return-object v0
.end method

.method public getRemovedParents()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/appsactivity/model/Parent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Move;->removedParents:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/model/Move;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/Move;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/model/Move;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/Move;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/Move;
    .locals 1

    .prologue
    .line 83
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/Move;

    return-object v0
.end method

.method public setAddedParents(Ljava/util/List;)Lcom/google/api/services/appsactivity/model/Move;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/appsactivity/model/Parent;",
            ">;)",
            "Lcom/google/api/services/appsactivity/model/Move;"
        }
    .end annotation

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Move;->addedParents:Ljava/util/List;

    .line 61
    return-object p0
.end method

.method public setRemovedParents(Ljava/util/List;)Lcom/google/api/services/appsactivity/model/Move;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/appsactivity/model/Parent;",
            ">;)",
            "Lcom/google/api/services/appsactivity/model/Move;"
        }
    .end annotation

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Move;->removedParents:Ljava/util/List;

    .line 78
    return-object p0
.end method
