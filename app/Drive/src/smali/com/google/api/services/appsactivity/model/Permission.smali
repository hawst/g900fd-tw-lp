.class public final Lcom/google/api/services/appsactivity/model/Permission;
.super LbeN;
.source "Permission.java"


# instance fields
.field private name:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private permissionId:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private role:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private type:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private user:Lcom/google/api/services/appsactivity/model/User;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private withLink:Ljava/lang/Boolean;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, LbeN;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/Permission;->clone()Lcom/google/api/services/appsactivity/model/Permission;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/Permission;->clone()Lcom/google/api/services/appsactivity/model/Permission;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/api/services/appsactivity/model/Permission;
    .locals 1

    .prologue
    .line 192
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/Permission;

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/Permission;->clone()Lcom/google/api/services/appsactivity/model/Permission;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Permission;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPermissionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Permission;->permissionId:Ljava/lang/String;

    return-object v0
.end method

.method public getRole()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Permission;->role:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Permission;->type:Ljava/lang/String;

    return-object v0
.end method

.method public getUser()Lcom/google/api/services/appsactivity/model/User;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Permission;->user:Lcom/google/api/services/appsactivity/model/User;

    return-object v0
.end method

.method public getWithLink()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/Permission;->withLink:Ljava/lang/Boolean;

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/model/Permission;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/Permission;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/model/Permission;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/Permission;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/Permission;
    .locals 1

    .prologue
    .line 187
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/Permission;

    return-object v0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/api/services/appsactivity/model/Permission;
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Permission;->name:Ljava/lang/String;

    .line 93
    return-object p0
.end method

.method public setPermissionId(Ljava/lang/String;)Lcom/google/api/services/appsactivity/model/Permission;
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Permission;->permissionId:Ljava/lang/String;

    .line 112
    return-object p0
.end method

.method public setRole(Ljava/lang/String;)Lcom/google/api/services/appsactivity/model/Permission;
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Permission;->role:Ljava/lang/String;

    .line 131
    return-object p0
.end method

.method public setType(Ljava/lang/String;)Lcom/google/api/services/appsactivity/model/Permission;
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Permission;->type:Ljava/lang/String;

    .line 148
    return-object p0
.end method

.method public setUser(Lcom/google/api/services/appsactivity/model/User;)Lcom/google/api/services/appsactivity/model/Permission;
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Permission;->user:Lcom/google/api/services/appsactivity/model/User;

    .line 165
    return-object p0
.end method

.method public setWithLink(Ljava/lang/Boolean;)Lcom/google/api/services/appsactivity/model/Permission;
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/Permission;->withLink:Ljava/lang/Boolean;

    .line 182
    return-object p0
.end method
