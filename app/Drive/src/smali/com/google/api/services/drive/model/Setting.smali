.class public final Lcom/google/api/services/drive/model/Setting;
.super LbeN;
.source "Setting.java"


# instance fields
.field private etag:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private key:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private kind:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private namespace:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private selfLink:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field

.field private value:Ljava/lang/String;
    .annotation runtime LbfE;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, LbeN;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/drive/model/Setting;
    .locals 1

    .prologue
    .line 183
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/Setting;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/google/api/services/drive/model/Setting;
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/api/services/drive/model/Setting;->key:Ljava/lang/String;

    .line 105
    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/Setting;
    .locals 1

    .prologue
    .line 178
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/Setting;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/api/services/drive/model/Setting;->value:Ljava/lang/String;

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/google/api/services/drive/model/Setting;
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/google/api/services/drive/model/Setting;->value:Ljava/lang/String;

    .line 173
    return-object p0
.end method

.method public synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/Setting;->a()Lcom/google/api/services/drive/model/Setting;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/Setting;->a()Lcom/google/api/services/drive/model/Setting;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/api/services/drive/model/Setting;->a()Lcom/google/api/services/drive/model/Setting;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/Setting;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/Setting;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/drive/model/Setting;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/drive/model/Setting;

    move-result-object v0

    return-object v0
.end method
