.class public final Lcom/google/api/services/appsactivity/model/PermissionChange;
.super LbeN;
.source "PermissionChange.java"


# instance fields
.field private addedPermissions:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/appsactivity/model/Permission;",
            ">;"
        }
    .end annotation
.end field

.field private removedPermissions:Ljava/util/List;
    .annotation runtime LbfE;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/appsactivity/model/Permission;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/google/api/services/appsactivity/model/Permission;

    invoke-static {v0}, Lbfs;->a(Ljava/lang/Class;)Ljava/lang/Object;

    .line 57
    const-class v0, Lcom/google/api/services/appsactivity/model/Permission;

    invoke-static {v0}, Lbfs;->a(Ljava/lang/Class;)Ljava/lang/Object;

    .line 58
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, LbeN;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic clone()LbeN;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/PermissionChange;->clone()Lcom/google/api/services/appsactivity/model/PermissionChange;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lbfz;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/PermissionChange;->clone()Lcom/google/api/services/appsactivity/model/PermissionChange;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/api/services/appsactivity/model/PermissionChange;
    .locals 1

    .prologue
    .line 101
    invoke-super {p0}, LbeN;->clone()LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/PermissionChange;

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/google/api/services/appsactivity/model/PermissionChange;->clone()Lcom/google/api/services/appsactivity/model/PermissionChange;

    move-result-object v0

    return-object v0
.end method

.method public getAddedPermissions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/appsactivity/model/Permission;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/PermissionChange;->addedPermissions:Ljava/util/List;

    return-object v0
.end method

.method public getRemovedPermissions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/appsactivity/model/Permission;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/api/services/appsactivity/model/PermissionChange;->removedPermissions:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)LbeN;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/model/PermissionChange;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/PermissionChange;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;)Lbfz;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0, p1, p2}, Lcom/google/api/services/appsactivity/model/PermissionChange;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/PermissionChange;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/services/appsactivity/model/PermissionChange;
    .locals 1

    .prologue
    .line 96
    invoke-super {p0, p1, p2}, LbeN;->set(Ljava/lang/String;Ljava/lang/Object;)LbeN;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/appsactivity/model/PermissionChange;

    return-object v0
.end method

.method public setAddedPermissions(Ljava/util/List;)Lcom/google/api/services/appsactivity/model/PermissionChange;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/appsactivity/model/Permission;",
            ">;)",
            "Lcom/google/api/services/appsactivity/model/PermissionChange;"
        }
    .end annotation

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/PermissionChange;->addedPermissions:Ljava/util/List;

    .line 74
    return-object p0
.end method

.method public setRemovedPermissions(Ljava/util/List;)Lcom/google/api/services/appsactivity/model/PermissionChange;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/appsactivity/model/Permission;",
            ">;)",
            "Lcom/google/api/services/appsactivity/model/PermissionChange;"
        }
    .end annotation

    .prologue
    .line 90
    iput-object p1, p0, Lcom/google/api/services/appsactivity/model/PermissionChange;->removedPermissions:Ljava/util/List;

    .line 91
    return-object p0
.end method
