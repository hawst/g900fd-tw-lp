.class public interface abstract Lcom/google/inject/Binder;
.super Ljava/lang/Object;
.source "Binder.java"


# virtual methods
.method public abstract a(Lbuv;)LbuE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbuv",
            "<TT;>;)",
            "LbuE",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Class;)LbuE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LbuE",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract a(LbuP;)LbuQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LbuP",
            "<TT;>;)",
            "LbuQ",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Class;)LbuQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "LbuQ",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract a()LbuR;
.end method

.method public abstract a(Lbuv;)LbuT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbuv",
            "<TT;>;)",
            "LbuT",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Object;)Lcom/google/inject/Binder;
.end method

.method public varargs abstract a([Ljava/lang/Class;)Lcom/google/inject/Binder;
.end method

.method public abstract a(LbuC;)V
.end method

.method public abstract a(Lbwy;)V
.end method

.method public abstract a(Ljava/lang/Class;LbuH;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "LbuH;",
            ")V"
        }
    .end annotation
.end method

.method public varargs abstract a(Ljava/lang/String;[Ljava/lang/Object;)V
.end method
