.class LNO;
.super Ljava/lang/Object;
.source "TranscriptList.java"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LNP;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {}, LbnG;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LNO;->a:Ljava/util/List;

    .line 49
    return-void
.end method

.method static a(Ljava/io/InputStream;)LNO;
    .locals 2

    .prologue
    .line 35
    :try_start_0
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    .line 36
    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 37
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    .line 38
    invoke-static {v0}, LNO;->a(Lorg/xmlpull/v1/XmlPullParser;)LNO;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 40
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    throw v0
.end method

.method private static a(Lorg/xmlpull/v1/XmlPullParser;)LNO;
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 74
    new-instance v0, LNO;

    invoke-direct {v0}, LNO;-><init>()V

    .line 75
    const/4 v1, 0x0

    const-string v2, "transcript_list"

    invoke-interface {p0, v3, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 77
    :cond_0
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    .line 78
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 81
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 83
    const-string v2, "track"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 84
    invoke-direct {v0, p0}, LNO;->a(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 86
    :cond_1
    invoke-static {p0}, LNO;->b(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 89
    :cond_2
    return-object v0
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 64
    new-instance v0, LNP;

    invoke-direct {v0}, LNP;-><init>()V

    .line 65
    const-string v1, "lang_code"

    invoke-interface {p1, v2, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LNP;->c:Ljava/lang/String;

    .line 66
    const-string v1, "name"

    invoke-interface {p1, v2, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LNP;->a:Ljava/lang/String;

    .line 67
    const-string v1, "kind"

    invoke-interface {p1, v2, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LNP;->b:Ljava/lang/String;

    .line 68
    const-string v1, "lang_default"

    invoke-interface {p1, v2, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, LNP;->a:Z

    .line 69
    iget-object v1, p0, LNO;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    return-void
.end method

.method private static b(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 2

    .prologue
    .line 93
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 94
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 96
    :cond_0
    const/4 v0, 0x1

    .line 97
    :goto_0
    if-eqz v0, :cond_1

    .line 98
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 103
    :pswitch_0
    add-int/lit8 v0, v0, 0x1

    .line 104
    goto :goto_0

    .line 100
    :pswitch_1
    add-int/lit8 v0, v0, -0x1

    .line 101
    goto :goto_0

    .line 109
    :cond_1
    return-void

    .line 98
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a()LNP;
    .locals 3

    .prologue
    .line 55
    iget-object v0, p0, LNO;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNP;

    .line 56
    iget-boolean v2, v0, LNP;->a:Z

    if-eqz v2, :cond_0

    .line 60
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LNO;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, LNO;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNP;

    goto :goto_0
.end method
