.class LWW;
.super Ljava/lang/Object;
.source "DisplayManagerHelperImpl.java"

# interfaces
.implements Landroid/hardware/display/DisplayManager$DisplayListener;


# instance fields
.field final synthetic a:LWV;


# direct methods
.method constructor <init>(LWV;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, LWW;->a:LWV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDisplayAdded(I)V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, LWW;->a:LWV;

    invoke-static {v0}, LWV;->a(LWV;)LWT;

    move-result-object v0

    invoke-interface {v0, p1}, LWT;->a(I)V

    .line 31
    return-void
.end method

.method public onDisplayChanged(I)V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, LWW;->a:LWV;

    invoke-static {v0}, LWV;->a(LWV;)LWT;

    move-result-object v0

    invoke-interface {v0, p1}, LWT;->c(I)V

    .line 36
    return-void
.end method

.method public onDisplayRemoved(I)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, LWW;->a:LWV;

    invoke-static {v0}, LWV;->a(LWV;)LWT;

    move-result-object v0

    invoke-interface {v0, p1}, LWT;->b(I)V

    .line 41
    return-void
.end method
