.class public Lakx;
.super Ljava/lang/Object;
.source "CancelableTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:LbsJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbsJ",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lakv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lakv",
            "<-TT;>;"
        }
    .end annotation
.end field

.field public final a:LbsU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LbsU",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Laky;

    invoke-direct {v0}, Laky;-><init>()V

    sput-object v0, Lakx;->a:LbsJ;

    return-void
.end method

.method public constructor <init>(LbsU;LbsJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LbsU",
            "<TT;>;",
            "LbsJ",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbsU;

    iput-object v0, p0, Lakx;->a:LbsU;

    .line 46
    invoke-static {p2}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    invoke-static {p2}, Lakv;->a(LbsJ;)Lakv;

    move-result-object v0

    iput-object v0, p0, Lakx;->a:Lakv;

    .line 48
    return-void
.end method

.method public static a()Lakx;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lakx",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 41
    new-instance v0, Lakx;

    const/4 v1, 0x0

    invoke-static {v1}, LbsK;->a(Ljava/lang/Object;)LbsU;

    move-result-object v1

    sget-object v2, Lakx;->a:LbsJ;

    invoke-direct {v0, v1, v2}, Lakx;-><init>(LbsU;LbsJ;)V

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lakx;->a:LbsU;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LbsU;->cancel(Z)Z

    .line 64
    iget-object v0, p0, Lakx;->a:Lakv;

    invoke-virtual {v0}, Lakv;->a()V

    .line 65
    return-void
.end method

.method public a(Ljava/util/concurrent/Executor;)V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lakx;->a:LbsU;

    iget-object v1, p0, Lakx;->a:Lakv;

    invoke-static {v0, v1, p1}, LbsK;->a(LbsU;LbsJ;Ljava/util/concurrent/Executor;)V

    .line 73
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lakx;->a:Lakv;

    invoke-virtual {v0}, Lakv;->a()Z

    move-result v0

    return v0
.end method
