.class public interface abstract LaGM;
.super Ljava/lang/Object;
.source "ModelLoader.java"


# virtual methods
.method public abstract a(J)LaFM;
.end method

.method public abstract a(LaFO;)LaFM;
.end method

.method public abstract a(LaFM;)LaFP;
.end method

.method public abstract a(LaFO;)LaFQ;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFV;
.end method

.method public abstract a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;LIK;[Ljava/lang/String;)LaGA;
.end method

.method public abstract a(Lcom/google/android/apps/docs/app/model/navigation/CriterionSet;LIK;[Ljava/lang/String;Ljava/lang/String;)LaGA;
.end method

.method public abstract a(LaFM;Ljava/lang/String;J)LaGW;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGo;
.end method

.method public abstract a(J)LaGp;
.end method

.method public abstract a(LaGo;LacY;)LaGp;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGu;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGu;
.end method

.method public abstract a()LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LbmY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ")",
            "LbmY",
            "<",
            "Lcom/google/android/gms/drive/database/data/EntrySpec;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(LaFM;)Lcom/google/android/gms/drive/database/data/EntrySpec;
.end method

.method public abstract a(LaFO;)Lcom/google/android/gms/drive/database/data/EntrySpec;
.end method

.method public abstract a(LaFO;Ljava/lang/String;)Lcom/google/android/gms/drive/database/data/EntrySpec;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)Lcom/google/android/gms/drive/database/data/EntrySpec;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcom/google/android/gms/drive/database/data/ResourceSpec;
.end method

.method public abstract a(LaGA;)Ljava/lang/String;
.end method

.method public abstract a()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LaFO;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(LaFM;)V
.end method

.method public abstract b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaFV;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract b(LaGp;)LaGo;
.end method

.method public abstract b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGo;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract b(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGu;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract b(Lcom/google/android/gms/drive/database/data/ResourceSpec;)LaGu;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract b(LaFM;)V
.end method

.method public abstract b(LaFM;)Z
.end method

.method public abstract b(LaGo;LacY;)Z
.end method

.method public abstract e()V
.end method
