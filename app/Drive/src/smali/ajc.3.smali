.class public Lajc;
.super Lbuo;
.source "ContextScopedProviderModule.java"


# instance fields
.field private final a:LbuC;

.field private final a:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<+",
            "LbuC;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/logging/Logger;

.field private final b:LbuC;


# direct methods
.method public constructor <init>(Ljava/lang/Iterable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "LbuC;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0}, Lbuo;-><init>()V

    .line 14
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    iput-object v0, p0, Lajc;->a:Ljava/util/logging/Logger;

    .line 20
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    iput-object v0, p0, Lajc;->a:Ljava/lang/Iterable;

    .line 21
    invoke-direct {p0}, Lajc;->a()LbuC;

    move-result-object v0

    iput-object v0, p0, Lajc;->a:LbuC;

    .line 22
    iget-object v0, p0, Lajc;->a:LbuC;

    if-nez v0, :cond_0

    .line 23
    new-instance v0, Laje;

    invoke-direct {v0, p1}, Laje;-><init>(Ljava/lang/Iterable;)V

    iput-object v0, p0, Lajc;->b:LbuC;

    .line 27
    :goto_0
    return-void

    .line 25
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lajc;->b:LbuC;

    goto :goto_0
.end method

.method private a()LbuC;
    .locals 5

    .prologue
    .line 31
    :try_start_0
    const-string v0, "ajf"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuC;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    :goto_0
    return-object v0

    .line 35
    :catch_0
    move-exception v0

    .line 36
    iget-object v1, p0, Lajc;->a:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cannot find generated module. Falling back. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 37
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lajc;->a:LbuC;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lajc;->a:LbuC;

    invoke-virtual {p0, v0}, Lajc;->a(LbuC;)V

    .line 45
    iget-object v0, p0, Lajc;->a:Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LbuC;

    .line 46
    invoke-virtual {p0, v0}, Lajc;->a(LbuC;)V

    goto :goto_0

    .line 49
    :cond_0
    iget-object v0, p0, Lajc;->b:LbuC;

    invoke-virtual {p0, v0}, Lajc;->a(LbuC;)V

    .line 51
    :cond_1
    return-void
.end method
