.class public LaFR;
.super LaGm;
.source "AppCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGm",
        "<",
        "LaEj;",
        "LaEz;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final a:Ljava/lang/String;

.field private final a:Ljava/util/Date;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(LaEz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 75
    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, v2

    move-object v5, v2

    move-object v6, p3

    move-object v7, p4

    move v8, p5

    invoke-direct/range {v0 .. v8}, LaFR;-><init>(LaEz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;I)V

    .line 76
    return-void
.end method

.method public constructor <init>(LaEz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 54
    invoke-static {}, LaEj;->a()LaEj;

    move-result-object v0

    sget-object v1, LaEG;->d:LaEG;

    invoke-virtual {v1}, LaEG;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, LaGm;-><init>(LaFm;LaFy;Landroid/net/Uri;)V

    .line 55
    iput-object p2, p0, LaFR;->a:Ljava/lang/String;

    .line 56
    iput-object p3, p0, LaFR;->b:Ljava/lang/String;

    .line 57
    iput-object p4, p0, LaFR;->c:Ljava/lang/String;

    .line 58
    iput-object p5, p0, LaFR;->a:Ljava/util/Date;

    .line 59
    iput-object p6, p0, LaFR;->d:Ljava/lang/String;

    .line 60
    iput-object p7, p0, LaFR;->e:Ljava/lang/String;

    .line 61
    iput p8, p0, LaFR;->a:I

    .line 62
    return-void
.end method

.method public static a(LaEz;Landroid/database/Cursor;)LaFR;
    .locals 9

    .prologue
    .line 116
    sget-object v0, LaEk;->a:LaEk;

    invoke-virtual {v0}, LaEk;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    .line 117
    sget-object v0, LaEk;->b:LaEk;

    invoke-virtual {v0}, LaEk;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    .line 118
    sget-object v0, LaEk;->c:LaEk;

    invoke-virtual {v0}, LaEk;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    .line 119
    sget-object v0, LaEk;->d:LaEk;

    invoke-virtual {v0}, LaEk;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    .line 120
    if-nez v0, :cond_0

    const/4 v5, 0x0

    .line 121
    :goto_0
    sget-object v0, LaEk;->e:LaEk;

    invoke-virtual {v0}, LaEk;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v6

    .line 122
    sget-object v0, LaEk;->f:LaEk;

    invoke-virtual {v0}, LaEk;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v7

    .line 123
    sget-object v0, LaEk;->g:LaEk;

    .line 124
    invoke-virtual {v0}, LaEk;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0, p1}, LaFr;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lbsy;->a(J)I

    move-result v8

    .line 125
    new-instance v0, LaFR;

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, LaFR;-><init>(LaEz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;I)V

    .line 133
    invoke-static {}, LaEj;->a()LaEj;

    move-result-object v1

    invoke-virtual {v1}, LaEj;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, LaFr;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LaFR;->c(J)V

    .line 134
    return-object v0

    .line 120
    :cond_0
    new-instance v5, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {v5, v0, v1}, Ljava/util/Date;-><init>(J)V

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, LaFR;->a:I

    return v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, LaFR;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()Ljava/util/Date;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, LaFR;->a:Ljava/util/Date;

    return-object v0
.end method

.method protected a(Landroid/content/ContentValues;)V
    .locals 2

    .prologue
    .line 140
    invoke-virtual {p0}, LaFR;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "App version should not be null."

    invoke-static {v0, v1}, LbiT;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v0, LaEk;->a:LaEk;

    invoke-virtual {v0}, LaEk;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaFR;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    sget-object v0, LaEk;->b:LaEk;

    invoke-virtual {v0}, LaEk;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaFR;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    sget-object v0, LaEk;->c:LaEk;

    invoke-virtual {v0}, LaEk;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaFR;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    invoke-virtual {p0}, LaFR;->a()Ljava/util/Date;

    move-result-object v0

    .line 146
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 147
    :goto_0
    sget-object v1, LaEk;->d:LaEk;

    invoke-virtual {v1}, LaEk;->a()LaFr;

    move-result-object v1

    invoke-virtual {v1}, LaFr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 148
    sget-object v0, LaEk;->e:LaEk;

    invoke-virtual {v0}, LaEk;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaFR;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    sget-object v0, LaEk;->f:LaEk;

    invoke-virtual {v0}, LaEk;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaFR;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    sget-object v0, LaEk;->g:LaEk;

    invoke-virtual {v0}, LaEk;->a()LaFr;

    move-result-object v0

    invoke-virtual {v0}, LaFr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaFR;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 151
    return-void

    .line 146
    :cond_0
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, LaFR;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, LaFR;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, LaFR;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, LaFR;->e:Ljava/lang/String;

    return-object v0
.end method
