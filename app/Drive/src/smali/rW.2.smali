.class public LrW;
.super LaGN;
.source "CreateNewDocActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LaGN",
        "<",
        "Lcom/google/android/gms/drive/database/data/ResourceSpec;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Intent;

.field final synthetic a:Lcom/google/android/apps/docs/app/CreateNewDocActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/app/CreateNewDocActivity;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, LrW;->a:Lcom/google/android/apps/docs/app/CreateNewDocActivity;

    iput-object p2, p0, LrW;->a:Landroid/content/Intent;

    invoke-direct {p0}, LaGN;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LaGM;)Lcom/google/android/gms/drive/database/data/ResourceSpec;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, LrW;->a:Lcom/google/android/apps/docs/app/CreateNewDocActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a()Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(LaGM;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 169
    invoke-virtual {p0, p1}, LrW;->a(LaGM;)Lcom/google/android/gms/drive/database/data/ResourceSpec;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 178
    const-string v0, "CreateNewDocActivity"

    const-string v1, "Redirecting creation of the document to: %s"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LrW;->a:Landroid/content/Intent;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LalV;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 179
    if-eqz p1, :cond_0

    .line 180
    iget-object v0, p0, LrW;->a:Landroid/content/Intent;

    const-string v1, "collectionResourceId"

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 182
    :cond_0
    iget-object v0, p0, LrW;->a:Lcom/google/android/apps/docs/app/CreateNewDocActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:Luc;

    iget-object v1, p0, LrW;->a:Landroid/content/Intent;

    invoke-interface {v0, v1, v5}, Luc;->a(Landroid/content/Intent;I)V

    .line 184
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 169
    check-cast p1, Lcom/google/android/gms/drive/database/data/ResourceSpec;

    invoke-virtual {p0, p1}, LrW;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;)V

    return-void
.end method
