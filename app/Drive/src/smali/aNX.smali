.class public final LaNX;
.super Ljava/lang/Object;

# interfaces
.implements LayC;


# instance fields
.field private final a:I

.field private final a:Ljava/util/Date;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Z

.field private final b:I


# direct methods
.method public constructor <init>(Ljava/util/Date;ILjava/util/Set;ZI)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            "I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;ZI)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LaNX;->a:Ljava/util/Date;

    iput p2, p0, LaNX;->a:I

    iput-object p3, p0, LaNX;->a:Ljava/util/Set;

    iput-boolean p4, p0, LaNX;->a:Z

    iput p5, p0, LaNX;->b:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, LaNX;->a:I

    return v0
.end method

.method public a()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, LaNX;->a:Ljava/util/Date;

    return-object v0
.end method

.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, LaNX;->a:Ljava/util/Set;

    return-object v0
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, LaNX;->a:Z

    return v0
.end method
