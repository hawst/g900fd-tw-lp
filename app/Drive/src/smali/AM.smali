.class final LAM;
.super LAL;
.source "DocListAdapterState.java"


# instance fields
.field final synthetic b:LAJ;


# direct methods
.method private constructor <init>(LAJ;)V
    .locals 1

    .prologue
    .line 400
    iput-object p1, p0, LAM;->b:LAJ;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LAL;-><init>(LAJ;LAK;)V

    return-void
.end method

.method synthetic constructor <init>(LAJ;LAK;)V
    .locals 0

    .prologue
    .line 400
    invoke-direct {p0, p1}, LAM;-><init>(LAJ;)V

    return-void
.end method

.method private a(LCA;)V
    .locals 6

    .prologue
    .line 417
    iget-object v0, p0, LAM;->b:LAJ;

    invoke-static {v0}, LAJ;->a(LAJ;)LCv;

    move-result-object v0

    invoke-interface {v0}, LCv;->b()Ljava/lang/String;

    move-result-object v0

    .line 418
    iget-object v1, p0, LAM;->b:LAJ;

    invoke-static {v1}, LAJ;->a(LAJ;)LabF;

    move-result-object v1

    sget-object v2, Lqx;->a:Lqx;

    invoke-interface {v1, v0, v2}, LabF;->a(Ljava/lang/String;Lqx;)LabD;

    move-result-object v0

    .line 419
    iget-object v1, p0, LAM;->b:LAJ;

    invoke-static {v1}, LAJ;->a(LAJ;)LabI;

    move-result-object v1

    iget-object v2, p1, LCA;->a:Landroid/widget/ImageView;

    invoke-interface {v0}, LabD;->a()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, LabI;->a(Landroid/widget/ImageView;J)V

    .line 420
    return-void
.end method

.method private b(LCA;)V
    .locals 2

    .prologue
    .line 423
    iget-object v0, p0, LAM;->b:LAJ;

    invoke-static {v0}, LAJ;->a(LAJ;)LCv;

    move-result-object v0

    invoke-interface {v0}, LCv;->b()Ljava/lang/String;

    move-result-object v0

    .line 424
    iget-object v1, p1, LCA;->d:Lcom/google/android/apps/docs/view/FixedSizeTextView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/view/FixedSizeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 425
    return-void
.end method

.method private c(LCA;)V
    .locals 5

    .prologue
    .line 428
    iget-object v0, p0, LAM;->b:LAJ;

    invoke-static {v0}, LAJ;->a(LAJ;)LIf;

    move-result-object v0

    iget-object v1, p0, LAM;->b:LAJ;

    invoke-static {v1}, LAJ;->a(LAJ;)LCv;

    move-result-object v1

    invoke-virtual {v0, v1}, LIf;->a(LCv;)Ljava/lang/Long;

    move-result-object v0

    .line 429
    if-nez v0, :cond_0

    .line 430
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 432
    :cond_0
    iget-object v1, p0, LAM;->b:LAJ;

    invoke-static {v1}, LAJ;->a(LAJ;)LamF;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LamF;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 433
    iget-object v1, p0, LAM;->b:LAJ;

    invoke-static {v1}, LAJ;->a(LAJ;)LIK;

    move-result-object v1

    invoke-virtual {v1}, LIK;->c()I

    move-result v1

    .line 434
    iget-object v2, p1, LCA;->a:Landroid/content/Context;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 435
    iget-object v1, p1, LCA;->e:Lcom/google/android/apps/docs/view/FixedSizeTextView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/view/FixedSizeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 436
    iget-object v1, p1, LCA;->e:Lcom/google/android/apps/docs/view/FixedSizeTextView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/view/FixedSizeTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 437
    return-void
.end method

.method private d(LCA;)V
    .locals 1

    .prologue
    .line 440
    iget-object v0, p1, LCA;->a:Lcom/google/android/apps/docs/view/FixedSizeImageView;

    invoke-virtual {p0, v0}, LAM;->a(Lcom/google/android/apps/docs/view/FixedSizeImageView;)V

    .line 441
    return-void
.end method


# virtual methods
.method protected a(Landroid/view/View;LaGA;Z)V
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, LAM;->b:LAJ;

    invoke-static {v0, p1}, LAJ;->a(LAJ;Landroid/view/View;)LBe;

    move-result-object v0

    check-cast v0, LCA;

    .line 405
    invoke-direct {p0, v0}, LAM;->a(LCA;)V

    .line 406
    invoke-direct {p0, v0}, LAM;->b(LCA;)V

    .line 407
    invoke-direct {p0, v0}, LAM;->c(LCA;)V

    .line 408
    invoke-direct {p0, v0}, LAM;->d(LCA;)V

    .line 412
    return-void
.end method
