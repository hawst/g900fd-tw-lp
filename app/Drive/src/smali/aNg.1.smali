.class public final LaNg;
.super Ljava/lang/Object;


# instance fields
.field private final a:LaMN;

.field private a:LaMU;

.field private final a:LaNN;

.field private final a:Landroid/content/Context;

.field private a:LayK;

.field private a:LayM;

.field private a:Lays;

.field private a:Layz;

.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-static {}, LaMN;->a()LaMN;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LaNg;-><init>(Landroid/content/Context;LaMN;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LaMN;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, LaNN;

    invoke-direct {v0}, LaNN;-><init>()V

    iput-object v0, p0, LaNg;->a:LaNN;

    iput-object p1, p0, LaNg;->a:Landroid/content/Context;

    iput-object p2, p0, LaNg;->a:LaMN;

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, LaNg;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, LaNg;->c(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, LaNg;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/internal/aq;

    invoke-direct {v1}, Lcom/google/android/gms/internal/aq;-><init>()V

    iget-object v2, p0, LaNg;->a:Ljava/lang/String;

    iget-object v3, p0, LaNg;->a:LaNN;

    invoke-static {v0, v1, v2, v3}, LaML;->a(Landroid/content/Context;Lcom/google/android/gms/internal/aq;Ljava/lang/String;LaNN;)LaMU;

    move-result-object v0

    iput-object v0, p0, LaNg;->a:LaMU;

    iget-object v0, p0, LaNg;->a:Lays;

    if-eqz v0, :cond_1

    iget-object v0, p0, LaNg;->a:LaMU;

    new-instance v1, LaMK;

    iget-object v2, p0, LaNg;->a:Lays;

    invoke-direct {v1, v2}, LaMK;-><init>(Lays;)V

    invoke-interface {v0, v1}, LaMU;->a(LaMR;)V

    :cond_1
    iget-object v0, p0, LaNg;->a:Layz;

    if-eqz v0, :cond_2

    iget-object v0, p0, LaNg;->a:LaMU;

    new-instance v1, LaMP;

    iget-object v2, p0, LaNg;->a:Layz;

    invoke-direct {v1, v2}, LaMP;-><init>(Layz;)V

    invoke-interface {v0, v1}, LaMU;->a(LaNa;)V

    :cond_2
    iget-object v0, p0, LaNg;->a:LayK;

    if-eqz v0, :cond_3

    iget-object v0, p0, LaNg;->a:LaMU;

    new-instance v1, LaOS;

    iget-object v2, p0, LaNg;->a:LayK;

    invoke-direct {v1, v2}, LaOS;-><init>(LayK;)V

    invoke-interface {v0, v1}, LaMU;->a(LaOH;)V

    :cond_3
    iget-object v0, p0, LaNg;->a:LayM;

    if-eqz v0, :cond_4

    iget-object v0, p0, LaNg;->a:LaMU;

    new-instance v1, LaOV;

    iget-object v2, p0, LaNg;->a:LayM;

    invoke-direct {v1, v2}, LaOV;-><init>(LayM;)V

    iget-object v2, p0, LaNg;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LaMU;->a(LaOP;Ljava/lang/String;)V

    :cond_4
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, LaNg;->a:LaMU;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The ad unit ID must be set on InterstitialAd before "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is called."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    :try_start_0
    const-string v0, "show"

    invoke-direct {p0, v0}, LaNg;->c(Ljava/lang/String;)V

    iget-object v0, p0, LaNg;->a:LaMU;

    invoke-interface {v0}, LaMU;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Failed to show interstitial."

    invoke-static {v1, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public a(LaNd;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, LaNg;->a:LaMU;

    if-nez v0, :cond_0

    const-string v0, "loadAd"

    invoke-direct {p0, v0}, LaNg;->b(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, LaNg;->a:LaMU;

    iget-object v1, p0, LaNg;->a:LaMN;

    iget-object v2, p0, LaNg;->a:Landroid/content/Context;

    invoke-virtual {v1, v2, p1}, LaMN;->a(Landroid/content/Context;LaNd;)Lcom/google/android/gms/internal/an;

    move-result-object v1

    invoke-interface {v0, v1}, LaMU;->a(Lcom/google/android/gms/internal/an;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LaNg;->a:LaNN;

    invoke-virtual {p1}, LaNd;->a()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, LaNN;->a(Ljava/util/Map;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Failed to load ad."

    invoke-static {v1, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public a(Lays;)V
    .locals 2

    :try_start_0
    iput-object p1, p0, LaNg;->a:Lays;

    iget-object v0, p0, LaNg;->a:LaMU;

    if-eqz v0, :cond_0

    iget-object v1, p0, LaNg;->a:LaMU;

    if-eqz p1, :cond_1

    new-instance v0, LaMK;

    invoke-direct {v0, p1}, LaMK;-><init>(Lays;)V

    :goto_0
    invoke-interface {v1, v0}, LaMU;->a(LaMR;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Failed to set the AdListener."

    invoke-static {v1, v0}, LaQc;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, LaNg;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The ad unit ID can only be set once on InterstitialAd."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, LaNg;->a:Ljava/lang/String;

    return-void
.end method
