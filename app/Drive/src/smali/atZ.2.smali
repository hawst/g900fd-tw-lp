.class public LatZ;
.super Ljava/lang/Object;
.source "PdfViewerActivity.java"

# interfaces
.implements LawC;


# instance fields
.field final synthetic a:Lcom/google/android/apps/viewer/PdfViewerActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/viewer/PdfViewerActivity;)V
    .locals 0

    .prologue
    .line 252
    iput-object p1, p0, LatZ;->a:Lcom/google/android/apps/viewer/PdfViewerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/viewer/PdfViewerActivity;LatV;)V
    .locals 0

    .prologue
    .line 252
    invoke-direct {p0, p1}, LatZ;-><init>(Lcom/google/android/apps/viewer/PdfViewerActivity;)V

    return-void
.end method


# virtual methods
.method public a(Lavg;)LawZ;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 255
    sget-object v0, Lavg;->d:Lavg;

    if-ne p1, v0, :cond_0

    .line 256
    new-instance v0, LawZ;

    invoke-direct {v0}, LawZ;-><init>()V

    .line 263
    :goto_0
    return-object v0

    .line 259
    :cond_0
    const-string v0, "PdfViewerActivity"

    const-string v1, "Can\'t handle files of type %s. Bail out."

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    iget-object v0, p0, LatZ;->a:Lcom/google/android/apps/viewer/PdfViewerActivity;

    iget-object v1, p0, LatZ;->a:Lcom/google/android/apps/viewer/PdfViewerActivity;

    sget v2, Laum;->error_file_format_pdf:I

    .line 261
    invoke-virtual {v1, v2}, Lcom/google/android/apps/viewer/PdfViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 260
    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 261
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 262
    iget-object v0, p0, LatZ;->a:Lcom/google/android/apps/viewer/PdfViewerActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/viewer/PdfViewerActivity;->finish()V

    .line 263
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic a(Lavg;)Laww;
    .locals 1

    .prologue
    .line 252
    invoke-virtual {p0, p1}, LatZ;->a(Lavg;)LawZ;

    move-result-object v0

    return-object v0
.end method
