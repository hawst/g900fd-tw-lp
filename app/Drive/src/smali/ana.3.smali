.class public Lana;
.super Ljava/lang/Object;
.source "ThumbnailFetcherImpl.java"

# interfaces
.implements LamY;
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:LamW;

.field private a:LamZ;

.field private a:Landroid/graphics/Bitmap;

.field private a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

.field private final a:Lcom/google/android/apps/docs/utils/BitmapUtilities;

.field private a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

.field private a:Ljava/lang/Thread;

.field private volatile a:Z


# direct methods
.method public constructor <init>(LamW;Lcom/google/android/apps/docs/utils/BitmapUtilities;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lana;->a:LamW;

    .line 48
    iput-object p2, p0, Lana;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities;

    .line 49
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 132
    iget-object v0, p0, Lana;->a:LamW;

    iget-object v1, p0, Lana;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    iget-object v2, p0, Lana;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, LamW;->a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;Z)Landroid/net/Uri;

    move-result-object v0

    .line 133
    iget-object v1, p0, Lana;->a:LamW;

    iget-object v2, p0, Lana;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    iget-object v2, v2, Lcom/google/android/gms/drive/database/data/ResourceSpec;->a:LaFO;

    .line 134
    invoke-interface {v1, v0, v2}, LamW;->a(Landroid/net/Uri;LaFO;)Ljava/io/InputStream;

    move-result-object v1

    .line 137
    :try_start_0
    iget-boolean v0, p0, Lana;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 145
    if-eqz v1, :cond_0

    .line 146
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    :try_start_1
    iget-object v0, p0, Lana;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities;

    iget-object v2, p0, Lana;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    const/4 v3, -0x1

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/docs/utils/BitmapUtilities;->a(Ljava/io/InputStream;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lana;->a:Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 143
    const/4 v0, 0x0

    .line 145
    if-eqz v0, :cond_0

    .line 146
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    goto :goto_0

    .line 145
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 146
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_2
    throw v0
.end method

.method private b(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;LamZ;)V
    .locals 1

    .prologue
    .line 77
    iput-object p1, p0, Lana;->a:Lcom/google/android/gms/drive/database/data/ResourceSpec;

    .line 78
    iput-object p2, p0, Lana;->a:Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;

    .line 79
    iput-object p3, p0, Lana;->a:LamZ;

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lana;->a:Landroid/graphics/Bitmap;

    .line 82
    const/4 v0, 0x0

    iput-boolean v0, p0, Lana;->a:Z

    .line 83
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lana;->a:Z

    .line 73
    return-void
.end method

.method public a(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;LamZ;)V
    .locals 2

    .prologue
    .line 55
    invoke-virtual {p0}, Lana;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ThumbnailFetcher is busy"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lana;->b(Lcom/google/android/gms/drive/database/data/ResourceSpec;Lcom/google/android/apps/docs/utils/BitmapUtilities$Dimension;LamZ;)V

    .line 61
    new-instance v0, Ljava/lang/Thread;

    const-string v1, "Fetch thumbnail"

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v0, p0, Lana;->a:Ljava/lang/Thread;

    .line 62
    iget-object v0, p0, Lana;->a:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 63
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lana;->a:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lana;->a:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 87
    const/4 v0, 0x0

    .line 90
    :try_start_0
    invoke-direct {p0}, Lana;->b()V
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lbxk; {:try_start_0 .. :try_end_0} :catch_2
    .catch LbwO; {:try_start_0 .. :try_end_0} :catch_3
    .catch LTr; {:try_start_0 .. :try_end_0} :catch_4
    .catch LTt; {:try_start_0 .. :try_end_0} :catch_5
    .catch LaoW; {:try_start_0 .. :try_end_0} :catch_6

    .line 110
    :goto_0
    iget-boolean v1, p0, Lana;->a:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lana;->a:LamZ;

    if-eqz v1, :cond_1

    .line 111
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lana;->a:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    .line 112
    const-string v0, "Fail to fetch/process the thumbnail for unknown reason"

    .line 115
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 116
    iget-object v0, p0, Lana;->a:LamZ;

    iget-object v1, p0, Lana;->a:Landroid/graphics/Bitmap;

    invoke-interface {v0, v1}, LamZ;->a(Landroid/graphics/Bitmap;)V

    .line 122
    :cond_1
    :goto_1
    return-void

    .line 91
    :catch_0
    move-exception v0

    .line 92
    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 93
    :catch_1
    move-exception v0

    .line 94
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 95
    :catch_2
    move-exception v0

    .line 96
    invoke-virtual {v0}, Lbxk;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 97
    :catch_3
    move-exception v0

    .line 98
    invoke-virtual {v0}, LbwO;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 99
    :catch_4
    move-exception v0

    .line 100
    invoke-virtual {v0}, LTr;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 101
    :catch_5
    move-exception v0

    .line 102
    invoke-virtual {v0}, LTt;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 103
    :catch_6
    move-exception v0

    .line 104
    invoke-virtual {v0}, LaoW;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 118
    :cond_2
    const-string v1, "thumbnail"

    invoke-static {v1, v0}, LalV;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    iget-object v1, p0, Lana;->a:LamZ;

    invoke-interface {v1, v0}, LamZ;->a(Ljava/lang/String;)V

    goto :goto_1
.end method
