.class public final Lwe;
.super Ljava/lang/Object;
.source "KindFilterCriterion.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;
    .locals 2

    .prologue
    .line 89
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/EnumSet;

    .line 90
    new-instance v1, Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;

    invoke-static {v0}, LbmY;->a(Ljava/util/Collection;)LbmY;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;-><init>(LbmY;)V

    return-object v1
.end method

.method public a(I)[Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;
    .locals 1

    .prologue
    .line 95
    new-array v0, p1, [Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lwe;->a(Landroid/os/Parcel;)Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lwe;->a(I)[Lcom/google/android/apps/docs/app/model/navigation/KindFilterCriterion;

    move-result-object v0

    return-object v0
.end method
