.class public final LaJI;
.super Ljava/lang/Object;


# static fields
.field private static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LaJt",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LaJI;->a:Ljava/util/Map;

    sget-object v0, LaSu;->a:LaJt;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->a:LaSC;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->a:LaSy;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->a:LaSB;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->a:LaSD;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->k:LaJt;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->a:LaSx;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->i:LaJt;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->m:LaJt;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->a:LaSz;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->b:LaJt;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->a:LaJz;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->c:LaJt;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->j:LaJt;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->d:LaJt;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->e:LaJt;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->f:LaJt;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->o:LaJt;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->l:LaJt;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->p:LaJt;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->a:LaSA;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->r:LaJt;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->s:LaJt;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->h:LaJt;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->g:LaJt;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->q:LaJt;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->n:LaJt;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->a:LaSw;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSu;->t:LaJt;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSF;->a:LaSG;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSF;->a:LaSJ;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSF;->a:LaSI;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSF;->a:LaSK;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSF;->a:LaSH;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSM;->a:LaJt;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    sget-object v0, LaSM;->b:LaJt;

    invoke-static {v0}, LaJI;->a(LaJt;)V

    return-void
.end method

.method public static a(Ljava/lang/String;)LaJt;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LaJt",
            "<*>;"
        }
    .end annotation

    sget-object v0, LaJI;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaJt;

    return-object v0
.end method

.method private static a(LaJt;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaJt",
            "<*>;)V"
        }
    .end annotation

    sget-object v0, LaJI;->a:Ljava/util/Map;

    invoke-interface {p0}, LaJt;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Duplicate field name registered: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p0}, LaJt;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-object v0, LaJI;->a:Ljava/util/Map;

    invoke-interface {p0}, LaJt;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
