.class LUn;
.super Ljava/lang/Object;
.source "OperationQueueMetadataChanger.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:LUl;

.field final synthetic a:LaHy;

.field final synthetic a:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field final synthetic b:Lcom/google/android/gms/drive/database/data/EntrySpec;


# direct methods
.method constructor <init>(LUl;Lcom/google/android/gms/drive/database/data/EntrySpec;LaHy;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, LUn;->a:LUl;

    iput-object p2, p0, LUn;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object p3, p0, LUn;->a:LaHy;

    iput-object p4, p0, LUn;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 212
    iget-object v0, p0, LUn;->a:LUl;

    invoke-static {v0}, LUl;->a(LUl;)LaGg;

    move-result-object v0

    iget-object v1, p0, LUn;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-interface {v0, v1}, LaGg;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)LaGd;

    move-result-object v0

    .line 213
    if-nez v0, :cond_0

    .line 214
    iget-object v0, p0, LUn;->a:LaHy;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LaHy;->a(ILjava/lang/Throwable;)V

    .line 225
    :goto_0
    return-void

    .line 218
    :cond_0
    iget-object v1, p0, LUn;->a:LUl;

    invoke-static {v1}, LUl;->a(LUl;)LahB;

    move-result-object v1

    invoke-virtual {v0}, LaGd;->a()Lcom/google/android/gms/drive/database/data/DatabaseEntrySpec;

    move-result-object v2

    invoke-interface {v1, v2}, LahB;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    .line 220
    invoke-virtual {v0}, LaGd;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 221
    iget-object v1, p0, LUn;->a:LUl;

    iget-object v2, p0, LUn;->a:LaHy;

    invoke-static {v1, v0, v2}, LUl;->a(LUl;LaGd;LaHy;)V

    goto :goto_0

    .line 223
    :cond_1
    iget-object v1, p0, LUn;->a:LUl;

    iget-object v2, p0, LUn;->b:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v3, p0, LUn;->a:LaHy;

    invoke-virtual {v1, v0, v2, v3}, LUl;->a(LaGd;Lcom/google/android/gms/drive/database/data/EntrySpec;LaHy;)V

    goto :goto_0
.end method
