.class public abstract LyR;
.super Landroid/widget/BaseAdapter;
.source "BaseCard.java"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 12
    const/4 v0, 0x1

    iput-boolean v0, p0, LyR;->a:Z

    return-void
.end method


# virtual methods
.method public abstract a()Landroid/view/View;
.end method

.method public abstract a()Z
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, LyR;->a:Z

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, LyR;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LyR;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 37
    return-object p0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 42
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, LyR;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x1

    return v0
.end method
