.class public abstract LTG;
.super Ljava/lang/Object;
.source "HttpIssuerBase.java"

# interfaces
.implements LTF;


# instance fields
.field private a:I

.field private a:LTK;

.field private final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "LTJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, LTG;->a:Ljava/lang/ThreadLocal;

    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, LTG;->a:LTK;

    .line 102
    const/4 v0, 0x0

    iput v0, p0, LTG;->a:I

    return-void
.end method

.method static synthetic a(LTG;LTJ;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, LTG;->b(LTJ;)V

    return-void
.end method

.method private a(LTJ;)V
    .locals 1

    .prologue
    .line 140
    invoke-virtual {p1}, LTJ;->a()LTN;

    move-result-object v0

    .line 141
    if-nez v0, :cond_1

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 144
    :cond_1
    invoke-virtual {v0}, LTN;->a()LTL;

    move-result-object v0

    .line 145
    if-eqz v0, :cond_0

    .line 147
    :try_start_0
    invoke-virtual {v0}, LTL;->consumeContent()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 148
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(Lorg/apache/http/params/HttpParams;)V
    .locals 1

    .prologue
    .line 266
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lorg/apache/http/conn/params/ConnManagerParams;->setMaxTotalConnections(Lorg/apache/http/params/HttpParams;I)V

    .line 267
    new-instance v0, LTI;

    invoke-direct {v0, p0}, LTI;-><init>(LTG;)V

    invoke-static {p1, v0}, Lorg/apache/http/conn/params/ConnManagerParams;->setMaxConnectionsPerRoute(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/params/ConnPerRoute;)V

    .line 273
    return-void
.end method

.method private declared-synchronized b()LTK;
    .locals 6

    .prologue
    const/16 v1, 0xa

    .line 248
    monitor-enter p0

    :try_start_0
    iget v0, p0, LTG;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LTG;->a:I

    if-le v0, v1, :cond_0

    .line 249
    const-string v0, "HttpIssuerBase"

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    const-string v2, "HttpIssuer connection leak, number of active connections exceeded %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/16 v5, 0xa

    .line 251
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 249
    invoke-static {v0, v1, v2, v3}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 252
    const/4 v0, 0x0

    iput-object v0, p0, LTG;->a:LTK;

    .line 253
    const/4 v0, 0x1

    iput v0, p0, LTG;->a:I

    .line 255
    :cond_0
    iget-object v0, p0, LTG;->a:LTK;

    if-nez v0, :cond_1

    .line 256
    invoke-virtual {p0}, LTG;->a()LTK;

    move-result-object v0

    iput-object v0, p0, LTG;->a:LTK;

    .line 257
    iget-object v0, p0, LTG;->a:LTK;

    invoke-interface {v0}, LTK;->a()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    invoke-direct {p0, v0}, LTG;->a(Lorg/apache/http/params/HttpParams;)V

    .line 259
    :cond_1
    iget-object v0, p0, LTG;->a:LTK;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 248
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    .locals 4

    .prologue
    .line 118
    const/4 v1, 0x0

    .line 120
    :try_start_0
    new-instance v0, LTN;

    invoke-direct {p0}, LTG;->b()LTK;

    move-result-object v2

    invoke-interface {v2, p1}, LTK;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v2

    invoke-direct {v0, v2}, LTN;-><init>(Lorg/apache/http/HttpResponse;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    if-eqz v0, :cond_0

    .line 124
    iget-object v1, p0, LTG;->a:Ljava/lang/ThreadLocal;

    new-instance v2, LTJ;

    invoke-direct {v2, p1, v0}, LTJ;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;LTN;)V

    invoke-virtual {v1, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :cond_0
    return-object v0

    .line 123
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 124
    iget-object v2, p0, LTG;->a:Ljava/lang/ThreadLocal;

    new-instance v3, LTJ;

    invoke-direct {v3, p1, v1}, LTJ;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;LTN;)V

    invoke-virtual {v2, v3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :cond_1
    throw v0
.end method

.method private b(LTJ;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 196
    invoke-static {p1}, LbiT;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    const-wide/16 v4, 0x0

    .line 200
    :try_start_0
    const-string v0, "HttpIssuerBase"

    const-string v3, "Start closing time"

    invoke-static {v0, v3}, LalV;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    invoke-direct {p0}, LTG;->d()V

    .line 202
    invoke-virtual {p1}, LTJ;->a()Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    .line 203
    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    .line 213
    invoke-direct {p0, p1}, LTG;->a(LTJ;)V

    .line 221
    invoke-virtual {p1}, LTJ;->a()V

    .line 223
    :goto_0
    return-void

    .line 206
    :cond_0
    :try_start_1
    invoke-virtual {p1}, LTJ;->a()LTN;

    move-result-object v3

    .line 207
    if-eqz v3, :cond_1

    invoke-virtual {v3}, LTN;->a()Z

    move-result v3

    if-nez v3, :cond_3

    .line 208
    :cond_1
    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    .line 209
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v4

    move v0, v2

    .line 213
    :goto_1
    invoke-direct {p0, p1}, LTG;->a(LTJ;)V

    .line 214
    if-eqz v0, :cond_2

    .line 215
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long v4, v6, v4

    .line 216
    const-wide/16 v6, 0x3e8

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    .line 217
    const-string v0, "HttpIssuerBase"

    new-instance v3, Ljava/io/IOException;

    invoke-direct {v3}, Ljava/io/IOException;-><init>()V

    const-string v6, "Excessive delay between abort and stream closure: %sms"

    new-array v2, v2, [Ljava/lang/Object;

    .line 218
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v1

    .line 217
    invoke-static {v0, v3, v6, v2}, LalV;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 221
    :cond_2
    invoke-virtual {p1}, LTJ;->a()V

    goto :goto_0

    .line 213
    :catchall_0
    move-exception v0

    invoke-direct {p0, p1}, LTG;->a(LTJ;)V

    .line 221
    invoke-virtual {p1}, LTJ;->a()V

    throw v0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private declared-synchronized d()V
    .locals 1

    .prologue
    .line 276
    monitor-enter p0

    :try_start_0
    iget v0, p0, LTG;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LTG;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 277
    monitor-exit p0

    return-void

    .line 276
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method protected abstract a()LTK;
.end method

.method public a()Ljava/io/Closeable;
    .locals 3

    .prologue
    .line 156
    .line 158
    :try_start_0
    iget-object v0, p0, LTG;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTJ;

    .line 159
    if-nez v0, :cond_0

    .line 160
    const-string v0, "HttpIssuerBase"

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    const-string v2, "Attempt to detach a request when no request is executing."

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 162
    sget-object v0, Lalc;->a:Lalc;

    .line 179
    :goto_0
    return-object v0

    .line 164
    :cond_0
    new-instance v1, LTH;

    invoke-direct {v1, p0, v0}, LTH;-><init>(LTG;LTJ;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179
    iget-object v0, p0, LTG;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    move-object v0, v1

    goto :goto_0

    .line 178
    :catchall_0
    move-exception v0

    .line 179
    throw v0
.end method

.method public a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, LTG;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 108
    invoke-direct {p0, p1}, LTG;->b(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0

    .line 110
    :cond_0
    new-instance v1, Ljava/io/IOException;

    const-string v0, "More than 1 active request per thread is not allowed."

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, LTG;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTJ;

    invoke-virtual {v0}, LTJ;->a()Ljava/io/IOException;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 113
    throw v1
.end method

.method public a()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, LTG;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTJ;

    .line 132
    if-nez v0, :cond_0

    .line 133
    const-string v0, "HttpIssuerBase"

    const-string v1, "Attempt to consume entity of HttpIssuer when no request is executing."

    invoke-static {v0, v1}, LalV;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    :goto_0
    return-void

    .line 136
    :cond_0
    invoke-direct {p0, v0}, LTG;->a(LTJ;)V

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 186
    iget-object v0, p0, LTG;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTJ;

    .line 187
    if-nez v0, :cond_0

    .line 188
    const-string v0, "HttpIssuerBase"

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    const-string v2, "Attempt to close HttpIssuer when no request is executing."

    invoke-static {v0, v1, v2}, LalV;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 193
    :goto_0
    return-void

    .line 191
    :cond_0
    invoke-direct {p0, v0}, LTG;->b(LTJ;)V

    .line 192
    iget-object v0, p0, LTG;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    goto :goto_0
.end method

.method public declared-synchronized c()V
    .locals 1

    .prologue
    .line 227
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LTG;->a:LTK;

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, LTG;->a:LTK;

    invoke-interface {v0}, LTK;->a()V

    .line 229
    const/4 v0, 0x0

    iput v0, p0, LTG;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 231
    :cond_0
    monitor-exit p0

    return-void

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
