.class public LalM;
.super Ljava/lang/Object;
.source "ImmutableSparseArray.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:LalK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LalK",
            "<TT;>;"
        }
    .end annotation
.end field

.field private a:Z


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LalL;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, LalM;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, LalM;->a:LalK;

    if-nez v0, :cond_0

    .line 83
    new-instance v0, LalK;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LalK;-><init>(LalL;)V

    iput-object v0, p0, LalM;->a:LalK;

    .line 85
    :cond_0
    iget-boolean v0, p0, LalM;->a:Z

    if-eqz v0, :cond_1

    .line 86
    iget-object v0, p0, LalM;->a:LalK;

    invoke-static {v0}, LalK;->a(Landroid/util/SparseArray;)LalK;

    move-result-object v0

    iput-object v0, p0, LalM;->a:LalK;

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, LalM;->a:Z

    .line 89
    :cond_1
    return-void
.end method


# virtual methods
.method public a()LalK;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LalK",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, LalM;->a:LalK;

    if-nez v0, :cond_0

    .line 74
    invoke-static {}, LalK;->a()LalK;

    move-result-object v0

    .line 78
    :goto_0
    return-object v0

    .line 76
    :cond_0
    invoke-direct {p0}, LalM;->a()V

    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, LalM;->a:Z

    .line 78
    iget-object v0, p0, LalM;->a:LalK;

    goto :goto_0
.end method

.method public a(ILjava/lang/Object;)LalM;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)",
            "LalM",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0}, LalM;->a()V

    .line 67
    iget-object v0, p0, LalM;->a:LalK;

    invoke-static {v0, p1, p2}, LalK;->a(LalK;ILjava/lang/Object;)V

    .line 68
    return-object p0
.end method
